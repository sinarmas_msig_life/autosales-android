package id.co.ajsmsig.nb.espaj.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 30/08/2017.
 */

public class DetilAgenModel implements Parcelable {

    private String tgl_spaj_da = "";
    private String nm_regional_da = "";
    private String kd_regional_da = "";
    private String kd_leader_da = "";
    private String nmlead_pnutup_da = "";
    private String kd_penutup_da = "";
    private String nm_pnutup_da = "";
    private int check_kd_ao_da = 0;
    private String kd_ao_da = "";
    private int check_kd_pribadi_da = 0;
    private String kd_pnagihan_da = "";
    private String nm_Regional_Penagihan_da = "";
    private int check_broker_da = 0;
    private String broker_da = "";
    private String nama_broker_da = "";
    private int bank_da = 0;
    private String norek_da = "";
    private int pnytaan_setuju_da = 0;
    private String id_sponsor_da = "";
    private String id_penempatan_da = "";
    private String id_member_da = "";
    private String id_refferal_da = "";
    private String nm_refferal_da = "";
    private String id_cabang_da = "";
    private String nm_cabang_da = "";
    private String email_da = "";
    private String lca_id;
    private String lwk_id;
    private String lsrg_id;
    private String KODE_REGIONAL = "";
    private int JENIS_LOGIN = 0;
    private int JENIS_LOGIN_BC = 0;
    private Boolean validation = false;
    private int group_id_da = 0;

    public DetilAgenModel() {
    }

    protected DetilAgenModel(Parcel in) {
        tgl_spaj_da = in.readString();
        nm_regional_da = in.readString();
        kd_regional_da = in.readString();
        kd_leader_da = in.readString();
        nmlead_pnutup_da = in.readString();
        kd_penutup_da = in.readString();
        nm_pnutup_da = in.readString();
        check_kd_ao_da = in.readInt();
        kd_ao_da = in.readString();
        check_kd_pribadi_da = in.readInt();
        kd_pnagihan_da = in.readString();
        nm_Regional_Penagihan_da = in.readString();
        check_broker_da = in.readInt();
        broker_da = in.readString();
        nama_broker_da = in.readString();
        bank_da = in.readInt();
        norek_da = in.readString();
        pnytaan_setuju_da = in.readInt();
        id_sponsor_da = in.readString();
        id_penempatan_da = in.readString();
        id_member_da = in.readString();
        id_refferal_da = in.readString();
        nm_refferal_da = in.readString();
        id_cabang_da = in.readString();
        nm_cabang_da = in.readString();
        email_da = in.readString();
        lca_id = in.readString();
        lwk_id = in.readString();
        lsrg_id = in.readString();
        KODE_REGIONAL = in.readString();
        JENIS_LOGIN = in.readInt();
        JENIS_LOGIN_BC = in.readInt();
        byte tmpValidation = in.readByte();
        validation = tmpValidation == 0 ? null : tmpValidation == 1;
        group_id_da = in.readInt();
    }

    public static final Creator<DetilAgenModel> CREATOR = new Creator<DetilAgenModel>() {
        @Override
        public DetilAgenModel createFromParcel(Parcel in) {
            return new DetilAgenModel(in);
        }

        @Override
        public DetilAgenModel[] newArray(int size) {
            return new DetilAgenModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(tgl_spaj_da);
        parcel.writeString(nm_regional_da);
        parcel.writeString(kd_regional_da);
        parcel.writeString(kd_leader_da);
        parcel.writeString(nmlead_pnutup_da);
        parcel.writeString(kd_penutup_da);
        parcel.writeString(nm_pnutup_da);
        parcel.writeInt(check_kd_ao_da);
        parcel.writeString(kd_ao_da);
        parcel.writeInt(check_kd_pribadi_da);
        parcel.writeString(kd_pnagihan_da);
        parcel.writeString(nm_Regional_Penagihan_da);
        parcel.writeInt(check_broker_da);
        parcel.writeString(broker_da);
        parcel.writeString(nama_broker_da);
        parcel.writeInt(bank_da);
        parcel.writeString(norek_da);
        parcel.writeInt(pnytaan_setuju_da);
        parcel.writeString(id_sponsor_da);
        parcel.writeString(id_penempatan_da);
        parcel.writeString(id_member_da);
        parcel.writeString(id_refferal_da);
        parcel.writeString(nm_refferal_da);
        parcel.writeString(id_cabang_da);
        parcel.writeString(nm_cabang_da);
        parcel.writeString(email_da);
        parcel.writeString(lca_id);
        parcel.writeString(lwk_id);
        parcel.writeString(lsrg_id);
        parcel.writeString(KODE_REGIONAL);
        parcel.writeInt(JENIS_LOGIN);
        parcel.writeInt(JENIS_LOGIN_BC);
        parcel.writeByte((byte) (validation == null ? 0 : validation ? 1 : 2));
        parcel.writeInt(group_id_da);
    }

    public String getTgl_spaj_da() {
        return tgl_spaj_da;
    }

    public void setTgl_spaj_da(String tgl_spaj_da) {
        this.tgl_spaj_da = tgl_spaj_da;
    }

    public String getNm_regional_da() {
        return nm_regional_da;
    }

    public void setNm_regional_da(String nm_regional_da) {
        this.nm_regional_da = nm_regional_da;
    }

    public String getKd_regional_da() {
        return kd_regional_da;
    }

    public void setKd_regional_da(String kd_regional_da) {
        this.kd_regional_da = kd_regional_da;
    }

    public String getKd_leader_da() {
        return kd_leader_da;
    }

    public void setKd_leader_da(String kd_leader_da) {
        this.kd_leader_da = kd_leader_da;
    }

    public String getNmlead_pnutup_da() {
        return nmlead_pnutup_da;
    }

    public void setNmlead_pnutup_da(String nmlead_pnutup_da) {
        this.nmlead_pnutup_da = nmlead_pnutup_da;
    }

    public String getKd_penutup_da() {
        return kd_penutup_da;
    }

    public void setKd_penutup_da(String kd_penutup_da) {
        this.kd_penutup_da = kd_penutup_da;
    }

    public String getNm_pnutup_da() {
        return nm_pnutup_da;
    }

    public void setNm_pnutup_da(String nm_pnutup_da) {
        this.nm_pnutup_da = nm_pnutup_da;
    }

    public int getCheck_kd_ao_da() {
        return check_kd_ao_da;
    }

    public void setCheck_kd_ao_da(int check_kd_ao_da) {
        this.check_kd_ao_da = check_kd_ao_da;
    }

    public String getKd_ao_da() {
        return kd_ao_da;
    }

    public void setKd_ao_da(String kd_ao_da) {
        this.kd_ao_da = kd_ao_da;
    }

    public int getCheck_kd_pribadi_da() {
        return check_kd_pribadi_da;
    }

    public void setCheck_kd_pribadi_da(int check_kd_pribadi_da) {
        this.check_kd_pribadi_da = check_kd_pribadi_da;
    }

    public String getKd_pnagihan_da() {
        return kd_pnagihan_da;
    }

    public void setKd_pnagihan_da(String kd_pnagihan_da) {
        this.kd_pnagihan_da = kd_pnagihan_da;
    }

    public String getNm_Regional_Penagihan_da() {
        return nm_Regional_Penagihan_da;
    }

    public void setNm_Regional_Penagihan_da(String nm_Regional_Penagihan_da) {
        this.nm_Regional_Penagihan_da = nm_Regional_Penagihan_da;
    }

    public int getCheck_broker_da() {
        return check_broker_da;
    }

    public void setCheck_broker_da(int check_broker_da) {
        this.check_broker_da = check_broker_da;
    }

    public String getBroker_da() {
        return broker_da;
    }

    public void setBroker_da(String broker_da) {
        this.broker_da = broker_da;
    }

    public String getNama_broker_da() {
        return nama_broker_da;
    }

    public void setNama_broker_da(String nama_broker_da) {
        this.nama_broker_da = nama_broker_da;
    }

    public int getBank_da() {
        return bank_da;
    }

    public void setBank_da(int bank_da) {
        this.bank_da = bank_da;
    }

    public String getNorek_da() {
        return norek_da;
    }

    public void setNorek_da(String norek_da) {
        this.norek_da = norek_da;
    }

    public int getPnytaan_setuju_da() {
        return pnytaan_setuju_da;
    }

    public void setPnytaan_setuju_da(int pnytaan_setuju_da) {
        this.pnytaan_setuju_da = pnytaan_setuju_da;
    }

    public String getId_sponsor_da() {
        return id_sponsor_da;
    }

    public void setId_sponsor_da(String id_sponsor_da) {
        this.id_sponsor_da = id_sponsor_da;
    }

    public String getId_penempatan_da() {
        return id_penempatan_da;
    }

    public void setId_penempatan_da(String id_penempatan_da) {
        this.id_penempatan_da = id_penempatan_da;
    }

    public String getId_member_da() {
        return id_member_da;
    }

    public void setId_member_da(String id_member_da) {
        this.id_member_da = id_member_da;
    }

    public String getId_refferal_da() {
        return id_refferal_da;
    }

    public void setId_refferal_da(String id_refferal_da) {
        this.id_refferal_da = id_refferal_da;
    }

    public String getNm_refferal_da() {
        return nm_refferal_da;
    }

    public void setNm_refferal_da(String nm_refferal_da) {
        this.nm_refferal_da = nm_refferal_da;
    }

    public String getId_cabang_da() {
        return id_cabang_da;
    }

    public void setId_cabang_da(String id_cabang_da) {
        this.id_cabang_da = id_cabang_da;
    }

    public String getNm_cabang_da() {
        return nm_cabang_da;
    }

    public void setNm_cabang_da(String nm_cabang_da) {
        this.nm_cabang_da = nm_cabang_da;
    }

    public String getEmail_da() {
        return email_da;
    }

    public void setEmail_da(String email_da) {
        this.email_da = email_da;
    }

    public String getLca_id() {
        return lca_id;
    }

    public void setLca_id(String lca_id) {
        this.lca_id = lca_id;
    }

    public String getLwk_id() {
        return lwk_id;
    }

    public void setLwk_id(String lwk_id) {
        this.lwk_id = lwk_id;
    }

    public String getLsrg_id() {
        return lsrg_id;
    }

    public void setLsrg_id(String lsrg_id) {
        this.lsrg_id = lsrg_id;
    }

    public String getKODE_REGIONAL() {
        return KODE_REGIONAL;
    }

    public void setKODE_REGIONAL(String KODE_REGIONAL) {
        this.KODE_REGIONAL = KODE_REGIONAL;
    }

    public int getJENIS_LOGIN() {
        return JENIS_LOGIN;
    }

    public void setJENIS_LOGIN(int JENIS_LOGIN) {
        this.JENIS_LOGIN = JENIS_LOGIN;
    }

    public int getJENIS_LOGIN_BC() {
        return JENIS_LOGIN_BC;
    }

    public void setJENIS_LOGIN_BC(int JENIS_LOGIN_BC) {
        this.JENIS_LOGIN_BC = JENIS_LOGIN_BC;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public int getGroup_id_da() {
        return group_id_da;
    }

    public void setGroup_id_da(int group_id_da) {
        this.group_id_da = group_id_da;
    }

    public static Creator<DetilAgenModel> getCREATOR() {
        return CREATOR;
    }
}
