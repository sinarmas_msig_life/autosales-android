package id.co.ajsmsig.nb.espaj.fragment;

import android.app.AlertDialog;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.database.E_Delete;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.method.NumberTextWatcher;
import id.co.ajsmsig.nb.espaj.method.ProgressDialogClass;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAskesUA;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_KetKesUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_NO9ppUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_NO9ttUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelMst_Answer;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTT_NO3UQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTT_NO5UQ;
import id.co.ajsmsig.nb.util.Const;

/**
 * Created by Bernard on 31/05/2017.
 */

/*Change from AppCompatActivity into fragment*/
public class E_QuestionnaireFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, RadioGroup.OnCheckedChangeListener {

    private Boolean radio3i = false, radio3ii = false, radio3iii = false, radio3iv = false, radio3v = false, radio3vi = false, radio3vii = false, radio3viii = false, radio3ix = false, radio3x = false;

    private TextView no_dk_ketkes_1, no_dk_ketkes_2, no_dk_ketkes_3, no_dk_ketkes_5, no_dk_ketkes_6, no_dk_ketkes_7,
            no_dk_ketkes_8, no_dk_ketkes_9, no_dk_ketkes_10;

    private TextView[] tv_no_dk_ketkes = {no_dk_ketkes_1, no_dk_ketkes_2, no_dk_ketkes_3, no_dk_ketkes_5, no_dk_ketkes_6,
            no_dk_ketkes_7, no_dk_ketkes_8, no_dk_ketkes_9, no_dk_ketkes_10};

    private EditText ket_dk_ketkes_1, ket_dk_ketkes_2, ket_dk_ketkes_3, ket_dk_ketkes_5, ket_dk_ketkes_6, ket_dk_ketkes_7,
            ket_dk_ketkes_8, ket_dk_ketkes_9, ket_dk_ketkes_10;

    private EditText[] ket_dk_ketkes = {ket_dk_ketkes_1, ket_dk_ketkes_2, ket_dk_ketkes_3, ket_dk_ketkes_5, ket_dk_ketkes_6,
            ket_dk_ketkes_7, ket_dk_ketkes_8, ket_dk_ketkes_9, ket_dk_ketkes_10};

    private EditText penyebab_dk_no9_pp_1, penyebab_dk_no9_pp_2, penyebab_dk_no9_pp_3, penyebab_dk_no9_pp_5, penyebab_dk_no9_pp_6, penyebab_dk_no9_pp_7,
            penyebab_dk_no9_pp_8, penyebab_dk_no9_pp_9, penyebab_dk_no9_pp_10;

    private EditText[] penyebab_dk_no9_pp = {penyebab_dk_no9_pp_1, penyebab_dk_no9_pp_2, penyebab_dk_no9_pp_3, penyebab_dk_no9_pp_5, penyebab_dk_no9_pp_6,
            penyebab_dk_no9_pp_7, penyebab_dk_no9_pp_8, penyebab_dk_no9_pp_9, penyebab_dk_no9_pp_10};

    private Spinner keadaan_dk_no9_pp_1, keadaan_dk_no9_pp_2, keadaan_dk_no9_pp_3, keadaan_dk_no9_pp_5, keadaan_dk_no9_pp_6, keadaan_dk_no9_pp_7,
            keadaan_dk_no9_pp_8, keadaan_dk_no9_pp_9, keadaan_dk_no9_pp_10;

    private Spinner[] keadaan_dk_no9_pp = {keadaan_dk_no9_pp_1, keadaan_dk_no9_pp_2, keadaan_dk_no9_pp_3, keadaan_dk_no9_pp_5, keadaan_dk_no9_pp_6,
            keadaan_dk_no9_pp_7, keadaan_dk_no9_pp_8, keadaan_dk_no9_pp_9, keadaan_dk_no9_pp_10};

    private EditText umur_dk_no9_pp_1, umur_dk_no9_pp_2, umur_dk_no9_pp_3, umur_dk_no9_pp_5, umur_dk_no9_pp_6, umur_dk_no9_pp_7,
            umur_dk_no9_pp_8, umur_dk_no9_pp_9, umur_dk_no9_pp_10;

    private EditText[] umur_dk_no9_pp = {umur_dk_no9_pp_1, umur_dk_no9_pp_2, umur_dk_no9_pp_3, umur_dk_no9_pp_5, umur_dk_no9_pp_6,
            umur_dk_no9_pp_7, umur_dk_no9_pp_8, umur_dk_no9_pp_9, umur_dk_no9_pp_10};

    private Spinner calon_dk_no9_pp_1, calon_dk_no9_pp_2, calon_dk_no9_pp_3, calon_dk_no9_pp_5, calon_dk_no9_pp_6, calon_dk_no9_pp_7,
            calon_dk_no9_pp_8, calon_dk_no9_pp_9, calon_dk_no9_pp_10;

    private Spinner[] calon_dk_no9_pp = {calon_dk_no9_pp_1, calon_dk_no9_pp_2, calon_dk_no9_pp_3, calon_dk_no9_pp_5, calon_dk_no9_pp_6,
            calon_dk_no9_pp_7, calon_dk_no9_pp_8, calon_dk_no9_pp_9, calon_dk_no9_pp_10};

    private Spinner calon_dk_no9_tt_1, calon_dk_no9_tt_2, calon_dk_no9_tt_3, calon_dk_no9_tt_5, calon_dk_no9_tt_6, calon_dk_no9_tt_7,
            calon_dk_no9_tt_8, calon_dk_no9_tt_9, calon_dk_no9_tt_10;

    private Spinner[] calon_dk_no9_tt = {calon_dk_no9_tt_1, calon_dk_no9_tt_2, calon_dk_no9_tt_3, calon_dk_no9_tt_5, calon_dk_no9_tt_6,
            calon_dk_no9_tt_7, calon_dk_no9_tt_8, calon_dk_no9_tt_9, calon_dk_no9_tt_10};

    private static ImageView img_calon_dk_no9_tt_1, img_calon_dk_no9_tt_2, img_calon_dk_no9_tt_3, img_calon_dk_no9_tt_5, img_calon_dk_no9_tt_6, img_calon_dk_no9_tt_7,
            img_calon_dk_no9_tt_8, img_calon_dk_no9_tt_9,img_calon_dk_no9_tt_10;

    private static ImageView [] img_calon_dk_no9_tt = {img_calon_dk_no9_tt_1, img_calon_dk_no9_tt_2, img_calon_dk_no9_tt_3, img_calon_dk_no9_tt_5, img_calon_dk_no9_tt_6,
            img_calon_dk_no9_tt_7, img_calon_dk_no9_tt_8, img_calon_dk_no9_tt_9,img_calon_dk_no9_tt_10};

    private static ImageView delete_adapter_dkno9_tt_1, delete_adapter_dkno9_tt_2, delete_adapter_dkno9_tt_3, delete_adapter_dkno9_tt_5, delete_adapter_dkno9_tt_6, delete_adapter_dkno9_tt_7,
            delete_adapter_dkno9_tt_8, delete_adapter_dkno9_tt_9,delete_adapter_dkno9_tt_10;

    private static ImageView [] delete_adapter_dkno9_tt = {delete_adapter_dkno9_tt_1, delete_adapter_dkno9_tt_2, delete_adapter_dkno9_tt_3, delete_adapter_dkno9_tt_5, delete_adapter_dkno9_tt_6,
            delete_adapter_dkno9_tt_7, delete_adapter_dkno9_tt_8, delete_adapter_dkno9_tt_9,delete_adapter_dkno9_tt_10};


    private static ImageView img_calon_dk_no9_pp_1, img_calon_dk_no9_pp_2, img_calon_dk_no9_pp_3, img_calon_dk_no9_pp_5, img_calon_dk_no9_pp_6, img_calon_dk_no9_pp_7,
            img_calon_dk_no9_pp_8, img_calon_dk_no9_pp_9,img_calon_dk_no9_pp_10;

    private static ImageView [] img_calon_dk_no9_pp = {img_calon_dk_no9_pp_1, img_calon_dk_no9_pp_2, img_calon_dk_no9_pp_3, img_calon_dk_no9_pp_5, img_calon_dk_no9_pp_6,
            img_calon_dk_no9_pp_7, img_calon_dk_no9_pp_8, img_calon_dk_no9_pp_9,img_calon_dk_no9_pp_10};

    private static ImageView delete_adapter_dkno9_pp_1, delete_adapter_dkno9_pp_2, delete_adapter_dkno9_pp_3, delete_adapter_dkno9_pp_5, delete_adapter_dkno9_pp_6, delete_adapter_dkno9_pp_7,
            delete_adapter_dkno9_pp_8, delete_adapter_dkno9_pp_9,delete_adapter_dkno9_pp_10;

    private static ImageView [] delete_adapter_dkno9_pp = {delete_adapter_dkno9_pp_1, delete_adapter_dkno9_pp_2, delete_adapter_dkno9_pp_3, delete_adapter_dkno9_pp_5, delete_adapter_dkno9_pp_6,
            delete_adapter_dkno9_pp_7, delete_adapter_dkno9_pp_8, delete_adapter_dkno9_pp_9,delete_adapter_dkno9_pp_10};


    private EditText umur_dk_no9_tt_1, umur_dk_no9_tt_2, umur_dk_no9_tt_3, umur_dk_no9_tt_5, umur_dk_no9_tt_6, umur_dk_no9_tt_7,
            umur_dk_no9_tt_8, umur_dk_no9_tt_9, umur_dk_no9_tt_10;

    private EditText[] umur_dk_no9_tt = {umur_dk_no9_tt_1, umur_dk_no9_tt_2, umur_dk_no9_tt_3, umur_dk_no9_tt_5, umur_dk_no9_tt_6,
            umur_dk_no9_tt_7, umur_dk_no9_tt_8, umur_dk_no9_tt_9, umur_dk_no9_tt_10};

    private Spinner keadaan_dk_no9_tt_1, keadaan_dk_no9_tt_2, keadaan_dk_no9_tt_3, keadaan_dk_no9_tt_5, keadaan_dk_no9_tt_6, keadaan_dk_no9_tt_7,
            keadaan_dk_no9_tt_8, keadaan_dk_no9_tt_9, keadaan_dk_no9_tt_10;

    private Spinner[] keadaan_dk_no9_tt = {keadaan_dk_no9_tt_1, keadaan_dk_no9_tt_2, keadaan_dk_no9_tt_3, keadaan_dk_no9_tt_5, keadaan_dk_no9_tt_6,
            keadaan_dk_no9_tt_7, keadaan_dk_no9_tt_8, keadaan_dk_no9_tt_9, keadaan_dk_no9_tt_10};

    private EditText penyebab_dk_no9_tt_1, penyebab_dk_no9_tt_2, penyebab_dk_no9_tt_3, penyebab_dk_no9_tt_5, penyebab_dk_no9_tt_6, penyebab_dk_no9_tt_7,
            penyebab_dk_no9_tt_8, penyebab_dk_no9_tt_9, penyebab_dk_no9_tt_10;

    private EditText[] penyebab_dk_no9_tt = {penyebab_dk_no9_tt_1, penyebab_dk_no9_tt_2, penyebab_dk_no9_tt_3, penyebab_dk_no9_tt_5, penyebab_dk_no9_tt_6,
            penyebab_dk_no9_tt_7, penyebab_dk_no9_tt_8, penyebab_dk_no9_tt_9, penyebab_dk_no9_tt_10};


    private TextView no_tt_no3_1, no_tt_no3_2, no_tt_no3_3, no_tt_no3_5, no_tt_no3_6, no_tt_no3_7, no_tt_no3_8, no_tt_no3_9, no_tt_no3_10;
    private TextView[] no_tt_no3 = {no_tt_no3_1, no_tt_no3_2, no_tt_no3_3, no_tt_no3_5, no_tt_no3_6, no_tt_no3_7, no_tt_no3_8, no_tt_no3_9, no_tt_no3_10};
    private EditText nm_pershn_tt_no3_1, nm_pershn_tt_no3_2, nm_pershn_tt_no3_3, nm_pershn_tt_no3_5, nm_pershn_tt_no3_6, nm_pershn_tt_no3_7, nm_pershn_tt_no3_8,
            nm_pershn_tt_no3_9, nm_pershn_tt_no3_10;
    private EditText[] nm_pershn_tt_no3 = {nm_pershn_tt_no3_1, nm_pershn_tt_no3_2, nm_pershn_tt_no3_3, nm_pershn_tt_no3_5, nm_pershn_tt_no3_6, nm_pershn_tt_no3_7,
            nm_pershn_tt_no3_8, nm_pershn_tt_no3_9, nm_pershn_tt_no3_10};
    private AutoCompleteTextView produk_tt_no3_1, produk_tt_no3_2, produk_tt_no3_3, produk_tt_no3_5, produk_tt_no3_6, produk_tt_no3_7, produk_tt_no3_8, produk_tt_no3_9, produk_tt_no3_10;
    private AutoCompleteTextView[] produk_tt_no3 = {produk_tt_no3_1, produk_tt_no3_2, produk_tt_no3_3, produk_tt_no3_5, produk_tt_no3_6, produk_tt_no3_7, produk_tt_no3_8, produk_tt_no3_9,
            produk_tt_no3_10};
    private ImageView img_produk_tt_no3_1, img_produk_tt_no3_2, img_produk_tt_no3_3, img_produk_tt_no3_5, img_produk_tt_no3_6, img_produk_tt_no3_7, img_produk_tt_no3_8,
            img_produk_tt_no3_9, img_produk_tt_no3_10;
    private ImageView[] img_produk_tt_no3 = {img_produk_tt_no3_1, img_produk_tt_no3_2, img_produk_tt_no3_3, img_produk_tt_no3_5, img_produk_tt_no3_6, img_produk_tt_no3_7,
            img_produk_tt_no3_8, img_produk_tt_no3_9, img_produk_tt_no3_10};
    private EditText up_tt_no3_1, up_tt_no3_2, up_tt_no3_3, up_tt_no3_5, up_tt_no3_6, up_tt_no3_7, up_tt_no3_8, up_tt_no3_9, up_tt_no3_10;
    private EditText[] up_tt_no3 = {up_tt_no3_1, up_tt_no3_2, up_tt_no3_3, up_tt_no3_5, up_tt_no3_6, up_tt_no3_7, up_tt_no3_8, up_tt_no3_9, up_tt_no3_10};
    private EditText tglpolis_tt_no3_1, tglpolis_tt_no3_2, tglpolis_tt_no3_3, tglpolis_tt_no3_5, tglpolis_tt_no3_6, tglpolis_tt_no3_7, tglpolis_tt_no3_8, tglpolis_tt_no3_9,
            tglpolis_tt_no3_10;
    private EditText[] tglpolis_tt_no3 = {tglpolis_tt_no3_1, tglpolis_tt_no3_2, tglpolis_tt_no3_3, tglpolis_tt_no3_5, tglpolis_tt_no3_6, tglpolis_tt_no3_7, tglpolis_tt_no3_8,
            tglpolis_tt_no3_9, tglpolis_tt_no3_10};
    private ImageView button_tglpolis_tt_no3_1, button_tglpolis_tt_no3_2, button_tglpolis_tt_no3_3, button_tglpolis_tt_no3_5, button_tglpolis_tt_no3_6, button_tglpolis_tt_no3_7,
            button_tglpolis_tt_no3_8, button_tglpolis_tt_no3_9, button_tglpolis_tt_no3_10;
    private ImageView[] button_tglpolis_tt_no3 = {button_tglpolis_tt_no3_1, button_tglpolis_tt_no3_2, button_tglpolis_tt_no3_3, button_tglpolis_tt_no3_5, button_tglpolis_tt_no3_6,
            button_tglpolis_tt_no3_7, button_tglpolis_tt_no3_8, button_tglpolis_tt_no3_9, button_tglpolis_tt_no3_10};
    //TT no 5

    private TextView no_tt_no5_1, no_tt_no5_2, no_tt_no5_3, no_tt_no5_5, no_tt_no5_6, no_tt_no5_7, no_tt_no5_8, no_tt_no5_9, no_tt_no5_10;
    private TextView[] no_tt_no5 = {no_tt_no5_1, no_tt_no5_2, no_tt_no5_3, no_tt_no5_5, no_tt_no5_6, no_tt_no5_7, no_tt_no5_8, no_tt_no5_9, no_tt_no5_10};
    private EditText nm_pershn_tt_no5_1, nm_pershn_tt_no5_2, nm_pershn_tt_no5_3, nm_pershn_tt_no5_5, nm_pershn_tt_no5_6, nm_pershn_tt_no5_7, nm_pershn_tt_no5_8,
            nm_pershn_tt_no5_9, nm_pershn_tt_no5_10;
    private EditText[] nm_pershn_tt_no5 = {nm_pershn_tt_no5_1, nm_pershn_tt_no5_2, nm_pershn_tt_no5_3, nm_pershn_tt_no5_5, nm_pershn_tt_no5_6, nm_pershn_tt_no5_7,
            nm_pershn_tt_no5_8, nm_pershn_tt_no5_9, nm_pershn_tt_no5_10};
    private AutoCompleteTextView produk_tt_no5_1, produk_tt_no5_2, produk_tt_no5_3, produk_tt_no5_5, produk_tt_no5_6, produk_tt_no5_7, produk_tt_no5_8, produk_tt_no5_9, produk_tt_no5_10;
    private AutoCompleteTextView[] produk_tt_no5 = {produk_tt_no5_1, produk_tt_no5_2, produk_tt_no5_3, produk_tt_no5_5, produk_tt_no5_6, produk_tt_no5_7, produk_tt_no5_8, produk_tt_no5_9,
            produk_tt_no5_10};
    private ImageView img_produk_tt_no5_1, img_produk_tt_no5_2, img_produk_tt_no5_3, img_produk_tt_no5_5, img_produk_tt_no5_6, img_produk_tt_no5_7, img_produk_tt_no5_8,
            img_produk_tt_no5_9, img_produk_tt_no5_10;
    private ImageView[] img_produk_tt_no5 = {img_produk_tt_no5_1, img_produk_tt_no5_2, img_produk_tt_no5_3, img_produk_tt_no5_5, img_produk_tt_no5_6, img_produk_tt_no5_7,
            img_produk_tt_no5_8, img_produk_tt_no5_9, img_produk_tt_no5_10};
    private EditText up_tt_no5_1, up_tt_no5_2, up_tt_no5_3, up_tt_no5_5, up_tt_no5_6, up_tt_no5_7, up_tt_no5_8, up_tt_no5_9, up_tt_no5_10;
    private EditText[] up_tt_no5 = {up_tt_no5_1, up_tt_no5_2, up_tt_no5_3, up_tt_no5_5, up_tt_no5_6, up_tt_no5_7, up_tt_no5_8, up_tt_no5_9, up_tt_no5_10};
    private TextView tglpolis_tt_no5_1, tglpolis_tt_no5_2, tglpolis_tt_no5_3, tglpolis_tt_no5_5, tglpolis_tt_no5_6, tglpolis_tt_no5_7, tglpolis_tt_no5_8, tglpolis_tt_no5_9,
            tglpolis_tt_no5_10;
    private TextView[] tglpolis_tt_no5 = {tglpolis_tt_no5_1, tglpolis_tt_no5_2, tglpolis_tt_no5_3, tglpolis_tt_no5_5, tglpolis_tt_no5_6, tglpolis_tt_no5_7, tglpolis_tt_no5_8,
            tglpolis_tt_no5_9, tglpolis_tt_no5_10};
    private ImageView button_tglpolis_tt_no5_1, button_tglpolis_tt_no5_2, button_tglpolis_tt_no5_3, button_tglpolis_tt_no5_5, button_tglpolis_tt_no5_6, button_tglpolis_tt_no5_7,
            button_tglpolis_tt_no5_8, button_tglpolis_tt_no5_9, button_tglpolis_tt_no5_10;
    private ImageView[] button_tglpolis_tt_no5 = {button_tglpolis_tt_no5_1, button_tglpolis_tt_no5_2, button_tglpolis_tt_no5_3, button_tglpolis_tt_no5_5, button_tglpolis_tt_no5_6,
            button_tglpolis_tt_no5_7, button_tglpolis_tt_no5_8, button_tglpolis_tt_no5_9, button_tglpolis_tt_no5_10};
    private AutoCompleteTextView klaim_tt_no5_1, klaim_tt_no5_2, klaim_tt_no5_3, klaim_tt_no5_5, klaim_tt_no5_6, klaim_tt_no5_7,
            klaim_tt_no5_8, klaim_tt_no5_9, klaim_tt_no5_10;
    private AutoCompleteTextView[] klaim_tt_no5 = {klaim_tt_no5_1, klaim_tt_no5_2, klaim_tt_no5_3, klaim_tt_no5_5, klaim_tt_no5_6,
            klaim_tt_no5_7, klaim_tt_no5_8, klaim_tt_no5_9, klaim_tt_no5_10};
    private ImageView img_klaim_tt_no5_1, img_klaim_tt_no5_2, img_klaim_tt_no5_3, img_klaim_tt_no5_5, img_klaim_tt_no5_6, img_klaim_tt_no5_7,
            img_klaim_tt_no5_8, img_klaim_tt_no5_9, img_klaim_tt_no5_10;
    private ImageView[] img_klaim_tt_no5 = {img_klaim_tt_no5_1, img_klaim_tt_no5_2, img_klaim_tt_no5_3, img_klaim_tt_no5_5, img_klaim_tt_no5_6,
            img_klaim_tt_no5_7, img_klaim_tt_no5_8, img_klaim_tt_no5_9, img_klaim_tt_no5_10};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem menu_validasi = menu.findItem(R.id.menu_validasi);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_validasi:
                me.getModelUpdateQuisioner().setValidation(validation());
                me.getModelID().setVal_qu(validation());
                loadview();
                if (ll_tambah_data_calon_tertanggung_qu.getChildCount() > 0) {
                    AddModel_dkno9tt(count_dk_9tt);
                }
                if (ll_tambah_data_calon_pemegang_polis_qu.getChildCount() > 0) {
                    AddModel_dkno9pp(count_dk_9pp);
                }
                MyTaskValidasi taskValidasi = new MyTaskValidasi();
                taskValidasi.execute();
                Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTaskValidasi extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_QUESTIONAIRE);
            return null;
        }
    }

    private class MyTaskSavePage extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_QUESTIONAIRE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View kuisioner = inflater.inflate(R.layout.e_fragment_questionnaire_layout, container,
                false);
        me = E_MainActivity.e_bundle.getParcelable(getString(R.string.modelespaj));
        ((E_MainActivity) getActivity()).setSecondToolbar("Pertanyaan Kesehatan (8/", 8);
        ButterKnife.bind(this, kuisioner);
        list = new ArrayList<ModelMst_Answer>();
        List_ttno3 = new ArrayList<ModelTT_NO3UQ>();
        List_ttno5 = new ArrayList<ModelTT_NO5UQ>();
        List_dkno9pp = new ArrayList<ModelDK_NO9ppUQ>();
        List_dkno9tt = new ArrayList<ModelDK_NO9ttUQ>();
        List_dkketkes = new ArrayList<ModelDK_KetKesUQ>();
        //tertanggung
        essay_tt_no1.setOnFocusChangeListener(this);
        essay_tt_no1.addTextChangedListener(new generalTextWatcher(essay_tt_no1));
        MethodSupport.active_view(0, essay_tt_no1);

        essay_tt_no2.setOnFocusChangeListener(this);
        essay_tt_no2.addTextChangedListener(new generalTextWatcher(essay_tt_no2));
        MethodSupport.active_view(0, essay_tt_no2);

        essay_tt_no4.setOnFocusChangeListener(this);
        essay_tt_no4.addTextChangedListener(new generalTextWatcher(essay_tt_no4));
        MethodSupport.active_view(0, essay_tt_no4);

        essay_tt_no6.setOnFocusChangeListener(this);
        essay_tt_no6.addTextChangedListener(new generalTextWatcher(essay_tt_no6));
        MethodSupport.active_view(0, essay_tt_no6);

        essay_tt_no7.setOnFocusChangeListener(this);
        essay_tt_no7.addTextChangedListener(new generalTextWatcher(essay_tt_no7));
        MethodSupport.active_view(0, essay_tt_no7);

        essay_tt_1_no8a.setOnFocusChangeListener(this);
        essay_tt_1_no8a.addTextChangedListener(new generalTextWatcher(essay_tt_1_no8a));
        MethodSupport.active_view(0, essay_tt_1_no8a);

        essay_tt_2_no8a.setOnFocusChangeListener(this);
        essay_tt_2_no8a.addTextChangedListener(new generalTextWatcher(essay_tt_2_no8a));
        MethodSupport.active_view(0, essay_tt_2_no8a);

        essay_tt_rkk_no10.setOnFocusChangeListener(this);
        essay_tt_rkk_no10.addTextChangedListener(new generalTextWatcher(essay_tt_rkk_no10));
        MethodSupport.active_view(0, essay_tt_rkk_no10);

        essay_tt_cm_no11a.setOnFocusChangeListener(this);
        essay_tt_cm_no11a.addTextChangedListener(new generalTextWatcher(essay_tt_cm_no11a));

        essay_tt_kg_no11a.setOnFocusChangeListener(this);
        essay_tt_kg_no11a.addTextChangedListener(new generalTextWatcher(essay_tt_kg_no11a));

        essay_tt_no11b.setOnFocusChangeListener(this);
        essay_tt_no11b.addTextChangedListener(new generalTextWatcher(essay_tt_no11b));
        MethodSupport.active_view(0, essay_tt_no11b);

        essay_tt_cm_no12.setOnFocusChangeListener(this);
        essay_tt_cm_no12.addTextChangedListener(new generalTextWatcher(essay_tt_cm_no12));

        essay_tt_kg_no12.setOnFocusChangeListener(this);
        essay_tt_kg_no12.addTextChangedListener(new generalTextWatcher(essay_tt_kg_no12));

        if (Integer.parseInt(me.getTertanggungModel().getUsia_tt().trim()) > 14) {
            iv_circle_questionnaire12_qu.setVisibility(View.INVISIBLE);
            MethodSupport.active_view(0, essay_tt_cm_no12);

            MethodSupport.active_view(0, essay_tt_kg_no12);
        } else if (Integer.parseInt(me.getTertanggungModel().getUsia_tt().trim()) < 15) {
            iv_circle_questionnaire12_qu.setVisibility(View.VISIBLE);
            MethodSupport.active_view(1, essay_tt_cm_no12);

            MethodSupport.active_view(1, essay_tt_kg_no12);
        }
        //pemegangpolis
        essay_pp_1_no13.setOnFocusChangeListener(this);
        essay_pp_1_no13.addTextChangedListener(new generalTextWatcher(essay_pp_1_no13));
        MethodSupport.active_view(0, essay_pp_1_no13);

        essay_pp_2_no13.setOnFocusChangeListener(this);
        essay_pp_2_no13.addTextChangedListener(new generalTextWatcher(essay_pp_2_no13));
        MethodSupport.active_view(0, essay_pp_2_no13);

        essay_pp_1_no14.setOnFocusChangeListener(this);
        essay_pp_1_no14.addTextChangedListener(new generalTextWatcher(essay_pp_1_no14));
        MethodSupport.active_view(0, essay_pp_1_no14);

        essay_pp_2_no14.setOnFocusChangeListener(this);
        essay_pp_2_no14.addTextChangedListener(new generalTextWatcher(essay_pp_2_no14));
        MethodSupport.active_view(0, essay_pp_2_no14);

        essay_pp_cm_no15.setOnFocusChangeListener(this);
        essay_pp_cm_no15.addTextChangedListener(new generalTextWatcher(essay_pp_cm_no15));

        essay_pp_kg_no15.setOnFocusChangeListener(this);
        essay_pp_kg_no15.addTextChangedListener(new generalTextWatcher(essay_pp_kg_no15));

        //data kesehatan
        essay_dk_no2.setOnFocusChangeListener(this);
        essay_dk_no2.addTextChangedListener(new generalTextWatcher(essay_dk_no2));
        MethodSupport.active_view(0, essay_dk_no2);

        essay_dk_no4.setOnFocusChangeListener(this);
        essay_dk_no4.addTextChangedListener(new generalTextWatcher(essay_dk_no4));
        MethodSupport.active_view(0, essay_dk_no4);

        essay_dk_no7a.setOnFocusChangeListener(this);
        essay_dk_no7a.addTextChangedListener(new generalTextWatcher(essay_dk_no7a));
        MethodSupport.active_view(0, essay_dk_no7a);

        essay_dk_no7b.setOnFocusChangeListener(this);
        essay_dk_no7b.addTextChangedListener(new generalTextWatcher(essay_dk_no7b));
        MethodSupport.active_view(0, essay_dk_no7b);

        essay_dk_no7c.setOnFocusChangeListener(this);
        essay_dk_no7c.addTextChangedListener(new generalTextWatcher(essay_dk_no7c));
        MethodSupport.active_view(0, essay_dk_no7c);

        essay_dk_no7d.setOnFocusChangeListener(this);
        essay_dk_no7d.addTextChangedListener(new generalTextWatcher(essay_dk_no7d));
        MethodSupport.active_view(0, essay_dk_no7d);

        essay_dk_no8a.setOnFocusChangeListener(this);
        essay_dk_no8a.addTextChangedListener(new generalTextWatcher(essay_dk_no8a));
        MethodSupport.active_view(0, essay_dk_no8a);

        //radiogroup
        //tertanggung
        MethodSupport.OnRadioGroup(option_grup_tt_no1, this);
        MethodSupport.OnRadioGroup(option_grup_tt_no2, this);
        MethodSupport.OnRadioGroup(option_grup_tt_no3, this);
        MethodSupport.OnRadioGroup(option_grup_tt_no4, this);
        MethodSupport.OnRadioGroup(option_grup_tt_no5, this);
        MethodSupport.OnRadioGroup(option_grup_tt_no6, this);
        MethodSupport.OnRadioGroup(option_grup_tt_no7, this);
        MethodSupport.OnRadioGroup(option_grup_tt_no8, this);
        MethodSupport.OnRadioGroup(option_grup_tt_no8b, this);
        MethodSupport.OnRadioGroup(option_grup_tt_no9, this);
        MethodSupport.OnRadioGroup(option_grup_tt_no10, this);
        MethodSupport.OnRadioGroup(option_grup_tt_no11b, this);

        //pemegangpolis
        MethodSupport.OnRadioGroup(option_grup_pp_no13, this);
        MethodSupport.OnRadioGroup(option_grup_pp_no14, this);

        //data kesehatan
        MethodSupport.OnRadioGroup(option_grup_dk_ppno1l, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1l, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1l, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1l, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1l, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1ii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1ii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1ii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1ii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1ii, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1iii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1iii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1iii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1iii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1iii, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1iv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1iv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1iv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1iv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1iv, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1v, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1v, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1v, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1v, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1v, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1vi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1vi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1vi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1vi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1vi, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1vii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1vii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1vii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1vii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1vii, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1viii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1viii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1viii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1viii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1viii, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1ix, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1ix, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1ix, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1ix, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1ix, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1x, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1x, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1x, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1x, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1x, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xi, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xii, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xiii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xiii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xiii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xiii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xiii, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xiv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xiv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xiv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xiv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xiv, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xv, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xvi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xvi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xvi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xvi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xvi, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xvii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xvii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xvii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xvii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xvii, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xviii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xviii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xviii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xviii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xviii, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xix, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xix, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xix, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xix, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xix, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xx, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xx, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xx, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xx, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xx, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xxi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xxi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xxi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xxi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xxi, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xxii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xxii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xxii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xxii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xxii, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xxiii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xxiii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xxiii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xxiii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xxiii, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xxiv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xxiv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xxiv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xxiv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xxiv, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xxv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xxv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xxv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xxv, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xxv, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xxvi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xxvi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xxvi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xxvi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xxvi, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xxvii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xxvii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xxvii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xxvii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xxvii, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xxviii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xxviii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xxviii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xxviii, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xxviii, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xxix, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xxix, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xxix, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xxix, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xxix, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xxx, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xxx, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xxx, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xxx, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xxx, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno1xxxi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno1xxxi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no1xxxi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no1xxxi, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no1xxxi, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno2, this);

        MethodSupport.OnRadioGroup(option_grup_dk_tt1no2, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no2, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no2, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno3a, this);

        MethodSupport.OnRadioGroup(option_grup_dk_tt1no3a, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no3a, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no3a, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno3b, this);

        MethodSupport.OnRadioGroup(option_grup_dk_tt1no3b, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no3b, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no3b, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno3c, this);

        MethodSupport.OnRadioGroup(option_grup_dk_tt1no3c, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no3c, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no3c, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno3d, this);

        MethodSupport.OnRadioGroup(option_grup_dk_tt1no3d, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no3d, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no3d, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno4, this);

        MethodSupport.OnRadioGroup(option_grup_dk_tt1no4, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no4, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no4, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno5, this);

        MethodSupport.OnRadioGroup(option_grup_dk_tt1no5, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no5, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no5, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno6, this);

        MethodSupport.OnRadioGroup(option_grup_dk_tt1no6, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no6, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no6, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno7, this);

        MethodSupport.OnRadioGroup(option_grup_dk_tt1no7, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no7, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no7, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno8a, this);

        MethodSupport.OnRadioGroup(option_grup_dk_tt1no8a, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no8a, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no8a, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno8b, this);

        MethodSupport.OnRadioGroup(option_grup_dk_tt1no8b, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no8b, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no8b, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno8c, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno8c, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no8c, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no8c, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no8c, this);

        MethodSupport.OnRadioGroup(option_grup_dk_ppno9, this);
        MethodSupport.OnRadioGroup(option_grup_dk_ttno9, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt1no9, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt2no9, this);
        MethodSupport.OnRadioGroup(option_grup_dk_tt3no9, this);

        //RadioButton

        MethodSupport.OnRadioButton(option_y_tt_no1, this);
        MethodSupport.OnRadioButton(option_n_tt_no1, this);
        MethodSupport.OnRadioButton(option_y_tt_no2, this);
        MethodSupport.OnRadioButton(option_n_tt_no2, this);
        MethodSupport.OnRadioButton(option_y_tt_no3, this);
        MethodSupport.OnRadioButton(option_n_tt_no3, this);
        MethodSupport.OnRadioButton(option_y_tt_no4, this);
        MethodSupport.OnRadioButton(option_n_tt_no4, this);
        MethodSupport.OnRadioButton(option_y_tt_no5, this);
        MethodSupport.OnRadioButton(option_n_tt_no5, this);
        MethodSupport.OnRadioButton(option_y_tt_no6, this);
        MethodSupport.OnRadioButton(option_n_tt_no6, this);
        MethodSupport.OnRadioButton(option_y_tt_no7, this);
        MethodSupport.OnRadioButton(option_n_tt_no7, this);
        MethodSupport.OnRadioButton(option_y_tt_no8, this);
        MethodSupport.OnRadioButton(option_n_tt_no8, this);
        MethodSupport.OnRadioButton(option_y_tt_no8b, this);
        MethodSupport.OnRadioButton(option_n_tt_no8b, this);
        MethodSupport.OnRadioButton(option_y_tt_no9, this);
        MethodSupport.OnRadioButton(option_n_tt_no9, this);
        MethodSupport.OnRadioButton(option_y_tt_no10, this);
        MethodSupport.OnRadioButton(option_n_tt_no10, this);
        MethodSupport.OnRadioButton(option_y_tt_no11b, this);
        MethodSupport.OnRadioButton(option_n_tt_no11b, this);
        MethodSupport.OnRadioButton(option_y_pp_no13, this);
        MethodSupport.OnRadioButton(option_n_pp_no13, this);
        MethodSupport.OnRadioButton(option_y_pp_no14, this);
        MethodSupport.OnRadioButton(option_n_pp_no14, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno1l, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1l, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1l, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1l, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1l, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1l, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1l, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1l, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1l, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1l, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1ii, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1ii, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1ii, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1ii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1ii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1ii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1ii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1ii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1ii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1ii, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1iii, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1iii, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1iii, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1iii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1iii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1iii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1iii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1iii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1iii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1iii, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1iv, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1iv, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1iv, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1iv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1iv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1iv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1iv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1iv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1iv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1iv, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1v, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1v, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1v, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1v, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1v, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1v, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1v, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1v, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1v, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1v, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1vi, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1vi, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1vi, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1vi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1vi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1vi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1vi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1vi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1vi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1vi, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno1vii, this);

        MethodSupport.OnRadioButton(option_n_dk_ppno1vii, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1vii, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1vii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1vii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1vii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1vii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1vii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1vii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1vii, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1viii, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1viii, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1viii, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1viii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1viii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1viii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1viii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1viii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1viii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1viii, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1ix, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1ix, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1ix, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1ix, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1ix, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1ix, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1ix, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1ix, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1ix, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1ix, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1x, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1x, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1x, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1x, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1x, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1x, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1x, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1x, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1x, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1x, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xi, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xi, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xi, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xi, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xii, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xii, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xii, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xii, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xiii, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xiii, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xiii, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xiii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xiii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xiii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xiii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xiii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xiii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xiii, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xiv, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xiv, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xiv, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xiv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xiv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xiv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xiv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xiv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xiv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xiv, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xv, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xv, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xv, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xv, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xvi, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xvi, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xvi, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xvi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xvi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xvi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xvi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xvi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xvi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xvi, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xvii, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xvii, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xvii, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xvii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xvii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xvii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xvii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xvii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xvii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xvii, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xviii, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xviii, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xviii, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xviii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xviii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xviii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xviii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xviii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xviii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xviii, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xix, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xix, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xix, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xix, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xix, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xix, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xix, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xix, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xix, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xix, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xx, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xx, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xx, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xx, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xx, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xx, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xx, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xx, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xx, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xx, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xxi, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xxi, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xxi, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xxi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xxi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xxi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xxi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xxi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xxi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xxi, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xxii, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xxii, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xxii, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xxii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xxii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xxii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xxii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xxii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xxii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xxii, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xxiii, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xxiii, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xxiii, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xxiii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xxiii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xxiii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xxiii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xxiii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xxiii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xxiii, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xxiv, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xxiv, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xxiv, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xxiv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xxiv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xxiv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xxiv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xxiv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xxiv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xxiv, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xxv, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xxv, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xxv, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xxv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xxv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xxv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xxv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xxv, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xxv, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xxv, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xxvi, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xxvi, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xxvi, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xxvi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xxvi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xxvi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xxvi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xxvi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xxvi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xxvi, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xxvii, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xxvii, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xxvii, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xxvii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xxvii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xxvii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xxvii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xxvii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xxvii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xxvii, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xxviii, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xxviii, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xxviii, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xxviii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xxviii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xxviii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xxviii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xxviii, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xxviii, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xxviii, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xxix, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xxix, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xxix, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xxix, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xxix, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xxix, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xxix, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xxix, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xxix, this);

        MethodSupport.OnRadioButton(option_n_dk_tt3no1xxix, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno1xxx, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xxx, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xxx, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xxx, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xxx, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xxx, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xxx, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xxx, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xxx, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xxx, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno1xxxi, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno1xxxi, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno1xxxi, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno1xxxi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no1xxxi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no1xxxi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no1xxxi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no1xxxi, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no1xxxi, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no1xxxi, this);

        MethodSupport.OnRadioButton(option_y_dk_ppno2, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno2, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno2, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno2, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no2, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no2, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no2, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no2, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no2, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no2, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno3a, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno3a, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno3a, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno3a, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no3a, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no3a, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no3a, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no3a, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no3a, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no3a, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno3b, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno3b, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno3b, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno3b, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no3b, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no3b, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no3b, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no3b, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no3b, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no3b, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno3c, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno3c, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno3c, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno3c, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no3c, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no3c, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no3c, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no3c, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no3c, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no3c, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno3d, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno3d, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno3d, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno3d, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no3d, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no3d, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no3d, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no3d, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no3d, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no3d, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno4, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno4, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno4, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno4, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no4, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no4, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no4, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no4, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no4, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no4, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno5, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno5, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno5, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno5, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no5, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no5, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no5, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no5, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no5, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no5, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno6, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno6, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno6, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno6, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no6, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no6, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no6, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no6, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no6, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no6, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno7, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno7, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno7, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno7, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no7, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no7, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no7, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no7, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no7, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no7, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno8a, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno8a, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno8a, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno8a, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no8a, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no8a, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no8a, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no8a, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no8a, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no8a, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno8b, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno8b, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno8b, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno8b, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no8b, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no8b, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no8b, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no8b, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no8b, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no8b, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno8c, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno8c, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno8c, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno8c, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no8c, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no8c, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no8c, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no8c, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no8c, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no8c, this);
        MethodSupport.OnRadioButton(option_y_dk_ppno9, this);
        MethodSupport.OnRadioButton(option_n_dk_ppno9, this);
        MethodSupport.OnRadioButton(option_y_dk_ttno9, this);
        MethodSupport.OnRadioButton(option_n_dk_ttno9, this);
        MethodSupport.OnRadioButton(option_y_dk_tt1no9, this);
        MethodSupport.OnRadioButton(option_n_dk_tt1no9, this);
        MethodSupport.OnRadioButton(option_y_dk_tt2no9, this);
        MethodSupport.OnRadioButton(option_n_dk_tt2no9, this);
        MethodSupport.OnRadioButton(option_y_dk_tt3no9, this);
        MethodSupport.OnRadioButton(option_n_dk_tt3no9, this);

        if(me.getPemegangPolisModel().getJekel_pp()== 1){ //0 itu wanita
            MethodSupport.active_view(0, option_y_dk_ppno8a);
            MethodSupport.active_view(0, option_n_dk_ppno8a);
            MethodSupport.active_view(0, option_y_dk_ppno8b);
            MethodSupport.active_view(0, option_n_dk_ppno8b);
            MethodSupport.active_view(0, option_y_dk_ppno8c);
            MethodSupport.active_view(0, option_n_dk_ppno8c);
        }
        if(me.getTertanggungModel().getJekel_tt() == 1){
            MethodSupport.active_view(0, option_y_dk_ttno8a);
            MethodSupport.active_view(0, option_n_dk_ttno8a);
            MethodSupport.active_view(0, option_y_dk_ttno8b);
            MethodSupport.active_view(0, option_n_dk_ttno8b);
            MethodSupport.active_view(0, option_y_dk_ttno8c);
            MethodSupport.active_view(0, option_n_dk_ttno8c);
        }

        //img button
        img_tambah_data_qu.setOnClickListener(this);
        MethodSupport.active_view(0, img_tambah_data_qu);
        img_tambah_data2_qu.setOnClickListener(this);
        MethodSupport.active_view(0, img_tambah_data2_qu);
        img_tambah_data_calon_pemegang_polis_qu.setOnClickListener(this);
        img_tambah_data_calon_tertanggung_qu.setOnClickListener(this);
        img_tambah_data_keterangan_kesehatan_qu.setOnClickListener(this);

        //button di Activity
        btn_lanjut = getActivity().findViewById(R.id.btn_lanjut);
        btn_lanjut.setOnClickListener(this);
        btn_kembali = getActivity().findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(this);
        second_toolbar = ((E_MainActivity) getActivity()).getSecond_toolbar();
        iv_save = second_toolbar.findViewById(R.id.iv_save);
        iv_save.setOnClickListener(this);
        iv_next = second_toolbar.findViewById(R.id.iv_next);
        iv_next.setOnClickListener(this);
        iv_prev = second_toolbar.findViewById(R.id.iv_prev);
        iv_prev.setOnClickListener(this);


        ListOfView = new ArrayList<View>();
        //berdasarkan API label
        View[] view = {essay_tt_no1, option_y_tt_no1, option_n_tt_no1, essay_tt_no2, option_y_tt_no2, option_n_tt_no2, option_y_tt_no3,
                option_n_tt_no3, essay_tt_no4, option_y_tt_no4, option_n_tt_no4, option_y_tt_no5, option_n_tt_no5, essay_tt_no6,
                option_y_tt_no6, option_n_tt_no6, essay_tt_no7, option_y_tt_no7, option_n_tt_no7, option_y_tt_no8, option_n_tt_no8,
                essay_tt_1_no8a, essay_tt_2_no8a, option_y_tt_no8b, option_n_tt_no8b, option_y_tt_no9, option_n_tt_no9, essay_tt_rkk_no10,
                option_y_tt_no10, option_n_tt_no10, essay_tt_cm_no11a, essay_tt_kg_no11a, essay_tt_no11b, option_y_tt_no11b, option_n_tt_no11b,
                essay_tt_cm_no12, essay_tt_kg_no12, essay_pp_1_no13, essay_pp_2_no13, option_y_pp_no13, option_n_pp_no13, essay_pp_1_no14,
                essay_pp_2_no14, option_y_pp_no14, option_n_pp_no14, essay_pp_cm_no15, essay_pp_kg_no15,
                option_y_dk_ppno1l, option_n_dk_ppno1l, option_y_dk_ttno1l, option_n_dk_ttno1l, option_y_dk_tt1no1l, option_n_dk_tt1no1l, option_y_dk_tt2no1l,
                option_n_dk_tt2no1l, option_y_dk_tt3no1l, option_n_dk_tt3no1l,
                option_y_dk_ppno1ii, option_n_dk_ppno1ii, option_y_dk_ttno1ii, option_n_dk_ttno1ii, option_y_dk_tt1no1ii, option_n_dk_tt1no1ii, option_y_dk_tt2no1ii,
                option_n_dk_tt2no1ii, option_y_dk_tt3no1ii, option_n_dk_tt3no1ii,
                option_y_dk_ppno1iii, option_n_dk_ppno1iii, option_y_dk_ttno1iii, option_n_dk_ttno1iii, option_y_dk_tt1no1iii, option_n_dk_tt1no1iii, option_y_dk_tt2no1iii,
                option_n_dk_tt2no1iii, option_y_dk_tt3no1iii, option_n_dk_tt3no1iii,
                option_y_dk_ppno1iv, option_n_dk_ppno1iv, option_y_dk_ttno1iv, option_n_dk_ttno1iv, option_y_dk_tt1no1iv, option_n_dk_tt1no1iv, option_y_dk_tt2no1iv,
                option_n_dk_tt2no1iv, option_y_dk_tt3no1iv, option_n_dk_tt3no1iv,
                option_y_dk_ppno1v, option_n_dk_ppno1v, option_y_dk_ttno1v, option_n_dk_ttno1v, option_y_dk_tt1no1v, option_n_dk_tt1no1v, option_y_dk_tt2no1v,
                option_n_dk_tt2no1v, option_y_dk_tt3no1v, option_n_dk_tt3no1v,
                option_y_dk_ppno1vi, option_n_dk_ppno1vi, option_y_dk_ttno1vi, option_n_dk_ttno1vi, option_y_dk_tt1no1vi, option_n_dk_tt1no1vi, option_y_dk_tt2no1vi,
                option_n_dk_tt2no1vi, option_y_dk_tt3no1vi, option_n_dk_tt3no1vi,
                option_y_dk_ppno1vii, option_n_dk_ppno1vii, option_y_dk_ttno1vii, option_n_dk_ttno1vii, option_y_dk_tt1no1vii, option_n_dk_tt1no1vii, option_y_dk_tt2no1vii,
                option_n_dk_tt2no1vii, option_y_dk_tt3no1vii, option_n_dk_tt3no1vii,
                option_y_dk_ppno1viii, option_n_dk_ppno1viii, option_y_dk_ttno1viii, option_n_dk_ttno1viii, option_y_dk_tt1no1viii, option_n_dk_tt1no1viii, option_y_dk_tt2no1viii,
                option_n_dk_tt2no1viii, option_y_dk_tt3no1viii, option_n_dk_tt3no1viii,
                option_y_dk_ppno1ix, option_n_dk_ppno1ix, option_y_dk_ttno1ix, option_n_dk_ttno1ix, option_y_dk_tt1no1ix, option_n_dk_tt1no1ix, option_y_dk_tt2no1ix,
                option_n_dk_tt2no1ix, option_y_dk_tt3no1ix, option_n_dk_tt3no1ix,
                option_y_dk_ppno1x, option_n_dk_ppno1x, option_y_dk_ttno1x, option_n_dk_ttno1x, option_y_dk_tt1no1x, option_n_dk_tt1no1x, option_y_dk_tt2no1x,
                option_n_dk_tt2no1x, option_y_dk_tt3no1x, option_n_dk_tt3no1x,
                option_y_dk_ppno1xi, option_n_dk_ppno1xi, option_y_dk_ttno1xi, option_n_dk_ttno1xi, option_y_dk_tt1no1xi, option_n_dk_tt1no1xi, option_y_dk_tt2no1xi,
                option_n_dk_tt2no1xi, option_y_dk_tt3no1xi, option_n_dk_tt3no1xi,
                option_y_dk_ppno1xii, option_n_dk_ppno1xii, option_y_dk_ttno1xii, option_n_dk_ttno1xii, option_y_dk_tt1no1xii, option_n_dk_tt1no1xii, option_y_dk_tt2no1xii,
                option_n_dk_tt2no1xii, option_y_dk_tt3no1xii, option_n_dk_tt3no1xii,
                option_y_dk_ppno1xiii, option_n_dk_ppno1xiii, option_y_dk_ttno1xiii, option_n_dk_ttno1xiii, option_y_dk_tt1no1xiii, option_n_dk_tt1no1xiii, option_y_dk_tt2no1xiii,
                option_n_dk_tt2no1xiii, option_y_dk_tt3no1xiii, option_n_dk_tt3no1xiii,
                option_y_dk_ppno1xiv, option_n_dk_ppno1xiv, option_y_dk_ttno1xiv, option_n_dk_ttno1xiv, option_y_dk_tt1no1xiv, option_n_dk_tt1no1xiv, option_y_dk_tt2no1xiv,
                option_n_dk_tt2no1xiv, option_y_dk_tt3no1xiv, option_n_dk_tt3no1xiv,
                option_y_dk_ppno1xv, option_n_dk_ppno1xv, option_y_dk_ttno1xv, option_n_dk_ttno1xv, option_y_dk_tt1no1xv, option_n_dk_tt1no1xv, option_y_dk_tt2no1xv,
                option_n_dk_tt2no1xv, option_y_dk_tt3no1xv, option_n_dk_tt3no1xv,
                option_y_dk_ppno1xvi, option_n_dk_ppno1xvi, option_y_dk_ttno1xvi, option_n_dk_ttno1xvi, option_y_dk_tt1no1xvi, option_n_dk_tt1no1xvi, option_y_dk_tt2no1xvi,
                option_n_dk_tt2no1xvi, option_y_dk_tt3no1xvi, option_n_dk_tt3no1xvi,
                option_y_dk_ppno1xvii, option_n_dk_ppno1xvii, option_y_dk_ttno1xvii, option_n_dk_ttno1xvii, option_y_dk_tt1no1xvii, option_n_dk_tt1no1xvii, option_y_dk_tt2no1xvii,
                option_n_dk_tt2no1xvii, option_y_dk_tt3no1xvii, option_n_dk_tt3no1xvii,
                option_y_dk_ppno1xviii, option_n_dk_ppno1xviii, option_y_dk_ttno1xviii, option_n_dk_ttno1xviii, option_y_dk_tt1no1xviii, option_n_dk_tt1no1xviii, option_y_dk_tt2no1xviii,
                option_n_dk_tt2no1xviii, option_y_dk_tt3no1xviii, option_n_dk_tt3no1xviii,
                option_y_dk_ppno1xix, option_n_dk_ppno1xix, option_y_dk_ttno1xix, option_n_dk_ttno1xix, option_y_dk_tt1no1xix, option_n_dk_tt1no1xix, option_y_dk_tt2no1xix,
                option_n_dk_tt2no1xix, option_y_dk_tt3no1xix, option_n_dk_tt3no1xix,
                option_y_dk_ppno1xx, option_n_dk_ppno1xx, option_y_dk_ttno1xx, option_n_dk_ttno1xx, option_y_dk_tt1no1xx, option_n_dk_tt1no1xx, option_y_dk_tt2no1xx,
                option_n_dk_tt2no1xx, option_y_dk_tt3no1xx, option_n_dk_tt3no1xx,
                option_y_dk_ppno1xxi, option_n_dk_ppno1xxi, option_y_dk_ttno1xxi, option_n_dk_ttno1xxi, option_y_dk_tt1no1xxi, option_n_dk_tt1no1xxi, option_y_dk_tt2no1xxi,
                option_n_dk_tt2no1xxi, option_y_dk_tt3no1xxi, option_n_dk_tt3no1xxi,
                option_y_dk_ppno1xxii, option_n_dk_ppno1xxii, option_y_dk_ttno1xxii, option_n_dk_ttno1xxii, option_y_dk_tt1no1xxii, option_n_dk_tt1no1xxii, option_y_dk_tt2no1xxii,
                option_n_dk_tt2no1xxii, option_y_dk_tt3no1xxii, option_n_dk_tt3no1xxii,
                option_y_dk_ppno1xxiii, option_n_dk_ppno1xxiii, option_y_dk_ttno1xxiii, option_n_dk_ttno1xxiii, option_y_dk_tt1no1xxiii, option_n_dk_tt1no1xxiii, option_y_dk_tt2no1xxiii,
                option_n_dk_tt2no1xxiii, option_y_dk_tt3no1xxiii, option_n_dk_tt3no1xxiii,
                option_y_dk_ppno1xxiv, option_n_dk_ppno1xxiv, option_y_dk_ttno1xxiv, option_n_dk_ttno1xxiv, option_y_dk_tt1no1xxiv, option_n_dk_tt1no1xxiv, option_y_dk_tt2no1xxiv,
                option_n_dk_tt2no1xxiv, option_y_dk_tt3no1xxiv, option_n_dk_tt3no1xxiv,
                option_y_dk_ppno1xxv, option_n_dk_ppno1xxv, option_y_dk_ttno1xxv, option_n_dk_ttno1xxv, option_y_dk_tt1no1xxv, option_n_dk_tt1no1xxv, option_y_dk_tt2no1xxv,
                option_n_dk_tt2no1xxv, option_y_dk_tt3no1xxv, option_n_dk_tt3no1xxv,
                option_y_dk_ppno1xxvi, option_n_dk_ppno1xxvi, option_y_dk_ttno1xxvi, option_n_dk_ttno1xxvi, option_y_dk_tt1no1xxvi, option_n_dk_tt1no1xxvi, option_y_dk_tt2no1xxvi,
                option_n_dk_tt2no1xxvi, option_y_dk_tt3no1xxvi, option_n_dk_tt3no1xxvi,
                option_y_dk_ppno1xxvii, option_n_dk_ppno1xxvii, option_y_dk_ttno1xxvii, option_n_dk_ttno1xxvii, option_y_dk_tt1no1xxvii, option_n_dk_tt1no1xxvii, option_y_dk_tt2no1xxvii,
                option_n_dk_tt2no1xxvii, option_y_dk_tt3no1xxvii, option_n_dk_tt3no1xxvii,
                option_y_dk_ppno1xxviii, option_n_dk_ppno1xxviii, option_y_dk_ttno1xxviii, option_n_dk_ttno1xxviii, option_y_dk_tt1no1xxviii, option_n_dk_tt1no1xxviii, option_y_dk_tt2no1xxviii,
                option_n_dk_tt2no1xxviii, option_y_dk_tt3no1xxviii, option_n_dk_tt3no1xxviii,
                option_y_dk_ppno1xxix, option_n_dk_ppno1xxix, option_y_dk_ttno1xxix, option_n_dk_ttno1xxix, option_y_dk_tt1no1xxix, option_n_dk_tt1no1xxix, option_y_dk_tt2no1xxix,
                option_n_dk_tt2no1xxix, option_y_dk_tt3no1xxix, option_n_dk_tt3no1xxix,
                option_y_dk_ppno1xxx, option_n_dk_ppno1xxx, option_y_dk_ttno1xxx, option_n_dk_ttno1xxx, option_y_dk_tt1no1xxx, option_n_dk_tt1no1xxx, option_y_dk_tt2no1xxx,
                option_n_dk_tt2no1xxx, option_y_dk_tt3no1xxx, option_n_dk_tt3no1xxx,
                option_y_dk_ppno1xxxi, option_n_dk_ppno1xxxi, option_y_dk_ttno1xxxi, option_n_dk_ttno1xxxi, option_y_dk_tt1no1xxxi, option_n_dk_tt1no1xxxi, option_y_dk_tt2no1xxxi,
                option_n_dk_tt2no1xxxi, option_y_dk_tt3no1xxxi, option_n_dk_tt3no1xxxi,
                essay_dk_no2, option_y_dk_ppno2, option_n_dk_ppno2, option_y_dk_ttno2, option_n_dk_ttno2, option_y_dk_tt1no2, option_n_dk_tt1no2, option_y_dk_tt2no2,
                option_n_dk_tt2no2, option_y_dk_tt3no2, option_n_dk_tt3no2,
                option_y_dk_ppno3a, option_n_dk_ppno3a, option_y_dk_ttno3a, option_n_dk_ttno3a, option_y_dk_tt1no3a, option_n_dk_tt1no3a, option_y_dk_tt2no3a,
                option_n_dk_tt2no3a, option_y_dk_tt3no3a, option_n_dk_tt3no3a,
                option_y_dk_ppno3b, option_n_dk_ppno3b, option_y_dk_ttno3b, option_n_dk_ttno3b, option_y_dk_tt1no3b, option_n_dk_tt1no3b, option_y_dk_tt2no3b,
                option_n_dk_tt2no3b, option_y_dk_tt3no3b, option_n_dk_tt3no3b,
                option_y_dk_ppno3c, option_n_dk_ppno3c, option_y_dk_ttno3c, option_n_dk_ttno3c, option_y_dk_tt1no3c, option_n_dk_tt1no3c, option_y_dk_tt2no3c,
                option_n_dk_tt2no3c, option_y_dk_tt3no3c, option_n_dk_tt3no3c,
                option_y_dk_ppno3d, option_n_dk_ppno3d, option_y_dk_ttno3d, option_n_dk_ttno3d, option_y_dk_tt1no3d, option_n_dk_tt1no3d, option_y_dk_tt2no3d,
                option_n_dk_tt2no3d, option_y_dk_tt3no3d, option_n_dk_tt3no3d,
                essay_dk_no4, option_y_dk_ppno4, option_n_dk_ppno4, option_y_dk_ttno4, option_n_dk_ttno4, option_y_dk_tt1no4, option_n_dk_tt1no4, option_y_dk_tt2no4,
                option_n_dk_tt2no4, option_y_dk_tt3no4, option_n_dk_tt3no4,
                option_y_dk_ppno5, option_n_dk_ppno5, option_y_dk_ttno5, option_n_dk_ttno5, option_y_dk_tt1no5, option_n_dk_tt1no5, option_y_dk_tt2no5,
                option_n_dk_tt2no5, option_y_dk_tt3no5, option_n_dk_tt3no5,
                option_y_dk_ppno6, option_n_dk_ppno6, option_y_dk_ttno6, option_n_dk_ttno6, option_y_dk_tt1no6, option_n_dk_tt1no6, option_y_dk_tt2no6,
                option_n_dk_tt2no6, option_y_dk_tt3no6, option_n_dk_tt3no6,
                option_y_dk_ppno7, option_n_dk_ppno7, option_y_dk_ttno7, option_n_dk_ttno7, option_y_dk_tt1no7, option_n_dk_tt1no7, option_y_dk_tt2no7,
                option_n_dk_tt2no7, option_y_dk_tt3no7, option_n_dk_tt3no7,
                essay_dk_no7a, essay_dk_no7b, essay_dk_no7c, essay_dk_no7d, essay_dk_no8a,
                option_y_dk_ppno8a, option_n_dk_ppno8a, option_y_dk_ttno8a, option_n_dk_ttno8a, option_y_dk_tt1no8a, option_n_dk_tt1no8a, option_y_dk_tt2no8a,
                option_n_dk_tt2no8a, option_y_dk_tt3no8a, option_n_dk_tt3no8a,
                option_y_dk_ppno8b, option_n_dk_ppno8b, option_y_dk_ttno8b, option_n_dk_ttno8b, option_y_dk_tt1no8b, option_n_dk_tt1no8b, option_y_dk_tt2no8b,
                option_n_dk_tt2no8b, option_y_dk_tt3no8b, option_n_dk_tt3no8b,
                option_y_dk_ppno8c, option_n_dk_ppno8c, option_y_dk_ttno8c, option_n_dk_ttno8c, option_y_dk_tt1no8c, option_n_dk_tt1no8c, option_y_dk_tt2no8c,
                option_n_dk_tt2no8c, option_y_dk_tt3no8c, option_n_dk_tt3no8c,
                option_y_dk_ppno9, option_n_dk_ppno9, option_y_dk_ttno9, option_n_dk_ttno9, option_y_dk_tt1no9, option_n_dk_tt1no9, option_y_dk_tt2no9,
                option_n_dk_tt2no9, option_y_dk_tt3no9, option_n_dk_tt3no9
        };

        ListOfView.addAll(Arrays.asList(view));
        ListOfRadioGrupDK = new ArrayList<View>();

        View[] view_radio = {option_grup_dk_ppno1l, option_grup_dk_ttno1l, option_grup_dk_tt1no1l, option_grup_dk_tt2no1l, option_grup_dk_tt3no1l,
                option_grup_dk_ppno1ii, option_grup_dk_ttno1ii, option_grup_dk_tt1no1ii, option_grup_dk_tt2no1ii, option_grup_dk_tt3no1ii,
                option_grup_dk_ppno1iii, option_grup_dk_ttno1iii, option_grup_dk_tt1no1iii, option_grup_dk_tt2no1iii, option_grup_dk_tt3no1iii,
                option_grup_dk_ppno1iv, option_grup_dk_ttno1iv, option_grup_dk_tt1no1iv, option_grup_dk_tt2no1iv, option_grup_dk_tt3no1iv,
                option_grup_dk_ppno1v, option_grup_dk_ttno1v, option_grup_dk_tt1no1v, option_grup_dk_tt2no1v, option_grup_dk_tt3no1v,
                option_grup_dk_ppno1vi, option_grup_dk_ttno1vi, option_grup_dk_tt1no1vi, option_grup_dk_tt2no1vi, option_grup_dk_tt3no1vi,
                option_grup_dk_ppno1vii, option_grup_dk_ttno1vii, option_grup_dk_tt1no1vii, option_grup_dk_tt2no1vii, option_grup_dk_tt3no1vii,
                option_grup_dk_ppno1viii, option_grup_dk_ttno1viii, option_grup_dk_tt1no1viii, option_grup_dk_tt2no1viii, option_grup_dk_tt3no1viii,
                option_grup_dk_ppno1ix, option_grup_dk_ttno1ix, option_grup_dk_tt1no1ix, option_grup_dk_tt2no1ix, option_grup_dk_tt3no1ix,
                option_grup_dk_ppno1x, option_grup_dk_ttno1x, option_grup_dk_tt1no1x, option_grup_dk_tt2no1x, option_grup_dk_tt3no1x,
                option_grup_dk_ppno1xi, option_grup_dk_ttno1xi, option_grup_dk_tt1no1xi, option_grup_dk_tt2no1xi, option_grup_dk_tt3no1xi,
                option_grup_dk_ppno1xii, option_grup_dk_ttno1xii, option_grup_dk_tt1no1xii, option_grup_dk_tt2no1xii, option_grup_dk_tt3no1xii,
                option_grup_dk_ppno1xiii, option_grup_dk_ttno1xiii, option_grup_dk_tt1no1xiii, option_grup_dk_tt2no1xiii, option_grup_dk_tt3no1xiii,
                option_grup_dk_ppno1xiv, option_grup_dk_ttno1xiv, option_grup_dk_tt1no1xiv, option_grup_dk_tt2no1xiv, option_grup_dk_tt3no1xiv,
                option_grup_dk_ppno1xv, option_grup_dk_ttno1xv, option_grup_dk_tt1no1xv, option_grup_dk_tt2no1xv, option_grup_dk_tt3no1xv,
                option_grup_dk_ppno1xvi, option_grup_dk_ttno1xvi, option_grup_dk_tt1no1xvi, option_grup_dk_tt2no1xvi, option_grup_dk_tt3no1xvi,
                option_grup_dk_ppno1xvii, option_grup_dk_ttno1xvii, option_grup_dk_tt1no1xvii, option_grup_dk_tt2no1xvii, option_grup_dk_tt3no1xvii,
                option_grup_dk_ppno1xviii, option_grup_dk_ttno1xviii, option_grup_dk_tt1no1xviii, option_grup_dk_tt2no1xviii, option_grup_dk_tt3no1xviii,
                option_grup_dk_ppno1xix, option_grup_dk_ttno1xix, option_grup_dk_tt1no1xix, option_grup_dk_tt2no1xix, option_grup_dk_tt3no1xix,
                option_grup_dk_ppno1xx, option_grup_dk_ttno1xx, option_grup_dk_tt1no1xx, option_grup_dk_tt2no1xx, option_grup_dk_tt3no1xx,
                option_grup_dk_ppno1xxi, option_grup_dk_ttno1xxi, option_grup_dk_tt1no1xxi, option_grup_dk_tt2no1xxi, option_grup_dk_tt3no1xxi,
                option_grup_dk_ppno1xxii, option_grup_dk_ttno1xxii, option_grup_dk_tt1no1xxii, option_grup_dk_tt2no1xxii, option_grup_dk_tt3no1xxii,
                option_grup_dk_ppno1xxiii, option_grup_dk_ttno1xxiii, option_grup_dk_tt1no1xxiii, option_grup_dk_tt2no1xxiii, option_grup_dk_tt3no1xxiii,
                option_grup_dk_ppno1xxiv, option_grup_dk_ttno1xxiv, option_grup_dk_tt1no1xxiv, option_grup_dk_tt2no1xxiv, option_grup_dk_tt3no1xxiv,
                option_grup_dk_ppno1xxv, option_grup_dk_ttno1xxv, option_grup_dk_tt1no1xxv, option_grup_dk_tt2no1xxv, option_grup_dk_tt3no1xxv,
                option_grup_dk_ppno1xxvi, option_grup_dk_ttno1xxvi, option_grup_dk_tt1no1xxvi, option_grup_dk_tt2no1xxvi, option_grup_dk_tt3no1xxvi,
                option_grup_dk_ppno1xxvii, option_grup_dk_ttno1xxvii, option_grup_dk_tt1no1xxvii, option_grup_dk_tt2no1xxvii, option_grup_dk_tt3no1xxvii,
                option_grup_dk_ppno1xxviii, option_grup_dk_ttno1xxviii, option_grup_dk_tt1no1xxviii, option_grup_dk_tt2no1xxviii, option_grup_dk_tt3no1xxviii,
                option_grup_dk_ppno1xxix, option_grup_dk_ttno1xxix, option_grup_dk_tt1no1xxix, option_grup_dk_tt2no1xxix, option_grup_dk_tt3no1xxix,
                option_grup_dk_ppno1xxx, option_grup_dk_ttno1xxx, option_grup_dk_tt1no1xxx, option_grup_dk_tt2no1xxx, option_grup_dk_tt3no1xxx,
                option_grup_dk_ppno1xxxi, option_grup_dk_ttno1xxxi, option_grup_dk_tt1no1xxxi, option_grup_dk_tt2no1xxxi, option_grup_dk_tt3no1xxxi,
                option_grup_dk_ppno2, option_grup_dk_ttno2, option_grup_dk_tt1no2, option_grup_dk_tt2no2, option_grup_dk_tt3no2,
                option_grup_dk_ppno3a, option_grup_dk_ttno3a, option_grup_dk_tt1no3a, option_grup_dk_tt2no3a, option_grup_dk_tt3no3a,
                option_grup_dk_ppno3b, option_grup_dk_ttno3b, option_grup_dk_tt1no3b, option_grup_dk_tt2no3b, option_grup_dk_tt3no3b,
                option_grup_dk_ppno3c, option_grup_dk_ttno3c, option_grup_dk_tt1no3c, option_grup_dk_tt2no3c, option_grup_dk_tt3no3c,
                option_grup_dk_ppno3d, option_grup_dk_ttno3d, option_grup_dk_tt1no3d, option_grup_dk_tt2no3d, option_grup_dk_tt3no3d,
                option_grup_dk_ppno4, option_grup_dk_ttno4, option_grup_dk_tt1no4, option_grup_dk_tt2no4, option_grup_dk_tt3no4,
                option_grup_dk_ppno5, option_grup_dk_ttno5, option_grup_dk_tt1no5, option_grup_dk_tt2no5, option_grup_dk_tt3no5,
                option_grup_dk_ppno6, option_grup_dk_ttno6, option_grup_dk_tt1no6, option_grup_dk_tt2no6, option_grup_dk_tt3no6,
                option_grup_dk_ppno7, option_grup_dk_ttno7, option_grup_dk_tt1no7, option_grup_dk_tt2no7, option_grup_dk_tt3no7,
                option_grup_dk_ppno8a, option_grup_dk_ttno8a, option_grup_dk_tt1no8a, option_grup_dk_tt2no8a, option_grup_dk_tt3no8a,
                option_grup_dk_ppno8b, option_grup_dk_ttno8b, option_grup_dk_tt1no8b, option_grup_dk_tt2no8b, option_grup_dk_tt3no8b,
                option_grup_dk_ppno8c, option_grup_dk_ttno8c, option_grup_dk_tt1no8c, option_grup_dk_tt2no8c, option_grup_dk_tt3no8c,
                option_grup_dk_ppno9, option_grup_dk_ttno9, option_grup_dk_tt1no9, option_grup_dk_tt2no9, option_grup_dk_tt3no9};
        ListOfRadioGrupDK.addAll(Arrays.asList(view_radio));

        if (new E_Select(getContext()).getLine_Bus(me.getUsulanAsuransiModel().getKd_produk_ua()) == 3) { //SYARIAH
            qu1_text.setText(getString(R.string.qu1_syariah));
            qu3_text.setText(getString(R.string.qu3_syariah));
            qu4_text.setText(getString(R.string.qu4_syariah));
            qu5_text.setText(getString(R.string.qu5_syariah));
            qu6_text.setText(getString(R.string.qu6_syariah));
        } else {
            qu1_text.setText(getString(R.string.qu1));
            qu3_text.setText(getString(R.string.qu3));
            qu4_text.setText(getString(R.string.qu4));
            qu5_text.setText(getString(R.string.qu5));
            qu6_text.setText(getString(R.string.qu6));
        }


        restore();
        return kuisioner;
    }

    @Override
    public void onResume() {
        super.onResume();

        MethodSupport.onFocusviewtest(scroll_qu, cl_child_qu, essay_tt_no1);

        MethodSupport.active_view(1, btn_lanjut);
        MethodSupport.active_view(1, iv_next);
        MethodSupport.active_view(1, btn_kembali);
        MethodSupport.active_view(1, iv_prev);
        ProgressDialogClass.removeSimpleProgressDialog();

    }

    @Override
    public void onPause() {
        super.onPause();
        ProgressDialogClass.removeSimpleProgressDialog();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//View di activity
            case R.id.btn_lanjut:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_next:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_save:
                loadview();
                if (ll_tambah_data_calon_tertanggung_qu.getChildCount() > 0) {
                    AddModel_dkno9tt(count_dk_9tt);
                }
                if (ll_tambah_data_calon_pemegang_polis_qu.getChildCount() > 0) {
                    AddModel_dkno9pp(count_dk_9pp);
                }
                MyTaskSavePage taskSavePage = new MyTaskSavePage();
                taskSavePage.execute();

                break;
            case R.id.iv_prev:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.btn_kembali:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
//            case R.id.img_tambah_data_calon_tertanggung_qu:
//                AddModel_dkno9tt(count_dk_9tt);
//                count_dk_9tt++;
//                inflatedk_no9tt(count_dk_9tt,-1, -1, "", "");
//                v.setVisibility(View.VISIBLE);
//                MethodSupport.onFocusviewtest(scroll_qu, cl_form_tambah_data_calon_tertanggung_qu, ll_tambah_data_calon_tertanggung_qu);
//                break;
//            case R.id.img_tambah_data_calon_pemegang_polis_qu:
//                AddModel_dkno9pp(count_dk_9pp);
//                count_dk_9pp++;
//                inflatedk_no9pp(count_dk_9pp, -1, -1, "", "");
//                v.setVisibility(View.VISIBLE);
//                MethodSupport.onFocusviewtest(scroll_qu, cl_form_tambah_data_calon_pemegang_polis_qu, ll_tambah_data_calon_pemegang_polis_qu);
//                break;
        }
    }

    private void AddModel_dkno9tt(int count_dk_9ttadd) {
        ModelDK_NO9ttUQ no9ttUQ = null;
        if (!me.getModelUpdateQuisioner().getList_dkno9tt().isEmpty()){
            me.getModelUpdateQuisioner().getList_dkno9tt().clear();
        }
        if (! List_dkno9tt.isEmpty()){
            List_dkno9tt.clear();
        }
        for (int i = 0; i < count_dk_9ttadd; i++) {
            String calon = "";
            String kondisi = "";
            if(calon_dk_no9_tt[i+1].getSelectedItem()!=null){
                calon = calon_dk_no9_tt[i+1].getSelectedItem().toString();
            }
            if(keadaan_dk_no9_tt[i+1].getSelectedItem()!=null){
                kondisi = keadaan_dk_no9_tt[i+1].getSelectedItem().toString();
            }
            no9ttUQ = new ModelDK_NO9ttUQ(
                    i+1,calon_dk_no9_tt[i+1].getSelectedItemPosition(),
                    calon, umur_dk_no9_tt[i+1].getText().toString(),
                    kondisi, penyebab_dk_no9_tt[i+1].getText().toString(), keadaan_dk_no9_tt[i+1].getSelectedItemPosition());

            List_dkno9tt.add(no9ttUQ);
        }
        me.getModelUpdateQuisioner().setList_dkno9tt(List_dkno9tt);
    }

    private void AddModel_dkno9pp(int count_dk_9ppadd) {
        ModelDK_NO9ppUQ no9ppUQ = null;
        if (!me.getModelUpdateQuisioner().getList_dkno9pp().isEmpty()){
            me.getModelUpdateQuisioner().getList_dkno9pp().clear();
        }
        if (! List_dkno9pp.isEmpty()){
            List_dkno9pp.clear();
        }
        for (int i = 0; i < count_dk_9ppadd; i++) {
            String calon = "";
            String kondisi = "";
            if(calon_dk_no9_pp[i+1].getSelectedItem()!=null){
                calon = calon_dk_no9_pp[i+1].getSelectedItem().toString();
            }
            if(keadaan_dk_no9_pp[i+1].getSelectedItem()!=null){
                kondisi = keadaan_dk_no9_pp[i+1].getSelectedItem().toString();
            }
            no9ppUQ = new ModelDK_NO9ppUQ(
                    i+1,calon_dk_no9_pp[i+1].getSelectedItemPosition(),
                    calon, umur_dk_no9_pp[i+1].getText().toString(),
                    kondisi, penyebab_dk_no9_pp[i+1].getText().toString(),keadaan_dk_no9_pp[i+1].getSelectedItemPosition());

            List_dkno9pp.add(no9ppUQ);
        }
        me.getModelUpdateQuisioner().setList_dkno9pp(List_dkno9pp);
    }

    private class generalTextWatcher implements TextWatcher {

        private View view;

        generalTextWatcher(View view) {
            this.view = view;
        }


        @Override
        public void afterTextChanged(Editable s) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            int color;


            switch (view.getId()) {
                case R.id.essay_tt_no1:
                    if (essay_tt_no1.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_tt_no1, iv_circle_questionnaire1_qu, tv_error_questionnaire1_qu, getContext());

                    }
                    break;
                case R.id.essay_tt_no2:
                    if (essay_tt_no2.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_tt_no2, iv_circle_questionnaire2_qu, tv_error_questionnaire2_qu, getContext());
                    }
                    break;
                case R.id.essay_tt_no4:
                    if (essay_tt_no4.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_tt_no4, iv_circle_questionnaire4_qu, tv_error_questionnaire4_qu, getContext());
                    }
                    break;
                case R.id.essay_tt_no6:
                    if (essay_tt_no6.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_tt_no6, iv_circle_questionnaire6_qu, tv_error_questionnaire6_qu, getContext());
                    }
                    break;
                case R.id.essay_tt_no7:
                    if (essay_tt_no7.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_tt_no7, iv_circle_questionnaire7_qu, tv_error_questionnaire7_qu, getContext());
                    }
                    break;
                case R.id.essay_tt_1_no8a:
                    if (essay_tt_1_no8a.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_tt_1_no8a, iv_circle_questionnaire8_qu, val_tt_no8, getContext());
                    }
                    break;
                case R.id.essay_tt_2_no8a:
                    if (essay_tt_2_no8a.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_tt_2_no8a, iv_circle_questionnaire8_qu, val_tt_no8, getContext());
                    }
                    break;
                case R.id.essay_tt_rkk_no10:
                    if (essay_tt_rkk_no10.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_tt_rkk_no10, iv_circle_questionnaire10_qu, tv_error_questionnaire10_qu, getContext());
                    }
                    break;
                case R.id.essay_tt_cm_no11a:
                    if (essay_tt_cm_no11a.isEnabled()) {
                        Method_Validator.ValEditWatcher2text(essay_tt_cm_no11a, essay_tt_kg_no11a, iv_circle_questionnaire11a_qu, tv_error_tinggi_badan_qu, getContext());
                    }
                    break;
                case R.id.essay_tt_kg_no11a:
                    if (essay_tt_kg_no11a.isEnabled()) {
                        Method_Validator.ValEditWatcher2text(essay_tt_kg_no11a, essay_tt_cm_no11a, iv_circle_questionnaire11a_qu, tv_error_tinggi_badan_qu, getContext());
                    }
                    break;
                case R.id.essay_tt_no11b:
                    if (essay_tt_no11b.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_tt_no11b, iv_circle_questionnaire11b_qu, tv_error_questionnaire11b_qu, getContext());
                    }
                    break;
                case R.id.essay_tt_cm_no12:
                    if (essay_tt_cm_no12.isEnabled()) {
                        Method_Validator.ValEditWatcher2text(essay_tt_cm_no12, essay_tt_kg_no12, iv_circle_questionnaire12_qu, tv_error_tinggi_badan12_qu, getContext());
                    }
                    break;
                case R.id.essay_tt_kg_no12:
                    if (essay_tt_kg_no12.isEnabled()) {
                        Method_Validator.ValEditWatcher2text(essay_tt_kg_no12, essay_tt_cm_no12, iv_circle_questionnaire12_qu, tv_error_tinggi_badan12_qu, getContext());
                    }
                    break;
                case R.id.essay_pp_1_no13:
                    if (essay_pp_1_no13.isEnabled()) {
//                        iv_circle_questionnaire13a_qu.setVisibility(View.INVISIBLE);
                        Method_Validator.ValEditWatcher(essay_pp_1_no13, iv_circle_questionnaire13_qu, tv_error_questionnaire13_qu, getContext());
                    }
                    break;
                case R.id.essay_pp_2_no13:
                    if (essay_pp_2_no13.isEnabled()) {
//                        iv_circle_questionnaire13a_qu.setVisibility(View.VISIBLE);
                        Method_Validator.ValEditWatcher(essay_pp_2_no13, iv_circle_questionnaire13a_qu, tv_error_questionnaire13_qu, getContext());
                    }
                case R.id.essay_pp_1_no14:
                    if (essay_pp_1_no14.isEnabled()) {
//                        iv_circle_questionnaire13a_qu.setVisibility(View.INVISIBLE);
                        Method_Validator.ValEditWatcher(essay_pp_1_no14, iv_circle_questionnaire14_qu, tv_error_questionnaire14_qu, getContext());
                    }
                    break;
                case R.id.essay_pp_2_no14:
                    if (essay_pp_2_no14.isEnabled()) {
//                        iv_circle_questionnaire13a_qu.setVisibility(View.VISIBLE);
                        Method_Validator.ValEditWatcher(essay_pp_2_no14, iv_circle_questionnaire14a_qu, tv_error_questionnaire14_qu, getContext());
                    }
                    break;
                case R.id.essay_pp_cm_no15:
                    if (essay_pp_cm_no15.isEnabled()) {
                        Method_Validator.ValEditWatcher2text(essay_pp_cm_no15, essay_pp_kg_no15, iv_circle_questionnaire15_qu, tv_error_tinggi_badan2_qu, getContext());
                    }
                    break;
                case R.id.essay_pp_kg_no15:
                    if (essay_pp_kg_no15.isEnabled()) {
                        Method_Validator.ValEditWatcher2text(essay_pp_kg_no15, essay_pp_cm_no15, iv_circle_questionnaire15_qu, tv_error_berat_badan2_qu, getContext());
                    }
                    break;
                case R.id.essay_dk_no7a:
                    if (essay_dk_no7a.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_dk_no7a, iv_circle_data_kesehatan7a_qu, tv_error_essay_dk_no7a_qu, getContext());
                    }
                    break;
                case R.id.essay_dk_no7b:
                    if (essay_dk_no7b.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_dk_no7b, iv_circle_data_kesehatan7b_qu, tv_error_essay_dk_no7b_qu, getContext());
                    }
                    break;
                case R.id.essay_dk_no7c:
                    if (essay_dk_no7c.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_dk_no7c, iv_circle_data_kesehatan7c_qu, tv_error_essay_dk_no7c_qu, getContext());
                    }
                    break;
                case R.id.essay_dk_no7d:
                    if (essay_dk_no7d.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_dk_no7d, iv_circle_data_kesehatan7d_qu, tv_error_essay_dk_no7d_qu, getContext());
                    }
                    break;
            }
        }

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.option_grup_tt_no1:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire1_isi_qu);
                break;
            case R.id.option_grup_tt_no2:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire2_isi_qu);
                break;
            case R.id.option_grup_tt_no3:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire3_isi_qu);
                break;
            case R.id.option_grup_tt_no4:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire4_isi_qu);
                break;
            case R.id.option_grup_tt_no5:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire5_isi_qu);
                break;
            case R.id.option_grup_tt_no6:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire6_isi_qu);
                break;
            case R.id.option_grup_tt_no7:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire7_isi_qu);
                break;
            case R.id.option_grup_tt_no8:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire8_isi_qu);
                break;
            case R.id.option_grup_tt_no8b:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire8b_isi_qu);
                break;
            case R.id.option_grup_tt_no9:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire9_isi_qu);
                break;
            case R.id.option_grup_tt_no10:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire10_isi_qu);
                break;
            case R.id.option_grup_tt_no11b:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire11b_isi_qu);
                break;
            //pemegang polis
            case R.id.option_grup_pp_no13:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire13_isi_qu);
                break;
            case R.id.option_grup_pp_no14:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire14_isi_qu);
                break;
            //Data Kesehatan
            case R.id.option_grup_dk_ppno1l:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppi_qu);
                break;
            case R.id.option_grup_dk_ttno1l:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tti_qu);
                break;
            case R.id.option_grup_dk_tt1no1l:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_i_qu);
                break;
            case R.id.option_grup_dk_tt2no1l:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_i_qu);
                break;
            case R.id.option_grup_dk_tt3no1l:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_i_qu);
                break;
            case R.id.option_grup_dk_ppno1ii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppii_qu);
                break;
            case R.id.option_grup_dk_ttno1ii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttii_qu);
                break;
            case R.id.option_grup_dk_tt1no1ii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_ii_qu);
                break;
            case R.id.option_grup_dk_tt2no1ii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_ii_qu);
                break;
            case R.id.option_grup_dk_tt3no1ii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_ii_qu);
                break;
            case R.id.option_grup_dk_ppno1iii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppiii_qu);
                break;
            case R.id.option_grup_dk_ttno1iii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttiii_qu);
                break;
            case R.id.option_grup_dk_tt1no1iii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_iii_qu);
                break;
            case R.id.option_grup_dk_tt2no1iii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_iii_qu);
                break;
            case R.id.option_grup_dk_tt3no1iii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_iii_qu);
                break;
            case R.id.option_grup_dk_ppno1iv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppiv_qu);
                break;
            case R.id.option_grup_dk_ttno1iv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttiv_qu);
                break;
            case R.id.option_grup_dk_tt1no1iv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_iv_qu);
                break;
            case R.id.option_grup_dk_tt2no1iv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_iv_qu);
                break;
            case R.id.option_grup_dk_tt3no1iv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_iv_qu);
                break;
            case R.id.option_grup_dk_ppno1v:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppv_qu);
                break;
            case R.id.option_grup_dk_ttno1v:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttv_qu);
                break;
            case R.id.option_grup_dk_tt1no1v:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_v_qu);
                break;
            case R.id.option_grup_dk_tt2no1v:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_v_qu);
                break;
            case R.id.option_grup_dk_tt3no1v:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_v_qu);
                break;
            case R.id.option_grup_dk_ppno1vi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppvi_qu);
                break;
            case R.id.option_grup_dk_ttno1vi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttvi_qu);
                break;
            case R.id.option_grup_dk_tt1no1vi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_vi_qu);
                break;
            case R.id.option_grup_dk_tt2no1vi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_vi_qu);
                break;
            case R.id.option_grup_dk_tt3no1vi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_vi_qu);
                break;
            case R.id.option_grup_dk_ppno1vii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppvii_qu);
                break;
            case R.id.option_grup_dk_ttno1vii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttvii_qu);
                break;
            case R.id.option_grup_dk_tt1no1vii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_vii_qu);
                break;
            case R.id.option_grup_dk_tt2no1vii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_vii_qu);
                break;
            case R.id.option_grup_dk_tt3no1vii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_vii_qu);
                break;
            case R.id.option_grup_dk_ppno1viii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppviii_qu);
                break;
            case R.id.option_grup_dk_ttno1viii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttviii_qu);
                break;
            case R.id.option_grup_dk_tt1no1viii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_viii_qu);
                break;
            case R.id.option_grup_dk_tt2no1viii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_viii_qu);
                break;
            case R.id.option_grup_dk_tt3no1viii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_viii_qu);
                break;
            case R.id.option_grup_dk_ppno1ix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppix_qu);
                break;
            case R.id.option_grup_dk_ttno1ix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttix_qu);
                break;
            case R.id.option_grup_dk_tt1no1ix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_ix_qu);
                break;
            case R.id.option_grup_dk_tt2no1ix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_ix_qu);
                break;
            case R.id.option_grup_dk_tt3no1ix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_ix_qu);
                break;
            case R.id.option_grup_dk_ppno1x:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppx_qu);
                break;
            case R.id.option_grup_dk_ttno1x:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttx_qu);
                break;
            case R.id.option_grup_dk_tt1no1x:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_x_qu);
                break;
            case R.id.option_grup_dk_tt2no1x:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_x_qu);
                break;
            case R.id.option_grup_dk_tt3no1x:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_x_qu);
                break;
            case R.id.option_grup_dk_ppno1xi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxi_qu);
                break;
            case R.id.option_grup_dk_ttno1xi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxi_qu);
                break;
            case R.id.option_grup_dk_tt1no1xi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xi_qu);
                break;
            case R.id.option_grup_dk_tt2no1xi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xi_qu);
                break;
            case R.id.option_grup_dk_tt3no1xi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xi_qu);
                break;
            case R.id.option_grup_dk_ppno1xii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxii_qu);
                break;
            case R.id.option_grup_dk_ttno1xii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxii_qu);
                break;
            case R.id.option_grup_dk_tt1no1xii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xii_qu);
                break;
            case R.id.option_grup_dk_tt2no1xii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xii_qu);
                break;
            case R.id.option_grup_dk_tt3no1xii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xii_qu);
                break;
            case R.id.option_grup_dk_ppno1xiii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxiii_qu);
                break;
            case R.id.option_grup_dk_ttno1xiii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxiii_qu);
                break;
            case R.id.option_grup_dk_tt1no1xiii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xiii_qu);
                break;
            case R.id.option_grup_dk_tt2no1xiii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xiii_qu);
                break;
            case R.id.option_grup_dk_tt3no1xiii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xiii_qu);
                break;
            case R.id.option_grup_dk_ppno1xiv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxiv_qu);
                break;
            case R.id.option_grup_dk_ttno1xiv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxiv_qu);
                break;
            case R.id.option_grup_dk_tt1no1xiv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xiv_qu);
                break;
            case R.id.option_grup_dk_tt2no1xiv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xiv_qu);
                break;
            case R.id.option_grup_dk_tt3no1xiv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xiv_qu);
                break;
            case R.id.option_grup_dk_ppno1xv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxv_qu);
                break;
            case R.id.option_grup_dk_ttno1xv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxv_qu);
                break;
            case R.id.option_grup_dk_tt1no1xv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xv_qu);
                break;
            case R.id.option_grup_dk_tt2no1xv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xv_qu);
                break;
            case R.id.option_grup_dk_tt3no1xv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xv_qu);
                break;
            case R.id.option_grup_dk_ppno1xvi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxvi_qu);
                break;
            case R.id.option_grup_dk_ttno1xvi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxvi_qu);
                break;
            case R.id.option_grup_dk_tt1no1xvi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xvi_qu);
                break;
            case R.id.option_grup_dk_tt2no1xvi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xvi_qu);
                break;
            case R.id.option_grup_dk_tt3no1xvi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xvi_qu);
                break;
            case R.id.option_grup_dk_ppno1xvii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxvii_qu);
                break;
            case R.id.option_grup_dk_ttno1xvii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxvii_qu);
                break;
            case R.id.option_grup_dk_tt1no1xvii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xvii_qu);
                break;
            case R.id.option_grup_dk_tt2no1xvii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xvii_qu);
                break;
            case R.id.option_grup_dk_tt3no1xvii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xvii_qu);
                break;
            case R.id.option_grup_dk_ppno1xviii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxviii_qu);
                break;
            case R.id.option_grup_dk_ttno1xviii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxviii_qu);
                break;
            case R.id.option_grup_dk_tt1no1xviii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xviii_qu);
                break;
            case R.id.option_grup_dk_tt2no1xviii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xviii_qu);
                break;
            case R.id.option_grup_dk_tt3no1xviii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xviii_qu);
                break;
            case R.id.option_grup_dk_ppno1xix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxix_qu);
                break;
            case R.id.option_grup_dk_ttno1xix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxix_qu);
                break;
            case R.id.option_grup_dk_tt1no1xix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xix_qu);
                break;
            case R.id.option_grup_dk_tt2no1xix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xix_qu);
                break;
            case R.id.option_grup_dk_tt3no1xix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xix_qu);
                break;
            case R.id.option_grup_dk_ppno1xx:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxx_qu);
                break;
            case R.id.option_grup_dk_ttno1xx:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxx_qu);
                break;
            case R.id.option_grup_dk_tt1no1xx:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xx_qu);
                break;
            case R.id.option_grup_dk_tt2no1xx:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xx_qu);
                break;
            case R.id.option_grup_dk_tt3no1xx:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xx_qu);
                break;
            case R.id.option_grup_dk_ppno1xxi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxxi_qu);
                break;
            case R.id.option_grup_dk_ttno1xxi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxxi_qu);
                break;
            case R.id.option_grup_dk_tt1no1xxi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xxi_qu);
                break;
            case R.id.option_grup_dk_tt2no1xxi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xxi_qu);
                break;
            case R.id.option_grup_dk_tt3no1xxi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xxi_qu);
                break;
            case R.id.option_grup_dk_ppno1xxii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxxii_qu);
                break;
            case R.id.option_grup_dk_ttno1xxii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxxii_qu);
                break;
            case R.id.option_grup_dk_tt1no1xxii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xxii_qu);
                break;
            case R.id.option_grup_dk_tt2no1xxii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xxii_qu);
                break;
            case R.id.option_grup_dk_tt3no1xxii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xxii_qu);
                break;
            case R.id.option_grup_dk_ppno1xxiii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxxiii_qu);
                break;
            case R.id.option_grup_dk_ttno1xxiii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxxiii_qu);
                break;
            case R.id.option_grup_dk_tt1no1xxiii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xxiii_qu);
                break;
            case R.id.option_grup_dk_tt2no1xxiii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xxiii_qu);
                break;
            case R.id.option_grup_dk_tt3no1xxiii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xxiii_qu);
                break;
            case R.id.option_grup_dk_ppno1xxiv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxxiv_qu);
                break;
            case R.id.option_grup_dk_ttno1xxiv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxxiv_qu);
                break;
            case R.id.option_grup_dk_tt1no1xxiv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xxiv_qu);
                break;
            case R.id.option_grup_dk_tt2no1xxiv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xxiv_qu);
                break;
            case R.id.option_grup_dk_tt3no1xxiv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xxiv_qu);
                break;
            case R.id.option_grup_dk_ppno1xxv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxxv_qu);
                break;
            case R.id.option_grup_dk_ttno1xxv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxxv_qu);
                break;
            case R.id.option_grup_dk_tt1no1xxv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xxv_qu);
                break;
            case R.id.option_grup_dk_tt2no1xxv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xxv_qu);
                break;
            case R.id.option_grup_dk_tt3no1xxv:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xxv_qu);
                break;
            case R.id.option_grup_dk_ppno1xxvi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxxvi_qu);
                break;
            case R.id.option_grup_dk_ttno1xxvi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxxvi_qu);
                break;
            case R.id.option_grup_dk_tt1no1xxvi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xxvi_qu);
                break;
            case R.id.option_grup_dk_tt2no1xxvi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xxvi_qu);
                break;
            case R.id.option_grup_dk_tt3no1xxvi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xxvi_qu);
                break;
            case R.id.option_grup_dk_ppno1xxvii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxxvii_qu);
                break;
            case R.id.option_grup_dk_ttno1xxvii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxxvii_qu);
                break;
            case R.id.option_grup_dk_tt1no1xxvii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xxvii_qu);
                break;
            case R.id.option_grup_dk_tt2no1xxvii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xxvii_qu);
                break;
            case R.id.option_grup_dk_tt3no1xxvii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xxvii_qu);
                break;
            case R.id.option_grup_dk_ppno1xxviii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxxviii_qu);
                break;
            case R.id.option_grup_dk_ttno1xxviii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxxviii_qu);
                break;
            case R.id.option_grup_dk_tt1no1xxviii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xxviii_qu);
                break;
            case R.id.option_grup_dk_tt2no1xxviii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xxviii_qu);
                break;
            case R.id.option_grup_dk_tt3no1xxviii:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xxviii_qu);
                break;
            case R.id.option_grup_dk_ppno1xxix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxxix_qu);
                break;
            case R.id.option_grup_dk_ttno1xxix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxxix_qu);
                break;
            case R.id.option_grup_dk_tt1no1xxix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xxix_qu);
                break;
            case R.id.option_grup_dk_tt2no1xxix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xxix_qu);
                break;
            case R.id.option_grup_dk_tt3no1xxix:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xxix_qu);
                break;
            case R.id.option_grup_dk_ppno1xxx:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxxx_qu);
                break;
            case R.id.option_grup_dk_ttno1xxx:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxxx_qu);
                break;
            case R.id.option_grup_dk_tt1no1xxx:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xxx_qu);
                break;
            case R.id.option_grup_dk_tt2no1xxx:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xxx_qu);
                break;
            case R.id.option_grup_dk_tt3no1xxx:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xxx_qu);
                break;
            case R.id.option_grup_dk_ppno1xxxi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ppxxxi_qu);
                break;
            case R.id.option_grup_dk_ttno1xxxi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_ttxxxi_qu);
                break;
            case R.id.option_grup_dk_tt1no1xxxi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_xxxi_qu);
                break;
            case R.id.option_grup_dk_tt2no1xxxi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_xxxi_qu);
                break;
            case R.id.option_grup_dk_tt3no1xxxi:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_xxxi_qu);
                break;
            case R.id.option_grup_dk_ppno2:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_pp_qu);
                break;
            case R.id.option_grup_dk_ttno2:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt_qu);
                break;
            case R.id.option_grup_dk_tt1no2:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_qu);
                break;
            case R.id.option_grup_dk_tt2no2:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_qu);
                break;
            case R.id.option_grup_dk_tt3no2:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_qu);
                break;
            case R.id.option_grup_dk_ppno3a:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_pp3a_qu);
                break;
            case R.id.option_grup_dk_ttno3a:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3a_qu);
                break;
            case R.id.option_grup_dk_tt1no3a:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_3a_qu);
                break;
            case R.id.option_grup_dk_tt2no3a:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_3a_qu);
                break;
            case R.id.option_grup_dk_tt3no3a:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_3a_qu);
                break;
            case R.id.option_grup_dk_ppno3b:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_pp3b_qu);
                break;
            case R.id.option_grup_dk_ttno3b:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3b_qu);
                break;
            case R.id.option_grup_dk_tt1no3b:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_3b_qu);
                break;
            case R.id.option_grup_dk_tt2no3b:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_3b_qu);
                break;
            case R.id.option_grup_dk_tt3no3b:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_3b_qu);
                break;
            case R.id.option_grup_dk_ppno3c:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_pp3c_qu);
                break;
            case R.id.option_grup_dk_ttno3c:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3c_qu);
                break;
            case R.id.option_grup_dk_tt1no3c:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_3c_qu);
                break;
            case R.id.option_grup_dk_tt2no3c:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_3c_qu);
                break;
            case R.id.option_grup_dk_tt3no3c:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_3c_qu);
                break;
            case R.id.option_grup_dk_ppno3d:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_pp3d_qu);
                break;
            case R.id.option_grup_dk_ttno3d:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3d_qu);
                break;
            case R.id.option_grup_dk_tt1no3d:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_3d_qu);
                break;
            case R.id.option_grup_dk_tt2no3d:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_3d_qu);
                break;
            case R.id.option_grup_dk_tt3no3d:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_3d_qu);
                break;
            case R.id.option_grup_dk_ppno4:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_pp4_qu);
                break;
            case R.id.option_grup_dk_ttno4:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt4_qu);
                break;
            case R.id.option_grup_dk_tt1no4:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_4_qu);
                break;
            case R.id.option_grup_dk_tt2no4:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_4_qu);
                break;
            case R.id.option_grup_dk_tt3no4:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_4_qu);
                break;
            case R.id.option_grup_dk_ppno5:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_pp5_qu);
                break;
            case R.id.option_grup_dk_ttno5:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt5_qu);
                break;
            case R.id.option_grup_dk_tt1no5:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_5_qu);
                break;
            case R.id.option_grup_dk_tt2no5:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_5_qu);
                break;
            case R.id.option_grup_dk_tt3no5:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_5_qu);
                break;
            case R.id.option_grup_dk_ppno6:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_pp6_qu);
                break;
            case R.id.option_grup_dk_ttno6:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt6_qu);
                break;
            case R.id.option_grup_dk_tt1no6:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_6_qu);
                break;
            case R.id.option_grup_dk_tt2no6:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_6_qu);
                break;
            case R.id.option_grup_dk_tt3no6:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_6_qu);
                break;
            case R.id.option_grup_dk_ppno7:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_pp7_qu);
                break;
            case R.id.option_grup_dk_ttno7:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt7_qu);
                break;
            case R.id.option_grup_dk_tt1no7:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_7_qu);
                break;
            case R.id.option_grup_dk_tt2no7:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_7_qu);
                break;
            case R.id.option_grup_dk_tt3no7:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_7_qu);
                break;
            case R.id.option_grup_dk_ppno8a:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_pp8a_qu);
                break;
            case R.id.option_grup_dk_ttno8a:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt8a_qu);
                break;
            case R.id.option_grup_dk_tt1no8a:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_8a_qu);
                break;
            case R.id.option_grup_dk_tt2no8a:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_8a_qu);
                break;
            case R.id.option_grup_dk_tt3no8a:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_8a_qu);
                break;
            case R.id.option_grup_dk_ppno8b:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_pp8b_qu);
                break;
            case R.id.option_grup_dk_ttno8b:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt8b_qu);
                break;
            case R.id.option_grup_dk_tt1no8b:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_8b_qu);
                break;
            case R.id.option_grup_dk_tt2no8b:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_8b_qu);
                break;
            case R.id.option_grup_dk_tt3no8b:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_8b_qu);
                break;
            case R.id.option_grup_dk_ppno8c:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_pp8c_qu);
                break;
            case R.id.option_grup_dk_ttno8c:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt8c_qu);
                break;
            case R.id.option_grup_dk_tt1no8c:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_8c_qu);
                break;
            case R.id.option_grup_dk_tt2no8c:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_8c_qu);
                break;
            case R.id.option_grup_dk_tt3no8c:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_8c_qu);
                break;
            case R.id.option_grup_dk_ppno9:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_pp9_qu);
                break;
            case R.id.option_grup_dk_ttno9:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt9_qu);
                break;
            case R.id.option_grup_dk_tt1no9:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt1_9_qu);
                break;
            case R.id.option_grup_dk_tt2no9:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt2_9_qu);
                break;
            case R.id.option_grup_dk_tt3no9:
                MethodSupport.focuslayouttext(hasFocus, cl_form_calon_tt3_9_qu);
                break;

            //RadioButton
            //Tertanggung
            case R.id.option_y_tt_no1:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire1_isi_qu, option_grup_tt_no1, R.id.option_y_tt_no1);
                break;
            case R.id.option_n_tt_no1:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire1_isi_qu, option_grup_tt_no1, R.id.option_n_tt_no1);
                break;
            case R.id.option_y_tt_no2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire2_isi_qu, option_grup_tt_no2, R.id.option_y_tt_no2);
                break;
            case R.id.option_n_tt_no2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire2_isi_qu, option_grup_tt_no2, R.id.option_n_tt_no2);
                break;
            case R.id.option_y_tt_no3:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire3_isi_qu, option_grup_tt_no3, R.id.option_y_tt_no3);
                break;
            case R.id.option_n_tt_no3:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire3_isi_qu, option_grup_tt_no3, R.id.option_n_tt_no3);
                break;
            case R.id.option_y_tt_no4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire4_isi_qu, option_grup_tt_no4, R.id.option_y_tt_no4);
                break;
            case R.id.option_n_tt_no4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire4_isi_qu, option_grup_tt_no4, R.id.option_n_tt_no4);
                break;
            case R.id.option_y_tt_no5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire5_isi_qu, option_grup_tt_no5, R.id.option_y_tt_no5);
                break;
            case R.id.option_n_tt_no5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire5_isi_qu, option_grup_tt_no5, R.id.option_n_tt_no5);
                break;
            case R.id.option_y_tt_no6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire6_isi_qu, option_grup_tt_no6, R.id.option_y_tt_no6);
                break;
            case R.id.option_n_tt_no6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire6_isi_qu, option_grup_tt_no6, R.id.option_n_tt_no6);
                break;
            case R.id.option_y_tt_no7:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire7_isi_qu, option_grup_tt_no7, R.id.option_y_tt_no7);
                break;
            case R.id.option_n_tt_no7:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire7_isi_qu, option_grup_tt_no7, R.id.option_n_tt_no7);
                break;
            case R.id.option_y_tt_no8:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire8_isi_qu, option_grup_tt_no8, R.id.option_y_tt_no8);
                break;
            case R.id.option_n_tt_no8:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire8_isi_qu, option_grup_tt_no8, R.id.option_n_tt_no8);
                break;
            case R.id.option_y_tt_no8b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire8b_isi_qu, option_grup_tt_no8b, R.id.option_y_tt_no8b);
                break;
            case R.id.option_n_tt_no8b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire8b_isi_qu, option_grup_tt_no8b, R.id.option_n_tt_no8b);
                break;
            case R.id.option_y_tt_no9:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire9_isi_qu, option_grup_tt_no9, R.id.option_y_tt_no9);
                break;
            case R.id.option_n_tt_no9:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire9_isi_qu, option_grup_tt_no9, R.id.option_n_tt_no9);
                break;
            case R.id.option_y_tt_no10:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire10_isi_qu, option_grup_tt_no10, R.id.option_y_tt_no10);
                break;
            case R.id.option_n_tt_no10:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire10_isi_qu, option_grup_tt_no10, R.id.option_n_tt_no10);
                break;
            case R.id.option_y_tt_no11b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire11b_isi_qu, option_grup_tt_no11b, R.id.option_y_tt_no11b);
                break;
            case R.id.option_n_tt_no11b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire11b_isi_qu, option_grup_tt_no11b, R.id.option_n_tt_no11b);
                break;
            //pemegang polis
            case R.id.option_y_pp_no13:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire13_isi_qu, option_grup_pp_no13, R.id.option_y_pp_no13);
                break;
            case R.id.option_n_pp_no13:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire13_isi_qu, option_grup_pp_no13, R.id.option_n_pp_no13);
                break;
            case R.id.option_y_pp_no14:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire14_isi_qu, option_grup_pp_no14, R.id.option_y_pp_no14);
                break;
            case R.id.option_n_pp_no14:
                MethodSupport.focuslayoutRadio(hasFocus, cl_questionnaire14_isi_qu, option_grup_pp_no14, R.id.option_n_pp_no14);
                break;
            //data kesehatan
            case R.id.option_y_dk_ppno1l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppi_qu, option_grup_dk_ppno1l, R.id.option_y_dk_ppno1l);
                break;
            case R.id.option_n_dk_ppno1l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppi_qu,
                        option_grup_dk_ppno1l, R.id.option_n_dk_ppno1l);
                break;
            case R.id.option_y_dk_ttno1l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tti_qu,
                        option_grup_dk_ttno1l, R.id.option_y_dk_ttno1l);
                break;
            case R.id.option_n_dk_ttno1l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tti_qu,
                        option_grup_dk_ttno1l, R.id.option_n_dk_ttno1l);
                break;
            case R.id.option_y_dk_tt1no1l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_i_qu,
                        option_grup_dk_tt1no1l, R.id.option_y_dk_tt1no1l);
                break;
            case R.id.option_n_dk_tt1no1l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_i_qu,
                        option_grup_dk_tt1no1l, R.id.option_n_dk_tt1no1l);
                break;
            case R.id.option_y_dk_tt2no1l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_i_qu,
                        option_grup_dk_tt2no1l, R.id.option_y_dk_tt2no1l);
                break;
            case R.id.option_n_dk_tt2no1l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_i_qu,
                        option_grup_dk_tt2no1l, R.id.option_n_dk_tt2no1l);
                break;
            case R.id.option_y_dk_tt3no1l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_i_qu,
                        option_grup_dk_tt3no1l, R.id.option_y_dk_tt3no1l);
                break;
            case R.id.option_n_dk_tt3no1l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_i_qu,
                        option_grup_dk_tt3no1l, R.id.option_n_dk_tt3no1l);
                break;
            case R.id.option_y_dk_ppno1ii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppii_qu,
                        option_grup_dk_ppno1ii, R.id.option_y_dk_ppno1ii);
                break;
            case R.id.option_n_dk_ppno1ii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppii_qu,
                        option_grup_dk_ppno1ii, R.id.option_n_dk_ppno1ii);
                break;
            case R.id.option_y_dk_ttno1ii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttii_qu,
                        option_grup_dk_ttno1ii, R.id.option_y_dk_ttno1ii);
                break;
            case R.id.option_n_dk_ttno1ii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttii_qu,
                        option_grup_dk_ttno1ii, R.id.option_n_dk_ttno1ii);
                break;
            case R.id.option_y_dk_tt1no1ii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_ii_qu,
                        option_grup_dk_tt1no1ii, R.id.option_y_dk_tt1no1ii);
                break;
            case R.id.option_n_dk_tt1no1ii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_ii_qu,
                        option_grup_dk_tt1no1ii, R.id.option_n_dk_tt1no1ii);
                break;
            case R.id.option_y_dk_tt2no1ii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_ii_qu,
                        option_grup_dk_tt2no1ii, R.id.option_y_dk_tt2no1ii);
                break;
            case R.id.option_n_dk_tt2no1ii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_ii_qu,
                        option_grup_dk_tt2no1ii, R.id.option_n_dk_tt2no1ii);
                break;
            case R.id.option_y_dk_tt3no1ii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_ii_qu,
                        option_grup_dk_tt3no1ii, R.id.option_y_dk_tt3no1ii);
                break;
            case R.id.option_n_dk_tt3no1ii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_ii_qu,
                        option_grup_dk_tt3no1ii, R.id.option_n_dk_tt3no1ii);
                break;
            case R.id.option_y_dk_ppno1iii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppiii_qu,
                        option_grup_dk_ppno1iii, R.id.option_y_dk_ppno1iii);
                break;
            case R.id.option_n_dk_ppno1iii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppiii_qu,
                        option_grup_dk_ppno1iii, R.id.option_n_dk_ppno1iii);
                break;
            case R.id.option_y_dk_ttno1iii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttiii_qu,
                        option_grup_dk_ttno1iii, R.id.option_y_dk_ttno1iii);
                break;
            case R.id.option_n_dk_ttno1iii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttiii_qu,
                        option_grup_dk_ttno1iii, R.id.option_n_dk_ttno1iii);
                break;
            case R.id.option_y_dk_tt1no1iii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_iii_qu,
                        option_grup_dk_tt1no1iii, R.id.option_y_dk_tt1no1iii);
                break;
            case R.id.option_n_dk_tt1no1iii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_iii_qu,
                        option_grup_dk_tt1no1iii, R.id.option_n_dk_tt1no1iii);
                break;
            case R.id.option_y_dk_tt2no1iii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_iii_qu,
                        option_grup_dk_tt2no1iii, R.id.option_y_dk_tt2no1iii);
                break;
            case R.id.option_n_dk_tt2no1iii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_iii_qu,
                        option_grup_dk_tt2no1iii, R.id.option_n_dk_tt2no1iii);
                break;
            case R.id.option_y_dk_tt3no1iii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_iii_qu,
                        option_grup_dk_tt3no1iii, R.id.option_y_dk_tt3no1iii);
                break;
            case R.id.option_n_dk_tt3no1iii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_iii_qu,
                        option_grup_dk_tt3no1iii, R.id.option_n_dk_tt3no1iii);
                break;
            case R.id.option_y_dk_ppno1iv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppiv_qu,
                        option_grup_dk_ppno1iv, R.id.option_y_dk_ppno1iv);
                break;
            case R.id.option_n_dk_ppno1iv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppiv_qu,
                        option_grup_dk_ppno1iv, R.id.option_n_dk_ppno1iv);
                break;
            case R.id.option_y_dk_ttno1iv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttiv_qu,
                        option_grup_dk_ttno1iv, R.id.option_y_dk_ttno1iv);
                break;
            case R.id.option_n_dk_ttno1iv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttiv_qu,
                        option_grup_dk_ttno1iv, R.id.option_n_dk_ttno1iv);
                break;
            case R.id.option_y_dk_tt1no1iv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_iv_qu,
                        option_grup_dk_tt1no1iv, R.id.option_y_dk_tt1no1iv);
                break;
            case R.id.option_n_dk_tt1no1iv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_iv_qu,
                        option_grup_dk_tt1no1iv, R.id.option_n_dk_tt1no1iv);
                break;
            case R.id.option_y_dk_tt2no1iv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_iv_qu,
                        option_grup_dk_tt2no1iv, R.id.option_y_dk_tt2no1iv);
                break;
            case R.id.option_n_dk_tt2no1iv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_iv_qu,
                        option_grup_dk_tt2no1iv, R.id.option_n_dk_tt2no1iv);
                break;
            case R.id.option_y_dk_tt3no1iv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_iv_qu,
                        option_grup_dk_tt3no1iv, R.id.option_y_dk_tt3no1iv);
                break;
            case R.id.option_n_dk_tt3no1iv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_iv_qu,
                        option_grup_dk_tt3no1iv, R.id.option_n_dk_tt3no1iv);
                break;
            case R.id.option_y_dk_ppno1v:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppv_qu,
                        option_grup_dk_ppno1v, R.id.option_y_dk_ppno1v);
                break;
            case R.id.option_n_dk_ppno1v:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppv_qu,
                        option_grup_dk_ppno1v, R.id.option_n_dk_ppno1v);
                break;
            case R.id.option_y_dk_ttno1v:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttv_qu,
                        option_grup_dk_ttno1v, R.id.option_y_dk_ttno1v);
                break;
            case R.id.option_n_dk_ttno1v:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttv_qu,
                        option_grup_dk_ttno1v, R.id.option_n_dk_ttno1v);
                break;
            case R.id.option_y_dk_tt1no1v:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_v_qu,
                        option_grup_dk_tt1no1v, R.id.option_y_dk_tt1no1v);
                break;
            case R.id.option_n_dk_tt1no1v:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_v_qu,
                        option_grup_dk_tt1no1v, R.id.option_n_dk_tt1no1v);
                break;
            case R.id.option_y_dk_tt2no1v:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_v_qu,
                        option_grup_dk_tt2no1v, R.id.option_y_dk_tt2no1v);
                break;
            case R.id.option_n_dk_tt2no1v:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_v_qu,
                        option_grup_dk_tt2no1v, R.id.option_n_dk_tt2no1v);
                break;
            case R.id.option_y_dk_tt3no1v:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_v_qu,
                        option_grup_dk_tt3no1v, R.id.option_y_dk_tt3no1v);
                break;
            case R.id.option_n_dk_tt3no1v:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_v_qu,
                        option_grup_dk_tt3no1v, R.id.option_n_dk_tt3no1v);
                break;
            case R.id.option_y_dk_ppno1vi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppvi_qu,
                        option_grup_dk_ppno1vi, R.id.option_y_dk_ppno1vi);
                break;
            case R.id.option_n_dk_ppno1vi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppvi_qu,
                        option_grup_dk_ppno1vi, R.id.option_n_dk_ppno1vi);
                break;
            case R.id.option_y_dk_ttno1vi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttvi_qu,
                        option_grup_dk_ttno1vi, R.id.option_y_dk_ttno1vi);
                break;
            case R.id.option_n_dk_ttno1vi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttvi_qu,
                        option_grup_dk_ttno1vi, R.id.option_n_dk_ttno1vi);
                break;
            case R.id.option_y_dk_tt1no1vi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_vi_qu,
                        option_grup_dk_tt1no1vi, R.id.option_y_dk_tt1no1vi);
                break;
            case R.id.option_n_dk_tt1no1vi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_vi_qu,
                        option_grup_dk_tt1no1vi, R.id.option_n_dk_tt1no1vi);
                break;
            case R.id.option_y_dk_tt2no1vi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_vi_qu,
                        option_grup_dk_tt2no1vi, R.id.option_y_dk_tt2no1vi);
                break;
            case R.id.option_n_dk_tt2no1vi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_vi_qu,
                        option_grup_dk_tt2no1vi, R.id.option_n_dk_tt2no1vi);
                break;
            case R.id.option_y_dk_tt3no1vi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_vi_qu,
                        option_grup_dk_tt3no1vi, R.id.option_y_dk_tt3no1vi);
                break;
            case R.id.option_n_dk_tt3no1vi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_vi_qu,
                        option_grup_dk_tt3no1vi, R.id.option_n_dk_tt3no1vi);
                break;
            case R.id.option_y_dk_ppno1vii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppvii_qu,
                        option_grup_dk_ppno1vii, R.id.option_y_dk_ppno1vii);
                break;
            case R.id.option_n_dk_ppno1vii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppvii_qu,
                        option_grup_dk_ppno1vii, R.id.option_n_dk_ppno1vii);
                break;
            case R.id.option_y_dk_ttno1vii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttvii_qu,
                        option_grup_dk_ttno1vii, R.id.option_y_dk_ttno1vii);
                break;
            case R.id.option_n_dk_ttno1vii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttvii_qu,
                        option_grup_dk_ttno1vii, R.id.option_n_dk_ttno1vii);
                break;
            case R.id.option_y_dk_tt1no1vii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_vii_qu,
                        option_grup_dk_tt1no1vii, R.id.option_y_dk_tt1no1vii);
                break;
            case R.id.option_n_dk_tt1no1vii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_vii_qu,
                        option_grup_dk_tt1no1vii, R.id.option_n_dk_tt1no1vii);
                break;
            case R.id.option_y_dk_tt2no1vii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_vii_qu,
                        option_grup_dk_tt2no1vii, R.id.option_y_dk_tt2no1vii);
                break;
            case R.id.option_n_dk_tt2no1vii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_vii_qu,
                        option_grup_dk_tt2no1vii, R.id.option_n_dk_tt2no1vii);
                break;
            case R.id.option_y_dk_tt3no1vii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_vii_qu,
                        option_grup_dk_tt3no1vii, R.id.option_y_dk_tt3no1vii);
                break;
            case R.id.option_n_dk_tt3no1vii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_vii_qu,
                        option_grup_dk_tt3no1vii, R.id.option_n_dk_tt3no1vii);
                break;
            case R.id.option_y_dk_ppno1viii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppviii_qu,
                        option_grup_dk_ppno1viii, R.id.option_y_dk_ppno1viii);
                break;
            case R.id.option_n_dk_ppno1viii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppviii_qu,
                        option_grup_dk_ppno1viii, R.id.option_n_dk_ppno1viii);
                break;
            case R.id.option_y_dk_ttno1viii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttviii_qu,
                        option_grup_dk_ttno1viii, R.id.option_y_dk_ttno1viii);
                break;
            case R.id.option_n_dk_ttno1viii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttviii_qu,
                        option_grup_dk_ttno1viii, R.id.option_n_dk_ttno1viii);
                break;
            case R.id.option_y_dk_tt1no1viii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_viii_qu,
                        option_grup_dk_tt1no1viii, R.id.option_y_dk_tt1no1viii);
                break;
            case R.id.option_n_dk_tt1no1viii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_viii_qu,
                        option_grup_dk_tt1no1viii, R.id.option_n_dk_tt1no1viii);
                break;
            case R.id.option_y_dk_tt2no1viii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_viii_qu,
                        option_grup_dk_tt2no1viii, R.id.option_y_dk_tt2no1viii);
                break;
            case R.id.option_n_dk_tt2no1viii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_viii_qu,
                        option_grup_dk_tt2no1viii, R.id.option_n_dk_tt2no1viii);
                break;
            case R.id.option_y_dk_tt3no1viii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_viii_qu,
                        option_grup_dk_tt3no1viii, R.id.option_y_dk_tt3no1viii);
                break;
            case R.id.option_n_dk_tt3no1viii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_viii_qu,
                        option_grup_dk_tt3no1viii, R.id.option_n_dk_tt3no1viii);
                break;
            case R.id.option_y_dk_ppno1ix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppix_qu,
                        option_grup_dk_ppno1ix, R.id.option_y_dk_ppno1ix);
                break;
            case R.id.option_n_dk_ppno1ix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppix_qu,
                        option_grup_dk_ppno1ix, R.id.option_n_dk_ppno1ix);
                break;
            case R.id.option_y_dk_ttno1ix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttix_qu,
                        option_grup_dk_ttno1ix, R.id.option_y_dk_ttno1ix);
                break;
            case R.id.option_n_dk_ttno1ix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttix_qu,
                        option_grup_dk_ttno1ix, R.id.option_n_dk_ttno1ix);
                break;
            case R.id.option_y_dk_tt1no1ix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_ix_qu,
                        option_grup_dk_tt1no1ix, R.id.option_y_dk_tt1no1ix);
                break;
            case R.id.option_n_dk_tt1no1ix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_ix_qu,
                        option_grup_dk_tt1no1ix, R.id.option_n_dk_tt1no1ix);
                break;
            case R.id.option_y_dk_tt2no1ix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_ix_qu,
                        option_grup_dk_tt2no1ix, R.id.option_y_dk_tt2no1ix);
                break;
            case R.id.option_n_dk_tt2no1ix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_ix_qu,
                        option_grup_dk_tt2no1ix, R.id.option_n_dk_tt2no1ix);
                break;
            case R.id.option_y_dk_tt3no1ix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_ix_qu,
                        option_grup_dk_tt3no1ix, R.id.option_y_dk_tt3no1ix);
                break;
            case R.id.option_n_dk_tt3no1ix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_ix_qu,
                        option_grup_dk_tt3no1ix, R.id.option_n_dk_tt3no1ix);
                break;
            case R.id.option_y_dk_ppno1x:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppx_qu,
                        option_grup_dk_ppno1x, R.id.option_y_dk_ppno1x);
                break;
            case R.id.option_n_dk_ppno1x:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppx_qu,
                        option_grup_dk_ppno1x, R.id.option_n_dk_ppno1x);
                break;
            case R.id.option_y_dk_ttno1x:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttx_qu,
                        option_grup_dk_ttno1x, R.id.option_y_dk_ttno1x);
                break;
            case R.id.option_n_dk_ttno1x:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttx_qu,
                        option_grup_dk_ttno1x, R.id.option_n_dk_ttno1x);
                break;
            case R.id.option_y_dk_tt1no1x:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_x_qu,
                        option_grup_dk_tt1no1x, R.id.option_y_dk_tt1no1x);
                break;
            case R.id.option_n_dk_tt1no1x:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_x_qu,
                        option_grup_dk_tt1no1x, R.id.option_n_dk_tt1no1x);
                break;
            case R.id.option_y_dk_tt2no1x:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_x_qu,
                        option_grup_dk_tt2no1x, R.id.option_y_dk_tt2no1x);
                break;
            case R.id.option_n_dk_tt2no1x:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_x_qu,
                        option_grup_dk_tt2no1x, R.id.option_n_dk_tt2no1x);
                break;
            case R.id.option_y_dk_tt3no1x:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_x_qu,
                        option_grup_dk_tt3no1x, R.id.option_y_dk_tt3no1x);
                break;
            case R.id.option_n_dk_tt3no1x:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_x_qu,
                        option_grup_dk_tt3no1x, R.id.option_n_dk_tt3no1x);
                break;
            case R.id.option_y_dk_ppno1xi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxi_qu,
                        option_grup_dk_ppno1xi, R.id.option_y_dk_ppno1xi);
                break;
            case R.id.option_n_dk_ppno1xi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxi_qu,
                        option_grup_dk_ppno1xi, R.id.option_n_dk_ppno1xi);
                break;
            case R.id.option_y_dk_ttno1xi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxi_qu,
                        option_grup_dk_ttno1xi, R.id.option_y_dk_ttno1xi);
                break;
            case R.id.option_n_dk_ttno1xi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxi_qu,
                        option_grup_dk_ttno1xi, R.id.option_n_dk_ttno1xi);
                break;
            case R.id.option_y_dk_tt1no1xi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xi_qu,
                        option_grup_dk_tt1no1xi, R.id.option_y_dk_tt1no1xi);
                break;
            case R.id.option_n_dk_tt1no1xi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xi_qu,
                        option_grup_dk_tt1no1xi, R.id.option_n_dk_tt1no1xi);
                break;
            case R.id.option_y_dk_tt2no1xi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xi_qu,
                        option_grup_dk_tt2no1xi, R.id.option_y_dk_tt2no1xi);
                break;
            case R.id.option_n_dk_tt2no1xi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xi_qu,
                        option_grup_dk_tt2no1xi, R.id.option_n_dk_tt2no1xi);
                break;
            case R.id.option_y_dk_tt3no1xi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xi_qu,
                        option_grup_dk_tt3no1xi, R.id.option_y_dk_tt3no1xi);
                break;
            case R.id.option_n_dk_tt3no1xi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xi_qu,
                        option_grup_dk_tt3no1xi, R.id.option_n_dk_tt3no1xi);
                break;
            case R.id.option_y_dk_ppno1xii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxii_qu,
                        option_grup_dk_ppno1xii, R.id.option_y_dk_ppno1xii);
                break;
            case R.id.option_n_dk_ppno1xii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxii_qu,
                        option_grup_dk_ppno1xii, R.id.option_n_dk_ppno1xii);
                break;
            case R.id.option_y_dk_ttno1xii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxii_qu,
                        option_grup_dk_ttno1xii, R.id.option_y_dk_ttno1xii);
                break;
            case R.id.option_n_dk_ttno1xii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxii_qu,
                        option_grup_dk_ttno1xii, R.id.option_n_dk_ttno1xii);
                break;
            case R.id.option_y_dk_tt1no1xii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xii_qu,
                        option_grup_dk_tt1no1xii, R.id.option_y_dk_tt1no1xii);
                break;
            case R.id.option_n_dk_tt1no1xii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xii_qu,
                        option_grup_dk_tt1no1xii, R.id.option_n_dk_tt1no1xii);
                break;
            case R.id.option_y_dk_tt2no1xii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xii_qu,
                        option_grup_dk_tt2no1xii, R.id.option_y_dk_tt2no1xii);
                break;
            case R.id.option_n_dk_tt2no1xii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xii_qu,
                        option_grup_dk_tt2no1xii, R.id.option_n_dk_tt2no1xii);
                break;
            case R.id.option_y_dk_tt3no1xii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xii_qu,
                        option_grup_dk_tt3no1xii, R.id.option_y_dk_tt3no1xii);
                break;
            case R.id.option_n_dk_tt3no1xii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xii_qu,
                        option_grup_dk_tt3no1xii, R.id.option_n_dk_tt3no1xii);
                break;
            case R.id.option_y_dk_ppno1xiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxiii_qu,
                        option_grup_dk_ppno1xiii, R.id.option_y_dk_ppno1xiii);
                break;
            case R.id.option_n_dk_ppno1xiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxiii_qu,
                        option_grup_dk_ppno1xiii, R.id.option_n_dk_ppno1xiii);
                break;
            case R.id.option_y_dk_ttno1xiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxiii_qu,
                        option_grup_dk_ttno1xiii, R.id.option_y_dk_ttno1xiii);
                break;
            case R.id.option_n_dk_ttno1xiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxiii_qu,
                        option_grup_dk_ttno1xiii, R.id.option_n_dk_ttno1xiii);
                break;
            case R.id.option_y_dk_tt1no1xiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xiii_qu,
                        option_grup_dk_tt1no1xiii, R.id.option_y_dk_tt1no1xiii);
                break;
            case R.id.option_n_dk_tt1no1xiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xiii_qu,
                        option_grup_dk_tt1no1xiii, R.id.option_n_dk_tt1no1xiii);
                break;
            case R.id.option_y_dk_tt2no1xiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xiii_qu,
                        option_grup_dk_tt2no1xiii, R.id.option_y_dk_tt2no1xiii);
                break;
            case R.id.option_n_dk_tt2no1xiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xiii_qu,
                        option_grup_dk_tt2no1xiii, R.id.option_n_dk_tt2no1xiii);
                break;
            case R.id.option_y_dk_tt3no1xiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xiii_qu,
                        option_grup_dk_tt3no1xiii, R.id.option_y_dk_tt3no1xiii);
                break;
            case R.id.option_n_dk_tt3no1xiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xiii_qu,
                        option_grup_dk_tt3no1xiii, R.id.option_n_dk_tt3no1xiii);
                break;
            case R.id.option_y_dk_ppno1xiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxiv_qu,
                        option_grup_dk_ppno1xiv, R.id.option_y_dk_ppno1xiv);
                break;
            case R.id.option_n_dk_ppno1xiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxiv_qu,
                        option_grup_dk_ppno1xiv, R.id.option_n_dk_ppno1xiv);
                break;
            case R.id.option_y_dk_ttno1xiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxiv_qu,
                        option_grup_dk_ttno1xiv, R.id.option_y_dk_ttno1xiv);
                break;
            case R.id.option_n_dk_ttno1xiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxiv_qu,
                        option_grup_dk_ttno1xiv, R.id.option_n_dk_ttno1xiv);
                break;
            case R.id.option_y_dk_tt1no1xiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xiv_qu,
                        option_grup_dk_tt1no1xiv, R.id.option_y_dk_tt1no1xiv);
                break;
            case R.id.option_n_dk_tt1no1xiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xiv_qu,
                        option_grup_dk_tt1no1xiv, R.id.option_n_dk_tt1no1xiv);
                break;
            case R.id.option_y_dk_tt2no1xiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xiv_qu,
                        option_grup_dk_tt2no1xiv, R.id.option_y_dk_tt2no1xiv);
                break;
            case R.id.option_n_dk_tt2no1xiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xiv_qu,
                        option_grup_dk_tt2no1xiv, R.id.option_n_dk_tt2no1xiv);
                break;
            case R.id.option_y_dk_tt3no1xiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xiv_qu,
                        option_grup_dk_tt3no1xiv, R.id.option_y_dk_tt3no1xiv);
                break;
            case R.id.option_n_dk_tt3no1xiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xiv_qu,
                        option_grup_dk_tt3no1xiv, R.id.option_n_dk_tt3no1xiv);
                break;
            case R.id.option_y_dk_ppno1xv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxv_qu,
                        option_grup_dk_ppno1xv, R.id.option_y_dk_ppno1xv);
                break;
            case R.id.option_n_dk_ppno1xv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxv_qu,
                        option_grup_dk_ppno1xv, R.id.option_n_dk_ppno1xv);
                break;
            case R.id.option_y_dk_ttno1xv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxv_qu,
                        option_grup_dk_ttno1xv, R.id.option_y_dk_ttno1xv);
                break;
            case R.id.option_n_dk_ttno1xv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxv_qu,
                        option_grup_dk_ttno1xv, R.id.option_n_dk_ttno1xv);
                break;
            case R.id.option_y_dk_tt1no1xv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xv_qu,
                        option_grup_dk_tt1no1xv, R.id.option_y_dk_tt1no1xv);
                break;
            case R.id.option_n_dk_tt1no1xv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xv_qu,
                        option_grup_dk_tt1no1xv, R.id.option_n_dk_tt1no1xv);
                break;
            case R.id.option_y_dk_tt2no1xv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xv_qu,
                        option_grup_dk_tt2no1xv, R.id.option_y_dk_tt2no1xv);
                break;
            case R.id.option_n_dk_tt2no1xv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xv_qu,
                        option_grup_dk_tt2no1xv, R.id.option_n_dk_tt2no1xv);
                break;
            case R.id.option_y_dk_tt3no1xv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xv_qu,
                        option_grup_dk_tt3no1xv, R.id.option_y_dk_tt3no1xv);
                break;
            case R.id.option_n_dk_tt3no1xv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xv_qu,
                        option_grup_dk_tt3no1xv, R.id.option_n_dk_tt3no1xv);
                break;
            case R.id.option_y_dk_ppno1xvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxvi_qu,
                        option_grup_dk_ppno1xvi, R.id.option_y_dk_ppno1xvi);
                break;
            case R.id.option_n_dk_ppno1xvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxvi_qu,
                        option_grup_dk_ppno1xvi, R.id.option_n_dk_ppno1xvi);
                break;
            case R.id.option_y_dk_ttno1xvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxvi_qu,
                        option_grup_dk_ttno1xvi, R.id.option_y_dk_ttno1xvi);
                break;
            case R.id.option_n_dk_ttno1xvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxvi_qu,
                        option_grup_dk_ttno1xvi, R.id.option_n_dk_ttno1xvi);
                break;
            case R.id.option_y_dk_tt1no1xvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xvi_qu,
                        option_grup_dk_tt1no1xvi, R.id.option_y_dk_tt1no1xvi);
                break;
            case R.id.option_n_dk_tt1no1xvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xvi_qu,
                        option_grup_dk_tt1no1xvi, R.id.option_n_dk_tt1no1xvi);
                break;
            case R.id.option_y_dk_tt2no1xvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xvi_qu,
                        option_grup_dk_tt2no1xvi, R.id.option_y_dk_tt2no1xvi);
                break;
            case R.id.option_n_dk_tt2no1xvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xvi_qu,
                        option_grup_dk_tt2no1xvi, R.id.option_n_dk_tt2no1xvi);
                break;
            case R.id.option_y_dk_tt3no1xvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xvi_qu,
                        option_grup_dk_tt3no1xvi, R.id.option_y_dk_tt3no1xvi);
                break;
            case R.id.option_n_dk_tt3no1xvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xvi_qu,
                        option_grup_dk_tt3no1xvi, R.id.option_n_dk_tt3no1xvi);
                break;
            case R.id.option_y_dk_ppno1xvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxvii_qu,
                        option_grup_dk_ppno1xvii, R.id.option_y_dk_ppno1xvii);
                break;
            case R.id.option_n_dk_ppno1xvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxvii_qu,
                        option_grup_dk_ppno1xvii, R.id.option_n_dk_ppno1xvii);
                break;
            case R.id.option_y_dk_ttno1xvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxvii_qu,
                        option_grup_dk_ttno1xvii, R.id.option_y_dk_ttno1xvii);
                break;
            case R.id.option_n_dk_ttno1xvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxvii_qu,
                        option_grup_dk_ttno1xvii, R.id.option_n_dk_ttno1xvii);
                break;
            case R.id.option_y_dk_tt1no1xvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xvii_qu,
                        option_grup_dk_tt1no1xvii, R.id.option_y_dk_tt1no1xvii);
                break;
            case R.id.option_n_dk_tt1no1xvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xvii_qu,
                        option_grup_dk_tt1no1xvii, R.id.option_n_dk_tt1no1xvii);
                break;
            case R.id.option_y_dk_tt2no1xvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xvii_qu,
                        option_grup_dk_tt2no1xvii, R.id.option_y_dk_tt2no1xvii);
                break;
            case R.id.option_n_dk_tt2no1xvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xvii_qu,
                        option_grup_dk_tt2no1xvii, R.id.option_n_dk_tt2no1xvii);
                break;
            case R.id.option_y_dk_tt3no1xvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xvii_qu,
                        option_grup_dk_tt3no1xvii, R.id.option_y_dk_tt3no1xvii);
                break;
            case R.id.option_n_dk_tt3no1xvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xvii_qu,
                        option_grup_dk_tt3no1xvii, R.id.option_n_dk_tt3no1xvii);
                break;
            case R.id.option_y_dk_ppno1xviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxviii_qu,
                        option_grup_dk_ppno1xviii, R.id.option_y_dk_ppno1xviii);
                break;
            case R.id.option_n_dk_ppno1xviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxviii_qu,
                        option_grup_dk_ppno1xviii, R.id.option_n_dk_ppno1xviii);
                break;
            case R.id.option_y_dk_ttno1xviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxviii_qu,
                        option_grup_dk_ttno1xviii, R.id.option_y_dk_ttno1xviii);
                break;
            case R.id.option_n_dk_ttno1xviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxviii_qu,
                        option_grup_dk_ttno1xviii, R.id.option_n_dk_ttno1xviii);
                break;
            case R.id.option_y_dk_tt1no1xviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xviii_qu,
                        option_grup_dk_tt1no1xviii, R.id.option_y_dk_tt1no1xviii);
                break;
            case R.id.option_n_dk_tt1no1xviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xviii_qu,
                        option_grup_dk_tt1no1xviii, R.id.option_n_dk_tt1no1xviii);
                break;
            case R.id.option_y_dk_tt2no1xviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xviii_qu,
                        option_grup_dk_tt2no1xviii, R.id.option_y_dk_tt2no1xviii);
                break;
            case R.id.option_n_dk_tt2no1xviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xviii_qu,
                        option_grup_dk_tt2no1xviii, R.id.option_n_dk_tt2no1xviii);
                break;
            case R.id.option_y_dk_tt3no1xviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xviii_qu,
                        option_grup_dk_tt3no1xviii, R.id.option_y_dk_tt3no1xviii);
                break;
            case R.id.option_n_dk_tt3no1xviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xviii_qu,
                        option_grup_dk_tt3no1xviii, R.id.option_n_dk_tt3no1xviii);
                break;
            case R.id.option_y_dk_ppno1xix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxix_qu,
                        option_grup_dk_ppno1xix, R.id.option_y_dk_ppno1xix);
                break;
            case R.id.option_n_dk_ppno1xix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxix_qu,
                        option_grup_dk_ppno1xix, R.id.option_n_dk_ppno1xix);
                break;
            case R.id.option_y_dk_ttno1xix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxix_qu,
                        option_grup_dk_ttno1xix, R.id.option_y_dk_ttno1xix);
                break;
            case R.id.option_n_dk_ttno1xix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxix_qu,
                        option_grup_dk_ttno1xix, R.id.option_n_dk_ttno1xix);
                break;
            case R.id.option_y_dk_tt1no1xix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xix_qu,
                        option_grup_dk_tt1no1xix, R.id.option_y_dk_tt1no1xix);
                break;
            case R.id.option_n_dk_tt1no1xix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xix_qu,
                        option_grup_dk_tt1no1xix, R.id.option_n_dk_tt1no1xix);
                break;
            case R.id.option_y_dk_tt2no1xix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xix_qu,
                        option_grup_dk_tt2no1xix, R.id.option_y_dk_tt2no1xix);
                break;
            case R.id.option_n_dk_tt2no1xix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xix_qu,
                        option_grup_dk_tt2no1xix, R.id.option_n_dk_tt2no1xix);
                break;
            case R.id.option_y_dk_tt3no1xix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xix_qu,
                        option_grup_dk_tt3no1xix, R.id.option_y_dk_tt3no1xix);
                break;
            case R.id.option_n_dk_tt3no1xix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xix_qu,
                        option_grup_dk_tt3no1xix, R.id.option_n_dk_tt3no1xix);
                break;
            case R.id.option_y_dk_ppno1xx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxx_qu,
                        option_grup_dk_ppno1xx, R.id.option_y_dk_ppno1xx);
                break;
            case R.id.option_n_dk_ppno1xx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxx_qu,
                        option_grup_dk_ppno1xx, R.id.option_n_dk_ppno1xx);
                break;
            case R.id.option_y_dk_ttno1xx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxx_qu,
                        option_grup_dk_ttno1xx, R.id.option_y_dk_ttno1xx);
                break;
            case R.id.option_n_dk_ttno1xx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxx_qu,
                        option_grup_dk_ttno1xx, R.id.option_n_dk_ttno1xx);
                break;
            case R.id.option_y_dk_tt1no1xx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xx_qu,
                        option_grup_dk_tt1no1xx, R.id.option_y_dk_tt1no1xx);
                break;
            case R.id.option_n_dk_tt1no1xx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xx_qu,
                        option_grup_dk_tt1no1xx, R.id.option_n_dk_tt1no1xx);
                break;
            case R.id.option_y_dk_tt2no1xx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xx_qu,
                        option_grup_dk_tt2no1xx, R.id.option_y_dk_tt2no1xx);
                break;
            case R.id.option_n_dk_tt2no1xx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xx_qu,
                        option_grup_dk_tt2no1xx, R.id.option_n_dk_tt2no1xx);
                break;
            case R.id.option_y_dk_tt3no1xx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xx_qu,
                        option_grup_dk_tt3no1xx, R.id.option_y_dk_tt3no1xx);
                break;
            case R.id.option_n_dk_tt3no1xx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xx_qu,
                        option_grup_dk_tt3no1xx, R.id.option_n_dk_tt3no1xx);
                break;
            case R.id.option_y_dk_ppno1xxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxi_qu,
                        option_grup_dk_ppno1xxi, R.id.option_y_dk_ppno1xxi);
                break;
            case R.id.option_n_dk_ppno1xxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxi_qu,
                        option_grup_dk_ppno1xxi, R.id.option_n_dk_ppno1xxi);
                break;
            case R.id.option_y_dk_ttno1xxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxi_qu,
                        option_grup_dk_ttno1xxi, R.id.option_y_dk_ttno1xxi);
                break;
            case R.id.option_n_dk_ttno1xxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxi_qu,
                        option_grup_dk_ttno1xxi, R.id.option_n_dk_ttno1xxi);
                break;
            case R.id.option_y_dk_tt1no1xxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxi_qu,
                        option_grup_dk_tt1no1xxi, R.id.option_y_dk_tt1no1xxi);
                break;
            case R.id.option_n_dk_tt1no1xxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxi_qu,
                        option_grup_dk_tt1no1xxi, R.id.option_n_dk_tt1no1xxi);
                break;
            case R.id.option_y_dk_tt2no1xxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxi_qu,
                        option_grup_dk_tt2no1xxi, R.id.option_y_dk_tt2no1xxi);
                break;
            case R.id.option_n_dk_tt2no1xxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxi_qu,
                        option_grup_dk_tt2no1xxi, R.id.option_n_dk_tt2no1xxi);
                break;
            case R.id.option_y_dk_tt3no1xxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxi_qu,
                        option_grup_dk_tt3no1xxi, R.id.option_y_dk_tt3no1xxi);
                break;
            case R.id.option_n_dk_tt3no1xxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxi_qu,
                        option_grup_dk_tt3no1xxi, R.id.option_n_dk_tt3no1xxi);
                break;
            case R.id.option_y_dk_ppno1xxii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxii_qu,
                        option_grup_dk_ppno1xxii, R.id.option_y_dk_ppno1xxii);
                break;
            case R.id.option_n_dk_ppno1xxii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxii_qu,
                        option_grup_dk_ppno1xxii, R.id.option_n_dk_ppno1xxii);
                break;
            case R.id.option_y_dk_ttno1xxii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxii_qu,
                        option_grup_dk_ttno1xxii, R.id.option_y_dk_ttno1xxii);
                break;
            case R.id.option_n_dk_ttno1xxii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxii_qu,
                        option_grup_dk_ttno1xxii, R.id.option_n_dk_ttno1xxii);
                break;
            case R.id.option_y_dk_tt1no1xxii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxii_qu,
                        option_grup_dk_tt1no1xxii, R.id.option_y_dk_tt1no1xxii);
                break;
            case R.id.option_n_dk_tt1no1xxii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxii_qu,
                        option_grup_dk_tt1no1xxii, R.id.option_n_dk_tt1no1xxii);
                break;
            case R.id.option_y_dk_tt2no1xxii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxii_qu,
                        option_grup_dk_tt2no1xxii, R.id.option_y_dk_tt2no1xxii);
                break;
            case R.id.option_n_dk_tt2no1xxii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxii_qu,
                        option_grup_dk_tt2no1xxii, R.id.option_n_dk_tt2no1xxii);
                break;
            case R.id.option_y_dk_tt3no1xxii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxii_qu,
                        option_grup_dk_tt3no1xxii, R.id.option_y_dk_tt3no1xxii);
                break;
            case R.id.option_n_dk_tt3no1xxii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxii_qu,
                        option_grup_dk_tt3no1xxii, R.id.option_n_dk_tt3no1xxii);
                break;
            case R.id.option_y_dk_ppno1xxiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxiii_qu,
                        option_grup_dk_ppno1xxiii, R.id.option_y_dk_ppno1xxiii);
                break;
            case R.id.option_n_dk_ppno1xxiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxiii_qu,
                        option_grup_dk_ppno1xxiii, R.id.option_n_dk_ppno1xxiii);
                break;
            case R.id.option_y_dk_ttno1xxiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxiii_qu,
                        option_grup_dk_ttno1xxiii, R.id.option_y_dk_ttno1xxiii);
                break;
            case R.id.option_n_dk_ttno1xxiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxiii_qu,
                        option_grup_dk_ttno1xxiii, R.id.option_n_dk_ttno1xxiii);
                break;
            case R.id.option_y_dk_tt1no1xxiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxiii_qu,
                        option_grup_dk_tt1no1xxiii, R.id.option_y_dk_tt1no1xxiii);
                break;
            case R.id.option_n_dk_tt1no1xxiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxiii_qu,
                        option_grup_dk_tt1no1xxiii, R.id.option_n_dk_tt1no1xxiii);
                break;
            case R.id.option_y_dk_tt2no1xxiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxiii_qu,
                        option_grup_dk_tt2no1xxiii, R.id.option_y_dk_tt2no1xxiii);
                break;
            case R.id.option_n_dk_tt2no1xxiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxiii_qu,
                        option_grup_dk_tt2no1xxiii, R.id.option_n_dk_tt2no1xxiii);
                break;
            case R.id.option_y_dk_tt3no1xxiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxiii_qu,
                        option_grup_dk_tt3no1xxiii, R.id.option_y_dk_tt3no1xxiii);
                break;
            case R.id.option_n_dk_tt3no1xxiii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxiii_qu,
                        option_grup_dk_tt3no1xxiii, R.id.option_n_dk_tt3no1xxiii);
                break;
            case R.id.option_y_dk_ppno1xxiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxiv_qu,
                        option_grup_dk_ppno1xxiv, R.id.option_y_dk_ppno1xxiv);
                break;
            case R.id.option_n_dk_ppno1xxiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxiv_qu,
                        option_grup_dk_ppno1xxiv, R.id.option_n_dk_ppno1xxiv);
                break;
            case R.id.option_y_dk_ttno1xxiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxiv_qu,
                        option_grup_dk_ttno1xxiv, R.id.option_y_dk_ttno1xxiv);
                break;
            case R.id.option_n_dk_ttno1xxiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxiv_qu,
                        option_grup_dk_ttno1xxiv, R.id.option_n_dk_ttno1xxiv);
                break;
            case R.id.option_y_dk_tt1no1xxiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxiv_qu,
                        option_grup_dk_tt1no1xxiv, R.id.option_y_dk_tt1no1xxiv);
                break;
            case R.id.option_n_dk_tt1no1xxiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxiv_qu,
                        option_grup_dk_tt1no1xxiv, R.id.option_n_dk_tt1no1xxiv);
                break;
            case R.id.option_y_dk_tt2no1xxiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxiv_qu,
                        option_grup_dk_tt2no1xxiv, R.id.option_y_dk_tt2no1xxiv);
                break;
            case R.id.option_n_dk_tt2no1xxiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxiv_qu,
                        option_grup_dk_tt2no1xxiv, R.id.option_n_dk_tt2no1xxiv);
                break;
            case R.id.option_y_dk_tt3no1xxiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxiv_qu,
                        option_grup_dk_tt3no1xxiv, R.id.option_y_dk_tt3no1xxiv);
                break;
            case R.id.option_n_dk_tt3no1xxiv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxiv_qu,
                        option_grup_dk_tt3no1xxiv, R.id.option_n_dk_tt3no1xxiv);
                break;
            case R.id.option_y_dk_ppno1xxv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxv_qu,
                        option_grup_dk_ppno1xxv, R.id.option_y_dk_ppno1xxv);
                break;
            case R.id.option_n_dk_ppno1xxv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxv_qu,
                        option_grup_dk_ppno1xxv, R.id.option_n_dk_ppno1xxv);
                break;
            case R.id.option_y_dk_ttno1xxv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxv_qu,
                        option_grup_dk_ttno1xxv, R.id.option_y_dk_ttno1xxv);
                break;
            case R.id.option_n_dk_ttno1xxv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxv_qu,
                        option_grup_dk_ttno1xxv, R.id.option_n_dk_ttno1xxv);
                break;
            case R.id.option_y_dk_tt1no1xxv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxv_qu,
                        option_grup_dk_tt1no1xxv, R.id.option_y_dk_tt1no1xxv);
                break;
            case R.id.option_n_dk_tt1no1xxv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxv_qu,
                        option_grup_dk_tt1no1xxv, R.id.option_n_dk_tt1no1xxv);
                break;
            case R.id.option_y_dk_tt2no1xxv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxv_qu,
                        option_grup_dk_tt2no1xxv, R.id.option_y_dk_tt2no1xxv);
                break;
            case R.id.option_n_dk_tt2no1xxv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxv_qu,
                        option_grup_dk_tt2no1xxv, R.id.option_n_dk_tt2no1xxv);
                break;
            case R.id.option_y_dk_tt3no1xxv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxv_qu,
                        option_grup_dk_tt3no1xxv, R.id.option_y_dk_tt3no1xxv);
                break;
            case R.id.option_n_dk_tt3no1xxv:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxv_qu,
                        option_grup_dk_tt3no1xxv, R.id.option_n_dk_tt3no1xxv);
                break;
            case R.id.option_y_dk_ppno1xxvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxvi_qu,
                        option_grup_dk_ppno1xxvi, R.id.option_y_dk_ppno1xxvi);
                break;
            case R.id.option_n_dk_ppno1xxvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxvi_qu,
                        option_grup_dk_ppno1xxvi, R.id.option_n_dk_ppno1xxvi);
                break;
            case R.id.option_y_dk_ttno1xxvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxvi_qu,
                        option_grup_dk_ttno1xxvi, R.id.option_y_dk_ttno1xxvi);
                break;
            case R.id.option_n_dk_ttno1xxvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxvi_qu,
                        option_grup_dk_ttno1xxvi, R.id.option_n_dk_ttno1xxvi);
                break;
            case R.id.option_y_dk_tt1no1xxvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxvi_qu,
                        option_grup_dk_tt1no1xxvi, R.id.option_y_dk_tt1no1xxvi);
                break;
            case R.id.option_n_dk_tt1no1xxvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxvi_qu,
                        option_grup_dk_tt1no1xxvi, R.id.option_n_dk_tt1no1xxvi);
                break;
            case R.id.option_y_dk_tt2no1xxvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxvi_qu,
                        option_grup_dk_tt2no1xxvi, R.id.option_y_dk_tt2no1xxvi);
                break;
            case R.id.option_n_dk_tt2no1xxvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxvi_qu,
                        option_grup_dk_tt2no1xxvi, R.id.option_n_dk_tt2no1xxvi);
                break;
            case R.id.option_y_dk_tt3no1xxvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxvi_qu,
                        option_grup_dk_tt3no1xxvi, R.id.option_y_dk_tt3no1xxvi);
                break;
            case R.id.option_n_dk_tt3no1xxvi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxvi_qu,
                        option_grup_dk_tt3no1xxvi, R.id.option_n_dk_tt3no1xxvi);
                break;
            case R.id.option_y_dk_ppno1xxvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxvii_qu,
                        option_grup_dk_ppno1xxvii, R.id.option_y_dk_ppno1xxvii);
                break;
            case R.id.option_n_dk_ppno1xxvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxvii_qu,
                        option_grup_dk_ppno1xxvii, R.id.option_n_dk_ppno1xxvii);
                break;
            case R.id.option_y_dk_ttno1xxvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxvii_qu,
                        option_grup_dk_ttno1xxvii, R.id.option_y_dk_ttno1xxvii);
                break;
            case R.id.option_n_dk_ttno1xxvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxvii_qu,
                        option_grup_dk_ttno1xxvii, R.id.option_n_dk_ttno1xxvii);
                break;
            case R.id.option_y_dk_tt1no1xxvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxvii_qu,
                        option_grup_dk_tt1no1xxvii, R.id.option_y_dk_tt1no1xxvii);
                break;
            case R.id.option_n_dk_tt1no1xxvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxvii_qu,
                        option_grup_dk_tt1no1xxvii, R.id.option_n_dk_tt1no1xxvii);
                break;
            case R.id.option_y_dk_tt2no1xxvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxvii_qu,
                        option_grup_dk_tt2no1xxvii, R.id.option_y_dk_tt2no1xxvii);
                break;
            case R.id.option_n_dk_tt2no1xxvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxvii_qu,
                        option_grup_dk_tt2no1xxvii, R.id.option_n_dk_tt2no1xxvii);
                break;
            case R.id.option_y_dk_tt3no1xxvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxvii_qu,
                        option_grup_dk_tt3no1xxvii, R.id.option_y_dk_tt3no1xxvii);
                break;
            case R.id.option_n_dk_tt3no1xxvii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxvii_qu,
                        option_grup_dk_tt3no1xxvii, R.id.option_n_dk_tt3no1xxvii);
                break;
            case R.id.option_y_dk_ppno1xxviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxviii_qu,
                        option_grup_dk_ppno1xxviii, R.id.option_y_dk_ppno1xxviii);
                break;
            case R.id.option_n_dk_ppno1xxviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxviii_qu,
                        option_grup_dk_ppno1xxviii, R.id.option_n_dk_ppno1xxviii);
                break;
            case R.id.option_y_dk_ttno1xxviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxviii_qu,
                        option_grup_dk_ttno1xxviii, R.id.option_y_dk_ttno1xxviii);
                break;
            case R.id.option_n_dk_ttno1xxviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxviii_qu,
                        option_grup_dk_ttno1xxviii, R.id.option_n_dk_ttno1xxviii);
                break;
            case R.id.option_y_dk_tt1no1xxviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxviii_qu,
                        option_grup_dk_tt1no1xxviii, R.id.option_y_dk_tt1no1xxviii);
                break;
            case R.id.option_n_dk_tt1no1xxviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxviii_qu,
                        option_grup_dk_tt1no1xxviii, R.id.option_n_dk_tt1no1xxviii);
                break;
            case R.id.option_y_dk_tt2no1xxviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxviii_qu,
                        option_grup_dk_tt2no1xxviii, R.id.option_y_dk_tt2no1xxviii);
                break;
            case R.id.option_n_dk_tt2no1xxviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxviii_qu,
                        option_grup_dk_tt2no1xxviii, R.id.option_n_dk_tt2no1xxviii);
                break;
            case R.id.option_y_dk_tt3no1xxviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxviii_qu,
                        option_grup_dk_tt3no1xxviii, R.id.option_y_dk_tt3no1xxviii);
                break;
            case R.id.option_n_dk_tt3no1xxviii:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxviii_qu,
                        option_grup_dk_tt3no1xxviii, R.id.option_n_dk_tt3no1xxviii);
                break;
            case R.id.option_y_dk_ppno1xxix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxix_qu,
                        option_grup_dk_ppno1xxix, R.id.option_y_dk_ppno1xxix);
                break;
            case R.id.option_n_dk_ppno1xxix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxix_qu,
                        option_grup_dk_ppno1xxix, R.id.option_n_dk_ppno1xxix);
                break;
            case R.id.option_y_dk_ttno1xxix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxix_qu,
                        option_grup_dk_ttno1xxix, R.id.option_y_dk_ttno1xxix);
                break;
            case R.id.option_n_dk_ttno1xxix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxix_qu,
                        option_grup_dk_ttno1xxix, R.id.option_n_dk_ttno1xxix);
                break;
            case R.id.option_y_dk_tt1no1xxix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxix_qu,
                        option_grup_dk_tt1no1xxix, R.id.option_y_dk_tt1no1xxix);
                break;
            case R.id.option_n_dk_tt1no1xxix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxix_qu,
                        option_grup_dk_tt1no1xxix, R.id.option_n_dk_tt1no1xxix);
                break;
            case R.id.option_y_dk_tt2no1xxix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxix_qu,
                        option_grup_dk_tt2no1xxix, R.id.option_y_dk_tt2no1xxix);
                break;
            case R.id.option_n_dk_tt2no1xxix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxix_qu,
                        option_grup_dk_tt2no1xxix, R.id.option_n_dk_tt2no1xxix);
                break;
            case R.id.option_y_dk_tt3no1xxix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxix_qu,
                        option_grup_dk_tt3no1xxix, R.id.option_y_dk_tt3no1xxix);
                break;
            case R.id.option_n_dk_tt3no1xxix:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxix_qu,
                        option_grup_dk_tt3no1xxix, R.id.option_n_dk_tt3no1xxix);
                break;
            case R.id.option_y_dk_ppno1xxx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxx_qu,
                        option_grup_dk_ppno1xxx, R.id.option_y_dk_ppno1xxx);
                break;
            case R.id.option_n_dk_ppno1xxx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxx_qu,
                        option_grup_dk_ppno1xxx, R.id.option_n_dk_ppno1xxx);
                break;
            case R.id.option_y_dk_ttno1xxx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxx_qu,
                        option_grup_dk_ttno1xxx, R.id.option_y_dk_ttno1xxx);
                break;
            case R.id.option_n_dk_ttno1xxx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxx_qu,
                        option_grup_dk_ttno1xxx, R.id.option_n_dk_ttno1xxx);
                break;
            case R.id.option_y_dk_tt1no1xxx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxx_qu,
                        option_grup_dk_tt1no1xxx, R.id.option_y_dk_tt1no1xxx);
                break;
            case R.id.option_n_dk_tt1no1xxx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxx_qu,
                        option_grup_dk_tt1no1xxx, R.id.option_n_dk_tt1no1xxx);
                break;
            case R.id.option_y_dk_tt2no1xxx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxx_qu,
                        option_grup_dk_tt2no1xxx, R.id.option_y_dk_tt2no1xxx);
                break;
            case R.id.option_n_dk_tt2no1xxx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxx_qu,
                        option_grup_dk_tt2no1xxx, R.id.option_n_dk_tt2no1xxx);
                break;
            case R.id.option_y_dk_tt3no1xxx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxx_qu,
                        option_grup_dk_tt3no1xxx, R.id.option_y_dk_tt3no1xxx);
                break;
            case R.id.option_n_dk_tt3no1xxx:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxx_qu,
                        option_grup_dk_tt3no1xxx, R.id.option_n_dk_tt3no1xxx);
                break;
            case R.id.option_y_dk_ppno1xxxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxxi_qu,
                        option_grup_dk_ppno1xxxi, R.id.option_y_dk_ppno1xxxi);
                break;
            case R.id.option_n_dk_ppno1xxxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ppxxxi_qu,
                        option_grup_dk_ppno1xxxi, R.id.option_n_dk_ppno1xxxi);
                break;
            case R.id.option_y_dk_ttno1xxxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxxi_qu,
                        option_grup_dk_ttno1xxxi, R.id.option_y_dk_ttno1xxxi);
                break;
            case R.id.option_n_dk_ttno1xxxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_ttxxxi_qu,
                        option_grup_dk_ttno1xxxi, R.id.option_n_dk_ttno1xxxi);
                break;
            case R.id.option_y_dk_tt1no1xxxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxxi_qu,
                        option_grup_dk_tt1no1xxxi, R.id.option_y_dk_tt1no1xxxi);
                break;
            case R.id.option_n_dk_tt1no1xxxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_xxxi_qu,
                        option_grup_dk_tt1no1xxxi, R.id.option_n_dk_tt1no1xxxi);
                break;
            case R.id.option_y_dk_tt2no1xxxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxxi_qu,
                        option_grup_dk_tt2no1xxxi, R.id.option_y_dk_tt2no1xxxi);
                break;
            case R.id.option_n_dk_tt2no1xxxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_xxxi_qu,
                        option_grup_dk_tt2no1xxxi, R.id.option_n_dk_tt2no1xxxi);
                break;
            case R.id.option_y_dk_tt3no1xxxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxxi_qu, option_grup_dk_tt3no1xxxi, R.id.option_y_dk_tt3no1xxxi);
                break;
            case R.id.option_n_dk_tt3no1xxxi:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_xxxi_qu, option_grup_dk_tt3no1xxxi, R.id.option_n_dk_tt3no1xxxi);
                break;
            case R.id.option_y_dk_ppno2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp_qu, option_grup_dk_ppno2, R.id.option_y_dk_ppno2);
                break;
            case R.id.option_n_dk_ppno2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp_qu, option_grup_dk_ppno2, R.id.option_n_dk_ppno2);
                break;
            case R.id.option_y_dk_ttno2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt_qu, option_grup_dk_ttno2, R.id.option_y_dk_ttno2);
                break;
            case R.id.option_n_dk_ttno2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt_qu, option_grup_dk_ttno2, R.id.option_n_dk_ttno2);
                break;
            case R.id.option_y_dk_tt1no2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_qu, option_grup_dk_tt1no2, R.id.option_y_dk_tt1no2);
                break;
            case R.id.option_n_dk_tt1no2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_qu, option_grup_dk_tt1no2, R.id.option_n_dk_tt1no2);
                break;
            case R.id.option_y_dk_tt2no2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_qu, option_grup_dk_tt2no2, R.id.option_y_dk_tt2no2);
                break;
            case R.id.option_n_dk_tt2no2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_qu, option_grup_dk_tt2no2, R.id.option_n_dk_tt2no2);
                break;
            case R.id.option_y_dk_tt3no2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_qu, option_grup_dk_tt3no2, R.id.option_y_dk_tt3no2);
                break;
            case R.id.option_n_dk_tt3no2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_qu, option_grup_dk_tt3no2, R.id.option_n_dk_tt3no2);
                break;
            case R.id.option_y_dk_ppno3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp3a_qu, option_grup_dk_ppno3a, R.id.option_y_dk_ppno3a);
                break;
            case R.id.option_n_dk_ppno3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp3a_qu, option_grup_dk_ppno3a, R.id.option_n_dk_ppno3a);
                break;
            case R.id.option_y_dk_ttno3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3a_qu, option_grup_dk_ttno3a, R.id.option_y_dk_ttno3a);
                break;
            case R.id.option_n_dk_ttno3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3a_qu, option_grup_dk_ttno3a, R.id.option_n_dk_ttno3a);
                break;
            case R.id.option_y_dk_tt1no3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_3a_qu, option_grup_dk_tt1no3a, R.id.option_y_dk_tt1no3a);
                break;
            case R.id.option_n_dk_tt1no3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_3a_qu, option_grup_dk_tt1no3a, R.id.option_n_dk_tt1no3a);
                break;
            case R.id.option_y_dk_tt2no3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_3a_qu, option_grup_dk_tt2no3a, R.id.option_y_dk_tt2no3a);
                break;
            case R.id.option_n_dk_tt2no3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_3a_qu, option_grup_dk_tt2no3a, R.id.option_n_dk_tt2no3a);
                break;
            case R.id.option_y_dk_tt3no3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_3a_qu, option_grup_dk_tt3no3a, R.id.option_y_dk_tt3no3a);
                break;
            case R.id.option_n_dk_tt3no3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_3a_qu, option_grup_dk_tt3no3a, R.id.option_n_dk_tt3no3a);
                break;
            case R.id.option_y_dk_ppno3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp3b_qu, option_grup_dk_ppno3b, R.id.option_y_dk_ppno3b);
                break;
            case R.id.option_n_dk_ppno3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp3b_qu, option_grup_dk_ppno3b, R.id.option_n_dk_ppno3b);
                break;
            case R.id.option_y_dk_ttno3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3b_qu, option_grup_dk_ttno3b, R.id.option_y_dk_ttno3b);
                break;
            case R.id.option_n_dk_ttno3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3b_qu, option_grup_dk_ttno3b, R.id.option_n_dk_ttno3b);
                break;
            case R.id.option_y_dk_tt1no3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_3b_qu, option_grup_dk_tt1no3b, R.id.option_y_dk_tt1no3b);
                break;
            case R.id.option_n_dk_tt1no3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_3b_qu, option_grup_dk_tt1no3b, R.id.option_n_dk_tt1no3b);
                break;
            case R.id.option_y_dk_tt2no3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_3b_qu, option_grup_dk_tt2no3b, R.id.option_y_dk_tt2no3b);
                break;
            case R.id.option_n_dk_tt2no3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_3b_qu, option_grup_dk_tt2no3b, R.id.option_n_dk_tt2no3b);
                break;
            case R.id.option_y_dk_tt3no3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_3b_qu, option_grup_dk_tt3no3b, R.id.option_y_dk_tt3no3b);
                break;
            case R.id.option_n_dk_tt3no3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_3b_qu, option_grup_dk_tt3no3b, R.id.option_n_dk_tt3no3b);
                break;
            case R.id.option_y_dk_ppno3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp3c_qu, option_grup_dk_ppno3c, R.id.option_y_dk_ppno3c);
                break;
            case R.id.option_n_dk_ppno3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp3c_qu, option_grup_dk_ppno3c, R.id.option_n_dk_ppno3c);
                break;
            case R.id.option_y_dk_ttno3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3c_qu, option_grup_dk_ttno3c, R.id.option_y_dk_ttno3c);
                break;
            case R.id.option_n_dk_ttno3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3c_qu, option_grup_dk_ttno3c, R.id.option_n_dk_ttno3c);
                break;
            case R.id.option_y_dk_tt1no3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_3c_qu, option_grup_dk_tt1no3c, R.id.option_y_dk_tt1no3c);
                break;
            case R.id.option_n_dk_tt1no3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_3c_qu, option_grup_dk_tt1no3c, R.id.option_n_dk_tt1no3c);
                break;
            case R.id.option_y_dk_tt2no3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_3c_qu, option_grup_dk_tt2no3c, R.id.option_y_dk_tt2no3c);
                break;
            case R.id.option_n_dk_tt2no3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_3c_qu, option_grup_dk_tt2no3c, R.id.option_n_dk_tt2no3c);
                break;
            case R.id.option_y_dk_tt3no3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_3c_qu, option_grup_dk_tt3no3c, R.id.option_y_dk_tt3no3c);
                break;
            case R.id.option_n_dk_tt3no3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_3c_qu, option_grup_dk_tt3no3c, R.id.option_n_dk_tt3no3c);
                break;
            case R.id.option_y_dk_ppno3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp3d_qu, option_grup_dk_ppno3d, R.id.option_y_dk_ppno3d);
                break;
            case R.id.option_n_dk_ppno3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp3d_qu, option_grup_dk_ppno3d, R.id.option_n_dk_ppno3d);
                break;
            case R.id.option_y_dk_ttno3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3d_qu, option_grup_dk_ttno3d, R.id.option_y_dk_ttno3d);
                break;
            case R.id.option_n_dk_ttno3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3d_qu, option_grup_dk_ttno3d, R.id.option_n_dk_ttno3d);
                break;
            case R.id.option_y_dk_tt1no3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_3d_qu, option_grup_dk_tt1no3d, R.id.option_y_dk_tt1no3d);
                break;
            case R.id.option_n_dk_tt1no3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_3d_qu, option_grup_dk_tt1no3d, R.id.option_n_dk_tt1no3d);
                break;
            case R.id.option_y_dk_tt2no3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_3d_qu, option_grup_dk_tt2no3d, R.id.option_y_dk_tt2no3d);
                break;
            case R.id.option_n_dk_tt2no3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_3d_qu, option_grup_dk_tt2no3d, R.id.option_n_dk_tt2no3d);
                break;
            case R.id.option_y_dk_tt3no3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_3d_qu, option_grup_dk_tt3no3d, R.id.option_y_dk_tt3no3d);
                break;
            case R.id.option_n_dk_tt3no3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_3d_qu, option_grup_dk_tt3no3d, R.id.option_n_dk_tt3no3d);
                break;
            case R.id.option_y_dk_ppno4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp4_qu, option_grup_dk_ppno4, R.id.option_y_dk_ppno4);
                break;
            case R.id.option_n_dk_ppno4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp4_qu, option_grup_dk_ppno4, R.id.option_n_dk_ppno4);
                break;
            case R.id.option_y_dk_ttno4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt4_qu, option_grup_dk_ttno4, R.id.option_y_dk_ttno4);
                break;
            case R.id.option_n_dk_ttno4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt4_qu, option_grup_dk_ttno4, R.id.option_n_dk_ttno4);
                break;
            case R.id.option_y_dk_tt1no4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_4_qu, option_grup_dk_tt1no4, R.id.option_y_dk_tt1no4);
                break;
            case R.id.option_n_dk_tt1no4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_4_qu, option_grup_dk_tt1no4, R.id.option_n_dk_tt1no4);
                break;
            case R.id.option_y_dk_tt2no4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_4_qu, option_grup_dk_tt2no4, R.id.option_y_dk_tt2no4);
                break;
            case R.id.option_n_dk_tt2no4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_4_qu, option_grup_dk_tt2no4, R.id.option_n_dk_tt2no4);
                break;
            case R.id.option_y_dk_tt3no4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_4_qu, option_grup_dk_tt3no4, R.id.option_y_dk_tt3no4);
                break;
            case R.id.option_n_dk_tt3no4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_4_qu, option_grup_dk_tt3no4, R.id.option_n_dk_tt3no4);
                break;
            case R.id.option_y_dk_ppno5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp5_qu, option_grup_dk_ppno5, R.id.option_y_dk_ppno5);
                break;
            case R.id.option_n_dk_ppno5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp5_qu, option_grup_dk_ppno5, R.id.option_n_dk_ppno5);
                break;
            case R.id.option_y_dk_ttno5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt5_qu, option_grup_dk_ttno5, R.id.option_y_dk_ttno5);
                break;
            case R.id.option_n_dk_ttno5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt5_qu, option_grup_dk_ttno5, R.id.option_n_dk_ttno5);
                break;
            case R.id.option_y_dk_tt1no5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_5_qu, option_grup_dk_tt1no5, R.id.option_y_dk_tt1no5);
                break;
            case R.id.option_n_dk_tt1no5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_5_qu, option_grup_dk_tt1no5, R.id.option_n_dk_tt1no5);
                break;
            case R.id.option_y_dk_tt2no5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_5_qu, option_grup_dk_tt2no5, R.id.option_y_dk_tt2no5);
                break;
            case R.id.option_n_dk_tt2no5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_5_qu, option_grup_dk_tt2no5, R.id.option_n_dk_tt2no5);
                break;
            case R.id.option_y_dk_tt3no5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_5_qu, option_grup_dk_tt3no5, R.id.option_y_dk_tt3no5);
                break;
            case R.id.option_n_dk_tt3no5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_5_qu, option_grup_dk_tt3no5, R.id.option_n_dk_tt3no5);
                break;
            case R.id.option_y_dk_ppno6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp6_qu, option_grup_dk_ppno6, R.id.option_y_dk_ppno6);
                break;
            case R.id.option_n_dk_ppno6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp6_qu, option_grup_dk_ppno6, R.id.option_n_dk_ppno6);
                break;
            case R.id.option_y_dk_ttno6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt6_qu, option_grup_dk_ttno6, R.id.option_y_dk_ttno6);
                break;
            case R.id.option_n_dk_ttno6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt6_qu, option_grup_dk_ttno6, R.id.option_n_dk_ttno6);
                break;
            case R.id.option_y_dk_tt1no6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_6_qu, option_grup_dk_tt1no6, R.id.option_y_dk_tt1no6);
                break;
            case R.id.option_n_dk_tt1no6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_6_qu, option_grup_dk_tt1no6, R.id.option_n_dk_tt1no6);
                break;
            case R.id.option_y_dk_tt2no6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_6_qu, option_grup_dk_tt2no6, R.id.option_y_dk_tt2no6);
                break;
            case R.id.option_n_dk_tt2no6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_6_qu, option_grup_dk_tt2no6, R.id.option_n_dk_tt2no6);
                break;
            case R.id.option_y_dk_tt3no6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_6_qu, option_grup_dk_tt3no6, R.id.option_y_dk_tt3no6);
                break;
            case R.id.option_n_dk_tt3no6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_6_qu, option_grup_dk_tt3no6, R.id.option_n_dk_tt3no6);
                break;
            case R.id.option_y_dk_ppno7:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp7_qu, option_grup_dk_ppno7, R.id.option_y_dk_ppno7);
                break;
            case R.id.option_n_dk_ppno7:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp7_qu, option_grup_dk_ppno7, R.id.option_n_dk_ppno7);
                break;
            case R.id.option_y_dk_ttno7:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt7_qu, option_grup_dk_ttno7, R.id.option_y_dk_ttno7);
                break;
            case R.id.option_n_dk_ttno7:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt7_qu, option_grup_dk_ttno7, R.id.option_n_dk_ttno7);
                break;
            case R.id.option_y_dk_tt1no7:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_7_qu, option_grup_dk_tt1no7, R.id.option_y_dk_tt1no7);
                break;
            case R.id.option_n_dk_tt1no7:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_7_qu, option_grup_dk_tt1no7, R.id.option_n_dk_tt1no7);
                break;
            case R.id.option_y_dk_tt2no7:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_7_qu, option_grup_dk_tt2no7, R.id.option_y_dk_tt2no7);
                break;
            case R.id.option_n_dk_tt2no7:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_7_qu, option_grup_dk_tt2no7, R.id.option_n_dk_tt2no7);
                break;
            case R.id.option_y_dk_tt3no7:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_7_qu, option_grup_dk_tt3no7, R.id.option_y_dk_tt3no7);
                break;
            case R.id.option_n_dk_tt3no7:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_7_qu, option_grup_dk_tt3no7, R.id.option_n_dk_tt3no7);
                break;
            case R.id.option_y_dk_ppno8a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp8a_qu, option_grup_dk_ppno8a, R.id.option_y_dk_ppno8a);
                break;
            case R.id.option_n_dk_ppno8a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp8a_qu, option_grup_dk_ppno8a, R.id.option_n_dk_ppno8a);
                break;
            case R.id.option_y_dk_ttno8a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt8a_qu, option_grup_dk_ttno8a, R.id.option_y_dk_ttno8a);
                break;
            case R.id.option_n_dk_ttno8a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt8a_qu, option_grup_dk_ttno8a, R.id.option_n_dk_ttno8a);
                break;
            case R.id.option_y_dk_tt1no8a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_8a_qu, option_grup_dk_tt1no8a, R.id.option_y_dk_tt1no8a);
                break;
            case R.id.option_n_dk_tt1no8a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_8a_qu, option_grup_dk_tt1no8a, R.id.option_n_dk_tt1no8a);
                break;
            case R.id.option_y_dk_tt2no8a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_8a_qu, option_grup_dk_tt2no8a, R.id.option_y_dk_tt2no8a);
                break;
            case R.id.option_n_dk_tt2no8a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_8a_qu, option_grup_dk_tt2no8a, R.id.option_n_dk_tt2no8a);
                break;
            case R.id.option_y_dk_tt3no8a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_8a_qu, option_grup_dk_tt3no8a, R.id.option_y_dk_tt3no8a);
                break;
            case R.id.option_n_dk_tt3no8a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_8a_qu, option_grup_dk_tt3no8a, R.id.option_n_dk_tt3no8a);
                break;
            case R.id.option_y_dk_ppno8b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp8b_qu, option_grup_dk_ppno8b, R.id.option_y_dk_ppno8b);
                break;
            case R.id.option_n_dk_ppno8b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp8b_qu, option_grup_dk_ppno8b, R.id.option_n_dk_ppno8b);
                break;
            case R.id.option_y_dk_ttno8b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt8b_qu, option_grup_dk_ttno8b, R.id.option_y_dk_ttno8b);
                break;
            case R.id.option_n_dk_ttno8b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt8b_qu, option_grup_dk_ttno8b, R.id.option_n_dk_ttno8b);
                break;
            case R.id.option_y_dk_tt1no8b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_8b_qu, option_grup_dk_tt1no8b, R.id.option_y_dk_tt1no8b);
                break;
            case R.id.option_n_dk_tt1no8b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_8b_qu, option_grup_dk_tt1no8b, R.id.option_n_dk_tt1no8b);
                break;
            case R.id.option_y_dk_tt2no8b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_8b_qu, option_grup_dk_tt2no8b, R.id.option_y_dk_tt2no8b);
                break;
            case R.id.option_n_dk_tt2no8b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_8b_qu, option_grup_dk_tt2no8b, R.id.option_n_dk_tt2no8b);
                break;
            case R.id.option_y_dk_tt3no8b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_8b_qu, option_grup_dk_tt3no8b, R.id.option_y_dk_tt3no8b);
                break;
            case R.id.option_n_dk_tt3no8b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_8b_qu, option_grup_dk_tt3no8b, R.id.option_n_dk_tt3no8b);
                break;
            case R.id.option_y_dk_ppno8c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp8c_qu, option_grup_dk_ppno8c, R.id.option_y_dk_ppno8c);
                break;
            case R.id.option_n_dk_ppno8c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp8c_qu, option_grup_dk_ppno8c, R.id.option_n_dk_ppno8c);
                break;
            case R.id.option_y_dk_ttno8c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt8c_qu, option_grup_dk_ttno8c, R.id.option_y_dk_ttno8c);
                break;
            case R.id.option_n_dk_ttno8c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt8c_qu, option_grup_dk_ttno8c, R.id.option_n_dk_ttno8c);
                break;
            case R.id.option_y_dk_tt1no8c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_8c_qu, option_grup_dk_tt1no8c, R.id.option_y_dk_tt1no8c);
                break;
            case R.id.option_n_dk_tt1no8c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_8c_qu, option_grup_dk_tt1no8c, R.id.option_n_dk_tt1no8c);
                break;
            case R.id.option_y_dk_tt2no8c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_8c_qu, option_grup_dk_tt2no8c, R.id.option_y_dk_tt2no8c);
                break;
            case R.id.option_n_dk_tt2no8c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_8c_qu, option_grup_dk_tt2no8c, R.id.option_n_dk_tt2no8c);
                break;
            case R.id.option_y_dk_tt3no8c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_8c_qu, option_grup_dk_tt3no8c, R.id.option_y_dk_tt3no8c);
                break;
            case R.id.option_n_dk_tt3no8c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_8c_qu, option_grup_dk_tt3no8c, R.id.option_n_dk_tt3no8c);
                break;
            case R.id.option_y_dk_ppno9:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp9_qu, option_grup_dk_ppno9, R.id.option_y_dk_ppno9);
                break;
            case R.id.option_n_dk_ppno9:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_pp9_qu, option_grup_dk_ppno9, R.id.option_n_dk_ppno9);
                break;
            case R.id.option_y_dk_ttno9:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt9_qu, option_grup_dk_ttno9, R.id.option_y_dk_ttno9);
                break;
            case R.id.option_n_dk_ttno9:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt9_qu, option_grup_dk_ttno9, R.id.option_n_dk_ttno9);
                break;
            case R.id.option_y_dk_tt1no9:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_9_qu, option_grup_dk_tt1no9, R.id.option_y_dk_tt1no9);
                break;
            case R.id.option_n_dk_tt1no9:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt1_9_qu, option_grup_dk_tt1no9, R.id.option_n_dk_tt1no9);
                break;
            case R.id.option_y_dk_tt2no9:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_9_qu, option_grup_dk_tt2no9, R.id.option_y_dk_tt2no9);
                break;
            case R.id.option_n_dk_tt2no9:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt2_9_qu, option_grup_dk_tt2no9, R.id.option_n_dk_tt2no9);
                break;
            case R.id.option_y_dk_tt3no9:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_9_qu, option_grup_dk_tt3no9, R.id.option_y_dk_tt3no9);
                break;
            case R.id.option_n_dk_tt3no9:
                MethodSupport.focuslayoutRadio(hasFocus, cl_form_calon_tt3_9_qu, option_grup_dk_tt3no9, R.id.option_n_dk_tt3no9);
                break;

            //tertanggung
            case R.id.essay_tt_no1:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire1_isi_qu);
                break;
            case R.id.essay_tt_no2:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire2_isi_qu);
                break;
            case R.id.essay_tt_no4:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire4_isi_qu);
                break;
            case R.id.essay_tt_no6:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire6_isi_qu);
                break;
            case R.id.essay_tt_no7:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire7_isi_qu);
                break;
            case R.id.essay_tt_1_no8a:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire8a_isi_qu);
                break;
            case R.id.essay_tt_2_no8a:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire8a_isi_qu);
                break;
            case R.id.essay_tt_rkk_no10:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire10_isi_qu);
                break;
            case R.id.essay_tt_cm_no11a:
                MethodSupport.focuslayouttext(hasFocus, cl_tinggi_badan_qu);
                break;
            case R.id.essay_tt_kg_no11a:
                MethodSupport.focuslayouttext(hasFocus, cl_berat_badan_qu);
                break;
            case R.id.essay_tt_no11b:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire11b_isi_qu);
                break;
            case R.id.essay_tt_cm_no12:
                MethodSupport.focuslayouttext(hasFocus, cl_tinggi_badan12_qu);
                break;
            case R.id.essay_tt_kg_no12:
                MethodSupport.focuslayouttext(hasFocus, cl_berat_badan12_qu);
                break;
            //pemegangpolis
            case R.id.essay_pp_1_no13:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire13_isi_qu);
                break;
            case R.id.essay_pp_2_no13:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire13a_isi_qu);
                break;
            case R.id.essay_pp_1_no14:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire14_isi_qu);
                break;
            case R.id.essay_pp_2_no14:
                MethodSupport.focuslayouttext(hasFocus, cl_questionnaire14a_isi_qu);
                break;
            case R.id.essay_pp_cm_no15:
                MethodSupport.focuslayouttext(hasFocus, cl_tinggi_badan2_qu);
                break;
            case R.id.essay_pp_kg_no15:
                MethodSupport.focuslayouttext(hasFocus, cl_berat_badan2_qu);
                break;
            //datakesehatan
            case R.id.essay_dk_no2:
                MethodSupport.focuslayouttext(hasFocus, cl_form_data_kesehatan2_ket_qu);
                break;
            case R.id.essay_dk_no4:
                MethodSupport.focuslayouttext(hasFocus, cl_form_data_kesehatan4_ket_qu);
                break;
            case R.id.essay_dk_no7a:
                MethodSupport.focuslayouttext(hasFocus, cl_form_data_kesehatan7a_jwb_qu);
                break;
            case R.id.essay_dk_no7b:
                MethodSupport.focuslayouttext(hasFocus, cl_form_data_kesehatan7b_jwb_qu);
                break;
            case R.id.essay_dk_no7c:
                MethodSupport.focuslayouttext(hasFocus, cl_form_data_kesehatan7c_jwb_qu);
                break;
            case R.id.essay_dk_no7d:
                MethodSupport.focuslayouttext(hasFocus, cl_form_data_kesehatan7d_jwb_qu);
                break;
            case R.id.essay_dk_no8a:
                MethodSupport.focuslayouttext(hasFocus, cl_form_data_kesehatan8a_ket_qu);
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (group.getId()) {
            //tertanggung
            case R.id.option_grup_tt_no1:
                HashMap<String, Integer> map = MethodSupport.OnCheckQuisionerRadio(option_grup_tt_no1, int_y_id1, int_n_id1, essay_tt_no1, checkedId, iv_circle_questionnaire1_qu, getContext());
                int_y_id1 = map.get("int_y_id1");
                int_n_id1 = map.get("int_n_id1");
//                iv_circle_questionnaire1_qu.setColorFilter(R.color.green);
                break;
            case R.id.option_grup_tt_no2:
                map = MethodSupport.OnCheckQuisionerRadio(option_grup_tt_no2, int_y_id2, int_n_id2, essay_tt_no2, checkedId, iv_circle_questionnaire2_qu, getContext());
                int_y_id2 = map.get("int_y_id1");
                int_n_id2 = map.get("int_n_id1");
//                iv_circle_questionnaire2_qu.setColorFilter(R.color.green);
                break;
            case R.id.option_grup_tt_no3:
                map = MethodSupport.OnCheckQuisionerRadioTabel(option_grup_tt_no3, int_y_id3, int_n_id3, img_tambah_data_qu, checkedId, iv_circle_questionnaire3_qu, getContext());
                int_y_id3 = map.get("int_y_id1");
                int_n_id3 = map.get("int_n_id1");
                //iv_circle_questionnaire3_qu.setColorFilter(R.color.green);
                break;
            case R.id.option_grup_tt_no4:
                map = MethodSupport.OnCheckQuisionerRadio(option_grup_tt_no4, int_y_id4, int_n_id4, essay_tt_no4, checkedId, iv_circle_questionnaire4_qu, getContext());
                int_y_id4 = map.get("int_y_id1");
                int_n_id4 = map.get("int_n_id1");
//                iv_circle_questionnaire4_qu.setColorFilter(R.color.green);
                break;
            case R.id.option_grup_tt_no5:
                map = MethodSupport.OnCheckQuisionerRadioValue(option_grup_tt_no5, int_y_id5, int_n_id5, checkedId, iv_circle_questionnaire5_qu, getContext());
                int_y_id5 = map.get("int_y_id1");
                int_n_id5 = map.get("int_n_id1");
//                iv_circle_questionnaire5_qu.setColorFilter(R.color.green);
                break;
            case R.id.option_grup_tt_no6:
                map = MethodSupport.OnCheckQuisionerRadio(option_grup_tt_no6, int_y_id6, int_n_id6, essay_tt_no6, checkedId, iv_circle_questionnaire6_qu, getContext());
                int_y_id6 = map.get("int_y_id1");
                int_n_id6 = map.get("int_n_id1");
//                iv_circle_questionnaire6_qu.setColorFilter(R.color.green);
                break;
            case R.id.option_grup_tt_no7:
                map = MethodSupport.OnCheckQuisionerRadio(option_grup_tt_no7, int_y_id7, int_n_id7, essay_tt_no7, checkedId, iv_circle_questionnaire7_qu, getContext());
                int_y_id7 = map.get("int_y_id1");
                int_n_id7 = map.get("int_n_id1");
//                iv_circle_questionnaire7_qu.setColorFilter(R.color.green);
                break;
            case R.id.option_grup_tt_no8:
                map = MethodSupport.OnCheckQuisionerRadio2(option_grup_tt_no8, int_y_id8, int_n_id8, essay_tt_1_no8a, essay_tt_2_no8a, checkedId, iv_circle_questionnaire8_qu, getContext());
                int_y_id8 = map.get("int_y_id1");
                int_n_id8 = map.get("int_n_id1");
//                iv_circle_questionnaire8_qu.setColorFilter(R.color.green);
                break;
            case R.id.option_grup_tt_no8b:
                map = MethodSupport.OnCheckQuisionerRadioValue(option_grup_tt_no8b, int_y_id10, int_n_id10, checkedId, iv_circle_questionnaire8b_qu, getContext());
                int_y_id10 = map.get("int_y_id1");
                int_n_id10 = map.get("int_n_id1");
//                iv_circle_questionnaire8b_qu.setColorFilter(R.color.green);
                break;
            case R.id.option_grup_tt_no9:
                map = MethodSupport.OnCheckQuisionerRadioValue(option_grup_tt_no9, int_y_id11, int_n_id11, checkedId, iv_circle_questionnaire9_qu, getContext());
                int_y_id11 = map.get("int_y_id1");
                int_n_id11 = map.get("int_n_id1");
//                iv_circle_questionnaire9_qu.setColorFilter(R.color.green);
                break;
            case R.id.option_grup_tt_no10:
                map = MethodSupport.OnCheckQuisionerRadio(option_grup_tt_no10, int_y_id12, int_n_id12, essay_tt_rkk_no10, checkedId, iv_circle_questionnaire10_qu, getContext());
                int_y_id12 = map.get("int_y_id1");
                int_n_id12 = map.get("int_n_id1");
//                iv_circle_questionnaire10_qu.setColorFilter(R.color.green);
                break;
            case R.id.option_grup_tt_no11b:
                map = MethodSupport.OnCheckQuisionerRadio(option_grup_tt_no11b, int_y_id14, int_n_id14, essay_tt_no11b, checkedId, iv_circle_questionnaire11b_qu, getContext());
                int_y_id14 = map.get("int_y_id1");
                int_n_id14 = map.get("int_n_id1");
//                iv_circle_questionnaire11b_qu.setColorFilter(R.color.green);
                break;
            //pemegang polis
            case R.id.option_grup_pp_no13:
                map = MethodSupport.OnCheckQuisionerRadioText2(option_grup_pp_no13, int_y_id16, int_n_id16, essay_pp_1_no13,
                        essay_pp_2_no13, checkedId, iv_circle_questionnaire13_qu, iv_circle_questionnaire13a_qu, getContext());
                int_y_id16 = map.get("int_y_id1");
                int_n_id16 = map.get("int_n_id1");

                if (essay_pp_1_no13.isEnabled()) {
                    iv_circle_questionnaire13a_qu.setVisibility(View.INVISIBLE);
                }

                if (essay_pp_2_no13.isEnabled()) {
                    iv_circle_questionnaire13a_qu.setVisibility(View.VISIBLE);
                }
//                iv_circle_questionnaire13_qu.setColorFilter(R.color.green);
                break;
            case R.id.option_grup_pp_no14:
                map = MethodSupport.OnCheckQuisionerRadioText2(option_grup_pp_no14, int_y_id17, int_n_id17, essay_pp_1_no14,
                        essay_pp_2_no14, checkedId, iv_circle_questionnaire14_qu, iv_circle_questionnaire14a_qu, getContext());
                int_y_id17 = map.get("int_y_id1");
                int_n_id17 = map.get("int_n_id1");

                if (essay_pp_1_no14.isEnabled()) {
                    iv_circle_questionnaire14a_qu.setVisibility(View.INVISIBLE);
                }

                if (essay_pp_2_no14.isEnabled()) {
                    iv_circle_questionnaire14a_qu.setVisibility(View.VISIBLE);
                }
//                iv_circle_questionnaire14_qu.setColorFilter(R.color.green);
                break;

            //Data Kesehatan
            case R.id.option_grup_dk_ppno1l:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1l, option_grup_dk_ttno1l, option_grup_dk_tt1no1l, option_grup_dk_tt2no1l, option_grup_dk_tt3no1l,
                        int_y_id106_1, int_n_id106_1,
                        checkedId, iv_circle_data_kesehatani_qu, getContext());
                int_y_id106_1 = map.get("int_y_id1");
                int_n_id106_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1l:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ttno1l, option_grup_dk_ppno1l, option_grup_dk_tt1no1l, option_grup_dk_tt2no1l, option_grup_dk_tt3no1l,
                        int_y_id106_2, int_n_id106_2,
                        checkedId, iv_circle_data_kesehatani_qu, getContext());
                int_y_id106_2 = map.get("int_y_id1");
                int_n_id106_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1l:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_tt1no1l, option_grup_dk_ppno1l, option_grup_dk_ttno1l, option_grup_dk_tt2no1l, option_grup_dk_tt3no1l,
                        int_y_id106_3, int_n_id106_3,
                        checkedId, iv_circle_data_kesehatani_qu, getContext());
                int_y_id106_3 = map.get("int_y_id1");
                int_n_id106_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1l:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_tt2no1l, option_grup_dk_ppno1l, option_grup_dk_ttno1l, option_grup_dk_tt1no1l, option_grup_dk_tt3no1l,
                        int_y_id106_4, int_n_id106_4,
                        checkedId, iv_circle_data_kesehatani_qu, getContext());
                int_y_id106_4 = map.get("int_y_id1");
                int_n_id106_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1l:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_tt3no1l, option_grup_dk_ppno1l, option_grup_dk_ttno1l, option_grup_dk_tt1no1l, option_grup_dk_tt2no1l,
                        int_y_id106_5, int_n_id106_5,
                        checkedId, iv_circle_data_kesehatani_qu, getContext());
                int_y_id106_5 = map.get("int_y_id1");
                int_n_id106_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1ii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1ii, option_grup_dk_ttno1ii, option_grup_dk_tt1no1ii, option_grup_dk_tt2no1ii, option_grup_dk_tt3no1ii,
                        int_y_id107_1, int_n_id107_1,
                        checkedId, iv_circle_data_kesehatanii_qu, getContext());
                int_y_id107_1 = map.get("int_y_id1");
                int_n_id107_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1ii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1ii, option_grup_dk_ttno1ii, option_grup_dk_tt1no1ii, option_grup_dk_tt2no1ii, option_grup_dk_tt3no1ii,
                        int_y_id107_2, int_n_id107_2,
                        checkedId, iv_circle_data_kesehatanii_qu, getContext());
                int_y_id107_2 = map.get("int_y_id1");
                int_n_id107_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1ii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1ii, option_grup_dk_ttno1ii, option_grup_dk_tt1no1ii, option_grup_dk_tt2no1ii, option_grup_dk_tt3no1ii,
                        int_y_id107_3, int_n_id107_3,
                        checkedId, iv_circle_data_kesehatanii_qu, getContext());
                int_y_id107_3 = map.get("int_y_id1");
                int_n_id107_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1ii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1ii, option_grup_dk_ttno1ii, option_grup_dk_tt1no1ii, option_grup_dk_tt2no1ii, option_grup_dk_tt3no1ii,
                        int_y_id107_4, int_n_id107_4,
                        checkedId, iv_circle_data_kesehatanii_qu, getContext());
                int_y_id107_4 = map.get("int_y_id1");
                int_n_id107_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1ii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1ii, option_grup_dk_ttno1ii, option_grup_dk_tt1no1ii, option_grup_dk_tt2no1ii, option_grup_dk_tt3no1ii,
                        int_y_id107_5, int_n_id107_5,
                        checkedId, iv_circle_data_kesehatanii_qu, getContext());
                int_y_id107_5 = map.get("int_y_id1");
                int_n_id107_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1iii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1iii, option_grup_dk_ttno1iii, option_grup_dk_tt1no1iii, option_grup_dk_tt2no1iii, option_grup_dk_tt3no1iii,
                        int_y_id108_1, int_n_id108_1,
                        checkedId, iv_circle_data_kesehataniii_qu, getContext());
                int_y_id108_1 = map.get("int_y_id1");
                int_n_id108_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1iii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1iii, option_grup_dk_ttno1iii, option_grup_dk_tt1no1iii, option_grup_dk_tt2no1iii, option_grup_dk_tt3no1iii,
                        int_y_id108_2, int_n_id108_2,
                        checkedId, iv_circle_data_kesehataniii_qu, getContext());
                int_y_id108_2 = map.get("int_y_id1");
                int_n_id108_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1iii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1iii, option_grup_dk_ttno1iii, option_grup_dk_tt1no1iii, option_grup_dk_tt2no1iii, option_grup_dk_tt3no1iii,
                        int_y_id108_3, int_n_id108_3,
                        checkedId, iv_circle_data_kesehataniii_qu, getContext());
                int_y_id108_3 = map.get("int_y_id1");
                int_n_id108_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1iii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1iii, option_grup_dk_ttno1iii, option_grup_dk_tt1no1iii, option_grup_dk_tt2no1iii, option_grup_dk_tt3no1iii,
                        int_y_id108_4, int_n_id108_4,
                        checkedId, iv_circle_data_kesehataniii_qu, getContext());
                int_y_id108_4 = map.get("int_y_id1");
                int_n_id108_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1iii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1iii, option_grup_dk_ttno1iii, option_grup_dk_tt1no1iii, option_grup_dk_tt2no1iii, option_grup_dk_tt3no1iii,
                        int_y_id108_5, int_n_id108_5,
                        checkedId, iv_circle_data_kesehataniii_qu, getContext());
                int_y_id108_5 = map.get("int_y_id1");
                int_n_id108_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1iv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1iv, option_grup_dk_ttno1iv, option_grup_dk_tt1no1iv, option_grup_dk_tt2no1iv, option_grup_dk_tt3no1iv,
                        int_y_id109_1, int_n_id109_1,
                        checkedId, iv_circle_data_kesehataniv_qu, getContext());
                int_y_id109_1 = map.get("int_y_id1");
                int_n_id109_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1iv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1iv, option_grup_dk_ttno1iv, option_grup_dk_tt1no1iv, option_grup_dk_tt2no1iv, option_grup_dk_tt3no1iv,
                        int_y_id109_2, int_n_id109_2,
                        checkedId, iv_circle_data_kesehataniv_qu, getContext());
                int_y_id109_2 = map.get("int_y_id1");
                int_n_id109_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1iv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1iv, option_grup_dk_ttno1iv, option_grup_dk_tt1no1iv, option_grup_dk_tt2no1iv, option_grup_dk_tt3no1iv,
                        int_y_id109_3, int_n_id109_3,
                        checkedId, iv_circle_data_kesehataniv_qu, getContext());
                int_y_id109_3 = map.get("int_y_id1");
                int_n_id109_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1iv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1iv, option_grup_dk_ttno1iv, option_grup_dk_tt1no1iv, option_grup_dk_tt2no1iv, option_grup_dk_tt3no1iv,
                        int_y_id109_4, int_n_id109_4,
                        checkedId, iv_circle_data_kesehataniv_qu, getContext());
                int_y_id109_4 = map.get("int_y_id1");
                int_n_id109_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1iv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1iv, option_grup_dk_ttno1iv, option_grup_dk_tt1no1iv, option_grup_dk_tt2no1iv, option_grup_dk_tt3no1iv,
                        int_y_id109_5, int_n_id109_5,
                        checkedId, iv_circle_data_kesehataniv_qu, getContext());
                int_y_id109_5 = map.get("int_y_id1");
                int_n_id109_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1v:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1v, option_grup_dk_ttno1v, option_grup_dk_tt1no1v, option_grup_dk_tt2no1v, option_grup_dk_tt3no1v,
                        int_y_id110_1, int_n_id110_1,
                        checkedId, iv_circle_data_kesehatanv_qu, getContext());
                int_y_id110_1 = map.get("int_y_id1");
                int_n_id110_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1v:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1v, option_grup_dk_ttno1v, option_grup_dk_tt1no1v, option_grup_dk_tt2no1v, option_grup_dk_tt3no1v,
                        int_y_id110_2, int_n_id110_2,
                        checkedId, iv_circle_data_kesehatanv_qu, getContext());
                int_y_id110_2 = map.get("int_y_id1");
                int_n_id110_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1v:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1v, option_grup_dk_ttno1v, option_grup_dk_tt1no1v, option_grup_dk_tt2no1v, option_grup_dk_tt3no1v,
                        int_y_id110_3, int_n_id110_3,
                        checkedId, iv_circle_data_kesehatanv_qu, getContext());
                int_y_id110_3 = map.get("int_y_id1");
                int_n_id110_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1v:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1v, option_grup_dk_ttno1v, option_grup_dk_tt1no1v, option_grup_dk_tt2no1v, option_grup_dk_tt3no1v,
                        int_y_id110_4, int_n_id110_4,
                        checkedId, iv_circle_data_kesehatanv_qu, getContext());
                int_y_id110_4 = map.get("int_y_id1");
                int_n_id110_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1v:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1v, option_grup_dk_ttno1v, option_grup_dk_tt1no1v, option_grup_dk_tt2no1v, option_grup_dk_tt3no1v,
                        int_y_id110_5, int_n_id110_5,
                        checkedId, iv_circle_data_kesehatanv_qu, getContext());
                int_y_id110_5 = map.get("int_y_id1");
                int_n_id110_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1vi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1vi, option_grup_dk_ttno1vi, option_grup_dk_tt1no1vi, option_grup_dk_tt2no1vi, option_grup_dk_tt3no1vi,
                        int_y_id111_1, int_n_id111_1,
                        checkedId, iv_circle_data_kesehatanvi_qu, getContext());
                int_y_id111_1 = map.get("int_y_id1");
                int_n_id111_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1vi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1vi, option_grup_dk_ttno1vi, option_grup_dk_tt1no1vi, option_grup_dk_tt2no1vi, option_grup_dk_tt3no1vi,
                        int_y_id111_2, int_n_id111_2,
                        checkedId, iv_circle_data_kesehatanvi_qu, getContext());
                int_y_id111_2 = map.get("int_y_id1");
                int_n_id111_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1vi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1vi, option_grup_dk_ttno1vi, option_grup_dk_tt1no1vi, option_grup_dk_tt2no1vi, option_grup_dk_tt3no1vi,
                        int_y_id111_3, int_n_id111_3,
                        checkedId, iv_circle_data_kesehatanvi_qu, getContext());
                int_y_id111_3 = map.get("int_y_id1");
                int_n_id111_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1vi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1vi, option_grup_dk_ttno1vi, option_grup_dk_tt1no1vi, option_grup_dk_tt2no1vi, option_grup_dk_tt3no1vi,
                        int_y_id111_4, int_n_id111_4,
                        checkedId, iv_circle_data_kesehatanvi_qu, getContext());
                int_y_id111_4 = map.get("int_y_id1");
                int_n_id111_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1vi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1vi, option_grup_dk_ttno1vi, option_grup_dk_tt1no1vi, option_grup_dk_tt2no1vi, option_grup_dk_tt3no1vi,
                        int_y_id111_5, int_n_id111_5,
                        checkedId, iv_circle_data_kesehatanvi_qu, getContext());
                int_y_id111_5 = map.get("int_y_id1");
                int_n_id111_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1vii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1vii, option_grup_dk_ttno1vii, option_grup_dk_tt1no1vii, option_grup_dk_tt2no1vii, option_grup_dk_tt3no1vii,
                        int_y_id112_1, int_n_id112_1,
                        checkedId, iv_circle_data_kesehatanvii_qu, getContext());
                int_y_id112_1 = map.get("int_y_id1");
                int_n_id112_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1vii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1vii, option_grup_dk_ttno1vii, option_grup_dk_tt1no1vii, option_grup_dk_tt2no1vii, option_grup_dk_tt3no1vii,
                        int_y_id112_2, int_n_id112_2,
                        checkedId, iv_circle_data_kesehatanvii_qu, getContext());
                int_y_id112_2 = map.get("int_y_id1");
                int_n_id112_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1vii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1vii, option_grup_dk_ttno1vii, option_grup_dk_tt1no1vii, option_grup_dk_tt2no1vii, option_grup_dk_tt3no1vii,
                        int_y_id112_3, int_n_id112_3,
                        checkedId, iv_circle_data_kesehatanvii_qu, getContext());
                int_y_id112_3 = map.get("int_y_id1");
                int_n_id112_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1vii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1vii, option_grup_dk_ttno1vii, option_grup_dk_tt1no1vii, option_grup_dk_tt2no1vii, option_grup_dk_tt3no1vii,
                        int_y_id112_4, int_n_id112_4,
                        checkedId, iv_circle_data_kesehatanvii_qu, getContext());
                int_y_id112_4 = map.get("int_y_id1");
                int_n_id112_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1vii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1vii, option_grup_dk_ttno1vii, option_grup_dk_tt1no1vii, option_grup_dk_tt2no1vii, option_grup_dk_tt3no1vii,
                        int_y_id112_5, int_n_id112_5,
                        checkedId, iv_circle_data_kesehatanvii_qu, getContext());
                int_y_id112_5 = map.get("int_y_id1");
                int_n_id112_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1viii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1viii, option_grup_dk_ttno1viii, option_grup_dk_tt1no1viii, option_grup_dk_tt2no1viii, option_grup_dk_tt3no1viii,
                        int_y_id113_1, int_n_id113_1,
                        checkedId, iv_circle_data_kesehatanviii_qu, getContext());
                int_y_id113_1 = map.get("int_y_id1");
                int_n_id113_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1viii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1viii, option_grup_dk_ttno1viii, option_grup_dk_tt1no1viii, option_grup_dk_tt2no1viii, option_grup_dk_tt3no1viii,
                        int_y_id113_2, int_n_id113_2,
                        checkedId, iv_circle_data_kesehatanviii_qu, getContext());
                int_y_id113_2 = map.get("int_y_id1");
                int_n_id113_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1viii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1viii, option_grup_dk_ttno1viii, option_grup_dk_tt1no1viii, option_grup_dk_tt2no1viii, option_grup_dk_tt3no1viii,
                        int_y_id113_3, int_n_id113_3,
                        checkedId, iv_circle_data_kesehatanviii_qu, getContext());
                int_y_id113_3 = map.get("int_y_id1");
                int_n_id113_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1viii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1viii, option_grup_dk_ttno1viii, option_grup_dk_tt1no1viii, option_grup_dk_tt2no1viii, option_grup_dk_tt3no1viii,
                        int_y_id113_4, int_n_id113_4,
                        checkedId, iv_circle_data_kesehatanviii_qu, getContext());
                int_y_id113_4 = map.get("int_y_id1");
                int_n_id113_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1viii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1viii, option_grup_dk_ttno1viii, option_grup_dk_tt1no1viii, option_grup_dk_tt2no1viii, option_grup_dk_tt3no1viii,
                        int_y_id113_5, int_n_id113_5,
                        checkedId, iv_circle_data_kesehatanviii_qu, getContext());
                int_y_id113_5 = map.get("int_y_id1");
                int_n_id113_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1ix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1ix, option_grup_dk_ttno1ix, option_grup_dk_tt1no1ix, option_grup_dk_tt2no1ix, option_grup_dk_tt3no1ix,
                        int_y_id114_1, int_n_id114_1,
                        checkedId, iv_circle_data_kesehatanix_qu, getContext());
                int_y_id114_1 = map.get("int_y_id1");
                int_n_id114_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1ix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1ix, option_grup_dk_ttno1ix, option_grup_dk_tt1no1ix, option_grup_dk_tt2no1ix, option_grup_dk_tt3no1ix,
                        int_y_id114_2, int_n_id114_2,
                        checkedId, iv_circle_data_kesehatanix_qu, getContext());
                int_y_id114_2 = map.get("int_y_id1");
                int_n_id114_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1ix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1ix, option_grup_dk_ttno1ix, option_grup_dk_tt1no1ix, option_grup_dk_tt2no1ix, option_grup_dk_tt3no1ix,
                        int_y_id114_3, int_n_id114_3,
                        checkedId, iv_circle_data_kesehatanix_qu, getContext());
                int_y_id114_3 = map.get("int_y_id1");
                int_n_id114_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1ix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1ix, option_grup_dk_ttno1ix, option_grup_dk_tt1no1ix, option_grup_dk_tt2no1ix, option_grup_dk_tt3no1ix,
                        int_y_id114_4, int_n_id114_4,
                        checkedId, iv_circle_data_kesehatanix_qu, getContext());
                int_y_id114_4 = map.get("int_y_id1");
                int_n_id114_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1ix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1ix, option_grup_dk_ttno1ix, option_grup_dk_tt1no1ix, option_grup_dk_tt2no1ix, option_grup_dk_tt3no1ix,
                        int_y_id114_5, int_n_id114_5,
                        checkedId, iv_circle_data_kesehatanix_qu, getContext());
                int_y_id114_5 = map.get("int_y_id1");
                int_n_id114_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1x:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1x, option_grup_dk_ttno1x, option_grup_dk_tt1no1x, option_grup_dk_tt2no1x, option_grup_dk_tt3no1x,
                        int_y_id115_1, int_n_id115_1,
                        checkedId, iv_circle_data_kesehatanx_qu, getContext());
                int_y_id115_1 = map.get("int_y_id1");
                int_n_id115_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1x:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1x, option_grup_dk_ttno1x, option_grup_dk_tt1no1x, option_grup_dk_tt2no1x, option_grup_dk_tt3no1x,
                        int_y_id115_2, int_n_id115_2,
                        checkedId, iv_circle_data_kesehatanx_qu, getContext());
                int_y_id115_2 = map.get("int_y_id1");
                int_n_id115_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1x:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1x, option_grup_dk_ttno1x, option_grup_dk_tt1no1x, option_grup_dk_tt2no1x, option_grup_dk_tt3no1x,
                        int_y_id115_3, int_n_id115_3,
                        checkedId, iv_circle_data_kesehatanx_qu, getContext());
                int_y_id115_3 = map.get("int_y_id1");
                int_n_id115_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1x:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1x, option_grup_dk_ttno1x, option_grup_dk_tt1no1x, option_grup_dk_tt2no1x, option_grup_dk_tt3no1x,
                        int_y_id115_4, int_n_id115_4,
                        checkedId, iv_circle_data_kesehatanx_qu, getContext());
                int_y_id115_4 = map.get("int_y_id1");
                int_n_id115_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1x:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1x, option_grup_dk_ttno1x, option_grup_dk_tt1no1x, option_grup_dk_tt2no1x, option_grup_dk_tt3no1x,
                        int_y_id115_5, int_n_id115_5,
                        checkedId, iv_circle_data_kesehatanx_qu, getContext());
                int_y_id115_5 = map.get("int_y_id1");
                int_n_id115_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xi, option_grup_dk_ttno1xi, option_grup_dk_tt1no1xi, option_grup_dk_tt2no1xi, option_grup_dk_tt3no1xi,
                        int_y_id116_1, int_n_id116_1,
                        checkedId, iv_circle_data_kesehatanxi_qu, getContext());
                int_y_id116_1 = map.get("int_y_id1");
                int_n_id116_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xi, option_grup_dk_ttno1xi, option_grup_dk_tt1no1xi, option_grup_dk_tt2no1xi, option_grup_dk_tt3no1xi,
                        int_y_id116_2, int_n_id116_2,
                        checkedId, iv_circle_data_kesehatanxi_qu, getContext());
                int_y_id116_2 = map.get("int_y_id1");
                int_n_id116_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xi, option_grup_dk_ttno1xi, option_grup_dk_tt1no1xi, option_grup_dk_tt2no1xi, option_grup_dk_tt3no1xi,
                        int_y_id116_3, int_n_id116_3,
                        checkedId, iv_circle_data_kesehatanxi_qu, getContext());
                int_y_id116_3 = map.get("int_y_id1");
                int_n_id116_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xi, option_grup_dk_ttno1xi, option_grup_dk_tt1no1xi, option_grup_dk_tt2no1xi, option_grup_dk_tt3no1xi,
                        int_y_id116_4, int_n_id116_4,
                        checkedId, iv_circle_data_kesehatanxi_qu, getContext());
                int_y_id116_4 = map.get("int_y_id1");
                int_n_id116_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xi, option_grup_dk_ttno1xi, option_grup_dk_tt1no1xi, option_grup_dk_tt2no1xi, option_grup_dk_tt3no1xi,
                        int_y_id116_5, int_n_id116_5,
                        checkedId, iv_circle_data_kesehatanxi_qu, getContext());
                int_y_id116_5 = map.get("int_y_id1");
                int_n_id116_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xii, option_grup_dk_ttno1xii, option_grup_dk_tt1no1xii, option_grup_dk_tt2no1xii, option_grup_dk_tt3no1xii,
                        int_y_id117_1, int_n_id117_1,
                        checkedId, iv_circle_data_kesehatanxii_qu, getContext());
                int_y_id117_1 = map.get("int_y_id1");
                int_n_id117_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xii, option_grup_dk_ttno1xii, option_grup_dk_tt1no1xii, option_grup_dk_tt2no1xii, option_grup_dk_tt3no1xii,
                        int_y_id117_2, int_n_id117_2,
                        checkedId, iv_circle_data_kesehatanxii_qu, getContext());
                int_y_id117_2 = map.get("int_y_id1");
                int_n_id117_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xii, option_grup_dk_ttno1xii, option_grup_dk_tt1no1xii, option_grup_dk_tt2no1xii, option_grup_dk_tt3no1xii,
                        int_y_id117_3, int_n_id117_3,
                        checkedId, iv_circle_data_kesehatanxii_qu, getContext());
                int_y_id117_3 = map.get("int_y_id1");
                int_n_id117_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xii, option_grup_dk_ttno1xii, option_grup_dk_tt1no1xii, option_grup_dk_tt2no1xii, option_grup_dk_tt3no1xii,
                        int_y_id117_4, int_n_id117_4,
                        checkedId, iv_circle_data_kesehatanxii_qu, getContext());
                int_y_id117_4 = map.get("int_y_id1");
                int_n_id117_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xii, option_grup_dk_ttno1xii, option_grup_dk_tt1no1xii, option_grup_dk_tt2no1xii, option_grup_dk_tt3no1xii,
                        int_y_id117_5, int_n_id117_5,
                        checkedId, iv_circle_data_kesehatanxii_qu, getContext());
                int_y_id117_5 = map.get("int_y_id1");
                int_n_id117_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xiii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xiii, option_grup_dk_ttno1xiii, option_grup_dk_tt1no1xiii, option_grup_dk_tt2no1xiii, option_grup_dk_tt3no1xiii,
                        int_y_id118_1, int_n_id118_1,
                        checkedId, iv_circle_data_kesehatanxiii_qu, getContext());
                int_y_id118_1 = map.get("int_y_id1");
                int_n_id118_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xiii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xiii, option_grup_dk_ttno1xiii, option_grup_dk_tt1no1xiii, option_grup_dk_tt2no1xiii, option_grup_dk_tt3no1xiii,
                        int_y_id118_2, int_n_id118_2,
                        checkedId, iv_circle_data_kesehatanxiii_qu, getContext());
                int_y_id118_2 = map.get("int_y_id1");
                int_n_id118_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xiii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xiii, option_grup_dk_ttno1xiii, option_grup_dk_tt1no1xiii, option_grup_dk_tt2no1xiii, option_grup_dk_tt3no1xiii,
                        int_y_id118_3, int_n_id118_3,
                        checkedId, iv_circle_data_kesehatanxiii_qu, getContext());
                int_y_id118_3 = map.get("int_y_id1");
                int_n_id118_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xiii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xiii, option_grup_dk_ttno1xiii, option_grup_dk_tt1no1xiii, option_grup_dk_tt2no1xiii, option_grup_dk_tt3no1xiii,
                        int_y_id118_4, int_n_id118_4,
                        checkedId, iv_circle_data_kesehatanxiii_qu, getContext());
                int_y_id118_4 = map.get("int_y_id1");
                int_n_id118_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xiii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xiii, option_grup_dk_ttno1xiii, option_grup_dk_tt1no1xiii, option_grup_dk_tt2no1xiii, option_grup_dk_tt3no1xiii,
                        int_y_id118_5, int_n_id118_5,
                        checkedId, iv_circle_data_kesehatanxiii_qu, getContext());
                int_y_id118_5 = map.get("int_y_id1");
                int_n_id118_5 = map.get("int_n_id1");
                break;
            //bernard batas
            case R.id.option_grup_dk_ppno1xiv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xiv, option_grup_dk_ttno1xiv, option_grup_dk_tt1no1xiv, option_grup_dk_tt2no1xiv, option_grup_dk_tt3no1xiv,
                        int_y_id119_1, int_n_id119_1,
                        checkedId, iv_circle_data_kesehatanxiv_qu, getContext());
                int_y_id119_1 = map.get("int_y_id1");
                int_n_id119_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xiv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xiv, option_grup_dk_ttno1xiv, option_grup_dk_tt1no1xiv, option_grup_dk_tt2no1xiv, option_grup_dk_tt3no1xiv,
                        int_y_id119_2, int_n_id119_2,
                        checkedId, iv_circle_data_kesehatanxiv_qu, getContext());
                int_y_id119_2 = map.get("int_y_id1");
                int_n_id119_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xiv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xiv, option_grup_dk_ttno1xiv, option_grup_dk_tt1no1xiv, option_grup_dk_tt2no1xiv, option_grup_dk_tt3no1xiv,
                        int_y_id119_3, int_n_id119_3,
                        checkedId, iv_circle_data_kesehatanxiv_qu, getContext());
                int_y_id119_3 = map.get("int_y_id1");
                int_n_id119_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xiv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xiv, option_grup_dk_ttno1xiv, option_grup_dk_tt1no1xiv, option_grup_dk_tt2no1xiv, option_grup_dk_tt3no1xiv,
                        int_y_id119_4, int_n_id119_4,
                        checkedId, iv_circle_data_kesehatanxiv_qu, getContext());
                int_y_id119_4 = map.get("int_y_id1");
                int_n_id119_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xiv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xiv, option_grup_dk_ttno1xiv, option_grup_dk_tt1no1xiv, option_grup_dk_tt2no1xiv, option_grup_dk_tt3no1xiv,
                        int_y_id119_5, int_n_id119_5,
                        checkedId, iv_circle_data_kesehatanxiv_qu, getContext());
                int_y_id119_5 = map.get("int_y_id1");
                int_n_id119_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xv, option_grup_dk_ttno1xv, option_grup_dk_tt1no1xv, option_grup_dk_tt2no1xv, option_grup_dk_tt3no1xv,
                        int_y_id120_1, int_n_id120_1,
                        checkedId, iv_circle_data_kesehatanxv_qu, getContext());
                int_y_id120_1 = map.get("int_y_id1");
                int_n_id120_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xv, option_grup_dk_ttno1xv, option_grup_dk_tt1no1xv, option_grup_dk_tt2no1xv, option_grup_dk_tt3no1xv,
                        int_y_id120_2, int_n_id120_2,
                        checkedId, iv_circle_data_kesehatanxv_qu, getContext());
                int_y_id120_2 = map.get("int_y_id1");
                int_n_id120_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xv, option_grup_dk_ttno1xv, option_grup_dk_tt1no1xv, option_grup_dk_tt2no1xv, option_grup_dk_tt3no1xv,
                        int_y_id120_3, int_n_id120_3,
                        checkedId, iv_circle_data_kesehatanxv_qu, getContext());
                int_y_id120_3 = map.get("int_y_id1");
                int_n_id120_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xv, option_grup_dk_ttno1xv, option_grup_dk_tt1no1xv, option_grup_dk_tt2no1xv, option_grup_dk_tt3no1xv,
                        int_y_id120_4, int_n_id120_4,
                        checkedId, iv_circle_data_kesehatanxv_qu, getContext());
                int_y_id120_4 = map.get("int_y_id1");
                int_n_id120_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xv, option_grup_dk_ttno1xv, option_grup_dk_tt1no1xv, option_grup_dk_tt2no1xv, option_grup_dk_tt3no1xv,
                        int_y_id120_5, int_n_id120_5,
                        checkedId, iv_circle_data_kesehatanxv_qu, getContext());
                int_y_id120_5 = map.get("int_y_id1");
                int_n_id120_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xvi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xvi, option_grup_dk_ttno1xvi, option_grup_dk_tt1no1xvi, option_grup_dk_tt2no1xvi, option_grup_dk_tt3no1xvi,
                        int_y_id121_1, int_n_id121_1,
                        checkedId, iv_circle_data_kesehatanxvi_qu, getContext());
                int_y_id121_1 = map.get("int_y_id1");
                int_n_id121_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xvi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xvi, option_grup_dk_ttno1xvi, option_grup_dk_tt1no1xvi, option_grup_dk_tt2no1xvi, option_grup_dk_tt3no1xvi,
                        int_y_id121_2, int_n_id121_2,
                        checkedId, iv_circle_data_kesehatanxvi_qu, getContext());
                int_y_id121_2 = map.get("int_y_id1");
                int_n_id121_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xvi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xvi, option_grup_dk_ttno1xvi, option_grup_dk_tt1no1xvi, option_grup_dk_tt2no1xvi, option_grup_dk_tt3no1xvi,
                        int_y_id121_3, int_n_id121_3,
                        checkedId, iv_circle_data_kesehatanxvi_qu, getContext());
                int_y_id121_3 = map.get("int_y_id1");
                int_n_id121_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xvi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xvi, option_grup_dk_ttno1xvi, option_grup_dk_tt1no1xvi, option_grup_dk_tt2no1xvi, option_grup_dk_tt3no1xvi,
                        int_y_id121_4, int_n_id121_4,
                        checkedId, iv_circle_data_kesehatanxvi_qu, getContext());
                int_y_id121_4 = map.get("int_y_id1");
                int_n_id121_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xvi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xvi, option_grup_dk_ttno1xvi, option_grup_dk_tt1no1xvi, option_grup_dk_tt2no1xvi, option_grup_dk_tt3no1xvi,
                        int_y_id121_5, int_n_id121_5,
                        checkedId, iv_circle_data_kesehatanxvi_qu, getContext());
                int_y_id121_5 = map.get("int_y_id1");
                int_n_id121_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xvii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xvii, option_grup_dk_ttno1xvii, option_grup_dk_tt1no1xvii, option_grup_dk_tt2no1xvii, option_grup_dk_tt3no1xvii,
                        int_y_id122_1, int_n_id122_1,
                        checkedId, iv_circle_data_kesehatanxvii_qu, getContext());
                int_y_id122_1 = map.get("int_y_id1");
                int_n_id122_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xvii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xvii, option_grup_dk_ttno1xvii, option_grup_dk_tt1no1xvii, option_grup_dk_tt2no1xvii, option_grup_dk_tt3no1xvii,
                        int_y_id122_2, int_n_id122_2,
                        checkedId, iv_circle_data_kesehatanxvii_qu, getContext());
                int_y_id122_2 = map.get("int_y_id1");
                int_n_id122_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xvii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xvii, option_grup_dk_ttno1xvii, option_grup_dk_tt1no1xvii, option_grup_dk_tt2no1xvii, option_grup_dk_tt3no1xvii,
                        int_y_id122_3, int_n_id122_3,
                        checkedId, iv_circle_data_kesehatanxvii_qu, getContext());
                int_y_id122_3 = map.get("int_y_id1");
                int_n_id122_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xvii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xvii, option_grup_dk_ttno1xvii, option_grup_dk_tt1no1xvii, option_grup_dk_tt2no1xvii, option_grup_dk_tt3no1xvii,
                        int_y_id122_4, int_n_id122_4,
                        checkedId, iv_circle_data_kesehatanxvii_qu, getContext());
                int_y_id122_4 = map.get("int_y_id1");
                int_n_id122_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xvii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xvii, option_grup_dk_ttno1xvii, option_grup_dk_tt1no1xvii, option_grup_dk_tt2no1xvii, option_grup_dk_tt3no1xvii,
                        int_y_id122_5, int_n_id122_5,
                        checkedId, iv_circle_data_kesehatanxvii_qu, getContext());
                int_y_id122_5 = map.get("int_y_id1");
                int_n_id122_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xviii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xviii, option_grup_dk_ttno1xviii, option_grup_dk_tt1no1xviii, option_grup_dk_tt2no1xviii, option_grup_dk_tt3no1xviii,
                        int_y_id123_1, int_n_id123_1,
                        checkedId, iv_circle_data_kesehatanxviii_qu, getContext());
                int_y_id123_1 = map.get("int_y_id1");
                int_n_id123_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xviii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xviii, option_grup_dk_ttno1xviii, option_grup_dk_tt1no1xviii, option_grup_dk_tt2no1xviii, option_grup_dk_tt3no1xviii,
                        int_y_id123_2, int_n_id123_2,
                        checkedId, iv_circle_data_kesehatanxviii_qu, getContext());
                int_y_id123_2 = map.get("int_y_id1");
                int_n_id123_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xviii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xviii, option_grup_dk_ttno1xviii, option_grup_dk_tt1no1xviii, option_grup_dk_tt2no1xviii, option_grup_dk_tt3no1xviii,
                        int_y_id123_3, int_n_id123_3,
                        checkedId, iv_circle_data_kesehatanxviii_qu, getContext());
                int_y_id123_3 = map.get("int_y_id1");
                int_n_id123_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xviii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xviii, option_grup_dk_ttno1xviii, option_grup_dk_tt1no1xviii, option_grup_dk_tt2no1xviii, option_grup_dk_tt3no1xviii,
                        int_y_id123_4, int_n_id123_4,
                        checkedId, iv_circle_data_kesehatanxviii_qu, getContext());
                int_y_id123_4 = map.get("int_y_id1");
                int_n_id123_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xviii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xviii, option_grup_dk_ttno1xviii, option_grup_dk_tt1no1xviii, option_grup_dk_tt2no1xviii, option_grup_dk_tt3no1xviii,
                        int_y_id123_5, int_n_id123_5,
                        checkedId, iv_circle_data_kesehatanxviii_qu, getContext());
                int_y_id123_5 = map.get("int_y_id1");
                int_n_id123_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xix, option_grup_dk_ttno1xix, option_grup_dk_tt1no1xix, option_grup_dk_tt2no1xix, option_grup_dk_tt3no1xix,
                        int_y_id124_1, int_n_id124_1,
                        checkedId, iv_circle_data_kesehatanxix_qu, getContext());
                int_y_id124_1 = map.get("int_y_id1");
                int_n_id124_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xix, option_grup_dk_ttno1xix, option_grup_dk_tt1no1xix, option_grup_dk_tt2no1xix, option_grup_dk_tt3no1xix,
                        int_y_id124_2, int_n_id124_2,
                        checkedId, iv_circle_data_kesehatanxix_qu, getContext());
                int_y_id124_2 = map.get("int_y_id1");
                int_n_id124_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xix, option_grup_dk_ttno1xix, option_grup_dk_tt1no1xix, option_grup_dk_tt2no1xix, option_grup_dk_tt3no1xix,
                        int_y_id124_3, int_n_id124_3,
                        checkedId, iv_circle_data_kesehatanxix_qu, getContext());
                int_y_id124_3 = map.get("int_y_id1");
                int_n_id124_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xix, option_grup_dk_ttno1xix, option_grup_dk_tt1no1xix, option_grup_dk_tt2no1xix, option_grup_dk_tt3no1xix,
                        int_y_id124_4, int_n_id124_4,
                        checkedId, iv_circle_data_kesehatanxix_qu, getContext());
                int_y_id124_4 = map.get("int_y_id1");
                int_n_id124_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xix, option_grup_dk_ttno1xix, option_grup_dk_tt1no1xix, option_grup_dk_tt2no1xix, option_grup_dk_tt3no1xix,
                        int_y_id124_5, int_n_id124_5,
                        checkedId, iv_circle_data_kesehatanxix_qu, getContext());
                int_y_id124_5 = map.get("int_y_id1");
                int_n_id124_5 = map.get("int_n_id1");
                break;
            // batas bernard 2
            case R.id.option_grup_dk_ppno1xx:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xx, option_grup_dk_ttno1xx, option_grup_dk_tt1no1xx, option_grup_dk_tt2no1xx, option_grup_dk_tt3no1xx,
                        int_y_id125_1, int_n_id125_1,
                        checkedId, iv_circle_data_kesehatanxx_qu, getContext());
                int_y_id125_1 = map.get("int_y_id1");
                int_n_id125_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xx:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xx, option_grup_dk_ttno1xx, option_grup_dk_tt1no1xx, option_grup_dk_tt2no1xx, option_grup_dk_tt3no1xx,
                        int_y_id125_2, int_n_id125_2,
                        checkedId, iv_circle_data_kesehatanxx_qu, getContext());
                int_y_id125_2 = map.get("int_y_id1");
                int_n_id125_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xx:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xx, option_grup_dk_ttno1xx, option_grup_dk_tt1no1xx, option_grup_dk_tt2no1xx, option_grup_dk_tt3no1xx,
                        int_y_id125_3, int_n_id125_3,
                        checkedId, iv_circle_data_kesehatanxx_qu, getContext());
                int_y_id125_3 = map.get("int_y_id1");
                int_n_id125_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xx:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xx, option_grup_dk_ttno1xx, option_grup_dk_tt1no1xx, option_grup_dk_tt2no1xx, option_grup_dk_tt3no1xx,
                        int_y_id125_4, int_n_id125_4,
                        checkedId, iv_circle_data_kesehatanxx_qu, getContext());
                int_y_id125_4 = map.get("int_y_id1");
                int_n_id125_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xx:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xx, option_grup_dk_ttno1xx, option_grup_dk_tt1no1xx, option_grup_dk_tt2no1xx, option_grup_dk_tt3no1xx,
                        int_y_id125_5, int_n_id125_5,
                        checkedId, iv_circle_data_kesehatanxx_qu, getContext());
                int_y_id125_5 = map.get("int_y_id1");
                int_n_id125_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xxi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxi, option_grup_dk_ttno1xxi, option_grup_dk_tt1no1xxi, option_grup_dk_tt2no1xxi, option_grup_dk_tt3no1xxi,
                        int_y_id126_1, int_n_id126_1,
                        checkedId, iv_circle_data_kesehatanxxi_qu, getContext());
                int_y_id126_1 = map.get("int_y_id1");
                int_n_id126_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xxi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxi, option_grup_dk_ttno1xxi, option_grup_dk_tt1no1xxi, option_grup_dk_tt2no1xxi, option_grup_dk_tt3no1xxi,
                        int_y_id126_2, int_n_id126_2,
                        checkedId, iv_circle_data_kesehatanxxi_qu, getContext());
                int_y_id126_2 = map.get("int_y_id1");
                int_n_id126_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xxi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxi, option_grup_dk_ttno1xxi, option_grup_dk_tt1no1xxi, option_grup_dk_tt2no1xxi, option_grup_dk_tt3no1xxi,
                        int_y_id126_3, int_n_id126_3,
                        checkedId, iv_circle_data_kesehatanxxi_qu, getContext());
                int_y_id126_3 = map.get("int_y_id1");
                int_n_id126_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xxi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxi, option_grup_dk_ttno1xxi, option_grup_dk_tt1no1xxi, option_grup_dk_tt2no1xxi, option_grup_dk_tt3no1xxi,
                        int_y_id126_4, int_n_id126_4,
                        checkedId, iv_circle_data_kesehatanxxi_qu, getContext());
                int_y_id126_4 = map.get("int_y_id1");
                int_n_id126_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xxi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxi, option_grup_dk_ttno1xxi, option_grup_dk_tt1no1xxi, option_grup_dk_tt2no1xxi, option_grup_dk_tt3no1xxi,
                        int_y_id126_5, int_n_id126_5,
                        checkedId, iv_circle_data_kesehatanxxi_qu, getContext());
                int_y_id126_5 = map.get("int_y_id1");
                int_n_id126_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xxii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxii, option_grup_dk_ttno1xxii, option_grup_dk_tt1no1xxii, option_grup_dk_tt2no1xxii, option_grup_dk_tt3no1xxii,
                        int_y_id127_1, int_n_id127_1,
                        checkedId, iv_circle_data_kesehatanxxii_qu, getContext());
                int_y_id127_1 = map.get("int_y_id1");
                int_n_id127_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xxii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxii, option_grup_dk_ttno1xxii, option_grup_dk_tt1no1xxii, option_grup_dk_tt2no1xxii, option_grup_dk_tt3no1xxii,
                        int_y_id127_2, int_n_id127_2,
                        checkedId, iv_circle_data_kesehatanxxii_qu, getContext());
                int_y_id127_2 = map.get("int_y_id1");
                int_n_id127_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xxii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxii, option_grup_dk_ttno1xxii, option_grup_dk_tt1no1xxii, option_grup_dk_tt2no1xxii, option_grup_dk_tt3no1xxii,
                        int_y_id127_3, int_n_id127_3,
                        checkedId, iv_circle_data_kesehatanxxii_qu, getContext());
                int_y_id127_3 = map.get("int_y_id1");
                int_n_id127_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xxii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxii, option_grup_dk_ttno1xxii, option_grup_dk_tt1no1xxii, option_grup_dk_tt2no1xxii, option_grup_dk_tt3no1xxii,
                        int_y_id127_4, int_n_id127_4,
                        checkedId, iv_circle_data_kesehatanxxii_qu, getContext());
                int_y_id127_4 = map.get("int_y_id1");
                int_n_id127_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xxii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxii, option_grup_dk_ttno1xxii, option_grup_dk_tt1no1xxii, option_grup_dk_tt2no1xxii, option_grup_dk_tt3no1xxii,
                        int_y_id127_5, int_n_id127_5,
                        checkedId, iv_circle_data_kesehatanxxii_qu, getContext());
                int_y_id127_5 = map.get("int_y_id1");
                int_n_id127_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xxiii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxiii, option_grup_dk_ttno1xxiii, option_grup_dk_tt1no1xxiii, option_grup_dk_tt2no1xxiii, option_grup_dk_tt3no1xxiii,
                        int_y_id128_1, int_n_id128_1,
                        checkedId, iv_circle_data_kesehatanxxiii_qu, getContext());
                int_y_id128_1 = map.get("int_y_id1");
                int_n_id128_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xxiii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxiii, option_grup_dk_ttno1xxiii, option_grup_dk_tt1no1xxiii, option_grup_dk_tt2no1xxiii, option_grup_dk_tt3no1xxiii,
                        int_y_id128_2, int_n_id128_2,
                        checkedId, iv_circle_data_kesehatanxxiii_qu, getContext());
                int_y_id128_2 = map.get("int_y_id1");
                int_n_id128_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xxiii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxiii, option_grup_dk_ttno1xxiii, option_grup_dk_tt1no1xxiii, option_grup_dk_tt2no1xxiii, option_grup_dk_tt3no1xxiii,
                        int_y_id128_3, int_n_id128_3,
                        checkedId, iv_circle_data_kesehatanxxiii_qu, getContext());
                int_y_id128_3 = map.get("int_y_id1");
                int_n_id128_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xxiii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxiii, option_grup_dk_ttno1xxiii, option_grup_dk_tt1no1xxiii, option_grup_dk_tt2no1xxiii, option_grup_dk_tt3no1xxiii,
                        int_y_id128_4, int_n_id128_4,
                        checkedId, iv_circle_data_kesehatanxxiii_qu, getContext());
                int_y_id128_4 = map.get("int_y_id1");
                int_n_id128_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xxiii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxiii, option_grup_dk_ttno1xxiii, option_grup_dk_tt1no1xxiii, option_grup_dk_tt2no1xxiii, option_grup_dk_tt3no1xxiii,
                        int_y_id128_5, int_n_id128_5,
                        checkedId, iv_circle_data_kesehatanxxiii_qu, getContext());
                int_y_id128_5 = map.get("int_y_id1");
                int_n_id128_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xxiv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxiv, option_grup_dk_ttno1xxiv, option_grup_dk_tt1no1xxiv, option_grup_dk_tt2no1xxiv, option_grup_dk_tt3no1xxiv,
                        int_y_id129_1, int_n_id129_1,
                        checkedId, iv_circle_data_kesehatanxxiv_qu, getContext());
                int_y_id129_1 = map.get("int_y_id1");
                int_n_id129_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xxiv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxiv, option_grup_dk_ttno1xxiv, option_grup_dk_tt1no1xxiv, option_grup_dk_tt2no1xxiv, option_grup_dk_tt3no1xxiv,
                        int_y_id129_2, int_n_id129_2,
                        checkedId, iv_circle_data_kesehatanxxiv_qu, getContext());
                int_y_id129_2 = map.get("int_y_id1");
                int_n_id129_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xxiv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxiv, option_grup_dk_ttno1xxiv, option_grup_dk_tt1no1xxiv, option_grup_dk_tt2no1xxiv, option_grup_dk_tt3no1xxiv,
                        int_y_id129_3, int_n_id129_3,
                        checkedId, iv_circle_data_kesehatanxxiv_qu, getContext());
                int_y_id129_3 = map.get("int_y_id1");
                int_n_id129_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xxiv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxiv, option_grup_dk_ttno1xxiv, option_grup_dk_tt1no1xxiv, option_grup_dk_tt2no1xxiv, option_grup_dk_tt3no1xxiv,
                        int_y_id129_4, int_n_id129_4,
                        checkedId, iv_circle_data_kesehatanxxiv_qu, getContext());
                int_y_id129_4 = map.get("int_y_id1");
                int_n_id129_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xxiv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxiv, option_grup_dk_ttno1xxiv, option_grup_dk_tt1no1xxiv, option_grup_dk_tt2no1xxiv, option_grup_dk_tt3no1xxiv,
                        int_y_id129_5, int_n_id129_5,
                        checkedId, iv_circle_data_kesehatanxxiv_qu, getContext());
                int_y_id129_5 = map.get("int_y_id1");
                int_n_id129_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xxv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxv, option_grup_dk_ttno1xxv, option_grup_dk_tt1no1xxv, option_grup_dk_tt2no1xxv, option_grup_dk_tt3no1xxv,
                        int_y_id130_1, int_n_id130_1,
                        checkedId, iv_circle_data_kesehatanxxv_qu, getContext());
                int_y_id130_1 = map.get("int_y_id1");
                int_n_id130_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xxv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxv, option_grup_dk_ttno1xxv, option_grup_dk_tt1no1xxv, option_grup_dk_tt2no1xxv, option_grup_dk_tt3no1xxv,
                        int_y_id130_2, int_n_id130_2,
                        checkedId, iv_circle_data_kesehatanxxv_qu, getContext());
                int_y_id130_2 = map.get("int_y_id1");
                int_n_id130_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xxv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxv, option_grup_dk_ttno1xxv, option_grup_dk_tt1no1xxv, option_grup_dk_tt2no1xxv, option_grup_dk_tt3no1xxv,
                        int_y_id130_3, int_n_id130_3,
                        checkedId, iv_circle_data_kesehatanxxv_qu, getContext());
                int_y_id130_3 = map.get("int_y_id1");
                int_n_id130_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xxv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxv, option_grup_dk_ttno1xxv, option_grup_dk_tt1no1xxv, option_grup_dk_tt2no1xxv, option_grup_dk_tt3no1xxv,
                        int_y_id130_4, int_n_id130_4,
                        checkedId, iv_circle_data_kesehatanxxv_qu, getContext());
                int_y_id130_4 = map.get("int_y_id1");
                int_n_id130_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xxv:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxv, option_grup_dk_ttno1xxv, option_grup_dk_tt1no1xxv, option_grup_dk_tt2no1xxv, option_grup_dk_tt3no1xxv,
                        int_y_id130_5, int_n_id130_5,
                        checkedId, iv_circle_data_kesehatanxxv_qu, getContext());
                int_y_id130_5 = map.get("int_y_id1");
                int_n_id130_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xxvi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxvi, option_grup_dk_ttno1xxvi, option_grup_dk_tt1no1xxvi, option_grup_dk_tt2no1xxvi, option_grup_dk_tt3no1xxvi,
                        int_y_id131_1, int_n_id131_1,
                        checkedId, iv_circle_data_kesehatanxxvi_qu, getContext());
                int_y_id131_1 = map.get("int_y_id1");
                int_n_id131_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xxvi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxvi, option_grup_dk_ttno1xxvi, option_grup_dk_tt1no1xxvi, option_grup_dk_tt2no1xxvi, option_grup_dk_tt3no1xxvi,
                        int_y_id131_2, int_n_id131_2,
                        checkedId, iv_circle_data_kesehatanxxvi_qu, getContext());
                int_y_id131_2 = map.get("int_y_id1");
                int_n_id131_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xxvi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxvi, option_grup_dk_ttno1xxvi, option_grup_dk_tt1no1xxvi, option_grup_dk_tt2no1xxvi, option_grup_dk_tt3no1xxvi,
                        int_y_id131_3, int_n_id131_3,
                        checkedId, iv_circle_data_kesehatanxxvi_qu, getContext());
                int_y_id131_3 = map.get("int_y_id1");
                int_n_id131_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xxvi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxvi, option_grup_dk_ttno1xxvi, option_grup_dk_tt1no1xxvi, option_grup_dk_tt2no1xxvi, option_grup_dk_tt3no1xxvi,
                        int_y_id131_4, int_n_id131_4,
                        checkedId, iv_circle_data_kesehatanxxvi_qu, getContext());
                int_y_id131_4 = map.get("int_y_id1");
                int_n_id131_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xxvi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxvi, option_grup_dk_ttno1xxvi, option_grup_dk_tt1no1xxvi, option_grup_dk_tt2no1xxvi, option_grup_dk_tt3no1xxvi,
                        int_y_id131_5, int_n_id131_5,
                        checkedId, iv_circle_data_kesehatanxxvi_qu, getContext());
                int_y_id131_5 = map.get("int_y_id1");
                int_n_id131_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xxvii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxvii, option_grup_dk_ttno1xxvii, option_grup_dk_tt1no1xxvii, option_grup_dk_tt2no1xxvii, option_grup_dk_tt3no1xxvii,
                        int_y_id132_1, int_n_id132_1,
                        checkedId, iv_circle_data_kesehatanxxvii_qu, getContext());
                int_y_id132_1 = map.get("int_y_id1");
                int_n_id132_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xxvii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxvii, option_grup_dk_ttno1xxvii, option_grup_dk_tt1no1xxvii, option_grup_dk_tt2no1xxvii, option_grup_dk_tt3no1xxvii,
                        int_y_id132_2, int_n_id132_2,
                        checkedId, iv_circle_data_kesehatanxxvii_qu, getContext());
                int_y_id132_2 = map.get("int_y_id1");
                int_n_id132_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xxvii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxvii, option_grup_dk_ttno1xxvii, option_grup_dk_tt1no1xxvii, option_grup_dk_tt2no1xxvii, option_grup_dk_tt3no1xxvii,
                        int_y_id132_3, int_n_id132_3,
                        checkedId, iv_circle_data_kesehatanxxvii_qu, getContext());
                int_y_id132_3 = map.get("int_y_id1");
                int_n_id132_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xxvii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxvii, option_grup_dk_ttno1xxvii, option_grup_dk_tt1no1xxvii, option_grup_dk_tt2no1xxvii, option_grup_dk_tt3no1xxvii,
                        int_y_id132_4, int_n_id132_4,
                        checkedId, iv_circle_data_kesehatanxxvii_qu, getContext());
                int_y_id132_4 = map.get("int_y_id1");
                int_n_id132_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xxvii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxvii, option_grup_dk_ttno1xxvii, option_grup_dk_tt1no1xxvii, option_grup_dk_tt2no1xxvii, option_grup_dk_tt3no1xxvii,
                        int_y_id132_5, int_n_id132_5,
                        checkedId, iv_circle_data_kesehatanxxvii_qu, getContext());
                int_y_id132_5 = map.get("int_y_id1");
                int_n_id132_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xxviii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxviii, option_grup_dk_ttno1xxviii, option_grup_dk_tt1no1xxviii, option_grup_dk_tt2no1xxviii, option_grup_dk_tt3no1xxviii,
                        int_y_id133_1, int_n_id133_1,
                        checkedId, iv_circle_data_kesehatanxxviii_qu, getContext());
                int_y_id133_1 = map.get("int_y_id1");
                int_n_id133_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xxviii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxviii, option_grup_dk_ttno1xxviii, option_grup_dk_tt1no1xxviii, option_grup_dk_tt2no1xxviii, option_grup_dk_tt3no1xxviii,
                        int_y_id133_2, int_n_id133_2,
                        checkedId, iv_circle_data_kesehatanxxviii_qu, getContext());
                int_y_id133_2 = map.get("int_y_id1");
                int_n_id133_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xxviii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxviii, option_grup_dk_ttno1xxviii, option_grup_dk_tt1no1xxviii, option_grup_dk_tt2no1xxviii, option_grup_dk_tt3no1xxviii,
                        int_y_id133_3, int_n_id133_3,
                        checkedId, iv_circle_data_kesehatanxxviii_qu, getContext());
                int_y_id133_3 = map.get("int_y_id1");
                int_n_id133_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xxviii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxviii, option_grup_dk_ttno1xxviii, option_grup_dk_tt1no1xxviii, option_grup_dk_tt2no1xxviii, option_grup_dk_tt3no1xxviii,
                        int_y_id133_4, int_n_id133_4,
                        checkedId, iv_circle_data_kesehatanxxviii_qu, getContext());
                int_y_id133_4 = map.get("int_y_id1");
                int_n_id133_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xxviii:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxviii, option_grup_dk_ttno1xxviii, option_grup_dk_tt1no1xxviii, option_grup_dk_tt2no1xxviii, option_grup_dk_tt3no1xxviii,
                        int_y_id133_5, int_n_id133_5,
                        checkedId, iv_circle_data_kesehatanxxviii_qu, getContext());
                int_y_id133_5 = map.get("int_y_id1");
                int_n_id133_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xxix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxix, option_grup_dk_ttno1xxix, option_grup_dk_tt1no1xxix, option_grup_dk_tt2no1xxix, option_grup_dk_tt3no1xxix,
                        int_y_id134_1, int_n_id134_1,
                        checkedId, iv_circle_data_kesehatanxxix_qu, getContext());
                int_y_id134_1 = map.get("int_y_id1");
                int_n_id134_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xxix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxix, option_grup_dk_ttno1xxix, option_grup_dk_tt1no1xxix, option_grup_dk_tt2no1xxix, option_grup_dk_tt3no1xxix,
                        int_y_id134_2, int_n_id134_2,
                        checkedId, iv_circle_data_kesehatanxxix_qu, getContext());
                int_y_id134_2 = map.get("int_y_id1");
                int_n_id134_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xxix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxix, option_grup_dk_ttno1xxix, option_grup_dk_tt1no1xxix, option_grup_dk_tt2no1xxix, option_grup_dk_tt3no1xxix,
                        int_y_id134_3, int_n_id134_3,
                        checkedId, iv_circle_data_kesehatanxxix_qu, getContext());
                int_y_id134_3 = map.get("int_y_id1");
                int_n_id134_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xxix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxix, option_grup_dk_ttno1xxix, option_grup_dk_tt1no1xxix, option_grup_dk_tt2no1xxix, option_grup_dk_tt3no1xxix,
                        int_y_id134_4, int_n_id134_4,
                        checkedId, iv_circle_data_kesehatanxxix_qu, getContext());
                int_y_id134_4 = map.get("int_y_id1");
                int_n_id134_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xxix:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxix, option_grup_dk_ttno1xxix, option_grup_dk_tt1no1xxix, option_grup_dk_tt2no1xxix, option_grup_dk_tt3no1xxix,
                        int_y_id134_5, int_n_id134_5,
                        checkedId, iv_circle_data_kesehatanxxix_qu, getContext());
                int_y_id134_5 = map.get("int_y_id1");
                int_n_id134_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xxx:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxx, option_grup_dk_ttno1xxx, option_grup_dk_tt1no1xxx, option_grup_dk_tt2no1xxx, option_grup_dk_tt3no1xxx,
                        int_y_id135_1, int_n_id135_1,
                        checkedId, iv_circle_data_kesehatanxxx_qu, getContext());
                int_y_id135_1 = map.get("int_y_id1");
                int_n_id135_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xxx:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxx, option_grup_dk_ttno1xxx, option_grup_dk_tt1no1xxx, option_grup_dk_tt2no1xxx, option_grup_dk_tt3no1xxx,
                        int_y_id135_2, int_n_id135_2,
                        checkedId, iv_circle_data_kesehatanxxx_qu, getContext());
                int_y_id135_2 = map.get("int_y_id1");
                int_n_id135_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xxx:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxx, option_grup_dk_ttno1xxx, option_grup_dk_tt1no1xxx, option_grup_dk_tt2no1xxx, option_grup_dk_tt3no1xxx,
                        int_y_id135_3, int_n_id135_3,
                        checkedId, iv_circle_data_kesehatanxxx_qu, getContext());
                int_y_id135_3 = map.get("int_y_id1");
                int_n_id135_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xxx:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxx, option_grup_dk_ttno1xxx, option_grup_dk_tt1no1xxx, option_grup_dk_tt2no1xxx, option_grup_dk_tt3no1xxx,
                        int_y_id135_4, int_n_id135_4,
                        checkedId, iv_circle_data_kesehatanxxx_qu, getContext());
                int_y_id135_4 = map.get("int_y_id1");
                int_n_id135_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xxx:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxx, option_grup_dk_ttno1xxx, option_grup_dk_tt1no1xxx, option_grup_dk_tt2no1xxx, option_grup_dk_tt3no1xxx,
                        int_y_id135_5, int_n_id135_5,
                        checkedId, iv_circle_data_kesehatanxxx_qu, getContext());
                int_y_id135_5 = map.get("int_y_id1");
                int_n_id135_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno1xxxi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxxi, option_grup_dk_ttno1xxxi, option_grup_dk_tt1no1xxxi, option_grup_dk_tt2no1xxxi, option_grup_dk_tt3no1xxxi,
                        int_y_id136_1, int_n_id136_1,
                        checkedId, iv_circle_data_kesehatanxxxi_qu, getContext());
                int_y_id136_1 = map.get("int_y_id1");
                int_n_id136_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno1xxxi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxxi, option_grup_dk_ttno1xxxi, option_grup_dk_tt1no1xxxi, option_grup_dk_tt2no1xxxi, option_grup_dk_tt3no1xxxi,
                        int_y_id136_2, int_n_id136_2,
                        checkedId, iv_circle_data_kesehatanxxxi_qu, getContext());
                int_y_id136_2 = map.get("int_y_id1");
                int_n_id136_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no1xxxi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxxi, option_grup_dk_ttno1xxxi, option_grup_dk_tt1no1xxxi, option_grup_dk_tt2no1xxxi, option_grup_dk_tt3no1xxxi,
                        int_y_id136_3, int_n_id136_3,
                        checkedId, iv_circle_data_kesehatanxxxi_qu, getContext());
                int_y_id136_3 = map.get("int_y_id1");
                int_n_id136_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no1xxxi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxxi, option_grup_dk_ttno1xxxi, option_grup_dk_tt1no1xxxi, option_grup_dk_tt2no1xxxi, option_grup_dk_tt3no1xxxi,
                        int_y_id136_4, int_n_id136_4,
                        checkedId, iv_circle_data_kesehatanxxxi_qu, getContext());
                int_y_id136_4 = map.get("int_y_id1");
                int_n_id136_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no1xxxi:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno1xxxi, option_grup_dk_ttno1xxxi, option_grup_dk_tt1no1xxxi, option_grup_dk_tt2no1xxxi, option_grup_dk_tt3no1xxxi,
                        int_y_id136_5, int_n_id136_5,
                        checkedId, iv_circle_data_kesehatanxxxi_qu, getContext());
                int_y_id136_5 = map.get("int_y_id1");
                int_n_id136_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno2:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno2, option_grup_dk_ttno2, option_grup_dk_tt1no2, option_grup_dk_tt2no2, option_grup_dk_tt3no2, essay_dk_no2,
                        int_y_id137_1, int_n_id137_1,
                        checkedId, iv_circle_data_kesehatan2_qu, getContext());
                int_y_id137_1 = map.get("int_y_id1");
                int_n_id137_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno2:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno2, option_grup_dk_ttno2, option_grup_dk_tt1no2, option_grup_dk_tt2no2, option_grup_dk_tt3no2, essay_dk_no2,
                        int_y_id137_2, int_n_id137_2,
                        checkedId, iv_circle_data_kesehatan2_qu, getContext());
                int_y_id137_2 = map.get("int_y_id1");
                int_n_id137_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no2:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno2, option_grup_dk_ttno2, option_grup_dk_tt1no2, option_grup_dk_tt2no2, option_grup_dk_tt3no2, essay_dk_no2,
                        int_y_id137_3, int_n_id137_3,
                        checkedId, iv_circle_data_kesehatan2_qu, getContext());
                int_y_id137_3 = map.get("int_y_id1");
                int_n_id137_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no2:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno2, option_grup_dk_ttno2, option_grup_dk_tt1no2, option_grup_dk_tt2no2, option_grup_dk_tt3no2, essay_dk_no2,
                        int_y_id137_4, int_n_id137_4,
                        checkedId, iv_circle_data_kesehatan2_qu, getContext());
                int_y_id137_4 = map.get("int_y_id1");
                int_n_id137_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no2:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno2, option_grup_dk_ttno2, option_grup_dk_tt1no2, option_grup_dk_tt2no2, option_grup_dk_tt3no2, essay_dk_no2,
                        int_y_id137_5, int_n_id137_5,
                        checkedId, iv_circle_data_kesehatan2_qu, getContext());
                int_y_id137_5 = map.get("int_y_id1");
                int_n_id137_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno3a:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3a, option_grup_dk_ttno3a, option_grup_dk_tt1no3a, option_grup_dk_tt2no3a, option_grup_dk_tt3no3a,
                        int_y_id139_1, int_n_id139_1,
                        checkedId, iv_circle_data_kesehatan3_qu, getContext());
                int_y_id139_1 = map.get("int_y_id1");
                int_n_id139_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno3a:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3a, option_grup_dk_ttno3a, option_grup_dk_tt1no3a, option_grup_dk_tt2no3a, option_grup_dk_tt3no3a,
                        int_y_id139_2, int_n_id139_2,
                        checkedId, iv_circle_data_kesehatan3_qu, getContext());
                int_y_id139_2 = map.get("int_y_id1");
                int_n_id139_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no3a:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3a, option_grup_dk_ttno3a, option_grup_dk_tt1no3a, option_grup_dk_tt2no3a, option_grup_dk_tt3no3a,
                        int_y_id139_3, int_n_id139_3,
                        checkedId, iv_circle_data_kesehatan3_qu, getContext());
                int_y_id139_3 = map.get("int_y_id1");
                int_n_id139_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no3a:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3a, option_grup_dk_ttno3a, option_grup_dk_tt1no3a, option_grup_dk_tt2no3a, option_grup_dk_tt3no3a,
                        int_y_id139_4, int_n_id139_4,
                        checkedId, iv_circle_data_kesehatan3_qu, getContext());
                int_y_id139_4 = map.get("int_y_id1");
                int_n_id139_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no3a:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3a, option_grup_dk_ttno3a, option_grup_dk_tt1no3a, option_grup_dk_tt2no3a, option_grup_dk_tt3no3a,
                        int_y_id139_5, int_n_id139_5,
                        checkedId, iv_circle_data_kesehatan3_qu, getContext());
                int_y_id139_5 = map.get("int_y_id1");
                int_n_id139_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno3b:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3b, option_grup_dk_ttno3b, option_grup_dk_tt1no3b, option_grup_dk_tt2no3b, option_grup_dk_tt3no3b,
                        int_y_id140_1, int_n_id140_1,
                        checkedId, iv_circle_data_kesehatan3b_qu, getContext());
                int_y_id140_1 = map.get("int_y_id1");
                int_n_id140_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno3b:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3b, option_grup_dk_ttno3b, option_grup_dk_tt1no3b, option_grup_dk_tt2no3b, option_grup_dk_tt3no3b,
                        int_y_id140_2, int_n_id140_2,
                        checkedId, iv_circle_data_kesehatan3b_qu, getContext());
                int_y_id140_2 = map.get("int_y_id1");
                int_n_id140_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no3b:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3b, option_grup_dk_ttno3b, option_grup_dk_tt1no3b, option_grup_dk_tt2no3b, option_grup_dk_tt3no3b,
                        int_y_id140_3, int_n_id140_3,
                        checkedId, iv_circle_data_kesehatan3b_qu, getContext());
                int_y_id140_3 = map.get("int_y_id1");
                int_n_id140_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no3b:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3b, option_grup_dk_ttno3b, option_grup_dk_tt1no3b, option_grup_dk_tt2no3b, option_grup_dk_tt3no3b,
                        int_y_id140_4, int_n_id140_4,
                        checkedId, iv_circle_data_kesehatan3b_qu, getContext());
                int_y_id140_4 = map.get("int_y_id1");
                int_n_id140_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no3b:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3b, option_grup_dk_ttno3b, option_grup_dk_tt1no3b, option_grup_dk_tt2no3b, option_grup_dk_tt3no3b,
                        int_y_id140_5, int_n_id140_5,
                        checkedId, iv_circle_data_kesehatan3b_qu, getContext());
                int_y_id140_5 = map.get("int_y_id1");
                int_n_id140_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno3c:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3c, option_grup_dk_ttno3c, option_grup_dk_tt1no3c, option_grup_dk_tt2no3c, option_grup_dk_tt3no3c,
                        int_y_id141_1, int_n_id141_1,
                        checkedId, iv_circle_data_kesehatan3c_qu, getContext());
                int_y_id141_1 = map.get("int_y_id1");
                int_n_id141_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno3c:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3c, option_grup_dk_ttno3c, option_grup_dk_tt1no3c, option_grup_dk_tt2no3c, option_grup_dk_tt3no3c,
                        int_y_id141_2, int_n_id141_2,
                        checkedId, iv_circle_data_kesehatan3c_qu, getContext());
                int_y_id141_2 = map.get("int_y_id1");
                int_n_id141_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no3c:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3c, option_grup_dk_ttno3c, option_grup_dk_tt1no3c, option_grup_dk_tt2no3c, option_grup_dk_tt3no3c,
                        int_y_id141_3, int_n_id141_3,
                        checkedId, iv_circle_data_kesehatan3c_qu, getContext());
                int_y_id141_3 = map.get("int_y_id1");
                int_n_id141_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no3c:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3c, option_grup_dk_ttno3c, option_grup_dk_tt1no3c, option_grup_dk_tt2no3c, option_grup_dk_tt3no3c,
                        int_y_id141_4, int_n_id141_4,
                        checkedId, iv_circle_data_kesehatan3c_qu, getContext());
                int_y_id141_4 = map.get("int_y_id1");
                int_n_id141_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no3c:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3c, option_grup_dk_ttno3c, option_grup_dk_tt1no3c, option_grup_dk_tt2no3c, option_grup_dk_tt3no3c,
                        int_y_id141_5, int_n_id141_5,
                        checkedId, iv_circle_data_kesehatan3c_qu, getContext());
                int_y_id141_5 = map.get("int_y_id1");
                int_n_id141_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno3d:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3d, option_grup_dk_ttno3d, option_grup_dk_tt1no3d, option_grup_dk_tt2no3d, option_grup_dk_tt3no3d,
                        int_y_id142_1, int_n_id142_1,
                        checkedId, iv_circle_data_kesehatan3d_qu, getContext());
                int_y_id142_1 = map.get("int_y_id1");
                int_n_id142_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno3d:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3d, option_grup_dk_ttno3d, option_grup_dk_tt1no3d, option_grup_dk_tt2no3d, option_grup_dk_tt3no3d,
                        int_y_id142_2, int_n_id142_2,
                        checkedId, iv_circle_data_kesehatan3d_qu, getContext());
                int_y_id142_2 = map.get("int_y_id1");
                int_n_id142_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no3d:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3d, option_grup_dk_ttno3d, option_grup_dk_tt1no3d, option_grup_dk_tt2no3d, option_grup_dk_tt3no3d,
                        int_y_id142_3, int_n_id142_3,
                        checkedId, iv_circle_data_kesehatan3d_qu, getContext());
                int_y_id142_3 = map.get("int_y_id1");
                int_n_id142_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no3d:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3d, option_grup_dk_ttno3d, option_grup_dk_tt1no3d, option_grup_dk_tt2no3d, option_grup_dk_tt3no3d,
                        int_y_id142_4, int_n_id142_4,
                        checkedId, iv_circle_data_kesehatan3d_qu, getContext());
                int_y_id142_4 = map.get("int_y_id1");
                int_n_id142_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no3d:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno3d, option_grup_dk_ttno3d, option_grup_dk_tt1no3d, option_grup_dk_tt2no3d, option_grup_dk_tt3no3d,
                        int_y_id142_5, int_n_id142_5,
                        checkedId, iv_circle_data_kesehatan3d_qu, getContext());
                int_y_id142_5 = map.get("int_y_id1");
                int_n_id142_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno4:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno4, option_grup_dk_ttno4, option_grup_dk_tt1no4, option_grup_dk_tt2no4, option_grup_dk_tt3no4, essay_dk_no4,
                        int_y_id143_1, int_n_id143_1,
                        checkedId, iv_circle_data_kesehatan4_qu, getContext());
                int_y_id143_1 = map.get("int_y_id1");
                int_n_id143_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno4:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno4, option_grup_dk_ttno4, option_grup_dk_tt1no4, option_grup_dk_tt2no4, option_grup_dk_tt3no4, essay_dk_no4,
                        int_y_id143_2, int_n_id143_2,
                        checkedId, iv_circle_data_kesehatan4_qu, getContext());
                int_y_id143_2 = map.get("int_y_id1");
                int_n_id143_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no4:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno4, option_grup_dk_ttno4, option_grup_dk_tt1no4, option_grup_dk_tt2no4, option_grup_dk_tt3no4, essay_dk_no4,
                        int_y_id143_3, int_n_id143_3,
                        checkedId, iv_circle_data_kesehatan4_qu, getContext());
                int_y_id143_3 = map.get("int_y_id1");
                int_n_id143_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no4:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno4, option_grup_dk_ttno4, option_grup_dk_tt1no4, option_grup_dk_tt2no4, option_grup_dk_tt3no4, essay_dk_no4,
                        int_y_id143_4, int_n_id143_4,
                        checkedId, iv_circle_data_kesehatan4_qu, getContext());
                int_y_id143_4 = map.get("int_y_id1");
                int_n_id143_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no4:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno4, option_grup_dk_ttno4, option_grup_dk_tt1no4, option_grup_dk_tt2no4, option_grup_dk_tt3no4, essay_dk_no4,
                        int_y_id143_5, int_n_id143_5,
                        checkedId, iv_circle_data_kesehatan4_qu, getContext());
                int_y_id143_5 = map.get("int_y_id1");
                int_n_id143_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno5:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno5, option_grup_dk_ttno5, option_grup_dk_tt1no5, option_grup_dk_tt2no5, option_grup_dk_tt3no5,
                        int_y_id144_1, int_n_id144_1,
                        checkedId, iv_circle_data_kesehatan5_qu, getContext());
                int_y_id144_1 = map.get("int_y_id1");
                int_n_id144_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno5:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno5, option_grup_dk_ttno5, option_grup_dk_tt1no5, option_grup_dk_tt2no5, option_grup_dk_tt3no5,
                        int_y_id144_2, int_n_id144_2,
                        checkedId, iv_circle_data_kesehatan5_qu, getContext());
                int_y_id144_2 = map.get("int_y_id1");
                int_n_id144_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no5:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno5, option_grup_dk_ttno5, option_grup_dk_tt1no5, option_grup_dk_tt2no5, option_grup_dk_tt3no5,
                        int_y_id144_3, int_n_id144_3,
                        checkedId, iv_circle_data_kesehatan5_qu, getContext());
                int_y_id144_3 = map.get("int_y_id1");
                int_n_id144_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no5:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno5, option_grup_dk_ttno5, option_grup_dk_tt1no5, option_grup_dk_tt2no5, option_grup_dk_tt3no5,
                        int_y_id144_4, int_n_id144_4,
                        checkedId, iv_circle_data_kesehatan5_qu, getContext());
                int_y_id144_4 = map.get("int_y_id1");
                int_n_id144_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no5:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno5, option_grup_dk_ttno5, option_grup_dk_tt1no5, option_grup_dk_tt2no5, option_grup_dk_tt3no5,
                        int_y_id144_5, int_n_id144_5,
                        checkedId, iv_circle_data_kesehatan5_qu, getContext());
                int_y_id144_5 = map.get("int_y_id1");
                int_n_id144_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno6:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno6, option_grup_dk_ttno6, option_grup_dk_tt1no6, option_grup_dk_tt2no6, option_grup_dk_tt3no6,
                        int_y_id145_1, int_n_id145_1,
                        checkedId, iv_circle_data_kesehatan6_qu, getContext());
                int_y_id145_1 = map.get("int_y_id1");
                int_n_id145_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno6:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno6, option_grup_dk_ttno6, option_grup_dk_tt1no6, option_grup_dk_tt2no6, option_grup_dk_tt3no6,
                        int_y_id145_2, int_n_id145_2,
                        checkedId, iv_circle_data_kesehatan6_qu, getContext());
                int_y_id145_2 = map.get("int_y_id1");
                int_n_id145_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no6:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno6, option_grup_dk_ttno6, option_grup_dk_tt1no6, option_grup_dk_tt2no6, option_grup_dk_tt3no6,
                        int_y_id145_3, int_n_id145_3,
                        checkedId, iv_circle_data_kesehatan6_qu, getContext());
                int_y_id145_3 = map.get("int_y_id1");
                int_n_id145_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no6:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno6, option_grup_dk_ttno6, option_grup_dk_tt1no6, option_grup_dk_tt2no6, option_grup_dk_tt3no6,
                        int_y_id145_4, int_n_id145_4,
                        checkedId, iv_circle_data_kesehatan6_qu, getContext());
                int_y_id145_4 = map.get("int_y_id1");
                int_n_id145_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no6:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno6, option_grup_dk_ttno6, option_grup_dk_tt1no6, option_grup_dk_tt2no6, option_grup_dk_tt3no6,
                        int_y_id145_5, int_n_id145_5,
                        checkedId, iv_circle_data_kesehatan6_qu, getContext());
                int_y_id145_5 = map.get("int_y_id1");
                int_n_id145_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno7:
                map = MethodSupport.OnCheckQuisionerRadioKesValuekhususno7(
                        option_grup_dk_ppno7, option_grup_dk_ttno7, option_grup_dk_tt1no7, option_grup_dk_tt2no7, option_grup_dk_tt3no7,
                        int_y_id146_1, int_n_id146_1,
                        checkedId, iv_circle_data_kesehatan7_qu, getContext(), essay_dk_no7a, essay_dk_no7b, essay_dk_no7c, essay_dk_no7d, iv_circle_data_kesehatan7a_qu,
                        iv_circle_data_kesehatan7b_qu, iv_circle_data_kesehatan7c_qu, iv_circle_data_kesehatan7d_qu);
                int_y_id146_1 = map.get("int_y_id1");
                int_n_id146_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno7:
                map = MethodSupport.OnCheckQuisionerRadioKesValuekhususno7(
                        option_grup_dk_ppno7, option_grup_dk_ttno7, option_grup_dk_tt1no7, option_grup_dk_tt2no7, option_grup_dk_tt3no7,
                        int_y_id146_2, int_n_id146_2,
                        checkedId, iv_circle_data_kesehatan7_qu, getContext(), essay_dk_no7a, essay_dk_no7b, essay_dk_no7c, essay_dk_no7d, iv_circle_data_kesehatan7a_qu,
                        iv_circle_data_kesehatan7b_qu, iv_circle_data_kesehatan7c_qu, iv_circle_data_kesehatan7d_qu);
                int_y_id146_2 = map.get("int_y_id1");
                int_n_id146_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no7:
                map = MethodSupport.OnCheckQuisionerRadioKesValuekhususno7(
                        option_grup_dk_ppno7, option_grup_dk_ttno7, option_grup_dk_tt1no7, option_grup_dk_tt2no7, option_grup_dk_tt3no7,
                        int_y_id146_3, int_n_id146_3,
                        checkedId, iv_circle_data_kesehatan7_qu, getContext(), essay_dk_no7a, essay_dk_no7b, essay_dk_no7c, essay_dk_no7d, iv_circle_data_kesehatan7a_qu,
                        iv_circle_data_kesehatan7b_qu, iv_circle_data_kesehatan7c_qu, iv_circle_data_kesehatan7d_qu);
                int_y_id146_3 = map.get("int_y_id1");
                int_n_id146_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no7:
                map = MethodSupport.OnCheckQuisionerRadioKesValuekhususno7(
                        option_grup_dk_ppno7, option_grup_dk_ttno7, option_grup_dk_tt1no7, option_grup_dk_tt2no7, option_grup_dk_tt3no7,
                        int_y_id146_4, int_n_id146_4,
                        checkedId, iv_circle_data_kesehatan7_qu, getContext(), essay_dk_no7a, essay_dk_no7b, essay_dk_no7c, essay_dk_no7d, iv_circle_data_kesehatan7a_qu,
                        iv_circle_data_kesehatan7b_qu, iv_circle_data_kesehatan7c_qu, iv_circle_data_kesehatan7d_qu);
                int_y_id146_4 = map.get("int_y_id1");
                int_n_id146_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no7:
                map = MethodSupport.OnCheckQuisionerRadioKesValuekhususno7(
                        option_grup_dk_ppno7, option_grup_dk_ttno7, option_grup_dk_tt1no7, option_grup_dk_tt2no7, option_grup_dk_tt3no7,
                        int_y_id146_5, int_n_id146_5,
                        checkedId, iv_circle_data_kesehatan7_qu, getContext(), essay_dk_no7a, essay_dk_no7b, essay_dk_no7c, essay_dk_no7d, iv_circle_data_kesehatan7a_qu,
                        iv_circle_data_kesehatan7b_qu, iv_circle_data_kesehatan7c_qu, iv_circle_data_kesehatan7d_qu);
                int_y_id146_5 = map.get("int_y_id1");
                int_n_id146_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno8a:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno8a, option_grup_dk_ttno8a, option_grup_dk_tt1no8a, option_grup_dk_tt2no8a, option_grup_dk_tt3no8a, essay_dk_no8a,
                        int_y_id152_1, int_n_id152_1,
                        checkedId, iv_circle_data_kesehatan8_qu, getContext());
                int_y_id152_1 = map.get("int_y_id1");
                int_n_id152_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno8a:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno8a, option_grup_dk_ttno8a, option_grup_dk_tt1no8a, option_grup_dk_tt2no8a, option_grup_dk_tt3no8a, essay_dk_no8a,
                        int_y_id152_2, int_n_id152_2,
                        checkedId, iv_circle_data_kesehatan8_qu, getContext());
                int_y_id152_2 = map.get("int_y_id1");
                int_n_id152_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no8a:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno8a, option_grup_dk_ttno8a, option_grup_dk_tt1no8a, option_grup_dk_tt2no8a, option_grup_dk_tt3no8a, essay_dk_no8a,
                        int_y_id152_3, int_n_id152_3,
                        checkedId, iv_circle_data_kesehatan8_qu, getContext());
                int_y_id152_3 = map.get("int_y_id1");
                int_n_id152_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no8a:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno8a, option_grup_dk_ttno8a, option_grup_dk_tt1no8a, option_grup_dk_tt2no8a, option_grup_dk_tt3no8a, essay_dk_no8a,
                        int_y_id152_4, int_n_id152_4,
                        checkedId, iv_circle_data_kesehatan8_qu, getContext());
                int_y_id152_4 = map.get("int_y_id1");
                int_n_id152_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no8a:
                map = MethodSupport.OnCheckQuisionerRadioKesValueText(
                        option_grup_dk_ppno8a, option_grup_dk_ttno8a, option_grup_dk_tt1no8a, option_grup_dk_tt2no8a, option_grup_dk_tt3no8a, essay_dk_no8a,
                        int_y_id152_5, int_n_id152_5,
                        checkedId, iv_circle_data_kesehatan8_qu, getContext());
                int_y_id152_5 = map.get("int_y_id1");
                int_n_id152_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno8b:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno8b, option_grup_dk_ttno8b, option_grup_dk_tt1no8b, option_grup_dk_tt2no8b, option_grup_dk_tt3no8b,
                        int_y_id153_1, int_n_id153_1,
                        checkedId, iv_circle_data_kesehatan8b_qu, getContext());
                int_y_id153_1 = map.get("int_y_id1");
                int_n_id153_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno8b:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno8b, option_grup_dk_ttno8b, option_grup_dk_tt1no8b, option_grup_dk_tt2no8b, option_grup_dk_tt3no8b,
                        int_y_id153_2, int_n_id153_2,
                        checkedId, iv_circle_data_kesehatan8b_qu, getContext());
                int_y_id153_2 = map.get("int_y_id1");
                int_n_id153_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no8b:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno8b, option_grup_dk_ttno8b, option_grup_dk_tt1no8b, option_grup_dk_tt2no8b, option_grup_dk_tt3no8b,
                        int_y_id153_3, int_n_id153_3,
                        checkedId, iv_circle_data_kesehatan8b_qu, getContext());
                int_y_id153_3 = map.get("int_y_id1");
                int_n_id153_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no8b:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno8b, option_grup_dk_ttno8b, option_grup_dk_tt1no8b, option_grup_dk_tt2no8b, option_grup_dk_tt3no8b,
                        int_y_id153_4, int_n_id153_4,
                        checkedId, iv_circle_data_kesehatan8b_qu, getContext());
                int_y_id153_4 = map.get("int_y_id1");
                int_n_id153_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no8b:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno8b, option_grup_dk_ttno8b, option_grup_dk_tt1no8b, option_grup_dk_tt2no8b, option_grup_dk_tt3no8b,
                        int_y_id153_5, int_n_id153_5,
                        checkedId, iv_circle_data_kesehatan8b_qu, getContext());
                int_y_id153_5 = map.get("int_y_id1");
                int_n_id153_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno8c:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno8c, option_grup_dk_ttno8c, option_grup_dk_tt1no8c, option_grup_dk_tt2no8c, option_grup_dk_tt3no8c,
                        int_y_id154_1, int_n_id154_1,
                        checkedId, iv_circle_data_kesehatan8c_qu, getContext());
                int_y_id154_1 = map.get("int_y_id1");
                int_n_id154_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ttno8c:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno8c, option_grup_dk_ttno8c, option_grup_dk_tt1no8c, option_grup_dk_tt2no8c, option_grup_dk_tt3no8c,
                        int_y_id154_2, int_n_id154_2,
                        checkedId, iv_circle_data_kesehatan8c_qu, getContext());
                int_y_id154_2 = map.get("int_y_id1");
                int_n_id154_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt1no8c:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno8c, option_grup_dk_ttno8c, option_grup_dk_tt1no8c, option_grup_dk_tt2no8c, option_grup_dk_tt3no8c,
                        int_y_id154_3, int_n_id154_3,
                        checkedId, iv_circle_data_kesehatan8c_qu, getContext());
                int_y_id154_3 = map.get("int_y_id1");
                int_n_id154_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no8c:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno8c, option_grup_dk_ttno8c, option_grup_dk_tt1no8c, option_grup_dk_tt2no8c, option_grup_dk_tt3no8c,
                        int_y_id154_4, int_n_id154_4,
                        checkedId, iv_circle_data_kesehatan8c_qu, getContext());
                int_y_id154_4 = map.get("int_y_id1");
                int_n_id154_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no8c:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno8c, option_grup_dk_ttno8c, option_grup_dk_tt1no8c, option_grup_dk_tt2no8c, option_grup_dk_tt3no8c,
                        int_y_id154_5, int_n_id154_5,
                        checkedId, iv_circle_data_kesehatan8c_qu, getContext());
                int_y_id154_5 = map.get("int_y_id1");
                int_n_id154_5 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_ppno9:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno9, option_grup_dk_ttno9, option_grup_dk_tt1no9, option_grup_dk_tt2no9, option_grup_dk_tt3no9,
                        int_y_id155_1, int_n_id155_1,
                        checkedId, iv_circle_data_kesehatan9_qu, getContext());
                int_y_id155_1 = map.get("int_y_id1");
                int_n_id155_1 = map.get("int_n_id1");
//                if (option_y_dk_ppno9.isChecked()) {
//                    MethodSupport.active_view(1, img_tambah_data_calon_pemegang_polis_qu);
//                    cl_form_tambah_data_calon_pemegang_polis_qu.setVisibility(View.VISIBLE);
//                    MethodSupport.onFocusviewtest(scroll_qu, cl_form_tambah_data_calon_pemegang_polis_qu, ll_tambah_data_calon_pemegang_polis_qu);
//                } else {
//                    MethodSupport.active_view(0, img_tambah_data_calon_pemegang_polis_qu);
//                    cl_form_tambah_data_calon_pemegang_polis_qu.setVisibility(View.GONE);
//                    count_dk_9pp = 0;
//                    E_Delete delete = new E_Delete(getActivity());
//                    delete.deleteListDK9pp(me.getModelID().getSPAJ_ID_TAB());
//                    List_dkno9pp.clear();
//                    ll_tambah_data_calon_pemegang_polis_qu.removeAllViews();
//                }
                break;
            case R.id.option_grup_dk_ttno9:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno9, option_grup_dk_ttno9, option_grup_dk_tt1no9, option_grup_dk_tt2no9, option_grup_dk_tt3no9,
                        int_y_id155_2, int_n_id155_2,
                        checkedId, iv_circle_data_kesehatan9_qu, getContext());
                int_y_id155_2 = map.get("int_y_id1");
                int_n_id155_2 = map.get("int_n_id1");
//                if (option_y_dk_ttno9.isChecked()) {
//                    MethodSupport.active_view(1, img_tambah_data_calon_tertanggung_qu);
//                    cl_form_tambah_data_calon_tertanggung_qu.setVisibility(View.VISIBLE);
//                    MethodSupport.onFocusviewtest(scroll_qu, cl_form_tambah_data_calon_tertanggung_qu, ll_tambah_data_calon_tertanggung_qu);
//                } else {
//                    MethodSupport.active_view(0, img_tambah_data_calon_tertanggung_qu);
//                    cl_form_tambah_data_calon_tertanggung_qu.setVisibility(View.GONE);
//                    count_dk_9tt = 0;
//                    E_Delete delete = new E_Delete(getActivity());
//                    delete.deleteListDK9tt(me.getModelID().getSPAJ_ID_TAB());
//                    List_dkno9tt.clear();
//                    ll_tambah_data_calon_tertanggung_qu.removeAllViews();
//                }
                break;
            case R.id.option_grup_dk_tt1no9:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno9, option_grup_dk_ttno9, option_grup_dk_tt1no9, option_grup_dk_tt2no9, option_grup_dk_tt3no9,
                        int_y_id155_3, int_n_id155_3,
                        checkedId, iv_circle_data_kesehatan9_qu, getContext());
                int_y_id155_3 = map.get("int_y_id1");
                int_n_id155_3 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt2no9:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno9, option_grup_dk_ttno9, option_grup_dk_tt1no9, option_grup_dk_tt2no9, option_grup_dk_tt3no9,
                        int_y_id155_4, int_n_id155_4,
                        checkedId, iv_circle_data_kesehatan9_qu, getContext());
                int_y_id155_4 = map.get("int_y_id1");
                int_n_id155_4 = map.get("int_n_id1");
                break;
            case R.id.option_grup_dk_tt3no9:
                map = MethodSupport.OnCheckQuisionerRadioKesValue(
                        option_grup_dk_ppno9, option_grup_dk_ttno9, option_grup_dk_tt1no9, option_grup_dk_tt2no9, option_grup_dk_tt3no9,
                        int_y_id155_5, int_n_id155_5,
                        checkedId, iv_circle_data_kesehatan9_qu, getContext());
                int_y_id155_5 = map.get("int_y_id1");
                int_n_id155_5 = map.get("int_n_id1");
                break;
        }
    }

    private void onClickLanjut() {
        if (layout_vis_tt.isShown()) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setTitle("Melakukan perpindahan halaman");
//            progressDialog.setMessage("Mohon Menunggu...");
//            progressDialog.show();
//            progressDialog.setCancelable(false);

            MyTask1 task = new MyTask1();
            task.execute();
//            Runnable progressRunnable = new Runnable() {
//                @Override
//                public void run() {
//                    MethodSupport.active_view(0, btn_lanjut);
//                    MethodSupport.active_view(0, iv_next);
//                    View cl_child_qu = getActivity().findViewById(R.id.cl_child_qu);
//                    File file_bitmap = new File(getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//                    if (!file_bitmap.exists()) {
//                        file_bitmap.mkdirs();
//                    }
//                    String fileName = "QU_HAL1_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
//                    Context context = getActivity();
//                    MethodSupport.onCreateBitmap(cl_child_qu, scroll_qu, file_bitmap, fileName, context);
//
//                    scroll_qu.scrollTo(0, 0);
//                    layout_vis_tt.setVisibility(View.GONE);
//                    cl_data_calon_pp_qu.setVisibility(View.VISIBLE);
//                    cl_data_kesehatan_qu.setVisibility(View.GONE);
//                    layout_vis_dk_2.setVisibility(View.GONE);
//                    MethodSupport.active_view(1, btn_lanjut);
//                    MethodSupport.active_view(1, iv_next);
//                    progressDialog.cancel();
//                }
//            };
//
//            Handler pdCanceller = new Handler();
//            pdCanceller.postDelayed(progressRunnable, 1000);
        } else if (cl_data_calon_pp_qu.isShown()) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setTitle("Melakukan perpindahan halaman");
//            progressDialog.setMessage("Mohon Menunggu...");
//            progressDialog.show();
//            progressDialog.setCancelable(false);

            MyTask2 task2 = new MyTask2();
            task2.execute();

//            Runnable progressRunnable = new Runnable() {
//                @Override
//                public void run() {
////                    MethodSupport.active_view(0, btn_lanjut);
////                    MethodSupport.active_view(0, iv_next);
//                    View cl_child_qu = getActivity().findViewById(R.id.cl_child_qu);
//                    File file_bitmap = new File(getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//                    if (!file_bitmap.exists()) {
//                        file_bitmap.mkdirs();
//                    }
//                    String fileName = "QU_HAL2_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
//                    Context context = getActivity();
//                    MethodSupport.onCreateBitmap(cl_child_qu, scroll_qu, file_bitmap, fileName, context);
//
//                    scroll_qu.scrollTo(0, 0);
//                    layout_vis_tt.setVisibility(View.GONE);
//                    cl_data_calon_pp_qu.setVisibility(View.GONE);
//                    cl_data_kesehatan_qu.setVisibility(View.VISIBLE);
//                    layout_vis_dk_2.setVisibility(View.GONE);
////                    MethodSupport.active_view(1, btn_lanjut);
////                    MethodSupport.active_view(1, iv_next);
////                    progressDialog.cancel();
//                }
//            };
//
//            Handler pdCanceller = new Handler();
//            pdCanceller.postDelayed(progressRunnable, 1000);
        } else if (cl_data_kesehatan_qu.isShown()) {

//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setTitle("Melakukan perpindahan halaman");
//            progressDialog.setMessage("Mohon Menunggu...");
//            progressDialog.show();
//            progressDialog.setCancelable(false);
            validateTask3();

//            Runnable progressRunnable = new Runnable() {
//                @Override
//                public void run() {
//                    MethodSupport.active_view(0, btn_lanjut);
//                    MethodSupport.active_view(0, iv_next);
//                    View cl_child_qu = getActivity().findViewById(R.id.cl_child_qu);
//                    File file_bitmap = new File(getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//                    if (!file_bitmap.exists()) {
//                        file_bitmap.mkdirs();
//                    }
//                    String fileName = "QU_HAL3_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
//                    Context context = getActivity();
//                    MethodSupport.onCreateBitmap(cl_child_qu, scroll_qu, file_bitmap, fileName, context);
//                    scroll_qu.scrollTo(0, 0);
//                    layout_vis_tt.setVisibility(View.GONE);
//                    cl_data_calon_pp_qu.setVisibility(View.GONE);
//                    cl_data_kesehatan_qu.setVisibility(View.GONE);
//                    layout_vis_dk_2.setVisibility(View.VISIBLE);
//                    MethodSupport.active_view(1, btn_lanjut);
//                    MethodSupport.active_view(1, iv_next);
//
//                    progressDialog.cancel();
//                }
//            };
//
//            Handler pdCanceller = new Handler();
//            pdCanceller.postDelayed(progressRunnable, 1000);
        } else if (layout_vis_dk_2.isShown()) {

//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setTitle("Melakukan perpindahan halaman");
//            progressDialog.setMessage("Mohon Menunggu...");
//            progressDialog.show();
//            progressDialog.setCancelable(false);
            if (ll_tambah_data_calon_tertanggung_qu.getChildCount() > 0) {
                AddModel_dkno9tt(count_dk_9tt);
            }
            if (ll_tambah_data_calon_pemegang_polis_qu.getChildCount() > 0) {
                AddModel_dkno9pp(count_dk_9pp);
            }
            validateTask4();
//            Runnable progressRunnable = new Runnable() {
//                @Override
//                public void run() {
//                    MethodSupport.active_view(0, btn_lanjut);
//                    MethodSupport.active_view(0, iv_next);
//                    ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
//
//                    loadview();
//                    ((E_MainActivity) getActivity()).onSave();
//                    View cl_child_qu = getActivity().findViewById(R.id.cl_child_qu);
//                    File file_bitmap = new File(
//                            getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//                    if (!file_bitmap.exists()) {
//                        file_bitmap.mkdirs();
//                    }
//                    String fileName = "QU_HAL4_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
//                    Context context = getActivity();
//                    MethodSupport.onCreateBitmap(cl_child_qu, scroll_qu, file_bitmap, fileName, context);
//                    if (new E_Select(getContext()).selectLinkNonLink(me.getUsulanAsuransiModel().getKd_produk_ua()) == 17) {
//                        ((E_MainActivity) getActivity()).setFragment(new E_ProfileResikoFragment(), getResources().getString(R.string.profil_nasabah));
//                    } else {
//                        Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
//                    }

//                    progressDialog.cancel();
//                }
//            };
//
//            Handler pdCanceller = new Handler();
//            pdCanceller.postDelayed(progressRunnable, 1000);
        }
    }

    private class MyTask1 extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            View cl_child_qu = getActivity().findViewById(R.id.cl_child_qu);
            File file_bitmap = new File(getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
            if (!file_bitmap.exists()) {
                file_bitmap.mkdirs();
            }
            String fileName = "QU_HAL1_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
            Context context = getActivity();
//            MethodSupport.onCreateBitmap(cl_child_qu, scroll_qu, file_bitmap, fileName, context);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            scroll_qu.scrollTo(0, 0);
            layout_vis_tt.setVisibility(View.GONE);
            cl_data_calon_pp_qu.setVisibility(View.VISIBLE);
            cl_data_kesehatan_qu.setVisibility(View.GONE);
            layout_vis_dk_2.setVisibility(View.GONE);
            ProgressDialogClass.removeSimpleProgressDialog();
        }
    }

    private class MyTask2 extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            View cl_child_qu = getActivity().findViewById(R.id.cl_child_qu);
            File file_bitmap = new File(getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
            if (!file_bitmap.exists()) {
                file_bitmap.mkdirs();
            }
            String fileName = "QU_HAL2_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
            Context context = getActivity();
//            MethodSupport.onCreateBitmap(cl_child_qu, scroll_qu, file_bitmap, fileName, context);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            scroll_qu.scrollTo(0, 0);
            layout_vis_tt.setVisibility(View.GONE);
            cl_data_calon_pp_qu.setVisibility(View.GONE);
            cl_data_kesehatan_qu.setVisibility(View.VISIBLE);
            layout_vis_dk_2.setVisibility(View.GONE);
            ProgressDialogClass.removeSimpleProgressDialog();
        }
    }

    private class MyTask3 extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            View cl_child_qu = getActivity().findViewById(R.id.cl_child_qu);
            File file_bitmap = new File(getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
            if (!file_bitmap.exists()) {
                file_bitmap.mkdirs();
            }
            String fileName = "QU_HAL3_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
            Context context = getActivity();
//            MethodSupport.onCreateBitmap(cl_child_qu, scroll_qu, file_bitmap, fileName, context);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            scroll_qu.scrollTo(0, 0);
            layout_vis_tt.setVisibility(View.GONE);
            cl_data_calon_pp_qu.setVisibility(View.GONE);
            cl_data_kesehatan_qu.setVisibility(View.GONE);
            layout_vis_dk_2.setVisibility(View.VISIBLE);
            ProgressDialogClass.removeSimpleProgressDialog();
        }
    }

    private class MyTask4 extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_QUESTIONAIRE);
            View cl_child_qu = getActivity().findViewById(R.id.cl_child_qu);
            File file_bitmap = new File(
                    getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
            if (!file_bitmap.exists()) {
                file_bitmap.mkdirs();
            }
            String fileName = "QU_HAL4_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
            Context context = getActivity();
//            MethodSupport.onCreateBitmap(cl_child_qu, scroll_qu, file_bitmap, fileName, context);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (new E_Select(getContext()).selectLinkNonLink(me.getUsulanAsuransiModel().getKd_produk_ua()) == 17) {
                ((E_MainActivity) getActivity()).setFragment(new E_ProfileResikoFragment(), getResources().getString(R.string.profil_nasabah));
            } else {
                Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
            }
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    private class MyTaskBack extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_QUESTIONAIRE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ((E_MainActivity) getActivity()).setFragment(new E_DokumenPendukungFragment(), getResources().getString(R.string.dokumen_pendukung));
        }
    }

    private void onClickBack() {

        loadview();
        if (ll_tambah_data_calon_tertanggung_qu.getChildCount() > 0) {
            AddModel_dkno9tt(count_dk_9tt);
        }
        if (ll_tambah_data_calon_pemegang_polis_qu.getChildCount() > 0) {
            AddModel_dkno9pp(count_dk_9pp);
        }
        MyTaskBack taskBack = new MyTaskBack();
        taskBack.execute();

//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setTitle("Melakukan perpindahan halaman");
//        progressDialog.setMessage("Mohon Menunggu...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
//        Runnable progressRunnable = new Runnable() {
//
//            @Override
//            public void run() {
//        MethodSupport.active_view(0, btn_kembali);
//        MethodSupport.active_view(0, iv_prev);
//        ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), false);
//
//        loadview();
//        ((E_MainActivity) getActivity()).onSave();
//        ((E_MainActivity) getActivity()).setFragment(new E_DokumenPendukungFragment(), getResources().getString(R.string.dokumen_pendukung));
//                progressDialog.cancel();
//            }
//        };
//
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 1000);
    }

    private ProgressDialog dialogmessage;

    public void startProgress() {
        dialogmessage = new ProgressDialog(getActivity());
        dialogmessage.setMessage("Mohon Menunggu ");
        dialogmessage.setTitle("Sedang Melakukan Perpindahan Halaman ...");
        dialogmessage.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialogmessage.show();
        dialogmessage.setCancelable(false);
        btn_lanjut.setEnabled(false);
        iv_next.setEnabled(false);
        iv_prev.setEnabled(false);
        btn_kembali.setEnabled(false);
        dialogmessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                btn_lanjut.setEnabled(true);
                iv_next.setEnabled(true);
                iv_prev.setEnabled(true);
                btn_kembali.setEnabled(true);
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (dialogmessage.getProgress() <= dialogmessage
                            .getMax()) {
                        Thread.sleep(10);
                        handle.sendMessage(handle.obtainMessage());
                        if (dialogmessage.getProgress() == dialogmessage
                                .getMax()) {
                            dialogmessage.dismiss();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    Handler handle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            dialogmessage.incrementProgressBy(1);
        }
    };

    private void loadview() {
        int QUESTION_ID = 0;
        int OPTION_ORDER = 0;
        int OPTION_GROUP = 0;
        if (!list.isEmpty()) {
            me.getModelUpdateQuisioner().getListQuisioner().clear();
            list.clear();

        }
        for (int i = 0; i < ListOfView.size(); i++) {
            int QUESTION_TYPE_ID = 0;
            int OPTION_TYPE = 0;
            String ANSWER = "";
            String QUESTION_VALID_DATE = "";
            String strres_1 = "";
            String substrres_1 = "";
            String strview = ListOfView.get(i).toString();//untuk dapatkan jenis viewnya
            Resources res = getResources();//untuk dapatkan nama resource
            int id = ListOfView.get(i).getId();
            String strres = res.getResourceName(id);
            String substrres = strres.substring(strres.lastIndexOf("no"));

            if (i != 0) {
                int id1 = ListOfView.get(i - 1).getId();
                strres_1 = res.getResourceName(id1);
                substrres_1 = strres_1.substring(strres_1.lastIndexOf("no"));
            }
            ModelMst_Answer msa = null;

            if (!(substrres).equals(substrres_1)) {
                QUESTION_ID++;
            }

            if (strres.contains("_tt_")) {
                QUESTION_TYPE_ID = 2;
            } else if (strres.contains("_pp_")) {
                QUESTION_TYPE_ID = 1;
            } else {
                QUESTION_TYPE_ID = 3;
                if (QUESTION_ID < 106) {
                    QUESTION_ID = 106;
                } else if (QUESTION_ID == 138) {
                    QUESTION_ID = 139;
                } else if (QUESTION_ID == 151) {
                    QUESTION_ID = 152;
                }
            }

            if (QUESTION_TYPE_ID == 3) {
                QUESTION_VALID_DATE = "01/09/2015";
            } else {
                QUESTION_VALID_DATE = "01/08/2014";
            }
            if (strview.toUpperCase().contains("EDITTEXT")) {
                OPTION_GROUP = 0;
                OPTION_ORDER = 1;
                OPTION_TYPE = 0;
                ANSWER = ((EditText) ListOfView.get(i)).getText().toString();
                if (strres.contains("_cm_") || strres.contains("_1_") | strres.contains("_rkk_")) {
                    OPTION_ORDER = 1;
                    OPTION_TYPE = 4;
                } else if (strres.contains("_kg_") || strres.contains("_2_")) {
                    OPTION_ORDER = 2;
                    OPTION_TYPE = 4;
                }
                msa = new ModelMst_Answer(i, QUESTION_VALID_DATE, QUESTION_TYPE_ID, QUESTION_ID,
                        OPTION_TYPE, OPTION_GROUP, OPTION_ORDER, ANSWER);
            }

            if (strview.toUpperCase().contains("RADIOBUTTON")) {
                OPTION_GROUP = 1;
                OPTION_TYPE = 1;
                if (strres.contains("_y_")) {
                    OPTION_ORDER = 1;
                } else {
                    OPTION_ORDER = 2;
                }

                if (strres.contains("_dk_ppno")) {
                    OPTION_GROUP = 1;
                } else if (strres.contains("_dk_ttno")) {
                    OPTION_GROUP = 2;
                } else if (strres.contains("_dk_tt1no")) {
                    OPTION_GROUP = 3;
                } else if (strres.contains("_dk_tt2no")) {
                    OPTION_GROUP = 4;
                } else if (strres.contains("_dk_tt3no")) {
                    OPTION_GROUP = 5;
                }

                if (((RadioButton) ListOfView.get(i)).isChecked()) {
                    ANSWER = "1";
                } else {
                    ANSWER = "0";
                }
                msa = new ModelMst_Answer(i, QUESTION_VALID_DATE, QUESTION_TYPE_ID, QUESTION_ID,
                        OPTION_TYPE, OPTION_GROUP, OPTION_ORDER, ANSWER);
            }

            list.add(msa);
        }

        System.out.println(list.size() + "&" + ListOfView.size());
        me.getModelUpdateQuisioner().setListQuisioner(list);
        me.getModelUpdateQuisioner().setValidation(validation());
        me.getModelID().setVal_qu(validation());
        E_MainActivity.e_bundle.putParcelable(getString(R.string.modelespaj), me);

    }

    private void restore() {
        IntCounter = 0;
        count_tt3 = 0;
        count_tt5 = 0;
        count_dk_9pp = 0;
        count_dk_9tt = 0;
        count_ketkes = 0;
        try {

        ArrayList<ModelAskesUA> ListAskesUA = me.getListASkesUA();
        List<String> List_DB = new ArrayList<String>();
        if (!ListAskesUA.isEmpty()) {
            for (int i = 0; i < ListAskesUA.size(); i++) {
                String peserta = new E_Select(getContext()).getSubPesertaI(ListAskesUA.get(i).getKode_produk_rider(),
                        ListAskesUA.get(i).getKode_subproduk_rider());
                List_DB.add(peserta.substring(peserta.lastIndexOf("(") + 1, peserta.lastIndexOf(")")));
            }
            if (MethodSupport.isExistStringInArray(List_DB, "TERTANGGUNG IV")){
                for (int i = 0; i < ListOfRadioGrupDK.size(); i++) {
                    Resources res = getContext().getResources();
                    int id = ListOfRadioGrupDK.get(i).getId();
//                    String strres = res.getResourceName(id);
//                    if (strres.contains("_dk_tt3no")) {
//                        ((RadioGroup) ListOfRadioGrupDK.get(i)).setVisibility(View.GONE);
//                    }

                }
            } else if (MethodSupport.isExistStringInArray(List_DB, "TERTANGGUNG III")) {
                for (int i = 0; i < ListOfRadioGrupDK.size(); i++) {
                    Resources res = getContext().getResources();
                    int id = ListOfRadioGrupDK.get(i).getId();
                    String strres = res.getResourceName(id);
                    if (strres.contains("_dk_tt3no")) {
                        ListOfRadioGrupDK.get(i).setVisibility(View.GONE);
                    }


                }
            } else if (MethodSupport.isExistStringInArray(List_DB, "TERTANGGUNG II")) {
                for (int i = 0; i < ListOfRadioGrupDK.size(); i++) {
                    Resources res = getContext().getResources();
                    int id = ListOfRadioGrupDK.get(i).getId();
                    String strres = res.getResourceName(id);

                    if (strres.contains("_dk_tt2no")) {
                        ListOfRadioGrupDK.get(i).setVisibility(View.GONE);
                    } else if (strres.contains("_dk_tt3no")) {
                        ListOfRadioGrupDK.get(i).setVisibility(View.GONE);
                    }

                }
            } else if (MethodSupport.isExistStringInArray(List_DB, "TERTANGGUNG I")){
                for (int i = 0; i < ListOfRadioGrupDK.size(); i++) {
                    Resources res = getContext().getResources();
                    int id = ListOfRadioGrupDK.get(i).getId();
                    String strres = res.getResourceName(id);

                    if (me.getPemegangPolisModel().getHubungan_pp() == 1) {

                        if (strres.contains("_dk_ttno")) {
                            ListOfRadioGrupDK.get(i).setVisibility(View.GONE);
                        }

                    }
                    if (strres.contains("_dk_tt1no")) {
                        ListOfRadioGrupDK.get(i).setVisibility(View.GONE);
                    } else if (strres.contains("_dk_tt2no")) {
                        ListOfRadioGrupDK.get(i).setVisibility(View.GONE);
                    } else if (strres.contains("_dk_tt3no")) {
                        ListOfRadioGrupDK.get(i).setVisibility(View.GONE);
                    }
                }
            }
        } else {
            for (int i = 0; i < ListOfRadioGrupDK.size(); i++) {
                Resources res = getContext().getResources();
                int id = ListOfRadioGrupDK.get(i).getId();
                String strres = res.getResourceName(id);

                if (me.getPemegangPolisModel().getHubungan_pp() == 1) {

                    if (strres.contains("_dk_ttno")) {
                        ListOfRadioGrupDK.get(i).setVisibility(View.GONE);
                    }

                }
                if (strres.contains("_dk_tt1no")) {
                    ListOfRadioGrupDK.get(i).setVisibility(View.GONE);
                } else if (strres.contains("_dk_tt2no")) {
                    ListOfRadioGrupDK.get(i).setVisibility(View.GONE);
                } else if (strres.contains("_dk_tt3no")) {
                    ListOfRadioGrupDK.get(i).setVisibility(View.GONE);
                }
            }
        }

        list = me.getModelUpdateQuisioner().getListQuisioner();
        for (int i = 0; i < ListOfView.size(); i++) {
            Resources res = getContext().getResources();//untuk dapatkan nama resource
            int id = ListOfView.get(i).getId();
            String strres = res.getResourceName(id);
            String strview = ListOfView.get(i).toString();
            if (!list.isEmpty()) {
                if (strview.toUpperCase().contains("EDITTEXT")) {
                    ((EditText) ListOfView.get(i)).setText(list.get(i).getAnswer());
                    System.out.println(((EditText) ListOfView.get(0)).getText().toString());
                }
                if (strview.toUpperCase().contains("RADIOBUTTON")) {
                    if (list.get(i).getAnswer().equals("1")) {
                        ((RadioButton) ListOfView.get(i)).setChecked(true);
                    } else {
                        ((RadioButton) ListOfView.get(i)).setChecked(false);
                    }
                }
            } else {
                if (strview.toUpperCase().contains("EDITTEXT")) {
                    ((EditText) ListOfView.get(i)).setText("");
                    System.out.println(((EditText) ListOfView.get(0)).getText().toString());
                }
                if (strview.toUpperCase().contains("RADIOBUTTON")) {
                    if (strres.contains("option_n_")) {
                        ((RadioButton) ListOfView.get(i)).setChecked(false);
                    } else {
                        ((RadioButton) ListOfView.get(i)).setChecked(false);
                    }
                }
            }
        }

        List_ttno3 = me.getModelUpdateQuisioner().getList_ttno3();
        for (int i = 0; i < List_ttno3.size(); i++) {
            count_tt3++;
            inflatett_no3(count_tt3, ((ModelTT_NO3UQ) List_ttno3.get(i)).getNama_perushn(),
                    ((ModelTT_NO3UQ) List_ttno3.get(i)).getSpin_produk(),
                    ((ModelTT_NO3UQ) List_ttno3.get(i)).getUp(),
                    ((ModelTT_NO3UQ) List_ttno3.get(i)).getTgl_polis());
        }

        List_ttno5 = me.getModelUpdateQuisioner().getList_ttno5();
        for (int i = 0; i < List_ttno5.size(); i++) {
            count_tt5++;
            inflatett_no5(count_tt5, ((ModelTT_NO5UQ) List_ttno5.get(i)).getNama_perushn(),
                    ((ModelTT_NO5UQ) List_ttno5.get(i)).getSpin_produk(),
                    ((ModelTT_NO5UQ) List_ttno5.get(i)).getUp(),
                    ((ModelTT_NO5UQ) List_ttno5.get(i)).getTgl_polis(),
                    ((ModelTT_NO5UQ) List_ttno5.get(i)).getSpin_klaim());
        }

        List_dkno9tt = me.getModelUpdateQuisioner().getList_dkno9tt();
        for (int i = 0; i < List_dkno9tt.size(); i++) {
            count_dk_9tt++;
            inflatedk_no9tt(count_dk_9tt, List_dkno9tt.get(i).getSpin_ct(),
                    List_dkno9tt.get(i).getId_keadaan(),
                    List_dkno9tt.get(i).getUmur(),
                    List_dkno9tt.get(i).getPenyebab());
        }

        List_dkno9pp = me.getModelUpdateQuisioner().getList_dkno9pp();
        for (int i = 0; i < List_dkno9pp.size(); i++) {
            count_dk_9pp++;
            inflatedk_no9pp(count_dk_9pp, List_dkno9pp.get(i).getSpin_cp(),
                    List_dkno9pp.get(i).getId_keadaan(),
                    List_dkno9pp.get(i).getUmur(),
                    List_dkno9pp.get(i).getPenyebab());
        }

        List_dkketkes = me.getModelUpdateQuisioner().getList_dkketkes();
        for (int i = 0; i < List_dkketkes.size(); i++) {
            count_ketkes++;
            inflatedk_ketkes(count_ketkes, ((ModelDK_KetKesUQ) List_dkketkes.get(i)).getKet());
        }
        if (!me.getValidation()) {
            validation();
        }
        if (me.getModelID().getFlag_aktif() == 1) {
            MethodSupport.disable(false, cl_child_qu);
        }
        } catch (IndexOutOfBoundsException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }

    }

    public int getIntCount() {
        return IntCounter;
    }

    private void tanggal() {
        Calendar minCal = Calendar.getInstance();
        year = minCal.get(Calendar.YEAR);
        month = minCal.get(Calendar.MONTH);
        day = minCal.get(Calendar.DAY_OF_MONTH);

    }

    private void Alert() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("DATA BELUM LENGKAP!!!");
        dialog.setMessage("Mohon Periksa Kembali, sebelum lanjut kehalaman berikutnya.\n\n\n");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void AlertQuisioner() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("DATA KUISIONER BELUM LENGKAP!!!");
        dialog.setMessage("Mohon Periksa Kembali, sebelum mengirimkan data. \n\n\n");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    //inflate tt no 3
    private void inflatett_no3(final int no, String nama, int spin, String up, String tgl) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dynamicview = inflater.inflate(R.layout.adapter_tt_no3, null);
        //deklarasi dan set Default
//        no_tt_no3[no]=(TextView)dynamicview.findViewById(R.id.no_tt_no3);
//        no_tt_no3[no].setText(String.valueOf(no));
        nm_pershn_tt_no3[no] = dynamicview.findViewById(R.id.et_nama_perusahaan_tt_no3);
        nm_pershn_tt_no3[no].setText(nama);
        produk_tt_no3[no] = dynamicview.findViewById(R.id.ac_produk_tt_no3);
        produk_tt_no3[no].setSelection(spin);
//        img_produk_tt_no3[no] = (ImageView)dynamicview.findViewById(R.id.img_produk_tt_no3);
        up_tt_no3[no] = dynamicview.findViewById(R.id.et_up_tt_no3);
        up_tt_no3[no].addTextChangedListener(new NumberTextWatcher(up_tt_no3[no]));
        up_tt_no3[no].setText(up);
        tglpolis_tt_no3[no] = dynamicview.findViewById(R.id.et_ttl_penerbitan_polis_tt_no3);
        tglpolis_tt_no3[no].setText(tgl);
//        button_tglpolis_tt_no3[no] = (ImageView)dynamicview.findViewById(R.id.button_tglpolis_tt_no3);
//        delete_adapter_ttno3[no] = (ImageButton)dynamicview.findViewById(R.id.	delete_adapter_ttno3);
//        //setonclick listener
        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                year = selectedYear;
                month = selectedMonth;
                day = selectedDay;
                tglpolis_tt_no3[no].setText(new StringBuilder()
                        .append(day).append("/")
                        .append(month + 1).append("/")
                        .append(year));
            }
        };
//        img_produk_tt_no3[no].setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                produk_tt_no3[no]=(Spinner)dynamicview.findViewById(R.id.produk_tt_no3);
//                produk_tt_no3[no].performClick();
//            }
//        });
//        button_tglpolis_tt_no3[no].setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                tanggal();
//                Calendar minCal = Calendar.getInstance();
//                DatePickerDialog Dialog =  new DatePickerDialog(getActivity(), datePickerListener, year, month,
//                        day);
//                Dialog.getDatePicker().setMaxDate(minCal.getTimeInMillis());
//                Dialog.show();}
//        });
//        delete_adapter_ttno3[no].setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                count_tt3--;
//                parentView_tt_no3.removeView((View) v.getParent());
//                if (count_tt3 != 0 && count_tt3 > 0) {
//                    no_tt_no3[count_tt3].setText(String.valueOf(count_tt3));
//                }
//
//            }
//        });
//
        ll_tambah_data_qu.addView(dynamicview, ll_tambah_data_qu.getChildCount() - 1);
        //counter++;
    }


    //inflate tt no 5
    private void inflatett_no5(final int no, String nama, int spin, String up, String tgl, int spin_klaim) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dynamicview = inflater.inflate(R.layout.adapter_tt_no5, null);
        //deklarasi dan set Default
//        no_tt_no5[no]=(TextView)dynamicview.findViewById(R.id.no_tt_no5);
//        no_tt_no5[no].setText(String.valueOf(no));
        nm_pershn_tt_no5[no] = dynamicview.findViewById(R.id.et_nama_perusahaan_tt_no5);
        nm_pershn_tt_no5[no].setText(nama);
        produk_tt_no5[no] = dynamicview.findViewById(R.id.ac_produk_tt_no5);
        produk_tt_no5[no].setSelection(spin);
//        img_produk_tt_no5[no] = (ImageView)dynamicview.findViewById(R.id.img_produk_tt_no5);
        up_tt_no5[no] = dynamicview.findViewById(R.id.et_up_tt_no5);
        up_tt_no5[no].addTextChangedListener(new NumberTextWatcher(up_tt_no5[no]));
        up_tt_no5[no].setText(up);
        tglpolis_tt_no5[no] = dynamicview.findViewById(R.id.et_ttl_penerbitan_polis_tt_no5);
        tglpolis_tt_no5[no].setText(tgl);
//        button_tglpolis_tt_no5[no] = (ImageView)dynamicview.findViewById(R.id.button_tglpolis_tt_no5);
        klaim_tt_no5[no] = dynamicview.findViewById(R.id.ac_klaim_dibayar_ditolak_tt_no5);
        klaim_tt_no5[no].setSelection(spin_klaim);
//        img_klaim_tt_no5[no] = (ImageView)dynamicview.findViewById(R.id.img_klaim_tt_no5);
//        delete_adapter_ttno5[no] = (ImageButton)dynamicview.findViewById(R.id.delete_adapter_ttno5);
//        //setonclick listener
        final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                year = selectedYear;
                month = selectedMonth;
                day = selectedDay;
                tglpolis_tt_no5[no].setText(new StringBuilder()
                        .append(day).append("/")
                        .append(month + 1).append("/")
                        .append(year));
            }
        };
//        img_produk_tt_no5[no].setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                produk_tt_no5[no]=(Spinner)dynamicview.findViewById(R.id.produk_tt_no5);
//                produk_tt_no5[no].performClick();
//            }
//        });
//        img_klaim_tt_no5[no].setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                klaim_tt_no5[no]=(Spinner)dynamicview.findViewById(R.id.klaim_tt_no5);
//                klaim_tt_no5[no].performClick();
//            }
//        });
//        button_tglpolis_tt_no5[no].setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                tanggal();
//                Calendar minCal = Calendar.getInstance();
//                DatePickerDialog Dialog =  new DatePickerDialog(getActivity(), datePickerListener, year, month,
//                        day);
//                Dialog.getDatePicker().setMaxDate(minCal.getTimeInMillis());
//                Dialog.show();}
//        });
//        delete_adapter_ttno5[no].setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                count_tt5--;
//                parentView_tt_no5.removeView((View) v.getParent());
//                if (count_tt5 != 0 && count_tt5 > 0) {
//                    no_tt_no5[count_tt5].setText(String.valueOf(count_tt5));
//                }
//            }
//        });
//
        ll_tambah_data2_qu.addView(dynamicview, ll_tambah_data2_qu.getChildCount() - 1);
        //counter++;
    }


    //inflate tt no 9tt
    private void inflatedk_no9tt(final int no, int spin, int spin2, String umur, String penyebab) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dynamicview = inflater.inflate(R.layout.adapter_dk_no9, null);
        delete_adapter_dkno9_tt[no]= dynamicview.findViewById(R.id.delete_adapter_dkno9);
        calon_dk_no9_tt[no] = dynamicview.findViewById(R.id.ac_dk_no9);
        calon_dk_no9_tt[no].setSelection(spin);
        img_calon_dk_no9_tt[no]= dynamicview.findViewById(R.id.img_calon_dk_no9);

        umur_dk_no9_tt[no] = dynamicview.findViewById(R.id.et_umur_dk_no9_diagnosa);
        umur_dk_no9_tt[no].setText(umur);

        keadaan_dk_no9_tt[no] = dynamicview.findViewById(R.id.et_keadaan_dk_no9);
        keadaan_dk_no9_tt[no].setSelection(spin2);
        penyebab_dk_no9_tt[no] = dynamicview.findViewById(R.id.et_penyebab_dk_no9);
        penyebab_dk_no9_tt[no].setText(penyebab);

        keadaan_dk_no9_tt[no].setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectKondisi = parent.getItemAtPosition(position).toString();
                if (selectKondisi.equals("Masih Hidup")){
                    dynamicview.findViewById(R.id.TIL1).setVisibility(View.VISIBLE);
                    dynamicview.findViewById(R.id.TIL2).setVisibility(View.GONE);

                    umur_dk_no9_tt[no] = dynamicview.findViewById(R.id.et_umur_dk_no9_diagnosa);
                    umur_dk_no9_tt[no].setHint("Usia saat Terdiagnosa");
                }else{
                    dynamicview.findViewById(R.id.TIL1).setVisibility(View.VISIBLE);
                    dynamicview.findViewById(R.id.TIL2).setVisibility(View.GONE);

                    umur_dk_no9_tt[no] = dynamicview.findViewById(R.id.et_umur_dk_no9_diagnosa);
                    umur_dk_no9_tt[no].setHint("Usia saat Meninggal");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        img_calon_dk_no9_tt[no].setOnClickListener(v -> {
            calon_dk_no9_tt[no]= dynamicview.findViewById(R.id.ac_dk_no9);
            calon_dk_no9_tt[no].performClick();

        });
        delete_adapter_dkno9_tt[no].setOnClickListener(v -> {
            count_dk_9tt = 0;
//            ll_tambah_data_calon_tertanggung_qu.removeView((View) v.getParent());
            E_Delete delete = new E_Delete(getActivity());
            delete.deleteListDK9tt(me.getModelID().getSPAJ_ID_TAB());
            List_dkno9tt.clear();
            ll_tambah_data_calon_tertanggung_qu.removeAllViews();
        });
//
        ll_tambah_data_calon_tertanggung_qu.addView(dynamicview, ll_tambah_data_calon_tertanggung_qu.getChildCount() - 1);
        //counter++;
    }


    //inflate tt no 9pp
    private void inflatedk_no9pp(final int no, int spin, int spin2, String umur, String penyebab) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dynamicview = inflater.inflate(R.layout.adapter_dk_no9, null);
        delete_adapter_dkno9_pp[no]= dynamicview.findViewById(R.id.delete_adapter_dkno9);
        calon_dk_no9_pp[no] = dynamicview.findViewById(R.id.ac_dk_no9);
        calon_dk_no9_pp[no].setSelection(spin);
        img_calon_dk_no9_pp[no]= dynamicview.findViewById(R.id.img_calon_dk_no9);
        umur_dk_no9_pp[no] = dynamicview.findViewById(R.id.et_umur_dk_no9_diagnosa);
        umur_dk_no9_pp[no].setText(umur);
        keadaan_dk_no9_pp[no] = dynamicview.findViewById(R.id.et_keadaan_dk_no9);
        keadaan_dk_no9_pp[no].setSelection(spin2);
        penyebab_dk_no9_pp[no] = dynamicview.findViewById(R.id.et_penyebab_dk_no9);
        penyebab_dk_no9_pp[no].setText(penyebab);

        keadaan_dk_no9_pp[no].setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectKondisi = parent.getItemAtPosition(position).toString();
                if (selectKondisi.equals("Masih Hidup")){
                    dynamicview.findViewById(R.id.TIL1).setVisibility(View.VISIBLE);
                    dynamicview.findViewById(R.id.TIL2).setVisibility(View.GONE);

                    umur_dk_no9_pp[no].setHint("Usia saat Terdiagnosa");
                }else{
                    dynamicview.findViewById(R.id.TIL1).setVisibility(View.VISIBLE);
                    dynamicview.findViewById(R.id.TIL2).setVisibility(View.GONE);

                    umur_dk_no9_pp[no].setHint("Usia saat Meninggal");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        img_calon_dk_no9_pp[no].setOnClickListener(v -> {
            calon_dk_no9_pp[no]= dynamicview.findViewById(R.id.ac_dk_no9);
            calon_dk_no9_pp[no].performClick();

        });
        delete_adapter_dkno9_pp[no].setOnClickListener(v -> {
            count_dk_9pp = 0;
//            ll_tambah_data_calon_pemegang_polis_qu.removeView((View) v.getParent());
            E_Delete delete = new E_Delete(getActivity());
            delete.deleteListDK9pp(me.getModelID().getSPAJ_ID_TAB());
            List_dkno9pp.clear();
            ll_tambah_data_calon_pemegang_polis_qu.removeAllViews();
        });

        ll_tambah_data_calon_pemegang_polis_qu.addView(dynamicview, ll_tambah_data_calon_pemegang_polis_qu.getChildCount() - 1);
        //counter++;
    }


    //inflate dk ketkes
    private void inflatedk_ketkes(final int no, String ket) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dynamicview = inflater.inflate(R.layout.adapter_dk_ketkes, null);
//        delete_adapter_dkketkes[no]=(ImageButton)dynamicview.findViewById(R.id.delete_adapter_dkketkes);
        tv_no_dk_ketkes[no] = dynamicview.findViewById(R.id.tv_no_dk_ketkes);
        tv_no_dk_ketkes[no].setText(String.valueOf(no));
        ket_dk_ketkes[no] = dynamicview.findViewById(R.id.et_ketkes);
        ket_dk_ketkes[no].setText(ket);

//        delete_adapter_dkketkes[no].setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                count_ketkes--;
//                parentView_dk_ketkes.removeView((View) v.getParent());
//                if (count_ketkes != 0 && count_ketkes > 0) {
//                    no_dk_ketkes[count_ketkes].setText(String.valueOf(count_ketkes));
//                }
//            }
//        });
//
        ll_tambah_data_keterangan_kesehatan_qu.addView(dynamicview, ll_tambah_data_keterangan_kesehatan_qu.getChildCount() - 1);
        //counter++;
    }

    private boolean validation() {
        boolean ready = true;
//        boolean val = true;
        int color = ContextCompat.getColor(getContext(), R.color.red);

        //edittext

        if (essay_tt_no1.isEnabled() && essay_tt_no1.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_no1, tv_error_questionnaire1_qu, iv_circle_questionnaire1_isi_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_tt_no1.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_no1, tv_error_questionnaire1_qu, iv_circle_questionnaire1_isi_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_tt_no2.isEnabled() && essay_tt_no2.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_no2, tv_error_questionnaire2_qu, iv_circle_questionnaire2_isi_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_tt_no2.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_no2, tv_error_questionnaire2_qu, iv_circle_questionnaire2_isi_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_tt_no4.isEnabled() && essay_tt_no4.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_no4, tv_error_questionnaire4_qu, iv_circle_questionnaire4_isi_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_tt_no4.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_no4, tv_error_questionnaire4_qu, iv_circle_questionnaire4_isi_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_tt_no6.isEnabled() && essay_tt_no6.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_no6, tv_error_questionnaire6_qu, iv_circle_questionnaire6_isi_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_tt_no6.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_no6, tv_error_questionnaire6_qu, iv_circle_questionnaire6_isi_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_tt_no7.isEnabled() && essay_tt_no7.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_no7, tv_error_questionnaire7_qu, iv_circle_questionnaire7_isi_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_tt_no7.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_no7, tv_error_questionnaire7_qu, iv_circle_questionnaire7_isi_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_tt_1_no8a.isEnabled() && essay_tt_1_no8a.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_1_no8a, val_tt_no8, iv_circle_questionnaire8_isi_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_tt_1_no8a.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_1_no8a, val_tt_no8, iv_circle_questionnaire8_isi_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_tt_2_no8a.isEnabled() && essay_tt_2_no8a.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_2_no8a, val_tt_no8, iv_circle_questionnaire8_isi_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_tt_2_no8a.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_2_no8a, val_tt_no8, iv_circle_questionnaire8_isi_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_tt_cm_no11a.isEnabled() && essay_tt_cm_no11a.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_cm_no11a, tv_error_tinggi_badan_qu, iv_circle_questionnaire11a_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_tt_cm_no11a.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_cm_no11a, tv_error_tinggi_badan_qu, iv_circle_questionnaire11a_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_tt_kg_no11a.isEnabled() && essay_tt_kg_no11a.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_kg_no11a, tv_error_tinggi_badan_qu, iv_circle_questionnaire11a_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_tt_kg_no11a.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_kg_no11a, tv_error_tinggi_badan_qu, iv_circle_questionnaire11a_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_tt_no11b.isEnabled() && essay_tt_no11b.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_no11b, tv_error_tinggi_badan_qu, iv_circle_questionnaire11a_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_tt_no11b.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_no11b, tv_error_tinggi_badan_qu, iv_circle_questionnaire11a_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_tt_cm_no12.isEnabled() && essay_tt_cm_no12.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_cm_no12, tv_error_tinggi_badan12_qu, iv_circle_questionnaire12_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_tt_cm_no12.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_cm_no12, tv_error_tinggi_badan12_qu, iv_circle_questionnaire12_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_tt_kg_no12.isEnabled() && essay_tt_kg_no12.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_kg_no12, tv_error_tinggi_badan12_qu, iv_circle_questionnaire12_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_tt_kg_no12.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_tt_kg_no12, tv_error_tinggi_badan12_qu, iv_circle_questionnaire12_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_pp_1_no13.isEnabled() && essay_pp_1_no13.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_pp_1_no13, tv_error_questionnaire13_qu, iv_circle_questionnaire13_isi_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_pp_1_no13.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_pp_1_no13, tv_error_questionnaire13_qu, iv_circle_questionnaire13_isi_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_pp_2_no13.isEnabled() && essay_pp_2_no13.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_pp_2_no13, tv_error_questionnaire13_qu, iv_circle_questionnaire13_isi_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_pp_2_no13.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_pp_2_no13, tv_error_questionnaire13_qu, iv_circle_questionnaire13_isi_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_pp_1_no14.isEnabled() && essay_pp_1_no14.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_pp_1_no14, tv_error_questionnaire14_qu, iv_circle_questionnaire14_isi_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_pp_1_no14.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_pp_1_no14, tv_error_questionnaire14_qu, iv_circle_questionnaire14_isi_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_pp_2_no14.isEnabled() && essay_pp_2_no14.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_pp_2_no14, tv_error_questionnaire14_qu, iv_circle_questionnaire14_isi_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_pp_2_no14.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_pp_2_no14, tv_error_questionnaire14_qu, iv_circle_questionnaire14_isi_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_pp_cm_no15.isEnabled() && essay_pp_cm_no15.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_pp_cm_no15, tv_error_tinggi_badan2_qu, iv_circle_questionnaire15_isi_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_pp_cm_no15.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_pp_cm_no15, tv_error_tinggi_badan2_qu, iv_circle_questionnaire15_isi_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (essay_pp_kg_no15.isEnabled() && essay_pp_kg_no15.getText().toString().trim().length() == 0) {
            ready = false;
            MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_pp_kg_no15, tv_error_tinggi_badan2_qu, iv_circle_questionnaire15_isi_qu, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(essay_pp_kg_no15.getText().toString())) {
                ready = false;
                MethodSupport.onFocusview(scroll_qu, cl_child_qu, essay_pp_kg_no15, tv_error_tinggi_badan2_qu, iv_circle_questionnaire15_isi_qu, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (!Method_Validator.onEmptyRadioText(option_grup_tt_no1, essay_tt_no1, iv_circle_questionnaire1_isi_qu, tv_error_questionnaire1_qu, cl_questionnaire1_isi_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadioText(option_grup_tt_no2, essay_tt_no2, iv_circle_questionnaire2_isi_qu, tv_error_questionnaire2_qu, cl_questionnaire2_isi_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadioTable("no3tt", option_grup_tt_no3, List_ttno3, List_ttno5, img_tambah_data_qu, iv_circle_questionnaire3_isi_qu, val_tt_no3, cl_questionnaire3_isi_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadioText(option_grup_tt_no4, essay_tt_no4, iv_circle_questionnaire4_isi_qu, tv_error_questionnaire4_qu, cl_questionnaire4_isi_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadioTable("no5tt", option_grup_tt_no5, List_ttno3, List_ttno5, img_tambah_data2_qu, iv_circle_questionnaire5_isi_qu, val_tt_no5, cl_questionnaire5_isi_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadioText(option_grup_tt_no6, essay_tt_no6, iv_circle_questionnaire6_isi_qu, tv_error_questionnaire6_qu, cl_questionnaire6_isi_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadioText(option_grup_tt_no7, essay_tt_no7, iv_circle_questionnaire7_isi_qu, tv_error_questionnaire7_qu, cl_questionnaire7_isi_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadioText2(option_grup_tt_no8, essay_tt_1_no8a, essay_tt_2_no8a, iv_circle_questionnaire8_isi_qu, val_tt_no8, cl_questionnaire8_isi_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_tt_no8b, iv_circle_questionnaire8b_isi_qu, val_tt_no8b, cl_questionnaire8b_isi_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_tt_no9, iv_circle_questionnaire9_isi_qu, val_tt_no9, cl_questionnaire9_isi_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadioText(option_grup_tt_no10, essay_tt_rkk_no10, iv_circle_questionnaire10_isi_qu, tv_error_questionnaire10_qu, cl_questionnaire10_isi_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyText(essay_tt_kg_no11a, essay_tt_cm_no11a, iv_circle_questionnaire11a_qu, tv_error_tinggi_badan_qu, layout_tt_no11a, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadioText(option_grup_tt_no11b, essay_tt_no11b, iv_circle_questionnaire11b_isi_qu, tv_error_questionnaire11b_qu, cl_questionnaire11b_isi_qu, getContext())) {
            ready = false;
        }
        if (Integer.parseInt(me.getTertanggungModel().getUsia_tt().trim()) < 15) {
            if (!Method_Validator.onEmptyText(essay_tt_kg_no12, essay_tt_cm_no12, iv_circle_questionnaire12_qu, tv_error_tinggi_badan12_qu, layout_tt_no12, getContext())) {
                ready = false;
            }
        }
        if (!ready) {
            scroll_qu.scrollTo(0, 0);
            layout_vis_tt.setVisibility(View.VISIBLE);
            cl_data_calon_pp_qu.setVisibility(View.GONE);
            cl_data_kesehatan_qu.setVisibility(View.GONE);
            layout_vis_dk_2.setVisibility(View.GONE);
//            text_indicator_qu.setText(getResources().getString(R.string.hal1));
        } else {
            if (!Method_Validator.onEmptyRadioText2(option_grup_pp_no13, essay_pp_1_no13, essay_pp_2_no13, iv_circle_questionnaire13_isi_qu, tv_error_questionnaire13_qu, layout_pp_no13, getContext())) {
                ready = false;
            }
            if (!Method_Validator.onEmptyRadioText2(option_grup_pp_no14, essay_pp_1_no14, essay_pp_2_no14, iv_circle_questionnaire14_isi_qu, tv_error_questionnaire14_qu, layout_pp_no14, getContext())) {
                ready = false;
            }
            if (!Method_Validator.onEmptyText(essay_pp_kg_no15, essay_pp_cm_no15, iv_circle_questionnaire15_isi_qu, tv_error_tinggi_badan2_qu, layout_pp_no15, getContext())) {
                ready = false;
            }
            if (!ready) {
                scroll_qu.scrollTo(0, 0);
                layout_vis_tt.setVisibility(View.GONE);
                cl_data_calon_pp_qu.setVisibility(View.VISIBLE);
                cl_data_kesehatan_qu.setVisibility(View.GONE);
                layout_vis_dk_2.setVisibility(View.GONE);
//                text_indicator_qu.setText(getResources().getString(R.string.hal2));
            } else {
                if (Method_Validator.onEmptyRadio(option_grup_dk_ppno1l, iv_circle_calon_ppi_qu, val_dk_no1l, cl_form_calon_ppi_qu, getContext())) {
                    radio3i = true;
                }
                if (Method_Validator.onEmptyRadio(option_grup_dk_ttno1l, iv_circle_calon_ppi_qu, val_dk_no1l, cl_form_calon_tti_qu, getContext())) {
                    radio3i = true;
                }
                if (Method_Validator.onEmptyRadio(option_grup_dk_tt1no1l, iv_circle_calon_ppi_qu, val_dk_no1l, cl_form_calon_tt1_i_qu, getContext())) {
                    radio3i = true;
                }
                if (Method_Validator.onEmptyRadio(option_grup_dk_tt2no1l, iv_circle_calon_ppi_qu, val_dk_no1l, cl_form_calon_tt2_i_qu, getContext())) {
                    radio3i = true;
                }
                if (Method_Validator.onEmptyRadio(option_grup_dk_tt3no1l, iv_circle_calon_ppi_qu, val_dk_no1l, cl_form_calon_tt3_i_qu, getContext())) {
                    radio3i = true;
                }//2
                if (Method_Validator.onEmptyRadio(option_grup_dk_ppno1ii, iv_circle_calon_ppii_qu, val_dk_no1ii, cl_form_calon_ppii_qu, getContext())) {
                    radio3ii = true;
                }
                if (Method_Validator.onEmptyRadio(option_grup_dk_ttno1ii, iv_circle_calon_ppii_qu, val_dk_no1ii, cl_form_calon_ttii_qu, getContext())) {
                    radio3ii = true;
                }
                if (Method_Validator.onEmptyRadio(option_grup_dk_tt1no1ii, iv_circle_calon_ppii_qu, val_dk_no1ii, cl_form_calon_tt1_ii_qu, getContext())) {
                    radio3ii = true;
                }
                if (Method_Validator.onEmptyRadio(option_grup_dk_tt2no1ii, iv_circle_calon_ppii_qu, val_dk_no1ii, cl_form_calon_tt2_ii_qu, getContext())) {
                    radio3ii = true;
                }
                if (Method_Validator.onEmptyRadio(option_grup_dk_tt3no1ii, iv_circle_calon_ppii_qu, val_dk_no1ii, cl_form_calon_tt3_ii_qu, getContext())) {
                    radio3ii = true;
                }//3
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1iii, iv_circle_calon_ppiii_qu, val_dk_no1iii, cl_form_calon_ppiii_qu, getContext())) {
                    ready = false;
                    radio3iii = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1iii, iv_circle_calon_ppiii_qu, val_dk_no1iii, cl_form_calon_ttiii_qu, getContext())) {
                    ready = false;
                    radio3iii = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1iii, iv_circle_calon_ppiii_qu, val_dk_no1iii, cl_form_calon_tt1_iii_qu, getContext())) {
                    ready = false;
                    radio3iii = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1iii, iv_circle_calon_ppiii_qu, val_dk_no1iii, cl_form_calon_tt2_iii_qu, getContext())) {
                    ready = false;
                    radio3iii = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1iii, iv_circle_calon_ppiii_qu, val_dk_no1iii, cl_form_calon_tt3_iii_qu, getContext())) {
                    ready = false;
                    radio3iii = true;
                }//4
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1iv, iv_circle_calon_ppiv_qu, val_dk_no1iv, cl_form_calon_ppiv_qu, getContext())) {
                    ready = false;
                    radio3iv = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1iv, iv_circle_calon_ppiv_qu, val_dk_no1iv, cl_form_calon_ttiv_qu, getContext())) {
                    ready = false;
                    radio3iv = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1iv, iv_circle_calon_ppiv_qu, val_dk_no1iv, cl_form_calon_tt1_iv_qu, getContext())) {
                    ready = false;
                    radio3iv = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1iv, iv_circle_calon_ppiv_qu, val_dk_no1iv, cl_form_calon_tt2_iv_qu, getContext())) {
                    ready = false;
                    radio3iv = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1iv, iv_circle_calon_ppiv_qu, val_dk_no1iv, cl_form_calon_tt3_iv_qu, getContext())) {
                    ready = false;
                    radio3iv = true;
                }//5
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1v, iv_circle_calon_ppv_qu, val_dk_no1v, cl_form_calon_ppv_qu, getContext())) {
                    ready = false;
                    radio3v = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1v, iv_circle_calon_ppv_qu, val_dk_no1v, cl_form_calon_ttv_qu, getContext())) {
                    ready = false;
                    radio3v = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1v, iv_circle_calon_ppv_qu, val_dk_no1v, cl_form_calon_tt1_v_qu, getContext())) {
                    ready = false;
                    radio3v = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1v, iv_circle_calon_ppv_qu, val_dk_no1v, cl_form_calon_tt2_v_qu, getContext())) {
                    ready = false;
                    radio3v = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1v, iv_circle_calon_ppv_qu, val_dk_no1v, cl_form_calon_tt3_v_qu, getContext())) {
                    ready = false;
                    radio3v = true;
                }//6
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1vi, iv_circle_calon_ppvi_qu, val_dk_no1vi, cl_form_calon_ppvi_qu, getContext())) {
                    ready = false;
                    radio3vi = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1vi, iv_circle_calon_ppvi_qu, val_dk_no1vi, cl_form_calon_ttvi_qu, getContext())) {
                    ready = false;
                    radio3vi = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1vi, iv_circle_calon_ppvi_qu, val_dk_no1vi, cl_form_calon_tt1_vi_qu, getContext())) {
                    ready = false;
                    radio3vi = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1vi, iv_circle_calon_ppvi_qu, val_dk_no1vi, cl_form_calon_tt2_vi_qu, getContext())) {
                    ready = false;
                    radio3vi = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1vi, iv_circle_calon_ppvi_qu, val_dk_no1vi, cl_form_calon_tt3_vi_qu, getContext())) {
                    ready = false;
                    radio3vi = true;
                }//7
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1vii, iv_circle_calon_ppvii_qu, val_dk_no1vii, cl_form_calon_ppvii_qu, getContext())) {
                    ready = false;
                    radio3vii = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1vii, iv_circle_calon_ppvii_qu, val_dk_no1vii, cl_form_calon_ttvii_qu, getContext())) {
                    ready = false;
                    radio3vii = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1vii, iv_circle_calon_ppvii_qu, val_dk_no1vii, cl_form_calon_tt1_vii_qu, getContext())) {
                    ready = false;
                    radio3vii = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1vii, iv_circle_calon_ppvii_qu, val_dk_no1vii, cl_form_calon_tt2_vii_qu, getContext())) {
                    ready = false;
                    radio3vii = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1vii, iv_circle_calon_ppvii_qu, val_dk_no1vii, cl_form_calon_tt3_vii_qu, getContext())) {
                    ready = false;
                    radio3vii = true;
                }//8
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1viii, iv_circle_calon_ppviii_qu, val_dk_no1viii, cl_form_calon_ppviii_qu, getContext())) {
                    ready = false;
                    radio3viii = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1viii, iv_circle_calon_ppviii_qu, val_dk_no1viii, cl_form_calon_ttviii_qu, getContext())) {
                    ready = false;
                    radio3viii = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1viii, iv_circle_calon_ppviii_qu, val_dk_no1viii, cl_form_calon_tt1_viii_qu, getContext())) {
                    ready = false;
                    radio3viii = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1viii, iv_circle_calon_ppviii_qu, val_dk_no1viii, cl_form_calon_tt2_viii_qu, getContext())) {
                    ready = false;
                    radio3viii = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1viii, iv_circle_calon_ppviii_qu, val_dk_no1viii, cl_form_calon_tt3_viii_qu, getContext())) {
                    ready = false;
                    radio3viii = true;
                }//9
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1ix, iv_circle_calon_ppix_qu, val_dk_no1ix, cl_form_calon_ppix_qu, getContext())) {
                    ready = false;
                    radio3ix = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1ix, iv_circle_calon_ppix_qu, val_dk_no1ix, cl_form_calon_ttix_qu, getContext())) {
                    ready = false;
                    radio3ix = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1ix, iv_circle_calon_ppix_qu, val_dk_no1ix, cl_form_calon_tt1_ix_qu, getContext())) {
                    ready = false;
                    radio3ix = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1ix, iv_circle_calon_ppix_qu, val_dk_no1ix, cl_form_calon_tt2_ix_qu, getContext())) {
                    ready = false;
                    radio3ix = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1ix, iv_circle_calon_ppix_qu, val_dk_no1ix, cl_form_calon_tt3_ix_qu, getContext())) {
                    ready = false;
                    radio3ix = true;
                }//10
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1x, iv_circle_calon_ppx_qu, val_dk_no1x, cl_form_calon_ppx_qu, getContext())) {
                    ready = false;
                    radio3x = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1x, iv_circle_calon_ppx_qu, val_dk_no1x, cl_form_calon_ttx_qu, getContext())) {
                    ready = false;
                    radio3x = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1x, iv_circle_calon_ppx_qu, val_dk_no1x, cl_form_calon_tt1_x_qu, getContext())) {
                    ready = false;
                    radio3x = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1x, iv_circle_calon_ppx_qu, val_dk_no1x, cl_form_calon_tt2_x_qu, getContext())) {
                    ready = false;
                    radio3x = true;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1x, iv_circle_calon_ppx_qu, val_dk_no1x, cl_form_calon_tt3_x_qu, getContext())) {
                    ready = false;
                    radio3x = true;
                }//11
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xi, iv_circle_calon_ppxi_qu, val_dk_no1xi, cl_form_calon_ppxi_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xi, iv_circle_calon_ppxi_qu, val_dk_no1xi, cl_form_calon_ttxi_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xi, iv_circle_calon_ppxi_qu, val_dk_no1xi, cl_form_calon_tt1_xi_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xi, iv_circle_calon_ppxi_qu, val_dk_no1xi, cl_form_calon_tt2_xi_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xi, iv_circle_calon_ppxi_qu, val_dk_no1xi, cl_form_calon_tt3_xi_qu, getContext())) {
                    ready = false;
                }//12
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xii, iv_circle_calon_ppxii_qu, val_dk_no1xii, cl_form_calon_ppxii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xii, iv_circle_calon_ppxii_qu, val_dk_no1xii, cl_form_calon_ttxii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xii, iv_circle_calon_ppxii_qu, val_dk_no1xii, cl_form_calon_tt1_xii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xii, iv_circle_calon_ppxii_qu, val_dk_no1xii, cl_form_calon_tt2_xii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xii, iv_circle_calon_ppxii_qu, val_dk_no1xii, cl_form_calon_tt3_xii_qu, getContext())) {
                    ready = false;
                }//13
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xiii, iv_circle_calon_ppxiii_qu, val_dk_no1xiii, cl_form_calon_ppxiii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xiii, iv_circle_calon_ppxiii_qu, val_dk_no1xiii, cl_form_calon_ttxiii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xiii, iv_circle_calon_ppxiii_qu, val_dk_no1xiii, cl_form_calon_tt1_xiii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xiii, iv_circle_calon_ppxiii_qu, val_dk_no1xiii, cl_form_calon_tt2_xiii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xiii, iv_circle_calon_ppxiii_qu, val_dk_no1xiii, cl_form_calon_tt3_xiii_qu, getContext())) {
                    ready = false;
                }//14
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xiv, iv_circle_calon_ppxiv_qu, val_dk_no1xiv, cl_form_calon_ppxiv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xiv, iv_circle_calon_ppxiv_qu, val_dk_no1xiv, cl_form_calon_ttxiv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xiv, iv_circle_calon_ppxiv_qu, val_dk_no1xiv, cl_form_calon_tt1_xiv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xiv, iv_circle_calon_ppxiv_qu, val_dk_no1xiv, cl_form_calon_tt2_xiv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xiv, iv_circle_calon_ppxiv_qu, val_dk_no1xiv, cl_form_calon_tt3_xiv_qu, getContext())) {
                    ready = false;
                }//15
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xv, iv_circle_calon_ppxv_qu, val_dk_no1xv, cl_form_calon_ppxv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xv, iv_circle_calon_ppxv_qu, val_dk_no1xv, cl_form_calon_ttxv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xv, iv_circle_calon_ppxv_qu, val_dk_no1xv, cl_form_calon_tt1_xv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xv, iv_circle_calon_ppxv_qu, val_dk_no1xv, cl_form_calon_tt2_xv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xv, iv_circle_calon_ppxv_qu, val_dk_no1xv, cl_form_calon_tt3_xv_qu, getContext())) {
                    ready = false;
                }//16
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xvi, iv_circle_calon_ppxvi_qu, val_dk_no1xvi, cl_form_calon_ppxvi_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xvi, iv_circle_calon_ppxvi_qu, val_dk_no1xvi, cl_form_calon_ttxvi_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xvi, iv_circle_calon_ppxvi_qu, val_dk_no1xvi, cl_form_calon_tt1_xvi_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xvi, iv_circle_calon_ppxvi_qu, val_dk_no1xvi, cl_form_calon_tt2_xvi_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xvi, iv_circle_calon_ppxvi_qu, val_dk_no1xvi, cl_form_calon_tt3_xvi_qu, getContext())) {
                    ready = false;
                }//17
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xvii, iv_circle_calon_ppxvii_qu, val_dk_no1xvii, cl_form_calon_ppxvii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xvii, iv_circle_calon_ppxvii_qu, val_dk_no1xvii, cl_form_calon_ttxvii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xvii, iv_circle_calon_ppxvii_qu, val_dk_no1xvii, cl_form_calon_tt1_xvii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xvii, iv_circle_calon_ppxvii_qu, val_dk_no1xvii, cl_form_calon_tt2_xvii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xvii, iv_circle_calon_ppxvii_qu, val_dk_no1xvii, cl_form_calon_tt3_xvii_qu, getContext())) {
                    ready = false;
                }//18
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xviii, iv_circle_calon_ppxviii_qu, val_dk_no1xviii, cl_form_calon_ppxviii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xviii, iv_circle_calon_ppxviii_qu, val_dk_no1xviii, cl_form_calon_ttxviii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xviii, iv_circle_calon_ppxviii_qu, val_dk_no1xviii, cl_form_calon_tt1_xviii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xviii, iv_circle_calon_ppxviii_qu, val_dk_no1xviii, cl_form_calon_tt2_xviii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xviii, iv_circle_calon_ppxviii_qu, val_dk_no1xviii, cl_form_calon_tt3_xviii_qu, getContext())) {
                    ready = false;
                }//19
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xix, iv_circle_calon_ppxix_qu, val_dk_no1xix, cl_form_calon_ppxix_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xix, iv_circle_calon_ppxix_qu, val_dk_no1xix, cl_form_calon_ttxix_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xix, iv_circle_calon_ppxix_qu, val_dk_no1xix, cl_form_calon_tt1_xix_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xix, iv_circle_calon_ppxix_qu, val_dk_no1xix, cl_form_calon_tt2_xix_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xix, iv_circle_calon_ppxix_qu, val_dk_no1xix, cl_form_calon_tt3_xix_qu, getContext())) {
                    ready = false;
                }//20
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xx, iv_circle_calon_ppxx_qu, val_dk_no1xx, cl_form_calon_ppxx_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xx, iv_circle_calon_ppxx_qu, val_dk_no1xx, cl_form_calon_ttxx_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xx, iv_circle_calon_ppxx_qu, val_dk_no1xx, cl_form_calon_tt1_xx_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xx, iv_circle_calon_ppxx_qu, val_dk_no1xx, cl_form_calon_tt2_xx_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xx, iv_circle_calon_ppxx_qu, val_dk_no1xx, cl_form_calon_tt3_xx_qu, getContext())) {
                    ready = false;
                }//21
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxi, iv_circle_calon_ppxxi_qu, val_dk_no1xxi, cl_form_calon_ppxxi_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxi, iv_circle_calon_ppxxi_qu, val_dk_no1xxi, cl_form_calon_ttxxi_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxi, iv_circle_calon_ppxxi_qu, val_dk_no1xxi, cl_form_calon_tt1_xxi_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxi, iv_circle_calon_ppxxi_qu, val_dk_no1xxi, cl_form_calon_tt2_xxi_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxi, iv_circle_calon_ppxxi_qu, val_dk_no1xxi, cl_form_calon_tt3_xxi_qu, getContext())) {
                    ready = false;
                }//22
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxii, iv_circle_calon_ppxxii_qu, val_dk_no1xxii, cl_form_calon_ppxxii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxii, iv_circle_calon_ppxxii_qu, val_dk_no1xxii, cl_form_calon_ttxxii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxii, iv_circle_calon_ppxxii_qu, val_dk_no1xxii, cl_form_calon_tt1_xxii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxii, iv_circle_calon_ppxxii_qu, val_dk_no1xxii, cl_form_calon_tt2_xxii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxii, iv_circle_calon_ppxxii_qu, val_dk_no1xxii, cl_form_calon_tt3_xxii_qu, getContext())) {
                    ready = false;
                }//23
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxiii, iv_circle_calon_ppxxiii_qu, val_dk_no1xxiii, cl_form_calon_ppxxiii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxiii, iv_circle_calon_ppxxiii_qu, val_dk_no1xxiii, cl_form_calon_ttxxiii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxiii, iv_circle_calon_ppxxiii_qu, val_dk_no1xxiii, cl_form_calon_tt1_xxiii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxiii, iv_circle_calon_ppxxiii_qu, val_dk_no1xxiii, cl_form_calon_tt2_xxiii_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxiii, iv_circle_calon_ppxxiii_qu, val_dk_no1xxiii, cl_form_calon_tt3_xxiii_qu, getContext())) {
                    ready = false;
                }//24
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxiv, iv_circle_calon_ppxxiv_qu, val_dk_no1xxiv, cl_form_calon_ppxxiv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxiv, iv_circle_calon_ppxxiv_qu, val_dk_no1xxiv, cl_form_calon_ttxxiv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxiv, iv_circle_calon_ppxxiv_qu, val_dk_no1xxiv, cl_form_calon_tt1_xxiv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxiv, iv_circle_calon_ppxxiv_qu, val_dk_no1xxiv, cl_form_calon_tt2_xxiv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxiv, iv_circle_calon_ppxxiv_qu, val_dk_no1xxiv, cl_form_calon_tt3_xxiv_qu, getContext())) {
                    ready = false;
                }//25
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxv, iv_circle_calon_ppxxv_qu, val_dk_no1xxv, cl_form_calon_ppxxv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxv, iv_circle_calon_ppxxv_qu, val_dk_no1xxv, cl_form_calon_ttxxv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxv, iv_circle_calon_ppxxv_qu, val_dk_no1xxv, cl_form_calon_tt1_xxv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxv, iv_circle_calon_ppxxv_qu, val_dk_no1xxv, cl_form_calon_tt2_xxv_qu, getContext())) {
                    ready = false;
                }
                if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxv, iv_circle_calon_ppxxv_qu, val_dk_no1xxv, cl_form_calon_tt3_xxv_qu, getContext())) {
                    ready = false;
                }
                if (!ready) {
                    scroll_qu.scrollTo(0, 0);
                    layout_vis_tt.setVisibility(View.GONE);
                    cl_data_calon_pp_qu.setVisibility(View.GONE);
                    cl_data_kesehatan_qu.setVisibility(View.VISIBLE);
                    layout_vis_dk_2.setVisibility(View.GONE);
//                    text_indicator_qu.setText(getResources().getString(R.string.hal3));
                } else {
                    //26
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxvi, iv_circle_calon_ppxxvi_qu, val_dk_no1xxvi, cl_form_calon_ppxxvi_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxvi, iv_circle_calon_ppxxvi_qu, val_dk_no1xxvi, cl_form_calon_ttxxvi_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxvi, iv_circle_calon_ppxxvi_qu, val_dk_no1xxvi, cl_form_calon_tt1_xxvi_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxvi, iv_circle_calon_ppxxvi_qu, val_dk_no1xxvi, cl_form_calon_tt2_xxvi_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxvi, iv_circle_calon_ppxxvi_qu, val_dk_no1xxvi, cl_form_calon_tt3_xxvi_qu, getContext())) {
                        ready = false;
                    }//27
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxvii, iv_circle_calon_ppxxvii_qu, val_dk_no1xxvii, cl_form_calon_ppxxvii_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxvii, iv_circle_calon_ppxxvii_qu, val_dk_no1xxvii, cl_form_calon_ttxxvii_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxvii, iv_circle_calon_ppxxvii_qu, val_dk_no1xxvii, cl_form_calon_tt1_xxvii_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxvii, iv_circle_calon_ppxxvii_qu, val_dk_no1xxvii, cl_form_calon_tt2_xxviii_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxvii, iv_circle_calon_ppxxvii_qu, val_dk_no1xxvii, cl_form_calon_tt3_xxvii_qu, getContext())) {
                        ready = false;
                    }//28
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxviii, iv_circle_calon_ppxxviii_qu, val_dk_no1xxviii, cl_form_calon_ppxxviii_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxviii, iv_circle_calon_ppxxviii_qu, val_dk_no1xxviii, cl_form_calon_ttxxviii_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxviii, iv_circle_calon_ppxxviii_qu, val_dk_no1xxviii, cl_form_calon_tt1_xxviii_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxviii, iv_circle_calon_ppxxviii_qu, val_dk_no1xxviii, cl_form_calon_tt2_xxviii_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxviii, iv_circle_calon_ppxxviii_qu, val_dk_no1xxviii, cl_form_calon_tt3_xxviii_qu, getContext())) {
                        ready = false;
                    }//29
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxix, iv_circle_calon_ppxxix_qu, val_dk_no1xxix, cl_form_calon_ppxxix_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxix, iv_circle_calon_ppxxix_qu, val_dk_no1xxix, cl_form_calon_ttxxix_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxix, iv_circle_calon_ppxxix_qu, val_dk_no1xxix, cl_form_calon_tt1_xxix_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxix, iv_circle_calon_ppxxix_qu, val_dk_no1xxix, cl_form_calon_tt2_xxix_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxix, iv_circle_calon_ppxxix_qu, val_dk_no1xxix, cl_form_calon_tt3_xxix_qu, getContext())) {
                        ready = false;
                    }//30
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxx, iv_circle_calon_ppxxxi_qu, val_dk_no1xxx, cl_form_calon_ppxxx_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxx, iv_circle_calon_ppxxxi_qu, val_dk_no1xxx, cl_form_calon_ttxxx_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxx, iv_circle_calon_ppxxxi_qu, val_dk_no1xxx, cl_form_calon_tt1_xxxi_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxx, iv_circle_calon_ppxxxi_qu, val_dk_no1xxx, cl_form_calon_tt2_xxx_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxx, iv_circle_calon_ppxxxi_qu, val_dk_no1xxx, cl_form_calon_tt3_xxxi_qu, getContext())) {
                        ready = false;
                    }//31
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxxi, iv_circle_calon_ppxxxi_qu, val_dk_no1xxxi, cl_form_calon_ppxxxi_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxxi, iv_circle_calon_ppxxxi_qu, val_dk_no1xxxi, cl_form_calon_ttxxxi_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxxi, iv_circle_calon_ppxxxi_qu, val_dk_no1xxxi, cl_form_calon_tt1_xxxi_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxxi, iv_circle_calon_ppxxxi_qu, val_dk_no1xxxi, cl_form_calon_tt2_xxxi_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxxi, iv_circle_calon_ppxxxi_qu, val_dk_no1xxxi, cl_form_calon_tt3_xxxi_qu, getContext())) {
                        ready = false;
                    }//32
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno2, iv_circle_calon_pp_qu, val_dk_no2, cl_form_calon_pp_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno2, iv_circle_calon_pp_qu, val_dk_no2, cl_form_calon_tt_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no2, iv_circle_calon_pp_qu, val_dk_no2, cl_form_calon_tt1_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no2, iv_circle_calon_pp_qu, val_dk_no2, cl_form_calon_tt2_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no2, iv_circle_calon_pp_qu, val_dk_no2, cl_form_calon_tt3_qu, getContext())) {
                        ready = false;
                    }//33
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno3a, iv_circle_calon_pp_qu, val_dk_no3a, cl_form_calon_pp3a_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno3a, iv_circle_calon_pp3a_qu, val_dk_no3a, cl_form_calon_tt3a_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no3a, iv_circle_calon_pp3a_qu, val_dk_no3a, cl_form_calon_tt1_3a_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no3a, iv_circle_calon_pp3a_qu, val_dk_no3a, cl_form_calon_tt2_3a_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no3a, iv_circle_calon_pp3a_qu, val_dk_no3a, cl_form_calon_tt3_3a_qu, getContext())) {
                        ready = false;
                    }//34
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno3b, iv_circle_calon_pp3b_qu, val_dk_no3b, cl_form_calon_pp3b_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno3b, iv_circle_calon_pp3b_qu, val_dk_no3b, cl_form_calon_tt3b_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no3b, iv_circle_calon_pp3b_qu, val_dk_no3b, cl_form_calon_tt1_3b_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no3b, iv_circle_calon_pp3b_qu, val_dk_no3b, cl_form_calon_tt2_3b_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no3b, iv_circle_calon_pp3b_qu, val_dk_no3b, cl_form_calon_tt3_3b_qu, getContext())) {
                        ready = false;
                    }//35
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno3c, iv_circle_calon_pp3c_qu, val_dk_no3c, cl_form_calon_pp3c_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno3c, iv_circle_calon_pp3c_qu, val_dk_no3c, cl_form_calon_tt3c_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no3c, iv_circle_calon_pp3c_qu, val_dk_no3c, cl_form_calon_tt1_3c_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no3c, iv_circle_calon_pp3c_qu, val_dk_no3c, cl_form_calon_tt2_3c_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no3c, iv_circle_calon_pp3c_qu, val_dk_no3c, cl_form_calon_tt3_3c_qu, getContext())) {
                        ready = false;
                    }//36
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno3d, iv_circle_calon_pp3d_qu, val_dk_no3d, cl_form_calon_pp3d_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno3d, iv_circle_calon_pp3d_qu, val_dk_no3d, cl_form_calon_tt3d_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no3d, iv_circle_calon_pp3d_qu, val_dk_no3d, cl_form_calon_tt1_3d_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no3d, iv_circle_calon_pp3d_qu, val_dk_no3d, cl_form_calon_tt2_3d_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no3d, iv_circle_calon_pp3d_qu, val_dk_no3d, cl_form_calon_tt3_3d_qu, getContext())) {
                        ready = false;
                    }//37
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno4, iv_circle_calon_pp4_qu, val_dk_no4, cl_form_calon_pp4_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno4, iv_circle_calon_pp4_qu, val_dk_no4, cl_form_calon_tt4_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no4, iv_circle_calon_pp4_qu, val_dk_no4, cl_form_calon_tt1_4_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no4, iv_circle_calon_pp4_qu, val_dk_no4, cl_form_calon_tt2_4_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no4, iv_circle_calon_pp4_qu, val_dk_no4, cl_form_calon_tt3_4_qu, getContext())) {
                        ready = false;
                    }//38
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno5, iv_circle_calon_pp5_qu, val_dk_no5, cl_form_calon_pp5_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno5, iv_circle_calon_pp5_qu, val_dk_no5, cl_form_calon_tt5_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no5, iv_circle_calon_pp5_qu, val_dk_no5, cl_form_calon_tt1_5_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no5, iv_circle_calon_pp5_qu, val_dk_no5, cl_form_calon_tt2_5_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no5, iv_circle_calon_pp5_qu, val_dk_no5, cl_form_calon_tt3_5_qu, getContext())) {
                        ready = false;
                    }//39
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno6, iv_circle_calon_pp6_qu, val_dk_no6, cl_form_calon_pp6_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno6, iv_circle_calon_pp6_qu, val_dk_no6, cl_form_calon_tt6_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no6, iv_circle_calon_pp6_qu, val_dk_no6, cl_form_calon_tt1_6_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no6, iv_circle_calon_pp6_qu, val_dk_no6, cl_form_calon_tt2_6_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no6, iv_circle_calon_pp6_qu, val_dk_no6, cl_form_calon_tt3_6_qu, getContext())) {
                        ready = false;
                    }//40
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno7, iv_circle_calon_pp7_qu, val_dk_no7, cl_form_calon_pp7_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno7, iv_circle_calon_pp7_qu, val_dk_no7, cl_form_calon_tt7_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no7, iv_circle_calon_pp7_qu, val_dk_no7, cl_form_calon_tt1_7_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no7, iv_circle_calon_pp7_qu, val_dk_no7, cl_form_calon_tt2_7_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no7, iv_circle_calon_pp7_qu, val_dk_no7, cl_form_calon_tt3_7_qu, getContext())) {
                        ready = false;
                    }//41
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno9, iv_circle_calon_pp9_qu, val_dk_no9, cl_form_calon_pp9_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno9, iv_circle_calon_pp9_qu, val_dk_no9, cl_form_calon_tt9_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no9, iv_circle_calon_pp9_qu, val_dk_no9, cl_form_calon_tt1_9_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no9, iv_circle_calon_pp9_qu, val_dk_no9, cl_form_calon_tt2_9_qu, getContext())) {
                        ready = false;
                    }
                    if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no9, iv_circle_calon_pp9_qu, val_dk_no9, cl_form_calon_tt3_9_qu, getContext())) {
                        ready = false;
                    }
                    if(me.getPemegangPolisModel().getJekel_pp()== 0){
                        if(option_grup_dk_ppno8a.getVisibility() == View.VISIBLE){
                            if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno8a, iv_circle_calon_pp8a_qu, val_dk_no9, cl_form_calon_pp8a_qu, getContext())) {
                                ready = false;
                            }
                        }
                        if(option_grup_dk_ppno8b.getVisibility() == View.VISIBLE){
                            if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno8b, iv_circle_calon_pp8b_qu, val_dk_no9, cl_form_calon_pp8b_qu, getContext())) {
                                ready = false;
                            }
                        }
                        if(option_grup_dk_ppno8c.getVisibility() == View.VISIBLE){
                            if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno8c, iv_circle_calon_pp8c_qu, val_dk_no9, cl_form_calon_pp8c_qu, getContext())) {
                                ready = false;
                            }
                        }
                    }
                    if(me.getTertanggungModel().getJekel_tt()== 0){
                        if(option_grup_dk_ttno8a.getVisibility() == View.VISIBLE){
                            if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno8a, iv_circle_calon_tt8a_qu, val_dk_no9, cl_form_calon_tt8a_qu, getContext())) {
                                ready = false;
                            }
                        }
                        if(option_grup_dk_ttno8b.getVisibility() == View.VISIBLE){
                            if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno8b, iv_circle_calon_tt8b_qu, val_dk_no9, cl_form_calon_tt8b_qu, getContext())) {
                                ready = false;
                            }
                        }
                        if(option_grup_dk_ttno8c.getVisibility() == View.VISIBLE){
                            if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno8c, iv_circle_calon_tt8c_qu, val_dk_no9, cl_form_calon_tt8c_qu, getContext())) {
                                ready = false;
                            }
                        }
                    }
//                    if (option_y_dk_ppno9.isChecked()) {
//                        //bernard
//                        if (count_dk_9pp < 1) {
//                            ready = false;
//                        }
//                        if (count_dk_9pp > 0) {
//                            List_dkno9pp = me.getModelUpdateQuisioner().getList_dkno9pp();
//                            for (int i = 0; i < List_dkno9pp.size(); i++) {
//                                if (List_dkno9pp.get(i).getUmur().equals("") || List_dkno9pp.get(i).getPenyebab().equals("")){
//                                    ready = false;
//                                }
//                            }
//                        }
//                    }
//                    if (option_y_dk_ttno9.isChecked()) {
//                        if (count_dk_9tt < 1) {
//                            ready = false;
//                        }
//                        if (count_dk_9tt > 0) {
//                            List_dkno9tt = me.getModelUpdateQuisioner().getList_dkno9tt();
//                            for (int i = 0; i < List_dkno9tt.size(); i++) {
//                                if (List_dkno9tt.get(i).getUmur().equals("") || List_dkno9tt.get(i).getPenyebab().equals("")){
//                                    ready = false;
//                                }
//                            }
//                        }
//                    }
                }

            }
        }

        return ready;
    }

    private boolean questionareTask3i ()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1l, iv_circle_calon_ppi_qu, val_dk_no1l, cl_form_calon_ppi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1l, iv_circle_calon_ppi_qu, val_dk_no1l, cl_form_calon_tti_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1l, iv_circle_calon_ppi_qu, val_dk_no1l, cl_form_calon_tt1_i_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1l, iv_circle_calon_ppi_qu, val_dk_no1l, cl_form_calon_tt2_i_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1l, iv_circle_calon_ppi_qu, val_dk_no1l, cl_form_calon_tt3_i_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3ii ()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1ii, iv_circle_calon_ppii_qu, val_dk_no1ii, cl_form_calon_ppii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1ii, iv_circle_calon_ppii_qu, val_dk_no1ii, cl_form_calon_ttii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1ii, iv_circle_calon_ppii_qu, val_dk_no1ii, cl_form_calon_tt1_ii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1ii, iv_circle_calon_ppii_qu, val_dk_no1ii, cl_form_calon_tt2_ii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1ii, iv_circle_calon_ppii_qu, val_dk_no1ii, cl_form_calon_tt3_ii_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3iii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1iii, iv_circle_calon_ppiii_qu, val_dk_no1iii, cl_form_calon_ppiii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1iii, iv_circle_calon_ppiii_qu, val_dk_no1iii, cl_form_calon_ttiii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1iii, iv_circle_calon_ppiii_qu, val_dk_no1iii, cl_form_calon_tt1_iii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1iii, iv_circle_calon_ppiii_qu, val_dk_no1iii, cl_form_calon_tt2_iii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1iii, iv_circle_calon_ppiii_qu, val_dk_no1iii, cl_form_calon_tt3_iii_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3iv()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1iv, iv_circle_calon_ppiv_qu, val_dk_no1iv, cl_form_calon_ppiv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1iv, iv_circle_calon_ppiv_qu, val_dk_no1iv, cl_form_calon_ttiv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1iv, iv_circle_calon_ppiv_qu, val_dk_no1iv, cl_form_calon_tt1_iv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1iv, iv_circle_calon_ppiv_qu, val_dk_no1iv, cl_form_calon_tt2_iv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1iv, iv_circle_calon_ppiv_qu, val_dk_no1iv, cl_form_calon_tt3_iv_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3v()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1v, iv_circle_calon_ppv_qu, val_dk_no1v, cl_form_calon_ppv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1v, iv_circle_calon_ppv_qu, val_dk_no1v, cl_form_calon_ttv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1v, iv_circle_calon_ppv_qu, val_dk_no1v, cl_form_calon_tt1_v_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1v, iv_circle_calon_ppv_qu, val_dk_no1v, cl_form_calon_tt2_v_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1v, iv_circle_calon_ppv_qu, val_dk_no1v, cl_form_calon_tt3_v_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3vi()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1vi, iv_circle_calon_ppvi_qu, val_dk_no1vi, cl_form_calon_ppvi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1vi, iv_circle_calon_ppvi_qu, val_dk_no1vi, cl_form_calon_ttvi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1vi, iv_circle_calon_ppvi_qu, val_dk_no1vi, cl_form_calon_tt1_vi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1vi, iv_circle_calon_ppvi_qu, val_dk_no1vi, cl_form_calon_tt2_vi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1vi, iv_circle_calon_ppvi_qu, val_dk_no1vi, cl_form_calon_tt3_vi_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3vii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1vii, iv_circle_calon_ppvii_qu, val_dk_no1vii, cl_form_calon_ppvii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1vii, iv_circle_calon_ppvii_qu, val_dk_no1vii, cl_form_calon_ttvii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1vii, iv_circle_calon_ppvii_qu, val_dk_no1vii, cl_form_calon_tt1_vii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1vii, iv_circle_calon_ppvii_qu, val_dk_no1vii, cl_form_calon_tt2_vii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1vii, iv_circle_calon_ppvii_qu, val_dk_no1vii, cl_form_calon_tt3_vii_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3viii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1viii, iv_circle_calon_ppviii_qu, val_dk_no1viii, cl_form_calon_ppviii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1viii, iv_circle_calon_ppviii_qu, val_dk_no1viii, cl_form_calon_ttviii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1viii, iv_circle_calon_ppviii_qu, val_dk_no1viii, cl_form_calon_tt1_viii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1viii, iv_circle_calon_ppviii_qu, val_dk_no1viii, cl_form_calon_tt2_viii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1viii, iv_circle_calon_ppviii_qu, val_dk_no1viii, cl_form_calon_tt3_viii_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3ix()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1ix, iv_circle_calon_ppix_qu, val_dk_no1ix, cl_form_calon_ppix_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1ix, iv_circle_calon_ppix_qu, val_dk_no1ix, cl_form_calon_ttix_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1ix, iv_circle_calon_ppix_qu, val_dk_no1ix, cl_form_calon_tt1_ix_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1ix, iv_circle_calon_ppix_qu, val_dk_no1ix, cl_form_calon_tt2_ix_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1ix, iv_circle_calon_ppix_qu, val_dk_no1ix, cl_form_calon_tt3_ix_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3x()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1x, iv_circle_calon_ppx_qu, val_dk_no1x, cl_form_calon_ppx_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1x, iv_circle_calon_ppx_qu, val_dk_no1x, cl_form_calon_ttx_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1x, iv_circle_calon_ppx_qu, val_dk_no1x, cl_form_calon_tt1_x_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1x, iv_circle_calon_ppx_qu, val_dk_no1x, cl_form_calon_tt2_x_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1x, iv_circle_calon_ppx_qu, val_dk_no1x, cl_form_calon_tt3_x_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xi()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xi, iv_circle_calon_ppxi_qu, val_dk_no1xi, cl_form_calon_ppxi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xi, iv_circle_calon_ppxi_qu, val_dk_no1xi, cl_form_calon_ttxi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xi, iv_circle_calon_ppxi_qu, val_dk_no1xi, cl_form_calon_tt1_xi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xi, iv_circle_calon_ppxi_qu, val_dk_no1xi, cl_form_calon_tt2_xi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xi, iv_circle_calon_ppxi_qu, val_dk_no1xi, cl_form_calon_tt3_xi_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xii, iv_circle_calon_ppxii_qu, val_dk_no1xii, cl_form_calon_ppxii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xii, iv_circle_calon_ppxii_qu, val_dk_no1xii, cl_form_calon_ttxii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xii, iv_circle_calon_ppxii_qu, val_dk_no1xii, cl_form_calon_tt1_xii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xii, iv_circle_calon_ppxii_qu, val_dk_no1xii, cl_form_calon_tt2_xii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xii, iv_circle_calon_ppxii_qu, val_dk_no1xii, cl_form_calon_tt3_xii_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xiii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xiii, iv_circle_calon_ppxiii_qu, val_dk_no1xiii, cl_form_calon_ppxiii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xiii, iv_circle_calon_ppxiii_qu, val_dk_no1xiii, cl_form_calon_ttxiii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xiii, iv_circle_calon_ppxiii_qu, val_dk_no1xiii, cl_form_calon_tt1_xiii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xiii, iv_circle_calon_ppxiii_qu, val_dk_no1xiii, cl_form_calon_tt2_xiii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xiii, iv_circle_calon_ppxiii_qu, val_dk_no1xiii, cl_form_calon_tt3_xiii_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xiv()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xiv, iv_circle_calon_ppxiv_qu, val_dk_no1xiv, cl_form_calon_ppxiv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xiv, iv_circle_calon_ppxiv_qu, val_dk_no1xiv, cl_form_calon_ttxiv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xiv, iv_circle_calon_ppxiv_qu, val_dk_no1xiv, cl_form_calon_tt1_xiv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xiv, iv_circle_calon_ppxiv_qu, val_dk_no1xiv, cl_form_calon_tt2_xiv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xiv, iv_circle_calon_ppxiv_qu, val_dk_no1xiv, cl_form_calon_tt3_xiv_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xv()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xv, iv_circle_calon_ppxv_qu, val_dk_no1xv, cl_form_calon_ppxv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xv, iv_circle_calon_ppxv_qu, val_dk_no1xv, cl_form_calon_ttxv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xv, iv_circle_calon_ppxv_qu, val_dk_no1xv, cl_form_calon_tt1_xv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xv, iv_circle_calon_ppxv_qu, val_dk_no1xv, cl_form_calon_tt2_xv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xv, iv_circle_calon_ppxv_qu, val_dk_no1xv, cl_form_calon_tt3_xv_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xvi()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xvi, iv_circle_calon_ppxvi_qu, val_dk_no1xvi, cl_form_calon_ppxvi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xvi, iv_circle_calon_ppxvi_qu, val_dk_no1xvi, cl_form_calon_ttxvi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xvi, iv_circle_calon_ppxvi_qu, val_dk_no1xvi, cl_form_calon_tt1_xvi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xvi, iv_circle_calon_ppxvi_qu, val_dk_no1xvi, cl_form_calon_tt2_xvi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xvi, iv_circle_calon_ppxvi_qu, val_dk_no1xvi, cl_form_calon_tt3_xvi_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xvii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xvii, iv_circle_calon_ppxvii_qu, val_dk_no1xvii, cl_form_calon_ppxvii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xvii, iv_circle_calon_ppxvii_qu, val_dk_no1xvii, cl_form_calon_ttxvii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xvii, iv_circle_calon_ppxvii_qu, val_dk_no1xvii, cl_form_calon_tt1_xvii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xvii, iv_circle_calon_ppxvii_qu, val_dk_no1xvii, cl_form_calon_tt2_xvii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xvii, iv_circle_calon_ppxvii_qu, val_dk_no1xvii, cl_form_calon_tt3_xvii_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xviii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xviii, iv_circle_calon_ppxviii_qu, val_dk_no1xviii, cl_form_calon_ppxviii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xviii, iv_circle_calon_ppxviii_qu, val_dk_no1xviii, cl_form_calon_ttxviii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xviii, iv_circle_calon_ppxviii_qu, val_dk_no1xviii, cl_form_calon_tt1_xviii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xviii, iv_circle_calon_ppxviii_qu, val_dk_no1xviii, cl_form_calon_tt2_xviii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xviii, iv_circle_calon_ppxviii_qu, val_dk_no1xviii, cl_form_calon_tt3_xviii_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xix()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xix, iv_circle_calon_ppxix_qu, val_dk_no1xix, cl_form_calon_ppxix_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xix, iv_circle_calon_ppxix_qu, val_dk_no1xix, cl_form_calon_ttxix_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xix, iv_circle_calon_ppxix_qu, val_dk_no1xix, cl_form_calon_tt1_xix_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xix, iv_circle_calon_ppxix_qu, val_dk_no1xix, cl_form_calon_tt2_xix_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xix, iv_circle_calon_ppxix_qu, val_dk_no1xix, cl_form_calon_tt3_xix_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xx()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xx, iv_circle_calon_ppxx_qu, val_dk_no1xx, cl_form_calon_ppxx_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xx, iv_circle_calon_ppxx_qu, val_dk_no1xx, cl_form_calon_ttxx_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xx, iv_circle_calon_ppxx_qu, val_dk_no1xx, cl_form_calon_tt1_xx_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xx, iv_circle_calon_ppxx_qu, val_dk_no1xx, cl_form_calon_tt2_xx_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xx, iv_circle_calon_ppxx_qu, val_dk_no1xx, cl_form_calon_tt3_xx_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xxi()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxi, iv_circle_calon_ppxxi_qu, val_dk_no1xxi, cl_form_calon_ppxxi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxi, iv_circle_calon_ppxxi_qu, val_dk_no1xxi, cl_form_calon_ttxxi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxi, iv_circle_calon_ppxxi_qu, val_dk_no1xxi, cl_form_calon_tt1_xxi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxi, iv_circle_calon_ppxxi_qu, val_dk_no1xxi, cl_form_calon_tt2_xxi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxi, iv_circle_calon_ppxxi_qu, val_dk_no1xxi, cl_form_calon_tt3_xxi_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xxii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxii, iv_circle_calon_ppxxii_qu, val_dk_no1xxii, cl_form_calon_ppxxii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxii, iv_circle_calon_ppxxii_qu, val_dk_no1xxii, cl_form_calon_ttxxii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxii, iv_circle_calon_ppxxii_qu, val_dk_no1xxii, cl_form_calon_tt1_xxii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxii, iv_circle_calon_ppxxii_qu, val_dk_no1xxii, cl_form_calon_tt2_xxii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxii, iv_circle_calon_ppxxii_qu, val_dk_no1xxii, cl_form_calon_tt3_xxii_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xxiii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxiii, iv_circle_calon_ppxxiii_qu, val_dk_no1xxiii, cl_form_calon_ppxxiii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxiii, iv_circle_calon_ppxxiii_qu, val_dk_no1xxiii, cl_form_calon_ttxxiii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxiii, iv_circle_calon_ppxxiii_qu, val_dk_no1xxiii, cl_form_calon_tt1_xxiii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxiii, iv_circle_calon_ppxxiii_qu, val_dk_no1xxiii, cl_form_calon_tt2_xxiii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxiii, iv_circle_calon_ppxxiii_qu, val_dk_no1xxiii, cl_form_calon_tt3_xxiii_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xxiv()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxiv, iv_circle_calon_ppxxiv_qu, val_dk_no1xxiv, cl_form_calon_ppxxiv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxiv, iv_circle_calon_ppxxiv_qu, val_dk_no1xxiv, cl_form_calon_ttxxiv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxiv, iv_circle_calon_ppxxiv_qu, val_dk_no1xxiv, cl_form_calon_tt1_xxiv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxiv, iv_circle_calon_ppxxiv_qu, val_dk_no1xxiv, cl_form_calon_tt2_xxiv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxiv, iv_circle_calon_ppxxiv_qu, val_dk_no1xxiv, cl_form_calon_tt3_xxiv_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xxv()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxv, iv_circle_calon_ppxxv_qu, val_dk_no1xxv, cl_form_calon_ppxxv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxv, iv_circle_calon_ppxxv_qu, val_dk_no1xxv, cl_form_calon_ttxxv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxv, iv_circle_calon_ppxxv_qu, val_dk_no1xxv, cl_form_calon_tt1_xxv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxv, iv_circle_calon_ppxxv_qu, val_dk_no1xxv, cl_form_calon_tt2_xxv_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxv, iv_circle_calon_ppxxv_qu, val_dk_no1xxv, cl_form_calon_tt3_xxv_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask3xxvi()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxvi, iv_circle_calon_ppxxvi_qu, val_dk_no1xxvi, cl_form_calon_ppxxvi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxvi, iv_circle_calon_ppxxvi_qu, val_dk_no1xxvi, cl_form_calon_ttxxvi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxvi, iv_circle_calon_ppxxvi_qu, val_dk_no1xxvi, cl_form_calon_tt1_xxvi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxvi, iv_circle_calon_ppxxvi_qu, val_dk_no1xxvi, cl_form_calon_tt2_xxvi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxvi, iv_circle_calon_ppxxvi_qu, val_dk_no1xxvi, cl_form_calon_tt3_xxvi_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxvii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxvii, iv_circle_calon_ppxxvii_qu, val_dk_no1xxvii, cl_form_calon_ppxxvii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxvii, iv_circle_calon_ppxxvii_qu, val_dk_no1xxvii, cl_form_calon_ttxxvii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxvii, iv_circle_calon_ppxxvii_qu, val_dk_no1xxvii, cl_form_calon_tt1_xxvii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxvii, iv_circle_calon_ppxxvii_qu, val_dk_no1xxvii, cl_form_calon_tt2_xxviii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxvii, iv_circle_calon_ppxxvii_qu, val_dk_no1xxvii, cl_form_calon_tt3_xxvii_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxviii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxviii, iv_circle_calon_ppxxviii_qu, val_dk_no1xxviii, cl_form_calon_ppxxviii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxviii, iv_circle_calon_ppxxviii_qu, val_dk_no1xxviii, cl_form_calon_ttxxviii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxviii, iv_circle_calon_ppxxviii_qu, val_dk_no1xxviii, cl_form_calon_tt1_xxviii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxviii, iv_circle_calon_ppxxviii_qu, val_dk_no1xxviii, cl_form_calon_tt2_xxviii_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxviii, iv_circle_calon_ppxxviii_qu, val_dk_no1xxviii, cl_form_calon_tt3_xxviii_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxix()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxix, iv_circle_calon_ppxxix_qu, val_dk_no1xxix, cl_form_calon_ppxxix_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxix, iv_circle_calon_ppxxix_qu, val_dk_no1xxix, cl_form_calon_ttxxix_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxix, iv_circle_calon_ppxxix_qu, val_dk_no1xxix, cl_form_calon_tt1_xxix_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxix, iv_circle_calon_ppxxix_qu, val_dk_no1xxix, cl_form_calon_tt2_xxix_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxix, iv_circle_calon_ppxxix_qu, val_dk_no1xxix, cl_form_calon_tt3_xxix_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxx()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxx, iv_circle_calon_ppxxxi_qu, val_dk_no1xxx, cl_form_calon_ppxxx_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxx, iv_circle_calon_ppxxxi_qu, val_dk_no1xxx, cl_form_calon_ttxxx_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxx, iv_circle_calon_ppxxxi_qu, val_dk_no1xxx, cl_form_calon_tt1_xxxi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxx, iv_circle_calon_ppxxxi_qu, val_dk_no1xxx, cl_form_calon_tt2_xxx_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxx, iv_circle_calon_ppxxxi_qu, val_dk_no1xxx, cl_form_calon_tt3_xxxi_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxxi()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno1xxxi, iv_circle_calon_ppxxxi_qu, val_dk_no1xxxi, cl_form_calon_ppxxxi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno1xxxi, iv_circle_calon_ppxxxi_qu, val_dk_no1xxxi, cl_form_calon_ttxxxi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no1xxxi, iv_circle_calon_ppxxxi_qu, val_dk_no1xxxi, cl_form_calon_tt1_xxxi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no1xxxi, iv_circle_calon_ppxxxi_qu, val_dk_no1xxxi, cl_form_calon_tt2_xxxi_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no1xxxi, iv_circle_calon_ppxxxi_qu, val_dk_no1xxxi, cl_form_calon_tt3_xxxi_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxxii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno2, iv_circle_calon_pp_qu, val_dk_no2, cl_form_calon_pp_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno2, iv_circle_calon_pp_qu, val_dk_no2, cl_form_calon_tt_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no2, iv_circle_calon_pp_qu, val_dk_no2, cl_form_calon_tt1_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no2, iv_circle_calon_pp_qu, val_dk_no2, cl_form_calon_tt2_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no2, iv_circle_calon_pp_qu, val_dk_no2, cl_form_calon_tt3_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxxiii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno3a, iv_circle_calon_pp_qu, val_dk_no3a, cl_form_calon_pp3a_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno3a, iv_circle_calon_pp3a_qu, val_dk_no3a, cl_form_calon_tt3a_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no3a, iv_circle_calon_pp3a_qu, val_dk_no3a, cl_form_calon_tt1_3a_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no3a, iv_circle_calon_pp3a_qu, val_dk_no3a, cl_form_calon_tt2_3a_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no3a, iv_circle_calon_pp3a_qu, val_dk_no3a, cl_form_calon_tt3_3a_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxxiv()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno3b, iv_circle_calon_pp3b_qu, val_dk_no3b, cl_form_calon_pp3b_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno3b, iv_circle_calon_pp3b_qu, val_dk_no3b, cl_form_calon_tt3b_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no3b, iv_circle_calon_pp3b_qu, val_dk_no3b, cl_form_calon_tt1_3b_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no3b, iv_circle_calon_pp3b_qu, val_dk_no3b, cl_form_calon_tt2_3b_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no3b, iv_circle_calon_pp3b_qu, val_dk_no3b, cl_form_calon_tt3_3b_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxxv()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno3c, iv_circle_calon_pp3c_qu, val_dk_no3c, cl_form_calon_pp3c_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno3c, iv_circle_calon_pp3c_qu, val_dk_no3c, cl_form_calon_tt3c_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no3c, iv_circle_calon_pp3c_qu, val_dk_no3c, cl_form_calon_tt1_3c_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no3c, iv_circle_calon_pp3c_qu, val_dk_no3c, cl_form_calon_tt2_3c_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no3c, iv_circle_calon_pp3c_qu, val_dk_no3c, cl_form_calon_tt3_3c_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxxvi()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno3d, iv_circle_calon_pp3d_qu, val_dk_no3d, cl_form_calon_pp3d_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno3d, iv_circle_calon_pp3d_qu, val_dk_no3d, cl_form_calon_tt3d_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no3d, iv_circle_calon_pp3d_qu, val_dk_no3d, cl_form_calon_tt1_3d_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no3d, iv_circle_calon_pp3d_qu, val_dk_no3d, cl_form_calon_tt2_3d_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no3d, iv_circle_calon_pp3d_qu, val_dk_no3d, cl_form_calon_tt3_3d_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxxvii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno4, iv_circle_calon_pp4_qu, val_dk_no4, cl_form_calon_pp4_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno4, iv_circle_calon_pp4_qu, val_dk_no4, cl_form_calon_tt4_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no4, iv_circle_calon_pp4_qu, val_dk_no4, cl_form_calon_tt1_4_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no4, iv_circle_calon_pp4_qu, val_dk_no4, cl_form_calon_tt2_4_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no4, iv_circle_calon_pp4_qu, val_dk_no4, cl_form_calon_tt3_4_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxxviii()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno5, iv_circle_calon_pp5_qu, val_dk_no5, cl_form_calon_pp5_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno5, iv_circle_calon_pp5_qu, val_dk_no5, cl_form_calon_tt5_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no5, iv_circle_calon_pp5_qu, val_dk_no5, cl_form_calon_tt1_5_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no5, iv_circle_calon_pp5_qu, val_dk_no5, cl_form_calon_tt2_5_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no5, iv_circle_calon_pp5_qu, val_dk_no5, cl_form_calon_tt3_5_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxxix()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno6, iv_circle_calon_pp6_qu, val_dk_no6, cl_form_calon_pp6_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno6, iv_circle_calon_pp6_qu, val_dk_no6, cl_form_calon_tt6_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no6, iv_circle_calon_pp6_qu, val_dk_no6, cl_form_calon_tt1_6_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no6, iv_circle_calon_pp6_qu, val_dk_no6, cl_form_calon_tt2_6_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no6, iv_circle_calon_pp6_qu, val_dk_no6, cl_form_calon_tt3_6_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxxx()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno7, iv_circle_calon_pp7_qu, val_dk_no7, cl_form_calon_pp7_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno7, iv_circle_calon_pp7_qu, val_dk_no7, cl_form_calon_tt7_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no7, iv_circle_calon_pp7_qu, val_dk_no7, cl_form_calon_tt1_7_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no7, iv_circle_calon_pp7_qu, val_dk_no7, cl_form_calon_tt2_7_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no7, iv_circle_calon_pp7_qu, val_dk_no7, cl_form_calon_tt3_7_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private boolean questionareTask4xxxxi()  {
        Boolean checked = false;
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ppno9, iv_circle_calon_pp9_qu, val_dk_no9, cl_form_calon_pp9_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_ttno9, iv_circle_calon_pp9_qu, val_dk_no9, cl_form_calon_tt9_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt1no9, iv_circle_calon_pp9_qu, val_dk_no9, cl_form_calon_tt1_9_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt2no9, iv_circle_calon_pp9_qu, val_dk_no9, cl_form_calon_tt2_9_qu, getContext())) {
            return checked;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_dk_tt3no9, iv_circle_calon_pp9_qu, val_dk_no9, cl_form_calon_tt3_9_qu, getContext())) {
            return checked;
        }
        return true;
    }

    private void validateTask4() {
        if (!questionareTask4xxvii()){
            setMessageTask("xxvii");
            return;
        }
        if (!questionareTask4xxviii()){
            setMessageTask("xxviii");
            return;
        }
        if (!questionareTask4xxix()){
            setMessageTask("xxix");
            return;
        }
        if (!questionareTask4xxx()){
            setMessageTask("xxx");
            return;
        }
        if (!questionareTask4xxxi()){
            setMessageTask("xxxi");
            return;
        }
        if (!questionareTask4xxxii()){
            setMessageTask("xxxii");
            return;
        }
        if (!questionareTask4xxxiii()){
            setMessageTask("xxxiii");
            return;
        }
        if (!questionareTask4xxxiv()){
            setMessageTask("xxxiv");
            return;
        }
        if (!questionareTask4xxxv()){
            setMessageTask("xxxv");
            return;
        }
        if (!questionareTask4xxxvi()){
            setMessageTask("xxxvi");
            return;
        }
        if (!questionareTask4xxxvii()){
            setMessageTask("xxxvii");
            return;
        }
        if (!questionareTask4xxxviii()){
            setMessageTask("xxxviii");
            return;
        }
        if (!questionareTask4xxxix()){
            setMessageTask("xxxix");
            return;
        }
        if (!questionareTask4xxxx()){
            setMessageTask("xxxx");
            return;
        }
        if (!questionareTask4xxxxi()){
            setMessageTask("xxxxi");
            return;
        }
        loadview();
        MyTask4 task4 = new MyTask4();
        task4.execute();
    }

    private void validateTask3() {
        if (!questionareTask3i()){
            setMessageTask("i");
            return;
        }
        if (!questionareTask3ii() ){
            setMessageTask("ii");
            return;
        }
        if (!questionareTask3iii()){
            setMessageTask("iii");
            return;
        }
        if (!questionareTask3iv()){
            setMessageTask("iv");
            return;
        }
        if (!questionareTask3v()){
            setMessageTask("v");
            return;
        }
        if (!questionareTask3vi()){
            setMessageTask("vi");
            return;
        }
        if (!questionareTask3vii()){
            setMessageTask("vii");
            return;
        }
        if (!questionareTask3viii()){
            setMessageTask("viii");
            return;
        }
        if (!questionareTask3ix()){
            setMessageTask("ix");
            return;
        }
        if (!questionareTask3x()){
            setMessageTask("x");
            return;
        }
        if (!questionareTask3xi()){
            setMessageTask("xi");
            return;
        }
        if (!questionareTask3xii()){
            setMessageTask("xii");
            return;
        }
        if (!questionareTask3xiii()){
            setMessageTask("xiii");
            return;
        }
        if (!questionareTask3xiv()){
            setMessageTask("xiv");
            return;
        }
        if (!questionareTask3xv()){
            setMessageTask("xv");
            return;
        }
        if (!questionareTask3xvi()){
            setMessageTask("xvi");
            return;
        }
        if (!questionareTask3xvii()){
            setMessageTask("xvii");
            return;
        }
        if (!questionareTask3xviii()){
            setMessageTask("xviii");
            return;
        }
        if (!questionareTask3xix()){
            setMessageTask("xix");
            return;
        }
        if (!questionareTask3xx()){
            setMessageTask("xx");
            return;
        }
        if (!questionareTask3xxi()){
            setMessageTask("xxi");
            return;
        }
        if (!questionareTask3xxii()){
            setMessageTask("xxii");
            return;
        }
        if (!questionareTask3xxiii()){
            setMessageTask("xxiii");
            return;
        }
        if (!questionareTask3xxiv()){
            setMessageTask("xxiv");
            return;
        }
        if (!questionareTask3xxv()){
            setMessageTask("xxv");
            return;
        }
        if (!questionareTask3xxvi()){
            setMessageTask("xxvi");
            return;
        }
        MyTask3 task3 = new MyTask3();
        task3.execute();
    }

    private void setMessageTask (String number) {
        Toast.makeText(getActivity(), "Pertanyaan untuk  "+number+" masih belum diisi", Toast.LENGTH_SHORT).show();
    }

    @BindView(R.id.val_tt_no3)
    TextView val_tt_no3;
    @BindView(R.id.val_tt_no5)
    TextView val_tt_no5;
    @BindView(R.id.val_tt_no8)
    TextView val_tt_no8;
    @BindView(R.id.val_tt_no8b)
    TextView val_tt_no8b;
    @BindView(R.id.val_tt_no9)
    TextView val_tt_no9;

    @BindView(R.id.val_dk_no1l)
    TextView val_dk_no1l;
    @BindView(R.id.val_dk_no1ii)
    TextView val_dk_no1ii;
    @BindView(R.id.val_dk_no1iii)
    TextView val_dk_no1iii;
    @BindView(R.id.val_dk_no1iv)
    TextView val_dk_no1iv;
    @BindView(R.id.val_dk_no1v)
    TextView val_dk_no1v;
    @BindView(R.id.val_dk_no1vi)
    TextView val_dk_no1vi;
    @BindView(R.id.val_dk_no1vii)
    TextView val_dk_no1vii;
    @BindView(R.id.val_dk_no1viii)
    TextView val_dk_no1viii;
    @BindView(R.id.val_dk_no1ix)
    TextView val_dk_no1ix;
    @BindView(R.id.val_dk_no1x)
    TextView val_dk_no1x;
    @BindView(R.id.val_dk_no1xi)
    TextView val_dk_no1xi;
    @BindView(R.id.val_dk_no1xii)
    TextView val_dk_no1xii;
    @BindView(R.id.val_dk_no1xiii)
    TextView val_dk_no1xiii;
    @BindView(R.id.val_dk_no1xiv)
    TextView val_dk_no1xiv;
    @BindView(R.id.val_dk_no1xv)
    TextView val_dk_no1xv;
    @BindView(R.id.val_dk_no1xvi)
    TextView val_dk_no1xvi;
    @BindView(R.id.val_dk_no1xvii)
    TextView val_dk_no1xvii;
    @BindView(R.id.val_dk_no1xviii)
    TextView val_dk_no1xviii;
    @BindView(R.id.val_dk_no1xix)
    TextView val_dk_no1xix;
    @BindView(R.id.val_dk_no1xx)
    TextView val_dk_no1xx;
    @BindView(R.id.val_dk_no1xxi)
    TextView val_dk_no1xxi;
    @BindView(R.id.val_dk_no1xxii)
    TextView val_dk_no1xxii;
    @BindView(R.id.val_dk_no1xxiii)
    TextView val_dk_no1xxiii;
    @BindView(R.id.val_dk_no1xxiv)
    TextView val_dk_no1xxiv;
    @BindView(R.id.val_dk_no1xxv)
    TextView val_dk_no1xxv;

    @BindView(R.id.val_dk_no1xxvi)
    TextView val_dk_no1xxvi;
    @BindView(R.id.val_dk_no1xxvii)
    TextView val_dk_no1xxvii;
    @BindView(R.id.val_dk_no1xxviii)
    TextView val_dk_no1xxviii;
    @BindView(R.id.val_dk_no1xxix)
    TextView val_dk_no1xxix;
    @BindView(R.id.val_dk_no1xxx)
    TextView val_dk_no1xxx;
    @BindView(R.id.val_dk_no1xxxi)
    TextView val_dk_no1xxxi;
    @BindView(R.id.val_dk_no2)
    TextView val_dk_no2;
    @BindView(R.id.val_dk_no3a)
    TextView val_dk_no3a;
    @BindView(R.id.val_dk_no3b)
    TextView val_dk_no3b;
    @BindView(R.id.val_dk_no3c)
    TextView val_dk_no3c;
    @BindView(R.id.val_dk_no3d)
    TextView val_dk_no3d;
    @BindView(R.id.val_dk_no4)
    TextView val_dk_no4;
    @BindView(R.id.val_dk_no5)
    TextView val_dk_no5;
    @BindView(R.id.val_dk_no6)
    TextView val_dk_no6;
    @BindView(R.id.val_dk_no7)
    TextView val_dk_no7;
    @BindView(R.id.val_dk_no9)
    TextView val_dk_no9;
    @BindView(R.id.tv_data_calon_tertanggung_qu)
    TextView tv_data_calon_tertanggung_qu;
    @BindView(R.id.tv_error_questionnaire1_qu)
    TextView tv_error_questionnaire1_qu;
    @BindView(R.id.tv_error_questionnaire2_qu)
    TextView tv_error_questionnaire2_qu;
    @BindView(R.id.tv_tambah_data_qu)
    TextView tv_tambah_data_qu;
    @BindView(R.id.tv_error_questionnaire4_qu)
    TextView tv_error_questionnaire4_qu;
    @BindView(R.id.tv_tambah_data2_qu)
    TextView tv_tambah_data2_qu;
    @BindView(R.id.tv_error_questionnaire6_qu)
    TextView tv_error_questionnaire6_qu;
    @BindView(R.id.tv_error_questionnaire7_qu)
    TextView tv_error_questionnaire7_qu;
    @BindView(R.id.tv_error_questionnaire8a1_qu)
    TextView tv_error_questionnaire8a1_qu;
    @BindView(R.id.tv_error_questionnaire8a2_qu)
    TextView tv_error_questionnaire8a2_qu;
    @BindView(R.id.tv_error_questionnaire10_qu)
    TextView tv_error_questionnaire10_qu;
    @BindView(R.id.tv_error_tinggi_badan_qu)
    TextView tv_error_tinggi_badan_qu;
    @BindView(R.id.tv_error_berat_badan_qu)
    TextView tv_error_berat_badan_qu;
    @BindView(R.id.tv_error_questionnaire11b_qu)
    TextView tv_error_questionnaire11b_qu;
    @BindView(R.id.tv_error_tinggi_badan12_qu)
    TextView tv_error_tinggi_badan12_qu;
    @BindView(R.id.tv_data_calon_pp_qu)
    TextView tv_data_calon_pp_qu;
    @BindView(R.id.tv_error_questionnaire13_qu)
    TextView tv_error_questionnaire13_qu;
    @BindView(R.id.tv_error_questionnaire13a_qu)
    TextView tv_error_questionnaire13a_qu;
    @BindView(R.id.tv_error_questionnaire14_qu)
    TextView tv_error_questionnaire14_qu;
    @BindView(R.id.tv_error_questionnaire14a_qu)
    TextView tv_error_questionnaire14a_qu;
    @BindView(R.id.tv_error_tinggi_badan2_qu)
    TextView tv_error_tinggi_badan2_qu;
    @BindView(R.id.tv_error_berat_badan2_qu)
    TextView tv_error_berat_badan2_qu;
    @BindView(R.id.tv_data_kesehatan_qu)
    TextView tv_data_kesehatan_qu;
    @BindView(R.id.tv_tambah_data_calon_pemegang_polis_qu)
    TextView tv_tambah_data_calon_pemegang_polis_qu;
    @BindView(R.id.tv_tambah_data_calon_tertanggung_qu)
    TextView tv_tambah_data_calon_tertanggung_qu;
    @BindView(R.id.tv_tambah_data_keterangan_kesehatan_qu)
    TextView tv_tambah_data_keterangan_kesehatan_qu;
    @BindView(R.id.tv_error_essay_dk_no7a_qu)
    TextView tv_error_essay_dk_no7a_qu;
    @BindView(R.id.tv_error_essay_dk_no7b_qu)
    TextView tv_error_essay_dk_no7b_qu;
    @BindView(R.id.tv_error_essay_dk_no7c_qu)
    TextView tv_error_essay_dk_no7c_qu;
    @BindView(R.id.tv_error_essay_dk_no7d_qu)
    TextView tv_error_essay_dk_no7d_qu;

    @BindView(R.id.qu1_text)
    TextView qu1_text;
    @BindView(R.id.qu3_text)
    TextView qu3_text;
    @BindView(R.id.qu4_text)
    TextView qu4_text;
    @BindView(R.id.qu5_text)
    TextView qu5_text;
    @BindView(R.id.qu6_text)
    TextView qu6_text;

    @BindView(R.id.cl_child_qu)
    ConstraintLayout cl_child_qu;
    @BindView(R.id.layout_vis_tt)
    ConstraintLayout layout_vis_tt;
    @BindView(R.id.cl_data_calon_tertanggung_qu)
    ConstraintLayout cl_data_calon_tertanggung_qu;
    @BindView(R.id.cl_form_data_calon_tertanggung_qu)
    ConstraintLayout cl_form_data_calon_tertanggung_qu;
    @BindView(R.id.cl_questionnaire1_qu)
    ConstraintLayout cl_questionnaire1_qu;
    @BindView(R.id.cl_questionnaire1_isi_qu)
    ConstraintLayout cl_questionnaire1_isi_qu;
    @BindView(R.id.cl_questionnaire2_qu)
    ConstraintLayout cl_questionnaire2_qu;
    @BindView(R.id.cl_questionnaire2_isi_qu)
    ConstraintLayout cl_questionnaire2_isi_qu;
    @BindView(R.id.cl_questionnaire3_qu)
    ConstraintLayout cl_questionnaire3_qu;
    @BindView(R.id.cl_questionnaire3_isi_qu)
    ConstraintLayout cl_questionnaire3_isi_qu;
    @BindView(R.id.cl_tambah_data_qu)
    ConstraintLayout cl_tambah_data_qu;
    @BindView(R.id.cl_form_questionnaire4_qu)
    ConstraintLayout cl_form_questionnaire4_qu;
    @BindView(R.id.cl_questionnaire4_qu)
    ConstraintLayout cl_questionnaire4_qu;
    @BindView(R.id.cl_questionnaire4_isi_qu)
    ConstraintLayout cl_questionnaire4_isi_qu;
    @BindView(R.id.cl_questionnaire5_qu)
    ConstraintLayout cl_questionnaire5_qu;
    @BindView(R.id.cl_questionnaire5_isi_qu)
    ConstraintLayout cl_questionnaire5_isi_qu;
    @BindView(R.id.cl_tambah_data2_qu)
    ConstraintLayout cl_tambah_data2_qu;
    @BindView(R.id.cl_form_questionnaire6_qu)
    ConstraintLayout cl_form_questionnaire6_qu;
    @BindView(R.id.cl_questionnaire6_qu)
    ConstraintLayout cl_questionnaire6_qu;
    @BindView(R.id.cl_questionnaire6_isi_qu)
    ConstraintLayout cl_questionnaire6_isi_qu;
    @BindView(R.id.cl_questionnaire7_qu)
    ConstraintLayout cl_questionnaire7_qu;
    @BindView(R.id.cl_questionnaire7_isi_qu)
    ConstraintLayout cl_questionnaire7_isi_qu;
    @BindView(R.id.cl_questionnaire8_qu)
    ConstraintLayout cl_questionnaire8_qu;
    @BindView(R.id.cl_questionnaire8_isi_qu)
    ConstraintLayout cl_questionnaire8_isi_qu;
    @BindView(R.id.cl_questionnaire8a_qu)
    ConstraintLayout cl_questionnaire8a_qu;
    @BindView(R.id.cl_questionnaire8a_isi_qu)
    ConstraintLayout cl_questionnaire8a_isi_qu;
    @BindView(R.id.cl_questionnaire8b_qu)
    ConstraintLayout cl_questionnaire8b_qu;
    @BindView(R.id.cl_questionnaire8b_isi_qu)
    ConstraintLayout cl_questionnaire8b_isi_qu;
    @BindView(R.id.cl_questionnaire9_qu)
    ConstraintLayout cl_questionnaire9_qu;
    @BindView(R.id.cl_questionnaire9_isi_qu)
    ConstraintLayout cl_questionnaire9_isi_qu;
    @BindView(R.id.cl_questionnaire10_qu)
    ConstraintLayout cl_questionnaire10_qu;
    @BindView(R.id.cl_questionnaire10_isi_qu)
    ConstraintLayout cl_questionnaire10_isi_qu;
    @BindView(R.id.cl_questionnaire11a_qu)
    ConstraintLayout cl_questionnaire11a_qu;
    @BindView(R.id.cl_questionnaire11a_isi_qu)
    ConstraintLayout cl_questionnaire11a_isi_qu;
    @BindView(R.id.cl_tinggi_badan_qu)
    ConstraintLayout cl_tinggi_badan_qu;
    @BindView(R.id.cl_berat_badan_qu)
    ConstraintLayout cl_berat_badan_qu;
    @BindView(R.id.cl_form_questionnaire11b_qu)
    ConstraintLayout cl_form_questionnaire11b_qu;
    @BindView(R.id.cl_questionnaire11b_qu)
    ConstraintLayout cl_questionnaire11b_qu;
    @BindView(R.id.cl_questionnaire11b_isi_qu)
    ConstraintLayout cl_questionnaire11b_isi_qu;
    @BindView(R.id.cl_questionnaire12_qu)
    ConstraintLayout cl_questionnaire12_qu;
    @BindView(R.id.cl_questionnaire12_isi_qu)
    ConstraintLayout cl_questionnaire12_isi_qu;
    @BindView(R.id.cl_tinggi_badan12_qu)
    ConstraintLayout cl_tinggi_badan12_qu;
    @BindView(R.id.cl_berat_badan12_qu)
    ConstraintLayout cl_berat_badan12_qu;

//    @BindView(R.id.cl_bt_kembali_qu)
//    ConstraintLayout cl_bt_kembali_qu;
//    @BindView(R.id.cl_bt_lanjut_qu)
//    ConstraintLayout cl_bt_lanjut_qu;

    @BindView(R.id.cl_data_calon_pp_qu)
    ConstraintLayout cl_data_calon_pp_qu;
    @BindView(R.id.cl_form_data_calon_pp_qu)
    ConstraintLayout cl_form_data_calon_pp_qu;
    @BindView(R.id.cl_questionnaire13_qu)
    ConstraintLayout cl_questionnaire13_qu;
    @BindView(R.id.cl_questionnaire13_isi_qu)
    ConstraintLayout cl_questionnaire13_isi_qu;
    @BindView(R.id.cl_questionnaire13a_qu)
    ConstraintLayout cl_questionnaire13a_qu;
    @BindView(R.id.cl_questionnaire13a_isi_qu)
    ConstraintLayout cl_questionnaire13a_isi_qu;
    @BindView(R.id.cl_questionnaire14_qu)
    ConstraintLayout cl_questionnaire14_qu;
    @BindView(R.id.cl_questionnaire14_isi_qu)
    ConstraintLayout cl_questionnaire14_isi_qu;
    @BindView(R.id.cl_questionnaire14a_qu)
    ConstraintLayout cl_questionnaire14a_qu;
    @BindView(R.id.cl_questionnaire14a_isi_qu)
    ConstraintLayout cl_questionnaire14a_isi_qu;
    @BindView(R.id.cl_questionnaire15_qu)
    ConstraintLayout cl_questionnaire15_qu;
    @BindView(R.id.cl_questionnaire15_isi_qu)
    ConstraintLayout cl_questionnaire15_isi_qu;
    @BindView(R.id.cl_tinggi_badan2_qu)
    ConstraintLayout cl_tinggi_badan2_qu;
    @BindView(R.id.cl_berat_badan2_qu)
    ConstraintLayout cl_berat_badan2_qu;
    @BindView(R.id.cl_data_kesehatan_qu)
    ConstraintLayout cl_data_kesehatan_qu;
    @BindView(R.id.cl_form_data_kesehatan1_qu)
    ConstraintLayout cl_form_data_kesehatan1_qu;
    @BindView(R.id.cl_data_kesehatan1_qu)
    ConstraintLayout cl_data_kesehatan1_qu;
    @BindView(R.id.cl_data_kesehatan1_isi_qu)
    ConstraintLayout cl_data_kesehatan1_isi_qu;
    @BindView(R.id.cl_data_kesehatani_qu)
    ConstraintLayout cl_data_kesehatani_qu;
    @BindView(R.id.cl_data_kesehatani_isi_qu)
    ConstraintLayout cl_data_kesehatani_isi_qu;
    @BindView(R.id.cl_form_calon_ppi_qu)
    ConstraintLayout cl_form_calon_ppi_qu;
    @BindView(R.id.cl_form_calon_tti_qu)
    ConstraintLayout cl_form_calon_tti_qu;
    @BindView(R.id.cl_form_calon_tt1_i_qu)
    ConstraintLayout cl_form_calon_tt1_i_qu;
    @BindView(R.id.cl_form_calon_tt2_i_qu)
    ConstraintLayout cl_form_calon_tt2_i_qu;
    @BindView(R.id.cl_form_calon_tt3_i_qu)
    ConstraintLayout cl_form_calon_tt3_i_qu;
    @BindView(R.id.cl_form_data_kesehatanii_qu)
    ConstraintLayout cl_form_data_kesehatanii_qu;
    @BindView(R.id.cl_data_kesehatanii_qu)
    ConstraintLayout cl_data_kesehatanii_qu;
    @BindView(R.id.cl_data_kesehatanii_isi_qu)
    ConstraintLayout cl_data_kesehatanii_isi_qu;
    @BindView(R.id.cl_form_calon_ppii_qu)
    ConstraintLayout cl_form_calon_ppii_qu;
    @BindView(R.id.cl_form_calon_ttii_qu)
    ConstraintLayout cl_form_calon_ttii_qu;
    @BindView(R.id.cl_form_calon_tt1_ii_qu)
    ConstraintLayout cl_form_calon_tt1_ii_qu;
    @BindView(R.id.cl_form_calon_tt2_ii_qu)
    ConstraintLayout cl_form_calon_tt2_ii_qu;
    @BindView(R.id.cl_form_calon_tt3_ii_qu)
    ConstraintLayout cl_form_calon_tt3_ii_qu;
    @BindView(R.id.cl_form_data_kesehataniii_qu)
    ConstraintLayout cl_form_data_kesehataniii_qu;
    @BindView(R.id.cl_data_kesehataniii_qu)
    ConstraintLayout cl_data_kesehataniii_qu;
    @BindView(R.id.cl_data_kesehataniii_isi_qu)
    ConstraintLayout cl_data_kesehataniii_isi_qu;
    @BindView(R.id.cl_form_calon_ppiii_qu)
    ConstraintLayout cl_form_calon_ppiii_qu;
    @BindView(R.id.cl_form_calon_ttiii_qu)
    ConstraintLayout cl_form_calon_ttiii_qu;
    @BindView(R.id.cl_form_calon_tt1_iii_qu)
    ConstraintLayout cl_form_calon_tt1_iii_qu;
    @BindView(R.id.cl_form_calon_tt2_iii_qu)
    ConstraintLayout cl_form_calon_tt2_iii_qu;
    @BindView(R.id.cl_form_calon_tt3_iii_qu)
    ConstraintLayout cl_form_calon_tt3_iii_qu;
    @BindView(R.id.cl_form_data_kesehataniv_qu)
    ConstraintLayout cl_form_data_kesehataniv_qu;
    @BindView(R.id.cl_data_kesehataniv_qu)
    ConstraintLayout cl_data_kesehataniv_qu;
    @BindView(R.id.cl_data_kesehataniv_isi_qu)
    ConstraintLayout cl_data_kesehataniv_isi_qu;
    @BindView(R.id.cl_form_calon_ppiv_qu)
    ConstraintLayout cl_form_calon_ppiv_qu;
    @BindView(R.id.cl_form_calon_ttiv_qu)
    ConstraintLayout cl_form_calon_ttiv_qu;
    @BindView(R.id.cl_form_calon_tt1_iv_qu)
    ConstraintLayout cl_form_calon_tt1_iv_qu;
    @BindView(R.id.cl_form_calon_tt2_iv_qu)
    ConstraintLayout cl_form_calon_tt2_iv_qu;
    @BindView(R.id.cl_form_calon_tt3_iv_qu)
    ConstraintLayout cl_form_calon_tt3_iv_qu;
    @BindView(R.id.cl_form_data_kesehatanv_qu)
    ConstraintLayout cl_form_data_kesehatanv_qu;
    @BindView(R.id.cl_data_kesehatanv_qu)
    ConstraintLayout cl_data_kesehatanv_qu;
    @BindView(R.id.cl_data_kesehatanv_isi_qu)
    ConstraintLayout cl_data_kesehatanv_isi_qu;
    @BindView(R.id.cl_form_calon_ppv_qu)
    ConstraintLayout cl_form_calon_ppv_qu;
    @BindView(R.id.cl_form_calon_ttv_qu)
    ConstraintLayout cl_form_calon_ttv_qu;
    @BindView(R.id.cl_form_calon_tt1_v_qu)
    ConstraintLayout cl_form_calon_tt1_v_qu;
    @BindView(R.id.cl_form_calon_tt2_v_qu)
    ConstraintLayout cl_form_calon_tt2_v_qu;
    @BindView(R.id.cl_form_calon_tt3_v_qu)
    ConstraintLayout cl_form_calon_tt3_v_qu;
    @BindView(R.id.cl_form_data_kesehatanvi_qu)
    ConstraintLayout cl_form_data_kesehatanvi_qu;
    @BindView(R.id.cl_data_kesehatanvi_qu)
    ConstraintLayout cl_data_kesehatanvi_qu;
    @BindView(R.id.cl_data_kesehatanvi_isi_qu)
    ConstraintLayout cl_data_kesehatanvi_isi_qu;
    @BindView(R.id.cl_form_calon_ppvi_qu)
    ConstraintLayout cl_form_calon_ppvi_qu;
    @BindView(R.id.cl_form_calon_ttvi_qu)
    ConstraintLayout cl_form_calon_ttvi_qu;
    @BindView(R.id.cl_form_calon_tt1_vi_qu)
    ConstraintLayout cl_form_calon_tt1_vi_qu;
    @BindView(R.id.cl_form_calon_tt2_vi_qu)
    ConstraintLayout cl_form_calon_tt2_vi_qu;
    @BindView(R.id.cl_form_calon_tt3_vi_qu)
    ConstraintLayout cl_form_calon_tt3_vi_qu;
    @BindView(R.id.cl_form_data_kesehatanvii_qu)
    ConstraintLayout cl_form_data_kesehatanvii_qu;
    @BindView(R.id.cl_data_kesehatanvii_qu)
    ConstraintLayout cl_data_kesehatanvii_qu;
    @BindView(R.id.cl_data_kesehatanvii_isi_qu)
    ConstraintLayout cl_data_kesehatanvii_isi_qu;
    @BindView(R.id.cl_form_calon_ppvii_qu)
    ConstraintLayout cl_form_calon_ppvii_qu;
    @BindView(R.id.cl_form_calon_ttvii_qu)
    ConstraintLayout cl_form_calon_ttvii_qu;
    @BindView(R.id.cl_form_calon_tt1_vii_qu)
    ConstraintLayout cl_form_calon_tt1_vii_qu;
    @BindView(R.id.cl_form_calon_tt2_vii_qu)
    ConstraintLayout cl_form_calon_tt2_vii_qu;
    @BindView(R.id.cl_form_calon_tt3_vii_qu)
    ConstraintLayout cl_form_calon_tt3_vii_qu;
    @BindView(R.id.cl_form_data_kesehatanviii_qu)
    ConstraintLayout cl_form_data_kesehatanviii_qu;
    @BindView(R.id.cl_data_kesehatanviii_qu)
    ConstraintLayout cl_data_kesehatanviii_qu;
    @BindView(R.id.cl_data_kesehatanviii_isi_qu)
    ConstraintLayout cl_data_kesehatanviii_isi_qu;
    @BindView(R.id.cl_form_calon_ppviii_qu)
    ConstraintLayout cl_form_calon_ppviii_qu;
    @BindView(R.id.cl_form_calon_ttviii_qu)
    ConstraintLayout cl_form_calon_ttviii_qu;
    @BindView(R.id.cl_form_calon_tt1_viii_qu)
    ConstraintLayout cl_form_calon_tt1_viii_qu;
    @BindView(R.id.cl_form_calon_tt2_viii_qu)
    ConstraintLayout cl_form_calon_tt2_viii_qu;
    @BindView(R.id.cl_form_calon_tt3_viii_qu)
    ConstraintLayout cl_form_calon_tt3_viii_qu;
    @BindView(R.id.cl_form_data_kesehatanix_qu)
    ConstraintLayout cl_form_data_kesehatanix_qu;
    @BindView(R.id.cl_data_kesehatanix_qu)
    ConstraintLayout cl_data_kesehatanix_qu;
    @BindView(R.id.cl_data_kesehatanix_isi_qu)
    ConstraintLayout cl_data_kesehatanix_isi_qu;
    @BindView(R.id.cl_form_calon_ppix_qu)
    ConstraintLayout cl_form_calon_ppix_qu;
    @BindView(R.id.cl_form_calon_ttix_qu)
    ConstraintLayout cl_form_calon_ttix_qu;
    @BindView(R.id.cl_form_calon_tt1_ix_qu)
    ConstraintLayout cl_form_calon_tt1_ix_qu;
    @BindView(R.id.cl_form_calon_tt2_ix_qu)
    ConstraintLayout cl_form_calon_tt2_ix_qu;
    @BindView(R.id.cl_form_calon_tt3_ix_qu)
    ConstraintLayout cl_form_calon_tt3_ix_qu;
    @BindView(R.id.cl_form_data_kesehatanx_qu)
    ConstraintLayout cl_form_data_kesehatanx_qu;
    @BindView(R.id.cl_data_kesehatanx_qu)
    ConstraintLayout cl_data_kesehatanx_qu;
    @BindView(R.id.cl_data_kesehatanx_isi_qu)
    ConstraintLayout cl_data_kesehatanx_isi_qu;
    @BindView(R.id.cl_form_calon_ppx_qu)
    ConstraintLayout cl_form_calon_ppx_qu;
    @BindView(R.id.cl_form_calon_ttx_qu)
    ConstraintLayout cl_form_calon_ttx_qu;
    @BindView(R.id.cl_form_calon_tt1_x_qu)
    ConstraintLayout cl_form_calon_tt1_x_qu;
    @BindView(R.id.cl_form_calon_tt2_x_qu)
    ConstraintLayout cl_form_calon_tt2_x_qu;
    @BindView(R.id.cl_form_calon_tt3_x_qu)
    ConstraintLayout cl_form_calon_tt3_x_qu;
    @BindView(R.id.cl_form_data_kesehatanxi_qu)
    ConstraintLayout cl_form_data_kesehatanxi_qu;
    @BindView(R.id.cl_data_kesehatanxi_qu)
    ConstraintLayout cl_data_kesehatanxi_qu;
    @BindView(R.id.cl_data_kesehatanxi_isi_qu)
    ConstraintLayout cl_data_kesehatanxi_isi_qu;
    @BindView(R.id.cl_form_calon_ppxi_qu)
    ConstraintLayout cl_form_calon_ppxi_qu;
    @BindView(R.id.cl_form_calon_ttxi_qu)
    ConstraintLayout cl_form_calon_ttxi_qu;
    @BindView(R.id.cl_form_calon_tt1_xi_qu)
    ConstraintLayout cl_form_calon_tt1_xi_qu;
    @BindView(R.id.cl_form_calon_tt2_xi_qu)
    ConstraintLayout cl_form_calon_tt2_xi_qu;
    @BindView(R.id.cl_form_calon_tt3_xi_qu)
    ConstraintLayout cl_form_calon_tt3_xi_qu;
    @BindView(R.id.cl_form_data_kesehatanxii_qu)
    ConstraintLayout cl_form_data_kesehatanxii_qu;
    @BindView(R.id.cl_data_kesehatanxii_qu)
    ConstraintLayout cl_data_kesehatanxii_qu;
    @BindView(R.id.cl_data_kesehatanxii_isi_qu)
    ConstraintLayout cl_data_kesehatanxii_isi_qu;
    @BindView(R.id.cl_form_calon_ppxii_qu)
    ConstraintLayout cl_form_calon_ppxii_qu;
    @BindView(R.id.cl_form_calon_ttxii_qu)
    ConstraintLayout cl_form_calon_ttxii_qu;
    @BindView(R.id.cl_form_calon_tt1_xii_qu)
    ConstraintLayout cl_form_calon_tt1_xii_qu;
    @BindView(R.id.cl_form_calon_tt2_xii_qu)
    ConstraintLayout cl_form_calon_tt2_xii_qu;
    @BindView(R.id.cl_form_calon_tt3_xii_qu)
    ConstraintLayout cl_form_calon_tt3_xii_qu;
    @BindView(R.id.cl_form_data_kesehatanxiii_qu)
    ConstraintLayout cl_form_data_kesehatanxiii_qu;
    @BindView(R.id.cl_data_kesehatanxiii_qu)
    ConstraintLayout cl_data_kesehatanxiii_qu;
    @BindView(R.id.cl_data_kesehatanxiii_isi_qu)
    ConstraintLayout cl_data_kesehatanxiii_isi_qu;
    @BindView(R.id.cl_form_calon_ppxiii_qu)
    ConstraintLayout cl_form_calon_ppxiii_qu;
    @BindView(R.id.cl_form_calon_ttxiii_qu)
    ConstraintLayout cl_form_calon_ttxiii_qu;
    @BindView(R.id.cl_form_calon_tt1_xiii_qu)
    ConstraintLayout cl_form_calon_tt1_xiii_qu;
    @BindView(R.id.cl_form_calon_tt2_xiii_qu)
    ConstraintLayout cl_form_calon_tt2_xiii_qu;
    @BindView(R.id.cl_form_calon_tt3_xiii_qu)
    ConstraintLayout cl_form_calon_tt3_xiii_qu;
    @BindView(R.id.cl_form_data_kesehatanxiv_qu)
    ConstraintLayout cl_form_data_kesehatanxiv_qu;
    @BindView(R.id.cl_data_kesehatanxiv_qu)
    ConstraintLayout cl_data_kesehatanxiv_qu;
    @BindView(R.id.cl_data_kesehatanxiv_isi_qu)
    ConstraintLayout cl_data_kesehatanxiv_isi_qu;
    @BindView(R.id.cl_form_calon_ppxiv_qu)
    ConstraintLayout cl_form_calon_ppxiv_qu;
    @BindView(R.id.cl_form_calon_ttxiv_qu)
    ConstraintLayout cl_form_calon_ttxiv_qu;
    @BindView(R.id.cl_form_calon_tt1_xiv_qu)
    ConstraintLayout cl_form_calon_tt1_xiv_qu;
    @BindView(R.id.cl_form_calon_tt2_xiv_qu)
    ConstraintLayout cl_form_calon_tt2_xiv_qu;
    @BindView(R.id.cl_form_calon_tt3_xiv_qu)
    ConstraintLayout cl_form_calon_tt3_xiv_qu;
    @BindView(R.id.cl_form_data_kesehatanxv_qu)
    ConstraintLayout cl_form_data_kesehatanxv_qu;
    @BindView(R.id.cl_data_kesehatanxv_qu)
    ConstraintLayout cl_data_kesehatanxv_qu;
    @BindView(R.id.cl_data_kesehatanxv_isi_qu)
    ConstraintLayout cl_data_kesehatanxv_isi_qu;
    @BindView(R.id.cl_form_calon_ppxv_qu)
    ConstraintLayout cl_form_calon_ppxv_qu;
    @BindView(R.id.cl_form_calon_ttxv_qu)
    ConstraintLayout cl_form_calon_ttxv_qu;
    @BindView(R.id.cl_form_calon_tt1_xv_qu)
    ConstraintLayout cl_form_calon_tt1_xv_qu;
    @BindView(R.id.cl_form_calon_tt2_xv_qu)
    ConstraintLayout cl_form_calon_tt2_xv_qu;
    @BindView(R.id.cl_form_calon_tt3_xv_qu)
    ConstraintLayout cl_form_calon_tt3_xv_qu;
    @BindView(R.id.cl_form_data_kesehatanxvi_qu)
    ConstraintLayout cl_form_data_kesehatanxvi_qu;
    @BindView(R.id.cl_data_kesehatanxvi_qu)
    ConstraintLayout cl_data_kesehatanxvi_qu;
    @BindView(R.id.cl_data_kesehatanxvi_isi_qu)
    ConstraintLayout cl_data_kesehatanxvi_isi_qu;
    @BindView(R.id.cl_form_calon_ppxvi_qu)
    ConstraintLayout cl_form_calon_ppxvi_qu;
    @BindView(R.id.cl_form_calon_ttxvi_qu)
    ConstraintLayout cl_form_calon_ttxvi_qu;
    @BindView(R.id.cl_form_calon_tt1_xvi_qu)
    ConstraintLayout cl_form_calon_tt1_xvi_qu;
    @BindView(R.id.cl_form_calon_tt2_xvi_qu)
    ConstraintLayout cl_form_calon_tt2_xvi_qu;
    @BindView(R.id.cl_form_calon_tt3_xvi_qu)
    ConstraintLayout cl_form_calon_tt3_xvi_qu;
    @BindView(R.id.cl_form_data_kesehatanxvii_qu)
    ConstraintLayout cl_form_data_kesehatanxvii_qu;
    @BindView(R.id.cl_data_kesehatanxvii_qu)
    ConstraintLayout cl_data_kesehatanxvii_qu;
    @BindView(R.id.cl_data_kesehatanxvii_isi_qu)
    ConstraintLayout cl_data_kesehatanxvii_isi_qu;
    @BindView(R.id.cl_form_calon_ppxvii_qu)
    ConstraintLayout cl_form_calon_ppxvii_qu;
    @BindView(R.id.cl_form_calon_ttxvii_qu)
    ConstraintLayout cl_form_calon_ttxvii_qu;
    @BindView(R.id.cl_form_calon_tt1_xvii_qu)
    ConstraintLayout cl_form_calon_tt1_xvii_qu;
    @BindView(R.id.cl_form_calon_tt2_xvii_qu)
    ConstraintLayout cl_form_calon_tt2_xvii_qu;
    @BindView(R.id.cl_form_calon_tt3_xvii_qu)
    ConstraintLayout cl_form_calon_tt3_xvii_qu;
    @BindView(R.id.cl_form_data_kesehatanxviii_qu)
    ConstraintLayout cl_form_data_kesehatanxviii_qu;
    @BindView(R.id.cl_data_kesehatanxviii_qu)
    ConstraintLayout cl_data_kesehatanxviii_qu;
    @BindView(R.id.cl_data_kesehatanxviii_isi_qu)
    ConstraintLayout cl_data_kesehatanxviii_isi_qu;
    @BindView(R.id.cl_form_calon_ppxviii_qu)
    ConstraintLayout cl_form_calon_ppxviii_qu;
    @BindView(R.id.cl_form_calon_ttxviii_qu)
    ConstraintLayout cl_form_calon_ttxviii_qu;
    @BindView(R.id.cl_form_calon_tt1_xviii_qu)
    ConstraintLayout cl_form_calon_tt1_xviii_qu;
    @BindView(R.id.cl_form_calon_tt2_xviii_qu)
    ConstraintLayout cl_form_calon_tt2_xviii_qu;
    @BindView(R.id.cl_form_calon_tt3_xviii_qu)
    ConstraintLayout cl_form_calon_tt3_xviii_qu;
    @BindView(R.id.cl_form_data_kesehatanxix_qu)
    ConstraintLayout cl_form_data_kesehatanxix_qu;
    @BindView(R.id.cl_data_kesehatanxix_qu)
    ConstraintLayout cl_data_kesehatanxix_qu;
    @BindView(R.id.cl_data_kesehatanxix_isi_qu)
    ConstraintLayout cl_data_kesehatanxix_isi_qu;
    @BindView(R.id.cl_form_calon_ppxix_qu)
    ConstraintLayout cl_form_calon_ppxix_qu;
    @BindView(R.id.cl_form_calon_ttxix_qu)
    ConstraintLayout cl_form_calon_ttxix_qu;
    @BindView(R.id.cl_form_calon_tt1_xix_qu)
    ConstraintLayout cl_form_calon_tt1_xix_qu;
    @BindView(R.id.cl_form_calon_tt2_xix_qu)
    ConstraintLayout cl_form_calon_tt2_xix_qu;
    @BindView(R.id.cl_form_calon_tt3_xix_qu)
    ConstraintLayout cl_form_calon_tt3_xix_qu;
    @BindView(R.id.cl_form_data_kesehatanxx_qu)
    ConstraintLayout cl_form_data_kesehatanxx_qu;
    @BindView(R.id.cl_data_kesehatanxx_qu)
    ConstraintLayout cl_data_kesehatanxx_qu;
    @BindView(R.id.cl_data_kesehatanxx_isi_qu)
    ConstraintLayout cl_data_kesehatanxx_isi_qu;
    @BindView(R.id.cl_form_calon_ppxx_qu)
    ConstraintLayout cl_form_calon_ppxx_qu;
    @BindView(R.id.cl_form_calon_ttxx_qu)
    ConstraintLayout cl_form_calon_ttxx_qu;
    @BindView(R.id.cl_form_calon_tt1_xx_qu)
    ConstraintLayout cl_form_calon_tt1_xx_qu;
    @BindView(R.id.cl_form_calon_tt2_xx_qu)
    ConstraintLayout cl_form_calon_tt2_xx_qu;
    @BindView(R.id.cl_form_calon_tt3_xx_qu)
    ConstraintLayout cl_form_calon_tt3_xx_qu;
    @BindView(R.id.cl_form_data_kesehatanxxi_qu)
    ConstraintLayout cl_form_data_kesehatanxxi_qu;
    @BindView(R.id.cl_data_kesehatanxxi_qu)
    ConstraintLayout cl_data_kesehatanxxi_qu;
    @BindView(R.id.cl_data_kesehatanxxi_isi_qu)
    ConstraintLayout cl_data_kesehatanxxi_isi_qu;
    @BindView(R.id.cl_form_calon_ppxxi_qu)
    ConstraintLayout cl_form_calon_ppxxi_qu;
    @BindView(R.id.cl_form_calon_ttxxi_qu)
    ConstraintLayout cl_form_calon_ttxxi_qu;
    @BindView(R.id.cl_form_calon_tt1_xxi_qu)
    ConstraintLayout cl_form_calon_tt1_xxi_qu;
    @BindView(R.id.cl_form_calon_tt2_xxi_qu)
    ConstraintLayout cl_form_calon_tt2_xxi_qu;
    @BindView(R.id.cl_form_calon_tt3_xxi_qu)
    ConstraintLayout cl_form_calon_tt3_xxi_qu;
    @BindView(R.id.cl_form_data_kesehatanxxii_qu)
    ConstraintLayout cl_form_data_kesehatanxxii_qu;
    @BindView(R.id.cl_data_kesehatanxxii_qu)
    ConstraintLayout cl_data_kesehatanxxii_qu;
    @BindView(R.id.cl_data_kesehatanxxii_isi_qu)
    ConstraintLayout cl_data_kesehatanxxii_isi_qu;
    @BindView(R.id.cl_form_calon_ppxxii_qu)
    ConstraintLayout cl_form_calon_ppxxii_qu;
    @BindView(R.id.cl_form_calon_ttxxii_qu)
    ConstraintLayout cl_form_calon_ttxxii_qu;
    @BindView(R.id.cl_form_calon_tt1_xxii_qu)
    ConstraintLayout cl_form_calon_tt1_xxii_qu;
    @BindView(R.id.cl_form_calon_tt2_xxii_qu)
    ConstraintLayout cl_form_calon_tt2_xxii_qu;
    @BindView(R.id.cl_form_calon_tt3_xxii_qu)
    ConstraintLayout cl_form_calon_tt3_xxii_qu;
    @BindView(R.id.cl_form_data_kesehatanxxiii_qu)
    ConstraintLayout cl_form_data_kesehatanxxiii_qu;
    @BindView(R.id.cl_data_kesehatanxxiii_qu)
    ConstraintLayout cl_data_kesehatanxxiii_qu;
    @BindView(R.id.cl_data_kesehatanxxiii_isi_qu)
    ConstraintLayout cl_data_kesehatanxxiii_isi_qu;
    @BindView(R.id.cl_form_calon_ppxxiii_qu)
    ConstraintLayout cl_form_calon_ppxxiii_qu;
    @BindView(R.id.cl_form_calon_ttxxiii_qu)
    ConstraintLayout cl_form_calon_ttxxiii_qu;
    @BindView(R.id.cl_form_calon_tt1_xxiii_qu)
    ConstraintLayout cl_form_calon_tt1_xxiii_qu;
    @BindView(R.id.cl_form_calon_tt2_xxiii_qu)
    ConstraintLayout cl_form_calon_tt2_xxiii_qu;
    @BindView(R.id.cl_form_calon_tt3_xxiii_qu)
    ConstraintLayout cl_form_calon_tt3_xxiii_qu;
    @BindView(R.id.cl_form_data_kesehatanxxiv_qu)
    ConstraintLayout cl_form_data_kesehatanxxiv_qu;
    @BindView(R.id.cl_data_kesehatanxxiv_qu)
    ConstraintLayout cl_data_kesehatanxxiv_qu;
    @BindView(R.id.cl_data_kesehatanxxiv_isi_qu)
    ConstraintLayout cl_data_kesehatanxxiv_isi_qu;
    @BindView(R.id.cl_form_calon_ppxxiv_qu)
    ConstraintLayout cl_form_calon_ppxxiv_qu;
    @BindView(R.id.cl_form_calon_ttxxiv_qu)
    ConstraintLayout cl_form_calon_ttxxiv_qu;
    @BindView(R.id.cl_form_calon_tt1_xxiv_qu)
    ConstraintLayout cl_form_calon_tt1_xxiv_qu;
    @BindView(R.id.cl_form_calon_tt2_xxiv_qu)
    ConstraintLayout cl_form_calon_tt2_xxiv_qu;
    @BindView(R.id.cl_form_calon_tt3_xxiv_qu)
    ConstraintLayout cl_form_calon_tt3_xxiv_qu;
    @BindView(R.id.cl_form_data_kesehatanxxv_qu)
    ConstraintLayout cl_form_data_kesehatanxxv_qu;
    @BindView(R.id.cl_data_kesehatanxxv_qu)
    ConstraintLayout cl_data_kesehatanxxv_qu;
    @BindView(R.id.cl_data_kesehatanxxv_isi_qu)
    ConstraintLayout cl_data_kesehatanxxv_isi_qu;
    @BindView(R.id.cl_form_calon_ppxxv_qu)
    ConstraintLayout cl_form_calon_ppxxv_qu;
    @BindView(R.id.cl_form_calon_ttxxv_qu)
    ConstraintLayout cl_form_calon_ttxxv_qu;
    @BindView(R.id.cl_form_calon_tt1_xxv_qu)
    ConstraintLayout cl_form_calon_tt1_xxv_qu;
    @BindView(R.id.cl_form_calon_tt2_xxv_qu)
    ConstraintLayout cl_form_calon_tt2_xxv_qu;
    @BindView(R.id.cl_form_calon_tt3_xxv_qu)
    ConstraintLayout cl_form_calon_tt3_xxv_qu;
    @BindView(R.id.cl_form_data_kesehatanxxvi_qu)
    ConstraintLayout cl_form_data_kesehatanxxvi_qu;
    @BindView(R.id.cl_data_kesehatanxxvi_qu)
    ConstraintLayout cl_data_kesehatanxxvi_qu;
    @BindView(R.id.cl_data_kesehatanxxvi_isi_qu)
    ConstraintLayout cl_data_kesehatanxxvi_isi_qu;
    @BindView(R.id.cl_form_calon_ppxxvi_qu)
    ConstraintLayout cl_form_calon_ppxxvi_qu;
    @BindView(R.id.cl_form_calon_ttxxvi_qu)
    ConstraintLayout cl_form_calon_ttxxvi_qu;
    @BindView(R.id.cl_form_calon_tt1_xxvi_qu)
    ConstraintLayout cl_form_calon_tt1_xxvi_qu;
    @BindView(R.id.cl_form_calon_tt2_xxvi_qu)
    ConstraintLayout cl_form_calon_tt2_xxvi_qu;
    @BindView(R.id.cl_form_calon_tt3_xxvi_qu)
    ConstraintLayout cl_form_calon_tt3_xxvi_qu;
    @BindView(R.id.cl_form_data_kesehatanxxvii_qu)
    ConstraintLayout cl_form_data_kesehatanxxvii_qu;
    @BindView(R.id.cl_data_kesehatanxxvii_qu)
    ConstraintLayout cl_data_kesehatanxxvii_qu;
    @BindView(R.id.cl_data_kesehatanxxvii_isi_qu)
    ConstraintLayout cl_data_kesehatanxxvii_isi_qu;
    @BindView(R.id.cl_form_calon_ppxxvii_qu)
    ConstraintLayout cl_form_calon_ppxxvii_qu;
    @BindView(R.id.cl_form_calon_ttxxvii_qu)
    ConstraintLayout cl_form_calon_ttxxvii_qu;
    @BindView(R.id.cl_form_calon_tt1_xxvii_qu)
    ConstraintLayout cl_form_calon_tt1_xxvii_qu;
    @BindView(R.id.cl_form_calon_tt2_xxvii_qu)
    ConstraintLayout cl_form_calon_tt2_xxvii_qu;
    @BindView(R.id.cl_form_calon_tt3_xxvii_qu)
    ConstraintLayout cl_form_calon_tt3_xxvii_qu;
    @BindView(R.id.cl_form_data_kesehatanxxviii_qu)
    ConstraintLayout cl_form_data_kesehatanxxviii_qu;
    @BindView(R.id.cl_data_kesehatanxxviii_qu)
    ConstraintLayout cl_data_kesehatanxxviii_qu;
    @BindView(R.id.cl_data_kesehatanxxviii_isi_qu)
    ConstraintLayout cl_data_kesehatanxxviii_isi_qu;
    @BindView(R.id.cl_form_calon_ppxxviii_qu)
    ConstraintLayout cl_form_calon_ppxxviii_qu;
    @BindView(R.id.cl_form_calon_ttxxviii_qu)
    ConstraintLayout cl_form_calon_ttxxviii_qu;
    @BindView(R.id.cl_form_calon_tt1_xxviii_qu)
    ConstraintLayout cl_form_calon_tt1_xxviii_qu;
    @BindView(R.id.cl_form_calon_tt2_xxviii_qu)
    ConstraintLayout cl_form_calon_tt2_xxviii_qu;
    @BindView(R.id.cl_form_calon_tt3_xxviii_qu)
    ConstraintLayout cl_form_calon_tt3_xxviii_qu;
    @BindView(R.id.cl_form_data_kesehatanxxix_qu)
    ConstraintLayout cl_form_data_kesehatanxxix_qu;
    @BindView(R.id.cl_data_kesehatanxxix_qu)
    ConstraintLayout cl_data_kesehatanxxix_qu;
    @BindView(R.id.cl_data_kesehatanxxix_isi_qu)
    ConstraintLayout cl_data_kesehatanxxix_isi_qu;
    @BindView(R.id.cl_form_calon_ppxxix_qu)
    ConstraintLayout cl_form_calon_ppxxix_qu;
    @BindView(R.id.cl_form_calon_ttxxix_qu)
    ConstraintLayout cl_form_calon_ttxxix_qu;
    @BindView(R.id.cl_form_calon_tt1_xxix_qu)
    ConstraintLayout cl_form_calon_tt1_xxix_qu;
    @BindView(R.id.cl_form_calon_tt2_xxix_qu)
    ConstraintLayout cl_form_calon_tt2_xxix_qu;
    @BindView(R.id.cl_form_calon_tt3_xxix_qu)
    ConstraintLayout cl_form_calon_tt3_xxix_qu;
    @BindView(R.id.cl_form_data_kesehatanxxx_qu)
    ConstraintLayout cl_form_data_kesehatanxxx_qu;
    @BindView(R.id.cl_data_kesehatanxxx_qu)
    ConstraintLayout cl_data_kesehatanxxx_qu;
    @BindView(R.id.cl_data_kesehatanxxx_isi_qu)
    ConstraintLayout cl_data_kesehatanxxx_isi_qu;
    @BindView(R.id.cl_form_calon_ppxxx_qu)
    ConstraintLayout cl_form_calon_ppxxx_qu;
    @BindView(R.id.cl_form_calon_ttxxx_qu)
    ConstraintLayout cl_form_calon_ttxxx_qu;
    @BindView(R.id.cl_form_calon_tt1_xxx_qu)
    ConstraintLayout cl_form_calon_tt1_xxx_qu;
    @BindView(R.id.cl_form_calon_tt2_xxx_qu)
    ConstraintLayout cl_form_calon_tt2_xxx_qu;
    @BindView(R.id.cl_form_calon_tt3_xxx_qu)
    ConstraintLayout cl_form_calon_tt3_xxx_qu;
    @BindView(R.id.cl_form_data_kesehatanxxxi_qu)
    ConstraintLayout cl_form_data_kesehatanxxxi_qu;
    @BindView(R.id.cl_data_kesehatanxxxi_qu)
    ConstraintLayout cl_data_kesehatanxxxi_qu;
    @BindView(R.id.cl_data_kesehatanxxxi_isi_qu)
    ConstraintLayout cl_data_kesehatanxxxi_isi_qu;
    @BindView(R.id.cl_form_calon_ppxxxi_qu)
    ConstraintLayout cl_form_calon_ppxxxi_qu;
    @BindView(R.id.cl_form_calon_ttxxxi_qu)
    ConstraintLayout cl_form_calon_ttxxxi_qu;
    @BindView(R.id.cl_form_calon_tt1_xxxi_qu)
    ConstraintLayout cl_form_calon_tt1_xxxi_qu;
    @BindView(R.id.cl_form_calon_tt2_xxxi_qu)
    ConstraintLayout cl_form_calon_tt2_xxxi_qu;
    @BindView(R.id.cl_form_calon_tt3_xxxi_qu)
    ConstraintLayout cl_form_calon_tt3_xxxi_qu;
    @BindView(R.id.cl_form_data_kesehatan2_qu)
    ConstraintLayout cl_form_data_kesehatan2_qu;
    @BindView(R.id.cl_data_kesehatan2_qu)
    ConstraintLayout cl_data_kesehatan2_qu;
    @BindView(R.id.cl_data_kesehatan2_isi_qu)
    ConstraintLayout cl_data_kesehatan2_isi_qu;
    @BindView(R.id.cl_form_calon_pp_qu)
    ConstraintLayout cl_form_calon_pp_qu;
    @BindView(R.id.cl_form_calon_tt_qu)
    ConstraintLayout cl_form_calon_tt_qu;
    @BindView(R.id.cl_form_calon_tt1_qu)
    ConstraintLayout cl_form_calon_tt1_qu;
    @BindView(R.id.cl_form_calon_tt2_qu)
    ConstraintLayout cl_form_calon_tt2_qu;
    @BindView(R.id.cl_form_calon_tt3_qu)
    ConstraintLayout cl_form_calon_tt3_qu;
    @BindView(R.id.cl_form_data_kesehatan2_ket_qu)
    ConstraintLayout cl_form_data_kesehatan2_ket_qu;
    @BindView(R.id.cl_form_data_kesehatan3_qu)
    ConstraintLayout cl_form_data_kesehatan3_qu;
    @BindView(R.id.cl_data_kesehatan3_qu)
    ConstraintLayout cl_data_kesehatan3_qu;
    @BindView(R.id.cl_data_kesehatan3_isi_qu)
    ConstraintLayout cl_data_kesehatan3_isi_qu;
    @BindView(R.id.cl_form_data_kesehatan3_isi2_qu)
    ConstraintLayout cl_form_data_kesehatan3_isi2_qu;
    @BindView(R.id.cl_data_kesehatan3_isi2_qu)
    ConstraintLayout cl_data_kesehatan3_isi2_qu;
    @BindView(R.id.cl_form_calon_pp3a_qu)
    ConstraintLayout cl_form_calon_pp3a_qu;
    @BindView(R.id.cl_form_calon_tt3a_qu)
    ConstraintLayout cl_form_calon_tt3a_qu;
    @BindView(R.id.cl_form_calon_tt1_3a_qu)
    ConstraintLayout cl_form_calon_tt1_3a_qu;
    @BindView(R.id.cl_form_calon_tt2_3a_qu)
    ConstraintLayout cl_form_calon_tt2_3a_qu;
    @BindView(R.id.cl_form_calon_tt3_3a_qu)
    ConstraintLayout cl_form_calon_tt3_3a_qu;
    @BindView(R.id.cl_form_data_kesehatan3b_qu)
    ConstraintLayout cl_form_data_kesehatan3b_qu;
    @BindView(R.id.cl_data_kesehatan3b_qu)
    ConstraintLayout cl_data_kesehatan3b_qu;
    @BindView(R.id.cl_data_kesehatan3b_isi_qu)
    ConstraintLayout cl_data_kesehatan3b_isi_qu;
    @BindView(R.id.cl_form_calon_pp3b_qu)
    ConstraintLayout cl_form_calon_pp3b_qu;
    @BindView(R.id.cl_form_calon_tt3b_qu)
    ConstraintLayout cl_form_calon_tt3b_qu;
    @BindView(R.id.cl_form_calon_tt1_3b_qu)
    ConstraintLayout cl_form_calon_tt1_3b_qu;
    @BindView(R.id.cl_form_calon_tt2_3b_qu)
    ConstraintLayout cl_form_calon_tt2_3b_qu;
    @BindView(R.id.cl_form_calon_tt3_3b_qu)
    ConstraintLayout cl_form_calon_tt3_3b_qu;
    @BindView(R.id.cl_form_data_kesehatan3c_qu)
    ConstraintLayout cl_form_data_kesehatan3c_qu;
    @BindView(R.id.cl_data_kesehatan3c_qu)
    ConstraintLayout cl_data_kesehatan3c_qu;
    @BindView(R.id.cl_data_kesehatan3c_isi_qu)
    ConstraintLayout cl_data_kesehatan3c_isi_qu;
    @BindView(R.id.cl_form_calon_pp3c_qu)
    ConstraintLayout cl_form_calon_pp3c_qu;
    @BindView(R.id.cl_form_calon_tt3c_qu)
    ConstraintLayout cl_form_calon_tt3c_qu;
    @BindView(R.id.cl_form_calon_tt1_3c_qu)
    ConstraintLayout cl_form_calon_tt1_3c_qu;
    @BindView(R.id.cl_form_calon_tt2_3c_qu)
    ConstraintLayout cl_form_calon_tt2_3c_qu;
    @BindView(R.id.cl_form_calon_tt3_3c_qu)
    ConstraintLayout cl_form_calon_tt3_3c_qu;
    @BindView(R.id.cl_form_data_kesehatan3d_qu)
    ConstraintLayout cl_form_data_kesehatan3d_qu;
    @BindView(R.id.cl_data_kesehatan3d_qu)
    ConstraintLayout cl_data_kesehatan3d_qu;
    @BindView(R.id.cl_data_kesehatan3d_isi_qu)
    ConstraintLayout cl_data_kesehatan3d_isi_qu;
    @BindView(R.id.cl_form_calon_pp3d_qu)
    ConstraintLayout cl_form_calon_pp3d_qu;
    @BindView(R.id.cl_form_calon_tt3d_qu)
    ConstraintLayout cl_form_calon_tt3d_qu;
    @BindView(R.id.cl_form_calon_tt1_3d_qu)
    ConstraintLayout cl_form_calon_tt1_3d_qu;
    @BindView(R.id.cl_form_calon_tt2_3d_qu)
    ConstraintLayout cl_form_calon_tt2_3d_qu;
    @BindView(R.id.cl_form_calon_tt3_3d_qu)
    ConstraintLayout cl_form_calon_tt3_3d_qu;
    @BindView(R.id.cl_form_data_kesehatan4_qu)
    ConstraintLayout cl_form_data_kesehatan4_qu;
    @BindView(R.id.cl_data_kesehatan4_qu)
    ConstraintLayout cl_data_kesehatan4_qu;
    @BindView(R.id.cl_data_kesehatan4_isi_qu)
    ConstraintLayout cl_data_kesehatan4_isi_qu;
    @BindView(R.id.cl_form_calon_pp4_qu)
    ConstraintLayout cl_form_calon_pp4_qu;
    @BindView(R.id.cl_form_calon_tt1_4_qu)
    ConstraintLayout cl_form_calon_tt1_4_qu;
    @BindView(R.id.cl_form_calon_tt2_4_qu)
    ConstraintLayout cl_form_calon_tt2_4_qu;
    @BindView(R.id.cl_form_calon_tt3_4_qu)
    ConstraintLayout cl_form_calon_tt3_4_qu;
    @BindView(R.id.cl_form_data_kesehatan4_ket_qu)
    ConstraintLayout cl_form_data_kesehatan4_ket_qu;
    @BindView(R.id.cl_form_data_kesehatan5_qu)
    ConstraintLayout cl_form_data_kesehatan5_qu;
    @BindView(R.id.cl_data_kesehatan5_qu)
    ConstraintLayout cl_data_kesehatan5_qu;
    @BindView(R.id.cl_data_kesehatan5_isi_qu)
    ConstraintLayout cl_data_kesehatan5_isi_qu;
    @BindView(R.id.cl_form_calon_pp5_qu)
    ConstraintLayout cl_form_calon_pp5_qu;
    @BindView(R.id.cl_form_calon_tt5_qu)
    ConstraintLayout cl_form_calon_tt5_qu;
    @BindView(R.id.cl_form_calon_tt1_5_qu)
    ConstraintLayout cl_form_calon_tt1_5_qu;
    @BindView(R.id.cl_form_calon_tt2_5_qu)
    ConstraintLayout cl_form_calon_tt2_5_qu;
    @BindView(R.id.cl_form_calon_tt3_5_qu)
    ConstraintLayout cl_form_calon_tt3_5_qu;
    @BindView(R.id.cl_form_data_kesehatan6_qu)
    ConstraintLayout cl_form_data_kesehatan6_qu;
    @BindView(R.id.cl_data_kesehatan6_qu)
    ConstraintLayout cl_data_kesehatan6_qu;
    @BindView(R.id.cl_data_kesehatan6_isi_qu)
    ConstraintLayout cl_data_kesehatan6_isi_qu;
    @BindView(R.id.cl_form_calon_pp6_qu)
    ConstraintLayout cl_form_calon_pp6_qu;
    @BindView(R.id.cl_form_calon_tt6_qu)
    ConstraintLayout cl_form_calon_tt6_qu;
    @BindView(R.id.cl_form_calon_tt1_6_qu)
    ConstraintLayout cl_form_calon_tt1_6_qu;
    @BindView(R.id.cl_form_calon_tt2_6_qu)
    ConstraintLayout cl_form_calon_tt2_6_qu;
    @BindView(R.id.cl_form_calon_tt3_6_qu)
    ConstraintLayout cl_form_calon_tt3_6_qu;
    @BindView(R.id.cl_form_data_kesehatan7_qu)
    ConstraintLayout cl_form_data_kesehatan7_qu;
    @BindView(R.id.cl_data_kesehatan7_qu)
    ConstraintLayout cl_data_kesehatan7_qu;
    @BindView(R.id.cl_data_kesehatan7_isi_qu)
    ConstraintLayout cl_data_kesehatan7_isi_qu;
    @BindView(R.id.cl_form_calon_pp7_qu)
    ConstraintLayout cl_form_calon_pp7_qu;
    @BindView(R.id.cl_form_calon_tt7_qu)
    ConstraintLayout cl_form_calon_tt7_qu;
    @BindView(R.id.cl_form_calon_tt1_7_qu)
    ConstraintLayout cl_form_calon_tt1_7_qu;
    @BindView(R.id.cl_form_calon_tt2_7_qu)
    ConstraintLayout cl_form_calon_tt2_7_qu;
    @BindView(R.id.cl_form_calon_tt3_7_qu)
    ConstraintLayout cl_form_calon_tt3_7_qu;
    @BindView(R.id.cl_form_data_kesehatan7a_qu)
    ConstraintLayout cl_form_data_kesehatan7a_qu;
    @BindView(R.id.cl_data_kesehatan7a_qu)
    ConstraintLayout cl_data_kesehatan7a_qu;
    @BindView(R.id.cl_data_kesehatan7a_isi_qu)
    ConstraintLayout cl_data_kesehatan7a_isi_qu;
    @BindView(R.id.cl_form_data_kesehatan7a_jwb_qu)
    ConstraintLayout cl_form_data_kesehatan7a_jwb_qu;
    @BindView(R.id.cl_form_data_kesehatan7b_qu)
    ConstraintLayout cl_form_data_kesehatan7b_qu;
    @BindView(R.id.cl_data_kesehatan7b_qu)
    ConstraintLayout cl_data_kesehatan7b_qu;
    @BindView(R.id.cl_data_kesehatan7b_isi_qu)
    ConstraintLayout cl_data_kesehatan7b_isi_qu;
    @BindView(R.id.cl_form_data_kesehatan7b_jwb_qu)
    ConstraintLayout cl_form_data_kesehatan7b_jwb_qu;
    @BindView(R.id.cl_form_data_kesehatan7c_qu)
    ConstraintLayout cl_form_data_kesehatan7c_qu;
    @BindView(R.id.cl_data_kesehatan7c_qu)
    ConstraintLayout cl_data_kesehatan7c_qu;
    @BindView(R.id.cl_data_kesehatan7c_isi_qu)
    ConstraintLayout cl_data_kesehatan7c_isi_qu;
    @BindView(R.id.cl_form_data_kesehatan7c_jwb_qu)
    ConstraintLayout cl_form_data_kesehatan7c_jwb_qu;
    @BindView(R.id.cl_form_data_kesehatan7d_qu)
    ConstraintLayout cl_form_data_kesehatan7d_qu;
    @BindView(R.id.cl_data_kesehatan7d_qu)
    ConstraintLayout cl_data_kesehatan7d_qu;
    @BindView(R.id.cl_data_kesehatan7d_isi_qu)
    ConstraintLayout cl_data_kesehatan7d_isi_qu;
    @BindView(R.id.cl_form_data_kesehatan7d_jwb_qu)
    ConstraintLayout cl_form_data_kesehatan7d_jwb_qu;
    @BindView(R.id.cl_form_data_kesehatan8_qu)
    ConstraintLayout cl_form_data_kesehatan8_qu;
    @BindView(R.id.cl_data_kesehatan8_qu)
    ConstraintLayout cl_data_kesehatan8_qu;
    @BindView(R.id.cl_data_kesehatan8_isi_qu)
    ConstraintLayout cl_data_kesehatan8_isi_qu;
    @BindView(R.id.cl_data_kesehatan8a_qu)
    ConstraintLayout cl_data_kesehatan8a_qu;
    @BindView(R.id.cl_data_kesehatan8a_isi_qu)
    ConstraintLayout cl_data_kesehatan8a_isi_qu;
    @BindView(R.id.cl_form_calon_pp8a_qu)
    ConstraintLayout cl_form_calon_pp8a_qu;
    @BindView(R.id.cl_form_calon_tt8a_qu)
    ConstraintLayout cl_form_calon_tt8a_qu;
    @BindView(R.id.cl_form_calon_tt1_8a_qu)
    ConstraintLayout cl_form_calon_tt1_8a_qu;
    @BindView(R.id.cl_form_calon_tt2_8a_qu)
    ConstraintLayout cl_form_calon_tt2_8a_qu;
    @BindView(R.id.cl_form_calon_tt3_8a_qu)
    ConstraintLayout cl_form_calon_tt3_8a_qu;
    @BindView(R.id.cl_form_data_kesehatan8a_ket_qu)
    ConstraintLayout cl_form_data_kesehatan8a_ket_qu;
    @BindView(R.id.cl_form_data_kesehatan8b_qu)
    ConstraintLayout cl_form_data_kesehatan8b_qu;
    @BindView(R.id.cl_data_kesehatan8b_qu)
    ConstraintLayout cl_data_kesehatan8b_qu;
    @BindView(R.id.cl_data_kesehatan8b_isi_qu)
    ConstraintLayout cl_data_kesehatan8b_isi_qu;
    @BindView(R.id.cl_form_calon_pp8b_qu)
    ConstraintLayout cl_form_calon_pp8b_qu;
    @BindView(R.id.cl_form_calon_tt8b_qu)
    ConstraintLayout cl_form_calon_tt8b_qu;
    @BindView(R.id.cl_form_calon_tt1_8b_qu)
    ConstraintLayout cl_form_calon_tt1_8b_qu;
    @BindView(R.id.cl_form_calon_tt2_8b_qu)
    ConstraintLayout cl_form_calon_tt2_8b_qu;
    @BindView(R.id.cl_form_calon_tt3_8b_qu)
    ConstraintLayout cl_form_calon_tt3_8b_qu;
    @BindView(R.id.cl_form_data_kesehatan8c_qu)
    ConstraintLayout cl_form_data_kesehatan8c_qu;
    @BindView(R.id.cl_data_kesehatan8c_qu)
    ConstraintLayout cl_data_kesehatan8c_qu;
    @BindView(R.id.cl_data_kesehatan8c_isi_qu)
    ConstraintLayout cl_data_kesehatan8c_isi_qu;
    @BindView(R.id.cl_form_calon_pp8c_qu)
    ConstraintLayout cl_form_calon_pp8c_qu;
    @BindView(R.id.cl_form_calon_tt8c_qu)
    ConstraintLayout cl_form_calon_tt8c_qu;
    @BindView(R.id.cl_form_calon_tt1_8c_qu)
    ConstraintLayout cl_form_calon_tt1_8c_qu;
    @BindView(R.id.cl_form_calon_tt2_8c_qu)
    ConstraintLayout cl_form_calon_tt2_8c_qu;
    @BindView(R.id.cl_form_calon_tt3_8c_qu)
    ConstraintLayout cl_form_calon_tt3_8c_qu;
    @BindView(R.id.cl_form_data_kesehatan9_qu)
    ConstraintLayout cl_form_data_kesehatan9_qu;
    @BindView(R.id.cl_data_kesehatan9_qu)
    ConstraintLayout cl_data_kesehatan9_qu;
    @BindView(R.id.cl_data_kesehatan9_isi_qu)
    ConstraintLayout cl_data_kesehatan9_isi_qu;
    @BindView(R.id.cl_form_calon_pp9_qu)
    ConstraintLayout cl_form_calon_pp9_qu;
    @BindView(R.id.cl_form_calon_tt9_qu)
    ConstraintLayout cl_form_calon_tt9_qu;
    @BindView(R.id.cl_form_calon_tt1_9_qu)
    ConstraintLayout cl_form_calon_tt1_9_qu;
    @BindView(R.id.cl_form_calon_tt2_9_qu)
    ConstraintLayout cl_form_calon_tt2_9_qu;
    @BindView(R.id.cl_form_calon_tt3_9_qu)
    ConstraintLayout cl_form_calon_tt3_9_qu;
    @BindView(R.id.cl_form_tambah_data_calon_pemegang_polis_qu)
    ConstraintLayout cl_form_tambah_data_calon_pemegang_polis_qu;
    @BindView(R.id.cl_form_tambah_data_calon_tertanggung_qu)
    ConstraintLayout cl_form_tambah_data_calon_tertanggung_qu;
    @BindView(R.id.cl_form_data_kesehatan10_qu)
    ConstraintLayout cl_form_data_kesehatan10_qu;
    @BindView(R.id.cl_data_kesehatan10_qu)
    ConstraintLayout cl_data_kesehatan10_qu;
    @BindView(R.id.cl_data_kesehatan10_isi_qu)
    ConstraintLayout cl_data_kesehatan10_isi_qu;
    @BindView(R.id.cl_form_tambah_data_keterangan_kesehatan_qu)
    ConstraintLayout cl_form_tambah_data_keterangan_kesehatan_qu;
    @BindView(R.id.cl_form_calon_tt4_qu)
    ConstraintLayout cl_form_calon_tt4_qu;
    @BindView(R.id.layout_pp_no15)
    ConstraintLayout layout_pp_no15;
    @BindView(R.id.layout_pp_no14)
    ConstraintLayout layout_pp_no14;
    @BindView(R.id.layout_pp_no13)
    ConstraintLayout layout_pp_no13;
    @BindView(R.id.layout_tt_no11a)
    ConstraintLayout layout_tt_no11a;
    @BindView(R.id.layout_tt_no12)
    ConstraintLayout layout_tt_no12;
    @BindView(R.id.layout_vis_dk_2)
    ConstraintLayout layout_vis_dk_2;

    @BindView(R.id.iv_circle_questionnaire1_qu)
    ImageView iv_circle_questionnaire1_qu;
    @BindView(R.id.iv_circle_questionnaire1_isi_qu)
    ImageView iv_circle_questionnaire1_isi_qu;
    @BindView(R.id.iv_circle_questionnaire2_qu)
    ImageView iv_circle_questionnaire2_qu;
    @BindView(R.id.iv_circle_questionnaire2_isi_qu)
    ImageView iv_circle_questionnaire2_isi_qu;
    @BindView(R.id.iv_circle_questionnaire3_qu)
    ImageView iv_circle_questionnaire3_qu;
    @BindView(R.id.iv_circle_questionnaire3_isi_qu)
    ImageView iv_circle_questionnaire3_isi_qu;
    @BindView(R.id.iv_circle_questionnaire4_qu)
    ImageView iv_circle_questionnaire4_qu;
    @BindView(R.id.iv_circle_questionnaire4_isi_qu)
    ImageView iv_circle_questionnaire4_isi_qu;
    @BindView(R.id.iv_circle_questionnaire5_qu)
    ImageView iv_circle_questionnaire5_qu;
    @BindView(R.id.iv_circle_questionnaire5_isi_qu)
    ImageView iv_circle_questionnaire5_isi_qu;
    @BindView(R.id.iv_circle_questionnaire6_qu)
    ImageView iv_circle_questionnaire6_qu;
    @BindView(R.id.iv_circle_questionnaire6_isi_qu)
    ImageView iv_circle_questionnaire6_isi_qu;
    @BindView(R.id.iv_circle_questionnaire7_qu)
    ImageView iv_circle_questionnaire7_qu;
    @BindView(R.id.iv_circle_questionnaire7_isi_qu)
    ImageView iv_circle_questionnaire7_isi_qu;
    @BindView(R.id.iv_circle_questionnaire8_qu)
    ImageView iv_circle_questionnaire8_qu;
    @BindView(R.id.iv_circle_questionnaire8_isi_qu)
    ImageView iv_circle_questionnaire8_isi_qu;
    @BindView(R.id.iv_circle_questionnaire8a_qu)
    ImageView iv_circle_questionnaire8a_qu;
    @BindView(R.id.iv_circle_questionnaire8a_isi_qu)
    ImageView iv_circle_questionnaire8a_isi_qu;
    @BindView(R.id.iv_circle_questionnaire8b_qu)
    ImageView iv_circle_questionnaire8b_qu;
    @BindView(R.id.iv_circle_questionnaire8b_isi_qu)
    ImageView iv_circle_questionnaire8b_isi_qu;
    @BindView(R.id.iv_circle_questionnaire9_qu)
    ImageView iv_circle_questionnaire9_qu;
    @BindView(R.id.iv_circle_questionnaire9_isi_qu)
    ImageView iv_circle_questionnaire9_isi_qu;
    @BindView(R.id.iv_circle_questionnaire10_qu)
    ImageView iv_circle_questionnaire10_qu;
    @BindView(R.id.iv_circle_questionnaire10_isi_qu)
    ImageView iv_circle_questionnaire10_isi_qu;
    @BindView(R.id.iv_circle_questionnaire11a_qu)
    ImageView iv_circle_questionnaire11a_qu;
    @BindView(R.id.iv_circle_questionnaire11a_isi_qu)
    ImageView iv_circle_questionnaire11a_isi_qu;
    @BindView(R.id.iv_circle_questionnaire11b_qu)
    ImageView iv_circle_questionnaire11b_qu;
    @BindView(R.id.iv_circle_questionnaire11b_isi_qu)
    ImageView iv_circle_questionnaire11b_isi_qu;
    @BindView(R.id.iv_circle_questionnaire12_qu)
    ImageView iv_circle_questionnaire12_qu;
    @BindView(R.id.iv_circle_questionnaire12_isi_qu)
    ImageView iv_circle_questionnaire12_isi_qu;
    @BindView(R.id.iv_circle_questionnaire13_qu)
    ImageView iv_circle_questionnaire13_qu;
    @BindView(R.id.iv_circle_questionnaire13_isi_qu)
    ImageView iv_circle_questionnaire13_isi_qu;
    @BindView(R.id.iv_circle_questionnaire13a_qu)
    ImageView iv_circle_questionnaire13a_qu;
    @BindView(R.id.iv_circle_questionnaire13a_isi_qu)
    ImageView iv_circle_questionnaire13a_isi_qu;
    @BindView(R.id.iv_circle_questionnaire14_qu)
    ImageView iv_circle_questionnaire14_qu;
    @BindView(R.id.iv_circle_questionnaire14_isi_qu)
    ImageView iv_circle_questionnaire14_isi_qu;
    @BindView(R.id.iv_circle_questionnaire14a_qu)
    ImageView iv_circle_questionnaire14a_qu;
    @BindView(R.id.iv_circle_questionnaire14a_isi_qu)
    ImageView iv_circle_questionnaire14a_isi_qu;
    @BindView(R.id.iv_circle_questionnaire15_qu)
    ImageView iv_circle_questionnaire15_qu;
    @BindView(R.id.iv_circle_questionnaire15_isi_qu)
    ImageView iv_circle_questionnaire15_isi_qu;
    @BindView(R.id.iv_circle_data_kesehatan1_qu)
    ImageView iv_circle_data_kesehatan1_qu;
    @BindView(R.id.iv_circle_data_kesehatan1_isi_qu)
    ImageView iv_circle_data_kesehatan1_isi_qu;
    @BindView(R.id.iv_circle_data_kesehatani_qu)
    ImageView iv_circle_data_kesehatani_qu;
    @BindView(R.id.iv_circle_data_kesehatani_isi_qu)
    ImageView iv_circle_data_kesehatani_isi_qu;
    @BindView(R.id.iv_circle_calon_ppi_qu)
    ImageView iv_circle_calon_ppi_qu;
    @BindView(R.id.iv_circle_calon_tti_qu)
    ImageView iv_circle_calon_tti_qu;
    @BindView(R.id.iv_circle_calon_tt1_i_qu)
    ImageView iv_circle_calon_tt1_i_qu;
    @BindView(R.id.iv_circle_calon_tt2_i_qu)
    ImageView iv_circle_calon_tt2_i_qu;
    @BindView(R.id.iv_circle_calon_tt3_i_qu)
    ImageView iv_circle_calon_tt3_i_qu;
    @BindView(R.id.iv_circle_data_kesehatanii_qu)
    ImageView iv_circle_data_kesehatanii_qu;
    @BindView(R.id.iv_circle_data_kesehatanii_isi_qu)
    ImageView iv_circle_data_kesehatanii_isi_qu;
    @BindView(R.id.iv_circle_calon_ppii_qu)
    ImageView iv_circle_calon_ppii_qu;
    @BindView(R.id.iv_circle_calon_ttii_qu)
    ImageView iv_circle_calon_ttii_qu;
    @BindView(R.id.iv_circle_calon_tt1_ii_qu)
    ImageView iv_circle_calon_tt1_ii_qu;
    @BindView(R.id.iv_circle_calon_tt2_ii_qu)
    ImageView iv_circle_calon_tt2_ii_qu;
    @BindView(R.id.iv_circle_calon_tt3_ii_qu)
    ImageView iv_circle_calon_tt3_ii_qu;
    @BindView(R.id.iv_circle_data_kesehataniii_qu)
    ImageView iv_circle_data_kesehataniii_qu;
    @BindView(R.id.iv_circle_data_kesehataniii_isi_qu)
    ImageView iv_circle_data_kesehataniii_isi_qu;
    @BindView(R.id.iv_circle_calon_ppiii_qu)
    ImageView iv_circle_calon_ppiii_qu;
    @BindView(R.id.iv_circle_calon_ttiii_qu)
    ImageView iv_circle_calon_ttiii_qu;
    @BindView(R.id.iv_circle_calon_tt1_iii_qu)
    ImageView iv_circle_calon_tt1_iii_qu;
    @BindView(R.id.iv_circle_calon_tt2_iii_qu)
    ImageView iv_circle_calon_tt2_iii_qu;
    @BindView(R.id.iv_circle_calon_tt3_iii_qu)
    ImageView iv_circle_calon_tt3_iii_qu;
    @BindView(R.id.iv_circle_data_kesehataniv_qu)
    ImageView iv_circle_data_kesehataniv_qu;
    @BindView(R.id.iv_circle_data_kesehataniv_isi_qu)
    ImageView iv_circle_data_kesehataniv_isi_qu;
    @BindView(R.id.iv_circle_calon_ppiv_qu)
    ImageView iv_circle_calon_ppiv_qu;
    @BindView(R.id.iv_circle_calon_ttiv_qu)
    ImageView iv_circle_calon_ttiv_qu;
    @BindView(R.id.iv_circle_calon_tt1_iv_qu)
    ImageView iv_circle_calon_tt1_iv_qu;
    @BindView(R.id.iv_circle_calon_tt2_iv_qu)
    ImageView iv_circle_calon_tt2_iv_qu;
    @BindView(R.id.iv_circle_calon_tt3_iv_qu)
    ImageView iv_circle_calon_tt3_iv_qu;
    @BindView(R.id.iv_circle_data_kesehatanv_qu)
    ImageView iv_circle_data_kesehatanv_qu;
    @BindView(R.id.iv_circle_data_kesehatanv_isi_qu)
    ImageView iv_circle_data_kesehatanv_isi_qu;
    @BindView(R.id.iv_circle_calon_ppv_qu)
    ImageView iv_circle_calon_ppv_qu;
    @BindView(R.id.iv_circle_calon_ttv_qu)
    ImageView iv_circle_calon_ttv_qu;
    @BindView(R.id.iv_circle_calon_tt1_v_qu)
    ImageView iv_circle_calon_tt1_v_qu;
    @BindView(R.id.iv_circle_calon_tt2_v_qu)
    ImageView iv_circle_calon_tt2_v_qu;
    @BindView(R.id.iv_circle_calon_tt3_v_qu)
    ImageView iv_circle_calon_tt3_v_qu;
    @BindView(R.id.iv_circle_data_kesehatanvi_qu)
    ImageView iv_circle_data_kesehatanvi_qu;
    @BindView(R.id.iv_circle_data_kesehatanvi_isi_qu)
    ImageView iv_circle_data_kesehatanvi_isi_qu;
    @BindView(R.id.iv_circle_calon_ppvi_qu)
    ImageView iv_circle_calon_ppvi_qu;
    @BindView(R.id.iv_circle_calon_ttvi_qu)
    ImageView iv_circle_calon_ttvi_qu;
    @BindView(R.id.iv_circle_calon_tt1_vi_qu)
    ImageView iv_circle_calon_tt1_vi_qu;
    @BindView(R.id.iv_circle_calon_tt2_vi_qu)
    ImageView iv_circle_calon_tt2_vi_qu;
    @BindView(R.id.iv_circle_calon_tt3_vi_qu)
    ImageView iv_circle_calon_tt3_vi_qu;
    @BindView(R.id.iv_circle_data_kesehatanvii_qu)
    ImageView iv_circle_data_kesehatanvii_qu;
    @BindView(R.id.iv_circle_data_kesehatanvii_isi_qu)
    ImageView iv_circle_data_kesehatanvii_isi_qu;
    @BindView(R.id.iv_circle_calon_ppvii_qu)
    ImageView iv_circle_calon_ppvii_qu;
    @BindView(R.id.iv_circle_calon_ttvii_qu)
    ImageView iv_circle_calon_ttvii_qu;
    @BindView(R.id.iv_circle_calon_tt1_vii_qu)
    ImageView iv_circle_calon_tt1_vii_qu;
    @BindView(R.id.iv_circle_calon_tt2_vii_qu)
    ImageView iv_circle_calon_tt2_vii_qu;
    @BindView(R.id.iv_circle_calon_tt3_vii_qu)
    ImageView iv_circle_calon_tt3_vii_qu;
    @BindView(R.id.iv_circle_data_kesehatanviii_qu)
    ImageView iv_circle_data_kesehatanviii_qu;
    @BindView(R.id.iv_circle_data_kesehatanviii_isi_qu)
    ImageView iv_circle_data_kesehatanviii_isi_qu;
    @BindView(R.id.iv_circle_calon_ppviii_qu)
    ImageView iv_circle_calon_ppviii_qu;
    @BindView(R.id.iv_circle_calon_ttviii_qu)
    ImageView iv_circle_calon_ttviii_qu;
    @BindView(R.id.iv_circle_calon_tt1_viii_qu)
    ImageView iv_circle_calon_tt1_viii_qu;
    @BindView(R.id.iv_circle_calon_tt2_viii_qu)
    ImageView iv_circle_calon_tt2_viii_qu;
    @BindView(R.id.iv_circle_calon_tt3_viii_qu)
    ImageView iv_circle_calon_tt3_viii_qu;
    @BindView(R.id.iv_circle_data_kesehatanix_qu)
    ImageView iv_circle_data_kesehatanix_qu;
    @BindView(R.id.iv_circle_data_kesehatanix_isi_qu)
    ImageView iv_circle_data_kesehatanix_isi_qu;
    @BindView(R.id.iv_circle_calon_ppix_qu)
    ImageView iv_circle_calon_ppix_qu;
    @BindView(R.id.iv_circle_calon_ttix_qu)
    ImageView iv_circle_calon_ttix_qu;
    @BindView(R.id.iv_circle_calon_tt1_ix_qu)
    ImageView iv_circle_calon_tt1_ix_qu;
    @BindView(R.id.iv_circle_calon_tt2_ix_qu)
    ImageView iv_circle_calon_tt2_ix_qu;
    @BindView(R.id.iv_circle_calon_tt3_ix_qu)
    ImageView iv_circle_calon_tt3_ix_qu;
    @BindView(R.id.iv_circle_data_kesehatanx_qu)
    ImageView iv_circle_data_kesehatanx_qu;
    @BindView(R.id.iv_circle_data_kesehatanx_isi_qu)
    ImageView iv_circle_data_kesehatanx_isi_qu;
    @BindView(R.id.iv_circle_calon_ppx_qu)
    ImageView iv_circle_calon_ppx_qu;
    @BindView(R.id.iv_circle_calon_ttx_qu)
    ImageView iv_circle_calon_ttx_qu;
    @BindView(R.id.iv_circle_calon_tt1_x_qu)
    ImageView iv_circle_calon_tt1_x_qu;
    @BindView(R.id.iv_circle_calon_tt2_x_qu)
    ImageView iv_circle_calon_tt2_x_qu;
    @BindView(R.id.iv_circle_calon_tt3_x_qu)
    ImageView iv_circle_calon_tt3_x_qu;
    @BindView(R.id.iv_circle_data_kesehatanxi_qu)
    ImageView iv_circle_data_kesehatanxi_qu;
    @BindView(R.id.iv_circle_data_kesehatanxi_isi_qu)
    ImageView iv_circle_data_kesehatanxi_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxi_qu)
    ImageView iv_circle_calon_ppxi_qu;
    @BindView(R.id.iv_circle_calon_ttxi_qu)
    ImageView iv_circle_calon_ttxi_qu;
    @BindView(R.id.iv_circle_calon_tt1_xi_qu)
    ImageView iv_circle_calon_tt1_xi_qu;
    @BindView(R.id.iv_circle_calon_tt2_xi_qu)
    ImageView iv_circle_calon_tt2_xi_qu;
    @BindView(R.id.iv_circle_calon_tt3_xi_qu)
    ImageView iv_circle_calon_tt3_xi_qu;
    @BindView(R.id.iv_circle_data_kesehatanxii_qu)
    ImageView iv_circle_data_kesehatanxii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxii_isi_qu)
    ImageView iv_circle_data_kesehatanxii_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxii_qu)
    ImageView iv_circle_calon_ppxii_qu;
    @BindView(R.id.iv_circle_calon_ttxii_qu)
    ImageView iv_circle_calon_ttxii_qu;
    @BindView(R.id.iv_circle_calon_tt1_xii_qu)
    ImageView iv_circle_calon_tt1_xii_qu;
    @BindView(R.id.iv_circle_calon_tt2_xii_qu)
    ImageView iv_circle_calon_tt2_xii_qu;
    @BindView(R.id.iv_circle_calon_tt3_xii_qu)
    ImageView iv_circle_calon_tt3_xii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxiii_qu)
    ImageView iv_circle_data_kesehatanxiii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxiii_isi_qu)
    ImageView iv_circle_data_kesehatanxiii_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxiii_qu)
    ImageView iv_circle_calon_ppxiii_qu;
    @BindView(R.id.iv_circle_calon_ttxiii_qu)
    ImageView iv_circle_calon_ttxiii_qu;
    @BindView(R.id.iv_circle_calon_tt1_xiii_qu)
    ImageView iv_circle_calon_tt1_xiii_qu;
    @BindView(R.id.iv_circle_calon_tt2_xiii_qu)
    ImageView iv_circle_calon_tt2_xiii_qu;
    @BindView(R.id.iv_circle_calon_tt3_xiii_qu)
    ImageView iv_circle_calon_tt3_xiii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxiv_qu)
    ImageView iv_circle_data_kesehatanxiv_qu;
    @BindView(R.id.iv_circle_data_kesehatanxiv_isi_qu)
    ImageView iv_circle_data_kesehatanxiv_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxiv_qu)
    ImageView iv_circle_calon_ppxiv_qu;
    @BindView(R.id.iv_circle_calon_ttxiv_qu)
    ImageView iv_circle_calon_ttxiv_qu;
    @BindView(R.id.iv_circle_calon_tt1_xiv_qu)
    ImageView iv_circle_calon_tt1_xiv_qu;
    @BindView(R.id.iv_circle_calon_tt2_xiv_qu)
    ImageView iv_circle_calon_tt2_xiv_qu;
    @BindView(R.id.iv_circle_calon_tt3_xiv_qu)
    ImageView iv_circle_calon_tt3_xiv_qu;
    @BindView(R.id.iv_circle_data_kesehatanxv_qu)
    ImageView iv_circle_data_kesehatanxv_qu;
    @BindView(R.id.iv_circle_data_kesehatanxv_isi_qu)
    ImageView iv_circle_data_kesehatanxv_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxv_qu)
    ImageView iv_circle_calon_ppxv_qu;
    @BindView(R.id.iv_circle_calon_ttxv_qu)
    ImageView iv_circle_calon_ttxv_qu;
    @BindView(R.id.iv_circle_calon_tt1_xv_qu)
    ImageView iv_circle_calon_tt1_xv_qu;
    @BindView(R.id.iv_circle_calon_tt2_xv_qu)
    ImageView iv_circle_calon_tt2_xv_qu;
    @BindView(R.id.iv_circle_calon_tt3_xv_qu)
    ImageView iv_circle_calon_tt3_xv_qu;
    @BindView(R.id.iv_circle_data_kesehatanxvi_qu)
    ImageView iv_circle_data_kesehatanxvi_qu;
    @BindView(R.id.iv_circle_data_kesehatanxvi_isi_qu)
    ImageView iv_circle_data_kesehatanxvi_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxvi_qu)
    ImageView iv_circle_calon_ppxvi_qu;
    @BindView(R.id.iv_circle_calon_ttxvi_qu)
    ImageView iv_circle_calon_ttxvi_qu;
    @BindView(R.id.iv_circle_calon_tt1_xvi_qu)
    ImageView iv_circle_calon_tt1_xvi_qu;
    @BindView(R.id.iv_circle_calon_tt2_xvi_qu)
    ImageView iv_circle_calon_tt2_xvi_qu;
    @BindView(R.id.iv_circle_calon_tt3_xvi_qu)
    ImageView iv_circle_calon_tt3_xvi_qu;
    @BindView(R.id.iv_circle_data_kesehatanxvii_qu)
    ImageView iv_circle_data_kesehatanxvii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxvii_isi_qu)
    ImageView iv_circle_data_kesehatanxvii_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxvii_qu)
    ImageView iv_circle_calon_ppxvii_qu;
    @BindView(R.id.iv_circle_calon_ttxvii_qu)
    ImageView iv_circle_calon_ttxvii_qu;
    @BindView(R.id.iv_circle_calon_tt1_xvii_qu)
    ImageView iv_circle_calon_tt1_xvii_qu;
    @BindView(R.id.iv_circle_calon_tt2_xvii_qu)
    ImageView iv_circle_calon_tt2_xvii_qu;
    @BindView(R.id.iv_circle_calon_tt3_xvii_qu)
    ImageView iv_circle_calon_tt3_xvii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxviii_qu)
    ImageView iv_circle_data_kesehatanxviii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxviii_isi_qu)
    ImageView iv_circle_data_kesehatanxviii_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxviii_qu)
    ImageView iv_circle_calon_ppxviii_qu;
    @BindView(R.id.iv_circle_calon_ttxviii_qu)
    ImageView iv_circle_calon_ttxviii_qu;
    @BindView(R.id.iv_circle_calon_tt1_xviii_qu)
    ImageView iv_circle_calon_tt1_xviii_qu;
    @BindView(R.id.iv_circle_calon_tt2_xviii_qu)
    ImageView iv_circle_calon_tt2_xviii_qu;
    @BindView(R.id.iv_circle_calon_tt3_xviii_qu)
    ImageView iv_circle_calon_tt3_xviii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxix_qu)
    ImageView iv_circle_data_kesehatanxix_qu;
    @BindView(R.id.iv_circle_data_kesehatanxix_isi_qu)
    ImageView iv_circle_data_kesehatanxix_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxix_qu)
    ImageView iv_circle_calon_ppxix_qu;
    @BindView(R.id.iv_circle_calon_ttxix_qu)
    ImageView iv_circle_calon_ttxix_qu;
    @BindView(R.id.iv_circle_calon_tt1_xix_qu)
    ImageView iv_circle_calon_tt1_xix_qu;
    @BindView(R.id.iv_circle_calon_tt2_xix_qu)
    ImageView iv_circle_calon_tt2_xix_qu;
    @BindView(R.id.iv_circle_calon_tt3_xix_qu)
    ImageView iv_circle_calon_tt3_xix_qu;
    @BindView(R.id.iv_circle_data_kesehatanxx_qu)
    ImageView iv_circle_data_kesehatanxx_qu;
    @BindView(R.id.iv_circle_data_kesehatanxx_isi_qu)
    ImageView iv_circle_data_kesehatanxx_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxx_qu)
    ImageView iv_circle_calon_ppxx_qu;
    @BindView(R.id.iv_circle_calon_ttxx_qu)
    ImageView iv_circle_calon_ttxx_qu;
    @BindView(R.id.iv_circle_calon_tt1_xx_qu)
    ImageView iv_circle_calon_tt1_xx_qu;
    @BindView(R.id.iv_circle_calon_tt2_xx_qu)
    ImageView iv_circle_calon_tt2_xx_qu;
    @BindView(R.id.iv_circle_calon_tt3_xx_qu)
    ImageView iv_circle_calon_tt3_xx_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxi_qu)
    ImageView iv_circle_data_kesehatanxxi_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxi_isi_qu)
    ImageView iv_circle_data_kesehatanxxi_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxxi_qu)
    ImageView iv_circle_calon_ppxxi_qu;
    @BindView(R.id.iv_circle_calon_ttxxi_qu)
    ImageView iv_circle_calon_ttxxi_qu;
    @BindView(R.id.iv_circle_calon_tt1_xxi_qu)
    ImageView iv_circle_calon_tt1_xxi_qu;
    @BindView(R.id.iv_circle_calon_tt2_xxi_qu)
    ImageView iv_circle_calon_tt2_xxi_qu;
    @BindView(R.id.iv_circle_calon_tt3_xxi_qu)
    ImageView iv_circle_calon_tt3_xxi_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxii_qu)
    ImageView iv_circle_data_kesehatanxxii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxii_isi_qu)
    ImageView iv_circle_data_kesehatanxxii_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxxii_qu)
    ImageView iv_circle_calon_ppxxii_qu;
    @BindView(R.id.iv_circle_calon_ttxxii_qu)
    ImageView iv_circle_calon_ttxxii_qu;
    @BindView(R.id.iv_circle_calon_tt1_xxii_qu)
    ImageView iv_circle_calon_tt1_xxii_qu;
    @BindView(R.id.iv_circle_calon_tt2_xxii_qu)
    ImageView iv_circle_calon_tt2_xxii_qu;
    @BindView(R.id.iv_circle_calon_tt3_xxii_qu)
    ImageView iv_circle_calon_tt3_xxii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxiii_qu)
    ImageView iv_circle_data_kesehatanxxiii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxiii_isi_qu)
    ImageView iv_circle_data_kesehatanxxiii_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxxiii_qu)
    ImageView iv_circle_calon_ppxxiii_qu;
    @BindView(R.id.iv_circle_calon_ttxxiii_qu)
    ImageView iv_circle_calon_ttxxiii_qu;
    @BindView(R.id.iv_circle_calon_tt1_xxiii_qu)
    ImageView iv_circle_calon_tt1_xxiii_qu;
    @BindView(R.id.iv_circle_calon_tt2_xxiii_qu)
    ImageView iv_circle_calon_tt2_xxiii_qu;
    @BindView(R.id.iv_circle_calon_tt3_xxiii_qu)
    ImageView iv_circle_calon_tt3_xxiii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxiv_qu)
    ImageView iv_circle_data_kesehatanxxiv_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxiv_isi_qu)
    ImageView iv_circle_data_kesehatanxxiv_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxxiv_qu)
    ImageView iv_circle_calon_ppxxiv_qu;
    @BindView(R.id.iv_circle_calon_ttxxiv_qu)
    ImageView iv_circle_calon_ttxxiv_qu;
    @BindView(R.id.iv_circle_calon_tt1_xxiv_qu)
    ImageView iv_circle_calon_tt1_xxiv_qu;
    @BindView(R.id.iv_circle_calon_tt2_xxiv_qu)
    ImageView iv_circle_calon_tt2_xxiv_qu;
    @BindView(R.id.iv_circle_calon_tt3_xxiv_qu)
    ImageView iv_circle_calon_tt3_xxiv_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxv_qu)
    ImageView iv_circle_data_kesehatanxxv_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxv_isi_qu)
    ImageView iv_circle_data_kesehatanxxv_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxxv_qu)
    ImageView iv_circle_calon_ppxxv_qu;
    @BindView(R.id.iv_circle_calon_ttxxv_qu)
    ImageView iv_circle_calon_ttxxv_qu;
    @BindView(R.id.iv_circle_calon_tt1_xxv_qu)
    ImageView iv_circle_calon_tt1_xxv_qu;
    @BindView(R.id.iv_circle_calon_tt2_xxv_qu)
    ImageView iv_circle_calon_tt2_xxv_qu;
    @BindView(R.id.iv_circle_calon_tt3_xxv_qu)
    ImageView iv_circle_calon_tt3_xxv_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxvi_qu)
    ImageView iv_circle_data_kesehatanxxvi_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxvi_isi_qu)
    ImageView iv_circle_data_kesehatanxxvi_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxxvi_qu)
    ImageView iv_circle_calon_ppxxvi_qu;
    @BindView(R.id.iv_circle_calon_ttxxvi_qu)
    ImageView iv_circle_calon_ttxxvi_qu;
    @BindView(R.id.iv_circle_calon_tt1_xxvi_qu)
    ImageView iv_circle_calon_tt1_xxvi_qu;
    @BindView(R.id.iv_circle_calon_tt2_xxvi_qu)
    ImageView iv_circle_calon_tt2_xxvi_qu;
    @BindView(R.id.iv_circle_calon_tt3_xxvi_qu)
    ImageView iv_circle_calon_tt3_xxvi_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxvii_qu)
    ImageView iv_circle_data_kesehatanxxvii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxvii_isi_qu)
    ImageView iv_circle_data_kesehatanxxvii_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxxvii_qu)
    ImageView iv_circle_calon_ppxxvii_qu;
    @BindView(R.id.iv_circle_calon_ttxxvii_qu)
    ImageView iv_circle_calon_ttxxvii_qu;
    @BindView(R.id.iv_circle_calon_tt1_xxvii_qu)
    ImageView iv_circle_calon_tt1_xxvii_qu;
    @BindView(R.id.iv_circle_calon_tt2_xxvii_qu)
    ImageView iv_circle_calon_tt2_xxvii_qu;
    @BindView(R.id.iv_circle_calon_tt3_xxvii_qu)
    ImageView iv_circle_calon_tt3_xxvii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxviii_qu)
    ImageView iv_circle_data_kesehatanxxviii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxviii_isi_qu)
    ImageView iv_circle_data_kesehatanxxviii_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxxviii_qu)
    ImageView iv_circle_calon_ppxxviii_qu;
    @BindView(R.id.iv_circle_calon_ttxxviii_qu)
    ImageView iv_circle_calon_ttxxviii_qu;
    @BindView(R.id.iv_circle_calon_tt1_xxviii_qu)
    ImageView iv_circle_calon_tt1_xxviii_qu;
    @BindView(R.id.iv_circle_calon_tt2_xxviii_qu)
    ImageView iv_circle_calon_tt2_xxviii_qu;
    @BindView(R.id.iv_circle_calon_tt3_xxviii_qu)
    ImageView iv_circle_calon_tt3_xxviii_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxix_qu)
    ImageView iv_circle_data_kesehatanxxix_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxix_isi_qu)
    ImageView iv_circle_data_kesehatanxxix_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxxix_qu)
    ImageView iv_circle_calon_ppxxix_qu;
    @BindView(R.id.iv_circle_calon_ttxxix_qu)
    ImageView iv_circle_calon_ttxxix_qu;
    @BindView(R.id.iv_circle_calon_tt1_xxix_qu)
    ImageView iv_circle_calon_tt1_xxix_qu;
    @BindView(R.id.iv_circle_calon_tt2_xxix_qu)
    ImageView iv_circle_calon_tt2_xxix_qu;
    @BindView(R.id.iv_circle_calon_tt3_xxix_qu)
    ImageView iv_circle_calon_tt3_xxix_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxx_qu)
    ImageView iv_circle_data_kesehatanxxx_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxx_isi_qu)
    ImageView iv_circle_data_kesehatanxxx_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxxx_qu)
    ImageView iv_circle_calon_ppxxx_qu;
    @BindView(R.id.iv_circle_calon_ttxxx_qu)
    ImageView iv_circle_calon_ttxxx_qu;
    @BindView(R.id.iv_circle_calon_tt1_xxx_qu)
    ImageView iv_circle_calon_tt1_xxx_qu;
    @BindView(R.id.iv_circle_calon_tt2_xxx_qu)
    ImageView iv_circle_calon_tt2_xxx_qu;
    @BindView(R.id.iv_circle_calon_tt3_xxx_qu)
    ImageView iv_circle_calon_tt3_xxx_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxxi_qu)
    ImageView iv_circle_data_kesehatanxxxi_qu;
    @BindView(R.id.iv_circle_data_kesehatanxxxi_isi_qu)
    ImageView iv_circle_data_kesehatanxxxi_isi_qu;
    @BindView(R.id.iv_circle_calon_ppxxxi_qu)
    ImageView iv_circle_calon_ppxxxi_qu;
    @BindView(R.id.iv_circle_calon_tt1_xxxi_qu)
    ImageView iv_circle_calon_tt1_xxxi_qu;
    @BindView(R.id.iv_circle_calon_ttxxxi_qu)
    ImageView iv_circle_calon_ttxxxi_qu;
    @BindView(R.id.iv_circle_calon_tt2_xxxi_qu)
    ImageView iv_circle_calon_tt2_xxxi_qu;
    @BindView(R.id.iv_circle_calon_tt3_xxxi_qu)
    ImageView iv_circle_calon_tt3_xxxi_qu;
    @BindView(R.id.iv_circle_data_kesehatan2_qu)
    ImageView iv_circle_data_kesehatan2_qu;
    @BindView(R.id.iv_circle_data_kesehatan2_isi_qu)
    ImageView iv_circle_data_kesehatan2_isi_qu;
    @BindView(R.id.iv_circle_calon_pp_qu)
    ImageView iv_circle_calon_pp_qu;
    @BindView(R.id.iv_circle_calon_tt_qu)
    ImageView iv_circle_calon_tt_qu;
    @BindView(R.id.iv_circle_calon_tt1_qu)
    ImageView iv_circle_calon_tt1_qu;
    @BindView(R.id.iv_circle_calon_tt2_qu)
    ImageView iv_circle_calon_tt2_qu;
    @BindView(R.id.iv_circle_calon_tt3_qu)
    ImageView iv_circle_calon_tt3_qu;
    @BindView(R.id.iv_circle_data_kesehatan2_ket_qu)
    ImageView iv_circle_data_kesehatan2_ket_qu;
    @BindView(R.id.iv_circle_data_kesehatan3_qu)
    ImageView iv_circle_data_kesehatan3_qu;
    @BindView(R.id.iv_circle_data_kesehatan3_isi_qu)
    ImageView iv_circle_data_kesehatan3_isi_qu;
    @BindView(R.id.iv_circle_data_kesehatan3_isi2_qu)
    ImageView iv_circle_data_kesehatan3_isi2_qu;
    @BindView(R.id.iv_circle_calon_pp3a_qu)
    ImageView iv_circle_calon_pp3a_qu;
    @BindView(R.id.iv_circle_calon_tt3a_qu)
    ImageView iv_circle_calon_tt3a_qu;
    @BindView(R.id.iv_circle_calon_tt1_3a_qu)
    ImageView iv_circle_calon_tt1_3a_qu;
    @BindView(R.id.iv_circle_calon_tt2_3a_qu)
    ImageView iv_circle_calon_tt2_3a_qu;
    @BindView(R.id.iv_circle_calon_tt3_3a_qu)
    ImageView iv_circle_calon_tt3_3a_qu;
    @BindView(R.id.iv_circle_data_kesehatan3b_qu)
    ImageView iv_circle_data_kesehatan3b_qu;
    @BindView(R.id.iv_circle_data_kesehatan3b_isi_qu)
    ImageView iv_circle_data_kesehatan3b_isi_qu;
    @BindView(R.id.iv_circle_calon_pp3b_qu)
    ImageView iv_circle_calon_pp3b_qu;
    @BindView(R.id.iv_circle_calon_tt3b_qu)
    ImageView iv_circle_calon_tt3b_qu;
    @BindView(R.id.iv_circle_calon_tt1_3b_qu)
    ImageView iv_circle_calon_tt1_3b_qu;
    @BindView(R.id.iv_circle_calon_tt2_3b_qu)
    ImageView iv_circle_calon_tt2_3b_qu;
    @BindView(R.id.iv_circle_calon_tt3_3b_qu)
    ImageView iv_circle_calon_tt3_3b_qu;
    @BindView(R.id.iv_circle_data_kesehatan3c_qu)
    ImageView iv_circle_data_kesehatan3c_qu;
    @BindView(R.id.iv_circle_data_kesehatan3c_isi_qu)
    ImageView iv_circle_data_kesehatan3c_isi_qu;
    @BindView(R.id.iv_circle_calon_pp3c_qu)
    ImageView iv_circle_calon_pp3c_qu;
    @BindView(R.id.iv_circle_calon_tt3c_qu)
    ImageView iv_circle_calon_tt3c_qu;
    @BindView(R.id.iv_circle_calon_tt1_3c_qu)
    ImageView iv_circle_calon_tt1_3c_qu;
    @BindView(R.id.iv_circle_calon_tt2_3c_qu)
    ImageView iv_circle_calon_tt2_3c_qu;
    @BindView(R.id.iv_circle_calon_tt3_3c_qu)
    ImageView iv_circle_calon_tt3_3c_qu;
    @BindView(R.id.iv_circle_data_kesehatan3d_qu)
    ImageView iv_circle_data_kesehatan3d_qu;
    @BindView(R.id.iv_circle_data_kesehatan3d_isi_qu)
    ImageView iv_circle_data_kesehatan3d_isi_qu;
    @BindView(R.id.iv_circle_calon_pp3d_qu)
    ImageView iv_circle_calon_pp3d_qu;
    @BindView(R.id.iv_circle_calon_tt3d_qu)
    ImageView iv_circle_calon_tt3d_qu;
    @BindView(R.id.iv_circle_calon_tt1_3d_qu)
    ImageView iv_circle_calon_tt1_3d_qu;
    @BindView(R.id.iv_circle_calon_tt2_3d_qu)
    ImageView iv_circle_calon_tt2_3d_qu;
    @BindView(R.id.iv_circle_calon_tt3_3d_qu)
    ImageView iv_circle_calon_tt3_3d_qu;
    @BindView(R.id.iv_circle_data_kesehatan4_qu)
    ImageView iv_circle_data_kesehatan4_qu;
    @BindView(R.id.iv_circle_data_kesehatan4_isi_qu)
    ImageView iv_circle_data_kesehatan4_isi_qu;
    @BindView(R.id.iv_circle_calon_pp4_qu)
    ImageView iv_circle_calon_pp4_qu;
    @BindView(R.id.iv_circle_calon_tt4_qu)
    ImageView iv_circle_calon_tt4_qu;
    @BindView(R.id.iv_circle_calon_tt1_4_qu)
    ImageView iv_circle_calon_tt1_4_qu;
    @BindView(R.id.iv_circle_calon_tt2_4_qu)
    ImageView iv_circle_calon_tt2_4_qu;
    @BindView(R.id.iv_circle_calon_tt3_4_qu)
    ImageView iv_circle_calon_tt3_4_qu;
    @BindView(R.id.iv_circle_data_kesehatan4_ket_qu)
    ImageView iv_circle_data_kesehatan4_ket_qu;
    @BindView(R.id.iv_circle_data_kesehatan5_qu)
    ImageView iv_circle_data_kesehatan5_qu;
    @BindView(R.id.iv_circle_data_kesehatan5_isi_qu)
    ImageView iv_circle_data_kesehatan5_isi_qu;
    @BindView(R.id.iv_circle_calon_pp5_qu)
    ImageView iv_circle_calon_pp5_qu;
    @BindView(R.id.iv_circle_calon_tt5_qu)
    ImageView iv_circle_calon_tt5_qu;
    @BindView(R.id.iv_circle_calon_tt1_5_qu)
    ImageView iv_circle_calon_tt1_5_qu;
    @BindView(R.id.iv_circle_calon_tt2_5_qu)
    ImageView iv_circle_calon_tt2_5_qu;
    @BindView(R.id.iv_circle_calon_tt3_5_qu)
    ImageView iv_circle_calon_tt3_5_qu;
    @BindView(R.id.iv_circle_data_kesehatan6_qu)
    ImageView iv_circle_data_kesehatan6_qu;
    @BindView(R.id.iv_circle_data_kesehatan6_isi_qu)
    ImageView iv_circle_data_kesehatan6_isi_qu;
    @BindView(R.id.iv_circle_calon_pp6_qu)
    ImageView iv_circle_calon_pp6_qu;
    @BindView(R.id.iv_circle_calon_tt6_qu)
    ImageView iv_circle_calon_tt6_qu;
    @BindView(R.id.iv_circle_calon_tt1_6_qu)
    ImageView iv_circle_calon_tt1_6_qu;
    @BindView(R.id.iv_circle_calon_tt2_6_qu)
    ImageView iv_circle_calon_tt2_6_qu;
    @BindView(R.id.iv_circle_calon_tt3_6_qu)
    ImageView iv_circle_calon_tt3_6_qu;
    @BindView(R.id.iv_circle_data_kesehatan7_qu)
    ImageView iv_circle_data_kesehatan7_qu;
    @BindView(R.id.iv_circle_data_kesehatan7_isi_qu)
    ImageView iv_circle_data_kesehatan7_isi_qu;
    @BindView(R.id.iv_circle_calon_pp7_qu)
    ImageView iv_circle_calon_pp7_qu;
    @BindView(R.id.iv_circle_calon_tt7_qu)
    ImageView iv_circle_calon_tt7_qu;
    @BindView(R.id.iv_circle_calon_tt1_7_qu)
    ImageView iv_circle_calon_tt1_7_qu;
    @BindView(R.id.iv_circle_calon_tt2_7_qu)
    ImageView iv_circle_calon_tt2_7_qu;
    @BindView(R.id.iv_circle_calon_tt3_7_qu)
    ImageView iv_circle_calon_tt3_7_qu;
    @BindView(R.id.iv_circle_data_kesehatan7a_qu)
    ImageView iv_circle_data_kesehatan7a_qu;
    @BindView(R.id.iv_circle_data_kesehatan7a_isi_qu)
    ImageView iv_circle_data_kesehatan7a_isi_qu;
    @BindView(R.id.iv_circle_data_kesehatan7a_jwb_qu)
    ImageView iv_circle_data_kesehatan7a_jwb_qu;
    @BindView(R.id.iv_circle_data_kesehatan7b_qu)
    ImageView iv_circle_data_kesehatan7b_qu;
    @BindView(R.id.iv_circle_data_kesehatan7b_isi_qu)
    ImageView iv_circle_data_kesehatan7b_isi_qu;
    @BindView(R.id.iv_circle_data_kesehatan7b_jwb_qu)
    ImageView iv_circle_data_kesehatan7b_jwb_qu;
    @BindView(R.id.iv_circle_data_kesehatan7c_qu)
    ImageView iv_circle_data_kesehatan7c_qu;
    @BindView(R.id.iv_circle_data_kesehatan7c_isi_qu)
    ImageView iv_circle_data_kesehatan7c_isi_qu;
    @BindView(R.id.iv_circle_data_kesehatan7c_jwb_qu)
    ImageView iv_circle_data_kesehatan7c_jwb_qu;
    @BindView(R.id.iv_circle_data_kesehatan7d_qu)
    ImageView iv_circle_data_kesehatan7d_qu;
    @BindView(R.id.iv_circle_data_kesehatan7d_isi_qu)
    ImageView iv_circle_data_kesehatan7d_isi_qu;
    @BindView(R.id.iv_circle_data_kesehatan7d_jwb_qu)
    ImageView iv_circle_data_kesehatan7d_jwb_qu;
    @BindView(R.id.iv_circle_data_kesehatan8_qu)
    ImageView iv_circle_data_kesehatan8_qu;
    @BindView(R.id.iv_circle_data_kesehatan8_isi_qu)
    ImageView iv_circle_data_kesehatan8_isi_qu;
    @BindView(R.id.iv_circle_data_kesehatan8a_qu)
    ImageView iv_circle_data_kesehatan8a_qu;
    @BindView(R.id.iv_circle_data_kesehatan8a_isi_qu)
    ImageView iv_circle_data_kesehatan8a_isi_qu;
    @BindView(R.id.iv_circle_calon_pp8a_qu)
    ImageView iv_circle_calon_pp8a_qu;
    @BindView(R.id.iv_circle_calon_tt8a_qu)
    ImageView iv_circle_calon_tt8a_qu;
    @BindView(R.id.iv_circle_calon_tt1_8a_qu)
    ImageView iv_circle_calon_tt1_8a_qu;
    @BindView(R.id.iv_circle_calon_tt2_8a_qu)
    ImageView iv_circle_calon_tt2_8a_qu;
    @BindView(R.id.iv_circle_calon_tt3_8a_qu)
    ImageView iv_circle_calon_tt3_8a_qu;
    @BindView(R.id.iv_circle_data_kesehatan8a_ket_qu)
    ImageView iv_circle_data_kesehatan8a_ket_qu;
    @BindView(R.id.iv_circle_data_kesehatan8b_qu)
    ImageView iv_circle_data_kesehatan8b_qu;
    @BindView(R.id.iv_circle_data_kesehatan8b_isi_qu)
    ImageView iv_circle_data_kesehatan8b_isi_qu;
    @BindView(R.id.iv_circle_calon_pp8b_qu)
    ImageView iv_circle_calon_pp8b_qu;
    @BindView(R.id.iv_circle_calon_tt8b_qu)
    ImageView iv_circle_calon_tt8b_qu;
    @BindView(R.id.iv_circle_calon_tt1_8b_qu)
    ImageView iv_circle_calon_tt1_8b_qu;
    @BindView(R.id.iv_circle_calon_tt2_8b_qu)
    ImageView iv_circle_calon_tt2_8b_qu;
    @BindView(R.id.iv_circle_calon_tt3_8b_qu)
    ImageView iv_circle_calon_tt3_8b_qu;
    @BindView(R.id.iv_circle_data_kesehatan8c_qu)
    ImageView iv_circle_data_kesehatan8c_qu;
    @BindView(R.id.iv_circle_data_kesehatan8c_isi_qu)
    ImageView iv_circle_data_kesehatan8c_isi_qu;
    @BindView(R.id.iv_circle_calon_pp8c_qu)
    ImageView iv_circle_calon_pp8c_qu;
    @BindView(R.id.iv_circle_calon_tt8c_qu)
    ImageView iv_circle_calon_tt8c_qu;
    @BindView(R.id.iv_circle_calon_tt1_8c_qu)
    ImageView iv_circle_calon_tt1_8c_qu;
    @BindView(R.id.iv_circle_calon_tt2_8c_qu)
    ImageView iv_circle_calon_tt2_8c_qu;
    @BindView(R.id.iv_circle_calon_tt3_8c_qu)
    ImageView iv_circle_calon_tt3_8c_qu;
    @BindView(R.id.iv_circle_data_kesehatan9_qu)
    ImageView iv_circle_data_kesehatan9_qu;
    @BindView(R.id.iv_circle_data_kesehatan9_isi_qu)
    ImageView iv_circle_data_kesehatan9_isi_qu;
    @BindView(R.id.iv_circle_calon_pp9_qu)
    ImageView iv_circle_calon_pp9_qu;
    @BindView(R.id.iv_circle_calon_tt9_qu)
    ImageView iv_circle_calon_tt9_qu;
    @BindView(R.id.iv_circle_calon_tt1_9_qu)
    ImageView iv_circle_calon_tt1_9_qu;
    @BindView(R.id.iv_circle_calon_tt2_9_qu)
    ImageView iv_circle_calon_tt2_9_qu;
    @BindView(R.id.iv_circle_calon_tt3_9_qu)
    ImageView iv_circle_calon_tt3_9_qu;
    @BindView(R.id.iv_circle_data_kesehatan10_qu)
    ImageView iv_circle_data_kesehatan10_qu;
    @BindView(R.id.iv_circle_data_kesehatan10_isi_qu)
    ImageView iv_circle_data_kesehatan10_isi_qu;

    @BindView(R.id.img_tambah_data_qu)
    ImageButton img_tambah_data_qu;
    @BindView(R.id.img_tambah_data2_qu)
    ImageButton img_tambah_data2_qu;
    @BindView(R.id.img_tambah_data_calon_pemegang_polis_qu)
    ImageButton img_tambah_data_calon_pemegang_polis_qu;
    @BindView(R.id.img_tambah_data_calon_tertanggung_qu)
    ImageButton img_tambah_data_calon_tertanggung_qu;
    @BindView(R.id.img_tambah_data_keterangan_kesehatan_qu)
    ImageButton img_tambah_data_keterangan_kesehatan_qu;


    @BindView(R.id.option_grup_tt_no1)
    RadioGroup option_grup_tt_no1;
    @BindView(R.id.option_grup_tt_no2)
    RadioGroup option_grup_tt_no2;
    @BindView(R.id.option_grup_tt_no3)
    RadioGroup option_grup_tt_no3;
    @BindView(R.id.option_grup_tt_no4)
    RadioGroup option_grup_tt_no4;
    @BindView(R.id.option_grup_tt_no5)
    RadioGroup option_grup_tt_no5;
    @BindView(R.id.option_grup_tt_no6)
    RadioGroup option_grup_tt_no6;
    @BindView(R.id.option_grup_tt_no7)
    RadioGroup option_grup_tt_no7;
    @BindView(R.id.option_grup_tt_no8)
    RadioGroup option_grup_tt_no8;
    @BindView(R.id.option_grup_tt_no8b)
    RadioGroup option_grup_tt_no8b;
    @BindView(R.id.option_grup_tt_no9)
    RadioGroup option_grup_tt_no9;
    @BindView(R.id.option_grup_tt_no10)
    RadioGroup option_grup_tt_no10;
    @BindView(R.id.option_grup_tt_no11b)
    RadioGroup option_grup_tt_no11b;
    @BindView(R.id.option_grup_pp_no13)
    RadioGroup option_grup_pp_no13;
    @BindView(R.id.option_grup_pp_no14)
    RadioGroup option_grup_pp_no14;

    @BindView(R.id.option_grup_dk_ppno1l)
    RadioGroup option_grup_dk_ppno1l;
    @BindView(R.id.option_grup_dk_ttno1l)
    RadioGroup option_grup_dk_ttno1l;
    @BindView(R.id.option_grup_dk_tt1no1l)
    RadioGroup option_grup_dk_tt1no1l;
    @BindView(R.id.option_grup_dk_tt2no1l)
    RadioGroup option_grup_dk_tt2no1l;
    @BindView(R.id.option_grup_dk_tt3no1l)
    RadioGroup option_grup_dk_tt3no1l;
    @BindView(R.id.option_grup_dk_ppno1ii)
    RadioGroup option_grup_dk_ppno1ii;
    @BindView(R.id.option_grup_dk_ttno1ii)
    RadioGroup option_grup_dk_ttno1ii;
    @BindView(R.id.option_grup_dk_tt1no1ii)
    RadioGroup option_grup_dk_tt1no1ii;
    @BindView(R.id.option_grup_dk_tt2no1ii)
    RadioGroup option_grup_dk_tt2no1ii;
    @BindView(R.id.option_grup_dk_tt3no1ii)
    RadioGroup option_grup_dk_tt3no1ii;
    @BindView(R.id.option_grup_dk_ppno1iii)
    RadioGroup option_grup_dk_ppno1iii;
    @BindView(R.id.option_grup_dk_ttno1iii)
    RadioGroup option_grup_dk_ttno1iii;
    @BindView(R.id.option_grup_dk_tt1no1iii)
    RadioGroup option_grup_dk_tt1no1iii;
    @BindView(R.id.option_grup_dk_tt2no1iii)
    RadioGroup option_grup_dk_tt2no1iii;
    @BindView(R.id.option_grup_dk_tt3no1iii)
    RadioGroup option_grup_dk_tt3no1iii;
    @BindView(R.id.option_grup_dk_ppno1iv)
    RadioGroup option_grup_dk_ppno1iv;
    @BindView(R.id.option_grup_dk_ttno1iv)
    RadioGroup option_grup_dk_ttno1iv;
    @BindView(R.id.option_grup_dk_tt1no1iv)
    RadioGroup option_grup_dk_tt1no1iv;
    @BindView(R.id.option_grup_dk_tt2no1iv)
    RadioGroup option_grup_dk_tt2no1iv;
    @BindView(R.id.option_grup_dk_tt3no1iv)
    RadioGroup option_grup_dk_tt3no1iv;
    @BindView(R.id.option_grup_dk_ppno1v)
    RadioGroup option_grup_dk_ppno1v;
    @BindView(R.id.option_grup_dk_ttno1v)
    RadioGroup option_grup_dk_ttno1v;
    @BindView(R.id.option_grup_dk_tt1no1v)
    RadioGroup option_grup_dk_tt1no1v;
    @BindView(R.id.option_grup_dk_tt2no1v)
    RadioGroup option_grup_dk_tt2no1v;
    @BindView(R.id.option_grup_dk_tt3no1v)
    RadioGroup option_grup_dk_tt3no1v;
    @BindView(R.id.option_grup_dk_ppno1vi)
    RadioGroup option_grup_dk_ppno1vi;
    @BindView(R.id.option_grup_dk_ttno1vi)
    RadioGroup option_grup_dk_ttno1vi;
    @BindView(R.id.option_grup_dk_tt1no1vi)
    RadioGroup option_grup_dk_tt1no1vi;
    @BindView(R.id.option_grup_dk_tt2no1vi)
    RadioGroup option_grup_dk_tt2no1vi;
    @BindView(R.id.option_grup_dk_tt3no1vi)
    RadioGroup option_grup_dk_tt3no1vi;
    @BindView(R.id.option_grup_dk_ppno1vii)
    RadioGroup option_grup_dk_ppno1vii;
    @BindView(R.id.option_grup_dk_ttno1vii)
    RadioGroup option_grup_dk_ttno1vii;
    @BindView(R.id.option_grup_dk_tt1no1vii)
    RadioGroup option_grup_dk_tt1no1vii;
    @BindView(R.id.option_grup_dk_tt2no1vii)
    RadioGroup option_grup_dk_tt2no1vii;
    @BindView(R.id.option_grup_dk_tt3no1vii)
    RadioGroup option_grup_dk_tt3no1vii;
    @BindView(R.id.option_grup_dk_ppno1viii)
    RadioGroup option_grup_dk_ppno1viii;
    @BindView(R.id.option_grup_dk_ttno1viii)
    RadioGroup option_grup_dk_ttno1viii;
    @BindView(R.id.option_grup_dk_tt1no1viii)
    RadioGroup option_grup_dk_tt1no1viii;
    @BindView(R.id.option_grup_dk_tt2no1viii)
    RadioGroup option_grup_dk_tt2no1viii;
    @BindView(R.id.option_grup_dk_tt3no1viii)
    RadioGroup option_grup_dk_tt3no1viii;
    @BindView(R.id.option_grup_dk_ppno1ix)
    RadioGroup option_grup_dk_ppno1ix;
    @BindView(R.id.option_grup_dk_ttno1ix)
    RadioGroup option_grup_dk_ttno1ix;
    @BindView(R.id.option_grup_dk_tt1no1ix)
    RadioGroup option_grup_dk_tt1no1ix;
    @BindView(R.id.option_grup_dk_tt2no1ix)
    RadioGroup option_grup_dk_tt2no1ix;
    @BindView(R.id.option_grup_dk_tt3no1ix)
    RadioGroup option_grup_dk_tt3no1ix;
    @BindView(R.id.option_grup_dk_ppno1x)
    RadioGroup option_grup_dk_ppno1x;
    @BindView(R.id.option_grup_dk_ttno1x)
    RadioGroup option_grup_dk_ttno1x;
    @BindView(R.id.option_grup_dk_tt1no1x)
    RadioGroup option_grup_dk_tt1no1x;
    @BindView(R.id.option_grup_dk_tt2no1x)
    RadioGroup option_grup_dk_tt2no1x;
    @BindView(R.id.option_grup_dk_tt3no1x)
    RadioGroup option_grup_dk_tt3no1x;
    @BindView(R.id.option_grup_dk_ppno1xi)
    RadioGroup option_grup_dk_ppno1xi;
    @BindView(R.id.option_grup_dk_ttno1xi)
    RadioGroup option_grup_dk_ttno1xi;
    @BindView(R.id.option_grup_dk_tt1no1xi)
    RadioGroup option_grup_dk_tt1no1xi;
    @BindView(R.id.option_grup_dk_tt2no1xi)
    RadioGroup option_grup_dk_tt2no1xi;
    @BindView(R.id.option_grup_dk_tt3no1xi)
    RadioGroup option_grup_dk_tt3no1xi;
    @BindView(R.id.option_grup_dk_ppno1xii)
    RadioGroup option_grup_dk_ppno1xii;
    @BindView(R.id.option_grup_dk_ttno1xii)
    RadioGroup option_grup_dk_ttno1xii;
    @BindView(R.id.option_grup_dk_tt1no1xii)
    RadioGroup option_grup_dk_tt1no1xii;
    @BindView(R.id.option_grup_dk_tt2no1xii)
    RadioGroup option_grup_dk_tt2no1xii;
    @BindView(R.id.option_grup_dk_tt3no1xii)
    RadioGroup option_grup_dk_tt3no1xii;
    @BindView(R.id.option_grup_dk_ppno1xiii)
    RadioGroup option_grup_dk_ppno1xiii;
    @BindView(R.id.option_grup_dk_ttno1xiii)
    RadioGroup option_grup_dk_ttno1xiii;
    @BindView(R.id.option_grup_dk_tt1no1xiii)
    RadioGroup option_grup_dk_tt1no1xiii;
    @BindView(R.id.option_grup_dk_tt2no1xiii)
    RadioGroup option_grup_dk_tt2no1xiii;
    @BindView(R.id.option_grup_dk_tt3no1xiii)
    RadioGroup option_grup_dk_tt3no1xiii;
    @BindView(R.id.option_grup_dk_ppno1xiv)
    RadioGroup option_grup_dk_ppno1xiv;
    @BindView(R.id.option_grup_dk_ttno1xiv)
    RadioGroup option_grup_dk_ttno1xiv;
    @BindView(R.id.option_grup_dk_tt1no1xiv)
    RadioGroup option_grup_dk_tt1no1xiv;
    @BindView(R.id.option_grup_dk_tt2no1xiv)
    RadioGroup option_grup_dk_tt2no1xiv;
    @BindView(R.id.option_grup_dk_tt3no1xiv)
    RadioGroup option_grup_dk_tt3no1xiv;
    @BindView(R.id.option_grup_dk_ppno1xv)
    RadioGroup option_grup_dk_ppno1xv;
    @BindView(R.id.option_grup_dk_ttno1xv)
    RadioGroup option_grup_dk_ttno1xv;
    @BindView(R.id.option_grup_dk_tt1no1xv)
    RadioGroup option_grup_dk_tt1no1xv;
    @BindView(R.id.option_grup_dk_tt2no1xv)
    RadioGroup option_grup_dk_tt2no1xv;
    @BindView(R.id.option_grup_dk_tt3no1xv)
    RadioGroup option_grup_dk_tt3no1xv;
    @BindView(R.id.option_grup_dk_ppno1xvi)
    RadioGroup option_grup_dk_ppno1xvi;
    @BindView(R.id.option_grup_dk_ttno1xvi)
    RadioGroup option_grup_dk_ttno1xvi;
    @BindView(R.id.option_grup_dk_tt1no1xvi)
    RadioGroup option_grup_dk_tt1no1xvi;
    @BindView(R.id.option_grup_dk_tt2no1xvi)
    RadioGroup option_grup_dk_tt2no1xvi;
    @BindView(R.id.option_grup_dk_tt3no1xvi)
    RadioGroup option_grup_dk_tt3no1xvi;
    @BindView(R.id.option_grup_dk_ppno1xvii)
    RadioGroup option_grup_dk_ppno1xvii;
    @BindView(R.id.option_grup_dk_ttno1xvii)
    RadioGroup option_grup_dk_ttno1xvii;
    @BindView(R.id.option_grup_dk_tt1no1xvii)
    RadioGroup option_grup_dk_tt1no1xvii;
    @BindView(R.id.option_grup_dk_tt2no1xvii)
    RadioGroup option_grup_dk_tt2no1xvii;
    @BindView(R.id.option_grup_dk_tt3no1xvii)
    RadioGroup option_grup_dk_tt3no1xvii;
    @BindView(R.id.option_grup_dk_ppno1xviii)
    RadioGroup option_grup_dk_ppno1xviii;
    @BindView(R.id.option_grup_dk_ttno1xviii)
    RadioGroup option_grup_dk_ttno1xviii;
    @BindView(R.id.option_grup_dk_tt1no1xviii)
    RadioGroup option_grup_dk_tt1no1xviii;
    @BindView(R.id.option_grup_dk_tt2no1xviii)
    RadioGroup option_grup_dk_tt2no1xviii;
    @BindView(R.id.option_grup_dk_tt3no1xviii)
    RadioGroup option_grup_dk_tt3no1xviii;
    @BindView(R.id.option_grup_dk_ppno1xix)
    RadioGroup option_grup_dk_ppno1xix;
    @BindView(R.id.option_grup_dk_ttno1xix)
    RadioGroup option_grup_dk_ttno1xix;
    @BindView(R.id.option_grup_dk_tt1no1xix)
    RadioGroup option_grup_dk_tt1no1xix;
    @BindView(R.id.option_grup_dk_tt2no1xix)
    RadioGroup option_grup_dk_tt2no1xix;
    @BindView(R.id.option_grup_dk_tt3no1xix)
    RadioGroup option_grup_dk_tt3no1xix;
    @BindView(R.id.option_grup_dk_ppno1xx)
    RadioGroup option_grup_dk_ppno1xx;
    @BindView(R.id.option_grup_dk_ttno1xx)
    RadioGroup option_grup_dk_ttno1xx;
    @BindView(R.id.option_grup_dk_tt1no1xx)
    RadioGroup option_grup_dk_tt1no1xx;
    @BindView(R.id.option_grup_dk_tt2no1xx)
    RadioGroup option_grup_dk_tt2no1xx;
    @BindView(R.id.option_grup_dk_tt3no1xx)
    RadioGroup option_grup_dk_tt3no1xx;
    @BindView(R.id.option_grup_dk_ppno1xxi)
    RadioGroup option_grup_dk_ppno1xxi;
    @BindView(R.id.option_grup_dk_ttno1xxi)
    RadioGroup option_grup_dk_ttno1xxi;
    @BindView(R.id.option_grup_dk_tt1no1xxi)
    RadioGroup option_grup_dk_tt1no1xxi;
    @BindView(R.id.option_grup_dk_tt2no1xxi)
    RadioGroup option_grup_dk_tt2no1xxi;
    @BindView(R.id.option_grup_dk_tt3no1xxi)
    RadioGroup option_grup_dk_tt3no1xxi;
    @BindView(R.id.option_grup_dk_ppno1xxii)
    RadioGroup option_grup_dk_ppno1xxii;
    @BindView(R.id.option_grup_dk_ttno1xxii)
    RadioGroup option_grup_dk_ttno1xxii;
    @BindView(R.id.option_grup_dk_tt1no1xxii)
    RadioGroup option_grup_dk_tt1no1xxii;
    @BindView(R.id.option_grup_dk_tt2no1xxii)
    RadioGroup option_grup_dk_tt2no1xxii;
    @BindView(R.id.option_grup_dk_tt3no1xxii)
    RadioGroup option_grup_dk_tt3no1xxii;
    @BindView(R.id.option_grup_dk_ppno1xxiii)
    RadioGroup option_grup_dk_ppno1xxiii;
    @BindView(R.id.option_grup_dk_ttno1xxiii)
    RadioGroup option_grup_dk_ttno1xxiii;
    @BindView(R.id.option_grup_dk_tt1no1xxiii)
    RadioGroup option_grup_dk_tt1no1xxiii;
    @BindView(R.id.option_grup_dk_tt2no1xxiii)
    RadioGroup option_grup_dk_tt2no1xxiii;
    @BindView(R.id.option_grup_dk_tt3no1xxiii)
    RadioGroup option_grup_dk_tt3no1xxiii;
    @BindView(R.id.option_grup_dk_ppno1xxiv)
    RadioGroup option_grup_dk_ppno1xxiv;
    @BindView(R.id.option_grup_dk_ttno1xxiv)
    RadioGroup option_grup_dk_ttno1xxiv;
    @BindView(R.id.option_grup_dk_tt1no1xxiv)
    RadioGroup option_grup_dk_tt1no1xxiv;
    @BindView(R.id.option_grup_dk_tt2no1xxiv)
    RadioGroup option_grup_dk_tt2no1xxiv;
    @BindView(R.id.option_grup_dk_tt3no1xxiv)
    RadioGroup option_grup_dk_tt3no1xxiv;
    @BindView(R.id.option_grup_dk_ppno1xxv)
    RadioGroup option_grup_dk_ppno1xxv;
    @BindView(R.id.option_grup_dk_ttno1xxv)
    RadioGroup option_grup_dk_ttno1xxv;
    @BindView(R.id.option_grup_dk_tt1no1xxv)
    RadioGroup option_grup_dk_tt1no1xxv;
    @BindView(R.id.option_grup_dk_tt2no1xxv)
    RadioGroup option_grup_dk_tt2no1xxv;
    @BindView(R.id.option_grup_dk_tt3no1xxv)
    RadioGroup option_grup_dk_tt3no1xxv;
    @BindView(R.id.option_grup_dk_ppno1xxvi)
    RadioGroup option_grup_dk_ppno1xxvi;
    @BindView(R.id.option_grup_dk_ttno1xxvi)
    RadioGroup option_grup_dk_ttno1xxvi;
    @BindView(R.id.option_grup_dk_tt1no1xxvi)
    RadioGroup option_grup_dk_tt1no1xxvi;
    @BindView(R.id.option_grup_dk_tt2no1xxvi)
    RadioGroup option_grup_dk_tt2no1xxvi;
    @BindView(R.id.option_grup_dk_tt3no1xxvi)
    RadioGroup option_grup_dk_tt3no1xxvi;
    @BindView(R.id.option_grup_dk_ppno1xxvii)
    RadioGroup option_grup_dk_ppno1xxvii;
    @BindView(R.id.option_grup_dk_ttno1xxvii)
    RadioGroup option_grup_dk_ttno1xxvii;
    @BindView(R.id.option_grup_dk_tt1no1xxvii)
    RadioGroup option_grup_dk_tt1no1xxvii;
    @BindView(R.id.option_grup_dk_tt2no1xxvii)
    RadioGroup option_grup_dk_tt2no1xxvii;
    @BindView(R.id.option_grup_dk_tt3no1xxvii)
    RadioGroup option_grup_dk_tt3no1xxvii;
    @BindView(R.id.option_grup_dk_ppno1xxviii)
    RadioGroup option_grup_dk_ppno1xxviii;
    @BindView(R.id.option_grup_dk_ttno1xxviii)
    RadioGroup option_grup_dk_ttno1xxviii;
    @BindView(R.id.option_grup_dk_tt1no1xxviii)
    RadioGroup option_grup_dk_tt1no1xxviii;
    @BindView(R.id.option_grup_dk_tt2no1xxviii)
    RadioGroup option_grup_dk_tt2no1xxviii;
    @BindView(R.id.option_grup_dk_tt3no1xxviii)
    RadioGroup option_grup_dk_tt3no1xxviii;
    @BindView(R.id.option_grup_dk_ppno1xxix)
    RadioGroup option_grup_dk_ppno1xxix;
    @BindView(R.id.option_grup_dk_ttno1xxix)
    RadioGroup option_grup_dk_ttno1xxix;
    @BindView(R.id.option_grup_dk_tt1no1xxix)
    RadioGroup option_grup_dk_tt1no1xxix;
    @BindView(R.id.option_grup_dk_tt2no1xxix)
    RadioGroup option_grup_dk_tt2no1xxix;
    @BindView(R.id.option_grup_dk_tt3no1xxix)
    RadioGroup option_grup_dk_tt3no1xxix;
    @BindView(R.id.option_grup_dk_ppno1xxx)
    RadioGroup option_grup_dk_ppno1xxx;
    @BindView(R.id.option_grup_dk_ttno1xxx)
    RadioGroup option_grup_dk_ttno1xxx;
    @BindView(R.id.option_grup_dk_tt1no1xxx)
    RadioGroup option_grup_dk_tt1no1xxx;
    @BindView(R.id.option_grup_dk_tt2no1xxx)
    RadioGroup option_grup_dk_tt2no1xxx;
    @BindView(R.id.option_grup_dk_tt3no1xxx)
    RadioGroup option_grup_dk_tt3no1xxx;
    @BindView(R.id.option_grup_dk_ppno1xxxi)
    RadioGroup option_grup_dk_ppno1xxxi;
    @BindView(R.id.option_grup_dk_ttno1xxxi)
    RadioGroup option_grup_dk_ttno1xxxi;
    @BindView(R.id.option_grup_dk_tt1no1xxxi)
    RadioGroup option_grup_dk_tt1no1xxxi;
    @BindView(R.id.option_grup_dk_tt2no1xxxi)
    RadioGroup option_grup_dk_tt2no1xxxi;
    @BindView(R.id.option_grup_dk_tt3no1xxxi)
    RadioGroup option_grup_dk_tt3no1xxxi;
    @BindView(R.id.option_grup_dk_ppno2)
    RadioGroup option_grup_dk_ppno2;
    @BindView(R.id.option_grup_dk_ttno2)
    RadioGroup option_grup_dk_ttno2;
    @BindView(R.id.option_grup_dk_tt1no2)
    RadioGroup option_grup_dk_tt1no2;
    @BindView(R.id.option_grup_dk_tt2no2)
    RadioGroup option_grup_dk_tt2no2;
    @BindView(R.id.option_grup_dk_tt3no2)
    RadioGroup option_grup_dk_tt3no2;
    @BindView(R.id.option_grup_dk_ppno3a)
    RadioGroup option_grup_dk_ppno3a;
    @BindView(R.id.option_grup_dk_ttno3a)
    RadioGroup option_grup_dk_ttno3a;
    @BindView(R.id.option_grup_dk_tt1no3a)
    RadioGroup option_grup_dk_tt1no3a;
    @BindView(R.id.option_grup_dk_tt2no3a)
    RadioGroup option_grup_dk_tt2no3a;
    @BindView(R.id.option_grup_dk_tt3no3a)
    RadioGroup option_grup_dk_tt3no3a;
    @BindView(R.id.option_grup_dk_ppno3b)
    RadioGroup option_grup_dk_ppno3b;
    @BindView(R.id.option_grup_dk_ttno3b)
    RadioGroup option_grup_dk_ttno3b;
    @BindView(R.id.option_grup_dk_tt1no3b)
    RadioGroup option_grup_dk_tt1no3b;
    @BindView(R.id.option_grup_dk_tt2no3b)
    RadioGroup option_grup_dk_tt2no3b;
    @BindView(R.id.option_grup_dk_tt3no3b)
    RadioGroup option_grup_dk_tt3no3b;
    @BindView(R.id.option_grup_dk_ppno3c)
    RadioGroup option_grup_dk_ppno3c;
    @BindView(R.id.option_grup_dk_ttno3c)
    RadioGroup option_grup_dk_ttno3c;
    @BindView(R.id.option_grup_dk_tt1no3c)
    RadioGroup option_grup_dk_tt1no3c;
    @BindView(R.id.option_grup_dk_tt2no3c)
    RadioGroup option_grup_dk_tt2no3c;
    @BindView(R.id.option_grup_dk_tt3no3c)
    RadioGroup option_grup_dk_tt3no3c;
    @BindView(R.id.option_grup_dk_ppno3d)
    RadioGroup option_grup_dk_ppno3d;
    @BindView(R.id.option_grup_dk_ttno3d)
    RadioGroup option_grup_dk_ttno3d;
    @BindView(R.id.option_grup_dk_tt1no3d)
    RadioGroup option_grup_dk_tt1no3d;
    @BindView(R.id.option_grup_dk_tt2no3d)
    RadioGroup option_grup_dk_tt2no3d;
    @BindView(R.id.option_grup_dk_tt3no3d)
    RadioGroup option_grup_dk_tt3no3d;
    @BindView(R.id.option_grup_dk_ppno4)
    RadioGroup option_grup_dk_ppno4;
    @BindView(R.id.option_grup_dk_ttno4)
    RadioGroup option_grup_dk_ttno4;
    @BindView(R.id.option_grup_dk_tt1no4)
    RadioGroup option_grup_dk_tt1no4;
    @BindView(R.id.option_grup_dk_tt2no4)
    RadioGroup option_grup_dk_tt2no4;
    @BindView(R.id.option_grup_dk_tt3no4)
    RadioGroup option_grup_dk_tt3no4;
    @BindView(R.id.option_grup_dk_ppno5)
    RadioGroup option_grup_dk_ppno5;
    @BindView(R.id.option_grup_dk_ttno5)
    RadioGroup option_grup_dk_ttno5;
    @BindView(R.id.option_grup_dk_tt1no5)
    RadioGroup option_grup_dk_tt1no5;
    @BindView(R.id.option_grup_dk_tt2no5)
    RadioGroup option_grup_dk_tt2no5;
    @BindView(R.id.option_grup_dk_tt3no5)
    RadioGroup option_grup_dk_tt3no5;
    @BindView(R.id.option_grup_dk_ppno6)
    RadioGroup option_grup_dk_ppno6;
    @BindView(R.id.option_grup_dk_ttno6)
    RadioGroup option_grup_dk_ttno6;
    @BindView(R.id.option_grup_dk_tt1no6)
    RadioGroup option_grup_dk_tt1no6;
    @BindView(R.id.option_grup_dk_tt2no6)
    RadioGroup option_grup_dk_tt2no6;
    @BindView(R.id.option_grup_dk_tt3no6)
    RadioGroup option_grup_dk_tt3no6;
    @BindView(R.id.option_grup_dk_ppno7)
    RadioGroup option_grup_dk_ppno7;
    @BindView(R.id.option_grup_dk_ttno7)
    RadioGroup option_grup_dk_ttno7;
    @BindView(R.id.option_grup_dk_tt1no7)
    RadioGroup option_grup_dk_tt1no7;
    @BindView(R.id.option_grup_dk_tt2no7)
    RadioGroup option_grup_dk_tt2no7;
    @BindView(R.id.option_grup_dk_tt3no7)
    RadioGroup option_grup_dk_tt3no7;
    @BindView(R.id.option_grup_dk_ppno8a)
    RadioGroup option_grup_dk_ppno8a;
    @BindView(R.id.option_grup_dk_ttno8a)
    RadioGroup option_grup_dk_ttno8a;
    @BindView(R.id.option_grup_dk_tt1no8a)
    RadioGroup option_grup_dk_tt1no8a;
    @BindView(R.id.option_grup_dk_tt2no8a)
    RadioGroup option_grup_dk_tt2no8a;
    @BindView(R.id.option_grup_dk_tt3no8a)
    RadioGroup option_grup_dk_tt3no8a;
    @BindView(R.id.option_grup_dk_ppno8b)
    RadioGroup option_grup_dk_ppno8b;
    @BindView(R.id.option_grup_dk_ttno8b)
    RadioGroup option_grup_dk_ttno8b;
    @BindView(R.id.option_grup_dk_tt1no8b)
    RadioGroup option_grup_dk_tt1no8b;
    @BindView(R.id.option_grup_dk_tt2no8b)
    RadioGroup option_grup_dk_tt2no8b;
    @BindView(R.id.option_grup_dk_tt3no8b)
    RadioGroup option_grup_dk_tt3no8b;
    @BindView(R.id.option_grup_dk_ppno8c)
    RadioGroup option_grup_dk_ppno8c;
    @BindView(R.id.option_grup_dk_ttno8c)
    RadioGroup option_grup_dk_ttno8c;
    @BindView(R.id.option_grup_dk_tt1no8c)
    RadioGroup option_grup_dk_tt1no8c;
    @BindView(R.id.option_grup_dk_tt2no8c)
    RadioGroup option_grup_dk_tt2no8c;
    @BindView(R.id.option_grup_dk_tt3no8c)
    RadioGroup option_grup_dk_tt3no8c;
    @BindView(R.id.option_grup_dk_ppno9)
    RadioGroup option_grup_dk_ppno9;
    @BindView(R.id.option_grup_dk_ttno9)
    RadioGroup option_grup_dk_ttno9;
    @BindView(R.id.option_grup_dk_tt1no9)
    RadioGroup option_grup_dk_tt1no9;
    @BindView(R.id.option_grup_dk_tt2no9)
    RadioGroup option_grup_dk_tt2no9;
    @BindView(R.id.option_grup_dk_tt3no9)
    RadioGroup option_grup_dk_tt3no9;


    @BindView(R.id.option_y_tt_no1)
    RadioButton option_y_tt_no1;
    @BindView(R.id.option_n_tt_no1)
    RadioButton option_n_tt_no1;
    @BindView(R.id.option_y_tt_no2)
    RadioButton option_y_tt_no2;
    @BindView(R.id.option_n_tt_no2)
    RadioButton option_n_tt_no2;
    @BindView(R.id.option_y_tt_no3)
    RadioButton option_y_tt_no3;
    @BindView(R.id.option_n_tt_no3)
    RadioButton option_n_tt_no3;
    @BindView(R.id.option_y_tt_no4)
    RadioButton option_y_tt_no4;
    @BindView(R.id.option_n_tt_no4)
    RadioButton option_n_tt_no4;
    @BindView(R.id.option_y_tt_no5)
    RadioButton option_y_tt_no5;
    @BindView(R.id.option_n_tt_no5)
    RadioButton option_n_tt_no5;
    @BindView(R.id.option_y_tt_no6)
    RadioButton option_y_tt_no6;
    @BindView(R.id.option_n_tt_no6)
    RadioButton option_n_tt_no6;
    @BindView(R.id.option_y_tt_no7)
    RadioButton option_y_tt_no7;
    @BindView(R.id.option_n_tt_no7)
    RadioButton option_n_tt_no7;
    @BindView(R.id.option_y_tt_no8)
    RadioButton option_y_tt_no8;
    @BindView(R.id.option_n_tt_no8)
    RadioButton option_n_tt_no8;
    @BindView(R.id.option_y_tt_no8b)
    RadioButton option_y_tt_no8b;
    @BindView(R.id.option_n_tt_no8b)
    RadioButton option_n_tt_no8b;
    @BindView(R.id.option_y_tt_no9)
    RadioButton option_y_tt_no9;
    @BindView(R.id.option_n_tt_no9)
    RadioButton option_n_tt_no9;
    @BindView(R.id.option_y_tt_no10)
    RadioButton option_y_tt_no10;
    @BindView(R.id.option_n_tt_no10)
    RadioButton option_n_tt_no10;
    @BindView(R.id.option_y_tt_no11b)
    RadioButton option_y_tt_no11b;
    @BindView(R.id.option_n_tt_no11b)
    RadioButton option_n_tt_no11b;
    @BindView(R.id.option_y_pp_no13)
    RadioButton option_y_pp_no13;
    @BindView(R.id.option_n_pp_no13)
    RadioButton option_n_pp_no13;
    @BindView(R.id.option_y_pp_no14)
    RadioButton option_y_pp_no14;
    @BindView(R.id.option_n_pp_no14)
    RadioButton option_n_pp_no14;

    @BindView(R.id.option_y_dk_ppno1l)
    RadioButton option_y_dk_ppno1l;
    @BindView(R.id.option_n_dk_ppno1l)
    RadioButton option_n_dk_ppno1l;
    @BindView(R.id.option_y_dk_ttno1l)
    RadioButton option_y_dk_ttno1l;
    @BindView(R.id.option_n_dk_ttno1l)
    RadioButton option_n_dk_ttno1l;
    @BindView(R.id.option_y_dk_tt1no1l)
    RadioButton option_y_dk_tt1no1l;
    @BindView(R.id.option_n_dk_tt1no1l)
    RadioButton option_n_dk_tt1no1l;
    @BindView(R.id.option_y_dk_tt2no1l)
    RadioButton option_y_dk_tt2no1l;

    @BindView(R.id.option_n_dk_tt2no1l)
    RadioButton option_n_dk_tt2no1l;
    @BindView(R.id.option_y_dk_tt3no1l)
    RadioButton option_y_dk_tt3no1l;
    @BindView(R.id.option_n_dk_tt3no1l)
    RadioButton option_n_dk_tt3no1l;
    @BindView(R.id.option_y_dk_ppno1ii)
    RadioButton option_y_dk_ppno1ii;
    @BindView(R.id.option_n_dk_ppno1ii)
    RadioButton option_n_dk_ppno1ii;
    @BindView(R.id.option_y_dk_ttno1ii)
    RadioButton option_y_dk_ttno1ii;
    @BindView(R.id.option_n_dk_ttno1ii)
    RadioButton option_n_dk_ttno1ii;
    @BindView(R.id.option_y_dk_tt1no1ii)
    RadioButton option_y_dk_tt1no1ii;
    @BindView(R.id.option_n_dk_tt1no1ii)
    RadioButton option_n_dk_tt1no1ii;
    @BindView(R.id.option_y_dk_tt2no1ii)
    RadioButton option_y_dk_tt2no1ii;
    @BindView(R.id.option_n_dk_tt2no1ii)
    RadioButton option_n_dk_tt2no1ii;
    @BindView(R.id.option_y_dk_tt3no1ii)
    RadioButton option_y_dk_tt3no1ii;
    @BindView(R.id.option_n_dk_tt3no1ii)
    RadioButton option_n_dk_tt3no1ii;
    @BindView(R.id.option_y_dk_ppno1iii)
    RadioButton option_y_dk_ppno1iii;
    @BindView(R.id.option_n_dk_ppno1iii)
    RadioButton option_n_dk_ppno1iii;
    @BindView(R.id.option_y_dk_ttno1iii)
    RadioButton option_y_dk_ttno1iii;
    @BindView(R.id.option_n_dk_ttno1iii)
    RadioButton option_n_dk_ttno1iii;
    @BindView(R.id.option_y_dk_tt1no1iii)
    RadioButton option_y_dk_tt1no1iii;
    @BindView(R.id.option_n_dk_tt1no1iii)
    RadioButton option_n_dk_tt1no1iii;
    @BindView(R.id.option_y_dk_tt2no1iii)
    RadioButton option_y_dk_tt2no1iii;
    @BindView(R.id.option_n_dk_tt2no1iii)
    RadioButton option_n_dk_tt2no1iii;
    @BindView(R.id.option_y_dk_tt3no1iii)
    RadioButton option_y_dk_tt3no1iii;
    @BindView(R.id.option_n_dk_tt3no1iii)
    RadioButton option_n_dk_tt3no1iii;

    @BindView(R.id.option_y_dk_ppno1iv)
    RadioButton option_y_dk_ppno1iv;
    @BindView(R.id.option_n_dk_ppno1iv)
    RadioButton option_n_dk_ppno1iv;
    @BindView(R.id.option_y_dk_ttno1iv)
    RadioButton option_y_dk_ttno1iv;
    @BindView(R.id.option_n_dk_ttno1iv)
    RadioButton option_n_dk_ttno1iv;
    @BindView(R.id.option_y_dk_tt1no1iv)
    RadioButton option_y_dk_tt1no1iv;
    @BindView(R.id.option_n_dk_tt1no1iv)
    RadioButton option_n_dk_tt1no1iv;
    @BindView(R.id.option_y_dk_tt2no1iv)
    RadioButton option_y_dk_tt2no1iv;
    @BindView(R.id.option_n_dk_tt2no1iv)
    RadioButton option_n_dk_tt2no1iv;
    @BindView(R.id.option_y_dk_tt3no1iv)
    RadioButton option_y_dk_tt3no1iv;
    @BindView(R.id.option_n_dk_tt3no1iv)
    RadioButton option_n_dk_tt3no1iv;
    @BindView(R.id.option_y_dk_ppno1v)
    RadioButton option_y_dk_ppno1v;
    @BindView(R.id.option_n_dk_ppno1v)
    RadioButton option_n_dk_ppno1v;
    @BindView(R.id.option_y_dk_ttno1v)
    RadioButton option_y_dk_ttno1v;
    @BindView(R.id.option_n_dk_ttno1v)
    RadioButton option_n_dk_ttno1v;
    @BindView(R.id.option_y_dk_tt1no1v)
    RadioButton option_y_dk_tt1no1v;
    @BindView(R.id.option_n_dk_tt1no1v)
    RadioButton option_n_dk_tt1no1v;
    @BindView(R.id.option_y_dk_tt2no1v)
    RadioButton option_y_dk_tt2no1v;
    @BindView(R.id.option_n_dk_tt2no1v)
    RadioButton option_n_dk_tt2no1v;
    @BindView(R.id.option_y_dk_tt3no1v)
    RadioButton option_y_dk_tt3no1v;
    @BindView(R.id.option_n_dk_tt3no1v)
    RadioButton option_n_dk_tt3no1v;
    @BindView(R.id.option_y_dk_ppno1vi)
    RadioButton option_y_dk_ppno1vi;
    @BindView(R.id.option_n_dk_ppno1vi)
    RadioButton option_n_dk_ppno1vi;
    @BindView(R.id.option_y_dk_ttno1vi)
    RadioButton option_y_dk_ttno1vi;
    @BindView(R.id.option_n_dk_ttno1vi)
    RadioButton option_n_dk_ttno1vi;
    @BindView(R.id.option_y_dk_tt1no1vi)
    RadioButton option_y_dk_tt1no1vi;
    @BindView(R.id.option_n_dk_tt1no1vi)
    RadioButton option_n_dk_tt1no1vi;
    @BindView(R.id.option_y_dk_tt2no1vi)
    RadioButton option_y_dk_tt2no1vi;
    @BindView(R.id.option_n_dk_tt2no1vi)
    RadioButton option_n_dk_tt2no1vi;
    @BindView(R.id.option_y_dk_tt3no1vi)
    RadioButton option_y_dk_tt3no1vi;
    @BindView(R.id.option_n_dk_tt3no1vi)
    RadioButton option_n_dk_tt3no1vi;
    @BindView(R.id.option_y_dk_ppno1vii)
    RadioButton option_y_dk_ppno1vii;
    @BindView(R.id.option_n_dk_ppno1vii)
    RadioButton option_n_dk_ppno1vii;
    @BindView(R.id.option_y_dk_ttno1vii)
    RadioButton option_y_dk_ttno1vii;
    @BindView(R.id.option_n_dk_ttno1vii)
    RadioButton option_n_dk_ttno1vii;
    @BindView(R.id.option_y_dk_tt1no1vii)
    RadioButton option_y_dk_tt1no1vii;
    @BindView(R.id.option_n_dk_tt1no1vii)
    RadioButton option_n_dk_tt1no1vii;
    @BindView(R.id.option_y_dk_tt2no1vii)
    RadioButton option_y_dk_tt2no1vii;
    @BindView(R.id.option_n_dk_tt2no1vii)
    RadioButton option_n_dk_tt2no1vii;
    @BindView(R.id.option_y_dk_tt3no1vii)
    RadioButton option_y_dk_tt3no1vii;
    @BindView(R.id.option_n_dk_tt3no1vii)
    RadioButton option_n_dk_tt3no1vii;
    @BindView(R.id.option_y_dk_ppno1viii)
    RadioButton option_y_dk_ppno1viii;
    @BindView(R.id.option_n_dk_ppno1viii)
    RadioButton option_n_dk_ppno1viii;
    @BindView(R.id.option_y_dk_ttno1viii)
    RadioButton option_y_dk_ttno1viii;
    @BindView(R.id.option_n_dk_ttno1viii)
    RadioButton option_n_dk_ttno1viii;
    @BindView(R.id.option_y_dk_tt1no1viii)
    RadioButton option_y_dk_tt1no1viii;
    @BindView(R.id.option_n_dk_tt1no1viii)
    RadioButton option_n_dk_tt1no1viii;
    @BindView(R.id.option_y_dk_tt2no1viii)
    RadioButton option_y_dk_tt2no1viii;
    @BindView(R.id.option_n_dk_tt2no1viii)
    RadioButton option_n_dk_tt2no1viii;
    @BindView(R.id.option_y_dk_tt3no1viii)
    RadioButton option_y_dk_tt3no1viii;
    @BindView(R.id.option_n_dk_tt3no1viii)
    RadioButton option_n_dk_tt3no1viii;
    @BindView(R.id.option_y_dk_ppno1ix)
    RadioButton option_y_dk_ppno1ix;
    @BindView(R.id.option_n_dk_ppno1ix)
    RadioButton option_n_dk_ppno1ix;
    @BindView(R.id.option_y_dk_ttno1ix)
    RadioButton option_y_dk_ttno1ix;
    @BindView(R.id.option_n_dk_ttno1ix)
    RadioButton option_n_dk_ttno1ix;
    @BindView(R.id.option_y_dk_tt1no1ix)
    RadioButton option_y_dk_tt1no1ix;
    @BindView(R.id.option_n_dk_tt1no1ix)
    RadioButton option_n_dk_tt1no1ix;
    @BindView(R.id.option_y_dk_tt2no1ix)
    RadioButton option_y_dk_tt2no1ix;
    @BindView(R.id.option_n_dk_tt2no1ix)
    RadioButton option_n_dk_tt2no1ix;
    @BindView(R.id.option_y_dk_tt3no1ix)
    RadioButton option_y_dk_tt3no1ix;
    @BindView(R.id.option_n_dk_tt3no1ix)
    RadioButton option_n_dk_tt3no1ix;
    @BindView(R.id.option_y_dk_ppno1x)
    RadioButton option_y_dk_ppno1x;
    @BindView(R.id.option_n_dk_ppno1x)
    RadioButton option_n_dk_ppno1x;
    @BindView(R.id.option_y_dk_ttno1x)
    RadioButton option_y_dk_ttno1x;
    @BindView(R.id.option_n_dk_ttno1x)
    RadioButton option_n_dk_ttno1x;
    @BindView(R.id.option_y_dk_tt1no1x)
    RadioButton option_y_dk_tt1no1x;
    @BindView(R.id.option_n_dk_tt1no1x)
    RadioButton option_n_dk_tt1no1x;
    @BindView(R.id.option_y_dk_tt2no1x)
    RadioButton option_y_dk_tt2no1x;
    @BindView(R.id.option_n_dk_tt2no1x)
    RadioButton option_n_dk_tt2no1x;
    @BindView(R.id.option_y_dk_tt3no1x)
    RadioButton option_y_dk_tt3no1x;
    @BindView(R.id.option_n_dk_tt3no1x)
    RadioButton option_n_dk_tt3no1x;
    @BindView(R.id.option_y_dk_ppno1xi)
    RadioButton option_y_dk_ppno1xi;
    @BindView(R.id.option_n_dk_ppno1xi)
    RadioButton option_n_dk_ppno1xi;
    @BindView(R.id.option_y_dk_ttno1xi)
    RadioButton option_y_dk_ttno1xi;
    @BindView(R.id.option_n_dk_ttno1xi)
    RadioButton option_n_dk_ttno1xi;
    @BindView(R.id.option_y_dk_tt1no1xi)
    RadioButton option_y_dk_tt1no1xi;
    @BindView(R.id.option_n_dk_tt1no1xi)
    RadioButton option_n_dk_tt1no1xi;
    @BindView(R.id.option_y_dk_tt2no1xi)
    RadioButton option_y_dk_tt2no1xi;
    @BindView(R.id.option_n_dk_tt2no1xi)
    RadioButton option_n_dk_tt2no1xi;
    @BindView(R.id.option_y_dk_tt3no1xi)
    RadioButton option_y_dk_tt3no1xi;
    @BindView(R.id.option_n_dk_tt3no1xi)
    RadioButton option_n_dk_tt3no1xi;
    @BindView(R.id.option_y_dk_ppno1xii)
    RadioButton option_y_dk_ppno1xii;
    @BindView(R.id.option_n_dk_ppno1xii)
    RadioButton option_n_dk_ppno1xii;
    @BindView(R.id.option_y_dk_ttno1xii)
    RadioButton option_y_dk_ttno1xii;
    @BindView(R.id.option_n_dk_ttno1xii)
    RadioButton option_n_dk_ttno1xii;
    @BindView(R.id.option_y_dk_tt1no1xii)
    RadioButton option_y_dk_tt1no1xii;
    @BindView(R.id.option_n_dk_tt1no1xii)
    RadioButton option_n_dk_tt1no1xii;
    @BindView(R.id.option_y_dk_tt2no1xii)
    RadioButton option_y_dk_tt2no1xii;
    @BindView(R.id.option_n_dk_tt2no1xii)
    RadioButton option_n_dk_tt2no1xii;
    @BindView(R.id.option_y_dk_tt3no1xii)
    RadioButton option_y_dk_tt3no1xii;
    @BindView(R.id.option_n_dk_tt3no1xii)
    RadioButton option_n_dk_tt3no1xii;
    @BindView(R.id.option_y_dk_ppno1xiii)
    RadioButton option_y_dk_ppno1xiii;
    @BindView(R.id.option_n_dk_ppno1xiii)
    RadioButton option_n_dk_ppno1xiii;
    @BindView(R.id.option_y_dk_ttno1xiii)
    RadioButton option_y_dk_ttno1xiii;
    @BindView(R.id.option_n_dk_ttno1xiii)
    RadioButton option_n_dk_ttno1xiii;
    @BindView(R.id.option_y_dk_tt1no1xiii)
    RadioButton option_y_dk_tt1no1xiii;
    @BindView(R.id.option_n_dk_tt1no1xiii)
    RadioButton option_n_dk_tt1no1xiii;
    @BindView(R.id.option_y_dk_tt2no1xiii)
    RadioButton option_y_dk_tt2no1xiii;
    @BindView(R.id.option_n_dk_tt2no1xiii)
    RadioButton option_n_dk_tt2no1xiii;
    @BindView(R.id.option_y_dk_tt3no1xiii)
    RadioButton option_y_dk_tt3no1xiii;
    @BindView(R.id.option_n_dk_tt3no1xiii)
    RadioButton option_n_dk_tt3no1xiii;
    @BindView(R.id.option_y_dk_ppno1xiv)
    RadioButton option_y_dk_ppno1xiv;
    @BindView(R.id.option_n_dk_ppno1xiv)
    RadioButton option_n_dk_ppno1xiv;
    @BindView(R.id.option_y_dk_ttno1xiv)
    RadioButton option_y_dk_ttno1xiv;
    @BindView(R.id.option_n_dk_ttno1xiv)
    RadioButton option_n_dk_ttno1xiv;
    @BindView(R.id.option_y_dk_tt1no1xiv)
    RadioButton option_y_dk_tt1no1xiv;
    @BindView(R.id.option_n_dk_tt1no1xiv)
    RadioButton option_n_dk_tt1no1xiv;
    @BindView(R.id.option_y_dk_tt2no1xiv)
    RadioButton option_y_dk_tt2no1xiv;
    @BindView(R.id.option_n_dk_tt2no1xiv)
    RadioButton option_n_dk_tt2no1xiv;
    @BindView(R.id.option_y_dk_tt3no1xiv)
    RadioButton option_y_dk_tt3no1xiv;
    @BindView(R.id.option_n_dk_tt3no1xiv)
    RadioButton option_n_dk_tt3no1xiv;
    @BindView(R.id.option_y_dk_ppno1xv)
    RadioButton option_y_dk_ppno1xv;
    @BindView(R.id.option_n_dk_ppno1xv)
    RadioButton option_n_dk_ppno1xv;
    @BindView(R.id.option_y_dk_ttno1xv)
    RadioButton option_y_dk_ttno1xv;
    @BindView(R.id.option_n_dk_ttno1xv)
    RadioButton option_n_dk_ttno1xv;
    @BindView(R.id.option_y_dk_tt1no1xv)
    RadioButton option_y_dk_tt1no1xv;
    @BindView(R.id.option_n_dk_tt1no1xv)
    RadioButton option_n_dk_tt1no1xv;
    @BindView(R.id.option_y_dk_tt2no1xv)
    RadioButton option_y_dk_tt2no1xv;
    @BindView(R.id.option_n_dk_tt2no1xv)
    RadioButton option_n_dk_tt2no1xv;
    @BindView(R.id.option_y_dk_tt3no1xv)
    RadioButton option_y_dk_tt3no1xv;
    @BindView(R.id.option_n_dk_tt3no1xv)
    RadioButton option_n_dk_tt3no1xv;
    @BindView(R.id.option_y_dk_ppno1xvi)
    RadioButton option_y_dk_ppno1xvi;
    @BindView(R.id.option_n_dk_ppno1xvi)
    RadioButton option_n_dk_ppno1xvi;
    @BindView(R.id.option_y_dk_ttno1xvi)
    RadioButton option_y_dk_ttno1xvi;
    @BindView(R.id.option_n_dk_ttno1xvi)
    RadioButton option_n_dk_ttno1xvi;
    @BindView(R.id.option_y_dk_tt1no1xvi)
    RadioButton option_y_dk_tt1no1xvi;
    @BindView(R.id.option_n_dk_tt1no1xvi)
    RadioButton option_n_dk_tt1no1xvi;
    @BindView(R.id.option_y_dk_tt2no1xvi)
    RadioButton option_y_dk_tt2no1xvi;
    @BindView(R.id.option_n_dk_tt2no1xvi)
    RadioButton option_n_dk_tt2no1xvi;
    @BindView(R.id.option_y_dk_tt3no1xvi)
    RadioButton option_y_dk_tt3no1xvi;
    @BindView(R.id.option_n_dk_tt3no1xvi)
    RadioButton option_n_dk_tt3no1xvi;
    @BindView(R.id.option_y_dk_ppno1xvii)
    RadioButton option_y_dk_ppno1xvii;
    @BindView(R.id.option_n_dk_ppno1xvii)
    RadioButton option_n_dk_ppno1xvii;
    @BindView(R.id.option_y_dk_ttno1xvii)
    RadioButton option_y_dk_ttno1xvii;
    @BindView(R.id.option_n_dk_ttno1xvii)
    RadioButton option_n_dk_ttno1xvii;
    @BindView(R.id.option_y_dk_tt1no1xvii)
    RadioButton option_y_dk_tt1no1xvii;
    @BindView(R.id.option_n_dk_tt1no1xvii)
    RadioButton option_n_dk_tt1no1xvii;
    @BindView(R.id.option_y_dk_tt2no1xvii)
    RadioButton option_y_dk_tt2no1xvii;
    @BindView(R.id.option_n_dk_tt2no1xvii)
    RadioButton option_n_dk_tt2no1xvii;
    @BindView(R.id.option_y_dk_tt3no1xvii)
    RadioButton option_y_dk_tt3no1xvii;
    @BindView(R.id.option_n_dk_tt3no1xvii)
    RadioButton option_n_dk_tt3no1xvii;
    @BindView(R.id.option_y_dk_ppno1xviii)
    RadioButton option_y_dk_ppno1xviii;
    @BindView(R.id.option_n_dk_ppno1xviii)
    RadioButton option_n_dk_ppno1xviii;
    @BindView(R.id.option_y_dk_ttno1xviii)
    RadioButton option_y_dk_ttno1xviii;
    @BindView(R.id.option_n_dk_ttno1xviii)
    RadioButton option_n_dk_ttno1xviii;
    @BindView(R.id.option_y_dk_tt1no1xviii)
    RadioButton option_y_dk_tt1no1xviii;
    @BindView(R.id.option_n_dk_tt1no1xviii)
    RadioButton option_n_dk_tt1no1xviii;
    @BindView(R.id.option_y_dk_tt2no1xviii)
    RadioButton option_y_dk_tt2no1xviii;
    @BindView(R.id.option_n_dk_tt2no1xviii)
    RadioButton option_n_dk_tt2no1xviii;
    @BindView(R.id.option_y_dk_tt3no1xviii)
    RadioButton option_y_dk_tt3no1xviii;
    @BindView(R.id.option_n_dk_tt3no1xviii)
    RadioButton option_n_dk_tt3no1xviii;
    @BindView(R.id.option_y_dk_ppno1xix)
    RadioButton option_y_dk_ppno1xix;
    @BindView(R.id.option_n_dk_ppno1xix)
    RadioButton option_n_dk_ppno1xix;
    @BindView(R.id.option_y_dk_ttno1xix)
    RadioButton option_y_dk_ttno1xix;
    @BindView(R.id.option_n_dk_ttno1xix)
    RadioButton option_n_dk_ttno1xix;
    @BindView(R.id.option_y_dk_tt1no1xix)
    RadioButton option_y_dk_tt1no1xix;
    @BindView(R.id.option_n_dk_tt1no1xix)
    RadioButton option_n_dk_tt1no1xix;
    @BindView(R.id.option_y_dk_tt2no1xix)
    RadioButton option_y_dk_tt2no1xix;
    @BindView(R.id.option_n_dk_tt2no1xix)
    RadioButton option_n_dk_tt2no1xix;
    @BindView(R.id.option_y_dk_tt3no1xix)
    RadioButton option_y_dk_tt3no1xix;
    @BindView(R.id.option_n_dk_tt3no1xix)
    RadioButton option_n_dk_tt3no1xix;
    @BindView(R.id.option_y_dk_ppno1xx)
    RadioButton option_y_dk_ppno1xx;
    @BindView(R.id.option_n_dk_ppno1xx)
    RadioButton option_n_dk_ppno1xx;
    @BindView(R.id.option_y_dk_ttno1xx)
    RadioButton option_y_dk_ttno1xx;
    @BindView(R.id.option_n_dk_ttno1xx)
    RadioButton option_n_dk_ttno1xx;
    @BindView(R.id.option_y_dk_tt1no1xx)
    RadioButton option_y_dk_tt1no1xx;
    @BindView(R.id.option_n_dk_tt1no1xx)
    RadioButton option_n_dk_tt1no1xx;
    @BindView(R.id.option_y_dk_tt2no1xx)
    RadioButton option_y_dk_tt2no1xx;
    @BindView(R.id.option_n_dk_tt2no1xx)
    RadioButton option_n_dk_tt2no1xx;
    @BindView(R.id.option_y_dk_tt3no1xx)
    RadioButton option_y_dk_tt3no1xx;
    @BindView(R.id.option_n_dk_tt3no1xx)
    RadioButton option_n_dk_tt3no1xx;
    @BindView(R.id.option_y_dk_ppno1xxi)
    RadioButton option_y_dk_ppno1xxi;
    @BindView(R.id.option_n_dk_ppno1xxi)
    RadioButton option_n_dk_ppno1xxi;
    @BindView(R.id.option_y_dk_ttno1xxi)
    RadioButton option_y_dk_ttno1xxi;
    @BindView(R.id.option_n_dk_ttno1xxi)
    RadioButton option_n_dk_ttno1xxi;
    @BindView(R.id.option_y_dk_tt1no1xxi)
    RadioButton option_y_dk_tt1no1xxi;
    @BindView(R.id.option_n_dk_tt1no1xxi)
    RadioButton option_n_dk_tt1no1xxi;
    @BindView(R.id.option_y_dk_tt2no1xxi)
    RadioButton option_y_dk_tt2no1xxi;
    @BindView(R.id.option_n_dk_tt2no1xxi)
    RadioButton option_n_dk_tt2no1xxi;
    @BindView(R.id.option_y_dk_tt3no1xxi)
    RadioButton option_y_dk_tt3no1xxi;
    @BindView(R.id.option_n_dk_tt3no1xxi)
    RadioButton option_n_dk_tt3no1xxi;
    @BindView(R.id.option_y_dk_ppno1xxii)
    RadioButton option_y_dk_ppno1xxii;
    @BindView(R.id.option_n_dk_ppno1xxii)
    RadioButton option_n_dk_ppno1xxii;
    @BindView(R.id.option_y_dk_ttno1xxii)
    RadioButton option_y_dk_ttno1xxii;
    @BindView(R.id.option_n_dk_ttno1xxii)
    RadioButton option_n_dk_ttno1xxii;
    @BindView(R.id.option_y_dk_tt1no1xxii)
    RadioButton option_y_dk_tt1no1xxii;
    @BindView(R.id.option_n_dk_tt1no1xxii)
    RadioButton option_n_dk_tt1no1xxii;
    @BindView(R.id.option_y_dk_tt2no1xxii)
    RadioButton option_y_dk_tt2no1xxii;
    @BindView(R.id.option_n_dk_tt2no1xxii)
    RadioButton option_n_dk_tt2no1xxii;
    @BindView(R.id.option_y_dk_tt3no1xxii)
    RadioButton option_y_dk_tt3no1xxii;
    @BindView(R.id.option_n_dk_tt3no1xxii)
    RadioButton option_n_dk_tt3no1xxii;
    @BindView(R.id.option_y_dk_ppno1xxiii)
    RadioButton option_y_dk_ppno1xxiii;
    @BindView(R.id.option_n_dk_ppno1xxiii)
    RadioButton option_n_dk_ppno1xxiii;
    @BindView(R.id.option_y_dk_ttno1xxiii)
    RadioButton option_y_dk_ttno1xxiii;
    @BindView(R.id.option_n_dk_ttno1xxiii)
    RadioButton option_n_dk_ttno1xxiii;
    @BindView(R.id.option_y_dk_tt1no1xxiii)
    RadioButton option_y_dk_tt1no1xxiii;
    @BindView(R.id.option_n_dk_tt1no1xxiii)
    RadioButton option_n_dk_tt1no1xxiii;
    @BindView(R.id.option_y_dk_tt2no1xxiii)
    RadioButton option_y_dk_tt2no1xxiii;
    @BindView(R.id.option_n_dk_tt2no1xxiii)
    RadioButton option_n_dk_tt2no1xxiii;
    @BindView(R.id.option_y_dk_tt3no1xxiii)
    RadioButton option_y_dk_tt3no1xxiii;
    @BindView(R.id.option_n_dk_tt3no1xxiii)
    RadioButton option_n_dk_tt3no1xxiii;
    @BindView(R.id.option_y_dk_ppno1xxiv)
    RadioButton option_y_dk_ppno1xxiv;
    @BindView(R.id.option_n_dk_ppno1xxiv)
    RadioButton option_n_dk_ppno1xxiv;
    @BindView(R.id.option_y_dk_ttno1xxiv)
    RadioButton option_y_dk_ttno1xxiv;
    @BindView(R.id.option_n_dk_ttno1xxiv)
    RadioButton option_n_dk_ttno1xxiv;
    @BindView(R.id.option_y_dk_tt1no1xxiv)
    RadioButton option_y_dk_tt1no1xxiv;
    @BindView(R.id.option_n_dk_tt1no1xxiv)
    RadioButton option_n_dk_tt1no1xxiv;
    @BindView(R.id.option_y_dk_tt2no1xxiv)
    RadioButton option_y_dk_tt2no1xxiv;
    @BindView(R.id.option_n_dk_tt2no1xxiv)
    RadioButton option_n_dk_tt2no1xxiv;
    @BindView(R.id.option_y_dk_tt3no1xxiv)
    RadioButton option_y_dk_tt3no1xxiv;
    @BindView(R.id.option_n_dk_tt3no1xxiv)
    RadioButton option_n_dk_tt3no1xxiv;
    @BindView(R.id.option_y_dk_ppno1xxv)
    RadioButton option_y_dk_ppno1xxv;
    @BindView(R.id.option_n_dk_ppno1xxv)
    RadioButton option_n_dk_ppno1xxv;
    @BindView(R.id.option_y_dk_ttno1xxv)
    RadioButton option_y_dk_ttno1xxv;
    @BindView(R.id.option_n_dk_ttno1xxv)
    RadioButton option_n_dk_ttno1xxv;
    @BindView(R.id.option_y_dk_tt1no1xxv)
    RadioButton option_y_dk_tt1no1xxv;
    @BindView(R.id.option_n_dk_tt1no1xxv)
    RadioButton option_n_dk_tt1no1xxv;
    @BindView(R.id.option_y_dk_tt2no1xxv)
    RadioButton option_y_dk_tt2no1xxv;
    @BindView(R.id.option_n_dk_tt2no1xxv)
    RadioButton option_n_dk_tt2no1xxv;
    @BindView(R.id.option_y_dk_tt3no1xxv)
    RadioButton option_y_dk_tt3no1xxv;
    @BindView(R.id.option_n_dk_tt3no1xxv)
    RadioButton option_n_dk_tt3no1xxv;
    @BindView(R.id.option_y_dk_ppno1xxvi)
    RadioButton option_y_dk_ppno1xxvi;
    @BindView(R.id.option_n_dk_ppno1xxvi)
    RadioButton option_n_dk_ppno1xxvi;
    @BindView(R.id.option_y_dk_ttno1xxvi)
    RadioButton option_y_dk_ttno1xxvi;
    @BindView(R.id.option_n_dk_ttno1xxvi)
    RadioButton option_n_dk_ttno1xxvi;
    @BindView(R.id.option_y_dk_tt1no1xxvi)
    RadioButton option_y_dk_tt1no1xxvi;
    @BindView(R.id.option_n_dk_tt1no1xxvi)
    RadioButton option_n_dk_tt1no1xxvi;
    @BindView(R.id.option_y_dk_tt2no1xxvi)
    RadioButton option_y_dk_tt2no1xxvi;
    @BindView(R.id.option_n_dk_tt2no1xxvi)
    RadioButton option_n_dk_tt2no1xxvi;
    @BindView(R.id.option_y_dk_tt3no1xxvi)
    RadioButton option_y_dk_tt3no1xxvi;
    @BindView(R.id.option_n_dk_tt3no1xxvi)
    RadioButton option_n_dk_tt3no1xxvi;
    @BindView(R.id.option_y_dk_ppno1xxvii)
    RadioButton option_y_dk_ppno1xxvii;
    @BindView(R.id.option_n_dk_ppno1xxvii)
    RadioButton option_n_dk_ppno1xxvii;
    @BindView(R.id.option_y_dk_ttno1xxvii)
    RadioButton option_y_dk_ttno1xxvii;
    @BindView(R.id.option_n_dk_ttno1xxvii)
    RadioButton option_n_dk_ttno1xxvii;
    @BindView(R.id.option_y_dk_tt1no1xxvii)
    RadioButton option_y_dk_tt1no1xxvii;
    @BindView(R.id.option_n_dk_tt1no1xxvii)
    RadioButton option_n_dk_tt1no1xxvii;
    @BindView(R.id.option_y_dk_tt2no1xxvii)
    RadioButton option_y_dk_tt2no1xxvii;
    @BindView(R.id.option_n_dk_tt2no1xxvii)
    RadioButton option_n_dk_tt2no1xxvii;
    @BindView(R.id.option_y_dk_tt3no1xxvii)
    RadioButton option_y_dk_tt3no1xxvii;
    @BindView(R.id.option_n_dk_tt3no1xxvii)
    RadioButton option_n_dk_tt3no1xxvii;
    @BindView(R.id.option_y_dk_ppno1xxviii)
    RadioButton option_y_dk_ppno1xxviii;
    @BindView(R.id.option_n_dk_ppno1xxviii)
    RadioButton option_n_dk_ppno1xxviii;
    @BindView(R.id.option_y_dk_ttno1xxviii)
    RadioButton option_y_dk_ttno1xxviii;
    @BindView(R.id.option_n_dk_ttno1xxviii)
    RadioButton option_n_dk_ttno1xxviii;
    @BindView(R.id.option_y_dk_tt1no1xxviii)
    RadioButton option_y_dk_tt1no1xxviii;
    @BindView(R.id.option_n_dk_tt1no1xxviii)
    RadioButton option_n_dk_tt1no1xxviii;
    @BindView(R.id.option_y_dk_tt2no1xxviii)
    RadioButton option_y_dk_tt2no1xxviii;
    @BindView(R.id.option_n_dk_tt2no1xxviii)
    RadioButton option_n_dk_tt2no1xxviii;
    @BindView(R.id.option_y_dk_tt3no1xxviii)
    RadioButton option_y_dk_tt3no1xxviii;
    @BindView(R.id.option_n_dk_tt3no1xxviii)
    RadioButton option_n_dk_tt3no1xxviii;
    @BindView(R.id.option_y_dk_ppno1xxix)
    RadioButton option_y_dk_ppno1xxix;
    @BindView(R.id.option_n_dk_ppno1xxix)
    RadioButton option_n_dk_ppno1xxix;
    @BindView(R.id.option_y_dk_ttno1xxix)
    RadioButton option_y_dk_ttno1xxix;
    @BindView(R.id.option_n_dk_ttno1xxix)
    RadioButton option_n_dk_ttno1xxix;
    @BindView(R.id.option_y_dk_tt1no1xxix)
    RadioButton option_y_dk_tt1no1xxix;
    @BindView(R.id.option_n_dk_tt1no1xxix)
    RadioButton option_n_dk_tt1no1xxix;
    @BindView(R.id.option_y_dk_tt2no1xxix)
    RadioButton option_y_dk_tt2no1xxix;
    @BindView(R.id.option_n_dk_tt2no1xxix)
    RadioButton option_n_dk_tt2no1xxix;
    @BindView(R.id.option_y_dk_tt3no1xxix)
    RadioButton option_y_dk_tt3no1xxix;
    @BindView(R.id.option_n_dk_tt3no1xxix)
    RadioButton option_n_dk_tt3no1xxix;
    @BindView(R.id.option_y_dk_ppno1xxx)
    RadioButton option_y_dk_ppno1xxx;
    @BindView(R.id.option_n_dk_ppno1xxx)
    RadioButton option_n_dk_ppno1xxx;
    @BindView(R.id.option_y_dk_ttno1xxx)
    RadioButton option_y_dk_ttno1xxx;
    @BindView(R.id.option_n_dk_ttno1xxx)
    RadioButton option_n_dk_ttno1xxx;
    @BindView(R.id.option_y_dk_tt1no1xxx)
    RadioButton option_y_dk_tt1no1xxx;
    @BindView(R.id.option_n_dk_tt1no1xxx)
    RadioButton option_n_dk_tt1no1xxx;
    @BindView(R.id.option_y_dk_tt2no1xxx)
    RadioButton option_y_dk_tt2no1xxx;
    @BindView(R.id.option_n_dk_tt2no1xxx)
    RadioButton option_n_dk_tt2no1xxx;
    @BindView(R.id.option_y_dk_tt3no1xxx)
    RadioButton option_y_dk_tt3no1xxx;
    @BindView(R.id.option_n_dk_tt3no1xxx)
    RadioButton option_n_dk_tt3no1xxx;
    @BindView(R.id.option_y_dk_ppno1xxxi)
    RadioButton option_y_dk_ppno1xxxi;
    @BindView(R.id.option_n_dk_ppno1xxxi)
    RadioButton option_n_dk_ppno1xxxi;
    @BindView(R.id.option_y_dk_ttno1xxxi)
    RadioButton option_y_dk_ttno1xxxi;
    @BindView(R.id.option_n_dk_ttno1xxxi)
    RadioButton option_n_dk_ttno1xxxi;
    @BindView(R.id.option_y_dk_tt1no1xxxi)
    RadioButton option_y_dk_tt1no1xxxi;
    @BindView(R.id.option_n_dk_tt1no1xxxi)
    RadioButton option_n_dk_tt1no1xxxi;
    @BindView(R.id.option_y_dk_tt2no1xxxi)
    RadioButton option_y_dk_tt2no1xxxi;
    @BindView(R.id.option_n_dk_tt2no1xxxi)
    RadioButton option_n_dk_tt2no1xxxi;
    @BindView(R.id.option_y_dk_tt3no1xxxi)
    RadioButton option_y_dk_tt3no1xxxi;
    @BindView(R.id.option_n_dk_tt3no1xxxi)
    RadioButton option_n_dk_tt3no1xxxi;
    @BindView(R.id.option_y_dk_ppno2)
    RadioButton option_y_dk_ppno2;
    @BindView(R.id.option_n_dk_ppno2)
    RadioButton option_n_dk_ppno2;
    @BindView(R.id.option_y_dk_ttno2)
    RadioButton option_y_dk_ttno2;
    @BindView(R.id.option_n_dk_ttno2)
    RadioButton option_n_dk_ttno2;
    @BindView(R.id.option_y_dk_tt1no2)
    RadioButton option_y_dk_tt1no2;
    @BindView(R.id.option_n_dk_tt1no2)
    RadioButton option_n_dk_tt1no2;
    @BindView(R.id.option_y_dk_tt2no2)
    RadioButton option_y_dk_tt2no2;
    @BindView(R.id.option_n_dk_tt2no2)
    RadioButton option_n_dk_tt2no2;
    @BindView(R.id.option_y_dk_tt3no2)
    RadioButton option_y_dk_tt3no2;
    @BindView(R.id.option_n_dk_tt3no2)
    RadioButton option_n_dk_tt3no2;
    @BindView(R.id.option_y_dk_ppno3a)
    RadioButton option_y_dk_ppno3a;
    @BindView(R.id.option_n_dk_ppno3a)
    RadioButton option_n_dk_ppno3a;
    @BindView(R.id.option_y_dk_ttno3a)
    RadioButton option_y_dk_ttno3a;
    @BindView(R.id.option_n_dk_ttno3a)
    RadioButton option_n_dk_ttno3a;
    @BindView(R.id.option_y_dk_tt1no3a)
    RadioButton option_y_dk_tt1no3a;
    @BindView(R.id.option_n_dk_tt1no3a)
    RadioButton option_n_dk_tt1no3a;
    @BindView(R.id.option_y_dk_tt2no3a)
    RadioButton option_y_dk_tt2no3a;
    @BindView(R.id.option_n_dk_tt2no3a)
    RadioButton option_n_dk_tt2no3a;
    @BindView(R.id.option_y_dk_tt3no3a)
    RadioButton option_y_dk_tt3no3a;
    @BindView(R.id.option_n_dk_tt3no3a)
    RadioButton option_n_dk_tt3no3a;
    @BindView(R.id.option_y_dk_ppno3b)
    RadioButton option_y_dk_ppno3b;
    @BindView(R.id.option_n_dk_ppno3b)
    RadioButton option_n_dk_ppno3b;
    @BindView(R.id.option_y_dk_ttno3b)
    RadioButton option_y_dk_ttno3b;
    @BindView(R.id.option_n_dk_ttno3b)
    RadioButton option_n_dk_ttno3b;
    @BindView(R.id.option_y_dk_tt1no3b)
    RadioButton option_y_dk_tt1no3b;
    @BindView(R.id.option_n_dk_tt1no3b)
    RadioButton option_n_dk_tt1no3b;
    @BindView(R.id.option_y_dk_tt2no3b)
    RadioButton option_y_dk_tt2no3b;
    @BindView(R.id.option_n_dk_tt2no3b)
    RadioButton option_n_dk_tt2no3b;
    @BindView(R.id.option_y_dk_tt3no3b)
    RadioButton option_y_dk_tt3no3b;
    @BindView(R.id.option_n_dk_tt3no3b)
    RadioButton option_n_dk_tt3no3b;
    @BindView(R.id.option_y_dk_ppno3c)
    RadioButton option_y_dk_ppno3c;
    @BindView(R.id.option_n_dk_ppno3c)
    RadioButton option_n_dk_ppno3c;
    @BindView(R.id.option_y_dk_ttno3c)
    RadioButton option_y_dk_ttno3c;
    @BindView(R.id.option_n_dk_ttno3c)
    RadioButton option_n_dk_ttno3c;
    @BindView(R.id.option_y_dk_tt1no3c)
    RadioButton option_y_dk_tt1no3c;
    @BindView(R.id.option_n_dk_tt1no3c)
    RadioButton option_n_dk_tt1no3c;
    @BindView(R.id.option_y_dk_tt2no3c)
    RadioButton option_y_dk_tt2no3c;
    @BindView(R.id.option_n_dk_tt2no3c)
    RadioButton option_n_dk_tt2no3c;
    @BindView(R.id.option_y_dk_tt3no3c)
    RadioButton option_y_dk_tt3no3c;
    @BindView(R.id.option_n_dk_tt3no3c)
    RadioButton option_n_dk_tt3no3c;
    @BindView(R.id.option_y_dk_ppno3d)
    RadioButton option_y_dk_ppno3d;
    @BindView(R.id.option_n_dk_ppno3d)
    RadioButton option_n_dk_ppno3d;
    @BindView(R.id.option_y_dk_ttno3d)
    RadioButton option_y_dk_ttno3d;
    @BindView(R.id.option_n_dk_ttno3d)
    RadioButton option_n_dk_ttno3d;
    @BindView(R.id.option_y_dk_tt1no3d)
    RadioButton option_y_dk_tt1no3d;
    @BindView(R.id.option_n_dk_tt1no3d)
    RadioButton option_n_dk_tt1no3d;
    @BindView(R.id.option_y_dk_tt2no3d)
    RadioButton option_y_dk_tt2no3d;
    @BindView(R.id.option_n_dk_tt2no3d)
    RadioButton option_n_dk_tt2no3d;
    @BindView(R.id.option_y_dk_tt3no3d)
    RadioButton option_y_dk_tt3no3d;
    @BindView(R.id.option_n_dk_tt3no3d)
    RadioButton option_n_dk_tt3no3d;
    @BindView(R.id.option_y_dk_ppno4)
    RadioButton option_y_dk_ppno4;
    @BindView(R.id.option_n_dk_ppno4)
    RadioButton option_n_dk_ppno4;
    @BindView(R.id.option_y_dk_ttno4)
    RadioButton option_y_dk_ttno4;
    @BindView(R.id.option_n_dk_ttno4)
    RadioButton option_n_dk_ttno4;
    @BindView(R.id.option_y_dk_tt1no4)
    RadioButton option_y_dk_tt1no4;
    @BindView(R.id.option_n_dk_tt1no4)
    RadioButton option_n_dk_tt1no4;
    @BindView(R.id.option_y_dk_tt2no4)
    RadioButton option_y_dk_tt2no4;
    @BindView(R.id.option_n_dk_tt2no4)
    RadioButton option_n_dk_tt2no4;
    @BindView(R.id.option_y_dk_tt3no4)
    RadioButton option_y_dk_tt3no4;
    @BindView(R.id.option_n_dk_tt3no4)
    RadioButton option_n_dk_tt3no4;
    @BindView(R.id.option_y_dk_ppno5)
    RadioButton option_y_dk_ppno5;
    @BindView(R.id.option_n_dk_ppno5)
    RadioButton option_n_dk_ppno5;
    @BindView(R.id.option_y_dk_ttno5)
    RadioButton option_y_dk_ttno5;
    @BindView(R.id.option_n_dk_ttno5)
    RadioButton option_n_dk_ttno5;
    @BindView(R.id.option_y_dk_tt1no5)
    RadioButton option_y_dk_tt1no5;
    @BindView(R.id.option_n_dk_tt1no5)
    RadioButton option_n_dk_tt1no5;
    @BindView(R.id.option_y_dk_tt2no5)
    RadioButton option_y_dk_tt2no5;
    @BindView(R.id.option_n_dk_tt2no5)
    RadioButton option_n_dk_tt2no5;
    @BindView(R.id.option_y_dk_tt3no5)
    RadioButton option_y_dk_tt3no5;
    @BindView(R.id.option_n_dk_tt3no5)
    RadioButton option_n_dk_tt3no5;
    @BindView(R.id.option_y_dk_ppno6)
    RadioButton option_y_dk_ppno6;
    @BindView(R.id.option_n_dk_ppno6)
    RadioButton option_n_dk_ppno6;
    @BindView(R.id.option_y_dk_ttno6)
    RadioButton option_y_dk_ttno6;
    @BindView(R.id.option_n_dk_ttno6)
    RadioButton option_n_dk_ttno6;
    @BindView(R.id.option_y_dk_tt1no6)
    RadioButton option_y_dk_tt1no6;
    @BindView(R.id.option_n_dk_tt1no6)
    RadioButton option_n_dk_tt1no6;
    @BindView(R.id.option_y_dk_tt2no6)
    RadioButton option_y_dk_tt2no6;
    @BindView(R.id.option_n_dk_tt2no6)
    RadioButton option_n_dk_tt2no6;
    @BindView(R.id.option_y_dk_tt3no6)
    RadioButton option_y_dk_tt3no6;
    @BindView(R.id.option_n_dk_tt3no6)
    RadioButton option_n_dk_tt3no6;
    @BindView(R.id.option_y_dk_ppno7)
    RadioButton option_y_dk_ppno7;
    @BindView(R.id.option_n_dk_ppno7)
    RadioButton option_n_dk_ppno7;
    @BindView(R.id.option_y_dk_ttno7)
    RadioButton option_y_dk_ttno7;
    @BindView(R.id.option_n_dk_ttno7)
    RadioButton option_n_dk_ttno7;
    @BindView(R.id.option_y_dk_tt1no7)
    RadioButton option_y_dk_tt1no7;
    @BindView(R.id.option_n_dk_tt1no7)
    RadioButton option_n_dk_tt1no7;
    @BindView(R.id.option_y_dk_tt2no7)
    RadioButton option_y_dk_tt2no7;
    @BindView(R.id.option_n_dk_tt2no7)
    RadioButton option_n_dk_tt2no7;
    @BindView(R.id.option_y_dk_tt3no7)
    RadioButton option_y_dk_tt3no7;
    @BindView(R.id.option_n_dk_tt3no7)
    RadioButton option_n_dk_tt3no7;
    @BindView(R.id.option_y_dk_ppno8a)
    RadioButton option_y_dk_ppno8a;
    @BindView(R.id.option_n_dk_ppno8a)
    RadioButton option_n_dk_ppno8a;
    @BindView(R.id.option_y_dk_ttno8a)
    RadioButton option_y_dk_ttno8a;
    @BindView(R.id.option_n_dk_ttno8a)
    RadioButton option_n_dk_ttno8a;
    @BindView(R.id.option_y_dk_tt1no8a)
    RadioButton option_y_dk_tt1no8a;
    @BindView(R.id.option_n_dk_tt1no8a)
    RadioButton option_n_dk_tt1no8a;
    @BindView(R.id.option_y_dk_tt2no8a)
    RadioButton option_y_dk_tt2no8a;
    @BindView(R.id.option_n_dk_tt2no8a)
    RadioButton option_n_dk_tt2no8a;
    @BindView(R.id.option_y_dk_tt3no8a)
    RadioButton option_y_dk_tt3no8a;
    @BindView(R.id.option_n_dk_tt3no8a)
    RadioButton option_n_dk_tt3no8a;
    @BindView(R.id.option_y_dk_ppno8b)
    RadioButton option_y_dk_ppno8b;
    @BindView(R.id.option_n_dk_ppno8b)
    RadioButton option_n_dk_ppno8b;
    @BindView(R.id.option_y_dk_ttno8b)
    RadioButton option_y_dk_ttno8b;
    @BindView(R.id.option_n_dk_ttno8b)
    RadioButton option_n_dk_ttno8b;
    @BindView(R.id.option_y_dk_tt1no8b)
    RadioButton option_y_dk_tt1no8b;
    @BindView(R.id.option_n_dk_tt1no8b)
    RadioButton option_n_dk_tt1no8b;
    @BindView(R.id.option_y_dk_tt2no8b)
    RadioButton option_y_dk_tt2no8b;
    @BindView(R.id.option_n_dk_tt2no8b)
    RadioButton option_n_dk_tt2no8b;
    @BindView(R.id.option_y_dk_tt3no8b)
    RadioButton option_y_dk_tt3no8b;
    @BindView(R.id.option_n_dk_tt3no8b)
    RadioButton option_n_dk_tt3no8b;
    @BindView(R.id.option_y_dk_ppno8c)
    RadioButton option_y_dk_ppno8c;
    @BindView(R.id.option_n_dk_ppno8c)
    RadioButton option_n_dk_ppno8c;
    @BindView(R.id.option_y_dk_ttno8c)
    RadioButton option_y_dk_ttno8c;
    @BindView(R.id.option_n_dk_ttno8c)
    RadioButton option_n_dk_ttno8c;
    @BindView(R.id.option_y_dk_tt1no8c)
    RadioButton option_y_dk_tt1no8c;
    @BindView(R.id.option_n_dk_tt1no8c)
    RadioButton option_n_dk_tt1no8c;
    @BindView(R.id.option_y_dk_tt2no8c)
    RadioButton option_y_dk_tt2no8c;
    @BindView(R.id.option_n_dk_tt2no8c)
    RadioButton option_n_dk_tt2no8c;
    @BindView(R.id.option_y_dk_tt3no8c)
    RadioButton option_y_dk_tt3no8c;
    @BindView(R.id.option_n_dk_tt3no8c)
    RadioButton option_n_dk_tt3no8c;
    @BindView(R.id.option_y_dk_ppno9)
    RadioButton option_y_dk_ppno9;
    @BindView(R.id.option_n_dk_ppno9)
    RadioButton option_n_dk_ppno9;
    @BindView(R.id.option_y_dk_ttno9)
    RadioButton option_y_dk_ttno9;
    @BindView(R.id.option_n_dk_ttno9)
    RadioButton option_n_dk_ttno9;
    @BindView(R.id.option_y_dk_tt1no9)
    RadioButton option_y_dk_tt1no9;
    @BindView(R.id.option_n_dk_tt1no9)
    RadioButton option_n_dk_tt1no9;
    @BindView(R.id.option_y_dk_tt2no9)
    RadioButton option_y_dk_tt2no9;
    @BindView(R.id.option_n_dk_tt2no9)
    RadioButton option_n_dk_tt2no9;
    @BindView(R.id.option_y_dk_tt3no9)
    RadioButton option_y_dk_tt3no9;
    @BindView(R.id.option_n_dk_tt3no9)
    RadioButton option_n_dk_tt3no9;


    @BindView(R.id.essay_tt_no1)
    EditText essay_tt_no1;
    @BindView(R.id.essay_tt_no2)
    EditText essay_tt_no2;
    @BindView(R.id.essay_tt_no4)
    EditText essay_tt_no4;
    @BindView(R.id.essay_tt_no6)
    EditText essay_tt_no6;
    @BindView(R.id.essay_tt_no7)
    EditText essay_tt_no7;
    @BindView(R.id.essay_tt_1_no8a)
    EditText essay_tt_1_no8a;
    @BindView(R.id.essay_tt_2_no8a)
    EditText essay_tt_2_no8a;
    @BindView(R.id.essay_tt_rkk_no10)
    EditText essay_tt_rkk_no10;
    @BindView(R.id.essay_tt_cm_no11a)
    EditText essay_tt_cm_no11a;
    @BindView(R.id.essay_tt_kg_no11a)
    EditText essay_tt_kg_no11a;
    @BindView(R.id.essay_tt_no11b)
    EditText essay_tt_no11b;
    @BindView(R.id.essay_tt_cm_no12)
    EditText essay_tt_cm_no12;
    @BindView(R.id.essay_tt_kg_no12)
    EditText essay_tt_kg_no12;
    @BindView(R.id.essay_pp_1_no13)
    EditText essay_pp_1_no13;
    @BindView(R.id.essay_pp_2_no13)
    EditText essay_pp_2_no13;
    @BindView(R.id.essay_pp_1_no14)
    EditText essay_pp_1_no14;
    @BindView(R.id.essay_pp_2_no14)
    EditText essay_pp_2_no14;
    @BindView(R.id.essay_pp_cm_no15)
    EditText essay_pp_cm_no15;
    @BindView(R.id.essay_pp_kg_no15)
    EditText essay_pp_kg_no15;
    @BindView(R.id.essay_dk_no2)
    EditText essay_dk_no2;
    @BindView(R.id.essay_dk_no4)
    EditText essay_dk_no4;
    @BindView(R.id.essay_dk_no7a)
    EditText essay_dk_no7a;
    @BindView(R.id.essay_dk_no7b)
    EditText essay_dk_no7b;
    @BindView(R.id.essay_dk_no7c)
    EditText essay_dk_no7c;
    @BindView(R.id.essay_dk_no7d)
    EditText essay_dk_no7d;
    @BindView(R.id.essay_dk_no8a)
    EditText essay_dk_no8a;

    @BindView(R.id.ll_tambah_data_qu)
    LinearLayout ll_tambah_data_qu;
    @BindView(R.id.ll_tambah_data2_qu)
    LinearLayout ll_tambah_data2_qu;
    @BindView(R.id.ll_questionnaire11a_isi_qu)
    LinearLayout ll_questionnaire11a_isi_qu;
    @BindView(R.id.ll_questionnaire12_isi_qu)
    LinearLayout ll_questionnaire12_isi_qu;
    @BindView(R.id.ll_tambah_data_calon_pemegang_polis_qu)
    LinearLayout ll_tambah_data_calon_pemegang_polis_qu;
    @BindView(R.id.ll_tambah_data_calon_tertanggung_qu)
    LinearLayout ll_tambah_data_calon_tertanggung_qu;
    @BindView(R.id.ll_tambah_data_keterangan_kesehatan_qu)
    LinearLayout ll_tambah_data_keterangan_kesehatan_qu;

    @BindView(R.id.scroll_qu)
    ScrollView scroll_qu;

    private Button btn_lanjut, btn_kembali;
    private ImageView iv_save, iv_next, iv_prev;
    private Toolbar second_toolbar;

    EspajModel me;
    ArrayList<View> ListOfView;

    ArrayList<ModelMst_Answer> list;
    private ArrayList<ModelTT_NO3UQ> List_ttno3;
    private ArrayList<ModelTT_NO5UQ> List_ttno5;
    private ArrayList<ModelDK_NO9ppUQ> List_dkno9pp;
    private ArrayList<ModelDK_NO9ttUQ> List_dkno9tt;
    private ArrayList<ModelDK_KetKesUQ> List_dkketkes;
    //All View
    ArrayList<View> ListOfRadioGrupDK;
    int IntCounter = 0;
    int count_tt3 = 0;
    int count_tt5 = 0;
    int count_dk_9pp = 0;
    int count_dk_9tt = 0;
    int count_ketkes = 0;
    int year = 0;
    int month = 0;
    int day = 0;

    int  //tertanggung
            int_y_id1, int_n_id1, int_y_id2, int_n_id2, int_y_id3, int_n_id3, int_y_id4, int_n_id4, int_y_id5, int_n_id5, int_y_id6,
            int_n_id6, int_y_id7, int_n_id7, int_y_id8, int_n_id8, int_y_id10, int_n_id10, int_y_id11, int_n_id11, int_y_id12,
            int_n_id12, int_y_id14, int_n_id14, int_y_id16, int_n_id16, int_y_id17, int_n_id17,
            int_y_id106_1, int_n_id106_1, int_y_id106_2, int_n_id106_2, int_y_id106_3, int_n_id106_3, int_y_id106_4, int_n_id106_4, int_y_id106_5, int_n_id106_5,
            int_y_id107_1, int_n_id107_1, int_y_id107_2, int_n_id107_2, int_y_id107_3, int_n_id107_3, int_y_id107_4, int_n_id107_4, int_y_id107_5, int_n_id107_5,
            int_y_id108_1, int_n_id108_1, int_y_id108_2, int_n_id108_2, int_y_id108_3, int_n_id108_3, int_y_id108_4, int_n_id108_4, int_y_id108_5, int_n_id108_5,
            int_y_id109_1, int_n_id109_1, int_y_id109_2, int_n_id109_2, int_y_id109_3, int_n_id109_3, int_y_id109_4, int_n_id109_4, int_y_id109_5, int_n_id109_5,
            int_y_id110_1, int_n_id110_1, int_y_id110_2, int_n_id110_2, int_y_id110_3, int_n_id110_3, int_y_id110_4, int_n_id110_4, int_y_id110_5, int_n_id110_5,
            int_y_id111_1, int_n_id111_1, int_y_id111_2, int_n_id111_2, int_y_id111_3, int_n_id111_3, int_y_id111_4, int_n_id111_4, int_y_id111_5, int_n_id111_5,
            int_y_id112_1, int_n_id112_1, int_y_id112_2, int_n_id112_2, int_y_id112_3, int_n_id112_3, int_y_id112_4, int_n_id112_4, int_y_id112_5, int_n_id112_5,
            int_y_id113_1, int_n_id113_1, int_y_id113_2, int_n_id113_2, int_y_id113_3, int_n_id113_3, int_y_id113_4, int_n_id113_4, int_y_id113_5, int_n_id113_5,
            int_y_id114_1, int_n_id114_1, int_y_id114_2, int_n_id114_2, int_y_id114_3, int_n_id114_3, int_y_id114_4, int_n_id114_4, int_y_id114_5, int_n_id114_5,
            int_y_id115_1, int_n_id115_1, int_y_id115_2, int_n_id115_2, int_y_id115_3, int_n_id115_3, int_y_id115_4, int_n_id115_4, int_y_id115_5, int_n_id115_5,
            int_y_id116_1, int_n_id116_1, int_y_id116_2, int_n_id116_2, int_y_id116_3, int_n_id116_3, int_y_id116_4, int_n_id116_4, int_y_id116_5, int_n_id116_5,
            int_y_id117_1, int_n_id117_1, int_y_id117_2, int_n_id117_2, int_y_id117_3, int_n_id117_3, int_y_id117_4, int_n_id117_4, int_y_id117_5, int_n_id117_5,
            int_y_id118_1, int_n_id118_1, int_y_id118_2, int_n_id118_2, int_y_id118_3, int_n_id118_3, int_y_id118_4, int_n_id118_4, int_y_id118_5, int_n_id118_5,
            int_y_id119_1, int_n_id119_1, int_y_id119_2, int_n_id119_2, int_y_id119_3, int_n_id119_3, int_y_id119_4, int_n_id119_4, int_y_id119_5, int_n_id119_5,
            int_y_id120_1, int_n_id120_1, int_y_id120_2, int_n_id120_2, int_y_id120_3, int_n_id120_3, int_y_id120_4, int_n_id120_4, int_y_id120_5, int_n_id120_5,
            int_y_id121_1, int_n_id121_1, int_y_id121_2, int_n_id121_2, int_y_id121_3, int_n_id121_3, int_y_id121_4, int_n_id121_4, int_y_id121_5, int_n_id121_5,
            int_y_id122_1, int_n_id122_1, int_y_id122_2, int_n_id122_2, int_y_id122_3, int_n_id122_3, int_y_id122_4, int_n_id122_4, int_y_id122_5, int_n_id122_5,
            int_y_id123_1, int_n_id123_1, int_y_id123_2, int_n_id123_2, int_y_id123_3, int_n_id123_3, int_y_id123_4, int_n_id123_4, int_y_id123_5, int_n_id123_5,
            int_y_id124_1, int_n_id124_1, int_y_id124_2, int_n_id124_2, int_y_id124_3, int_n_id124_3, int_y_id124_4, int_n_id124_4, int_y_id124_5, int_n_id124_5,
            int_y_id125_1, int_n_id125_1, int_y_id125_2, int_n_id125_2, int_y_id125_3, int_n_id125_3, int_y_id125_4, int_n_id125_4, int_y_id125_5, int_n_id125_5,
            int_y_id126_1, int_n_id126_1, int_y_id126_2, int_n_id126_2, int_y_id126_3, int_n_id126_3, int_y_id126_4, int_n_id126_4, int_y_id126_5, int_n_id126_5,
            int_y_id127_1, int_n_id127_1, int_y_id127_2, int_n_id127_2, int_y_id127_3, int_n_id127_3, int_y_id127_4, int_n_id127_4, int_y_id127_5, int_n_id127_5,
            int_y_id128_1, int_n_id128_1, int_y_id128_2, int_n_id128_2, int_y_id128_3, int_n_id128_3, int_y_id128_4, int_n_id128_4, int_y_id128_5, int_n_id128_5,
            int_y_id129_1, int_n_id129_1, int_y_id129_2, int_n_id129_2, int_y_id129_3, int_n_id129_3, int_y_id129_4, int_n_id129_4, int_y_id129_5, int_n_id129_5,
            int_y_id130_1, int_n_id130_1, int_y_id130_2, int_n_id130_2, int_y_id130_3, int_n_id130_3, int_y_id130_4, int_n_id130_4, int_y_id130_5, int_n_id130_5,
            int_y_id131_1, int_n_id131_1, int_y_id131_2, int_n_id131_2, int_y_id131_3, int_n_id131_3, int_y_id131_4, int_n_id131_4, int_y_id131_5, int_n_id131_5,
            int_y_id132_1, int_n_id132_1, int_y_id132_2, int_n_id132_2, int_y_id132_3, int_n_id132_3, int_y_id132_4, int_n_id132_4, int_y_id132_5, int_n_id132_5,
            int_y_id133_1, int_n_id133_1, int_y_id133_2, int_n_id133_2, int_y_id133_3, int_n_id133_3, int_y_id133_4, int_n_id133_4, int_y_id133_5, int_n_id133_5,
            int_y_id134_1, int_n_id134_1, int_y_id134_2, int_n_id134_2, int_y_id134_3, int_n_id134_3, int_y_id134_4, int_n_id134_4, int_y_id134_5, int_n_id134_5,
            int_y_id135_1, int_n_id135_1, int_y_id135_2, int_n_id135_2, int_y_id135_3, int_n_id135_3, int_y_id135_4, int_n_id135_4, int_y_id135_5, int_n_id135_5,
            int_y_id136_1, int_n_id136_1, int_y_id136_2, int_n_id136_2, int_y_id136_3, int_n_id136_3, int_y_id136_4, int_n_id136_4, int_y_id136_5, int_n_id136_5,
            int_y_id137_1, int_n_id137_1, int_y_id137_2, int_n_id137_2, int_y_id137_3, int_n_id137_3, int_y_id137_4, int_n_id137_4, int_y_id137_5, int_n_id137_5,
            int_y_id139_1, int_n_id139_1, int_y_id139_2, int_n_id139_2, int_y_id139_3, int_n_id139_3, int_y_id139_4, int_n_id139_4, int_y_id139_5, int_n_id139_5,
            int_y_id140_1, int_n_id140_1, int_y_id140_2, int_n_id140_2, int_y_id140_3, int_n_id140_3, int_y_id140_4, int_n_id140_4, int_y_id140_5, int_n_id140_5,
            int_y_id141_1, int_n_id141_1, int_y_id141_2, int_n_id141_2, int_y_id141_3, int_n_id141_3, int_y_id141_4, int_n_id141_4, int_y_id141_5, int_n_id141_5,
            int_y_id142_1, int_n_id142_1, int_y_id142_2, int_n_id142_2, int_y_id142_3, int_n_id142_3, int_y_id142_4, int_n_id142_4, int_y_id142_5, int_n_id142_5,
            int_y_id143_1, int_n_id143_1, int_y_id143_2, int_n_id143_2, int_y_id143_3, int_n_id143_3, int_y_id143_4, int_n_id143_4, int_y_id143_5, int_n_id143_5,
            int_y_id144_1, int_n_id144_1, int_y_id144_2, int_n_id144_2, int_y_id144_3, int_n_id144_3, int_y_id144_4, int_n_id144_4, int_y_id144_5, int_n_id144_5,
            int_y_id145_1, int_n_id145_1, int_y_id145_2, int_n_id145_2, int_y_id145_3, int_n_id145_3, int_y_id145_4, int_n_id145_4, int_y_id145_5, int_n_id145_5,
            int_y_id146_1, int_n_id146_1, int_y_id146_2, int_n_id146_2, int_y_id146_3, int_n_id146_3, int_y_id146_4, int_n_id146_4, int_y_id146_5, int_n_id146_5,
            int_y_id152_1, int_n_id152_1, int_y_id152_2, int_n_id152_2, int_y_id152_3, int_n_id152_3, int_y_id152_4, int_n_id152_4, int_y_id152_5, int_n_id152_5,
            int_y_id153_1, int_n_id153_1, int_y_id153_2, int_n_id153_2, int_y_id153_3, int_n_id153_3, int_y_id153_4, int_n_id153_4, int_y_id153_5, int_n_id153_5,
            int_y_id154_1, int_n_id154_1, int_y_id154_2, int_n_id154_2, int_y_id154_3, int_n_id154_3, int_y_id154_4, int_n_id154_4, int_y_id154_5, int_n_id154_5,
            int_y_id155_1, int_n_id155_1, int_y_id155_2, int_n_id155_2, int_y_id155_3, int_n_id155_3, int_y_id155_4, int_n_id155_4, int_y_id155_5, int_n_id155_5;

    private ProgressDialog progressDialog;
}
