package id.co.ajsmsig.nb.prop.adapter;

/**
 * Created by rizky_c on 23/08/2017.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import id.co.ajsmsig.nb.R;

public class P_ImageAdapter extends BaseAdapter {
    private Context mContext;


    // Keep all Images in array
    public ArrayList<HashMap<String, String>> imagePaths;

    // Constructor
    public P_ImageAdapter(Context c, ArrayList<HashMap<String, String>> imagePaths) {
        mContext = c;
        this.imagePaths = imagePaths;
        Log.d("TAG", "P_ImageAdapter: ");
    }

    @Override
    public int getCount() {
        return imagePaths.size();
    }

    @Override
    public Object getItem(int position) {
        return imagePaths.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;


        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            view = inflater.inflate(R.layout.p_manfaat_gridview_item_layout, null);
            ImageView img = view.findViewById(R.id.iv_manfaat);
            TextView tv = view.findViewById(R.id.tv_manfaat);

            HashMap<String , String> imagePath = imagePaths.get(position);

            int resID = mContext.getResources().getIdentifier(imagePath.get("path") , "drawable",  mContext.getPackageName());
            Picasso.with(mContext).load(resID).into(img);
            tv.setText(imagePath.get("name"));

        } else {
            view =  convertView;
        }

        return view;

    }
}


