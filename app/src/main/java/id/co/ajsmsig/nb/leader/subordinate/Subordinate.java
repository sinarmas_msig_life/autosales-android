
package id.co.ajsmsig.nb.leader.subordinate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subordinate {

    @SerializedName("MSAG_ID")
    @Expose
    private String mSAGID;
    @SerializedName("MSPE_EMAIL")
    @Expose
    private String mSPEEMAIL;
    @SerializedName("NAMA_REFF")
    @Expose
    private String nAMAREFF;
    @SerializedName("NM_BANK")
    @Expose
    private String nMBANK;
    @SerializedName("JENIS")
    @Expose
    private String jENIS;

    private String initialName;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Subordinate() {
    }

    /**
     * 
     * @param mSAGID
     * @param jENIS
     * @param mSPEEMAIL
     * @param nAMAREFF
     * @param nMBANK
     */
    public Subordinate(String mSAGID, String mSPEEMAIL, String nAMAREFF, String nMBANK, String jENIS, String initialName) {
        super();
        this.mSAGID = mSAGID;
        this.mSPEEMAIL = mSPEEMAIL;
        this.nAMAREFF = nAMAREFF;
        this.nMBANK = nMBANK;
        this.jENIS = jENIS;
        this.initialName = initialName;
    }

    public String getMSAGID() {
        return mSAGID;
    }

    public void setMSAGID(String mSAGID) {
        this.mSAGID = mSAGID;
    }

    public String getMSPEEMAIL() {
        return mSPEEMAIL;
    }

    public void setMSPEEMAIL(String mSPEEMAIL) {
        this.mSPEEMAIL = mSPEEMAIL;
    }

    public String getNAMAREFF() {
        return nAMAREFF;
    }

    public void setNAMAREFF(String nAMAREFF) {
        this.nAMAREFF = nAMAREFF;
    }

    public String getNMBANK() {
        return nMBANK;
    }

    public void setNMBANK(String nMBANK) {
        this.nMBANK = nMBANK;
    }

    public String getJENIS() {
        return jENIS;
    }

    public void setJENIS(String jENIS) {
        this.jENIS = jENIS;
    }

    public String getInitialName() {
        return initialName;
    }

    public void setInitialName(String initialName) {
        this.initialName = initialName;
    }

}
