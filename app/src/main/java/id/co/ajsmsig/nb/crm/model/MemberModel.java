package id.co.ajsmsig.nb.crm.model;

/**
 * Created by eriza on 05/01/2018.
 */

public class MemberModel {
    private String NOID = "";
    private String FULLNAME = "";
    private String BTEXT = "";
    private String PACKET = "";
    private String SOCIALID = "";
    private String MPHONE = "";
    private String EMAIL = "";
    private String SPONSOR = "";
    private String UPLINE = "";
    private String SEXID = "";
    private String NO_PROPOSAL_TAB = "";
    private String NO_PROPOSAL = "";
    private String SPAJ_ID_TAB = "";
    private String SPAJ_ID = "";
    private String VIRTUAL_ACC = "";
    private String OTP = "";
    private Double HITUNG = (double) 0;

    public MemberModel() {
    }

    public MemberModel(String NOID, String FULLNAME, String BTEXT, String PACKET, String SOCIALID, String MPHONE, String EMAIL, String SPONSOR, String UPLINE, String SEXID, String NO_PROPOSAL_TAB, String NO_PROPOSAL, String SPAJ_ID_TAB, String SPAJ_ID, String VIRTUAL_ACC, String OTP, Double HITUNG) {
        this.NOID = NOID;
        this.FULLNAME = FULLNAME;
        this.BTEXT = BTEXT;
        this.PACKET = PACKET;
        this.SOCIALID = SOCIALID;
        this.MPHONE = MPHONE;
        this.EMAIL = EMAIL;
        this.SPONSOR = SPONSOR;
        this.UPLINE = UPLINE;
        this.SEXID = SEXID;
        this.NO_PROPOSAL_TAB = NO_PROPOSAL_TAB;
        this.NO_PROPOSAL = NO_PROPOSAL;
        this.SPAJ_ID_TAB = SPAJ_ID_TAB;
        this.SPAJ_ID = SPAJ_ID;
        this.VIRTUAL_ACC = VIRTUAL_ACC;
        this.OTP = OTP;
        this.HITUNG = HITUNG;
    }

    public String getNOID() {
        return NOID;
    }

    public void setNOID(String NOID) {
        this.NOID = NOID;
    }

    public String getFULLNAME() {
        return FULLNAME;
    }

    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }

    public String getBTEXT() {
        return BTEXT;
    }

    public void setBTEXT(String BTEXT) {
        this.BTEXT = BTEXT;
    }

    public String getPACKET() {
        return PACKET;
    }

    public void setPACKET(String PACKET) {
        this.PACKET = PACKET;
    }

    public String getSOCIALID() {
        return SOCIALID;
    }

    public void setSOCIALID(String SOCIALID) {
        this.SOCIALID = SOCIALID;
    }

    public String getMPHONE() {
        return MPHONE;
    }

    public void setMPHONE(String MPHONE) {
        this.MPHONE = MPHONE;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getSPONSOR() {
        return SPONSOR;
    }

    public void setSPONSOR(String SPONSOR) {
        this.SPONSOR = SPONSOR;
    }

    public String getUPLINE() {
        return UPLINE;
    }

    public void setUPLINE(String UPLINE) {
        this.UPLINE = UPLINE;
    }

    public String getSEXID() {
        return SEXID;
    }

    public void setSEXID(String SEXID) {
        this.SEXID = SEXID;
    }

    public String getNO_PROPOSAL_TAB() {
        return NO_PROPOSAL_TAB;
    }

    public void setNO_PROPOSAL_TAB(String NO_PROPOSAL_TAB) {
        this.NO_PROPOSAL_TAB = NO_PROPOSAL_TAB;
    }

    public String getNO_PROPOSAL() {
        return NO_PROPOSAL;
    }

    public void setNO_PROPOSAL(String NO_PROPOSAL) {
        this.NO_PROPOSAL = NO_PROPOSAL;
    }

    public String getSPAJ_ID_TAB() {
        return SPAJ_ID_TAB;
    }

    public void setSPAJ_ID_TAB(String SPAJ_ID_TAB) {
        this.SPAJ_ID_TAB = SPAJ_ID_TAB;
    }

    public String getSPAJ_ID() {
        return SPAJ_ID;
    }

    public void setSPAJ_ID(String SPAJ_ID) {
        this.SPAJ_ID = SPAJ_ID;
    }

    public String getVIRTUAL_ACC() {
        return VIRTUAL_ACC;
    }

    public void setVIRTUAL_ACC(String VIRTUAL_ACC) {
        this.VIRTUAL_ACC = VIRTUAL_ACC;
    }

    public String getOTP() {
        return OTP;
    }

    public void setOTP(String OTP) {
        this.OTP = OTP;
    }

    public Double getHITUNG(){
        return HITUNG;
    }

    public void setHITUNG(Double HITUNG){
        this.HITUNG = HITUNG;
    }
}
