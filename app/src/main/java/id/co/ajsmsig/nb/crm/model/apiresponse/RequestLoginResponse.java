package id.co.ajsmsig.nb.crm.model.apiresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class RequestLoginResponse {
    @SerializedName("state")
    @Expose
    private boolean state;
    @SerializedName("msg")
    @Expose
    private String message;

    public boolean isState() {
        return state;
    }

    public String getMessage() {
        return message;
    }
}
