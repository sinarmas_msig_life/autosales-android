package id.co.ajsmsig.nb.espaj.method;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;

/**
 * Created by eriza on 24/08/2017.
 */

public class XMLParser {
    private static ArrayList<ModelDropdownInt> list_model;
    private static ModelDropdownInt model;
    private static ArrayList<ModelDropDownString> list_model_string;
    private static ModelDropDownString modelString;
    private static String text;

    public static ArrayList<ModelDropdownInt> XMLParse(XmlPullParser parser, int flag_dropdown) throws XmlPullParserException, IOException {
        list_model = new ArrayList<ModelDropdownInt>();
        int id = 0;
        int eventType = parser.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            String tagxml = parser.getName();
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (tagxml.equalsIgnoreCase("Position")) {
                        model = new ModelDropdownInt();
                    }
                    break;

                case XmlPullParser.TEXT:
                    text = parser.getText();
                    break;

                case XmlPullParser.END_TAG:
                    if (tagxml.equalsIgnoreCase("Position")) {
                        switch (flag_dropdown) {
                            case 0: //umum
                                list_model.add(model);
                                break;
                            case 1: // Relasi PP
                                if (id >= 1 && id < 36) {
                                    list_model.add(model);
                                }
                                break;
                            case 2: // Relasi CP
                                if (id >= 40 && id <= 48) {
                                    list_model.add(model);
                                }
                                break;
                            case 3: // Bentuk bayar premi tabungan
                                if (id == 2) {
                                    list_model.add(model);
                                }
                                break;
                            case 4: // Relasi PP khusus proposal
                                if (id >= 2 && id < 36) {
                                    list_model.add(model);
                                }
                                break;
                            case 5: // Jenis Tabungan untuk Bank
                                if (id == 1 || id == 2) {
                                    list_model.add(model);
                                }
                                break;
                            case 6: // Jenis Nasabah untuk Bank BJB
                                if (id == 2 || id == 3) {
                                    list_model.add(model);
                                }
                                break;
                            case 7: // Jenis Nasabah untuk Bank Jatim
                                if (id == 2 || id == 4) {
                                    list_model.add(model);
                                }
                                break;
                            case 8: // untuk bentuk premi
                                if (id == 0 || id == 2) {
                                    list_model.add(model);
                                }
                                break;
                            case 9: // untuk KTP SIAP2U
                                if (id == 1 || id == 9) {
                                    list_model.add(model);
                                }
                                break;
                            case 10:// Jenis Nasabah Bank BTN
                                if (id == 0) {
                                    list_model.add(model);
                                }
                                break;
                            case 11 :
                                if (id == 0 || id == 1 || id == 2) {
                                    list_model.add(model);
                                }
                                break;
                        }
                    } else if (tagxml.equalsIgnoreCase("ID")) {
                        id = Integer.parseInt(XMLParser.text);
                        XMLParser.model.setId(id);
                    } else if (tagxml.equalsIgnoreCase("VALUE")) {
                        XMLParser.model.setValue(XMLParser.text);
                    }
                    break;
            }
            eventType = parser.next();
        }
        return list_model;
    }

    public static ArrayList<ModelDropDownString> XMLParseString(XmlPullParser parser, int flag_dropdown) throws XmlPullParserException, IOException {
        list_model_string = new ArrayList<ModelDropDownString>();
        String id = "";
        int eventType = parser.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            String tagxml = parser.getName();
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (tagxml.equalsIgnoreCase("Position")) {
                        modelString = new ModelDropDownString();
                    }
                    break;

                case XmlPullParser.TEXT:
                    text = parser.getText();
                    break;

                case XmlPullParser.END_TAG:
                    if (tagxml.equalsIgnoreCase("Position")) {
                        switch (flag_dropdown) {
                            case 0: //umum
                                list_model_string.add(modelString);
                                break;
                        }
                    } else if (tagxml.equalsIgnoreCase("ID")) {
                        id = XMLParser.text;
                        XMLParser.modelString.setId(id);
                    } else if (tagxml.equalsIgnoreCase("VALUE")) {
                        XMLParser.modelString.setValue(XMLParser.text);
                    }
                    break;
            }
            eventType = parser.next();
        }
        return list_model_string;
    }

}
