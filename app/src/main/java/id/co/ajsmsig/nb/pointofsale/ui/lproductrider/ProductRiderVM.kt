package id.co.ajsmsig.nb.pointofsale.ui.lproductrider

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import id.co.ajsmsig.nb.pointofsale.binding.LoadingButtonListener
import id.co.ajsmsig.nb.pointofsale.model.Resource
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Analytics
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductRider
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.ProductDetail
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 08/03/18.
 */
class ProductRiderVM
@Inject
constructor(application: Application, val repository: PrePopulatedRepository, val mainRepository: MainRepository, val sharedViewModel: SharedViewModel): AndroidViewModel(application){

    val observableProductRiders = MediatorLiveData<Resource<List<ProductRider>>>()
    val observableProductDetails = MediatorLiveData<List<ProductDetail>>()

    var page: Page? = null
    set(value) {
        if (field == null) {
            field = value

            sharedViewModel.selectedProduct?.let {

                val product = it

                val productDetails = ArrayList<ProductDetail>()
                product.RiderSheet?.let { productDetails.add(ProductDetail("Rider Sheet", it)) }

                observableProductDetails.postValue(productDetails)

                observableProductRiders.addSource(mainRepository.getProductRidersWith(it.LsbsId, sharedViewModel.groupId), observableProductRiders::setValue)
            }
        }
    }

    val loadingButtonListener = object : LoadingButtonListener {
        override fun onClick(eventName: String?) {
            mainRepository.sendAnalytics(
                    Analytics(
                            agentCode = sharedViewModel.agentCode,
                            agentName = sharedViewModel.agentName,
                            group = sharedViewModel.groupId,
                            event = eventName,
                            timestamp = System.currentTimeMillis()
                    ))
        }

    }
}