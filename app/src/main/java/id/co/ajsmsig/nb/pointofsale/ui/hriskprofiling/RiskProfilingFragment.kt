package id.co.ajsmsig.nb.pointofsale.ui.hriskprofiling


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosFragmentRiskProfilingBinding
import id.co.ajsmsig.nb.pointofsale.ui.fragment.BaseFragment
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class RiskProfilingFragment : BaseFragment() {

    private lateinit var dataBinding: PosFragmentRiskProfilingBinding

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(RiskProfilingVM::class.java)
        subscribeUi(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_risk_profiling, container, false)

        return dataBinding.root
    }

    private fun subscribeUi(viewModel: RiskProfilingVM) {
        dataBinding.vm = viewModel
        viewModel.observableQuestions.observe(this, Observer {
            it?.forEach {
                val question = it

                val answers = question.answers
                sharedViewModel.riskProfileQuestionnaireAnswers?.forEach {
                    val savedAnswer = it
                    answers?.firstOrNull { it.id == savedAnswer.id }?.selected?.set(true)
                }

                // Make randomly selected at first
//                answers?.get(Random().nextInt(answers.size))?.selected?.set(true)
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()

        sharedViewModel.riskProfileQuestionnaireAnswers = null
    }

    override fun canNext(): Boolean {
        return isEveryQuestionAnswered()
    }

    override fun validationMessageIDRes(): Int? {
        return R.string.validation_message
    }

    private fun isEveryQuestionAnswered(): Boolean {
        var everyQuestionAnswered = true
        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(RiskProfilingVM::class.java)

        sharedViewModel.riskProfileQuestionnaireAnswers = ArrayList()
        viewModel.observableQuestions.value?.forEach {
            val selectedAnswer = it.answers?.firstOrNull { it.selected.get() }

            if (selectedAnswer == null) { everyQuestionAnswered = false; return@forEach }

            sharedViewModel.riskProfileQuestionnaireAnswers?.add(selectedAnswer)
        }
        return everyQuestionAnswered
    }

}// Required empty public constructor
