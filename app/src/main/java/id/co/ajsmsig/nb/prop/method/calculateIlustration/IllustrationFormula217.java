package id.co.ajsmsig.nb.prop.method.calculateIlustration;

import android.content.Context;
import android.util.Log;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.prop.method.util.ArrUtil;
import id.co.ajsmsig.nb.prop.method.util.CommonUtil;
import id.co.ajsmsig.nb.prop.method.util.FormatNumber;
import id.co.ajsmsig.nb.prop.method.util.ProposalStringFormatter;
import id.co.ajsmsig.nb.prop.model.hardCode.Proposal;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.IllustrationResultVO;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.S_biaya;

public class IllustrationFormula217 extends IllustrationFormula{
    IllustrationFormula217(Context context) {
        super(context);
    }

    @Override
    public HashMap<String, Object> getIllustrationResult(String no_proposal) {
        IllustrationResultVO result = new IllustrationResultVO();
        ArrayList<LinkedHashMap<String, String>> mapList = new ArrayList<>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        ArrayList<HashMap<String, Object>> topupDrawList = getSelect().selectTopupAndWithdrawList(no_proposal);
        Integer defaultTopupDrawListSize = 50;
        String premiumTotal;
        String topup;
        String draw;

        int li_ke = 0, li_bagi = 1000, li_hal = 3;
        double[] ldec_bak;
        double[] ldec_hasil_ppokok = new double[3 + 1];
        double[] ldec_hasil_fondationfee = new double[3 + 1];
        double[] ldec_hasil_pptu = new double[3 + 1];
        double[] ldec_hasil_total = new double[3 + 1];
        double[] ldec_basic_wdraw = new double[3 + 1];
        double[] ldec_topup_wdraw = new double[3 + 1];
        double[] ldec_hasil_ppokok_wdraw = new double[3 + 1];
        double[] ldec_hasil_pptu_wdraw = new double[3 + 1];

        double[] ldec_ttl_byi_prmtu_habis = new double[3 + 1];
        double[] ldec_ttl_byi_prmtu_ada = new double[3 + 1];
        double[] ldec_ttl_byi_prmtu_checking = new double[3 + 1];
        double[] ldec_ttl_byi_prmtu_checking2 = new double[3 + 1];
        double[] ldec_hasil_pptu_mula = new double[3 + 1];
        double[] ldec_hasil_ppokok2 = new double[3 + 1];
        double[] ldec_hasil_pptu_mula_checking = new double[3 + 1];
        double[] ldec_hasil_pptu2 = new double[3 + 1];

        double[] ldec_basic_premium_bd = new double[3 + 1];
        double[] ldec_topup_bd = new double[3 + 1];
        double[] ldec_basic_cash_value = new double[3 + 1];
        double[] ldec_hasil_ptu = new double[3 + 1];
        double[] ldec_fph = {Proposal.DUMMY_ZERO, 0, 0, 0.9, 0.85, 0.8};
        double[] ldec_bpp = {Proposal.DUMMY_ZERO, 0, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.5, 0, 0, 0, 0, 0.25};
        double[] ldec_surr_charge = {Proposal.DUMMY_ZERO, 0.6, 0.5, 0.4, 0.3, 0.2};
        double ldec_bawal = 100000;
        double[] ldec_man_non = new double[2 + 1];
        double ldec_bak_tu = 0.05;
        double ldec_bak_tut = 0.05;
        double ldec_akuisisi;

        double[][] ldec_hasil_invest = new double[5 + 1][3 + 1];
        double[] ldec_tarik = {Proposal.DUMMY_ZERO, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05};
        double ldec_wdraw;
        double[] ldec_premi_bulan = new double[12 + 1];
        double[] ldec_premi_ppokok_bulan = new double[12 + 1];
        double ldec_topup;
        double ldec_topup_temp;
        double ldec_bass;
        double[][] ldec_bunga = new double[5 + 1][3 + 1];
        double[] ldec_bunga_avg = new double[3 + 1];
        double ldec_fee = 0.995;

        double ldec_coi = 0, ldec_mfc, ldec_sc, ldec_cost, ldec_cost_temp = 0, ldec_ppokok, ldec_ptu, ldec_aph, ldec_ff, ldec_man_celaka, ldec_temp = 0; //, ldec_man[10+1] = {1, 1, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7}
        boolean[] lb_minus = {Proposal.DUMMY_FALSE, false, false, false};
        boolean lb_rider = false;
        String ls_sy = "", ls_temp, bonus = "";
        S_biaya lstr;
        double ldec_manfaat, ldec_premi_setahun = 0, ldec_premi_ppokok_setahun = 0, ldec_coi24 = 0;
        ldec_mfc = 27500;

        Integer umur_tt = 0, bisnis_id = 0, bisnis_no = 0, ins_per = 0, lscb_kali = 0, lscb_id = 0, lamaBayar = 0, cuti_premi = 0;
        double premi = 0, premi_komb = 0, premi_pokok = 0, up = 0;
        String kondisi_layak_jual = null, lscb_pay_mode = null, lku_id = "00", userMsagId = "";
        HashMap<String, Object> dataProposal = getSelect().selectDataProposal(no_proposal);
        if (!CommonUtil.isEmpty(dataProposal)) {
            bisnis_id = ((BigDecimal) dataProposal.get("LSBS_ID")).intValue();
            bisnis_no = ((BigDecimal) dataProposal.get("LSDBS_NUMBER")).intValue();
            umur_tt = Integer.valueOf(dataProposal.get("UMUR_TT").toString());
            premi = Double.valueOf(dataProposal.get("PREMI").toString());
            premi_komb = Double.valueOf(dataProposal.get("PREMI_KOMB").toString());
            premi_pokok = Double.valueOf(dataProposal.get("PREMI_POKOK").toString());
            up = Double.valueOf(dataProposal.get("UP").toString());
            ins_per = Integer.valueOf(dataProposal.get("THN_MASA_KONTRAK").toString());
            kondisi_layak_jual = (String) dataProposal.get("NAME_ELIGIBLE");
            lscb_kali = Integer.valueOf(dataProposal.get("LSCB_KALI").toString());
            lscb_pay_mode = dataProposal.get("LSCB_PAY_MODE").toString();
            lscb_id = Integer.valueOf(dataProposal.get("LSCB_ID").toString());
            lku_id = dataProposal.get("LKU_ID").toString();
            cuti_premi = Integer.valueOf(dataProposal.get("THN_CUTI_PREMI").toString());
            lamaBayar = Integer.valueOf(dataProposal.get("THN_LAMA_BAYAR").toString());
            userMsagId = dataProposal.get("MSAG_ID").toString();
        }

        if (Proposal.CUR_USD_CD.equals(lku_id)) {
            ldec_mfc = 2.75;
            li_bagi = 1;
        }

        for (int i = 1; i <= 12; i++) {
            ldec_premi_bulan[i] = 0;
            ldec_premi_ppokok_bulan[i] = 0;
            if (i == 1) {
                ldec_premi_bulan[i] = premi;
                ldec_premi_ppokok_bulan[i] = premi_pokok;
            }
            if (Proposal.PAY_MODE_CD_TRIWULANAN == lscb_id) {
                if (i == 4 || i == 7 || i == 10) {
                    ldec_premi_bulan[i] = premi;
                    ldec_premi_ppokok_bulan[i] = premi_pokok;
                }
            } else if (Proposal.PAY_MODE_CD_SEMESTERAN == lscb_id) {
                if (i == 7) {
                    ldec_premi_bulan[i] = premi;
                    ldec_premi_ppokok_bulan[i] = premi_pokok;
                }
            } else if (Proposal.PAY_MODE_CD_BULANAN == lscb_id) {
                ldec_premi_bulan[i] = premi;
                ldec_premi_ppokok_bulan[i] = premi_pokok;
            }
            ldec_premi_setahun += ldec_premi_bulan[i];
            ldec_premi_ppokok_setahun += ldec_premi_ppokok_bulan[i];
        }

        for (int i = 1; i <= 3; i++) {
            ldec_hasil_invest[1][i] = 0;
            ldec_hasil_ppokok[i] = 0;
            ldec_hasil_ppokok2[i] = 0;

            ldec_hasil_fondationfee[i] = 0;
            ldec_hasil_pptu[i] = 0;
            ldec_hasil_total[i] = 0;

            ldec_hasil_ptu[i] = 0;
            ldec_basic_wdraw[i] = 0;
            ldec_topup_wdraw[i] = 0;
            ldec_hasil_ppokok_wdraw[i] = 0;
            ldec_hasil_pptu_wdraw[i] = 0;

            ldec_ttl_byi_prmtu_habis[i] = 0;
            ldec_ttl_byi_prmtu_ada[i] = 0;
            ldec_ttl_byi_prmtu_checking[i] = 0;
            ldec_ttl_byi_prmtu_checking2[i] = 0;
            ldec_hasil_pptu_mula_checking[i] = 0;
            ldec_hasil_pptu2[i] = 0;
        }

        ldec_manfaat = up;
        lstr = getBiaya(bisnis_id, bisnis_no, no_proposal);
        ldec_bak = lstr.bak;
        ldec_bunga = lstr.bunga;

        ldec_bunga_avg = getSelect().selectBungaAvg(no_proposal);//TODO: Bila ada beda coba cek disini dulu

        double[] np = new double[4];
        double[] celaka = new double[4];

        int j;
        Integer flag_syarat_bonus_premi; // masih belum di pakai

        boolean cekAdaTopUp = false;
        double ldec_topup_cek = 0;
        for (int i = 1; i <= ins_per; i++) {
            if (i <= ArrUtil.upperBound(lstr.topup)) ldec_topup_cek = lstr.topup[i];
            if (ldec_topup_cek > 0) {
                cekAdaTopUp = true;
                break;
            }
        }

        for (int i = 1; i <= ins_per; i++) {
            ldec_sc = 0;
            ldec_wdraw = 0;
            ldec_topup = 0;
            ldec_coi = getLdec_coi(dataProposal, i);
            ldec_akuisisi = 0;
            ldec_topup_temp = 0;
//            ldec_coi = of_get_coi_190( i );

            if (i <= ArrUtil.upperBound(lstr.topup)) ldec_topup = lstr.topup[i];
            ldec_topup_temp = ldec_topup;
            if (ArrUtil.upperBound(ldec_bak) > i) {
                ldec_akuisisi = ldec_bak[i];
            } else {
                ldec_akuisisi = 0;
            }
            if (i <= 7) ldec_sc = ldec_tarik[i];

            for (int k = 1; k <= 3; k++) {
                if (i <= ArrUtil.upperBound(lstr.tarik)) ldec_wdraw = lstr.tarik[i];
                for (int li_bulan = 1; li_bulan <= 12; li_bulan++) {

                    ldec_cost = (ldec_coi + ldec_mfc);
                    if (li_bulan == 12) {
                        ldec_cost_temp = (ldec_coi + ldec_mfc);
                    }
                    ldec_ppokok = 0;
                    ldec_ptu = 0;
                    ldec_ff = 0;
                    ldec_aph = 0;
                    if (i <= cuti_premi) {
                        ldec_ppokok = ldec_premi_bulan[li_bulan] * (premi_komb / 100) * (1 - ldec_akuisisi);
                        ldec_ptu = (ldec_premi_bulan[li_bulan] * (100 - premi_komb) / 100);
                    }
                    if (li_bulan == 1) {
                        ldec_topup = ldec_topup_temp;
                    } else {
                        ldec_topup = 0;
                        ldec_wdraw = 0;
                    }

                    //Jika kombinasi PP tidak 100% maka pake rumus yang udah di install di production !!
                    if (premi_komb != 100 || (premi_komb == 100 && cekAdaTopUp)) {

                        if(li_bulan == 1) {
                            ldec_basic_wdraw[k] = ldec_wdraw*( 1 + ldec_sc);
                            ldec_topup_wdraw[k] = 0;
                        }else{
                            ldec_basic_wdraw[k] = 0;
                            ldec_topup_wdraw[k] = 0;
                        }

                        ldec_hasil_ppokok[k] = (ldec_ppokok * (Math.pow((1 + ldec_bunga_avg[k]), ((double) 1 / 12)))) + ((ldec_hasil_ppokok[k] - ldec_basic_wdraw[k]) * (Math.pow((1 + ldec_bunga_avg[k]), ((double) 1 / 12))));

                        if (i <= 5) {
                            ldec_hasil_fondationfee[k] = ldec_hasil_ppokok[k] * (0.06 / 12);
                        } else {
                            ldec_hasil_fondationfee[k] = 0;
                        }

                        ldec_hasil_pptu[k] = (ldec_hasil_pptu[k] * (Math.pow((1 + ldec_bunga_avg[k]), ((double) 1 / 12)))) + ((ldec_ptu + ldec_topup) * (1 - ldec_bak_tut) * (Math.pow((1 + ldec_bunga_avg[k]), ((double) 1 / 12)))) - (ldec_cost + ldec_hasil_fondationfee[k]);

                        ldec_hasil_total[k] = ldec_hasil_ppokok[k] + ldec_hasil_pptu[k];


                    } else if (premi_komb == 100 && !cekAdaTopUp) {
                        //Jika kombinasi PP 100% pkae rumus yang kemarin kita buat !!
                        if (i == 1 && li_bulan == 1) {
                            ldec_hasil_ppokok[k] = ldec_ppokok * (Math.pow((1 + ldec_bunga_avg[k]), ((double) 1 / 12)));
                            ldec_cost_temp = (ldec_coi + ldec_mfc);
                        } else {
                            ldec_hasil_ppokok[k] = (ldec_hasil_ppokok2[k] * (Math.pow((1 + ldec_bunga_avg[k]), ((double) 1 / 12)))) + (ldec_ppokok * (Math.pow((1 + ldec_bunga_avg[k]), ((double) 1 / 12))));
                        }

                        if (i <= 5) {
                            ldec_hasil_fondationfee[k] = ldec_hasil_ppokok[k] * (0.06 / 12);
                        } else {
                            ldec_hasil_fondationfee[k] = 0;
                        }

                        ldec_ttl_byi_prmtu_ada[k] = ldec_cost + ldec_hasil_fondationfee[k];


                        //Draw Basic-TopUp
                        if (li_bulan == 1) {
                            ldec_basic_wdraw[k] = ldec_wdraw*( 1 + ldec_sc );
                            ldec_basic_wdraw[k] = 0;
                        } else {
                            ldec_basic_wdraw[k] = 0;
                            ldec_topup_wdraw[k] = 0;
                        }
                        // ldec_hasil_ppokok_wdraw[k] = (ldec_hasil_ppokok[k] - ldec_basic_wdraw[k]);
                        // ldec_hasil_pptu_wdraw[k]     = (ldec_ttl_byi_prmtu_checking[k] - ldec_topup_wdraw[k]);

                        //TopUp Reguler + Sekaligus
                        if (ldec_topup != 0) {
                            if (i == 1 && li_bulan == 1) {
                                ldec_hasil_pptu[k] = (ldec_ptu + ldec_topup) * (1 - ldec_bak_tut) * (Math.pow((1 + ldec_bunga_avg[k]), ((double) 1 / 12)));
                            } else {
                                ldec_hasil_pptu[k] = (ldec_ttl_byi_prmtu_checking2[k] * (Math.pow((1 + ldec_bunga_avg[k]), ((double) 1 / 12)))) + ((ldec_ptu + ldec_topup) * (1 - ldec_bak_tut) * (Math.pow((1 + ldec_bunga_avg[k]), ((double) 1 / 12))));
                                //  ldec_hasil_pptu[k] =  (ldec_ttl_byi_prmtu_checking[k]* (Math.pow((1 + ldec_bunga_avg[k]),( ( double ) 1/12)))) + ((ldec_ptu+ldec_topup)*( 1 - ldec_bak_tut)* (Math.pow((1 + ldec_bunga_avg[k]),( ( double ) 1/12))));
                            }
                        } else {
                            ldec_hasil_pptu[k] = 0;
                        }


                        //tambah variabel baru=> LDEC_HASIL_PPTU2
                        ldec_hasil_pptu2[k] = ldec_hasil_pptu[k] - ldec_ttl_byi_prmtu_ada[k];

                        if (ldec_hasil_pptu2[k] < 0) {
                            ldec_ttl_byi_prmtu_checking[k] = 0;
                        } else {
                            ldec_ttl_byi_prmtu_checking[k] = ldec_hasil_pptu2[k] - ldec_topup_wdraw[k];
                        }

                        //hapus variabel baru=> ldec_ttl_byi_prmtu_checking2[k]
                        if (ldec_ttl_byi_prmtu_checking[k] < 0) {
                            ldec_ttl_byi_prmtu_checking2[k] = 0;
                        } else {
                            ldec_ttl_byi_prmtu_checking2[k] = ldec_ttl_byi_prmtu_checking[k];
                        }


                        if (ldec_ttl_byi_prmtu_checking[k] <= 0) {
                            ldec_ttl_byi_prmtu_habis[k] = ldec_ttl_byi_prmtu_ada[k] - ldec_hasil_pptu[k];
                        } else {
                            ldec_ttl_byi_prmtu_habis[k] = 0;
                        }

                        //tambah variabel baru=> LDEC_HASIL_PPOKOK2

                        ldec_hasil_ppokok2[k] = ldec_hasil_ppokok[k] - ldec_ttl_byi_prmtu_habis[k] - ldec_basic_wdraw[k];

                        ldec_hasil_total[k] = (ldec_hasil_ppokok2[k] + ldec_ttl_byi_prmtu_checking[k]);

                    }

                }


                ldec_hasil_invest[1][k] = ldec_hasil_total[k];
                if (ldec_hasil_invest[1][k] <= 0 && (i <= 10)) {
                    lb_minus[k] = true;
                }
            }


            j = umur_tt + i;
            if (i <= 23 || j == 55 || j == 65 || j == 75 || j == 80 || j == 100) {
                for (int k = 1; k <= 3; k++) {
                    np[k] = FormatNumber.round(ldec_hasil_invest[1][k] / li_bagi, 0);
                    celaka[k] = FormatNumber.round((ldec_hasil_invest[1][k] + ldec_manfaat) / li_bagi, 0);
                }

                if (i <= cuti_premi) {
                    premiumTotal = ProposalStringFormatter.convertToStringWithoutCent(ldec_premi_setahun / li_bagi);
                } else {
                    premiumTotal = "";
                }

                if (i < defaultTopupDrawListSize) {
                    topup = "0";
                    draw = "0";

                    for (HashMap<String, Object> topupDraw : topupDrawList) {
                        Integer thn_ke = ((BigDecimal) topupDraw.get("THN_KE")).intValue();
                        if (i == thn_ke) {
                            BigDecimal topupAmount = (BigDecimal) topupDraw.get("TOPUP");
                            BigDecimal drawAmount = (BigDecimal) topupDraw.get("TARIK");

                            topup = StaticMethods.toMoney(topupAmount.divide(BigDecimal.valueOf(1000), 2, BigDecimal.ROUND_HALF_UP));
                            draw = StaticMethods.toMoney(drawAmount.divide(BigDecimal.valueOf(1000), 2, BigDecimal.ROUND_HALF_UP));
                        }
                    }

//                    if ("0".equals(topup)) topup = "0.00";
//                    if ("0".equals(draw)) draw = "0.00";
                } else {
//                    topup = "0.00";
//                    draw = "0.00";
                    topup = "0";
                    draw = "0";
                }

                if (i <= 15) {
                    //  double bonusValue = ( ldec_premi_ppokok_setahun * ldec_bpp[i]) / li_bagi;
                    //  bonus = editorUtil.convertToStringWithoutCentAndNillIfNegative( new BigDecimal(bonusValue) );
                } else {
                    bonus = "0";
                }
                if ("0".equals(bonus)) bonus = null;
                String valueLow = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative(new BigDecimal(np[1]));
                String valueMid = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative(new BigDecimal(np[2]));
                String valueHi = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative(new BigDecimal(np[3]));

                String benefitLow = ProposalStringFormatter.convertToStringWithoutCentAndSetNill(celaka[1], np[1]);
                String benefitMid = ProposalStringFormatter.convertToStringWithoutCentAndSetNill(celaka[2], np[2]);
                String benefitHi = ProposalStringFormatter.convertToStringWithoutCentAndSetNill(celaka[3], np[3]);

                LinkedHashMap<String, String> map1 = new LinkedHashMap<>();
                map1.put(getContext().getString(R.string.yearNo), ProposalStringFormatter.convertToString(i));
                map1.put(getContext().getString(R.string.insuredAge), ProposalStringFormatter.convertToString(umur_tt + i));
                map1.put(getContext().getString(R.string.premiumTotal), premiumTotal);
                map1.put(getContext().getString(R.string.topupAssumption), topup);
                map1.put(getContext().getString(R.string.drawAssumption), draw);
                map1.put(getContext().getString(R.string.valueLow), valueLow);
                map1.put(getContext().getString(R.string.valueMid), valueMid);
                map1.put(getContext().getString(R.string.valueHi), valueHi);
                map1.put(getContext().getString(R.string.benefitLow), benefitLow);
                map1.put(getContext().getString(R.string.benefitMid), benefitMid);
                map1.put(getContext().getString(R.string.benefitHi), benefitHi);
                Log.d(TAG, "map1: " + map1);
                mapList.add(map1);

            }
        }

        result.setValidityMsg(lb_minus[1] ? "Maaf, Proposal yang Anda buat Tidak Layak Jual." : "");
        result.setIllustrationList(mapList);
        map.put("Illustration1", result);
        return map;
    }

}
