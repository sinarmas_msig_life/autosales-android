package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */
@Entity
class Input(
        @PrimaryKey
        val id: Int,
        var type: String?,
        var pageId: Int?,
        var description: String?,
        var imageName: String?,
        var range: Int?,
        var min: Long?,
        var max: Long?,
        var text: String?,
        var showOnlyIfSelectPageOptionIds: String?,
        var formulaRepresentation: String?,
        var defaultValue: Int?
)