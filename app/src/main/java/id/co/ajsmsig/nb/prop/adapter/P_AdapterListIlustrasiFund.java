package id.co.ajsmsig.nb.prop.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import id.co.ajsmsig.nb.R;

import static id.co.ajsmsig.nb.crm.method.StaticMethods.toMoney;


public class P_AdapterListIlustrasiFund extends ArrayAdapter<HashMap<String, Object>> {
    private ArrayList<HashMap<String, Object>> arrayListInvest;
    private LayoutInflater layoutInflater;
    private int Resource;

//    private ViewHolder holder;

    public P_AdapterListIlustrasiFund(Context context, int resource, ArrayList<HashMap<String, Object>> objects) {
        super(context, resource, objects);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = resource;
        arrayListInvest = objects;

    }


    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            v = layoutInflater.inflate(Resource, null);
            holder.nama_investasi = v.findViewById(R.id.nama_investasi);
            holder.asumsi_rendah = v.findViewById(R.id.asumsi_rendah);
            holder.asumsi_sedang = v.findViewById(R.id.asumsi_sedang);
            holder.asumsi_tinggi = v.findViewById(R.id.asumsi_tinggi);
            holder.alokasi_investasi = v.findViewById(R.id.alokasi_investasi);
            holder.alokasi_investasi_rendah = v.findViewById(R.id.alokasi_investasi_rendah);
            holder.alokasi_investasi_sedang = v.findViewById(R.id.alokasi_investasi_sedang);
            holder.alokasi_investasi_tinggi = v.findViewById(R.id.alokasi_investasi_tinggi);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        calculateInvest(holder, position);

        return v;

    }

    private static class ViewHolder {
        TextView nama_investasi;
        TextView asumsi_rendah;
        TextView asumsi_sedang;
        TextView asumsi_tinggi;
        TextView alokasi_investasi;
        TextView alokasi_investasi_rendah;
        TextView alokasi_investasi_sedang;
        TextView alokasi_investasi_tinggi;

    }

    private void calculateInvest(ViewHolder holder, int position) {
        String namaInvest = (String) arrayListInvest.get(position).get(getContext().getString(R.string.LJI_INVEST));
        String asumsiRendah = toMoney((Double) arrayListInvest.get(position).get(getContext().getString(R.string.RATE_1))) + "%";
        String asumsiSedang = toMoney((Double) arrayListInvest.get(position).get(getContext().getString(R.string.RATE_2))) + "%";
        String asumsiTinggi = toMoney((Double) arrayListInvest.get(position).get(getContext().getString(R.string.RATE_3))) + "%";
        String alokasiInvestasi = toMoney((Double) arrayListInvest.get(position).get(getContext().getString(R.string.MDU_PERSEN))) + "%";
        String alokasiRendah = toMoney((Double) arrayListInvest.get(position).get(getContext().getString(R.string.ALLOCATION_1))) + "%";
        String alokasiSedang = toMoney((Double) arrayListInvest.get(position).get(getContext().getString(R.string.ALLOCATION_2))) + "%";
        String alokasiTinggi = toMoney((Double) arrayListInvest.get(position).get(getContext().getString(R.string.ALLOCATION_3))) + "%";

        holder.nama_investasi.setText(namaInvest);
        holder.asumsi_rendah.setText(asumsiRendah);
        holder.asumsi_sedang.setText(asumsiSedang);
        holder.asumsi_tinggi.setText(asumsiTinggi);
        holder.alokasi_investasi.setText(alokasiInvestasi);
        holder.alokasi_investasi_rendah.setText(alokasiRendah);
        holder.alokasi_investasi_sedang.setText(alokasiSedang);
        holder.alokasi_investasi_tinggi.setText(alokasiTinggi);
    }
}
