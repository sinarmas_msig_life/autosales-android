package id.co.ajsmsig.nb.espaj.model.arraylist;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 30/08/2017.
 */

public class ModelDataDitunjukDI implements Parcelable{

    private int counter = 0;
    private String nama="";
    private Double manfaat=(double)0;
    private int jekel= 0;
    private String ttl="";
    private int hub_dgcalon_tt=0;
    private int warganegara=0;
    private Boolean validation = true;

    public ModelDataDitunjukDI() {
    }

    public ModelDataDitunjukDI(int counter, String nama, Double manfaat, int jekel, String ttl, int hub_dgcalon_tt, int warganegara, Boolean validation) {
        this.counter = counter;
        this.nama = nama;
        this.manfaat = manfaat;
        this.jekel = jekel;
        this.ttl = ttl;
        this.hub_dgcalon_tt = hub_dgcalon_tt;
        this.warganegara = warganegara;
        this.validation = validation;
    }

    protected ModelDataDitunjukDI(Parcel in) {
        counter = in.readInt();
        nama = in.readString();
        jekel = in.readInt();
        ttl = in.readString();
        hub_dgcalon_tt = in.readInt();
        warganegara = in.readInt();
        manfaat = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(counter);
        dest.writeString(nama);
        dest.writeInt(jekel);
        dest.writeString(ttl);
        dest.writeInt(hub_dgcalon_tt);
        dest.writeInt(warganegara);
        dest.writeDouble(manfaat);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelDataDitunjukDI> CREATOR = new Creator<ModelDataDitunjukDI>() {
        @Override
        public ModelDataDitunjukDI createFromParcel(Parcel in) {
            return new ModelDataDitunjukDI(in);
        }

        @Override
        public ModelDataDitunjukDI[] newArray(int size) {
            return new ModelDataDitunjukDI[size];
        }
    };

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Double getManfaat() {
        return manfaat;
    }

    public void setManfaat(Double manfaat) {
        this.manfaat = manfaat;
    }

    public int getJekel() {
        return jekel;
    }

    public void setJekel(int jekel) {
        this.jekel = jekel;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public int getHub_dgcalon_tt() {
        return hub_dgcalon_tt;
    }

    public void setHub_dgcalon_tt(int hub_dgcalon_tt) {
        this.hub_dgcalon_tt = hub_dgcalon_tt;
    }

    public int getWarganegara() {
        return warganegara;
    }

    public void setWarganegara(int warganegara) {
        this.warganegara = warganegara;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public static Creator<ModelDataDitunjukDI> getCREATOR() {
        return CREATOR;
    }
}
