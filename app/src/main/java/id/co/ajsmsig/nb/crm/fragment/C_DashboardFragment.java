package id.co.ajsmsig.nb.crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.activity.C_LeadActivity;
import id.co.ajsmsig.nb.crm.activity.C_ProfileAgentActivity;
import id.co.ajsmsig.nb.crm.adapter.DashboardActivityListAdapter;
import id.co.ajsmsig.nb.crm.database.C_Insert;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.method.BackUpDatabase;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.DashboardActivityListModel;
import id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.DashboardResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.Data;
import id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.bancass.Closing;
import id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.bancass.Meetandpre;
import id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.leader.Leader;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.LeadActivityList;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.DateUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by faiz_f on 03/02/2017.
 */

public class C_DashboardFragment extends Fragment implements View.OnClickListener, DashboardActivityListAdapter.ClickListener, LoaderManager.LoaderCallbacks<Cursor>, Callback<List<LeadActivityList>> {
    private final static String TAG = C_DashboardFragment.class.getSimpleName();
    private String kodeAgen;
    private DashboardActivityListAdapter mAdapter;
    private List<DashboardActivityListModel> activities;
    private final int LOADER_DISPLAY_DASHBOARD_ACTIVITY = 200;
    private Cursor cursor;
    private C_Select select;
    private E_Select eSelect;
    private final int HEADER_TYPE = 0;
    private final int CONTENT_TYPE = 1;
    private final int SPAJ_MONTHLY_TARGET = 8;
    private final int ACTIVITY_MONTHLY_TARGET = 50;
    private C_Insert insert;


    public C_DashboardFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.c_fragment_dashboard, container, false);
        ButterKnife.bind(this, view);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), Context.MODE_PRIVATE);
        kodeAgen = sharedPreferences.getString("KODE_AGEN", "");

        initRecyclerView();

        select = new C_Select(getActivity());
        insert = new C_Insert(getActivity());

        tvViewAllActivity.setOnClickListener(this);

        view.findViewById(R.id.tvRatio).setOnClickListener(this);

        if (StaticMethods.isNetworkAvailable(getActivity())) {
            if (!TextUtils.isEmpty(kodeAgen)) {

                //Fetch activity from server, and insert it to the database
                fetchActivityFromServer(kodeAgen, false, "SLA_CRTD_DATE", "desc");
                getActivity().getSupportLoaderManager().initLoader(LOADER_DISPLAY_DASHBOARD_ACTIVITY, null, this);
            }

        } else {
            hideLoadingIndicator();
            showRecyclerView();
            getActivity().getSupportLoaderManager().initLoader(LOADER_DISPLAY_DASHBOARD_ACTIVITY, null, this);
        }

        downloadDashboardInfo(kodeAgen);
        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_dashboard, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_refresh_dashboard:
                downloadDashboardInfo(kodeAgen);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Fetch list of BC activity
     *
     * @param msagId        agent code
     * @param sortBy        SLA_CRTD_DATEx
     * @param sortDirection asc / desc
     */
    private void fetchActivityFromServer(String msagId, boolean useLimit, String sortBy, String sortDirection) {

        String afterDate = new C_Select(getActivity()).getLatestActivityInputDate(kodeAgen);
        afterDate = DateUtils.addASecondFrom(afterDate);
        Log.v(TAG, "Downloading activity after " + afterDate);
        if (TextUtils.isEmpty(afterDate)) {
            //There are no lead saved on the database. Fetch all lead from server
            downloadActivity(msagId, useLimit, sortBy, sortDirection, null);
        } else {
            //There are already some lead on the database, fetch only new lead after the specified date
            downloadActivity(msagId, useLimit, sortBy, sortDirection, afterDate);
        }

    }


    private void downloadActivity(String msagId, boolean useLimit, String sortBy, String sortDirection, String afterDate) {
        hideRecyclerView();
        showLoadingIndicator();

        String url = AppConfig.getBaseUrlCRM().concat("v2/toll/module_activity2");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<List<LeadActivityList>> call = api.fetchActivityForDashboard(url, msagId, useLimit, sortBy, sortDirection, afterDate);
        call.enqueue(this);
    }


    private void downloadDashboardInfo(String msagId) {

        if (!StaticMethods.isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Tidak dapat menampilkan data pencapaian terbaru Anda karena tidak ada koneksi Internet. Silahkan periksa koneksi internet Anda dan coba kembali", Toast.LENGTH_LONG).show();
            return;
        }

        if (!TextUtils.isEmpty(msagId)) {

            String url = AppConfig.getBaseUrlCRM().concat("v2/index.php/toll/getagentactivity");
            ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
            Call<DashboardResponse> call = api.getDashboardInfo(url, msagId);
            call.enqueue(new Callback<DashboardResponse>() {
                @Override
                public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                    if (response.body() != null) {
                        DashboardResponse body = response.body();
                        fetchActivityDataFromServer(body);
                    }
                }

                @Override
                public void onFailure(Call<DashboardResponse> call, Throwable t) {
                    Toast.makeText(getActivity(), "Terjadi kesalahan dalam mengunduh data aktivitas terbaru " + t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });

        }
    }

    private void fetchActivityDataFromServer(DashboardResponse response) {
        if (!response.isError() && response.getData() != null) {
            Data data = response.getData();

            if (data.getBancass() != null) {
                displayMeetAndPreActivityCount(data.getBancass().getMeetandpre());
                displayClosingActivityCount(data.getBancass().getClosing());
            }

            if (data.getLeader() != null) {
                displayMeetAndPreCountWithLeaderView(data.getLeader());
            }

            if (data.getBancass() != null && data.getLeader() != null) {
                displayRatio(data.getBancass().getMeetandpre(), data.getBancass().getClosing());
            }

        }
    }

    /**
     * Init recyclerview to display activity list
     */
    private void initRecyclerView() {
        activities = new ArrayList<>();
        mAdapter = new DashboardActivityListAdapter(activities);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        downloadDashboardInfo(kodeAgen);

        //Refresh activity list
        getActivity().getSupportLoaderManager().restartLoader(LOADER_DISPLAY_DASHBOARD_ACTIVITY, null, this);
    }

    private void displayRatio(Meetandpre meetandpre, Closing closing) {
        if (meetandpre != null && closing != null) {

            String ratioPercentage;
            float closingActivityCount = closing.getCurrentMonth() == null ? 0 : closing.getCurrentMonth();
            float kunjunganPresentasiActivityCount = meetandpre.getCurrentMonth() == null ? 0 : meetandpre.getCurrentMonth();

            float ratio = ((closingActivityCount / kunjunganPresentasiActivityCount) * 100);
            String text = (int) closingActivityCount + " dari " + (int) kunjunganPresentasiActivityCount;

            //Handle divided by zero that causing NaN
            if (kunjunganPresentasiActivityCount != 0.0f) {
                ratioPercentage = String.format("%.2f", ratio) + "%";
            } else {
                ratioPercentage = "0%";
            }

            tvRatioValue.setText(text);
            progressBar.setProgress((int) ratio);
            tvRatioPercentage.setText(ratioPercentage);
        }

    }

    private void displayMeetAndPreActivityCount(Meetandpre meetandpre) {
        if (meetandpre != null) {

            Integer lastMonthActivity = meetandpre.getLastMonth() == null ? 0 : meetandpre.getLastMonth();
            Integer currentMonthActivity = meetandpre.getCurrentMonth() == null ? 0 : meetandpre.getCurrentMonth();
            Integer todayActivity = meetandpre.getToday() == null ? 0 : meetandpre.getToday();

            tvLastMonthActivity.setText(displayCount(lastMonthActivity, ACTIVITY_MONTHLY_TARGET));
            tvCurrentMonthActivity.setText(displayCount(currentMonthActivity, ACTIVITY_MONTHLY_TARGET));
            tvTodayActivity.setText(String.valueOf(todayActivity));

        }

    }

    private void displayClosingActivityCount(Closing closing) {
        if (closing != null) {

            Integer lastMonthClosing = closing.getLastMonth() == null ? 0 : closing.getLastMonth();
            Integer currentMonthClosing = closing.getCurrentMonth() == null ? 0 : closing.getCurrentMonth();
            Integer todayClosing = closing.getToday() == null ? 0 : closing.getToday();

            tvLastMonthClosing.setText(displayCount(lastMonthClosing, SPAJ_MONTHLY_TARGET));
            tvCurrentMonthClosing.setText(displayCount(currentMonthClosing, SPAJ_MONTHLY_TARGET));
            tvTodayClosing.setText(String.valueOf(todayClosing));
        }
    }

    private void displayMeetAndPreCountWithLeaderView(Leader leader) {
        if (leader != null) {

            Integer lastMonthActivityAsLeader = leader.getMeetandpre().getLastMonth() == null ? 0 : leader.getMeetandpre().getLastMonth();
            Integer currentMonthActivityAsLeader = leader.getMeetandpre().getCurrentMonth() == null ? 0 : leader.getMeetandpre().getCurrentMonth();

            tvLeaderLastMonthClosing.setText(String.valueOf(lastMonthActivityAsLeader));
            tvLeaderCurrentMonthClosing.setText(String.valueOf(currentMonthActivityAsLeader));
        }
    }

    private String displayCount(int current, int target) {
        String count = current + " / " + target;
        return count;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvRatio:
                new BackUpDatabase(getContext()).backUpDatabase();
                break;
            case R.id.tvViewAllActivity:
                openAktivitasProfile(-1);
                break;
            default:
                throw new IllegalArgumentException("salah id");
        }

    }

    private void openAktivitasProfile(long idAkt) {
        Intent intent = new Intent(getActivity(), C_ProfileAgentActivity.class);
        Bundle bundle = new Bundle();
        bundle.putLong(getString(R.string.SLA_TAB_ID), idAkt);
        bundle.putInt("location", 1);
        intent.putExtras(bundle);
        startActivity(intent);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        CursorLoader cursorLoader;

        switch (id) {
            case LOADER_DISPLAY_DASHBOARD_ACTIVITY:
                cursor = new C_Select(getActivity()).getActivityListForDashboard(kodeAgen, 5);
                break;
        }


        cursorLoader = new CursorLoader(getContext()) {
            @Override
            public Cursor loadInBackground() {
                return cursor;
            }
        };

        return cursorLoader;

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case LOADER_DISPLAY_DASHBOARD_ACTIVITY:

                if (cursor.getCount() > 0) {

                    clearPreviousResult();
                    layoutNoActivityFound.setVisibility(View.GONE);

                    List<DashboardActivityListModel> activities = new ArrayList<>();
                    C_Select select = new C_Select(getActivity());

                    while (cursor.moveToNext()) {
                        long slId = cursor.getLong(cursor.getColumnIndexOrThrow("SL_ID_"));
                        long slaInstTabId = cursor.getLong(cursor.getColumnIndexOrThrow("SLA_INST_TAB_ID"));
                        String slName = cursor.getString(cursor.getColumnIndexOrThrow("SL_NAME"));
                        String activityType = cursor.getString(cursor.getColumnIndexOrThrow("SLA_TYPE"));
                        String subActivityType = cursor.getString(cursor.getColumnIndexOrThrow("SLA_SUBTYPE"));
                        String activityDate = cursor.getString(cursor.getColumnIndexOrThrow("SLA_CRTD_DATE"));
                        int slaLastPost = cursor.getInt(cursor.getColumnIndexOrThrow("SLA_LAST_POS"));
                        boolean isSynced = slaLastPost == 0; //Synced = 0

                        String activityName = select.getSelectedActivityFromSlaType(activityType);
                        String subActivityName = select.getSelectedSubActivityFromSlaSubType(subActivityType);
                        String activityCreatedDate = DateUtils.getDateFrom(activityDate);

                        DashboardActivityListModel activity = new DashboardActivityListModel(slId, slaInstTabId, slName, activityName, subActivityName, activityCreatedDate, CONTENT_TYPE, null,isSynced);

                        activities.add(activity);
                    }

                    //Add and display the activities
                    mAdapter.addAll(activities);

                } else {
                    layoutNoActivityFound.setVisibility(View.VISIBLE);
                }

                break;
        }

    }


    private void showRecyclerView() {
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void hideRecyclerView() {
        recyclerView.setVisibility(View.INVISIBLE);
    }

    private void showLoadingIndicator() {
        layoutLoadingActivityList.setVisibility(View.VISIBLE);
    }

    private void hideLoadingIndicator() {
        layoutLoadingActivityList.setVisibility(View.GONE);
    }


    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }

    private void clearPreviousResult() {
        if (activities != null && activities.size() > 0) {
            activities.clear();
        }
    }

    @Override
    public void onActivitySelected(int position, DashboardActivityListModel model) {

        //Ketika aktivitas ditambahkan melalui download dari web service, SL_ID sudah ada. Convert SL_ID menjadi SL_TAB_ID
        long slId = model.getSlId();
        long slTabId = select.getSlTabIdFromSlId(slId);

        //Ketika aktivitas ditambahkan, SL ID belum ada. Karena SL_ID baru didapatkan setelah aktivitas tersebut berhasil di upload
        if (slId == 0) {
            long slaInstTabId = model.getSlaInstTabId();
            slTabId = slaInstTabId;
        }

        //Gunakan sl tab id sebagai params untuk intent
        Intent intent = new Intent(getActivity(), C_LeadActivity.class);
        Bundle bundle = new Bundle();
        bundle.putLong(getString(R.string.SL_TAB_ID), slTabId);
        bundle.putLong(Const.INTENT_KEY_SL_ID, model.getSlId());
        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    public void onResponse(Call<List<LeadActivityList>> call, Response<List<LeadActivityList>> response) {
        showRecyclerView();
        hideLoadingIndicator();

        if (response.body() != null) {
            List<LeadActivityList> activities = response.body();

            //Refresh  with the latest data
            if (activities != null && activities.size() > 0) {

                Log.v(TAG, "Downloading activity. New data found. Count : " + activities.size());

                for (LeadActivityList activity : activities) {
                    insert.insertActivityToDatabase(activity);
                }

                if (getActivity().getSupportLoaderManager() != null) {
                    //Display the data
                    getActivity().getSupportLoaderManager().restartLoader(LOADER_DISPLAY_DASHBOARD_ACTIVITY, null, this);
                }


            }

        } else if (response.body() == null) {
            //If there is no activity from the selected lead, the server return "null"
            Log.v(TAG, "Downloading activity. No new activity found ");
        }
    }

    @Override
    public void onFailure(Call<List<LeadActivityList>> call, Throwable t) {
        hideRecyclerView();
        hideLoadingIndicator();
        Toast.makeText(getActivity(), "Tidak dapat mengunduh aktivitas terbaru. " + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }







    @BindView(R.id.tvLastMonthActivity)
    TextView tvLastMonthActivity;
    @BindView(R.id.tvCurrentMonthActivity)
    TextView tvCurrentMonthActivity;
    @BindView(R.id.tvTodayActivity)
    TextView tvTodayActivity;

    @BindView(R.id.tvLastMonthClosing)
    TextView tvLastMonthClosing;
    @BindView(R.id.tvCurrentMonthClosing)
    TextView tvCurrentMonthClosing;
    @BindView(R.id.tvTodayClosing)
    TextView tvTodayClosing;

    @BindView(R.id.tvLeaderLastMonthClosing)
    TextView tvLeaderLastMonthClosing;
    @BindView(R.id.tvLeaderCurrentMonthClosing)
    TextView tvLeaderCurrentMonthClosing;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tvRatioValue)
    TextView tvRatioValue;

    @BindView(R.id.tvRatioPercentage)
    TextView tvRatioPercentage;

    @BindView(R.id.tvViewAllActivity)
    TextView tvViewAllActivity;


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.layoutLoadingActivityList)
    LinearLayout layoutLoadingActivityList;

    @BindView(R.id.layoutNoActivityFound)
    LinearLayout layoutNoActivityFound;

}
