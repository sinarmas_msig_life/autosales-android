
package id.co.ajsmsig.nb.crm.model.apiresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListProvider {

    @SerializedName("LSKA_NOTE")
    @Expose
    private String lSKANOTE;
    @SerializedName("RSALAMAT")
    @Expose
    private String rSALAMAT;
    @SerializedName("RSTYPE")
    @Expose
    private Integer rSTYPE;
    @SerializedName("MAPPOS")
    @Expose
    private String mAPPOS;
    @SerializedName("RSTELEPON")
    @Expose
    private String rSTELEPON;
    @SerializedName("RSID")
    @Expose
    private Integer rSID;
    @SerializedName("RSNAMA")
    @Expose
    private String rSNAMA;

    public String getLSKANOTE() {
        return lSKANOTE;
    }

    public void setLSKANOTE(String lSKANOTE) {
        this.lSKANOTE = lSKANOTE;
    }

    public String getRSALAMAT() {
        return rSALAMAT;
    }

    public void setRSALAMAT(String rSALAMAT) {
        this.rSALAMAT = rSALAMAT;
    }

    public Integer getRSTYPE() {
        return rSTYPE;
    }

    public void setRSTYPE(Integer rSTYPE) {
        this.rSTYPE = rSTYPE;
    }

    public String getMAPPOS() {
        return mAPPOS;
    }

    public void setMAPPOS(String mAPPOS) {
        this.mAPPOS = mAPPOS;
    }

    public String getRSTELEPON() {
        return rSTELEPON;
    }

    public void setRSTELEPON(String rSTELEPON) {
        this.rSTELEPON = rSTELEPON;
    }

    public Integer getRSID() {
        return rSID;
    }

    public void setRSID(Integer rSID) {
        this.rSID = rSID;
    }

    public String getRSNAMA() {
        return rSNAMA;
    }

    public void setRSNAMA(String rSNAMA) {
        this.rSNAMA = rSNAMA;
    }
}
