package id.co.ajsmsig.nb.crm.adapter;

import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.fragment.listlead.AgencyLeadChildList;
import id.co.ajsmsig.nb.crm.fragment.listlead.AgencyLeadParentList;
import id.co.ajsmsig.nb.crm.fragment.listlead.ChildLeadList;
import id.co.ajsmsig.nb.crm.fragment.listlead.ParentLeadList;

public class AgencyLeadAdapter extends ExpandableRecyclerViewAdapter<AgencyLeadAdapter.AgencyGroupLeadViewHolder, AgencyLeadAdapter.AgencyChildLeadViewHolder> {
    private ClickListener clickListener;
    public AgencyLeadAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    class AgencyChildLeadViewHolder extends ChildViewHolder {
        private TextView tvLeadNameInitial;
        private TextView tvName;
        private TextView tvAge;
        private ConstraintLayout layoutLeads;
        private ImageView imgFavoriteLead;
        private TextView tvPresentToClient;
        private TextView tvEditClient;
        private TextView tvRemoveClient;

        public AgencyChildLeadViewHolder(View view) {
            super(view);
            layoutLeads = view.findViewById(R.id.layoutLeads);
            tvLeadNameInitial = view.findViewById(R.id.tvLeadNameInitial);
            tvName = view.findViewById(R.id.tvName);
            tvAge = view.findViewById(R.id.tvAge);
            imgFavoriteLead = view.findViewById(R.id.imgFavoriteLead);
            tvPresentToClient = view.findViewById(R.id.tvPresentToClient);
            tvEditClient = view.findViewById(R.id.tvEditClient);
            tvRemoveClient = view.findViewById(R.id.tvRemoveClient);
        }

    }

    class AgencyGroupLeadViewHolder extends GroupViewHolder {
        private TextView groupName;
        private ImageView imgExpandCollapse;

        public AgencyGroupLeadViewHolder(View itemView) {
            super(itemView);
            groupName = itemView.findViewById(R.id.tvLeadGroupName);
            imgExpandCollapse = itemView.findViewById(R.id.imgExpandCollapse);
        }

        @Override
        public void expand() {
            imgExpandCollapse.setImageResource(R.drawable.ic_expand_less);
        }

        @Override
        public void collapse() {
            imgExpandCollapse.setImageResource(R.drawable.ic_expand_more);
        }

    }

    @Override
    public AgencyGroupLeadViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.c_group_lead, parent, false);
        return new AgencyGroupLeadViewHolder(view);
    }

    @Override
    public AgencyChildLeadViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_agency_lead, parent, false);
        return new AgencyChildLeadViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(final AgencyChildLeadViewHolder holder, int flatPosition, final ExpandableGroup group, int childIndex) {
        final AgencyLeadChildList model = ((AgencyLeadParentList) group).getItems().get(childIndex);

        String age = String.valueOf(model.getLeadAge()).concat(" tahun");


        if (model.isLeadFavorite()) {
            holder.imgFavoriteLead.setImageResource(R.drawable.ic_star_gold);
        } else {
            holder.imgFavoriteLead.setImageResource(R.drawable.ic_star_gray);
        }

        if (model.getLeadAge() > 0) {
            holder.tvAge.setVisibility(View.VISIBLE);
            holder.tvAge.setText(age);
        } else {
            holder.tvAge.setVisibility(View.INVISIBLE);
        }

        String firstNameInitial = model.getFirstNameInitial();
        String lastNameInitial = model.getLastNameInital();

        if (!TextUtils.isEmpty(firstNameInitial)) {
            String initial = firstNameInitial + lastNameInitial;
            holder.tvLeadNameInitial.setText(initial);
        }

        holder.tvName.setText(model.getLeadName());

        holder.layoutLeads.setOnClickListener(view -> clickListener.onLeadSelected(holder.getAdapterPosition(), view, model));
        holder.imgFavoriteLead.setOnClickListener(view -> {

            if (!model.isLeadFavorite()) {
                model.setLeadFavorite(true);
                holder.imgFavoriteLead.setImageResource(R.drawable.ic_star_gold);

            } else {
                model.setLeadFavorite(false);
                holder.imgFavoriteLead.setImageResource(R.drawable.ic_star_gray);
            }

            clickListener.onLeadSelected(holder.getAdapterPosition(), view, model);
        });

        holder.tvPresentToClient.setOnClickListener(view -> clickListener.onLeadSelected(holder.getAdapterPosition(), view, model));
        holder.tvEditClient.setOnClickListener(view -> clickListener.onLeadSelected(holder.getAdapterPosition(), view, model));
        holder.tvRemoveClient.setOnClickListener(view -> clickListener.onLeadSelected(holder.getAdapterPosition(), view, model));


    }

    @Override
    public void onBindGroupViewHolder(final AgencyGroupLeadViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.groupName.setText(group.getTitle());

    }

    public void setClickListener (ClickListener clickListener) {
        this.clickListener = clickListener;
    }
    public interface ClickListener{
        void onLeadSelected(int position, View view, AgencyLeadChildList leads);
    }


}
