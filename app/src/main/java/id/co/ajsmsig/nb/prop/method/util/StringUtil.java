package id.co.ajsmsig.nb.prop.method.util;

/**
 * Created by faiz_f on 18/10/2016.
 */

public class StringUtil {

    public static void addNewLineToSBuilderIfFilled(StringBuilder stringBuilder) {
        if (stringBuilder != null) {
            if (stringBuilder.length() != 0) {
                stringBuilder.append("\n");
            }
        }
    }
}
