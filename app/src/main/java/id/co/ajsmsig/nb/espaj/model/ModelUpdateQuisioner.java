package id.co.ajsmsig.nb.espaj.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_KetKesUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_NO9ppUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_NO9ttUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelMst_Answer;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTT_NO3UQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTT_NO5UQ;

/**
 * Created by Bernard on 28/09/2017.
 */

public class ModelUpdateQuisioner implements Parcelable {
    private ArrayList<ModelMst_Answer> ListQuisioner;
    private ArrayList<ModelTT_NO3UQ> List_ttno3;
    private ArrayList<ModelTT_NO5UQ> List_ttno5;
    private ArrayList<ModelDK_NO9ppUQ> List_dkno9pp;
    private ArrayList<ModelDK_NO9ttUQ> List_dkno9tt;
    private ArrayList<ModelDK_KetKesUQ> List_dkketkes;
    private Boolean validation = false ;

    public ModelUpdateQuisioner() {
    }


    protected ModelUpdateQuisioner(Parcel in) {
        ListQuisioner = in.createTypedArrayList(ModelMst_Answer.CREATOR);
        List_ttno3 = in.createTypedArrayList(ModelTT_NO3UQ.CREATOR);
        List_ttno5 = in.createTypedArrayList(ModelTT_NO5UQ.CREATOR);
        List_dkno9pp = in.createTypedArrayList(ModelDK_NO9ppUQ.CREATOR);
        List_dkno9tt = in.createTypedArrayList(ModelDK_NO9ttUQ.CREATOR);
        List_dkketkes = in.createTypedArrayList(ModelDK_KetKesUQ.CREATOR);
    }

    public ModelUpdateQuisioner(ArrayList<ModelMst_Answer> listQuisioner, ArrayList<ModelTT_NO3UQ> list_ttno3, ArrayList<ModelTT_NO5UQ> list_ttno5, ArrayList<ModelDK_NO9ppUQ> list_dkno9pp, ArrayList<ModelDK_NO9ttUQ> list_dkno9tt, ArrayList<ModelDK_KetKesUQ> list_dkketkes, Boolean validation) {
        ListQuisioner = listQuisioner;
        List_ttno3 = list_ttno3;
        List_ttno5 = list_ttno5;
        List_dkno9pp = list_dkno9pp;
        List_dkno9tt = list_dkno9tt;
        List_dkketkes = list_dkketkes;
        this.validation = validation;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(ListQuisioner);
        dest.writeTypedList(List_ttno3);
        dest.writeTypedList(List_ttno5);
        dest.writeTypedList(List_dkno9pp);
        dest.writeTypedList(List_dkno9tt);
        dest.writeTypedList(List_dkketkes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelUpdateQuisioner> CREATOR = new Creator<ModelUpdateQuisioner>() {
        @Override
        public ModelUpdateQuisioner createFromParcel(Parcel in) {
            return new ModelUpdateQuisioner(in);
        }

        @Override
        public ModelUpdateQuisioner[] newArray(int size) {
            return new ModelUpdateQuisioner[size];
        }
    };

    public ArrayList<ModelMst_Answer> getListQuisioner() {
        return ListQuisioner;
    }

    public void setListQuisioner(ArrayList<ModelMst_Answer> listQuisioner) {
        ListQuisioner = listQuisioner;
    }

    public ArrayList<ModelTT_NO3UQ> getList_ttno3() {
        return List_ttno3;
    }

    public void setList_ttno3(ArrayList<ModelTT_NO3UQ> list_ttno3) {
        List_ttno3 = list_ttno3;
    }

    public ArrayList<ModelTT_NO5UQ> getList_ttno5() {
        return List_ttno5;
    }

    public void setList_ttno5(ArrayList<ModelTT_NO5UQ> list_ttno5) {
        List_ttno5 = list_ttno5;
    }

    public ArrayList<ModelDK_NO9ppUQ> getList_dkno9pp() {
        return List_dkno9pp;
    }

    public void setList_dkno9pp(ArrayList<ModelDK_NO9ppUQ> list_dkno9pp) {
        List_dkno9pp = list_dkno9pp;
    }

    public ArrayList<ModelDK_NO9ttUQ> getList_dkno9tt() {
        return List_dkno9tt;
    }

    public void setList_dkno9tt(ArrayList<ModelDK_NO9ttUQ> list_dkno9tt) {
        List_dkno9tt = list_dkno9tt;
    }

    public ArrayList<ModelDK_KetKesUQ> getList_dkketkes() {
        return List_dkketkes;
    }

    public void setList_dkketkes(ArrayList<ModelDK_KetKesUQ> list_dkketkes) {
        List_dkketkes = list_dkketkes;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public static Creator<ModelUpdateQuisioner> getCREATOR() {
        return CREATOR;
    }
}
