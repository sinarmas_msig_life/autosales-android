package id.co.ajsmsig.nb.pointofsale.utils

import android.content.Context
import android.support.v4.content.ContextCompat
import java.util.*

/**
 * Created by andreyyoshuamanik on 13/03/18.
 */
class DownloaderUtil {

    companion object {
        fun getRootDirPath(context: Context?): String? {
            context?.let {
                if (isExternalStorageWritable()) {
                    val file = ContextCompat.getExternalFilesDirs(context.applicationContext, null)[0]
                    return file.absolutePath
                } else {
                    return context.applicationContext.filesDir.absolutePath
                }
            }
            return null
        }

        fun getProgressDisplayLine(curretBytes: Long, totalBytes: Long) : String {
            return "${getBytesToMBString(curretBytes)}/${getBytesToMBString(totalBytes)}"
        }

        fun getBytesToMBString(bytes: Long) : String {
            return String.format(Locale.ENGLISH, "%.2fMb", bytes / (1024.00 * 1024.00))
        }
    }



}