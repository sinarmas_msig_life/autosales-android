
package id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data_ {

    @SerializedName("SLA2_ADDR_TYPE")
    @Expose
    private String sLA2ADDRTYPE;
    @SerializedName("SLA2_HP1")
    @Expose
    private String sLA2HP1;
    @SerializedName("SLA2_TAB_ID")
    @Expose
    private String sLA2TABID;

    public String getSLA2ADDRTYPE() {
        return sLA2ADDRTYPE;
    }

    public void setSLA2ADDRTYPE(String sLA2ADDRTYPE) {
        this.sLA2ADDRTYPE = sLA2ADDRTYPE;
    }

    public String getSLA2HP1() {
        return sLA2HP1;
    }

    public void setSLA2HP1(String sLA2HP1) {
        this.sLA2HP1 = sLA2HP1;
    }

    public String getSLA2TABID() {
        return sLA2TABID;
    }

    public void setSLA2TABID(String sLA2TABID) {
        this.sLA2TABID = sLA2TABID;
    }

}
