package id.co.ajsmsig.nb.espaj.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.pdf.PdfRenderer;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.support.annotation.IdRes;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.vision.CameraSource;
//import com.google.android.gms.vision.text.TextRecognizer;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.TabSettings;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.AutoCompleteTextViewClickListener;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.hardcode.RequestCode;
import id.co.ajsmsig.nb.espaj.activity.E_FormAlamatActivity;
import id.co.ajsmsig.nb.espaj.activity.E_FormAlamatAutoZipActivity;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.adapter.Adapter_Pdf;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.method.ExtendedViewPager;
import id.co.ajsmsig.nb.espaj.method.F_Hit_Umur;
import id.co.ajsmsig.nb.espaj.method.GetTime;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.method.ProgressDialogClass;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.PemegangPolisModel;
import id.co.ajsmsig.nb.espaj.model.TertanggungModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAddRiderUA;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAskesUA;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBank;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBankCab;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDataDitunjukDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelFoto;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelJnsDanaDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelMst_Answer;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTT_NO3UQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTT_NO5UQ;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.method.ValueView;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.PaymentEvent;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

import static id.co.ajsmsig.nb.crm.method.StaticMethods.toCalendar;

/**
 * Created by Bernard on 15/05/2017.
 * edited by eriza
 */

public class E_PemegangPolisFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, AdapterView.OnItemClickListener, RadioGroup.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener, TextView.OnEditorActionListener {
    private boolean isJobDescriptionValid;
    private boolean isClick = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        groupId = sharedPreferences.getInt("GROUP_ID", 0);
        JENIS_LOGIN = sharedPreferences.getInt("JENIS_LOGIN", 0);
        JENIS_LOGIN_BC = sharedPreferences.getInt("JENIS_LOGIN_BC", 2);
        KODE_REG1 = sharedPreferences.getString("KODE_REG1", "");
        KODE_REG2 = sharedPreferences.getString("KODE_REG2", "");
        KODE_REG3 = sharedPreferences.getString("KODE_REG3", "");
        ROLE_ID = sharedPreferences.getInt("ROLE_ID",0);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem menu_validasi = menu.findItem(R.id.menu_validasi);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        polis = inflater.inflate(R.layout.e_fragment_pemegangpolis_layout, container,
                false);
        me = E_MainActivity.e_bundle.getParcelable(getString(R.string.modelespaj));
        ButterKnife.bind(this, polis);
        ((E_MainActivity) getActivity()).setSecondToolbar("Pemegang Polis (1/", 1);
        //AutoCompleteTextView
        ac_channel_distribusi_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_channel_distribusi_pp, this));
        ac_channel_distribusi_pp.addTextChangedListener(new generalTextWatcher(ac_channel_distribusi_pp));
        MethodSupport.active_view(0, ac_channel_distribusi_pp);

        ac_produk_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_produk_pp, this));
        ac_produk_pp.setOnClickListener(this);
        ac_produk_pp.setOnFocusChangeListener(this);
        ac_produk_pp.addTextChangedListener(new generalTextWatcher(ac_produk_pp));

        ac_sub_produk_pp.setOnClickListener(this);
        ac_sub_produk_pp.setOnFocusChangeListener(this);
        ac_sub_produk_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_sub_produk_pp, this));
        ac_sub_produk_pp.addTextChangedListener(new generalTextWatcher(ac_sub_produk_pp));

        ac_jns_spaj.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jns_spaj, this));
        ac_jns_spaj.setOnClickListener(this);
        ac_jns_spaj.setOnFocusChangeListener(this);
        ac_jns_spaj.addTextChangedListener(new generalTextWatcher(ac_jns_spaj));

        ac_agama_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_agama_pp, this));
        ac_agama_pp.setOnClickListener(this);
        ac_agama_pp.setOnFocusChangeListener(this);
        ac_agama_pp.addTextChangedListener(new generalTextWatcher(ac_agama_pp));

        ac_pendidikan_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_pendidikan_pp, this));
        ac_pendidikan_pp.setOnClickListener(this);
        ac_pendidikan_pp.setOnFocusChangeListener(this);
        ac_pendidikan_pp.addTextChangedListener(new generalTextWatcher(ac_pendidikan_pp));

        ac_bukti_identitas_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_bukti_identitas_pp, this));
        ac_bukti_identitas_pp.setOnClickListener(this);
        ac_bukti_identitas_pp.setOnFocusChangeListener(this);
        ac_bukti_identitas_pp.addTextChangedListener(new generalTextWatcher(ac_bukti_identitas_pp));

        ac_status_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_status_pp, this));
        ac_status_pp.setOnClickListener(this);
        ac_status_pp.setOnFocusChangeListener(this);
        ac_status_pp.addTextChangedListener(new generalTextWatcher(ac_status_pp));

        ac_tagihan_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_tagihan_pp, this));
        ac_tagihan_pp.setOnClickListener(this);
        ac_tagihan_pp.setOnFocusChangeListener(this);
        ac_tagihan_pp.addTextChangedListener(new generalTextWatcher(ac_tagihan_pp));

        ac_pendapatan_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_pendapatan_pp, this));
        ac_pendapatan_pp.setOnClickListener(this);
        ac_pendapatan_pp.setOnFocusChangeListener(this);
        ac_pendapatan_pp.addTextChangedListener(new generalTextWatcher(ac_pendapatan_pp));

        ac_hubungancp_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_hubungancp_pp, this));
        ac_hubungancp_pp.setOnClickListener(this);
        ac_hubungancp_pp.setOnFocusChangeListener(this);
        ac_hubungancp_pp.addTextChangedListener(new generalTextWatcher(ac_hubungancp_pp));

        ac_hubungan_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_hubungan_pp, this));
        ac_hubungan_pp.setOnClickListener(this);
        ac_hubungan_pp.setOnFocusChangeListener(this);
        ac_hubungan_pp.addTextChangedListener(new generalTextWatcher(ac_hubungan_pp));

        ac_warganegara_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_warganegara_pp, this));
        ac_warganegara_pp.setOnClickListener(this);
        ac_warganegara_pp.setOnFocusChangeListener(this);
        ac_warganegara_pp.addTextChangedListener(new generalTextWatcher(ac_warganegara_pp));

        ac_klasifikasi_pekerjaan_suami_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_klasifikasi_pekerjaan_suami_pp, this));
        ac_klasifikasi_pekerjaan_suami_pp.setOnClickListener(this);
        ac_klasifikasi_pekerjaan_suami_pp.setOnFocusChangeListener(this);
        ac_klasifikasi_pekerjaan_suami_pp.addTextChangedListener(new generalTextWatcher(ac_klasifikasi_pekerjaan_suami_pp));

        ac_jabatan_suami_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jabatan_suami_pp, this));
        ac_jabatan_suami_pp.setOnClickListener(this);
        ac_jabatan_suami_pp.setOnFocusChangeListener(this);
        ac_jabatan_suami_pp.addTextChangedListener(new generalTextWatcher(ac_jabatan_suami_pp));

        ac_penghasilan_pertahun_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_penghasilan_pertahun_pp, this));
        ac_penghasilan_pertahun_pp.setOnClickListener(this);
        ac_penghasilan_pertahun_pp.setOnFocusChangeListener(this);
        ac_penghasilan_pertahun_pp.addTextChangedListener(new generalTextWatcher(ac_penghasilan_pertahun_pp));

        ac_klasifikasi_pekerjaan_ayah_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_klasifikasi_pekerjaan_ayah_pp, this));
        ac_klasifikasi_pekerjaan_ayah_pp.setOnClickListener(this);
        ac_klasifikasi_pekerjaan_ayah_pp.setOnFocusChangeListener(this);
        ac_klasifikasi_pekerjaan_ayah_pp.addTextChangedListener(new generalTextWatcher(ac_klasifikasi_pekerjaan_ayah_pp));

        ac_jabatan_ayah_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jabatan_ayah_pp, this));
        ac_jabatan_ayah_pp.setOnClickListener(this);
        ac_jabatan_ayah_pp.setOnFocusChangeListener(this);
        ac_jabatan_ayah_pp.addTextChangedListener(new generalTextWatcher(ac_jabatan_ayah_pp));

        ac_penghasilan_pertahun_ayah_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_penghasilan_pertahun_ayah_pp, this));
        ac_penghasilan_pertahun_ayah_pp.setOnClickListener(this);
        ac_penghasilan_pertahun_ayah_pp.setOnFocusChangeListener(this);
        ac_penghasilan_pertahun_ayah_pp.addTextChangedListener(new generalTextWatcher(ac_penghasilan_pertahun_ayah_pp));

        ac_klasifikasi_pekerjaan_ibu_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_klasifikasi_pekerjaan_ibu_pp, this));
        ac_klasifikasi_pekerjaan_ibu_pp.setOnClickListener(this);
        ac_klasifikasi_pekerjaan_ibu_pp.setOnFocusChangeListener(this);
        ac_klasifikasi_pekerjaan_ibu_pp.addTextChangedListener(new generalTextWatcher(ac_klasifikasi_pekerjaan_ibu_pp));

        ac_jabatan_ibu_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jabatan_ibu_pp, this));
        ac_jabatan_ibu_pp.setOnClickListener(this);
        ac_jabatan_ibu_pp.setOnFocusChangeListener(this);
        ac_jabatan_ibu_pp.addTextChangedListener(new generalTextWatcher(ac_jabatan_ibu_pp));

        ac_penghasilan_pertahun_ibu_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_penghasilan_pertahun_ibu_pp, this));
        ac_penghasilan_pertahun_ibu_pp.setOnClickListener(this);
        ac_penghasilan_pertahun_ibu_pp.setOnFocusChangeListener(this);
        ac_penghasilan_pertahun_ibu_pp.addTextChangedListener(new generalTextWatcher(ac_penghasilan_pertahun_ibu_pp));

        ac_tagihan_korepondensi_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_tagihan_korepondensi_pp, this));
        ac_tagihan_korepondensi_pp.setOnClickListener(this);
        ac_tagihan_korepondensi_pp.setOnFocusChangeListener(this);
        ac_tagihan_korepondensi_pp.addTextChangedListener(new generalTextWatcher(ac_tagihan_korepondensi_pp));

        ac_klasifikasi_pekerjaan_ayah_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_klasifikasi_pekerjaan_ayah_pp, this));
        ac_klasifikasi_pekerjaan_ayah_pp.setOnClickListener(this);
        ac_klasifikasi_pekerjaan_ayah_pp.setOnFocusChangeListener(this);
        ac_klasifikasi_pekerjaan_ayah_pp.addTextChangedListener(new generalTextWatcher(ac_klasifikasi_pekerjaan_ayah_pp));

        ac_polis_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_polis_pp, this));
        ac_polis_pp.setOnClickListener(this);
        ac_polis_pp.setOnFocusChangeListener(this);
        ac_polis_pp.addTextChangedListener(new generalTextWatcher(ac_polis_pp));


        ac_klasifikasi_pekerjaan_pp.addTextChangedListener(new generalTextWatcher(ac_klasifikasi_pekerjaan_pp));
        ac_klasifikasi_pekerjaan_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_klasifikasi_pekerjaan_pp, this));

        ac_jabatan_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jabatan_pp, this));
        ac_jabatan_pp.setOnClickListener(this);
        ac_jabatan_pp.setOnFocusChangeListener(this);
        ac_jabatan_pp.addTextChangedListener(new generalTextWatcher(ac_jabatan_pp));

        ac_paket_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_paket_pp, this));
        ac_paket_pp.setOnClickListener(this);
        ac_paket_pp.setOnFocusChangeListener(this);
        ac_paket_pp.addTextChangedListener(new generalTextWatcher(ac_paket_pp));
        //button
        //ediText

        et_namaayah_pp.addTextChangedListener(new generalTextWatcher(et_namaayah_pp));
        et_ttl_ayah_pp.addTextChangedListener(new generalTextWatcher(et_ttl_ayah_pp));
        et_usia_ayah_pp.addTextChangedListener(new generalTextWatcher(et_usia_ayah_pp));
        et_nama_perusahaan_ayah_pp.addTextChangedListener(new generalTextWatcher(et_nama_perusahaan_ayah_pp));
        et_bidang_usaha_ayah_pp.addTextChangedListener(new generalTextWatcher(et_bidang_usaha_ayah_pp));
        et_npwpayah_pp.addTextChangedListener(new generalTextWatcher(et_npwpayah_pp));
        et_namaibu_pp.addTextChangedListener(new generalTextWatcher(et_namaibu_pp));
        et_ttl_ibu_pp.addTextChangedListener(new generalTextWatcher(et_ttl_ibu_pp));
        et_usia_ibu_pp.addTextChangedListener(new generalTextWatcher(et_usia_ibu_pp));
        et_nama_perusahaan_ibu_pp.addTextChangedListener(new generalTextWatcher(et_nama_perusahaan_ibu_pp));
        et_bidang_usaha_ibu_pp.addTextChangedListener(new generalTextWatcher(et_bidang_usaha_ibu_pp));
        et_npwpibu_pp.addTextChangedListener(new generalTextWatcher(et_npwpibu_pp));
        et_namasuami_pp.addTextChangedListener(new generalTextWatcher(et_namasuami_pp));
        et_ttl_suami_pp.addTextChangedListener(new generalTextWatcher(et_ttl_suami_pp));
        et_usia_suami_pp.addTextChangedListener(new generalTextWatcher(et_usia_suami_pp));
        et_nama_perusahaan_suami_pp.addTextChangedListener(new generalTextWatcher(et_nama_perusahaan_suami_pp));
        et_bidang_usaha_suami_pp.addTextChangedListener(new generalTextWatcher(et_bidang_usaha_suami_pp));
        et_npwpsuami_pp.addTextChangedListener(new generalTextWatcher(et_npwpsuami_pp));
        et_npwp_pp.addTextChangedListener(new generalTextWatcher(et_npwp_pp));

        et_no_proposal_pp.addTextChangedListener(new generalTextWatcher(et_no_proposal_pp));
        et_no_ciff_pp.addTextChangedListener(new generalTextWatcher(et_no_ciff_pp));
        et_nama_pp.addTextChangedListener(new generalTextWatcher(et_nama_pp));
        et_tempat_pp.addTextChangedListener(new generalTextWatcher(et_tempat_pp));

        et_ttl_pp.setOnClickListener(this);
        et_ttl_pp.setOnFocusChangeListener(this);
        et_ttl_pp.addTextChangedListener(new generalTextWatcher(et_ttl_pp));

        et_usia_pp.addTextChangedListener(new generalTextWatcher(et_usia_pp));
        et_ibu_pp.addTextChangedListener(new generalTextWatcher(et_ibu_pp));
        et_nomor_bukti_pp.addTextChangedListener(new generalTextWatcher(et_nomor_bukti_pp));
        et_tgl_berlaku_pp.addTextChangedListener(new generalTextWatcher(et_tgl_berlaku_pp));
        et_uraikan_tugas_pp.addTextChangedListener(new generalTextWatcher(et_uraikan_tugas_pp));
        et_nama_perusahaan_pp.addTextChangedListener(new generalTextWatcher(et_nama_perusahaan_pp));

        et_ttl_suami_pp.setOnClickListener(this);
        et_ttl_suami_pp.setOnFocusChangeListener(this);

        et_ttl_ayah_pp.setOnClickListener(this);
        et_ttl_ayah_pp.setOnFocusChangeListener(this);

        et_ttl_ibu_pp.setOnClickListener(this);
        et_ttl_ibu_pp.setOnFocusChangeListener(this);

        et_tgl_berlaku_pp.setOnClickListener(this);
        et_tgl_berlaku_pp.setOnFocusChangeListener(this);

        et_pekerjaansuami_pp.setOnEditorActionListener(this);
        et_pekerjaanayah_pp.setOnEditorActionListener(this);
        et_pekerjaanibu_pp.setOnEditorActionListener(this);
        //RadioGroup
        rg_jekel_pp.setOnCheckedChangeListener(this);
        rg_green_card_pp.setOnCheckedChangeListener(this);
        //CheckButton
        check_dana = new ArrayList<CheckBox>();
        check_dana.add(cb_checkd_gaji_pp);
        check_dana.add(cb_checkd_tabungan_pp);
        check_dana.add(cb_checkd_warisan_pp);
        check_dana.add(cb_checkd_hibah_pp);
        check_dana.add(cb_checkd_lainnya_pp);
        cb_checkd_gaji_pp.setOnCheckedChangeListener(this);
        cb_checkd_tabungan_pp.setOnCheckedChangeListener(this);
        cb_checkd_warisan_pp.setOnCheckedChangeListener(this);
        cb_checkd_hibah_pp.setOnCheckedChangeListener(this);
        cb_checkd_lainnya_pp.setOnCheckedChangeListener(this);

        check_tujuan = new ArrayList<CheckBox>();
        check_tujuan.add(cb_checkt_inves_pp);
        check_tujuan.add(cb_checkt_proteksi_pp);
        check_tujuan.add(cb_checkt_lainnya_pp);
        cb_checkt_inves_pp.setOnCheckedChangeListener(this);
        cb_checkt_lainnya_pp.setOnCheckedChangeListener(this);
        cb_checkt_proteksi_pp.setOnCheckedChangeListener(this);
        //TextView
        tv_isi_alamat_kantor_pp.setOnClickListener(this);
        tv_isi_alamat_rumah_pp.setOnClickListener(this);
        tv_isi_alamat_tempat_tinggal_pp.setOnClickListener(this);
        tv_ubah_alamat_kantor_pp.setOnClickListener(this);
        tv_ubah_alamat_rumah_pp.setOnClickListener(this);
        tv_ubah_alamat_tempat_tinggal_pp.setOnClickListener(this);
        tv_isi_hp_email_pp.setOnClickListener(this);
        tv_ubah_hp_email_pp.setOnClickListener(this);
        //ImageView
        img_alamat_kantor_pp.setOnClickListener(this);
        img_alamat_rumah_pp.setOnClickListener(this);
        img_alamat_tempat_tinggal_pp.setOnClickListener(this);
        img_hp_email_pp.setOnClickListener(this);

        //button di Activity
        btn_lanjut = getActivity().findViewById(R.id.btn_lanjut);
        btn_lanjut.setOnClickListener(this);
        btn_kembali = getActivity().findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(this);
        second_toolbar = ((E_MainActivity) getActivity()).getSecond_toolbar();
        iv_save = second_toolbar.findViewById(R.id.iv_save);
        iv_save.setOnClickListener(this);
        iv_next = second_toolbar.findViewById(R.id.iv_next);
        iv_next.setOnClickListener(this);
        iv_prev = second_toolbar.findViewById(R.id.iv_prev);
        iv_prev.setOnClickListener(this);
        setAdapterProduk();
        setAdapterSubProduk();
        restore();


        return polis;
    }

    @Override
    public void onPause() {
        super.onPause();
        ProgressDialogClass.removeSimpleProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        ProgressDialogClass.removeSimpleProgressDialog();
        MethodSupport.active_view(1, btn_lanjut);
        MethodSupport.active_view(1, iv_next);
        MethodSupport.active_view(1, btn_kembali);
        MethodSupport.active_view(1, iv_prev);
        try {
            MethodSupport.load_setDropXml(R.xml.e_agama, ac_agama_pp, getContext(), 0);
            if (me.getDetilAgenModel().getGroup_id_da() == 40) {
                MethodSupport.load_setDropXml(R.xml.e_identity, ac_bukti_identitas_pp, getContext(), 9);
            } else {
                MethodSupport.load_setDropXml(R.xml.e_identity, ac_bukti_identitas_pp, getContext(), 0);
            }
            MethodSupport.load_setDropXml(R.xml.e_marital, ac_status_pp, getContext(), 0);
            MethodSupport.load_setDropXml(R.xml.e_pendidikan, ac_pendidikan_pp, getContext(), 0);
            MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_warganegara_pp, getContext(), 0);
            MethodSupport.load_setDropXml(R.xml.e_jenis_spaj, ac_jns_spaj, getContext(), 0);
            MethodSupport.load_setDropXml(R.xml.e_alamat_tagihan, ac_tagihan_pp, getContext(), 0);
            if (!me.getModelID().getProposal_tab().equals("")) {
                MethodSupport.load_setDropXml(R.xml.e_relasi, ac_hubungan_pp, getContext(), 4);
            } else {
                MethodSupport.load_setDropXml(R.xml.e_relasi, ac_hubungan_pp, getContext(), 1);
            }
            MethodSupport.load_setDropXml(R.xml.e_relasi, ac_hubungancp_pp, getContext(), 2);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
//        setAdapterProduk();
//        setAdapterSubProduk();

        if(wn_pp==0) {
            ac_warganegara_pp.setText("INDONESIA");
            wn_pp=1;
        }else {
            try {
                MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_warganegara_pp, getContext(), 0);
            }catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
        }

        setAdapterPekerjaan("", ac_klasifikasi_pekerjaan_pp);
        setAdapterPekerjaan("", ac_klasifikasi_pekerjaan_ayah_pp);
        setAdapterPekerjaan("", ac_klasifikasi_pekerjaan_ibu_pp);
        setAdapterPekerjaan("", ac_klasifikasi_pekerjaan_suami_pp);
        setAdapterJabatan();
        setAdapterChannelDist();
        setAdapterPenghasilan();

        checkForSelectedJobDescriptionIsOnTheList(ac_klasifikasi_pekerjaan_pp.getText().toString().trim(), ac_klasifikasi_pekerjaan_pp);
        //Make the job description field dots orange as default when user haven't select any job desc.
        if (TextUtils.isEmpty(ac_klasifikasi_pekerjaan_pp.getText().toString().trim())) {
            iv_circle_klasifikasi_pekerjaan_pp.setColorFilter(ContextCompat.getColor(getActivity(), R.color.orange));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_validasi:
                me.getPemegangPolisModel().setValidation(Validation());
                loadview();
                ((E_MainActivity) getActivity()).onSave(Const.PAGE_PEMEGANG_POLIS);
                Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTaskValidasi extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_PEMEGANG_POLIS);
            return null;
        }
    }

    private class MyTaskSavePage extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_PEMEGANG_POLIS);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //View di activity
            case R.id.btn_lanjut:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_next:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_save:
                //To handle user type "tidak ada" on deskripsi pekerjaan
                String jobDescription = ac_klasifikasi_pekerjaan_pp.getText().toString().trim();
                if (TextUtils.isEmpty(jobDescription)) {
                    ac_klasifikasi_pekerjaan_pp.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
                    ac_klasifikasi_pekerjaan_pp.requestFocus();
                    return;
                }

                if (!isJobDescriptionValid) {
                    ac_klasifikasi_pekerjaan_pp.setError("Deskripsi pekerjaan ini tidak ada dalam pilihan");
                    ac_klasifikasi_pekerjaan_pp.requestFocus();
                    return;
                }

                loadview();
//                MyTaskSavePage taskSavePage = new MyTaskSavePage();
//                taskSavePage.execute();
                ((E_MainActivity) getActivity()).onSave(Const.PAGE_PEMEGANG_POLIS);
                Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();

                break;
            case R.id.iv_prev:
                onClickBack();
                break;
            case R.id.btn_kembali:
                onClickBack();
                break;
            //dropdown:
            case R.id.ac_channel_distribusi_pp:
                ac_channel_distribusi_pp.showDropDown();
                break;
            case R.id.ac_produk_pp:
                ac_produk_pp.showDropDown();
                break;
            case R.id.ac_sub_produk_pp:
                ac_sub_produk_pp.showDropDown();
                break;
            case R.id.ac_jns_spaj:
                ac_jns_spaj.showDropDown();
                break;
            case R.id.ac_agama_pp:
                ac_agama_pp.showDropDown();
                break;
            case R.id.ac_pendidikan_pp:
                ac_pendidikan_pp.showDropDown();
                break;
            case R.id.ac_bukti_identitas_pp:
                ac_bukti_identitas_pp.showDropDown();
                break;
            case R.id.ac_status_pp:
                ac_status_pp.showDropDown();
                break;
            case R.id.ac_tagihan_pp:
                ac_tagihan_pp.showDropDown();
                break;
            case R.id.ac_pendapatan_pp:
                ac_pendapatan_pp.showDropDown();
                break;
            case R.id.ac_hubungancp_pp:
                ac_hubungancp_pp.showDropDown();
                break;
            case R.id.ac_hubungan_pp:
                ac_hubungan_pp.showDropDown();
                break;
            case R.id.ac_warganegara_pp:
                try {
                    MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_warganegara_pp, getContext(), 0);
                }catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
                ac_warganegara_pp.showDropDown();
                break;
            case R.id.ac_klasifikasi_pekerjaan_suami_pp:
                ac_klasifikasi_pekerjaan_suami_pp.showDropDown();
                break;
            case R.id.ac_jabatan_suami_pp:
                ac_jabatan_suami_pp.showDropDown();
                break;
            case R.id.ac_penghasilan_pertahun_pp:
                ac_penghasilan_pertahun_pp.showDropDown();
                break;
            case R.id.ac_klasifikasi_pekerjaan_ayah_pp:
                ac_klasifikasi_pekerjaan_ayah_pp.showDropDown();
                break;
            case R.id.ac_jabatan_ayah_pp:
                ac_jabatan_ayah_pp.showDropDown();
                break;
            case R.id.ac_penghasilan_pertahun_ayah_pp:
                ac_penghasilan_pertahun_ayah_pp.showDropDown();
                break;
            case R.id.ac_klasifikasi_pekerjaan_ibu_pp:
                ac_klasifikasi_pekerjaan_ibu_pp.showDropDown();
                break;
            case R.id.ac_jabatan_ibu_pp:
                ac_jabatan_ibu_pp.showDropDown();
                break;
            case R.id.ac_penghasilan_pertahun_ibu_pp:
                ac_penghasilan_pertahun_ibu_pp.showDropDown();
                break;
            case R.id.ac_tagihan_korepondensi_pp:
                ac_tagihan_korepondensi_pp.showDropDown();
                break;
            case R.id.ac_polis_pp:
                ac_polis_pp.showDropDown();
                break;
            case R.id.ac_klasifikasi_pekerjaan_pp:
                ac_klasifikasi_pekerjaan_pp.showDropDown();
                break;
            case R.id.ac_jabatan_pp:
                ac_jabatan_pp.showDropDown();
                break;
            case R.id.ac_paket_pp:
                ac_paket_pp.showDropDown();
                break;
            //TextView
            case R.id.tv_isi_alamat_rumah_pp:
                goToForm(1);
                break;
            case R.id.tv_isi_alamat_kantor_pp:
                goToForm(2);
                break;
            case R.id.tv_isi_alamat_tempat_tinggal_pp:
                goToForm(3);
                break;
            case R.id.tv_ubah_alamat_rumah_pp:
                goToForm(1);
                break;
            case R.id.tv_ubah_alamat_kantor_pp:
                goToForm(2);
                break;
            case R.id.tv_ubah_alamat_tempat_tinggal_pp:
                goToForm(3);
                break;
            case R.id.tv_isi_hp_email_pp:
                goToForm(7);
                break;
            case R.id.tv_ubah_hp_email_pp:
                goToForm(7);
                break;
            //ImageView
            case R.id.img_alamat_rumah_pp:
                goToForm(1);
                break;
            case R.id.img_alamat_kantor_pp:
                goToForm(2);
                break;
            case R.id.img_alamat_tempat_tinggal_pp:
                goToForm(3);
                break;
            case R.id.img_hp_email_pp:
                goToForm(7);
                break;
            //editText
            case R.id.et_ttl_pp:
                showDateDialog(1, et_ttl_pp.getText().toString(), et_ttl_pp);
                break;
            case R.id.et_ttl_suami_pp:
                showDateDialog(2, et_ttl_suami_pp.getText().toString(), et_ttl_suami_pp);
                break;
            case R.id.et_ttl_ayah_pp:
                showDateDialog(3, et_ttl_ayah_pp.getText().toString(), et_ttl_ayah_pp);
                break;
            case R.id.et_ttl_ibu_pp:
                showDateDialog(4, et_ttl_ibu_pp.getText().toString(), et_ttl_ibu_pp);
                break;
            case R.id.et_tgl_berlaku_pp:
                showDateDialog(5, et_tgl_berlaku_pp.getText().toString(), et_tgl_berlaku_pp);
                break;
        }
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.ac_channel_distribusi_pp:
                if (hasFocus) ac_channel_distribusi_pp.showDropDown();
                break;
            case R.id.ac_produk_pp:
                if (hasFocus) ac_produk_pp.showDropDown();
                break;
            case R.id.ac_sub_produk_pp:
                if (hasFocus) ac_sub_produk_pp.showDropDown();
                break;
            case R.id.ac_jns_spaj:
                if (hasFocus) ac_jns_spaj.showDropDown();
                break;
            case R.id.ac_agama_pp:
                if (hasFocus) ac_agama_pp.showDropDown();
                break;
            case R.id.ac_pendidikan_pp:
                if (hasFocus) ac_pendidikan_pp.showDropDown();
                break;
            case R.id.ac_bukti_identitas_pp:
                if (hasFocus) ac_bukti_identitas_pp.showDropDown();
                break;
            case R.id.ac_status_pp:
                if (hasFocus) ac_status_pp.showDropDown();
                break;
            case R.id.ac_tagihan_pp:
                if (hasFocus) ac_tagihan_pp.showDropDown();
                break;
            case R.id.ac_pendapatan_pp:
                if (hasFocus) ac_pendapatan_pp.showDropDown();
                break;
            case R.id.ac_hubungancp_pp:
                if (hasFocus) ac_hubungancp_pp.showDropDown();
                break;
            case R.id.ac_hubungan_pp:
                if (hasFocus) ac_hubungan_pp.showDropDown();
                break;
            case R.id.ac_warganegara_pp:
                try {
                    MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_warganegara_pp, getContext(), 0);
                }catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
                if (hasFocus) ac_warganegara_pp.showDropDown();
                break;
            case R.id.ac_klasifikasi_pekerjaan_suami_pp:
                if (hasFocus) ac_klasifikasi_pekerjaan_suami_pp.showDropDown();
                break;
            case R.id.ac_jabatan_suami_pp:
                if (hasFocus) ac_jabatan_suami_pp.showDropDown();
                break;
            case R.id.ac_penghasilan_pertahun_pp:
                if (hasFocus) ac_penghasilan_pertahun_pp.showDropDown();
                break;
            case R.id.ac_klasifikasi_pekerjaan_ayah_pp:
                if (hasFocus) ac_klasifikasi_pekerjaan_ayah_pp.showDropDown();
                break;
            case R.id.ac_jabatan_ayah_pp:
                if (hasFocus) ac_jabatan_ayah_pp.showDropDown();
                break;
            case R.id.ac_penghasilan_pertahun_ayah_pp:
                if (hasFocus) ac_penghasilan_pertahun_ayah_pp.showDropDown();
                break;
            case R.id.ac_klasifikasi_pekerjaan_ibu_pp:
                if (hasFocus) ac_klasifikasi_pekerjaan_ibu_pp.showDropDown();
                break;
            case R.id.ac_jabatan_ibu_pp:
                if (hasFocus) ac_jabatan_ibu_pp.showDropDown();
                break;
            case R.id.ac_penghasilan_pertahun_ibu_pp:
                if (hasFocus) ac_penghasilan_pertahun_ibu_pp.showDropDown();
                break;
            case R.id.ac_tagihan_korepondensi_pp:
                if (hasFocus) ac_tagihan_korepondensi_pp.showDropDown();
                break;
            case R.id.ac_polis_pp:
                if (hasFocus) ac_polis_pp.showDropDown();
                break;
            case R.id.ac_klasifikasi_pekerjaan_pp:
                if (hasFocus) ac_klasifikasi_pekerjaan_pp.showDropDown();
                break;
            case R.id.ac_jabatan_pp:
                if (hasFocus) ac_jabatan_pp.showDropDown();
                break;
            case R.id.ac_paket_pp:
                if (hasFocus) ac_paket_pp.showDropDown();
                break;
            //editText
            case R.id.et_ttl_pp:
                if (hasFocus) showDateDialog(1, et_ttl_pp.getText().toString(), et_ttl_pp);
                break;
            case R.id.et_ttl_suami_pp:
                if (hasFocus)
                    showDateDialog(2, et_ttl_suami_pp.getText().toString(), et_ttl_suami_pp);
                break;
            case R.id.et_ttl_ayah_pp:
                if (hasFocus)
                    showDateDialog(3, et_ttl_ayah_pp.getText().toString(), et_ttl_ayah_pp);
                break;
            case R.id.et_ttl_ibu_pp:
                if (hasFocus) showDateDialog(4, et_ttl_ibu_pp.getText().toString(), et_ttl_ibu_pp);
                break;
            case R.id.et_tgl_berlaku_pp:
                if (hasFocus)
                    showDateDialog(5, et_tgl_berlaku_pp.getText().toString(), et_tgl_berlaku_pp);
                break;
        }

    }

    private void showDateDialog(int flag_ttl, String tanggal, EditText edit) {
        switch (flag_ttl) {
            case 1:
                if (tanggal.length() != 0) {
                    cal_pp = toCalendar(tanggal);
                } else {
                    cal_pp = Calendar.getInstance();
                }
                DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                        datePickerListener, cal_pp.get(Calendar.YEAR), cal_pp.get(Calendar.MONTH), cal_pp.get(Calendar.DATE));
                dialog.getDatePicker().setMaxDate(cal_pp.getTimeInMillis());
                dialog.show();
                break;
            case 2:
                if (tanggal.length() != 0) {
                    cal_suami_pp = toCalendar(tanggal);
                } else {
                    cal_suami_pp = Calendar.getInstance();
                }
                dialog = new DatePickerDialog(getActivity(),
                        datePickerListener_suami, cal_suami_pp.get(Calendar.YEAR), cal_suami_pp.get(Calendar.MONTH), cal_suami_pp.get(Calendar.DATE));
                dialog.getDatePicker().setMaxDate(cal_suami_pp.getTimeInMillis());
                dialog.show();
                break;
            case 3:
                if (tanggal.length() != 0) {
                    cal_ayah_pp = toCalendar(tanggal);
                } else {
                    cal_ayah_pp = Calendar.getInstance();
                }
                dialog = new DatePickerDialog(getActivity(),
                        datePickerListener_ayah, cal_ayah_pp.get(Calendar.YEAR), cal_ayah_pp.get(Calendar.MONTH), cal_ayah_pp.get(Calendar.DATE));
                dialog.getDatePicker().setMaxDate(cal_ayah_pp.getTimeInMillis());
                dialog.show();
                break;
            case 4:
                if (tanggal.length() != 0) {
                    cal_ibu_pp = toCalendar(tanggal);
                } else {
                    cal_ibu_pp = Calendar.getInstance();
                }
                dialog = new DatePickerDialog(getActivity(),
                        datePickerListener_ibu, cal_ibu_pp.get(Calendar.YEAR), cal_ibu_pp.get(Calendar.MONTH), cal_ibu_pp.get(Calendar.DATE));
                dialog.getDatePicker().setMaxDate(cal_ibu_pp.getTimeInMillis());
                dialog.show();
                break;
            case 5:
                if (tanggal.length() != 0) {
                    cal_berlaku_pp = toCalendar(tanggal);
                } else {
                    cal_berlaku_pp = Calendar.getInstance();
                }
                dialog = new DatePickerDialog(getActivity(),
                        datePickerListener_berlaku, cal_berlaku_pp.get(Calendar.YEAR), cal_berlaku_pp.get(Calendar.MONTH), cal_berlaku_pp.get(Calendar.DATE));
                dialog.getDatePicker().setMinDate(cal_berlaku_pp.getTimeInMillis());
                dialog.show();
                break;
        }
    }


    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            cal_pp.set(Calendar.YEAR, selectedYear);
            cal_pp.set(Calendar.MONTH, selectedMonth);
            cal_pp.set(Calendar.DAY_OF_MONTH, selectedDay);
            et_ttl_pp.setText(StaticMethods.toStringDate(cal_pp, 5));
            et_usia_pp.setText(F_Hit_Umur.OnCountUsia(cal_pp.get(Calendar.YEAR), cal_pp.get(Calendar.MONTH), cal_pp.get(Calendar.DATE)));
        }
    };
    private DatePickerDialog.OnDateSetListener datePickerListener_suami = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            cal_suami_pp.set(Calendar.YEAR, selectedYear);
            cal_suami_pp.set(Calendar.MONTH, selectedMonth);
            cal_suami_pp.set(Calendar.DAY_OF_MONTH, selectedDay);
            et_ttl_suami_pp.setText(StaticMethods.toStringDate(cal_suami_pp, 5));
            et_usia_suami_pp.setText(F_Hit_Umur.OnCountUsia(cal_suami_pp.get(Calendar.YEAR), cal_suami_pp.get(Calendar.MONTH), cal_suami_pp.get(Calendar.DATE)));
        }
    };
    private DatePickerDialog.OnDateSetListener datePickerListener_ayah = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            cal_ayah_pp.set(Calendar.YEAR, selectedYear);
            cal_ayah_pp.set(Calendar.MONTH, selectedMonth);
            cal_ayah_pp.set(Calendar.DAY_OF_MONTH, selectedDay);
            et_ttl_ayah_pp.setText(StaticMethods.toStringDate(cal_ayah_pp, 5));
            et_usia_ayah_pp.setText(F_Hit_Umur.OnCountUsia(cal_ayah_pp.get(Calendar.YEAR), cal_ayah_pp.get(Calendar.MONTH), cal_ayah_pp.get(Calendar.DATE)));
        }
    };
    private DatePickerDialog.OnDateSetListener datePickerListener_ibu = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            cal_ibu_pp.set(Calendar.YEAR, selectedYear);
            cal_ibu_pp.set(Calendar.MONTH, selectedMonth);
            cal_ibu_pp.set(Calendar.DAY_OF_MONTH, selectedDay);
            et_ttl_ibu_pp.setText(StaticMethods.toStringDate(cal_ibu_pp, 5));
            et_usia_ibu_pp.setText(F_Hit_Umur.OnCountUsia(cal_ibu_pp.get(Calendar.YEAR), cal_ibu_pp.get(Calendar.MONTH), cal_ibu_pp.get(Calendar.DATE)));
        }
    };
    private DatePickerDialog.OnDateSetListener datePickerListener_berlaku = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            cal_berlaku_pp.set(Calendar.YEAR, selectedYear);
            cal_berlaku_pp.set(Calendar.MONTH, selectedMonth);
            cal_berlaku_pp.set(Calendar.DAY_OF_MONTH, selectedDay);
            et_tgl_berlaku_pp.setText(StaticMethods.toStringDate(cal_berlaku_pp, 5));
        }
    };

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.ac_channel_distribusi_pp:
                ModelDropdownInt modelChannel = (ModelDropdownInt) parent.getAdapter().getItem(position);
                kd_jns_produk = modelChannel.getId();
                break;
            case R.id.ac_produk_pp:
                DropboxModel chooseItem = (DropboxModel) parent.getAdapter().getItem(position);
                produk = chooseItem.getId();
                if (produk != me.getUsulanAsuransiModel().getKd_produk_ua()) {
                    setAdapterSubProduk();
                    ac_sub_produk_pp.setText("");
                }
                me.getUsulanAsuransiModel().setKd_produk_ua(produk);

                ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_jenis_spaj, 0);
                int flag_spaj = new E_Select(getContext()).SelectJnsSPAJ(produk);
                ac_jns_spaj.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, flag_spaj));
                MethodSupport.active_view(0, ac_jns_spaj);
                break;
            case R.id.ac_sub_produk_pp:
                DropboxModel chooseSub = (DropboxModel) parent.getAdapter().getItem(position);
                nm_produk = chooseSub.getId();

                break;
            case R.id.ac_jns_spaj:
                ModelDropdownInt modelJns = (ModelDropdownInt) parent.getAdapter().getItem(position);
                jnsspaj = modelJns.getId();
                if (jnsspaj != 2) {
                    cl_alias_pp.setVisibility(View.GONE);
                    cl_form_korespondensi_polis_pp.setVisibility(View.GONE);
                }
                break;
            case R.id.ac_agama_pp:
                ModelDropdownInt modelAgama = (ModelDropdownInt) parent.getAdapter().getItem(position);
                agama = modelAgama.getId();
                val_agama(agama);
                break;
            case R.id.ac_warganegara_pp:
                ModelDropdownInt ModelWarga = (ModelDropdownInt) parent.getAdapter().getItem(position);
                wn_pp = ModelWarga.getId();
                break;
            case R.id.ac_pendidikan_pp:
                ModelDropdownInt modelPendidikan = (ModelDropdownInt) parent.getAdapter().getItem(position);
                pend_pp = modelPendidikan.getId();
                break;
            case R.id.ac_bukti_identitas_pp:
                ModelDropdownInt modelBuktiIdentitas = (ModelDropdownInt) parent.getAdapter().getItem(position);
                bukti_pp = modelBuktiIdentitas.getId();
                val_bukti(bukti_pp);
                break;
            case R.id.ac_status_pp:
                ModelDropdownInt modelStatus = (ModelDropdownInt) parent.getAdapter().getItem(position);
                stat_pp = modelStatus.getId();
                break;
            case R.id.ac_tagihan_pp:
                ModelDropdownInt modelTagihan = (ModelDropdownInt) parent.getAdapter().getItem(position);
                tagihan = modelTagihan.getId();
                val_tagihan(tagihan);
                me.getPemegangPolisModel().setTagihan_pp(tagihan);
                break;
            case R.id.ac_pendapatan_pp:
                str_pendapatan = ac_pendapatan_pp.getText().toString();
                break;
            case R.id.ac_hubungancp_pp:
                ModelDropdownInt modelcp_pp = (ModelDropdownInt) parent.getAdapter().getItem(position);
                cp_pp = modelcp_pp.getId();
                break;
            case R.id.ac_hubungan_pp:
                ModelDropdownInt modelHub_pp = (ModelDropdownInt) parent.getAdapter().getItem(position);
                hub = modelHub_pp.getId();
                val_hubpptt();
                break;
            case R.id.ac_jabatan_suami_pp:
                ModelDropDownString modelJabatan_suami = (ModelDropDownString) parent.getAdapter().getItem(position);
                str_jabatansuami_rt = modelJabatan_suami.getId();
                break;
            case R.id.ac_penghasilan_pertahun_pp:
                str_penghasilan_suamithn_pp = ac_penghasilan_pertahun_pp.getText().toString();
                break;
            case R.id.ac_jabatan_ayah_pp:
                ModelDropDownString modelJabatan_ayah = (ModelDropDownString) parent.getAdapter().getItem(position);
                str_jabatan_ayah = modelJabatan_ayah.getId();
                break;
            case R.id.ac_penghasilan_pertahun_ayah_pp:
                str_penghasilan_ayah_pp = ac_penghasilan_pertahun_ayah_pp.getText().toString();
                break;
            case R.id.ac_jabatan_ibu_pp:
                ModelDropDownString modelJabatan_ibu = (ModelDropDownString) parent.getAdapter().getItem(position);
                str_jabatan_ibu = modelJabatan_ibu.getId();
                break;
            case R.id.ac_penghasilan_pertahun_ibu_pp:
                str_penghasilan_ibu_pp = ac_penghasilan_pertahun_ibu_pp.getText().toString();
                break;
            case R.id.ac_tagihan_korepondensi_pp:

                break;
            case R.id.ac_polis_pp:
                ModelDropDownString modelKrm_polis = (ModelDropDownString) parent.getAdapter().getItem(position);
                str_krm_polis = modelKrm_polis.getId();
                break;
            case R.id.ac_klasifikasi_pekerjaan_pp:
                //To make sure job description is only allowed picked from the dropdown
                isJobDescriptionValid = true;
                iv_circle_klasifikasi_pekerjaan_pp.setColorFilter(ContextCompat.getColor(getContext(), R.color.green));
//                ModelDropdownInt model_pekerjaan = (ModelDropdownInt) parent.getAdapter().getItem(position);
                pekerjaan = ac_klasifikasi_pekerjaan_pp.getText().toString();
                val_pekerjaan(pekerjaan);
                break;
            case R.id.ac_jabatan_pp:
                ModelDropDownString modeljbtn = (ModelDropDownString) parent.getAdapter().getItem(position);
                str_jabatan_klasifikasi = modeljbtn.getValue();
                break;
            case R.id.ac_paket_pp:
                DropboxModel paketModel = (DropboxModel) parent.getAdapter().getItem(position);
                paket = paketModel.getFlagPacket();
                break;
        }
    }

    //Khusus RadioGroup
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
        int color = ContextCompat.getColor(getContext(), R.color.red);
        switch (radioGroup.getId()) {
            case R.id.rg_green_card_pp:
                greencard = MethodSupport.OnCheckRadio(rg_green_card_pp, checkedId);
                break;
            case R.id.rg_jekel_pp:
                jekel = MethodSupport.OnCheckRadio(rg_jekel_pp, checkedId);
                iv_circle_jekel_pp.setColorFilter(ContextCompat.getColor(getContext(), R.color.green));
                break;
        }
    }

    //Khusus Checkbox
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_checkd_gaji_pp:
                d_gaji_pp = (isChecked) ? cb_checkd_gaji_pp.getText().toString() : "";
                bool_dana = Method_Validator.ValidCheckDana(check_dana, iv_circle_sumber_dana, getContext());
                break;
            case R.id.cb_checkd_tabungan_pp:
                d_tabungan_pp = (isChecked) ? cb_checkd_tabungan_pp.getText().toString() : "";
                bool_dana = Method_Validator.ValidCheckDana(check_dana, iv_circle_sumber_dana, getContext());
            case R.id.cb_checkd_warisan_pp:
                d_warisan_pp = (isChecked) ? cb_checkd_warisan_pp.getText().toString() : "";
                bool_dana = Method_Validator.ValidCheckDana(check_dana, iv_circle_sumber_dana, getContext());
                break;
            case R.id.cb_checkd_hibah_pp:
                d_hibah_pp = (isChecked) ? cb_checkd_hibah_pp.getText().toString() : "";
                bool_dana = Method_Validator.ValidCheckDana(check_dana, iv_circle_sumber_dana, getContext());
                break;
            case R.id.cb_checkd_lainnya_pp:
                d_lainnya_pp = (isChecked) ? cb_checkd_lainnya_pp.getText().toString() : "";
                bool_dana = Method_Validator.ValidCheckDana(check_dana, iv_circle_sumber_dana, getContext());
                break;
            case R.id.cb_checkt_proteksi_pp:
                t_proteksi_pp = (isChecked) ? cb_checkt_proteksi_pp.getText().toString() : "";
                bool_tujuan = Method_Validator.ValidCheckDana(check_tujuan, iv_circle_checkt_proteksi_pp, getContext());
                break;
            case R.id.cb_checkt_inves_pp:
                t_inves_pp = (isChecked) ? cb_checkt_inves_pp.getText().toString() : "";
                bool_tujuan = Method_Validator.ValidCheckDana(check_tujuan, iv_circle_checkt_proteksi_pp, getContext());
                break;
            case R.id.cb_checkt_lainnya_pp:
                t_lainnya_pp = (isChecked) ? cb_checkt_lainnya_pp.getText().toString() : "";
                bool_tujuan = Method_Validator.ValidCheckDana(check_tujuan, iv_circle_checkt_proteksi_pp, getContext());
                break;

        }

    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        switch (textView.getId()) {

            case R.id.et_pekerjaanayah_pp:
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) getActivity()
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(et_pekerjaanayah_pp.getWindowToken(),
                            0);
                    setAdapterPekerjaan(et_pekerjaanayah_pp.getText().toString(), ac_klasifikasi_pekerjaan_ayah_pp);
                    return true;
                }
                break;
            case R.id.et_pekerjaanibu_pp:
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) getActivity()
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(et_pekerjaanibu_pp.getWindowToken(),
                            0);
                    setAdapterPekerjaan(et_pekerjaanibu_pp.getText().toString(), ac_klasifikasi_pekerjaan_ibu_pp);
                    return true;
                }
                break;
            case R.id.et_pekerjaansuami_pp:
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) getActivity()
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(et_pekerjaansuami_pp.getWindowToken(),
                            0);
                    setAdapterPekerjaan(et_pekerjaansuami_pp.getText().toString(), ac_klasifikasi_pekerjaan_suami_pp);
                    return true;
                }
                break;
        }
        return false;
    }

    //AutoCompleteTextView atau EditText
    private class generalTextWatcher implements TextWatcher {
        private View view;

        generalTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                //autocompleteTextView
                case R.id.ac_channel_distribusi_pp:
                    Method_Validator.ValTextWatcher(ac_channel_distribusi_pp, iv_circle_channel_distribusi_pp,
                            tv_error_channel_distribusi_pp, getContext());
                    break;
                case R.id.ac_produk_pp:
                    if (StaticMethods.isTextWidgetEmpty(ac_produk_pp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_produk_pp.setColorFilter(color);
                    tv_error_produk_pp.setVisibility(View.GONE);
                    break;
                case R.id.ac_sub_produk_pp:
                    if (StaticMethods.isTextWidgetEmpty(ac_sub_produk_pp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_sub_produk_pp.setColorFilter(color);
                    tv_error_sub_produk_pp.setVisibility(View.GONE);
                    break;
                case R.id.ac_jns_spaj:
                    if (StaticMethods.isTextWidgetEmpty(ac_jns_spaj)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jns_spaj.setColorFilter(color);
                    tv_error_jns_spaj.setVisibility(View.GONE);
                    break;
                case R.id.ac_agama_pp:
                    Method_Validator.ValTextWatcher(ac_agama_pp, iv_circle_agama_pp,
                            tv_error_agama_pp, getContext());
                    break;

                case R.id.ac_jabatan_ayah_pp:
                    Method_Validator.ValTextWatcher(ac_jabatan_ayah_pp, iv_circle_jabatan_ayah_pp,
                            tv_error_jabatan_ayah_pp, getContext());
                    break;
                case R.id.ac_klasifikasi_pekerjaan_ayah_pp:
                    Method_Validator.ValTextWatcher(ac_klasifikasi_pekerjaan_ayah_pp, iv_circle_klasifikasi_pekerjaan_ayah_pp,
                            tv_error_klasifikasi_pekerjaan_ayah_pp, getContext());
                    break;
                case R.id.ac_penghasilan_pertahun_ayah_pp:
                    Method_Validator.ValTextWatcher(ac_penghasilan_pertahun_ayah_pp, iv_circle_penghasilan_pertahun_ayah_pp,
                            tv_error_penghasilan_pertahun_ayah_pp, getContext());
                    break;
                case R.id.ac_klasifikasi_pekerjaan_ibu_pp:
                    Method_Validator.ValTextWatcher(ac_klasifikasi_pekerjaan_ibu_pp, iv_circle_klasifikasi_pekerjaan_ibu_pp,
                            tv_error_klasifikasi_pekerjaan_ibu_pp, getContext());
                    break;
                case R.id.ac_jabatan_ibu_pp:
                    Method_Validator.ValTextWatcher(ac_jabatan_ibu_pp, iv_circle_jabatan_ibu_pp,
                            tv_error_jabatan_ibu_pp, getContext());
                    break;
                case R.id.ac_penghasilan_pertahun_ibu_pp:
                    Method_Validator.ValTextWatcher(ac_penghasilan_pertahun_ibu_pp, iv_circle_penghasilan_pertahun_ibu_pp,
                            tv_error_penghasilan_pertahun_ibu_pp, getContext());
                    break;
                case R.id.ac_klasifikasi_pekerjaan_suami_pp:
                    Method_Validator.ValTextWatcher(ac_klasifikasi_pekerjaan_suami_pp, iv_circle_klasifikasi_pekerjaan_suami_pp,
                            tv_error_klasifikasi_pekerjaan_suami_pp, getContext());
                    break;
                case R.id.ac_jabatan_suami_pp:
                    Method_Validator.ValTextWatcher(ac_jabatan_suami_pp, iv_circle_jabatan_suami_pp,
                            tv_error_jabatan_suami_pp, getContext());
                    break;
                case R.id.ac_penghasilan_pertahun_pp:
                    Method_Validator.ValTextWatcher(ac_penghasilan_pertahun_pp, iv_circle_penghasilan_pertahun_pp,
                            tv_error_penghasilan_pertahun_pp, getContext());
                    break;

                case R.id.ac_pendidikan_pp:
                    if (StaticMethods.isTextWidgetEmpty(ac_pendidikan_pp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_pendidikan_pp.setColorFilter(color);
                    tv_error_pendidikan_pp.setVisibility(View.GONE);
                    break;
                case R.id.ac_bukti_identitas_pp:
                    if (StaticMethods.isTextWidgetEmpty(ac_bukti_identitas_pp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_bukti_identitas_pp.setColorFilter(color);
                    tv_error_bukti_identitas_pp.setVisibility(View.GONE);
                    break;
                case R.id.ac_status_pp:
                    if (StaticMethods.isTextWidgetEmpty(ac_status_pp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_status_pp.setColorFilter(color);
                    tv_error_status_pp.setVisibility(View.GONE);
                    break;
                case R.id.ac_tagihan_pp:
                    if (StaticMethods.isTextWidgetEmpty(ac_tagihan_pp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_tagihan_pp.setColorFilter(color);
                    tv_error_tagihan_pp.setVisibility(View.GONE);
                    break;
                case R.id.ac_pendapatan_pp:
                    if (StaticMethods.isTextWidgetEmpty(ac_pendapatan_pp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_pendapatan_pp.setColorFilter(color);
                    tv_error_pendapatan_pp.setVisibility(View.GONE);
                    break;
                case R.id.ac_hubungancp_pp:
                    if (StaticMethods.isTextWidgetEmpty(ac_hubungancp_pp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_hubungancp_pp.setColorFilter(color);
                    tv_error_hubungancp_pp.setVisibility(View.GONE);
                    break;
                case R.id.ac_hubungan_pp:
                    if (StaticMethods.isTextWidgetEmpty(ac_hubungan_pp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_hubungan_pp.setColorFilter(color);
                    tv_error_hubungan_pp.setVisibility(View.GONE);
                    break;
                case R.id.ac_warganegara_pp:
                    if (StaticMethods.isTextWidgetEmpty(ac_warganegara_pp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_warganegara_pp.setColorFilter(color);
                    tv_error_warganegara_pp.setVisibility(View.GONE);
                    break;
                case R.id.ac_klasifikasi_pekerjaan_pp:
                    setAdapterPekerjaan(ac_klasifikasi_pekerjaan_pp.getText().toString().trim(), ac_klasifikasi_pekerjaan_pp);
                    validateJobDescription(ac_klasifikasi_pekerjaan_pp.getText().toString().trim(), ac_klasifikasi_pekerjaan_pp);
                    break;
                case R.id.ac_jabatan_pp:
                    if (StaticMethods.isTextWidgetEmpty(ac_jabatan_pp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jabatan_pp.setColorFilter(color);
                    tv_error_jabatan_pp.setVisibility(View.GONE);
                    break;
                case R.id.ac_paket_pp:
                    if (StaticMethods.isTextWidgetEmpty(ac_paket_pp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_paket_pp.setColorFilter(color);
                    tv_error_paket_pp.setVisibility(View.GONE);
                    break;
                //EditText
                case R.id.et_namaayah_pp:
                    Method_Validator.ValEditWatcher(et_namaayah_pp, iv_circle_namaayah_pp,
                            tv_error_namaayah_pp, getContext());
                    break;
                case R.id.et_ttl_ayah_pp:
                    Method_Validator.ValEditWatcher(et_ttl_ayah_pp, iv_circle_ttl_ayah_pp,
                            tv_error_ttl_ayah_pp, getContext());
                    break;
                case R.id.et_usia_ayah_pp:
                    Method_Validator.ValEditWatcher(et_usia_ayah_pp, iv_circle_usia_ayah_pp,
                            tv_error_usia_ayah_pp, getContext());
                    break;
                case R.id.et_nama_perusahaan_ayah_pp:
                    Method_Validator.ValEditWatcher(et_nama_perusahaan_ayah_pp, iv_circle_nama_perusahaan_ayah_pp,
                            tv_error_nama_perusahaan_ayah_pp, getContext());
                    break;
                case R.id.et_bidang_usaha_ayah_pp:
                    Method_Validator.ValEditWatcher(et_bidang_usaha_ayah_pp, iv_circle_bidang_usaha_ayah_pp,
                            tv_error_bidang_usaha_ayah_pp, getContext());
                    break;
                case R.id.et_npwpayah_pp:
                    Method_Validator.ValEditWatcher(et_npwpayah_pp, iv_circle_npwpayah_pp,
                            tv_error_npwpayah_pp, getContext());
                    break;
                case R.id.et_namaibu_pp:
                    Method_Validator.ValEditWatcher(et_namaibu_pp, iv_circle_namaibu_pp,
                            tv_error_namaibu_pp, getContext());
                    break;
                case R.id.et_ttl_ibu_pp:
                    Method_Validator.ValEditWatcher(et_ttl_ibu_pp, iv_circle_ttl_ibu_pp,
                            tv_error_ttl_ibu_pp, getContext());
                    break;
                case R.id.et_usia_ibu_pp:
                    Method_Validator.ValEditWatcher(et_usia_ibu_pp, iv_circle_usia_ibu_pp,
                            tv_error_usia_ibu_pp, getContext());
                    break;
                case R.id.et_nama_perusahaan_ibu_pp:
                    Method_Validator.ValEditWatcher(et_nama_perusahaan_ibu_pp, iv_circle_nama_perusahaan_ibu_pp,
                            tv_error_nama_perusahaan_ibu_pp, getContext());
                    break;
                case R.id.et_bidang_usaha_ibu_pp:
                    Method_Validator.ValEditWatcher(et_bidang_usaha_ibu_pp, iv_circle_bidang_usaha_ibu_pp,
                            tv_error_bidang_usaha_ibu_pp, getContext());
                    break;
                case R.id.et_npwpibu_pp:
                    Method_Validator.ValEditWatcher(et_npwpibu_pp, iv_circle_npwpibu_pp,
                            tv_error_npwpibu_pp, getContext());
                    break;
                case R.id.et_namasuami_pp:
                    Method_Validator.ValEditWatcher(et_namasuami_pp, iv_circle_namasuami_pp,
                            tv_error_namasuami_pp, getContext());
                    break;
                case R.id.et_ttl_suami_pp:
                    Method_Validator.ValEditWatcher(et_ttl_suami_pp, iv_circle_ttl_suami_pp,
                            tv_error_ttl_suami_pp, getContext());
                    break;
                case R.id.et_usia_suami_pp:
                    Method_Validator.ValEditWatcher(et_usia_suami_pp, iv_circle_usia_suami_pp,
                            tv_error_usia_suami_pp, getContext());
                    break;
                case R.id.et_nama_perusahaan_suami_pp:
                    Method_Validator.ValEditWatcher(et_nama_perusahaan_suami_pp, iv_circle_nama_perusahaan_suami_pp,
                            tv_error_nama_perusahaan_suami_pp, getContext());
                    break;
                case R.id.et_bidang_usaha_suami_pp:
                    Method_Validator.ValEditWatcher(et_bidang_usaha_suami_pp, iv_circle_bidang_usaha_suami_pp,
                            tv_error_bidang_usaha_suami_pp, getContext());
                    break;
                case R.id.et_npwpsuami_pp:
                    Method_Validator.ValEditWatcher(et_npwpsuami_pp, iv_circle_npwpsuami_pp,
                            tv_error_npwpsuami_pp, getContext());
                    break;


                case R.id.et_no_proposal_pp:
                    Method_Validator.ValEditWatcher(et_no_proposal_pp, iv_circle_no_proposal_pp,
                            tv_error_no_proposal_pp, getContext());
                    break;
                case R.id.et_no_ciff_pp:
                    Method_Validator.ValEditWatcher(et_no_ciff_pp, iv_circle_no_ciff_pp,
                            tv_error_no_ciff_pp, getContext());
                    break;
                case R.id.et_nama_pp:
                    Method_Validator.ValEditWatcher(et_nama_pp, iv_circle_nama_pp,
                            tv_error_nama_pp, getContext());
                    break;
                case R.id.et_tempat_pp:
                    Method_Validator.ValEditWatcher(et_tempat_pp, iv_circle_tempat_pp,
                            tv_error_tempat_pp, getContext());
                    break;
                case R.id.et_ttl_pp:
                    Method_Validator.ValEditWatcher(et_ttl_pp, iv_circle_ttl_pp,
                            tv_error_ttl_pp, getContext());
                    break;
                case R.id.et_usia_pp:
                    Method_Validator.ValEditWatcher(et_usia_pp, iv_circle_usia_pp,
                            tv_error_usia_pp, getContext());
                    break;
                case R.id.et_ibu_pp:
                    Method_Validator.ValEditWatcher(et_ibu_pp, iv_circle_ibu_pp,
                            tv_error_ibu_pp, getContext());
                    break;
                case R.id.et_nomor_bukti_pp:
                    Method_Validator.ValEditWatcher(et_nomor_bukti_pp, iv_circle_nomor_bukti_pp,
                            tv_error_nomor_bukti_pp, getContext());
                    break;
                case R.id.et_tgl_berlaku_pp:
                    Method_Validator.ValEditWatcher(et_tgl_berlaku_pp, iv_circle_tgl_berlaku_pp,
                            tv_error_tgl_berlaku_pp, getContext());
                    break;
                case R.id.et_nama_perusahaan_pp:
                    Method_Validator.ValEditWatcher(et_nama_perusahaan_pp, iv_circle_nama_perusahaan_pp,
                            tv_error_nama_perusahaan_pp, getContext());
                    break;
                case R.id.et_uraikan_tugas_pp:
                    Method_Validator.ValEditWatcher(et_uraikan_tugas_pp, iv_circle_uraikan_tugas_pp,
                            tv_error_uraikan_tugas_pp, getContext());
                    break;
            }
        }
    }

    private void loadview() {
        me.getUsulanAsuransiModel().setKd_produk_ua(produk);
        me.getUsulanAsuransiModel().setSub_produk_ua(nm_produk);
        me.getUsulanAsuransiModel().setJenis_produk_ua(kd_jns_produk);
        me.getModelID().setJns_spaj(jnsspaj);

//        me.getPemegangPolisModel().setva
        me.getPemegangPolisModel().setNama_pp(et_nama_pp.getText().toString());
        me.getPemegangPolisModel().setGelar_pp(et_gelar_pp.getText().toString());
        me.getPemegangPolisModel().setNama_ibu_pp(et_ibu_pp.getText().toString());
        me.getPemegangPolisModel().setBukti_identitas_pp(bukti_pp);
        me.getPemegangPolisModel().setBukti_identitas_lain_pp(et_teks_bukti_lain_pp.getText().toString());
        me.getPemegangPolisModel().setNo_identitas_pp(et_nomor_bukti_pp.getText().toString());
        me.getPemegangPolisModel().setTgl_berlaku_pp(et_tgl_berlaku_pp.getText().toString());
        me.getPemegangPolisModel().setWarga_negara_pp(wn_pp);
        me.getPemegangPolisModel().setTempat_pp(et_tempat_pp.getText().toString());
        me.getPemegangPolisModel().setTtl_pp(et_ttl_pp.getText().toString());
        me.getPemegangPolisModel().setUsia_pp(et_usia_pp.getText().toString());
        me.getPemegangPolisModel().setJekel_pp(jekel);
        me.getPemegangPolisModel().setStatus_pp(stat_pp);
        me.getPemegangPolisModel().setAgama_pp(agama);
        me.getPemegangPolisModel().setAgama_lain_pp(et_teks_agama_lain_pp.getText().toString());
        me.getPemegangPolisModel().setPendidikan_pp(pend_pp);
        me.getPemegangPolisModel().setKlasifikasi_pekerjaan_pp(ac_klasifikasi_pekerjaan_pp.getText().toString());
        me.getPemegangPolisModel().setUraian_pekerjaan_pp(et_uraikan_tugas_pp.getText().toString());
        me.getPemegangPolisModel().setJabatan_klasifikasi_pp(str_jabatan_klasifikasi);
        me.getPemegangPolisModel().setHubungan_pp(hub);
        me.getPemegangPolisModel().setTagihan_pp(tagihan);
        me.getPemegangPolisModel().setGreencard_pp(greencard);
        me.getPemegangPolisModel().setAlias_pp(et_alias_pp.getText().toString());
        me.getPemegangPolisModel().setNm_perusahaan_pp(et_nama_perusahaan_pp.getText().toString());
        me.getPemegangPolisModel().setNpwp_pp(et_npwp_pp.getText().toString());
        me.getPemegangPolisModel().setTagihan_rutin_pp(str_tagihanrtn);
        me.getPemegangPolisModel().setKrm_polis_pp(str_krm_polis);
        me.getPemegangPolisModel().setSuamirt_pp(et_namasuami_pp.getText().toString());
        me.getPemegangPolisModel().setTtlsuami_rt_pp(et_ttl_suami_pp.getText().toString());
        me.getPemegangPolisModel().setUsiasuami_rt_pp(et_usia_suami_pp.getText().toString());
        me.getPemegangPolisModel().setPekerjaansuami_rt_pp(ac_klasifikasi_pekerjaan_suami_pp.getText().toString());
        me.getPemegangPolisModel().setJabatansuami_rt_pp(str_jabatansuami_rt);
        me.getPemegangPolisModel().setPerusahaansuami_rt_pp(et_nama_perusahaan_suami_pp.getText().toString());
        me.getPemegangPolisModel().setBidusaha_suamirt_pp(et_bidang_usaha_suami_pp.getText().toString());
        me.getPemegangPolisModel().setNpwp_suamirt_pp(et_npwpsuami_pp.getText().toString());
        me.getPemegangPolisModel().setPenghasilan_suamithn_pp(str_penghasilan_suamithn_pp);
        me.getPemegangPolisModel().setAyah_pp(et_namaayah_pp.getText().toString());
        me.getPemegangPolisModel().setTtl_ayah_pp(et_ttl_ayah_pp.getText().toString());
        me.getPemegangPolisModel().setUsia_ayah_pp(et_usia_ayah_pp.getText().toString());
        me.getPemegangPolisModel().setPekerjaan_ayah_pp(ac_klasifikasi_pekerjaan_ayah_pp.getText().toString());
        me.getPemegangPolisModel().setJabatan_ayah_pp(str_jabatan_ayah);
        me.getPemegangPolisModel().setPerusahaan_ayah_pp(et_nama_perusahaan_ayah_pp.getText().toString());
        me.getPemegangPolisModel().setBidusaha_ayah_pp(et_bidang_usaha_ayah_pp.getText().toString());
        me.getPemegangPolisModel().setNpwp_ayah_pp(et_npwpayah_pp.getText().toString());
        me.getPemegangPolisModel().setPenghasilan_ayah_pp(str_penghasilan_ayah_pp);
        me.getPemegangPolisModel().setNm_ibu_pp(et_namaibu_pp.getText().toString());
        me.getPemegangPolisModel().setTtl_ibu_pp(et_ttl_ibu_pp.getText().toString());
        me.getPemegangPolisModel().setUsia_ibu_pp(et_usia_ibu_pp.getText().toString());
        me.getPemegangPolisModel().setPekerjaan_ibu_pp(ac_klasifikasi_pekerjaan_ibu_pp.getText().toString());
        me.getPemegangPolisModel().setJabatan_ibu_pp(str_jabatan_ibu);
        me.getPemegangPolisModel().setPerusahaan_ibu_pp(et_nama_perusahaan_ibu_pp.getText().toString());
        me.getPemegangPolisModel().setBidusaha_ibu_pp(et_bidang_usaha_ibu_pp.getText().toString());
        me.getPemegangPolisModel().setNpwp_ibu_pp(et_npwpibu_pp.getText().toString());
        me.getPemegangPolisModel().setPenghasilan_ibu_pp(str_penghasilan_ibu_pp);
        me.getPemegangPolisModel().setPendapatan_pp(str_pendapatan);
        me.getPemegangPolisModel().setHubungancp_pp(cp_pp);
        me.getPemegangPolisModel().setNo_ciff_pp(et_no_ciff_pp.getText().toString());
        me.getPemegangPolisModel().setD_gaji_pp(d_gaji_pp);
        me.getPemegangPolisModel().setD_tabungan_pp(d_tabungan_pp);
        me.getPemegangPolisModel().setD_warisan_pp(d_warisan_pp);
        me.getPemegangPolisModel().setD_hibah_pp(d_hibah_pp);
        me.getPemegangPolisModel().setD_lainnya_pp(d_lainnya_pp);
        me.getPemegangPolisModel().setEdit_d_lainnya_pp(et_editd_lainnya_pp.getText().toString());
        me.getPemegangPolisModel().setT_proteksi_pp(t_proteksi_pp);
        me.getPemegangPolisModel().setT_inves_pp(t_inves_pp);
        me.getPemegangPolisModel().setT_lainnya_pp(t_lainnya_pp);
        me.getPemegangPolisModel().setEdit_t_lainnya(et_editt_lainnya_pp.getText().toString());

        me.getCalonPembayarPremiModel().setInt_ket_cp(cp_pp);
        if (cp_pp != 40) {
            me.getCalonPembayarPremiModel().setInt_spin_hub_dgppphk3_cp(cp_pp);
        }
        E_MainActivity.e_bundle.putParcelable(getString(R.string.modelespaj), me);
    }


    private void restore() {
        //editText
        if (JENIS_LOGIN == 9 && JENIS_LOGIN_BC == 2) {
            cl_no_ciff_pp.setVisibility(View.VISIBLE);
            et_no_ciff_pp.setText(me.getPemegangPolisModel().getNo_ciff_pp());
        } else {
            cl_no_ciff_pp.setVisibility(View.GONE);
            iv_circle_no_ciff_pp.setVisibility(View.GONE);
        }
        et_nama_pp.setText(me.getPemegangPolisModel().getNama_pp());
        et_gelar_pp.setText(me.getPemegangPolisModel().getGelar_pp());
        et_ibu_pp.setText(me.getPemegangPolisModel().getNama_ibu_pp());
        et_teks_bukti_lain_pp.setText(me.getPemegangPolisModel().getBukti_identitas_lain_pp());
        et_nomor_bukti_pp.setText(me.getPemegangPolisModel().getNo_identitas_pp());
        et_tgl_berlaku_pp.setText(me.getPemegangPolisModel().getTgl_berlaku_pp());
        et_tempat_pp.setText(me.getPemegangPolisModel().getTempat_pp());
        et_ttl_pp.setText(me.getPemegangPolisModel().getTtl_pp());
        et_usia_pp.setText(me.getPemegangPolisModel().getUsia_pp());
        et_teks_agama_lain_pp.setText(me.getPemegangPolisModel().getAgama_lain_pp());
        et_alias_pp.setText(me.getPemegangPolisModel().getAlias_pp());
        et_nama_perusahaan_pp.setText(me.getPemegangPolisModel().getNm_perusahaan_pp());
        et_npwp_pp.setText(me.getPemegangPolisModel().getNpwp_pp());
        et_namasuami_pp.setText(me.getPemegangPolisModel().getSuamirt_pp());
        et_ttl_suami_pp.setText(me.getPemegangPolisModel().getTtlsuami_rt_pp());
        et_usia_suami_pp.setText(me.getPemegangPolisModel().getUsiasuami_rt_pp());
        et_nama_perusahaan_suami_pp.setText(me.getPemegangPolisModel().getPerusahaansuami_rt_pp());
        et_bidang_usaha_suami_pp.setText(me.getPemegangPolisModel().getBidusaha_suamirt_pp());
        et_npwpsuami_pp.setText(me.getPemegangPolisModel().getNpwp_suamirt_pp());
        et_namaayah_pp.setText(me.getPemegangPolisModel().getAyah_pp());
        et_ttl_ayah_pp.setText(me.getPemegangPolisModel().getTtl_ayah_pp());
        et_usia_ayah_pp.setText(me.getPemegangPolisModel().getUsia_ayah_pp());
        et_nama_perusahaan_ayah_pp.setText(me.getPemegangPolisModel().getPerusahaan_ayah_pp());
        et_bidang_usaha_ayah_pp.setText(me.getPemegangPolisModel().getBidusaha_ayah_pp());
        et_npwpayah_pp.setText(me.getPemegangPolisModel().getNpwp_ayah_pp());
        et_namaibu_pp.setText(me.getPemegangPolisModel().getNm_ibu_pp());
        et_ttl_ibu_pp.setText(me.getPemegangPolisModel().getTtl_ibu_pp());
        et_usia_ibu_pp.setText(me.getPemegangPolisModel().getUsia_ibu_pp());
        et_nama_perusahaan_ibu_pp.setText(me.getPemegangPolisModel().getPerusahaan_ibu_pp());
        et_bidang_usaha_ibu_pp.setText(me.getPemegangPolisModel().getBidusaha_ibu_pp());
        et_npwpibu_pp.setText(me.getPemegangPolisModel().getNpwp_ibu_pp());
        et_uraikan_tugas_pp.setText(me.getPemegangPolisModel().getUraian_pekerjaan_pp());

        et_editd_lainnya_pp.setText(me.getPemegangPolisModel().getEdit_d_lainnya_pp());
        et_editt_lainnya_pp.setText(me.getPemegangPolisModel().getEdit_t_lainnya());
        //dropdown
        ListDropDownSPAJ = new E_Select(getContext()).getLstChannelDist(JENIS_LOGIN, JENIS_LOGIN_BC, KODE_REG1, KODE_REG2);
        kd_jns_produk = ListDropDownSPAJ.get(0).getId();
        ac_channel_distribusi_pp.setText(ListDropDownSPAJ.get(0).getValue());

        produk = me.getUsulanAsuransiModel().getKd_produk_ua();
        ListDropInt = new P_Select(getContext()).selectListRencana(groupId);
        ac_produk_pp.setText(MethodSupport.getAdapterPosition(ListDropInt, produk));
        setAdapterSubProduk();

        nm_produk = me.getUsulanAsuransiModel().getSub_produk_ua();
        ListDropInt = new P_Select(getContext()).selectListRencanaSub(groupId, produk);
        ac_sub_produk_pp.setText(MethodSupport.getSubAdapterPosition(ListDropInt, nm_produk));

        paket = me.getUsulanAsuransiModel().getPaket_ua();
        ListDropInt = new P_Select(getContext()).selectListPacket(groupId, me.getUsulanAsuransiModel().getKd_produk_ua());
        ac_paket_pp.setText(MethodSupport.getSubAdapterPacket(ListDropInt, paket));

        if (ROLE_ID == 1) {
            ac_paket_pp.setVisibility(View.GONE);
            iv_circle_paket_pp.setVisibility(View.GONE);
        }

        if (new E_Select(getContext()).getFlagPaket(produk, nm_produk) == 0) {
            ac_paket_pp.setVisibility(View.GONE);
            iv_circle_paket_pp.setVisibility(View.GONE);
        }

        ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_jenis_spaj, 0);
        //hardcode sementara untuk jnsspaj
        if (produk == 190 || produk == 200) {
            if (groupId == 40) {
                jnsspaj = new E_Select(getContext()).SelectJnsSPAJ(produk);
                ac_jns_spaj.setText("GIO");
            } else {
                jnsspaj = 3;
                ac_jns_spaj.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, jnsspaj));

            }
        } else {
            jnsspaj = new E_Select(getContext()).SelectJnsSPAJ(produk);
            if (jnsspaj == 4) {
                if (produk == 190 || produk == 212 || produk == 200) {
                    ac_jns_spaj.setText("GIO");
                } else {
                    ac_jns_spaj.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, jnsspaj));
                }
            } else {
                ac_jns_spaj.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, jnsspaj));
            }
        }

        MethodSupport.active_view(0, ac_jns_spaj);
        if (jnsspaj != 2) {
            cl_alias_pp.setVisibility(View.GONE);
            cl_form_korespondensi_polis_pp.setVisibility(View.GONE);
        }

        bukti_pp = me.getPemegangPolisModel().getBukti_identitas_pp();
        ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_identity, 0);
        if (me.getDetilAgenModel().getGroup_id_da() == 40) {
            ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_identity, 9);
        } else {
            ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_identity, 0);
        }
        ac_bukti_identitas_pp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, bukti_pp));
        val_bukti(bukti_pp);

        wn_pp = me.getPemegangPolisModel().getWarga_negara_pp();
        ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_warganegara, 0);
        ac_warganegara_pp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, wn_pp));

        stat_pp = me.getPemegangPolisModel().getStatus_pp();
        ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_marital, 0);
        ac_status_pp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, stat_pp));

        agama = me.getPemegangPolisModel().getAgama_pp();
        ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_agama, 0);
        ac_agama_pp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, agama));
        val_agama(agama);

        pend_pp = me.getPemegangPolisModel().getPendidikan_pp();
        ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_pendidikan, 0);
        ac_pendidikan_pp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, pend_pp));

        str_jabatan_klasifikasi = me.getPemegangPolisModel().getJabatan_klasifikasi_pp();
        ListDropDownString = new E_Select(getContext()).getLstJabatan();
        ac_jabatan_pp.setText(MethodSupport.getAdapterPositionString(ListDropDownString, str_jabatan_klasifikasi));

        hub = me.getPemegangPolisModel().getHubungan_pp();
        ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_relasi, 1);
        ac_hubungan_pp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, hub));

        str_tagihanrtn = me.getPemegangPolisModel().getTagihan_rutin_pp();

        str_krm_polis = me.getPemegangPolisModel().getKrm_polis_pp();
        ac_klasifikasi_pekerjaan_suami_pp.setText(me.getPemegangPolisModel().getPekerjaansuami_rt_pp());

        str_jabatansuami_rt = me.getPemegangPolisModel().getJabatansuami_rt_pp();
        ListDropDownString = new E_Select(getContext()).getLstJabatan();
        ac_jabatan_suami_pp.setText(MethodSupport.getAdapterPositionString(ListDropDownString, str_jabatansuami_rt));

        str_penghasilan_suamithn_pp = me.getPemegangPolisModel().getPenghasilan_suamithn_pp();
        ac_penghasilan_pertahun_pp.setText(str_penghasilan_suamithn_pp);

        //masih belum
        ac_klasifikasi_pekerjaan_ayah_pp.setText(me.getPemegangPolisModel().getPekerjaan_ayah_pp());

        str_jabatan_ayah = me.getPemegangPolisModel().getJabatan_ayah_pp();
        ListDropDownString = new E_Select(getContext()).getLstJabatan();
        ac_jabatan_ayah_pp.setText(MethodSupport.getAdapterPositionString(ListDropDownString, str_jabatan_ayah));

        str_penghasilan_ayah_pp = me.getPemegangPolisModel().getPenghasilan_ayah_pp();
        ac_penghasilan_pertahun_ayah_pp.setText(str_penghasilan_ayah_pp);

        ac_klasifikasi_pekerjaan_ibu_pp.setText(me.getPemegangPolisModel().getPekerjaan_ibu_pp());

        str_jabatan_ibu = me.getPemegangPolisModel().getJabatan_ibu_pp();
        ListDropDownString = new E_Select(getContext()).getLstJabatan();
        ac_jabatan_ibu_pp.setText(MethodSupport.getAdapterPositionString(ListDropDownString, str_jabatan_ibu));

        str_penghasilan_ibu_pp = me.getPemegangPolisModel().getPenghasilan_ibu_pp();
        ac_penghasilan_pertahun_ibu_pp.setText(me.getPemegangPolisModel().getPenghasilan_ibu_pp());

        str_pendapatan = me.getPemegangPolisModel().getPendapatan_pp();
        Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.penghasilan));
        ac_pendapatan_pp.setText(str_pendapatan);
//        ac_pendapatan_pp.setText(me.getTertanggungModel().getPenghasilan_thn_tt());

        // masih belum
        cp_pp = me.getPemegangPolisModel().getHubungancp_pp();
        ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_relasi, 2);
        ac_hubungancp_pp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, cp_pp));

        pekerjaan = me.getPemegangPolisModel().getKlasifikasi_pekerjaan_pp();
        ac_klasifikasi_pekerjaan_pp.setText(pekerjaan);
        val_pekerjaan(pekerjaan);

        tagihan = me.getPemegangPolisModel().getTagihan_pp();
        ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_alamat_tagihan, 0);
        ac_tagihan_pp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, tagihan));
        val_tagihan(tagihan);

        //checkbox
        d_gaji_pp = me.getPemegangPolisModel().getD_gaji_pp();
        if (!d_gaji_pp.equals("")) {
            cb_checkd_gaji_pp.setChecked(true);
        }
        d_tabungan_pp = me.getPemegangPolisModel().getD_tabungan_pp();
        if (!d_tabungan_pp.equals("")) {
            cb_checkd_tabungan_pp.setChecked(true);
        }
        d_warisan_pp = me.getPemegangPolisModel().getD_warisan_pp();
        if (!d_warisan_pp.equals("")) {
            cb_checkd_warisan_pp.setChecked(true);
        }
        d_hibah_pp = me.getPemegangPolisModel().getD_hibah_pp();
        if (!d_hibah_pp.equals("")) {
            cb_checkd_hibah_pp.setChecked(true);
        }
        d_lainnya_pp = me.getPemegangPolisModel().getD_lainnya_pp();
        if (!d_lainnya_pp.equals("")) {
            cb_checkd_lainnya_pp.setChecked(true);
        }
        et_editd_lainnya_pp.setText(me.getPemegangPolisModel().getEdit_d_lainnya_pp());
        t_proteksi_pp = me.getPemegangPolisModel().getT_proteksi_pp();
        if (!t_proteksi_pp.equals("")) {
            cb_checkt_proteksi_pp.setChecked(true);
        }
        t_inves_pp = me.getPemegangPolisModel().getT_inves_pp();
        if (!t_inves_pp.equals("")) {
            cb_checkt_inves_pp.setChecked(true);
        }
        t_lainnya_pp = me.getPemegangPolisModel().getT_lainnya_pp();
        if (!t_lainnya_pp.equals("")) {
            cb_checkt_lainnya_pp.setChecked(true);
        }
        //radioGroup
        jekel = me.getPemegangPolisModel().getJekel_pp();
        MethodSupport.OnsetTwoRadio(rg_jekel_pp, jekel);
        iv_circle_jekel_pp.setColorFilter(ContextCompat.getColor(getContext(), R.color.green));

        greencard = me.getPemegangPolisModel().getGreencard_pp();
        MethodSupport.OnsetTwoRadio(rg_green_card_pp, greencard);
        //kalau ada nilai proposal
        if (!me.getModelID().getProposal_tab().equals("")) {
            onDeactiveView();
            et_no_proposal_pp.setText(me.getModelID().getProposal_tab());
        }

        updateAlamatRmh();
        updateAlamatKantor();
        updateAlamatTinggal();
        UpdateHP();
        if (me.getModelID().getFlag_aktif() == 1) {
            MethodSupport.disable(false, cl_child_pp);
        }
        if (me.getValidation() == false) {
            Validation();
        }
        //di delete untuk next version
        MethodSupport.active_view(1, et_tgl_berlaku_pp);
    }
//      ac_tagihan_pp (Clear)
//      str_jabatan_klasifikasi (Clear)
//      str_penghasilan_suamithn_pp (Not )
//      str_penghasilan_ibu_pp (Clear)
//      str_pendapatan (Clear)


    private void setAdapterProduk() {
        ArrayList<DropboxModel> dRencanas = new P_Select(getContext()).selectListRencana(groupId);
        ArrayAdapter<DropboxModel> mRencanaAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, dRencanas);
        ac_produk_pp.setAdapter(mRencanaAdapter);
    }

    private void setAdapterSubProduk() {
        ArrayList<DropboxModel> dRencanaSubs = new P_Select(getContext()).selectListRencanaSub(groupId, produk);
        ArrayAdapter<DropboxModel> mRencanaSubAdapter = new ArrayAdapter<DropboxModel>(getContext(),
                android.R.layout.simple_dropdown_item_1line, dRencanaSubs);
        ac_sub_produk_pp.setAdapter(mRencanaSubAdapter);
    }

    private void setAdapterJabatan() {
        ArrayList<ModelDropDownString> Dropdown = new E_Select(getContext()).getLstJabatan();
        ArrayAdapter<ModelDropDownString> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_jabatan_pp.setAdapter(Adapter);
        ac_jabatan_ayah_pp.setAdapter(Adapter);
        ac_jabatan_ibu_pp.setAdapter(Adapter);
        ac_jabatan_suami_pp.setAdapter(Adapter);
    }

    private void setAdapterPekerjaan(String cari_kerja, AutoCompleteTextView ac_kerja) {
        ArrayList<ModelDropdownInt> Dropdown = new E_Select(getActivity()).getLst_Pekerjaan(cari_kerja);
        ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_kerja.setAdapter(Adapter);
    }

    /**
     * To determine is user selected job description is from dropdown or not
     * If user type the job description, the input will not be valid
     * User will have to select from the dropdown
     *
     * @param searchQuery
     * @param ac_kerja
     * @return
     */
    private void validateJobDescription(String searchQuery, AutoCompleteTextView ac_kerja) {
        //Check if job desc search query empty
        if (!TextUtils.isEmpty(searchQuery)) {
            ac_kerja.setError(null);
            iv_circle_klasifikasi_pekerjaan_pp.setColorFilter(ContextCompat.getColor(getActivity(), R.color.orange));

            //Search from DB
            ArrayList<ModelDropdownInt> result = new E_Select(getActivity()).getLst_Pekerjaan(searchQuery);
            if (result.isEmpty()) {
                ac_kerja.setError(getString(R.string.err_msg_jobdesc_not_found_on_the_list));
            } else {
                ac_kerja.setError(null);
                ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, result);
                ac_kerja.setAdapter(Adapter);
            }
            // To make sure user only pick job desc from dropdown so we make it false
            isJobDescriptionValid = false;

        } else {
            ac_kerja.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
            iv_circle_klasifikasi_pekerjaan_pp.setColorFilter(ContextCompat.getColor(getActivity(), R.color.red));

            // To make sure user only pick job desc from dropdown so we make it false
            isJobDescriptionValid = false;
        }
    }

    /**
     * Check whether user previously selected job description is on the suggestion list
     * Invoked when user go to this process from the previous step
     *
     * @param selectedJobDesc
     * @param ac_kerja
     * @return
     */
    private void checkForSelectedJobDescriptionIsOnTheList(String selectedJobDesc, AutoCompleteTextView ac_kerja) {
        //Search from DB
        ArrayList<ModelDropdownInt> result = new E_Select(getActivity()).getLst_Pekerjaan(selectedJobDesc);
        if (result.isEmpty()) {
            ac_kerja.setError(getString(R.string.err_msg_jobdesc_not_found_on_the_list));
            isJobDescriptionValid = false;
        } else {
            ac_kerja.setError(null);
            iv_circle_klasifikasi_pekerjaan_pp.setColorFilter(ContextCompat.getColor(getActivity(), R.color.green));
            ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, result);
            ac_kerja.setAdapter(Adapter);
            isJobDescriptionValid = true;
        }
    }

    private void setAdapterChannelDist() {
        ArrayList<ModelDropdownInt> Dropdown = new E_Select(getContext()).getLstChannelDist(JENIS_LOGIN, JENIS_LOGIN_BC,
                KODE_REG1, KODE_REG2);
        ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_channel_distribusi_pp.setAdapter(Adapter);
    }

    private void setAdapterPenghasilan() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.penghasilan));
        ac_pendapatan_pp.setAdapter(Adapter);
        ac_penghasilan_pertahun_pp.setAdapter(Adapter);
        ac_penghasilan_pertahun_ayah_pp.setAdapter(Adapter);
        ac_penghasilan_pertahun_ibu_pp.setAdapter(Adapter);
    }

    private boolean Validation() {
        boolean val = true;
        int color = ContextCompat.getColor(getContext(), R.color.red);

        //RadioGroup

        if (!Method_Validator.onEmptyRadio(rg_green_card_pp, iv_circle_green_card_pp, tv_error_green_card_pp, cl_green_card_pp, getContext())) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, rg_green_card_pp, tv_error_green_card_pp, iv_circle_green_card_pp, color, getContext(), getString(R.string.error_field_required));
        }

        if (!Method_Validator.onEmptyRadio(rg_jekel_pp, iv_circle_jekel_pp, tv_error_jekel_pp, cl_jekel_pp, getContext())) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, rg_jekel_pp, tv_error_jekel_pp, iv_circle_jekel_pp, color, getContext(), getString(R.string.error_field_required));
        }

        //EditText
//        if (et_no_ciff_pp.isShown() && et_no_ciff_pp.getText().toString().trim().length() == 0) {
//            val = false;
//            tv_error_no_ciff_pp.setVisibility(View.VISIBLE);
//            tv_error_no_ciff_pp.setText(getString(R.string.error_field_required));
//            iv_circle_no_ciff_pp.setColorFilter(color);
//        }

        if (!Method_Validator.ValEditRegex(et_gelar_pp.getText().toString())) {
            val = false;
            MethodSupport.onFocusviewNoValidation(scroll_pp, cl_child_pp, et_gelar_pp, tv_error_gelar_pp, color, getContext(), getString(R.string.error_regex));
        }

        if (et_nama_pp.isShown() && et_nama_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nama_pp, tv_error_nama_pp, iv_circle_nama_pp, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(et_nama_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nama_pp, tv_error_nama_pp, iv_circle_nama_pp, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (et_ibu_pp.isShown() && et_ibu_pp.getText().toString().trim().length() == 0) {
            val = false;
//            tv_error_ibu_pp.setVisibility(View.VISIBLE);
//            tv_error_ibu_pp.setText(getString(R.string.error_field_required));
//            iv_circle_ibu_pp.setColorFilter(color);
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_ibu_pp, tv_error_ibu_pp, iv_circle_ibu_pp, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(et_ibu_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_ibu_pp, tv_error_ibu_pp, iv_circle_ibu_pp, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (et_nomor_bukti_pp.isShown() && et_nomor_bukti_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nomor_bukti_pp, tv_error_nomor_bukti_pp, iv_circle_nomor_bukti_pp, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(et_nomor_bukti_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nomor_bukti_pp, tv_error_nomor_bukti_pp, iv_circle_nomor_bukti_pp, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (et_tgl_berlaku_pp.isShown() && et_tgl_berlaku_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_tgl_berlaku_pp, tv_error_tgl_berlaku_pp, iv_circle_tgl_berlaku_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_tgl_berlaku_pp.setVisibility(View.VISIBLE);
//            tv_error_tgl_berlaku_pp.setText(getString(R.string.error_field_required));
//            iv_circle_tgl_berlaku_pp.setColorFilter(color);
        }
        if (et_tempat_pp.isShown() && et_tempat_pp.getText().toString().trim().length() == 0) {
            val = false;
//            tv_error_tempat_pp.setVisibility(View.VISIBLE);
//            tv_error_tempat_pp.setText(getString(R.string.error_field_required));
//            iv_circle_tempat_pp.setColorFilter(color);
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_tempat_pp, tv_error_tempat_pp, iv_circle_tempat_pp, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(et_tempat_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_tempat_pp, tv_error_tempat_pp, iv_circle_tempat_pp, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (et_ttl_pp.isShown() && et_ttl_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_ttl_pp, tv_error_ttl_pp, iv_circle_ttl_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_ttl_pp.setVisibility(View.VISIBLE);
//            tv_error_ttl_pp.setText(getString(R.string.error_field_required));
//            iv_circle_ttl_pp.setColorFilter(color);
        }

        if (et_nama_perusahaan_pp.isShown() && et_nama_perusahaan_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nama_perusahaan_pp, tv_error_nama_perusahaan_pp, iv_circle_nama_perusahaan_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nama_perusahaan_pp.setVisibility(View.VISIBLE);
//            tv_error_nama_perusahaan_pp.setText(getString(R.string.error_field_required));
//            iv_circle_nama_perusahaan_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_nama_perusahaan_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nama_perusahaan_pp, tv_error_nama_perusahaan_pp, iv_circle_nama_perusahaan_pp, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (et_uraikan_tugas_pp.isShown() && et_uraikan_tugas_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_uraikan_tugas_pp, tv_error_uraikan_tugas_pp, iv_circle_uraikan_tugas_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_uraikan_tugas_pp.setVisibility(View.VISIBLE);
//            tv_error_uraikan_tugas_pp.setText(getString(R.string.error_field_required));
//            iv_circle_uraikan_tugas_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_uraikan_tugas_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_uraikan_tugas_pp, tv_error_uraikan_tugas_pp, iv_circle_uraikan_tugas_pp, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (et_namaayah_pp.isShown() && et_namaayah_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_namaayah_pp, tv_error_namaayah_pp, iv_circle_namaayah_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_namaayah_pp.setVisibility(View.VISIBLE);
//            tv_error_namaayah_pp.setText(getString(R.string.error_field_required));
//            iv_circle_namaayah_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_namaayah_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_namaayah_pp, tv_error_namaayah_pp, iv_circle_namaayah_pp, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (et_ttl_ayah_pp.isShown() && et_ttl_ayah_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_ttl_ayah_pp, tv_error_ttl_ayah_pp, iv_circle_ttl_ayah_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_ttl_ayah_pp.setVisibility(View.VISIBLE);
//            tv_error_ttl_ayah_pp.setText(getString(R.string.error_field_required));
//            iv_circle_ttl_ayah_pp.setColorFilter(color);
        }

        if (et_usia_ayah_pp.isShown() && et_usia_ayah_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_usia_ayah_pp, tv_error_usia_ayah_pp, iv_circle_usia_ayah_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_usia_ayah_pp.setVisibility(View.VISIBLE);
//            tv_error_usia_ayah_pp.setText(getString(R.string.error_field_required));
//            iv_circle_usia_ayah_pp.setColorFilter(color);
        }
        if (et_nama_perusahaan_ayah_pp.isShown() && et_nama_perusahaan_ayah_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nama_perusahaan_ayah_pp, tv_error_nama_perusahaan_ayah_pp, iv_circle_nama_perusahaan_ayah_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nama_perusahaan_ayah_pp.setVisibility(View.VISIBLE);
//            tv_error_nama_perusahaan_ayah_pp.setText(getString(R.string.error_field_required));
//            iv_circle_nama_perusahaan_ayah_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_nama_perusahaan_ayah_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nama_perusahaan_ayah_pp, tv_error_nama_perusahaan_ayah_pp, iv_circle_nama_perusahaan_ayah_pp, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_bidang_usaha_ayah_pp.isShown() && et_bidang_usaha_ayah_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_bidang_usaha_ayah_pp, tv_error_bidang_usaha_ayah_pp, iv_circle_bidang_usaha_ayah_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_bidang_usaha_ayah_pp.setVisibility(View.VISIBLE);
//            tv_error_bidang_usaha_ayah_pp.setText(getString(R.string.error_field_required));
//            iv_circle_bidang_usaha_ayah_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_bidang_usaha_ayah_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_bidang_usaha_ayah_pp, tv_error_bidang_usaha_ayah_pp, iv_circle_bidang_usaha_ayah_pp, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_npwpayah_pp.isShown() && et_npwpayah_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_npwpayah_pp, tv_error_npwpayah_pp, iv_circle_npwpayah_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_npwpayah_pp.setVisibility(View.VISIBLE);
//            tv_error_npwpayah_pp.setText(getString(R.string.error_field_required));
//            iv_circle_npwpayah_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_npwpayah_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_npwpayah_pp, tv_error_npwpayah_pp, iv_circle_npwpayah_pp, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_namaibu_pp.isShown() && et_namaibu_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_namaibu_pp, tv_error_namaibu_pp, iv_circle_namaibu_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_namaibu_pp.setVisibility(View.VISIBLE);
//            tv_error_namaibu_pp.setText(getString(R.string.error_field_required));
//            iv_circle_namaibu_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_namaibu_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_namaibu_pp, tv_error_namaibu_pp, iv_circle_namaibu_pp, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_ttl_ibu_pp.isShown() && et_ttl_ibu_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_ttl_ibu_pp, tv_error_ttl_ibu_pp, iv_circle_ttl_ibu_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_ttl_ibu_pp.setVisibility(View.VISIBLE);
//            tv_error_ttl_ibu_pp.setText(getString(R.string.error_field_required));
//            iv_circle_ttl_ibu_pp.setColorFilter(color);
        }
        if (et_usia_ibu_pp.isShown() && et_usia_ibu_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_usia_ibu_pp, tv_error_usia_ibu_pp, iv_circle_usia_ibu_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_usia_ibu_pp.setVisibility(View.VISIBLE);
//            tv_error_usia_ibu_pp.setText(getString(R.string.error_field_required));
//            iv_circle_usia_ibu_pp.setColorFilter(color);
        }
        if (et_nama_perusahaan_ibu_pp.isShown() && et_nama_perusahaan_ibu_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nama_perusahaan_ibu_pp, tv_error_nama_perusahaan_ibu_pp, iv_circle_nama_perusahaan_ibu_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nama_perusahaan_ibu_pp.setVisibility(View.VISIBLE);
//            tv_error_nama_perusahaan_ibu_pp.setText(getString(R.string.error_field_required));
//            iv_circle_nama_perusahaan_ibu_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_nama_perusahaan_ibu_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nama_perusahaan_ibu_pp, tv_error_nama_perusahaan_ibu_pp, iv_circle_nama_perusahaan_ibu_pp, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_bidang_usaha_ibu_pp.isShown() && et_bidang_usaha_ibu_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_bidang_usaha_ibu_pp, tv_error_bidang_usaha_ibu_pp, iv_circle_bidang_usaha_ibu_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_bidang_usaha_ibu_pp.setVisibility(View.VISIBLE);
//            tv_error_bidang_usaha_ibu_pp.setText(getString(R.string.error_field_required));
//            iv_circle_bidang_usaha_ibu_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_bidang_usaha_ibu_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_bidang_usaha_ibu_pp, tv_error_bidang_usaha_ibu_pp, iv_circle_bidang_usaha_ibu_pp, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_npwpibu_pp.isShown() && et_npwpibu_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_npwpibu_pp, tv_error_npwpibu_pp, iv_circle_npwpibu_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_npwpibu_pp.setVisibility(View.VISIBLE);
//            tv_error_npwpibu_pp.setText(getString(R.string.error_field_required));
//            iv_circle_npwpibu_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_npwpibu_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_npwpibu_pp, tv_error_npwpibu_pp, iv_circle_npwpibu_pp, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_namasuami_pp.isShown() && et_namasuami_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_namasuami_pp, tv_error_namasuami_pp, iv_circle_namasuami_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_namasuami_pp.setVisibility(View.VISIBLE);
//            tv_error_namasuami_pp.setText(getString(R.string.error_field_required));
//            iv_circle_namasuami_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_namasuami_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_namasuami_pp, tv_error_namasuami_pp, iv_circle_namasuami_pp, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_ttl_suami_pp.isShown() && et_ttl_suami_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_ttl_suami_pp, tv_error_ttl_suami_pp, iv_circle_ttl_suami_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_ttl_suami_pp.setVisibility(View.VISIBLE);
//            tv_error_ttl_suami_pp.setText(getString(R.string.error_field_required));
//            iv_circle_ttl_suami_pp.setColorFilter(color);
        }
        if (et_usia_suami_pp.isShown() && et_usia_suami_pp.getText().toString().trim().length() == 0) {
            val = false;
            tv_error_usia_suami_pp.setVisibility(View.VISIBLE);
            tv_error_usia_suami_pp.setText(getString(R.string.error_field_required));
            iv_circle_usia_suami_pp.setColorFilter(color);
        }
        if (et_nama_perusahaan_suami_pp.isShown() && et_nama_perusahaan_suami_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nama_perusahaan_suami_pp, tv_error_nama_perusahaan_suami_pp, iv_circle_nama_perusahaan_suami_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nama_perusahaan_suami_pp.setVisibility(View.VISIBLE);
//            tv_error_nama_perusahaan_suami_pp.setText(getString(R.string.error_field_required));
//            iv_circle_nama_perusahaan_suami_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_nama_perusahaan_suami_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nama_perusahaan_suami_pp, tv_error_nama_perusahaan_suami_pp, iv_circle_nama_perusahaan_suami_pp, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_bidang_usaha_suami_pp.isShown() && et_bidang_usaha_suami_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_bidang_usaha_suami_pp, tv_error_bidang_usaha_suami_pp, iv_circle_bidang_usaha_suami_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_bidang_usaha_suami_pp.setVisibility(View.VISIBLE);
//            tv_error_bidang_usaha_suami_pp.setText(getString(R.string.error_field_required));
//            iv_circle_bidang_usaha_suami_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_bidang_usaha_suami_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_bidang_usaha_suami_pp, tv_error_bidang_usaha_suami_pp, iv_circle_bidang_usaha_suami_pp, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_npwpsuami_pp.isShown() && et_npwpsuami_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_npwpsuami_pp, tv_error_npwpsuami_pp, iv_circle_npwpsuami_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_npwpsuami_pp.setVisibility(View.VISIBLE);
//            tv_error_npwpsuami_pp.setText(getString(R.string.error_field_required));
//            iv_circle_npwpsuami_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_npwpsuami_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_npwpsuami_pp, tv_error_npwpsuami_pp, iv_circle_npwpsuami_pp, color, getContext(), getString(R.string.error_regex));
            }
        }

        //AutoComplete

        if (ac_jabatan_ayah_pp.isShown() && ac_jabatan_ayah_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_jabatan_ayah_pp, tv_error_jabatan_ayah_pp, iv_circle_jabatan_ayah_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jabatan_ayah_pp.setVisibility(View.VISIBLE);
//            tv_error_jabatan_ayah_pp.setText(getString(R.string.error_field_required));
//            iv_circle_jabatan_ayah_pp.setColorFilter(color);
        }
        if (ac_klasifikasi_pekerjaan_ayah_pp.isShown() && ac_klasifikasi_pekerjaan_ayah_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_klasifikasi_pekerjaan_ayah_pp, tv_error_klasifikasi_pekerjaan_ayah_pp, iv_circle_klasifikasi_pekerjaan_ayah_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_klasifikasi_pekerjaan_ayah_pp.setVisibility(View.VISIBLE);
//            tv_error_klasifikasi_pekerjaan_ayah_pp.setText(getString(R.string.error_field_required));
//            iv_circle_klasifikasi_pekerjaan_ayah_pp.setColorFilter(color);
        }
        if (ac_penghasilan_pertahun_ayah_pp.isShown() && ac_penghasilan_pertahun_ayah_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_penghasilan_pertahun_ayah_pp, tv_error_penghasilan_pertahun_ayah_pp, iv_circle_penghasilan_pertahun_ayah_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_penghasilan_pertahun_ayah_pp.setVisibility(View.VISIBLE);
//            tv_error_penghasilan_pertahun_ayah_pp.setText(getString(R.string.error_field_required));
//            iv_circle_penghasilan_pertahun_ayah_pp.setColorFilter(color);
        }
        if (ac_klasifikasi_pekerjaan_ibu_pp.isShown() && ac_klasifikasi_pekerjaan_ibu_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_klasifikasi_pekerjaan_ibu_pp, tv_error_klasifikasi_pekerjaan_ibu_pp, iv_circle_klasifikasi_pekerjaan_ibu_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_klasifikasi_pekerjaan_ibu_pp.setVisibility(View.VISIBLE);
//            tv_error_klasifikasi_pekerjaan_ibu_pp.setText(getString(R.string.error_field_required));
//            iv_circle_klasifikasi_pekerjaan_ibu_pp.setColorFilter(color);
        }
        if (ac_jabatan_ibu_pp.isShown() && ac_jabatan_ibu_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_jabatan_ibu_pp, tv_error_jabatan_ibu_pp, iv_circle_jabatan_ibu_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jabatan_ibu_pp.setVisibility(View.VISIBLE);
//            tv_error_jabatan_ibu_pp.setText(getString(R.string.error_field_required));
//            iv_circle_jabatan_ibu_pp.setColorFilter(color);
        }
        if (ac_penghasilan_pertahun_ibu_pp.isShown() && ac_penghasilan_pertahun_ibu_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_penghasilan_pertahun_ibu_pp, tv_error_penghasilan_pertahun_ibu_pp, iv_circle_penghasilan_pertahun_ibu_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_penghasilan_pertahun_ibu_pp.setVisibility(View.VISIBLE);
//            tv_error_penghasilan_pertahun_ibu_pp.setText(getString(R.string.error_field_required));
//            iv_circle_penghasilan_pertahun_ibu_pp.setColorFilter(color);
        }
        if (ac_klasifikasi_pekerjaan_suami_pp.isShown() && ac_klasifikasi_pekerjaan_suami_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_klasifikasi_pekerjaan_suami_pp, tv_error_klasifikasi_pekerjaan_suami_pp, iv_circle_klasifikasi_pekerjaan_suami_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_klasifikasi_pekerjaan_suami_pp.setVisibility(View.VISIBLE);
//            tv_error_klasifikasi_pekerjaan_suami_pp.setText(getString(R.string.error_field_required));
//            iv_circle_klasifikasi_pekerjaan_suami_pp.setColorFilter(color);
        }
        if (ac_jabatan_suami_pp.isShown() && ac_jabatan_suami_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_jabatan_suami_pp, tv_error_jabatan_suami_pp, iv_circle_jabatan_suami_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jabatan_suami_pp.setVisibility(View.VISIBLE);
//            tv_error_jabatan_suami_pp.setText(getString(R.string.error_field_required));
//            iv_circle_jabatan_suami_pp.setColorFilter(color);
        }
        if (ac_penghasilan_pertahun_pp.isShown() && ac_penghasilan_pertahun_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_penghasilan_pertahun_pp, tv_error_penghasilan_pertahun_pp, iv_circle_penghasilan_pertahun_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_penghasilan_pertahun_pp.setVisibility(View.VISIBLE);
//            tv_error_penghasilan_pertahun_pp.setText(getString(R.string.error_field_required));
//            iv_circle_penghasilan_pertahun_pp.setColorFilter(color);
        }
        if (ac_bukti_identitas_pp.isShown() && ac_bukti_identitas_pp.getText().toString().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_bukti_identitas_pp, tv_error_bukti_identitas_pp, iv_circle_bukti_identitas_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_bukti_identitas_pp.setVisibility(View.VISIBLE);
//            tv_error_bukti_identitas_pp.setText(getString(R.string.error_field_required));
//            iv_circle_bukti_identitas_pp.setColorFilter(color);
        }
        if (ac_warganegara_pp.isShown() && ac_warganegara_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_warganegara_pp, tv_error_warganegara_pp, iv_circle_warganegara_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_warganegara_pp.setVisibility(View.VISIBLE);
//            tv_error_warganegara_pp.setText(getString(R.string.error_field_required));
//            iv_circle_warganegara_pp.setColorFilter(color);
        }
        if (ac_status_pp.isShown() && ac_status_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_status_pp, tv_error_status_pp, iv_circle_status_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_status_pp.setVisibility(View.VISIBLE);
//            tv_error_status_pp.setText(getString(R.string.error_field_required));
//            iv_circle_status_pp.setColorFilter(color);
        }
        if (ac_agama_pp.isShown() && ac_agama_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_agama_pp, tv_error_agama_pp, iv_circle_agama_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_agama_pp.setVisibility(View.VISIBLE);
//            tv_error_agama_pp.setText(getString(R.string.error_field_required));
//            iv_circle_agama_pp.setColorFilter(color);
        }
        if (ac_pendidikan_pp.isShown() && ac_pendidikan_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_pendidikan_pp, tv_error_pendidikan_pp, iv_circle_pendidikan_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_pendidikan_pp.setVisibility(View.VISIBLE);
//            tv_error_pendidikan_pp.setText(getString(R.string.error_field_required));
//            iv_circle_pendidikan_pp.setColorFilter(color);
        }
        if (ac_tagihan_pp.isShown() && ac_tagihan_pp.getText().toString().trim().length() == 0) {
            val = false;
            tv_error_tagihan_pp.setVisibility(View.VISIBLE);
            tv_error_tagihan_pp.setText(getString(R.string.error_field_required));
            iv_circle_tagihan_pp.setColorFilter(color);
        } else {
            if (!val_tagihan(tagihan)) {
                val = false;
                tv_error_tagihan_pp.setVisibility(View.VISIBLE);
                tv_error_tagihan_pp.setText(getString(R.string.error_field_required));
                iv_circle_tagihan_pp.setColorFilter(color);
            }
        }
        if (ac_klasifikasi_pekerjaan_pp.isShown() && ac_klasifikasi_pekerjaan_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_klasifikasi_pekerjaan_pp, tv_error_klasifikasi_pekerjaan_pp, iv_circle_klasifikasi_pekerjaan_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_klasifikasi_pekerjaan_pp.setVisibility(View.VISIBLE);
//            tv_error_klasifikasi_pekerjaan_pp.setText(getString(R.string.error_field_required));
//            iv_circle_klasifikasi_pekerjaan_pp.setColorFilter(color);
        }
        if (ac_hubungan_pp.isShown() && ac_hubungan_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_hubungan_pp, tv_error_hubungan_pp, iv_circle_hubungan_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_hubungan_pp.setVisibility(View.VISIBLE);
//            tv_error_hubungan_pp.setText(getString(R.string.error_field_required));
//            iv_circle_hubungan_pp.setColorFilter(color);
        }
        if (ac_hubungancp_pp.isShown() && ac_hubungancp_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_hubungancp_pp, tv_error_hubungancp_pp, iv_circle_hubungancp_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_hubungancp_pp.setVisibility(View.VISIBLE);
//            tv_error_hubungancp_pp.setText(getString(R.string.error_field_required));
//            iv_circle_hubungancp_pp.setColorFilter(color);
        }

        if (me.getPemegangPolisModel().getAlamat_pp().equals("")) {
//            tv_error_tagihan_pp.setVisibility(View.VISIBLE);
//            tv_error_tagihan_pp.setText("Mohon lengkapi detil alamat yang dipilih");

            MethodSupport.onFocusviewNoValidation(scroll_pp, cl_child_pp, cl_alamat_rumah_pp, tv_error_alamat_rumah_pp, color, getContext(), getString(R.string.error_field_adress_home));
            tv_error_alamat_rumah_pp.setVisibility(View.VISIBLE);
            val = false;
        } else {
//            tv_error_tagihan_pp.setVisibility(View.GONE);
            tv_error_alamat_rumah_pp.setVisibility(View.GONE);
        }

        if (me.getPemegangPolisModel().getHp1_pp().length() == 0) {
            MethodSupport.onFocusviewNoValidation(scroll_pp, cl_child_pp, cl_hp_email_pp, tv_error_hp_email_pp, color, getContext(), getString(R.string.error_field_required));
            val = false;
        }
        //radiogroup
        if (jekel == -1) {
            val = false;
            tv_error_jekel_pp.setVisibility(View.VISIBLE);
            tv_error_jekel_pp.setText(getString(R.string.error_field_required));
            iv_circle_jekel_pp.setColorFilter(color);
        }
        //chckbox
        if (!bool_dana) {
            val = false;
        }
        if (!bool_tujuan) {
            val = false;
        }
        return val;
    }

    private boolean val_page() {
        boolean val = true;
        int color = ContextCompat.getColor(getContext(), R.color.red);
        if (et_nama_pp.isShown() && et_nama_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nama_pp, tv_error_nama_pp, iv_circle_nama_pp, color, getContext(), getString(R.string.error_field_required));
        } else {
            if (!Method_Validator.ValEditRegex(et_nama_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_nama_pp, tv_error_nama_pp, iv_circle_nama_pp, color, getContext(), getString(R.string.error_regex));
            }
        }

        if (et_ttl_pp.isShown() && et_ttl_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_ttl_pp, tv_error_ttl_pp, iv_circle_ttl_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_ttl_pp.setVisibility(View.VISIBLE);
//            tv_error_ttl_pp.setText(getString(R.string.error_field_required));
//            iv_circle_ttl_pp.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_ttl_pp.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_pp, cl_child_pp, et_ttl_pp, tv_error_ttl_pp, iv_circle_ttl_pp, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (ac_hubungan_pp.isShown() && ac_hubungan_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_hubungan_pp, tv_error_hubungan_pp, iv_circle_hubungan_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_hubungan_pp.setVisibility(View.VISIBLE);
//            tv_error_hubungan_pp.setText(getString(R.string.error_field_required));
//            iv_circle_hubungan_pp.setColorFilter(color);
        }
        if (ac_hubungancp_pp.isShown() && ac_hubungancp_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_hubungancp_pp, tv_error_hubungancp_pp, iv_circle_hubungancp_pp, color, getContext(), getString(R.string.error_field_required));
//            tv_error_hubungancp_pp.setVisibility(View.VISIBLE);
//            tv_error_hubungancp_pp.setText(getString(R.string.error_field_required));
//            iv_circle_hubungancp_pp.setColorFilter(color);
        }
//        if (ac_tagihan_pp.isShown() && ac_tagihan_pp.getText().toString().trim().length() == 0) {
//            val = false;
//            MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_tagihan_pp, tv_error_tagihan_pp, iv_circle_tagihan_pp, color, getContext(), getString(R.string.error_field_required));
//        } else {
//            if (!val_tagihan(tagihan)) {
//                val = false;
//                MethodSupport.onFocusview(scroll_pp, cl_child_pp, ac_tagihan_pp, tv_error_tagihan_pp, iv_circle_tagihan_pp, color, getContext(), getString(R.string.error_field_adress));
//            }
//
//        }

        String jobDescription = ac_klasifikasi_pekerjaan_pp.getText().toString().trim();
        if (TextUtils.isEmpty(jobDescription)) {
            ac_klasifikasi_pekerjaan_pp.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
            ac_klasifikasi_pekerjaan_pp.requestFocus();
            val = false;
        }

        if (!isJobDescriptionValid) {
            ac_klasifikasi_pekerjaan_pp.setError("Deskripsi pekerjaan ini tidak ada dalam pilihan");
            ac_klasifikasi_pekerjaan_pp.requestFocus();
            val = false;
        }
//
        return val;
    }

    private void onDeactiveView() {
        MethodSupport.active_view(0, et_no_proposal_pp);
        MethodSupport.active_view(0, ac_produk_pp);
        MethodSupport.active_view(0, ac_sub_produk_pp);
        MethodSupport.active_view(0, et_nama_pp);
        MethodSupport.active_view(0, et_ttl_pp);
        MethodSupport.active_view(0, et_usia_pp);
        MethodSupport.active_radio(0, rg_jekel_pp);
        if (me.getPemegangPolisModel().getHubungan_pp() == 1) {
            MethodSupport.active_view(0, ac_hubungan_pp);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCode.FILL_ALAMAT_SPAJ) {
            if (resultCode == RESULT_OK) {
                EspajModel modelUpdate = data.getParcelableExtra(getString(R.string.modelespaj));
                switch (data.getIntExtra(getString(R.string.flag), 0)) {
                    case 1:
                        me.getPemegangPolisModel().setAlamat_pp(modelUpdate.getPemegangPolisModel().getAlamat_pp());
                        me.getPemegangPolisModel().setPropinsi_pp(modelUpdate.getPemegangPolisModel().getPropinsi_pp());
                        me.getPemegangPolisModel().setKabupaten_pp(modelUpdate.getPemegangPolisModel().getKabupaten_pp());
                        me.getPemegangPolisModel().setKecamatan_pp(modelUpdate.getPemegangPolisModel().getKecamatan_pp());
                        me.getPemegangPolisModel().setKelurahan_pp(modelUpdate.getPemegangPolisModel().getKelurahan_pp());
                        me.getPemegangPolisModel().setKdpos_pp(modelUpdate.getPemegangPolisModel().getKdpos_pp());
                        me.getPemegangPolisModel().setKdtelp1_pp(modelUpdate.getPemegangPolisModel().getKdtelp1_pp());
                        me.getPemegangPolisModel().setTelp1_pp(modelUpdate.getPemegangPolisModel().getTelp1_pp());
                        me.getPemegangPolisModel().setKdtelp2_pp(modelUpdate.getPemegangPolisModel().getKdtelp2_pp());
                        me.getPemegangPolisModel().setTelp2_pp(modelUpdate.getPemegangPolisModel().getTelp2_pp());
                        updateAlamatRmh();
                        break;
                    case 2:
                        me.getPemegangPolisModel().setAlamat_kantor_pp(modelUpdate.getPemegangPolisModel().getAlamat_kantor_pp());
                        me.getPemegangPolisModel().setPropinsi_kantor_pp(modelUpdate.getPemegangPolisModel().getPropinsi_kantor_pp());
                        me.getPemegangPolisModel().setKabupaten_kantor_pp(modelUpdate.getPemegangPolisModel().getKabupaten_kantor_pp());
                        me.getPemegangPolisModel().setKecamatan_kantor_pp(modelUpdate.getPemegangPolisModel().getKecamatan_kantor_pp());
                        me.getPemegangPolisModel().setKelurahan_kantor_pp(modelUpdate.getPemegangPolisModel().getKelurahan_kantor_pp());
                        me.getPemegangPolisModel().setKdpos_kantor_pp(modelUpdate.getPemegangPolisModel().getKdpos_kantor_pp());
                        me.getPemegangPolisModel().setKdtelp1_kantor_pp(modelUpdate.getPemegangPolisModel().getKdtelp1_kantor_pp());
                        me.getPemegangPolisModel().setTelp1_kantor_pp(modelUpdate.getPemegangPolisModel().getTelp1_kantor_pp());
                        me.getPemegangPolisModel().setKdtelp2_kantor_pp(modelUpdate.getPemegangPolisModel().getKdtelp2_kantor_pp());
                        me.getPemegangPolisModel().setTelp2_kantor_pp(modelUpdate.getPemegangPolisModel().getTelp2_kantor_pp());
                        me.getPemegangPolisModel().setKdfax_kantor_pp(modelUpdate.getPemegangPolisModel().getKdfax_kantor_pp());
                        me.getPemegangPolisModel().setFax_kantor_pp(modelUpdate.getPemegangPolisModel().getFax_kantor_pp());
                        updateAlamatKantor();
                        break;
                    case 3:
                        me.getPemegangPolisModel().setAlamat_tinggal_pp(modelUpdate.getPemegangPolisModel().getAlamat_tinggal_pp());
                        me.getPemegangPolisModel().setPropinsi_tinggal_pp(modelUpdate.getPemegangPolisModel().getPropinsi_tinggal_pp());
                        me.getPemegangPolisModel().setKabupaten_tinggal_pp(modelUpdate.getPemegangPolisModel().getKabupaten_tinggal_pp());
                        me.getPemegangPolisModel().setKecamatan_tinggal_pp(modelUpdate.getPemegangPolisModel().getKecamatan_tinggal_pp());
                        me.getPemegangPolisModel().setKelurahan_tinggal_pp(modelUpdate.getPemegangPolisModel().getKelurahan_tinggal_pp());
                        me.getPemegangPolisModel().setKdpos_tinggal_pp(modelUpdate.getPemegangPolisModel().getKdpos_tinggal_pp());
                        me.getPemegangPolisModel().setKdtelp1_tinggal_pp(modelUpdate.getPemegangPolisModel().getKdtelp1_tinggal_pp());
                        me.getPemegangPolisModel().setTelp1_tinggal_pp(modelUpdate.getPemegangPolisModel().getTelp1_tinggal_pp());
                        me.getPemegangPolisModel().setKdtelp2_tinggal_pp(modelUpdate.getPemegangPolisModel().getKdtelp2_tinggal_pp());
                        me.getPemegangPolisModel().setTelp2_tinggal_pp(modelUpdate.getPemegangPolisModel().getTelp2_tinggal_pp());
                        me.getPemegangPolisModel().setKdfax_tinggal_pp(modelUpdate.getPemegangPolisModel().getKdfax_tinggal_pp());
                        me.getPemegangPolisModel().setFax_tinggal_pp(modelUpdate.getPemegangPolisModel().getFax_tinggal_pp());
                        updateAlamatTinggal();
                        break;
                    case 7:
                        me.getPemegangPolisModel().setHp1_pp(modelUpdate.getPemegangPolisModel().getHp1_pp());
                        me.getPemegangPolisModel().setHp2_pp(modelUpdate.getPemegangPolisModel().getHp2_pp());
                        me.getPemegangPolisModel().setEmail_pp(modelUpdate.getPemegangPolisModel().getEmail_pp());
                        UpdateHP();
                        break;
                    default:
                        throw new IllegalArgumentException("Field Type belum diset");
                }
            }
        }
    }

    public void goToForm(int flag) {
        Intent intent = new Intent(getContext(), E_FormAlamatAutoZipActivity.class);
        intent.putExtra(getString(R.string.flag), flag);
        intent.putExtra(getString(R.string.modelespaj), me);
        startActivityForResult(intent, RequestCode.FILL_ALAMAT_SPAJ);
    }

    private void updateAlamatRmh() {
        if (!me.getPemegangPolisModel().getAlamat_pp().equals("")) {
            tv_isi_alamat_rumah_pp.setVisibility(View.GONE);
            img_alamat_rumah_pp.setVisibility(View.GONE);
            tv_ubah_alamat_rumah_pp.setVisibility(View.VISIBLE);
            ll_alamat_rumah_pp.setVisibility(View.VISIBLE);
            String Alamatpostalcode = me.getPemegangPolisModel().getKdpos_pp();
            ListDropDownKodepos = new E_Select(getContext()).getLst_Kodepos_PP(me.getPemegangPolisModel().getKdpos_pp());
            ListDropDownKelurahan = new E_Select(getContext()).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_pp());
            ListDropDownKecamatan = new E_Select(getContext()).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_pp());
            ListDropDownKabupaten = new E_Select(getContext()).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_pp());
            ListDropDownProvinsi = new E_Select(getContext()).getLst_Propinsi_PP();
            String[][] values = new String[][]{

                    new String[]{getString(R.string.alamat), me.getPemegangPolisModel().getAlamat_pp()},
                    new String[]{getString(R.string.provinsi), MethodSupport.getAdapterPositionString(ListDropDownProvinsi, me.getPemegangPolisModel().getPropinsi_pp())},
                    new String[]{getString(R.string.kabupaten), MethodSupport.getAdapterPositionString(ListDropDownKabupaten, me.getPemegangPolisModel().getKabupaten_pp())},
                    new String[]{getString(R.string.kecamatan), MethodSupport.getAdapterPositionString(ListDropDownKecamatan, me.getPemegangPolisModel().getKecamatan_pp())},
                    new String[]{getString(R.string.kelurahan), MethodSupport.getAdapterPositionString(ListDropDownKelurahan, me.getPemegangPolisModel().getKelurahan_pp())},
                    new String[]{getString(R.string.kode_pos), Alamatpostalcode},
                    new String[]{getString(R.string.telp_1), me.getPemegangPolisModel().getKdtelp1_pp() + " - " + me.getPemegangPolisModel().getTelp1_pp()},
                    new String[]{getString(R.string.telp_2), me.getPemegangPolisModel().getKdtelp1_pp() + " - " + me.getPemegangPolisModel().getTelp2_pp()}
            };
            new ValueView(getContext()).addToLinearLayout(ll_alamat_rumah_pp, values);
        } else {
            tv_isi_alamat_rumah_pp.setVisibility(View.VISIBLE);
            img_alamat_rumah_pp.setVisibility(View.VISIBLE);
            tv_ubah_alamat_rumah_pp.setVisibility(View.GONE);
            ll_alamat_rumah_pp.setVisibility(View.GONE);
        }
    }

    private void updateAlamatKantor() {
        if (!me.getPemegangPolisModel().getAlamat_kantor_pp().equals("")) {
            tv_isi_alamat_kantor_pp.setVisibility(View.GONE);
            img_alamat_kantor_pp.setVisibility(View.GONE);
            tv_ubah_alamat_kantor_pp.setVisibility(View.VISIBLE);
            ll_alamat_kantor_pp.setVisibility(View.VISIBLE);

            String AlamatpostalCode = me.getPemegangPolisModel().getKdpos_kantor_pp();
            ListDropDownKodepos = new E_Select(getContext()).getLst_Kodepos_PP(me.getPemegangPolisModel().getKdpos_kantor_pp());
            ListDropDownKelurahan = new E_Select(getContext()).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_kantor_pp());
            ListDropDownKecamatan = new E_Select(getContext()).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_kantor_pp());
            ListDropDownKabupaten = new E_Select(getContext()).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_kantor_pp());
            ListDropDownProvinsi = new E_Select(getContext()).getLst_Propinsi_PP();
            String[][] values = new String[][]{
                    new String[]{getString(R.string.alamat), me.getPemegangPolisModel().getAlamat_kantor_pp()},
                    new String[]{getString(R.string.provinsi), MethodSupport.getAdapterPositionString(ListDropDownProvinsi, me.getPemegangPolisModel().getPropinsi_kantor_pp())},
                    new String[]{getString(R.string.kabupaten), MethodSupport.getAdapterPositionString(ListDropDownKabupaten, me.getPemegangPolisModel().getKabupaten_kantor_pp())},
                    new String[]{getString(R.string.kecamatan), MethodSupport.getAdapterPositionString(ListDropDownKecamatan, me.getPemegangPolisModel().getKecamatan_kantor_pp())},
                    new String[]{getString(R.string.kelurahan), MethodSupport.getAdapterPositionString(ListDropDownKelurahan, me.getPemegangPolisModel().getKelurahan_kantor_pp())},
                    new String[]{getString(R.string.kode_pos), AlamatpostalCode},
                    new String[]{getString(R.string.telp_1), me.getPemegangPolisModel().getKdtelp1_kantor_pp() + " - " + me.getPemegangPolisModel().getTelp1_kantor_pp()},
                    new String[]{getString(R.string.telp_2), me.getPemegangPolisModel().getKdtelp2_kantor_pp() + " - " + me.getPemegangPolisModel().getTelp2_kantor_pp()},
                    new String[]{getString(R.string.no_fax), me.getPemegangPolisModel().getKdfax_kantor_pp() + " - " + me.getPemegangPolisModel().getFax_kantor_pp()}
            };
            new ValueView(getContext()).addToLinearLayout(ll_alamat_kantor_pp, values);
        } else {
            tv_isi_alamat_kantor_pp.setVisibility(View.VISIBLE);
            img_alamat_kantor_pp.setVisibility(View.VISIBLE);
            tv_ubah_alamat_kantor_pp.setVisibility(View.GONE);
            ll_alamat_kantor_pp.setVisibility(View.GONE);
        }
    }

    private void updateAlamatTinggal() {
        if (!me.getPemegangPolisModel().getAlamat_tinggal_pp().equals("")) {
            tv_isi_alamat_tempat_tinggal_pp.setVisibility(View.GONE);
            img_alamat_tempat_tinggal_pp.setVisibility(View.GONE);
            tv_ubah_alamat_tempat_tinggal_pp.setVisibility(View.VISIBLE);
            ll_alamat_tempat_tinggal_pp.setVisibility(View.VISIBLE);
            String AlamatpostalCode = me.getPemegangPolisModel().getKdpos_tinggal_pp();
            ListDropDownKodepos = new E_Select(getContext()).getLst_Kodepos_PP(me.getPemegangPolisModel().getKdpos_tinggal_pp());
            ListDropDownKabupaten = new E_Select(getContext()).getLst_Kabupaten_PP(me.getPemegangPolisModel().getKdpos_tinggal_pp());
            ListDropDownKelurahan = new E_Select(getContext()).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_tinggal_pp());
            ListDropDownKecamatan = new E_Select(getContext()).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_tinggal_pp());
            ListDropDownKabupaten = new E_Select(getContext()).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_tinggal_pp());
            ListDropDownProvinsi = new E_Select(getContext()).getLst_Propinsi_PP();
            String[][] values = new String[][]{
                    new String[]{getString(R.string.alamat), me.getPemegangPolisModel().getAlamat_tinggal_pp()},
                    new String[]{getString(R.string.provinsi), MethodSupport.getAdapterPositionString(ListDropDownProvinsi, me.getPemegangPolisModel().getPropinsi_tinggal_pp())},
                    new String[]{getString(R.string.kabupaten), MethodSupport.getAdapterPositionString(ListDropDownKabupaten, me.getPemegangPolisModel().getKabupaten_tinggal_pp())},
                    new String[]{getString(R.string.kecamatan), MethodSupport.getAdapterPositionString(ListDropDownKecamatan, me.getPemegangPolisModel().getKecamatan_tinggal_pp())},
                    new String[]{getString(R.string.kelurahan), MethodSupport.getAdapterPositionString(ListDropDownKelurahan, me.getPemegangPolisModel().getKelurahan_tinggal_pp())},
                    new String[]{getString(R.string.kode_pos), AlamatpostalCode},
                    new String[]{getString(R.string.telp_1), me.getPemegangPolisModel().getKdtelp1_tinggal_pp() + " - " + me.getPemegangPolisModel().getTelp1_tinggal_pp()},
                    new String[]{getString(R.string.telp_2), me.getPemegangPolisModel().getKdtelp2_tinggal_pp() + " - " + me.getPemegangPolisModel().getTelp2_tinggal_pp()},
                    new String[]{getString(R.string.no_fax), me.getPemegangPolisModel().getKdfax_tinggal_pp() + " - " + me.getPemegangPolisModel().getFax_tinggal_pp()}
            };
            new ValueView(getContext()).addToLinearLayout(ll_alamat_tempat_tinggal_pp, values);
        } else {
            tv_isi_alamat_tempat_tinggal_pp.setVisibility(View.VISIBLE);
            img_alamat_tempat_tinggal_pp.setVisibility(View.VISIBLE);
            tv_ubah_alamat_tempat_tinggal_pp.setVisibility(View.GONE);
            ll_alamat_tempat_tinggal_pp.setVisibility(View.GONE);
        }
    }

    private void UpdateHP() {
        if (!me.getPemegangPolisModel().getHp1_pp().equals("")) {
            tv_isi_hp_email_pp.setVisibility(View.GONE);
            img_hp_email_pp.setVisibility(View.GONE);
            tv_ubah_hp_email_pp.setVisibility(View.VISIBLE);
            ll_hp_email_pp.setVisibility(View.VISIBLE);

            String[][] values = new String[][]{
                    new String[]{getString(R.string.hp1), me.getPemegangPolisModel().getHp1_pp()},
                    new String[]{getString(R.string.hp2), me.getPemegangPolisModel().getHp2_pp()},
                    new String[]{getString(R.string.email), me.getPemegangPolisModel().getEmail_pp()}
            };
            new ValueView(getContext()).addToLinearLayout(ll_hp_email_pp, values);
        } else {
            tv_isi_hp_email_pp.setVisibility(View.VISIBLE);
            img_hp_email_pp.setVisibility(View.VISIBLE);
            tv_ubah_hp_email_pp.setVisibility(View.GONE);
            ll_hp_email_pp.setVisibility(View.GONE);
        }
    }

    private void val_pekerjaan(String pekerjaan) {
        if (pekerjaan.equals("Ibu rumah tangga")) {
            cl_pemegangpolis_ibu_pelajar_pp.setVisibility(View.VISIBLE);
            cl_form_pemegangpolis_ibu_pelajar_pp.setVisibility(View.VISIBLE);
            cl_form_pemegangpolis_ibu_pelajar2_pp.setVisibility(View.GONE);
            et_namaayah_pp.setText("");
            ac_jabatan_ayah_pp.setText("");
            et_nama_perusahaan_ayah_pp.setText("");
            et_bidang_usaha_ayah_pp.setText("");
            et_npwpayah_pp.setText("");
            et_ttl_ayah_pp.setText("");
            et_usia_ayah_pp.setText("");
            ac_klasifikasi_pekerjaan_ayah_pp.setText("");
            ac_penghasilan_pertahun_ayah_pp.setText("");
            str_penghasilan_ayah_pp = "";
            et_namaibu_pp.setText("");
            ac_jabatan_ibu_pp.setText("");
            et_nama_perusahaan_ibu_pp.setText("");
            et_bidang_usaha_ibu_pp.setText("");
            et_npwpibu_pp.setText("");
            et_ttl_ibu_pp.setText("");
            et_usia_ibu_pp.setText("");
            ac_klasifikasi_pekerjaan_ibu_pp.setText("");
            ac_penghasilan_pertahun_ibu_pp.setText("");
            str_penghasilan_ibu_pp = "";
        } else if (pekerjaan.equals("Pelajar/ mahasiswa ")) {
            cl_pemegangpolis_ibu_pelajar_pp.setVisibility(View.VISIBLE);
            cl_form_pemegangpolis_ibu_pelajar_pp.setVisibility(View.GONE);
            cl_form_pemegangpolis_ibu_pelajar2_pp.setVisibility(View.VISIBLE);
            et_namasuami_pp.setText("");
            ac_jabatan_suami_pp.setText("");
            et_nama_perusahaan_suami_pp.setText("");
            et_bidang_usaha_suami_pp.setText("");
            et_npwpsuami_pp.setText("");
            et_ttl_suami_pp.setText("");
            et_usia_suami_pp.setText("");
            ac_klasifikasi_pekerjaan_suami_pp.setText("");
            ac_penghasilan_pertahun_pp.setText("");
            str_penghasilan_suamithn_pp = "";
        } else {
            cl_pemegangpolis_ibu_pelajar_pp.setVisibility(View.GONE);
            et_namasuami_pp.setText("");
            ac_jabatan_suami_pp.setText("");
            et_nama_perusahaan_suami_pp.setText("");
            et_bidang_usaha_suami_pp.setText("");
            et_npwpsuami_pp.setText("");
            et_ttl_suami_pp.setText("");
            et_usia_suami_pp.setText("");
            ac_klasifikasi_pekerjaan_suami_pp.setText("");
            ac_penghasilan_pertahun_pp.setText("");
            str_penghasilan_suamithn_pp = "";

            et_namaayah_pp.setText("");
            ac_jabatan_ayah_pp.setText("");
            et_nama_perusahaan_ayah_pp.setText("");
            et_bidang_usaha_ayah_pp.setText("");
            et_npwpayah_pp.setText("");
            et_ttl_ayah_pp.setText("");
            et_usia_ayah_pp.setText("");
            ac_klasifikasi_pekerjaan_ayah_pp.setText("");
            ac_penghasilan_pertahun_ayah_pp.setText("");
            str_penghasilan_ayah_pp = "";

            et_namaibu_pp.setText("");
            ac_jabatan_ibu_pp.setText("");
            et_nama_perusahaan_ibu_pp.setText("");
            et_bidang_usaha_ibu_pp.setText("");
            et_npwpibu_pp.setText("");
            et_ttl_ibu_pp.setText("");
            et_usia_ibu_pp.setText("");
            ac_klasifikasi_pekerjaan_ibu_pp.setText("");
            ac_penghasilan_pertahun_ibu_pp.setText("");
            str_penghasilan_ibu_pp = "";
        }
    }


//    private boolean val_hp_email (int hp_email){
//        Boolean val = true;
//        switch (hp_email){
//            case 0:
//                if (me.getPemegangPolisModel().getHp1_pp().equals("")){
//                    tv_error_hp_email_pp.setVisibility(View.VISIBLE);
//                    tv_error_hp_email_pp.setText("Mohon lengkapi detil No Hp yang dipilih");
//                    val = false;
//                } else {
//                    tv_error_hp_email_pp.setVisibility(View.GONE);
//                }
//        }
//        return val;
//    }

    private boolean val_tagihan(int tagihan) {
        Boolean val = true;
        switch (tagihan) {
            case 0:
                if (me.getPemegangPolisModel().getAlamat_pp().equals("")) {
                    tv_error_tagihan_pp.setVisibility(View.VISIBLE);
                    tv_error_tagihan_pp.setText("Mohon lengkapi detil alamat yang dipilih");
                    val = false;
                } else {
                    tv_error_tagihan_pp.setVisibility(View.GONE);
                    tv_error_alamat_rumah_pp.setVisibility(View.GONE);
                }
                break;
            case 1:
                if (me.getPemegangPolisModel().getAlamat_kantor_pp().equals("")) {
                    tv_error_tagihan_pp.setVisibility(View.VISIBLE);
                    tv_error_tagihan_pp.setText("Mohon lengkapi detil alamat yang dipilih");
                    val = false;
                } else {
                    tv_error_tagihan_pp.setVisibility(View.GONE);
                }

                break;
            case 2:
                if (me.getPemegangPolisModel().getAlamat_tinggal_pp().equals("")) {
                    tv_error_tagihan_pp.setVisibility(View.VISIBLE);
                    tv_error_tagihan_pp.setText("Mohon lengkapi detil alamat yang dipilih");
                    val = false;
                } else {
                    tv_error_tagihan_pp.setVisibility(View.GONE);
                }
                break;
        }
        return val;
    }

    private void val_agama(int agama) {
        if (agama == 6) {
            et_teks_agama_lain_pp.setVisibility(View.VISIBLE);
        } else {
            et_teks_agama_lain_pp.setVisibility(View.GONE);
        }
    }

    private void val_bukti(int bukti) {
        if (bukti == 7) {
            et_teks_bukti_lain_pp.setVisibility(View.VISIBLE);
        } else {
            et_teks_bukti_lain_pp.setVisibility(View.GONE);
        }
        if (bukti == 9 || bukti == 5) {
            et_tgl_berlaku_pp.setVisibility(View.GONE);
            iv_circle_tgl_berlaku_pp.setVisibility(View.GONE);
        } else {
            et_tgl_berlaku_pp.setVisibility(View.VISIBLE);
            iv_circle_tgl_berlaku_pp.setVisibility(View.VISIBLE);
        }
    }

    private void val_hubpptt() {
        if (hub == 1) {
            me.setTertanggungModel(new TertanggungModel());
            me.getTertanggungModel().setNama_tt(et_nama_pp.getText().toString());
            me.getTertanggungModel().setGelar_tt(et_gelar_pp.getText().toString());
            me.getTertanggungModel().setNama_ibu_tt(et_ibu_pp.getText().toString());
            me.getTertanggungModel().setBukti_identitas_tt(bukti_pp);
            me.getTertanggungModel().setNo_identitas_tt(et_nomor_bukti_pp.getText().toString());
            me.getTertanggungModel().setTgl_berlaku_tt(et_tgl_berlaku_pp.getText().toString());
            me.getTertanggungModel().setWarga_negara_tt(wn_pp);
            me.getTertanggungModel().setTempat_tt(et_tempat_pp.getText().toString());
            me.getTertanggungModel().setTtl_tt(et_ttl_pp.getText().toString());
            me.getTertanggungModel().setUsia_tt(et_usia_pp.getText().toString());
            me.getTertanggungModel().setJekel_tt(jekel);
            me.getTertanggungModel().setStatus_tt(stat_pp);
            me.getTertanggungModel().setAgama_tt(agama);
            me.getTertanggungModel().setAgama_lain_tt(et_teks_agama_lain_pp.getText().toString());
            me.getTertanggungModel().setPendidikan_tt(pend_pp);

            me.getTertanggungModel().setAlamat_tt(me.getPemegangPolisModel().getAlamat_pp());
//            me.getTertanggungModel().setKota_tt(me.getPemegangPolisModel().getKota_pp());
            me.getTertanggungModel().setPropinsi_tt(me.getPemegangPolisModel().getPropinsi_pp());
            me.getTertanggungModel().setKabupaten_tt(me.getPemegangPolisModel().getKabupaten_pp());
            me.getTertanggungModel().setKecamatan_tt(me.getPemegangPolisModel().getKecamatan_pp());
            me.getTertanggungModel().setKelurahan_tt(me.getPemegangPolisModel().getKelurahan_pp());
            me.getTertanggungModel().setKdpos_tt(me.getPemegangPolisModel().getKdpos_pp());
            me.getTertanggungModel().setKdtelp1_tt(me.getPemegangPolisModel().getKdtelp1_pp());
            me.getTertanggungModel().setTelp1_tt(me.getPemegangPolisModel().getTelp1_pp());
            me.getTertanggungModel().setKdtelp2_tt(me.getPemegangPolisModel().getKdtelp2_pp());
            me.getTertanggungModel().setTelp2_tt(me.getPemegangPolisModel().getTelp2_pp());
            me.getTertanggungModel().setAlamat_kantor_tt(me.getPemegangPolisModel().getAlamat_kantor_pp());
//            me.getTertanggungModel().setKota_kantor_tt(me.getPemegangPolisModel().getKota_kantor_pp());
            me.getTertanggungModel().setPropinsi_kantor_tt(me.getPemegangPolisModel().getPropinsi_kantor_pp());
            me.getTertanggungModel().setKabupaten_kantor_tt(me.getPemegangPolisModel().getKabupaten_kantor_pp());
            me.getTertanggungModel().setKecamatan_kantor_tt(me.getPemegangPolisModel().getKecamatan_kantor_pp());
            me.getTertanggungModel().setKelurahan_kantor_tt(me.getPemegangPolisModel().getKelurahan_kantor_pp());
            me.getTertanggungModel().setKdpos_kantor_tt(me.getPemegangPolisModel().getKdpos_kantor_pp());
            me.getTertanggungModel().setKdtelp1_kantor_tt(me.getPemegangPolisModel().getKdtelp1_kantor_pp());
            me.getTertanggungModel().setTelp1_kantor_tt(me.getPemegangPolisModel().getTelp1_kantor_pp());
            me.getTertanggungModel().setKdtelp2_kantor_tt(me.getPemegangPolisModel().getKdtelp2_kantor_pp());
            me.getTertanggungModel().setTelp2_kantor_tt(me.getPemegangPolisModel().getTelp2_kantor_pp());
            me.getTertanggungModel().setFax_kantor_tt(me.getPemegangPolisModel().getFax_kantor_pp());
            me.getTertanggungModel().setAlamat_tinggal_tt(me.getPemegangPolisModel().getAlamat_tinggal_pp());
//            me.getTertanggungModel().setKota_tinggal_tt(me.getPemegangPolisModel().getKota_tinggal_pp());
            me.getTertanggungModel().setPropinsi_tinggal_tt(me.getPemegangPolisModel().getPropinsi_tinggal_pp());
            me.getTertanggungModel().setKabupaten_tinggal_tt(me.getPemegangPolisModel().getKabupaten_tinggal_pp());
            me.getTertanggungModel().setKecamatan_tinggal_tt(me.getPemegangPolisModel().getKecamatan_tinggal_pp());
            me.getTertanggungModel().setKelurahan_tinggal_tt(me.getPemegangPolisModel().getKelurahan_tinggal_pp());
            me.getTertanggungModel().setKdpos_tinggal_tt(me.getPemegangPolisModel().getKdpos_tinggal_pp());
            me.getTertanggungModel().setKdtelp1_tinggal_tt(me.getPemegangPolisModel().getKdtelp1_tinggal_pp());
            me.getTertanggungModel().setTelp1_kantor_tt(me.getPemegangPolisModel().getTelp1_tinggal_pp());
            me.getTertanggungModel().setKdtelp2_tinggal_tt(me.getPemegangPolisModel().getKdtelp2_tinggal_pp());
            me.getTertanggungModel().setTelp2_tinggal_tt(me.getPemegangPolisModel().getTelp2_tinggal_pp());
            me.getTertanggungModel().setFax_tinggal_tt(me.getPemegangPolisModel().getFax_tinggal_pp());
            me.getTertanggungModel().setHp1_tt(me.getPemegangPolisModel().getHp1_pp());
            me.getTertanggungModel().setHp2_tt(me.getPemegangPolisModel().getHp2_pp());
            me.getTertanggungModel().setEmail_tt(me.getPemegangPolisModel().getEmail_pp());

            me.getTertanggungModel().setPenghasilan_thn_tt(str_pendapatan);
            me.getTertanggungModel().setKlasifikasi_pekerjaan_tt(ac_klasifikasi_pekerjaan_pp.getText().toString());
            me.getTertanggungModel().setUraian_pekerjaan_tt(et_uraikan_tugas_pp.getText().toString());
            me.getTertanggungModel().setJabatan_klasifikasi_tt(str_jabatan_klasifikasi);
            me.getTertanggungModel().setGreencard_tt(greencard);
            me.getTertanggungModel().setAlias_tt(et_alias_pp.getText().toString());
            me.getTertanggungModel().setNm_perusahaan_tt(et_nama_perusahaan_pp.getText().toString());
            me.getTertanggungModel().setNpwp_tt(et_npwp_pp.getText().toString());
            me.getTertanggungModel().setDana_gaji_tt(d_gaji_pp);
            me.getTertanggungModel().setDana_tabungan_tt(d_tabungan_pp);
            me.getTertanggungModel().setDana_warisan_tt(d_warisan_pp);
            me.getTertanggungModel().setDana_hibah_tt(d_hibah_pp);
            me.getTertanggungModel().setDana_lainnya_tt(d_lainnya_pp);
            me.getTertanggungModel().setEdit_d_lainnya_tt(et_editd_lainnya_pp.getText().toString());
            me.getTertanggungModel().setTujuan_proteksi_tt(t_proteksi_pp);
            me.getTertanggungModel().setTujuan_inves_tt(t_inves_pp);
            me.getTertanggungModel().setTujuan_lainnya_tt(t_lainnya_pp);
            me.getTertanggungModel().setEdit_t_lainnya_tt(et_editt_lainnya_pp.getText().toString());
        }
        if (me.getModelID().getProposal_tab().equals("")) {
            if (hub != me.getPemegangPolisModel().getHubungan_pp()) {
                me.setTertanggungModel(new TertanggungModel());
            } else {
                me.getTertanggungModel().setGelar_tt("");
                me.getTertanggungModel().setNama_ibu_tt("");
                me.getTertanggungModel().setBukti_identitas_tt(0);
                me.getTertanggungModel().setNo_identitas_tt("");
                me.getTertanggungModel().setTgl_berlaku_tt("");
                me.getTertanggungModel().setWarga_negara_tt(0);
                me.getTertanggungModel().setTempat_tt("");
                me.getTertanggungModel().setStatus_tt(0);
                me.getTertanggungModel().setAgama_tt(0);
                me.getTertanggungModel().setAgama_lain_tt("");
                me.getTertanggungModel().setPendidikan_tt(0);
                me.getTertanggungModel().setAlamat_tt("");
//                me.getTertanggungModel().setKota_tt("");
                me.getTertanggungModel().setPropinsi_tt("");
                me.getTertanggungModel().setKabupaten_tt("");
                me.getTertanggungModel().setKecamatan_tt("");
                me.getTertanggungModel().setKelurahan_tt("");
                me.getTertanggungModel().setKdpos_tt("");
                me.getTertanggungModel().setKdtelp1_tt("");
                me.getTertanggungModel().setTelp1_tt("");
                me.getTertanggungModel().setKdtelp2_tt("");
                me.getTertanggungModel().setTelp2_tt("");
                me.getTertanggungModel().setAlamat_kantor_tt("");
//                me.getTertanggungModel().setKota_kantor_tt("");
                me.getTertanggungModel().setPropinsi_kantor_tt("");
                me.getTertanggungModel().setKabupaten_kantor_tt("");
                me.getTertanggungModel().setKecamatan_kantor_tt("");
                me.getTertanggungModel().setKelurahan_kantor_tt("");
                me.getTertanggungModel().setKdpos_kantor_tt("");
                me.getTertanggungModel().setKdtelp1_kantor_tt("");
                me.getTertanggungModel().setTelp1_kantor_tt("");
                me.getTertanggungModel().setKdtelp2_kantor_tt("");
                me.getTertanggungModel().setTelp2_kantor_tt("");
                me.getTertanggungModel().setFax_kantor_tt("");
                me.getTertanggungModel().setAlamat_tinggal_tt("");
//                me.getTertanggungModel().setKota_tinggal_tt("");
                me.getTertanggungModel().setPropinsi_tinggal_tt("");
                me.getTertanggungModel().setKabupaten_tinggal_tt("");
                me.getTertanggungModel().setKecamatan_tinggal_tt("");
                me.getTertanggungModel().setKelurahan_tinggal_tt("");
                me.getTertanggungModel().setKdpos_tinggal_tt("");
                me.getTertanggungModel().setKdtelp1_tinggal_tt("");
                me.getTertanggungModel().setTelp1_kantor_tt("");
                me.getTertanggungModel().setKdtelp2_tinggal_tt("");
                me.getTertanggungModel().setTelp2_tinggal_tt("");
                me.getTertanggungModel().setFax_tinggal_tt("");
                me.getTertanggungModel().setHp1_tt("");
                me.getTertanggungModel().setHp2_tt("");
                me.getTertanggungModel().setEmail_tt("");
                me.getTertanggungModel().setPenghasilan_thn_tt("");
                me.getTertanggungModel().setKlasifikasi_pekerjaan_tt("");
                me.getTertanggungModel().setUraian_pekerjaan_tt("");
                me.getTertanggungModel().setJabatan_klasifikasi_tt("");
                me.getTertanggungModel().setGreencard_tt(0);
                me.getTertanggungModel().setAlias_tt("");
                me.getTertanggungModel().setNm_perusahaan_tt("");
                me.getTertanggungModel().setNpwp_tt("");
                me.getTertanggungModel().setDana_gaji_tt("");
                me.getTertanggungModel().setDana_tabungan_tt("");
                me.getTertanggungModel().setDana_warisan_tt("");
                me.getTertanggungModel().setDana_hibah_tt("");
                me.getTertanggungModel().setDana_lainnya_tt("");
                me.getTertanggungModel().setEdit_d_lainnya_tt("");
                me.getTertanggungModel().setTujuan_proteksi_tt("");
                me.getTertanggungModel().setTujuan_inves_tt("");
                me.getTertanggungModel().setTujuan_lainnya_tt("");
                me.getTertanggungModel().setEdit_t_lainnya_tt("");
            }
        }

        if (hub != 1 && hub != 7 && hub != 4 && hub != 2 && hub != 5) {
            // masih di tanyakan ke Mba Tities
        }
    }

    private void createPdf() throws FileNotFoundException, DocumentException {
        try {

            File docsFolder = new File(Environment.getExternalStorageDirectory() + "/Documents");
            if (!docsFolder.exists()) {
                docsFolder.mkdir();
//            Log.i(TAG, "Created a new directory for PDF");
            }

//            pdfFile = new File(docsFolder.getAbsolutePath(), "PDF STRING "+ me.getModelID().getSPAJ_ID_TAB()+".pdf");

            Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 14);
            Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 14,
                    Font.BOLD);

            pdfFile = new File(docsFolder.getAbsolutePath(), "HelloWorld.pdf");

            OutputStream output = new FileOutputStream(pdfFile);

            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, output);
            writer.setFullCompression();
            document.open();
            document.add(new Paragraph(getString(R.string.pemegang_polis) + "\n\n", normalFont));

            Phrase alamat_rmh = new Phrase(getString(R.string.alamat) + " " + "Rumah", normalFont);
            alamat_rmh.setTabSettings(new TabSettings(250f));
            alamat_rmh.add(Chunk.TABBING);
            alamat_rmh.add(new Chunk("     : " + me.getPemegangPolisModel().getAlamat_pp() + "\n", boldFont));
            document.add(alamat_rmh);

            ListDropDownString = new E_Select(getContext()).getLst_Propinsi_PP();
            Phrase provinsi = new Phrase(getString(R.string.provinsi), normalFont);
            provinsi.setTabSettings(new TabSettings(250f));
            provinsi.add(Chunk.TABBING);
            provinsi.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getPropinsi_pp())) + "\n", boldFont));
            document.add(provinsi);

            ListDropDownString = new E_Select(getContext()).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_pp());
            Phrase kabupaten = new Phrase(getString(R.string.kabupaten), normalFont);
            kabupaten.setTabSettings(new TabSettings(250f));
            kabupaten.add(Chunk.TABBING);
            kabupaten.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKabupaten_pp())) + "\n", boldFont));
            document.add(kabupaten);

            ListDropDownString = new E_Select(getContext()).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_pp());
            Phrase kecamatan = new Phrase(getString(R.string.kecamatan), normalFont);
            kecamatan.setTabSettings(new TabSettings(250f));
            kecamatan.add(Chunk.TABBING);
            kecamatan.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKecamatan_pp())) + "\n", boldFont));
            document.add(kecamatan);

            ListDropDownString = new E_Select(getContext()).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_pp());
            Phrase kelurahan = new Phrase(getString(R.string.kelurahan), normalFont);
            kelurahan.setTabSettings(new TabSettings(250f));
            kelurahan.add(Chunk.TABBING);
            kelurahan.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKelurahan_pp())) + "\n", boldFont));
            document.add(kelurahan);

            ListDropDownString = new E_Select(getContext()).getLst_Kodepos_PP(me.getPemegangPolisModel().getKelurahan_pp());
            Phrase kdpos_rmh = new Phrase(getString(R.string.kode_pos), normalFont);
            kdpos_rmh.setTabSettings(new TabSettings(250f));
            kdpos_rmh.add(Chunk.TABBING);
            kdpos_rmh.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString,(me.getPemegangPolisModel().getKdpos_pp())) + "\n", boldFont));
            document.add(kdpos_rmh);

            Phrase telp1_rmh = new Phrase(getString(R.string.telp_1), normalFont);
            telp1_rmh.setTabSettings(new TabSettings(250f));
            telp1_rmh.add(Chunk.TABBING);
            telp1_rmh.add(new Chunk("     : " + me.getPemegangPolisModel().getKdtelp1_pp() + " - " + me.getPemegangPolisModel().getTelp1_pp() + "\n", boldFont));
            document.add(telp1_rmh);

            Phrase telp2_rmh = new Phrase(getString(R.string.telp_2), normalFont);
            telp2_rmh.setTabSettings(new TabSettings(250f));
            telp2_rmh.add(Chunk.TABBING);
            telp2_rmh.add(new Chunk("     : " + me.getPemegangPolisModel().getKdtelp2_pp() + " - " + me.getPemegangPolisModel().getTelp2_pp() + "\n", boldFont));
            document.add(telp2_rmh);

            Phrase alamat_kntr = new Phrase(getString(R.string.alamat) + " " + "Kantor", normalFont);
            alamat_kntr.setTabSettings(new TabSettings(250f));
            alamat_kntr.add(Chunk.TABBING);
            alamat_kntr.add(new Chunk("     : " + me.getPemegangPolisModel().getAlamat_kantor_pp() + "\n", boldFont));
            document.add(alamat_kntr);

            ListDropDownString = new E_Select(getContext()).getLst_Propinsi_PP();
            Phrase provinsi_kntr = new Phrase(getString(R.string.provinsi), normalFont);
            provinsi_kntr.setTabSettings(new TabSettings(250f));
            provinsi_kntr.add(Chunk.TABBING);
            provinsi_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getPropinsi_kantor_pp())) + "\n", boldFont));
            document.add(provinsi_kntr);

            ListDropDownString = new E_Select(getContext()).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_kantor_pp());
            Phrase kabupaten_kntr = new Phrase(getString(R.string.kabupaten), normalFont);
            kabupaten_kntr.setTabSettings(new TabSettings(250f));
            kabupaten_kntr.add(Chunk.TABBING);
            kabupaten_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKabupaten_kantor_pp())) + "\n", boldFont));
            document.add(kabupaten_kntr);

            ListDropDownString = new E_Select(getContext()).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_kantor_pp());
            Phrase kecamatan_kntr = new Phrase(getString(R.string.kecamatan), normalFont);
            kecamatan_kntr.setTabSettings(new TabSettings(250f));
            kecamatan_kntr.add(Chunk.TABBING);
            kecamatan_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKecamatan_kantor_pp())) + "\n", boldFont));
            document.add(kecamatan_kntr);

            ListDropDownString = new E_Select(getContext()).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_kantor_pp());
            Phrase kelurahan_kntr = new Phrase(getString(R.string.kelurahan), normalFont);
            kelurahan_kntr.setTabSettings(new TabSettings(250f));
            kelurahan_kntr.add(Chunk.TABBING);
            kelurahan_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKelurahan_kantor_pp())) + "\n", boldFont));
            document.add(kelurahan_kntr);

            ListDropDownString = new E_Select(getContext()).getLst_Kodepos_PP(me.getPemegangPolisModel().getKelurahan_kantor_pp());
            Phrase kdpos_kntr = new Phrase(getString(R.string.kode_pos), normalFont);
            kdpos_kntr.setTabSettings(new TabSettings(250f));
            kdpos_kntr.add(Chunk.TABBING);
            kdpos_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKdpos_kantor_pp())) + "\n", boldFont));
            document.add(kdpos_kntr);

            Phrase telp1_kntr = new Phrase(getString(R.string.telp_1), normalFont);
            telp1_kntr.setTabSettings(new TabSettings(250f));
            telp1_kntr.add(Chunk.TABBING);
            telp1_kntr.add(new Chunk("     : " + me.getPemegangPolisModel().getKdtelp1_kantor_pp() + " - " + me.getPemegangPolisModel().getTelp1_kantor_pp() + "\n", boldFont));
            document.add(telp1_kntr);

            Phrase telp2_kntr = new Phrase(getString(R.string.telp_2), normalFont);
            telp2_kntr.setTabSettings(new TabSettings(250f));
            telp2_kntr.add(Chunk.TABBING);
            telp2_kntr.add(new Chunk("     : " + me.getPemegangPolisModel().getKdtelp2_kantor_pp() + " - " + me.getPemegangPolisModel().getTelp2_kantor_pp() + "\n", boldFont));
            document.add(telp2_kntr);

            Phrase alamat_tgl = new Phrase(getString(R.string.alamat) + " " + "Tinggal", normalFont);
            alamat_tgl.setTabSettings(new TabSettings(250f));
            alamat_tgl.add(Chunk.TABBING);
            alamat_tgl.add(new Chunk("     : " + me.getPemegangPolisModel().getAlamat_tinggal_pp() + "\n", boldFont));
            document.add(alamat_tgl);

            ListDropDownString = new E_Select(getContext()).getLst_Propinsi_PP();
            Phrase provinsi_tgl = new Phrase(getString(R.string.provinsi), normalFont);
            provinsi_tgl.setTabSettings(new TabSettings(250f));
            provinsi_tgl.add(Chunk.TABBING);
            provinsi_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getPropinsi_tinggal_pp())) + "\n", boldFont));
            document.add(provinsi_tgl);

            ListDropDownString = new E_Select(getContext()).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_tinggal_pp());
            Phrase kabupaten_tgl = new Phrase(getString(R.string.kabupaten), normalFont);
            kabupaten_tgl.setTabSettings(new TabSettings(250f));
            kabupaten_tgl.add(Chunk.TABBING);
            kabupaten_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKabupaten_tinggal_pp())) + "\n", boldFont));
            document.add(kabupaten_tgl);

            ListDropDownString = new E_Select(getContext()).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_tinggal_pp());
            Phrase kecamatan_tgl = new Phrase(getString(R.string.kecamatan), normalFont);
            kecamatan_tgl.setTabSettings(new TabSettings(250f));
            kecamatan_tgl.add(Chunk.TABBING);
            kecamatan_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKecamatan_tinggal_pp())) + "\n", boldFont));
            document.add(kecamatan_tgl);

            ListDropDownString = new E_Select(getContext()).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_tinggal_pp());
            Phrase kelurahan_tgl = new Phrase(getString(R.string.kelurahan), normalFont);
            kelurahan_tgl.setTabSettings(new TabSettings(250f));
            kelurahan_tgl.add(Chunk.TABBING);
            kelurahan_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKelurahan_tinggal_pp())) + "\n", boldFont));
            document.add(kelurahan_tgl);

            ListDropDownString = new E_Select(getContext()).getLst_Kodepos_PP(me.getPemegangPolisModel().getKelurahan_tinggal_pp());
            Phrase kdpos_tgl = new Phrase(getString(R.string.kode_pos), normalFont);
            kdpos_tgl.setTabSettings(new TabSettings(250f));
            kdpos_tgl.add(Chunk.TABBING);
            kdpos_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKdpos_tinggal_pp())) + "\n", boldFont));
            document.add(kdpos_tgl);

            Phrase telp1_tgl = new Phrase(getString(R.string.telp_1), normalFont);
            telp1_tgl.setTabSettings(new TabSettings(250f));
            telp1_tgl.add(Chunk.TABBING);
            telp1_tgl.add(new Chunk("     : " + me.getPemegangPolisModel().getKdtelp1_tinggal_pp() + " - " + me.getPemegangPolisModel().getTelp1_tinggal_pp() + "\n", boldFont));
            document.add(telp1_tgl);

            Phrase telp2_tgl = new Phrase(getString(R.string.telp_2), normalFont);
            telp2_tgl.setTabSettings(new TabSettings(250f));
            telp2_tgl.add(Chunk.TABBING);
            telp2_tgl.add(new Chunk("     : " + me.getPemegangPolisModel().getKdtelp2_tinggal_pp() + " - " + me.getPemegangPolisModel().getTelp2_tinggal_pp() + "\n", boldFont));
            document.add(telp2_tgl);


            document.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    private void onClickLanjut() {

        if (me.getModelID().getSPAJ_ID_TAB().equals("")) {
            MethodSupport.Alert(getActivity(), getString(R.string.title_error), getString(R.string.msg_save), 0);

        } else {

            if (!val_page()) {
                MethodSupport.Alert(getActivity(), getString(R.string.title_error), getString(R.string.msg_val), 0);

            } else {
                me.getPemegangPolisModel().setValidation(Validation());
                loadview();
//                ((E_MainActivity) getActivity()).onSave();
//                val_hubpptt();

                MyTask task = new MyTask();
                task.execute();

//                View cl_child_pp = getActivity().findViewById(R.id.cl_child_pp);
//                File file_bitmap = new File(
//                        getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//                if (!file_bitmap.exists()) {
//                    file_bitmap.mkdirs();
//                }

                //Create JPG
//                String fileName = "PP_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
//                Context context = getActivity();
//                MethodSupport.onCreateBitmap(cl_child_pp, scroll_pp, file_bitmap, fileName, context);

//                ((E_MainActivity) getActivity()).setFragment(new E_TertanggungFragment(), getResources().getString(R.string.tertanggung));


//                progressDialog = new ProgressDialog(getActivity());
//                progressDialog.setTitle("Melakukan perpindahan halaman");
//                progressDialog.setMessage("Mohon Menunggu...");
//                progressDialog.show();
//                progressDialog.setCancelable(false);
//                Runnable progressRunnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        MethodSupport.active_view(0, btn_lanjut);
//                        MethodSupport.active_view(0, iv_next);
//                        me.getPemegangPolisModel().setValidation(Validation());
//                        loadview();
//                        ((E_MainActivity) getActivity()).onSave();
//                        val_hubpptt();
//                        View cl_child_pp = getActivity().findViewById(R.id.cl_child_pp);
//                        File file_bitmap = new File(
//                                getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//                        if (!file_bitmap.exists()) {
//                            file_bitmap.mkdirs();
//                        }
//                        String fileName = "PP_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
//                        Context context = getActivity();
//                        MethodSupport.onCreateBitmap(cl_child_pp, scroll_pp, file_bitmap, fileName, context);
//                        ((E_MainActivity) getActivity()).setFragment(new E_TertanggungFragment(), getResources().getString(R.string.tertanggung));
//
//                        progressDialog.cancel();
//                    }
//                };

//                Handler pdCanceller = new Handler();
//                pdCanceller.postDelayed(progressRunnable, 5000);
                try {
                    createPdf();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (DocumentException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    private void onClickBack() {
//        MethodSupport.active_view(0, btn_lanjut);
        loadview();
        ((E_MainActivity) getActivity()).onBackPressed();
    }

    private class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
        }

        @Override
        protected Void doInBackground(Void... voids) {
//            val_hubpptt();
//            View cl_child_pp = getActivity().findViewById(R.id.cl_child_pp);
//            File file_bitmap = new File(
//                    getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//            if (!file_bitmap.exists()) {
//                file_bitmap.mkdirs();
//            }
//
////            String fileName = "PP_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
////            Context context = getActivity();
////            MethodSupport.onCreateBitmap(cl_child_pp, scroll_pp, file_bitmap, fileName, context);
//
//
//            ((E_MainActivity) getActivity()).setFragment(new E_TertanggungFragment(), getResources().getString(R.string.tertanggung));
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_PEMEGANG_POLIS);
            return null;
        }

        @Override
        protected void onPostExecute(Void params) {
            val_hubpptt();
            ((E_MainActivity) getActivity()).setFragment(new E_TertanggungFragment(), getResources().getString(R.string.tertanggung));
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }


    }

    /**
     * Register the eventbus. To listen when the selected product changed on P_Data Proposal
     */
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    /**
     * Unregister the eventbus. To listen when the selected product changed on P_Data Proposal
     */
    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    //Receive data from E_MainActivity when 'bayar via credit card' option menu selected
    //Agar data yg sebelumnya belum disimpan di pemegang polis saat bayar via credit card ditekan, data nya ter save
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PaymentEvent event) {
        if (event.isShouldOnlyExecutedByPemegangPolisFragment()){
            loadview();
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_PEMEGANG_POLIS);
        }
    }

    @BindView(R.id.tv_data_produk_pp)
    TextView tv_data_produk_pp;
    @BindView(R.id.tv_error_channel_distribusi_pp)
    TextView tv_error_channel_distribusi_pp;
    @BindView(R.id.tv_error_produk_pp)
    TextView tv_error_produk_pp;
    @BindView(R.id.tv_error_sub_produk_pp)
    TextView tv_error_sub_produk_pp;
    @BindView(R.id.tv_error_jns_spaj)
    TextView tv_error_jns_spaj;
    @BindView(R.id.tv_error_no_proposal_pp)
    TextView tv_error_no_proposal_pp;
    @BindView(R.id.tv_data_diri_pp)
    TextView tv_data_diri_pp;
    @BindView(R.id.tv_error_no_ciff_pp)
    TextView tv_error_no_ciff_pp;
    @BindView(R.id.tv_error_nama_pp)
    TextView tv_error_nama_pp;
    @BindView(R.id.tv_error_tempat_pp)
    TextView tv_error_tempat_pp;
    @BindView(R.id.tv_error_ttl_pp)
    TextView tv_error_ttl_pp;
    @BindView(R.id.tv_error_usia_pp)
    TextView tv_error_usia_pp;
    @BindView(R.id.tv_error_jekel_pp)
    TextView tv_error_jekel_pp;
    @BindView(R.id.tv_error_agama_pp)
    TextView tv_error_agama_pp;
    @BindView(R.id.tv_error_teks_agama_lain_pp)
    TextView tv_error_teks_agama_lain_pp;
    @BindView(R.id.tv_error_pendidikan_pp)
    TextView tv_error_pendidikan_pp;
    @BindView(R.id.tv_error_gelar_pp)
    TextView tv_error_gelar_pp;
    @BindView(R.id.tv_error_ibu_pp)
    TextView tv_error_ibu_pp;
    @BindView(R.id.tv_error_warganegara_pp)
    TextView tv_error_warganegara_pp;
    @BindView(R.id.tv_error_green_card_pp)
    TextView tv_error_green_card_pp;
    @BindView(R.id.tv_bukti_identitas_pp)
    TextView tv_bukti_identitas_pp;
    @BindView(R.id.tv_error_bukti_identitas_pp)
    TextView tv_error_bukti_identitas_pp;
    @BindView(R.id.tv_error_teks_bukti_lain_pp)
    TextView tv_error_teks_bukti_lain_pp;
    @BindView(R.id.tv_error_nomor_bukti_pp)
    TextView tv_error_nomor_bukti_pp;
    @BindView(R.id.tv_error_tgl_berlaku_pp)
    TextView tv_error_tgl_berlaku_pp;
    @BindView(R.id.tv_error_status_pp)
    TextView tv_error_status_pp;
    @BindView(R.id.tv_alamat_kantor_pp)
    TextView tv_alamat_kantor_pp;
    @BindView(R.id.tv_isi_alamat_kantor_pp)
    TextView tv_isi_alamat_kantor_pp;
    @BindView(R.id.tv_alamat_rumah_pp)
    TextView tv_alamat_rumah_pp;
    @BindView(R.id.tv_ubah_alamat_rumah_pp)
    TextView tv_ubah_alamat_rumah_pp;
    @BindView(R.id.tv_alamat_tempat_tinggal_pp)
    TextView tv_alamat_tempat_tinggal_pp;
    @BindView(R.id.tv_isi_alamat_tempat_tinggal_pp)
    TextView tv_isi_alamat_tempat_tinggal_pp;
    @BindView(R.id.tv_alamat_tagihan_pp)
    TextView tv_alamat_tagihan_pp;
    @BindView(R.id.tv_error_tagihan_pp)
    TextView tv_error_tagihan_pp;
    @BindView(R.id.tv_hp_email_pp)
    TextView tv_hp_email_pp;
    @BindView(R.id.tv_ubah_hp_email_pp)
    TextView tv_ubah_hp_email_pp;
    @BindView(R.id.tv_penghasilan_pp)
    TextView tv_penghasilan_pp;
    @BindView(R.id.tv_error_npwp_pp)
    TextView tv_error_npwp_pp;
    @BindView(R.id.tv_error_pendapatan_pp)
    TextView tv_error_pendapatan_pp;
    @BindView(R.id.tv_sumber_dana_pp)
    TextView tv_sumber_dana_pp;
    @BindView(R.id.tv_lain_lain_pp)
    TextView tv_lain_lain_pp;
    @BindView(R.id.tv_tujuan_pengajuan_asuransi_pp)
    TextView tv_tujuan_pengajuan_asuransi_pp;
    @BindView(R.id.tv_error_hubungancp_pp)
    TextView tv_error_hubungancp_pp;
    @BindView(R.id.tv_error_hubungan_pp)
    TextView tv_error_hubungan_pp;
    @BindView(R.id.tv_ubah_alamat_kantor_pp)
    TextView tv_ubah_alamat_kantor_pp;
    @BindView(R.id.tv_isi_alamat_rumah_pp)
    TextView tv_isi_alamat_rumah_pp;
    @BindView(R.id.tv_ubah_alamat_tempat_tinggal_pp)
    TextView tv_ubah_alamat_tempat_tinggal_pp;
    @BindView(R.id.tv_isi_hp_email_pp)
    TextView tv_isi_hp_email_pp;
    @BindView(R.id.tv_pemegangpolis_ibu_pelajar_pp)
    TextView tv_pemegangpolis_ibu_pelajar_pp;
    @BindView(R.id.tv_error_paket_pp)
    TextView tv_error_paket_pp;
    @BindView(R.id.tv_error_uraikan_tugas_pp)
    TextView tv_error_uraikan_tugas_pp;
    @BindView(R.id.tv_error_jabatan_pp)
    TextView tv_error_jabatan_pp;
    @BindView(R.id.tv_error_klasifikasi_pekerjaan_pp)
    TextView tv_error_klasifikasi_pekerjaan_pp;
    @BindView(R.id.tv_error_pekerjaan_pp)
    TextView tv_error_pekerjaan_pp;
    @BindView(R.id.tv_error_nama_perusahaan_pp)
    TextView tv_error_nama_perusahaan_pp;
    @BindView(R.id.tv_error_polis_pp)
    TextView tv_error_polis_pp;
    @BindView(R.id.tv_error_tagihan_korepondensi_pp)
    TextView tv_error_tagihan_korepondensi_pp;
    @BindView(R.id.tv_error_penghasilan_pertahun_ibu_pp)
    TextView tv_error_penghasilan_pertahun_ibu_pp;
    @BindView(R.id.tv_error_npwpibu_pp)
    TextView tv_error_npwpibu_pp;
    @BindView(R.id.tv_error_bidang_usaha_ibu_pp)
    TextView tv_error_bidang_usaha_ibu_pp;
    @BindView(R.id.tv_error_nama_perusahaan_ibu_pp)
    TextView tv_error_nama_perusahaan_ibu_pp;
    @BindView(R.id.tv_error_jabatan_ibu_pp)
    TextView tv_error_jabatan_ibu_pp;
    @BindView(R.id.tv_error_klasifikasi_pekerjaan_ibu_pp)
    TextView tv_error_klasifikasi_pekerjaan_ibu_pp;
    @BindView(R.id.tv_error_pekerjaanibu_pp)
    TextView tv_error_pekerjaanibu_pp;
    @BindView(R.id.tv_error_usia_ibu_pp)
    TextView tv_error_usia_ibu_pp;
    @BindView(R.id.tv_error_ttl_ibu_pp)
    TextView tv_error_ttl_ibu_pp;
    @BindView(R.id.tv_error_namaibu_pp)
    TextView tv_error_namaibu_pp;
    @BindView(R.id.tv_error_penghasilan_pertahun_ayah_pp)
    TextView tv_error_penghasilan_pertahun_ayah_pp;
    @BindView(R.id.tv_error_npwpayah_pp)
    TextView tv_error_npwpayah_pp;
    @BindView(R.id.tv_error_bidang_usaha_ayah_pp)
    TextView tv_error_bidang_usaha_ayah_pp;
    @BindView(R.id.tv_error_nama_perusahaan_ayah_pp)
    TextView tv_error_nama_perusahaan_ayah_pp;
    @BindView(R.id.tv_error_jabatan_ayah_pp)
    TextView tv_error_jabatan_ayah_pp;
    @BindView(R.id.tv_error_klasifikasi_pekerjaan_ayah_pp)
    TextView tv_error_klasifikasi_pekerjaan_ayah_pp;
    @BindView(R.id.tv_error_pekerjaanayah_pp)
    TextView tv_error_pekerjaanayah_pp;
    @BindView(R.id.tv_error_usia_ayah_pp)
    TextView tv_error_usia_ayah_pp;
    @BindView(R.id.tv_error_ttl_ayah_pp)
    TextView tv_error_ttl_ayah_pp;
    @BindView(R.id.tv_error_namaayah_pp)
    TextView tv_error_namaayah_pp;
    @BindView(R.id.tv_error_penghasilan_pertahun_pp)
    TextView tv_error_penghasilan_pertahun_pp;
    @BindView(R.id.tv_error_npwpsuami_pp)
    TextView tv_error_npwpsuami_pp;
    @BindView(R.id.tv_error_bidang_usaha_suami_pp)
    TextView tv_error_bidang_usaha_suami_pp;
    @BindView(R.id.tv_error_nama_perusahaan_suami_pp)
    TextView tv_error_nama_perusahaan_suami_pp;
    @BindView(R.id.tv_error_jabatan_suami_pp)
    TextView tv_error_jabatan_suami_pp;
    @BindView(R.id.tv_error_klasifikasi_pekerjaan_suami_pp)
    TextView tv_error_klasifikasi_pekerjaan_suami_pp;
    @BindView(R.id.tv_error_pekerjaansuami_pp)
    TextView tv_error_pekerjaansuami_pp;
    @BindView(R.id.tv_error_usia_suami_pp)
    TextView tv_error_usia_suami_pp;
    @BindView(R.id.tv_error_ttl_suami_pp)
    TextView tv_error_ttl_suami_pp;
    @BindView(R.id.tv_error_namasuami_pp)
    TextView tv_error_namasuami_pp;
    @BindView(R.id.tv_error_alias_pp)
    TextView tv_error_alias_pp;
    @BindView(R.id.tv_error_hp_email_pp)
    TextView tv_error_hp_email_pp;
    @BindView(R.id.tv_error_alamat_rumah_pp)
    TextView tv_error_alamat_rumah_pp;

    @BindView(R.id.cl_data_produk_pp)
    ConstraintLayout cl_data_produk_pp;
    @BindView(R.id.cl_form_data_produk_pp)
    ConstraintLayout cl_form_data_produk_pp;
    @BindView(R.id.cl_channel_distribusi_pp)
    ConstraintLayout cl_channel_distribusi_pp;
    @BindView(R.id.cl_produk_pp)
    ConstraintLayout cl_produk_pp;
    @BindView(R.id.cl_sub_produk_pp)
    ConstraintLayout cl_sub_produk_pp;
    @BindView(R.id.cl_jns_spaj)
    ConstraintLayout cl_jns_spaj;
    @BindView(R.id.cl_no_proposal_pp)
    ConstraintLayout cl_no_proposal_pp;
    @BindView(R.id.cl_data_diri_pp)
    ConstraintLayout cl_data_diri_pp;
    @BindView(R.id.cl_form_data_diri_pp)
    ConstraintLayout cl_form_data_diri_pp;
    @BindView(R.id.cl_no_ciff_pp)
    ConstraintLayout cl_no_ciff_pp;
    @BindView(R.id.cl_nama_pp)
    ConstraintLayout cl_nama_pp;
    @BindView(R.id.cl_tempat_pp)
    ConstraintLayout cl_tempat_pp;
    @BindView(R.id.cl_ttl_pp)
    ConstraintLayout cl_ttl_pp;
    @BindView(R.id.cl_usia_pp)
    ConstraintLayout cl_usia_pp;
    @BindView(R.id.cl_jekel_pp)
    ConstraintLayout cl_jekel_pp;
    @BindView(R.id.cl_agama_pp)
    ConstraintLayout cl_agama_pp;
    @BindView(R.id.cl_teks_agama_lain_pp)
    ConstraintLayout cl_teks_agama_lain_pp;
    @BindView(R.id.cl_pendidikan_pp)
    ConstraintLayout cl_pendidikan_pp;
    @BindView(R.id.cl_gelar_pp)
    ConstraintLayout cl_gelar_pp;
    @BindView(R.id.cl_ibu_pp)
    ConstraintLayout cl_ibu_pp;
    @BindView(R.id.cl_green_card_pp)
    ConstraintLayout cl_green_card_pp;
    @BindView(R.id.cl_warganegara_pp)
    ConstraintLayout cl_warganegara_pp;
    @BindView(R.id.cl_bukti_identitas_pp)
    ConstraintLayout cl_bukti_identitas_pp;
    @BindView(R.id.cl_form_bukti_identitas_pp)
    ConstraintLayout cl_form_bukti_identitas_pp;
    @BindView(R.id.cl_bukti_identitas_head_pp)
    ConstraintLayout cl_bukti_identitas_head_pp;
    @BindView(R.id.cl_teks_bukti_lain_pp)
    ConstraintLayout cl_teks_bukti_lain_pp;
    @BindView(R.id.cl_nomor_bukti_pp)
    ConstraintLayout cl_nomor_bukti_pp;
    @BindView(R.id.cl_tgl_berlaku_pp)
    ConstraintLayout cl_tgl_berlaku_pp;
    @BindView(R.id.cl_status_pp)
    ConstraintLayout cl_status_pp;
    @BindView(R.id.cl_alamat_kantor_pp)
    ConstraintLayout cl_alamat_kantor_pp;
    @BindView(R.id.cl_alamat_rumah_pp)
    ConstraintLayout cl_alamat_rumah_pp;
    @BindView(R.id.cl_alamat_tempat_tinggal_pp)
    ConstraintLayout cl_alamat_tempat_tinggal_pp;
    @BindView(R.id.cl_alamat_tagihan_pp)
    ConstraintLayout cl_alamat_tagihan_pp;
    @BindView(R.id.cl_form_alamat_tagihan_pp)
    ConstraintLayout cl_form_alamat_tagihan_pp;
    @BindView(R.id.cl_tagihan_pp)
    ConstraintLayout cl_tagihan_pp;
    @BindView(R.id.cl_hp_email_pp)
    ConstraintLayout cl_hp_email_pp;
    @BindView(R.id.cl_penghasilan_pp)
    ConstraintLayout cl_penghasilan_pp;
    @BindView(R.id.cl_form_penghasilan_pp)
    ConstraintLayout cl_form_penghasilan_pp;
    @BindView(R.id.cl_npwp_pp)
    ConstraintLayout cl_npwp_pp;
    @BindView(R.id.cl_pendapatan_pp)
    ConstraintLayout cl_pendapatan_pp;
    @BindView(R.id.cl_sumber_dana_pp)
    ConstraintLayout cl_sumber_dana_pp;
    @BindView(R.id.cl_sumber_dana2_pp)
    ConstraintLayout cl_sumber_dana2_pp;
    @BindView(R.id.cl_sumber_dana3_pp)
    ConstraintLayout cl_sumber_dana3_pp;
    @BindView(R.id.cl_sumber_dana4_pp)
    ConstraintLayout cl_sumber_dana4_pp;
    @BindView(R.id.cl_sumber_dana5_pp)
    ConstraintLayout cl_sumber_dana5_pp;
    @BindView(R.id.cl_sumber_dana6_pp)
    ConstraintLayout cl_sumber_dana6_pp;
    @BindView(R.id.cl_sumber_dana7_pp)
    ConstraintLayout cl_sumber_dana7_pp;
    @BindView(R.id.cl_lain_lain_pp)
    ConstraintLayout cl_lain_lain_pp;
    @BindView(R.id.cl_form_lain_lain_pp)
    ConstraintLayout cl_form_lain_lain_pp;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi_pp)
    ConstraintLayout cl_tujuan_pengajuan_asuransi_pp;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi2_pp)
    ConstraintLayout cl_tujuan_pengajuan_asuransi2_pp;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi3_pp)
    ConstraintLayout cl_tujuan_pengajuan_asuransi3_pp;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi4_pp)
    ConstraintLayout cl_tujuan_pengajuan_asuransi4_pp;
    @BindView(R.id.cl_hubungancp_pp)
    ConstraintLayout cl_hubungancp_pp;
    @BindView(R.id.cl_hubungan_pp)
    ConstraintLayout cl_hubungan_pp_dengan_tt_pp;
    @BindView(R.id.cl_pemegangpolis_ibu_pelajar_pp)
    ConstraintLayout cl_pemegangpolis_ibu_pelajar_pp;
    @BindView(R.id.cl_form_pemegangpolis_ibu_pelajar_pp)
    ConstraintLayout cl_form_pemegangpolis_ibu_pelajar_pp;
    @BindView(R.id.cl_namasuami_pp)
    ConstraintLayout cl_namasuami_pp;
    @BindView(R.id.cl_paket_pp)
    ConstraintLayout cl_paket_pp;
    @BindView(R.id.cl_uraikan_tugas_pp)
    ConstraintLayout cl_uraikan_tugas_pp;
    @BindView(R.id.cl_ttl_suami_pp)
    ConstraintLayout cl_ttl_suami_pp;
    @BindView(R.id.cl_usia_suami_pp)
    ConstraintLayout cl_usia_suami_pp;
    @BindView(R.id.cl_pekerjaansuami_pp)
    ConstraintLayout cl_pekerjaansuami_pp;
    @BindView(R.id.cl_klasifikasi_pekerjaan_suami_pp)
    ConstraintLayout cl_klasifikasi_pekerjaan_suami_pp;
    @BindView(R.id.cl_jabatan_suami_pp)
    ConstraintLayout cl_jabatan_suami_pp;
    @BindView(R.id.cl_nama_perusahaan_suami_pp)
    ConstraintLayout cl_nama_perusahaan_suami_pp;
    @BindView(R.id.cl_bidang_usaha_suami_pp)
    ConstraintLayout cl_bidang_usaha_suami_pp;
    @BindView(R.id.cl_npwpsuami_pp)
    ConstraintLayout cl_npwpsuami_pp;
    @BindView(R.id.cl_penghasilan_pertahun_pp)
    ConstraintLayout cl_penghasilan_pertahun_pp;
    @BindView(R.id.cl_form_pemegangpolis_ibu_pelajar2_pp)
    ConstraintLayout cl_form_pemegangpolis_ibu_pelajar2_pp;
    @BindView(R.id.cl_namaayah_pp)
    ConstraintLayout cl_namaayah_pp;
    @BindView(R.id.cl_ttl_ayah_pp)
    ConstraintLayout cl_ttl_ayah_pp;
    @BindView(R.id.cl_usia_ayah_pp)
    ConstraintLayout cl_usia_ayah_pp;
    @BindView(R.id.cl_pekerjaanayah_pp)
    ConstraintLayout cl_pekerjaanayah_pp;
    @BindView(R.id.cl_nama_perusahaan_ayah_pp)
    ConstraintLayout cl_nama_perusahaan_ayah_pp;
    @BindView(R.id.cl_bidang_usaha_ayah_pp)
    ConstraintLayout cl_bidang_usaha_ayah_pp;
    @BindView(R.id.cl_npwpayah_pp)
    ConstraintLayout cl_npwpayah_pp;
    @BindView(R.id.cl_penghasilan_pertahun_ayah_pp)
    ConstraintLayout cl_penghasilan_pertahun_ayah_pp;
    @BindView(R.id.cl_namaibu_pp)
    ConstraintLayout cl_namaibu_pp;
    @BindView(R.id.cl_ttl_ibu_pp)
    ConstraintLayout cl_ttl_ibu_pp;
    @BindView(R.id.cl_usia_ibu_pp)
    ConstraintLayout cl_usia_ibu_pp;
    @BindView(R.id.cl_pekerjaanibu_pp)
    ConstraintLayout cl_pekerjaanibu_pp;
    @BindView(R.id.cl_klasifikasi_pekerjaan_ibu_pp)
    ConstraintLayout cl_klasifikasi_pekerjaan_ibu_pp;
    @BindView(R.id.cl_nama_perusahaan_ibu_pp)
    ConstraintLayout cl_nama_perusahaan_ibu_pp;
    @BindView(R.id.cl_bidang_usaha_ibu_pp)
    ConstraintLayout cl_bidang_usaha_ibu_pp;
    @BindView(R.id.cl_npwpibu_pp)
    ConstraintLayout cl_npwpibu_pp;
    @BindView(R.id.cl_penghasilan_pertahun_ibu_pp)
    ConstraintLayout cl_penghasilan_pertahun_ibu_pp;
    @BindView(R.id.cl_form_korespondensi_polis_pp)
    ConstraintLayout cl_form_korespondensi_polis_pp;
    @BindView(R.id.cl_tagihan_korepondensi_pp)
    ConstraintLayout cl_tagihan_korepondensi_pp;
    @BindView(R.id.cl_nama_perusahaan_pp)
    ConstraintLayout cl_nama_perusahaan_pp;
    @BindView(R.id.cl_klasifikasi_pekerjaan_pp)
    ConstraintLayout cl_klasifikasi_pekerjaan_pp;
    @BindView(R.id.cl_jabatan_pp)
    ConstraintLayout cl_jabatan_pp;
    @BindView(R.id.cl_polis_pp)
    ConstraintLayout cl_polis_pp;
    @BindView(R.id.cl_jabatan_ayah_pp)
    ConstraintLayout cl_jabatan_ayah_pp;
    @BindView(R.id.cl_jabatan_ibu_pp)
    ConstraintLayout cl_jabatan_ibu_pp;
    @BindView(R.id.cl_klasifikasi_pekerjaan_ayah_pp)
    ConstraintLayout cl_klasifikasi_pekerjaan_ayah_pp;
    @BindView(R.id.cl_alias_pp)
    ConstraintLayout cl_alias_pp;

    @BindView(R.id.ll_alamat_rumah_pp)
    LinearLayout ll_alamat_rumah_pp;
    @BindView(R.id.ll_alamat_kantor_pp)
    LinearLayout ll_alamat_kantor_pp;
    @BindView(R.id.ll_alamat_tempat_tinggal_pp)
    LinearLayout ll_alamat_tempat_tinggal_pp;
    @BindView(R.id.ll_hp_email_pp)
    LinearLayout ll_hp_email_pp;

    @BindView(R.id.iv_circle_channel_distribusi_pp)
    ImageView iv_circle_channel_distribusi_pp;
    @BindView(R.id.iv_circle_produk_pp)
    ImageView iv_circle_produk_pp;
    @BindView(R.id.iv_circle_sub_produk_pp)
    ImageView iv_circle_sub_produk_pp;
    @BindView(R.id.iv_circle_jns_spaj)
    ImageView iv_circle_jns_spaj;
    @BindView(R.id.iv_circle_no_proposal_pp)
    ImageView iv_circle_no_proposal_pp;
    @BindView(R.id.iv_circle_no_ciff_pp)
    ImageView iv_circle_no_ciff_pp;
    @BindView(R.id.iv_circle_nama_pp)
    ImageView iv_circle_nama_pp;
    @BindView(R.id.iv_circle_tempat_pp)
    ImageView iv_circle_tempat_pp;
    @BindView(R.id.iv_circle_ttl_pp)
    ImageView iv_circle_ttl_pp;
    @BindView(R.id.iv_circle_usia_pp)
    ImageView iv_circle_usia_pp;
    @BindView(R.id.iv_circle_jekel_pp)
    ImageView iv_circle_jekel_pp;
    @BindView(R.id.iv_circle_agama_pp)
    ImageView iv_circle_agama_pp;
    @BindView(R.id.iv_circle_teks_agama_lain_pp)
    ImageView iv_circle_teks_agama_lain_pp;
    @BindView(R.id.iv_circle_pendidikan_pp)
    ImageView iv_circle_pendidikan_pp;
    @BindView(R.id.iv_circle_ibu_pp)
    ImageView iv_circle_ibu_pp;
    @BindView(R.id.iv_circle_warganegara_pp)
    ImageView iv_circle_warganegara_pp;
    @BindView(R.id.iv_circle_bukti_identitas_pp)
    ImageView iv_circle_bukti_identitas_pp;
    @BindView(R.id.iv_circle_teks_bukti_lain_pp)
    ImageView iv_circle_teks_bukti_lain_pp;
    @BindView(R.id.iv_circle_nomor_bukti_pp)
    ImageView iv_circle_nomor_bukti_pp;
    @BindView(R.id.iv_circle_tgl_berlaku_pp)
    ImageView iv_circle_tgl_berlaku_pp;
    @BindView(R.id.iv_circle_status_pp)
    ImageView iv_circle_status_pp;
    @BindView(R.id.iv_circle_tagihan_pp)
    ImageView iv_circle_tagihan_pp;
    @BindView(R.id.iv_circle_npwp_pp)
    ImageView iv_circle_npwp_pp;
    @BindView(R.id.iv_circle_pendapatan_pp)
    ImageView iv_circle_pendapatan_pp;
    @BindView(R.id.iv_circle_sumber_dana_pp)
    ImageView iv_circle_sumber_dana_pp;
    @BindView(R.id.iv_circle_sumber_dana)
    ImageView iv_circle_sumber_dana;
    @BindView(R.id.iv_circle_checkt_proteksi_pp)
    ImageView iv_circle_checkt_proteksi_pp;
    @BindView(R.id.iv_circle_hubungancp_pp)
    ImageView iv_circle_hubungancp_pp;
    @BindView(R.id.iv_circle_hubungan_pp)
    ImageView iv_circle_hubungan_pp;
    ImageView iv_circle_tagihan_korepondensi_pp;
    @BindView(R.id.iv_circle_polis_pp)
    ImageView iv_circle_polis_pp;
    @BindView(R.id.iv_circle_nama_perusahaan_pp)
    ImageView iv_circle_nama_perusahaan_pp;
    @BindView(R.id.iv_circle_klasifikasi_pekerjaan_pp)
    ImageView iv_circle_klasifikasi_pekerjaan_pp;
    @BindView(R.id.iv_circle_jabatan_pp)
    ImageView iv_circle_jabatan_pp;
    @BindView(R.id.iv_circle_uraikan_tugas_pp)
    ImageView iv_circle_uraikan_tugas_pp;
    @BindView(R.id.iv_circle_paket_pp)
    ImageView iv_circle_paket_pp;
    @BindView(R.id.iv_circle_alias_pp)
    ImageView iv_circle_alias_pp;
    @BindView(R.id.iv_circle_namaayah_pp)
    ImageView iv_circle_namaayah_pp;
    @BindView(R.id.iv_circle_ttl_ayah_pp)
    ImageView iv_circle_ttl_ayah_pp;
    @BindView(R.id.iv_circle_usia_ayah_pp)
    ImageView iv_circle_usia_ayah_pp;
    @BindView(R.id.iv_circle_klasifikasi_pekerjaan_ayah_pp)
    ImageView iv_circle_klasifikasi_pekerjaan_ayah_pp;
    @BindView(R.id.iv_circle_nama_perusahaan_ayah_pp)
    ImageView iv_circle_nama_perusahaan_ayah_pp;
    @BindView(R.id.iv_circle_bidang_usaha_ayah_pp)
    ImageView iv_circle_bidang_usaha_ayah_pp;
    @BindView(R.id.iv_circle_npwpayah_pp)
    ImageView iv_circle_npwpayah_pp;
    @BindView(R.id.iv_circle_namaibu_pp)
    ImageView iv_circle_namaibu_pp;
    @BindView(R.id.iv_circle_ttl_ibu_pp)
    ImageView iv_circle_ttl_ibu_pp;
    @BindView(R.id.iv_circle_usia_ibu_pp)
    ImageView iv_circle_usia_ibu_pp;
    @BindView(R.id.iv_circle_nama_perusahaan_ibu_pp)
    ImageView iv_circle_nama_perusahaan_ibu_pp;
    @BindView(R.id.iv_circle_bidang_usaha_ibu_pp)
    ImageView iv_circle_bidang_usaha_ibu_pp;
    @BindView(R.id.iv_circle_npwpibu_pp)
    ImageView iv_circle_npwpibu_pp;
    @BindView(R.id.iv_circle_jabatan_ayah_pp)
    ImageView iv_circle_jabatan_ayah_pp;
    @BindView(R.id.iv_circle_penghasilan_pertahun_ayah_pp)
    ImageView iv_circle_penghasilan_pertahun_ayah_pp;
    @BindView(R.id.iv_circle_klasifikasi_pekerjaan_ibu_pp)
    ImageView iv_circle_klasifikasi_pekerjaan_ibu_pp;
    @BindView(R.id.iv_circle_jabatan_ibu_pp)
    ImageView iv_circle_jabatan_ibu_pp;
    @BindView(R.id.iv_circle_penghasilan_pertahun_ibu_pp)
    ImageView iv_circle_penghasilan_pertahun_ibu_pp;
    @BindView(R.id.iv_circle_namasuami_pp)
    ImageView iv_circle_namasuami_pp;
    @BindView(R.id.iv_circle_ttl_suami_pp)
    ImageView iv_circle_ttl_suami_pp;
    @BindView(R.id.iv_circle_usia_suami_pp)
    ImageView iv_circle_usia_suami_pp;
    @BindView(R.id.iv_circle_klasifikasi_pekerjaan_suami_pp)
    ImageView iv_circle_klasifikasi_pekerjaan_suami_pp;
    @BindView(R.id.iv_circle_jabatan_suami_pp)
    ImageView iv_circle_jabatan_suami_pp;
    @BindView(R.id.iv_circle_nama_perusahaan_suami_pp)
    ImageView iv_circle_nama_perusahaan_suami_pp;
    @BindView(R.id.iv_circle_bidang_usaha_suami_pp)
    ImageView iv_circle_bidang_usaha_suami_pp;
    @BindView(R.id.iv_circle_npwpsuami_pp)
    ImageView iv_circle_npwpsuami_pp;
    @BindView(R.id.iv_circle_penghasilan_pertahun_pp)
    ImageView iv_circle_penghasilan_pertahun_pp;
    @BindView(R.id.iv_circle_green_card_pp)
    ImageView iv_circle_green_card_pp;
    @BindView(R.id.img_alamat_kantor_pp)
    ImageButton img_alamat_kantor_pp;
    @BindView(R.id.img_alamat_tempat_tinggal_pp)
    ImageButton img_alamat_tempat_tinggal_pp;
    @BindView(R.id.img_alamat_rumah_pp)
    ImageButton img_alamat_rumah_pp;
    @BindView(R.id.img_hp_email_pp)
    ImageButton img_hp_email_pp;

    @BindView(R.id.ac_channel_distribusi_pp)
    AutoCompleteTextView ac_channel_distribusi_pp;
    @BindView(R.id.ac_produk_pp)
    AutoCompleteTextView ac_produk_pp;
    @BindView(R.id.ac_sub_produk_pp)
    AutoCompleteTextView ac_sub_produk_pp;
    @BindView(R.id.ac_jns_spaj)
    AutoCompleteTextView ac_jns_spaj;
    @BindView(R.id.ac_agama_pp)
    AutoCompleteTextView ac_agama_pp;
    @BindView(R.id.ac_pendidikan_pp)
    AutoCompleteTextView ac_pendidikan_pp;
    @BindView(R.id.ac_bukti_identitas_pp)
    AutoCompleteTextView ac_bukti_identitas_pp;
    @BindView(R.id.ac_status_pp)
    AutoCompleteTextView ac_status_pp;
    @BindView(R.id.ac_tagihan_pp)
    AutoCompleteTextView ac_tagihan_pp;
    @BindView(R.id.ac_pendapatan_pp)
    AutoCompleteTextView ac_pendapatan_pp;
    @BindView(R.id.ac_hubungancp_pp)
    AutoCompleteTextView ac_hubungancp_pp;
    @BindView(R.id.ac_hubungan_pp)
    AutoCompleteTextView ac_hubungan_pp;
    @BindView(R.id.ac_warganegara_pp)
    AutoCompleteTextView ac_warganegara_pp;
    @BindView(R.id.ac_paket_pp)
    AutoCompleteTextView ac_paket_pp;
    @BindView(R.id.ac_jabatan_pp)
    AutoCompleteTextView ac_jabatan_pp;
    @BindView(R.id.ac_klasifikasi_pekerjaan_suami_pp)
    AutoCompleteTextView ac_klasifikasi_pekerjaan_suami_pp;
    @BindView(R.id.ac_jabatan_suami_pp)
    AutoCompleteTextView ac_jabatan_suami_pp;
    @BindView(R.id.ac_jabatan_ibu_pp)
    AutoCompleteTextView ac_jabatan_ibu_pp;
    @BindView(R.id.ac_penghasilan_pertahun_ibu_pp)
    AutoCompleteTextView ac_penghasilan_pertahun_ibu_pp;
    @BindView(R.id.ac_tagihan_korepondensi_pp)
    AutoCompleteTextView ac_tagihan_korepondensi_pp;
    @BindView(R.id.ac_polis_pp)
    AutoCompleteTextView ac_polis_pp;
    @BindView(R.id.ac_klasifikasi_pekerjaan_pp)
    AutoCompleteTextView ac_klasifikasi_pekerjaan_pp;
    @BindView(R.id.ac_jabatan_ayah_pp)
    AutoCompleteTextView ac_jabatan_ayah_pp;
    @BindView(R.id.ac_penghasilan_pertahun_ayah_pp)
    AutoCompleteTextView ac_penghasilan_pertahun_ayah_pp;
    @BindView(R.id.ac_klasifikasi_pekerjaan_ibu_pp)
    AutoCompleteTextView ac_klasifikasi_pekerjaan_ibu_pp;
    @BindView(R.id.ac_penghasilan_pertahun_pp)
    AutoCompleteTextView ac_penghasilan_pertahun_pp;
    @BindView(R.id.ac_klasifikasi_pekerjaan_ayah_pp)
    AutoCompleteTextView ac_klasifikasi_pekerjaan_ayah_pp;

    @BindView(R.id.et_no_proposal_pp)
    EditText et_no_proposal_pp;
    @BindView(R.id.et_no_ciff_pp)
    EditText et_no_ciff_pp;
    @BindView(R.id.et_nama_pp)
    EditText et_nama_pp;
    @BindView(R.id.et_alias_pp)
    EditText et_alias_pp;
    @BindView(R.id.et_tempat_pp)
    EditText et_tempat_pp;
    @BindView(R.id.et_ttl_pp)
    EditText et_ttl_pp;
    @BindView(R.id.et_usia_pp)
    EditText et_usia_pp;
    @BindView(R.id.et_teks_agama_lain_pp)
    EditText et_teks_agama_lain_pp;
    @BindView(R.id.et_gelar_pp)
    EditText et_gelar_pp;
    @BindView(R.id.et_ibu_pp)
    EditText et_ibu_pp;
    @BindView(R.id.et_teks_bukti_lain_pp)
    EditText et_teks_bukti_lain_pp;
    @BindView(R.id.et_nomor_bukti_pp)
    EditText et_nomor_bukti_pp;
    @BindView(R.id.et_tgl_berlaku_pp)
    EditText et_tgl_berlaku_pp;
    @BindView(R.id.et_npwp_pp)
    EditText et_npwp_pp;
    @BindView(R.id.et_editd_lainnya_pp)
    EditText et_editd_lainnya_pp;
    @BindView(R.id.et_editt_lainnya_pp)
    EditText et_editt_lainnya_pp;
    @BindView(R.id.et_namasuami_pp)
    EditText et_namasuami_pp;
    @BindView(R.id.et_ttl_suami_pp)
    EditText et_ttl_suami_pp;
    @BindView(R.id.et_usia_suami_pp)
    EditText et_usia_suami_pp;
    @BindView(R.id.et_pekerjaansuami_pp)
    EditText et_pekerjaansuami_pp;
    @BindView(R.id.et_nama_perusahaan_suami_pp)
    EditText et_nama_perusahaan_suami_pp;
    @BindView(R.id.et_bidang_usaha_suami_pp)
    EditText et_bidang_usaha_suami_pp;
    @BindView(R.id.et_npwpsuami_pp)
    EditText et_npwpsuami_pp;
    @BindView(R.id.et_namaayah_pp)
    EditText et_namaayah_pp;
    @BindView(R.id.et_ttl_ayah_pp)
    EditText et_ttl_ayah_pp;
    @BindView(R.id.et_usia_ayah_pp)
    EditText et_usia_ayah_pp;
    @BindView(R.id.et_pekerjaanayah_pp)
    EditText et_pekerjaanayah_pp;
    @BindView(R.id.et_npwpayah_pp)
    EditText et_npwpayah_pp;
    @BindView(R.id.et_bidang_usaha_ayah_pp)
    EditText et_bidang_usaha_ayah_pp;
    @BindView(R.id.et_nama_perusahaan_ayah_pp)
    EditText et_nama_perusahaan_ayah_pp;
    @BindView(R.id.et_namaibu_pp)
    EditText et_namaibu_pp;
    @BindView(R.id.et_ttl_ibu_pp)
    EditText et_ttl_ibu_pp;
    @BindView(R.id.et_usia_ibu_pp)
    EditText et_usia_ibu_pp;
    @BindView(R.id.et_pekerjaanibu_pp)
    EditText et_pekerjaanibu_pp;
    @BindView(R.id.et_nama_perusahaan_ibu_pp)
    EditText et_nama_perusahaan_ibu_pp;
    @BindView(R.id.et_bidang_usaha_ibu_pp)
    EditText et_bidang_usaha_ibu_pp;
    @BindView(R.id.et_npwpibu_pp)
    EditText et_npwpibu_pp;
    @BindView(R.id.et_nama_perusahaan_pp)
    EditText et_nama_perusahaan_pp;
    @BindView(R.id.et_uraikan_tugas_pp)
    EditText et_uraikan_tugas_pp;

    @BindView(R.id.rg_jekel_pp)
    RadioGroup rg_jekel_pp;
    @BindView(R.id.rb_pria_pp)
    RadioButton rb_pria_pp;
    @BindView(R.id.rb_wanita_pp)
    RadioButton rb_wanita_pp;

    @BindView(R.id.rg_green_card_pp)
    RadioGroup rg_green_card_pp;
    @BindView(R.id.rb_gc_y_pp)
    RadioButton rb_gc_y_pp;
    @BindView(R.id.rb_gc_n_pp)
    RadioButton rb_gc_n_pp;

    @BindView(R.id.cb_checkd_gaji_pp)
    CheckBox cb_checkd_gaji_pp;
    @BindView(R.id.cb_checkd_tabungan_pp)
    CheckBox cb_checkd_tabungan_pp;
    @BindView(R.id.cb_checkd_warisan_pp)
    CheckBox cb_checkd_warisan_pp;
    @BindView(R.id.cb_checkd_hibah_pp)
    CheckBox cb_checkd_hibah_pp;
    @BindView(R.id.cb_checkd_lainnya_pp)
    CheckBox cb_checkd_lainnya_pp;
    @BindView(R.id.cb_checkt_proteksi_pp)
    CheckBox cb_checkt_proteksi_pp;
    @BindView(R.id.cb_checkt_inves_pp)
    CheckBox cb_checkt_inves_pp;
    @BindView(R.id.cb_checkt_lainnya_pp)
    CheckBox cb_checkt_lainnya_pp;

    @BindView(R.id.scroll_pp)
    ScrollView scroll_pp;

    private Button btn_lanjut, btn_kembali;
    private ImageView iv_save, iv_next, iv_prev;
    private Toolbar second_toolbar;
    View polis;

    private EspajModel me;
    private PemegangPolisModel ppModel;

    private int ROLE_ID = 0;
    private int groupId = 0;
    private int jekel = -1;
    private int greencard = 0;
    private int hub = 0;
    private int bukti_pp = 0;
    private int wn_pp = 0;
    private int stat_pp = 0;
    private int agama = 0;
    private int pend_pp = 0;
    private int jnsspaj = 0;
    private int produk = 0;
    private int nm_produk = 0;
    private int kd_jns_produk = 0;
    private int paket = 0;
    private int JENIS_LOGIN = 0;
    private int JENIS_LOGIN_BC = 0;
    private int cp_pp = 0;
    private int tagihan = 0;
    int int_spinbisnis_cp = 0;
    @BindView(R.id.cl_child_pp)
    ConstraintLayout cl_child_pp;

    private String d_gaji_pp = "";
    private String d_tabungan_pp = "";
    private String d_warisan_pp = "";
    private String d_hibah_pp = "";
    private String d_lainnya_pp = "";
    private String edit_d_lainnya_pp = "";
    private String t_proteksi_pp = "";
    private String t_inves_pp = "";
    private String t_lainnya_pp = "";
    private String edit_t_lainnya = "";

    private String id_spaj = "";
    private String StrNama = "";
    private String pekerjaan = "";
    private String KODE_REG1 = "";
    private String KODE_REG2 = "";
    private String KODE_REG3 = "";
    private String str_penghasilan_suamithn_pp = "";
    private String str_penghasilan_ayah_pp = "";
    private String str_penghasilan_ibu_pp = "";
    private String str_jabatan_klasifikasi = "";
    private String str_jabatansuami_rt = "";
    private String str_jabatan_ayah = "";
    private String str_jabatan_ibu = "";
    private String str_pendapatan = "";
    private String str_tagihanrtn = "";
    private String str_krm_polis = "";

    private ArrayList<CheckBox> check_dana;
    private ArrayList<CheckBox> check_tujuan;
    private ArrayList<ModelDropdownInt> ListDropDownSPAJ;
    private ArrayList<ModelDropDownString> ListDropDownString;
    private ArrayList<DropboxModel> ListDropInt;
    private ArrayAdapter<String> Adapter;
    private ArrayList<ModelBank> ListDropDownBank;
    private ArrayList<ModelBankCab> ListDropDownBankCabang;
    private ModelDataDitunjukDI modelDataDitunjukDI;
    private ArrayList<ModelDropDownString> ListDropDownProvinsi;
    private ArrayList<ModelDropDownString> ListDropDownKabupaten;
    private ArrayList<ModelDropDownString> ListDropDownKecamatan;
    private ArrayList<ModelDropDownString> ListDropDownKelurahan;
    private ArrayList<ModelDropDownString> ListDropDownKodepos;

    private String[] str_value;

    private boolean bool_dana = false;
    private boolean bool_tujuan = false;

    private Calendar cal_pp, cal_ayah_pp, cal_ibu_pp, cal_suami_pp, cal_berlaku_pp;

    private ProgressDialog progressDialog;

    private File pdfFile;
    private PdfRenderer renderer;
    private ExtendedViewPager pager;

    private ArrayList<ModelJnsDanaDI> lst_Dana;
    private ModelAddRiderUA modelRider;
    private int produkRider = 0;

    private File file_foto;
    private File file_ttd;
    private ArrayList<ModelFoto> ListFoto;
}
