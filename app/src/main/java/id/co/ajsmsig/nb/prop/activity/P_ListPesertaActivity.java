package id.co.ajsmsig.nb.prop.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.hardcode.RequestCode;
import id.co.ajsmsig.nb.prop.adapter.P_AdapterListPeserta;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductPesertaModel;

public class P_ListPesertaActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "ListPesertaActivity";
    private ArrayList<P_MstProposalProductPesertaModel> listPesertaModel;
    private P_AdapterListPeserta pesertaAdapter;
    private boolean isRiderLadies;
    private int lsbsId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: status");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_activity_list_peserta);

        Bundle bundle = getIntent().getExtras();
        listPesertaModel = bundle.getParcelableArrayList(getString(R.string.peserta));
        isRiderLadies = bundle.getBoolean(getString(R.string.isLadies));
        lsbsId = bundle.getInt(getString(R.string.LSBS_ID));
        String nama = bundle.getString(getString(R.string.nama));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(nama);
        }


        findViewById(R.id.btn_hapus).setOnClickListener(this);
        findViewById(R.id.btn_ok).setOnClickListener(this);

        pesertaAdapter = new P_AdapterListPeserta(this,
                R.layout.p_fragment_topup_list,
                listPesertaModel,
                isRiderLadies);

        ListView lv_peserta = findViewById(R.id.lv_peserta);
        lv_peserta.setAdapter(pesertaAdapter);
        lv_peserta.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openFormPeserta(position);
            }
        });

    }

    private void beforeCloseMustSave() {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(getString(R.string.peserta), listPesertaModel);
        intent.putExtra(getString(R.string.LSBS_ID), lsbsId);
        setResult(RESULT_OK, intent);
    }

    @Override
    public void onBackPressed() {
        beforeCloseMustSave();
        finish();
    }

    public void openFormPeserta(int position) {
        Intent intent = new Intent(P_ListPesertaActivity.this, P_FormPesertaActivity.class);
        intent.putExtra(getString(R.string.isLadies), isRiderLadies);
        intent.putExtra(getString(R.string.MST_PROPOSAL_PRODUCT_PESERTA), (listPesertaModel.size() == position) ? new P_MstProposalProductPesertaModel() : listPesertaModel.get(position));
        intent.putExtra(getString(R.string.FIELD_TYPE), position);
        intent.putExtra(getString(R.string.LSBS_ID), lsbsId);
        startActivityForResult(intent, RequestCode.FILL_FORM_PESERTA_PROPOSAL);
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: status");
        super.onResume();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_peserta_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_add:
                if (pesertaAdapter.getCount() == 4) {
                    if (lsbsId == 826 || lsbsId == 819 || lsbsId == 831) {
                        showInfoMaxPeserta();
                    } else {
                        openFormPeserta(listPesertaModel.size());
                    }
                } else {
                    openFormPeserta(listPesertaModel.size());
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showInfoMaxPeserta() {
        AlertDialog.Builder msgBox = new AlertDialog.Builder(this);
        msgBox.setMessage("Jumlah peserta pada rider ini maksimal 4");
        msgBox.setNegativeButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //no code
            }
        });
        msgBox.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.btn_ok:
                onBackPressed();
                break;
            case R.id.btn_hapus:
                int lastPosition = listPesertaModel.size() - 1;
                if (lastPosition >= 0) {
                    listPesertaModel.remove(lastPosition);
                    pesertaAdapter.updateListTP(listPesertaModel);
                }
                break;
            default:
                throw new IllegalArgumentException("salah id");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        switch (requestCode) {
            case RequestCode.FILL_FORM_PESERTA_PROPOSAL:
                // Make sure the request was successful
                if (resultCode == RESULT_OK) {
                    Bundle mBundle = data.getExtras();
                    P_MstProposalProductPesertaModel pesertaModel = mBundle.getParcelable(getString(R.string.MST_PROPOSAL_PRODUCT_PESERTA));
                    int mPos = mBundle.getInt(getString(R.string.FIELD_TYPE));

                    if (listPesertaModel.size() == mPos) {
                        listPesertaModel.add(pesertaModel);
                    } else {
                        listPesertaModel.set(mPos, pesertaModel);
                    }
                    Log.d(TAG, "onActivityResult: size array  " + listPesertaModel.size());
                    pesertaAdapter.updateListTP(listPesertaModel);
                }
                break;
            default:
                break;
        }
    }
}
