package id.co.ajsmsig.nb.crm.fragment.listlead;

public class AgencyLeadModel {
    private long id;
    private String leadName;
    private int leadAge;
    private int leadGender;
    private boolean isLeadFavorite;
    private String createdDate;
    private String updatedDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLeadName() {
        return leadName;
    }

    public void setLeadName(String leadName) {
        this.leadName = leadName;
    }

    public int getLeadAge() {
        return leadAge;
    }

    public void setLeadAge(int leadAge) {
        this.leadAge = leadAge;
    }

    public int getLeadGender() {
        return leadGender;
    }

    public void setLeadGender(int leadGender) {
        this.leadGender = leadGender;
    }

    public boolean isLeadFavorite() {
        return isLeadFavorite;
    }

    public void setLeadFavorite(boolean leadFavorite) {
        isLeadFavorite = leadFavorite;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }


}
