package id.co.ajsmsig.nb.pointofsale.ui.dneedanalysis


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosFragmentNeedAnalysisBinding
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import id.co.ajsmsig.nb.pointofsale.ui.fragment.SingleOptionFragment

/**
 * A simple [Fragment] subclass.
 */
class NeedAnalysisFragment : SingleOptionFragment() {

    private lateinit var dataBinding: PosFragmentNeedAnalysisBinding

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(NeedAnalysisVM::class.java)
        subscribeUi(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_need_analysis, container, false)

        return dataBinding.root
    }

    private fun subscribeUi(viewModel: NeedAnalysisVM) {
        dataBinding.vm = viewModel

        viewModel.page = page
        mainVM.hideSubtitle()

    }

    override fun onDestroy() {
        super.onDestroy()

        sharedViewModel.selectedNeedAnalysis = null
    }

    override fun optionSelected(): PageOption? {
        val viewModel = ViewModelProviders.of(this).get(NeedAnalysisVM::class.java)

        val selectedOption = viewModel.observablePageOptions.value?.firstOrNull { it.selected.get() }
        sharedViewModel.selectedNeedAnalysis = selectedOption

        return selectedOption
    }

}// Required empty public constructor
