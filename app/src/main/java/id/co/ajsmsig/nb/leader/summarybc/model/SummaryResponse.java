
package id.co.ajsmsig.nb.leader.summarybc.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SummaryResponse implements Parcelable {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<SummaryData> data;

    protected SummaryResponse(Parcel in) {
        byte tmpError = in.readByte();
        error = tmpError == 0 ? null : tmpError == 1;
        message = in.readString();
        data = in.createTypedArrayList(SummaryData.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (error == null ? 0 : error ? 1 : 2));
        dest.writeString(message);
        dest.writeTypedList(data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SummaryResponse> CREATOR = new Creator<SummaryResponse>() {
        @Override
        public SummaryResponse createFromParcel(Parcel in) {
            return new SummaryResponse(in);
        }

        @Override
        public SummaryResponse[] newArray(int size) {
            return new SummaryResponse[size];
        }
    };

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SummaryData> getData() {
        return data;
    }

    public void setData(List<SummaryData> data) {
        this.data = data;
    }

}
