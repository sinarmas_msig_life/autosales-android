package id.co.ajsmsig.nb.crm.model.hardcode;
/*
 Created by faiz_f on 01/08/2017.
 */

public class CrmTableCode {
    public static int USER = 1;
    public static int LEAD = 2;
    public static int ACCOUNT = 3;
    public static int CAMPAIGN = 4;
    public static int PRODUCT = 5;
    public static int SPAJ = 6;
    public static int ACTIVITY = 7;
    public static int ADDRESS = 8;
    public static int MAINMENU = 9;
    public static int ACCESS = 10;
}
