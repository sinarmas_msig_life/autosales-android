package id.co.ajsmsig.nb.pointofsale.ui.eneedcalculator


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import id.co.ajsmsig.nb.R
import android.view.ViewGroup

import id.co.ajsmsig.nb.databinding.PosFragmentNeedCalculatorBinding
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.fragment.BaseFragment
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class NeedCalculatorFragment : BaseFragment() {

    lateinit var dataBinding: PosFragmentNeedCalculatorBinding

    @Inject lateinit var populatedRepository: PrePopulatedRepository

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(NeedCalculatorVM::class.java)
        subscribeUi(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_need_calculator, container, false)

        return dataBinding.root
    }

    private fun subscribeUi(viewModel: NeedCalculatorVM) {
        dataBinding.vm = viewModel

        viewModel.page = page
        viewModel.observableInputs.observe(this, Observer {
            it?.let {
                if (sharedViewModel.needCalculatorInputs != null) {
                    viewModel.inputs.set(sharedViewModel.needCalculatorInputs)
                } else {
                    it.forEach {
                        val inputAllChoice = it
                        it.input?.defaultValue?.let { inputAllChoice.selectedValue.set(it.toLong()) }

                    }
                    viewModel.inputs.set(it)
                }
            }
        })


        if (sharedViewModel.selectedNeedAnalysis != null) {
            mainVM.subtitle.set(sharedViewModel.selectedNeedAnalysis?.title)

        }
    }

    override fun onDestroy() {
        super.onDestroy()

        sharedViewModel.needCalculatorInputs = null
    }

    override fun canNext(): Boolean {

        val viewModel = ViewModelProviders.of(this).get(NeedCalculatorVM::class.java)
        sharedViewModel.needCalculatorInputs = ArrayList()
        viewModel.inputs.get()?.forEach {
            sharedViewModel.needCalculatorInputs?.add(it)

            val inputAllChoices = it

            it.input?.let {

                val input = it
                input.formulaRepresentation?.let {
                    val key = it
                    inputAllChoices.selectedValue.get()?.let { sharedViewModel.inputs[key] = it.toString() }
                    inputAllChoices.selectedChoice.get()?.let { sharedViewModel.inputs[key] = it.text  }
                }
            }

            if (it.input?.description?.toLowerCase()?.contains("usia anda") == true) {
                sharedViewModel.leadAge = it.selectedValue.get()?.toInt()
            }
            it.selectedChoice.get()?.let { Log.i("INFO", it.text + " ${it.value ?: ""}");  }
            it.selectedValue.get()?.let { Log.i("INFO", "$it") }

            if (it.selectedChoice.get() == null && it.selectedValue.get() == null) return false

        }
        return super.canNext()
    }

    override fun validationMessageIDRes(): Int? {
        return R.string.validation_message
    }
}// Required empty public constructor
