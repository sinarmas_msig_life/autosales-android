package id.co.ajsmsig.nb.pointofsale.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosRowSelectionBinding
import id.co.ajsmsig.nb.pointofsale.handler.SelectionHandler
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption

/**
 * Created by andreyyoshuamanik on 08/02/18.
 */
class SelectionAdapter(val handler: SelectionHandler): RecyclerView.Adapter<SelectionAdapter.SelectionViewHolder>() {

    var menus: Array<PageOption>? = null
    set(value) {
        field = value
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectionViewHolder {

        val dataBinding = DataBindingUtil.inflate<PosRowSelectionBinding>(LayoutInflater.from(parent.context), R.layout.pos_row_selection, parent, false)
        return SelectionViewHolder(dataBinding)
    }

    override fun getItemCount(): Int {
        return menus?.size ?: 0
    }

    override fun onBindViewHolder(holder: SelectionViewHolder, position: Int) {
        menus?.let {
            holder.bindMenu(it[position])
        }
    }


    // View Holder
    inner class SelectionViewHolder(private val dataBinding: PosRowSelectionBinding): RecyclerView.ViewHolder(dataBinding.root) {

        fun bindMenu(menu: PageOption) {

            dataBinding.vm = menu
            dataBinding.listener = handler
        }
    }
}