package id.co.ajsmsig.nb.prop.model.modelCalcIllustration;

public class Prop_Model_LimitVO
{
    private int down;
    private int up;

    public Prop_Model_LimitVO(int down, int up )
    {
        this.up = up;
        this.down = down;
    }

    public int getDown()
    {
        return down;
    }

    public void setDown( int down )
    {
        this.down = down;
    }

    public int getUp()
    {
        return up;
    }

    public void setUp( int up )
    {
        this.up = up;
    }


}
