package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.InputType


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface InputTypeDao {
    @Query("SELECT * from InputType")
    fun loadAllInputTypes(): LiveData<List<InputType>>

    @Query("SELECT * from InputType where type = :type limit 1")
    fun getInputTypeWith(type: String?): LiveData<InputType>

}