package id.co.ajsmsig.nb.crm.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.fragment.listlead.AgencyLeadModel;
import id.co.ajsmsig.nb.crm.model.ActivityListModel;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.BcBranch;
import id.co.ajsmsig.nb.crm.model.form.SimbakAccountModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakActivityModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakAddressModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakLeadModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakLinkAccToLeadModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakProductModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakSpajModel;
import id.co.ajsmsig.nb.crm.model.list.ListActivityModel;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.util.DateUtils;

/**
 * Created by faiz_f on 17/03/2017.
 */

public class C_Select {
    private Context context;
    private DBHelper dbHelper;

    public C_Select(Context context) {
        this.context = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.update_data_preferences), Context.MODE_PRIVATE);
        int version = sharedPreferences.getInt(context.getString(R.string.lav_data_id), 1);
        this.dbHelper = new DBHelper(context, version);
    }

    public boolean isLeadExist(String sl_name, String sl_bplace) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.IS_LEAD_EXIST).
                replace("#{sl_name}", "'" + sl_name + "'").
                replace("#{sl_bplace}", "'" + sl_bplace + "'");
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();

        cursor.close();

        return count > 0;
    }


    public String getCurrentDatabaseSchemaVersion() {

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "select data_version from LST_APP_VERSION order by lav_data_id  desc limit 1";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            return cursor.getString(cursor.getColumnIndexOrThrow("DATA_VERSION"));
        }
        cursor.close();
        db.close();

        return null;
    }

    //    select data lead for list
    public ArrayList<SimbakLeadModel> selectListLead(String KODE_AGEN) {
        ArrayList<SimbakLeadModel> simbakLeadModels = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String query = context.getString(R.string.SELECT_LIST_LEAD).
                replace("{#KODE_AGEN}", KODE_AGEN);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            SimbakLeadModel simbakLeadModel = new SimbakLeadModel();
            simbakLeadModel.setSL_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SL_TAB_ID))));
            simbakLeadModel.setSL_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SL_ID))));
            simbakLeadModel.setSL_NAME(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NAME))));
            simbakLeadModel.setSL_BDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BDATE))));
            simbakLeadModel.setSL_BAC_CURR_BRANCH(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_CURR_BRANCH))));
            simbakLeadModel.setSL_FAV(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_FAV))));

            simbakLeadModels.add(simbakLeadModel);
            cursor.moveToNext();
        }
        cursor.close();
        return simbakLeadModels;
    }

    public DropboxModel selectDropDown(String tableName, int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        DropboxModel dropboxModel = new DropboxModel();

        String query = context.getString(R.string.SELECT_JSON_DROPDOWN_CRM).
                replace("#table", tableName).
                replace("{#ID}", String.valueOf(id));
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            dropboxModel.setId(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.ID))));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LABEL))));
            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModel;
    }

    public DropboxModel selectDropDown(String tableName, String id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        DropboxModel dropboxModel = new DropboxModel();

        String query = context.getString(R.string.SELECT_JSON_DROPDOWN_CRM).
                replace("#table", tableName).
                replace("{#ID}", "'" + id + "'");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            dropboxModel.setIdString(cursor.getString(cursor.getColumnIndex(context.getString(R.string.ID))));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LABEL))));
            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModel;
    }

    public ArrayList<DropboxModel> selectDropDownList(String tableName) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();
        String query;
        switch (tableName) {
            case "JSON_ID_TYPE":
                query = context.getString(R.string.SELECT_LIST_JSON_DROPDOWN_CRM_ID_TYPE).
                        replace("#table", tableName);
                break;
            default:
                query = context.getString(R.string.SELECT_LIST_JSON_DROPDOWN_CRM).
                        replace("#table", tableName);
                break;
        }

        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setId(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.ID))));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LABEL))));
            dropboxModels.add(dropboxModel);
            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModels;
    }

    public ArrayList<DropboxModel> selectDropDownList(
            String tableName, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();

        Cursor cursor = new QueryUtil(context).query(tableName, projection, selection, selectionArgs, sortOrder);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setId(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.ID))));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LABEL))));
            dropboxModels.add(dropboxModel);
            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModels;
    }

    public SimbakProductModel selectSimbakProductModel(Long SLP_TAB_ID) {
        SimbakProductModel simbakProductModel = new SimbakProductModel();
        String tableName = context.getString(R.string.TABLE_SIMBAK_LIST_PRODUCT);
        String selection = context.getString(R.string.SLP_TAB_ID) + " = ?";
        String[] selectionArgs = new String[]{String.valueOf(SLP_TAB_ID)};

        Cursor cursor = new QueryUtil(context).query(tableName, null, selection, selectionArgs, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_TAB_ID))))
                simbakProductModel.setSLP_TAB_ID(cursor.getLong(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_ID))))
                simbakProductModel.setSLP_ID(cursor.getLong(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_ID))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_INST_ID))))
                simbakProductModel.setSLP_INST_ID(cursor.getLong(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_INST_ID))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_INST_TAB_ID))))
                simbakProductModel.setSLP_INST_TAB_ID(cursor.getLong(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_INST_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_LSBS_ID))))
                simbakProductModel.setSLP_LSBS_ID(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_LSBS_ID))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_LSBS_NO))))
                simbakProductModel.setSLP_LSBS_NO(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_LSBS_NO))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_NAME))))
                simbakProductModel.setSLP_NAME(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_NAME))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_LAST_PREMI))))
                simbakProductModel.setSLP_LAST_PREMI(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_LAST_PREMI))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_OLD_ID))))
                simbakProductModel.setSLP_OLD_ID(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_OLD_ID))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_INST_TYPE))))
                simbakProductModel.setSLP_INST_TYPE(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_INST_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_CORP_STAGE))))
                simbakProductModel.setSLP_CORP_STAGE(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_CORP_STAGE))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_ACTIVE))))
                simbakProductModel.setSLP_ACTIVE(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_ACTIVE))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_BRANCH))))
                simbakProductModel.setSLP_BRANCH(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_BRANCH))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_CAMPAIGN))))
                simbakProductModel.setSLP_CAMPAIGN(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_CAMPAIGN))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_CREATED))))
                simbakProductModel.setSLP_CREATED(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_CREATED))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_CRTD_ID))))
                simbakProductModel.setSLP_CRTD_ID(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_CRTD_ID))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_UPDATED))))
                simbakProductModel.setSLP_UPDATED(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_UPDATED))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_UPDTD_ID))))
                simbakProductModel.setSLP_UPDTD_ID(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_UPDTD_ID))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_LAST_PROCESS_LOCATION))))
                simbakProductModel.setSLP_LAST_PROCESS_LOCATION(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.SLP_LAST_PROCESS_LOCATION))));
            cursor.moveToNext();
        }
        cursor.close();

        return simbakProductModel;
    }

    public SimbakSpajModel selectSimbakSpajModelByActivityId(Long SLA_TAB_ID) {
        String tableName = context.getString(R.string.TABLE_SIMBAK_LIST_SPAJ);
        String selection = context.getString(R.string.SLS_INST_TAB_ID) + " = ?";
        String[] selectionArgs = new String[]{String.valueOf(SLA_TAB_ID)};

        Cursor cursor = new QueryUtil(context).query(tableName, null, selection, selectionArgs, null);

        return new SimbakSpajTable(context).getObject(cursor);
    }

    public ArrayList<DropboxModel> selectDropDownListSubAktivity(
            String tableName, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();

        Cursor cursor = new QueryUtil(context).query(tableName, projection, selection, selectionArgs, sortOrder);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setId(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.ID))));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LABEL))));
            dropboxModels.add(dropboxModel);
            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModels;
    }

    public ArrayList<DropboxModel> selectDropDownListIdString(String tableName) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();

        String query = context.getString(R.string.SELECT_LIST_JSON_DROPDOWN_CRM).
                replace("#table", tableName);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setIdString(cursor.getString(cursor.getColumnIndex(context.getString(R.string.ID))));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LABEL))));
            dropboxModels.add(dropboxModel);
            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModels;
    }

    public ArrayList<DropboxModel> selectDropDownListIdString(String tableName, String args1) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();

        String query = context.getString(R.string.SELECT_LIST_JSON_DROPDOWN_CRM_WITH_ARGUMEN).
                replace("#table", tableName).
                replace("#{CABANG_ID}", args1);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setIdString(cursor.getString(cursor.getColumnIndex(context.getString(R.string.ID))));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LABEL))));
            dropboxModels.add(dropboxModel);
            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModels;
    }

    public SimbakLeadModel selectSimbakLead(long SL_TAB_ID) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        SimbakLeadModel simbakLeadModel = new SimbakLeadModel();
        String query = context.getString(R.string.SELECT_SIMBAK_LEAD).
                replace("#{SL_TAB_ID}", String.valueOf(SL_TAB_ID));
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_TAB_ID))))
                simbakLeadModel.setSL_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SL_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ID))))
                simbakLeadModel.setSL_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SL_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NAME))))
                simbakLeadModel.setSL_NAME(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NAME))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_APP))))
                simbakLeadModel.setSL_APP(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_APP))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CREATED))))
                simbakLeadModel.setSL_CREATED(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_CREATED))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CRTD_DATE))))
                simbakLeadModel.setSL_CRTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_CRTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_UPDATED))))
                simbakLeadModel.setSL_UPDATED(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_UPDATED))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_UPTD_DATE))))
                simbakLeadModel.setSL_UPTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_UPTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ACTIVE))))
                simbakLeadModel.setSL_ACTIVE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_ACTIVE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_RATE))))
                simbakLeadModel.setSL_RATE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_RATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_REMARK))))
                simbakLeadModel.setSL_REMARK(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_REMARK))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_INIT))))
                simbakLeadModel.setSL_INIT(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_INIT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_SRC))))
                simbakLeadModel.setSL_SRC(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_SRC))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CHAR))))
                simbakLeadModel.setSL_CHAR(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CHAR))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_STATE))))
                simbakLeadModel.setSL_STATE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_STATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MOTHER))))
                simbakLeadModel.setSL_MOTHER(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_MOTHER))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CITIZEN))))
                simbakLeadModel.setSL_CITIZEN(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CITIZEN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CORP_POS))))
                simbakLeadModel.setSL_CORP_POS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CORP_POS))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_GENDER))))
                simbakLeadModel.setSL_GENDER(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_GENDER))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_RELIGION))))
                simbakLeadModel.setSL_RELIGION(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_RELIGION))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PERSONAL_REL))))
                simbakLeadModel.setSL_PERSONAL_REL(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_PERSONAL_REL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BPLACE))))
                simbakLeadModel.setSL_BPLACE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BPLACE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_LAST_EDU))))
                simbakLeadModel.setSL_LAST_EDU(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_LAST_EDU))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_POLIS_PURPOSE))))
                simbakLeadModel.setSL_POLIS_PURPOSE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_POLIS_PURPOSE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PAYMENT_SOURCE))))
                simbakLeadModel.setSL_PAYMENT_SOURCE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_PAYMENT_SOURCE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OTHER_TYPE))))
                simbakLeadModel.setSL_OTHER_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_OTHER_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_IDNO))))
                simbakLeadModel.setSL_IDNO(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_IDNO))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BDATE))))
                simbakLeadModel.setSL_BDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BDATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_UMUR))))
                simbakLeadModel.setSL_UMUR(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_UMUR))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CSTMR_FUND_TYPE))))
                simbakLeadModel.setSL_CSTMR_FUND_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CSTMR_FUND_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_IDTYPE))))
                simbakLeadModel.setSL_IDTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_IDTYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_RES_PREMIUM))))
                simbakLeadModel.setSL_RES_PREMIUM(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_RES_PREMIUM))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_SALARY))))
                simbakLeadModel.setSL_SALARY(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_SALARY))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF))))
                simbakLeadModel.setSL_BAC_REFF(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BUYER_FUND_TYPE))))
                simbakLeadModel.setSL_BUYER_FUND_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_BUYER_FUND_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PEND_ID))))
                simbakLeadModel.setSL_PEND_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_PEND_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_APPROVE))))
                simbakLeadModel.setSL_APPROVE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_APPROVE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_REQUEST))))
                simbakLeadModel.setSL_REQUEST(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_REQUEST))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ALASAN))))
                simbakLeadModel.setSL_ALASAN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_ALASAN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NO_NASABAH))))
                simbakLeadModel.setSL_NO_NASABAH(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NO_NASABAH))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_APPROVER))))
                simbakLeadModel.setSL_APPROVER(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_APPROVER))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_FIRST_BRANCH))))
                simbakLeadModel.setSL_BAC_FIRST_BRANCH(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_FIRST_BRANCH))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_CURR_BRANCH))))
                simbakLeadModel.setSL_BAC_CURR_BRANCH(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_CURR_BRANCH))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MALL_GUESTCODE))))
                simbakLeadModel.setSL_MALL_GUESTCODE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MALL_GUESTCODE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MALL_MALLCODE))))
                simbakLeadModel.setSL_MALL_MALLCODE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MALL_MALLCODE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MALL_ROOMCODE))))
                simbakLeadModel.setSL_MALL_ROOMCODE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MALL_ROOMCODE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ROOMOUT))))
                simbakLeadModel.setSL_ROOMOUT(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_ROOMOUT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CORRESPONDENT))))
                simbakLeadModel.setSL_CORRESPONDENT(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CORRESPONDENT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ROOMIN))))
                simbakLeadModel.setSL_ROOMIN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_ROOMIN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF_TITLE))))
                simbakLeadModel.setSL_BAC_REFF_TITLE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF_TITLE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OCCUPATION))))
                simbakLeadModel.setSL_OCCUPATION(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_OCCUPATION))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_IDEXP))))
                simbakLeadModel.setSL_IDEXP(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_IDEXP))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NPWP))))
                simbakLeadModel.setSL_NPWP(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NPWP))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MARRIAGE))))
                simbakLeadModel.setSL_MARRIAGE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MARRIAGE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OWNERID))))
                simbakLeadModel.setSL_OWNERID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_OWNERID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OWNERTYPE))))
                simbakLeadModel.setSL_OWNERTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_OWNERTYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CIF))))
                simbakLeadModel.setSL_CIF(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_CIF))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PINBB))))
                simbakLeadModel.setSL_PINBB(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_PINBB))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OLD_ID))))
                simbakLeadModel.setSL_OLD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_OLD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_TYPE))))
                simbakLeadModel.setSL_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_LAST_POS))))
                simbakLeadModel.setSL_LAST_POS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_LAST_POS))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_FAV))))
                simbakLeadModel.setSL_FAV(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_FAV))));

            cursor.moveToNext();
        }
        cursor.close();
        return simbakLeadModel;
    }

    public SimbakAccountModel selectSimbakAccountBaseLeadId(long SL_TAB_ID) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        SimbakAccountModel simbakAccountModel = null;
        String query = context.getString(R.string.SELECT_SIMBAK_ACCOUNT_BASE_LEAD_TAB_ID).
                replace("#{SL_TAB_ID}", String.valueOf(SL_TAB_ID));
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            simbakAccountModel = new SimbakAccountModel();
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA3_TAB_ID))))
                simbakAccountModel.setSA3_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SA3_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA3_NAME))))
                simbakAccountModel.setSA3_NAME(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA3_NAME))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA3_CRTDID))))
                simbakAccountModel.setSA3_CRTDID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA3_CRTDID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA3_CREATE))))
                simbakAccountModel.setSA3_CREATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA3_CREATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA3_UPDATE))))
                simbakAccountModel.setSA3_UPDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA3_UPDATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA3_UPTDID))))
                simbakAccountModel.setSA3_UPTDID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA3_UPTDID))));

            cursor.moveToNext();
        }
        cursor.close();
        return simbakAccountModel;
    }

    public SimbakAddressModel selectSimbakAddress(long SLA2_INST_TAB_ID, int SL2_ADDR_TYPE) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        SimbakAddressModel simbakAddressModel = new SimbakAddressModel();
        String query = context.getString(R.string.SELECT_SIMBAK_LIST_ADDRESS).
                replace("#{SLA2_INST_TAB_ID}", String.valueOf(SLA2_INST_TAB_ID)).
                replace("#{SLA2_ADDR_TYPE}", String.valueOf(SL2_ADDR_TYPE));
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_TAB_ID))))
                simbakAddressModel.setSLA2_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA2_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_ID))))
                simbakAddressModel.setSLA2_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA2_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_INST_ID))))
                simbakAddressModel.setSLA2_INST_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA2_INST_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_INST_TAB_ID))))
                simbakAddressModel.setSLA2_INST_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA2_INST_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_INST_TYPE))))
                simbakAddressModel.setSLA2_INST_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA2_INST_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_ADDR_TYPE))))
                simbakAddressModel.setSLA2_ADDR_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA2_ADDR_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_DETAIL))))
                simbakAddressModel.setSLA2_DETAIL(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_DETAIL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_POSTCODE))))
                simbakAddressModel.setSLA2_POSTCODE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_POSTCODE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_CITY))))
                simbakAddressModel.setSLA2_CITY(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_CITY))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_STATE))))
                simbakAddressModel.setSLA2_STATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_STATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_HP1))))
                simbakAddressModel.setSLA2_HP1(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_HP1))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_HP2))))
                simbakAddressModel.setSLA2_HP2(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_HP2))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_PHONECODE))))
                simbakAddressModel.setSLA2_PHONECODE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_PHONECODE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_PHONE1))))
                simbakAddressModel.setSLA2_PHONE1(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_PHONE1))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_PHONE2))))
                simbakAddressModel.setSLA2_PHONE2(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_PHONE2))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_EMAIL))))
                simbakAddressModel.setSLA2_EMAIL(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_EMAIL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_FAXCODE))))
                simbakAddressModel.setSLA2_FAXCODE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_FAXCODE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_FAX))))
                simbakAddressModel.setSLA2_FAX(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_FAX))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_LAT))))
                simbakAddressModel.setSLA2_LAT(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_LAT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_LON))))
                simbakAddressModel.setSLA2_LON(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_LON))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA2_OLD_ID))))
                simbakAddressModel.setSLA2_OLD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA2_OLD_ID))));

            cursor.moveToNext();
        }
        cursor.close();
        return simbakAddressModel;
    }

    public SimbakActivityModel selectSimbakActivity(long SLA_TAB_ID) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        SimbakActivityModel simbakActivityModel = new SimbakActivityModel();
        String query = context.getString(R.string.SELECT_SIMBAK_LIST_ACTIVITY).
                replace("#{SLA_TAB_ID}", String.valueOf(SLA_TAB_ID));
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_TAB_ID))))
                simbakActivityModel.setSLA_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_ID))))
                simbakActivityModel.setSLA_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_INST_ID))))
                simbakActivityModel.setSLA_INST_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_INST_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_INST_TAB_ID))))
                simbakActivityModel.setSLA_INST_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_INST_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_NAME))))
                simbakActivityModel.setSLA_NAME(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_NAME))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_DETAIL))))
                simbakActivityModel.setSLA_DETAIL(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_DETAIL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI))))
                simbakActivityModel.setSLA_PREMI(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_IDATE))))
                simbakActivityModel.setSLA_IDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_IDATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_SDATE))))
                simbakActivityModel.setSLA_SDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_SDATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_EDATE))))
                simbakActivityModel.setSLA_EDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_EDATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_REMINDER_DATE))))
                simbakActivityModel.setSLA_REMINDER_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_REMINDER_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CRTD_DATE))))
                simbakActivityModel.setSLA_CRTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_CRTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CRTD_ID))))
                simbakActivityModel.setSLA_CRTD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_CRTD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OWNER_ID))))
                simbakActivityModel.setSLA_OWNER_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_OWNER_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_DATE))))
                simbakActivityModel.setSLA_UPDTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_ID))))
                simbakActivityModel.setSLA_UPDTD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_LON))))
                simbakActivityModel.setSLA_LON(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_LON))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_LAT))))
                simbakActivityModel.setSLA_LAT(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_LAT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_NEXT_ID))))
                simbakActivityModel.setSLA_NEXT_ID(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_NEXT_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_REFF_ID))))
                simbakActivityModel.setSLA_REFF_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_REFF_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_ID))))
                simbakActivityModel.setSLA_OLD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_INST_TYPE))))
                simbakActivityModel.setSLA_INST_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_INST_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_ACTIVE))))
                simbakActivityModel.setSLA_ACTIVE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_ACTIVE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_TYPE))))
                simbakActivityModel.setSLA_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_SUBTYPE))))
                simbakActivityModel.setSLA_SUBTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_SUBTYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CORP_STAGE))))
                simbakActivityModel.setSLA_CORP_STAGE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_CORP_STAGE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_STATUS))))
                simbakActivityModel.setSLA_STATUS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_STATUS))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_REMINDER_TYPE))))
                simbakActivityModel.setSLA_REMINDER_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_REMINDER_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OWNER_TYPE))))
                simbakActivityModel.setSLA_OWNER_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_OWNER_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_TYPE))))
                simbakActivityModel.setSLA_OLD_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_SUBTYPE))))
                simbakActivityModel.setSLA_OLD_SUBTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_SUBTYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_USER_ID))))
                simbakActivityModel.setSLA_USER_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_USER_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_RPT_CAT))))
                simbakActivityModel.setSLA_RPT_CAT(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_RPT_CAT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_NXT_ACTION))))
                simbakActivityModel.setSLA_NXT_ACTION(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_NXT_ACTION))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CURR_LVL))))
                simbakActivityModel.setSLA_CURR_LVL(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_CURR_LVL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CORP_COMMEN))))
                simbakActivityModel.setSLA_CORP_COMMEN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_CORP_COMMEN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI2))))
                simbakActivityModel.setSLA_PREMI2(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI2))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI3))))
                simbakActivityModel.setSLA_PREMI3(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI3))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_LAST_POS))))
                simbakActivityModel.setSLA_LAST_POS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_LAST_POS))));

            cursor.moveToNext();
        }
        cursor.close();
        return simbakActivityModel;
    }

    public long selectProductIdWhichNotClosing(long SL_TAB_ID) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long SLP_TAB_ID = -1;
        String query = context.getString(R.string.SELECT_PRODUCT_ID_WHICH_NOT_CLOSING).
                replace("#{SL_TAB_ID}", String.valueOf(SL_TAB_ID));
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            SLP_TAB_ID = cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLP_TAB_ID)));
            cursor.moveToNext();
        }
        cursor.close();
        return SLP_TAB_ID;
    }

    public SimbakLeadModel selectUpdatedDateAndLastPosFromLead(long sl_id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        SimbakLeadModel simbakLeadModel = null;
        String query = context.getString(R.string.SELECT_UPDATED_DATE_AND_LAST_POS_SIMBAK_LEAD).
                replace("#{SL_ID}", String.valueOf(sl_id));
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            simbakLeadModel = new SimbakLeadModel();
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_TAB_ID))))
                simbakLeadModel.setSL_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SL_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ID))))
                simbakLeadModel.setSL_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SL_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_UPTD_DATE))))
                simbakLeadModel.setSL_UPTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_UPTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_LAST_POS))))
                simbakLeadModel.setSL_LAST_POS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_LAST_POS))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NAMA_CABANG))))
                simbakLeadModel.setSL_NAMA_CABANG(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NAMA_CABANG))));
            cursor.moveToNext();
        }
        cursor.close();
        return simbakLeadModel;
    }

    public SimbakActivityModel selectUpdatedDateAndLastPosFromActivity(long sla_id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        SimbakActivityModel simbakActivityModel = null;
        String query = context.getString(R.string.SELECT_UPDATED_DATE_AND_LAST_POS_SIMBAK_ACTIVITY).
                replace("#{SLA_ID}", String.valueOf(sla_id));
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            simbakActivityModel = new SimbakActivityModel();
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_TAB_ID))))
                simbakActivityModel.setSLA_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_ID))))
                simbakActivityModel.setSLA_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_DATE))))
                simbakActivityModel.setSLA_UPDTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_LAST_POS))))
                simbakActivityModel.setSLA_LAST_POS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_LAST_POS))));
            cursor.moveToNext();
        }
        cursor.close();
        return simbakActivityModel;
    }


    //    public ArrayList<SimbakActivityModel> selectListActivity(int slaInstId) {
//        ArrayList<SimbakActivityModel> simbakActivityModels = new ArrayList<>();
//        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        String query = context.getString(R.string.SELECT_ACTIVITY_LIST).
//                replace("#{SLA_INST_ID}", "" + slaInstId);
//        Cursor cursor = db.rawQuery(query, null);
//        cursor.moveToFirst();
//        while (!cursor.isAfterLast()) {
//            SimbakActivityModel simbakActivityModel = new SimbakActivityModel();
//            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLP_TAB_ID))))
//                simbakActivityModel.setSLA_TAB_ID(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_TAB_ID))));
//            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLP_NAME))))
//                simbakActivityModel.setSLA_NAME(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LABEL))));
//
//            simbakActivityModels.add(simbakActivityModel);
//            cursor.moveToNext();
//        }
//        cursor.close();
//        return simbakActivityModels;
//    }FIXME

    public SimbakLinkAccToLeadModel selectSimbakLinkAccToLeadModel(long ATL_ID) {
        SimbakLinkAccToLeadModel simbakLinkAccToLeadModel = null;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String query = context.getString(R.string.SELECT_SIMBAK_LINK_ACC_TO_LEAD).
                replace("#{ATL_ID}", "" + ATL_ID);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            simbakLinkAccToLeadModel = new SimbakLinkAccToLeadModel();
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.ATL_ID))))
                simbakLinkAccToLeadModel.setATL_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.ATL_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.ATL_ACC_ID))))
                simbakLinkAccToLeadModel.setATL_ACC_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.ATL_ACC_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.ATL_LEAD_ID))))
                simbakLinkAccToLeadModel.setATL_LEAD_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.ATL_LEAD_ID))));

            cursor.moveToNext();
        }
        cursor.close();
        return simbakLinkAccToLeadModel;
    }

    public ArrayList<ListActivityModel> SelectListActivity(long SL_TAB_ID, int flag) {
        String rawQuery;
        switch (flag) {
            case 1://after today
                rawQuery = context.getString(R.string.SELECT_LIST_ACTIVITY_AFTER_TODAY);
                break;
            case 2: // today
                rawQuery = context.getString(R.string.SELECT_LIST_ACTIVITY_TODAY);
                break;
            case 3://before today
                rawQuery = context.getString(R.string.SELECT_LIST_ACTIVITY_BEFORE_TODAY);
                break;
            default:
                rawQuery = context.getString(R.string.SELECT_LIST_ACTIVITY_AFTER_TODAY);
                break;
        }

        ArrayList<ListActivityModel> listActivityModels = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String query = rawQuery.replace("#{SL_TAB_ID}", "" + SL_TAB_ID);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ListActivityModel listActivityModel = new ListActivityModel(
                    cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SL_TAB_ID))),
                    cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLP_TAB_ID))),
                    cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_TAB_ID))),
                    cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_SUBTYPE))),
                    cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_SDATE)))
            );
            listActivityModels.add(listActivityModel);
            cursor.moveToNext();
        }
        cursor.close();
        return listActivityModels;
    }

    public String selectActivityLabel(int ID) {
        String label = "";
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String query = context.getString(R.string.SELECT_ACTIVITY_LABEL).
                replace("#{ID}", "" + ID);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            label = cursor.getString(cursor.getColumnIndex(context.getString(R.string.LABEL)));
            cursor.moveToNext();
        }
        cursor.close();
        return label;
    }

    public List<SimbakLeadModel> selectLeadToUpdate() {
        List<SimbakLeadModel> simbakLeadModels = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String query = context.getString(R.string.SELECT_LIST_SIMBAK_LEAD_TO_UPLOAD);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            SimbakLeadModel simbakLeadModel = new SimbakLeadModel();
            int SL_TAB_ID = cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_TAB_ID)));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_TAB_ID))))
                simbakLeadModel.setSL_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SL_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ID))))
                simbakLeadModel.setSL_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SL_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NAME))))
                simbakLeadModel.setSL_NAME(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NAME))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_APP))))
                simbakLeadModel.setSL_APP(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_APP))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CREATED))))
                simbakLeadModel.setSL_CREATED(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_CREATED))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CRTD_DATE))))
                simbakLeadModel.setSL_CRTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_CRTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_UPDATED))))
                simbakLeadModel.setSL_UPDATED(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_UPDATED))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_UPTD_DATE))))
                simbakLeadModel.setSL_UPTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_UPTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ACTIVE))))
                simbakLeadModel.setSL_ACTIVE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_ACTIVE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_RATE))))
                simbakLeadModel.setSL_RATE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_RATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_REMARK))))
                simbakLeadModel.setSL_REMARK(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_REMARK))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_INIT))))
                simbakLeadModel.setSL_INIT(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_INIT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_SRC))))
                simbakLeadModel.setSL_SRC(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_SRC))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CHAR))))
                simbakLeadModel.setSL_CHAR(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CHAR))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_STATE))))
                simbakLeadModel.setSL_STATE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_STATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MOTHER))))
                simbakLeadModel.setSL_MOTHER(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_MOTHER))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CITIZEN))))
                simbakLeadModel.setSL_CITIZEN(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CITIZEN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CORP_POS))))
                simbakLeadModel.setSL_CORP_POS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CORP_POS))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_GENDER))))
                simbakLeadModel.setSL_GENDER(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_GENDER))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_RELIGION))))
                simbakLeadModel.setSL_RELIGION(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_RELIGION))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PERSONAL_REL))))
                simbakLeadModel.setSL_PERSONAL_REL(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_PERSONAL_REL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BPLACE))))
                simbakLeadModel.setSL_BPLACE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BPLACE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_LAST_EDU))))
                simbakLeadModel.setSL_LAST_EDU(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_LAST_EDU))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_POLIS_PURPOSE))))
                simbakLeadModel.setSL_POLIS_PURPOSE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_POLIS_PURPOSE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PAYMENT_SOURCE))))
                simbakLeadModel.setSL_PAYMENT_SOURCE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_PAYMENT_SOURCE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OTHER_TYPE))))
                simbakLeadModel.setSL_OTHER_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_OTHER_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_IDNO))))
                simbakLeadModel.setSL_IDNO(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_IDNO))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BDATE))))
                simbakLeadModel.setSL_BDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BDATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CSTMR_FUND_TYPE))))
                simbakLeadModel.setSL_CSTMR_FUND_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CSTMR_FUND_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_IDTYPE))))
                simbakLeadModel.setSL_IDTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_IDTYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_RES_PREMIUM))))
                simbakLeadModel.setSL_RES_PREMIUM(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_RES_PREMIUM))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_SALARY))))
                simbakLeadModel.setSL_SALARY(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_SALARY))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF))))
                simbakLeadModel.setSL_BAC_REFF(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BUYER_FUND_TYPE))))
                simbakLeadModel.setSL_BUYER_FUND_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_BUYER_FUND_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PEND_ID))))
                simbakLeadModel.setSL_PEND_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_PEND_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_APPROVE))))
                simbakLeadModel.setSL_APPROVE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_APPROVE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_REQUEST))))
                simbakLeadModel.setSL_REQUEST(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_REQUEST))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ALASAN))))
                simbakLeadModel.setSL_ALASAN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_ALASAN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NO_NASABAH))))
                simbakLeadModel.setSL_NO_NASABAH(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NO_NASABAH))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_APPROVER))))
                simbakLeadModel.setSL_APPROVER(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_APPROVER))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_FIRST_BRANCH))))
                simbakLeadModel.setSL_BAC_FIRST_BRANCH(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_FIRST_BRANCH))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_CURR_BRANCH))))
                simbakLeadModel.setSL_BAC_CURR_BRANCH(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_CURR_BRANCH))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MALL_GUESTCODE))))
                simbakLeadModel.setSL_MALL_GUESTCODE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MALL_GUESTCODE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MALL_MALLCODE))))
                simbakLeadModel.setSL_MALL_MALLCODE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MALL_MALLCODE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MALL_ROOMCODE))))
                simbakLeadModel.setSL_MALL_ROOMCODE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MALL_ROOMCODE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ROOMOUT))))
                simbakLeadModel.setSL_ROOMOUT(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_ROOMOUT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CORRESPONDENT))))
                simbakLeadModel.setSL_CORRESPONDENT(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CORRESPONDENT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ROOMIN))))
                simbakLeadModel.setSL_ROOMIN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_ROOMIN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF_TITLE))))
                simbakLeadModel.setSL_BAC_REFF_TITLE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF_TITLE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OCCUPATION))))
                simbakLeadModel.setSL_OCCUPATION(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_OCCUPATION))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_IDEXP))))
                simbakLeadModel.setSL_IDEXP(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_IDEXP))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NPWP))))
                simbakLeadModel.setSL_NPWP(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NPWP))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MARRIAGE))))
                simbakLeadModel.setSL_MARRIAGE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MARRIAGE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OWNERID))))
                simbakLeadModel.setSL_OWNERID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_OWNERID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OWNERTYPE))))
                simbakLeadModel.setSL_OWNERTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_OWNERTYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CIF))))
                simbakLeadModel.setSL_CIF(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_CIF))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PINBB))))
                simbakLeadModel.setSL_PINBB(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_PINBB))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OLD_ID))))
                simbakLeadModel.setSL_OLD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_OLD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_TYPE))))
                simbakLeadModel.setSL_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_LAST_POS))))
                simbakLeadModel.setSL_LAST_POS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_LAST_POS))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_FAV))))
                simbakLeadModel.setSL_FAV(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_FAV))));

            SimbakAddressModel home = this.selectSimbakAddress(SL_TAB_ID, 1);
            SimbakAddressModel office = this.selectSimbakAddress(SL_TAB_ID, 2);
            SimbakAddressModel other = this.selectSimbakAddress(SL_TAB_ID, 3);
            SimbakAccountModel account = this.selectSimbakAccountBaseLeadId(SL_TAB_ID);

            if (home.getSLA2_TAB_ID() != null) {
                simbakLeadModel.setHome(home);
            }
            if (office.getSLA2_TAB_ID() != null) {
                simbakLeadModel.setOffice(office);
            }
            if (other.getSLA2_TAB_ID() != null) {
                simbakLeadModel.setOther(other);
            }
            if (account != null) {
                simbakLeadModel.setAccount(account);
            }
            simbakLeadModels.add(simbakLeadModel);
            cursor.moveToNext();
        }
        cursor.close();
        return simbakLeadModels;
    }

    public long getLeadFavCount(int favType) {
        long count = 0;
//        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        String query = context.getString(R.string.SELECT_LEAD_FAV_COUNT).
//                replace("#{sl_fav}", "" + favType);
//        Cursor cursor = db.rawQuery(query, null);

        String[] projection = new String[]{"count(" + context.getString(R.string.SL_FAV) + ") count"};
        String selection = null;
        String[] selectionArgs = null;
        if (favType == 1) {
            selection = context.getString(R.string.SL_FAV) + " = ?";
            selectionArgs = new String[]{String.valueOf(favType)};
        }

        Cursor cursor = new QueryUtil(context).query(
                context.getString(R.string.SIMBAK_LEAD),
                projection,
                selection,
                selectionArgs,
                null
        );
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            count = cursor.getLong(cursor.getColumnIndexOrThrow("count"));
            cursor.moveToNext();
        }
        cursor.close();
        return count;
    }

    public long selectLeadTabIdFromActivityId(long SLA_TAB_ID) {
        long leadTabId = -1;
        String[] projection = new String[]{context.getString(R.string.SLA_INST_TAB_ID)};
        String selection = context.getString(R.string.SLA_TAB_ID) + " = ?";
        String[] selectionArgs = new String[]{String.valueOf(SLA_TAB_ID)};

        Cursor cursor = new QueryUtil(context).query(
                context.getString(R.string.TABLE_SIMBAK_LIST_ACTIVITY),
                projection,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();
        leadTabId = cursor.getLong(cursor.getColumnIndexOrThrow(context.getString(R.string.SLA_INST_TAB_ID)));

        cursor.close();

        return leadTabId;
    }

    public ArrayList<HashMap<String, String>> selectTop5Activity(String kodeAgen) {

        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
        String[] projection = new String[]{
                context.getString(R.string.SLA_TAB_ID),
                context.getString(R.string.SL_NAME),
                "(CASE WHEN JSON_SUB_ACTIVITY.LABEL IS NOT NULL THEN JSON_ACTIVITY.LABEL || ' ' || JSON_SUB_ACTIVITY.LABEL ELSE JSON_ACTIVITY.LABEL END)  LABEL",
                context.getString(R.string.SLA_SDATE),
                context.getString(R.string.SLA_UPDTD_DATE)};
        String selection = context.getString(R.string.UTL_USER_ID) + " = ? AND SLA_TAB_ID IS NOT NULL";
        String[] selectionArgs = new String[]{kodeAgen};
        String sortOrder = "DATETIME(" + context.getString(R.string.SLA_SDATE) + ") DESC LIMIT 5";

        Cursor cursor = new QueryUtil(context).query(
                context.getString(R.string.TABLE_JOIN_AGEN_LEAD_ACT),
                projection,
                selection,
                selectionArgs,
                sortOrder
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(context.getString(R.string.SLA_TAB_ID), String.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow(context.getString(R.string.SLA_TAB_ID)))));
            hashMap.put(context.getString(R.string.SL_NAME), (cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SL_NAME)))));
            hashMap.put(context.getString(R.string.LABEL), (cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.LABEL)))));
            hashMap.put(context.getString(R.string.SLA_SDATE), (cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SLA_SDATE)))));
            hashMap.put(context.getString(R.string.SLA_UPDTD_DATE), (cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SLA_UPDTD_DATE)))));

            arrayList.add(hashMap);
            cursor.moveToNext();
        }
        cursor.close();

        return arrayList;
    }


    public ArrayList<SimbakActivityModel> selectListActivityToUploadFromLead(Long SL_TAB_ID) {
        Cursor cursor = new QueryUtil(context).query(
                context.getString(R.string.TABLE_SIMBAK_LIST_ACTIVITY),
                null,
                context.getString(R.string.SLA_INST_TAB_ID) + " = ? AND " +
                        context.getString(R.string.SLA_LAST_POS) + " = 1",
                new String[]{String.valueOf(SL_TAB_ID)},
                null
        );

        return new SimbakActivityTable(context).getArrayObject(cursor);
    }

    public Long selectActivityIdFromTabId(Long SL_TAB_ID) {
        Long slId = (long) -1;

        Cursor cursor = new QueryUtil(context).query(
                context.getString(R.string.SIMBAK_LEAD),
                new String[]{context.getString(R.string.SL_ID)},
                context.getString(R.string.SL_TAB_ID) + " = ?",
                new String[]{String.valueOf(SL_TAB_ID)},
                null
        );

        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SL_ID))))
                    slId = cursor.getLong(cursor.getColumnIndexOrThrow(context.getString(R.string.SL_ID)));

                cursor.moveToNext();
            }
            cursor.close();
        }
        return slId;
    }

    public List<Long> selectListActivityIdWhichNeedToBeUpdate() {
        List<Long> listIdActs = new ArrayList<>();

        Cursor cursor = new QueryUtil(context).query(
                context.getString(R.string.TABLE_SIMBAK_LIST_ACTIVITY),
                new String[]{context.getString(R.string.SLA_TAB_ID)},
                context.getString(R.string.SLA_LAST_POS) + " = ?",
                new String[]{"1"},
                null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Long idAct = cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_TAB_ID)));
            listIdActs.add(idAct);
            cursor.moveToNext();
        }

        return listIdActs;
    }

    public HashMap<String, String> selectLastActivityForUpdatedTOYes(String SL_TAB_ID) {
        HashMap<String, String> hashMap = new HashMap<>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String query = context.getString(R.string.SELECT_LAST_ACTIVITY_FOR_UPDATED_TO_YES).
                replace("#{sla_inst_tab_id}", "'" + SL_TAB_ID + "'");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            hashMap.put(context.getString(R.string.SLA_TAB_ID), cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_TAB_ID))));
            hashMap.put(context.getString(R.string.SLA_SUBTYPE), cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_SUBTYPE))));

            cursor.moveToNext();
        }
        cursor.close();
        return hashMap;
    }

    public int selectCurrentActivityYesLastPos(String spajId, String proposalId) {
        int lastPos = -1;

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String query = context.getString(R.string.SELECT_CURRENT_ACTIVITY_YES_LAST_POS).
                replace("#{spaj_id}", "'" + spajId + "'").
                replace("#{proposal_id}", "'" + proposalId + "'");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            lastPos = cursor.getInt(0);
            cursor.moveToNext();
        }
        cursor.close();
        return lastPos;
    }

    public int selectSLType(long slTabId) {
        int slType = -1;

        Cursor cursor = new QueryUtil(context).query(
                context.getString(R.string.TABLE_SIMBAK_LEAD),
                new String[]{context.getString(R.string.SL_TYPE)},
                context.getString(R.string.SL_TAB_ID) + " = ?",
                new String[]{String.valueOf(slTabId)},
                null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (!cursor.isNull(0))
                slType = cursor.getInt(0);
            cursor.moveToNext();
        }

        return slType;
    }

    public List<BcBranch> getBcBranchFromDatabase(String msagId) {

        if (!TextUtils.isEmpty(msagId)) {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String query = "select distinct ajs_cbng, cabang from SIMBAK_AGENT_BRANCH where msag_id = " + msagId;
            Cursor cursor = db.rawQuery(query, null);


            List<BcBranch> branches = new ArrayList<>();

            while (cursor.moveToNext()) {
                String ajsCabang = cursor.getString(cursor.getColumnIndexOrThrow("ajs_cbng"));
                String cabang = cursor.getString(cursor.getColumnIndexOrThrow("cabang"));

                BcBranch branch = new BcBranch();

                branch.setAjsCbng(ajsCabang);
                branch.setCabang(cabang);

                branches.add(branch);
            }

            cursor.close();
            return branches;

        }

        return null;
    }

    public Cursor getActivityListFromLead(String slTabId, String slId) {

        if (!TextUtils.isEmpty(slTabId)) {

            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select sla_tab_id, sla_id, sla_type, sla_subtype, sla_crtd_date,sla_detail,sla_last_pos from  SIMBAK_LIST_ACTIVITY where sla_inst_tab_id = '" + slTabId + "' or sl_id_ = '" + slId + "' ORDER BY SLA_CRTD_DATE DESC ";
            Cursor cursor = db.rawQuery(query, null);
            return cursor;

        }

        return null;
    }

    public Cursor getActivityListForDashboard(String msagId, int limit) {

        if (!TextUtils.isEmpty(msagId)) {

            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select SL_NAME, SL_ID_, sla_type, sla_subtype, sla_crtd_date,sla_inst_tab_id,sla_detail,sla_last_pos from  SIMBAK_LIST_ACTIVITY where SLA_CRTD_ID = '" + msagId + "' ORDER BY SLA_CRTD_DATE DESC LIMIT " + limit;
            Cursor cursor = db.rawQuery(query, null);
            return cursor;

        }

        return null;
    }

    public Cursor getActivityListFromAgent(String msagId) {

        if (!TextUtils.isEmpty(msagId)) {

            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select SL_NAME, SL_ID_, sla_type, sla_subtype, sla_crtd_date,sla_inst_tab_id,sla_detail,sla_last_pos from  SIMBAK_LIST_ACTIVITY where SLA_CRTD_ID = '" + msagId + "' ORDER BY SLA_CRTD_DATE DESC";
            Cursor cursor = db.rawQuery(query, null);
            return cursor;

        }

        return null;
    }

    /**
     * @param slId = slaInstId
     * @return
     */
    public long getSlTabIdFromSlId(long slId) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "select SL_TAB_ID from SIMBAK_LEAD WHERE SL_ID = '" + slId + "'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            long slTabId = cursor.getLong(cursor.getColumnIndexOrThrow("SL_TAB_ID"));
            cursor.close();
            return slTabId;
        }

        return -1;

    }

    public boolean isLeadSlIdNull(long slIdInput) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "select SL_ID from SIMBAK_LEAD WHERE SL_TAB_ID = '" + slIdInput + "'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            String slId = cursor.getString(cursor.getColumnIndexOrThrow("SL_ID"));
            cursor.close();
            return slId == null;
        }

        return false;

    }

    public Cursor getProposalList(String leadTabId) {

        if (!TextUtils.isEmpty(leadTabId)) {

            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "SELECT MST_DATA_PROPOSAL._id,MST_DATA_PROPOSAL.NO_PROPOSAL_TAB, MST_DATA_PROPOSAL.NO_PROPOSAL, MST_DATA_PROPOSAL.NAMA_PP, " +
                    "MST_DATA_PROPOSAL.NAMA_TT, MST_PROPOSAL_PRODUCT.PREMI, MST_PROPOSAL_PRODUCT.UP, LST_DET_BISNIS.LSDBS_NAME from " +
                    "MST_DATA_PROPOSAL LEFT JOIN MST_PROPOSAL_PRODUCT ON MST_DATA_PROPOSAL.NO_PROPOSAL_TAB = MST_PROPOSAL_PRODUCT.NO_PROPOSAL_TAB " +
                    "LEFT JOIN LST_DET_BISNIS ON MST_PROPOSAL_PRODUCT.LSBS_ID = LST_DET_BISNIS.LSBS_ID and MST_PROPOSAL_PRODUCT.LSDBS_NUMBER = LST_DET_BISNIS.LSDBS_NUMBER " +
                    "where MST_DATA_PROPOSAL.LEAD_TAB_ID = '" + leadTabId + "' order by MST_DATA_PROPOSAL.TGL_INPUT desc";

            Cursor cursor = db.rawQuery(query, null);
            return cursor;

        }

        return null;
    }

    public Cursor getAgentProposalList(String kodeAgen) {

        if (!TextUtils.isEmpty(kodeAgen)) {

            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "SELECT MST_DATA_PROPOSAL._id,MST_DATA_PROPOSAL.NO_PROPOSAL_TAB, MST_DATA_PROPOSAL.NO_PROPOSAL, MST_DATA_PROPOSAL.NAMA_PP, " +
                    "MST_DATA_PROPOSAL.NAMA_TT, MST_PROPOSAL_PRODUCT.PREMI, MST_PROPOSAL_PRODUCT.UP, LST_DET_BISNIS.LSDBS_NAME from " +
                    "MST_DATA_PROPOSAL LEFT JOIN MST_PROPOSAL_PRODUCT ON MST_DATA_PROPOSAL.NO_PROPOSAL_TAB = MST_PROPOSAL_PRODUCT.NO_PROPOSAL_TAB " +
                    "LEFT JOIN LST_DET_BISNIS ON MST_PROPOSAL_PRODUCT.LSBS_ID = LST_DET_BISNIS.LSBS_ID and MST_PROPOSAL_PRODUCT.LSDBS_NUMBER = LST_DET_BISNIS.LSDBS_NUMBER " +
                    "where MST_DATA_PROPOSAL.MSAG_ID = '" + kodeAgen + "' order by MST_DATA_PROPOSAL.TGL_INPUT desc";

            Cursor cursor = db.rawQuery(query, null);
            return cursor;

        }

        return null;
    }

    public List<ActivityListModel> getSavedActivityList(String slaInstTabId, String slId) {

        if (!TextUtils.isEmpty(slaInstTabId)) {

            List<ActivityListModel> activities = new ArrayList<>();
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select sla_tab_id, sla_id, sla_type, sla_subtype, sla_crtd_date, sla_detail,sla_last_pos from  SIMBAK_LIST_ACTIVITY where sla_inst_tab_id = '" + slaInstTabId + "' or SL_ID_ = '" + slId + "' ORDER BY SLA_CRTD_DATE DESC ";
            Cursor cursor = db.rawQuery(query, null);

            while (cursor.moveToNext()) {
                String activityType = cursor.getString(cursor.getColumnIndexOrThrow("SLA_TYPE"));
                String subActivityType = cursor.getString(cursor.getColumnIndexOrThrow("SLA_SUBTYPE"));
                String activityDate = cursor.getString(cursor.getColumnIndexOrThrow("SLA_CRTD_DATE"));
                String slaId = cursor.getString(cursor.getColumnIndexOrThrow("SLA_ID"));
                String slaTabId = cursor.getString(cursor.getColumnIndexOrThrow("SLA_TAB_ID"));

                String activityCreatedDate = DateUtils.getDateFrom(activityDate);
                String activityName = getSelectedActivityFromSlaType(activityType);
                String subActivityName = getSelectedSubActivityFromSlaSubType(subActivityType);
                String slaDetail = cursor.getString(cursor.getColumnIndexOrThrow("SLA_DETAIL"));
                int slaLastPost = cursor.getInt(cursor.getColumnIndexOrThrow("SLA_LAST_POS"));
                boolean isSynced = slaLastPost == 0;

                ActivityListModel activity = new ActivityListModel(slaId, slaTabId, activityName, subActivityName, activityCreatedDate, activityDate, subActivityType, slaDetail,isSynced);

                activities.add(activity);
            }

            cursor.close();
            return activities;

        }

        return null;
    }

    public String getSelectedActivityFromSlaType(String slaType) {

        if (!TextUtils.isEmpty(slaType)) {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String query = "select label from JSON_ACTIVITY where id = '" + slaType + "'";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                String selectedActivity = cursor.getString(cursor.getColumnIndexOrThrow("LABEL"));
                cursor.close();
                return selectedActivity;
            }

            return null;

        }

        return null;
    }

    public String getSelectedSubActivityFromSlaSubType(String slaSubType) {

        if (!TextUtils.isEmpty(slaSubType)) {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String query = "select label from JSON_SUB_ACTIVITY where id = '" + slaSubType + "'";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                String selectedSubActivity = cursor.getString(cursor.getColumnIndexOrThrow("LABEL"));
                cursor.close();
                return selectedSubActivity;
            }

            return null;

        }

        return null;
    }

    public String getLatestLeadInputDate(String msagId) {
        if (!TextUtils.isEmpty(msagId)) {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select SL_UPTD_DATE from SIMBAK_LEAD where sl_created = '" + msagId + "' order by SL_UPTD_DATE desc limit 1";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                String latestInputDate = cursor.getString(cursor.getColumnIndexOrThrow("SL_UPTD_DATE"));
                cursor.close();
                return latestInputDate;
            }
            cursor.close();
        }

        return null;

    }

    public String getLatestActivityInputDate(String msagId) {
        if (!TextUtils.isEmpty(msagId)) {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select SLA_UPDTD_DATE from SIMBAK_LIST_ACTIVITY where sla_crtd_id = '" + msagId + "' order by SLA_UPDTD_DATE desc limit 1";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                String latestInputDate = cursor.getString(cursor.getColumnIndexOrThrow("SLA_UPDTD_DATE"));
                cursor.close();
                return latestInputDate;
            }

            cursor.close();
        }

        return null;

    }

    public String getLeadNameFromSLID(String slId) {
        if (!TextUtils.isEmpty(slId)) {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select SL_NAME from SIMBAK_LEAD where SL_ID = '" + slId + "'";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                String leadName = cursor.getString(cursor.getColumnIndexOrThrow("SL_NAME"));
                cursor.close();
                return leadName;
            }
        }

        return null;

    }

    public Cursor getAllSavedLead(String msagId, String orderBy) {
        String query = "select sl_tab_id,sl_id,sl_name,sl_crtd_date,sl_umur, sl_last_pos,sl_fav,nama_cabang from SIMBAK_LEAD where sl_created = ? or utl_user_id = ? order by " + orderBy;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{msagId, msagId});
        return cursor;
    }

    public SimbakLeadModel getLeadInfo(String slTabId) {
        if (!TextUtils.isEmpty(slTabId)) {

            SimbakLeadModel lead = new SimbakLeadModel();
            String query = "select * from simbak_lead where SL_TAB_ID = '" + slTabId + "'";
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_TAB_ID))))
                    lead.setSL_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SL_TAB_ID))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ID))))
                    lead.setSL_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SL_ID))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NAME))))
                    lead.setSL_NAME(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NAME))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_APP))))
                    lead.setSL_APP(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_APP))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CREATED))))
                    lead.setSL_CREATED(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_CREATED))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CRTD_DATE))))
                    lead.setSL_CRTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_CRTD_DATE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_UPDATED))))
                    lead.setSL_UPDATED(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_UPDATED))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_UPTD_DATE))))
                    lead.setSL_UPTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_UPTD_DATE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ACTIVE))))
                    lead.setSL_ACTIVE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_ACTIVE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_RATE))))
                    lead.setSL_RATE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_RATE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_REMARK))))
                    lead.setSL_REMARK(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_REMARK))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_INIT))))
                    lead.setSL_INIT(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_INIT))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_SRC))))
                    lead.setSL_SRC(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_SRC))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CHAR))))
                    lead.setSL_CHAR(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CHAR))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_STATE))))
                    lead.setSL_STATE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_STATE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MOTHER))))
                    lead.setSL_MOTHER(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_MOTHER))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CITIZEN))))
                    lead.setSL_CITIZEN(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CITIZEN))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CORP_POS))))
                    lead.setSL_CORP_POS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CORP_POS))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_GENDER))))
                    lead.setSL_GENDER(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_GENDER))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_RELIGION))))
                    lead.setSL_RELIGION(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_RELIGION))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PERSONAL_REL))))
                    lead.setSL_PERSONAL_REL(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_PERSONAL_REL))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BPLACE))))
                    lead.setSL_BPLACE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BPLACE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_LAST_EDU))))
                    lead.setSL_LAST_EDU(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_LAST_EDU))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_POLIS_PURPOSE))))
                    lead.setSL_POLIS_PURPOSE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_POLIS_PURPOSE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PAYMENT_SOURCE))))
                    lead.setSL_PAYMENT_SOURCE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_PAYMENT_SOURCE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OTHER_TYPE))))
                    lead.setSL_OTHER_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_OTHER_TYPE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_IDNO))))
                    lead.setSL_IDNO(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_IDNO))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BDATE))))
                    lead.setSL_BDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BDATE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_UMUR))))
                    lead.setSL_UMUR(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_UMUR))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CSTMR_FUND_TYPE))))
                    lead.setSL_CSTMR_FUND_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CSTMR_FUND_TYPE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_IDTYPE))))
                    lead.setSL_IDTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_IDTYPE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_RES_PREMIUM))))
                    lead.setSL_RES_PREMIUM(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_RES_PREMIUM))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_SALARY))))
                    lead.setSL_SALARY(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_SALARY))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF))))
                    lead.setSL_BAC_REFF(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF_NAME))))
                    lead.setSL_BAC_REFF_NAME(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF_NAME))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BUYER_FUND_TYPE))))
                    lead.setSL_BUYER_FUND_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_BUYER_FUND_TYPE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PEND_ID))))
                    lead.setSL_PEND_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_PEND_ID))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_APPROVE))))
                    lead.setSL_APPROVE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_APPROVE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_REQUEST))))
                    lead.setSL_REQUEST(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_REQUEST))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ALASAN))))
                    lead.setSL_ALASAN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_ALASAN))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NO_NASABAH))))
                    lead.setSL_NO_NASABAH(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NO_NASABAH))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_APPROVER))))
                    lead.setSL_APPROVER(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_APPROVER))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_FIRST_BRANCH))))
                    lead.setSL_BAC_FIRST_BRANCH(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_FIRST_BRANCH))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_CURR_BRANCH))))
                    lead.setSL_BAC_CURR_BRANCH(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_CURR_BRANCH))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MALL_GUESTCODE))))
                    lead.setSL_MALL_GUESTCODE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MALL_GUESTCODE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MALL_MALLCODE))))
                    lead.setSL_MALL_MALLCODE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MALL_MALLCODE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MALL_ROOMCODE))))
                    lead.setSL_MALL_ROOMCODE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MALL_ROOMCODE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ROOMOUT))))
                    lead.setSL_ROOMOUT(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_ROOMOUT))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CORRESPONDENT))))
                    lead.setSL_CORRESPONDENT(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CORRESPONDENT))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ROOMIN))))
                    lead.setSL_ROOMIN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_ROOMIN))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF_TITLE))))
                    lead.setSL_BAC_REFF_TITLE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF_TITLE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OCCUPATION))))
                    lead.setSL_OCCUPATION(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_OCCUPATION))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_IDEXP))))
                    lead.setSL_IDEXP(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_IDEXP))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NPWP))))
                    lead.setSL_NPWP(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NPWP))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MARRIAGE))))
                    lead.setSL_MARRIAGE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MARRIAGE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OWNERID))))
                    lead.setSL_OWNERID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_OWNERID))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OWNERTYPE))))
                    lead.setSL_OWNERTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_OWNERTYPE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CIF))))
                    lead.setSL_CIF(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_CIF))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PINBB))))
                    lead.setSL_PINBB(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_PINBB))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OLD_ID))))
                    lead.setSL_OLD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_OLD_ID))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_TYPE))))
                    lead.setSL_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_TYPE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_LAST_POS))))
                    lead.setSL_LAST_POS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_LAST_POS))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_FAV))))
                    lead.setSL_FAV(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_FAV))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NAMA_CABANG))))
                    lead.setSL_NAMA_CABANG(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NAMA_CABANG))));
                return lead;
            }
            cursor.close();
        }

        return null;
    }

    /**
     * SLA INST TAB ID = SL TAB ID
     */
    public Long getSlTabIdFrom(long slaTabId) {
        //Get sl tab id of a selected activity

        String query = "select SLA_INST_TAB_ID from SIMBAK_LIST_ACTIVITY where SLA_TAB_ID = '" + slaTabId + "'";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            long leadTabId = cursor.getLong(cursor.getColumnIndexOrThrow("SLA_INST_TAB_ID"));
            return leadTabId;
        }
        cursor.close();
        return null;
    }

    public Cursor getActivityInfo(String slaTabId, String slaId) {
        String query = "select * from SIMBAK_LIST_ACTIVITY where SLA_TAB_ID =  '" + slaTabId + "'" + " or SLA_ID = '" + slaId + "'";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        return cursor;
    }

    public int getSlType(String slTabId) {
        if (!TextUtils.isEmpty(slTabId)) {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select sl_type from simbak_lead where sl_tab_id  = '" + slTabId + "'";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                int slType = cursor.getInt(cursor.getColumnIndexOrThrow("SL_TYPE"));
                cursor.close();
                return slType;
            }
        }

        return -1;
    }

    public Cursor checkIfActivityContainsBuatProposal(String slTabId) {
        if (!TextUtils.isEmpty(slTabId)) {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select label " +
                    "from JSON_SUB_ACTIVITY a\n" +
                    "left join SIMBAK_LIST_ACTIVITY b \n" +
                    "on a.ID = b.SLA_SUBTYPE\n" +
                    "where sla_inst_tab_id = " + slTabId;
            Cursor cursor = db.rawQuery(query, null);
            return cursor;
        }
        return null;
    }

    public int checkIfLeadExistOnUsrToLeadTable(String slTabId, String msagId) {
        if (!TextUtils.isEmpty(slTabId) && !TextUtils.isEmpty(msagId)) {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select * from SIMBAK_LINK_USR_TO_LEAD where utl_lead_id = '" + slTabId + "'" + " and utl_user_id = " + msagId;
            Cursor cursor = db.rawQuery(query, null);
            return cursor.getCount();
        }

        return -1;
    }

    public Cursor searchLeadWithNameOf(String leadName, String msagId) {
        if (!TextUtils.isEmpty(leadName) && !TextUtils.isEmpty(msagId)) {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select sl_name , sl_umur , sl_tab_id,sl_id from SIMBAK_LEAD where sl_name like '%" + leadName + "%'" + " and (sl_created = '" + msagId + "' or utl_user_id = '" + msagId + "') order by sl_name asc";
            Cursor cursor = db.rawQuery(query, null);
            return cursor;
        }
        return null;
    }

    public Cursor getLeadInfoForProposal(String slTabId) {
        if (!TextUtils.isEmpty(slTabId)) {
            String query = "select * from simbak_lead where SL_TAB_ID = '" + slTabId + "'";
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            return cursor;
        }

        return null;
    }

    public float getTotalKunjunganPresentasiActivityCount(String msagId, String monthStartDate, String monthEndDate) {
        String query = "select count(*) from SIMBAK_LIST_ACTIVITY where sla_type = 51 and sla_crtd_id = ? and sla_crtd_date between ? and ?";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{msagId, monthStartDate, monthEndDate});
        if (cursor.moveToFirst()) {
            float totalKunjunganPresentasiActivityCount = cursor.getFloat(0);
            cursor.close();
            return totalKunjunganPresentasiActivityCount;
        }
        return 0;
    }

    public float getClosingActivityCount(String msagId, String monthStartDate, String monthEndDate) {
        if (!TextUtils.isEmpty(msagId)) {
            String query = "select count(*) from SIMBAK_LIST_ACTIVITY where sla_subtype = 45 and sla_crtd_id = ? and sla_crtd_date between ? and ?";
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, new String[]{msagId, monthStartDate, monthEndDate});
            if (cursor.moveToFirst()) {
                float closingActivityCount = cursor.getFloat(0);
                cursor.close();
                return closingActivityCount;
            }
        }
        return 0;
    }

    public int getMonthActivityCountFromDate(String msagId, String monthStartDate, String monthEndDate) {
        if (!TextUtils.isEmpty(msagId) && !TextUtils.isEmpty(monthStartDate) && !TextUtils.isEmpty(monthEndDate)) {
            String query = "select count(*) from SIMBAK_LIST_ACTIVITY where sla_crtd_id = ? and sla_type = 51 and sla_crtd_date between ? and ? ORDER BY sla_crtd_date ASC";
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, new String[]{msagId, monthStartDate, monthEndDate});
            if (cursor.moveToFirst()) {
                int monthActivityCount = cursor.getInt(0);
                cursor.close();
                return monthActivityCount;
            }
        }
        return 0;
    }

    public int getTodayCreatedActivityCount(String msagId) {
        if (!TextUtils.isEmpty(msagId)) {
            String query = "SELECT COUNT(*) FROM SIMBAK_LIST_ACTIVITY WHERE DATE(SLA_CRTD_DATE) = date('now','localtime') AND SLA_CRTD_ID = ? and sla_type = 51";
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, new String[]{msagId});
            if (cursor.moveToFirst()) {
                int todayCreatedActivityCount = cursor.getInt(0);
                cursor.close();
                return todayCreatedActivityCount;
            }
        }
        return 0;
    }

    public int getMenuCountForUser(String msagId) {
        if (!TextUtils.isEmpty(msagId)) {
            String query = "select count(*) from LST_USER_MENU where msag_id = ?";
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, new String[]{msagId});
            if (cursor.moveToFirst()) {
                int menuCount = cursor.getInt(0);
                cursor.close();
                return menuCount;
            }
        }
        return 0;
    }

    public List<String> getUserMenuForUser(String msagId) {
        if (!TextUtils.isEmpty(msagId)) {

            List<String> menuList = new ArrayList<>();

            String query = "select MENU_ID from LST_USER_MENU where msag_id = ?";
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, new String[]{msagId});

            while (cursor.moveToNext()) {
                String menuId = cursor.getString(cursor.getColumnIndexOrThrow("MENU_ID"));
                menuList.add(menuId);
            }

            return menuList;

        }

        return null;
    }

    public Cursor getAllSavedAgencyLead(String agentCode, String orderBy) {
        String query = "select * from LST_AGENCY_LEAD where AGENT_CODE = '"+agentCode+"' order by " + orderBy;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        return cursor;
    }

    public int getAgencyLeadCount(String agentCode) {
        if (!TextUtils.isEmpty(agentCode)) {
            String query = "SELECT COUNT(*) FROM LST_AGENCY_LEAD WHERE AGENT_CODE = '"+agentCode+"'";
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                int leadCount = cursor.getInt(0);
                cursor.close();
                return leadCount;
            }
        }
        return 0;
    }

    public AgencyLeadModel getAgencyLeadInfoOf(String leadId, String agentCode) {
        if (!TextUtils.isEmpty(leadId) && !TextUtils.isEmpty(agentCode)) {
            AgencyLeadModel model = new AgencyLeadModel();
            String query = "select * from LST_AGENCY_LEAD where id = ? and agent_code = ?";
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, new String[]{leadId, agentCode});

            if (cursor.moveToFirst()) {
                long id = cursor.getLong(cursor.getColumnIndexOrThrow("ID"));
                String leadName = cursor.getString(cursor.getColumnIndexOrThrow("LEAD_NAME"));
                int leadAge = cursor.getInt(cursor.getColumnIndexOrThrow("LEAD_AGE"));
                int leadGender = cursor.getInt(cursor.getColumnIndexOrThrow("LEAD_GENDER"));

                model.setId(id);
                model.setLeadName(leadName);
                model.setLeadAge(leadAge);
                model.setLeadGender(leadGender);

                return model;
            }

            return null;
        }

        return null;

    }

    public Cursor searchAgencyLeadWithNameOf(String leadName, String agentCode) {
        if (!TextUtils.isEmpty(leadName) && !TextUtils.isEmpty(agentCode)) {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select ID, LEAD_NAME, LEAD_AGE, LEAD_GENDER from LST_AGENCY_LEAD where LEAD_NAME like '%" + leadName + "%'" + " and AGENT_CODE ='"+agentCode+"' order by CREATED_DATE desc, LEAD_NAME asc";
            Cursor cursor = db.rawQuery(query, null);
            return cursor;
        }
        return null;
    }


}
