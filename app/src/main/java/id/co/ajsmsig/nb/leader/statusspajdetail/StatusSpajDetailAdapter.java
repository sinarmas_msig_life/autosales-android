package id.co.ajsmsig.nb.leader.statusspajdetail;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ajsmsig.nb.R;

public class StatusSpajDetailAdapter extends RecyclerView.Adapter<StatusSpajDetailAdapter.MyViewHolder> {

    private ArrayList<Status> statusDetail;


    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTimestamp;
        private TextView tvStatusAccept;
        private TextView tvStatusDetail;
        private TextView tvCheckerName;
        private ImageView imgPolis;


        MyViewHolder(View view) {
            super(view);
            tvTimestamp = view.findViewById(R.id.tvTimestamp);
            tvStatusAccept = view.findViewById(R.id.tvStatusAccept);
            tvStatusDetail = view.findViewById(R.id.tvStatusDetail);
            tvCheckerName = view.findViewById(R.id.tvCheckerName);
            imgPolis = view.findViewById(R.id.imgPolis);
        }

    }

    public StatusSpajDetailAdapter(ArrayList<Status> statusDetail) {
        this.statusDetail = statusDetail;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_spaj_detail, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Status model = statusDetail.get(position);
        holder.tvStatusAccept.setText(model.getStatusAccept());
        holder.tvStatusDetail.setText("Note : "+model.getMspsDesc());
        holder.tvTimestamp.setText(model.getMspsDate());
        String checkerName = model.getLusFullName() + " - " + model.getLdeDept();
        holder.tvCheckerName.setText(checkerName);


        if (model.isPending()) {
            holder.imgPolis.setImageResource(R.drawable.ic_warning);
        } else {
            holder.imgPolis.setImageResource(R.drawable.ic_policy) ;
        }


        if (model.isInforce()) {
            holder.imgPolis.setImageResource(R.drawable.ic_complete);
        }
    }


    @Override
    public int getItemCount() {
        return statusDetail.size();
    }

    void refreshSpajDetail(ArrayList<Status> newStatus) {
        //Hapus list yg lama
        statusDetail.clear();
        //Update dengan data yg baru
        statusDetail = newStatus;
        //Refresh recyclerview
        notifyDataSetChanged();
    }

}


