package id.co.ajsmsig.nb.leader.summarybc.datasource;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.leader.summarybc.utils.NetworkState;
import id.co.ajsmsig.nb.leader.summarybc.model.SummaryData;
import id.co.ajsmsig.nb.leader.summarybc.model.SummaryResponse;
import id.co.ajsmsig.nb.util.AppConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SummaryDataSource extends PageKeyedDataSource<Integer, SummaryData> {

    private static final String TAG = SummaryDataSource.class.getSimpleName();

    private String agentCode;
    private String channelBank;
    private MutableLiveData networkState;
    private MutableLiveData initialLoading;


    public SummaryDataSource(String agentCode, String channelBank) {
        this.agentCode = agentCode;
        this.channelBank = channelBank;
        networkState = new MutableLiveData();
        initialLoading = new MutableLiveData();
    }

    public MutableLiveData getNetworkState() {
        return networkState;
    }

    public MutableLiveData getInitialLoading() {
        return initialLoading;
    }

    public MutableLiveData setNoData() {
        return initialLoading;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, SummaryData> callback) {
        initialLoading.postValue(NetworkState.LOADING);
        networkState.postValue(NetworkState.LOADING);
        String url = AppConfig.getBaseUrlSummaryFragment().concat("/autosales/json/getsubordinatesummary");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<SummaryResponse> call = api.getSummaryReport(url, agentCode, channelBank, 1, params.requestedLoadSize);
        call.enqueue(new Callback<SummaryResponse>() {
            @Override
            public void onResponse(Call<SummaryResponse> call, Response<SummaryResponse> response) {
                if (response.isSuccessful()) {
                    callback.onResult(response.body().getData(), null, 2);
                    initialLoading.postValue(NetworkState.LOADED);
                    networkState.postValue(NetworkState.LOADED);
                }
                if (response.body().getData().isEmpty()) {
                    networkState.postValue(NetworkState.NODATA);
                } else {
                    initialLoading.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
                    networkState.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
                }
            }

            @Override
            public void onFailure(Call<SummaryResponse> call, Throwable t) {
                networkState.postValue(NetworkState.FAILED);
            }
        });
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, SummaryData> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, SummaryData> callback) {
        networkState.postValue(NetworkState.LOADED);
        String url = AppConfig.getBaseUrlSummaryFragment().concat("/autosales/json/getsubordinatesummary");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<SummaryResponse> call = api.getSummaryReport(url, agentCode, channelBank, params.key, params.requestedLoadSize);
        call.enqueue(new Callback<SummaryResponse>() {
            @Override
            public void onResponse(Call<SummaryResponse> call, Response<SummaryResponse> response) {
                if (response.isSuccessful()) {
                    int nextKey = params.key + 1;
                    callback.onResult(response.body().getData(), nextKey);
                    networkState.postValue(NetworkState.LOADED);
                }
                if (response.body().getData().isEmpty()) {
                    networkState.postValue(NetworkState.LOADED);
                } else
                    networkState.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
            }

            @Override
            public void onFailure(Call<SummaryResponse> call, Throwable t) {
                networkState.postValue(NetworkState.FAILED);
            }
        });
    }


}
