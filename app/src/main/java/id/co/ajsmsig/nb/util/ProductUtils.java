package id.co.ajsmsig.nb.util;

public class ProductUtils {

    public static boolean isMagnaLink(int lsbsId, int lsdbsNumber) {
        return lsbsId == Const.PRODUCT_MAGNA_LINK && lsdbsNumber == Const.SUB_PRODUCT_MAGNA_LINK;
    }

    public static boolean isPrimeLink(int lsbsId, int lsdbsNumber) {
        return lsbsId == Const.PRODUCT_PRIME_LINK && lsdbsNumber == Const.SUB_PRODUCT_PRIME_LINK;
    }

    public static boolean isPrimeLinkSyariah(int lsbsId, int lsdbsNumber) {
        return lsbsId == Const.PRODUCT_PRIME_LINK_SYARIAH && lsdbsNumber == Const.SUB_PRODUCT_PRIME_LINK_SYARIAH;
    }

    public static boolean isMagnaLinkSyariah(int lsbsId, int lsdbsNumber) {
        return lsbsId == Const.PRODUCT_MAGNA_LINK_SYARIAH && lsdbsNumber == Const.SUB_PRODUCT_MAGNA_LINK_SYARIAH;
    }

}
