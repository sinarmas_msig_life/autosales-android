package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.lifecycle.MediatorLiveData
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Product

/**
 * Created by andreyyoshuamanik on 02/03/18.
 */
@Entity
class ProductRecommendation (
        @PrimaryKey
        val id: Int,
        var productGroupId: Int?,
        var pageOptionId: Int?,
        var riskProfileScoringId: Int?,
        var productId: Int?,
        var order: Int?,
        var maxAge: Int?
): Option() {

        @Ignore
        var product: MediatorLiveData<Product> = MediatorLiveData()
}