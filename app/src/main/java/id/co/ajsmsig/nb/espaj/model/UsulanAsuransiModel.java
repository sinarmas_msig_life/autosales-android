package id.co.ajsmsig.nb.espaj.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAskesUA;

/**
 * Created by Bernard on 30/08/2017.
 */

public class UsulanAsuransiModel implements Parcelable {

    private int jenis_produk_ua = 0;
    private int kd_produk_ua = 0;
    private int sub_produk_ua = 0;
    private int klas_ua = 0;
    private int paket_ua = 0;
    private int ekasehat_plan_ua = 0;
    private int masa_pertanggungan_ua = 0;
    private int cuti_premi_ua = 0;
    private String kurs_up_ua = "";
    private Double up_ua = (double) 0;
    private int unitlink_opsipremi_ua = 0;
    private String unitlink_kurs_ua = "";
    private Double unitlink_premistandard_ua = (double) 0;
    private String unitlink_kombinasi_ua = "";
    private String unitlink_kurs_total_ua = "";
    private Double unitlink_total_ua = (double) 0;
    private int carabayar_ua = 0;
    private int bentukbayar_ua = 0;
    private String awalpertanggungan_ua = "";
    private String akhirpertanggungan_ua = "";
    private int masa_pembayaran_ua = 0;
    private List<ModelAskesUA> ListModelAskesUA;
    private Boolean validation = false;

    public UsulanAsuransiModel() {
    }


    protected UsulanAsuransiModel(Parcel in) {
        jenis_produk_ua = in.readInt();
        kd_produk_ua = in.readInt();
        sub_produk_ua = in.readInt();
        klas_ua = in.readInt();
        paket_ua = in.readInt();
        ekasehat_plan_ua = in.readInt();
        masa_pertanggungan_ua = in.readInt();
        cuti_premi_ua = in.readInt();
        kurs_up_ua = in.readString();
        unitlink_opsipremi_ua = in.readInt();
        unitlink_kurs_ua = in.readString();
        unitlink_kombinasi_ua = in.readString();
        unitlink_kurs_total_ua = in.readString();
        carabayar_ua = in.readInt();
        bentukbayar_ua = in.readInt();
        awalpertanggungan_ua = in.readString();
        akhirpertanggungan_ua = in.readString();
        masa_pembayaran_ua = in.readInt();
        ListModelAskesUA = in.createTypedArrayList(ModelAskesUA.CREATOR);
    }

    public UsulanAsuransiModel(int jenis_produk_ua, int kd_produk_ua, int sub_produk_ua, int klas_ua, int paket_ua, int ekasehat_plan_ua, int masa_pertanggungan_ua, int cuti_premi_ua, String kurs_up_ua, Double up_ua, int unitlink_opsipremi_ua, String unitlink_kurs_ua, Double unitlink_premistandard_ua, String unitlink_kombinasi_ua, String unitlink_kurs_total_ua, Double unitlink_total_ua, int carabayar_ua, int bentukbayar_ua, String awalpertanggungan_ua, String akhirpertanggungan_ua, int masa_pembayaran_ua, List<ModelAskesUA> listModelAskesUA, Boolean validation) {
        this.jenis_produk_ua = jenis_produk_ua;
        this.kd_produk_ua = kd_produk_ua;
        this.sub_produk_ua = sub_produk_ua;
        this.klas_ua = klas_ua;
        this.paket_ua = paket_ua;
        this.ekasehat_plan_ua = ekasehat_plan_ua;
        this.masa_pertanggungan_ua = masa_pertanggungan_ua;
        this.cuti_premi_ua = cuti_premi_ua;
        this.kurs_up_ua = kurs_up_ua;
        this.up_ua = up_ua;
        this.unitlink_opsipremi_ua = unitlink_opsipremi_ua;
        this.unitlink_kurs_ua = unitlink_kurs_ua;
        this.unitlink_premistandard_ua = unitlink_premistandard_ua;
        this.unitlink_kombinasi_ua = unitlink_kombinasi_ua;
        this.unitlink_kurs_total_ua = unitlink_kurs_total_ua;
        this.unitlink_total_ua = unitlink_total_ua;
        this.carabayar_ua = carabayar_ua;
        this.bentukbayar_ua = bentukbayar_ua;
        this.awalpertanggungan_ua = awalpertanggungan_ua;
        this.akhirpertanggungan_ua = akhirpertanggungan_ua;
        this.masa_pembayaran_ua = masa_pembayaran_ua;
        ListModelAskesUA = listModelAskesUA;
        this.validation = validation;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(jenis_produk_ua);
        dest.writeInt(kd_produk_ua);
        dest.writeInt(sub_produk_ua);
        dest.writeInt(klas_ua);
        dest.writeInt(paket_ua);
        dest.writeInt(ekasehat_plan_ua);
        dest.writeInt(masa_pertanggungan_ua);
        dest.writeInt(cuti_premi_ua);
        dest.writeString(kurs_up_ua);
        dest.writeInt(unitlink_opsipremi_ua);
        dest.writeString(unitlink_kurs_ua);
        dest.writeString(unitlink_kombinasi_ua);
        dest.writeString(unitlink_kurs_total_ua);
        dest.writeInt(carabayar_ua);
        dest.writeInt(bentukbayar_ua);
        dest.writeString(awalpertanggungan_ua);
        dest.writeString(akhirpertanggungan_ua);
        dest.writeInt(masa_pembayaran_ua);
        dest.writeTypedList(ListModelAskesUA);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UsulanAsuransiModel> CREATOR = new Creator<UsulanAsuransiModel>() {
        @Override
        public UsulanAsuransiModel createFromParcel(Parcel in) {
            return new UsulanAsuransiModel(in);
        }

        @Override
        public UsulanAsuransiModel[] newArray(int size) {
            return new UsulanAsuransiModel[size];
        }
    };

    public int getJenis_produk_ua() {
        return jenis_produk_ua;
    }

    public void setJenis_produk_ua(int jenis_produk_ua) {
        this.jenis_produk_ua = jenis_produk_ua;
    }

    public int getKd_produk_ua() {
        return kd_produk_ua;
    }

    public void setKd_produk_ua(int kd_produk_ua) {
        this.kd_produk_ua = kd_produk_ua;
    }

    public int getSub_produk_ua() {
        return sub_produk_ua;
    }

    public void setSub_produk_ua(int sub_produk_ua) {
        this.sub_produk_ua = sub_produk_ua;
    }

    public int getKlas_ua() {
        return klas_ua;
    }

    public void setKlas_ua(int klas_ua) {
        this.klas_ua = klas_ua;
    }

    public int getPaket_ua() {
        return paket_ua;
    }

    public void setPaket_ua(int paket_ua) {
        this.paket_ua = paket_ua;
    }

    public int getEkasehat_plan_ua() {
        return ekasehat_plan_ua;
    }

    public void setEkasehat_plan_ua(int ekasehat_plan_ua) {
        this.ekasehat_plan_ua = ekasehat_plan_ua;
    }

    public int getMasa_pertanggungan_ua() {
        return masa_pertanggungan_ua;
    }

    public void setMasa_pertanggungan_ua(int masa_pertanggungan_ua) {
        this.masa_pertanggungan_ua = masa_pertanggungan_ua;
    }

    public int getCuti_premi_ua() {
        return cuti_premi_ua;
    }

    public void setCuti_premi_ua(int cuti_premi_ua) {
        this.cuti_premi_ua = cuti_premi_ua;
    }

    public String getKurs_up_ua() {
        return kurs_up_ua;
    }

    public void setKurs_up_ua(String kurs_up_ua) {
        this.kurs_up_ua = kurs_up_ua;
    }

    public Double getUp_ua() {
        return up_ua;
    }

    public void setUp_ua(Double up_ua) {
        this.up_ua = up_ua;
    }

    public int getUnitlink_opsipremi_ua() {
        return unitlink_opsipremi_ua;
    }

    public void setUnitlink_opsipremi_ua(int unitlink_opsipremi_ua) {
        this.unitlink_opsipremi_ua = unitlink_opsipremi_ua;
    }

    public String getUnitlink_kurs_ua() {
        return unitlink_kurs_ua;
    }

    public void setUnitlink_kurs_ua(String unitlink_kurs_ua) {
        this.unitlink_kurs_ua = unitlink_kurs_ua;
    }

    public Double getUnitlink_premistandard_ua() {
        return unitlink_premistandard_ua;
    }

    public void setUnitlink_premistandard_ua(Double unitlink_premistandard_ua) {
        this.unitlink_premistandard_ua = unitlink_premistandard_ua;
    }

    public String getUnitlink_kombinasi_ua() {
        return unitlink_kombinasi_ua;
    }

    public void setUnitlink_kombinasi_ua(String unitlink_kombinasi_ua) {
        this.unitlink_kombinasi_ua = unitlink_kombinasi_ua;
    }

    public String getUnitlink_kurs_total_ua() {
        return unitlink_kurs_total_ua;
    }

    public void setUnitlink_kurs_total_ua(String unitlink_kurs_total_ua) {
        this.unitlink_kurs_total_ua = unitlink_kurs_total_ua;
    }

    public Double getUnitlink_total_ua() {
        return unitlink_total_ua;
    }

    public void setUnitlink_total_ua(Double unitlink_total_ua) {
        this.unitlink_total_ua = unitlink_total_ua;
    }

    public int getCarabayar_ua() {
        return carabayar_ua;
    }

    public void setCarabayar_ua(int carabayar_ua) {
        this.carabayar_ua = carabayar_ua;
    }

    public int getBentukbayar_ua() {
        return bentukbayar_ua;
    }

    public void setBentukbayar_ua(int bentukbayar_ua) {
        this.bentukbayar_ua = bentukbayar_ua;
    }

    public String getAwalpertanggungan_ua() {
        return awalpertanggungan_ua;
    }

    public void setAwalpertanggungan_ua(String awalpertanggungan_ua) {
        this.awalpertanggungan_ua = awalpertanggungan_ua;
    }

    public String getAkhirpertanggungan_ua() {
        return akhirpertanggungan_ua;
    }

    public void setAkhirpertanggungan_ua(String akhirpertanggungan_ua) {
        this.akhirpertanggungan_ua = akhirpertanggungan_ua;
    }

    public int getMasa_pembayaran_ua() {
        return masa_pembayaran_ua;
    }

    public void setMasa_pembayaran_ua(int masa_pembayaran_ua) {
        this.masa_pembayaran_ua = masa_pembayaran_ua;
    }

    public List<ModelAskesUA> getListModelAskesUA() {
        return ListModelAskesUA;
    }

    public void setListModelAskesUA(List<ModelAskesUA> listModelAskesUA) {
        ListModelAskesUA = listModelAskesUA;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public static Creator<UsulanAsuransiModel> getCREATOR() {
        return CREATOR;
    }
}
