package id.co.ajsmsig.nb.espaj.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.AutoCompleteTextViewClickListener;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.hardcode.RequestCode;
import id.co.ajsmsig.nb.espaj.activity.E_FormAsuransiTambahanActivity;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.adapter.ListRiderAdapter;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.database.E_Update;
import id.co.ajsmsig.nb.espaj.method.GetTime;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.method.NumberTextWatcher;
import id.co.ajsmsig.nb.espaj.method.ProgressDialogClass;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAddRiderUA;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAskesUA;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.PaymentEvent;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static id.co.ajsmsig.nb.crm.method.StaticMethods.toCalendar;

/**
 * Created by Bernard on 24/05/2017.
 */

public class E_UsulanAsuransiFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, AdapterView.OnItemClickListener, RadioGroup.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener {

    public E_UsulanAsuransiFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        groupId = sharedPreferences.getInt("GROUP_ID", 0);
        JENIS_LOGIN = sharedPreferences.getInt("JENIS_LOGIN", 0);
        JENIS_LOGIN_BC = sharedPreferences.getInt("JENIS_LOGIN_BC", 2);
        KODE_REG1 = sharedPreferences.getString("KODE_REG1", "");
        KODE_REG2 = sharedPreferences.getString("KODE_REG2", "");
        KODE_REG3 = sharedPreferences.getString("KODE_REG3", "");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem menu_validasi = menu.findItem(R.id.menu_validasi);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.e_fragment_usulan_asuransi_layout, container,
                false);
        me = E_MainActivity.e_bundle.getParcelable(getString(R.string.modelespaj));
        ButterKnife.bind(this, view);
        ((E_MainActivity) getActivity()).setSecondToolbar("Usulan Asuransi (4/", 4);

        ListRider = new ArrayList<ModelAddRiderUA>();
        ListAskes = new ArrayList<ModelAskesUA>();

        //autocompletetextview

        ac_channel_distribusi_ua.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_channel_distribusi_ua, this));
        ac_channel_distribusi_ua.setOnClickListener(this);
        ac_channel_distribusi_ua.setOnFocusChangeListener(this);
        ac_channel_distribusi_ua.addTextChangedListener(new generalTextWatcher(ac_channel_distribusi_ua));

        ac_produk_ua.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_produk_ua, this));
        ac_produk_ua.setOnClickListener(this);
        ac_produk_ua.setOnFocusChangeListener(this);
        ac_produk_ua.addTextChangedListener(new generalTextWatcher(ac_produk_ua));

        ac_paket_ua.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_paket_ua, this));
        ac_paket_ua.setOnClickListener(this);
        ac_paket_ua.setOnFocusChangeListener(this);
        ac_paket_ua.addTextChangedListener(new generalTextWatcher(ac_paket_ua));

        ac_nm_produk_ua.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_nm_produk_ua, this));
        ac_nm_produk_ua.setOnClickListener(this);
        ac_nm_produk_ua.setOnFocusChangeListener(this);
        ac_nm_produk_ua.addTextChangedListener(new generalTextWatcher(ac_nm_produk_ua));

        ac_carabayar_ua.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_carabayar_ua, this));
        ac_carabayar_ua.setOnClickListener(this);
        ac_carabayar_ua.setOnFocusChangeListener(this);
        ac_carabayar_ua.addTextChangedListener(new generalTextWatcher(ac_carabayar_ua));

        ac_opsi_premi_ua.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_opsi_premi_ua, this));
        ac_opsi_premi_ua.setOnClickListener(this);
        ac_opsi_premi_ua.setOnFocusChangeListener(this);
        ac_opsi_premi_ua.addTextChangedListener(new generalTextWatcher(ac_opsi_premi_ua));

        ac_kombinasi_ua.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_kombinasi_ua, this));
        ac_kombinasi_ua.setOnClickListener(this);
        ac_kombinasi_ua.setOnFocusChangeListener(this);
        ac_kombinasi_ua.addTextChangedListener(new generalTextWatcher(ac_kombinasi_ua));

        ac_bntukbayar_ua.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_bntukbayar_ua, this));
        ac_bntukbayar_ua.setOnClickListener(this);
        ac_bntukbayar_ua.setOnFocusChangeListener(this);
        ac_bntukbayar_ua.addTextChangedListener(new generalTextWatcher(ac_bntukbayar_ua));

        ac_kurs_premi_standart_pokok_ua.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_kurs_premi_standart_pokok_ua, this));
        ac_kurs_premi_standart_pokok_ua.setOnClickListener(this);
        ac_kurs_premi_standart_pokok_ua.setOnFocusChangeListener(this);
        ac_kurs_premi_standart_pokok_ua.addTextChangedListener(new generalTextWatcher(ac_kurs_premi_standart_pokok_ua));

        ac_kurs_total_premi_ua.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_kurs_total_premi_ua, this));
        ac_kurs_total_premi_ua.setOnClickListener(this);
        ac_kurs_total_premi_ua.setOnFocusChangeListener(this);
        ac_kurs_total_premi_ua.addTextChangedListener(new generalTextWatcher(ac_kurs_total_premi_ua));

        ac_kurs_uang_pertanggungan_ua.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_kurs_uang_pertanggungan_ua, this));
        ac_kurs_uang_pertanggungan_ua.setOnClickListener(this);
        ac_kurs_uang_pertanggungan_ua.setOnFocusChangeListener(this);
        ac_kurs_uang_pertanggungan_ua.addTextChangedListener(new generalTextWatcher(ac_kurs_uang_pertanggungan_ua));

        //button
//        next_btn_ua.setOnClickListener(this);
//        prev_btn_ua.setOnClickListener(this);

        //edittext
        et_bayar_premi_ua.addTextChangedListener(new generalTextWatcher(et_bayar_premi_ua));
        et_bayar_premi_ua.addTextChangedListener(new NumberTextWatcher(et_bayar_premi_ua));

        et_total_premi_ua.addTextChangedListener(new generalTextWatcher(et_total_premi_ua));
        et_total_premi_ua.addTextChangedListener(new NumberTextWatcher(et_total_premi_ua));

        et_uang_pertanggungan_ua.addTextChangedListener(new generalTextWatcher(et_uang_pertanggungan_ua));
        et_uang_pertanggungan_ua.addTextChangedListener(new NumberTextWatcher(et_uang_pertanggungan_ua));

        et_premi_standart_pokok_ua.addTextChangedListener(new generalTextWatcher(et_premi_standart_pokok_ua));
        et_premi_standart_pokok_ua.addTextChangedListener(new NumberTextWatcher(et_premi_standart_pokok_ua));

        et_tgl_awal_ua.setOnClickListener(this);
        et_tgl_awal_ua.setOnFocusChangeListener(this);
        et_tgl_awal_ua.addTextChangedListener(new generalTextWatcher(et_tgl_awal_ua));

        et_tgl_akhir_ua.addTextChangedListener(new generalTextWatcher(et_tgl_akhir_ua));
        et_masa_pembayaran_stable_link_save_ua.addTextChangedListener(new generalTextWatcher(et_masa_pembayaran_stable_link_save_ua));
        et_masa_tanggung_ua.addTextChangedListener(new generalTextWatcher(et_masa_tanggung_ua));
        et_cutipremi_ua.addTextChangedListener(new generalTextWatcher(et_cutipremi_ua));

        //radio Group
        rg_stable_link_save_ua.setOnCheckedChangeListener(this);

        //checkbox
        cb_special_case_bp_rate_ua.setOnCheckedChangeListener(this);
        cb_karyawan_smile_ua.setOnCheckedChangeListener(this);
        cb_plan_provider_ua.setOnCheckedChangeListener(this);

        //button di Activity
        btn_lanjut = getActivity().findViewById(R.id.btn_lanjut);
        btn_lanjut.setOnClickListener(this);
        btn_kembali = getActivity().findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(this);
        second_toolbar = ((E_MainActivity) getActivity()).getSecond_toolbar();
        iv_save = second_toolbar.findViewById(R.id.iv_save);
        iv_save.setOnClickListener(this);
        iv_next = second_toolbar.findViewById(R.id.iv_next);
        iv_next.setOnClickListener(this);
        iv_prev = second_toolbar.findViewById(R.id.iv_prev);
        iv_prev.setOnClickListener(this);

        lv_tambah_rider_ua.setOnClickListener(this);

        restore();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        ProgressDialogClass.removeSimpleProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        MethodSupport.active_view(1, btn_lanjut);
        MethodSupport.active_view(1, iv_next);
        MethodSupport.active_view(1, btn_kembali);
        MethodSupport.active_view(1, iv_prev);
        ProgressDialogClass.removeSimpleProgressDialog();
        try {
            if (me.getDetilAgenModel().getJENIS_LOGIN() == 2) {
                if (me.getUsulanAsuransiModel().getKd_produk_ua() == 190 && me.getUsulanAsuransiModel().getSub_produk_ua() == 3){
                    MethodSupport.load_setDropXml(R.xml.e_bentukpremi, ac_bntukbayar_ua, getContext(), 10);
                }else {
                    MethodSupport.load_setDropXml(R.xml.e_bentukpremi, ac_bntukbayar_ua, getContext(), 8);
                }
            } else if (me.getDetilAgenModel().getJENIS_LOGIN() == 9) {
                if (me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 12
                        || me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 5
                        || me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 8
                        || me.getUsulanAsuransiModel().getKd_produk_ua() == 190 && me.getUsulanAsuransiModel().getSub_produk_ua() == 7
                        || me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 7
                        || me.getUsulanAsuransiModel().getKd_produk_ua() == 215 && me.getUsulanAsuransiModel().getSub_produk_ua() == 3
                        || me.getUsulanAsuransiModel().getKd_produk_ua() == 215 && me.getUsulanAsuransiModel().getSub_produk_ua() == 1
                        || me.getUsulanAsuransiModel().getKd_produk_ua() == 215 && me.getUsulanAsuransiModel().getSub_produk_ua() == 4) {
                    MethodSupport.load_setDropXml(R.xml.e_bentukpremi, ac_bntukbayar_ua, getContext(), 10);
                } else {
                    MethodSupport.load_setDropXml(R.xml.e_bentukpremi, ac_bntukbayar_ua, getContext(), 3);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        /*if (isAlreadyHaveTransactionNumber() && isCreditCardPaymentType()) {
            disablePaymentDropdown();
        }*/

    }
    /*private boolean isAlreadyHaveTransactionNumber() {
        String spajIdTab = me.getModelID().getSPAJ_ID_TAB();
        E_Select select = new E_Select(getActivity());

        String transactionNumber = select.getPaymentTransactionNumber(spajIdTab);

        if (!TextUtils.isEmpty(transactionNumber)) {
            return true;
        }

        return false;
    }

    private boolean isCreditCardPaymentType() {
        String spajIdTab = me.getModelID().getSPAJ_ID_TAB();
        E_Select select = new E_Select(getActivity());

        int paymentType = select.getPaymentType(spajIdTab);
        if (paymentType == Const.CREDIT_CARD_PAYMENT_CODE) {
            return true;
        }

        return false;
    }*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_validasi:
                me.getUsulanAsuransiModel().setValidation(Validation());
                loadview();
                MyTaskValidasi taskValidasi = new MyTaskValidasi();
                taskValidasi.execute();
                Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTaskValidasi extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_USULAN_ASURANSI);
            return null;
        }
    }

    private class MyTaskSavePage extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_USULAN_ASURANSI);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
//View di activity
            case R.id.et_tgl_awal_ua:
                showDateDialog(1, et_tgl_awal_ua.getText().toString(), et_tgl_awal_ua);
                break;
            case R.id.btn_lanjut:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_next:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_save:
                loadview();
                MyTaskSavePage taskSavePage = new MyTaskSavePage();
                taskSavePage.execute();

                break;
            case R.id.iv_prev:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.btn_kembali:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            //drop down
            case R.id.ac_channel_distribusi_ua:
                ac_channel_distribusi_ua.showDropDown();
                break;
            case R.id.ac_produk_ua:
                ac_produk_ua.showDropDown();
                break;
            case R.id.ac_paket_ua:
                ac_paket_ua.showDropDown();
                break;
            case R.id.ac_nm_produk_ua:
                ac_nm_produk_ua.showDropDown();
                break;
            case R.id.ac_opsi_premi_ua:
                ac_opsi_premi_ua.showDropDown();
                break;
            case R.id.ac_carabayar_ua:
                ac_carabayar_ua.showDropDown();
                break;
            case R.id.ac_kombinasi_ua:
                ac_kombinasi_ua.showDropDown();
                break;
            case R.id.ac_bntukbayar_ua:
                ac_bntukbayar_ua.showDropDown();
                break;
            case R.id.ac_kurs_premi_standart_pokok_ua:
                ac_kurs_premi_standart_pokok_ua.showDropDown();
                break;
            case R.id.ac_kurs_total_premi_ua:
                ac_kurs_total_premi_ua.showDropDown();
                break;
            case R.id.ac_kurs_uang_pertanggungan_ua:
                ac_kurs_uang_pertanggungan_ua.showDropDown();
                break;
            //recycleView
            case R.id.lv_tambah_rider_ua:

                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.ac_channel_distribusi_ua:
                if (hasFocus) ac_channel_distribusi_ua.showDropDown();
                break;
            case R.id.ac_produk_ua:
                if (hasFocus) ac_produk_ua.showDropDown();
                break;
            case R.id.ac_paket_ua:
                if (hasFocus) ac_paket_ua.showDropDown();
                break;
            case R.id.ac_nm_produk_ua:
                if (hasFocus) ac_nm_produk_ua.showDropDown();
                break;
            case R.id.ac_opsi_premi_ua:
                if (hasFocus) ac_opsi_premi_ua.showDropDown();
                break;
            case R.id.ac_carabayar_ua:
                if (hasFocus) ac_carabayar_ua.showDropDown();
                break;
            case R.id.ac_kombinasi_ua:
                if (hasFocus) ac_kombinasi_ua.showDropDown();
                break;
            case R.id.ac_bntukbayar_ua:
                if (hasFocus) ac_bntukbayar_ua.showDropDown();
                break;
            case R.id.ac_kurs_premi_standart_pokok_ua:
                if (hasFocus) ac_kurs_premi_standart_pokok_ua.showDropDown();
                break;
            case R.id.ac_kurs_total_premi_ua:
                if (hasFocus) ac_kurs_total_premi_ua.showDropDown();
                break;
            case R.id.ac_kurs_uang_pertanggungan_ua:
                if (hasFocus) ac_kurs_uang_pertanggungan_ua.showDropDown();
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.ac_channel_distribusi_ua:
                ModelDropdownInt modelChannel = (ModelDropdownInt) parent.getAdapter().getItem(position);
                kd_jns_produk = modelChannel.getId();
                break;
            case R.id.ac_produk_ua:
                DropboxModel modelProduk_ua = (DropboxModel) parent.getAdapter().getItem(position);
                produk = modelProduk_ua.getId();
//                if (produk != me.getUsulanAsuransiModel().getKd_produk_ua()) {
                setAdapterSubProduk();
                ac_nm_produk_ua.setText("");
//                }
                me.getUsulanAsuransiModel().setKd_produk_ua(produk);
                break;
            case R.id.ac_nm_produk_ua:
                DropboxModel modelNm_produk_ua = (DropboxModel) parent.getAdapter().getItem(position);
                nm_produk = modelNm_produk_ua.getId();
                break;
            case R.id.ac_paket_ua:
                ModelDropdownInt modelPaket_ua = (ModelDropdownInt) parent.getAdapter().getItem(position);
                paket = modelPaket_ua.getId();
                break;
            case R.id.ac_opsi_premi_ua:
//                ModelDropdownInt modelCarabayar_ua = (ModelDropdownInt) parent.getAdapter().getItem(position);
//                carabayar = modelCarabayar_ua.getId();
                break;
            case R.id.ac_carabayar_ua:
                ModelDropdownInt modelCarabayar_ua = (ModelDropdownInt) parent.getAdapter().getItem(position);
                carabayar = modelCarabayar_ua.getId();
                break;
            case R.id.ac_kombinasi_ua:
                ModelDropDownString modelKombinasi_ua = (ModelDropDownString) parent.getAdapter().getItem(position);
                kombinasi = modelKombinasi_ua.getId();
                break;
            case R.id.ac_bntukbayar_ua:
                ModelDropdownInt modelBntukbayar_ua = (ModelDropdownInt) parent.getAdapter().getItem(position);
                bntukbayar = modelBntukbayar_ua.getId();

//                Log.v(E_UsulanAsuransiFragment.class.getSimpleName(), "Payment method id : "+bntukbayar);

//               if (groupId == Const.SIAP2U_GROUP_ID) {
//                    //Save the selection to database
//                    ((E_MainActivity) getActivity()).onSave(Const.PAGE_USULAN_ASURANSI);
//
//                    //Update the payment type on E_MST_SPAJ table according to user latest selected payment type
//                    String spajIdTab = me.getModelID().getSPAJ_ID_TAB();
//
//                    E_Update update = new E_Update(getActivity());
//
//                    //Update to E_USULAN_ASURANSI` table
//                    update.updatePaymentTypeOnUsulanAsuransiTable(spajIdTab, String.valueOf(bntukbayar));
//                }


                break;
            case R.id.ac_kurs_premi_standart_pokok_ua:

                break;
            case R.id.ac_kurs_total_premi_ua:

                break;
            case R.id.ac_kurs_uang_pertanggungan_ua:

                break;
        }
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    private class generalTextWatcher implements TextWatcher {
        private View view;

        generalTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                //autotextcompletetextview
                case R.id.ac_channel_distribusi_ua:
                    if (StaticMethods.isTextWidgetEmpty(ac_channel_distribusi_ua)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jns_produk_ua.setColorFilter(color);
                    tv_error_jns_produk_ua.setVisibility(View.GONE);
                    break;
                case R.id.ac_produk_ua:
                    if (StaticMethods.isTextWidgetEmpty(ac_produk_ua)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_produk_ua.setColorFilter(color);
                    tv_error_produk_ua.setVisibility(View.GONE);
                    break;
                case R.id.ac_nm_produk_ua:
                    if (StaticMethods.isTextWidgetEmpty(ac_nm_produk_ua)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_nm_produk_ua.setColorFilter(color);
                    tv_error_nm_produk_ua.setVisibility(View.GONE);
                    break;
                case R.id.ac_carabayar_ua:
                    if (StaticMethods.isTextWidgetEmpty(ac_carabayar_ua)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_carabayar_ua.setColorFilter(color);
                    tv_error_carabayar_ua.setVisibility(View.GONE);
                    break;
                case R.id.ac_opsi_premi_ua:
                    if (StaticMethods.isTextWidgetEmpty(ac_opsi_premi_ua)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_opsi_premi_ua.setColorFilter(color);
                    tv_error_opsi_premi_ua.setVisibility(View.GONE);
                    break;
                case R.id.ac_kombinasi_ua:
                    if (StaticMethods.isTextWidgetEmpty(ac_kombinasi_ua)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_kombinasi_ua.setColorFilter(color);
                    tv_error_kombinasi_ua.setVisibility(View.GONE);
                    break;
                case R.id.ac_bntukbayar_ua:
                    if (StaticMethods.isTextWidgetEmpty(ac_bntukbayar_ua)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_bntukbayar_ua.setColorFilter(color);
                    tv_error_bntukbayar_ua.setVisibility(View.GONE);
                    break;
                case R.id.ac_kurs_premi_standart_pokok_ua:
                    if (StaticMethods.isTextWidgetEmpty(ac_kurs_premi_standart_pokok_ua)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_kurs_premi_standart_pokok_ua.setColorFilter(color);
                    tv_error_premi_standart_pokok_ua.setVisibility(View.GONE);
                    break;
                case R.id.ac_kurs_total_premi_ua:
                    if (StaticMethods.isTextWidgetEmpty(ac_kurs_total_premi_ua)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_kurs_total_premi_ua.setColorFilter(color);
                    tv_error_total_premi_ua.setVisibility(View.GONE);
                    break;
                case R.id.ac_kurs_uang_pertanggungan_ua:
                    if (StaticMethods.isTextWidgetEmpty(ac_kurs_uang_pertanggungan_ua)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_kurs_uang_pertanggungan_ua.setColorFilter(color);
                    tv_error_uang_pertanggungan_ua.setVisibility(View.GONE);
                    break;
                case R.id.ac_paket_ua:
                    if (StaticMethods.isTextWidgetEmpty(ac_paket_ua)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_paket_ua.setColorFilter(color);
                    tv_error_paket_ua.setVisibility(View.GONE);
                    break;

                //edittext
                case R.id.et_bayar_premi_ua:
                    Method_Validator.ValEditWatcher(et_bayar_premi_ua, iv_circle_masa_tanggung_ua,
                            tv_error_bayar_premi_ua, getContext());
                    break;
                case R.id.et_total_premi_ua:
                    Method_Validator.ValEditWatcher(et_total_premi_ua, iv_circle_kurs_total_premi_ua,
                            tv_error_total_premi_ua, getContext());
                    break;
                case R.id.et_uang_pertanggungan_ua:
                    Method_Validator.ValEditWatcher(et_uang_pertanggungan_ua, iv_circle_kurs_uang_pertanggungan_ua,
                            tv_error_uang_pertanggungan_ua, getContext());
                    break;
                case R.id.et_tgl_awal_ua:
                    Method_Validator.ValEditWatcher(et_tgl_awal_ua, iv_circle_tgl_awal_ua,
                            tv_error_tgl_awal_ua, getContext());
                    break;
                case R.id.et_tgl_akhir_ua:
                    Method_Validator.ValEditWatcher(et_tgl_akhir_ua, iv_circle_tgl_akhir_ua,
                            tv_error_tgl_akhir_ua, getContext());
                    break;
                case R.id.et_premi_standart_pokok_ua:
                    Method_Validator.ValEditWatcher(et_premi_standart_pokok_ua, iv_circle_kurs_premi_standart_pokok_ua,
                            tv_error_premi_standart_pokok_ua, getContext());
                    break;
                case R.id.et_cutipremi_ua:
                    Method_Validator.ValEditWatcher(et_cutipremi_ua, iv_circle_cutipremi_ua,
                            tv_error_cutipremi_ua, getContext());
                    break;
                case R.id.et_masa_tanggung_ua:
                    Method_Validator.ValEditWatcher(et_masa_tanggung_ua, iv_circle_masa_tanggung_ua,
                            tv_error_masa_tanggung_ua, getContext());
                    break;
            }
        }
    }

    private void loadview() {
        me.getUsulanAsuransiModel().setJenis_produk_ua(kd_jns_produk);
        me.getUsulanAsuransiModel().setKd_produk_ua(produk);
        me.getUsulanAsuransiModel().setSub_produk_ua(nm_produk);
        me.getUsulanAsuransiModel().setKlas_ua(klas_ua);
        me.getUsulanAsuransiModel().setPaket_ua(paket);
        me.getUsulanAsuransiModel().setEkasehat_plan_ua(ekasehat_plan);
        me.getUsulanAsuransiModel().setMasa_pertanggungan_ua(Integer.parseInt(et_masa_tanggung_ua.getText().toString()));
        me.getUsulanAsuransiModel().setCuti_premi_ua(Integer.parseInt(et_cutipremi_ua.getText().toString()));
        me.getUsulanAsuransiModel().setKurs_up_ua(up);
        me.getUsulanAsuransiModel().setUp_ua(MethodSupport.ConverttoDouble(et_uang_pertanggungan_ua));
        me.getUsulanAsuransiModel().setUnitlink_opsipremi_ua(opsi_premi);
        me.getUsulanAsuransiModel().setUnitlink_kurs_ua(premi_standart);
        me.getUsulanAsuransiModel().setUnitlink_premistandard_ua(MethodSupport.ConverttoDouble(et_premi_standart_pokok_ua));
        me.getUsulanAsuransiModel().setUnitlink_kombinasi_ua(kombinasi);
        me.getUsulanAsuransiModel().setUnitlink_kurs_total_ua(total_premi);
        me.getUsulanAsuransiModel().setUnitlink_total_ua(MethodSupport.ConverttoDouble(et_total_premi_ua));
        me.getUsulanAsuransiModel().setCarabayar_ua(carabayar);
        me.getUsulanAsuransiModel().setBentukbayar_ua(bntukbayar);
        me.getUsulanAsuransiModel().setAwalpertanggungan_ua(et_tgl_awal_ua.getText().toString());
        me.getUsulanAsuransiModel().setAkhirpertanggungan_ua(et_tgl_akhir_ua.getText().toString());
        me.getUsulanAsuransiModel().setMasa_pembayaran_ua(Integer.parseInt(et_bayar_premi_ua.getText().toString()));
        me.setListRiderUA(ListRider);
        for (int i = 0; i < ListRider.size(); i++) {
            for (int j = 0; j < ListAskes.size(); j++) {
                if ((ListRider.get(i).getKode_produk_rider() == ListAskes.get(j).getKode_produk_rider()) &&
                        (ListRider.get(i).getKode_subproduk_rider() == ListAskes.get(j).getKode_subproduk_rider())) {
                    ListAskes.get(j).setCounter(ListRider.get(i).getCounter());
                    ListAskes.get(j).setPekerjaan_askes(ListRider.get(i).getPekerjaan_askes());
                    ListAskes.get(j).setJekel_askes(ListRider.get(i).getJekel_askes());
                    ListAskes.get(j).setTtl_askes(ListRider.get(i).getTtl_askes());
                    ListAskes.get(j).setUsia_askes(ListRider.get(i).getUsia_askes());
                    ListAskes.get(j).setHubungan_askes(ListRider.get(i).getHubungan_askes());
                    ListAskes.get(j).setProduk_askes(ListRider.get(i).getProduk_askes());
                    ListAskes.get(j).setRider_askes(ListRider.get(i).getRider_askes());
                    ListAskes.get(j).setKode_produk_rider(ListRider.get(i).getKode_produk_rider());
                    ListAskes.get(j).setKode_subproduk_rider(ListRider.get(i).getKode_subproduk_rider());
                    ListAskes.get(j).setTinggi_askes(ListRider.get(i).getTinggi_askes());
                    ListAskes.get(j).setBerat_askes(ListRider.get(i).getBerat_askes());
                    ListAskes.get(j).setWarganegara_askes(ListRider.get(i).getWarganegara_askes());
                    ListAskes.get(j).setPekerjaan_askes(ListRider.get(i).getPekerjaan_askes());
                    ListAskes.get(j).setValidation(ListRider.get(i).getVal_rider());
                }
            }
        }
        me.setListASkesUA(ListAskes);
        E_MainActivity.e_bundle.putParcelable(getString(R.string.modelespaj), me);
    }

    private void restore() {
//        try {
        setAdapterProduk();
        setAdapterSubProduk();
        setAdapterKurs();
        setADapterCaraBayar();
        //autocomplete
        ListDropDownInt = new E_Select(getContext()).getLstChannelDist(JENIS_LOGIN, JENIS_LOGIN_BC, KODE_REG1, KODE_REG2);
        kd_jns_produk = ListDropDownInt.get(0).getId();
        ac_channel_distribusi_ua.setText(ListDropDownInt.get(0).getValue());

        produk = me.getUsulanAsuransiModel().getKd_produk_ua();
        ListDropInt = new P_Select(getContext()).selectListRencana(groupId);
        ac_produk_ua.setText(MethodSupport.getAdapterPosition(ListDropInt, produk));
        setAdapterSubProduk();

        nm_produk = me.getUsulanAsuransiModel().getSub_produk_ua();
        ListDropInt = new P_Select(getContext()).selectListRencanaSub(groupId, produk);
        ac_nm_produk_ua.setText(MethodSupport.getSubAdapterPosition(ListDropInt, nm_produk));
        if (new E_Select(getContext()).getFlagPaket(produk, nm_produk) == 0) {
            ac_paket_ua.setVisibility(View.GONE);
            iv_circle_paket_ua.setVisibility(View.GONE);
        }

        carabayar = me.getUsulanAsuransiModel().getCarabayar_ua();
        ListDropDownInt = new E_Select(getContext()).getCaraBayar(produk, nm_produk);
        ac_carabayar_ua.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownInt, carabayar));

        premi_standart = me.getUsulanAsuransiModel().getUnitlink_kurs_ua();
        ListDropDownString = new E_Select(getContext()).getLst_Kurs(produk, nm_produk);
        ac_kurs_premi_standart_pokok_ua.setText(MethodSupport.getAdapterPositionString(ListDropDownString, premi_standart));

        paket = me.getUsulanAsuransiModel().getPaket_ua();
        ListDropInt = new P_Select(getContext()).selectListPacket(groupId, me.getUsulanAsuransiModel().getKd_produk_ua());
        ac_paket_ua.setText(MethodSupport.getSubAdapterPacket(ListDropInt, paket));

        total_premi = me.getUsulanAsuransiModel().getUnitlink_kurs_total_ua();
        ListDropDownString = new E_Select(getContext()).getLst_Kurs(produk, nm_produk);
        ac_kurs_total_premi_ua.setText(MethodSupport.getAdapterPositionString(ListDropDownString, total_premi));

        up = me.getUsulanAsuransiModel().getKurs_up_ua();
        ListDropDownString = new E_Select(getContext()).getLst_Kurs(produk, nm_produk);
        ac_kurs_uang_pertanggungan_ua.setText(MethodSupport.getAdapterPositionString(ListDropDownString, up));

        opsi_premi = me.getUsulanAsuransiModel().getUnitlink_opsipremi_ua();
//        ac_opsi_premi_ua.setText(MethodSupport.getAdapterPositionSPAJ(ac_opsi_premi_ua.getAdapter(), ListDropDownInt, opsi_premi));
        val_opsipremi(opsi_premi);

        setAdapterKomb();
        kombinasi = me.getUsulanAsuransiModel().getUnitlink_kombinasi_ua();
        ListDropDownString = new E_Select(getContext()).getLst_KombinasiNew(produk, nm_produk);
        ac_kombinasi_ua.setText(MethodSupport.getAdapterPositionString(ListDropDownString, kombinasi));

        bntukbayar = me.getUsulanAsuransiModel().getBentukbayar_ua();
        if (me.getDetilAgenModel().getJENIS_LOGIN() == 2) {
            if (me.getUsulanAsuransiModel().getKd_produk_ua() == 190 && me.getUsulanAsuransiModel().getSub_produk_ua() == 3){
                ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_bentukpremi, 10);
            }else {
                ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_bentukpremi, 8);
            }
        } else if (me.getDetilAgenModel().getJENIS_LOGIN() == 9) {
            //MethodSupport.load_setDropXml(R.xml.e_bentukpremi, ac_bntukbayar_ua, getContext(), 0);
            if (me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 12
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 5
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 8
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 190 && me.getUsulanAsuransiModel().getSub_produk_ua() == 7
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 7
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 215 && me.getUsulanAsuransiModel().getSub_produk_ua() == 3
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 215 && me.getUsulanAsuransiModel().getSub_produk_ua() == 1
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 215 && me.getUsulanAsuransiModel().getSub_produk_ua() == 4) {
                ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_bentukpremi, 10);
            } else {
                ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_bentukpremi, 3);
            }
        }
        //ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_bentukpremi, 3); //bernard
        String s = MethodSupport.getAdapterPositionSPAJ(ListDropDownInt, bntukbayar);
        ac_bntukbayar_ua.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownInt, bntukbayar));

        //edittext
        et_tgl_awal_ua.setText(me.getUsulanAsuransiModel().getAwalpertanggungan_ua());
        MethodSupport.active_view(0, et_tgl_awal_ua);
        et_tgl_akhir_ua.setText(GetTime.getEndDate(me.getUsulanAsuransiModel().getAwalpertanggungan_ua(), me.getUsulanAsuransiModel().getMasa_pertanggungan_ua()));
        MethodSupport.active_view(0, et_tgl_akhir_ua);
        et_masa_tanggung_ua.setText(String.valueOf(me.getUsulanAsuransiModel().getMasa_pertanggungan_ua()));
        et_bayar_premi_ua.setText(String.valueOf(me.getUsulanAsuransiModel().getMasa_pembayaran_ua()));
        et_cutipremi_ua.setText(String.valueOf(me.getUsulanAsuransiModel().getCuti_premi_ua()));
        et_premi_standart_pokok_ua.setText(new BigDecimal(me.getUsulanAsuransiModel().getUnitlink_premistandard_ua()).toString());
        et_total_premi_ua.setText(new BigDecimal(me.getUsulanAsuransiModel().getUnitlink_total_ua()).toString());
        et_uang_pertanggungan_ua.setText(new BigDecimal(me.getUsulanAsuransiModel().getUp_ua()).toString());

        //ListRider
        ListAskes = me.getListASkesUA();
        ListRider = me.getListRiderUA();
        if (!ListRider.isEmpty()) {
            tv_tambah_rider_ua.setVisibility(View.GONE);
            img_tambah_rider_ua.setVisibility(View.GONE);
//                ll_tambah_rider_ua.setVisibility(View.VISIBLE);
            lv_tambah_rider_ua.setVisibility(View.VISIBLE);
            listRiderAdapter = new ListRiderAdapter(getActivity(), R.layout.e_activity_form_asuransi_tambahan_layout, ListRider, me, this);
            lv_tambah_rider_ua.setAdapter(listRiderAdapter);
            lv_tambah_rider_ua.setLayoutManager(new LinearLayoutManager(getActivity()));
//                lv_tambah_rider_ua.setHasFixedSize(true);
            lv_tambah_rider_ua.setNestedScrollingEnabled(false);
        } else {
            tv_tambah_rider_ua.setVisibility(View.VISIBLE);
            img_tambah_rider_ua.setVisibility(View.VISIBLE);
//                ll_tambah_rider_ua.setVisibility(View.GONE);
            lv_tambah_rider_ua.setVisibility(View.GONE);
        }

        //kalau ada nilai proposal
        if (!me.getModelID().getProposal_tab().equals("")) {
            onDeactiveView();
        }
        if (!me.getValidation()) {
            Validation();
        }
        if (me.getModelID().getFlag_aktif() == 1) {
            MethodSupport.disable(false, cl_child_ua);
        }
//        } catch (Exception e) {
//            System.err.println(e.getClass().getName() + ": " + e.getMessage());
//        }
    }
//    kd_jns_produk
//    klas_ua
//    paket
//    ekasehat_plan
//    opsi_premi
//    et_tgl_awal_ua
//    et_tgl_akhir_ua


    private void onDeactiveView() {
        MethodSupport.active_view(0, ac_produk_ua);
        MethodSupport.active_view(0, ac_nm_produk_ua);
        MethodSupport.active_view(0, ac_channel_distribusi_ua);
        MethodSupport.active_view(0, ac_carabayar_ua);
        MethodSupport.active_view(0, ac_kurs_premi_standart_pokok_ua);
        MethodSupport.active_view(0, ac_kurs_total_premi_ua);
        MethodSupport.active_view(0, ac_kurs_uang_pertanggungan_ua);
        MethodSupport.active_view(0, et_bayar_premi_ua);
        MethodSupport.active_view(0, et_cutipremi_ua);
        MethodSupport.active_view(0, et_masa_tanggung_ua);
        MethodSupport.active_view(0, et_total_premi_ua);
        MethodSupport.active_view(0, et_premi_standart_pokok_ua);
        MethodSupport.active_view(0, et_uang_pertanggungan_ua);
        MethodSupport.active_view(0, ac_opsi_premi_ua);
    }


    private void setAdapterProduk() {
        ArrayList<DropboxModel> dRencanas = new P_Select(getContext()).selectListRencana(groupId);
        ArrayAdapter<DropboxModel> mRencanaAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, dRencanas);
        ac_produk_ua.setAdapter(mRencanaAdapter);
    }

    private void setAdapterSubProduk() {
        ArrayList<DropboxModel> dRencanaSubs = new P_Select(getContext()).selectListRencanaSub(groupId, produk);
        ArrayAdapter<DropboxModel> mRencanaSubAdapter = new ArrayAdapter<DropboxModel>(getContext(),
                android.R.layout.simple_dropdown_item_1line, dRencanaSubs);
        ac_nm_produk_ua.setAdapter(mRencanaSubAdapter);
    }

    private void setAdapterKurs() {
        ArrayList<ModelDropDownString> Dropdown = new E_Select(getContext()).getLst_Kurs(me.getUsulanAsuransiModel().getKd_produk_ua(), me.getUsulanAsuransiModel().getSub_produk_ua());
        ArrayAdapter<ModelDropDownString> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_kurs_premi_standart_pokok_ua.setAdapter(Adapter);
        ac_kurs_total_premi_ua.setAdapter(Adapter);
        ac_kurs_uang_pertanggungan_ua.setAdapter(Adapter);
    }

    private void setAdapterKomb() {
        ArrayList<ModelDropDownString> Dropdown = new E_Select(getContext()).getLst_Kombinasi(produk, nm_produk);
        ArrayAdapter<ModelDropDownString> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_kombinasi_ua.setAdapter(Adapter);
//        if (Dropdown.isEmpty()) {
//            opsi_premi = 1;
//        } else {
//            opsi_premi = 0;
//        }
//        val_opsipremi(opsi_premi);
    }

    private void setADapterCaraBayar() {
        ArrayList<ModelDropdownInt> Dropdown = new E_Select(getContext()).getCaraBayar(produk, nm_produk);
        ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_carabayar_ua.setAdapter(Adapter);
    }

    private boolean Validation() {
        boolean val = true;
        int color = ContextCompat.getColor(getContext(), R.color.red);
        if (ac_produk_ua.isShown() && ac_produk_ua.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, ac_produk_ua, tv_error_produk_ua, iv_circle_produk_ua, color, getContext(), getString(R.string.error_field_required));
//            tv_error_produk_ua.setVisibility(View.VISIBLE);
//            tv_error_produk_ua.setText(getString(R.string.error_field_required));
//            iv_circle_produk_ua.setColorFilter(color);
        }
        if (ac_nm_produk_ua.isShown() && ac_nm_produk_ua.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, ac_nm_produk_ua, tv_error_nm_produk_ua, iv_circle_nm_produk_ua, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nm_produk_ua.setVisibility(View.VISIBLE);
//            tv_error_nm_produk_ua.setText(getString(R.string.error_field_required));
//            iv_circle_nm_produk_ua.setColorFilter(color);
        }
        if (ac_paket_ua.isShown() && ac_paket_ua.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, ac_paket_ua, tv_error_paket_ua, iv_circle_paket_ua, color, getContext(), getString(R.string.error_field_required));
//            tv_error_paket_ua.setVisibility(View.VISIBLE);
//            tv_error_paket_ua.setText(getString(R.string.error_field_required));
//            iv_circle_paket_ua.setColorFilter(color);
        }
        if (ac_carabayar_ua.isShown() && ac_carabayar_ua.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, ac_carabayar_ua, tv_error_carabayar_ua, iv_circle_carabayar_ua, color, getContext(), getString(R.string.error_field_required));
//            tv_error_carabayar_ua.setVisibility(View.VISIBLE);
//            tv_error_carabayar_ua.setText(getString(R.string.error_field_required));
//            iv_circle_carabayar_ua.setColorFilter(color);
        }
        if (ac_opsi_premi_ua.isShown() && ac_opsi_premi_ua.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, ac_opsi_premi_ua, tv_error_opsi_premi_ua, iv_circle_opsi_premi_ua, color, getContext(), getString(R.string.error_field_required));
//            tv_error_opsi_premi_ua.setVisibility(View.VISIBLE);
//            tv_error_opsi_premi_ua.setText(getString(R.string.error_field_required));
//            iv_circle_opsi_premi_ua.setColorFilter(color);
        }
        if (et_premi_standart_pokok_ua.isShown() && et_premi_standart_pokok_ua.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, et_premi_standart_pokok_ua, tv_error_premi_standart_pokok_ua, iv_circle_kurs_premi_standart_pokok_ua, color, getContext(), getString(R.string.error_field_required));
//            tv_error_premi_standart_pokok_ua.setVisibility(View.VISIBLE);
//            tv_error_premi_standart_pokok_ua.setText(getString(R.string.error_field_required));
//            iv_circle_kurs_premi_standart_pokok_ua.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_premi_standart_pokok_ua.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, et_premi_standart_pokok_ua, tv_error_premi_standart_pokok_ua, iv_circle_kurs_premi_standart_pokok_ua, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (ac_kombinasi_ua.isShown() && ac_kombinasi_ua.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, ac_kombinasi_ua, tv_error_kombinasi_ua, iv_circle_kombinasi_ua, color, getContext(), getString(R.string.error_field_required));
//            tv_error_kombinasi_ua.setVisibility(View.VISIBLE);
//            tv_error_kombinasi_ua.setText(getString(R.string.error_field_required));
//            iv_circle_kombinasi_ua.setColorFilter(color);
        }
        if (et_total_premi_ua.isShown() && et_total_premi_ua.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, et_total_premi_ua, tv_error_total_premi_ua, iv_circle_kurs_total_premi_ua, color, getContext(), getString(R.string.error_field_required));
//            tv_error_total_premi_ua.setVisibility(View.VISIBLE);
//            tv_error_total_premi_ua.setText(getString(R.string.error_field_required));
//            iv_circle_kurs_total_premi_ua.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_total_premi_ua.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, et_total_premi_ua, tv_error_total_premi_ua, iv_circle_kurs_total_premi_ua, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_uang_pertanggungan_ua.isShown() && et_uang_pertanggungan_ua.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, et_uang_pertanggungan_ua, tv_error_uang_pertanggungan_ua, iv_circle_kurs_uang_pertanggungan_ua, color, getContext(), getString(R.string.error_field_required));
//            tv_error_uang_pertanggungan_ua.setVisibility(View.VISIBLE);
//            tv_error_uang_pertanggungan_ua.setText(getString(R.string.error_field_required));
//            iv_circle_kurs_uang_pertanggungan_ua.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_uang_pertanggungan_ua.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, et_uang_pertanggungan_ua, tv_error_uang_pertanggungan_ua, iv_circle_kurs_uang_pertanggungan_ua, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (ac_bntukbayar_ua.isShown() && ac_bntukbayar_ua.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, ac_bntukbayar_ua, tv_error_bntukbayar_ua, iv_circle_bntukbayar_ua, color, getContext(), getString(R.string.error_field_required));
//            tv_error_bntukbayar_ua.setVisibility(View.VISIBLE);
//            tv_error_bntukbayar_ua.setText(getString(R.string.error_field_required));
//            iv_circle_bntukbayar_ua.setColorFilter(color);
        }
        if (et_tgl_awal_ua.isShown() && et_tgl_awal_ua.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, et_tgl_awal_ua, tv_error_tgl_awal_ua, iv_circle_tgl_awal_ua, color, getContext(), getString(R.string.error_field_required));
//            tv_error_tgl_awal_ua.setVisibility(View.VISIBLE);
//            tv_error_tgl_awal_ua.setText(getString(R.string.error_field_required));
//            iv_circle_tgl_awal_ua.setColorFilter(color);
        }
        if (et_tgl_akhir_ua.isShown() && et_tgl_akhir_ua.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_ua, cl_child_ua, et_tgl_akhir_ua, tv_error_tgl_akhir_ua, iv_circle_tgl_akhir_ua, color, getContext(), getString(R.string.error_field_required));
//            tv_error_tgl_akhir_ua.setVisibility(View.VISIBLE);
//            tv_error_tgl_akhir_ua.setText(getString(R.string.error_field_required));
//            iv_circle_tgl_akhir_ua.setColorFilter(color);
        }

        if (!ListRider.isEmpty()) {
            for (int i = 0; i < ListRider.size(); i++) {
//                if (!ListRider.get(i).getVal_rider()) {
//                    val = false;
//                    tv_error_rider_dp.setText(getString(R.string.error_field_required));
//                    tv_error_rider_dp.setVisibility(View.VISIBLE);
//                }
                if (ListRider.get(i).getPeserta_askes().length() != 0) {
                    if (ListRider.get(i).getPekerjaan_askes().length() == 0 ||
                            ListRider.get(i).getJekel_askes() == -1 ||
                            ListRider.get(i).getTtl_askes().length() == 0 ||
                            ListRider.get(i).getTinggi_askes() == 0 ||
                            ListRider.get(i).getBerat_askes() == 0 ||
                            ListRider.get(i).getWarganegara_askes() == 0) {
                        val = false;
                        tv_error_rider_dp.setText(getString(R.string.error_field_required));
                        tv_error_rider_dp.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
        return val;
    }

    private void showDateDialog(int flag_ttl, String tanggal, EditText edit) {
        switch (flag_ttl) {
            case 1:
                if (tanggal.length() != 0) {
                    cal_awal = toCalendar(tanggal);
                } else {
                    cal_awal = Calendar.getInstance();
                }
                DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                        datePickerListener, cal_awal.get(Calendar.YEAR), cal_awal.get(Calendar.MONTH), cal_awal.get(Calendar.DATE));
                dialog.getDatePicker().setMaxDate(cal_awal.getTimeInMillis());
                dialog.show();
                break;
        }
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            cal_awal.set(Calendar.YEAR, selectedYear);
            cal_awal.set(Calendar.MONTH, selectedMonth);
            cal_awal.set(Calendar.DAY_OF_MONTH, selectedDay);
            et_tgl_awal_ua.setText(StaticMethods.toStringDate(cal_awal, 5));
        }
    };

    private void val_opsipremi(int opsi_premi) {
        if (opsi_premi == 0) {
            ac_opsi_premi_ua.setText("ISI TOTAL PREMI DAN PILIH KOMBINASI");
            ac_kurs_premi_standart_pokok_ua.setVisibility(View.GONE);
            et_premi_standart_pokok_ua.setVisibility(View.GONE);
            iv_circle_kurs_premi_standart_pokok_ua.setVisibility(View.GONE);

            ac_kombinasi_ua.setVisibility(View.VISIBLE);
            iv_circle_kombinasi_ua.setVisibility(View.VISIBLE);
            ac_kurs_total_premi_ua.setVisibility(View.VISIBLE);
            et_total_premi_ua.setVisibility(View.VISIBLE);
            iv_circle_kurs_total_premi_ua.setVisibility(View.VISIBLE);
        } else {
            ac_opsi_premi_ua.setText("ISI PREMI");
            ac_kurs_premi_standart_pokok_ua.setVisibility(View.VISIBLE);
            et_premi_standart_pokok_ua.setVisibility(View.VISIBLE);
            iv_circle_kurs_premi_standart_pokok_ua.setVisibility(View.VISIBLE);

            ac_kombinasi_ua.setVisibility(View.GONE);
            iv_circle_kombinasi_ua.setVisibility(View.GONE);
            ac_kurs_total_premi_ua.setVisibility(View.GONE);
            et_total_premi_ua.setVisibility(View.GONE);
            iv_circle_kurs_total_premi_ua.setVisibility(View.GONE);
        }
    }

    private void onClickLanjut() {
//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setTitle("Melakukan perpindahan halaman");
//        progressDialog.setMessage("Mohon Menunggu...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
//        Runnable progressRunnable = new Runnable() {
//            @Override
//            public void run() {
//                MethodSupport.active_view(0, btn_lanjut);
//                MethodSupport.active_view(0, iv_next);
//                ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
        me.getUsulanAsuransiModel().setValidation(Validation());
        loadview();
        MyTask task = new MyTask();
        task.execute();

//        me.getUsulanAsuransiModel().setValidation(Validation());
//        loadview();
//        ((E_MainActivity) getActivity()).onSave();
//                View cl_child_ua = getActivity().findViewById(R.id.cl_child_ua);
//                File file_bitmap = new File(
//                        getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//                if (!file_bitmap.exists()) {
//                    file_bitmap.mkdirs();
//                }

        //create JPG
//                String fileName = "UA_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
//                Context context = getActivity();
//                MethodSupport.onCreateBitmapScroll(cl_child_ua, scroll_ua, file_bitmap, fileName, context);

//        ((E_MainActivity) getActivity()).setFragment(new E_DetilInvestasiFragment(), getResources().getString(R.string.detil_investasi));
//                progressDialog.cancel();
//            }
//        };

//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 1000);

    }

    private class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_USULAN_ASURANSI);
            return null;
        }

        @Override
        protected void onPostExecute(Void params) {
            ((E_MainActivity) getActivity()).setFragment(new E_DetilInvestasiFragment(), getResources().getString(R.string.detil_investasi));
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    private class MyTaskBack extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_USULAN_ASURANSI);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ((E_MainActivity) getActivity()).setFragment(new E_CalonPembayarPremiFragment(), getResources().getString(R.string.calon_pembayar_premi));
        }
    }

    private void onClickBack() {

        loadview();
        MyTaskBack taskBack = new MyTaskBack();
        taskBack.execute();

//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setTitle("Melakukan perpindahan halaman");
//        progressDialog.setMessage("Mohon Menunggu...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
//        Runnable progressRunnable = new Runnable() {
//
//            @Override
//            public void run() {
//                MethodSupport.active_view(0, btn_kembali);
//                MethodSupport.active_view(0, iv_prev);
//                ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
//
//                loadview();
//                ((E_MainActivity) getActivity()).onSave();
//                ((E_MainActivity) getActivity()).setFragment(new E_CalonPembayarPremiFragment(), getResources().getString(R.string.calon_pembayar_premi));
//                progressDialog.cancel();
//            }
//        };
//
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 1000);
    }

    public void goToForm(int position) {
        Intent intent = new Intent(getContext(), E_FormAsuransiTambahanActivity.class);
        intent.putExtra(getString(R.string.pos), position);
        intent.putExtra(getString(R.string.modelrider), (ListRider.size() == position) ? new ModelAddRiderUA() : ListRider.get(position));
        intent.putExtra(getString(R.string.modelespaj), me);
        startActivityForResult(intent, RequestCode.FILL_RIDER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        switch (requestCode) {
            case RequestCode.FILL_RIDER:
                // Make sure the request was successful
                if (resultCode == RESULT_OK) {
                    Bundle mBundle = data.getExtras();
                    ModelAddRiderUA modelRider = mBundle.getParcelable(getString(R.string.modelrider));
                    int mPos = mBundle.getInt(getString(R.string.pos));

                    if (ListRider.size() == mPos) {
                        ListRider.add(modelRider);
                    } else {
                        ListRider.set(mPos, modelRider);
                    }

                    listRiderAdapter.updateListTP(ListRider);
                }
                break;
            default:
                break;
        }
    }

    private ProgressDialog dialogmessage;


    public void startProgress() {
        dialogmessage = new ProgressDialog(getActivity());
        dialogmessage.setMessage("Mohon Menunggu ");
        dialogmessage.setTitle("Sedang Melakukan Perpindahan Halaman ...");
        dialogmessage.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialogmessage.show();
        dialogmessage.setCancelable(false);
        btn_lanjut.setEnabled(false);
        iv_next.setEnabled(false);
        iv_prev.setEnabled(false);
        btn_kembali.setEnabled(false);
        dialogmessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                btn_lanjut.setEnabled(true);
                iv_next.setEnabled(true);
                iv_prev.setEnabled(true);
                btn_kembali.setEnabled(true);
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (dialogmessage.getProgress() <= dialogmessage
                            .getMax()) {
                        Thread.sleep(10);
                        handle.sendMessage(handle.obtainMessage());
                        if (dialogmessage.getProgress() == dialogmessage
                                .getMax()) {
                            dialogmessage.dismiss();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    Handler handle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            dialogmessage.incrementProgressBy(1);
        }
    };
    @BindView(R.id.tv_jenis_asuransi_pokok_ua)
    TextView tv_jenis_asuransi_pokok_ua;
    @BindView(R.id.tv_error_jns_produk_ua)
    TextView tv_error_jns_produk_ua;
    @BindView(R.id.tv_error_produk_ua)
    TextView tv_error_produk_ua;
    @BindView(R.id.tv_error_nm_produk_ua)
    TextView tv_error_nm_produk_ua;
    @BindView(R.id.tv_error_masa_tanggung_ua)
    TextView tv_error_masa_tanggung_ua;
    @BindView(R.id.tv_error_bayar_premi_ua)
    TextView tv_error_bayar_premi_ua;
    @BindView(R.id.tv_error_carabayar_ua)
    TextView tv_error_carabayar_ua;
    @BindView(R.id.tv_error_opsi_premi_ua)
    TextView tv_error_opsi_premi_ua;
    @BindView(R.id.tv_error_kombinasi_ua)
    TextView tv_error_kombinasi_ua;
    @BindView(R.id.tv_error_uang_pertanggungan_ua)
    TextView tv_error_uang_pertanggungan_ua;
    @BindView(R.id.tv_error_bntukbayar_ua)
    TextView tv_error_bntukbayar_ua;
    @BindView(R.id.tv_bukti_identitas_ua)
    TextView tv_bukti_identitas_ua;
    @BindView(R.id.tv_error_tgl_awal_ua)
    TextView tv_error_tgl_awal_ua;
    @BindView(R.id.tv_error_tgl_akhir_ua)
    TextView tv_error_tgl_akhir_ua;
    @BindView(R.id.tv_jenis_asuransi_tambahan_ua)
    TextView tv_jenis_asuransi_tambahan_ua;
    @BindView(R.id.tv_tambah_rider_ua)
    TextView tv_tambah_rider_ua;
    @BindView(R.id.tv_smile_medical_ua)
    TextView tv_smile_medical_ua;
    @BindView(R.id.tv_error_premi_standart_pokok_ua)
    TextView tv_error_premi_standart_pokok_ua;
    @BindView(R.id.tv_error_total_premi_ua)
    TextView tv_error_total_premi_ua;
    @BindView(R.id.tv_error_cutipremi_ua)
    TextView tv_error_cutipremi_ua;
    @BindView(R.id.tv_error_paket_ua)
    TextView tv_error_paket_ua;
    @BindView(R.id.tv_error_rider_dp)
    TextView tv_error_rider_dp;

    @BindView(R.id.cl_jenis_asuransi_pokok_ua)
    ConstraintLayout cl_jenis_asuransi_pokok_ua;
    @BindView(R.id.cl_form_jenis_asuransi_pokok_ua)
    ConstraintLayout cl_form_jenis_asuransi_pokok_ua;
    @BindView(R.id.cl_paket_ua)
    ConstraintLayout cl_paket_ua;
    @BindView(R.id.cl_jns_produk_ua)
    ConstraintLayout cl_jns_produk_ua;
    @BindView(R.id.cl_produk_ua)
    ConstraintLayout cl_produk_ua;
    @BindView(R.id.cl_nm_produk_ua)
    ConstraintLayout cl_nm_produk_ua;
    @BindView(R.id.cl_masa_tanggung_ua)
    ConstraintLayout cl_masa_tanggung_ua;
    @BindView(R.id.cl_bayar_premi_ua)
    ConstraintLayout cl_bayar_premi_ua;
    @BindView(R.id.cl_carabayar_ua)
    ConstraintLayout cl_carabayar_ua;
    @BindView(R.id.cl_opsi_premi_ua)
    ConstraintLayout cl_opsi_premi_ua;
    @BindView(R.id.cl_kombinasi_ua)
    ConstraintLayout cl_kombinasi_ua;
    @BindView(R.id.cl_total_premi_ua)
    ConstraintLayout cl_total_premi_ua;
    @BindView(R.id.cl_uang_pertanggungan_ua)
    ConstraintLayout cl_uang_pertanggungan_ua;
    @BindView(R.id.cl_bntukbayar_ua)
    ConstraintLayout cl_bntukbayar_ua;
    @BindView(R.id.cl_bukti_identitas_ua)
    ConstraintLayout cl_bukti_identitas_ua;
    @BindView(R.id.cl_form_bukti_identitas_ua)
    ConstraintLayout cl_form_bukti_identitas_ua;
    @BindView(R.id.cl_tgl_awal_ua)
    ConstraintLayout cl_tgl_awal_ua;
    @BindView(R.id.cl_tgl_akhir_ua)
    ConstraintLayout cl_tgl_akhir_ua;
    @BindView(R.id.cl_jenis_asuransi_tambahan_ua)
    ConstraintLayout cl_jenis_asuransi_tambahan_ua;

    @BindView(R.id.cl_stable_link_save_ua)
    ConstraintLayout cl_stable_link_save_ua;
    @BindView(R.id.cl_kategori_stable_link_save_ua)
    ConstraintLayout cl_kategori_stable_link_save_ua;
    @BindView(R.id.cl_kategori_stable_link_save2_ua)
    ConstraintLayout cl_kategori_stable_link_save2_ua;
    @BindView(R.id.cl_masa_pembayaran_stable_link_save_ua)
    ConstraintLayout cl_masa_pembayaran_stable_link_save_ua;
    @BindView(R.id.cl_smile_medical_ua)
    ConstraintLayout cl_smile_medical_ua;
    @BindView(R.id.cl_kurs_premi_standart_pokok_ua)
    ConstraintLayout cl_kurs_premi_standart_pokok_ua;
    @BindView(R.id.cl_kurs_total_premi_ua)
    ConstraintLayout cl_kurs_total_premi_ua;
    @BindView(R.id.cl_kurs_uang_pertanggungan_ua)
    ConstraintLayout cl_kurs_uang_pertanggungan_ua;
    @BindView(R.id.cl_cutipremi_ua)
    ConstraintLayout cl_cutipremi_ua;
    @BindView(R.id.cl_child_ua)
    ConstraintLayout cl_child_ua;

    @BindView(R.id.iv_circle_paket_ua)
    ImageView iv_circle_paket_ua;
    @BindView(R.id.iv_circle_jns_produk_ua)
    ImageView iv_circle_jns_produk_ua;
    @BindView(R.id.iv_circle_produk_ua)
    ImageView iv_circle_produk_ua;
    @BindView(R.id.iv_circle_nm_produk_ua)
    ImageView iv_circle_nm_produk_ua;
    @BindView(R.id.iv_circle_masa_tanggung_ua)
    ImageView iv_circle_masa_tanggung_ua;
    @BindView(R.id.iv_circle_carabayar_ua)
    ImageView iv_circle_carabayar_ua;
    @BindView(R.id.iv_circle_opsi_premi_ua)
    ImageView iv_circle_opsi_premi_ua;
    @BindView(R.id.iv_circle_kombinasi_ua)
    ImageView iv_circle_kombinasi_ua;
    @BindView(R.id.iv_circle_bntukbayar_ua)
    ImageView iv_circle_bntukbayar_ua;
    @BindView(R.id.iv_circle_tgl_awal_ua)
    ImageView iv_circle_tgl_awal_ua;
    @BindView(R.id.iv_circle_tgl_akhir_ua)
    ImageView iv_circle_tgl_akhir_ua;
    @BindView(R.id.iv_circle_stable_link_save_ua)
    ImageView iv_circle_stable_link_save_ua;
    @BindView(R.id.iv_circle_smile_medical_ua)
    ImageView iv_circle_smile_medical_ua;
    @BindView(R.id.iv_circle_kurs_premi_standart_pokok_ua)
    ImageView iv_circle_kurs_premi_standart_pokok_ua;
    @BindView(R.id.iv_circle_kurs_total_premi_ua)
    ImageView iv_circle_kurs_total_premi_ua;
    @BindView(R.id.iv_circle_kurs_uang_pertanggungan_ua)
    ImageView iv_circle_kurs_uang_pertanggungan_ua;
    @BindView(R.id.iv_circle_cutipremi_ua)
    ImageView iv_circle_cutipremi_ua;

    @BindView(R.id.et_bayar_premi_ua)
    EditText et_bayar_premi_ua;
    @BindView(R.id.et_total_premi_ua)
    EditText et_total_premi_ua;
    @BindView(R.id.et_uang_pertanggungan_ua)
    EditText et_uang_pertanggungan_ua;
    @BindView(R.id.et_tgl_awal_ua)
    EditText et_tgl_awal_ua;
    @BindView(R.id.et_tgl_akhir_ua)
    EditText et_tgl_akhir_ua;
    @BindView(R.id.et_premi_standart_pokok_ua)
    EditText et_premi_standart_pokok_ua;
    @BindView(R.id.et_masa_pembayaran_stable_link_save_ua)
    EditText et_masa_pembayaran_stable_link_save_ua;
    @BindView(R.id.et_cutipremi_ua)
    EditText et_cutipremi_ua;
    @BindView(R.id.et_masa_tanggung_ua)
    EditText et_masa_tanggung_ua;

    @BindView(R.id.ac_channel_distribusi_ua)
    AutoCompleteTextView ac_channel_distribusi_ua;
    @BindView(R.id.ac_produk_ua)
    AutoCompleteTextView ac_produk_ua;
    @BindView(R.id.ac_nm_produk_ua)
    AutoCompleteTextView ac_nm_produk_ua;
    @BindView(R.id.ac_carabayar_ua)
    AutoCompleteTextView ac_carabayar_ua;
    @BindView(R.id.ac_opsi_premi_ua)
    AutoCompleteTextView ac_opsi_premi_ua;
    @BindView(R.id.ac_kombinasi_ua)
    AutoCompleteTextView ac_kombinasi_ua;
    @BindView(R.id.ac_bntukbayar_ua)
    AutoCompleteTextView ac_bntukbayar_ua;
    @BindView(R.id.ac_kurs_premi_standart_pokok_ua)
    AutoCompleteTextView ac_kurs_premi_standart_pokok_ua;
    @BindView(R.id.ac_kurs_total_premi_ua)
    AutoCompleteTextView ac_kurs_total_premi_ua;
    @BindView(R.id.ac_kurs_uang_pertanggungan_ua)
    AutoCompleteTextView ac_kurs_uang_pertanggungan_ua;
    @BindView(R.id.ac_paket_ua)
    AutoCompleteTextView ac_paket_ua;


    @BindView(R.id.img_tambah_rider_ua)
    ImageButton img_tambah_rider_ua;

    @BindView(R.id.ll_kategori_stable_link_save_ua)
    LinearLayout ll_kategori_stable_link_save_ua;

    @BindView(R.id.rg_stable_link_save_ua)
    RadioGroup rg_stable_link_save_ua;

    @BindView(R.id.rb_biasa_stable_link_save_ua)
    RadioButton rb_biasa_stable_link_save_ua;
    @BindView(R.id.rb_manfaatbulanan_stable_link_save_ua)
    RadioButton rb_manfaatbulanan_stable_link_save_ua;

    @BindView(R.id.cb_special_case_bp_rate_ua)
    CheckBox cb_special_case_bp_rate_ua;
    @BindView(R.id.cb_karyawan_smile_ua)
    CheckBox cb_karyawan_smile_ua;
    @BindView(R.id.cb_plan_provider_ua)
    CheckBox cb_plan_provider_ua;

    @BindView(R.id.scroll_ua)
    NestedScrollView scroll_ua;

//    @BindView(R.id.ll_tambah_rider_ua)
//    LinearLayout ll_tambah_rider_ua;

    @BindView(R.id.lv_tambah_rider_ua)
    RecyclerView lv_tambah_rider_ua;

    private Button btn_lanjut, btn_kembali;
    private ImageView iv_save, iv_next, iv_prev;
    private Toolbar second_toolbar;

    View view;
    private EspajModel me;
    private Calendar cal_awal;

    private String KODE_REG1 = "";
    private String KODE_REG2 = "";
    private String KODE_REG3 = "";

    private String premi_standart = "";
    private String total_premi = "";
    private String kombinasi = "";
    private String up = "";

    private int kd_jns_produk = 0;
    private int produk = 0;
    private int nm_produk = 0;
    private int paket = 0;
    private int carabayar;
    private int bntukbayar;
    private int int_kusus;
    private int opsi_premi = 0;
    private int groupId = 0;
    private int JENIS_LOGIN = 0;
    private int JENIS_LOGIN_BC = 0;
    private int count_rider = 0;
    private int lsbs_id = 0;

    private int klas_ua = 1;
    private int ekasehat_plan = 1;

    private ArrayList<ModelDropdownInt> ListDropDownInt;
    private ArrayList<ModelDropDownString> ListDropDownString;
    private ArrayList<DropboxModel> ListDropInt;
    private ArrayList<ModelAddRiderUA> ListRider;
    private ArrayList<ModelAskesUA> ListAskes;

    private ListRiderAdapter listRiderAdapter;

    //Khusus Rider, ke depannya cari yang lebih simpel oke
    private ProgressDialog progressDialog;

}