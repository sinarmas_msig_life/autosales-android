
package id.co.ajsmsig.nb.crm.model.apiresponse.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DownloadLeadResponse {

    @SerializedName("SL_ID")
    @Expose
    private String sLID;
    @SerializedName("SL_TAB_ID")
    @Expose
    private String SL_TAB_ID;
    @SerializedName("SL_NAME")
    @Expose
    private String sLNAME;
    @SerializedName("SL_APP")
    @Expose
    private String sLAPP;
    @SerializedName("SL_ACTIVE")
    @Expose
    private String sLACTIVE;
    @SerializedName("SL_UMUR")
    @Expose
    private String sLUMUR;
    @SerializedName("SL_GENDER")
    @Expose
    private String sLGENDER;
    @SerializedName("SL_LAST_POS")
    @Expose
    private String sLLASTPOS;
    @SerializedName("SL_UPTD_DATE")
    @Expose
    private String sLUPTDDATE;
    @SerializedName("SL_SRC")
    @Expose
    private String sLSRC;
    @SerializedName("SL_TYPE")
    @Expose
    private String sLTYPE;
    @SerializedName("SL_BAC_REFF")
    @Expose
    private String sLBACREFF;
    @SerializedName("SL_BAC_REFF_NAME")
    @Expose
    private String sLBACREFFNAME;
    @SerializedName("SL_BAC_FIRST_BRANCH")
    @Expose
    private String sLBACFIRSTBRANCH;
    @SerializedName("SL_BAC_CURR_BRANCH")
    @Expose
    private String sLBACCURRBRANCH;
    @SerializedName("NAMA_CABANG")
    @Expose
    private String nAMACABANG;
    @SerializedName("UTL_USER_ID")
    @Expose
    private String UTL_USER_ID;
    @SerializedName("SL_CREATED")
    @Expose
    private String sLCREATED;
    @SerializedName("SL_UPDATED")
    @Expose
    private String sLUPDATED;
    @SerializedName("RNUM")
    @Expose
    private String rNUM;
    @SerializedName("home")
    @Expose
    private Home home;
    @SerializedName("office")
    @Expose
    private Object office;
    @SerializedName("other")
    @Expose
    private Object other;

    public String getSLID() {
        return sLID;
    }

    public void setSLID(String sLID) {
        this.sLID = sLID;
    }

    public String getSLNAME() {
        return sLNAME;
    }

    public void setSLNAME(String sLNAME) {
        this.sLNAME = sLNAME;
    }

    public String getSLAPP() {
        return sLAPP;
    }

    public void setSLAPP(String sLAPP) {
        this.sLAPP = sLAPP;
    }

    public String getSLACTIVE() {
        return sLACTIVE;
    }

    public void setSLACTIVE(String sLACTIVE) {
        this.sLACTIVE = sLACTIVE;
    }

    public String getSLUMUR() {
        return sLUMUR;
    }

    public void setSLUMUR(String sLUMUR) {
        this.sLUMUR = sLUMUR;
    }

    public String getSLGENDER() {
        return sLGENDER;
    }

    public void setSLGENDER(String sLGENDER) {
        this.sLGENDER = sLGENDER;
    }

    public String getSLLASTPOS() {
        return sLLASTPOS;
    }

    public void setSLLASTPOS(String sLLASTPOS) {
        this.sLLASTPOS = sLLASTPOS;
    }

    public String getSLUPTDDATE() {
        return sLUPTDDATE;
    }

    public void setSLUPTDDATE(String sLUPTDDATE) {
        this.sLUPTDDATE = sLUPTDDATE;
    }

    public String getSLSRC() {
        return sLSRC;
    }

    public void setSLSRC(String sLSRC) {
        this.sLSRC = sLSRC;
    }

    public String getSLTYPE() {
        return sLTYPE;
    }

    public void setSLTYPE(String sLTYPE) {
        this.sLTYPE = sLTYPE;
    }

    public String getSLBACREFF() {
        return sLBACREFF;
    }

    public void setSLBACREFF(String sLBACREFF) {
        this.sLBACREFF = sLBACREFF;
    }

    public String getSLBACREFFNAME() {
        return sLBACREFFNAME;
    }

    public void setSLBACREFFNAME(String sLBACREFFNAME) {
        this.sLBACREFFNAME = sLBACREFFNAME;
    }

    public String getSLBACFIRSTBRANCH() {
        return sLBACFIRSTBRANCH;
    }

    public void setSLBACFIRSTBRANCH(String sLBACFIRSTBRANCH) {
        this.sLBACFIRSTBRANCH = sLBACFIRSTBRANCH;
    }

    public String getSLBACCURRBRANCH() {
        return sLBACCURRBRANCH;
    }

    public void setSLBACCURRBRANCH(String sLBACCURRBRANCH) {
        this.sLBACCURRBRANCH = sLBACCURRBRANCH;
    }

    public String getNAMACABANG() {
        return nAMACABANG;
    }

    public void setNAMACABANG(String nAMACABANG) {
        this.nAMACABANG = nAMACABANG;
    }

    public String getSLCREATED() {
        return sLCREATED;
    }

    public void setSLCREATED(String sLCREATED) {
        this.sLCREATED = sLCREATED;
    }

    public String getSLUPDATED() {
        return sLUPDATED;
    }

    public void setSLUPDATED(String sLUPDATED) {
        this.sLUPDATED = sLUPDATED;
    }

    public String getRNUM() {
        return rNUM;
    }

    public void setRNUM(String rNUM) {
        this.rNUM = rNUM;
    }

    public Home getHome() {
        return home;
    }

    public void setHome(Home home) {
        this.home = home;
    }

    public Object getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public Object getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getSL_TAB_ID() {
        return SL_TAB_ID;
    }

    public String getUTL_USER_ID() {
        return UTL_USER_ID;
    }

}



