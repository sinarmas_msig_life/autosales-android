package id.co.ajsmsig.nb.prop.method.calculateIlustration;

import android.content.Context;
import android.content.SharedPreferences;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.method.util.ArrUtil;
import id.co.ajsmsig.nb.prop.method.util.CommonUtil;
import id.co.ajsmsig.nb.prop.method.util.FormatNumber;
import id.co.ajsmsig.nb.prop.method.util.ProposalStringFormatter;
import id.co.ajsmsig.nb.prop.model.hardCode.Proposal;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.IllustrationResultVO;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.S_biaya;


/**
 * New Illustration Formula Class that only need no_proposal parameter
 *
 * @author Daru
 * @since Jun 3, 2016
 * <p>
 * edit a little bit for mobile version by faiz
 */

//ini awalnya
class IllustrationFormula {

    protected static final String TAG = "IllustrationFormula";
    private P_Select select;
    private Context context;

    IllustrationFormula(Context context) {
        this.context = context;
        select = new P_Select(context);
    }

//    DBDAOIlustrasi getDbdaoIlustrasi() {
//        return dbdaoIlustrasi;
//    }

    public P_Select getSelect() {
        return select;
    }

    public void setSelect(P_Select select) {
        this.select = select;
    }

    protected Context getContext() {
        return context;
    }

    public HashMap<String, Object> getIllustrationResult( String no_proposal) {
        IllustrationResultVO result = new IllustrationResultVO();
        IllustrationResultVO result2 = new IllustrationResultVO();
        ArrayList<LinkedHashMap<String, String>> mapList = new ArrayList<>();
        ArrayList<HashMap<String, String>> mapList2 = new ArrayList<HashMap<String, String>>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        ArrayList<HashMap<String, Object>> topupDrawList = getSelect().selectTopupAndWithdrawList(no_proposal);
        Integer defaultTopupDrawListSize = 50;// Di eproposal topup list selalu 50 walaupun tidak diisi
        String premiumTotal;
        String topup;
        String draw;

        int li_bagi = 1000;
        double[] ldec_bak;
        double ldec_bak_tu = 0.05;
        double ldec_bak_tut = 0.05;
        double ldec_akuisisi;
        double ldec_premi_invest;
        double[][] ldec_hasil_invest = new double[6][4];
        double[] ldec_tarik = { Proposal.DUMMY_ZERO, 0, 0, 0, 0, 0 };
        double ldec_wdraw, li_persen_wdraw;
        double[] ldec_premi_bulan = new double[13];
        double ldec_topup;
        double[][] ldec_bunga = new double[6][4];
        double[] ldec_bunga_avg = new double[4];
        double ldec_coi = 0, ldec_mfc, ldec_sc, ldec_cost;
        boolean[] lb_minus = { Proposal.DUMMY_FALSE, false, false, false };
        S_biaya lstr;
        double ldec_manfaat, ldec_premi_setahun = 0;
        ldec_mfc = 15000;//TODO get ldec_mfc(monthly fix cost) berdasarkan produknya
        Boolean lb_hal_tarik = true;
        ldec_coi = 0;//ulinkBusiness.of_get_coi_120_in_header( 1, 1 );

        Integer umur_tt = 0, bisnis_id = 0, bisnis_no = 0, ins_per = 0, lscb_kali=0, lscb_id = 0, lamaBayar = 0;
        double up = 0;
        String kondisi_layak_jual = null, lscb_pay_mode = null, lku_id = "00", userMsagId = "";
        HashMap<String, Object> dataProposal = getSelect().selectDataProposal(no_proposal);
        if(!CommonUtil.isEmpty(dataProposal)) {
            bisnis_id = ((BigDecimal) dataProposal.get("LSBS_ID")).intValue();
            bisnis_no = ((BigDecimal) dataProposal.get("LSDBS_NUMBER")).intValue();
            umur_tt =  Integer.valueOf(dataProposal.get("UMUR_TT").toString());
            up =  Double.valueOf(dataProposal.get("UP").toString());
            ins_per =  Integer.valueOf(dataProposal.get("THN_MASA_KONTRAK").toString());
            kondisi_layak_jual = (String) dataProposal.get("NAME_ELIGIBLE");
            lscb_kali = Integer.valueOf(dataProposal.get("LSCB_KALI").toString());
            lscb_pay_mode = dataProposal.get("LSCB_PAY_MODE").toString();
            lscb_id = Integer.valueOf(dataProposal.get("LSCB_ID").toString());
            lku_id = dataProposal.get("LKU_ID").toString();
            lamaBayar = Integer.valueOf(dataProposal.get("THN_LAMA_BAYAR").toString());
            userMsagId = dataProposal.get("MSAG_ID").toString();
        }

        Integer pset_id = getSelect().selectPsetIdProposal(no_proposal);

        if(Proposal.CUR_USD_CD.equals(lku_id)) {
            ldec_mfc = 2;
            if(bisnis_id == 120 || bisnis_id == 127 || bisnis_id == 128 || bisnis_id == 129 || bisnis_id == 202 ){
                if(bisnis_no >= 4 && bisnis_no <= 6) ldec_mfc = 2.5;
                if (bisnis_no >= 7 && bisnis_no <= 24)  ldec_mfc = 2;
            }
            li_bagi = 1;
        } else {
//          ld_max = 1000000000; unused
        }

        ldec_premi_setahun = getSelect().selectPremiSetahun(no_proposal);

        // ??? - what is this for?
        for( int i = 1; i <= 3; i++ )
        {
            ldec_hasil_invest[ 1 ][ i ] = 0;
        }

        ldec_manfaat = up;
        lstr = getBiaya(bisnis_id, bisnis_no, no_proposal);
        ldec_bak = lstr.bak;
        ldec_bunga = lstr.bunga;

        ldec_bunga_avg = getSelect().selectBungaAvg( no_proposal );

        // ???
        double[] np = new double[4];
        double[] np_nonUP = new double [4];
        double[] celaka = new double[4];
        double[] celaka_nonUP = new double[4];

        ldec_premi_invest = 0;
        int j;
        // This is the main calculation
        for(int i = 1; i <= ins_per; i++) {
            ldec_sc = 0;
            ldec_wdraw = 0;
            ldec_topup = 0;
            ldec_akuisisi = 0;
            if(i <= ArrUtil.upperBound(lstr.tarik))ldec_wdraw = lstr.tarik[i];
            if(i <= ArrUtil.upperBound(lstr.topup))ldec_topup = lstr.topup[i];
            if(ArrUtil.upperBound(ldec_bak) > i )ldec_akuisisi = ldec_bak[i];

            int li_usia = umur_tt + i - 1;
            Map<String, Object> par = new HashMap<String, Object>();
            par.put( "liUsia", li_usia );
            par.put( "no_proposal" , no_proposal);
            ldec_coi = getLdec_coi(dataProposal, i);

            ldec_cost = (ldec_coi + ldec_mfc);
            li_persen_wdraw = ldec_wdraw;
            j = umur_tt + i;
            for(int k = 1; k <= 3; k++) {
                double ldec_premi_invest_temp = 0;
                ldec_premi_invest = 0;
                HashMap<String, Object> param = new HashMap<String, Object>();
                param.put( "ldec_akuisisi", ldec_akuisisi );
                param.put( "no_proposal" , no_proposal);
                param.put("thn_ke", i);
                ldec_premi_invest_temp = getSelect().selectPremiInvest(param);

                for(int li_bulan = 1; li_bulan <= 12; li_bulan++) {
                    ldec_premi_invest = 0;

                    if(i <= lamaBayar ) {
                        if( Proposal.PAY_MODE_CD_SEMESTERAN == lscb_id ) {
                            if(li_bulan == 1  ||  li_bulan == 7  )ldec_premi_invest = ldec_premi_invest_temp;
                        } else if( Proposal.PAY_MODE_CD_TRIWULANAN == lscb_id ) {
                            if( li_bulan == 1  || li_bulan == 4 || li_bulan == 7 || li_bulan == 10 )ldec_premi_invest = ldec_premi_invest_temp;
                        } else if( Proposal.PAY_MODE_CD_TAHUNAN == lscb_id ) {
                            if(li_bulan == 1 ) ldec_premi_invest = ldec_premi_invest_temp;
                        } else if( Proposal.PAY_MODE_CD_SEKALIGUS == lscb_id ) {
                            if(li_bulan == 1) ldec_premi_invest = ldec_premi_invest_temp;
                        }
                        else ldec_premi_invest = ldec_premi_invest_temp;
                    }

                    ldec_hasil_invest[1][k] = FormatNumber.round(( ldec_premi_invest + ldec_hasil_invest[1][k] - ldec_cost) * FormatNumber.round(Math.pow( ( 1 + ldec_bunga_avg[ k ] ), ( ( double ) 1 / 12 )), 15),2);
                }

                ldec_tarik[k] = 0;
                if(lb_hal_tarik){// TODO: this should be from db
                    if(bisnis_id == 127 || bisnis_id == 141){
                        //ldec_tarik[k] = Round(ldec_hasil_invest[1][k] * li_persen_wdraw/100, 2);
                        ldec_tarik[k] = FormatNumber.round(ldec_hasil_invest[1][k] * li_persen_wdraw/100, 2);
                        //ldec_wdraw = Max(0, ldec_tarik[k]);
                        ldec_wdraw = Math.max(0, ldec_tarik[k]);
                    }else if( bisnis_id == 128){
                        //proteksi
                        //th 20: 20%, tiap 5th: 5%, usia 70: 100%
                        if(j < 70){
                            if(i == 20){
                                //ldec_tarik[k] = Round(ldec_hasil_invest[1][k] * 0.2, 2);
                                ldec_tarik[k] = FormatNumber.round(ldec_hasil_invest[1][k] * 0.2, 2);
                            }else if( i > 20 && (i%5) == 0){
                                //ldec_tarik[k] = Round(ldec_hasil_invest[1][k] * 0.05, 2);
                                ldec_tarik[k] = FormatNumber.round(ldec_hasil_invest[1][k] * 0.05, 2);
                            }
                        }else if( j == 70){
                            //ldec_tarik[k] = ldec_hasil_invest[1, k]
                        }
                        //ldec_wdraw = Max(0, ldec_tarik[k]);
                        ldec_wdraw = Math.max(0, ldec_tarik[k]);
                    }else if( bisnis_id == 129){
                        //sejahtera
                        //usia 55: 50%, usia 65: 100%
                        if( j == 55){
                            //ldec_tarik[k] = Round(ldec_hasil_invest[1][k] * 0.5, 2);
                            ldec_tarik[k] = FormatNumber.round(ldec_hasil_invest[1][k] * 0.5, 2);
                        }else if( j == 65){
                            //ldec_tarik[k] = ldec_hasil_invest[1, k]
                        }
                        //ldec_wdraw = Max(0, ldec_tarik[k]);
                        ldec_wdraw = Math.max(0, ldec_tarik[k]);
                    }
                }

                ldec_wdraw = FormatNumber.round(ldec_wdraw,2);
                BigDecimal ldec_wdrawbd = new BigDecimal(ldec_wdraw);

                ldec_hasil_invest[1][k] = FormatNumber.round(ldec_hasil_invest[1][k] - (new Double(ldec_wdrawbd.toString()) * (1 + ldec_sc)), 2);

                //TODO: Ini untuk layak / tdk layak jual blkgan aj
                if(kondisi_layak_jual != null){
                    if(kondisi_layak_jual.equals("ELIGIBLE_COND_1")){
                        SharedPreferences sharedpreferences = context.getSharedPreferences(context.getString(R.string.app_preferences), Context.MODE_PRIVATE);
                        String userGroupId = sharedpreferences.getString("GROUP_ID", "");
                        if( Proposal.GROUP_DMTM.equals(userGroupId) ){
                            if( ldec_hasil_invest[ 1 ][ k ] <= 0 &&  ( i <= 10 )  )
                            {
                                lb_minus[ k ] = true;
                            }
                        }else{
                            if( i == 1 ){
                                if( ldec_hasil_invest[ 1 ][ k ] < 0 ){
                                    lb_minus[ k ] = true;
                                }
                            }else{
                                if(k>2){
                                    if( ldec_hasil_invest[ 1 ][ k ] < 0 ){
                                        lb_minus[ 1 ] = true;
                                    }
                                }
                            }
                        }
                    } else if(kondisi_layak_jual.equals("ELIGIBLE_COND_2")){
                        if( i == 1 ){
                            if( ldec_hasil_invest[ 1 ][ k ] < 0 ){
                                lb_minus[ k ] = true;
                            }
                        }else{
                            if(k>=2){
                                if( ldec_hasil_invest[ 1 ][ k ] < 0 ){
                                    lb_minus[ 1 ] = true;
                                }
                            }
                        }
                    } else if(kondisi_layak_jual.equals("ELIGIBLE_COND_3")){
                        if( ldec_hasil_invest[ 1 ][ k ] <= 0 && ( ( umur_tt < 50 && i <= 15 ) || ( umur_tt >= 50 && i <= 10 ) ) )
                        {
                            lb_minus[ k ] = true;
                        }
                    }
                }
            }

            HashMap<String, Object> param = new HashMap<String, Object>();
            param.put( "jenis", 0 );
            param.put("pset_id", pset_id);

            ArrayList<HashMap<String, Object>> resultList;
            resultList = getSelect().selectIllustrationShow( param );
            int year_i = Integer.valueOf(resultList.get(0).get("YEAR").toString());
            param.put( "jenis", 1 );
            resultList = getSelect().selectIllustrationShow( param );

            int[] resultArr = new int[resultList.size()];

            for(int l = 0; l < resultList.size(); l++) {
                resultArr[l] = Integer.valueOf(resultList.get(l).get("YEAR").toString());
            }

            boolean year_j = false;
            for(int l = 0; l < resultArr.length; l++) {
                if(resultArr[l] == j){
                    year_j = true;
                    break;
                }
            }

            if( i <= year_i || year_j ) {
                for( int k = 1; k <= 3; k++ )
                {
                    if(j == 100){
                        if( lscb_id == 0 ){
                            np[ k ] = FormatNumber.round( ( ldec_hasil_invest[ 1 ][ k ] ) / li_bagi, 0 );
                            np_nonUP[ k ] = FormatNumber.round( ( ldec_hasil_invest[ 1 ][ k ] ) / li_bagi, 0 );
                        }else{
                            np[ k ] = FormatNumber.round( ( ldec_hasil_invest[ 1 ][ k ] + ldec_manfaat ) / li_bagi, 0 );
                            np_nonUP[ k ] = FormatNumber.round( ( ldec_hasil_invest[ 1 ][ k ] ) / li_bagi, 0 );
                        }

                    }else {
                        np[ k ] = FormatNumber.round( ldec_hasil_invest[ 1 ][ k ] / li_bagi, 0 );
                        np_nonUP[ k ] = FormatNumber.round( ( ldec_hasil_invest[ 1 ][ k ] ) / li_bagi, 0 );
                    }
                    celaka[ k ] = FormatNumber.round( ( ldec_hasil_invest[ 1 ][ k ] + ldec_manfaat ) / li_bagi, 0 );
                    celaka_nonUP[ k ] = FormatNumber.round( ( ldec_hasil_invest[ 1 ][ k ] ) / li_bagi, 0 );
                }

                if( i <= lamaBayar )
                {
                    premiumTotal = FormatNumber.round( ( ldec_premi_setahun / li_bagi ), 0) + "";
                }
                else
                {
                    premiumTotal = "";
                }

                if( i < defaultTopupDrawListSize )
                {
                    topup = "0";
                    draw = "0";

                    // why ( i - 1 )? becoz index in Java start from 0, not like PB programming language
//                    if(topupDrawList.get(i - 1) != null) {
//                        HashMap<String, Object> topupDraw = topupDrawList.get(i - 1);
//                        BigDecimal topupAmount = (BigDecimal) topupDraw.get("TOPUP");
//                        BigDecimal drawAmount = (BigDecimal) topupDraw.get("TARIK");
//
//                        topup = ProposalStringFormatter.convertToString(topupAmount.divide(new BigDecimal("1000")));
//                        draw = ProposalStringFormatter.convertToString(drawAmount.divide(new BigDecimal("1000")));
//                    }
                    for(HashMap<String, Object> topupDraw : topupDrawList) {
                        Integer thn_ke = ((BigDecimal) topupDraw.get("THN_KE")).intValue();
                        if(i == thn_ke) {
                            BigDecimal topupAmount = (BigDecimal) topupDraw.get("TOPUP");
                            BigDecimal drawAmount = (BigDecimal) topupDraw.get("TARIK");

                            topup = ProposalStringFormatter.convertToString(topupAmount.divide(new BigDecimal("1000")));
                            draw = ProposalStringFormatter.convertToString(drawAmount.divide(new BigDecimal("1000")));
                        }
                    }

                    if( "0".equals( topup ) ) topup = "0.00";
                    if( "0".equals( draw ) ) draw = "0.00";
                }
                else
                {
                    topup = "0.00";
                    draw = "0.00";
                }

                String valueLow = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np_nonUP[ 1 ] ) );
                String valueMid = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np_nonUP[ 2 ] ) );
                String valueHi = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np_nonUP[ 3 ] ) );
                if(!"nil".equals(valueLow)){
                    valueLow = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 1 ] ) );
                }
                if(!"nil".equals(valueMid)){
                    valueMid = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 2 ] ) );
                }
                if(!"nil".equals(valueHi)){
                    valueHi = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 3 ] ) );
                }

                String benefitLow = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka_nonUP[ 1 ], np_nonUP[ 1 ] );
                String benefitMid = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka_nonUP[ 2 ], np_nonUP[ 2 ] );
                String benefitHi = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka_nonUP[ 3 ], np_nonUP[ 3 ] );
                if(!"nil".equals(benefitLow)){
                    benefitLow = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 1 ], np[ 1 ] );
                }
                if(!"nil".equals(benefitMid)){
                    benefitMid = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 2 ], np[ 2 ] );
                }
                if(!"nil".equals(benefitHi)){
                    benefitHi = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 3 ], np[ 3 ] );
                }


                if("nil".equals(valueLow)){
                    valueLow = "**";
                }
                if("nil".equals(valueMid)){
                    valueMid = "**";
                }
                if("nil".equals(valueHi)){
                    valueHi = "**";
                }
                if("nil".equals(benefitLow)){
                    benefitLow = "**";
                }
                if("nil".equals(benefitMid)){
                    benefitMid = "**";
                }
                if("nil".equals(benefitHi)){
                    benefitHi = "**";
                }

                LinkedHashMap<String, String> map1 = new LinkedHashMap<>();
                    map1.put( "yearNo", ProposalStringFormatter.convertToString( i ) );
                    map1.put( "insuredAge", ProposalStringFormatter.convertToString( umur_tt + i ) );
                    map1.put( "premiumTotal", ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( premiumTotal ) );
                    map1.put( "topupAssumption", topup );
                    map1.put( "drawAssumption", draw );
                    map1.put( "valueLow", valueLow );
                    map1.put( "valueMid", valueMid );
                    map1.put( "valueHi", valueHi );
                    map1.put( "benefitLow", benefitLow );
                    map1.put( "benefitMid", benefitMid );
                    map1.put( "benefitHi", benefitHi );

                mapList.add( map1 );

            }

            if ((i <= 25 || (j % 5) == 0)){
                HashMap<String, String> map2 = new HashMap<>();
                String drawAssumptionLow = ProposalStringFormatter.convertToRoundedNoDigit( new BigDecimal( ldec_tarik[ 1 ] / li_bagi ) );
                String drawAssumptionMid = ProposalStringFormatter.convertToRoundedNoDigit( new BigDecimal( ldec_tarik[ 2 ] / li_bagi ) );
                String drawAssumptionHi = ProposalStringFormatter.convertToRoundedNoDigit( new BigDecimal( ldec_tarik[ 3 ] / li_bagi ) );

                map2.put( "yearNo", ProposalStringFormatter.convertToString( i ) );
                map2.put( "insuredAge", ProposalStringFormatter.convertToString( umur_tt + i ) );
                map2.put( "drawAssumptionLow", drawAssumptionLow );
                map2.put( "drawAssumptionMid", drawAssumptionMid );
                map2.put( "drawAssumptionHi", drawAssumptionHi );
                mapList2.add( map2 );
            }
        }

        result.setValidityMsg( lb_minus[ 1 ] ? "Maaf, Proposal yang Anda buat Tidak Layak Jual." : "" );
        result.setIllustrationList( mapList );
        map.put("Illustration1", result);

        return map;
    }

    protected S_biaya getBiaya(int product_id, int product_no, String no_proposal) {
        S_biaya lstr = new S_biaya();

        // lsbs_id 120
//      lstr.bak = new double[] {0.0, 0.8, 0.15, 0.0, 0.0, 0.0, 0.0};

        Integer pset_id = getSelect().selectPsetIdProposal(no_proposal);
        // Adrian => Declare di eproposalManager to mark off
        lstr.bak = getSelect().selectBiayaAkuisisi(pset_id);

        /*
        // Ini hampir semua produk sama kecuali bbrp produk
        //Nilai Aasumsi hasil Investasi
        //fix
        lstr.bunga[1][1] = 0.05; //0.06 10 may 2013 .audit
        lstr.bunga[1][2] = 0.08;
        lstr.bunga[1][3] = 0.10;
        //dyn
        lstr.bunga[2][1] = 0.06;
        //If Pos('160', string(istr_prop.bisnis_id, '000#')) > 0 Then lstr.bunga[2, 1] = 0.13
        lstr.bunga[2][2] = 0.10; //0.15 10 may 2013 .audit //0.18
        lstr.bunga[2][3] = 0.14; //0.2 10 may 2013 .audit//0.25
        //agr
        lstr.bunga[3][1] = 0.07;
        //If Pos('160', string(istr_prop.bisnis_id, '000#')) > 0 Then lstr.bunga[3, 1] = 0.13
        lstr.bunga[3][2] = 0.12; //0.2 10 may 2013 .audit //0.22
        lstr.bunga[3][3] = 0.18; //0.3 10 may 2013 .audit//0.32
        //sec$
        lstr.bunga[4][1] = 0.03;
        lstr.bunga[4][2] = 0.05;
        lstr.bunga[4][3] = 0.07;
        //dyn$
        lstr.bunga[5][1] = 0.05;
        lstr.bunga[5][2] = 0.08;
        lstr.bunga[5][3] = 0.10;
        */
        lstr.bunga = getSelect().selectInvestRate();

        //Set topup & tarik berdasarkan pilihan user
        ArrayList<HashMap<String, Object>> listTopup = getSelect().selectTopupAndWithdrawList(no_proposal);
        if(listTopup.size() > 0) {
            HashMap<String, Object> topup0 = new HashMap<String, Object>();
            topup0.put("TOPUP", 0.0);
            topup0.put("TARIK", 0.0);
            topup0.put("THN_KE", 0);
            listTopup.add(0, topup0);
        }

        for(int i = 0; i < listTopup.size(); i++) {
            Integer thn_ke = Integer.valueOf(listTopup.get(i).get("THN_KE").toString());
            lstr.topup[thn_ke] = Double.valueOf(listTopup.get(i).get("TOPUP").toString());
            lstr.tarik[thn_ke] = Double.valueOf(listTopup.get(i).get("TARIK").toString());
        }

        return lstr;
    }

    protected double getLdec_coi(HashMap<String, Object> dataProposal, int thn_ke) {
        double ldec_total = 0;
        int li_usia = ((BigDecimal) dataProposal.get("UMUR_TT")).intValue() + thn_ke - 1;
        int li_usia_pp = ((BigDecimal) dataProposal.get("UMUR_PP")).intValue() + thn_ke - 1;
        String no_proposal = (String) dataProposal.get("NO_PROPOSAL_TAB");

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put( "liUsia", li_usia );
        params.put( "li_usia_pp", li_usia_pp );
        params.put( "no_proposal" , no_proposal );
        params.put( "thn_ke", thn_ke );

        ldec_total = getSelect().selectLdecCoi( params );
        ldec_total = FormatNumber.round((((Double.valueOf(dataProposal.get("UP").toString())/1000)* ldec_total)* 0.1), 2, dataProposal.get("LKU_ID").toString());
        double ldec_factor = getSelect().selectProductFactor(no_proposal);
        ArrayList<HashMap<String, Object>> listRider = getSelect().selectListRiderData(params);

        for (HashMap<String, Object> rider : listRider) {
            int batasUsia = ((BigDecimal) rider.get("INSURED_AGE_TO")).intValue();
            int flagBatasUsia = ((BigDecimal) rider.get("FLAG_AGE_VALIDATION")).intValue();

            if(flagBatasUsia == 0 || (flagBatasUsia == 1 && li_usia <= batasUsia)) {
                double ldec_rate = ((BigDecimal) rider.get("RATE")).doubleValue();
                int rider_id = ((BigDecimal) rider.get("RIDER_ID")).intValue();
                int rider_number = ((BigDecimal) rider.get("RIDER_NUMBER")).intValue();
                int lscb_id = (int) rider.get("LSCB_ID");
                int pset_id = getSelect().selectPsetIdProductSetup(rider_id, rider_number);

                String formulaQuery = (String) rider.get("F_COI_RIDER");
                formulaQuery = formulaQuery.replaceAll("\\$P\\{NO_PROPOSAL\\}", "'" + no_proposal + "'");
                formulaQuery = formulaQuery.replaceAll("\\$P\\{LDEC_RATE\\}", String.valueOf(ldec_rate));
                formulaQuery = formulaQuery.replaceAll("\\$P\\{LI_USIA\\}", String.valueOf(li_usia));
                formulaQuery = formulaQuery.replaceAll("\\$P\\{RIDER_ID\\}", String.valueOf(rider_id));
                formulaQuery = formulaQuery.replaceAll("\\$P\\{RIDER_NUMBER\\}", String.valueOf(rider_number));
                formulaQuery = formulaQuery.replaceAll("\\$P\\{LDEC_FACTOR\\}", String.valueOf(ldec_factor));
                formulaQuery = formulaQuery.replaceAll("\\$P\\{PSET_ID\\}", String.valueOf(pset_id));
                formulaQuery = formulaQuery.replaceAll("\\$P\\{LSCB_ID\\}", String.valueOf(lscb_id));

                ldec_total += getSelect().selectFormulaResult(formulaQuery);

                // Peserta tambahan
                params.put("lku_id", rider.get("LKU_ID"));
                params.put("rider_id", rider_id);
                params.put("rider_number", rider_number);

                ArrayList<HashMap<String, Object>> listRiderTambahan = getSelect().selectListRiderDataPesertaTambahan(params);
                if(listRiderTambahan != null && listRiderTambahan.size() > 0) {
                    for (HashMap<String, Object> riderTamb : listRiderTambahan) {
                        ldec_rate = ((BigDecimal) riderTamb.get("RATE")).doubleValue();
                        int li_usia_tt = ((BigDecimal) riderTamb.get("USIA")).intValue() + thn_ke - 1;
                        int tt_tambahan_ke = ((BigDecimal) riderTamb.get("PESERTA_KE")).intValue();

                        formulaQuery = (String) rider.get("F_COI_RIDER_TAMB");
                        formulaQuery = formulaQuery.replaceAll("\\$P\\{LI_USIA\\}", String.valueOf(li_usia));
                        formulaQuery = formulaQuery.replaceAll("\\$P\\{LI_USIA_TT\\}", String.valueOf(li_usia_tt));
                        formulaQuery = formulaQuery.replaceAll("\\$P\\{NO_PROPOSAL\\}", "'" + no_proposal + "'");
                        formulaQuery = formulaQuery.replaceAll("\\$P\\{LDEC_RATE\\}", String.valueOf(ldec_rate));
                        formulaQuery = formulaQuery.replaceAll("\\$P\\{PESERTA_KE\\}", String.valueOf(tt_tambahan_ke));

                        ldec_total += getSelect().selectFormulaResult(formulaQuery);
                    }
                }
            }
        }

//        Log.e(TAG, "getLdec_coi: " + ldec_total);
        return ldec_total;
    }

}
