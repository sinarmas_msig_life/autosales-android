//package id.co.ajsmsig.nb.prop.fragment;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.HashMap;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import id.co.ajsmsig.nb.R;
//import id.co.ajsmsig.nb.crm.method.StaticMethods;
//import id.co.ajsmsig.nb.prop.activity.P_IllustrationActivity;
//import id.co.ajsmsig.nb.prop.database.P_Select;
//import id.co.ajsmsig.nb.prop.method.ValueView;
//import id.co.ajsmsig.nb.prop.method.calculateIlustration.CalculateRidersRate;
//import id.co.ajsmsig.nb.prop.model.DropboxModel;
//import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;
//import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
//import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductRiderModel;
//import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;
//
///**
// * Created by faiz_f on 02/06/2017.
// */
//
//public class P_HomeIllustrationFragment extends Fragment {
//    private static final String TAG = P_HomeIllustrationFragment.class.getSimpleName();
//
//    @BindView(R.id.ll_rangkuman)
//    LinearLayout ll_rangkuman;
//    @BindView(R.id.ll_calculation)
//    LinearLayout ll_calculation;
//
//
//    public P_HomeIllustrationFragment() {
//    }
//
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.p_fragment_home_illustration, container, false);
//        ButterKnife.bind(this, view);
//
//        showView();
//        return view;
//    }
//
//
//    public void showView() {
//        /*
//        * variable with q mean value from query
//        * variable with n mean from calculation
//        * */
//        P_ProposalModel proposalModel = P_IllustrationActivity.myBundle.getParcelable(getString(R.string.proposal_model));
//        Log.d(TAG, "initiateView: proposal model is = " + ((proposalModel==null)? "null": "not null"));
//        assert proposalModel != null;
//        P_MstDataProposalModel dataProposalModel = proposalModel.getMst_data_proposal();
//        P_MstProposalProductModel productModel = proposalModel.getMst_proposal_product();
//        ArrayList<P_MstProposalProductRiderModel> riderModels = proposalModel.getMst_proposal_product_rider();
//
//        P_Select select = new P_Select(getContext());
//        CalculateRidersRate calculateRidersRate = new CalculateRidersRate(getContext(), dataProposalModel.getNo_proposal_tab());
//
//        DropboxModel dRencanaSub = select.selectRencanaSub(productModel.getLsbs_id(), productModel.getLsdbs_number());
//        String q_namaPP = dataProposalModel.getNama_pp();
//        int q_umurPP = dataProposalModel.getUmur_pp();
//        String q_namaTT = dataProposalModel.getNama_tt();
//        int q_umurTT = dataProposalModel.getUmur_tt();
//        String q_kurs = productModel.getLku_id();
//        String q_premi = productModel.getPremi();
//        long q_premiPokok = productModel.getPremi_pokok();
//        long q_premiTopup = productModel.getPremi_topup();
//        int q_premiKomb = productModel.getPremi_komb();
//        int q_caraBayar = productModel.getCara_bayar();
//        int q_thnLamaBayar = productModel.getThn_lama_bayar();
//        int q_thnMasaKontrak = productModel.getThn_masa_kontrak();
//        long q_up = productModel.getUp();
//
//
//        String[][] viewInfo = new String[13][];
//
//        DropboxModel dKurs = select.selectKurs(productModel.getLku_id());
//        String kurs = dKurs.getLabel();
//        String persenPremiPokok = "(" + productModel.getPremi_komb() + "%)";
//        String persenPremiTopup = "(" + String.valueOf(100 - q_premiKomb) + "%)";
//
//        String umurPP = q_umurPP + " " + getString(R.string.tahun).toLowerCase();
//        String umurTT = q_umurTT + " " + getString(R.string.tahun).toLowerCase();
//        String masaPertanggungan = q_thnMasaKontrak + " " + getString(R.string.tahun).toLowerCase();
//        String premiPokokTahunan = kurs + StaticMethods.toMoney(q_premiPokok) + " " + persenPremiPokok;
//        String premiTopupTahunan = kurs + StaticMethods.toMoney(q_premiTopup) + " " + persenPremiTopup;
//        String totalPremiTahunan = kurs + StaticMethods.toMoney(q_premi);
//        String masaPembayaranPremi = q_thnLamaBayar + " " + getString(R.string.tahun).toLowerCase();
//        String up = kurs + StaticMethods.toMoney(q_up);
//
//        viewInfo[0] = new String[]{getString(R.string.produk), dRencanaSub.getLabel()};
//        viewInfo[1] = new String[]{getString(R.string.Nama_Pemegang_Polis), q_namaPP};
//        viewInfo[2] = new String[]{getString(R.string.Usia_Pemegang_Polis), umurPP};
//        viewInfo[3] = new String[]{getString(R.string.Nama_Tertanggung), q_namaTT};
//        viewInfo[4] = new String[]{getString(R.string.Usia_Tertanggung), umurTT};
//        viewInfo[5] = new String[]{getString(R.string.Masa_Pertanggungan), masaPertanggungan};
//        viewInfo[6] = new String[]{getString(R.string.Premi_Pokok_Tahunan), premiPokokTahunan};
//        if (q_premiTopup != 0) {
//            viewInfo[7] = new String[]{getString(R.string.Premi_Top_Up_Berkala_Tahunan), premiTopupTahunan};
//            int length = totalPremiTahunan.length();
//            StringBuilder stringBuilder = new StringBuilder();
//            for (int i = 0; i < length; i++) {
//                stringBuilder.append("-");
//            }
//            viewInfo[8] = new String[]{" ", stringBuilder.toString()};
//            viewInfo[9] = new String[]{getString(R.string.Total_Premi_Tahunan), totalPremiTahunan};
//        }
//        viewInfo[10] = new String[]{getString(R.string.Masa_Pembayaran_Premi), masaPembayaranPremi};
//        viewInfo[11] = new String[]{getString(R.string.Uang_Pertanggungan), up};
//        viewInfo[12] = new String[]{getString(R.string.Tipe_Medis_Calon_Tertanggung), "\"NM\", jika belum punya polis di AJS MSIG"};
//
//        new ValueView(getContext()).addToLinearLayout(ll_rangkuman, viewInfo);
//
///*    BAGIAN 2          */
//        int riderSize = riderModels.size();
//        premiPokokTahunan = StaticMethods.toMoney(q_premiPokok);
//        premiTopupTahunan = StaticMethods.toMoney(q_premiTopup);
//        double n_alokasiBiayaAkuisisiPremiPokok = (q_caraBayar == 0) ? (q_premiPokok * 0.05) : (q_premiPokok * 0.8);
//        double n_biayaTopupBerkala = q_premiTopup * 0.05;
//
//        HashMap<String, Object> params = new HashMap<>();
//        params.put("no_proposal", dataProposalModel.getNo_proposal_tab());
//        params.put("liUsia", q_umurTT);
//
//        double n_biayaAsuransiPokokPerbulan = select.selectLdecCoi(params);
//
//        double n_biayaAdministrasiPerBulan = (q_kurs.equals("01")) ? 15000 : 2;
//
//        double n_totalAlokasiInvestasi = Double.valueOf(String.valueOf(BigDecimal.valueOf(q_premi - n_alokasiBiayaAkuisisiPremiPokok - n_biayaTopupBerkala - n_biayaAsuransiPokokPerbulan - n_biayaAdministrasiPerBulan)));
//
//
//        viewInfo = new String[10 + riderSize][];
////        viewInfo = new String[9][];
//
//        viewInfo[1] = new String[]{getString(R.string.Premi_Pokok_Tahunan), kurs + premiPokokTahunan};
//        viewInfo[2] = new String[]{getString(R.string.Alokasi_Biaya_Akuisisi_Premi_Pokok), kurs + StaticMethods.toMoney(n_alokasiBiayaAkuisisiPremiPokok)};
//        viewInfo[3] = new String[]{getString(R.string.Premi_Top_Up_Berkala_Tahunan), kurs + premiTopupTahunan};
//        viewInfo[4] = new String[]{getString(R.string.Biaya_Top_Up_Berkala_Tahunan), kurs + StaticMethods.toMoney(n_biayaTopupBerkala)};
//        viewInfo[5] = new String[]{getString(R.string.Biaya_Asuransi_Pokok_Perbulan), kurs + StaticMethods.toMoney(n_biayaAsuransiPokokPerbulan)};
//        viewInfo[6] = null;
//        double totalRider = 0;
//        if (riderSize > 0) {
//            int currentLoc = 6;
//            viewInfo[currentLoc] = new String[]{getString(R.string.Biaya_Asuransi_Tambahan_Perbulan), ""};
//            ArrayList<HashMap<String, Object>> hashMaps = calculateRidersRate.getRiderPriceTag(1);
//            for (HashMap<String, Object> hashMap : hashMaps) {
//                String nama = (String) hashMap.get("NAMA");
//                double ldecTotal = (Double) hashMap.get("LDECTOTAL");
//                totalRider += ldecTotal;
//                Log.d("TAG", "initiateView: nama = " + nama + " --nilai = "+ ldecTotal);
//                currentLoc++;
//                viewInfo[currentLoc] = new String[]{nama, kurs + StaticMethods.toMoney(ldecTotal)};
//            }
//        }
//        viewInfo[viewInfo.length - 3] = new String[]{getString(R.string.Biaya_Administrasi_Perbulan), kurs + StaticMethods.toMoney(n_biayaAdministrasiPerBulan)};
//        int lengthPremiPokok = premiPokokTahunan.length();
//        StringBuilder stringBuilder = new StringBuilder();
//        for (int i = 0; i < lengthPremiPokok; i++) {
//            stringBuilder.append("-");
//        }
//        viewInfo[viewInfo.length - 2] = new String[]{"", stringBuilder.toString()};
//        viewInfo[viewInfo.length - 1] = new String[]{getString(R.string.Total_Alokasi_Investasi), kurs + StaticMethods.toMoney(n_totalAlokasiInvestasi - totalRider)};
//
//        new ValueView(getContext()).addToLinearLayout(ll_calculation, viewInfo);
//    }
//
//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.menu_va_proposal, menu);
//    }
//}
//
