package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDataDitunjukDI;

/**
 * Created by rizky_c on 30/09/2017.
 */

public class TableDataDitunjukDI {

    private Context context;

    public TableDataDitunjukDI(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(ModelDataDitunjukDI me, int counter, String SPAJ_ID_TAB) {
        ContentValues cv = new ContentValues();
        cv.put("COUNTER", counter);
        cv.put("SPAJ_ID_TAB", SPAJ_ID_TAB);
        cv.put("NAMA", me.getNama());
        cv.put("MANFAAT", me.getManfaat());
        cv.put("JEKEL", me.getJekel());
        cv.put("TTL", me.getTtl());
        cv.put("HUB_DGCALON_TT", me.getHub_dgcalon_tt());
        cv.put("WARGANEGARA", me.getWarganegara());

        return cv;
    }

    public ArrayList<ModelDataDitunjukDI> getObjectArray(Cursor cursor) {

        ArrayList<ModelDataDitunjukDI> mes = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ModelDataDitunjukDI me = new ModelDataDitunjukDI();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("COUNTER"))) {
                    me.setCounter(cursor.getInt(cursor.getColumnIndexOrThrow("COUNTER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("NAMA"))) {
                    me.setNama(cursor.getString(cursor.getColumnIndexOrThrow("NAMA")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("MANFAAT"))) {
                    me.setManfaat(cursor.getDouble(cursor.getColumnIndexOrThrow("MANFAAT")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("JEKEL"))) {
                    me.setJekel(cursor.getInt(cursor.getColumnIndexOrThrow("JEKEL")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("TTL"))) {
                    me.setTtl(cursor.getString(cursor.getColumnIndexOrThrow("TTL")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("HUB_DGCALON_TT"))) {
                    me.setHub_dgcalon_tt(cursor.getInt(cursor.getColumnIndexOrThrow("HUB_DGCALON_TT")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("WARGANEGARA"))) {
                    me.setWarganegara(cursor.getInt(cursor.getColumnIndexOrThrow("WARGANEGARA")));
                }
                mes.add(me);
                cursor.moveToNext();
            }

            // always close the cursor
            cursor.close();
        }

        return mes;
    }
}
