package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.UsulanAsuransiModel;

/**
 * Created by Bernard on 05/09/2017.
 */

public class TableUsulanAsuransi {
    private Context context;

    public TableUsulanAsuransi(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(EspajModel me){
        ContentValues cv = new ContentValues();
        cv.put("JENIS_PRODUK_UA", me.getUsulanAsuransiModel().getJenis_produk_ua());
        cv.put("KD_PRODUK_UA", me.getUsulanAsuransiModel().getKd_produk_ua());
        cv.put("SUB_PRODUK_UA", me.getUsulanAsuransiModel().getSub_produk_ua());
        cv.put("KLAS_UA", me.getUsulanAsuransiModel().getKlas_ua());
        cv.put("PAKET_UA", me.getUsulanAsuransiModel().getPaket_ua());
        cv.put("EKASEHAT_PLAN_UA", me.getUsulanAsuransiModel().getEkasehat_plan_ua());
        cv.put("MASA_PERTANGGUNGAN_UA", me.getUsulanAsuransiModel().getMasa_pertanggungan_ua());
        cv.put("CUTI_PREMI_UA", me.getUsulanAsuransiModel().getCuti_premi_ua());
        cv.put("KURS_UP_UA", me.getUsulanAsuransiModel().getKurs_up_ua());
        cv.put("UP_UA", me.getUsulanAsuransiModel().getUp_ua());
        cv.put("UNITLINK_OPSIPREMI_UA", me.getUsulanAsuransiModel().getUnitlink_opsipremi_ua());
        cv.put("UNITLINK_KURS_UA", me.getUsulanAsuransiModel().getUnitlink_kurs_ua());
        cv.put("UNITLINK_PREMISTANDARD_UA", me.getUsulanAsuransiModel().getUnitlink_premistandard_ua());
        cv.put("UNITLINK_KOMBINASI_UA", me.getUsulanAsuransiModel().getUnitlink_kombinasi_ua());
        cv.put("UNITLINK_KURS_TOTAL_UA", me.getUsulanAsuransiModel().getUnitlink_kurs_total_ua());
        cv.put("UNITLINK_TOTAL_UA", me.getUsulanAsuransiModel().getUnitlink_total_ua());
        cv.put("CARABAYAR_UA", me.getUsulanAsuransiModel().getCarabayar_ua());
        cv.put("BENTUKBAYAR_UA", me.getUsulanAsuransiModel().getBentukbayar_ua());
        cv.put("AWALPERTANGGUNGAN_UA", me.getUsulanAsuransiModel().getAwalpertanggungan_ua());
        cv.put("AKHIRPERTANGGUNGAN_UA", me.getUsulanAsuransiModel().getAkhirpertanggungan_ua());
        cv.put("MASA_PEMBAYARAN_UA", me.getUsulanAsuransiModel().getMasa_pembayaran_ua());
        cv.put("VALIDATION", me.getUsulanAsuransiModel().getValidation());

        return cv;
    }

    public UsulanAsuransiModel getObject (Cursor cursor){
        UsulanAsuransiModel usulanAsuransiModel= new UsulanAsuransiModel();
        if (cursor != null && cursor.getCount()>0){
            cursor.moveToFirst();
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("JENIS_PRODUK_UA"))){
                usulanAsuransiModel.setJenis_produk_ua(cursor.getInt(cursor.getColumnIndexOrThrow("JENIS_PRODUK_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KD_PRODUK_UA"))){
                usulanAsuransiModel.setKd_produk_ua(cursor.getInt(cursor.getColumnIndexOrThrow("KD_PRODUK_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("SUB_PRODUK_UA"))){
                usulanAsuransiModel.setSub_produk_ua(cursor.getInt(cursor.getColumnIndexOrThrow("SUB_PRODUK_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KLAS_UA"))){
                usulanAsuransiModel.setKlas_ua(cursor.getInt(cursor.getColumnIndexOrThrow("KLAS_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PAKET_UA"))){
                usulanAsuransiModel.setPaket_ua(cursor.getInt(cursor.getColumnIndexOrThrow("PAKET_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("EKASEHAT_PLAN_UA"))){
                usulanAsuransiModel.setEkasehat_plan_ua(cursor.getInt(cursor.getColumnIndexOrThrow("EKASEHAT_PLAN_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("MASA_PERTANGGUNGAN_UA"))){
                usulanAsuransiModel.setMasa_pertanggungan_ua(cursor.getInt(cursor.getColumnIndexOrThrow("MASA_PERTANGGUNGAN_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("CUTI_PREMI_UA"))){
                usulanAsuransiModel.setCuti_premi_ua(cursor.getInt(cursor.getColumnIndexOrThrow("CUTI_PREMI_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KURS_UP_UA"))){
                usulanAsuransiModel.setKurs_up_ua(cursor.getString(cursor.getColumnIndexOrThrow("KURS_UP_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("UP_UA"))){
                usulanAsuransiModel.setUp_ua(cursor.getDouble(cursor.getColumnIndexOrThrow("UP_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("UNITLINK_OPSIPREMI_UA"))){
                usulanAsuransiModel.setUnitlink_opsipremi_ua(cursor.getInt(cursor.getColumnIndexOrThrow("UNITLINK_OPSIPREMI_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("UNITLINK_KURS_UA"))){
                usulanAsuransiModel.setUnitlink_kurs_ua(cursor.getString(cursor.getColumnIndexOrThrow("UNITLINK_KURS_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("UNITLINK_PREMISTANDARD_UA"))){
                usulanAsuransiModel.setUnitlink_premistandard_ua(cursor.getDouble(cursor.getColumnIndexOrThrow("UNITLINK_PREMISTANDARD_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("UNITLINK_KOMBINASI_UA"))){
                usulanAsuransiModel.setUnitlink_kombinasi_ua(cursor.getString(cursor.getColumnIndexOrThrow("UNITLINK_KOMBINASI_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("UNITLINK_KURS_TOTAL_UA"))){
                usulanAsuransiModel.setUnitlink_kurs_total_ua(cursor.getString(cursor.getColumnIndexOrThrow("UNITLINK_KURS_TOTAL_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("UNITLINK_TOTAL_UA"))){
                usulanAsuransiModel.setUnitlink_total_ua(cursor.getDouble(cursor.getColumnIndexOrThrow("UNITLINK_TOTAL_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("CARABAYAR_UA"))){
                usulanAsuransiModel.setCarabayar_ua(cursor.getInt(cursor.getColumnIndexOrThrow("CARABAYAR_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("BENTUKBAYAR_UA"))){
                usulanAsuransiModel.setBentukbayar_ua(cursor.getInt(cursor.getColumnIndexOrThrow("BENTUKBAYAR_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AWALPERTANGGUNGAN_UA"))){
                usulanAsuransiModel.setAwalpertanggungan_ua(cursor.getString(cursor.getColumnIndexOrThrow("AWALPERTANGGUNGAN_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AKHIRPERTANGGUNGAN_UA"))){
                usulanAsuransiModel.setAkhirpertanggungan_ua(cursor.getString(cursor.getColumnIndexOrThrow("AKHIRPERTANGGUNGAN_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("MASA_PEMBAYARAN_UA"))){
                usulanAsuransiModel.setMasa_pembayaran_ua(cursor.getInt(cursor.getColumnIndexOrThrow("MASA_PEMBAYARAN_UA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("VALIDATION"))){
                int valid = cursor.getInt(cursor.getColumnIndexOrThrow("VALIDATION"));
//                boolean isValid = valid == 1 ? true : false;
                boolean val;
                val = valid == 1;
                usulanAsuransiModel.setValidation(val);
            }
            cursor.close();
        }
        return usulanAsuransiModel;
    }
}
