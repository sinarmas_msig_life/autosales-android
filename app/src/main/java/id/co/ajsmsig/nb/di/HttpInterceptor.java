package id.co.ajsmsig.nb.di;

import android.app.Application;
import android.content.Context;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import java.util.concurrent.TimeUnit;


import javax.inject.Inject;

import id.co.ajsmsig.nb.di.AppController;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import okhttp3.Interceptor;

/**
 * Created by fajarca on 12/5/17.
 */

public class HttpInterceptor implements Interceptor {

    private final String TAG = getClass().getSimpleName();

    public static final String CONNECT_TIMEOUT = "CONNECT_TIMEOUT";
    public static final String READ_TIMEOUT = "READ_TIMEOUT";
    public static final String WRITE_TIMEOUT = "WRITE_TIMEOUT";

    public HttpInterceptor() {
    }


    @Override
    public Response intercept(Chain chain) throws IOException {
        //Request
        Request request = chain.request();

        int connectTimeout = chain.connectTimeoutMillis();
        int readTimeout = chain.readTimeoutMillis();
        int writeTimeout = chain.writeTimeoutMillis();

        String connectNew = request.header(CONNECT_TIMEOUT);
        String readNew = request.header(READ_TIMEOUT);
        String writeNew = request.header(WRITE_TIMEOUT);

        if (!TextUtils.isEmpty(connectNew)) {
            connectTimeout = Integer.valueOf(connectNew);
        }
        if (!TextUtils.isEmpty(readNew)) {
            readTimeout = Integer.valueOf(readNew);

        }
        if (!TextUtils.isEmpty(writeNew)) {
            writeTimeout = Integer.valueOf(writeNew);
        }

        return chain
                .withConnectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
                .withReadTimeout(readTimeout, TimeUnit.MILLISECONDS)
                .withWriteTimeout(writeTimeout, TimeUnit.MILLISECONDS)
                .proceed(request);
    }
}
