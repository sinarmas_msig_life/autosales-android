package id.co.ajsmsig.nb.prop.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.adapter.P_AdapterListLinkIlustrasi;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.IllustrationResultVO;

/**
 * Created by faiz_f on 03/02/2017.
 */

public class P_IllustrationLinkFragment extends Fragment {
    //    private ArrayList<P_IllustrationDevelopmentFundModel> illustrationDevelopmentFundModels;
    public P_IllustrationLinkFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.p_fragment_illustration, container, false);

        Bundle bundle = getActivity().getIntent().getExtras();
        @SuppressWarnings("unchecked")
        HashMap<String, Object> iHash = (HashMap<String, Object>) bundle.getSerializable(getString(R.string.Illustrasi));
        if (iHash != null) {
            IllustrationResultVO illustrationResult = (IllustrationResultVO) iHash.get(getContext().getString(R.string.Illustration1));
            String validityMsq = illustrationResult.getValidityMsg();
            ArrayList<LinkedHashMap<String, String>> mapList = illustrationResult.getIllustrationList();

//
//            illustrationDevelopmentFundModels = P_IllustrationActivity.myBundle.getParcelableArrayList(getString(R.string.Illustrasi));
//            if (illustrationDevelopmentFundModels == null) {
//                Log.e("array ilustrasi", "check array ilustrasi: null");
//                illustrationDevelopmentFundModels = new ArrayList<>();
//            }
//
            if (mapList != null) {

                ListView lv_ilustrasi = view.findViewById(R.id.lv_ilustrasi);

                P_AdapterListLinkIlustrasi adapterListIlustrasi = new P_AdapterListLinkIlustrasi(getContext(), R.layout.p_fragment_illustration_item, mapList);
                lv_ilustrasi.setAdapter(adapterListIlustrasi);
            }
        }

        return view;
    }


}
