
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveProposalDataResponse {

    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("flag")
    @Expose
    private String flag;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

}
