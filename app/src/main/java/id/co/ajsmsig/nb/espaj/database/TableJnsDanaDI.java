package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelJnsDanaDI;

/**
 * Created by rizky_c on 30/09/2017.
 */

public class TableJnsDanaDI {

    private Context context;

    public TableJnsDanaDI(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(ModelJnsDanaDI me, int counter, String SPAJ_ID_TAB) {
        ContentValues cv = new ContentValues();
        cv.put("COUNTER", counter);
        cv.put("SPAJ_ID_TAB", SPAJ_ID_TAB);
        cv.put("JENIS_DANA", me.getJenis_dana());
        cv.put("JUMLAH_ALOKASI", me.getJumlah_alokasi());
        cv.put("PERSEN_ALOKASI", me.getPersen_alokasi());

        return cv;
    }

    public ArrayList<ModelJnsDanaDI> getObjectArray(Cursor cursor) {
        ArrayList<ModelJnsDanaDI> mes = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ModelJnsDanaDI me = new ModelJnsDanaDI();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("COUNTER"))) {
                    me.setCounter(cursor.getInt(cursor.getColumnIndexOrThrow("COUNTER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("JENIS_DANA"))) {
                    me.setJenis_dana(cursor.getString(cursor.getColumnIndexOrThrow("JENIS_DANA")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("JUMLAH_ALOKASI"))) {
                    me.setJumlah_alokasi(cursor.getDouble(cursor.getColumnIndexOrThrow("JUMLAH_ALOKASI")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("PERSEN_ALOKASI"))) {
                    me.setPersen_alokasi(cursor.getDouble(cursor.getColumnIndexOrThrow("PERSEN_ALOKASI")));
                }
                mes.add(me);
                cursor.moveToNext();
            }

            // always close the cursor
            cursor.close();
        }

        return mes;
    }
}
