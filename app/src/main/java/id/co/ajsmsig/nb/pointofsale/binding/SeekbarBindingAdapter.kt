package id.co.ajsmsig.nb.pointofsale.binding

import android.databinding.BindingAdapter
import id.co.ajsmsig.nb.pointofsale.custom.indicatorseekbar.IndicatorSeekBar
import id.co.ajsmsig.nb.pointofsale.custom.indicatorseekbar.IndicatorSeekBarType

/**
 * Created by andreyyoshuamanik on 11/03/18.
 */

@BindingAdapter("app:seekbar_minvalue")
fun setSeekbarMinValue(view: IndicatorSeekBar, seekbar_minvalue: Long) {
    view.min = seekbar_minvalue.toFloat()
}

@BindingAdapter("app:seekbar_maxvalue")
fun setSeekbarMaxValue(view: IndicatorSeekBar, seekbar_maxvalue: Long) {
    view.max = seekbar_maxvalue.toFloat()
}


@BindingAdapter("app:seekbar_range")
fun setSeekbarRange(view: IndicatorSeekBar, seekbarRange: Int) {
    view.builder.setSeekBarType(IndicatorSeekBarType.DISCRETE_TICKS)
    val totalTickNum = view.max - view.min / seekbarRange
    view.builder.setTickNum(totalTickNum.toInt())
}

@BindingAdapter("app:seekbar_progress")
fun setSeekbarProgressValue(view: IndicatorSeekBar, value: Long) {
    view.setProgress(value.toFloat())
}