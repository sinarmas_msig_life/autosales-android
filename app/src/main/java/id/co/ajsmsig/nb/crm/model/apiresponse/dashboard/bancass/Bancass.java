
package id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.bancass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bancass {

    @SerializedName("meetandpre")
    @Expose
    private Meetandpre meetandpre;
    @SerializedName("closing")
    @Expose
    private Closing closing;

    public Meetandpre getMeetandpre() {
        return meetandpre;
    }

    public void setMeetandpre(Meetandpre meetandpre) {
        this.meetandpre = meetandpre;
    }

    public Closing getClosing() {
        return closing;
    }

    public void setClosing(Closing closing) {
        this.closing = closing;
    }

}
