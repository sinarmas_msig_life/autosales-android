package id.co.ajsmsig.nb.prop.method.util;

import java.util.ArrayList;
import java.util.List;

public class ListUtil
{
    public static List<String> addNothingIfEmpty(String content )
    {
        List<String> result = new ArrayList<String>();
        if( content.isEmpty())
        {
            result.add( content );
        }
        return result;
    }
    
    public static ArrayList serializableList(List dataList){
		if(dataList!=null){
			return new ArrayList(dataList);
		}else{
			return null;
		}
	}
	
}
