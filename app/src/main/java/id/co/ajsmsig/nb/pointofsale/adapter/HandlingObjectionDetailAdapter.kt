package id.co.ajsmsig.nb.pointofsale.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosRowHandlingObjectionQuestionBinding
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.HandlingObjectionAllAnswers

/**
 * Created by andreyyoshuamanik on 21/02/18.
 */
class HandlingObjectionDetailAdapter: RecyclerView.Adapter<HandlingObjectionDetailAdapter.HandlingObjectionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HandlingObjectionViewHolder {

        val dataBinding = DataBindingUtil.inflate<PosRowHandlingObjectionQuestionBinding>(LayoutInflater.from(parent.context), R.layout.pos_row_handling_objection_question, parent, false)
        return HandlingObjectionViewHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: HandlingObjectionViewHolder, position: Int) {
        options?.let {
            holder.bind(it[position])
        }
    }

    var options: Array<HandlingObjectionAllAnswers>? = null
    set(value) {
        field = value

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return options?.size ?: 0
    }

    inner class HandlingObjectionViewHolder(private val dataBinding: PosRowHandlingObjectionQuestionBinding): RecyclerView.ViewHolder(dataBinding.root) {

        fun bind(option: HandlingObjectionAllAnswers?) {
            dataBinding.vm = option
            dataBinding.viewHolder = this
        }

        fun onClickOption(option: HandlingObjectionAllAnswers) {

            options?.forEach {
                if (it.selected.get()) {
                    it.selected.set(false)
                } else {

                    it.selected.set(option == it)
                }
            }

        }
    }
}