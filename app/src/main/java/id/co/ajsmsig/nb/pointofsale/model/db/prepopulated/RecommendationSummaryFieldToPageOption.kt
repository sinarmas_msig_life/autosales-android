package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by andreyyoshuamanik on 02/03/18.
 */
@Entity
class RecommendationSummaryFieldToPageOption(
        @PrimaryKey
        val id: Int,
        var recommendationSummaryFieldId: Int?,
        var pageOptionid: Int?
)