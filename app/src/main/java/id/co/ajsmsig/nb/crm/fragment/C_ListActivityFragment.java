package id.co.ajsmsig.nb.crm.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.activity.C_FormAktifitasActivity;
import id.co.ajsmsig.nb.crm.adapter.LeadActivityListAdapter;
import id.co.ajsmsig.nb.crm.database.C_Delete;
import id.co.ajsmsig.nb.crm.database.C_Insert;
import id.co.ajsmsig.nb.crm.database.C_Select;

import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.method.async.CrmRestClientUsage;
import id.co.ajsmsig.nb.crm.model.ActivityListModel;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.LeadActivityList;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.DateUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
  Created by faiz_f on 03/02/2017.
 */

public class C_ListActivityFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, LeadActivityListAdapter.ClickListener {
    private long SL_TAB_ID;
    private long SL_ID;
    private String msagId;
    private int bcLoginType;
    private C_Insert insert;
    private C_Delete delete;
    private LeadActivityListAdapter mAdapter;
    private List<ActivityListModel> activities;
    private final int LOADER_DISPLAY_LEAD_ACTIVITY = 340;
    private Cursor cursor;
    private final String CLOSING_SUB_ACTIVITY = "45";

    @BindView(R.id.layoutNoActivity)
    LinearLayout layoutNoActivity;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tvSync)
    TextView tvSync;
    @BindView(R.id.layoutNoInternetConnection)
    ConstraintLayout layoutNoInternetConnection;
    @BindView(R.id.layoutLoading)
    LinearLayout layoutLoading;

    public C_ListActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intentData = getActivity().getIntent();
        SL_TAB_ID = intentData.getLongExtra(getString(R.string.SL_TAB_ID), -1);
        SL_ID = intentData.getLongExtra(Const.INTENT_KEY_SL_ID, -1);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
        msagId = sharedPreferences.getString("KODE_AGEN", "");
        bcLoginType = sharedPreferences.getInt("JENIS_LOGIN_BC", 0);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.c_fragment_list_activity, container, false);
        ButterKnife.bind(this, view);
        initRecyclerView();
        insert = new C_Insert(getActivity());
        delete = new C_Delete(getActivity());

        if (StaticMethods.isNetworkAvailable(getActivity())) {
            if (!TextUtils.isEmpty(msagId) && SL_ID != -1 && SL_ID != 0) {
                fetchActivityListFromLead(msagId, String.valueOf(SL_ID), "SLA_CRTD_DATE", "desc");
            }
            getActivity().getSupportLoaderManager().initLoader(LOADER_DISPLAY_LEAD_ACTIVITY, null, this);
            //Hide no activity layout
            layoutNoInternetConnection.setVisibility(View.GONE);
        } else {
            //Show no activity layout
            layoutNoInternetConnection.setVisibility(View.VISIBLE);
            getActivity().getSupportLoaderManager().initLoader(LOADER_DISPLAY_LEAD_ACTIVITY, null, this);
        }


        return view;
    }

    @OnClick(R.id.btnAddActivity)
    void onBtnAddActivityPressed() {
        Intent intent = new Intent(getActivity(), C_FormAktifitasActivity.class);
        Bundle bundle = new Bundle();
        bundle.putLong(getString(R.string.SL_TAB_ID), SL_TAB_ID);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader cursorLoader;

        switch (id) {
            case LOADER_DISPLAY_LEAD_ACTIVITY:
                cursor = new C_Select(getActivity()).getActivityListFromLead(String.valueOf(SL_TAB_ID), String.valueOf(SL_ID));
                break;
        }


        cursorLoader = new CursorLoader(getContext()) {
            @Override
            public Cursor loadInBackground() {
                return cursor;
            }
        };

        return cursorLoader;

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case LOADER_DISPLAY_LEAD_ACTIVITY:
                if (cursor.getCount() > 0) {
                    clearPreviousResult();
                    layoutNoActivity.setVisibility(View.GONE);

                    List<ActivityListModel> activities = new ArrayList<>();
                    C_Select select = new C_Select(getActivity());

                    while (cursor.moveToNext()) {
                        String activityType = cursor.getString(cursor.getColumnIndexOrThrow("SLA_TYPE"));
                        String subActivityType = cursor.getString(cursor.getColumnIndexOrThrow("SLA_SUBTYPE"));
                        String activityDate = cursor.getString(cursor.getColumnIndexOrThrow("SLA_CRTD_DATE"));
                        String slaId = cursor.getString(cursor.getColumnIndexOrThrow("SLA_ID"));
                        String slaTabId = cursor.getString(cursor.getColumnIndexOrThrow("SLA_TAB_ID"));
                        String slaDetail = cursor.getString(cursor.getColumnIndexOrThrow("SLA_DETAIL"));
                        int slaLastPost = cursor.getInt(cursor.getColumnIndexOrThrow("SLA_LAST_POS"));
                        boolean isSynced = slaLastPost == 0; //Synced = 0

                        String activityCreatedDate = DateUtils.getDateFrom(activityDate);
                        String activityName = select.getSelectedActivityFromSlaType(activityType);
                        String subActivityName = select.getSelectedSubActivityFromSlaSubType(subActivityType);

                        ActivityListModel activity = new ActivityListModel(slaId, slaTabId, activityName, subActivityName, activityCreatedDate, activityDate, subActivityType, slaDetail,isSynced);

                        activities.add(activity);
                    }

                    //Add and display the activities
                    mAdapter.addAll(activities);

                } else {
                    layoutNoActivity.setVisibility(View.VISIBLE);
                }

                break;
        }

    }

    private void clearPreviousResult() {
        if (activities != null && activities.size() > 0) {
            activities.clear();
        }
    }

    @OnClick(R.id.tvSync)
    void onSyncPressed() {
        if (StaticMethods.isNetworkAvailable(getActivity())) {
            if (!TextUtils.isEmpty(msagId) && SL_ID != -1) {
                fetchActivityListFromLead(msagId, String.valueOf(SL_ID), "SLA_CRTD_DATE", "desc");
            }
            //Hide no activity layout
            layoutNoInternetConnection.setVisibility(View.GONE);
            return;
        }

        Toast.makeText(getActivity(), "Anda tidak terkoneksi dengan internet", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().getSupportLoaderManager().restartLoader(LOADER_DISPLAY_LEAD_ACTIVITY, null, this);
    }


    /**
     * Fetch selected lead activity list by specifying it's SL_ID
     *
     * @param msagId        agent code
     * @param slId          simbak lead id
     * @param sortBy        SLA_CRTD_DATE
     * @param sortDirection asc / desc
     */
    private void fetchActivityListFromLead(String msagId, String slId, String sortBy, String sortDirection) {
        C_Select select = new C_Select(getActivity());
        layoutLoading.setVisibility(View.VISIBLE);
        String url = AppConfig.getBaseUrlCRM().concat("v2/toll/module_activity2");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<List<LeadActivityList>> call = api.fetchActivityListFromLead(url, msagId, false, slId, sortBy, sortDirection);
        call.enqueue(new Callback<List<LeadActivityList>>() {
            @Override
            public void onResponse(@NonNull Call<List<LeadActivityList>> call, @NonNull Response<List<LeadActivityList>> response) {

                if (response.body() != null) {
                    List<LeadActivityList> activities = response.body();

                    //Refresh the spinnerBcBranch with the latest data
                    if (activities != null && activities.size() > 0) {

                        //Delete previous activity
                        delete.deleteActivityFromLead(String.valueOf(SL_TAB_ID), String.valueOf(slId));

                        //Insert the new activity
                        insert.insertActivityListToDatabase(activities);

                        List<ActivityListModel> savedActivityList = select.getSavedActivityList(String.valueOf(SL_TAB_ID), String.valueOf(slId));

                        clearPreviousLeadResult();

                        //Add data to recyclerView model and display it
                        mAdapter.addAll(savedActivityList);


                    }
                } else {
                    List<ActivityListModel> savedActivityList = select.getSavedActivityList(String.valueOf(SL_TAB_ID), String.valueOf(slId));

                    clearPreviousLeadResult();

                    //Add data to recyclerView model and display it
                    mAdapter.addAll(savedActivityList);
                }

                layoutLoading.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(@NonNull Call<List<LeadActivityList>> call, @NonNull Throwable t) {
                layoutLoading.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Tidak dapat mengunduh aktivitas terbaru "+t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Remove previous lead result when user changed sort mode
     */
    private void clearPreviousLeadResult() {
        if (activities != null && activities.size() > 0) {
            activities.clear();
        }
    }


    /**
     * Init recyclerview to display activity list
     */
    private void initRecyclerView() {
        activities = new ArrayList<>();
        mAdapter = new LeadActivityListAdapter(activities);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setClickListener(this);
    }

    @Override
    public void onActivitySelected(int position, ActivityListModel model) {
        //SLA_SUBTYPE CLOSING YAITU 45
        String activityCreatedDate = model.getRawActivityCreatedDate();
        String subActivityType = model.getRawSubActivityType();
        boolean isClosingActivity = isClosingActivity(subActivityType);
        boolean isAllowedToEditClosingActivity = isAllowedToEditClosingActivity(activityCreatedDate);

        boolean isAllowedToEditActivity = isAllowedToEditActivity(activityCreatedDate);

        //Closing activity
        if (isClosingActivity && !isAllowedToEditClosingActivity) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Sudah melewati batas waktu edit")
                    .setMessage("Batas waktu edit aktivitas closing ini adalah "+getMaxEditDateForClosingActivity(activityCreatedDate))
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                    .show();
            return;
        }

        //Non closing activity
        if (!isClosingActivity && !isAllowedToEditActivity) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Sudah melewati batas waktu edit")
                    .setMessage("Batas waktu untuk edit aktivitas adalah di minggu berjalan penginputan aktivitas ini, dengan batas waktu edit maksimum "+getMaxEditDate(activityCreatedDate))
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                    .show();
            return;
        }

        Intent intent = new Intent(getActivity(), C_FormAktifitasActivity.class);
        Bundle bundle = new Bundle();
        bundle.putLong(Const.INTENT_KEY_SL_TAB_ID, SL_TAB_ID);
        bundle.putString(Const.INTENT_KEY_SLA_TAB_ID, model.getSlaTabId());
        bundle.putString(Const.INTENT_KEY_SLA_ID, model.getSlaId());
        bundle.putBoolean(Const.INTENT_KEY_ADD_ACTIVITY_MODE, false);
        intent.putExtras(bundle);
        startActivity(intent);


    }

    /**
     * Check if user is allowed to make closing activity at the current date
     * Rules : SPAJ is able to input and edit on the present month with maximum input for the fourth day of next month
     *
     * @return
     */
    private boolean isAllowedToEditClosingActivity(String closingActivityCreatedDate) {

        if (!TextUtils.isEmpty(closingActivityCreatedDate)) {
            try {
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(closingActivityCreatedDate);
                long closingActivityCreatedDateMillis = date.getTime();

                //Test untuk closing activity
                //closingActivityCreatedDateMillis = 1533402000000L;

                Calendar calendar = Calendar.getInstance();
                //Get maximum days count in current month
                int daysCountInCurrentMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

                //Add it by 4 days, set the hours, minute, and second to 0.
                //So the converted millis will produce ex: Wed Apr 04 2018 00:00:00
                calendar.set(Calendar.DATE, daysCountInCurrentMonth + 4);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);

                //Get the 4th day on next month millis
                long nextMonthAt4thDayMillis = calendar.getTimeInMillis();


                //Will return true if current datetime is before deadline date
                return closingActivityCreatedDateMillis < nextMonthAt4thDayMillis;

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private boolean isAllowedToEditActivity(String activityCreatedDate) {

        if (!TextUtils.isEmpty(activityCreatedDate)) {
            try {
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(activityCreatedDate);

                Calendar calendar = Calendar.getInstance();
                long currentMillis = calendar.getTimeInMillis();

                calendar.setTime(date);

                int currentWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                calendar.set(Calendar.WEEK_OF_YEAR, currentWeekOfYear + 1);

                long sundayMillis = calendar.getTimeInMillis();

                return currentMillis < sundayMillis;


            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return false;
    }
    private String getMaxEditDate(String activityCreatedDate) {
        if (!TextUtils.isEmpty(activityCreatedDate)) {
            try {
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(activityCreatedDate);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                int currentWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

                calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                calendar.set(Calendar.WEEK_OF_YEAR, currentWeekOfYear + 1);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                String maxEditDate = DateUtils.getDateFrom(calendar.getTime());

                return maxEditDate;

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
    private String getMaxEditDateForClosingActivity(String activityCreatedDate) {
        if (!TextUtils.isEmpty(activityCreatedDate)) {
            try {
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(activityCreatedDate);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                //Get maximum days count in current month
                int daysCountInCurrentMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

                //Add it by 4 days, set the hours, minute, and second to 0.
                //So the converted millis will produce ex: Wed Apr 04 2018 00:00:00
                calendar.set(Calendar.DATE, daysCountInCurrentMonth + 4);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                String maxEditDate = DateUtils.getDateFrom(calendar.getTime());

                return maxEditDate;

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
    private boolean isClosingActivity(String subActivityId) {
        return subActivityId.equalsIgnoreCase(CLOSING_SUB_ACTIVITY);
    }


}

