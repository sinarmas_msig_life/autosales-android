package id.co.ajsmsig.nb.espaj.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAskesUA;

/**
 * Created by Bernard on 30/08/2017.
 */

public class DetilInvestasiModel implements Parcelable{

    private Double premi_pokok_di= (double)0;
    private Double premi_berkala_di=(double)0;
    private int premitopup_berkala_di=0;
   	private Double premi_tunggal_di=(double)0;
   	private int premitopup_tunggal_di=0;
   	private Double premi_tambahan_di=(double)0;
   	private Double jumlah_di=(double)0;
   	private int invest_bayar_premi_di=0;
   	private int invest_bank_di=0;
   	private String invest_cabang_di="";
   	private String invest_kota_di="";
   	private int invest_jnstab_di=0;
   	private int invest_jnsnasabah_di=0;
   	private String invest_norek_di="";
   	private String invest_nama_di="";
   	private String invest_kurs_di="";
   	private int invest_berikuasa_di=0;
   	private String invest_tglkuasa_di="";
   	private String invest_keterangan_di="";
   	private int autodbt_bank_di=0;
   	private int autodbt_jnstab_di=0;
   	private String autodbt_kurs_di="";
   	private String autodbt_norek_di="";
   	private String autodbt_nama_di="";
   	private String autodbt_tgldebet_di="";
   	private String autodbt_tglvalid_di="";
   	private int autodbt_aktif_di=0;
   	private int autodbt_premipertama_di=0;
   	private String autodbt_no_simascard_di="";
   	private int danainvest_alokasi_di=0;
    private Double jumlah1_di= (double)0;
    private Double persen1_di= (double)0;
    private Double jumlah2_di= (double)0;
    private Double persen2_di= (double)0;
    private Double jumlah3_di= (double)0;
    private Double persen3_di= (double)0;
    private List<ModelAskesUA> ListModelDanaDI;
    private Boolean validation = false;

    public DetilInvestasiModel() {
    }


    protected DetilInvestasiModel(Parcel in) {
        premitopup_berkala_di = in.readInt();
        premitopup_tunggal_di = in.readInt();
        invest_bayar_premi_di = in.readInt();
        invest_bank_di = in.readInt();
        invest_cabang_di = in.readString();
        invest_kota_di = in.readString();
        invest_jnstab_di = in.readInt();
        invest_jnsnasabah_di = in.readInt();
        invest_norek_di = in.readString();
        invest_nama_di = in.readString();
        invest_kurs_di = in.readString();
        invest_berikuasa_di = in.readInt();
        invest_tglkuasa_di = in.readString();
        invest_keterangan_di = in.readString();
        autodbt_bank_di = in.readInt();
        autodbt_jnstab_di = in.readInt();
        autodbt_kurs_di = in.readString();
        autodbt_norek_di = in.readString();
        autodbt_nama_di = in.readString();
        autodbt_tgldebet_di = in.readString();
        autodbt_tglvalid_di = in.readString();
        autodbt_aktif_di = in.readInt();
        autodbt_premipertama_di = in.readInt();
        autodbt_no_simascard_di = in.readString();
        danainvest_alokasi_di = in.readInt();
        ListModelDanaDI = in.createTypedArrayList(ModelAskesUA.CREATOR);
    }

    public DetilInvestasiModel(Double premi_pokok_di, Double premi_berkala_di, int premitopup_berkala_di, Double premi_tunggal_di, int premitopup_tunggal_di, Double premi_tambahan_di, Double jumlah_di, int invest_bayar_premi_di, int invest_bank_di, String invest_cabang_di, String invest_kota_di, int invest_jnstab_di, int invest_jnsnasabah_di, String invest_norek_di, String invest_nama_di, String invest_kurs_di, int invest_berikuasa_di, String invest_tglkuasa_di, String invest_keterangan_di, int autodbt_bank_di, int autodbt_jnstab_di, String autodbt_kurs_di, String autodbt_norek_di, String autodbt_nama_di, String autodbt_tgldebet_di, String autodbt_tglvalid_di, int autodbt_aktif_di, int autodbt_premipertama_di, String autodbt_no_simascard_di, int danainvest_alokasi_di, Double jumlah1_di, Double persen1_di, Double jumlah2_di, Double persen2_di, Double jumlah3_di, Double persen3_di, List<ModelAskesUA> listModelDanaDI, Boolean validation) {
        this.premi_pokok_di = premi_pokok_di;
        this.premi_berkala_di = premi_berkala_di;
        this.premitopup_berkala_di = premitopup_berkala_di;
        this.premi_tunggal_di = premi_tunggal_di;
        this.premitopup_tunggal_di = premitopup_tunggal_di;
        this.premi_tambahan_di = premi_tambahan_di;
        this.jumlah_di = jumlah_di;
        this.invest_bayar_premi_di = invest_bayar_premi_di;
        this.invest_bank_di = invest_bank_di;
        this.invest_cabang_di = invest_cabang_di;
        this.invest_kota_di = invest_kota_di;
        this.invest_jnstab_di = invest_jnstab_di;
        this.invest_jnsnasabah_di = invest_jnsnasabah_di;
        this.invest_norek_di = invest_norek_di;
        this.invest_nama_di = invest_nama_di;
        this.invest_kurs_di = invest_kurs_di;
        this.invest_berikuasa_di = invest_berikuasa_di;
        this.invest_tglkuasa_di = invest_tglkuasa_di;
        this.invest_keterangan_di = invest_keterangan_di;
        this.autodbt_bank_di = autodbt_bank_di;
        this.autodbt_jnstab_di = autodbt_jnstab_di;
        this.autodbt_kurs_di = autodbt_kurs_di;
        this.autodbt_norek_di = autodbt_norek_di;
        this.autodbt_nama_di = autodbt_nama_di;
        this.autodbt_tgldebet_di = autodbt_tgldebet_di;
        this.autodbt_tglvalid_di = autodbt_tglvalid_di;
        this.autodbt_aktif_di = autodbt_aktif_di;
        this.autodbt_premipertama_di = autodbt_premipertama_di;
        this.autodbt_no_simascard_di = autodbt_no_simascard_di;
        this.danainvest_alokasi_di = danainvest_alokasi_di;
        this.jumlah1_di = jumlah1_di;
        this.persen1_di = persen1_di;
        this.jumlah2_di = jumlah2_di;
        this.persen2_di = persen2_di;
        this.jumlah3_di = jumlah3_di;
        this.persen3_di = persen3_di;
        ListModelDanaDI = listModelDanaDI;
        this.validation = validation;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(premitopup_berkala_di);
        dest.writeInt(premitopup_tunggal_di);
        dest.writeInt(invest_bayar_premi_di);
        dest.writeInt(invest_bank_di);
        dest.writeString(invest_cabang_di);
        dest.writeString(invest_kota_di);
        dest.writeInt(invest_jnstab_di);
        dest.writeInt(invest_jnsnasabah_di);
        dest.writeString(invest_norek_di);
        dest.writeString(invest_nama_di);
        dest.writeString(invest_kurs_di);
        dest.writeInt(invest_berikuasa_di);
        dest.writeString(invest_tglkuasa_di);
        dest.writeString(invest_keterangan_di);
        dest.writeInt(autodbt_bank_di);
        dest.writeInt(autodbt_jnstab_di);
        dest.writeString(autodbt_kurs_di);
        dest.writeString(autodbt_norek_di);
        dest.writeString(autodbt_nama_di);
        dest.writeString(autodbt_tgldebet_di);
        dest.writeString(autodbt_tglvalid_di);
        dest.writeInt(autodbt_aktif_di);
        dest.writeInt(autodbt_premipertama_di);
        dest.writeString(autodbt_no_simascard_di);
        dest.writeInt(danainvest_alokasi_di);
        dest.writeTypedList(ListModelDanaDI);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DetilInvestasiModel> CREATOR = new Creator<DetilInvestasiModel>() {
        @Override
        public DetilInvestasiModel createFromParcel(Parcel in) {
            return new DetilInvestasiModel(in);
        }

        @Override
        public DetilInvestasiModel[] newArray(int size) {
            return new DetilInvestasiModel[size];
        }
    };

    public Double getPremi_pokok_di() {
        return premi_pokok_di;
    }

    public void setPremi_pokok_di(Double premi_pokok_di) {
        this.premi_pokok_di = premi_pokok_di;
    }

    public Double getPremi_berkala_di() {
        return premi_berkala_di;
    }

    public void setPremi_berkala_di(Double premi_berkala_di) {
        this.premi_berkala_di = premi_berkala_di;
    }

    public int getPremitopup_berkala_di() {
        return premitopup_berkala_di;
    }

    public void setPremitopup_berkala_di(int premitopup_berkala_di) {
        this.premitopup_berkala_di = premitopup_berkala_di;
    }

    public Double getPremi_tunggal_di() {
        return premi_tunggal_di;
    }

    public void setPremi_tunggal_di(Double premi_tunggal_di) {
        this.premi_tunggal_di = premi_tunggal_di;
    }

    public int getPremitopup_tunggal_di() {
        return premitopup_tunggal_di;
    }

    public void setPremitopup_tunggal_di(int premitopup_tunggal_di) {
        this.premitopup_tunggal_di = premitopup_tunggal_di;
    }

    public Double getPremi_tambahan_di() {
        return premi_tambahan_di;
    }

    public void setPremi_tambahan_di(Double premi_tambahan_di) {
        this.premi_tambahan_di = premi_tambahan_di;
    }

    public Double getJumlah_di() {
        return jumlah_di;
    }

    public void setJumlah_di(Double jumlah_di) {
        this.jumlah_di = jumlah_di;
    }

    public int getInvest_bayar_premi_di() {
        return invest_bayar_premi_di;
    }

    public void setInvest_bayar_premi_di(int invest_bayar_premi_di) {
        this.invest_bayar_premi_di = invest_bayar_premi_di;
    }

    public int getInvest_bank_di() {
        return invest_bank_di;
    }

    public void setInvest_bank_di(int invest_bank_di) {
        this.invest_bank_di = invest_bank_di;
    }

    public String getInvest_cabang_di() {
        return invest_cabang_di;
    }

    public void setInvest_cabang_di(String invest_cabang_di) {
        this.invest_cabang_di = invest_cabang_di;
    }

    public String getInvest_kota_di() {
        return invest_kota_di;
    }

    public void setInvest_kota_di(String invest_kota_di) {
        this.invest_kota_di = invest_kota_di;
    }

    public int getInvest_jnstab_di() {
        return invest_jnstab_di;
    }

    public void setInvest_jnstab_di(int invest_jnstab_di) {
        this.invest_jnstab_di = invest_jnstab_di;
    }

    public int getInvest_jnsnasabah_di() {
        return invest_jnsnasabah_di;
    }

    public void setInvest_jnsnasabah_di(int invest_jnsnasabah_di) {
        this.invest_jnsnasabah_di = invest_jnsnasabah_di;
    }

    public String getInvest_norek_di() {
        return invest_norek_di;
    }

    public void setInvest_norek_di(String invest_norek_di) {
        this.invest_norek_di = invest_norek_di;
    }

    public String getInvest_nama_di() {
        return invest_nama_di;
    }

    public void setInvest_nama_di(String invest_nama_di) {
        this.invest_nama_di = invest_nama_di;
    }

    public String getInvest_kurs_di() {
        return invest_kurs_di;
    }

    public void setInvest_kurs_di(String invest_kurs_di) {
        this.invest_kurs_di = invest_kurs_di;
    }

    public int getInvest_berikuasa_di() {
        return invest_berikuasa_di;
    }

    public void setInvest_berikuasa_di(int invest_berikuasa_di) {
        this.invest_berikuasa_di = invest_berikuasa_di;
    }

    public String getInvest_tglkuasa_di() {
        return invest_tglkuasa_di;
    }

    public void setInvest_tglkuasa_di(String invest_tglkuasa_di) {
        this.invest_tglkuasa_di = invest_tglkuasa_di;
    }

    public String getInvest_keterangan_di() {
        return invest_keterangan_di;
    }

    public void setInvest_keterangan_di(String invest_keterangan_di) {
        this.invest_keterangan_di = invest_keterangan_di;
    }

    public int getAutodbt_bank_di() {
        return autodbt_bank_di;
    }

    public void setAutodbt_bank_di(int autodbt_bank_di) {
        this.autodbt_bank_di = autodbt_bank_di;
    }

    public int getAutodbt_jnstab_di() {
        return autodbt_jnstab_di;
    }

    public void setAutodbt_jnstab_di(int autodbt_jnstab_di) {
        this.autodbt_jnstab_di = autodbt_jnstab_di;
    }

    public String getAutodbt_kurs_di() {
        return autodbt_kurs_di;
    }

    public void setAutodbt_kurs_di(String autodbt_kurs_di) {
        this.autodbt_kurs_di = autodbt_kurs_di;
    }

    public String getAutodbt_norek_di() {
        return autodbt_norek_di;
    }

    public void setAutodbt_norek_di(String autodbt_norek_di) {
        this.autodbt_norek_di = autodbt_norek_di;
    }

    public String getAutodbt_nama_di() {
        return autodbt_nama_di;
    }

    public void setAutodbt_nama_di(String autodbt_nama_di) {
        this.autodbt_nama_di = autodbt_nama_di;
    }

    public String getAutodbt_tgldebet_di() {
        return autodbt_tgldebet_di;
    }

    public void setAutodbt_tgldebet_di(String autodbt_tgldebet_di) {
        this.autodbt_tgldebet_di = autodbt_tgldebet_di;
    }

    public String getAutodbt_tglvalid_di() {
        return autodbt_tglvalid_di;
    }

    public void setAutodbt_tglvalid_di(String autodbt_tglvalid_di) {
        this.autodbt_tglvalid_di = autodbt_tglvalid_di;
    }

    public int getAutodbt_aktif_di() {
        return autodbt_aktif_di;
    }

    public void setAutodbt_aktif_di(int autodbt_aktif_di) {
        this.autodbt_aktif_di = autodbt_aktif_di;
    }

    public int getAutodbt_premipertama_di() {
        return autodbt_premipertama_di;
    }

    public void setAutodbt_premipertama_di(int autodbt_premipertama_di) {
        this.autodbt_premipertama_di = autodbt_premipertama_di;
    }

    public String getAutodbt_no_simascard_di() {
        return autodbt_no_simascard_di;
    }

    public void setAutodbt_no_simascard_di(String autodbt_no_simascard_di) {
        this.autodbt_no_simascard_di = autodbt_no_simascard_di;
    }

    public int getDanainvest_alokasi_di() {
        return danainvest_alokasi_di;
    }

    public void setDanainvest_alokasi_di(int danainvest_alokasi_di) {
        this.danainvest_alokasi_di = danainvest_alokasi_di;
    }

    public Double getJumlah1_di() {
        return jumlah1_di;
    }

    public void setJumlah1_di(Double jumlah1_di) {
        this.jumlah1_di = jumlah1_di;
    }

    public Double getPersen1_di() {
        return persen1_di;
    }

    public void setPersen1_di(Double persen1_di) {
        this.persen1_di = persen1_di;
    }

    public Double getJumlah2_di() {
        return jumlah2_di;
    }

    public void setJumlah2_di(Double jumlah2_di) {
        this.jumlah2_di = jumlah2_di;
    }

    public Double getPersen2_di() {
        return persen2_di;
    }

    public void setPersen2_di(Double persen2_di) {
        this.persen2_di = persen2_di;
    }

    public Double getJumlah3_di() {
        return jumlah3_di;
    }

    public void setJumlah3_di(Double jumlah3_di) {
        this.jumlah3_di = jumlah3_di;
    }

    public Double getPersen3_di() {
        return persen3_di;
    }

    public void setPersen3_di(Double persen3_di) {
        this.persen3_di = persen3_di;
    }

    public List<ModelAskesUA> getListModelDanaDI() {
        return ListModelDanaDI;
    }

    public void setListModelDanaDI(List<ModelAskesUA> listModelDanaDI) {
        ListModelDanaDI = listModelDanaDI;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public static Creator<DetilInvestasiModel> getCREATOR() {
        return CREATOR;
    }
}
