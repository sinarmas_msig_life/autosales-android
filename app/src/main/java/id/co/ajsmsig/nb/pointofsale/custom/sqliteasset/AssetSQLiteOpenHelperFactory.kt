package id.co.ajsmsig.nb.pointofsale.custom.sqliteasset

import android.arch.persistence.db.SupportSQLiteOpenHelper
import android.database.DatabaseErrorHandler
import android.util.Log

/**
 * Implements [SupportSQLiteOpenHelper.Factory] using the SQLite implementation in the
 * framework.
 */
class AssetSQLiteOpenHelperFactory : SupportSQLiteOpenHelper.Factory {
    override fun create(configuration: SupportSQLiteOpenHelper.Configuration): SupportSQLiteOpenHelper {
        val helper = AssetSQLiteOpenHelper(
                configuration.context, configuration.name!!, null, configuration.callback.version, DatabaseErrorHandler { Log.i("On Corruption", "Error") }, configuration.callback
        )
        return helper
    }
}
