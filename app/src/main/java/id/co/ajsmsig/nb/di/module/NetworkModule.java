package id.co.ajsmsig.nb.di.module;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import id.co.ajsmsig.nb.data.HttpInterceptor;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by fajarca on 12/18/17.
 */

@Module
public class NetworkModule {
    private String mBaseUrl;
    private Application application;

    public NetworkModule(String mBaseUrl, Application application) {
        this.mBaseUrl = mBaseUrl;
        this.application = application;
    }

    @Provides
    @Singleton



    Cache provideHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024;
        return new Cache(application.getCacheDir(), cacheSize);


    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Provides
    @Singleton
    HttpInterceptor provideHttpInterceptor() {
        return new HttpInterceptor(application);
    }

    @Provides
    @Singleton
    OkHttpClient provideHttpClient(Cache cache, HttpLoggingInterceptor loggingInterceptor, HttpInterceptor httpInterceptor) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(loggingInterceptor);
        client.addInterceptor(httpInterceptor);
        client.cache(cache);


        return client.build();
    }

    @Provides
    @Singleton

    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(mBaseUrl)
                .client(okHttpClient)
                .build();

    }
}
