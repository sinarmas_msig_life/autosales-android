
/**
 * Created by faiz_f on 08/04/2016.
 */


package id.co.ajsmsig.nb.prop.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

//import id.co.ajsmsig.nb.prop.fragment.P_HomeIllustrationFragment;


public class P_AdapterIlustrasiTab extends FragmentStatePagerAdapter {
//    private Fragment[] fragments;
    private List<Fragment> fragments;

    public P_AdapterIlustrasiTab(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
