package id.co.ajsmsig.nb.espaj.model.arraylist;

/**
 * Created by eriza on 14/11/2017.
 */

public class ModelReferralDa {
    private String NAMA_REFF = "";
    private String AGENT_CODE = "";
    private String NAMA_CABANG = "";
    private String LCB_NO = "";
    private Integer FLAG_LISENSI;

    public ModelReferralDa() {
    }

    public ModelReferralDa(String NAMA_REFF, String AGENT_CODE, String NAMA_CABANG, String LCB_NO, Integer FLAG_LISENSI) {
        this.NAMA_REFF = NAMA_REFF;
        this.AGENT_CODE = AGENT_CODE;
        this.NAMA_CABANG = NAMA_CABANG;
        this.LCB_NO = LCB_NO;
        this.FLAG_LISENSI = FLAG_LISENSI;
    }

    public String getNAMA_REFF() {
        return NAMA_REFF;
    }

    public void setNAMA_REFF(String NAMA_REFF) {
        this.NAMA_REFF = NAMA_REFF;
    }

    public String getAGENT_CODE() {
        return AGENT_CODE;
    }

    public void setAGENT_CODE(String AGENT_CODE) {
        this.AGENT_CODE = AGENT_CODE;
    }

    public String getNAMA_CABANG() {
        return NAMA_CABANG;
    }

    public void setNAMA_CABANG(String NAMA_CABANG) {
        this.NAMA_CABANG = NAMA_CABANG;
    }

    public String getLCB_NO() {
        return LCB_NO;
    }

    public void setLCB_NO(String LCB_NO) {
        this.LCB_NO = LCB_NO;
    }

    public String toString() {
        return NAMA_REFF;
    }

    public Integer getFLAG_LISENSI() {
        return FLAG_LISENSI;
    }

    public void setFLAG_LISENSI(Integer FLAG_LISENSI) {
        this.FLAG_LISENSI = FLAG_LISENSI;
    }
}
