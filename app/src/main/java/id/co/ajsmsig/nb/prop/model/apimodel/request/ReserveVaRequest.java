package id.co.ajsmsig.nb.prop.model.apimodel.request;

public class ReserveVaRequest {
    private String JUMLAH;
    private String LSBS_ID;
    private String LSBP_ID;
    private String MSAG_ID;

    public ReserveVaRequest() {
    }
    public String getJUMLAH() {
        return JUMLAH;
    }

    public void setJUMLAH(String JUMLAH) {
        this.JUMLAH = JUMLAH;
    }

    public String getLSBS_ID() {
        return LSBS_ID;
    }

    public void setLSBS_ID(String LSBS_ID) {
        this.LSBS_ID = LSBS_ID;
    }

    public String getLSBP_ID() {
        return LSBP_ID;
    }

    public void setLSBP_ID(String LSBP_ID) {
        this.LSBP_ID = LSBP_ID;
    }

    public String getMSAG_ID() {
        return MSAG_ID;
    }

    public void setMSAG_ID(String MSAG_ID) {
        this.MSAG_ID = MSAG_ID;
    }
}
