package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import java.util.ArrayList;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.DBHelper;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.prop.method.QueryUtil;

/**
 * Created by Envy on 10/6/2017.
 */

public class E_Update {
    private DBHelper dbHelper;
    private Context context;

    public E_Update(Context context) {
        this.context = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.update_data_preferences), Context.MODE_PRIVATE);
        int version = sharedPreferences.getInt(context.getString(R.string.lav_data_id), 0);
        this.dbHelper = new DBHelper(context, version);
    }

    public void UpdatePage(EspajModel me) {
        ContentValues cv = new ContentValues();
        QueryUtil queryUtil = new QueryUtil(context);
        String selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        String[] selectionArgs = new String[]{me.getModelID().getSPAJ_ID_TAB()};
        cv.put("PAGE_POSITION", me.getModelID().getPage_position());
        queryUtil.update(context.getString(R.string.TABLE_E_MST_SPAJ), cv, selection, selectionArgs);
    }

    public void UpdateSUBMIT(EspajModel me) {
        ContentValues cv = new ContentValues();
        QueryUtil queryUtil = new QueryUtil(context);
        String selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        String[] selectionArgs = new String[]{me.getModelID().getSPAJ_ID_TAB()};
        cv.put("SPAJ_ID", me.getModelID().getSPAJ_ID());
        cv.put("IDPROPOSAL", me.getModelID().getProposal());
        cv.put("FLAG_AKTIF", me.getModelID().getFlag_aktif());
        cv.put("FLAG_DOKUMEN", me.getModelID().getFlag_dokumen());
        cv.put("FLAG_PROPOSAL", me.getModelID().getFlag_proposal());
        cv.put("VIRTUAL_ACC", me.getModelID().getVa_number());
        queryUtil.update(context.getString(R.string.TABLE_E_MST_SPAJ), cv, selection, selectionArgs);
    }

    public void UpdateSertifikat(EspajModel me) {
        ContentValues cv = new ContentValues();
        QueryUtil queryUtil = new QueryUtil(context);
        String selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        String[] selectionArgs = new String[]{me.getModelID().getSPAJ_ID_TAB()};
        cv.put("SERTIFIKAT", me.getModelID().getSertifikat());
        cv.put("FLAG_SERTIFIKAT", me.getModelID().getFlag_sertifikat());
        cv.put("count_sertifikat", me.getModelID().getCount_sertifikat());
        queryUtil.update(context.getString(R.string.TABLE_E_MST_SPAJ), cv, selection, selectionArgs);
    }

    public void UpdateESPAJ(EspajModel espajModel) {
        ContentValues cv = new ContentValues();
        QueryUtil queryUtil = new QueryUtil(context);
        String selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        String[] selectionArgs = new String[]{espajModel.getModelID().getSPAJ_ID_TAB()};

        cv = new TableMST_SPAJ(context).getContentValues(espajModel);
        queryUtil.update(context.getString(R.string.TABLE_E_MST_SPAJ), cv, selection, selectionArgs);
        cv = new TablePemegangPolis(context).getContentValues(espajModel);
        queryUtil.update(context.getString(R.string.TABLE_E_PP), cv, selection, selectionArgs);
        cv = new TableTertanggung(context).getContentValues(espajModel);
        queryUtil.update(context.getString(R.string.TABLE_E_TT), cv, selection, selectionArgs);
        cv = new TableCalonPembayarPremi(context).getContentValues(espajModel);
        queryUtil.update(context.getString(R.string.TABLE_E_CP), cv, selection, selectionArgs);
        cv = new TableUsulanAsuransi(context).getContentValues(espajModel);
        queryUtil.update(context.getString(R.string.TABLE_E_UA), cv, selection, selectionArgs);
        cv = new TableDetilInvestasi(context).getContentValues(espajModel);
        queryUtil.update(context.getString(R.string.TABLE_E_DI), cv, selection, selectionArgs);
        cv = new TableDetilAgen(context).getContentValues(espajModel);
        queryUtil.update(context.getString(R.string.TABLE_E_DA), cv, selection, selectionArgs);
        cv = new TableProfileResiko(context).getContentValues(espajModel);
        queryUtil.update(context.getString(R.string.TABLE_E_PR), cv, selection, selectionArgs);

        //ArrayList
        selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        selectionArgs = new String[]{espajModel.getModelID().getSPAJ_ID_TAB()};
        queryUtil.delete(context.getString(R.string.TABLE_E_ASKES), selection, selectionArgs);
        if (!espajModel.getListASkesUA().isEmpty()) {
            for (int i = 0; i < espajModel.getListASkesUA().size(); i++) {
                cv = new TableAskesUA(context).getContentValues(espajModel.getListASkesUA().get(i), i + 1, espajModel.getModelID().getSPAJ_ID_TAB());
                queryUtil.insert(context.getString(R.string.TABLE_E_ASKES), cv);
            }
        }

        selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        selectionArgs = new String[]{espajModel.getModelID().getSPAJ_ID_TAB()};
        queryUtil.delete(context.getString(R.string.TABLE_E_RIDER), selection, selectionArgs);
        if (!espajModel.getListRiderUA().isEmpty()) {
            for (int i = 0; i < espajModel.getListRiderUA().size(); i++) {
                cv = new TableAddRiderUA(context).getContentValues(espajModel.getListRiderUA().get(i), i + 1, espajModel.getModelID().getSPAJ_ID_TAB(),
                        espajModel.getListRiderUA().get(i).getKode_produk_rider());
                queryUtil.insert(context.getString(R.string.TABLE_E_RIDER), cv);
            }
        }

        selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        selectionArgs = new String[]{espajModel.getModelID().getSPAJ_ID_TAB()};
        queryUtil.delete(context.getString(R.string.TABLE_E_NO9TTUQ), selection, selectionArgs);
        if (!espajModel.getModelUpdateQuisioner().getList_dkno9tt().isEmpty()) {
            for (int i = 0; i < espajModel.getModelUpdateQuisioner().getList_dkno9tt().size(); i++) {
                cv = new TableDKno9_tt(context).getContentValues(espajModel.getModelUpdateQuisioner().getList_dkno9tt().get(i), i + 1, espajModel.getModelID().getSPAJ_ID_TAB());
                queryUtil.insert(context.getString(R.string.TABLE_E_NO9TTUQ), cv);
            }
        }

        selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        selectionArgs = new String[]{espajModel.getModelID().getSPAJ_ID_TAB()};
        queryUtil.delete(context.getString(R.string.TABLE_E_NO9PPUQ), selection, selectionArgs);
        if (!espajModel.getModelUpdateQuisioner().getList_dkno9pp().isEmpty()) {
            for (int i = 0; i < espajModel.getModelUpdateQuisioner().getList_dkno9pp().size(); i++) {
                cv = new TableDKno9_pp(context).getContentValues(espajModel.getModelUpdateQuisioner().getList_dkno9pp().get(i), i + 1, espajModel.getModelID().getSPAJ_ID_TAB());
                queryUtil.insert(context.getString(R.string.TABLE_E_NO9PPUQ), cv);
            }
        }

        selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        selectionArgs = new String[]{espajModel.getModelID().getSPAJ_ID_TAB()};
        queryUtil.delete(context.getString(R.string.TABLE_E_DANA), selection, selectionArgs);
        if (!espajModel.getListDanaDI().isEmpty()) {
            for (int i = 0; i < espajModel.getListDanaDI().size(); i++) {
                cv = new TableJnsDanaDI(context).getContentValues(espajModel.getListDanaDI().get(i), i + 1, espajModel.getModelID().getSPAJ_ID_TAB());
                queryUtil.insert(context.getString(R.string.TABLE_E_DANA), cv);
            }
        }

        selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        selectionArgs = new String[]{espajModel.getModelID().getSPAJ_ID_TAB()};
        queryUtil.delete(context.getString(R.string.TABLE_E_MANFAAT), selection, selectionArgs);
        if (!espajModel.getListManfaatDI().isEmpty()) {
            for (int i = 0; i < espajModel.getListManfaatDI().size(); i++) {
                cv = new TableDataDitunjukDI(context).getContentValues(espajModel.getListManfaatDI().get(i), i + 1, espajModel.getModelID().getSPAJ_ID_TAB());
                queryUtil.insert(context.getString(R.string.TABLE_E_MANFAAT), cv);
            }
        }

        selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        selectionArgs = new String[]{espajModel.getModelID().getSPAJ_ID_TAB()};
        queryUtil.delete(context.getString(R.string.TABLE_E_FOTO), selection, selectionArgs);
        if (!espajModel.getDokumenPendukungModel().getListFoto().isEmpty()) {
            for (int i = 0; i < espajModel.getDokumenPendukungModel().getListFoto().size(); i++) {
                cv = new TableDP(context).getContentValues(espajModel.getDokumenPendukungModel().getListFoto().get(i), i + 1, espajModel.getModelID().getSPAJ_ID_TAB());
                queryUtil.insert(context.getString(R.string.TABLE_E_FOTO), cv);
            }
        }

        selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        selectionArgs = new String[]{espajModel.getModelID().getSPAJ_ID_TAB()};
        queryUtil.delete(context.getString(R.string.TABLE_E_TTD), selection, selectionArgs);
        if (!espajModel.getDokumenPendukungModel().getListTTD().isEmpty()) {
            for (int i = 0; i < espajModel.getDokumenPendukungModel().getListTTD().size(); i++) {
                cv = new TableTTD(context).getContentValues(espajModel.getDokumenPendukungModel().getListTTD().get(i), i + 1, espajModel.getModelID().getSPAJ_ID_TAB());
                queryUtil.insert(context.getString(R.string.TABLE_E_TTD), cv);
            }
        }

        selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        selectionArgs = new String[]{espajModel.getModelID().getSPAJ_ID_TAB()};
        queryUtil.delete(context.getString(R.string.TABLE_E_QA), selection, selectionArgs);
        if (!espajModel.getModelUpdateQuisioner().getListQuisioner().isEmpty()) {
            for (int i = 0; i < espajModel.getModelUpdateQuisioner().getListQuisioner().size(); i++) {
                cv = new TableMSTAnswer(context).getContentValues(espajModel.getModelUpdateQuisioner().getListQuisioner().get(i), i + 1, espajModel.getModelID().getSPAJ_ID_TAB());
                queryUtil.insert(context.getString(R.string.TABLE_E_QA), cv);
            }
        }
    }

    public void UpdateProfileResikoPOS(String no_proposal_tab, ArrayList<Integer> List_answer) {
        QueryUtil queryUtil = new QueryUtil(context);
        String selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        String[] selectionArgs = new String[]{no_proposal_tab};
        ContentValues cv = new TableProfileResiko(context).getContentValuesPOS(List_answer);
        queryUtil.update(context.getString(R.string.TABLE_E_PR), cv, selection, selectionArgs);
    }

    public void updateTransactionNumber(String spajIdTab, String paymentType, String transactionNumber) {
        if (!TextUtils.isEmpty(spajIdTab) && !TextUtils.isEmpty(paymentType)) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues contentValues = new ContentValues();

            contentValues.put("PAYMENT_TYPE", paymentType);
            contentValues.put("PAYMENT_CC_TX_ID", transactionNumber);

            db.update("E_MST_SPAJ", contentValues, "SPAJ_ID_TAB = ?", new String[]{spajIdTab});
        }
    }

    public void updatePaymentType(String spajIdTab, String paymentType) {
        if (!TextUtils.isEmpty(spajIdTab) && !TextUtils.isEmpty(paymentType)) {

            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put("PAYMENT_TYPE", paymentType);

            db.update("E_MST_SPAJ", contentValues, "SPAJ_ID_TAB = ?", new String[]{spajIdTab});
        }

    }
}
