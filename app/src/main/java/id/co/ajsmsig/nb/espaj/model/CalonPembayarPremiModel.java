package id.co.ajsmsig.nb.espaj.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 30/08/2017.
 */

public class CalonPembayarPremiModel implements Parcelable {

    //editText
    private String nama_perush_cp = "";
    private String almt_perush_cp = "";
    private String kota_perush_cp = "";
    private String kdpos_perush_cp = "";
    private String telp_perush_cp = "";
    private String nofax_perush_cp = "";
    private String bid_usaha_cp = "";
    private String editbisnis_cp = "";
    private String editpihak3_cp = "";
    private String edit_nmpihak3_cp = "";
    private String almt_phk3_cp = "";
    private String notlp_phk3_cp = "";
    private String notlp_kntrphk3_cp = "";
    private String edit_emailphk3_cp = "";
    private String tmpt_kedudukan_cp = "";
    private String bidusaha_phk3_cp = "";
    private String lain_jns_pkrjaan_cp = "";
    private String instansi_phk3_cp = "";
    private String npwp_phk3_cp = "";
    private String sumber_dana_phk3_cp = "";
    private String tujuan_phk3_cp = "";
    private String alasan_phk3_cp = "";
    private String dittd_phk3_cp = "";
    private String nama_ttdphk3_cp = "";
    private Boolean validation = false;

    //textview
    private String tgl_pendirian_cp = "";
    private String tgl_lhrphk3_cp = "";
    private String pdtgl_phk3_cp = "";
    //spinner
    int ket_cp = -1;
    int int_ket_cp = 0;
    int prov_perush_cp = -1;
    int int_propinsi_cp = 0;
    int spinbisnis_cp = -1;
    int int_spinbisnis_cp = 0;
    int spintotal_bln_cp = -1;
    String str_spintotal_bln_cp = "";
    int spintotal_thn_cp = -1;
    String str_spintotal_thn_cp = "";
    int spinpihak3_cp = 0;
    int int_spinpihak3_cp = 0;
    int spin_kewrgn_cp = 0;
    int int_spin_kewrgn_cp = -1;
    int spin_pekerjaan_cp = -1;
    String str_spin_pekerjaan_cp = "";
    int spin_jns_pkrjaan_cp = -1;
    String str_spin_jns_pkrjaan_cp = "";
    int spin_jabatanphk3_cp = -1;
    String str_spin_jabatanphk3_cp = "";
    int spin_hub_dgppphk3_cp = -1;
    int int_spin_hub_dgppphk3_cp = 0;

    int int_gaji_cp = 0;
    String str_gaji_cp = "";
    int int_penghsl_cp = 0;
    String str_penghsl_cp = "";
    int int_ortu_cp = 0;
    String str_ortu_cp = "";
    int int_laba_cp = 0;
    String str_laba_cp = "";
    int int_hslusaha_cp = 0;
    String str_hslusaha_cp = "";
    String edit_hslusaha_cp = "";
    int int_hslinves_cp = 0;
    String str_hslinves_cp = "";
    String edit_hslinves_cp = "";
    int int_lainnya_cp = 0;
    String str_lainnya_cp = "";
    String edit_lainnya_cp = "";
    int int_bonus_cp = 0;
    String str_bonus_cp = "";
    int int_komisi_cp = 0;
    String str_komisi_cp = "";
    int int_aset_cp = 0;
    String str_aset_cp = "";
    int int_hadiah_cp = 0;
    String str_hadiah_cp = "";
    int int_hslinves_thn_cp = 0;
    String str_hslinves_thn_cp = "";
    String edit_hslinves_thn_cp = "";
    int int_lainnya_thn_cp = 0;
    String str_lainnya_thn_cp = "";
    String edit_lainnya_thn_cp = "";
    int int_proteksi_cp = 0;
    String str_proteksi_cp = "";
    int int_pend_cp = 0;
    String str_pend_cp = "";
    int int_inves_cp = 0;
    String str_inves_cp = "";
    int int_tabungan_cp = 0;
    String str_tabungan_cp = "";
    int int_pensiun_cp = 0;
    String str_pensiun_cp = "";
    int int_lainnya_tujuan_cp = 0;
    String str_lainnya_tujuan_cp = "";
    String edit_lainnya_tujuan_cp = "";
    String tmpt_lhrphk3_cp = "";
    //Path Tanda Tangan
    String file_ttd_cp = "";

    public CalonPembayarPremiModel() {
    }

    protected CalonPembayarPremiModel(Parcel in) {
        nama_perush_cp = in.readString();
        almt_perush_cp = in.readString();
        kota_perush_cp = in.readString();
        kdpos_perush_cp = in.readString();
        telp_perush_cp = in.readString();
        nofax_perush_cp = in.readString();
        bid_usaha_cp = in.readString();
        editbisnis_cp = in.readString();
        editpihak3_cp = in.readString();
        edit_nmpihak3_cp = in.readString();
        almt_phk3_cp = in.readString();
        notlp_phk3_cp = in.readString();
        notlp_kntrphk3_cp = in.readString();
        edit_emailphk3_cp = in.readString();
        tmpt_kedudukan_cp = in.readString();
        bidusaha_phk3_cp = in.readString();
        lain_jns_pkrjaan_cp = in.readString();
        instansi_phk3_cp = in.readString();
        npwp_phk3_cp = in.readString();
        sumber_dana_phk3_cp = in.readString();
        tujuan_phk3_cp = in.readString();
        alasan_phk3_cp = in.readString();
        dittd_phk3_cp = in.readString();
        nama_ttdphk3_cp = in.readString();
        tgl_pendirian_cp = in.readString();
        tgl_lhrphk3_cp = in.readString();
        pdtgl_phk3_cp = in.readString();
        ket_cp = in.readInt();
        int_ket_cp = in.readInt();
        prov_perush_cp = in.readInt();
        int_propinsi_cp = in.readInt();
        spinbisnis_cp = in.readInt();
        int_spinbisnis_cp = in.readInt();
        spintotal_bln_cp = in.readInt();
        str_spintotal_bln_cp = in.readString();
        spintotal_thn_cp = in.readInt();
        str_spintotal_thn_cp = in.readString();
        spinpihak3_cp = in.readInt();
        int_spinpihak3_cp = in.readInt();
        spin_kewrgn_cp = in.readInt();
        int_spin_kewrgn_cp = in.readInt();
        spin_pekerjaan_cp = in.readInt();
        str_spin_pekerjaan_cp = in.readString();
        spin_jns_pkrjaan_cp = in.readInt();
        str_spin_jns_pkrjaan_cp = in.readString();
        spin_jabatanphk3_cp = in.readInt();
        str_spin_jabatanphk3_cp = in.readString();
        spin_hub_dgppphk3_cp = in.readInt();
        int_spin_hub_dgppphk3_cp = in.readInt();
        int_gaji_cp = in.readInt();
        str_gaji_cp = in.readString();
        int_penghsl_cp = in.readInt();
        str_penghsl_cp = in.readString();
        int_ortu_cp = in.readInt();
        str_ortu_cp = in.readString();
        int_laba_cp = in.readInt();
        str_laba_cp = in.readString();
        int_hslusaha_cp = in.readInt();
        str_hslusaha_cp = in.readString();
        edit_hslusaha_cp = in.readString();
        int_hslinves_cp = in.readInt();
        str_hslinves_cp = in.readString();
        edit_hslinves_cp = in.readString();
        int_lainnya_cp = in.readInt();
        str_lainnya_cp = in.readString();
        edit_lainnya_cp = in.readString();
        int_bonus_cp = in.readInt();
        str_bonus_cp = in.readString();
        int_komisi_cp = in.readInt();
        str_komisi_cp = in.readString();
        int_aset_cp = in.readInt();
        str_aset_cp = in.readString();
        int_hadiah_cp = in.readInt();
        str_hadiah_cp = in.readString();
        int_hslinves_thn_cp = in.readInt();
        str_hslinves_thn_cp = in.readString();
        edit_hslinves_thn_cp = in.readString();
        int_lainnya_thn_cp = in.readInt();
        str_lainnya_thn_cp = in.readString();
        edit_lainnya_thn_cp = in.readString();
        int_proteksi_cp = in.readInt();
        str_proteksi_cp = in.readString();
        int_pend_cp = in.readInt();
        str_pend_cp = in.readString();
        int_inves_cp = in.readInt();
        str_inves_cp = in.readString();
        int_tabungan_cp = in.readInt();
        str_tabungan_cp = in.readString();
        int_pensiun_cp = in.readInt();
        str_pensiun_cp = in.readString();
        int_lainnya_tujuan_cp = in.readInt();
        str_lainnya_tujuan_cp = in.readString();
        edit_lainnya_tujuan_cp = in.readString();
        tmpt_lhrphk3_cp = in.readString();
        file_ttd_cp = in.readString();
    }

    public CalonPembayarPremiModel(String nama_perush_cp, String almt_perush_cp, String kota_perush_cp, String kdpos_perush_cp, String telp_perush_cp, String nofax_perush_cp, String bid_usaha_cp, String editbisnis_cp, String editpihak3_cp, String edit_nmpihak3_cp, String almt_phk3_cp, String notlp_phk3_cp, String notlp_kntrphk3_cp, String edit_emailphk3_cp, String tmpt_kedudukan_cp, String bidusaha_phk3_cp, String lain_jns_pkrjaan_cp, String instansi_phk3_cp, String npwp_phk3_cp, String sumber_dana_phk3_cp, String tujuan_phk3_cp, String alasan_phk3_cp, String dittd_phk3_cp, String nama_ttdphk3_cp, Boolean validation, String tgl_pendirian_cp, String tgl_lhrphk3_cp, String pdtgl_phk3_cp, int ket_cp, int int_ket_cp, int prov_perush_cp, int int_propinsi_cp, int spinbisnis_cp, int int_spinbisnis_cp, int spintotal_bln_cp, String str_spintotal_bln_cp, int spintotal_thn_cp, String str_spintotal_thn_cp, int spinpihak3_cp, int int_spinpihak3_cp, int spin_kewrgn_cp, int int_spin_kewrgn_cp, int spin_pekerjaan_cp, String str_spin_pekerjaan_cp, int spin_jns_pkrjaan_cp, String str_spin_jns_pkrjaan_cp, int spin_jabatanphk3_cp, String str_spin_jabatanphk3_cp, int spin_hub_dgppphk3_cp, int int_spin_hub_dgppphk3_cp, int int_gaji_cp, String str_gaji_cp, int int_penghsl_cp, String str_penghsl_cp, int int_ortu_cp, String str_ortu_cp, int int_laba_cp, String str_laba_cp, int int_hslusaha_cp, String str_hslusaha_cp, String edit_hslusaha_cp, int int_hslinves_cp, String str_hslinves_cp, String edit_hslinves_cp, int int_lainnya_cp, String str_lainnya_cp, String edit_lainnya_cp, int int_bonus_cp, String str_bonus_cp, int int_komisi_cp, String str_komisi_cp, int int_aset_cp, String str_aset_cp, int int_hadiah_cp, String str_hadiah_cp, int int_hslinves_thn_cp, String str_hslinves_thn_cp, String edit_hslinves_thn_cp, int int_lainnya_thn_cp, String str_lainnya_thn_cp, String edit_lainnya_thn_cp, int int_proteksi_cp, String str_proteksi_cp, int int_pend_cp, String str_pend_cp, int int_inves_cp, String str_inves_cp, int int_tabungan_cp, String str_tabungan_cp, int int_pensiun_cp, String str_pensiun_cp, int int_lainnya_tujuan_cp, String str_lainnya_tujuan_cp, String edit_lainnya_tujuan_cp, String tmpt_lhrphk3_cp, String file_ttd_cp) {
        this.nama_perush_cp = nama_perush_cp;
        this.almt_perush_cp = almt_perush_cp;
        this.kota_perush_cp = kota_perush_cp;
        this.kdpos_perush_cp = kdpos_perush_cp;
        this.telp_perush_cp = telp_perush_cp;
        this.nofax_perush_cp = nofax_perush_cp;
        this.bid_usaha_cp = bid_usaha_cp;
        this.editbisnis_cp = editbisnis_cp;
        this.editpihak3_cp = editpihak3_cp;
        this.edit_nmpihak3_cp = edit_nmpihak3_cp;
        this.almt_phk3_cp = almt_phk3_cp;
        this.notlp_phk3_cp = notlp_phk3_cp;
        this.notlp_kntrphk3_cp = notlp_kntrphk3_cp;
        this.edit_emailphk3_cp = edit_emailphk3_cp;
        this.tmpt_kedudukan_cp = tmpt_kedudukan_cp;
        this.bidusaha_phk3_cp = bidusaha_phk3_cp;
        this.lain_jns_pkrjaan_cp = lain_jns_pkrjaan_cp;
        this.instansi_phk3_cp = instansi_phk3_cp;
        this.npwp_phk3_cp = npwp_phk3_cp;
        this.sumber_dana_phk3_cp = sumber_dana_phk3_cp;
        this.tujuan_phk3_cp = tujuan_phk3_cp;
        this.alasan_phk3_cp = alasan_phk3_cp;
        this.dittd_phk3_cp = dittd_phk3_cp;
        this.nama_ttdphk3_cp = nama_ttdphk3_cp;
        this.validation = validation;
        this.tgl_pendirian_cp = tgl_pendirian_cp;
        this.tgl_lhrphk3_cp = tgl_lhrphk3_cp;
        this.pdtgl_phk3_cp = pdtgl_phk3_cp;
        this.ket_cp = ket_cp;
        this.int_ket_cp = int_ket_cp;
        this.prov_perush_cp = prov_perush_cp;
        this.int_propinsi_cp = int_propinsi_cp;
        this.spinbisnis_cp = spinbisnis_cp;
        this.int_spinbisnis_cp = int_spinbisnis_cp;
        this.spintotal_bln_cp = spintotal_bln_cp;
        this.str_spintotal_bln_cp = str_spintotal_bln_cp;
        this.spintotal_thn_cp = spintotal_thn_cp;
        this.str_spintotal_thn_cp = str_spintotal_thn_cp;
        this.spinpihak3_cp = spinpihak3_cp;
        this.int_spinpihak3_cp = int_spinpihak3_cp;
        this.spin_kewrgn_cp = spin_kewrgn_cp;
        this.int_spin_kewrgn_cp = int_spin_kewrgn_cp;
        this.spin_pekerjaan_cp = spin_pekerjaan_cp;
        this.str_spin_pekerjaan_cp = str_spin_pekerjaan_cp;
        this.spin_jns_pkrjaan_cp = spin_jns_pkrjaan_cp;
        this.str_spin_jns_pkrjaan_cp = str_spin_jns_pkrjaan_cp;
        this.spin_jabatanphk3_cp = spin_jabatanphk3_cp;
        this.str_spin_jabatanphk3_cp = str_spin_jabatanphk3_cp;
        this.spin_hub_dgppphk3_cp = spin_hub_dgppphk3_cp;
        this.int_spin_hub_dgppphk3_cp = int_spin_hub_dgppphk3_cp;
        this.int_gaji_cp = int_gaji_cp;
        this.str_gaji_cp = str_gaji_cp;
        this.int_penghsl_cp = int_penghsl_cp;
        this.str_penghsl_cp = str_penghsl_cp;
        this.int_ortu_cp = int_ortu_cp;
        this.str_ortu_cp = str_ortu_cp;
        this.int_laba_cp = int_laba_cp;
        this.str_laba_cp = str_laba_cp;
        this.int_hslusaha_cp = int_hslusaha_cp;
        this.str_hslusaha_cp = str_hslusaha_cp;
        this.edit_hslusaha_cp = edit_hslusaha_cp;
        this.int_hslinves_cp = int_hslinves_cp;
        this.str_hslinves_cp = str_hslinves_cp;
        this.edit_hslinves_cp = edit_hslinves_cp;
        this.int_lainnya_cp = int_lainnya_cp;
        this.str_lainnya_cp = str_lainnya_cp;
        this.edit_lainnya_cp = edit_lainnya_cp;
        this.int_bonus_cp = int_bonus_cp;
        this.str_bonus_cp = str_bonus_cp;
        this.int_komisi_cp = int_komisi_cp;
        this.str_komisi_cp = str_komisi_cp;
        this.int_aset_cp = int_aset_cp;
        this.str_aset_cp = str_aset_cp;
        this.int_hadiah_cp = int_hadiah_cp;
        this.str_hadiah_cp = str_hadiah_cp;
        this.int_hslinves_thn_cp = int_hslinves_thn_cp;
        this.str_hslinves_thn_cp = str_hslinves_thn_cp;
        this.edit_hslinves_thn_cp = edit_hslinves_thn_cp;
        this.int_lainnya_thn_cp = int_lainnya_thn_cp;
        this.str_lainnya_thn_cp = str_lainnya_thn_cp;
        this.edit_lainnya_thn_cp = edit_lainnya_thn_cp;
        this.int_proteksi_cp = int_proteksi_cp;
        this.str_proteksi_cp = str_proteksi_cp;
        this.int_pend_cp = int_pend_cp;
        this.str_pend_cp = str_pend_cp;
        this.int_inves_cp = int_inves_cp;
        this.str_inves_cp = str_inves_cp;
        this.int_tabungan_cp = int_tabungan_cp;
        this.str_tabungan_cp = str_tabungan_cp;
        this.int_pensiun_cp = int_pensiun_cp;
        this.str_pensiun_cp = str_pensiun_cp;
        this.int_lainnya_tujuan_cp = int_lainnya_tujuan_cp;
        this.str_lainnya_tujuan_cp = str_lainnya_tujuan_cp;
        this.edit_lainnya_tujuan_cp = edit_lainnya_tujuan_cp;
        this.tmpt_lhrphk3_cp = tmpt_lhrphk3_cp;
        this.file_ttd_cp = file_ttd_cp;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nama_perush_cp);
        dest.writeString(almt_perush_cp);
        dest.writeString(kota_perush_cp);
        dest.writeString(kdpos_perush_cp);
        dest.writeString(telp_perush_cp);
        dest.writeString(nofax_perush_cp);
        dest.writeString(bid_usaha_cp);
        dest.writeString(editbisnis_cp);
        dest.writeString(editpihak3_cp);
        dest.writeString(edit_nmpihak3_cp);
        dest.writeString(almt_phk3_cp);
        dest.writeString(notlp_phk3_cp);
        dest.writeString(notlp_kntrphk3_cp);
        dest.writeString(edit_emailphk3_cp);
        dest.writeString(tmpt_kedudukan_cp);
        dest.writeString(bidusaha_phk3_cp);
        dest.writeString(lain_jns_pkrjaan_cp);
        dest.writeString(instansi_phk3_cp);
        dest.writeString(npwp_phk3_cp);
        dest.writeString(sumber_dana_phk3_cp);
        dest.writeString(tujuan_phk3_cp);
        dest.writeString(alasan_phk3_cp);
        dest.writeString(dittd_phk3_cp);
        dest.writeString(nama_ttdphk3_cp);
        dest.writeString(tgl_pendirian_cp);
        dest.writeString(tgl_lhrphk3_cp);
        dest.writeString(pdtgl_phk3_cp);
        dest.writeInt(ket_cp);
        dest.writeInt(int_ket_cp);
        dest.writeInt(prov_perush_cp);
        dest.writeInt(int_propinsi_cp);
        dest.writeInt(spinbisnis_cp);
        dest.writeInt(int_spinbisnis_cp);
        dest.writeInt(spintotal_bln_cp);
        dest.writeString(str_spintotal_bln_cp);
        dest.writeInt(spintotal_thn_cp);
        dest.writeString(str_spintotal_thn_cp);
        dest.writeInt(spinpihak3_cp);
        dest.writeInt(int_spinpihak3_cp);
        dest.writeInt(spin_kewrgn_cp);
        dest.writeInt(int_spin_kewrgn_cp);
        dest.writeInt(spin_pekerjaan_cp);
        dest.writeString(str_spin_pekerjaan_cp);
        dest.writeInt(spin_jns_pkrjaan_cp);
        dest.writeString(str_spin_jns_pkrjaan_cp);
        dest.writeInt(spin_jabatanphk3_cp);
        dest.writeString(str_spin_jabatanphk3_cp);
        dest.writeInt(spin_hub_dgppphk3_cp);
        dest.writeInt(int_spin_hub_dgppphk3_cp);
        dest.writeInt(int_gaji_cp);
        dest.writeString(str_gaji_cp);
        dest.writeInt(int_penghsl_cp);
        dest.writeString(str_penghsl_cp);
        dest.writeInt(int_ortu_cp);
        dest.writeString(str_ortu_cp);
        dest.writeInt(int_laba_cp);
        dest.writeString(str_laba_cp);
        dest.writeInt(int_hslusaha_cp);
        dest.writeString(str_hslusaha_cp);
        dest.writeString(edit_hslusaha_cp);
        dest.writeInt(int_hslinves_cp);
        dest.writeString(str_hslinves_cp);
        dest.writeString(edit_hslinves_cp);
        dest.writeInt(int_lainnya_cp);
        dest.writeString(str_lainnya_cp);
        dest.writeString(edit_lainnya_cp);
        dest.writeInt(int_bonus_cp);
        dest.writeString(str_bonus_cp);
        dest.writeInt(int_komisi_cp);
        dest.writeString(str_komisi_cp);
        dest.writeInt(int_aset_cp);
        dest.writeString(str_aset_cp);
        dest.writeInt(int_hadiah_cp);
        dest.writeString(str_hadiah_cp);
        dest.writeInt(int_hslinves_thn_cp);
        dest.writeString(str_hslinves_thn_cp);
        dest.writeString(edit_hslinves_thn_cp);
        dest.writeInt(int_lainnya_thn_cp);
        dest.writeString(str_lainnya_thn_cp);
        dest.writeString(edit_lainnya_thn_cp);
        dest.writeInt(int_proteksi_cp);
        dest.writeString(str_proteksi_cp);
        dest.writeInt(int_pend_cp);
        dest.writeString(str_pend_cp);
        dest.writeInt(int_inves_cp);
        dest.writeString(str_inves_cp);
        dest.writeInt(int_tabungan_cp);
        dest.writeString(str_tabungan_cp);
        dest.writeInt(int_pensiun_cp);
        dest.writeString(str_pensiun_cp);
        dest.writeInt(int_lainnya_tujuan_cp);
        dest.writeString(str_lainnya_tujuan_cp);
        dest.writeString(edit_lainnya_tujuan_cp);
        dest.writeString(tmpt_lhrphk3_cp);
        dest.writeString(file_ttd_cp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CalonPembayarPremiModel> CREATOR = new Creator<CalonPembayarPremiModel>() {
        @Override
        public CalonPembayarPremiModel createFromParcel(Parcel in) {
            return new CalonPembayarPremiModel(in);
        }

        @Override
        public CalonPembayarPremiModel[] newArray(int size) {
            return new CalonPembayarPremiModel[size];
        }
    };

    public String getNama_perush_cp() {
        return nama_perush_cp;
    }

    public void setNama_perush_cp(String nama_perush_cp) {
        this.nama_perush_cp = nama_perush_cp;
    }

    public String getAlmt_perush_cp() {
        return almt_perush_cp;
    }

    public void setAlmt_perush_cp(String almt_perush_cp) {
        this.almt_perush_cp = almt_perush_cp;
    }

    public String getKota_perush_cp() {
        return kota_perush_cp;
    }

    public void setKota_perush_cp(String kota_perush_cp) {
        this.kota_perush_cp = kota_perush_cp;
    }

    public String getKdpos_perush_cp() {
        return kdpos_perush_cp;
    }

    public void setKdpos_perush_cp(String kdpos_perush_cp) {
        this.kdpos_perush_cp = kdpos_perush_cp;
    }

    public String getTelp_perush_cp() {
        return telp_perush_cp;
    }

    public void setTelp_perush_cp(String telp_perush_cp) {
        this.telp_perush_cp = telp_perush_cp;
    }

    public String getNofax_perush_cp() {
        return nofax_perush_cp;
    }

    public void setNofax_perush_cp(String nofax_perush_cp) {
        this.nofax_perush_cp = nofax_perush_cp;
    }

    public String getBid_usaha_cp() {
        return bid_usaha_cp;
    }

    public void setBid_usaha_cp(String bid_usaha_cp) {
        this.bid_usaha_cp = bid_usaha_cp;
    }

    public String getEditbisnis_cp() {
        return editbisnis_cp;
    }

    public void setEditbisnis_cp(String editbisnis_cp) {
        this.editbisnis_cp = editbisnis_cp;
    }

    public String getEditpihak3_cp() {
        return editpihak3_cp;
    }

    public void setEditpihak3_cp(String editpihak3_cp) {
        this.editpihak3_cp = editpihak3_cp;
    }

    public String getEdit_nmpihak3_cp() {
        return edit_nmpihak3_cp;
    }

    public void setEdit_nmpihak3_cp(String edit_nmpihak3_cp) {
        this.edit_nmpihak3_cp = edit_nmpihak3_cp;
    }

    public String getAlmt_phk3_cp() {
        return almt_phk3_cp;
    }

    public void setAlmt_phk3_cp(String almt_phk3_cp) {
        this.almt_phk3_cp = almt_phk3_cp;
    }

    public String getNotlp_phk3_cp() {
        return notlp_phk3_cp;
    }

    public void setNotlp_phk3_cp(String notlp_phk3_cp) {
        this.notlp_phk3_cp = notlp_phk3_cp;
    }

    public String getNotlp_kntrphk3_cp() {
        return notlp_kntrphk3_cp;
    }

    public void setNotlp_kntrphk3_cp(String notlp_kntrphk3_cp) {
        this.notlp_kntrphk3_cp = notlp_kntrphk3_cp;
    }

    public String getEdit_emailphk3_cp() {
        return edit_emailphk3_cp;
    }

    public void setEdit_emailphk3_cp(String edit_emailphk3_cp) {
        this.edit_emailphk3_cp = edit_emailphk3_cp;
    }

    public String getTmpt_kedudukan_cp() {
        return tmpt_kedudukan_cp;
    }

    public void setTmpt_kedudukan_cp(String tmpt_kedudukan_cp) {
        this.tmpt_kedudukan_cp = tmpt_kedudukan_cp;
    }

    public String getBidusaha_phk3_cp() {
        return bidusaha_phk3_cp;
    }

    public void setBidusaha_phk3_cp(String bidusaha_phk3_cp) {
        this.bidusaha_phk3_cp = bidusaha_phk3_cp;
    }

    public String getLain_jns_pkrjaan_cp() {
        return lain_jns_pkrjaan_cp;
    }

    public void setLain_jns_pkrjaan_cp(String lain_jns_pkrjaan_cp) {
        this.lain_jns_pkrjaan_cp = lain_jns_pkrjaan_cp;
    }

    public String getInstansi_phk3_cp() {
        return instansi_phk3_cp;
    }

    public void setInstansi_phk3_cp(String instansi_phk3_cp) {
        this.instansi_phk3_cp = instansi_phk3_cp;
    }

    public String getNpwp_phk3_cp() {
        return npwp_phk3_cp;
    }

    public void setNpwp_phk3_cp(String npwp_phk3_cp) {
        this.npwp_phk3_cp = npwp_phk3_cp;
    }

    public String getSumber_dana_phk3_cp() {
        return sumber_dana_phk3_cp;
    }

    public void setSumber_dana_phk3_cp(String sumber_dana_phk3_cp) {
        this.sumber_dana_phk3_cp = sumber_dana_phk3_cp;
    }

    public String getTujuan_phk3_cp() {
        return tujuan_phk3_cp;
    }

    public void setTujuan_phk3_cp(String tujuan_phk3_cp) {
        this.tujuan_phk3_cp = tujuan_phk3_cp;
    }

    public String getAlasan_phk3_cp() {
        return alasan_phk3_cp;
    }

    public void setAlasan_phk3_cp(String alasan_phk3_cp) {
        this.alasan_phk3_cp = alasan_phk3_cp;
    }

    public String getDittd_phk3_cp() {
        return dittd_phk3_cp;
    }

    public void setDittd_phk3_cp(String dittd_phk3_cp) {
        this.dittd_phk3_cp = dittd_phk3_cp;
    }

    public String getNama_ttdphk3_cp() {
        return nama_ttdphk3_cp;
    }

    public void setNama_ttdphk3_cp(String nama_ttdphk3_cp) {
        this.nama_ttdphk3_cp = nama_ttdphk3_cp;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public String getTgl_pendirian_cp() {
        return tgl_pendirian_cp;
    }

    public void setTgl_pendirian_cp(String tgl_pendirian_cp) {
        this.tgl_pendirian_cp = tgl_pendirian_cp;
    }

    public String getTgl_lhrphk3_cp() {
        return tgl_lhrphk3_cp;
    }

    public void setTgl_lhrphk3_cp(String tgl_lhrphk3_cp) {
        this.tgl_lhrphk3_cp = tgl_lhrphk3_cp;
    }

    public String getPdtgl_phk3_cp() {
        return pdtgl_phk3_cp;
    }

    public void setPdtgl_phk3_cp(String pdtgl_phk3_cp) {
        this.pdtgl_phk3_cp = pdtgl_phk3_cp;
    }

    public int getKet_cp() {
        return ket_cp;
    }

    public void setKet_cp(int ket_cp) {
        this.ket_cp = ket_cp;
    }

    public int getInt_ket_cp() {
        return int_ket_cp;
    }

    public void setInt_ket_cp(int int_ket_cp) {
        this.int_ket_cp = int_ket_cp;
    }

    public int getProv_perush_cp() {
        return prov_perush_cp;
    }

    public void setProv_perush_cp(int prov_perush_cp) {
        this.prov_perush_cp = prov_perush_cp;
    }

    public int getInt_propinsi_cp() {
        return int_propinsi_cp;
    }

    public void setInt_propinsi_cp(int int_propinsi_cp) {
        this.int_propinsi_cp = int_propinsi_cp;
    }

    public int getSpinbisnis_cp() {
        return spinbisnis_cp;
    }

    public void setSpinbisnis_cp(int spinbisnis_cp) {
        this.spinbisnis_cp = spinbisnis_cp;
    }

    public int getInt_spinbisnis_cp() {
        return int_spinbisnis_cp;
    }

    public void setInt_spinbisnis_cp(int int_spinbisnis_cp) {
        this.int_spinbisnis_cp = int_spinbisnis_cp;
    }

    public int getSpintotal_bln_cp() {
        return spintotal_bln_cp;
    }

    public void setSpintotal_bln_cp(int spintotal_bln_cp) {
        this.spintotal_bln_cp = spintotal_bln_cp;
    }

    public String getStr_spintotal_bln_cp() {
        return str_spintotal_bln_cp;
    }

    public void setStr_spintotal_bln_cp(String str_spintotal_bln_cp) {
        this.str_spintotal_bln_cp = str_spintotal_bln_cp;
    }

    public int getSpintotal_thn_cp() {
        return spintotal_thn_cp;
    }

    public void setSpintotal_thn_cp(int spintotal_thn_cp) {
        this.spintotal_thn_cp = spintotal_thn_cp;
    }

    public String getStr_spintotal_thn_cp() {
        return str_spintotal_thn_cp;
    }

    public void setStr_spintotal_thn_cp(String str_spintotal_thn_cp) {
        this.str_spintotal_thn_cp = str_spintotal_thn_cp;
    }

    public int getSpinpihak3_cp() {
        return spinpihak3_cp;
    }

    public void setSpinpihak3_cp(int spinpihak3_cp) {
        this.spinpihak3_cp = spinpihak3_cp;
    }

    public int getInt_spinpihak3_cp() {
        return int_spinpihak3_cp;
    }

    public void setInt_spinpihak3_cp(int int_spinpihak3_cp) {
        this.int_spinpihak3_cp = int_spinpihak3_cp;
    }

    public int getSpin_kewrgn_cp() {
        return spin_kewrgn_cp;
    }

    public void setSpin_kewrgn_cp(int spin_kewrgn_cp) {
        this.spin_kewrgn_cp = spin_kewrgn_cp;
    }

    public int getInt_spin_kewrgn_cp() {
        return int_spin_kewrgn_cp;
    }

    public void setInt_spin_kewrgn_cp(int int_spin_kewrgn_cp) {
        this.int_spin_kewrgn_cp = int_spin_kewrgn_cp;
    }

    public int getSpin_pekerjaan_cp() {
        return spin_pekerjaan_cp;
    }

    public void setSpin_pekerjaan_cp(int spin_pekerjaan_cp) {
        this.spin_pekerjaan_cp = spin_pekerjaan_cp;
    }

    public String getStr_spin_pekerjaan_cp() {
        return str_spin_pekerjaan_cp;
    }

    public void setStr_spin_pekerjaan_cp(String str_spin_pekerjaan_cp) {
        this.str_spin_pekerjaan_cp = str_spin_pekerjaan_cp;
    }

    public int getSpin_jns_pkrjaan_cp() {
        return spin_jns_pkrjaan_cp;
    }

    public void setSpin_jns_pkrjaan_cp(int spin_jns_pkrjaan_cp) {
        this.spin_jns_pkrjaan_cp = spin_jns_pkrjaan_cp;
    }

    public String getStr_spin_jns_pkrjaan_cp() {
        return str_spin_jns_pkrjaan_cp;
    }

    public void setStr_spin_jns_pkrjaan_cp(String str_spin_jns_pkrjaan_cp) {
        this.str_spin_jns_pkrjaan_cp = str_spin_jns_pkrjaan_cp;
    }

    public int getSpin_jabatanphk3_cp() {
        return spin_jabatanphk3_cp;
    }

    public void setSpin_jabatanphk3_cp(int spin_jabatanphk3_cp) {
        this.spin_jabatanphk3_cp = spin_jabatanphk3_cp;
    }

    public String getStr_spin_jabatanphk3_cp() {
        return str_spin_jabatanphk3_cp;
    }

    public void setStr_spin_jabatanphk3_cp(String str_spin_jabatanphk3_cp) {
        this.str_spin_jabatanphk3_cp = str_spin_jabatanphk3_cp;
    }

    public int getSpin_hub_dgppphk3_cp() {
        return spin_hub_dgppphk3_cp;
    }

    public void setSpin_hub_dgppphk3_cp(int spin_hub_dgppphk3_cp) {
        this.spin_hub_dgppphk3_cp = spin_hub_dgppphk3_cp;
    }

    public int getInt_spin_hub_dgppphk3_cp() {
        return int_spin_hub_dgppphk3_cp;
    }

    public void setInt_spin_hub_dgppphk3_cp(int int_spin_hub_dgppphk3_cp) {
        this.int_spin_hub_dgppphk3_cp = int_spin_hub_dgppphk3_cp;
    }

    public int getInt_gaji_cp() {
        return int_gaji_cp;
    }

    public void setInt_gaji_cp(int int_gaji_cp) {
        this.int_gaji_cp = int_gaji_cp;
    }

    public String getStr_gaji_cp() {
        return str_gaji_cp;
    }

    public void setStr_gaji_cp(String str_gaji_cp) {
        this.str_gaji_cp = str_gaji_cp;
    }

    public int getInt_penghsl_cp() {
        return int_penghsl_cp;
    }

    public void setInt_penghsl_cp(int int_penghsl_cp) {
        this.int_penghsl_cp = int_penghsl_cp;
    }

    public String getStr_penghsl_cp() {
        return str_penghsl_cp;
    }

    public void setStr_penghsl_cp(String str_penghsl_cp) {
        this.str_penghsl_cp = str_penghsl_cp;
    }

    public int getInt_ortu_cp() {
        return int_ortu_cp;
    }

    public void setInt_ortu_cp(int int_ortu_cp) {
        this.int_ortu_cp = int_ortu_cp;
    }

    public String getStr_ortu_cp() {
        return str_ortu_cp;
    }

    public void setStr_ortu_cp(String str_ortu_cp) {
        this.str_ortu_cp = str_ortu_cp;
    }

    public int getInt_laba_cp() {
        return int_laba_cp;
    }

    public void setInt_laba_cp(int int_laba_cp) {
        this.int_laba_cp = int_laba_cp;
    }

    public String getStr_laba_cp() {
        return str_laba_cp;
    }

    public void setStr_laba_cp(String str_laba_cp) {
        this.str_laba_cp = str_laba_cp;
    }

    public int getInt_hslusaha_cp() {
        return int_hslusaha_cp;
    }

    public void setInt_hslusaha_cp(int int_hslusaha_cp) {
        this.int_hslusaha_cp = int_hslusaha_cp;
    }

    public String getStr_hslusaha_cp() {
        return str_hslusaha_cp;
    }

    public void setStr_hslusaha_cp(String str_hslusaha_cp) {
        this.str_hslusaha_cp = str_hslusaha_cp;
    }

    public String getEdit_hslusaha_cp() {
        return edit_hslusaha_cp;
    }

    public void setEdit_hslusaha_cp(String edit_hslusaha_cp) {
        this.edit_hslusaha_cp = edit_hslusaha_cp;
    }

    public int getInt_hslinves_cp() {
        return int_hslinves_cp;
    }

    public void setInt_hslinves_cp(int int_hslinves_cp) {
        this.int_hslinves_cp = int_hslinves_cp;
    }

    public String getStr_hslinves_cp() {
        return str_hslinves_cp;
    }

    public void setStr_hslinves_cp(String str_hslinves_cp) {
        this.str_hslinves_cp = str_hslinves_cp;
    }

    public String getEdit_hslinves_cp() {
        return edit_hslinves_cp;
    }

    public void setEdit_hslinves_cp(String edit_hslinves_cp) {
        this.edit_hslinves_cp = edit_hslinves_cp;
    }

    public int getInt_lainnya_cp() {
        return int_lainnya_cp;
    }

    public void setInt_lainnya_cp(int int_lainnya_cp) {
        this.int_lainnya_cp = int_lainnya_cp;
    }

    public String getStr_lainnya_cp() {
        return str_lainnya_cp;
    }

    public void setStr_lainnya_cp(String str_lainnya_cp) {
        this.str_lainnya_cp = str_lainnya_cp;
    }

    public String getEdit_lainnya_cp() {
        return edit_lainnya_cp;
    }

    public void setEdit_lainnya_cp(String edit_lainnya_cp) {
        this.edit_lainnya_cp = edit_lainnya_cp;
    }

    public int getInt_bonus_cp() {
        return int_bonus_cp;
    }

    public void setInt_bonus_cp(int int_bonus_cp) {
        this.int_bonus_cp = int_bonus_cp;
    }

    public String getStr_bonus_cp() {
        return str_bonus_cp;
    }

    public void setStr_bonus_cp(String str_bonus_cp) {
        this.str_bonus_cp = str_bonus_cp;
    }

    public int getInt_komisi_cp() {
        return int_komisi_cp;
    }

    public void setInt_komisi_cp(int int_komisi_cp) {
        this.int_komisi_cp = int_komisi_cp;
    }

    public String getStr_komisi_cp() {
        return str_komisi_cp;
    }

    public void setStr_komisi_cp(String str_komisi_cp) {
        this.str_komisi_cp = str_komisi_cp;
    }

    public int getInt_aset_cp() {
        return int_aset_cp;
    }

    public void setInt_aset_cp(int int_aset_cp) {
        this.int_aset_cp = int_aset_cp;
    }

    public String getStr_aset_cp() {
        return str_aset_cp;
    }

    public void setStr_aset_cp(String str_aset_cp) {
        this.str_aset_cp = str_aset_cp;
    }

    public int getInt_hadiah_cp() {
        return int_hadiah_cp;
    }

    public void setInt_hadiah_cp(int int_hadiah_cp) {
        this.int_hadiah_cp = int_hadiah_cp;
    }

    public String getStr_hadiah_cp() {
        return str_hadiah_cp;
    }

    public void setStr_hadiah_cp(String str_hadiah_cp) {
        this.str_hadiah_cp = str_hadiah_cp;
    }

    public int getInt_hslinves_thn_cp() {
        return int_hslinves_thn_cp;
    }

    public void setInt_hslinves_thn_cp(int int_hslinves_thn_cp) {
        this.int_hslinves_thn_cp = int_hslinves_thn_cp;
    }

    public String getStr_hslinves_thn_cp() {
        return str_hslinves_thn_cp;
    }

    public void setStr_hslinves_thn_cp(String str_hslinves_thn_cp) {
        this.str_hslinves_thn_cp = str_hslinves_thn_cp;
    }

    public String getEdit_hslinves_thn_cp() {
        return edit_hslinves_thn_cp;
    }

    public void setEdit_hslinves_thn_cp(String edit_hslinves_thn_cp) {
        this.edit_hslinves_thn_cp = edit_hslinves_thn_cp;
    }

    public int getInt_lainnya_thn_cp() {
        return int_lainnya_thn_cp;
    }

    public void setInt_lainnya_thn_cp(int int_lainnya_thn_cp) {
        this.int_lainnya_thn_cp = int_lainnya_thn_cp;
    }

    public String getStr_lainnya_thn_cp() {
        return str_lainnya_thn_cp;
    }

    public void setStr_lainnya_thn_cp(String str_lainnya_thn_cp) {
        this.str_lainnya_thn_cp = str_lainnya_thn_cp;
    }

    public String getEdit_lainnya_thn_cp() {
        return edit_lainnya_thn_cp;
    }

    public void setEdit_lainnya_thn_cp(String edit_lainnya_thn_cp) {
        this.edit_lainnya_thn_cp = edit_lainnya_thn_cp;
    }

    public int getInt_proteksi_cp() {
        return int_proteksi_cp;
    }

    public void setInt_proteksi_cp(int int_proteksi_cp) {
        this.int_proteksi_cp = int_proteksi_cp;
    }

    public String getStr_proteksi_cp() {
        return str_proteksi_cp;
    }

    public void setStr_proteksi_cp(String str_proteksi_cp) {
        this.str_proteksi_cp = str_proteksi_cp;
    }

    public int getInt_pend_cp() {
        return int_pend_cp;
    }

    public void setInt_pend_cp(int int_pend_cp) {
        this.int_pend_cp = int_pend_cp;
    }

    public String getStr_pend_cp() {
        return str_pend_cp;
    }

    public void setStr_pend_cp(String str_pend_cp) {
        this.str_pend_cp = str_pend_cp;
    }

    public int getInt_inves_cp() {
        return int_inves_cp;
    }

    public void setInt_inves_cp(int int_inves_cp) {
        this.int_inves_cp = int_inves_cp;
    }

    public String getStr_inves_cp() {
        return str_inves_cp;
    }

    public void setStr_inves_cp(String str_inves_cp) {
        this.str_inves_cp = str_inves_cp;
    }

    public int getInt_tabungan_cp() {
        return int_tabungan_cp;
    }

    public void setInt_tabungan_cp(int int_tabungan_cp) {
        this.int_tabungan_cp = int_tabungan_cp;
    }

    public String getStr_tabungan_cp() {
        return str_tabungan_cp;
    }

    public void setStr_tabungan_cp(String str_tabungan_cp) {
        this.str_tabungan_cp = str_tabungan_cp;
    }

    public int getInt_pensiun_cp() {
        return int_pensiun_cp;
    }

    public void setInt_pensiun_cp(int int_pensiun_cp) {
        this.int_pensiun_cp = int_pensiun_cp;
    }

    public String getStr_pensiun_cp() {
        return str_pensiun_cp;
    }

    public void setStr_pensiun_cp(String str_pensiun_cp) {
        this.str_pensiun_cp = str_pensiun_cp;
    }

    public int getInt_lainnya_tujuan_cp() {
        return int_lainnya_tujuan_cp;
    }

    public void setInt_lainnya_tujuan_cp(int int_lainnya_tujuan_cp) {
        this.int_lainnya_tujuan_cp = int_lainnya_tujuan_cp;
    }

    public String getStr_lainnya_tujuan_cp() {
        return str_lainnya_tujuan_cp;
    }

    public void setStr_lainnya_tujuan_cp(String str_lainnya_tujuan_cp) {
        this.str_lainnya_tujuan_cp = str_lainnya_tujuan_cp;
    }

    public String getEdit_lainnya_tujuan_cp() {
        return edit_lainnya_tujuan_cp;
    }

    public void setEdit_lainnya_tujuan_cp(String edit_lainnya_tujuan_cp) {
        this.edit_lainnya_tujuan_cp = edit_lainnya_tujuan_cp;
    }

    public String getTmpt_lhrphk3_cp() {
        return tmpt_lhrphk3_cp;
    }

    public void setTmpt_lhrphk3_cp(String tmpt_lhrphk3_cp) {
        this.tmpt_lhrphk3_cp = tmpt_lhrphk3_cp;
    }

    public String getFile_ttd_cp() {
        return file_ttd_cp;
    }

    public void setFile_ttd_cp(String file_ttd_cp) {
        this.file_ttd_cp = file_ttd_cp;
    }

    public static Creator<CalonPembayarPremiModel> getCREATOR() {
        return CREATOR;
    }
}

