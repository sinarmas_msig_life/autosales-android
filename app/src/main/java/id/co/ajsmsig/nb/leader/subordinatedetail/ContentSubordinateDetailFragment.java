package id.co.ajsmsig.nb.leader.subordinatedetail;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.DashboardResponse;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.leader.statusspaj.StatusSpajActivity;
import id.co.ajsmsig.nb.leader.subordinate.DataSubordinate;
import id.co.ajsmsig.nb.leader.subordinate.GetSubordinateResponse;
import id.co.ajsmsig.nb.leader.subordinate.Subordinate;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContentSubordinateDetailFragment extends Fragment implements SubordinateDetailAdapter.OnItemPressed {

    private ArrayList<Subordinate> subordinateList = new ArrayList<>();
    private SubordinateDetailAdapter mAdapter;
    private String agentCode;
    private String agentName;
    private String keyChannel;

    @BindView(R.id.tvLastMonthActivity)
    TextView tvLastMonthActivity;
    @BindView(R.id.tvCurrentMonthActivity)
    TextView tvCurrentMonthActivity;
    @BindView(R.id.tvTodayActivity)
    TextView tvTodayActivity;

    @BindView(R.id.tvLastMonthClosing)
    TextView tvLastMonthClosing;
    @BindView(R.id.tvCurrentMonthClosing)
    TextView tvCurrentMonthClosing;
    @BindView(R.id.tvTodayClosing)
    TextView tvTodayClosing;

    @BindView(R.id.tvSubmit)
    TextView tvSubmit;
    @BindView(R.id.tvOnProcess)
    TextView tvOnProcess;
    @BindView(R.id.tvCancel)
    TextView tvCancel;
    @BindView(R.id.tvPending)
    TextView tvPending;
    @BindView(R.id.tvInforce)
    TextView tvInforce;

//    @BindView(R.id.tvSubordinateName)
//    TextView tvSubordinateName;

//    @BindView(R.id.contentReportSpajDetail)
//    ScrollView contentReportSpajDetail;

//    @BindView(R.id.layoutNoInternetConnection)
//    ConstraintLayout layoutNoInternetConnection;

    @BindView(R.id.tvDetailAgentReport)
    TextView tvDetailAgentReport;

//    @BindView(R.id.toolbar)
//    Toolbar toolbar;
//
//    @BindView(R.id.progressBar)
//    ProgressBar progressBar;

    @BindView(R.id.rvSummaryData)
    RecyclerView recyclerView;

//    @BindView(R.id.tvAgentInitialNameDetail)
//    TextView tvAgentInitialNameDetail;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_content_subordinate_detail, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        Intent intent = getActivity().getIntent();
        agentCode = intent.getStringExtra(Const.INTENT_KEY_AGENT_CODE);
        keyChannel = intent.getStringExtra(Const.INTENT_KEY_CHANNEL);

        agentName = intent.getStringExtra(Const.INTENT_KEY_AGENT_NAME);
        String keyChannel = intent.getStringExtra(Const.INTENT_KEY_CHANNEL);

        initRecyclerView();
        initView();
        if (StaticMethods.isNetworkAvailable(getActivity())) {
            hideNoInternetConnection();
            getListSubordinate(agentCode, keyChannel);
            getSpajCountFrom(agentCode);

        } else {
            hideLoading();
            showNoInternetConnection();
            hideSubordianteDetailLayout();
        }


    }
    private void initRecyclerView() {
        mAdapter = new SubordinateDetailAdapter(subordinateList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ContentSubordinateDetailFragment.this.getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    private void initView() {

        TextView nbrSubmit = getView().findViewById(R.id.tvSubmit);
        nbrSubmit.setOnClickListener(v -> launchStatusListActivityWithMode(Const.ALL_SUBMIT, agentCode));

        TextView buttonAllSubmit = getView().findViewById(R.id.tv_submit);
        buttonAllSubmit.setOnClickListener(v -> launchStatusListActivityWithMode(Const.ALL_SUBMIT, agentCode));

        TextView nbrProcess = getView().findViewById(R.id.tvOnProcess);
        nbrProcess.setOnClickListener(v -> launchStatusListActivityWithMode(Const.ON_PROCESS, agentCode));

        TextView buttonProcess = getView().findViewById(R.id.on_process);
        buttonProcess.setOnClickListener(v -> launchStatusListActivityWithMode(Const.ON_PROCESS, agentCode));

        TextView nbrCancelled = getView().findViewById(R.id.tvCancel);
        nbrCancelled.setOnClickListener(v -> launchStatusListActivityWithMode(Const.CANCELLED, agentCode));

        TextView buttonCancelled = getView().findViewById(R.id.tvCancelled);
        buttonCancelled.setOnClickListener(v -> launchStatusListActivityWithMode(Const.CANCELLED, agentCode));

        TextView nbrPending = getView().findViewById(R.id.tvPending);
        nbrPending.setOnClickListener((View v) -> launchStatusListActivityWithMode(Const.PENDING, agentCode));

        TextView buttonPending = getView().findViewById(R.id.pending);
        buttonPending.setOnClickListener(v -> launchStatusListActivityWithMode(Const.PENDING, agentCode));

        TextView nbrInforce = getView().findViewById(R.id.tvInforce);
        nbrInforce.setOnClickListener(v -> launchStatusListActivityWithMode(Const.INFORCE, agentCode));

        TextView buttonInforce = getView().findViewById(R.id.inforce);
        buttonInforce.setOnClickListener(v -> launchStatusListActivityWithMode(Const.INFORCE, agentCode));
    }

    private void launchStatusListActivityWithMode(int modeId, String msagId) {
        Intent intent = new Intent(getActivity(), StatusSpajActivity.class);
        intent.putExtra(Const.INTENT_KEY_REPORT_MODE, modeId);
        intent.putExtra(Const.INTENT_KEY_AGENT_CODE, msagId);
        startActivity(intent);
    }

    private void getListSubordinate(String agentCode, String keyChannel) {
        showLoading();
        hideRecyclerView();
        hideSubordianteDetailLayout();
        String url = AppConfig.getBaseUrlWs().concat("member/api/json/getsubordinate");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<GetSubordinateResponse> call = api.getSubordinate(url, agentCode, keyChannel);
        call.enqueue(new Callback<GetSubordinateResponse>() {
            @Override
            public void onResponse(Call<GetSubordinateResponse> call, Response<GetSubordinateResponse> response) {
                GetSubordinateResponse responseBody = response.body();
                if (responseBody != null) {

                    boolean error = responseBody.isError();

                    String message = responseBody.getMessage();
                    DataSubordinate data = responseBody.getDataSubordinate();
                    ArrayList<Subordinate> subordinates = (ArrayList<Subordinate>) data.getSubordinate();

                    if (!error) {
                        displaySubordinateData(subordinates);
                    } else {
                        Toast.makeText(getActivity(), "Terjadi kesalahan " + message, Toast.LENGTH_SHORT).show();
                    }

                }
                hideLoading();
                showRecyclerView();
                showSubordiateDetailLayout();
            }

            @Override
            public void onFailure(Call<GetSubordinateResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Tidak dapat menampilkan subordinate " + t.getMessage(), Toast.LENGTH_SHORT).show();
//                hideSubordianteDetailLayout();
                hideLoading();
                hideRecyclerView();
            }
        });
    }

    private void displaySubordinateData(ArrayList<Subordinate> subordinates) {
        for (int i = 0; i < subordinates.size(); i++) {
            Subordinate subordinate = subordinates.get(i);
            String agentName = subordinate.getNAMAREFF();
            String initialName = getAgentInitialName(agentName);
            subordinate.setInitialName(initialName);
        }
        //Ada list bawahan
        if (subordinates.size() > 0) {
            mAdapter.refreshSubordinate(subordinates);
            showUnderBC(agentName);
            getAgentActivityCount(agentCode, subordinates);
        } else {
            hideUnderBC();
            getAgentActivityCount(agentCode, subordinates);
        }
    }

    private void getSpajCountFrom(String agentCode) {
        showLoading();
        hideSubordianteDetailLayout();
        String url = AppConfig.getBaseUrlSPAJ().concat("spaj/api/json/getallspajcount/" + agentCode);
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<GetAllSpajCountResponse> call = api.getAllSpajCount(url);
        call.enqueue(new Callback<GetAllSpajCountResponse>() {
            @Override
            public void onResponse(Call<GetAllSpajCountResponse> call, Response<GetAllSpajCountResponse> response) {
                GetAllSpajCountResponse responsebody = response.body();
                if (responsebody != null) {
                    boolean error = responsebody.isError();
                    String message = responsebody.getMessage();
                    Data data = responsebody.getData();

                    if (!error && data != null) {
                        String submit = data.getTOTALENTRY();
                        String onProcess = data.getONPROSES();
                        String canceled = data.getCANCELED();
                        String pending = data.getFURTHER();
                        String inforce = data.getDONE();

                        tvSubmit.setText(convertNullToZero(submit));
                        tvOnProcess.setText(convertNullToZero(onProcess));
                        tvCancel.setText(convertNullToZero(canceled));
                        tvPending.setText(convertNullToZero(pending));
                        tvInforce.setText(convertNullToZero(inforce));

                    }
                }
                hideLoading();
                showSubordiateDetailLayout();
            }

            @Override
            public void onFailure(Call<GetAllSpajCountResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "periksa kembali koneksi anda" + t.getMessage(), Toast.LENGTH_SHORT).show();
                hideSubordianteDetailLayout();
                //onBtnRetryPressed();
                hideLoading();
            }
        });

    }

    private void getAgentActivityCount(String msagId, ArrayList<Subordinate> subordinates) {
        showLoading();
        hideSubordianteDetailLayout();
        String url = AppConfig.getBaseUrlCRM().concat("v2/index.php/toll/getagentactivity");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<DashboardResponse> call = api.getDashboardInfo(url, msagId);
        call.enqueue(new Callback<DashboardResponse>() {
            @Override
            public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                DashboardResponse responsebody = response.body();
                if (responsebody != null) {
                    boolean error = responsebody.isError();
                    String message = responsebody.getMessage();
                    id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.Data data = responsebody.getData();
                    if (!error) {
                        processData(data, subordinates);
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                }
                showSubordiateDetailLayout();
                hideLoading();
            }

            @Override
            public void onFailure(Call<DashboardResponse> call, Throwable t) {
                hideLoading();
                //onBtnRetryPressed();
                hideSubordianteDetailLayout();
            }
        });

    }

    private void processData(id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.Data data, ArrayList<Subordinate> dataSubordinates) {


        if (dataSubordinates.size() > 0) {

            //Activity meet and pre
            Integer todayActivity = data.getLeader().getMeetandpre().getToday();
            Integer lastMonthActivity = data.getLeader().getMeetandpre().getLastMonth();
            Integer currentMonthActivity = data.getLeader().getMeetandpre().getCurrentMonth();

            //Closing
            Integer todayClosing = data.getLeader().getClosing().getToday();
            Integer lastMonthClosing = data.getLeader().getClosing().getLastMonth();
            Integer currentMonthClosing = data.getLeader().getClosing().getCurrentMonth();

            tvTodayActivity.setText(convertNullToZero(todayActivity));
            tvLastMonthActivity.setText(convertNullToZero(lastMonthActivity));
            tvCurrentMonthActivity.setText(convertNullToZero(currentMonthActivity));

            tvTodayClosing.setText(convertNullToZero(todayClosing));
            tvLastMonthClosing.setText(convertNullToZero(lastMonthClosing));
            tvCurrentMonthClosing.setText(convertNullToZero(currentMonthClosing));

        } else {

            //Activity meet and pre
            Integer todayActivity = data.getBancass().getMeetandpre().getToday();
            Integer lastMonthActivity = data.getBancass().getMeetandpre().getLastMonth();
            Integer currentMonthActivity = data.getBancass().getMeetandpre().getCurrentMonth();

            //Closing
            Integer todayClosing = data.getBancass().getClosing().getToday();
            Integer lastMonthClosing = data.getBancass().getClosing().getLastMonth();
            Integer currentMonthClosing = data.getBancass().getClosing().getCurrentMonth();

            tvTodayActivity.setText(convertNullToZero(todayActivity));
            tvLastMonthActivity.setText(convertNullToZero(lastMonthActivity));
            tvCurrentMonthActivity.setText(convertNullToZero(currentMonthActivity));

            tvTodayClosing.setText(convertNullToZero(todayClosing));
            tvLastMonthClosing.setText(convertNullToZero(lastMonthClosing));
            tvCurrentMonthClosing.setText(convertNullToZero(currentMonthClosing));

        }
    }

    private String getAgentInitialName(String agentName) {

        if (!TextUtils.isEmpty(agentName)) {
            String[] splitByWhiteSpace = agentName.split("\\s+");

            if (splitByWhiteSpace != null && splitByWhiteSpace.length > 0) {

                if (splitByWhiteSpace.length == 3) {

                    //Name contains first, middle, last name
                    if (!TextUtils.isEmpty(splitByWhiteSpace[0]) && !TextUtils.isEmpty(splitByWhiteSpace[2])) {
                        String agentNameFirstNameInitial = splitByWhiteSpace[0];
                        agentNameFirstNameInitial = String.valueOf(agentNameFirstNameInitial.charAt(0)).toUpperCase();

                        String agentNameLastNameInital = splitByWhiteSpace[2];
                        agentNameLastNameInital = String.valueOf(agentNameLastNameInital.charAt(0)).toUpperCase();

                        String firstLastNameInitial = agentNameFirstNameInitial + agentNameLastNameInital;

                        return firstLastNameInitial;
                    }


                } else if (splitByWhiteSpace.length == 2) {

                    if (!TextUtils.isEmpty(splitByWhiteSpace[0]) && !TextUtils.isEmpty(splitByWhiteSpace[1])) {
                        //Name contains first, middle name only
                        String agentNameFirstNameInitial = splitByWhiteSpace[0];
                        agentNameFirstNameInitial = String.valueOf(agentNameFirstNameInitial.charAt(0)).toUpperCase();

                        String agentNameLastNameInital = splitByWhiteSpace[1];
                        agentNameLastNameInital = String.valueOf(agentNameLastNameInital.charAt(0)).toUpperCase();

                        String firstLastNameInitial = agentNameFirstNameInitial + agentNameLastNameInital;

                        return firstLastNameInitial;

                    }


                } else if (splitByWhiteSpace.length == 1) {

                    if (!TextUtils.isEmpty(splitByWhiteSpace[0])) {
                        //Name contains first name only
                        String agentNameFirstNameInitial = splitByWhiteSpace[0];
                        agentNameFirstNameInitial = String.valueOf(agentNameFirstNameInitial.charAt(0)).toUpperCase();

                        String agentNameLastNameInital = "";

                        String firstLastNameInitial = agentNameFirstNameInitial + agentNameLastNameInital;

                        return firstLastNameInitial;
                    }

                }
            }

        }

        return "";
    }

    @Override
    public void onSubordinatePressed(int position, Subordinate subordinate) {
        Intent intent = new Intent(getActivity(), SubordinateDetail.class);
        String msagId = subordinate.getMSAGID();
        String agentName = subordinate.getNAMAREFF();
        String channelId = subordinate.getJENIS();
        intent.putExtra(Const.INTENT_KEY_AGENT_CODE, msagId);
        intent.putExtra(Const.INTENT_KEY_AGENT_NAME, agentName);
        intent.putExtra(Const.INTENT_KEY_CHANNEL, channelId);
        startActivity(intent);

    }

    private void hideUnderBC() {
        tvDetailAgentReport.setVisibility(View.GONE);
    }

    private void showUnderBC(String agentName) {
        tvDetailAgentReport.setText("UNDER " + agentName);
        tvDetailAgentReport.setVisibility(View.VISIBLE);
    }

    private String convertNullToZero(Integer activityCount) {
        if (activityCount == null) {
            return 0 + "";
        }
        return String.valueOf(activityCount);
    }

    private String convertNullToZero(String activityCount) {
        if (activityCount == null) {
            return 0 + "";
        }
        return activityCount;
    }

    private void showLoading() {
      //  progressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
     //   progressBar.setVisibility(View.GONE);
    }

    private void showRecyclerView() {
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void hideRecyclerView() {
        recyclerView.setVisibility(View.GONE);
    }

    private void showSubordiateDetailLayout() {
      //  contentReportSpajDetail.setVisibility(View.VISIBLE);
    }

    private void hideSubordianteDetailLayout() {
        //contentReportSpajDetail.setVisibility(View.GONE);
    }


    private void showNoInternetConnection() {
       // layoutNoInternetConnection.setVisibility(View.VISIBLE);
    }

    private void hideNoInternetConnection() {
       // layoutNoInternetConnection.setVisibility(View.GONE);
    }
}
