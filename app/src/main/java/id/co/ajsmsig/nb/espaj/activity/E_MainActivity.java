package id.co.ajsmsig.nb.espaj.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.C_Insert;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.method.async.CrmRestClientUsage;
import id.co.ajsmsig.nb.crm.method.async.SpajApiRestClientUsage;
import id.co.ajsmsig.nb.crm.method.async.WsApiRestClientUsage;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.BANKCABANG;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.BANKPUSAT;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.BankCabangResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.BankPusatResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.ReferralResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.SertifikatResponse;
import id.co.ajsmsig.nb.crm.model.form.SimbakActivityModel;
import id.co.ajsmsig.nb.crm.model.hardcode.RequestCode;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.espaj.database.E_Delete;
import id.co.ajsmsig.nb.espaj.database.E_Insert;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.database.E_Update;
import id.co.ajsmsig.nb.espaj.database.TableLST_Bank;
import id.co.ajsmsig.nb.espaj.database.TableLST_Reff;
import id.co.ajsmsig.nb.espaj.fragment.E_CalonPembayarPremiFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_DetilAgenFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_DetilInvestasiFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_DokumenPendukungFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_KonfirmasiDanKirimFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_PemegangPolisFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_ProfileResikoFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_QuestionnaireFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_TertanggungFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_UpdateQuisionerSIOFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_UsulanAsuransiFragment;
import id.co.ajsmsig.nb.espaj.method.GetTime;
import id.co.ajsmsig.nb.espaj.method.JSONToString;
import id.co.ajsmsig.nb.espaj.method.MethodModel;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.apimodel.request.PayViaCcRequest;
import id.co.ajsmsig.nb.espaj.model.apimodel.response.PayViaCcResponse;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBank;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBankCab;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelReferralDa;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.model.Model;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.DateUtils;
import id.co.ajsmsig.nb.util.MessageEvent;
import id.co.ajsmsig.nb.util.PaymentEvent;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eriza on 21/07/2017.
 */

public class E_MainActivity extends AppCompatActivity implements View.OnClickListener, SpajApiRestClientUsage.SertifikatListener, WsApiRestClientUsage.ReferralListener,
        SpajApiRestClientUsage.getVaListener, SpajApiRestClientUsage.generateCertificateListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.second_toolbar)
    Toolbar second_toolbar;
    @BindView(R.id.btn_lanjut)
    Button btn_lanjut;
    @BindView(R.id.btn_kembali)
    Button btn_kembali;
    private List<ModelBank> ListBank = new ArrayList<>();
    private List<ModelBankCab> ListBankCab = new ArrayList<>();
    private List<ModelReferralDa> ListReferral = new ArrayList<>();
    private TextView tv_toolbar_title;
    private View view1, view2, view3, view4, view5, view6, view7, view8, view9, view10;
    private ImageView iv_save, iv_next, iv_prev;
    private String TAG = getClass().getSimpleName();
    private Fragment pemegangPolisFragment;
    private Fragment tertanggungFragment;
    private Fragment calonPembayarPremiFragment;
    private Fragment usulanAsuransiFragment;
    private Fragment detilInvestasiFragment;
    private Fragment detilAgenFragment;
    private Fragment profileResikoFragment;
    private Fragment dokumenPendukungFragment;
    private Fragment questionnaireFragment;
    private Fragment konfirmasiDanKirimFragment;

    public static Bundle e_bundle;
    private EspajModel espajModel;

    private String SPAJ_ID_TAB = "";
    private String idproposal = "";
    private String idproposal_tab = "";
    private String noid = "";
    private String kodeAgen = "";
    private String lca_id;
    private String lwk_id;
    private String lsrg_id;
    private String KODE_REGIONAL;
    private String EMAIL;
    private int JENIS_LOGIN = 0;
    private int JENIS_LOGIN_BC = 0;
    private int GROUP_ID = 0;
    private ProgressDialog generateCertificatePDialog;
    private SpajApiRestClientUsage ewsRestClientUsage;
    private WsApiRestClientUsage wsApiRestClientUsage;
    private ProgressDialog progressDialog;
    private boolean isVaExist;
    private String vaNumber;
    private String ppName;
    private Long slTabId;
    private boolean shouldHideTarikVa;
    private final int KUNJUNGAN_DAN_PRESENTASI_ID = 51;
    private final int NASABAH_TERTARIK_BUAT_PROPOSAL_ID = 47;


    private final String SUCCESS_CODE = "00";
    private final String PAID = "1";
    private final String WAITING_FOR_PAYMENT = "0";
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.e_activity_main);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            SPAJ_ID_TAB = extras.getString(getString(R.string.SPAJ_ID_TAB), "");
            idproposal = extras.getString(getString(R.string.idproposal), "");
            idproposal_tab = extras.getString(getString(R.string.idproposal_tab), "");
            noid = extras.getString(getString(R.string.noid_member), ""); // untuk id member siap2u
            isVaExist = extras.getBoolean(Const.INTENT_KEY_VA_EXIST, false);
            vaNumber = extras.getString(Const.INTENT_KEY_VA_NUMBER);
            ppName = extras.getString(Const.INTENT_KEY_PP_NAME);
            slTabId = extras.getLong(Const.INTENT_KEY_LEAD_TAB_ID);
            shouldHideTarikVa = extras.getBoolean(Const.INTENT_KEY_SHOULD_HIDE_TARIK_VA_MENU);
        }

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
        lca_id = sharedPreferences.getString("KODE_REG1", "");
        lwk_id = sharedPreferences.getString("KODE_REG2", "");
        lsrg_id = sharedPreferences.getString("KODE_REG3", "");
        KODE_REGIONAL = sharedPreferences.getString("KODE_REGIONAL", "");
        JENIS_LOGIN_BC = sharedPreferences.getInt("JENIS_LOGIN_BC", 0);
        JENIS_LOGIN = sharedPreferences.getInt("JENIS_LOGIN", 0);
        EMAIL = sharedPreferences.getString("EMAIL", "");
        GROUP_ID = sharedPreferences.getInt("GROUP_ID", 0);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        tv_toolbar_title = second_toolbar.findViewById(R.id.tv_toolbar_title);
        view1 = second_toolbar.findViewById(R.id.view1);
        view2 = second_toolbar.findViewById(R.id.view2);
        view3 = second_toolbar.findViewById(R.id.view3);
        view4 = second_toolbar.findViewById(R.id.view4);
        view5 = second_toolbar.findViewById(R.id.view5);
        view6 = second_toolbar.findViewById(R.id.view6);
        view7 = second_toolbar.findViewById(R.id.view7);
        view8 = second_toolbar.findViewById(R.id.view8);
        view9 = second_toolbar.findViewById(R.id.view9);
        view10 = second_toolbar.findViewById(R.id.view10);

        setBundle(SPAJ_ID_TAB, idproposal_tab);

        if (savedInstanceState == null) {
            setFragment(new E_PemegangPolisFragment(), getResources().getString(R.string.pemegang_polis));
        }

        wsApiRestClientUsage = new WsApiRestClientUsage(this);
        wsApiRestClientUsage.setonReferralListener(this);

        ewsRestClientUsage = new SpajApiRestClientUsage(this);
        ewsRestClientUsage.setonGetVaListener(this);
        ewsRestClientUsage.setonSertifikatListener(this);
        ewsRestClientUsage.setOnGenerateCertificateListener(this);
        if (JENIS_LOGIN == 9) {
            if (new E_Select(this).getCountReferral() == 0) {
                getReferral();
            }
        }

        E_Select select = new E_Select(this);
        String noProposalTab = espajModel.getModelID().getProposal_tab();
        int lastOpenedPagePosition = select.getLastOpenedPagePositionFromSPAJ(noProposalTab);
        switch (lastOpenedPagePosition) {
            case 1:
                setFragment(new E_PemegangPolisFragment(), getResources().getString(R.string.pemegang_polis));
                break;
            case 2:
                setFragment(new E_TertanggungFragment(), getResources().getString(R.string.tertanggung));
                break;
            case 3:
                setFragment(new E_CalonPembayarPremiFragment(), getResources().getString(R.string.calon_pembayar_premi));
                break;
            case 4:
                setFragment(new E_UsulanAsuransiFragment(), getResources().getString(R.string.usulan_asuransi));
                break;
            case 5:
                setFragment(new E_DetilInvestasiFragment(), getResources().getString(R.string.detil_investasi));
                break;
            case 6:
                setFragment(new E_DetilAgenFragment(), getResources().getString(R.string.detil_agen));
                break;
            case 7:
                setFragment(new E_DokumenPendukungFragment(), getResources().getString(R.string.dokumen_pendukung));
                break;
            case 8:
                setFragment(new E_QuestionnaireFragment(), getResources().getString(R.string.questionnaire));
                break;
            case 9:
                setFragment(new E_UpdateQuisionerSIOFragment(), getResources().getString(R.string.questionnaire));
                break;
            case 10:
                setFragment(new E_ProfileResikoFragment(), getResources().getString(R.string.profil));
                break;
            case 11:
                setFragment(new E_KonfirmasiDanKirimFragment(), getResources().getString(R.string.konfirmasi_dan_kirim));
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_espaj, menu);
        MenuItem menu_va = menu.findItem(R.id.menu_va);
        MenuItem menu_sertifikat = menu.findItem(R.id.menu_sertifikat);
        MenuItem menu_referral = menu.findItem(R.id.menu_referral);
        MenuItem menu_spaj = menu.findItem(R.id.menu_spaj);
        MenuItem menuShowVa = menu.findItem(R.id.menuShowVa);
        MenuItem menu_validasi = menu.findItem(R.id.menu_validasi);
        MenuItem menu_exit = menu.findItem(R.id.menu_exit);
        MenuItem menu_rekening = menu.findItem(R.id.menu_rekening);
        MenuItem menu_cc_payment = menu.findItem(R.id.menu_cc_payment);

        menu_spaj.setVisible(true);

        menu_va.setVisible(false);

        int flag_va = new E_Select(this).getFlagVa(JENIS_LOGIN, JENIS_LOGIN_BC, lca_id, lwk_id);
        if (flag_va == 1) {
            menu_va.setVisible(true);
        } else {
            menu_va.setVisible(false);
        }

        if (espajModel.getDetilAgenModel().getJENIS_LOGIN_BC() == 58 || espajModel.getDetilAgenModel().getJENIS_LOGIN_BC() == 51 || espajModel.getDetilAgenModel().getJENIS_LOGIN_BC() == 50 ||
                espajModel.getDetilAgenModel().getJENIS_LOGIN_BC() == 60 || espajModel.getDetilAgenModel().getJENIS_LOGIN_BC() == 44){
            menu_rekening.setVisible(true);
        }else {
            menu_rekening.setVisible(false);
        }
        
        if (espajModel.getDetilAgenModel().getJENIS_LOGIN() == 9) {
            if (espajModel.getDetilAgenModel().getJENIS_LOGIN_BC() == 50
                    && espajModel.getUsulanAsuransiModel().getKd_produk_ua() == 212 || espajModel.getDetilAgenModel().getJENIS_LOGIN_BC() == 60 && espajModel.getUsulanAsuransiModel().getKd_produk_ua() == 223) {
                menu_sertifikat.setVisible(true);
            } else {
                menu_sertifikat.setVisible(false);
            }
            menu_referral.setVisible(true);
        } else if (espajModel.getDetilAgenModel().getJENIS_LOGIN() == 2) {
            menu_referral.setVisible(false);
            menu_sertifikat.setVisible(false);
        }


        //If virtual account is exist, hide the "tarik virtual account" options menu
        if (isVaExist) {
            menu_va.setVisible(false);
            menuShowVa.setVisible(true);
        } else {
            //Show only options menu if Siap2U, BSIM and BTNS
            if (shouldShowTarikVirtualAccountOptionsMenu()) menu_va.setVisible(true);
            else menu_va.setVisible(false);

            menuShowVa.setVisible(false);
        }


        //If user navigate from proposal to ESPAJ
        if (shouldHideTarikVa) {
            menu_va.setVisible(false);
        }

        //If user navigate from SPAJ tab to SPAJ or if user navigate from lead SPAJ to SPAJ
        if (shouldHideTarikVaMenuOnEspaj()) {
            menu_va.setVisible(false);
        }

        if (JENIS_LOGIN == Const.SIAP2U_LOGIN_TYPE) {
            if (GROUP_ID == 40) {
                menu_cc_payment.setVisible(false); // karena cc masih belum siap
            }
        } else {
            menu_cc_payment.setVisible(false);
        }


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bundle bundle;
        switch (item.getItemId()) {
            case R.id.menu_va:
                if (espajModel.getModelID().getSPAJ_ID_TAB().equals("")) {
                    MethodSupport.Alert(this, getString(R.string.title_error), getString(R.string.msg_save3), 0);
                } else {
                    if (espajModel.getModelID().getVa_number() == null || espajModel.getModelID().getVa_number().equals("")) {
                        if (StaticMethods.isNetworkAvailable(this)) {
                            getVirtualAcc();
                        } else {
                            MethodSupport.Alert(this, getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi), 0);
                        }
                    } else {
                        if (espajModel.getUsulanAsuransiModel().getPaket_ua() == 35) {
                            MethodSupport.Alert(this, getString(R.string.title_va), getString(R.string.msg_va) + espajModel.getModelID().getVa_number()
                                    + "\n" + "Total Premi" + " : " + StaticMethods.toMoney(espajModel.getUsulanAsuransiModel().getUnitlink_total_ua()) +
                                    "\n" + "Nama Member" + " : " + espajModel.getPemegangPolisModel().getNama_pp(), 0);
                        } else if (espajModel.getUsulanAsuransiModel().getPaket_ua() == 36) {
                            MethodSupport.Alert(this, getString(R.string.title_va), getString(R.string.msg_va) + espajModel.getModelID().getVa_number()
                                    + "\n" + "Total Premi" + " : " + StaticMethods.toMoney(espajModel.getUsulanAsuransiModel().getUnitlink_total_ua()) +
                                    "\n" + "Nama Member" + " : " + espajModel.getPemegangPolisModel().getNama_pp(), 0);
                        } else if (espajModel.getUsulanAsuransiModel().getPaket_ua() == 37) {
                            MethodSupport.Alert(this, getString(R.string.title_va), getString(R.string.msg_va) + espajModel.getModelID().getVa_number()
                                    + "\n" + "Total Premi" + " : " + StaticMethods.toMoney(espajModel.getUsulanAsuransiModel().getUnitlink_total_ua()) +
                                    "\n" + "Nama Member" + " : " + espajModel.getPemegangPolisModel().getNama_pp(), 0);
                        } else {
                            MethodSupport.Alert(this, getString(R.string.title_va), getString(R.string.msg_va) + espajModel.getModelID().getVa_number(), 0);
                        }
                    }
                }
                break;
            case R.id.menu_sertifikat:
                if (espajModel.getModelID().getSPAJ_ID_TAB().equals("")) {
                    MethodSupport.Alert(this, getString(R.string.title_error), getString(R.string.msg_save2), 0);
                } else {
                    if (espajModel.getModelID().getSertifikat().equals("")) {
                        if (StaticMethods.isNetworkAvailable(this)) {
                            if (val_sertifikat()) {
                                progressDialog = new ProgressDialog(this);
                                progressDialog.setTitle("Menarik Sertifikat");
                                progressDialog.setMessage("Mohon Menunggu...");
                                progressDialog.show();
                                progressDialog.setCancelable(false);
                                ewsRestClientUsage.getSertifikatSPAJ(this, espajModel);
//                                sertifikat(espajModel);
                            } else {
                                MethodSupport.Alert(this, getString(R.string.title_error_val), getString(R.string.msg_sertifikat), 0);
                            }
                        } else {
                            MethodSupport.Alert(this, getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi), 0);
                        }
                    } else {
                        //Load saved certificate pdf
                        loadSavedCertificatePdf();
                    }
                }
                break;
            case R.id.menu_referral:
                if (StaticMethods.isNetworkAvailable(this)) {
                    getReferral();
                } else {
                    MethodSupport.Alert(this, getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi), 0);
                }
                break;
            case R.id.menu_spaj:
                if (StaticMethods.isNetworkAvailable(this)) {
                    getBankPusat();
                } else {
                    MethodSupport.Alert(this, getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi), 0);
                }
                break;
            case R.id.menuShowVa:
                displaySavedVirtualAccount(ppName, vaNumber);
                break;
            case R.id.menu_validasi:
//                Method_Validator.onGetAllValidation(espajModel, E_MainActivity.this, E_MainActivity.this);
                break;
            case R.id.menu_rekening:
                displaySavedRekeningBank();
                break;
            case R.id.menu_exit:
                onBackPressed();
                break;
            case R.id.menu_cc_payment :

//                EventBus.getDefault().post(new PaymentEvent(Const.CREDIT_CARD_PAYMENT_CODE,true, "KARTU KREDIT", true));

//                boolean isFirstAndSecondPageAreFilled = Method_Validator.validateFirstPageValidation(espajModel, E_MainActivity.this, E_MainActivity.this);
//
//                boolean isSwitchingToCreditCardPayment = isSwitchingToCreditCardPayment();
                if (espajModel.getModelID().getSPAJ_ID_TAB().equals("")) {
                    displaySpajNoSaved();
                } else if (!StaticMethods.isNetworkAvailable(this)) {
                    displayNoInternetConnection();
                } else {
                    displayCreditCardPaymentConfirmationDialog();
                }



                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void displaySavedRekeningBank() {
        //Init custom alert dialog
        View view = getLayoutInflater().inflate(R.layout.dialog_display_rekening_jatim, null);
        TextView tvNoRek = view.findViewById(R.id.tvNoRek);
        TextView tvRekNumber = view.findViewById(R.id.tvRekNumber);
        TextView tvKeteranganNoRek = view.findViewById(R.id.tvKeteranganNoRek);
        Button btnOk = view.findViewById(R.id.btnOk);

        String tvNoRekJatimKonven = getString(R.string.title_rekening_jatim_konven);
        String tvNoRekJatimSyariah = getString(R.string.title_rekening_jatim_syariah);
        String tvNoRekBukopin = getString(R.string.title_rekening_bukopin);
        String tvNoRekBukopinSyariah = getString(R.string.title_rekening_bukopin_syariah);
        String tvNoRekBJB = getString(R.string.title_rekening_bjb);

        String tvRekNumberJatimKonven = "0011281087"+"\n"+"A/n PT Asuransi Jiwa Sinarmas MSIG"+"\n"+"Bank Jatim Cabang Surabaya";
        String tvRekNumberJatimSyariah = "6101006055"+"\n"+"A/n Asuransi Jiwa Sinarmas MSIG PT"+"\n"+"Bank Jatim Syariah Cabang Surabaya";
        String tvRekNumberBukopin = "102.4054.019"+"\n"+"A/n PT Asuransi Jiwa Sinarmas MSIG Tbk"+"\n"+"Bank Bukopin Cabang MT Haryono - Jakarta";
        String tvRekNumberBukopinSyariah = "8802174102"+"\n"+"A/n. A.J. SINARMAS MSIG SYARIAH,PT"+"\n"+"BANK SYARIAH BUKOPIN";
        String tvRekNumberBJB = "0074676456001"+"\n"+"A/n. PT Asuransi Jiwa Sinarmas MSIG"+"\n"+"Bank BJB (Bank Jabar Banten) Cabang Khusus Jakarta";

        String tvKeteranganNoRekJatimKonven = getString(R.string.msg_rek_jatim_konven);
        String tvKeteranganNoRekJatimSyariah = getString(R.string.msg_rek_jatim_syariah);
        String tvKeteranganNoRekBukopin = getString(R.string.msg_rek_bukopin);
        String tvKeteranganNoRekBukopinSyariah = getString(R.string.msg_rek_bukopin_syariah);
        String tvKeteranganNoRekBJB = getString(R.string.msg_rek_bjb);

        if(espajModel.getDetilAgenModel().getJENIS_LOGIN_BC() == 58){ // jatim syariah
            tvNoRek.setText(tvNoRekJatimSyariah);
            tvRekNumber.setText(tvRekNumberJatimSyariah);
            tvKeteranganNoRek.setText(tvKeteranganNoRekJatimSyariah);
        }else if(espajModel.getDetilAgenModel().getJENIS_LOGIN_BC() == 51){ // jatim konven
            tvNoRek.setText(tvNoRekJatimKonven);
            tvRekNumber.setText(tvRekNumberJatimKonven);
            tvKeteranganNoRek.setText(tvKeteranganNoRekJatimKonven);
        }else if(espajModel.getDetilAgenModel().getJENIS_LOGIN_BC() == 50){ // bukopin
            tvNoRek.setText(tvNoRekBukopin);
            tvRekNumber.setText(tvRekNumberBukopin);
            tvKeteranganNoRek.setText(tvKeteranganNoRekBukopin);
        }else if(espajModel.getDetilAgenModel().getJENIS_LOGIN_BC() == 60){ // bukopin syariah
            tvNoRek.setText(tvNoRekBukopinSyariah);
            tvRekNumber.setText(tvRekNumberBukopinSyariah);
            tvKeteranganNoRek.setText(tvKeteranganNoRekBukopinSyariah);
        }else if(espajModel.getDetilAgenModel().getJENIS_LOGIN_BC() == 44){ // BJB
            tvNoRek.setText(tvNoRekBJB);
            tvRekNumber.setText(tvRekNumberBJB);
            tvKeteranganNoRek.setText(tvKeteranganNoRekBJB);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(E_MainActivity.this);
        builder.setView(view);
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();

        btnOk.setOnClickListener(v -> dialog.dismiss());
    }

//    public void sertifikat(EspajModel espajModel) {
//        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
//        String url_path = "";
//        final String spajJson = JSONToString.JSONSertifikat(this, espajModel);
//        url_path = "spaj/api/json/get_nopol";
//        String url = AppConfig.getBaseUrlSPAJ().concat(url_path);
//        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), spajJson);
//        Call<SertifikatResponse> call = api.sertifikat(url, requestBody);
//
//        call.enqueue(new Callback<SertifikatResponse>() {
//            @Override
//            public void onResponse(Call<SertifikatResponse> call, Response<SertifikatResponse> response) {
//                progressDialog.dismiss();
//                try {
//                    SertifikatResponse body = response.body();
//                    String ERROR = "";
//                    String no_polis = "";
//
//                    if (body.getNoPolis() != null) {
//                        no_polis = body.getNoPolis();
//                    }
//
//                    ERROR = body.getERROR();
//                    if (ERROR.equals("")) {
//                        espajModel.getModelID().setSertifikat(no_polis);
//                        new E_Update(E_MainActivity.this).UpdateSertifikat(espajModel);
//                        generateCertificatePDialog = new ProgressDialog(E_MainActivity.this);
//                        generateCertificatePDialog.setTitle("Mengunduh sertifikat");
//                        generateCertificatePDialog.setMessage("Mohon tunggu");
//                        generateCertificatePDialog.setCancelable(false);
//                        generateCertificatePDialog.show();
//
//                        //Generate certificate
////                        ewsRestClientUsage.generateCertificate(E_MainActivity.this, espajModel);
//                    } else {
//                        AlertDialog.Builder dialog = new AlertDialog.Builder(E_MainActivity.this);
//                        dialog.setTitle("TERDAPAT KESALAHAN...");
//                        dialog.setMessage(ERROR);
//                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        });
//                        dialog.show();
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<SertifikatResponse> call, Throwable t) {
//                progressDialog.dismiss();
//                MethodSupport.Alert(E_MainActivity.this, getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi), 0);
//            }
//        });
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            case R.id.iv_save:
//                onSave();
//                break;
        }
    }

    private void setBundle(String SPAJ_ID_TAB, String idproposal_tab) {
        QueryUtil QueryUtil = new QueryUtil(this);

        espajModel = (MethodModel.getNewModel(this, espajModel));
        espajModel.getDetilAgenModel().setLca_id(lca_id);
        espajModel.getDetilAgenModel().setLwk_id(lwk_id);
        espajModel.getDetilAgenModel().setLsrg_id(lsrg_id);
        espajModel.getDetilAgenModel().setKODE_REGIONAL(KODE_REGIONAL);
        espajModel.getDetilAgenModel().setKd_regional_da(KODE_REGIONAL);
        espajModel.getDetilAgenModel().setJENIS_LOGIN(JENIS_LOGIN);
        espajModel.getDetilAgenModel().setJENIS_LOGIN_BC(JENIS_LOGIN_BC);
        espajModel.getDetilAgenModel().setEmail_da(EMAIL);
        espajModel.getDetilAgenModel().setKd_penutup_da(kodeAgen);
        espajModel.getDetilAgenModel().setId_member_da(noid);
        espajModel.getDetilAgenModel().setGroup_id_da(GROUP_ID);

        if (!SPAJ_ID_TAB.equals("")) {
            espajModel.getModelID().setSPAJ_ID_TAB(SPAJ_ID_TAB); //getSPAJ
            MethodModel.getSPAJ(this, QueryUtil, SPAJ_ID_TAB, espajModel);
        } else if (!idproposal_tab.equals("") || !idproposal_tab.equals(null)) {
            MethodModel.getProposal(this, QueryUtil, idproposal_tab, espajModel);
        }

        espajModel.getDetilAgenModel().setGroup_id_da(GROUP_ID);
        e_bundle = new Bundle();
        e_bundle.putParcelable(getString(R.string.modelespaj), espajModel);
    }

    /**
     * Show only options menu if login type is  Siap2U or jenis login BC is BSIM and BTNS
     *
     * @return
     */
    private boolean shouldShowTarikVirtualAccountOptionsMenu() {
        return JENIS_LOGIN == Const.SIAP2U_LOGIN_TYPE || JENIS_LOGIN_BC == Const.BANK_CODE_SINARMAS || JENIS_LOGIN_BC == Const.BANK_CODE_BTN_SYARIAH;
    }

    //method yang dipanggil fragment buat ganti judul diatas
    public void setSecondToolbar(String title, int flag_toolbar) {
        //jumlah halaman berdasarkan jenis produk link/nonlink
        tv_toolbar_title.setText(title + "10)");
        if (flag_toolbar == 1) {
            changeColorView(view1, view2, view3, view4, view5, view6, view7, view8, view9, view10);
        } else if (flag_toolbar == 2) {
            changeColorView(view2, view1, view3, view4, view5, view6, view7, view8, view9, view10);
        } else if (flag_toolbar == 3) {
            changeColorView(view3, view2, view1, view4, view5, view6, view7, view8, view9, view10);
        } else if (flag_toolbar == 4) {
            changeColorView(view4, view2, view3, view1, view5, view6, view7, view8, view9, view10);
        } else if (flag_toolbar == 5) {
            changeColorView(view5, view2, view3, view4, view1, view6, view7, view8, view9, view10);
        } else if (flag_toolbar == 6) {
            changeColorView(view6, view2, view3, view4, view5, view1, view7, view8, view9, view10);
        } else if (flag_toolbar == 7) {
            changeColorView(view7, view2, view3, view4, view5, view6, view1, view8, view9, view10);
        } else if (flag_toolbar == 8) {
            changeColorView(view8, view2, view3, view4, view5, view6, view7, view1, view9, view10);
        } else if (flag_toolbar == 9) {
            changeColorView(view9, view2, view3, view4, view5, view6, view7, view8, view1, view10);
        } else if (flag_toolbar == 10) {
            changeColorView(view10, view2, view3, view4, view5, view6, view7, view8, view1, view1);
        }
    }

    private void changeColorView(View view1, View view2, View view3, View view4, View view5, View view6, View view7, View view8, View view9, View view10) {
        view1.setBackgroundColor(ContextCompat.getColor(this, R.color.dua));
        view2.setBackgroundColor(ContextCompat.getColor(this, R.color.lima));
        view3.setBackgroundColor(ContextCompat.getColor(this, R.color.lima));
        view4.setBackgroundColor(ContextCompat.getColor(this, R.color.lima));
        view5.setBackgroundColor(ContextCompat.getColor(this, R.color.lima));
        view6.setBackgroundColor(ContextCompat.getColor(this, R.color.lima));
        view7.setBackgroundColor(ContextCompat.getColor(this, R.color.lima));
        view8.setBackgroundColor(ContextCompat.getColor(this, R.color.lima));
        view9.setBackgroundColor(ContextCompat.getColor(this, R.color.lima));
        view10.setBackgroundColor(ContextCompat.getColor(this, R.color.lima));
    }

    public void setFragment(Fragment fragment, String Tag) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
            fragmentTransaction.replace(R.id.fl_container, fragment, Tag);
            fragmentTransaction.commit();

        }
    }

    public void onSave(int pagePosition) {

        String ERROR = "";
        // JSONObject espaj=json.getJSONObject("ESPAJ");
        if (ERROR.equals("")) {
            espajModel.getModelID().setPage_position(pagePosition);
            new E_Update(this).UpdatePage(espajModel);
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(
                    this);
            dialog.setTitle("TERDAPAT KESALAHAN...");
            dialog.setMessage(ERROR);
            dialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int which) {
                            dialog.dismiss();
                        }
                    });
            dialog.show();
        }

        QueryUtil queryUtil = new QueryUtil(this);
        String selection = getString(R.string.NO_PROPOSAL_TAB) + " = ?";
        String[] selectionArgs = new String[]{idproposal_tab};
        if (SPAJ_ID_TAB.equals("") || espajModel.getModelID().getSPAJ_ID_TAB().equals("") || espajModel.getModelID().getSPAJ_ID_TAB() == null) {
            SPAJ_ID_TAB = "AndroidESPAJ" + GetTime.getCurrentDate("yyyy.MM.dd.hh.mm.ss.SSS");
            TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            String imei = mngr.getDeviceId();
            espajModel.getModelID().setImei(imei);
            espajModel.getModelID().setSPAJ_ID_TAB(SPAJ_ID_TAB);
            espajModel.getUsulanAsuransiModel().setAwalpertanggungan_ua(GetTime.getCurrentDate());
            espajModel.getUsulanAsuransiModel().setAkhirpertanggungan_ua(GetTime.getEndDate(espajModel.getUsulanAsuransiModel().getAwalpertanggungan_ua(), espajModel.getUsulanAsuransiModel().getMasa_pertanggungan_ua()));
            espajModel.getDetilAgenModel().setTgl_spaj_da(GetTime.getCurrentDate());
            espajModel.getDetilAgenModel().setKd_penutup_da(kodeAgen);
            espajModel.getModelID().setProposal(idproposal);

            new E_Insert(this).InsertESPAJ(espajModel);
            new E_Update(this).UpdateESPAJ(espajModel);

            addProposalCreatedActivity();

        } else {
            new E_Update(this).UpdateESPAJ(espajModel);
        }
//        Toast.makeText(this, "DATA BERHASIL TERSIMPAN", Toast.LENGTH_LONG).show();
    }

    private int getSLAType(Long slTabId) {
        Cursor cursor = new QueryUtil(E_MainActivity.this).query(
                getString(R.string.TABLE_SIMBAK_LEAD),
                null,
                getString(R.string.SL_TAB_ID) + "=?",
                new String[]{String.valueOf(slTabId)},
                null
        );
        if (cursor.moveToFirst()) {
            int slType = cursor.getInt(cursor.getColumnIndexOrThrow(getString(R.string.SL_TYPE)));
            cursor.close();
            return slType;
        }

        return 0;
    }

    /**
     * Automatically add "kunjungan dan presentasi" "nasabah tertarik (buat proposal)" activity on lead when user saved the SPAJ
     */
    private void addProposalCreatedActivity() {
        C_Select select = new C_Select(E_MainActivity.this);
        C_Insert insert = new C_Insert(E_MainActivity.this);

        SimbakActivityModel model = new SimbakActivityModel();

        model.setSLA_UPDTD_ID(kodeAgen);
        model.setSLA_UPDTD_DATE(StaticMethods.getTodayDateTime());
        model.setSLA_ACTIVE(1);
        model.setSLA_LAST_POS(1);
        model.setSLA_SDATE(StaticMethods.getTodayDateTime());
        model.setSLA_TYPE(KUNJUNGAN_DAN_PRESENTASI_ID);
        model.setSLA_SUBTYPE(NASABAH_TERTARIK_BUAT_PROPOSAL_ID);
        model.setSLA_DETAIL(idproposal_tab);

        int slaType = getSLAType(slTabId);
        model.setSLA_SRC(slaType);

        model.setSLA_IDATE(StaticMethods.getTodayDateTime());
        model.setSLA_CRTD_ID(kodeAgen);
        model.setSLA_CRTD_DATE(StaticMethods.getTodayDateTime());
        model.setSLA_INST_TAB_ID(slTabId);
        model.setSLA_INST_TYPE(2);
        long timestamp = DateUtils.generateTimeStamp();
        model.setSLA_TAB_ID(timestamp);

        String slName = select.getLeadNameFromSLID(String.valueOf(slTabId));
        model.setSL_NAME(slName);
        model.setSL_ID_(null); //SL_ID must be ID from the from server, so we set it null

        Long SLA_TAB_ID = insert.insertToSimbakListActivity(model);

        model.setSLA_TAB_ID(SLA_TAB_ID);

        List<Long> listActivity = new ArrayList<>();
        listActivity.add(model.getSLA_TAB_ID());


        if (listActivity.size() > 0) {
            if (GROUP_ID != Const.SIAP2U_GROUP_ID) {
                new CrmRestClientUsage(this).createUploadActivityRequestBodyFrom(listActivity);
            }
        }

    }


    /**
     * Access SD card, load certificate and display it if the certificate pdf is exist.
     */
    private void loadSavedCertificatePdf() {
        String pdfName = "SERTIFIKAT" + espajModel.getModelID().getSertifikat() + ".PDF";
        File file = new File(getFilesDir() + "/ESPAJ/SERTIFIKAT/" + pdfName);

        //Check if file is exist
//        if (file.exists()) {
//            if (file.length() == 0) {
                generateCertificatePDialog = new ProgressDialog(E_MainActivity.this);
                generateCertificatePDialog.setTitle("Mengunduh sertifikat");
                generateCertificatePDialog.setMessage("Mohon tunggu");
                generateCertificatePDialog.setCancelable(false);
                generateCertificatePDialog.show();
                //Generate certificate
                ewsRestClientUsage.generateCertificate(this, espajModel);

//            } else {
//                Intent intent = new Intent(this, E_SertifikatPdfReader.class);
//                intent.putExtra(Const.INTENT_KEY_CERTIFICATE_FILE, file);
//                startActivity(intent);
//            }
//        } else {
//            file = new File(Environment.getExternalStorageDirectory() + "/ESPAJ/SERTIFIKAT/" + pdfName);
//            if (file.exists()) {
//                if (file.length() == 0) {
//                    generateCertificatePDialog = new ProgressDialog(E_MainActivity.this);
//                    generateCertificatePDialog.setTitle("Mengunduh sertifikat");
//                    generateCertificatePDialog.setMessage("Mohon tunggu");
//                    generateCertificatePDialog.setCancelable(false);
//                    generateCertificatePDialog.show();
                    //Generate certificate
//                    ewsRestClientUsage.generateCertificate(this, espajModel);

//                } else {
//                    Intent intent = new Intent(this, E_SertifikatPdfReader.class);
//                    intent.putExtra(Const.INTENT_KEY_CERTIFICATE_FILE, file);
//                    startActivity(intent);
//                }
//            } else {
//                Toast.makeText(this, R.string.err_msg_certificate_not_found, Toast.LENGTH_SHORT).show();
//            }
//        }
    }

    /**
     * Display saved virtual account of the selected proposal when user pressed 'tampilkan virtual account' options menu
     *
     * @param virtualAccount
     */
    private void displaySavedVirtualAccount(String ppName, String virtualAccount) {
        //Init custom alert dialog
        View view = getLayoutInflater().inflate(R.layout.dialog_display_saved_va, null);
        TextView tvHint = view.findViewById(R.id.tvVirtualAccount);
        TextView tvVaNumber = view.findViewById(R.id.tvVaNumber);
        TextView tvCopy = view.findViewById(R.id.tvCopyVa);
        Button btnOk = view.findViewById(R.id.btnOk);

        String hintMessage = "Silakan gunakan nomor virtual account dibawah ini untuk nabasah\n\n " + "a/n " + ppName;
        tvHint.setText(hintMessage);
        tvVaNumber.setText(virtualAccount);
        tvCopy.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            if (clipboard != null) {
                ClipData clip = ClipData.newPlainText("va-number", virtualAccount);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(E_MainActivity.this, R.string.virtual_account_copied_to_clipboard, Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(E_MainActivity.this);
        builder.setView(view);
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();

        btnOk.setOnClickListener(v -> dialog.dismiss());
    }

    public Toolbar getSecond_toolbar() {
        return second_toolbar;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Konfirmasi...");
        dialog.setMessage(R.string.exit_message_confirmation);
        dialog.setNegativeButton(getString(R.string.tidak), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                    MethodSupport.active_view(1, btn_lanjut);
//                    MethodSupport.active_view(1, iv_next);
            }
        });
        dialog.setPositiveButton(getString(R.string.iya), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.show();
    }

    private boolean val_sertifikat() {
        Boolean val = true;
        if (espajModel.getPemegangPolisModel().getNama_pp().equals("")) {
            val = false;
        }

        if (espajModel.getPemegangPolisModel().getTagihan_pp() == 0) {
            if (espajModel.getPemegangPolisModel().getAlamat_pp().equals("")) {
                val = false;
            }
        } else if (espajModel.getPemegangPolisModel().getTagihan_pp() == 1) {
            if (espajModel.getPemegangPolisModel().getAlamat_kantor_pp().equals("")) {
                val = false;
            }
        } else if (espajModel.getPemegangPolisModel().getTagihan_pp() == 2) {
            if (espajModel.getPemegangPolisModel().getAlamat_tinggal_pp().equals("")) {
                val = false;
            }
        }

        if (espajModel.getPemegangPolisModel().getUsia_pp().equals("")) {
            val = false;
        }
        if (espajModel.getPemegangPolisModel().getTtl_pp().equals("")) {
            val = false;
        }
        if (espajModel.getTertanggungModel().getNama_tt().equals("")) {
            val = false;
        }
        if (espajModel.getTertanggungModel().getTtl_tt().equals("")) {
            val = false;
        }
        if (espajModel.getTertanggungModel().getUsia_tt().equals("")) {
            val = false;
        }
        if (espajModel.getUsulanAsuransiModel().getKd_produk_ua() == 0) {
            val = false;
        }
        if (espajModel.getUsulanAsuransiModel().getSub_produk_ua() == 0) {
            val = false;
        }
        if (espajModel.getUsulanAsuransiModel().getUp_ua() == (double) 0) {
            val = false;
        }
        if (espajModel.getUsulanAsuransiModel().getUnitlink_premistandard_ua() == (double) 0) {
            val = false;
        }
        if (espajModel.getUsulanAsuransiModel().getAwalpertanggungan_ua().equals("")) {
            val = false;
        }
        return val;
    }

    @Override
    public void onSertifikatSuccess(int statusCode, Header[] headers, JSONObject json) {
        progressDialog.dismiss();

        //Response would be like this : {"ERROR":"","no_polis":"09412121800468"}
        try {
            String no_policy = "";
            String ERROR = "";
            // JSONObject espaj=json.getJSONObject("ESPAJ");
            if (json.has("no_polis")) {
                no_policy = json.getString("no_polis");
            }
            ERROR = json.getString("ERROR");
            if (ERROR.equals("")) {
                espajModel.getModelID().setSertifikat(no_policy);
                new E_Update(this).UpdateSertifikat(espajModel);

                generateCertificatePDialog = new ProgressDialog(E_MainActivity.this);
                generateCertificatePDialog.setTitle("Mengunduh sertifikat");
                generateCertificatePDialog.setMessage("Mohon tunggu");
                generateCertificatePDialog.setCancelable(false);
                generateCertificatePDialog.show();

                //Generate certificate
                ewsRestClientUsage.generateCertificate(this, espajModel);

            } else {
                AlertDialog.Builder dialog = new AlertDialog.Builder(
                        this);
                dialog.setTitle("TERDAPAT KESALAHAN...");
                dialog.setMessage(ERROR);
                dialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });
                dialog.show();
            }
            // dialog.dismiss();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processReferralProgress(List<ReferralResponse> referralResponses) {
        if (!referralResponses.isEmpty()) {

            for (ReferralResponse referralResponse : referralResponses) {
                int flaglisensi = referralResponse.getFLAGLISENSI();
                String agentcode = referralResponse.getAGENTCODE();
                String lcbno = referralResponse.getLCBNO();
                String namacabang = referralResponse.getNAMACABANG();
                String namareff = referralResponse.getNAMAREFF();

                ModelReferralDa model = new ModelReferralDa(namareff, agentcode, namacabang, lcbno, flaglisensi);
                ListReferral.add(model);
            }
        }
    }

    public void getReferralRetrofit(int jenis_bank) {
        String url_path = "member/api/json/data_bc/";
        String url = AppConfig.getBaseUrlWs().concat(url_path)+jenis_bank;
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call <List<ReferralResponse>> call = api.getReferral(url);
        call.enqueue(new Callback<List<ReferralResponse>>() {
            @Override
            public void onResponse(Call<List<ReferralResponse>> call, Response<List<ReferralResponse>> response) {
                try{
                    if (response.body() != null && response.isSuccessful()) {
                        List<ReferralResponse> referralResponses = response.body();

                        if (referralResponses != null && referralResponses.size() > 0){
                            QueryUtil queryUtil = new QueryUtil(E_MainActivity.this);
                            new ContentValues();
                            ContentValues cv;

                            processReferralProgress(referralResponses);

                            if (!ListReferral.isEmpty()) {
                                E_Delete delete = new E_Delete(E_MainActivity.this);
                                delete.deleteLstRef();
                                for (int i = 0; i < ListReferral.size(); i++) {
                                    cv = new TableLST_Reff(E_MainActivity.this).getContentValues(ListReferral.get(i));
                                    queryUtil.insert(getString(R.string.TABLE_E_LST_REFF), cv);
                                }
                            }
                            progressDialog.dismiss();
                            Toast.makeText(E_MainActivity.this, "Data berhasil ditarik", Toast.LENGTH_SHORT).show();

                        }

                    }
                }catch (Exception e){
                    Log.d("TES", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<ReferralResponse>> call, Throwable t) {
                progressDialog.dismiss();
                MethodSupport.Alert(E_MainActivity.this, getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi), 0);
            }
        });
    }

    public void getBankCabRetrofit() {
        String url_path = "spaj/api/json/spaj_bank?type=cabang";
        String url = AppConfig.getBaseUrlSPAJBank().concat(url_path);
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call <BankCabangResponse> call = api.getBankCabang(url);
        call.enqueue(new Callback<BankCabangResponse>() {
            @Override
            public void onResponse(Call<BankCabangResponse> call, Response<BankCabangResponse> response) {
                try {
                    if (response.body() != null && response.isSuccessful()) {
                        List<BANKCABANG> BankCab;

                        BankCabangResponse body = response.body();
                        BankCab = body.getBANKCABANG();

                        QueryUtil queryUtil = new QueryUtil(E_MainActivity.this);
                        new ContentValues();
                        ContentValues cv;

                        processBANKCABProgress(BankCab);
                        if (!ListBankCab.isEmpty()) {
                            queryUtil.delete(getString(R.string.TABLE_E_LST_BANKCAB), null, null);
                            for (int i = 0; i < ListBankCab.size(); i++) {
                                cv = new TableLST_Bank(E_MainActivity.this).getContentBankCab(ListBankCab.get(i));
                                queryUtil.insert(getString(R.string.TABLE_E_LST_BANKCAB), cv);
                            }
                            if (JENIS_LOGIN == 9) {
                                if (new E_Select(E_MainActivity.this).getCountReferral() == 0) {
                                    getReferral();
                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(E_MainActivity.this, "Data berhasil ditarik", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(E_MainActivity.this, "Data berhasil ditarik", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                } catch (Exception e) {
                    Log.d("TES", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<BankCabangResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(E_MainActivity.this ,t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getBankRetrofit() {
        String url_path = "spaj/api/json/spaj_bank?type=pusat";
        String url = AppConfig.getBaseUrlSPAJBank().concat(url_path);
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call <BankPusatResponse> call = api.getBankPusat(url);
        call.enqueue(new Callback<BankPusatResponse>() {
            @Override
            public void onResponse(Call<BankPusatResponse> call, Response<BankPusatResponse> response) {
                try{
                    if (response.body() != null && response.isSuccessful()) {
                        List<BANKPUSAT> Bank;

                        BankPusatResponse body = response.body();
                        Bank = body.getBANKPUSAT();

                        QueryUtil queryUtil = new QueryUtil(E_MainActivity.this);
                        new ContentValues();
                        ContentValues cv;
                        processBANKProgress(Bank);
                        if (!ListBank.isEmpty()) {
                            queryUtil.delete(getString(R.string.TABLE_E_LST_BANK), null, null);
                            for (int i = 0; i < ListBank.size(); i++) {
                                cv = new TableLST_Bank(E_MainActivity.this).getContentBank(ListBank.get(i));
                                queryUtil.insert(getString(R.string.TABLE_E_LST_BANK), cv);
                            }
                            getBankCabRetrofit();
                        }
                    }
                }catch (Exception e) {
                    Log.d("TES", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<BankPusatResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(E_MainActivity.this ,t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void processBANKCABProgress(List<BANKCABANG> BankCab) {
        if (!BankCab.isEmpty()) {

            for (BANKCABANG bankcabang : BankCab) {
                int lsbpid = bankcabang.getLSBPID();
                String lbnnama = bankcabang.getLBNNAMA();
                int lsbppanjangrekening = bankcabang.getLSBPPANJANGREKENING();
                int lbnid = bankcabang.getLBNID();

                ModelBankCab model = new ModelBankCab(lsbpid, lbnid, lbnnama, lsbppanjangrekening);
                ListBankCab.add(model);
            }
        }
    }

    private void processBANKProgress(List<BANKPUSAT> Bank) {
        if (!Bank.isEmpty()) {

            for (BANKPUSAT bankpusat : Bank) {
                int lsbpid = bankpusat.getLSBPID();
                String lsbpnama = bankpusat.getLSBPNAMA();
                int lsbppanjangrekening = bankpusat.getLSBPPANJANGREKENING();
                int lsbpminpanjangrekening = bankpusat.getLSBPMINPANJANGREKENING();

                ModelBank model = new ModelBank(lsbpid, lsbpnama, lsbppanjangrekening, lsbpminpanjangrekening);
                ListBank.add(model);
            }
        }
    }

    @Override
    public void onSertifikatFailure(String title, String message) {
        progressDialog.dismiss();
        MethodSupport.Alert(this, getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi), 0);
    }

    private void getBankPusat() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Menarik Data Bank");
        progressDialog.setMessage("Mohon Menunggu...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        getBankRetrofit();
    }

    private void getReferral() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Menarik Data Refferal");
        progressDialog.setMessage("Mohon Menunggu...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        if(espajModel.getDetilAgenModel().getJENIS_LOGIN_BC()==2 && espajModel.getDetilInvestasiModel().getInvest_bank_di()==224){
//            wsApiRestClientUsage.getRefferal(16);
            getReferralRetrofit(16);
        }else {
//            wsApiRestClientUsage.getRefferal(espajModel.getDetilAgenModel().getJENIS_LOGIN_BC());
            getReferralRetrofit(espajModel.getDetilAgenModel().getJENIS_LOGIN_BC());
        }
    }

    private void getVirtualAcc() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Menarik Nomor Virtual Accout");
        progressDialog.setMessage("Mohon Menunggu...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        ewsRestClientUsage.getVirtualAcc(espajModel);
    }

    public void loadingpage() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Melakukan perpindahan halaman");
        progressDialog.setMessage("Mohon Menunggu...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        Runnable progressRunnable = new Runnable() {

            @Override
            public void run() {
                progressDialog.cancel();
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, 1000);
    }

    //All Impelement method Success and failure Here
    @Override
    public void onReferralSuccess(int statusCode, Header[] headers, JSONArray response) {
        QueryUtil queryUtil = new QueryUtil(this);
        ContentValues cv = new ContentValues();
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<ModelReferralDa>>() {
        }.getType();
        List<ModelReferralDa> listReferral = gson.fromJson(response.toString(), type);
        if (!listReferral.isEmpty()) {
            queryUtil.delete(getString(R.string.TABLE_E_LST_REFF), null, null);
            for (int i = 0; i < listReferral.size(); i++) {
                cv = new TableLST_Reff(this).getContentValues(listReferral.get(i));
                queryUtil.insert(getString(R.string.TABLE_E_LST_REFF), cv);
            }
        }
        progressDialog.dismiss();
        Toast.makeText(this, "Data berhasil ditarik", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReferralFailure(String title, String message) {
        progressDialog.dismiss();
        MethodSupport.Alert(this, getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi), 0);
    }


    @Override
    public void ongetVaSuccess(int statusCode, Header[] headers, JSONObject response) {
        progressDialog.dismiss();

        try {
            String ERROR = "";
            // JSONObject espaj=json.getJSONObject("ESPAJ");
            if (response.has("virtual")) {
                espajModel.getModelID().setVa_number(response.getString("virtual"));
                new E_Update(this).UpdateSUBMIT(espajModel);
                MethodSupport.Alert(this, getString(R.string.title_va), getString(R.string.msg_va) + espajModel.getModelID().getVa_number(), 0);
            }
            ERROR = response.getString("ERROR");
            if (!ERROR.equals("")) {
                MethodSupport.Alert(this, getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi), 0);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void ongetVaFailure(String title, String message) {
        progressDialog.dismiss();
        MethodSupport.Alert(this, getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi), 0);
    }

//    @Override
//    public void onGetBankSuccess(int statusCode, Header[] headers, JSONObject response) {
//        try {
//            QueryUtil queryUtil = new QueryUtil(this);
//            ContentValues cv = new ContentValues();
//            Gson gson = new Gson();
//            JSONArray Bank = response.getJSONArray("BANK PUSAT");
//            Type type = new TypeToken<ArrayList<ModelBank>>() {
//            }.getType();
//            List<ModelBank> List = gson.fromJson(Bank.toString(), type);
//            if (!List.isEmpty()) {
//                queryUtil.delete(getString(R.string.TABLE_E_LST_BANK), null, null);
//                for (int i = 0; i < List.size(); i++) {
//                    cv = new TableLST_Bank(this).getContentBank(List.get(i));
//                    queryUtil.insert(getString(R.string.TABLE_E_LST_BANK), cv);
//                }
////                ewsRestClientUsage.getBankCab();
//            }
////        progressDialog.dismiss();
////        Toast.makeText(this, "Data referral berhasil ditarik", Toast.LENGTH_SHORT).show();
//        } catch (Exception e) {
//            Log.d("TES", e.getMessage());
//        }
//    }

//    @Override
//    public void ongetBankFailure(String title, String message) {
//
//    }

//    @Override
//    public void onGetBankCabSuccess(int statusCode, Header[] headers, JSONObject response) {
//        try {
//            QueryUtil queryUtil = new QueryUtil(this);
//            ContentValues cv = new ContentValues();
//            Gson gson = new Gson();
//            JSONArray Bank = response.getJSONArray("BANK CABANG");
//            Type type = new TypeToken<ArrayList<ModelBankCab>>() {
//            }.getType();
//            List<ModelBankCab> List = gson.fromJson(Bank.toString(), type);
//            if (!List.isEmpty()) {
//                queryUtil.delete(getString(R.string.TABLE_E_LST_BANKCAB), null, null);
//                for (int i = 0; i < List.size(); i++) {
//                    cv = new TableLST_Bank(this).getContentBankCab(List.get(i));
//                    queryUtil.insert(getString(R.string.TABLE_E_LST_BANKCAB), cv);
//                }
//                if (JENIS_LOGIN == 9) {
//                    if (new E_Select(this).getCountReferral() == 0) {
//                        getReferral();
//                    } else {
//                        progressDialog.dismiss();
//                        Toast.makeText(this, "Data berhasil ditarik", Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    progressDialog.dismiss();
//                    Toast.makeText(this, "Data berhasil ditarik", Toast.LENGTH_SHORT).show();
//                }
//            }
//        } catch (Exception e) {
//            Log.d("TES", e.getMessage());
//        }
//    }

//    @Override
//    public void ongetBankCabFailure(String title, String message) {
//
//    }

    @Override
    public void onGenerateCertificateSuccess(int statusCode, Header[] headers, JSONObject response) {

    }

    @Override
    public void onGenerateCertificateSuccess(String title, String message, File file) {
        if (generateCertificatePDialog.isShowing()) {
            generateCertificatePDialog.dismiss();
        }
        if (file.exists()) {
            String pdfName = "SERTIFIKAT" + espajModel.getModelID().getSertifikat() + ".PDF";
            File myFilesDir = new File(getFilesDir() + "/ESPAJ/SERTIFIKAT/");
            if (!myFilesDir.exists()) {
                myFilesDir.mkdirs();
            }
            File pdfFile = new File(myFilesDir, pdfName);
            pdfFile.getParentFile().mkdirs();
            try {
                FileChannel src = new FileInputStream(file).getChannel();
                FileChannel dst = new FileOutputStream(pdfFile).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                file.delete();
            }
            if (pdfFile.length() == 0) {
                generateCertificatePDialog = new ProgressDialog(E_MainActivity.this);
                generateCertificatePDialog.setTitle("Mengunduh sertifikat");
                generateCertificatePDialog.setMessage("Mohon tunggu");
                generateCertificatePDialog.setCancelable(false);
                generateCertificatePDialog.show();
                //Generate certificate
                ewsRestClientUsage.generateCertificate(this, espajModel);

            } else {
                Intent intent = new Intent(E_MainActivity.this, E_SertifikatPdfReader.class);
                intent.putExtra(Const.INTENT_KEY_CERTIFICATE_FILE, pdfFile);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onGenerateCertificateFailure(String title, String message) {
        if (generateCertificatePDialog.isShowing()) {
            generateCertificatePDialog.dismiss();
        }
        new AlertDialog.Builder(E_MainActivity.this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, (dialog, which) -> dialog.dismiss())
                .show();

    }

    private boolean shouldHideTarikVaMenuOnEspaj() {
        return isUsingUSDCurrency() && (isMagnaLink() || isPrimeLink());
    }


    private boolean isUsingUSDCurrency() {
        return espajModel.getUsulanAsuransiModel().getKurs_up_ua() != null && espajModel.getUsulanAsuransiModel().getKurs_up_ua().equalsIgnoreCase("02");
    }

    private boolean isMagnaLink() {
        return espajModel.getUsulanAsuransiModel() != null && espajModel.getUsulanAsuransiModel().getKd_produk_ua() == 213 && espajModel.getUsulanAsuransiModel().getSub_produk_ua() == 1;
    }

    private boolean isPrimeLink() {
        return espajModel.getUsulanAsuransiModel() != null && espajModel.getUsulanAsuransiModel().getKd_produk_ua() == 134 && espajModel.getUsulanAsuransiModel().getSub_produk_ua() == 5;
    }

    /**
     * Produce true if the previous payment method is not credit card
     * Produce false if the previous payment method is credit card
     * @return
     */
    private boolean isSwitchingToCreditCardPayment() {
        return !isCreditCardPaymentType();
    }


    private boolean isCreditCardPaymentType() {
        String spajIdTab = espajModel.getModelID().getSPAJ_ID_TAB();
        E_Select select = new E_Select(this);

        int paymentType = select.getPaymentType(spajIdTab);
        return paymentType == Const.CREDIT_CARD_PAYMENT_CODE;
    }

    private void showPaymentCreditCardProgress(String title) {
        dialog = new ProgressDialog(this);
        dialog.setTitle(title);
        dialog.setMessage("Mohon tunggu");
        dialog.setCancelable(false);
        dialog.show();
    }

    private void dismissPaymentCreditCardProgress() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void payViaCreditCard(String virtualAccountNumber, String noId, String paymentMethod, String channel, String transactionAmount, String currency, String description) {
        showPaymentCreditCardProgress("Menghubungkan ke pembayaran");

        String url = AppConfig.getBaseUrlCreditCardPayment().concat("api/tesVa/submit_va");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        PayViaCcRequest requestBody = new PayViaCcRequest(virtualAccountNumber, noId, paymentMethod, channel, transactionAmount, currency,description);
        Call<PayViaCcResponse> call = api.payUsingCreditCard(url, requestBody);
        call.enqueue(new Callback<PayViaCcResponse>() {
            @Override
            public void onResponse(Call<PayViaCcResponse> call, Response<PayViaCcResponse> response) {
                if (response.body() != null) {
                    PayViaCcResponse responseBody = response.body();

                    handleResponse(responseBody);

                    dismissPaymentCreditCardProgress();
                }
            }

            @Override
            public void onFailure(Call<PayViaCcResponse> call, Throwable t) {
                dismissPaymentCreditCardProgress();
                showAlertDialog("Terjadi kesalahan", "Terjadi kesalahan dalam memproses pembayaran.\nError : " + t.getMessage());
            }
        });
    }

    private void handleResponse(PayViaCcResponse responseBody) {

        if (responseBody != null) {

            E_Select select = new E_Select(this);
            String spajIdTab = espajModel.getModelID().getSPAJ_ID_TAB();

            String result = responseBody.getResult(); //0 : Failed, 1 : Success
            String message = responseBody.getMessage();
            String redirectUrl = responseBody.getRedirectURL();
            String noVa = responseBody.getNo_va();
            String paymentMethod = responseBody.getPayment_method();
            String transactionNumber = select.getPaymentTransactionNumber(spajIdTab);

            if (result.equalsIgnoreCase(WAITING_FOR_PAYMENT)) {

                if (!TextUtils.isEmpty(redirectUrl)) {
                    launchWebView(redirectUrl);
                } else {
                    showAlertDialog("Terjadi kesalahan", "Tidak dapat mengakses web pembayaran karena redirect URL kosong");
                }

            } else if (result.equalsIgnoreCase(PAID)) {
                showAlertDialog("Pembayaran berhasil", "Terima kasih telah melakukan pembayaran. Status pembayaran untuk SPAJ ini sudah dibayar dengan kode transaksi "+ transactionNumber);
            } else {
                showAlertDialog("Terjadi kesalahan", "Status pembayaran tidak diketahui. Pesan error : " + message);
            }

        }

    }

    private void launchWebView(String paymentUrl) {
        Intent intent = new Intent(E_MainActivity.this, E_CreditCardPaymentActivity.class);
        intent.putExtra(Const.INTENT_KEY_WEBVIEW_URL, paymentUrl);
        startActivityForResult(intent, RequestCode.REQ_PAYMENT);
    }

    private void showAlertDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss());
        builder.show();
    }

    private boolean isPaymentSuccessful(String paymentStatus) {
        return paymentStatus.equalsIgnoreCase(SUCCESS_CODE);
    }

    private void displayCreditCardPaymentConfirmationDialog() {
//        android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Konfirmasi");
//        builder.setMessage("Apakah anda ingin melakukan pembayaran premi menggunakan credit card sekarang?");
//        builder.setCancelable(false);
//        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
//            prepareCreditCardPaymentData();
//            dialog.dismiss();
//        });
//        builder.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss());
//        builder.show();

        View view = getLayoutInflater().inflate(R.layout.dialog_cc, null);
        ImageView closeView = view.findViewById(R.id.close_view);
        Button btnUseCc = view.findViewById(R.id.btnUseCc);

        AlertDialog.Builder builder = new AlertDialog.Builder(E_MainActivity.this);
        builder.setView(view);
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();

        btnUseCc.setOnClickListener(v -> {
            prepareCreditCardPaymentData();
            dialog.dismiss();
        });

        closeView.setOnClickListener(v -> dialog.dismiss());

    }
    
    private void displayPaymentMethodChangeConfirmationDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Konfirmasi");
        builder.setMessage("Metode pembayaran yang telah Anda pilih sebelumnya adalah " + getSelectedPaymentMethodName() +". Apakah Anda ingin merubah metode pembayaran ke credit card?");
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {

            //Replace payment method to credit card
            espajModel.getUsulanAsuransiModel().setBentukbayar_ua(Const.CREDIT_CARD_PAYMENT_CODE);
            onSave(Const.PAGE_USULAN_ASURANSI);

            Toast.makeText(E_MainActivity.this, "Pembayaran berhasil dirubah ke credit card", Toast.LENGTH_SHORT).show();

            //If the options menu triggered on usulan asuransi, refresh the usulan asuransi page
            EventBus.getDefault().post(new PaymentEvent(Const.CREDIT_CARD_PAYMENT_CODE,true, "KARTU KREDIT", false));

            dialog.dismiss();

        });
        builder.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss());
        builder.show();
    }
    
    private void prepareCreditCardPaymentData() {
        String vaNumber = espajModel.getModelID().getVa_number();
        String noId = espajModel.getDetilAgenModel().getId_member_da();
        String premi = new BigDecimal(espajModel.getUsulanAsuransiModel().getUnitlink_total_ua()).toPlainString();
        String paymentMethod = String.valueOf(Const.CREDIT_CARD_PAYMENT_CODE);

        payViaCreditCard(vaNumber, noId, paymentMethod, "cc", premi, "IDR", "Pembayaran Premi");
    }

    private String getSelectedPaymentMethodName() {

        int bentukBayar = espajModel.getUsulanAsuransiModel().getBentukbayar_ua();

        if (bentukBayar == Const.TUNAITRANSFER_PAYMENT_CODE) {
            return "Tunai/Transfer";
        }

        if (bentukBayar == Const.CREDIT_CARD_PAYMENT_CODE) {
            return "Credit Card";
        }

        if (bentukBayar == Const.TABUNGAN_PAYMENT_CODE) {
            return "Tabungan";
        }

        return "Metode pembayaran belum di setup";

    }

    private void displayNoInternetConnection() {
        android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Tidak ada koneksi internet");
        builder.setMessage("Fitur ini membutuhkan koneksi intenet. Mohon periksa kembali koneksi internet Anda dan coba kembali");
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss());
        builder.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss());
        builder.show();
    }

    private void displaySpajNoSaved() {
        android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.title_error);
        builder.setMessage("Anda tidak dapat melanjutkan proses kartu kredit dikarenakan SPAJ ini belum disimpan.");
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss());
        builder.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss());
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RequestCode.REQ_PAYMENT :
                if (resultCode == RESULT_OK) {
                    E_Update update = new E_Update(this);
                    String transactionNumber = data.getStringExtra(Const.INTENT_KEY_TRANSACTION_NUMBER);
                    String paymentStatus = data.getStringExtra(Const.INTENT_KEY_PAYMENT_STATUS);
                    String paymentMessage = data.getStringExtra(Const.INTENT_KEY_PAYMENT_MESSAGE);

                    if (isPaymentSuccessful(paymentStatus)) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                String spajIdTab = espajModel.getModelID().getSPAJ_ID_TAB();
                                update.updateTransactionNumber(spajIdTab, String.valueOf(Const.CREDIT_CARD_PAYMENT_CODE), transactionNumber);
                                showAlertDialog("Pembayaran berhasil", "Pembayaran berhasil dilakukan. Anda dapat melanjutkan mengisi SPAJ dan kemudian melakukan submit");
                            }
                        }, 7000);

                    } else {
                        showAlertDialog("Terjadi kesalahan dalam pembayaran", "Kode error "+paymentStatus+". "+paymentMessage);
                    }

                }
                break;

                default:
                    break;
        }

    }
}
