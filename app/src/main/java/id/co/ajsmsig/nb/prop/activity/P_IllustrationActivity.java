package id.co.ajsmsig.nb.prop.activity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.SmartViewPager;
import id.co.ajsmsig.nb.prop.adapter.P_AdapterIlustrasiTab;
import id.co.ajsmsig.nb.prop.fragment.P_IllustrationLinkFragmentV2;
import id.co.ajsmsig.nb.prop.fragment.P_InvestationIllustraionFragment;
import id.co.ajsmsig.nb.prop.fragment.P_ListImageManfaatFragment;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.IllustrationResultVO;

public class P_IllustrationActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = P_IllustrationActivity.class.getSimpleName();
    @BindView(R.id.container)
    SmartViewPager mViewPager;

    @BindView(R.id.iv_previous)
    ImageView iv_previous;
    @BindView(R.id.iv_next)
    ImageView iv_next;

    public static Bundle myBundle;

    private int currentPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_activity_illustration);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        myBundle = getIntent().getExtras();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        Bundle bundle = getIntent().getExtras();
        @SuppressWarnings("unchecked")
        HashMap<String, Object> iHash = (HashMap<String, Object>) bundle.getSerializable(getString(R.string.Illustrasi));
        String validityMsg = null;
        if (iHash != null) {
            IllustrationResultVO illustrationResult = (IllustrationResultVO) iHash.get(getString(R.string.Illustration1));
            validityMsg = illustrationResult.getValidityMsg();
            if(validityMsg!=null){
                TextView tv_message = findViewById(R.id.tv_message);
                if (validityMsg.trim().length() != 0) {

                    tv_message.setText(validityMsg);
                    tv_message.setBackgroundResource(R.color.validity_msg);
                    tv_message.setPadding(
                            (int) getResources().getDimension(R.dimen.activity_vertical_margin),
                            (int) getResources().getDimension(R.dimen.activity_vertical_margin),
                            (int) getResources().getDimension(R.dimen.activity_vertical_margin),
                            (int) getResources().getDimension(R.dimen.activity_vertical_margin)
                    );
                    tv_message.setVisibility(View.VISIBLE);
                }else {
                    tv_message.setVisibility(View.GONE);
                }
            }
        }
        P_ProposalModel proposalModel = bundle.getParcelable(getString(R.string.proposal_model));




        iv_previous.setVisibility(View.GONE);

//        // Set up the ViewPager with the sections adapter.
//        Fragment[] fragments = new Fragment[]{
////                new P_ListImageManfaatFragment(),
//                new P_IllustrationLinkFragmentV2(),
////                new P_IllustrationLinkFragment(),
//                new P_InvestationIllustraionFragment()
//        };

        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new P_ListImageManfaatFragment());
        fragments.add(new P_IllustrationLinkFragmentV2());
//        fragments.add(new P_IllustrationLinkFragment());

        assert proposalModel != null;
        if (proposalModel.getMst_proposal_product_ulink().size() > 0) {
            fragments.add(new P_InvestationIllustraionFragment());
        }

        final P_AdapterIlustrasiTab adapterIlustrasiTab = new P_AdapterIlustrasiTab(getSupportFragmentManager(), fragments);
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(adapterIlustrasiTab);
        if (mViewPager.getAdapter().getCount() == 1) iv_next.setVisibility(View.GONE);

        iv_previous.setOnClickListener(this);
        iv_next.setOnClickListener(this);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;

                if (position == 0) {
                    iv_previous.setVisibility(View.GONE);
                    if ((position + 1) == mViewPager.getAdapter().getCount()) {
                        iv_next.setVisibility(View.GONE);
                    } else {
                        iv_next.setVisibility(View.VISIBLE);
                    }
                } else if (position == mViewPager.getAdapter().getCount() - 1) {
                    iv_previous.setVisibility(View.VISIBLE);
                    iv_next.setVisibility(View.GONE);
                } else {
                    iv_previous.setVisibility(View.VISIBLE);
                    iv_next.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_previous:
                mViewPager.setCurrentItem(currentPosition - 1, true);
                break;
            case R.id.iv_next:
                mViewPager.setCurrentItem(currentPosition + 1, true);
                break;
            default:
                break;
        }
    }
}
