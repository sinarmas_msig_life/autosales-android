package id.co.ajsmsig.nb.prop.fragment;

import android.app.Dialog;
import android.database.Cursor;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.prop.adapter.P_AdapterListTopUp;
import id.co.ajsmsig.nb.prop.method.MoneyTextWatcher;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductTopUpModel;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;

import static id.co.ajsmsig.nb.prop.model.hardCode.P_FragmentPagePosition.DATA_PROPOSAL_PAGE;

/**
 * Created by faiz_f on 03/02/2017.
 */

public class P_TopUpFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = P_TopUpFragment.class.getSimpleName();

    @BindView(R.id.lv_tp)
    ListView lv_tp;
    @BindView(R.id.btn_lanjut)
    Button btn_lanjut;
    @BindView(R.id.btn_kembali)
    Button btn_kembali;
    @BindView(R.id.tv_error_total)
    TextView tv_error_total;
    @BindView(R.id.tv_total_top_up)
    TextView tv_total_top_up;
    @BindView(R.id.tv_total_penarikan)
    TextView tv_total_penarikan;

    private P_ProposalModel proposalModel;
    private P_MstDataProposalModel mstDataProposalModel;
    private P_MstProposalProductModel mstProposalProductModel;
    private ArrayList<P_MstProposalProductTopUpModel> mstProposalProductTopUpModels;
    private P_AdapterListTopUp adapterListTopUp;

    public P_TopUpFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.p_fragment_top_up, container, false);
        ButterKnife.bind(this, view);
        ((P_MainActivity) getActivity()).setSecondToolbarTitle(getString(R.string.fragment_title_topup_proposal));
        loadMyBundle();

//        btn_lanjut.setOnClickListener(this);
//        btn_kembali.setOnClickListener(this);
        getActivity().findViewById(R.id.btn_lanjut).setOnClickListener(this);
        getActivity().findViewById(R.id.btn_kembali).setOnClickListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setView();
    }

    private void loadMyBundle() {

        proposalModel = P_MainActivity.myBundle.getParcelable(getString(R.string.proposal_model));
        mstDataProposalModel = proposalModel != null ? proposalModel.getMst_data_proposal() : null;
        mstProposalProductModel = proposalModel != null ? proposalModel.getMst_proposal_product() : null;
        mstProposalProductTopUpModels = proposalModel != null ? proposalModel.getMst_proposal_product_topup() : null;


    }

    private void setView() {
        ((P_MainActivity) getActivity()).getSecond_toolbar().setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_save:
                        saveToMyBundle();
                        P_MainActivity.saveState(getContext(), true);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        adapterListTopUp = new P_AdapterListTopUp(getContext(), R.layout.p_fragment_topup_list, mstProposalProductTopUpModels, P_TopUpFragment.this);


        lv_tp.setAdapter(adapterListTopUp);


        calculateFirstTotalList(mstProposalProductTopUpModels);

        lv_tp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
//                Toast.makeText(Prop_Activity_TopUp.this, "Show", Toast.LENGTH_SHORT).show();
                P_MstProposalProductTopUpModel mstProposalProductTopUpModel = (P_MstProposalProductTopUpModel) parent.getItemAtPosition(position);
                callDialogTP(position, mstProposalProductTopUpModel);
            }
        });


    }

    private void calculateFirstTotalList(ArrayList<P_MstProposalProductTopUpModel> mstProposalProductTopUpModels) {
//        pada saat load pertama totalnya harus diitung manual dulu
        BigDecimal totalTP = BigDecimal.ZERO;
        BigDecimal totalTR = BigDecimal.ZERO;
        for (P_MstProposalProductTopUpModel mstProposalProductTopUpModel : mstProposalProductTopUpModels) {
            if (mstProposalProductTopUpModel.getSelected()) {
                totalTP  = totalTP.add(new BigDecimal(mstProposalProductTopUpModel.getTopup()));
                totalTR = totalTR.add(new BigDecimal(mstProposalProductTopUpModel.getTarik()));
            }
        }
        adapterListTopUp.setTotalTP(totalTP);
        adapterListTopUp.setTotalTR(totalTR);
        setTotal(totalTP, totalTR);
    }

    private void attemptOk() {
        boolean success;

        success = isTotalOkay(true);

        if (success) {
            goToNextPage();
        }
    }

    private void goToNextPage() {
        /*first check if there are any possible fund*/
        QueryUtil queryUtil = new QueryUtil(getContext());

        Fragment fragment;

        Cursor riderCursor = queryUtil.query(
                getString(R.string.TABLE_LST_BISNIS_RIDER),
                new String[]{"RIDER_ID"},
                getString(R.string.LSBS_ID) + " = ? AND " + getString(R.string.LSDBS_NUMBER) + " = ?",
                new String[]{String.valueOf(mstProposalProductModel.getLsbs_id()), String.valueOf(mstProposalProductModel.getLsdbs_number())},
                null
        );
        int riderCount = riderCursor.getCount();
        if (riderCount > 0) {
            fragment = new P_RiderFragment();
        } else {
            saveToMyBundle();
            P_MainActivity.saveState(getContext(), true);
            fragment = new P_ResultFragment();
        }


        ((P_MainActivity) getActivity()).setFragmentBackStack(fragment, "fragment");
    }


    @Override
    public void onPause() {
        super.onPause();
        saveToMyBundle();
    }

    public void saveToMyBundle() {
        mstDataProposalModel.setLast_page_position(DATA_PROPOSAL_PAGE);
        proposalModel.setMst_data_proposal(mstDataProposalModel);
        proposalModel.setMst_proposal_product_topup(adapterListTopUp.getMstProposalProductTopUpModels());
        P_MainActivity.myBundle.putParcelable(getString(R.string.proposal_model), proposalModel);
    }


    public void setTotal(BigDecimal totalTP, BigDecimal totalPenarikan) {
        String sTotalTopUp = (totalTP.compareTo(BigDecimal.ZERO) == 0) ? "" : StaticMethods.toMoney(totalTP);
        String sTotalPenarikan = (totalPenarikan.compareTo(BigDecimal.ZERO) == 0) ? "" : StaticMethods.toMoney(totalPenarikan);
        tv_total_top_up.setText(sTotalTopUp);
        tv_total_penarikan.setText(sTotalPenarikan);
    }

    private boolean isTotalOkay(boolean success) {
        tv_error_total.setText(null);
        long totalTopup = ((tv_total_top_up.getText().toString().trim().length() == 0) ? 0 : StaticMethods.toLong(tv_total_top_up.getText().toString()));
        long totalPenarikan = (tv_total_penarikan.getText().toString().trim().length() == 0) ? 0 : StaticMethods.toLong(tv_total_penarikan.getText().toString());

        long nilaiTotalMax = (mstProposalProductModel.getLku_id().equals("01")) ? 50000000000L : 5000000L;

        if (totalTopup > nilaiTotalMax || totalPenarikan > nilaiTotalMax) {
            success = false;
            tv_error_total.setText(getString(R.string.nilai_max) + " " + StaticMethods.toMoney(nilaiTotalMax));
        }

        return success;
    }

    private Dialog myDialog;

    private void callDialogTP(final int position, P_MstProposalProductTopUpModel mstProposalProductTopUpModel) {
        myDialog = new Dialog(getContext());
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        myDialog.setContentView(R.layout.p_fragment_topup_dialog);
        myDialog.setCancelable(true);

        Button btn_c = myDialog.findViewById(R.id.btn_c);
        Button btn_o = myDialog.findViewById(R.id.btn_o);

        TextView tv_title = myDialog.findViewById(R.id.tv_title);
        final EditText et_tp = myDialog.findViewById(R.id.et_tp);
        final EditText et_tr = myDialog.findViewById(R.id.et_tr);

        et_tp.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        et_tr.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

        final TextView tv_error_tp = myDialog.findViewById(R.id.tv_error_tp);
        final TextView tv_error_tr = myDialog.findViewById(R.id.tv_error_tr);

        et_tp.addTextChangedListener(new MoneyTextWatcher(et_tp));
        et_tr.addTextChangedListener(new MoneyTextWatcher(et_tr));
        et_tp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tv_error_tp.setVisibility(View.GONE);
            }
        });
        et_tr.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tv_error_tr.setVisibility(View.GONE);
            }
        });

        tv_title.setText("Tahun" + " " + mstProposalProductTopUpModel.getThn_ke());
        et_tp.setText((new BigDecimal(mstProposalProductTopUpModel.getTopup()).compareTo(BigDecimal.ZERO) == 0) ? "" : String.valueOf(mstProposalProductTopUpModel.getTopup()));
        et_tr.setText((new BigDecimal(mstProposalProductTopUpModel.getTarik()).compareTo(BigDecimal.ZERO) == 0) ? "" : String.valueOf(mstProposalProductTopUpModel.getTarik()));


        // retrieve display dimensions
        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(myDialog.getWindow().getAttributes());
        lp.width = (int) (displayRectangle.width() * 0.8f);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        myDialog.show();
        myDialog.getWindow().setAttributes(lp);


        btn_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialog.cancel();
            }
        });

        btn_o.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean success = true;
                et_tp.setError(null);
                et_tr.setError(null);

                BigDecimal topup = (et_tp.getText().toString().trim().length() == 0) ? BigDecimal.ZERO : StaticMethods.toBigDecimal(et_tp.getText().toString());
                BigDecimal tarik = (et_tr.getText().toString().trim().length() == 0) ? BigDecimal.ZERO : StaticMethods.toBigDecimal(et_tr.getText().toString());

//                Log.d(TAG, "onClick: " + et_tr.getText().toString());
//                Log.d(TAG, "onClick: " + StaticMethods.toBigDecimal(et_tr.getText().toString()));

                BigDecimal nilaiMin = (mstProposalProductModel.getLku_id().equals("01")) ? BigDecimal.valueOf(1000000) : BigDecimal.valueOf(100);
                BigDecimal nilaiMax = (mstProposalProductModel.getLku_id().equals("01")) ? BigDecimal.valueOf(50000000000L) : BigDecimal.valueOf(5000000);
                if (topup.compareTo(BigDecimal.ZERO)!= 0) {
                    if (topup.compareTo(nilaiMin) == -1) {
                        success = false;
                        tv_error_tp.setVisibility(View.VISIBLE);
                        tv_error_tp.setText(getString(R.string.nilai_min) + " " + StaticMethods.toMoney(nilaiMin));
                    } else if (topup.compareTo(nilaiMax)==1) {
                        success = false;
                        tv_error_tp.setVisibility(View.VISIBLE);
                        tv_error_tp.setText(getString(R.string.nilai_max) + " " + StaticMethods.toMoney(nilaiMax));
                    }
                }

                if (tarik.compareTo(BigDecimal.ZERO) != 0) {
                    if (tarik.compareTo(nilaiMin)==-1) {
                        success = false;
                        tv_error_tr.setVisibility(View.VISIBLE);
                        tv_error_tr.setText(getString(R.string.nilai_min) + " " + StaticMethods.toMoney(nilaiMin));
                    } else if (tarik.compareTo(nilaiMax)==1) {
                        success = false;
                        tv_error_tr.setVisibility(View.VISIBLE);
                        tv_error_tr.setText(getString(R.string.nilai_max) + " " + StaticMethods.toMoney(nilaiMax));
                    }
                }

                if (success) {
                    adapterListTopUp.notifyDataSetChanged();
                    BigDecimal tempTotalTP = adapterListTopUp.getTotalTP().subtract(new BigDecimal(adapterListTopUp.getMstProposalProductTopUpModels().get(position).getTopup()));
                    BigDecimal tempTotalTR = adapterListTopUp.getTotalTR().subtract(new BigDecimal(adapterListTopUp.getMstProposalProductTopUpModels().get(position).getTarik()));

                    adapterListTopUp.getMstProposalProductTopUpModels().get(position).setTopup(topup.toString());
                    adapterListTopUp.getMstProposalProductTopUpModels().get(position).setTarik(tarik.toString());
                    adapterListTopUp.getMstProposalProductTopUpModels().get(position).setSelected(true);

                    if (adapterListTopUp.getMstProposalProductTopUpModels().get(position).getSelected()) {
                        topup = topup.add(tempTotalTP);
                        tarik = tarik.add(tempTotalTR);
                        adapterListTopUp.setTotalTP(topup);
                        adapterListTopUp.setTotalTR(tarik);


                        setTotal(topup, tarik);
                        Log.d("TAG", "onClick: " + topup + "   tarik : " + tarik);
                    }
                    myDialog.cancel();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_lanjut:
                Log.d("TAG", "onClick: top up button lanjut");
                attemptOk();
                break;
            case R.id.btn_kembali:
                Log.d("TAG", "onClick: top up button kembali");
                saveToMyBundle();
                ((P_MainActivity) getActivity()).onKembaliPressed();
                break;
            default:
                throw new IllegalArgumentException("Salah Id");
        }
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.menu_va_proposal, menu);
//    }

    public void setFragment(Fragment fragment, String fragmentTag) {
        if (fragment != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
            fragmentTransaction.replace(R.id.fl_container, fragment);
            fragmentTransaction.addToBackStack(fragmentTag);
            fragmentTransaction.commit();
        }
    }


//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_logout, menu);
//        super.onCreateOptionsMenu(menu,inflater);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.action_logout:
//                logout();
//                break;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

}
