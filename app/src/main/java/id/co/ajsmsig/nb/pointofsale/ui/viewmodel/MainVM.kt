package id.co.ajsmsig.nb.pointofsale.ui.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import android.databinding.Observable
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageScenario
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 21/02/18.
 */
class MainVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository): AndroidViewModel(application) {

    val currentPage: MediatorLiveData<Page> = MediatorLiveData()
    val isThereTitle: ObservableBoolean = ObservableBoolean(true)
    val subtitle: ObservableField<String> = ObservableField()
    val isThereSubtitle: ObservableBoolean = ObservableBoolean(false)
    val isSubtitleTop: ObservableBoolean = ObservableBoolean(false)
    val observablePages: MediatorLiveData<List<Page>> = MediatorLiveData()
    val observablePageScenarios: MediatorLiveData<List<PageScenario>> = MediatorLiveData()
    val isHome: ObservableBoolean = ObservableBoolean(true)

    var tahapanKehidupan: String? = null
    var kebutuhanPrioritasMasaDepan: String? = null

    init {
        observablePages.value = null
        observablePageScenarios.value = null

        val pages = repository.getAllPages()
        observablePages.addSource(pages, observablePages::setValue)

        val pageScenarios = repository.getAllPageScenarios()
        observablePageScenarios.addSource(pageScenarios, observablePageScenarios::setValue)

        subtitle.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(p0: Observable?, p1: Int) {
                isThereSubtitle.set(subtitle.get() != null)
            }

        })
    }

    fun hideSubtitle() {
        subtitle.set(null)
    }

    fun subtitleTop() {
        isSubtitleTop.set(true)
    }
    fun subtitleBottom() {
        isSubtitleTop.set(false)
    }
}