package id.co.ajsmsig.nb.prop.method.calculateIlustration;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.model.P_IllustrationDevelopmentFundModel;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.IllustrationResultVO;


///**
// * Created by faiz_f on 05/08/2016.
// */
public class IlustrasionValue {
    private Context context;
    private String no_proposal;

    public IlustrasionValue(Context context, String no_proposal) {
        this.context = context;
        this.no_proposal = no_proposal;
    }

    public HashMap<String, Object> calculateIllustration() {
        IllustrationFormula illustrationFormula;

        HashMap<String, Integer> idProduct = new P_Select(context).selectProductIdByNoProposal(no_proposal);
        int lsbsId = idProduct.get(context.getString(R.string.LSBS_ID));
//        int lsdbsNumber = idProduct.get(R.string.LSDBS_NUMBER);
        int lsdbsNumber = idProduct.get(context.getString(R.string.LSDBS_NUMBER));

        switch (lsbsId){
            case 120://simpol
                illustrationFormula = new IllustrationFormula120(context);
                break;
            case 212://keluarga life care plus
                illustrationFormula = new IllustrationFormula212(context);
                break;
            case 213://magna link
                illustrationFormula = new IllustrationFormulaMagnaLink(context);
                break;
            case 216://magna link syariah
                illustrationFormula = new IllustrationFormulaMagnaLink(context);
                break;
            case 134://keluarga prime link
                illustrationFormula = new IllustrationFormula134(context);
                break;
            case 215: //3 PREMIUM
                illustrationFormula = new IllustrationFormula134(context);
                break;
            case 219://simas kids syariah
                illustrationFormula = new IllustrationFormula208(context);
                break;
            case 190:
                illustrationFormula = new IllustrationFormula190(context);
                break;
            case 208://keluarga kids
                illustrationFormula = new IllustrationFormula208(context);
                break;
            case 220://Jempol //4B Smile Protection
                if (lsdbsNumber == 4) {
                    illustrationFormula = new IllustrationFormula220B(context);
                } else {
                    illustrationFormula = new IllustrationFormula220(context);
                }
                break;
            case 223://smile life syariah
                illustrationFormula = new IllustrationFormula212(context);
                break;
            case 224://smile Link Pro Syariah //2 JEMPOL
                if (lsdbsNumber == 3) { // b smile protection syariah
                    illustrationFormula = new IllustrationFormula220B(context);
                } else {
                    illustrationFormula = new IllustrationFormula220(context);
                }
                break;
            case 116:// smile link 88
                illustrationFormula = new IllustrationFormula116(context);
                break;
            case 217:// smile link pro 100
                illustrationFormula = new IllustrationFormula217(context);
                break;
            default:
                illustrationFormula = new IllustrationFormula(context);
                break;
        }


        return illustrationFormula.getIllustrationResult(no_proposal);
    }
}
