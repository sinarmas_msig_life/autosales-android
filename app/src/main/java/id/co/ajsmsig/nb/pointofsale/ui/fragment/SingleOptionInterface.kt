package id.co.ajsmsig.nb.pointofsale.ui.fragment

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */
interface SingleOptionInterface: BaseValidationInterface {

    fun optionSelected() : PageOption?

    override fun validationMessageIDRes(): Int? {
        return R.string.validation_message
    }

    override fun canNext(): Boolean {
        return optionSelected() != null
    }
}