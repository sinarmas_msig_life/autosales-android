/**
 * @author Eriza Siti Mulyani
 */
package id.co.ajsmsig.nb.espaj.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelID implements Parcelable {
    private String SPAJ_ID_TAB = "";
    private String SPAJ_ID = "";
    private String proposal = "";
    private String proposal_tab = "";
    private String imei = "";
    private String va_number = "";
    private int flag_aktif;
    private int flag_dokumen = 0;
    private int jns_spaj = 0;
    private int flag_bayar = 0;
    private String id_crm = "";
    private String id_crm_tab = "";
    private int flag_proposal = 0;
    private String sertifikat = "";
    private int flag_sertifikat = 0;
    private int count_sertifikat = 0;
    private String tgl_sertifikat = "";
    private boolean hasval; //untuk mengecek sudah masuk halaman terakhir atau belum
    private int page_position = 0; //untuk mengecek di halaman berapa espaj
    private Boolean val_dp = false;
    private Boolean val_qu = false;
    private int flag_pos = 0;


    public ModelID() {
        super();
    }

    protected ModelID(Parcel in) {
        SPAJ_ID_TAB = in.readString();
        SPAJ_ID = in.readString();
        proposal = in.readString();
        proposal_tab = in.readString();
        imei = in.readString();
        va_number = in.readString();
        flag_aktif = in.readInt();
        flag_dokumen = in.readInt();
        jns_spaj = in.readInt();
        flag_bayar = in.readInt();
        id_crm = in.readString();
        id_crm_tab = in.readString();
        flag_proposal = in.readInt();
        sertifikat = in.readString();
        flag_sertifikat = in.readInt();
        count_sertifikat = in.readInt();
        tgl_sertifikat = in.readString();
        hasval = in.readByte() != 0;
        page_position = in.readInt();
        byte tmpVal_dp = in.readByte();
        val_dp = tmpVal_dp == 0 ? null : tmpVal_dp == 1;
        byte tmpVal_qu = in.readByte();
        val_qu = tmpVal_qu == 0 ? null : tmpVal_qu == 1;
        flag_pos = in.readInt();
    }

    public static final Creator<ModelID> CREATOR = new Creator<ModelID>() {
        @Override
        public ModelID createFromParcel(Parcel in) {
            return new ModelID(in);
        }

        @Override
        public ModelID[] newArray(int size) {
            return new ModelID[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(SPAJ_ID_TAB);
        parcel.writeString(SPAJ_ID);
        parcel.writeString(proposal);
        parcel.writeString(proposal_tab);
        parcel.writeString(imei);
        parcel.writeString(va_number);
        parcel.writeInt(flag_aktif);
        parcel.writeInt(flag_dokumen);
        parcel.writeInt(jns_spaj);
        parcel.writeInt(flag_bayar);
        parcel.writeString(id_crm);
        parcel.writeString(id_crm_tab);
        parcel.writeInt(flag_proposal);
        parcel.writeString(sertifikat);
        parcel.writeInt(flag_sertifikat);
        parcel.writeInt(count_sertifikat);
        parcel.writeString(tgl_sertifikat);
        parcel.writeByte((byte) (hasval ? 1 : 0));
        parcel.writeInt(page_position);
        parcel.writeByte((byte) (val_dp == null ? 0 : val_dp ? 1 : 2));
        parcel.writeByte((byte) (val_qu == null ? 0 : val_qu ? 1 : 2));
        parcel.writeInt(flag_pos);
    }

    public String getSPAJ_ID_TAB() {
        return SPAJ_ID_TAB;
    }

    public void setSPAJ_ID_TAB(String SPAJ_ID_TAB) {
        this.SPAJ_ID_TAB = SPAJ_ID_TAB;
    }

    public String getSPAJ_ID() {
        return SPAJ_ID;
    }

    public void setSPAJ_ID(String SPAJ_ID) {
        this.SPAJ_ID = SPAJ_ID;
    }

    public String getProposal() {
        return proposal;
    }

    public void setProposal(String proposal) {
        this.proposal = proposal;
    }

    public String getProposal_tab() {
        return proposal_tab;
    }

    public void setProposal_tab(String proposal_tab) {
        this.proposal_tab = proposal_tab;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getVa_number() {
        return va_number;
    }

    public void setVa_number(String va_number) {
        this.va_number = va_number;
    }

    public int getFlag_aktif() {
        return flag_aktif;
    }

    public void setFlag_aktif(int flag_aktif) {
        this.flag_aktif = flag_aktif;
    }

    public int getFlag_dokumen() {
        return flag_dokumen;
    }

    public void setFlag_dokumen(int flag_dokumen) {
        this.flag_dokumen = flag_dokumen;
    }

    public int getJns_spaj() {
        return jns_spaj;
    }

    public void setJns_spaj(int jns_spaj) {
        this.jns_spaj = jns_spaj;
    }

    public int getFlag_bayar() {
        return flag_bayar;
    }

    public void setFlag_bayar(int flag_bayar) {
        this.flag_bayar = flag_bayar;
    }

    public String getId_crm() {
        return id_crm;
    }

    public void setId_crm(String id_crm) {
        this.id_crm = id_crm;
    }

    public String getId_crm_tab() {
        return id_crm_tab;
    }

    public void setId_crm_tab(String id_crm_tab) {
        this.id_crm_tab = id_crm_tab;
    }

    public int getFlag_proposal() {
        return flag_proposal;
    }

    public void setFlag_proposal(int flag_proposal) {
        this.flag_proposal = flag_proposal;
    }

    public String getSertifikat() {
        return sertifikat;
    }

    public void setSertifikat(String sertifikat) {
        this.sertifikat = sertifikat;
    }

    public int getFlag_sertifikat() {
        return flag_sertifikat;
    }

    public void setFlag_sertifikat(int flag_sertifikat) {
        this.flag_sertifikat = flag_sertifikat;
    }

    public int getCount_sertifikat() {
        return count_sertifikat;
    }

    public void setCount_sertifikat(int count_sertifikat) {
        this.count_sertifikat = count_sertifikat;
    }

    public String getTgl_sertifikat() {
        return tgl_sertifikat;
    }

    public void setTgl_sertifikat(String tgl_sertifikat) {
        this.tgl_sertifikat = tgl_sertifikat;
    }

    public boolean isHasval() {
        return hasval;
    }

    public void setHasval(boolean hasval) {
        this.hasval = hasval;
    }

    public int getPage_position() {
        return page_position;
    }

    public void setPage_position(int page_position) {
        this.page_position = page_position;
    }

    public Boolean getVal_dp() {
        return val_dp;
    }

    public void setVal_dp(Boolean val_dp) {
        this.val_dp = val_dp;
    }

    public Boolean getVal_qu() {
        return val_qu;
    }

    public void setVal_qu(Boolean val_qu) {
        this.val_qu = val_qu;
    }

    public int getFlag_pos() {
        return flag_pos;
    }

    public void setFlag_pos(int flag_pos) {
        this.flag_pos = flag_pos;
    }

    public static Creator<ModelID> getCREATOR() {
        return CREATOR;
    }
}