package id.co.ajsmsig.nb.crm.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.model.list.ListActivityModel;

public class C_ListActivityElvAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private ArrayList<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, ArrayList<ListActivityModel>> _listDataChild;

    public C_ListActivityElvAdapter(Context context, ArrayList<String> listDataHeader,
                                    HashMap<String, ArrayList<ListActivityModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.c_fragment_agent_list_activity_header_elv, null);
        }

        TextView tv_header_list = convertView
                .findViewById(R.id.tv_header_list);
        tv_header_list.setTypeface(null, Typeface.BOLD);
        tv_header_list.setText(headerTitle);

        ImageView iv_header_list = convertView
                .findViewById(R.id.iv_header_list);

        TextView tv_empty_notif = convertView.findViewById(R.id.tv_empty_notif);

        View divider = convertView.findViewById(R.id.divider);

        switch (groupPosition) {
            case 0:
                tv_empty_notif.setText("Tidak Ada Aktivitas Yang Akan Datang");
                break;
            case 1:
                tv_empty_notif.setText("Tidak Ada Aktifitas Hari Ini");
                break;
            case 2:
                tv_empty_notif.setText("Tidak Ada Aktifitas Lampau");
                break;
            default:
                break;
        }

        if (isExpanded) {
            iv_header_list.setImageResource(R.drawable.ic_expand_less_light_grey);
            int emptyNotifvisibility;
            int dividerVisibility;
            if (getChildrenCount(groupPosition) == 0) {
                emptyNotifvisibility = View.VISIBLE;
                dividerVisibility = View.VISIBLE;
            } else {
                emptyNotifvisibility = View.GONE;
                dividerVisibility = View.INVISIBLE;
            }
            divider.setVisibility(dividerVisibility);
            tv_empty_notif.setVisibility(emptyNotifvisibility);
        } else {
            iv_header_list.setImageResource(R.drawable.ic_expand_more_light_grey);
            divider.setVisibility(View.VISIBLE);
            tv_empty_notif.setVisibility(View.GONE);
        }


        return convertView;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.c_fragment_agent_list_activity_content_evl, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);


        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ListActivityModel listActivityModel = (ListActivityModel) getChild(groupPosition, childPosition);

        String aktifitas = new C_Select(_context).selectActivityLabel(listActivityModel.getSLA_SUBTYPE());
        String tanggal = listActivityModel.getSLA_SDATE();

        holder.tv_aktivitas.setText(aktifitas);
        holder.tv_tanggal.setText(tanggal);

        if (isLastChild) {
            holder.vDivider.setVisibility(View.VISIBLE);
        } else {
            holder.vDivider.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.tv_aktivitas)
        TextView tv_aktivitas;
        @BindView(R.id.tv_tanggal)
        TextView tv_tanggal;
        @BindView(R.id.child_divider)
        View vDivider;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void updateList(ArrayList<String> listDataHeader, HashMap<String, ArrayList<ListActivityModel>> listChildData) {
        this._listDataHeader.clear();
        this._listDataHeader.addAll(listDataHeader);

        this._listDataChild.clear();
        this._listDataChild = listChildData;

        this.notifyDataSetChanged();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }


    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}