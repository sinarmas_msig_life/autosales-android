/**
 * @author Eriza Siti Mulyani
 *
 */
package id.co.ajsmsig.nb.espaj.model.arraylist;

public class ModelBank{
	private int LSBP_ID;
	private String LSBP_NAMA;
	private int LSBP_PANJANG_REKENING;
	private int LSBP_MIN_PANJANG_REKENING;

	public ModelBank() {
	}

	public ModelBank(int LSBP_ID, String LSBP_NAMA, int LSBP_PANJANG_REKENING, int LSBP_MIN_PANJANG_REKENING) {
		this.LSBP_ID = LSBP_ID;
		this.LSBP_NAMA = LSBP_NAMA;
		this.LSBP_PANJANG_REKENING = LSBP_PANJANG_REKENING;
		this.LSBP_MIN_PANJANG_REKENING = LSBP_MIN_PANJANG_REKENING;
	}

	public int getLSBP_ID() {
		return LSBP_ID;
	}

	public void setLSBP_ID(int LSBP_ID) {
		this.LSBP_ID = LSBP_ID;
	}

	public String getLSBP_NAMA() {
		return LSBP_NAMA;
	}

	public void setLSBP_NAMA(String LSBP_NAMA) {
		this.LSBP_NAMA = LSBP_NAMA;
	}

	public int getLSBP_PANJANG_REKENING() {
		return LSBP_PANJANG_REKENING;
	}

	public void setLSBP_PANJANG_REKENING(int LSBP_PANJANG_REKENING) {
		this.LSBP_PANJANG_REKENING = LSBP_PANJANG_REKENING;
	}

	public int getLSBP_MIN_PANJANG_REKENING() {
		return LSBP_MIN_PANJANG_REKENING;
	}

	public void setLSBP_MIN_PANJANG_REKENING(int LSBP_MIN_PANJANG_REKENING) {
		this.LSBP_MIN_PANJANG_REKENING = LSBP_MIN_PANJANG_REKENING;
	}
	public String toString() {
		return LSBP_NAMA;
	}
}