package id.co.ajsmsig.nb.prop.model.modelCalcIllustration;

import java.io.Serializable;

public class Prop_Model_AssurancePlanList implements Serializable
{
	private static final long serialVersionUID = 9128249777663547769L;

//	protected final Log logger = LogFactory.getLog( getClass() );

    private String value;
    private String label;
    private Integer flag_paket;
    private String flag_aktif;
    
//    public Prop_Model_AssurancePlanList()
//    {
//        logger.info( "---------- Prop_Model_OptionVO constructor is called ..." );
//    }

    public Prop_Model_AssurancePlanList(String value, String label, Integer flag_paket)
    {
        this.value = value;
        this.label = label;
        this.flag_paket = flag_paket;
    }
    
    public Prop_Model_AssurancePlanList(String value, String label )
    {
        this.value = value;
        this.label = label;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue( String value )
    {
        this.value = value;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel( String label )
    {
        this.label = label;
    }

	public Integer getFlag_paket() {
		return flag_paket;
	}

	public void setFlag_paket(Integer flag_paket) {
		this.flag_paket = flag_paket;
	}

	public String getFlag_aktif() {
		return flag_aktif;
	}

	public void setFlag_aktif(String flag_aktif) {
		this.flag_aktif = flag_aktif;
	}
    
    
}
