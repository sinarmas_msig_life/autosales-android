
package id.co.ajsmsig.nb.crm.model.apiresponse.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.bancass.Bancass;
import id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.leader.Leader;

public class Data {

    @SerializedName("agent_code")
    @Expose
    private String agentCode;
    @SerializedName("bancass")
    @Expose
    private Bancass bancass;
    @SerializedName("leader")
    @Expose
    private Leader leader;

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Bancass getBancass() {
        return bancass;
    }

    public void setBancass(Bancass bancass) {
        this.bancass = bancass;
    }

    public Leader getLeader() {
        return leader;
    }

    public void setLeader(Leader leader) {
        this.leader = leader;
    }

}
