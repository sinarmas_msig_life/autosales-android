package id.co.ajsmsig.nb.crm.method;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import id.co.ajsmsig.nb.prop.model.DropboxModel;

import static android.os.Build.VERSION_CODES.M;

/**
 * Created by faiz_f on 17/03/2017.
 */

public class StaticMethods {
    public static String getNoProposalTab() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 1);
//        int year = c.get(Calendar.YEAR);
//        int month = c.get(Calendar.MONTH);
//        int day = c.get(Calendar.DATE);
//        int hour = c.get(Calendar.HOUR);
//        int minute = c.get(Calendar.MINUTE);
//        int second = c.get(Calendar.SECOND);
//        int milliSecond = c.get(Calendar.MILLISECOND);

        String formatDate = "'Prop'.yy.MM.dd.HH.mm.ss.SSS";
        SimpleDateFormat sdf = new SimpleDateFormat(formatDate, Locale.US);

//        return "EProposalM." + year + "." + month + "." + day + "." + hour + "." + minute + "." + second + "." + milliSecond;
        return sdf.format(c.getTime());
    }


    public static Calendar toCalendar(String sDate) {
        String formatDate;
        Calendar calendar = Calendar.getInstance();
        switch (sDate.length()) {
            case 10://format standar
                formatDate = "yyyy-MM-dd";
                break;
            case 16://format standar with time
                formatDate = "yyyy-MM-dd HH:mm";
                break;
            case 19://format standar with time
                formatDate = "yyyy-MM-dd HH:mm:ss";
                break;
            case 11://format nama bulan
                formatDate = "dd-MMM-yyyy";
                break;
            case 17://format nama bulan with time
                formatDate = "dd-MMM-yyyy HH:mm";
                break;
            case 20://format nama bulan with time
                formatDate = "dd-MMM-yyyy HH:mm:ss";
                break;
            default:
                formatDate = "yyyy-MM-dd";
                break;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(formatDate, Locale.US);

        try {
            calendar.setTime(sdf.parse(sDate));// all done
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar;
    }


    public static String toStringDate(Calendar calendar, int flagFormat) {
        String formatDate;
        switch (flagFormat) {
            case 0://format standar
                formatDate = "yyyy-MM-dd";
                break;
            case 1://format standar with time
                formatDate = "yyyy-MM-dd HH:mm:ss";
                break;
            case 2://format nama bulan
                formatDate = "dd-MMM-yyyy";
                break;
            case 3://format nama bulan with time
                formatDate = "dd-MMM-yyyy HH:mm:ss";
                break;
            case 4://format nama bulan with time no second
                formatDate = "dd-MMM-yyyy HH:mm";
                break;
            case 5:
                formatDate = "dd/MM/yyyy";
                break;
            default:
                formatDate = "yyyy-MM-dd";
                break;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(formatDate, Locale.US);

        return sdf.format(calendar.getTime());
    }

    public static Calendar toCalendarMember(String sDate) {
        Calendar calendar = Calendar.getInstance();
        String formatDate = "dd/M/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(formatDate, Locale.US);

        try {
            calendar.setTime(sdf.parse(sDate));// all done
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar;
    }

    public static Calendar toCalendarTime(String calText) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.US);
        try {
//            if (calText != null)
            calendar.setTime(sdf.parse(calText));// all done
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar;
    }

    public static Calendar toCalendarTime(String calText, boolean withSecond) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf;
        if (withSecond) {
            sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        } else {
            sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.US);
        }
        try {
//            if (calText != null)
            calendar.setTime(sdf.parse(calText));// all done
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar;
    }

    public static String toStringDate(Calendar calendar) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        return sdf.format(calendar.getTime());
    }


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static String getTodayDate() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        Calendar c = Calendar.getInstance();
//        c.add(Calendar.MONTH, 1);
        return sdf.format(c.getTime());
    }

    public static String getTodayDateTime() {
        String myFormat = "yyyy-MM-dd HH:mm:ss"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        Calendar c = Calendar.getInstance();
//        c.add(Calendar.MONTH, 1);
        return sdf.format(c.getTime());
    }

    public static boolean isTextWidgetEmpty(EditText editText) {
        return editText.getText().toString().trim().length() == 0;
    }

    public static boolean isTextWidgetEmpty(AutoCompleteTextView autoCompleteTextView) {
        return autoCompleteTextView.getText().toString().trim().length() == 0;
    }

    public static ArrayList<DropboxModel> getListGender() {
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();
        dropboxModels.add(0, new DropboxModel("Laki-laki", 1));
        dropboxModels.add(1, new DropboxModel("Perempuan", 0));

        return dropboxModels;
    }

    //method hitung usia
    public static String onCountUsia(int year, int month, int day) {
        int li_month;
        int li_Umur = 0;
        int li_add = 0;
        int li_curr_year;
        int li_curr_month;
        int li_curr_day;
        Calendar now = Calendar.getInstance();
        li_curr_year = now.get(Calendar.YEAR);
        li_curr_month = now.get(Calendar.MONTH);
        li_curr_day = now.get(Calendar.DATE);

        if (year != li_curr_year) {
            if (li_curr_month >= month) {
                li_Umur = li_curr_year - year;
            } else {
                li_Umur = (li_curr_year - year) - 1;
                li_add = 12;
            }
            li_month = li_curr_month + li_add - month;
            if (li_month >= 6) {
                if (li_month == 6) {
                    if ((li_curr_day - day) >= 0) {
                        li_Umur = li_Umur + 1;
                    }
                } else {
                    li_Umur = li_Umur + 1;
                }

            }
        }
        if (li_Umur < 0) {
            li_Umur = 0;
        }
        return String.valueOf(li_Umur);
    }

    public static String onCountUsia(Calendar calendar) {
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);

        int li_month;
        int li_Umur = 0;
        int li_add = 0;
        int li_curr_year;
        int li_curr_month;
        int li_curr_day;
        Calendar now = Calendar.getInstance();
        li_curr_year = now.get(Calendar.YEAR);
        li_curr_month = now.get(Calendar.MONTH);
        li_curr_day = now.get(Calendar.DATE);

        if (year != li_curr_year) {
            if (li_curr_month >= month) {
                li_Umur = li_curr_year - year;
            } else {
                li_Umur = (li_curr_year - year) - 1;
                li_add = 12;
            }
            li_month = li_curr_month + li_add - month;
            if (li_month >= 6) {
                if (li_month == 6) {
                    if ((li_curr_day - day) >= 0) {
                        li_Umur = li_Umur + 1;
                    }
                } else {
                    li_Umur = li_Umur + 1;
                }

            }
        } else if (year == li_curr_year) {
            li_month = li_curr_month - month;
            if (li_month >= 6){
                li_Umur = 1;
            } else {
                li_Umur = 0;
            }
        }
        if (li_Umur < 0) {
            li_Umur = 0;
        }
        return String.valueOf(li_Umur);
    }

    public static int onCountHari(Calendar before) {
        Calendar now = Calendar.getInstance();
        long diff = now.getTimeInMillis() - before.getTimeInMillis();

        return (int) (diff / (24 * 60 * 60 * 1000));
    }

    //menghitung bulan
    public static int onCountBulan(Calendar before) {
        return Math.round( onCountHari (before)/30);
    }

    public static void backUpToJsonFile(String json, String folderName, String fileFormat) {
        File sd = Environment.getExternalStorageDirectory();
        if (sd.canWrite()) {
            Log.d("TAG", "upload: can write");
            Calendar c = Calendar.getInstance();
//        c.add(Calendar.MONTH, 1);
            String backupJsonLeadPath = "DBAutoSales/" + folderName + "/" + fileFormat + " " + StaticMethods.toStringDate(c, 3) + ".json";
            File backupJsonLead = new File(sd, backupJsonLeadPath);
            backupJsonLead.getParentFile().mkdirs();
            try {
                FileOutputStream stream = new FileOutputStream(backupJsonLead);
                stream.write(json.getBytes());
                stream.close();
                Log.d("TAG", "upload: path = " + backupJsonLead.getPath());
            } catch (IOException e) {
                Log.d("TAG", "upload: " + e.getMessage());
                e.printStackTrace();
            }
        } else {
            Log.d("TAG", "upload: can not write");
        }
    }

    public static boolean isViewVisible(View v) {
        return v.getVisibility() == View.VISIBLE;
    }

    public static long toLong(String sMoney) {
        String stringMoney = sMoney.replace(".", "").replace(",", "");
        if (stringMoney.equals("")) {
            stringMoney = "0";
        }
        return Long.parseLong(stringMoney);
    }

    public static String toStringNumber(String sMoney) {
        String stringMoney = sMoney.replace(".", "").replace(",", "");
        if (stringMoney.equals("")) {
            stringMoney = "0";
        }
        return stringMoney;
    }

    public static BigDecimal toBigDecimal(String sMoney) {
        String stringMoney = sMoney.replace(".", "").replace(",", "");
        if (stringMoney.equals("")) {
            stringMoney = "0";
        }
        return new BigDecimal(stringMoney);
    }


    public static String toMoney(BigDecimal number) {
        DecimalFormat formatter = new DecimalFormat("#,###");
        return formatter.format(number).replace(",", ".");
    }

    public static String toMoney(Long number) {
        DecimalFormat formatter = new DecimalFormat("#,###");
        return formatter.format(number).replace(",", ".");
    }

    public static String toMoney(Double number) {
        DecimalFormat formatter = new DecimalFormat("#,###");
        return formatter.format(number).replace(",", ".");
    }

    public static String toMoney(String number) {
        DecimalFormat formatter = new DecimalFormat("#,###");
        try {
            return formatter.format(formatter.parse(number)).replace(",", ".");
        } catch (ParseException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public static Rect locateView(View v) {
        int[] loc_int = new int[2];
        if (v == null) return null;
        try {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe) {
            //Happens when the view doesn't exist on screen anymore.
            return null;
        }
        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1];
        location.right = location.left + v.getWidth();
        location.bottom = location.top + v.getHeight();
        return location;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public static void openPDF(Context context, File file) {
        if (file.exists()) {
            Uri path;
            if (Build.VERSION.SDK_INT > M) {
                path = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".my.package.name.provider", file);
            } else {
                path = Uri.fromFile(file);
            }
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            try {
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
//                    Status ini maksudnya itu gak ada aplikasi yang buka pdf
                Toast.makeText(context, "Silakan download pdf reader di playstore terlebih dahulu", Toast.LENGTH_SHORT).show();
//                Log.e("TAG", "openPDF: " + e.toString());
            }
        }
    }

    public static void writeFileOnInternalStorage(Context mcoContext, String sFileName, String sBody, @NonNull boolean rewrite) {
        File file = new File(mcoContext.getFilesDir(), "mydir");
        if (!file.exists()) {
            file.mkdir();
        }

        try {
            File gpxfile = new File(file, sFileName);
            if (rewrite) {
                if (gpxfile.exists())
                    gpxfile.delete();
            }
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();

        } catch (Exception e) {
            Log.e("TAG", "wrtieFileOnInternalStorage: " + e.getMessage());
        }
    }


}
