package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.ModelID;

/**
 * Created by Bernard on 04/10/2017.
 */

public class TableMST_SPAJ {
    private Context context;

    public TableMST_SPAJ(Context context){
        this.context = context;
    }

    public ContentValues getContentValues(EspajModel me){
        ContentValues cv = new ContentValues();
        cv.put("SPAJ_ID_TAB", me.getModelID().getSPAJ_ID_TAB());
        cv.put("SPAJ_ID", me.getModelID().getSPAJ_ID());
        cv.put("IDPROPOSAL", me.getModelID().getProposal());
        cv.put("NO_PROPOSAL_TAB", me.getModelID().getProposal_tab());
        cv.put("IMEI", me.getModelID().getImei());
        cv.put("VIRTUAL_ACC", me.getModelID().getVa_number());
        cv.put("FLAG_AKTIF", me.getModelID().getFlag_aktif());
        cv.put("FLAG_DOKUMEN", me.getModelID().getFlag_dokumen());
        cv.put("JNS_SPAJ", me.getModelID().getJns_spaj());
        cv.put("FLAG_BAYAR", me.getModelID().getFlag_bayar());
        cv.put("ID_CRM", me.getModelID().getId_crm());
        cv.put("ID_CRM_TAB", me.getModelID().getId_crm_tab());
        cv.put("FLAG_PROPOSAL", me.getModelID().getFlag_proposal());
        cv.put("SERTIFIKAT", me.getModelID().getSertifikat());
        cv.put("FLAG_SERTIFIKAT", me.getModelID().getFlag_sertifikat());
        cv.put("count_sertifikat", me.getModelID().getCount_sertifikat());
        cv.put("tgl_sertifikat", me.getModelID().getTgl_sertifikat());
        cv.put("VAL_DOKUMEN", me.getModelID().getVal_dp());
        cv.put("VAL_KUISIONER", me.getModelID().getVal_qu());
//        cv.put("KODE_AGEN", me.getModelID().getIDSPAJ());
//        cv.put("OLD_SPAJ_ID", me.getModelID().getIDSPAJ());
        cv.put("PAGE_POSITION", me.getModelID().getPage_position());
        cv.put("FLAG_POS", me.getModelID().getFlag_pos());
        return cv;
    }

    public ModelID getObject(Cursor cursor){
        ModelID modelID= new ModelID();
        if (cursor != null && cursor.getCount()>0){
            cursor.moveToFirst();
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SPAJ_ID_TAB"))){
                modelID.setSPAJ_ID_TAB(cursor.getString(cursor.getColumnIndexOrThrow("SPAJ_ID_TAB")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SPAJ_ID"))){
                modelID.setSPAJ_ID(cursor.getString(cursor.getColumnIndexOrThrow("SPAJ_ID")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("IDPROPOSAL"))){
                modelID.setProposal(cursor.getString(cursor.getColumnIndexOrThrow("IDPROPOSAL")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NO_PROPOSAL_TAB"))){
                modelID.setProposal_tab(cursor.getString(cursor.getColumnIndexOrThrow("NO_PROPOSAL_TAB")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("IMEI"))){
                modelID.setImei(cursor.getString(cursor.getColumnIndexOrThrow("IMEI")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("VIRTUAL_ACC"))){
                modelID.setVa_number(cursor.getString(cursor.getColumnIndexOrThrow("VIRTUAL_ACC")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("FLAG_AKTIF"))){
                modelID.setFlag_aktif(cursor.getInt(cursor.getColumnIndexOrThrow("FLAG_AKTIF")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("FLAG_DOKUMEN"))){
                modelID.setFlag_dokumen(cursor.getInt(cursor.getColumnIndexOrThrow("FLAG_DOKUMEN")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("JNS_SPAJ"))){
                modelID.setJns_spaj(cursor.getInt(cursor.getColumnIndexOrThrow("JNS_SPAJ")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("FLAG_BAYAR"))){
                modelID.setFlag_bayar(cursor.getInt(cursor.getColumnIndexOrThrow("FLAG_BAYAR")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("ID_CRM"))){
                modelID.setId_crm(cursor.getString(cursor.getColumnIndexOrThrow("ID_CRM")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("ID_CRM_TAB"))){
                modelID.setId_crm_tab(cursor.getString(cursor.getColumnIndexOrThrow("ID_CRM_TAB")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("FLAG_PROPOSAL"))){
                modelID.setFlag_proposal(cursor.getInt(cursor.getColumnIndexOrThrow("FLAG_PROPOSAL")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SERTIFIKAT"))){
                modelID.setSertifikat(cursor.getString(cursor.getColumnIndexOrThrow("SERTIFIKAT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("FLAG_SERTIFIKAT"))){
                modelID.setFlag_sertifikat(cursor.getInt(cursor.getColumnIndexOrThrow("FLAG_SERTIFIKAT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("count_sertifikat"))){
                modelID.setCount_sertifikat(cursor.getInt(cursor.getColumnIndexOrThrow("count_sertifikat")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("tgl_sertifikat"))){
                modelID.setTgl_sertifikat(cursor.getString(cursor.getColumnIndexOrThrow("tgl_sertifikat")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PAGE_POSITION"))){
                modelID.setPage_position(cursor.getInt(cursor.getColumnIndexOrThrow("PAGE_POSITION")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("VAL_DOKUMEN"))){
                int valid = cursor.getInt(cursor.getColumnIndexOrThrow("VAL_DOKUMEN"));
//                boolean isValid = valid == 1 ? true : false;
                boolean val;
                val = valid == 1;
                modelID.setVal_dp(val);
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("VAL_KUISIONER"))){
                int valid = cursor.getInt(cursor.getColumnIndexOrThrow("VAL_KUISIONER"));
//                boolean isValid = valid == 1 ? true : false;
                boolean val;
                val = valid == 1;
                modelID.setVal_qu(val);
            }

            if (!cursor.isNull(cursor.getColumnIndexOrThrow("FLAG_POS"))){
                modelID.setFlag_pos(cursor.getInt(cursor.getColumnIndexOrThrow("FLAG_POS")));
            }

            cursor.close();
        }

        return modelID;
    }
}
