package id.co.ajsmsig.nb.prop.model.form;


import android.os.Parcel;
import android.os.Parcelable;

public class P_MstProposalProductModel implements Parcelable {
    private String no_proposal_tab;
    private String no_proposal;
    private Integer lsbs_id;
    private Integer lsdbs_number;
    private String lku_id;
    private String grp_product_name;
    private String premi;
    private Integer premi_komb;
    private String premi_pokok;
    private String premi_topup;
    private String up;
    private Integer cara_bayar;
    private Integer thn_cuti_premi;
    private Integer thn_masa_kontrak;
    private Integer thn_lama_bayar;
    private Integer inv_fix;
    private Integer inv_dynamic;
    private Integer inv_aggressive;
    private String lji_fix;
    private String lji_dynamic;
    private String lji_aggresive;
    private Integer flag_packet; // baru di tambahkan siap2u


    public P_MstProposalProductModel() {
    }


    protected P_MstProposalProductModel(Parcel in) {
        no_proposal_tab = in.readString();
        no_proposal = in.readString();
        lsbs_id = (Integer) in.readValue(Integer.class.getClassLoader());
        lsdbs_number = (Integer) in.readValue(Integer.class.getClassLoader());
        lku_id = in.readString();
        grp_product_name = in.readString();
        premi = in.readString();
        premi_komb = (Integer) in.readValue(Integer.class.getClassLoader());
        premi_pokok = in.readString();
        premi_topup = in.readString();
        up = in.readString();
        cara_bayar = (Integer) in.readValue(Integer.class.getClassLoader());
        thn_cuti_premi = (Integer) in.readValue(Integer.class.getClassLoader());
        thn_masa_kontrak = (Integer) in.readValue(Integer.class.getClassLoader());
        thn_lama_bayar = (Integer) in.readValue(Integer.class.getClassLoader());
        inv_fix = (Integer) in.readValue(Integer.class.getClassLoader());
        inv_dynamic = (Integer) in.readValue(Integer.class.getClassLoader());
        inv_aggressive = (Integer) in.readValue(Integer.class.getClassLoader());
        lji_fix = in.readString();
        lji_dynamic = in.readString();
        lji_aggresive = in.readString();
        flag_packet = in.readInt();
    }

    public static final Creator<P_MstProposalProductModel> CREATOR = new Creator<P_MstProposalProductModel>() {
        @Override
        public P_MstProposalProductModel createFromParcel(Parcel in) {
            return new P_MstProposalProductModel(in);
        }

        @Override
        public P_MstProposalProductModel[] newArray(int size) {
            return new P_MstProposalProductModel[size];
        }
    };

    public String getNo_proposal_tab() {
        return no_proposal_tab;
    }

    public void setNo_proposal_tab(String no_proposal_tab) {
        this.no_proposal_tab = no_proposal_tab;
    }

    public String getNo_proposal() {
        return no_proposal;
    }

    public void setNo_proposal(String no_proposal) {
        this.no_proposal = no_proposal;
    }

    public Integer getLsbs_id() {
        return lsbs_id;
    }

    public void setLsbs_id(Integer lsbs_id) {
        this.lsbs_id = lsbs_id;
    }

    public Integer getLsdbs_number() {
        return lsdbs_number;
    }

    public void setLsdbs_number(Integer lsdbs_number) {
        this.lsdbs_number = lsdbs_number;
    }

    public String getLku_id() {
        return lku_id;
    }

    public void setLku_id(String lku_id) {
        this.lku_id = lku_id;
    }


    public String getGrp_product_name() {
        return grp_product_name;
    }

    public void setGrp_product_name(String grp_product_name) {
        this.grp_product_name = grp_product_name;
    }


    public String getPremi() {
        return premi;
    }

    public void setPremi(String premi) {
        this.premi = premi;
    }

    public Integer getPremi_komb() {
        return premi_komb;
    }

    public void setPremi_komb(Integer premi_komb) {
        this.premi_komb = premi_komb;
    }

    public String getPremi_pokok() {
        return premi_pokok;
    }

    public void setPremi_pokok(String premi_pokok) {
        this.premi_pokok = premi_pokok;
    }

    public String getPremi_topup() {
        return premi_topup;
    }

    public void setPremi_topup(String premi_topup) {
        this.premi_topup = premi_topup;
    }

    public String getUp() {
        return up;
    }

    public void setUp(String up) {
        this.up = up;
    }

    public Integer getCara_bayar() {
        return cara_bayar;
    }

    public void setCara_bayar(Integer cara_bayar) {
        this.cara_bayar = cara_bayar;
    }

    public Integer getThn_cuti_premi() {
        return thn_cuti_premi;
    }

    public void setThn_cuti_premi(Integer thn_cuti_premi) {
        this.thn_cuti_premi = thn_cuti_premi;
    }

    public Integer getThn_masa_kontrak() {
        return thn_masa_kontrak;
    }

    public void setThn_masa_kontrak(Integer thn_masa_kontrak) {
        this.thn_masa_kontrak = thn_masa_kontrak;
    }

    public Integer getThn_lama_bayar() {
        return thn_lama_bayar;
    }

    public void setThn_lama_bayar(Integer thn_lama_bayar) {
        this.thn_lama_bayar = thn_lama_bayar;
    }

    public Integer getInv_fix() {
        return inv_fix;
    }

    public void setInv_fix(Integer inv_fix) {
        this.inv_fix = inv_fix;
    }

    public Integer getInv_dynamic() {
        return inv_dynamic;
    }

    public void setInv_dynamic(Integer inv_dynamic) {
        this.inv_dynamic = inv_dynamic;
    }

    public Integer getInv_aggressive() {
        return inv_aggressive;
    }

    public void setInv_aggressive(Integer inv_aggressive) {
        this.inv_aggressive = inv_aggressive;
    }

    public String getLji_fix() {
        return lji_fix;
    }

    public void setLji_fix(String lji_fix) {
        this.lji_fix = lji_fix;
    }

    public String getLji_dynamic() {
        return lji_dynamic;
    }

    public void setLji_dynamic(String lji_dynamic) {
        this.lji_dynamic = lji_dynamic;
    }

    public String getLji_aggresive() {
        return lji_aggresive;
    }

    public void setLji_aggresive(String lji_aggresive) {
        this.lji_aggresive = lji_aggresive;
    }

    public Integer getFlag_packet(){
        return flag_packet;
    }

    public void setFlag_packet(Integer flag_packet){
        this.flag_packet = flag_packet;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(no_proposal_tab);
        dest.writeString(no_proposal);
        dest.writeValue(lsbs_id);
        dest.writeValue(lsdbs_number);
        dest.writeString(lku_id);
        dest.writeString(grp_product_name);
        dest.writeString(premi);
        dest.writeValue(premi_komb);
        dest.writeString(premi_pokok);
        dest.writeString(premi_topup);
        dest.writeString(up);
        dest.writeValue(cara_bayar);
        dest.writeValue(thn_cuti_premi);
        dest.writeValue(thn_masa_kontrak);
        dest.writeValue(thn_lama_bayar);
        dest.writeValue(inv_fix);
        dest.writeValue(inv_dynamic);
        dest.writeValue(inv_aggressive);
        dest.writeString(lji_fix);
        dest.writeString(lji_dynamic);
        dest.writeString(lji_aggresive);
        dest.writeValue(flag_packet);
    }
}
