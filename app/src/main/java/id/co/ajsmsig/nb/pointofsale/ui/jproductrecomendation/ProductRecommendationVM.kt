package id.co.ajsmsig.nb.pointofsale.ui.jproductrecomendation

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import android.databinding.ObservableBoolean
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.ProductRecommendation
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 08/03/18.
 */
class ProductRecommendationVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository, var sharedViewModel: SharedViewModel, var mainRepository: MainRepository): AndroidViewModel(application){

    val observableProductRecommendation = MediatorLiveData<List<ProductRecommendation>>()
    val isThereProductRecommendation = ObservableBoolean(false)

    var page: Page? = null
    set(value) {

        // For kid product, query also by age
        val age = sharedViewModel.needCalculatorInputs?.firstOrNull { it.input?.description?.toLowerCase()?.contains("usia anak") == true }?.selectedValue?.get()?.toInt()


        if (sharedViewModel.selectedNeedAnalysis != null && sharedViewModel.selectedRiskProfile != null && age != null) {

            val observableProductRecommendations = repository.getProductRecommendationWithGroupId(sharedViewModel.groupId, sharedViewModel.selectedRiskProfile?.id, sharedViewModel.selectedNeedAnalysis?.id, age)
            observableProductRecommendation.addSource(observableProductRecommendations, observableProductRecommendation::setValue)

        } else if (sharedViewModel.selectedNeedAnalysis != null && sharedViewModel.selectedRiskProfile != null) {

            val observableProductRecommendations = repository.getProductRecommendationWithGroupId(sharedViewModel.groupId, sharedViewModel.selectedRiskProfile?.id, sharedViewModel.selectedNeedAnalysis?.id)
            observableProductRecommendation.addSource(observableProductRecommendations, observableProductRecommendation::setValue)

        } else if (sharedViewModel.selectedNeedAnalysis == null && sharedViewModel.selectedRiskProfile != null) {

            val observableProductRecommendations = repository.getProductRecommendationWithGroupId(sharedViewModel.groupId, sharedViewModel.selectedRiskProfile?.id)
            observableProductRecommendation.addSource(observableProductRecommendations, observableProductRecommendation::setValue)
        } else {

            val observableProductRecommendations = repository.getAllProductRecommendationWithGroupId(sharedViewModel.groupId)
            observableProductRecommendation.addSource(observableProductRecommendations, observableProductRecommendation::setValue)

        }

//        observableProducts.observeForever(object : Observer<Resource<List<Product>>> {
//            override fun onChanged(t: Resource<List<Product>>?) {
//
//                t?.data?.let { products ->
//                    products.forEach { it.Icon = "product_recommendation_1.png" }
//
//                    Transformations.map(observableProductRecommendations, { productRecommendations: List<ProductRecommendation>? ->
//                        val getProductRecommendations = productRecommendations?.filter { productRecommendation -> products.indexOfFirst { it.ProductId == productRecommendation.productId } >= 0 }
//                        getProductRecommendations?.forEach { productRecommendation ->
//                            productRecommendation.product.postValue(products.firstOrNull { it.ProductId == productRecommendation.productId })
//                        }
//                        observableProductRecommendation.value = getProductRecommendations
//                    })
//                    observableProducts.removeObserver(this)
//                }
//
//            }
//
//        })

    }
}