package id.co.ajsmsig.nb.pointofsale.ui.jproductrecomendation


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.pointofsale.adapter.ProductRecommendationOptionAdapter
import id.co.ajsmsig.nb.pointofsale.custom.flexbox.*
import id.co.ajsmsig.nb.databinding.PosFragmentProductRecomendationBinding
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository
import id.co.ajsmsig.nb.pointofsale.ui.fragment.SingleOptionFragment
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class ProductRecomendationFragment : SingleOptionFragment() {

    private lateinit var dataBinding: PosFragmentProductRecomendationBinding
    private lateinit var optionAdapter: ProductRecommendationOptionAdapter

    @Inject lateinit var repository: MainRepository

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        
        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductRecommendationVM::class.java)
        subscribeUi(viewModel)
    }
    
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_product_recomendation, container, false)

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupList(dataBinding.recyclerView)
    }
    
    fun setupList(recyclerView: RecyclerView) {
        optionAdapter = ProductRecommendationOptionAdapter()
        recyclerView.adapter = optionAdapter

        val layoutManager = FlexboxLayoutManager(activity, FlexDirection.ROW, JustifyContent.CENTER, true, 3, 2)
        recyclerView.layoutManager = layoutManager
        recyclerView.setHasFixedSize(true)
    }
    
    fun subscribeUi(viewModel: ProductRecommendationVM) {
        dataBinding.vm = viewModel

        viewModel.page = page
        viewModel.observableProductRecommendation.observe(this, Observer {
            it?.let {

                it.firstOrNull { it.productId == sharedViewModel.selectedProduct?.LsbsId && it.productGroupId == sharedViewModel.selectedProduct?.Group_ID }?.selected?.set(true)

                it.forEach {
                    val productRecommendation = it
                    repository.getProductWithId(it.productId, sharedViewModel.groupId).observe(this, Observer {
                        it?.data?.firstOrNull()?.let {
                            if (it.Icon == null) it.Icon = "product_recommendation_1.png"
                            if ((it.Icon?.contains("/upload/")) != true) it.Icon = "~/upload/" + it.Icon
                            productRecommendation.product.postValue(it)
                        }
                    })
                }
                viewModel.isThereProductRecommendation.set(it.isNotEmpty())

                optionAdapter.options = it.toTypedArray()
                viewModel.observableProductRecommendation.removeObservers(this)
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()

        sharedViewModel.selectedProduct = null
    }

    override fun optionSelected(): PageOption? {
        val viewModel = ViewModelProviders.of(this).get(ProductRecommendationVM::class.java)
        val selectedOption = viewModel.observableProductRecommendation.value?.firstOrNull { it.selected.get() }

        sharedViewModel.selectedProduct = selectedOption?.product?.value

        return null
    }

    override fun canNext(): Boolean {
        val viewModel = ViewModelProviders.of(this).get(ProductRecommendationVM::class.java)
        val selectedOption = viewModel.observableProductRecommendation.value?.firstOrNull { it.selected.get() }

        return selectedOption?.product?.value != null
    }

    override fun validationMessageIDRes(): Int? {
        return R.string.product_recommendation_validation_message
    }


}// Required empty public constructor
