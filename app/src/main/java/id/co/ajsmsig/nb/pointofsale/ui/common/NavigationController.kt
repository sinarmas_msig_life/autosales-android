package id.co.ajsmsig.nb.pointofsale.ui.common

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.util.Log
import android.view.View
import android.widget.Toast
import id.co.ajsmsig.nb.pointofsale.POSActivity
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Analytics
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageScenario
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.fragment.*
import id.co.ajsmsig.nb.pointofsale.ui.handlingobjection.HandlingObjectionListFragment
import id.co.ajsmsig.nb.pointofsale.utils.Constants
import id.co.ajsmsig.nb.pointofsale.ui.handlingobjection.HandlingObjectionDetailFragment
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.MainVM
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.PageViewModelFactory
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import kotlinx.android.synthetic.main.pos_activity_pos.*
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 25/02/18.
 */
class NavigationController
@Inject
constructor(var activity: POSActivity, val repository: PrePopulatedRepository, val viewModelFactory: PageViewModelFactory, val sharedViewModel: SharedViewModel, val mainRepository: MainRepository) {

    private val containerId: Int = R.id.container
    private val fragmentManager: FragmentManager = activity.supportFragmentManager


    private lateinit var availablePages: List<Page>
    private lateinit var pageScenarios: List<PageScenario>
    private val mainVM = ViewModelProviders.of(activity, viewModelFactory).get(MainVM::class.java)

    val isThereNextPage: Boolean
        get() {

            try {
                val currentPage = mainVM.currentPage.value
                return pageScenarios.lastOrNull { (it.pageId == currentPage?.id) } != null
            } catch (e: Exception) { return true }
        }

    init {
        repository.observablePages.observe(activity, Observer {
            it?.let {
                availablePages = it
            }
        })
        repository.observablePageScenarios.observe(activity, Observer {
            it?.let {
                pageScenarios = it
            }
        })

        fragmentManager.addOnBackStackChangedListener {

            val backstackCount = fragmentManager.backStackEntryCount
            if (backstackCount > 1) {
                showingOrDismissNextButton()
            }
        }

        fragmentManager.beginTransaction().replace(R.id.header, HeaderFragment()).commit()
    }

    fun goToHome() {
        repository.observablePages.observeForever(object : Observer<List<Page>> {
            override fun onChanged(t: List<Page>?) {
                t?.let {
                    goToPage(t.firstOrNull())
                    repository.observablePages.removeObserver(this)
                }
            }

        })
    }

    fun back() {
        activity.onBackPressed()
    }

    fun backPage() {
        fragmentManager.popBackStack()
    }

    fun next() {
        val currentFragment = activity.supportFragmentManager.findFragmentById(R.id.container)

        if (currentFragment is BaseValidationInterface) {
            val canNotNext = !(currentFragment as BaseValidationInterface).canNext()

            if (canNotNext) {

                val message = (currentFragment as BaseValidationInterface).validationMessageIDRes()
                message?.let { Toast.makeText(activity, message, Toast.LENGTH_SHORT).show() }

                return
            }

            if (currentFragment is SingleOptionInterface) {
                val selected = (currentFragment as SingleOptionInterface).optionSelected()

                val currentFragmentTitle = (currentFragment as BaseFragment).page?.title

                Log.i("INFO", "TITLE ${currentFragmentTitle}")

                val event = selected?.title
                mainRepository.sendAnalytics(Analytics(
                        agentCode = sharedViewModel.agentCode,
                        agentName = sharedViewModel.agentName,
                        group = sharedViewModel.groupId,
                        event = event,
                        timestamp = System.currentTimeMillis()
                ))
                optionSelected(selected)
                return
            }
        }

        optionSelected(null)
    }

    fun home() {
        if (sharedViewModel.userRecord?.complete == true) {
            goToHome()
        } else {
            AlertDialogFragment.create {
                if (it) goToHome()
            }.show(fragmentManager, "")
        }
    }


    fun optionSelected(option: PageOption?) {

        val currentPage = mainVM.currentPage.value

        val nextPageScenario = pageScenarios.lastOrNull { (it.pageId == currentPage?.id && it.optionId == option?.id) || (it.optionId == null && it.pageId == currentPage?.id) }

        nextPageScenario?.let {
            val pageScenario = it
            val nextPage = availablePages.firstOrNull { it.id == pageScenario.nextPageId }

            goToPage(nextPage)
        }
    }


    fun goToPage(page: Page?, args: Bundle? = null) {
        page?.let {
            val bundle = Bundle()
            bundle.putSerializable(Constants.PAGE, page)
            args?.let { bundle.putAll(it) }

            try {
                val packageName = activity::class.java.canonicalName.replace(".${activity::class.java.simpleName}", "")
                val fragmentName = packageName + ".ui." + page.type
                val fragment = Fragment.instantiate(activity, fragmentName)

                if (!(fragment is BaseFragment)) { throw Exception() }

                replaceFragment(fragment, page.type!!, bundle)

                Log.i("INFO", "Now is $fragmentName")

                showingOrDismissNextButton()
            } catch (e: Exception) { e.printStackTrace() }

        }
    }

    fun goToPageId(pageId: Int) {
        repository.observablePages.observeForever(object : Observer<List<Page>> {
            override fun onChanged(t: List<Page>?) {

                t?.let { goToPage(t.firstOrNull { it.id == pageId }) }
                repository.observablePages.removeObserver(this)
            }

        })
    }

    private fun replaceFragment(fragment: Fragment, backStateName: String, args: Bundle? = null, animate: Boolean = true) {

        fragment.arguments = args

        val fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0)

        if (!fragmentPopped && fragmentManager.findFragmentByTag(backStateName) == null) { //fragment not in back stack, create it.
            val ft = fragmentManager.beginTransaction()
            if (animate)
                ft.setCustomAnimations(R.anim.pos_slide_left_in, R.anim.pos_slide_left_out, R.anim.pos_slide_right_in, R.anim.pos_slide_right_out)
            ft.replace(containerId, fragment, backStateName)
            ft.addToBackStack(backStateName)
            ft.commit()

        }
    }

    private fun showingOrDismissNextButton() {

        activity.nextButton.visibility = if (isThereNextPage && mainVM.isHome.get() == false) View.VISIBLE else View.GONE

    }


    fun removeAllPage() {
        for (i in 1 until fragmentManager.backStackEntryCount) {
            fragmentManager.popBackStack()
        }
    }

    fun goToHandlingObjectionList() {
        repository.getPageFromType(HandlingObjectionListFragment::class.java.simpleName).observe(activity, Observer {
            goToPage(it)
        })
    }

    fun goToHandlingObjectionDetail() {
        repository.getPageFromType(HandlingObjectionDetailFragment::class.java.simpleName).observe(activity, Observer {
            goToPage(it)
        })
    }
}
