package id.co.ajsmsig.nb.pointofsale.ui.griskprofiletools

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 01/03/18.
 */
class RiskProfileToolsVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository): AndroidViewModel(application){

    var additionalText: ObservableField<String> = ObservableField()
    var imageName = ObservableField<String>()

    var page: Page? = null
    set(value) {

        additionalText.set(value?.additionalText)
        imageName.set(value?.imageName)
    }
}