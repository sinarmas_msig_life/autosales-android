package id.co.ajsmsig.nb.prop.model.modelCalcIllustration;

import java.io.Serializable;
import java.util.Date;

public class Prop_Model_S_hcpLad implements Serializable
{
	private static final long serialVersionUID = 5364300230098368962L;
	public int peserta;
    public String[] nama = new String[5 + 1]; 
    public Date[] tgl = new Date[5 + 1];
    public int[] usia = new int[5 + 1];
    public boolean changed; 
}
