package id.co.ajsmsig.nb.espaj.model.arraylist;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by eriza on 19/09/2017.
 */

public class ModelFoto implements Parcelable {

    private String id_foto = "";
    private int counter = 0;
    private String file_foto = "";
    private String judul_foto = "";
    private int flag_default = 0;
    private Boolean validation = false;
    private int flag_status = 0;

    public ModelFoto() {
    }

    protected ModelFoto(Parcel in) {
        id_foto = in.readString();
        counter = in.readInt();
        file_foto = in.readString();
        judul_foto = in.readString();
        flag_default = in.readInt();
        byte tmpValidation = in.readByte();
        validation = tmpValidation == 0 ? null : tmpValidation == 1;
        flag_status = in.readInt();
    }

    public static final Creator<ModelFoto> CREATOR = new Creator<ModelFoto>() {
        @Override
        public ModelFoto createFromParcel(Parcel in) {
            return new ModelFoto(in);
        }

        @Override
        public ModelFoto[] newArray(int size) {
            return new ModelFoto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id_foto);
        parcel.writeInt(counter);
        parcel.writeString(file_foto);
        parcel.writeString(judul_foto);
        parcel.writeInt(flag_default);
        parcel.writeByte((byte) (validation == null ? 0 : validation ? 1 : 2));
        parcel.writeInt(flag_status);
    }

    public String getId_foto() {
        return id_foto;
    }

    public void setId_foto(String id_foto) {
        this.id_foto = id_foto;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getFile_foto() {
        return file_foto;
    }

    public void setFile_foto(String file_foto) {
        this.file_foto = file_foto;
    }

    public String getJudul_foto() {
        return judul_foto;
    }

    public void setJudul_foto(String judul_foto) {
        this.judul_foto = judul_foto;
    }

    public int getFlag_default() {
        return flag_default;
    }

    public void setFlag_default(int flag_default) {
        this.flag_default = flag_default;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public int getFlag_status() {
        return flag_status;
    }

    public void setFlag_status(int flag_status) {
        this.flag_status = flag_status;
    }

    public static Creator<ModelFoto> getCREATOR() {
        return CREATOR;
    }
}
