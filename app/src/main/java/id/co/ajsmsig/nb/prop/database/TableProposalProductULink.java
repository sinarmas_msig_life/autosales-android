package id.co.ajsmsig.nb.prop.database;
/*
 *Created by faiz_f on 10/05/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductUlinkModel;

public class TableProposalProductULink {
    private Context context;

    public TableProposalProductULink(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(P_MstProposalProductUlinkModel p_mstProposalProductUlinkModel){
        ContentValues cv = new ContentValues();
        cv.put(context.getString(R.string.NO_PROPOSAL_TAB), p_mstProposalProductUlinkModel.getNo_proposal_tab());
        cv.put(context.getString(R.string.NO_PROPOSAL), p_mstProposalProductUlinkModel.getNo_proposal());
        cv.put(context.getString(R.string.LJI_ID), p_mstProposalProductUlinkModel.getLji_id());
        cv.put(context.getString(R.string.MDU_PERSEN), p_mstProposalProductUlinkModel.getMdu_persen());

        return cv;
    }

    public ArrayList<P_MstProposalProductUlinkModel> getObjectArray(Cursor cursor){
        ArrayList<P_MstProposalProductUlinkModel> p_mstProposalProductUlinkModels = new ArrayList<>();
        if (cursor != null && cursor.getCount()>0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                P_MstProposalProductUlinkModel p_mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB)))) {
                    p_mstProposalProductUlinkModel.setNo_proposal_tab(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL)))) {
                    p_mstProposalProductUlinkModel.setNo_proposal(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LJI_ID)))) {
                    p_mstProposalProductUlinkModel.setLji_id(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.LJI_ID))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.MDU_PERSEN)))) {
                    p_mstProposalProductUlinkModel.setMdu_persen(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.MDU_PERSEN))));
                }
                p_mstProposalProductUlinkModels.add(p_mstProposalProductUlinkModel);
                cursor.moveToNext();
            }
            // always close the cursor
            cursor.close();
        }

        return p_mstProposalProductUlinkModels;
    }
}
