package id.co.ajsmsig.nb.data;

import com.google.gson.JsonObject;

import java.util.List;

import id.co.ajsmsig.nb.crm.model.apiresponse.ListProvider;
import id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.DashboardResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.request.LoginRequest;
import id.co.ajsmsig.nb.crm.model.apiresponse.request.PostCasePegaRequest;
import id.co.ajsmsig.nb.crm.model.apiresponse.request.UpdatePasswordRequest;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.BankCabangResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.BankPusatResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.BcBranch;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.CheckForSPAJNumberValidityResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.DataVersionResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.DownloadLeadResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.LeadActivityList;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.LoginResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.PostCasePegaResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.Referral;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.ReferralResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.SaveProposalDataResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.SertifikatResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.SubmitResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.TrackPolisResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.DownloadActivityTypeForDropdownResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.DownloadSubActivityTypeForDropdownResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.RequestLoginResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.UpdatePasswordResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.response.UploadLeadResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.userlogin.UserLoginResponse;
import id.co.ajsmsig.nb.espaj.model.apimodel.request.CheckPaymentSuccessfulRequest;
import id.co.ajsmsig.nb.espaj.model.apimodel.request.PayViaCcRequest;
import id.co.ajsmsig.nb.espaj.model.apimodel.response.CheckPaymentSuccessfulResponse;
import id.co.ajsmsig.nb.espaj.model.apimodel.response.PayViaCcResponse;
import id.co.ajsmsig.nb.leader.statusspajdetail.GetSpajDetailResponse;
import id.co.ajsmsig.nb.leader.subordinatedetail.GetAllSpajCountResponse;
import id.co.ajsmsig.nb.leader.subordinatedetail.PostCount;
import id.co.ajsmsig.nb.leader.subordinate.GetJenisChannelResponse;
import id.co.ajsmsig.nb.leader.subordinate.Subordinate;
import id.co.ajsmsig.nb.leader.subordinate.GetSubordinateResponse;
import id.co.ajsmsig.nb.leader.statusspaj.GetAllSpajResponse;
import id.co.ajsmsig.nb.leader.summarybc.model.SummaryData;
import id.co.ajsmsig.nb.leader.summarybc.model.SummaryResponse;
import id.co.ajsmsig.nb.prop.model.apimodel.request.ReserveVaRequest;
import id.co.ajsmsig.nb.prop.model.apimodel.request.ValidateVaRequest;
import id.co.ajsmsig.nb.prop.model.apimodel.response.ReserveVaResponse;
import id.co.ajsmsig.nb.prop.model.apimodel.response.ValidateVaResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;


/**
 * Created by fajarca on 11/18/17.
 */

public interface ApiEndpoints {

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<CheckForSPAJNumberValidityResponse> checkSPAJValidity(@Url String url, @Query("msag_id") String agentCode, @Query("reg_spaj") String regSpaj);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @POST
    Call<ReserveVaResponse> reserveVirtualAccount(@Url String url, @Body ReserveVaRequest reserveVaRequest);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @POST
    Call<ValidateVaResponse> validateVirtualAccountNumber(@Url String url, @Body ValidateVaRequest validateVaRequest);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<TrackPolisResponse> trackPolisProgress(@Url String url);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<List<BcBranch>> fetchBcBranch(@Url String url);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<List<Referral>> fetchReferral(@Url String url, @Query("limit_ajscb") String ajsCbng, @Query("limit_reff_active") boolean fetchActiveReferralOnly, @Query("limit_msag_id") String msagId);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<List<LeadActivityList>> fetchActivityListFromLead(@Url String url, @Query("msag_id") String msagId, @Query("limit") boolean useLimit, @Query("filter_sl_id") String slId, @Query("ordid") String sortBy, @Query("ordtype") String sortDirection);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<List<LeadActivityList>> fetchActivityForDashboard(@Url String url, @Query("msag_id") String msagId, @Query("limit") int limit, @Query("ordid") String sortBy, @Query("ordtype") String sortDirection, @Query("after_date") String afterDate);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<List<LeadActivityList>> fetchActivityForDashboard(@Url String url, @Query("msag_id") String msagId, @Query("limit") boolean useLimit, @Query("ordid") String sortBy, @Query("ordtype") String sortDirection, @Query("after_date") String afterDate);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:150000", "READ_TIMEOUT:150000", "WRITE_TIMEOUT:150000"})
    @GET
    Call<List<DownloadLeadResponse>> downloadLatestLead(@Url String url, @Query("msag_id") String msagId, @Query("limit") int limit, @Query("return") String returnType, @Query("ordid") String sortBy, @Query("ordtype") String sortDirection, @Query("include_address") boolean includeAddress, @Query("after_date") String afterDate);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:150000", "READ_TIMEOUT:150000", "WRITE_TIMEOUT:150000"})
    @GET
    Call<List<DownloadLeadResponse>> downloadAllLead(@Url String url, @Query("msag_id") String msagId, @Query("limit") boolean useLimit, @Query("return") String returnType, @Query("ordid") String sortBy, @Query("ordtype") String sortDirection, @Query("include_address") boolean includeAddress, @Query("after_date") String afterDate);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @POST
    Call<List<LoginResponse>> login(@Url String url, @Body LoginRequest loginRequest);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @POST
    Call<List<UserLoginResponse>> loginWith(@Url String url, @Body LoginRequest loginRequest);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @POST
    Call<SubmitResponse> submit(@Url String url, @Body RequestBody spajJson);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @POST
    Call<SertifikatResponse> sertifikat(@Url String url, @Body RequestBody spajJson);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<DataVersionResponse> getLastDataVersion(@Url String url);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<BankPusatResponse> getBankPusat(@Url String url);

    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<BankCabangResponse> getBankCabang(@Url String url);

    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<List<ReferralResponse>> getReferral(@Url String url);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @POST
    Call<List<UploadLeadResponse>> uploadListActivity(@Url String url, @Body RequestBody jsonBody);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @FormUrlEncoded
    @POST
    Call<RequestLoginResponse> requestLogin(@Url String url, @Field("key") String key, @Field("jenis") String jenis, @Field("req") String req);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @GET
    Call<List<DownloadActivityTypeForDropdownResponse>> getActivityTypeForDropdown(@Url String url, @Query("msag_id") String msagId);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @GET
    Call<List<DownloadSubActivityTypeForDropdownResponse>> getSubActivityTypeForDropdown(@Url String url, @Query("msag_id") String msagId, @Query("limit_appCode") int limitAppCode);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @POST
    Call<PayViaCcResponse> payUsingCreditCard(@Url String url, @Body PayViaCcRequest requestBody);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @POST
    Call<CheckPaymentSuccessfulResponse> checkCCPaymentSuccessful(@Url String url, @Body CheckPaymentSuccessfulRequest requestBody);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @POST
    Call<List<UpdatePasswordResponse>> updatePassword(@Url String url, @Body UpdatePasswordRequest requestBody);

    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<List<ListProvider>> downloadListProvider(@Url String url);


    //Add a custom timeout for a request
    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<DashboardResponse> getDashboardInfo(@Url String url, @Query("msag_id") String msagId);

    @Headers({"CONNECT_TIMEOUT:60000", "READ_TIMEOUT:60000", "WRITE_TIMEOUT:60000"})
    @GET
    Call<ResponseBody> performDownloadPdfProposalRetrofit(@Url String url);

    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @POST
    Call<SaveProposalDataResponse> PerformUploadProposalRetrofit(@Url String url, @Body RequestBody proposalJson);

    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @Multipart
    @POST
    Call<JsonObject> uploadImage(@Url String url, @Part MultipartBody.Part file);

    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @POST
    Call<PostCasePegaResponse> PostCasePega(@Url String url, @Header("Authorization") String authToken, @Body PostCasePegaRequest requestBody);

    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @GET
    Call<GetSubordinateResponse> getSubordinate(@Url String url, @Query("msag_id") String agentCode, @Query("jenis") String bcLoginType);

    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @GET
    Call<GetSpajDetailResponse> getDetailSpaj(@Url String url);

    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @POST
    Call<GetAllSpajResponse> postSpajList(@Url String url, @Body PostCount requestPostMenu);

    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @GET
    Call<GetAllSpajCountResponse> getAllSpajCount(@Url String url);

    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @GET
    Call<Subordinate> getName(@Url String url, @Query("msag_id") String agentCode);

    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @GET
    Call<GetJenisChannelResponse> getJenisChannel(@Url String url, @Query("msag_id") String agentCode);

    @Headers({"CONNECT_TIMEOUT:90000", "READ_TIMEOUT:90000", "WRITE_TIMEOUT:90000"})
    @GET
    Call<SummaryResponse> getSummaryReport(@Url String url, @Query("msag_id") String agentCode, @Query("jenis") String channelBank, @Query("pageNumber") int numberPage, @Query("pageSize") int pageSize);

}
