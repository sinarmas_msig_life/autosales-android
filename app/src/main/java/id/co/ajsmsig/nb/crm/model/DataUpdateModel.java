package id.co.ajsmsig.nb.crm.model;

/**
 * Created by faiz_f on 30/03/2017.
 */

public class DataUpdateModel {
    private String LOG_ID;
    private String DESCRIPTION;
    private String QUERY_VALUE;

    public DataUpdateModel(String LOG_ID, String DESCRIPTION, String QUERY_VALUE) {
        this.LOG_ID = LOG_ID;
        this.DESCRIPTION = DESCRIPTION;
        this.QUERY_VALUE = QUERY_VALUE;
    }

    public String getLOG_ID() {
        return LOG_ID;
    }

    public void setLOG_ID(String LOG_ID) {
        this.LOG_ID = LOG_ID;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getQUERY_VALUE() {
        return QUERY_VALUE;
    }

    public void setQUERY_VALUE(String QUERY_VALUE) {
        this.QUERY_VALUE = QUERY_VALUE;
    }
}
