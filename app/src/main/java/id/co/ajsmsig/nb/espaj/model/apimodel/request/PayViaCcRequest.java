package id.co.ajsmsig.nb.espaj.model.apimodel.request;

public class PayViaCcRequest {
    private String no_va;
    private String noid;
    private String payment_method;
    private String channel;
    private String transactionAmount;
    private String currency;
    private String description;

    public PayViaCcRequest(String no_va, String noid, String payment_method, String channel, String transactionAmount, String currency, String description) {
        this.no_va = no_va;
        this.noid = noid;
        this.payment_method = payment_method;
        this.channel = channel;
        this.transactionAmount = transactionAmount;
        this.currency = currency;
        this.description = description;
    }

}
