package id.co.ajsmsig.nb.crm.method;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

/**
 * Created by faiz_f on 05/04/2017.
 */

public class AutoCompleteTextViewClickListener implements AdapterView.OnItemClickListener, View.OnClickListener {
    private AutoCompleteTextView mAutoComplete;
    private AdapterView.OnItemClickListener mOriginalListener;

    public AutoCompleteTextViewClickListener(AutoCompleteTextView acTextView,
                                             AdapterView.OnItemClickListener originalListener) {
        mAutoComplete = acTextView;
        mOriginalListener = originalListener;
    }

    public void onItemClick(AdapterView<?> adView, View view, int position,
                            long id) {
        mOriginalListener.onItemClick(adView, mAutoComplete, position, id);
    }

    @Override
    public void onClick(View v) {

    }
}
