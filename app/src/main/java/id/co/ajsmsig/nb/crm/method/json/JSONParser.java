package id.co.ajsmsig.nb.crm.method.json;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

/**
 * Created by faiz_f on 22/04/2016.
 */
public class JSONParser {
    private static final String TAG = JSONParser.class.getSimpleName();

    public static HashMap<String, Object> postJsonObject(String urlInj, JSONObject urlParameter) {
        HashMap<String, Object> resultConnection = new HashMap<>();
        JSONObject object = null;
        HttpURLConnection conn = null;
        int status = 0;
        long startTime = System.nanoTime();
        try {
            URL url = new URL(urlInj);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            OutputStream os = conn.getOutputStream();
            os.write(urlParameter.toString().getBytes(StandardCharsets.UTF_8));
            os.flush();
            os.close();

            int responseCode = conn.getResponseCode();
            StringBuilder sb = new StringBuilder();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                status = 1;
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            } else if (responseCode >= 400 && responseCode < 500) {
//                status 2, it means clien is the problem
                status = 2;
            } else if (responseCode >= 500 && responseCode < 600) {
                // status 3, it mean the server is the problem.
                status = 3;
            }
            String json = sb.toString();
            Log.d(TAG, "postJsonObjectFromUrl: json =" + json);
            object = new JSONObject(json);

        } catch (Exception e) {
            status = 5;
            Log.e(TAG, "postJsonObjectFromUrl: " + e.getMessage());
        } finally {

            long endTime = System.nanoTime();
            Log.e(TAG, "close inputstream took " + (endTime - startTime) / 1000000);

            if (conn != null) {
                conn.disconnect();
            }

            resultConnection.put("object", object);
            resultConnection.put("status", status);
        }
        return resultConnection;
    }

    public static HashMap<String, Object> postJsonArray(String urlInj, JSONObject urlParameter) {
        HashMap<String, Object> resultConnection = new HashMap<>();
        JSONArray jsonArray = null;
        HttpURLConnection conn = null;
        int status = 0;
        long startTime = System.nanoTime();
        try {
            URL url = new URL(urlInj);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            OutputStream os = conn.getOutputStream();
            os.write(urlParameter.toString().getBytes(StandardCharsets.UTF_8));
            os.flush();
            os.close();

            int responseCode = conn.getResponseCode();
            StringBuilder sb = new StringBuilder();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                status = 1;
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            } else if (responseCode >= 400 && responseCode < 500) {
//                status 2, it means clien is the problem
                status = 2;
            } else if (responseCode >= 500 && responseCode < 600) {
                // status 3, it mean the server is the problem.
                status = 3;
            }
            String json = sb.toString();
            Log.d(TAG, "postJsonObjectFromUrl: json =" + json);
            jsonArray = new JSONArray(json);

        } catch (Exception e) {
            status = 5;
            Log.e(TAG, "postJsonObjectFromUrl: " + e.getMessage());
        } finally {

            long endTime = System.nanoTime();
            Log.e(TAG, "close inputstream took " + (endTime - startTime) / 1000000);

            if (conn != null) {
                conn.disconnect();
            }

            resultConnection.put("json", jsonArray);
            resultConnection.put("status", status);
        }
        return resultConnection;
    }

    public static HashMap<String, Object> getJsonObjFromUrl(String urlInj) {
        HashMap<String, Object> resultConnection = new HashMap<>();
        JSONObject object = null;
        HttpURLConnection connection = null;
        InputStream is = null;
        int status = 0;
        long startTime = System.nanoTime();
        try {
            URL url = new URL(urlInj);
            connection = (HttpURLConnection) url.openConnection();
//            connection.setReadTimeout(50000);
//            connection.setConnectTimeout(50000);
            connection.setUseCaches(false);

            is = connection.getInputStream();

            int responseCode = connection.getResponseCode();
            StringBuilder sb = new StringBuilder();
            if (responseCode == HttpURLConnection.HTTP_OK) {
//                status 1, connection success.
                status = 1;
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, StandardCharsets.ISO_8859_1));
                String line;
                int lineeee = 0;
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                    lineeee = lineeee + 1;
                }
                String json = sb.toString();
                object = new JSONObject(json);
            } else if (responseCode >= 400 && responseCode < 500) {
//                status 2, it means clien is the problem
                status = 2;
            } else if (responseCode >= 500 && responseCode < 600) {
                // status 3, it mean the server is the problem.
                status = 3;
            }
            is.close();
        } catch (JSONException | IOException e) {
            status = 5;
            Log.e(TAG, "getJsonObjFromUrl: " + e.toString());
        } finally {
            if (is != null) {
                try {
                    is.close();
                    long endTime = System.nanoTime();
                    Log.e(TAG, "close inputstream took " + (endTime - startTime) / 1000000);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                connection.disconnect();
            }

            resultConnection.put("object", object);
            resultConnection.put("status", status);
        }
        return resultConnection;
    }

    public static HashMap<String, Object> getJsonArray(String url) {
        HashMap<String, Object> hashMap = getJsonFromUrl(url);
        int status = (int) hashMap.get("status");
        String jsonString = (String) hashMap.get("json");

        JSONArray jsonArray = null;
        try {
            if (jsonString != null)
                jsonArray = new JSONArray(jsonString);
        } catch (JSONException e) {
            Log.e(TAG, "url: " + url);
            Log.e(TAG, "getJsonArray: " + e.getMessage());
            status = 5;
            e.printStackTrace();
        }

        hashMap = new HashMap<>();
        hashMap.put("json", jsonArray);
        hashMap.put("status", status);

        return hashMap;
    }

    public static HashMap<String, Object> getJsonObject(String url) {
        HashMap<String, Object> hashMap = getJsonFromUrl(url);
        int status = (int) hashMap.get("status");
        String jsonString = (String) hashMap.get("json");

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            status = 5;
            e.printStackTrace();
        }

        hashMap = new HashMap<>();
        hashMap.put("json", jsonObject);
        hashMap.put("status", status);

        return hashMap;
    }

    public static HashMap<String, Object> getJsonFromUrl(String urlInj) {
        HashMap<String, Object> resultConnection = new HashMap<>();
        HttpURLConnection connection = null;
        String json = null;
        InputStream is = null;
        int status = 0;
        long startTime = System.nanoTime();
        try {
            URL url = new URL(urlInj);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(20000);
            connection.setConnectTimeout(20000);
            connection.setUseCaches(false);

            is = connection.getInputStream();

            int responseCode = connection.getResponseCode();
            StringBuilder sb = new StringBuilder();
            if (responseCode == HttpURLConnection.HTTP_OK) {
//                status 1, connection success.
                status = 1;
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, StandardCharsets.ISO_8859_1));
                String line;
                int lineeee = 0;
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                    lineeee = lineeee + 1;
                }
                json = sb.toString();
            } else if (responseCode >= 400 && responseCode < 500) {
//                status 2, it means clien is the problem
                status = 2;
            } else if (responseCode >= 500 && responseCode < 600) {
                // status 3, it mean the server is the problem.
                status = 3;
            }
            is.close();
        } catch (IOException e) {
            status = 5;
            Log.e(TAG, "getJsonObjFromUrl: " + e.toString());
        } finally {
            if (is != null) {
                try {
                    is.close();
                    long endTime = System.nanoTime();
                    Log.e(TAG, "close inputstream took " + (endTime - startTime) / 1000000);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                connection.disconnect();
            }

            resultConnection.put("json", json);
            resultConnection.put("status", status);
        }
        return resultConnection;
    }


}
