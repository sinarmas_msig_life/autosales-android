package id.co.ajsmsig.nb.crm.model;

public class AgencySearchModel {
    private long id;
    private String leadName;
    private int leadAge;

    public AgencySearchModel(long id, String leadName, int leadAge, int leadGender) {
        this.id = id;
        this.leadName = leadName;
        this.leadAge = leadAge;
        this.leadGender = leadGender;
    }

    private int leadGender;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLeadName() {
        return leadName;
    }

    public void setLeadName(String leadName) {
        this.leadName = leadName;
    }

    public int getLeadAge() {
        return leadAge;
    }

    public void setLeadAge(int leadAge) {
        this.leadAge = leadAge;
    }

    public int getLeadGender() {
        return leadGender;
    }

    public void setLeadGender(int leadGender) {
        this.leadGender = leadGender;
    }

}
