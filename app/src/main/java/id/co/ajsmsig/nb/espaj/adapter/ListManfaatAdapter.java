package id.co.ajsmsig.nb.espaj.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.espaj.fragment.E_DetilInvestasiFragment;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDataDitunjukDI;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;

/**
 * Created by Envy on 9/30/2017.
 */

public class ListManfaatAdapter extends RecyclerView.Adapter<ListManfaatAdapter.ViewHolder> {
    private ArrayList<ModelDataDitunjukDI> list_Manfaat;
    private int resourceId;
    private Context context;
    private E_DetilInvestasiFragment detilInvestasi;

    public ArrayList<ModelDataDitunjukDI> getList_Manfaat() {
        return list_Manfaat;
    }

    public ListManfaatAdapter(Context context, int resourceId, ArrayList<ModelDataDitunjukDI> list_Manfaat, E_DetilInvestasiFragment detilInvestasi) {
        this.context = context;
        this.list_Manfaat = new ArrayList<>();
        this.list_Manfaat.addAll(list_Manfaat);
        this.resourceId = resourceId;
        this.detilInvestasi = detilInvestasi;

    }

    public void updateListTP(ArrayList<ModelDataDitunjukDI> newList) {
        list_Manfaat.clear();
        list_Manfaat.addAll(newList);
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.e_adapter_manfaat, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ModelDataDitunjukDI list = list_Manfaat.get(position);
        holder.tv_header.setText("Data Manfaat ke-" + (position + 1));

        disable(holder.cl_form_info_manfaat);
        holder.et_nama.setText(list.getNama());
        holder.et_tgl_lahir.setText(list.getTtl());
        if (list.getManfaat() == 0){
            holder.et_manfaat.setText("");
        }else {
            holder.et_manfaat.setText(new BigDecimal(list.getManfaat()).toString());
        }

        ArrayList<ModelDropdownInt> ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_relasi, 1);
        holder.ac_hubungan_dengan_pp.setText(MethodSupport.getAdapterPositionSPAJ( ListDropDownSPAJ, list.getHub_dgcalon_tt()));

        ArrayList<ModelDropdownInt> ListDropDownSPAJ2 = MethodSupport.getXML(context, R.xml.e_warganegara, 0);
        holder.ac_warga_negara.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ2, list.getWarganegara()));

        MethodSupport.OnsetTwoRadio(holder.rg_jeniskel, list.getJekel());

        holder.iv_menumanfaat_dp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(view.getContext(), view);
                popup.getMenuInflater().inflate(R.menu.menu_edit_delete, popup.getMenu());
                popup.show();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener(){

                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.edit:
                                detilInvestasi.goToForm(holder.getAdapterPosition());
                                break;
                            case R.id.delete:
                                removeItem(holder.getAdapterPosition());
                                break;
                        }
                        return true;
                    }
                });

            }
        });

    }

    @Override
    public int getItemCount() {
        return list_Manfaat.size();
    }

    public void removeItem(int position) {
        if (list_Manfaat.size() != 1) {
            list_Manfaat.remove(position);
            notifyItemRemoved(position);
        }else {
            Toast.makeText(context, "Data Manfaat Minimal Harus diisi satu", Toast.LENGTH_SHORT).show();
        }
    }

    public ArrayList<ModelDataDitunjukDI> getList() {
        return list_Manfaat;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.cl_nama)
        RelativeLayout cl_nama;
        @BindView(R.id.et_nama)
        EditText et_nama;
        @BindView(R.id.iv_circle_nama)
        ImageView iv_circle_nama;
        @BindView(R.id.tv_error_nama)
        TextView tv_error_nama;

        @BindView(R.id.cl_jenis_kelamin)
        RelativeLayout cl_jenis_kelamin;
        @BindView(R.id.iv_circle_jenis_kelamin)
        ImageView iv_circle_jenis_kelamin;
        @BindView(R.id.rg_jeniskel)
        RadioGroup rg_jeniskel;
        @BindView(R.id.rb_pria)
        RadioButton rb_pria;
        @BindView(R.id.rb_wanita)
        RadioButton rb_wanita;
        @BindView(R.id.tv_error_jenis_kelamin)
        TextView tv_error_jenis_kelamin;

        @BindView(R.id.cl_tgl_lahir)
        RelativeLayout cl_tgl_lahir;
        @BindView(R.id.iv_circle_tgl_lahir)
        ImageView iv_circle_tgl_lahir;
        @BindView(R.id.et_tgl_lahir)
        EditText et_tgl_lahir;
        @BindView(R.id.tv_error_tgl_lahir)
        TextView tv_error_tgl_lahir;
        @BindView(R.id.iv_menumanfaat_dp)
        ImageView iv_menumanfaat_dp;

        @BindView(R.id.cl_hubungan_dengan_pp)
        RelativeLayout cl_hubungan_dengan_pp;
        @BindView(R.id.iv_circle_hubungan_dengan_pp)
        ImageView iv_circle_hubungan_dengan_pp;
        @BindView(R.id.ac_hubungan_dengan_pp)
        AutoCompleteTextView ac_hubungan_dengan_pp;
        @BindView(R.id.tv_error_hubungan_dengan_pp)
        TextView tv_error_hubungan_dengan_pp;

        @BindView(R.id.cl_warga_negara)
        RelativeLayout cl_warga_negara;
        @BindView(R.id.iv_circle_warga_negara)
        ImageView iv_circle_warga_negara;
        @BindView(R.id.ac_warga_negara)
        AutoCompleteTextView ac_warga_negara;
        @BindView(R.id.tv_error_warga_negara)
        TextView tv_error_warga_negara;

        @BindView(R.id.cl_manfaat)
        RelativeLayout cl_manfaat;
        @BindView(R.id.et_manfaat)
        EditText et_manfaat;
        @BindView(R.id.iv_circle_manfaat)
        ImageView iv_circle_manfaat;
        @BindView(R.id.tv_error_manfaat)
        TextView tv_error_manfaat;

        @BindView(R.id.cl_form_info_manfaat)
        RelativeLayout cl_form_info_manfaat;
        @BindView(R.id.tv_header)
        TextView tv_header;

        int position = 0;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            position = getAdapterPosition();
            detilInvestasi.goToForm(position);
        }
    }

    private void disable(ViewGroup layout) {
        layout.setEnabled(false);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                disable((ViewGroup) child);
            } else {
                child.setEnabled(false);
            }
        }
    }
}

