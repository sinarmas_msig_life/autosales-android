package id.co.ajsmsig.nb.pointofsale.ui.ffinancialcalculator

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Observer
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.CalculatorResult
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import id.co.ajsmsig.nb.pointofsale.utils.Calculator
import id.co.ajsmsig.nb.pointofsale.utils.MathEval
import id.co.ajsmsig.nb.pointofsale.utils.moneyFormat
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */
class FinancialCalculatorVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository, var sharedViewModel: SharedViewModel): AndroidViewModel(application) {


    val calculatorResultModel = MediatorLiveData<CalculatorResult>()
    val kebutuhanDanaText = ObservableField<String>()
    val tabunganText = ObservableField<String>()
    val kebutuhanDanaImageName = ObservableField<String>()
    val tabunganImageName = ObservableField<String>()
    val isThereKebutuhan = ObservableBoolean(false)
    val isThereTabungan = ObservableBoolean(false)

    var page: Page? = null
        set(value) {

            sharedViewModel.selectedNeedAnalysis?.let {

                calculatorResultModel.addSource(repository.getCalculatorResultFromNeedAnalysisId(it.id), Observer {

                    calculatorResultModel.value = it
                    it?.let {


                        kebutuhanDanaImageName.set(it.totalEstimatedNeededImageName)
                        tabunganImageName.set(it.totalEstimatedInvestmentImageName)

                        isThereKebutuhan.set(it.totalEstimatedNeededText != null)
                        isThereTabungan.set(it.totalEstimatedInvestmentText != null)

                        val calculator = it
                        var math = MathEval()

                        sharedViewModel.needCalculatorInputs?.forEach {
                            it.let {

                                var value = 0.0
                                if (it.selectedChoice.get() != null) {
                                    value = it.selectedChoice.get()!!.value?.toDouble() ?: 0.0
                                } else if (it.selectedValue.get() != null) {
                                    value = it.selectedValue.get()?.toDouble() ?: 0.0
                                    if (it.input?.type == "percent") {
                                        value /= 100.0
                                    }
                                }
                                math.setVariable(it.input?.formulaRepresentation, value)
                            }
                        }

                        math = Calculator(math, calculator.totalFormula).calculate()
                        math = Calculator(math, calculator.totalPerYearFormula).calculate()
                        math = Calculator(math, calculator.totalPerMonthFormula).calculate()


                        math.variables.entries.forEach {
                            val key = it.key; val value = it.value
                            calculator.totalEstimatedNeededText = calculator.totalEstimatedNeededText?.replace("*$key*", "${value.toInt()}")
                            calculator.totalEstimatedInvestmentText = calculator.totalEstimatedInvestmentText?.replace("*$key*", "${value.toInt()}")
                            calculator.totalEstimatedNeededText = calculator.totalEstimatedNeededText?.replace("%$key%", "Rp. ${value.moneyFormat()}")
                            calculator.totalEstimatedInvestmentText = calculator.totalEstimatedInvestmentText?.replace("%$key%", "Rp. ${value.moneyFormat()}")
                        }
                        kebutuhanDanaText.set(calculator.totalEstimatedNeededText)
                        tabunganText.set(calculator.totalEstimatedInvestmentText)

                    }
                })

            }

        }
}