package id.co.ajsmsig.nb.prop.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseIntArray;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.DBHelper;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductPesertaModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductRiderModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductTopUpModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductUlinkModel;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;

/**
 * Created by faiz_f on 04/04/2017.
 */

public class P_Select {
    private final static String TAG = P_Select.class.getSimpleName();
    private DBHelper dbHelper;
    private Context context;

    public P_Select(Context context) {
        this.context = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.update_data_preferences), Context.MODE_PRIVATE);
        int version = sharedPreferences.getInt(context.getString(R.string.lav_data_id), 0);
        this.dbHelper = new DBHelper(context, version);
    }

    public Cursor query(@NonNull String tableName,
                        @Nullable String[] projection,
                        @Nullable String selection,
                        @Nullable String[] selectionArgs,
                        @Nullable String sortOrder) {

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(tableName);

        Cursor cursor = qb.query(db,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);

        return cursor;
    }

    public ArrayList<DropboxModel> selectListRencana(int groupId) {
        ArrayList<DropboxModel> propModelSpinners = new ArrayList<>();
        QueryUtil queryUtil = new QueryUtil(context);

//        Cursor cursor = queryUtil.query(
//                "LST_USER_PRODUCT_GROUP_DETAIL LEFT JOIN LST_BISNIS",
//                new String[]{"DISTINCT LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID," +
//                        "IFNULL( LST_USER_PRODUCT_GROUP_DETAIL.HARDCODED_FLAG, LST_BISNIS.LSBS_NAME ) LABEL"},
//                "LST_USER_PRODUCT_GROUP_DETAIL.GROUP_ID = ? AND LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID = LST_BISNIS.LSBS_ID",
//                new String[]{String.valueOf(groupId)},
//                null
//        );
// PUNYA KAK FAIZ

        Cursor cursor = queryUtil.query(
                "LST_USER_PRODUCT_GROUP_DETAIL LEFT JOIN E_LST_PACKET_DET\n" +
                        "\tON LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID = E_LST_PACKET_DET.LSBS_ID\n" +
                        "\tLEFT JOIN LST_BISNIS\n" +
                        "\tON LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID = LST_BISNIS.LSBS_ID\n" +
                        "\tLEFT JOIN E_LST_PACKET_CALC\n" +
                        "\tON E_LST_PACKET_DET.FLAG_PACKET = E_LST_PACKET_CALC.FLAG_PACKET",
                new String[]{"DISTINCT LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID," +
                        "IFNULL( LST_USER_PRODUCT_GROUP_DETAIL.HARDCODED_FLAG, LST_BISNIS.LSBS_NAME ) LABEL"},
                "LST_USER_PRODUCT_GROUP_DETAIL.GROUP_ID = ? AND LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID = LST_BISNIS.LSBS_ID",
                new String[]{String.valueOf(groupId)},
                null
        );
        // UNTUK TES SIAP2U

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setId(cursor.getInt(cursor.getColumnIndex("LSBS_ID")));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex("LABEL")));

            propModelSpinners.add(dropboxModel);
            cursor.moveToNext();
        }
        cursor.close();
        return propModelSpinners;
    }

    public DropboxModel selectRencana(int lsbsId, int groupId) {
        QueryUtil queryUtil = new QueryUtil(context);
//        String tableName = context.getString(R.string.LST_USER_PRODUCT_GROUP_DETAIL_LEFT_JOIN_LST_BISNIS_ON_LSBS_ID); //punya kak faiz
        String tableName = context.getString(R.string.LST_USER_PRODUCT_GROUP_DETAIL_LEFT_JOIN_E_LST_PACKET_DET_LEFT_JOIN_LST_BISNIS_LEFT_JOIN_E_LST_PACKET_CALC);
        String[] projection = new String[]{
                "LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID",
                "IFNULL( LST_USER_PRODUCT_GROUP_DETAIL.hardcoded_flag,LST_BISNIS.lsbs_name ) LSBS_NAME"
        };
        String selection = "LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID = ? and LST_USER_PRODUCT_GROUP_DETAIL.group_id = ?";
        String[] selectionArgs = new String[]{String.valueOf(lsbsId), String.valueOf(groupId)};

        Cursor cursor = queryUtil.query(
                tableName,
                projection,
                selection,
                selectionArgs,
                null
        );

        DropboxModel dropboxModel = new DropboxModel();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            dropboxModel.setId(cursor.getInt(cursor.getColumnIndex("LSBS_ID")));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex("LSBS_NAME")));

            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModel;
    }

    public ArrayList<DropboxModel> selectListRencanaSub(int groupId, int lsbsId) {
        ArrayList<DropboxModel> propModelSpinners = new ArrayList<>();
        QueryUtil queryUtil = new QueryUtil(context);

        Cursor cursor = queryUtil.query(
                "LST_USER_PRODUCT_GROUP_DETAIL LEFT JOIN LST_DET_BISNIS\n" +
                        "\tON LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID = LST_DET_BISNIS.LSBS_ID\n" +
                        "\tAND LST_USER_PRODUCT_GROUP_DETAIL.LSDBS_NUMBER = LST_DET_BISNIS.LSDBS_NUMBER",
                new String[]{"DISTINCT LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID,\n" +
                        "\tLST_USER_PRODUCT_GROUP_DETAIL.LSDBS_NUMBER, \t\n" +
                        "\tLST_DET_BISNIS.LSDBS_NAME LABEL"},
                "LST_USER_PRODUCT_GROUP_DETAIL.GROUP_ID = ?\n" +
                        "\tAND LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID = ?",
                new String[]{String.valueOf(groupId), String.valueOf(lsbsId)},
                "LST_USER_PRODUCT_GROUP_DETAIL.LSDBS_NUMBER  ASC"
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setId(cursor.getInt(cursor.getColumnIndex("LSBS_ID")));
            dropboxModel.setIdSecond(cursor.getInt(cursor.getColumnIndex("LSDBS_NUMBER")));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex("LABEL")));

            propModelSpinners.add(dropboxModel);

            cursor.moveToNext();
        }
        cursor.close();
        return propModelSpinners;
    }

    public DropboxModel selectRencanaSub(int lsbs_id, int lsdbsNumber) {
        DropboxModel dropboxModel = new DropboxModel();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectRencanaAsuransi).
                replace("#{LSBS_ID}", "" + lsbs_id).
                replace("#{LSDBS_NUMBER}", "" + lsdbsNumber);
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            dropboxModel.setId(cursor.getInt(cursor.getColumnIndex("LSBS_ID")));
            dropboxModel.setIdSecond(cursor.getInt(cursor.getColumnIndex("LSDBS_NUMBER")));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex("LSDBS_NAME")));

            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModel;
    }

    public ArrayList<DropboxModel> selectListPacket(int groupId, int lsbsId) {
        ArrayList<DropboxModel> propModelSpinners = new ArrayList<>();
        QueryUtil queryUtil = new QueryUtil(context);

        Cursor cursor = queryUtil.query(
                "LST_USER_PRODUCT_GROUP_DETAIL LEFT JOIN E_LST_PACKET_DET\n" +
                        "\tON LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID = E_LST_PACKET_DET.LSBS_ID\n" +
                        "\tAND LST_USER_PRODUCT_GROUP_DETAIL.LSDBS_NUMBER = E_LST_PACKET_DET.LSDBS_NUMBER\n" +
                        "\tLEFT JOIN E_LST_PACKET ON E_LST_PACKET_DET.FLAG_PACKET = E_LST_PACKET.FLAG_PACKET",
                new String[]{"DISTINCT LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID,\n" +
                        "\tE_LST_PACKET_DET.LSDBS_NUMBER, \t\n" +
                        "\tE_LST_PACKET.FLAG_PACKET, \t\n" +
                        "\tE_LST_PACKET.NAMA_PACKET LABEL"},
                "LST_USER_PRODUCT_GROUP_DETAIL.GROUP_ID = ?\n" +
                        "\tAND LST_USER_PRODUCT_GROUP_DETAIL.LSBS_ID = ?",
                new String[]{String.valueOf(groupId), String.valueOf(lsbsId)},
                "E_LST_PACKET.FLAG_PACKET ASC"
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setId(cursor.getInt(cursor.getColumnIndex("LSBS_ID")));
            dropboxModel.setIdSecond(cursor.getInt(cursor.getColumnIndex("LSDBS_NUMBER")));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex("LABEL")));
            dropboxModel.setFlagPacket(cursor.getInt(cursor.getColumnIndex("FLAG_PACKET")));

            propModelSpinners.add(dropboxModel);

            cursor.moveToNext();
        }

        cursor.close();
        return propModelSpinners;
    }

    public DropboxModel selectPacket(int lsbs_id, int lsdbsNumber, int flagPacket) {
        DropboxModel dropboxModel = new DropboxModel();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectPacketAsuransi).
                replace("#{LSBS_ID}", "" + lsbs_id).
                replace("#{LSDBS_NUMBER}", "" + lsdbsNumber).
                replace("#{FLAG_PACKET}", "" + flagPacket);
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            dropboxModel.setId(cursor.getInt(cursor.getColumnIndex("LSBS_ID")));
            dropboxModel.setIdSecond(cursor.getInt(cursor.getColumnIndex("LSDBS_NUMBER")));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex("LABEL")));
            dropboxModel.setFlagPacket(cursor.getInt(cursor.getColumnIndex("FLAG_PACKET")));

            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModel;
    }

    public int selectPsetIdProductSetup(int product_code, int plan) {
//        Log.d("TAG", "selectPsetIdProductSetup: pc = " + product_code + " pl = " + plan);
        int psetId = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectPsetIdProductSetup).
                replace("#{product_code}", "" + product_code + "").
                replace("#{plan}", "" + plan + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            psetId = cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PSET_ID)));
            cursor.moveToNext();
        }
        cursor.close();
//        Log.d("TAG", "selectPsetIdProductSetup: pset id = " + psetId);
        return psetId;
    }

    public int selectPsetIdProductSetupPacket(int product_code, int plan, int flag_packet) {
//        Log.d("TAG", "selectPsetIdProductSetup: pc = " + product_code + " pl = " + plan);
        int psetId = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectPsetIdProductSetupPacket).
                replace("#{product_code}", "" + product_code + "").
                replace("#{plan}", "" + plan + "").
                replace("#{flag_packet}", +flag_packet + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            psetId = cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PSET_ID)));
            cursor.moveToNext();
        }
        cursor.close();
//        Log.d("TAG", "selectPsetIdProductSetup: pset id = " + psetId);
        return psetId;
    }

    public HashMap<String, Integer> selectProductSetupInteger(int product_code, int plan) {
        HashMap<String, Integer> productSetup = new HashMap<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectProductSetup).
                replace("#{product_code}", "" + product_code + "").
                replace("#{plan}", "" + plan + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            productSetup.put(context.getString(R.string.PSET_ID), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PSET_ID))));
//            productSetup.put(context.getString(R.string.PREMIUM), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PREMIUM))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.INSURED_PERIOD))))
                productSetup.put(context.getString(R.string.INSURED_PERIOD), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.INSURED_PERIOD))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.PAY_PERIOD))))
                productSetup.put(context.getString(R.string.PAY_PERIOD), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PAY_PERIOD))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.PREMIUM_HOLIDAY))))
                productSetup.put(context.getString(R.string.PREMIUM_HOLIDAY), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PREMIUM_HOLIDAY))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.PREMIUM_HOLIDAY_FLAG))))
                productSetup.put(context.getString(R.string.PREMIUM_HOLIDAY_FLAG), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PREMIUM_HOLIDAY_FLAG))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.PREMIUM))))
                productSetup.put(context.getString(R.string.PREMIUM), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PREMIUM))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.UP))))
                productSetup.put(context.getString(R.string.UP), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.UP))));

            cursor.moveToNext();
        }
        cursor.close();
//        Log.d("TAG", "selectPsetIdProductSetup: pset id = " + psetId);
        return productSetup;
    }

    public HashMap<String, Integer> selectProductSetupPacket(int product_code, int plan, int flag_packet) {
        HashMap<String, Integer> productSetup = new HashMap<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectProductSetupPacket).
                replace("#{product_code}", "" + product_code + "").
                replace("#{plan}", "" + plan + "").
                replace("#{flag_packet}", "" + flag_packet + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            productSetup.put(context.getString(R.string.PSET_ID), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PSET_ID))));
//            productSetup.put(context.getString(R.string.PREMIUM), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PREMIUM))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.INSURED_PERIOD))))
                productSetup.put(context.getString(R.string.INSURED_PERIOD), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.INSURED_PERIOD))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.PAY_PERIOD))))
                productSetup.put(context.getString(R.string.PAY_PERIOD), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PAY_PERIOD))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.PREMIUM_HOLIDAY))))
                productSetup.put(context.getString(R.string.PREMIUM_HOLIDAY), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PREMIUM_HOLIDAY))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.PREMIUM_HOLIDAY_FLAG))))
                productSetup.put(context.getString(R.string.PREMIUM_HOLIDAY_FLAG), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PREMIUM_HOLIDAY_FLAG))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.PREMIUM))))
                productSetup.put(context.getString(R.string.PREMIUM), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PREMIUM))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.UP))))
                productSetup.put(context.getString(R.string.UP), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.UP))));

            cursor.moveToNext();
        }
        cursor.close();
//        Log.d("TAG", "selectPsetIdProductSetup: pset id = " + psetId);
        return productSetup;
    }

    public int selectPsetIdProposal(String no_proposal) {
        int psetId = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectPsetIdProposal).
                replace("#{no_proposal}", "'" + no_proposal + "'");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            psetId = cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.PSET_ID)));
            cursor.moveToNext();
        }
        cursor.close();
//        Log.d("TAG", "selectPsetIdProductSetup: pset id = " + psetId);
        return psetId;
    }

    public String selectProductFormula(int lpfId) {
        String queryFormula = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectProductFormula).
                replace("#{lpf_id}", "" + lpfId + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            queryFormula = cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.LPF_QUERY)));
            cursor.moveToNext();
        }
        cursor.close();
        return queryFormula;
    }

    public HashMap<String, Object> selectProductCalc(int psetId, String lkuId, int lscbId) {
        HashMap<String, Object> hashMap = new HashMap<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectProductCalc).
                replace("#{pset_id}", "" + psetId + "").
                replace("#{lku_id}", "'" + lkuId + "'").
                replace("#{lscb_id}", "" + lscbId + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            hashMap.put(context.getString(R.string.LPF_PREMIUM), cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LPF_PREMIUM))));
            hashMap.put(context.getString(R.string.LPF_POLICY_VALUE), cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LPF_POLICY_VALUE))));
            hashMap.put(context.getString(R.string.LPF_PREMI_O), cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LPF_PREMI_O))));

            cursor.moveToNext();
        }
        cursor.close();
        return hashMap;
    }


    public ArrayList<DropboxModel> selectListKurs(int pset_id) {
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectListKursBaseProduct).
                replace("#{pset_id}", "" + pset_id + "");
//        Log.d(TAG, "selectListKurs: " + query);
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setIdString(cursor.getString(cursor.getColumnIndex("LKU_ID")));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex("LKU_SYMBOL")));

            dropboxModels.add(dropboxModel);
            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModels;
    }

    public DropboxModel selectKurs(String lkuId) {
        DropboxModel dropboxModel = new DropboxModel();
        String selection = "LKU_ID = ?";
        String[] selectionArgs = new String[]{lkuId};

        Cursor cursor = new QueryUtil(context).query(
                "LST_KURS",
                null,
                selection,
                selectionArgs,
                null
        );
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            dropboxModel.setIdString(cursor.getString(cursor.getColumnIndex("LKU_ID")));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex("LKU_SYMBOL")));
            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModel;
    }

    public ArrayList<DropboxModel> selectListCaraBayar(int pset_id) {
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectListPayMode).
                replace("#{pset_id}", "" + pset_id + "");
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setId(cursor.getInt(cursor.getColumnIndex("LSCB_ID")));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex("LSCB_PAY_MODE")));

            dropboxModels.add(dropboxModel);
            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModels;
    }

    public DropboxModel selectCaraBayar(int lscbId) {
        DropboxModel dropboxModel = new DropboxModel();
        String[] projection = new String[]{"LSCB_ID", "LSCB_PAY_MODE"};
        String selection = "LSCB_ID = ?";
        String[] selectionArgs = new String[]{String.valueOf(lscbId)};

        Cursor cursor = new QueryUtil(context).query(
                "LST_PAY_MODE",
                projection,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            dropboxModel.setId(cursor.getInt(cursor.getColumnIndex("LSCB_ID")));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex("LSCB_PAY_MODE")));

            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModel;
    }

    public ArrayList<Integer> selectListRiderIdLstBisnisRider(int lsbs_id, int lsdbs_number, String lku_id) {
        ArrayList<Integer> integers = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectListRiderIdLstBisnisRider).
                replace("#{lsbs_id}", "" + lsbs_id + "").
                replace("#{lsdbs_number}", "" + lsdbs_number + "").
                replace("#{lku_id}", "'" + lku_id + "'");
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int anInt = cursor.getInt(cursor.getColumnIndex(context.getString(R.string.RIDER_ID)));
            integers.add(anInt);
            cursor.moveToNext();
        }
        cursor.close();
        return integers;
    }

    public ArrayList<Integer> selectListRiderIdLstBisnisRiderPACKETSIAP2U(int lsbs_id, int lsdbs_number, int flag_packet) {
        ArrayList<Integer> integers = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectListRiderIdLstBisnisRiderPACKETSIAP2U).
                replace("#{lsbs_id}", "" + lsbs_id + "").
                replace("#{lsdbs_number}", "" + lsdbs_number + "").
                replace("#{flag_packet}", "" + flag_packet + "");
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int anInt = cursor.getInt(cursor.getColumnIndex(context.getString(R.string.RIDER_ID)));
            integers.add(anInt);
            cursor.moveToNext();
        }
        cursor.close();
        return integers;
    }

    public Integer selectRiderNumberLstBisnisRider(int lsbs_id, int lsdbs_number, int rider_id, String lku_id) {
        Integer integer = null;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectRiderNumberLstBisnisRider).
                replace("#{lsbs_id}", "" + lsbs_id + "").
                replace("#{lsdbs_number}", "" + lsdbs_number + "").
                replace("#{rider_id}", "" + rider_id + "").
                replace("#{lku_id}", "'" + lku_id + "'");

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() == 1) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                integer = cursor.getInt(cursor.getColumnIndex(context.getString(R.string.RIDER_NUMBER)));
                cursor.moveToNext();
            }
        }
        cursor.close();
        return integer;
    }

    public Integer selectRiderNumberLstBisnisRiderPACKETSIAP2U(int lsbs_id, int lsdbs_number, int rider_id, int packet) {
        Integer integer = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectRiderNumberLstBisnisRiderPACKETSIAP2U).
                replace("#{lsbs_id}", "" + lsbs_id + "").
                replace("#{lsdbs_number}", "" + lsdbs_number + "").
                replace("#{rider_id}", "" + rider_id + "").
                replace("#{flag_packet}", "" + packet + "");

        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            integer = cursor.getInt(cursor.getColumnIndex(context.getString(R.string.RIDER_NUMBER)));
            cursor.moveToNext();
        }
        cursor.close();
        return integer;
    }

    public List<Integer> selectRiderNumberLstBisnisRiders(int lsbs_id, int lsdbs_number, int rider_id, String lku_id) {
        List<Integer> plans = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectRiderNumberLstBisnisRider).
                replace("#{lsbs_id}", "" + lsbs_id + "").
                replace("#{lsdbs_number}", "" + lsdbs_number + "").
                replace("#{rider_id}", "" + rider_id + "").
                replace("#{lku_id}", "'" + lku_id + "'");

        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            plans.add(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.RIDER_NUMBER))));
            cursor.moveToNext();

        }
        cursor.close();

        return plans;
    }


    public ArrayList<DropboxModel> selectListRiderNumber(int lsbs_id, int start, int end) {
        ArrayList<DropboxModel> propModelSpinners = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectListLsdbsNumberFromLstBisnisName).
                replace("#{lsbs_id}", "" + lsbs_id + "").
                replace("#{start}", "" + start + "").
                replace("#{end}", "" + end + "");
        Log.d(TAG, "selectListRiderNumber: query " + query);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel propModelSpinner = new DropboxModel();
            propModelSpinner.setId(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LSDBS_NUMBER))));
            propModelSpinner.setLabel(cursor.getString(cursor.getColumnIndex(context.getString(R.string.NAME))));
            propModelSpinners.add(propModelSpinner);
            cursor.moveToNext();
        }
        cursor.close();

        return propModelSpinners;
    }

    public String selectRiderBisnisName(int lsbs_id, int lsdbs_number) {
        String name = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLsdbsNumberFromLstBisnisName).
                replace("#{lsbs_id}", "" + lsbs_id + "").
                replace("#{lsdbs_number}", "" + lsdbs_number + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            name = cursor.getString(cursor.getColumnIndex(context.getString(R.string.NAME)));
            cursor.moveToNext();
        }
        cursor.close();

        return name;
    }

    public String selectRiderBisnisNamePacket(int lsbs_id, int lsdbs_number, int flag_packet) {
        String name = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLsdbsNumberFromLstBisnisNamePacket).
                replace("#{lsbs_id}", "" + lsbs_id + "").
                replace("#{lsdbs_number}", "" + lsdbs_number + "").
                replace("#{flag_packet}", "" + flag_packet + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            name = cursor.getString(cursor.getColumnIndex(context.getString(R.string.NAME)));
            cursor.moveToNext();
        }
        cursor.close();

        return name;
    }

    public ArrayList<DropboxModel> selectListKombinasiPremiBaseProduct(int pset_id) {
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectListKombinasiPremiBaseProduct).
                replace("#{pset_id}", "" + pset_id + "");
        Log.d(TAG, "selectListKombinasiPremiBaseProduct: " + query);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setId(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.BASE_PREMIUM))));
            dropboxModel.setIdSecond(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.TOPUP_PREMIUM))));

            if (dropboxModel.getId().equals(100)) {
                dropboxModel.setLabel("PP100%");
            } else {
                dropboxModel.setLabel("PP " + dropboxModel.getId() + "%, PTU " +
                        dropboxModel.getIdSecond() + "%");
            }

            dropboxModels.add(dropboxModel);
            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModels;
    }

    public DropboxModel selectKombinasiPremi(int premiPokokPercentage) {
        DropboxModel dropboxModel = new DropboxModel();
        dropboxModel.setId(premiPokokPercentage);
        dropboxModel.setIdSecond(100 - premiPokokPercentage);

        if (dropboxModel.getId().equals(100)) {
            dropboxModel.setLabel("PP100%");
        } else {
            dropboxModel.setLabel("PP " + dropboxModel.getId() + "%, PTU " +
                    dropboxModel.getIdSecond() + "%");
        }
        return dropboxModel;
    }


    public HashMap<String, Integer> selectPeriodBaseProduct(int pset_id) {
        HashMap<String, Integer> lstProductSetup = new HashMap<>();
        QueryUtil queryUtil = new QueryUtil(context);
        Cursor cursor = queryUtil.query(
                context.getString(R.string.TABLE_LST_PRODUCT_SETUP),
                new String[]{
                        context.getString(R.string.INSURED_PERIOD),
                        context.getString(R.string.PAY_PERIOD),
                        context.getString(R.string.PREMIUM_HOLIDAY),
                        context.getString(R.string.PREMIUM_HOLIDAY_FLAG)
                },
                context.getString(R.string.PSET_ID) + " = ?",
                new String[]{String.valueOf(pset_id)},
                null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            lstProductSetup.put(context.getString(R.string.INSURED_PERIOD), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.INSURED_PERIOD))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.PAY_PERIOD)))) {
                lstProductSetup.put(context.getString(R.string.PAY_PERIOD), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PAY_PERIOD))));
            }
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.PREMIUM_HOLIDAY)))) {
                lstProductSetup.put(context.getString(R.string.PREMIUM_HOLIDAY), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PREMIUM_HOLIDAY))));
            }
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.PREMIUM_HOLIDAY_FLAG)))) {
                lstProductSetup.put(context.getString(R.string.PREMIUM_HOLIDAY_FLAG), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PREMIUM_HOLIDAY_FLAG))));
            }
            cursor.moveToNext();
        }
        cursor.close();
        return lstProductSetup;
    }

    public ArrayList<DropboxModel> selectMasaPembayaranPremi(int lsbsId, HashMap<String, Integer> productSetup, int umurTT) {
        Integer masaAsuransi = productSetup.get(context.getString(R.string.INSURED_PERIOD));
        Integer lamaBayar = productSetup.get(context.getString(R.string.PAY_PERIOD));
        Integer cutiPremi = productSetup.get(context.getString(R.string.PREMIUM_HOLIDAY));
        Integer cutiPremiFlag = productSetup.get(context.getString(R.string.PREMIUM_HOLIDAY_FLAG));

        int masaKontrak = masaAsuransi - umurTT;

        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();

        if (cutiPremiFlag == 0) {
            if (cutiPremi != null) {
                DropboxModel dropboxModel = new DropboxModel();
                dropboxModel.setLabel(String.valueOf(cutiPremi));
                dropboxModels.add(dropboxModel);
            } else {
                DropboxModel dropboxModel = new DropboxModel();
                dropboxModel.setLabel(String.valueOf(lamaBayar));
                dropboxModels.add(dropboxModel);
            }
        } else {
            int maxCutiPremi;
            int minCutiPremi = 0;
            if (cutiPremi != null)
                minCutiPremi = cutiPremi;

            if (lamaBayar != null) {
                maxCutiPremi = lamaBayar;
            } else if (lsbsId == 213 || lsbsId == 216) {
                maxCutiPremi = masaKontrak - 1;
            } else {
                maxCutiPremi = masaKontrak;
            }

            if (lsbsId == 212) {
                if (maxCutiPremi + umurTT > 60) {
                    maxCutiPremi = 60 - umurTT;
                }
            }

            Log.d(TAG, "selectMasaPembayaranPremi: minCutiPremi is " + minCutiPremi + " and  maxCutiPremi is " + maxCutiPremi);
            for (int i = minCutiPremi; i <= maxCutiPremi; i++) {
                DropboxModel dropboxModel = new DropboxModel();
                dropboxModel.setLabel(String.valueOf(i));
                dropboxModels.add(dropboxModel);
            }
        }

        return dropboxModels;
    }

    public ArrayList<SparseIntArray> selectLstProposalField(int pset_id) {
        ArrayList<SparseIntArray> listField = new ArrayList<>();

        SparseIntArray display = new SparseIntArray();
        SparseIntArray enable = new SparseIntArray();

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLstProposalField).
                replace("#{pset_id}", "" + pset_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            display.put(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LPF_ID))), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.FLAG_DISPLAY))));
            enable.put(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LPF_ID))), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.FLAG_ENABLE))));
            cursor.moveToNext();
        }
        cursor.close();

        listField.add(0, display);
        listField.add(1, enable);

        return listField;
    }

    public HashMap<String, Object> selectMinUp(HashMap<String, Object> params) {
        HashMap<String, Object> hashMap = new HashMap<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectMinUp).
                replace("#{pset_id}", "" + params.get(context.getString(R.string.PSET_ID)) + "").
                replace("#{lku_id}", "'" + params.get(context.getString(R.string.LKU_ID)) + "'").
                replace("#{lscb_id}", "" + params.get(context.getString(R.string.LSCB_ID)) + "");
        Log.d(TAG, "selectMinUp: query " + query);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.MIN_UP))))
                hashMap.put(context.getString(R.string.MIN_UP), new BigDecimal(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.MIN_UP)))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LPF_MIN_UP))))
                hashMap.put(context.getString(R.string.LPF_MIN_UP), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LPF_MIN_UP))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LPF_QUERY))))
                hashMap.put(context.getString(R.string.LPF_QUERY), cursor.getString(cursor.getColumnIndex(context.getString(R.string.LPF_QUERY))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI))))
                hashMap.put(context.getString(R.string.LSCB_KALI), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI))));

            cursor.moveToNext();
        }
        cursor.close();
        return hashMap;
    }

    public HashMap<String, Object> selectMaxUp(HashMap<String, Object> params) {
        HashMap<String, Object> hashMap = new HashMap<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectMaxUp).
                replace("#{pset_id}", "" + params.get(context.getString(R.string.PSET_ID)) + "").
                replace("#{lku_id}", "'" + params.get(context.getString(R.string.LKU_ID)) + "'").
                replace("#{lscb_id}", "" + params.get(context.getString(R.string.LSCB_ID)) + "");
        Log.d(TAG, "selectMaxUp: " + query);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.MAX_UP))))
                hashMap.put(context.getString(R.string.MAX_UP), new BigDecimal(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.MAX_UP)))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LPF_MAX_UP))))
                hashMap.put(context.getString(R.string.LPF_MAX_UP), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LPF_MAX_UP))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LPF_QUERY))))
                hashMap.put(context.getString(R.string.LPF_QUERY), cursor.getString(cursor.getColumnIndex(context.getString(R.string.LPF_QUERY))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI))))
                hashMap.put(context.getString(R.string.LSCB_KALI), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI))));

            cursor.moveToNext();
        }
        cursor.close();
        return hashMap;
    }


    public HashMap<String, Object> selectPaymode(int lscbId) {
        HashMap<String, Object> payMode = new HashMap<>();
        String tableName = "LST_PAY_MODE";
        String[] projection = new String[]{
                context.getString(R.string.LSCB_KALI),
                context.getString(R.string.LSCB_KALI_OLD)
        };
        String selection = context.getString(R.string.LSCB_ID) + " = ?";
        String[] selectionArgs = new String[]{String.valueOf(lscbId)};

        Cursor cursor = new QueryUtil(context).query(
                tableName,
                projection,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI))))
                payMode.put(context.getString(R.string.LSCB_KALI), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI_OLD))))
                payMode.put(context.getString(R.string.LSCB_KALI_OLD), cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI_OLD))));

            cursor.moveToNext();
        }
        cursor.close();
        return payMode;
    }

    public HashMap<String, Object> selectPremi(HashMap<String, Object> params) {
        HashMap<String, Object> hashMap = new HashMap<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectPremi).
                replace("#{pset_id}", "" + params.get(context.getString(R.string.PSET_ID)) + "").
                replace("#{lku_id}", "'" + params.get(context.getString(R.string.LKU_ID)) + "'").
                replace("#{lscb_id}", "" + params.get(context.getString(R.string.LSCB_ID)) + "");
        Log.d(TAG, "selectMaxUp: " + query);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LPF_PREMIUM))))
                hashMap.put(context.getString(R.string.LPF_PREMIUM), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LPF_PREMIUM))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LPF_QUERY))))
                hashMap.put(context.getString(R.string.LPF_QUERY), cursor.getString(cursor.getColumnIndex(context.getString(R.string.LPF_QUERY))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI))))
                hashMap.put(context.getString(R.string.LSCB_KALI), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI_OLD))))
                hashMap.put(context.getString(R.string.LSCB_KALI_OLD), cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI_OLD))));

            cursor.moveToNext();
        }
        cursor.close();
        return hashMap;
    }

    public HashMap<String, Object> selectUp(HashMap<String, Object> params) {
        HashMap<String, Object> hashMap = new HashMap<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectUp).
                replace("#{pset_id}", "" + params.get(context.getString(R.string.PSET_ID)) + "").
                replace("#{lku_id}", "'" + params.get(context.getString(R.string.LKU_ID)) + "'").
                replace("#{lscb_id}", "" + params.get(context.getString(R.string.LSCB_ID)) + "");
        Log.d(TAG, "selectMaxUp: " + query);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.UP))))
                hashMap.put(context.getString(R.string.UP), new BigDecimal(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.UP)))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LPF_UP))))
                hashMap.put(context.getString(R.string.LPF_UP), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LPF_UP))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LPF_QUERY))))
                hashMap.put(context.getString(R.string.LPF_QUERY), cursor.getString(cursor.getColumnIndex(context.getString(R.string.LPF_QUERY))));

            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI))))
                hashMap.put(context.getString(R.string.LSCB_KALI), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI))));

            cursor.moveToNext();
        }
        cursor.close();
        return hashMap;
    }


    public BigDecimal executeQueryMinUp(HashMap<String, Object> params) {
        BigDecimal minUp = BigDecimal.valueOf(0);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = String.valueOf(params.get(context.getString(R.string.LPF_QUERY))).
                replace("EKA.", "").
                replace("GREATEST", "MAX").
                replace("$P{BASE_PREMIUM}", "" + params.get(context.getString(R.string.PREMI_POKOK)) + "").
                replace("$P{JENIS_APP}", "" + params.get(context.getString(R.string.JENIS_APP)) + "").
                replace("$P{LKU_ID}", "'" + params.get(context.getString(R.string.LKU_ID)) + "'").
                replace("$P{LSCB_ID}", "" + params.get(context.getString(R.string.LSCB_ID)) + "").
                replace("$P{PREMIUM}", "" + params.get(context.getString(R.string.PREMIUM)) + "").
                replace("$P{LI_KALI}", "" + params.get(context.getString(R.string.LSCB_KALI)) + "").
                replace("$P{PREMIUM_COMB}", "" + params.get(context.getString(R.string.PREMIUM_COMB)) + "").
                replace("FROM DUAL", "");
        Log.e(TAG, "executeQueryMinUp Query: " + query);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
//            minUp = BigDecimal.valueOf(cursor.getLong(cursor.getColumnIndex("F_UP")));
            minUp = BigDecimal.valueOf(cursor.getLong(0));
            Log.d(TAG, "executeQueryMinUp: nilai min up is " + minUp);
            cursor.moveToNext();
        }
        cursor.close();

        return minUp;
    }


    public BigDecimal executeQueryMaxUp(HashMap<String, Object> params) {
        BigDecimal maxUp = BigDecimal.valueOf(0);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = String.valueOf(params.get(context.getString(R.string.LPF_QUERY))).
                replace("EKA.", "").
                replace("GREATEST", "MAX").
                replace("$P{BASE_PREMIUM}", "" + params.get(context.getString(R.string.PREMI_POKOK)) + "").
                replace("$P{JENIS_APP}", "" + params.get(context.getString(R.string.JENIS_APP)) + "").
                replace("$P{LKU_ID}", "'" + params.get(context.getString(R.string.LKU_ID)) + "'").
                replace("$P{LSCB_ID}", "" + params.get(context.getString(R.string.LSCB_ID)) + "").
                replace("$P{PREMIUM}", "" + params.get(context.getString(R.string.PREMIUM)) + "").
                replace("P{UMUR_TT}", "" + params.get(context.getString(R.string.UMUR_TT)) + "").
                replace("$P{LI_KALI}", "" + params.get(context.getString(R.string.LSCB_KALI)) + "").
                replace("$P{PREMIUM_COMB}", "" + params.get(context.getString(R.string.PREMIUM_COMB)) + "").
                replace("$P{LSBS_ID}", "" + params.get(context.getString(R.string.PRODUCT_CODE)) + "").
                replace("$P{LI_USIA_TT}", "" + params.get(context.getString(R.string.UMUR_TT)) + "").
                replace("FROM DUAL", "");

        Log.d("TAG", "getMaxUP: query = " + query);

        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
//            maxUp = BigDecimal.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow("F_UP")));
            maxUp = BigDecimal.valueOf(cursor.getLong(0));
            cursor.moveToNext();
        }
        cursor.close();
        return maxUp;
    }

    public BigDecimal executeQueryPremi(HashMap<String, Object> params) {
        BigDecimal premi = BigDecimal.ZERO;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = String.valueOf(params.get(context.getString(R.string.LPF_QUERY))).
                replace("EKA.", "").
                replace("eka.", "").
                replace("GREATEST", "MAX").
                replace("CEIL", "ROUND").
                replace("$P{BASE_PREMIUM}", "" + params.get(context.getString(R.string.PREMI_POKOK)) + "").
                replace("$P{JENIS_APP}", "" + params.get(context.getString(R.string.JENIS_APP)) + "").
                replace("$P{LKU_ID}", "'" + params.get(context.getString(R.string.LKU_ID)) + "'").
                replace("$P{LSCB_ID}", "" + params.get(context.getString(R.string.LSCB_ID)) + "").
                replace("$P{PREMIUM}", "" + params.get(context.getString(R.string.PREMIUM)) + "").
                replace("$P{UP}", "" + params.get(context.getString(R.string.UP)) + "").
                replace("P{UMUR_TT}", "" + params.get(context.getString(R.string.UMUR_TT)) + "").
                replace("$P{LI_KALI}", "" + params.get(context.getString(R.string.LSCB_KALI)) + "").
                replace("$P{PREMIUM_COMB}", "" + params.get(context.getString(R.string.PREMIUM_COMB)) + "").
                replace("$P{LSBS_ID}", "" + params.get(context.getString(R.string.PRODUCT_CODE)) + "").
                replace("$P{LSDBS_NUMBER}", "" + params.get(context.getString(R.string.PLAN)) + "").
                replace("$P{LI_USIA_TT}", "" + params.get(context.getString(R.string.UMUR_TT)) + "").
                replace("$P{LSCB_KALI}", "" + params.get(context.getString(R.string.LSCB_KALI)) + "").
                replace("$P{LDEC_FACTOR}", "" + params.get(context.getString(R.string.LSCB_KALI_OLD)) + "").
                replace("$P{FLAG_TT_CALON_BAYI}", "" + params.get(context.getString(R.string.FLAG_TT_CALON_BAYI)) + "").
                replace("nvl", "ifnull").
                replace("NVL", "ifnull").
                replace("FROM DUAL", "");

        Log.d("TAG", "getPremi: query = " + query);

        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
//            premi = BigDecimal.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow("F_PREMI")));
            premi = BigDecimal.valueOf(cursor.getDouble(0));
            cursor.moveToNext();
        }
        cursor.close();
        return premi;
    }

    public BigDecimal executeQueryUp(HashMap<String, Object> params) {
        BigDecimal maxUp = BigDecimal.valueOf(0);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = String.valueOf(params.get(context.getString(R.string.LPF_QUERY))).
                replace("EKA.", "").
                replace("GREATEST", "MAX").
                replace("$P{BASE_PREMIUM}", "" + params.get(context.getString(R.string.PREMI_POKOK)) + "").
                replace("$P{JENIS_APP}", "" + params.get(context.getString(R.string.JENIS_APP)) + "").
                replace("$P{LKU_ID}", "'" + params.get(context.getString(R.string.LKU_ID)) + "'").
                replace("$P{LSCB_ID}", "" + params.get(context.getString(R.string.LSCB_ID)) + "").
                replace("$P{PREMIUM}", "" + params.get(context.getString(R.string.PREMIUM)) + "").
                replace("P{UMUR_TT}", "" + params.get(context.getString(R.string.UMUR_TT)) + "").
                replace("$P{LI_KALI}", "" + params.get(context.getString(R.string.LSCB_KALI)) + "").
                replace("$P{PREMIUM_COMB}", "" + params.get(context.getString(R.string.PREMIUM_COMB)) + "").
                replace("$P{LSBS_ID}", "" + params.get(context.getString(R.string.PRODUCT_CODE)) + "").
                replace("$P{LI_USIA_TT}", "" + params.get(context.getString(R.string.UMUR_TT)) + "").
                replace("$P{LSCB_TTL_MONTH}", "" + params.get(context.getString(R.string.LSCB_TTL_MONTH)) + "").
                replace("FROM DUAL", "");

        Log.d("TAG", "getUP: query = " + query);

        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
//            maxUp = BigDecimal.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow("F_UP")));
            maxUp = BigDecimal.valueOf(cursor.getLong(0));
            cursor.moveToNext();
        }
        cursor.close();
        return maxUp;
    }

    public HashMap<String, BigDecimal> selectPremiRule(int psetId, int lscb_id, String lku_id) {
        HashMap<String, BigDecimal> hmMinPremi = new HashMap<>();
        String tableName = "LST_PRODUCT_CALC";
        String[] projection = new String[]{context.getString(R.string.MIN_PREMIUM), context.getString(R.string.MAX_PREMIUM), context.getString(R.string.MIN_PREMI_POKOK), context.getString(R.string.FLAG_PREMIUM)};
        String selection = context.getString(R.string.PSET_ID) + " = ? AND " +
                context.getString(R.string.LKU_ID) + " = ? AND " +
                context.getString(R.string.LSCB_ID) + " = ?";
        String[] selectionArgs = new String[]{
                String.valueOf(psetId),
                lku_id,
                String.valueOf(lscb_id),
        };
        Cursor cursor = new QueryUtil(context).query(
                tableName,
                projection,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.MIN_PREMIUM))))
                hmMinPremi.put(context.getString(R.string.MIN_PREMIUM), new BigDecimal(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.MIN_PREMIUM)))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.MAX_PREMIUM))))
                hmMinPremi.put(context.getString(R.string.MAX_PREMIUM), new BigDecimal(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.MAX_PREMIUM)))));
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.MIN_PREMI_POKOK))))
                hmMinPremi.put(context.getString(R.string.MIN_PREMI_POKOK), new BigDecimal(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.MIN_PREMI_POKOK)))));
//            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_PREMIUM))))
//            hmMinPremi.put(context.getString(R.string.FLAG_PREMIUM), new BigDecimal(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.FLAG_PREMIUM)))));
            cursor.moveToNext();
        }
        cursor.close();
        return hmMinPremi;
    }


    public int selectFlagInvestFund(int pset_id, String lku_id, int lscb_id) {
        int flagInvestFund = -1;

        String tableName = "LST_PRODUCT_SETUP";
        String[] projection = new String[]{context.getString(R.string.FLAG_INVEST_FUND)};
        String selection = context.getString(R.string.PSET_ID) + " = ?";
        String[] selectionArgs = new String[]{String.valueOf(pset_id)};
        Cursor cursor = new QueryUtil(context).query(
                tableName,
                projection,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            flagInvestFund = cursor.getInt(cursor.getColumnIndex(context.getString(R.string.FLAG_INVEST_FUND)));
            cursor.moveToNext();
        }
        cursor.close();

        return flagInvestFund;
    }

    public String[] selectJenisInvest(int pset_id, String lku_id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectJenisInvest).
                replace("#{pset_id}", "" + pset_id + "").
                replace("#{lku_id}", "'" + lku_id + "'");
        Cursor cursor = db.rawQuery(query, null);
        String[] jenisInvest = new String[cursor.getCount()];
        cursor.moveToFirst();
        int counter = 0;
        while (!cursor.isAfterLast()) {
            jenisInvest[counter] = cursor.getString(cursor.getColumnIndex(context.getString(R.string.JENIS)));
            counter++;
            cursor.moveToNext();
        }
        cursor.close();
        return jenisInvest;
    }


    public ArrayList<DropboxModel> selectJenisInvest2(int pset_id, String lku_id) {
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectJenisInvest2).
                replace("#{pset_id}", "" + pset_id + "").
                replace("#{lku_id}", "'" + lku_id + "'");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setIdString(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LJI_ID))));
            dropboxModel.setDetail(cursor.getString(cursor.getColumnIndex(context.getString(R.string.JENIS_NAME))));
            dropboxModel.setLabelSecond(cursor.getString(cursor.getColumnIndex(context.getString(R.string.JENIS))));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LJI_INVEST))));

            dropboxModels.add(dropboxModel);
            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModels;
    }

    public ArrayList<DropboxModel> selectListJenisInvest(int pset_id, String lku_id, String jenis) {
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectListJenisInvest).
                replace("#{pset_id}", "" + pset_id + "").
                replace("#{lku_id}", "'" + lku_id + "'").
                replace("#{jenis}", "'" + jenis + "'");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setIdString(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LJI_ID))));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LJI_INVEST))));
            dropboxModel.setLabelSecond(cursor.getString(cursor.getColumnIndex(context.getString(R.string.JENIS))));
            dropboxModels.add(dropboxModel);
            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModels;
    }


    public DropboxModel selectInfoInvest(String lji_id) {
        DropboxModel dropboxModel = new DropboxModel();

        String tableName = "LST_JENIS_INVEST";
        String[] projection = new String[]{context.getString(R.string.LJI_ID),
                context.getString(R.string.LJI_INVEST),
                context.getString(R.string.JENIS)};
        String selection = context.getString(R.string.LJI_ID) + " = ?";
        String[] selectionArgs = new String[]{lji_id};
        Cursor cursor = new QueryUtil(context).query(
                tableName,
                projection,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            dropboxModel.setIdString(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LJI_ID))));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndex(context.getString(R.string.LJI_INVEST))));
            dropboxModel.setLabelSecond(cursor.getString(cursor.getColumnIndex(context.getString(R.string.JENIS))));

            cursor.moveToNext();
        }
        cursor.close();
        return dropboxModel;
    }

    public HashMap<String, Integer> selectAgeBound(int pset_id) {
        HashMap<String, Integer> hashMap = new HashMap<>();

        String tableName = "LST_PRODUCT_SETUP";
        String[] projection = new String[]{context.getString(R.string.HOLDER_AGE_FROM),
                context.getString(R.string.HOLDER_AGE_TO),
                context.getString(R.string.INSURED_AGE_FROM),
                context.getString(R.string.INSURED_AGE_TO),
                context.getString(R.string.INSURED_AGE_FROM_FLAG)};
        String selection = context.getString(R.string.PSET_ID) + " = ?";
        String[] selectionArgs = new String[]{String.valueOf(pset_id)};
        Cursor cursor = new QueryUtil(context).query(
                tableName,
                projection,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.HOLDER_AGE_FROM))))
                hashMap.put(context.getString(R.string.HOLDER_AGE_FROM), cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.HOLDER_AGE_FROM))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.HOLDER_AGE_TO))))
                hashMap.put(context.getString(R.string.HOLDER_AGE_TO), cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.HOLDER_AGE_TO))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.INSURED_AGE_FROM))))
                hashMap.put(context.getString(R.string.INSURED_AGE_FROM), cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.INSURED_AGE_FROM))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.INSURED_AGE_TO))))
                hashMap.put(context.getString(R.string.INSURED_AGE_TO), cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.INSURED_AGE_TO))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.INSURED_AGE_FROM_FLAG))))
                hashMap.put(context.getString(R.string.INSURED_AGE_FROM_FLAG), cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.INSURED_AGE_FROM_FLAG))));
            cursor.moveToNext();
        }
        cursor.close();
        return hashMap;
    }

    public ArrayList<DropboxModel> selectListHubunganTtTamb() {
        ArrayList<DropboxModel> listHubungan = new ArrayList<>();

        String tableName = "LST_RELATION";
        String[] projection = new String[]{context.getString(R.string.LSRE_ID),
                context.getString(R.string.LSRE_RELATION)};
        String selection = context.getString(R.string.LSRE_ID) + " IN(1,7,4,2,5)";
        Cursor cursor = new QueryUtil(context).query(
                tableName,
                projection,
                selection,
                null,
                null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setId(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LSRE_ID))));
            dropboxModel.setLabel(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.LSRE_RELATION))));

            listHubungan.add(dropboxModel);
            cursor.moveToNext();
        }
        cursor.close();

        return listHubungan;
    }

    public DropboxModel selectHubunganTtTamb(int lsreId) {
        DropboxModel hubungan = new DropboxModel();

        String tableName = "LST_RELATION";
        String[] projection = new String[]{context.getString(R.string.LSRE_ID),
                context.getString(R.string.LSRE_RELATION)};
        String selection = context.getString(R.string.LSRE_ID) + " =?";
        String[] selectionArgs = new String[]{String.valueOf(lsreId)};
        Cursor cursor = new QueryUtil(context).query(
                tableName,
                projection,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();

        hubungan.setId(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LSRE_ID))));
        hubungan.setLabel(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.LSRE_RELATION))));

        cursor.close();

        return hubungan;
    }

    public HashMap<String, Integer> selectProductIdByNoProposal(String noProposal) {
        HashMap<String, Integer> productId = new HashMap<>();

        String tableName = "MST_PROPOSAL_PRODUCT";
        String[] projection = new String[]{context.getString(R.string.LSBS_ID),
                context.getString(R.string.LSDBS_NUMBER)};
        String selection = context.getString(R.string.NO_PROPOSAL_TAB) + " =?";
        String[] selectionArgs = new String[]{String.valueOf(noProposal)};
        Cursor cursor = new QueryUtil(context).query(
                tableName,
                projection,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            productId.put(context.getString(R.string.LSBS_ID), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LSBS_ID))));
            productId.put(context.getString(R.string.LSDBS_NUMBER), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LSDBS_NUMBER))));

            cursor.moveToNext();
        }
        cursor.close();

        Log.d(TAG, "selectProductIdByNoProposal: " + productId);
        return productId;
    }

    public double selectLdecCoi(HashMap<String, Object> params) {
        double ldec_total = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecCoi)
                .replace("#{no_proposal}", "'" + params.get("no_proposal") + "'")
                .replace("#{liUsia}", "" + params.get("liUsia") + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ldec_total = cursor.getDouble(0);
            cursor.moveToNext();
        }
        cursor.close();
        return ldec_total;
    }


    public ArrayList<HashMap<String, Object>> selectRidersRate(HashMap<String, Object> params) {
        ArrayList<HashMap<String, Object>> listRider = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectRidersRate)
                .replace("#{no_proposal}", "'" + params.get("no_proposal") + "'")
                .replace("#{thn_ke}", "" + params.get("thn_ke") + "")
                .replace("#{li_usia_pp}", "" + params.get("li_usia_pp") + "")
                .replace("#{liUsia}", "" + params.get("liUsia") + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            HashMap<String, Object> rider = new HashMap<>();
            rider.put("NO_PROPOSAL_TAB", cursor.getString(cursor.getColumnIndex("NO_PROPOSAL_TAB")));
            rider.put("RIDER_ID", cursor.getInt(cursor.getColumnIndex("RIDER_ID")));
            rider.put("RIDER_NUMBER", cursor.getInt(cursor.getColumnIndex("RIDER_NUMBER")));
            rider.put("LKU_ID", cursor.getString(cursor.getColumnIndex("LKU_ID")));
            rider.put("RATE", cursor.getDouble(cursor.getColumnIndex("RATE")));
            listRider.add(rider);
            cursor.moveToNext();
        }
        cursor.close();
        return listRider;
    }

    public ArrayList<Double> selectListRiderRatePesertaTambahan(HashMap<String, Object> params) {
        ArrayList<Double> listRate = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectListRiderRatePesertaTambahan)
                .replace("#{no_proposal}", "'" + params.get("no_proposal") + "'")
                .replace("#{thn_ke}", "" + params.get("thn_ke") + "")
                .replace("#{lku_id}", "'" + params.get("lku_id") + "'")
                .replace("#{rider_id}", "" + params.get("rider_id") + "")
                .replace("#{rider_number}", "" + params.get("rider_number") + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            double ldecRate = cursor.getDouble(cursor.getColumnIndex("RATE"));
            listRate.add(ldecRate);
            cursor.moveToNext();
        }
        cursor.close();
        return listRate;
    }

    public ArrayList<HashMap<String, Object>> selectTopupAndWithdrawList(String no_proposal) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<HashMap<String, Object>> topupDrawList = new ArrayList<>();
        String query = context.getString(R.string.selectTopupAndWithdrawList).replace("#{no_proposal}", "'" + no_proposal + "'");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            HashMap<String, Object> topupDraw = new HashMap<>();
            topupDraw.put("THN_KE", new BigDecimal(cursor.getInt(cursor.getColumnIndex("THN_KE"))));
            topupDraw.put("TOPUP", new BigDecimal(cursor.getDouble(cursor.getColumnIndex("TOPUP"))));
            topupDraw.put("TARIK", new BigDecimal(cursor.getDouble(cursor.getColumnIndex("TARIK"))));
            topupDrawList.add(topupDraw);
            cursor.moveToNext();
        }
        cursor.close();
        return topupDrawList;
    }

    public HashMap<String, Object> selectDataProposal(String no_proposal) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        HashMap<String, Object> dataProposal = new HashMap<>();
        String query = context.getString(R.string.selectDataProposal).replace("#{no_proposal}", "'" + no_proposal + "'");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            dataProposal.put("NO_PROPOSAL_TAB", cursor.getString(cursor.getColumnIndex("NO_PROPOSAL_TAB")));
            dataProposal.put("UMUR_PP", new BigDecimal(cursor.getInt(cursor.getColumnIndex("UMUR_PP"))));
            dataProposal.put("UMUR_TT", new BigDecimal(cursor.getInt(cursor.getColumnIndex("UMUR_TT"))));
            dataProposal.put("LSBS_ID", new BigDecimal(cursor.getInt(cursor.getColumnIndex("LSBS_ID"))));
            dataProposal.put("LSDBS_NUMBER", new BigDecimal(cursor.getInt(cursor.getColumnIndex("LSDBS_NUMBER"))));
            dataProposal.put("UP", new BigDecimal(cursor.getDouble(cursor.getColumnIndex("UP"))));
            dataProposal.put(context.getString(R.string.PREMI), new BigDecimal(cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.PREMI)))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.PREMI_KOMB)))) {
                dataProposal.put(context.getString(R.string.PREMI_KOMB), new BigDecimal(cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.PREMI_KOMB)))));
            } else {
                dataProposal.put(context.getString(R.string.PREMI_KOMB), 100);
            }

            dataProposal.put(context.getString(R.string.PREMI_POKOK), new BigDecimal(cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.PREMI_POKOK)))));
            dataProposal.put("THN_MASA_KONTRAK", new BigDecimal(cursor.getInt(cursor.getColumnIndex("THN_MASA_KONTRAK"))));
            dataProposal.put("NAME_ELIGIBLE", cursor.getString(cursor.getColumnIndex("NAME_ELIGIBLE")));
            dataProposal.put("LSCB_KALI", new BigDecimal(cursor.getInt(cursor.getColumnIndex("LSCB_KALI"))));
            dataProposal.put("LSCB_PAY_MODE", cursor.getString(cursor.getColumnIndex("LSCB_PAY_MODE")));
            dataProposal.put("LSCB_ID", new BigDecimal(cursor.getInt(cursor.getColumnIndex("LSCB_ID"))));
            dataProposal.put("LKU_ID", cursor.getString(cursor.getColumnIndex("LKU_ID")));
            dataProposal.put("THN_LAMA_BAYAR", new BigDecimal(cursor.getInt(cursor.getColumnIndex("THN_LAMA_BAYAR"))));
            dataProposal.put("THN_CUTI_PREMI", new BigDecimal(cursor.getInt(cursor.getColumnIndex("THN_CUTI_PREMI"))));
            dataProposal.put("MSAG_ID", cursor.getString(cursor.getColumnIndex("MSAG_ID")));
            dataProposal.put("CARA_BAYAR", new BigDecimal(cursor.getInt(cursor.getColumnIndex("CARA_BAYAR"))));
            dataProposal.put("TGL_INPUT", cursor.getString(cursor.getColumnIndex("TGL_INPUT")));
            dataProposal.put("FLAG_TT_CALON_BAYI", cursor.getInt(cursor.getColumnIndex("FLAG_TT_CALON_BAYI")));

            cursor.moveToNext();
        }
        cursor.close();
        return dataProposal;

    }

    public double selectPremiSetahun(String no_proposal) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        double ldec_premi_setahun = 0;
        String query = context.getString(R.string.selectPremiSetahun)
                .replace("#{no_proposal}", "'" + no_proposal + "'");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ldec_premi_setahun = cursor.getDouble(cursor.getColumnIndex("PREMI_SETAHUN"));
            cursor.moveToNext();
        }
        cursor.close();
        return ldec_premi_setahun;
    }

    public double[] selectBungaAvg(String no_proposal) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        double[] ldec_bunga_avg = new double[4];
        String query = context.getString(R.string.selectBungaAvg).replace("#{no_proposal}", "'" + no_proposal + "'");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ldec_bunga_avg[0] = cursor.getDouble(cursor.getColumnIndex("AVG_0"));
            ldec_bunga_avg[1] = cursor.getDouble(cursor.getColumnIndex("AVG_1"));
            ldec_bunga_avg[2] = cursor.getDouble(cursor.getColumnIndex("AVG_2"));
            ldec_bunga_avg[3] = cursor.getDouble(cursor.getColumnIndex("AVG_3"));
            cursor.moveToNext();
        }
        cursor.close();
        return ldec_bunga_avg;
    }

    //    ini double
    public double selectPremiInvest(HashMap<String, Object> param) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        double ldec_premi_invest_temp = 0;
        String query = context.getString(R.string.selectPremiInvest)
                .replace("#{ldec_akuisisi}", "" + param.get("ldec_akuisisi") + "")
                .replace("#{no_proposal}", "'" + param.get("no_proposal") + "'")
                .replace("#{thn_ke}", "" + param.get("thn_ke") + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ldec_premi_invest_temp = cursor.getDouble(cursor.getColumnIndex("PREMI_INVEST"));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_premi_invest_temp;
    }

    public ArrayList<HashMap<String, Object>> selectIllustrationShow(HashMap<String, Object> param) {
        ArrayList<HashMap<String, Object>> resultList = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectIllustrationShow)
                .replace("#{jenis}", "" + param.get("jenis") + "")
                .replace("#{pset_id}", "" + param.get("pset_id") + "");

//        Log.d(TAG, "selectIllustrationShow: query is = " + query);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            HashMap<String, Object> result = new HashMap<>();
            result.put("YEAR", cursor.getInt(cursor.getColumnIndex("YEAR")));
            resultList.add(result);
            cursor.moveToNext();
        }
        cursor.close();
        return resultList;
    }

    public double[] selectBiayaAkuisisi(int pset_id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectBiayaAkuisisi)
                .replace("#{pset_id}", "" + pset_id + "");
        Cursor cursor = db.rawQuery(query, null);
        double[] lstr_bak = new double[cursor.getCount()];
        cursor.moveToFirst();
        int counter = 0;
        while (!cursor.isAfterLast()) {
            lstr_bak[counter] = cursor.getDouble(cursor.getColumnIndex("RATE"));
            counter++;
            cursor.moveToNext();
        }
        cursor.close();
        return lstr_bak;
    }

    public String getPersentase(String no_proposal) {
        String value = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select  persen_up from MST_PROPOSAL_PRODUCT_RIDER  where NO_PROPOSAL_TAB = " + "'" + no_proposal + "'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            value = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        return value;
    }

    public String getLsrJenis(String no_proposal) {
        String value = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select  lsdbs_number from MST_PROPOSAL_PRODUCT_RIDER  where NO_PROPOSAL_TAB = " + "'" + no_proposal + "'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            value = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        return value;
    }

    public String getUnit(String no_proposal) {
        String value = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select  JML_UNIT from MST_PROPOSAL_PRODUCT_RIDER  where NO_PROPOSAL_TAB = " + "'" + no_proposal + "'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            value = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        return value;
    }

    public String getTermUp(String no_proposal) {
        String value = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select UP from MST_PROPOSAL_PRODUCT_RIDER  where NO_PROPOSAL_TAB = " + "'" + no_proposal + "'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            value = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        return value;
    }

    public String getLkuId(String no_proposal) {
        String value = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select LKU_ID from MST_PROPOSAL_PRODUCT_RIDER  where NO_PROPOSAL_TAB = " + "'" + no_proposal + "'" , null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            value = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        return value;
    }

    public String getUp(String no_proposal) {
        String value = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select  UP from MST_PROPOSAL_PRODUCT_RIDER  where NO_PROPOSAL_TAB = " + "'" + no_proposal + "'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            value = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        return value;
    }

    public int RiderId (String no_proposal_tab){
        if (!TextUtils.isEmpty(no_proposal_tab)){
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select LSBS_ID from MST_PROPOSAL_PRODUCT_RIDER WHERE NO_PROPOSAL_TAB = ?";
            Cursor cursor = db.rawQuery(query, new String[]{no_proposal_tab});
            if (cursor.moveToFirst()) {
                int lsbsId = cursor.getInt(cursor.getColumnIndexOrThrow("LSBS_ID"));
                return lsbsId;
            } else {
                return -1;
            }

        }
        return -1;
    }

    public double selectLdecRatePa(double lsr_jenis, int usia_pp) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRatePa)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_pp}", "" + usia_pp + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateHcp(double lsr_jenis, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRatePa)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateTpd(double lsr_jenis, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRateTpd)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateCi(int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRateCi)
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "'" + lku_id + "'");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateWp10Tpd(double lsr_jenis, int usia_pp, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRateWp10Tpd)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{usia_pp}", "" + usia_pp + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRatePb25Tpd(double lsr_jenis, int usia_pp, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRatePb25Tpd)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{usia_pp}", "" + usia_pp + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateWp10Ci(double lsr_jenis, int usia_pp, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRateWp10Ci)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{usia_pp}", "" + usia_pp + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRatePb25CiDeath(double lsr_jenis, int usia_pp, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRatePb25CiDeath)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{usia_pp}", "" + usia_pp + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRatePb25Ci(double lsr_jenis, int usia_pp, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRatePb25Ci)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{usia_pp}", "" + usia_pp + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRatePayorSpouse(double lsr_jenis, int usia_pp, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRatePb25Ci)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{usia_pp}", "" + usia_pp + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateHcpFamily(double lsr_jenis, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRateHcpFamily)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateHcpFamilyPesertaTambahan(double lsr_jenis, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRateHcpFamilyPesertaTambahan)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateTermRider(int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRateHcpFamilyPesertaTambahan)
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectEkaSehatRider(double lsr_jenis, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectEkaSehatRider)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectEkaSehatInnerLimitRider(double lsr_jenis, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectEkaSehatInnerLimitRider)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateHcpProvider(double lsr_jenis, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRateHcpProvider)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateHcpProviderPesertaTambahan(double lsr_jenis, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRateHcpProviderPesertaTambahan)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateWaiverTpdCi(double lsr_jenis, int usia_tt,int usia_pp, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRateWaiverTpdCi)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{usia_pp}", "" + usia_pp + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRatePayorTpdCiDeath(double lsr_jenis, int usia_tt,int usia_pp, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRatePayorTpdCiDeath)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{usia_pp}", "" + usia_pp + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateLadiesIns(double lsr_jenis, int usia_tt,int usia_pp, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRateLadiesIns)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_pp}", "" + usia_pp + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateHcpLadies(double lsr_jenis, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRateHcpLadies)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLdecRateHcpLadiesPesertaTambahan(double lsr_jenis, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLdecRateHcpLadiesPesertaTambahan)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLadiesMedExpenseRider(double lsr_jenis, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLadiesMedExpenseRider)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double selectLadiesMedExpenseInnerLimitRider(double lsr_jenis, int usia_tt, String lku_id) {
        double ldec_rate = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectLadiesMedExpenseInnerLimitRider)
                .replace("#{lsdbs_number}", "" + lsr_jenis + "")
                .replace("#{usia_tt}", "" + usia_tt + "")
                .replace("#{lku_id}", "" + lku_id + "");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_rate = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_rate;
    }

    public double[][] selectInvestRate() {
        double[][] lstr_bunga = new double[7][4];
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectInvestRate);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        int x = 1;
        while (!cursor.isAfterLast()) {
            lstr_bunga[x][1] = cursor.getDouble(cursor.getColumnIndex("RATE_1"));
            lstr_bunga[x][2] = cursor.getDouble(cursor.getColumnIndex("RATE_2"));
            lstr_bunga[x][3] = cursor.getDouble(cursor.getColumnIndex("RATE_3"));

            x = x + 1;
            cursor.moveToNext();
        }
        cursor.close();
        return lstr_bunga;
    }

    public ArrayList<HashMap<String, Object>> selectListRiderData(HashMap<String, Object> params) {
        ArrayList<HashMap<String, Object>> listRider = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectListRiderData)
                .replace("#{no_proposal}", "'" + params.get("no_proposal") + "'")
                .replace("#{thn_ke}", "" + params.get("thn_ke") + "")
                .replace("#{li_usia_pp}", "" + params.get("li_usia_pp") + "")
                .replace("#{liUsia}", "" + params.get("liUsia") + "");
//        Log.d(TAG, "selectListRiderData: query = " + query);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            HashMap<String, Object> rider = new HashMap<>();
            rider.put("NO_PROPOSAL_TAB", cursor.getString(cursor.getColumnIndex("NO_PROPOSAL_TAB")));
            rider.put("INSURED_AGE_FROM", new BigDecimal(cursor.getInt(cursor.getColumnIndex("INSURED_AGE_FROM"))));
            rider.put("INSURED_AGE_TO", new BigDecimal(cursor.getInt(cursor.getColumnIndex("INSURED_AGE_TO"))));
            rider.put("F_COI_RIDER", cursor.getString(cursor.getColumnIndex("F_COI_RIDER")));
            rider.put("F_COI_RIDER_TAMB", cursor.getString(cursor.getColumnIndex("F_COI_RIDER_TAMB")));
            rider.put("FLAG_AGE_VALIDATION", new BigDecimal(cursor.getInt(cursor.getColumnIndex("FLAG_AGE_VALIDATION"))));
            rider.put("RIDER_ID", new BigDecimal(cursor.getInt(cursor.getColumnIndex("RIDER_ID"))));
            rider.put("RIDER_NUMBER", new BigDecimal(cursor.getInt(cursor.getColumnIndex("RIDER_NUMBER"))));
            rider.put("LKU_ID", cursor.getString(cursor.getColumnIndex("LKU_ID")));
            rider.put("LSCB_ID", cursor.getInt(cursor.getColumnIndex("LSCB_ID")));
            rider.put("RATE", new BigDecimal(cursor.getDouble(cursor.getColumnIndex("RATE"))));
            listRider.add(rider);
            cursor.moveToNext();
        }
        cursor.close();
        return listRider;
    }

    public double selectRiderFormula(String formulaQuery) {
//        Log.e("FORMULAQUERY", "Formula Query: " + formulaQuery);
        double ldec_total = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        formulaQuery = formulaQuery.
                replace("eka.", "").
                replace("EKA.", "").
                replace("LEAST", "MIN").
                replace("nvl", "ifnull").
                replace("NVL", "ifnull");
        Cursor cursor = db.rawQuery(formulaQuery, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ldec_total = cursor.getDouble(0);


            cursor.moveToNext();
        }
        cursor.close();
        return ldec_total;
    }

    public double selectFormulaResult(String formulaQuery) {
//        Log.e("FORMULAQUERY", "Formula Query: " + formulaQuery);
        double ldec_total = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        formulaQuery = formulaQuery
                .replace("eka.", "")
                .replace("EKA.", "")
                .replace("LEAST", "MIN")
                .replace("NVL", "IFNULL")
                .replace("no_proposal", "no_proposal_tab");
        Cursor cursor = db.rawQuery(formulaQuery, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ldec_total = cursor.getDouble(0);

            cursor.moveToNext();
        }

        cursor.close();
        return ldec_total;
    }

    public double selectRiderFormula(String formulaQuery, Boolean fillAnythingIfTambahan) {

//        Log.e("FORMULAQUERY", "Formula Query Tambahan: " + formulaQuery);
        double ldec_total = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(formulaQuery.replace("eka.", "").replace("LEAST", "MIN"), null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ldec_total = cursor.getDouble(0);

            cursor.moveToNext();
        }
        cursor.close();
        return ldec_total;

    }

    public ArrayList<HashMap<String, Object>> selectListRiderDataPesertaTambahan(HashMap<String, Object> params) {
        ArrayList<HashMap<String, Object>> listRiderTambahan = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectListRiderDataPesertaTambahan)
                .replace("#{no_proposal}", "'" + params.get("no_proposal") + "'")
                .replace("#{thn_ke}", "" + params.get("thn_ke") + "")
                .replace("#{lku_id}", "'" + params.get("lku_id") + "'")
                .replace("#{rider_id}", "" + params.get("rider_id") + "")
                .replace("#{rider_number}", "" + params.get("rider_number") + "")
                .replace("eka.", "")
                .replace("EKA.", "")
                .replace("LEAST", "MIN")
                .replace("nvl", "ifnull")
                .replace("NVL", "ifnull");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            HashMap<String, Object> riderTamb = new HashMap<>();
            riderTamb.put("NO_PROPOSAL_TAB", cursor.getString(cursor.getColumnIndex("NO_PROPOSAL_TAB")));
            riderTamb.put("LSBS_ID", new BigDecimal(cursor.getInt(cursor.getColumnIndex("LSBS_ID"))));
            riderTamb.put("LSDBS_NUMBER", new BigDecimal(cursor.getInt(cursor.getColumnIndex("LSDBS_NUMBER"))));
            riderTamb.put("NAMA_PESERTA", cursor.getString(cursor.getColumnIndex("NAMA_PESERTA")));
            riderTamb.put("TGL_LAHIR", cursor.getString(cursor.getColumnIndex("TGL_LAHIR")));
            riderTamb.put("USIA", new BigDecimal(cursor.getInt(cursor.getColumnIndex("USIA"))));
            riderTamb.put("LSRE_ID", new BigDecimal(cursor.getInt(cursor.getColumnIndex("LSRE_ID"))));
            riderTamb.put("PESERTA_KE", new BigDecimal(cursor.getInt(cursor.getColumnIndex("PESERTA_KE"))));
            riderTamb.put("RATE", new BigDecimal(cursor.getDouble(cursor.getColumnIndex("RATE"))));

            listRiderTambahan.add(riderTamb);
            cursor.moveToNext();
        }
        cursor.close();
        return listRiderTambahan;

    }


    public HashMap<String, Object> selectProposalInfo(String no_proposal) {
        HashMap<String, Object> itemProposal = new HashMap<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectProposalInfo).
                replace("#{no_proposal}", "'" + no_proposal + "'");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            itemProposal.put(context.getString(R.string.NAMA_PP), cursor.getString(cursor.getColumnIndex(context.getString(R.string.NAMA_PP))));
            itemProposal.put(context.getString(R.string.UMUR_PP), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.UMUR_PP))));
            itemProposal.put("NAMA_TT", cursor.getString(cursor.getColumnIndex(context.getString(R.string.NAMA_TT))));
            itemProposal.put("UMUR_TT", cursor.getInt(cursor.getColumnIndex(context.getString(R.string.UMUR_TT))));
            itemProposal.put(context.getString(R.string.TGL_INPUT), cursor.getString(cursor.getColumnIndex(context.getString(R.string.TGL_INPUT))));
            itemProposal.put("LKU_ID", cursor.getString(cursor.getColumnIndex(context.getString(R.string.LKU_ID))));
            itemProposal.put("THN_MASA_KONTRAK", cursor.getInt(cursor.getColumnIndex(context.getString(R.string.THN_MASA_KONTRAK))));
            itemProposal.put("PREMI_POKOK", cursor.getLong(cursor.getColumnIndex(context.getString(R.string.PREMI_POKOK))));
            itemProposal.put("PREMI_TOPUP", cursor.getLong(cursor.getColumnIndex(context.getString(R.string.PREMI_TOPUP))));
            itemProposal.put("PREMI_KOMB", cursor.getInt(cursor.getColumnIndex(context.getString(R.string.PREMI_KOMB))));
            itemProposal.put("CARA_BAYAR", cursor.getInt(cursor.getColumnIndex(context.getString(R.string.CARA_BAYAR))));
            itemProposal.put("PREMI", cursor.getLong(cursor.getColumnIndex(context.getString(R.string.PREMI))));
            itemProposal.put("THN_LAMA_BAYAR", cursor.getInt(cursor.getColumnIndex(context.getString(R.string.THN_LAMA_BAYAR))));
            itemProposal.put("UP", cursor.getLong(cursor.getColumnIndex(context.getString(R.string.UP))));
            itemProposal.put("INV_FIX", cursor.getInt(cursor.getColumnIndex(context.getString(R.string.INV_FIX))));
            itemProposal.put("INV_DYNAMIC", cursor.getInt(cursor.getColumnIndex(context.getString(R.string.INV_DYNAMIC))));
            itemProposal.put("INV_AGGRESSIVE", cursor.getInt(cursor.getColumnIndex(context.getString(R.string.INV_AGGRESSIVE))));
            itemProposal.put("LJI_FIX", cursor.getString(cursor.getColumnIndex(context.getString(R.string.LJI_FIX))));
            itemProposal.put("LJI_DYNAMIC", cursor.getString(cursor.getColumnIndex(context.getString(R.string.LJI_DYNAMIC))));
            itemProposal.put(context.getString(R.string.LJI_AGGRESIVE), cursor.getString(cursor.getColumnIndex(context.getString(R.string.LJI_AGGRESIVE))));
            itemProposal.put(context.getString(R.string.LSBS_ID), cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LSBS_ID))));
            itemProposal.put(context.getString(R.string.LSDBS_NAME), cursor.getString(cursor.getColumnIndex(context.getString(R.string.LSDBS_NAME))));
            itemProposal.put(context.getString(R.string.LSDBS_VIEW), cursor.getString(cursor.getColumnIndex(context.getString(R.string.LSDBS_VIEW))));
            cursor.moveToNext();
        }
        cursor.close();

        return itemProposal;
    }


    public double selectProductFactor(String no_proposal) {
        double ldec_factor = 0.0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectProductFactor).
                replace("#{no_proposal}", "'" + no_proposal + "'");
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ldec_factor = cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.FACTOR)));
            cursor.moveToNext();
        }
        cursor.close();

        return ldec_factor;
    }

    public ArrayList<HashMap<String, Object>> selectInvestRateAssumption(String no_proposal) {
        ArrayList<HashMap<String, Object>> listInvestRateAssumption = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = context.getString(R.string.selectInvestRateAssumption).
                replace("#{no_proposal}", "'" + no_proposal + "'");

        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            HashMap<String, Object> investRateAssumption = new HashMap<>();
            investRateAssumption.put(context.getString(R.string.LJI_INVEST), cursor.getString(cursor.getColumnIndex(context.getString(R.string.LJI_INVEST))));
            investRateAssumption.put(context.getString(R.string.RATE_1), cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE_1))));
            investRateAssumption.put(context.getString(R.string.RATE_2), cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE_2))));
            investRateAssumption.put(context.getString(R.string.RATE_3), cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.RATE_3))));
            investRateAssumption.put(context.getString(R.string.MDU_PERSEN), cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.MDU_PERSEN))));
            investRateAssumption.put(context.getString(R.string.ALLOCATION_1), cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.ALLOCATION_1))));
            investRateAssumption.put(context.getString(R.string.ALLOCATION_2), cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.ALLOCATION_2))));
            investRateAssumption.put(context.getString(R.string.ALLOCATION_3), cursor.getDouble(cursor.getColumnIndex(context.getString(R.string.ALLOCATION_3))));

            listInvestRateAssumption.add(investRateAssumption);
            cursor.moveToNext();
        }
        cursor.close();

        return listInvestRateAssumption;
    }
    /*
        End Untuk Illustrasi
    */

    public P_MstDataProposalModel selectDataProposalByNoProposal(String no_proposal_tab) {
        String selection = context.getString(R.string.NO_PROPOSAL_TAB) + " = ?";
        String[] selectionArgs = new String[]{no_proposal_tab};

        Cursor cursor = new QueryUtil(context).query(
                context.getString(R.string.MST_DATA_PROPOSAL),
                null,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();

        TableDataProposal tableDataProposal = new TableDataProposal(context);
        P_MstDataProposalModel p_mstDataProposalModel = tableDataProposal.getObject(cursor);

        cursor.close();

        return p_mstDataProposalModel;
    }

    public P_MstDataProposalModel selectDataProposalByNoProposal(long _id) {
        String selection = context.getString(R.string._ID) + " = ?";
        String[] selectionArgs = new String[]{String.valueOf(_id)};

        Cursor cursor = new QueryUtil(context).query(
                context.getString(R.string.MST_DATA_PROPOSAL),
                null,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();

        TableDataProposal tableDataProposal = new TableDataProposal(context);
        P_MstDataProposalModel p_mstDataProposalModel = tableDataProposal.getObject(cursor);

        cursor.close();

        return p_mstDataProposalModel;
    }

    public P_MstProposalProductModel selectProposalProductByNoProposal(String no_proposal_tab) {
        String selection = context.getString(R.string.NO_PROPOSAL_TAB) + " = ?";
        String[] selectionArgs = new String[]{no_proposal_tab};

        Cursor cursor = new QueryUtil(context).query(
                context.getString(R.string.MST_PROPOSAL_PRODUCT),
                null,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();

        P_MstProposalProductModel productModel = new TableProposalProduct(context).getObject(cursor);

        cursor.close();

        return productModel;
    }

    public P_MstProposalProductRiderModel selectRider(String no_proposal_tab, int rider_id, int riderNumber) {
        String selection = context.getString(R.string.NO_PROPOSAL_TAB) + " = ? AND " +
                context.getString(R.string.LSBS_ID) + " = ? AND " +
                context.getString(R.string.LSDBS_NUMBER) + " = ?";
        String[] selectionArgs = new String[]{no_proposal_tab, String.valueOf(rider_id), String.valueOf(riderNumber)};

        Cursor cursor = new QueryUtil(context).query(
                context.getString(R.string.MST_PROPOSAL_PRODUCT_RIDER),
                null,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();

        P_MstProposalProductRiderModel riderModel = new TableProposalProductRider(context).getObject(cursor);

        cursor.close();

        return riderModel;
    }

    public int selectLiKali(int lscb_id) {
        String selection = context.getString(R.string.LSCB_ID) + " = ?";
        String[] selectionArgs = new String[]{String.valueOf(lscb_id)};

        Cursor cursor = new QueryUtil(context).query(
                "LST_PAY_MODE",
                null,
                selection,
                selectionArgs,
                null
        );

        cursor.moveToFirst();

        int liKali = cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LSCB_KALI)));

        cursor.close();

        return liKali;
    }

    public ArrayList<P_MstProposalProductRiderModel> selectListRider(String no_proposal_tab) {
        String selection = context.getString(R.string.NO_PROPOSAL_TAB) + "=?";
        String[] selectionArgs = new String[]{no_proposal_tab};

        Cursor cursor = query(
                context.getString(R.string.MST_PROPOSAL_PRODUCT_RIDER),
                null,
                selection,
                selectionArgs,
                null
        );
        ArrayList<P_MstProposalProductRiderModel> riderModels = new TableProposalProductRider(context).getObjectArray(cursor, true);

        int countRider = 0;
        for (P_MstProposalProductRiderModel p_mstProposalProductRiderModel : riderModels) {
            selection = context.getString(R.string.NO_PROPOSAL_TAB) + " = ? AND " +
                    context.getString(R.string.LSBS_ID) + " = ? AND " +
                    context.getString(R.string.LSDBS_NUMBER) + " = ?";
            selectionArgs = new String[]{
                    no_proposal_tab,
                    String.valueOf(p_mstProposalProductRiderModel.getLsbs_id()),
                    String.valueOf(p_mstProposalProductRiderModel.getLsdbs_number())
            };
            cursor = query(
                    context.getString(R.string.MST_PROPOSAL_PRODUCT_PESERTA),
                    null,
                    selection,
                    selectionArgs,
                    null
            );
            ArrayList<P_MstProposalProductPesertaModel> p_mstProposalProductPesertaModels = new TableProposalProductPeserta(context).getObjectArray(cursor, true);
            riderModels.get(countRider).setP_mstProposalProductPesertaModels(p_mstProposalProductPesertaModels);
            countRider++;
        }
        return riderModels;
    }

    public String selectProductName(int lsbs_id, int lsdbs_number) {
        String selection = "LST_USER_PRODUCT_GROUP_DETAIL.lsbs_id = ? and LST_USER_PRODUCT_GROUP_DETAIL.lsdbs_number = ?";
        String[] selectionArgs = new String[]{String.valueOf(lsbs_id), String.valueOf(lsdbs_number)};

        Cursor cursor = query(
                "LST_USER_PRODUCT_GROUP_DETAIL left join LST_BISNIS\n" +
                        "        on LST_USER_PRODUCT_GROUP_DETAIL.lsbs_id =LST_BISNIS.lsbs_id",
                new String[]{"ifnull( LST_USER_PRODUCT_GROUP_DETAIL.hardcoded_flag, LST_BISNIS.lsbs_name ) LABEL"},
                selection,
                selectionArgs,
                null
        );

        String label = "";

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            if (!cursor.isNull(0))
                label = cursor.getString(0);

            cursor.moveToNext();
        }
        cursor.close();

        return label;
    }

    public int selectLsgbId(Integer lsbsId) {
        int lsgbId = 0;
        if (lsbsId != null) {
            String[] projection = new String[]{"LSGB_ID"};
            String selection = context.getString(R.string.LSBS_ID) + "=?";
            String[] selectionArgs = new String[]{String.valueOf(lsbsId)};

            Cursor cursor = query(
                    "LST_BISNIS",
                    projection,
                    selection,
                    selectionArgs,
                    null
            );
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                lsgbId = cursor.getInt(cursor.getColumnIndex("LSGB_ID"));

                cursor.moveToNext();
            }
            cursor.close();
        }
        return lsgbId;
    }

    public P_ProposalModel getAllDataProposalForUpload(String noProposalTab) {
        P_Select select = new P_Select(context);
        String selection = context.getString(R.string.NO_PROPOSAL_TAB) + "=?";
        String[] selectionArgs = new String[]{noProposalTab};

        P_ProposalModel proposalModel = new P_ProposalModel();
        P_MstDataProposalModel p_mstDataProposalModel;
        P_MstProposalProductModel p_mstProposalProductModel;
        ArrayList<P_MstProposalProductRiderModel> p_mstProposalProductRiderModels;
        ArrayList<P_MstProposalProductTopUpModel> p_mstProposalProductTopUpModels;
        ArrayList<P_MstProposalProductUlinkModel> p_mstProposalProductUlinkModels;
        ArrayList<P_MstProposalProductPesertaModel> p_mstProposalProductPesertaModels;

        /*set data proposal*/
        Cursor cursor;
        cursor = select.query(
                context.getString(R.string.MST_DATA_PROPOSAL),
                null,
                selection,
                selectionArgs,
                null
        );
        p_mstDataProposalModel = new TableDataProposal(context).getObject(cursor);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        Calendar calendar;
        if (p_mstDataProposalModel.getTgl_input() != null) {
            calendar = StaticMethods.toCalendar(p_mstDataProposalModel.getTgl_input());
            p_mstDataProposalModel.setTgl_input(sdf.format(calendar.getTime()));
        }
        if (p_mstDataProposalModel.getTgl_lahir_pp() != null) {
            calendar = StaticMethods.toCalendar(p_mstDataProposalModel.getTgl_lahir_pp());
            p_mstDataProposalModel.setTgl_lahir_pp(sdf.format(calendar.getTime()));
        }
        if (p_mstDataProposalModel.getTgl_lahir_tt() != null) {
            calendar = StaticMethods.toCalendar(p_mstDataProposalModel.getTgl_lahir_tt());
            p_mstDataProposalModel.setTgl_lahir_tt(sdf.format(calendar.getTime()));
        }


        /*set proposal product*/
        cursor = select.query(
                context.getString(R.string.MST_PROPOSAL_PRODUCT),
                null,
                selection,
                selectionArgs,
                null
        );
        p_mstProposalProductModel = new TableProposalProduct(context).getObject(cursor);

        /*set ulink*/
        cursor = select.query(
                context.getString(R.string.MST_PROPOSAL_PRODUCT_ULINK),
                null,
                selection,
                selectionArgs,
                null
        );
        p_mstProposalProductUlinkModels = new TableProposalProductULink(context).getObjectArray(cursor);

        /*set topup*/
        cursor = select.query(
                context.getString(R.string.MST_PROPOSAL_PRODUCT_TOPUP),
                null,
                selection,
                selectionArgs,
                null
        );
        p_mstProposalProductTopUpModels = new TableProposalProductTopUp(context).getObjectArray(cursor);

        /*set rider*/
        cursor = select.query(
                context.getString(R.string.MST_PROPOSAL_PRODUCT_RIDER),
                null,
                selection,
                selectionArgs,
                null
        );
        p_mstProposalProductRiderModels = new TableProposalProductRider(context).getObjectArray(cursor, false);

        /*set peserta*/
        cursor = select.query(
                context.getString(R.string.MST_PROPOSAL_PRODUCT_PESERTA),
                null,
                selection,
                selectionArgs,
                null
        );
        p_mstProposalProductPesertaModels = new TableProposalProductPeserta(context).getObjectArray(cursor, false);


        /*set into proposal model*/
        proposalModel.setMst_data_proposal(p_mstDataProposalModel);
        proposalModel.setMst_proposal_product(p_mstProposalProductModel);
        proposalModel.setMst_proposal_product_ulink(p_mstProposalProductUlinkModels);
        proposalModel.setMst_proposal_product_topup(p_mstProposalProductTopUpModels);
        proposalModel.setMst_proposal_product_rider(p_mstProposalProductRiderModels);
        proposalModel.setMst_proposal_product_peserta(p_mstProposalProductPesertaModels);

        return proposalModel;
    }

    public List<String> getLstPacket(int lsbs_id, int lsdbs_number) {
        ArrayList<String> List = new ArrayList<String>();
        String value = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select  b.nama_packet from E_LST_PACKET b, E_LST_PACKET_DET a" +
                " where a.flag_packet = b.flag_packet" +
                " and a.lsbs_id =" + lsbs_id +
                " and a.lsdbs_number =" + lsdbs_number +
                " and A.FLAG_ACTIVE=1 and B.FLAG_ACTIVE=1", null);
        if (cursor.moveToFirst()) {
            do {
                List.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return List;
    }

    /**
     * Get number of available (un-used) virtual account
     *
     * @param productId
     * @param agentCode
     * @return
     */
    public int getVaCount(String productId, String agentCode) {
        String query = "SELECT * FROM E_LST_VA WHERE FLAG_AKTIF = 1 AND LSBS_ID = " + productId + " AND MSAG_ID = " + agentCode;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    /**
     * Return a virtual account from the reserved VA list.
     *
     * @param lsbsId
     * @param msagId
     * @return
     */
    public String getVirtualAccount(String lsbsId, String msagId) {
        String query = "SELECT NO_VA FROM E_LST_VA WHERE FLAG_AKTIF = 1 AND LSBS_ID = " + lsbsId + " AND MSAG_ID = " + msagId + " LIMIT 1";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            String vaNumber = cursor.getString(0);
            return vaNumber;
        }

        cursor.close();

        return null;
    }

    /**
     * Check whether spaj is has been sent so server
     * Return 1: Sent to server
     * Return 0 : Not sent to server
     * Return -1 : Unknown error occurred.
     *
     * @param spajId
     * @return
     */
    public int isSPAJSubmitted(String spajId) {
        if (TextUtils.isEmpty(spajId)) {
            return 0;
        }
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT FLAG_AKTIF FROM E_MST_SPAJ WHERE SPAJ_ID = " + spajId;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            int isSubmit = cursor.getInt(0);
            cursor.close();
            return isSubmit;
        }
        cursor.close();

        return -1;
    }

    /**
     * Get virtual account of the selected proposal by specifying its no proposal tab
     * @param noProposalTab
     * @return
     */
    public String getProposalVirtualAccount(String noProposalTab) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "select virtual_acc from MST_DATA_PROPOSAL where no_proposal_tab = '" + noProposalTab + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            String virtualAccount = cursor.getString(cursor.getColumnIndexOrThrow("VIRTUAL_ACC"));
            cursor.close();
            return virtualAccount;
        }
        cursor.close();
        return null;
    }

    /**
     * Get product name by specifying it's id
     * @param productId
     * @return
     */
    public String getProductNameWithProductId(String productId) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "select lsbs_name from LST_BISNIS where lsbs_id = " + productId;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            String productName = cursor.getString(cursor.getColumnIndexOrThrow("LSBS_NAME"));
            cursor.close();
            return productName;
        }
        cursor.close();
        return null;
    }


    public String getProductNameForVa(String productId , String productSub) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "select LSDBS_NAME from LST_DET_BISNIS where LSBS_ID = " +productId+" AND LSDBS_NUMBER = " + productSub;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            String productName = cursor.getString(cursor.getColumnIndexOrThrow("LSDBS_NAME"));
            cursor.close();
            return productName;
        }
        cursor.close();
        return null;
    }

    public String getProposalNoInSpaj(String proposalId) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "select IDPROPOSAL from E_MST_SPAJ where NO_PROPOSAL_TAB = '"+proposalId+"'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            String productName = cursor.getString(cursor.getColumnIndexOrThrow("IDPROPOSAL"));
            cursor.close();
            return productName;
        }
        cursor.close();
        return null;
    }

    /**
     * Get proposal input date by specifying its no propsoal tab
     * @param noProposalTab
     * @return
     */
    public Date getProposalInputDate(String noProposalTab) {
        if (!TextUtils.isEmpty(noProposalTab)) {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select tgl_input from MST_DATA_PROPOSAL where no_proposal_tab = '"+noProposalTab+"'";
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                String inputDate = cursor.getString(cursor.getColumnIndexOrThrow("TGL_INPUT"));

                if (!TextUtils.isEmpty(inputDate)) {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        return formatter.parse(inputDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

            }
            cursor.close();

        }

        return null;

    }

    public Cursor getProposalInfo(String noProposalTab) {
        if (!TextUtils.isEmpty(noProposalTab)) {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String query = "select * from MST_DATA_PROPOSAL where NO_PROPOSAL_TAB =  '"+noProposalTab+"'";
            Cursor cursor = db.rawQuery(query, null);
            return cursor;
        }

        return null;
    }

}
