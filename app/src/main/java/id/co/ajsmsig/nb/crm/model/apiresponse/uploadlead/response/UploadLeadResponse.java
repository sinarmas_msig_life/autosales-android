
package id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UploadLeadResponse {

    @SerializedName("state")
    @Expose
    private Boolean state;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("home")
    @Expose
    private Home home;
    @SerializedName("activity")
    @Expose
    private List<Activity> activity = null;
    @SerializedName("sl_tab_id")
    @Expose
    private String slTabId;

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Home getHome() {
        return home;
    }

    public void setHome(Home home) {
        this.home = home;
    }

    public List<Activity> getActivity() {
        return activity;
    }

    public void setActivity(List<Activity> activity) {
        this.activity = activity;
    }

    public String getSlTabId() {
        return slTabId;
    }

    public void setSlTabId(String slTabId) {
        this.slTabId = slTabId;
    }

}
