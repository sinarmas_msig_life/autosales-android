package id.co.ajsmsig.nb.prop.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author : Faiz
 * @since : Jun 7, 2016 (4:08:04 PM)
 */

public class P_IllustrationDevelopmentFundModel implements Parcelable {
    private String no_proposal;
    private String yearNo;
    private String insuredAge;
    private String premiumTotal;
    private String topupAssumption;
    private String drawAssumption;
    private String valueLow;
    private String valueMid;
    private String valueHi;
    private String benefitLow;
    private String benefitMid;
    private String benefitHi;

    public P_IllustrationDevelopmentFundModel() {
    }


    protected P_IllustrationDevelopmentFundModel(Parcel in) {
        no_proposal = in.readString();
        yearNo = in.readString();
        insuredAge = in.readString();
        premiumTotal = in.readString();
        topupAssumption = in.readString();
        drawAssumption = in.readString();
        valueLow = in.readString();
        valueMid = in.readString();
        valueHi = in.readString();
        benefitLow = in.readString();
        benefitMid = in.readString();
        benefitHi = in.readString();
    }

    public static final Creator<P_IllustrationDevelopmentFundModel> CREATOR = new Creator<P_IllustrationDevelopmentFundModel>() {
        @Override
        public P_IllustrationDevelopmentFundModel createFromParcel(Parcel in) {
            return new P_IllustrationDevelopmentFundModel(in);
        }

        @Override
        public P_IllustrationDevelopmentFundModel[] newArray(int size) {
            return new P_IllustrationDevelopmentFundModel[size];
        }
    };

    public String getNo_proposal() {
        return no_proposal;
    }

    public void setNo_proposal(String no_proposal) {
        this.no_proposal = no_proposal;
    }

    public String getYearNo() {
        return yearNo;
    }

    public void setYearNo(String yearNo) {
        this.yearNo = yearNo;
    }

    public String getInsuredAge() {
        return insuredAge;
    }

    public void setInsuredAge(String insuredAge) {
        this.insuredAge = insuredAge;
    }

    public String getPremiumTotal() {
        return premiumTotal;
    }

    public void setPremiumTotal(String premiumTotal) {
        this.premiumTotal = premiumTotal;
    }

    public String getTopupAssumption() {
        return topupAssumption;
    }

    public void setTopupAssumption(String topupAssumption) {
        this.topupAssumption = topupAssumption;
    }

    public String getDrawAssumption() {
        return drawAssumption;
    }

    public void setDrawAssumption(String drawAssumption) {
        this.drawAssumption = drawAssumption;
    }

    public String getValueLow() {
        return valueLow;
    }

    public void setValueLow(String valueLow) {
        this.valueLow = valueLow;
    }

    public String getValueMid() {
        return valueMid;
    }

    public void setValueMid(String valueMid) {
        this.valueMid = valueMid;
    }

    public String getValueHi() {
        return valueHi;
    }

    public void setValueHi(String valueHi) {
        this.valueHi = valueHi;
    }

    public String getBenefitLow() {
        return benefitLow;
    }

    public void setBenefitLow(String benefitLow) {
        this.benefitLow = benefitLow;
    }

    public String getBenefitMid() {
        return benefitMid;
    }

    public void setBenefitMid(String benefitMid) {
        this.benefitMid = benefitMid;
    }

    public String getBenefitHi() {
        return benefitHi;
    }

    public void setBenefitHi(String benefitHi) {
        this.benefitHi = benefitHi;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(no_proposal);
        dest.writeString(yearNo);
        dest.writeString(insuredAge);
        dest.writeString(premiumTotal);
        dest.writeString(topupAssumption);
        dest.writeString(drawAssumption);
        dest.writeString(valueLow);
        dest.writeString(valueMid);
        dest.writeString(valueHi);
        dest.writeString(benefitLow);
        dest.writeString(benefitMid);
        dest.writeString(benefitHi);
    }
}
