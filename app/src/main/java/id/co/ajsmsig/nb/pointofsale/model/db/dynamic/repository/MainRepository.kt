package id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.util.Log
import id.co.ajsmsig.nb.pointofsale.model.*
import id.co.ajsmsig.nb.pointofsale.AppExecutors
import id.co.ajsmsig.nb.pointofsale.api.APIService
import id.co.ajsmsig.nb.pointofsale.api.ApiResponse
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.*
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.NetworkBoundResource
import javax.inject.Singleton

/**
 * Created by andreyyoshuamanik on 11/03/18.
 */
@Singleton
class MainRepository
private constructor(val mainDB: MainDB,val appExecutors: AppExecutors, val apiService: APIService) {

    companion object {
        private var instance: MainRepository? = null

        fun getInstance(database: MainDB, appExecutors: AppExecutors, apiService: APIService): MainRepository {
            synchronized(MainRepository::class.java) {
                if (instance == null) {
                    instance = MainRepository(database, appExecutors, apiService)
                }
            }
            return instance!!
        }
    }

    fun sendAnalytics(analytic: Analytics) {

        Log.i("ANALYTICS POS", analytic.event ?: "")
        val sendAnalyticObservable = apiService.sendAnalytics(arrayListOf(analytic))
        sendAnalyticObservable.observeForever(object : Observer<ApiResponse<String>> {
            override fun onChanged(t: ApiResponse<String>?) {
                t ?: return

                if (!t.isSuccessful) {
                    appExecutors.diskIO().execute {
                        mainDB.analyticsDao().insert(analytic)
                    }
                }

                sendAnalyticObservable.removeObserver(this)
            }

        })
    }

    fun getAllUnsentAnalytics(): LiveData<List<Analytics>> {
        return mainDB.analyticsDao().loadUnsentAnalytics()
    }

    fun update(analytics: List<Analytics>){
        appExecutors.diskIO().execute {
            analytics.forEach { mainDB.analyticsDao().update(it) }
        }
    }


    fun insert(userRecord: UserRecord?): Long {
        return mainDB.userRecordDao().insert(userRecord)
    }

    fun update(userRecord: UserRecord?) {
        mainDB.userRecordDao().update(userRecord)
    }

    fun getUserRecordWithUserId(userId: Long?): LiveData<UserRecord> {
        return mainDB.userRecordDao().loadUserAndRecordWithUserId(userId)
    }

    fun getUserRecordWithUserId(userId: Long?, groupId: Int?): LiveData<UserRecord> {
        return mainDB.userRecordDao().loadUserAndRecordWithUserId(userId, groupId)
    }

    fun getProductWithId(lsbsId: Int?, groupId: Int?) : LiveData<Resource<List<Product>>> {
        return object :NetworkBoundResource<List<Product>, List<Product>>(appExecutors) {
            override fun saveCallResult(item: List<Product>?) {
                item?.let {
                    it.forEach {
                        mainDB.productDao().insert(it)
                    }
                }
            }

            override fun shouldFetch(data: List<Product>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<List<Product>> {
                return mainDB.productDao().loadProductsWithId(lsbsId, groupId)
            }

            override fun createCall(): LiveData<ApiResponse<List<Product>>> {
                return apiService.getProductsWithGroupId("$groupId", "$lsbsId")

            }

        }.asLiveData()
    }

    fun getAllProducts(groupId: Int?): LiveData<Resource<List<Product>>> {
        return object :NetworkBoundResource<List<Product>, List<Product>>(appExecutors) {
            override fun saveCallResult(item: List<Product>?) {
                item?.let {
                    it.forEach {
                        mainDB.productDao().insert(it)
                    }
                }
            }

            override fun shouldFetch(data: List<Product>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<List<Product>> {
                return mainDB.productDao().loadAllProducts()
            }

            override fun createCall(): LiveData<ApiResponse<List<Product>>> {
                return apiService.getProductsWithGroupId("$groupId")
            }


        }.asLiveData()
    }

    fun getProductRidersWith(lsbsId: Int?, groupId: Int?): LiveData<Resource<List<ProductRider>>> {
        return object : NetworkBoundResource<List<ProductRider>, List<ProductRider>>(appExecutors) {
            override fun saveCallResult(item: List<ProductRider>?) {
                item?.let {
                    it.forEach {
                        mainDB.productRiderDao().insert(it)
                    }
                }
            }

            override fun shouldFetch(data: List<ProductRider>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<List<ProductRider>> {
                return mainDB.productRiderDao().loadAllProductRidersWith(lsbsId)
            }

            override fun createCall(): LiveData<ApiResponse<List<ProductRider>>> {
                return apiService.getRidersWith("$groupId", "$lsbsId")
            }

        }.asLiveData()
    }

    fun getProductBenefitsWith(lsbsId: Int?, groupId: Int?): LiveData<Resource<List<ProductBenefit>>> {
        return object : NetworkBoundResource<List<ProductBenefit>, List<ProductBenefit>>(appExecutors) {
            override fun saveCallResult(item: List<ProductBenefit>?) {
                item?.let {
                    it.forEach {
                        mainDB.productBenefitDao().insert(it)
                    }
                }
            }

            override fun shouldFetch(data: List<ProductBenefit>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<List<ProductBenefit>> {
                return mainDB.productBenefitDao().loadAllProductBenefitWithId(lsbsId)
            }

            override fun createCall(): LiveData<ApiResponse<List<ProductBenefit>>> {
                return apiService.getBenefitsWith("$groupId", "$lsbsId")
            }

        }.asLiveData()
    }

}