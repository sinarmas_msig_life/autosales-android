package id.co.ajsmsig.nb.espaj.method;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.pdf.PdfRenderer;
import android.nfc.Tag;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.TabSettings;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAddRiderUA;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAskesUA;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBank;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBankCab;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_NO9ppUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_NO9ttUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDataDitunjukDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelJnsDanaDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelMst_Answer;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.model.DropboxModel;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Bernard on 26/02/2018.
 */

public class JSONToPDF{
    public static String JSONPDF (Context context, EspajModel me) {
        Context mContext;
        String PDF = "";
        File pdfFile;
        mContext = context;
        PdfRenderer renderer;
        ExtendedViewPager pager;
        ArrayList<ModelDataDitunjukDI> ListManfaat;
        ArrayList<ModelJnsDanaDI> ListDana;
        ArrayList<DropboxModel> ListDropInt;
        ArrayList<ModelDropDownString> ListDropDownString;
        ArrayList<ModelBank> ListDropDownBank;
        ArrayList<ModelBankCab> ListDropDownBankCabang;
        ArrayList<ModelDropdownInt> ListDropDownSPAJ;
        File file_foto;
        Document document = new Document();

        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_preferences), MODE_PRIVATE);

        int groupId = 0;
        groupId = sharedPreferences.getInt("GROUP_ID", 0);
        int JENIS_LOGIN = 0;
        JENIS_LOGIN = sharedPreferences.getInt("JENIS_LOGIN", 0);
        int JENIS_LOGIN_BC = 0;
        JENIS_LOGIN_BC = sharedPreferences.getInt("JENIS_LOGIN_BC", 2);
        String KODE_REG1 = "";
        KODE_REG1 = sharedPreferences.getString("KODE_REG1", "");
        String KODE_REG2 = "";
        KODE_REG2 = sharedPreferences.getString("KODE_REG2", "");
        String KODE_REG3 = "";
        KODE_REG3 = sharedPreferences.getString("KODE_REG3", "");
        String NAMA_AGEN ="";
        NAMA_AGEN = sharedPreferences.getString("NAMA_AGEN", "");



        try {
//            File docsFolder = new File(Environment.getExternalStorageDirectory() + "/Documents");
            File docsFolder = new File(context.getFilesDir() + "/ESPAJ/PDF/");
            if (!docsFolder.exists()) {
                docsFolder.mkdirs();
//            Log.i(TAG, "Created a new directory for PDF");
            }

            Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 14);
            Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 14,
                    Font.BOLD);

//            pdfFile = new File(docsFolder.getAbsolutePath(), "PDF" + me.getModelID().getSPAJ_ID_TAB() + ".PDF");
            pdfFile = new File(docsFolder, "PDF" + me.getModelID().getSPAJ_ID_TAB() + ".PDF");
            OutputStream output = new FileOutputStream(pdfFile);


            PdfWriter.getInstance(document, output);

            if (!document.isOpen()) {
                document.open();
            }

            document.add(new Paragraph(context.getString(R.string.pemegang_polis) + "\n\n", normalFont));


            ListDropDownSPAJ = new E_Select(context).getLstChannelDist(JENIS_LOGIN, JENIS_LOGIN_BC, KODE_REG1, KODE_REG2);
            Phrase channel = new Phrase(context.getString(R.string.channel_distribusi), normalFont);
            channel.setTabSettings(new TabSettings(250f));
            channel.add(Chunk.TABBING);
            channel.add(new Chunk("     : " + ListDropDownSPAJ.get(0).getValue() + "\n", boldFont));
            document.add(channel);

            ListDropInt = new P_Select(context).selectListRencana(groupId);
            Phrase prdk = new Phrase(context.getString(R.string.produk), normalFont);
            prdk.setTabSettings(new TabSettings(250f));
            prdk.add(Chunk.TABBING);
            prdk.add(new Chunk("     : " +MethodSupport.getAdapterPosition(ListDropInt, me.getUsulanAsuransiModel().getKd_produk_ua()) + "\n", boldFont));
            document.add(prdk);

            try {
                if (MethodSupport.getSubAdapterPacket(ListDropInt, me.getUsulanAsuransiModel().getPaket_ua()).toString() != "null") {
                    ListDropInt = new P_Select(context).selectListPacket(groupId, me.getUsulanAsuransiModel().getKd_produk_ua());
                    Phrase pkt = new Phrase(context.getString(R.string.paket), normalFont);
                    pkt.setTabSettings(new TabSettings(250f));
                    pkt.add(Chunk.TABBING);
                    pkt.add(new Chunk( "     : " + MethodSupport.getSubAdapterPacket( ListDropInt, me.getUsulanAsuransiModel().getPaket_ua())+ "\n", boldFont));
                    document.add(pkt);
                } else {

                }
            } catch (Exception e) {

            }

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_jenis_spaj, 0);
            Phrase jns_spaj = new Phrase(context.getString(R.string.jenis_spaj), normalFont);
            jns_spaj.setTabSettings(new TabSettings(250f));
            jns_spaj.add(Chunk.TABBING);
            jns_spaj.add(new Chunk( "     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getModelID().getJns_spaj()) + "\n", boldFont));
            document.add(jns_spaj);

            Phrase no_pps = new Phrase(context.getString(R.string.nomor_eproposal), normalFont);
            no_pps.setTabSettings(new TabSettings(250f));
            no_pps.add(Chunk.TABBING);
            no_pps.add(new Chunk("     : " + me.getModelID().getProposal_tab() + "\n", boldFont));
            document.add(no_pps);

            Phrase no_ciff = new Phrase(context.getString(R.string.no_cif), normalFont);
            no_ciff.setTabSettings(new TabSettings(250f));
            no_ciff.add(Chunk.TABBING);
            no_ciff.add(new Chunk("     : " + me.getPemegangPolisModel().getNo_ciff_pp() + "\n", boldFont));
            document.add(no_ciff);

            Phrase nm_lengkap = new Phrase(context.getString(R.string.nama_lengkap), normalFont);
            nm_lengkap.setTabSettings(new TabSettings(250f));
            nm_lengkap.add(Chunk.TABBING);
            nm_lengkap.add(new Chunk("     : " + me.getPemegangPolisModel().getNama_pp() + "\n", boldFont));
            document.add(nm_lengkap);

            Phrase nm_alias = new Phrase(context.getString(R.string.nama_alias), normalFont);
            nm_alias.setTabSettings(new TabSettings(250f));
            nm_alias.add(Chunk.TABBING);
            nm_alias.add(new Chunk("     : " + me.getPemegangPolisModel().getAlias_pp() + "\n", boldFont));
            document.add(nm_alias);

            Phrase tpt_lahir = new Phrase(context.getString(R.string.tempat_lahir), normalFont);
            tpt_lahir.setTabSettings(new TabSettings(250f));
            tpt_lahir.add(Chunk.TABBING);
            tpt_lahir.add(new Chunk("     : " + me.getPemegangPolisModel().getTempat_pp() + "\n", boldFont));
            document.add(tpt_lahir);

            Phrase tgl_lahir = new Phrase(context.getString(R.string.tanggal_lahir), normalFont);
            tgl_lahir.setTabSettings(new TabSettings(250f));
            tgl_lahir.add(Chunk.TABBING);
            tgl_lahir.add(new Chunk( "     : " + me.getPemegangPolisModel().getTtl_pp() + "\n", boldFont));
            document.add(tgl_lahir);

            Phrase umr = new Phrase(context.getString(R.string.umur), normalFont);
            umr.setTabSettings(new TabSettings(250f));
            umr.add(Chunk.TABBING);
            umr.add(new Chunk("     : " + me.getPemegangPolisModel().getUsia_pp() + "\n", boldFont));
            document.add(umr);

            if (me.getPemegangPolisModel().getJekel_pp() == 0) {
                Phrase jekel_w = new Phrase(context.getString(R.string.jenis_kelamin), normalFont);
                jekel_w.setTabSettings(new TabSettings(250f));
                jekel_w.add(Chunk.TABBING);
                jekel_w.add(new Chunk("     : Wanita" + "\n", boldFont));
                document.add(jekel_w);
            } else {
                Phrase jekel_p = new Phrase(context.getString(R.string.jenis_kelamin), normalFont);
                jekel_p.setTabSettings(new TabSettings(250f));
                jekel_p.add(Chunk.TABBING);
                jekel_p.add(new Chunk("     : Pria" + "\n", boldFont));
                document.add(jekel_p);
            }

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_agama, 0);
            Phrase agma = new Phrase(context.getString(R.string.agama), normalFont);
            agma.setTabSettings(new TabSettings(250f));
            agma.add(Chunk.TABBING);
            agma.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getPemegangPolisModel().getAgama_pp()) + "\n", boldFont));
            document.add(agma);

            Phrase ktrngn = new Phrase(context.getString(R.string.keterangan), normalFont);
            ktrngn.setTabSettings(new TabSettings(250f));
            ktrngn.add(Chunk.TABBING);
            ktrngn.add(new Chunk("     : " + me.getPemegangPolisModel().getAgama_lain_pp() + "\n", boldFont));
            document.add(ktrngn);

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_pendidikan, 0);
            Phrase pend_akhir = new Phrase(context.getString(R.string.pendidikan_terakhir_pp), normalFont);
            pend_akhir.setTabSettings(new TabSettings(250f));
            pend_akhir.add(Chunk.TABBING);
            pend_akhir.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getPemegangPolisModel().getPendidikan_pp()) + "\n", boldFont));
            document.add(pend_akhir);

            Phrase glr_pend = new Phrase(context.getString(R.string.gelar_pendidikan), normalFont);
            glr_pend.setTabSettings(new TabSettings(250f));
            glr_pend.add(Chunk.TABBING);
            glr_pend.add(new Chunk("     : " + me.getPemegangPolisModel().getGelar_pp() + "\n", boldFont));
            document.add(glr_pend);

            Phrase nm_ibu = new Phrase(context.getString(R.string.nama_ibu), normalFont);
            nm_ibu.setTabSettings(new TabSettings(250f));
            nm_ibu.add(Chunk.TABBING);
            nm_ibu.add(new Chunk( "     : " + me.getPemegangPolisModel().getNama_ibu_pp() + "\n", boldFont));
            document.add(nm_ibu);

            if (me.getPemegangPolisModel().getGreencard_pp() == 0) {
                Phrase card_t = new Phrase(context.getString(R.string.wn_amerika_serikat), normalFont);
                card_t.add(new Chunk("   : Tidak" + "\n", boldFont));
                document.add(card_t);
            } else {
                Phrase card_y = new Phrase(context.getString(R.string.wn_amerika_serikat), normalFont);
                card_y.add(new Chunk("   : Ya" + "\n", boldFont));
                document.add(card_y);
            }

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_warganegara, 0);
            Phrase wn = new Phrase(context.getString(R.string.warga_negara), normalFont);
            wn.setTabSettings(new TabSettings(250f));
            wn.add(Chunk.TABBING);
            wn.add(new Chunk(( "     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getPemegangPolisModel().getWarga_negara_pp())) + "\n", boldFont));
            document.add(wn);

            Phrase nm_prshn = new Phrase(context.getString(R.string.nama_perusahaan_tempat_bekerja), normalFont);
            nm_prshn.setTabSettings(new TabSettings(250f));
            nm_prshn.add(Chunk.TABBING);
            nm_prshn.add(new Chunk("     : " + me.getPemegangPolisModel().getNm_perusahaan_pp() + "\n", boldFont));
            document.add(nm_prshn);

            Phrase klsfks_pkrjn = new Phrase(context.getString(R.string.klasifikasi_pekerjaan), normalFont);
            klsfks_pkrjn.setTabSettings(new TabSettings(250f));
            klsfks_pkrjn.add(Chunk.TABBING);
            klsfks_pkrjn.add(new Chunk("     : " + me.getPemegangPolisModel().getKlasifikasi_pekerjaan_pp() + "\n", boldFont));
            document.add(klsfks_pkrjn);

            Phrase jbtn = new Phrase(context.getString(R.string.jabatan), normalFont);
            jbtn.setTabSettings(new TabSettings(250f));
            jbtn.add(Chunk.TABBING);
            jbtn.add(new Chunk("     : " + me.getPemegangPolisModel().getJabatan_klasifikasi_pp() + "\n", boldFont));
            document.add(jbtn);

            Phrase uraian_tgs = new Phrase(context.getString(R.string.uraikan_tugas), normalFont);
            uraian_tgs.setTabSettings(new TabSettings(250f));
            uraian_tgs.add(Chunk.TABBING);
            uraian_tgs.add(new Chunk("     : " + me.getPemegangPolisModel().getUraian_pekerjaan_pp() + "\n", boldFont));
            document.add(uraian_tgs);

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_identity, 0);
            Phrase bkt_pp = new Phrase(context.getString(R.string.bukti_identitas), normalFont);
            bkt_pp.setTabSettings(new TabSettings(250f));
            bkt_pp.add(Chunk.TABBING);
            bkt_pp.add(new Chunk(("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getPemegangPolisModel().getBukti_identitas_pp())) + "\n", boldFont));
            document.add(bkt_pp);

            Phrase ktrng = new Phrase(context.getString(R.string.keterangan), normalFont);
            ktrng.setTabSettings(new TabSettings(250f));
            ktrng.add(Chunk.TABBING);
            ktrng.add(new Chunk("     : " + me.getPemegangPolisModel().getBukti_identitas_lain_pp() + "\n", boldFont));
            document.add(ktrng);

            Phrase no_idntts = new Phrase(context.getString(R.string.no_identitas), normalFont);
            no_idntts.setTabSettings(new TabSettings(250f));
            no_idntts.add(Chunk.TABBING);
            no_idntts.add(new Chunk("     : " + me.getPemegangPolisModel().getNo_identitas_pp() + "\n", boldFont));
            document.add(no_idntts);

            Phrase berlaku = new Phrase(context.getString(R.string.berlaku_hingga), normalFont);
            berlaku.setTabSettings(new TabSettings(250f));
            berlaku.add(Chunk.TABBING);
            berlaku.add(new Chunk("     : " + me.getPemegangPolisModel().getTgl_berlaku_pp() + "\n", boldFont));
            document.add(berlaku);

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_marital, 0);
            Phrase status = new Phrase(context.getString(R.string.status_nikah), normalFont);
            status.setTabSettings(new TabSettings(250f));
            status.add(Chunk.TABBING);
            status.add(new Chunk(("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getPemegangPolisModel().getStatus_pp())) + "\n", boldFont));
            document.add(status);

            Phrase alamat_rmh = new Phrase(context.getString(R.string.alamat) + " " + "Rumah", normalFont);
            alamat_rmh.setTabSettings(new TabSettings(250f));
            alamat_rmh.add(Chunk.TABBING);
            alamat_rmh.add(new Chunk("     : " + me.getPemegangPolisModel().getAlamat_pp() + "\n", boldFont));
            document.add(alamat_rmh);

            ListDropDownString = new E_Select(context).getLst_Propinsi_PP();
            Phrase provinsi = new Phrase(context.getString(R.string.provinsi), normalFont);
            provinsi.setTabSettings(new TabSettings(250f));
            provinsi.add(Chunk.TABBING);
            provinsi.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getPropinsi_pp())) + "\n", boldFont));
            document.add(provinsi);

            ListDropDownString = new E_Select(context).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_pp());
            Phrase kabupaten = new Phrase(context.getString(R.string.kabupaten), normalFont);
            kabupaten.setTabSettings(new TabSettings(250f));
            kabupaten.add(Chunk.TABBING);
            kabupaten.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKabupaten_pp())) + "\n", boldFont));
            document.add(kabupaten);

            ListDropDownString = new E_Select(context).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_pp());
            Phrase kecamatan = new Phrase(context.getString(R.string.kecamatan), normalFont);
            kecamatan.setTabSettings(new TabSettings(250f));
            kecamatan.add(Chunk.TABBING);
            kecamatan.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKecamatan_pp())) + "\n", boldFont));
            document.add(kecamatan);

            ListDropDownString = new E_Select(context).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_pp());
            Phrase kelurahan = new Phrase(context.getString(R.string.kelurahan), normalFont);
            kelurahan.setTabSettings(new TabSettings(250f));
            kelurahan.add(Chunk.TABBING);
            kelurahan.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKelurahan_pp())) + "\n", boldFont));
            document.add(kelurahan);

            ListDropDownString = new E_Select(context).getLst_Kodepos_PP(me.getPemegangPolisModel().getKelurahan_pp());
            Phrase kdpos_rmh = new Phrase(context.getString(R.string.kode_pos), normalFont);
            kdpos_rmh.setTabSettings(new TabSettings(250f));
            kdpos_rmh.add(Chunk.TABBING);
            kdpos_rmh.add(new Chunk("     : " + me.getPemegangPolisModel().getKdpos_pp() + "\n", boldFont));
            document.add(kdpos_rmh);

            Phrase telp1_rmh = new Phrase(context.getString(R.string.telp_1), normalFont);
            telp1_rmh.setTabSettings(new TabSettings(250f));
            telp1_rmh.add(Chunk.TABBING);
            telp1_rmh.add(new Chunk("     : " + me.getPemegangPolisModel().getKdtelp1_pp() + " - " + me.getPemegangPolisModel().getTelp1_pp() + "\n", boldFont));
            document.add(telp1_rmh);

            Phrase telp2_rmh = new Phrase(context.getString(R.string.telp_2), normalFont);
            telp2_rmh.setTabSettings(new TabSettings(250f));
            telp2_rmh.add(Chunk.TABBING);
            telp2_rmh.add(new Chunk("     : " + me.getPemegangPolisModel().getKdtelp2_pp() + " - " + me.getPemegangPolisModel().getTelp2_pp() + "\n", boldFont));
            document.add(telp2_rmh);

            Phrase alamat_kntr = new Phrase(context.getString(R.string.alamat) + " " + "Kantor", normalFont);
            alamat_kntr.setTabSettings(new TabSettings(250f));
            alamat_kntr.add(Chunk.TABBING);
            alamat_kntr.add(new Chunk("     : " + me.getPemegangPolisModel().getAlamat_kantor_pp() + "\n", boldFont));
            document.add(alamat_kntr);

            ListDropDownString = new E_Select(context).getLst_Propinsi_PP();
            Phrase provinsi_kntr = new Phrase(context.getString(R.string.provinsi), normalFont);
            provinsi_kntr.setTabSettings(new TabSettings(250f));
            provinsi_kntr.add(Chunk.TABBING);
            provinsi_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getPropinsi_kantor_pp())) + "\n", boldFont));
            document.add(provinsi_kntr);

            ListDropDownString = new E_Select(context).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_kantor_pp());
            Phrase kabupaten_kntr = new Phrase(context.getString(R.string.kabupaten), normalFont);
            kabupaten_kntr.setTabSettings(new TabSettings(250f));
            kabupaten_kntr.add(Chunk.TABBING);
            kabupaten_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKabupaten_kantor_pp())) + "\n", boldFont));
            document.add(kabupaten_kntr);

            ListDropDownString = new E_Select(context).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_kantor_pp());
            Phrase kecamatan_kntr = new Phrase(context.getString(R.string.kecamatan), normalFont);
            kecamatan_kntr.setTabSettings(new TabSettings(250f));
            kecamatan_kntr.add(Chunk.TABBING);
            kecamatan_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKecamatan_kantor_pp())) + "\n", boldFont));
            document.add(kecamatan_kntr);

            ListDropDownString = new E_Select(context).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_kantor_pp());
            Phrase kelurahan_kntr = new Phrase(context.getString(R.string.kelurahan), normalFont);
            kelurahan_kntr.setTabSettings(new TabSettings(250f));
            kelurahan_kntr.add(Chunk.TABBING);
            kelurahan_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKelurahan_kantor_pp())) + "\n", boldFont));
            document.add(kelurahan_kntr);

            ListDropDownString = new E_Select(context).getLst_Kodepos_PP(me.getPemegangPolisModel().getKelurahan_kantor_pp());
            Phrase kdpos_kntr = new Phrase(context.getString(R.string.kode_pos), normalFont);
            kdpos_kntr.setTabSettings(new TabSettings(250f));
            kdpos_kntr.add(Chunk.TABBING);
            kdpos_kntr.add(new Chunk("     : " + me.getPemegangPolisModel().getKdpos_kantor_pp() + "\n", boldFont));
            document.add(kdpos_kntr);

            Phrase telp1_kntr = new Phrase(context.getString(R.string.telp_1), normalFont);
            telp1_kntr.setTabSettings(new TabSettings(250f));
            telp1_kntr.add(Chunk.TABBING);
            telp1_kntr.add(new Chunk("     : " + me.getPemegangPolisModel().getKdtelp1_kantor_pp() + " - " + me.getPemegangPolisModel().getTelp1_kantor_pp() + "\n", boldFont));
            document.add(telp1_kntr);

            Phrase telp2_kntr = new Phrase(context.getString(R.string.telp_2), normalFont);
            telp2_kntr.setTabSettings(new TabSettings(250f));
            telp2_kntr.add(Chunk.TABBING);
            telp2_kntr.add(new Chunk("     : " + me.getPemegangPolisModel().getKdtelp2_kantor_pp() + " - " + me.getPemegangPolisModel().getTelp2_kantor_pp() + "\n", boldFont));
            document.add(telp2_kntr);

            Phrase fax_kntr = new Phrase(context.getString(R.string.no_fax), normalFont);
            fax_kntr.setTabSettings(new TabSettings(250f));
            fax_kntr.add(Chunk.TABBING);
            fax_kntr.add(new Chunk("     : " + me.getPemegangPolisModel().getKdfax_kantor_pp() + " - " + me.getPemegangPolisModel().getFax_kantor_pp() + "\n", boldFont));
            document.add(fax_kntr);

            Phrase alamat_tgl = new Phrase(context.getString(R.string.alamat) + " " + "Tinggal", normalFont);
            alamat_tgl.setTabSettings(new TabSettings(250f));
            alamat_tgl.add(Chunk.TABBING);
            alamat_tgl.add(new Chunk("     : " + me.getPemegangPolisModel().getAlamat_tinggal_pp() + "\n", boldFont));
            document.add(alamat_tgl);

            ListDropDownString = new E_Select(context).getLst_Propinsi_PP();
            Phrase provinsi_tgl = new Phrase(context.getString(R.string.provinsi), normalFont);
            provinsi_tgl.setTabSettings(new TabSettings(250f));
            provinsi_tgl.add(Chunk.TABBING);
            provinsi_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getPropinsi_tinggal_pp())) + "\n", boldFont));
            document.add(provinsi_tgl);

            ListDropDownString = new E_Select(context).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_tinggal_pp());
            Phrase kabupaten_tgl = new Phrase(context.getString(R.string.kabupaten), normalFont);
            kabupaten_tgl.setTabSettings(new TabSettings(250f));
            kabupaten_tgl.add(Chunk.TABBING);
            kabupaten_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKabupaten_tinggal_pp())) + "\n", boldFont));
            document.add(kabupaten_tgl);

            ListDropDownString = new E_Select(context).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_tinggal_pp());
            Phrase kecamatan_tgl = new Phrase(context.getString(R.string.kecamatan), normalFont);
            kecamatan_tgl.setTabSettings(new TabSettings(250f));
            kecamatan_tgl.add(Chunk.TABBING);
            kecamatan_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKecamatan_tinggal_pp())) + "\n", boldFont));
            document.add(kecamatan_tgl);

            ListDropDownString = new E_Select(context).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_tinggal_pp());
            Phrase kelurahan_tgl = new Phrase(context.getString(R.string.kelurahan), normalFont);
            kelurahan_tgl.setTabSettings(new TabSettings(250f));
            kelurahan_tgl.add(Chunk.TABBING);
            kelurahan_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getPemegangPolisModel().getKelurahan_tinggal_pp())) + "\n", boldFont));
            document.add(kelurahan_tgl);

            ListDropDownString = new E_Select(context).getLst_Kodepos_PP(me.getPemegangPolisModel().getKelurahan_tinggal_pp());
            Phrase kdpos_tgl = new Phrase(context.getString(R.string.kode_pos), normalFont);
            kdpos_tgl.setTabSettings(new TabSettings(250f));
            kdpos_tgl.add(Chunk.TABBING);
            kdpos_tgl.add(new Chunk("     : " + me.getPemegangPolisModel().getKdpos_tinggal_pp() + "\n", boldFont));
            document.add(kdpos_tgl);

            Phrase telp1_tgl = new Phrase(context.getString(R.string.telp_1), normalFont);
            telp1_tgl.setTabSettings(new TabSettings(250f));
            telp1_tgl.add(Chunk.TABBING);
            telp1_tgl.add(new Chunk("     : " + me.getPemegangPolisModel().getKdtelp1_tinggal_pp() + " - " + me.getPemegangPolisModel().getTelp1_tinggal_pp() + "\n", boldFont));
            document.add(telp1_tgl);

            Phrase telp2_tgl = new Phrase(context.getString(R.string.telp_2), normalFont);
            telp2_tgl.setTabSettings(new TabSettings(250f));
            telp2_tgl.add(Chunk.TABBING);
            telp2_tgl.add(new Chunk("     : " + me.getPemegangPolisModel().getKdtelp2_tinggal_pp() + " - " + me.getPemegangPolisModel().getTelp2_tinggal_pp() + "\n", boldFont));
            document.add(telp2_tgl);

            Phrase fax_tgl = new Phrase(context.getString(R.string.no_fax), normalFont);
            fax_tgl.setTabSettings(new TabSettings(250f));
            fax_tgl.add(Chunk.TABBING);
            fax_tgl.add(new Chunk("     : " + me.getPemegangPolisModel().getKdfax_tinggal_pp() + " - " + me.getPemegangPolisModel().getFax_tinggal_pp() + "\n", boldFont));
            document.add(fax_tgl);

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_alamat_tagihan, 0);
            Phrase almttghn = new Phrase(context.getString(R.string.alamat_tagihan_korespondensi), normalFont);
            almttghn.setTabSettings(new TabSettings(250f));
            almttghn.add(Chunk.TABBING);
            almttghn.add(new Chunk(("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getPemegangPolisModel().getTagihan_pp())) + "\n", boldFont));
            document.add(almttghn);

            Phrase hp1 = new Phrase(context.getString(R.string.hp1), normalFont);
            hp1.setTabSettings(new TabSettings(250f));
            hp1.add(Chunk.TABBING);
            hp1.add(new Chunk("     : " + me.getPemegangPolisModel().getHp1_pp() + "\n", boldFont));
            document.add(hp1);

            Phrase hp2 = new Phrase(context.getString(R.string.hp2), normalFont);
            hp2.setTabSettings(new TabSettings(250f));
            hp2.add(Chunk.TABBING);
            hp2.add(new Chunk("     : " + me.getPemegangPolisModel().getHp2_pp() + "\n", boldFont));
            document.add(hp2);

            Phrase email = new Phrase(context.getString(R.string.email), normalFont);
            email.setTabSettings(new TabSettings(250f));
            email.add(Chunk.TABBING);
            email.add(new Chunk("     : " + me.getPemegangPolisModel().getEmail_pp() + "\n", boldFont));
            document.add(email);

            Phrase nm_suami = new Phrase(context.getString(R.string.namasuami), normalFont);
            nm_suami.setTabSettings(new TabSettings(250f));
            nm_suami.add(Chunk.TABBING);
            nm_suami.add(new Chunk("     : " + me.getPemegangPolisModel().getSuamirt_pp() + "\n", boldFont));
            document.add(nm_suami);

            Phrase ttl_suami = new Phrase(context.getString(R.string.tanggal_lahir_suami), normalFont);
            ttl_suami.setTabSettings(new TabSettings(250f));
            ttl_suami.add(Chunk.TABBING);
            ttl_suami.add(new Chunk("     : " + me.getPemegangPolisModel().getTtlsuami_rt_pp() + "\n", boldFont));
            document.add(ttl_suami);

            Phrase umr_suami = new Phrase(context.getString(R.string.umur), normalFont);
            umr_suami.setTabSettings(new TabSettings(250f));
            umr_suami.add(Chunk.TABBING);
            umr_suami.add(new Chunk("     : " + me.getPemegangPolisModel().getUsiasuami_rt_pp() + "\n", boldFont));
            document.add(umr_suami);

            Phrase klsfks_suami = new Phrase(context.getString(R.string.klasifikasi_pekerjaan_suami), normalFont);
            klsfks_suami.setTabSettings(new TabSettings(250f));
            klsfks_suami.add(Chunk.TABBING);
            klsfks_suami.add(new Chunk("     : " + me.getPemegangPolisModel().getPekerjaansuami_rt_pp() + "\n", boldFont));
            document.add(klsfks_suami);

            Phrase jbtn_suami = new Phrase(context.getString(R.string.jabatan) + "     : ", normalFont);
            jbtn_suami.add(new Chunk(me.getPemegangPolisModel().getJabatansuami_rt_pp() + "\n", boldFont));
            document.add(jbtn_suami);

            Phrase nmprshn_suami = new Phrase(context.getString(R.string.nama_perusahaan_suami) + "     : ", normalFont);
            nmprshn_suami.add(new Chunk(me.getPemegangPolisModel().getPerusahaansuami_rt_pp() + "\n", boldFont));
            document.add(nmprshn_suami);

            Phrase bidusaha_suami = new Phrase(context.getString(R.string.bidang_usaha_suami), normalFont);
            bidusaha_suami.setTabSettings(new TabSettings(250f));
            bidusaha_suami.add(Chunk.TABBING);
            bidusaha_suami.add(new Chunk("     : " + me.getPemegangPolisModel().getBidusaha_suamirt_pp() + "\n", boldFont));
            document.add(bidusaha_suami);

            Phrase npwp_suami = new Phrase(context.getString(R.string.npwpsuami), normalFont);
            npwp_suami.setTabSettings(new TabSettings(250f));
            npwp_suami.add(Chunk.TABBING);
            npwp_suami.add(new Chunk("     : " + me.getPemegangPolisModel().getNpwp_suamirt_pp() + "\n", boldFont));
            document.add(npwp_suami);

            Phrase pnghsln_pertahun = new Phrase(context.getString(R.string.penghasilan_pertahun), normalFont);
            pnghsln_pertahun.setTabSettings(new TabSettings(250f));
            pnghsln_pertahun.add(Chunk.TABBING);
            pnghsln_pertahun.add(new Chunk("     : " + me.getPemegangPolisModel().getPenghasilan_suamithn_pp() + "\n", boldFont));
            document.add(pnghsln_pertahun);

            Phrase nm_ayah = new Phrase(context.getString(R.string.namaayah), normalFont);
            nm_ayah.setTabSettings(new TabSettings(250f));
            nm_ayah.add(Chunk.TABBING);
            nm_ayah.add(new Chunk("     : " + me.getPemegangPolisModel().getAyah_pp() + "\n", boldFont));
            document.add(nm_ayah);

            Phrase ttl_ayah = new Phrase(context.getString(R.string.tanggal_lahir_ayah), normalFont);
            ttl_ayah.setTabSettings(new TabSettings(250f));
            ttl_ayah.add(Chunk.TABBING);
            ttl_ayah.add(new Chunk("     : " + me.getPemegangPolisModel().getTtl_ayah_pp() + "\n", boldFont));
            document.add(ttl_ayah);

            Phrase usia_ayah = new Phrase(context.getString(R.string.umur), normalFont);
            usia_ayah.setTabSettings(new TabSettings(250f));
            usia_ayah.add(Chunk.TABBING);
            usia_ayah.add(new Chunk("     : " + me.getPemegangPolisModel().getUsia_ayah_pp() + "\n", boldFont));
            document.add(usia_ayah);

            Phrase klsfks_ayah = new Phrase(context.getString(R.string.klasifikasi_pekerjaan_ayah), normalFont);
            klsfks_ayah.setTabSettings(new TabSettings(250f));
            klsfks_ayah.add(Chunk.TABBING);
            klsfks_ayah.add(new Chunk("     : " + me.getPemegangPolisModel().getPekerjaan_ayah_pp() + "\n", boldFont));
            document.add(klsfks_ayah);

            Phrase jbtn_ayah = new Phrase(context.getString(R.string.jabatan) , normalFont);
            jbtn_ayah.setTabSettings(new TabSettings(250f));
            jbtn_ayah.add(Chunk.TABBING);
            jbtn_ayah.add(new Chunk("     : " + me.getPemegangPolisModel().getJabatan_ayah_pp() + "\n", boldFont));
            document.add(jbtn_ayah);

            Phrase nmprshn_ayah = new Phrase(context.getString(R.string.nama_perusahaan_ayah), normalFont);
            nmprshn_ayah.add(new Chunk(me.getPemegangPolisModel().getPerusahaan_ayah_pp() + "\n", boldFont));
            document.add(nmprshn_ayah);

            Phrase bidusaha_ayah = new Phrase(context.getString(R.string.bidang_usaha_ayah), normalFont);
            bidusaha_ayah.setTabSettings(new TabSettings(250f));
            bidusaha_ayah.add(Chunk.TABBING);
            bidusaha_ayah.add(new Chunk("     : " + me.getPemegangPolisModel().getBidusaha_ayah_pp() + "\n", boldFont));
            document.add(bidusaha_ayah);

            Phrase npwp_ayah = new Phrase(context.getString(R.string.npwpayah), normalFont);
            npwp_ayah.setTabSettings(new TabSettings(250f));
            npwp_ayah.add(Chunk.TABBING);
            npwp_ayah.add(new Chunk("     : " + me.getPemegangPolisModel().getNpwp_ayah_pp() + "\n", boldFont));
            document.add(npwp_ayah);

            Phrase penghsln_ayah = new Phrase(context.getString(R.string.penghasilan_pertahun), normalFont);
            penghsln_ayah.setTabSettings(new TabSettings(250f));
            penghsln_ayah.add(Chunk.TABBING);
            penghsln_ayah.add(new Chunk("     : " + me.getPemegangPolisModel().getPenghasilan_ayah_pp() + "\n", boldFont));
            document.add(penghsln_ayah);

            Phrase nama_ibu = new Phrase(context.getString(R.string.namaibu), normalFont);
            nama_ibu.setTabSettings(new TabSettings(250f));
            nama_ibu.add(Chunk.TABBING);
            nama_ibu.add(new Chunk("     : " + me.getPemegangPolisModel().getNm_ibu_pp() + "\n", boldFont));
            document.add(nama_ibu);

            Phrase ttl_ibu = new Phrase(context.getString(R.string.tanggal_lahir_ibu), normalFont);
            ttl_ibu.setTabSettings(new TabSettings(250f));
            ttl_ibu.add(Chunk.TABBING);
            ttl_ibu.add(new Chunk("     : " + me.getPemegangPolisModel().getTtl_ibu_pp() + "\n", boldFont));
            document.add(ttl_ibu);

            Phrase umur_ibu = new Phrase(context.getString(R.string.umur), normalFont);
            umur_ibu.setTabSettings(new TabSettings(250f));
            umur_ibu.add(Chunk.TABBING);
            umur_ibu.add(new Chunk("     : " + me.getPemegangPolisModel().getUsia_ibu_pp() + "\n", boldFont));
            document.add(umur_ibu);

            Phrase klsfks_ibu = new Phrase(context.getString(R.string.klasifikasi_pekerjaan_ibu), normalFont);
            klsfks_ibu.setTabSettings(new TabSettings(250f));
            klsfks_ibu.add(Chunk.TABBING);
            klsfks_ibu.add(new Chunk("     : " + me.getPemegangPolisModel().getPekerjaan_ibu_pp() + "\n", boldFont));
            document.add(klsfks_ibu);

            Phrase jbtn_ibu = new Phrase(context.getString(R.string.jabatan), normalFont);
            jbtn_ibu.setTabSettings(new TabSettings(250f));
            jbtn_ibu.add(Chunk.TABBING);
            jbtn_ibu.add(new Chunk("     : " + me.getPemegangPolisModel().getJabatan_ibu_pp() + "\n", boldFont));
            document.add(jbtn_ibu);

            Phrase nmprshn_ibu = new Phrase(context.getString(R.string.nama_perusahaan_ibu) + "     : ", normalFont);
            nmprshn_ibu.add(new Chunk(me.getPemegangPolisModel().getPerusahaan_ibu_pp() + "\n", boldFont));
            document.add(nmprshn_ibu);

            Phrase bidusaha_ibu = new Phrase(context.getString(R.string.bidang_usaha_ibu), normalFont);
            bidusaha_ibu.setTabSettings(new TabSettings(250f));
            bidusaha_ibu.add(Chunk.TABBING);
            bidusaha_ibu.add(new Chunk("     : " + me.getPemegangPolisModel().getBidusaha_ibu_pp() + "\n", boldFont));
            document.add(bidusaha_ibu);

            Phrase npwp_ibu = new Phrase(context.getString(R.string.npwpibu), normalFont);
            npwp_ibu.setTabSettings(new TabSettings(250f));
            npwp_ibu.add(Chunk.TABBING);
            npwp_ibu.add(new Chunk("     : " + me.getPemegangPolisModel().getNpwp_ibu_pp() + "\n", boldFont));
            document.add(npwp_ibu);

            Phrase pnghsln_ibu = new Phrase(context.getString(R.string.penghasilan_pertahun), normalFont);
            pnghsln_ibu.setTabSettings(new TabSettings(250f));
            pnghsln_ibu.add(Chunk.TABBING);
            pnghsln_ibu.add(new Chunk( "     : " + me.getPemegangPolisModel().getPenghasilan_ibu_pp() + "\n", boldFont));
            document.add(pnghsln_ibu);

            Phrase npwp_pp = new Phrase(context.getString(R.string.npwp), normalFont);
            npwp_pp.setTabSettings(new TabSettings(250f));
            npwp_pp.add(Chunk.TABBING);
            npwp_pp.add(new Chunk("     : " + me.getPemegangPolisModel().getNpwp_pp() + "\n", boldFont));
            document.add(npwp_pp);

            Phrase pndptn = new Phrase("Total pendapatan bersih per tahun", normalFont);
            pndptn.setTabSettings(new TabSettings(250f));
            pndptn.add(Chunk.TABBING);
            pndptn.add(new Chunk("     : " + me.getPemegangPolisModel().getPendapatan_pp() + "\n", boldFont));
            document.add(pndptn);

            if (!me.getPemegangPolisModel().getD_gaji_pp().equals("")) {
                Phrase smbrdn_gaji = new Phrase(context.getString(R.string.sumber_dana), normalFont);
                smbrdn_gaji.setTabSettings(new TabSettings(250f));
                smbrdn_gaji.add(Chunk.TABBING);
                smbrdn_gaji.add(new Chunk("     : Gaji" + "\n", boldFont));
                document.add(smbrdn_gaji);
            }
            if (!me.getPemegangPolisModel().getD_tabungan_pp().equals("")) {
                Phrase smbrdn_tbngn = new Phrase(context.getString(R.string.sumber_dana), normalFont);
                smbrdn_tbngn.setTabSettings(new TabSettings(250f));
                smbrdn_tbngn.add(Chunk.TABBING);
                smbrdn_tbngn.add(new Chunk("     : Tabungan" + "\n", boldFont));
                document.add(smbrdn_tbngn);
            }
            if (!me.getPemegangPolisModel().getD_warisan_pp().equals("")) {
                Phrase smbrdn_wrsn = new Phrase(context.getString(R.string.sumber_dana), normalFont);
                smbrdn_wrsn.setTabSettings(new TabSettings(250f));
                smbrdn_wrsn.add(Chunk.TABBING);
                smbrdn_wrsn.add(new Chunk("     : Warisan" + "\n", boldFont));
                document.add(smbrdn_wrsn);
            }
            if (!me.getPemegangPolisModel().getD_hibah_pp().equals("")) {
                Phrase smbrdn_hbh = new Phrase(context.getString(R.string.sumber_dana), normalFont);
                smbrdn_hbh.setTabSettings(new TabSettings(250f));
                smbrdn_hbh.add(Chunk.TABBING);
                smbrdn_hbh.add(new Chunk("     : Hibah" + "\n", boldFont));
                document.add(smbrdn_hbh);
            }
            if (!me.getPemegangPolisModel().getD_lainnya_pp().equals("")) {
                Phrase smbrdn_lain = new Phrase(context.getString(R.string.sumber_dana), normalFont);
                smbrdn_lain.setTabSettings(new TabSettings(250f));
                smbrdn_lain.add(Chunk.TABBING);
                smbrdn_lain.add(new Chunk("     : Lainnya" + "\n", boldFont));
                document.add(smbrdn_lain);
            }
            if (!me.getPemegangPolisModel().getEdit_d_lainnya_pp().equals("")) {
                Phrase smbrdn_ket = new Phrase(context.getString(R.string.sebutkan) + "     : ", normalFont);
                smbrdn_ket.setTabSettings(new TabSettings(250f));
                smbrdn_ket.add(Chunk.TABBING);
                smbrdn_ket.add(new Chunk("     : " + me.getPemegangPolisModel().getEdit_d_lainnya_pp() + "\n", boldFont));
                document.add(smbrdn_ket);
            }

            Phrase polis = new Phrase(context.getString(R.string.polis), normalFont);
            polis.add(new Chunk(me.getPemegangPolisModel().getKrm_polis_pp() + "\n", boldFont));
            document.add(polis);
//bernard
            if (!me.getPemegangPolisModel().getT_proteksi_pp().equals("")) {
                Phrase tujuan_p = new Phrase(context.getString(R.string.tujuan_pengajuan_asuransi), normalFont);
                tujuan_p.add(new Chunk("  : Proteksi" + "\n", boldFont));
                document.add(tujuan_p); // masih belum check tujuan asuransi
            }
            if (!me.getPemegangPolisModel().getT_inves_pp().equals("")) {
                Phrase tujuan_i = new Phrase(context.getString(R.string.tujuan_pengajuan_asuransi), normalFont);
                tujuan_i.add(new Chunk("  : Investasi" + "\n", boldFont));
                document.add(tujuan_i);
            }
            if (!me.getPemegangPolisModel().getT_lainnya_pp().equals("")) {
                Phrase tujuan_l = new Phrase(context.getString(R.string.tujuan_pengajuan_asuransi), normalFont);
                tujuan_l.add(new Chunk("  : Lainnya" + "\n", boldFont));
                document.add(tujuan_l);
            }
            if (!me.getPemegangPolisModel().getEdit_t_lainnya().equals("")) {
                Phrase tujuan_k = new Phrase(context.getString(R.string.sebutkan) + "     : ", normalFont);
                tujuan_k.setTabSettings(new TabSettings(250f));
                tujuan_k.add(Chunk.TABBING);
                tujuan_k.add(new Chunk("     : " + me.getPemegangPolisModel().getEdit_t_lainnya() + "\n", boldFont));
                document.add(tujuan_k);
            }

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_relasi, 0);
            Phrase hub_pp = new Phrase(context.getString(R.string.hubungan_calon_pembayar_premi) + "     : ", normalFont);
            hub_pp.add(new Chunk((MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getPemegangPolisModel().getHubungancp_pp())) + "\n", boldFont));
            document.add(hub_pp);

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_relasi, 0);
            Phrase hub_pp_tt = new Phrase(context.getString(R.string.hubungan_pp_dengan_tt) + "     : ", normalFont);
            hub_pp_tt.add(new Chunk((MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getPemegangPolisModel().getHubungan_pp())) + "\n", boldFont));
            document.add(hub_pp_tt);

            document.newPage();
            document.add(new Paragraph(context.getString(R.string.tertanggung) + "\n\n", normalFont));

            Phrase nm_tt = new Phrase(context.getString(R.string.nama_lengkap), normalFont);
            nm_tt.setTabSettings(new TabSettings(250f));
            nm_tt.add(Chunk.TABBING);
            nm_tt.add(new Chunk("     : " + me.getTertanggungModel().getNama_tt() + "\n", boldFont));
            document.add(nm_tt);

            Phrase tpt_tt = new Phrase(context.getString(R.string.tempat_lahir), normalFont);
            tpt_tt.setTabSettings(new TabSettings(250f));
            tpt_tt.add(Chunk.TABBING);
            tpt_tt.add(new Chunk("     : " + me.getTertanggungModel().getTempat_tt() + "\n", boldFont));
            document.add(tpt_tt);

            Phrase tgl_tt = new Phrase(context.getString(R.string.tanggal_lahir), normalFont);
            tgl_tt.setTabSettings(new TabSettings(250f));
            tgl_tt.add(Chunk.TABBING);
            tgl_tt.add(new Chunk("     : " + me.getTertanggungModel().getTtl_tt() + "\n", boldFont));
            document.add(tgl_tt);

            Phrase umur_tt = new Phrase(context.getString(R.string.umur), normalFont);
            umur_tt.setTabSettings(new TabSettings(250f));
            umur_tt.add(Chunk.TABBING);
            umur_tt.add(new Chunk("     : " + me.getTertanggungModel().getUsia_tt() + "\n", boldFont));
            document.add(umur_tt);

            if (me.getTertanggungModel().getJekel_tt() == 0) {
                Phrase jekel_w = new Phrase(context.getString(R.string.jenis_kelamin), normalFont);
                jekel_w.setTabSettings(new TabSettings(250f));
                jekel_w.add(Chunk.TABBING);
                jekel_w.add(new Chunk("     : Wanita" + "\n", boldFont));
                document.add(jekel_w);
            } else {
                Phrase jekel_p = new Phrase(context.getString(R.string.jenis_kelamin), normalFont);
                jekel_p.setTabSettings(new TabSettings(250f));
                jekel_p.add(Chunk.TABBING);
                jekel_p.add(new Chunk("     : Pria" + "\n", boldFont));
                document.add(jekel_p);
            }

            if (me.getTertanggungModel().getGreencard_tt() == 0) {
                Phrase card_t = new Phrase(context.getString(R.string.wn_amerika_serikat), normalFont);
                card_t.add(new Chunk("   : Tidak" + "\n", boldFont));
                document.add(card_t);
            } else {
                Phrase card_y = new Phrase(context.getString(R.string.wn_amerika_serikat), normalFont);
                card_y.add(new Chunk("   : Ya" + "\n", boldFont));
                document.add(card_y);
            }

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_agama, 0);
            Phrase agma_tt = new Phrase(context.getString(R.string.agama), normalFont);
            agma_tt.setTabSettings(new TabSettings(250f));
            agma_tt.add(Chunk.TABBING);
            agma_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getTertanggungModel().getAgama_tt()) + "\n", boldFont));
            document.add(agma_tt);

            Phrase agmalain_tt = new Phrase(context.getString(R.string.keterangan), normalFont);
            agmalain_tt.setTabSettings(new TabSettings(250f));
            agmalain_tt.add(Chunk.TABBING);
            agmalain_tt.add(new Chunk("     : " + me.getTertanggungModel().getAgama_lain_tt() + "\n", boldFont));
            document.add(agmalain_tt);

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_pendidikan, 0);
            Phrase pend_tt = new Phrase(context.getString(R.string.pendidikan_terakhir_pp), normalFont);
            pend_tt.setTabSettings(new TabSettings(250f));
            pend_tt.add(Chunk.TABBING);
            pend_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getTertanggungModel().getPendidikan_tt()) + "\n", boldFont));
            document.add(pend_tt);

            Phrase gelar_tt = new Phrase(context.getString(R.string.gelar_pendidikan), normalFont);
            gelar_tt.setTabSettings(new TabSettings(250f));
            gelar_tt.add(Chunk.TABBING);
            gelar_tt.add(new Chunk("     : " + me.getTertanggungModel().getGelar_tt() + "\n", boldFont));
            document.add(gelar_tt);

            Phrase nmibu_tt = new Phrase(context.getString(R.string.nama_ibu), normalFont);
            nmibu_tt.setTabSettings(new TabSettings(250f));
            nmibu_tt.add(Chunk.TABBING);
            nmibu_tt.add(new Chunk("     : " + me.getTertanggungModel().getNama_ibu_tt() + "\n", boldFont));
            document.add(nmibu_tt);

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_warganegara, 0);
            Phrase wn_tt = new Phrase(context.getString(R.string.warga_negara), normalFont);
            wn_tt.setTabSettings(new TabSettings(250f));
            wn_tt.add(Chunk.TABBING);
            wn_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getTertanggungModel().getWarga_negara_tt()) + "\n", boldFont));
            document.add(wn_tt);

            Phrase nmprshn_tt = new Phrase(context.getString(R.string.nama_perusahaan_tempat_bekerja), normalFont);
            nmprshn_tt.setTabSettings(new TabSettings(250f));
            nmprshn_tt.add(Chunk.TABBING);
            nmprshn_tt.add(new Chunk("     : " + me.getTertanggungModel().getNm_perusahaan_tt() + "\n", boldFont));
            document.add(nmprshn_tt);

            Phrase klsfks_tt = new Phrase(context.getString(R.string.klasifikasi_pekerjaan), normalFont);
            klsfks_tt.setTabSettings(new TabSettings(250f));
            klsfks_tt.add(Chunk.TABBING);
            klsfks_tt.add(new Chunk("     : " + me.getTertanggungModel().getKlasifikasi_pekerjaan_tt() + "\n", boldFont));
            document.add(klsfks_tt);

            Phrase jbtn_tt = new Phrase(context.getString(R.string.jabatan), normalFont);
            jbtn_tt.setTabSettings(new TabSettings(250f));
            jbtn_tt.add(Chunk.TABBING);
            jbtn_tt.add(new Chunk("     : " + me.getTertanggungModel().getJabatan_klasifikasi_tt() + "\n", boldFont));
            document.add(jbtn_tt);

            Phrase uraian_tt = new Phrase(context.getString(R.string.uraikan_tugas), normalFont);
            uraian_tt.setTabSettings(new TabSettings(250f));
            uraian_tt.add(Chunk.TABBING);
            uraian_tt.add(new Chunk("     : " + me.getTertanggungModel().getUraian_pekerjaan_tt() + "\n", boldFont));
            document.add(uraian_tt);

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_identity, 0);
            Phrase bukti_tt = new Phrase(context.getString(R.string.bukti_identitas), normalFont);
            bukti_tt.setTabSettings(new TabSettings(250f));
            bukti_tt.add(Chunk.TABBING);
            bukti_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getTertanggungModel().getBukti_identitas_tt()) + "\n", boldFont));
            document.add(bukti_tt);

            Phrase no_tt = new Phrase(context.getString(R.string.no_identitas), normalFont);
            no_tt.setTabSettings(new TabSettings(250f));
            no_tt.add(Chunk.TABBING);
            no_tt.add(new Chunk("     : " + me.getTertanggungModel().getNo_identitas_tt() + "\n", boldFont));
            document.add(no_tt);

            Phrase berlaku_tt = new Phrase(context.getString(R.string.berlaku_hingga), normalFont);
            berlaku_tt.setTabSettings(new TabSettings(250f));
            berlaku_tt.add(Chunk.TABBING);
            berlaku_tt.add(new Chunk("     : " + me.getTertanggungModel().getTgl_berlaku_tt() + "\n", boldFont));
            document.add(berlaku_tt);

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_marital, 0);
            Phrase status_tt = new Phrase(context.getString(R.string.status_nikah), normalFont);
            status_tt.setTabSettings(new TabSettings(250f));
            status_tt.add(Chunk.TABBING);
            status_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getTertanggungModel().getStatus_tt()) + "\n", boldFont));
            document.add(status_tt);

            Phrase alamat_rmh_tt = new Phrase(context.getString(R.string.alamat) + " " + "Rumah" , normalFont);
            alamat_rmh_tt.setTabSettings(new TabSettings(250f));
            alamat_rmh_tt.add(Chunk.TABBING);
            alamat_rmh_tt.add(new Chunk("     : " + me.getTertanggungModel().getAlamat_tt() + "\n", boldFont));
            document.add(alamat_rmh_tt);

            ListDropDownString = new E_Select(context).getLst_Propinsi_PP();
            Phrase provinsi_tt = new Phrase(context.getString(R.string.provinsi), normalFont);
            provinsi_tt.setTabSettings(new TabSettings(250f));
            provinsi_tt.add(Chunk.TABBING);
            provinsi_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getPropinsi_tt())) + "\n", boldFont));
            document.add(provinsi_tt);

            ListDropDownString = new E_Select(context).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_tt());
            Phrase kabupaten_tt = new Phrase(context.getString(R.string.kabupaten), normalFont);
            kabupaten_tt.setTabSettings(new TabSettings(250f));
            kabupaten_tt.add(Chunk.TABBING);
            kabupaten_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKabupaten_tt())) + "\n", boldFont));
            document.add(kabupaten_tt);

            ListDropDownString = new E_Select(context).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_tt());
            Phrase kecamatan_tt = new Phrase(context.getString(R.string.kecamatan), normalFont);
            kecamatan_tt.setTabSettings(new TabSettings(250f));
            kecamatan_tt.add(Chunk.TABBING);
            kecamatan_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKecamatan_tt())) + "\n", boldFont));
            document.add(kecamatan_tt);

            ListDropDownString = new E_Select(context).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_tt());
            Phrase kelurahan_tt = new Phrase(context.getString(R.string.kelurahan), normalFont);
            kelurahan_tt.setTabSettings(new TabSettings(250f));
            kelurahan_tt.add(Chunk.TABBING);
            kelurahan_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKelurahan_tt())) + "\n", boldFont));
            document.add(kelurahan_tt);

            ListDropDownString = new E_Select(context).getLst_Kelurahan_PP(me.getTertanggungModel().getKelurahan_tt());
            Phrase kdpos_rmh_tt = new Phrase(context.getString(R.string.kode_pos), normalFont);
            kdpos_rmh_tt.setTabSettings(new TabSettings(250f));
            kdpos_rmh_tt.add(Chunk.TABBING);
            kdpos_rmh_tt.add(new Chunk("     : " + me.getTertanggungModel().getKdpos_tt() + "\n", boldFont));
            document.add(kdpos_rmh_tt);

            Phrase telp1_rmh_tt = new Phrase(context.getString(R.string.telp_1), normalFont);
            telp1_rmh_tt.setTabSettings(new TabSettings(250f));
            telp1_rmh_tt.add(Chunk.TABBING);
            telp1_rmh_tt.add(new Chunk("     : " + me.getTertanggungModel().getKdtelp1_tt() + " - " + me.getTertanggungModel().getTelp1_tt() + "\n", boldFont));
            document.add(telp1_rmh_tt);

            Phrase telp2_rmh_tt = new Phrase(context.getString(R.string.telp_2), normalFont);
            telp2_rmh_tt.setTabSettings(new TabSettings(250f));
            telp2_rmh_tt.add(Chunk.TABBING);
            telp2_rmh_tt.add(new Chunk("     : " + me.getTertanggungModel().getKdtelp2_tt() + " - " + me.getTertanggungModel().getTelp2_tt() + "\n", boldFont));
            document.add(telp2_rmh_tt);

            Phrase alamat_kntr_tt = new Phrase(context.getString(R.string.alamat) + " " + "Kantor", normalFont);
            alamat_kntr_tt.setTabSettings(new TabSettings(250f));
            alamat_kntr_tt.add(Chunk.TABBING);
            alamat_kntr_tt.add(new Chunk("     : " + me.getTertanggungModel().getAlamat_kantor_tt() + "\n", boldFont));
            document.add(alamat_kntr_tt);

            ListDropDownString = new E_Select(context).getLst_Propinsi_PP();
            Phrase provinsi_kntr_tt = new Phrase(context.getString(R.string.provinsi), normalFont);
            provinsi_kntr_tt.setTabSettings(new TabSettings(250f));
            provinsi_kntr_tt.add(Chunk.TABBING);
            provinsi_kntr_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getPropinsi_kantor_tt())) + "\n", boldFont));
            document.add(provinsi_kntr_tt);

            ListDropDownString = new E_Select(context).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_kantor_tt());
            Phrase kabupaten_kntr_tt = new Phrase(context.getString(R.string.kabupaten), normalFont);
            kabupaten_kntr_tt.setTabSettings(new TabSettings(250f));
            kabupaten_kntr_tt.add(Chunk.TABBING);
            kabupaten_kntr_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKabupaten_kantor_tt())) + "\n", boldFont));
            document.add(kabupaten_kntr_tt);

            ListDropDownString = new E_Select(context).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_kantor_tt());
            Phrase kecamatan_kntr_tt = new Phrase(context.getString(R.string.kecamatan), normalFont);
            kecamatan_kntr_tt.setTabSettings(new TabSettings(250f));
            kecamatan_kntr_tt.add(Chunk.TABBING);
            kecamatan_kntr_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKecamatan_kantor_tt())) + "\n", boldFont));
            document.add(kecamatan_kntr_tt);

            ListDropDownString = new E_Select(context).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_kantor_tt());
            Phrase kelurahan_kntr_tt = new Phrase(context.getString(R.string.kelurahan), normalFont);
            kelurahan_kntr_tt.setTabSettings(new TabSettings(250f));
            kelurahan_kntr_tt.add(Chunk.TABBING);
            kelurahan_kntr_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKelurahan_kantor_tt())) + "\n", boldFont));
            document.add(kelurahan_kntr_tt);

            ListDropDownString = new E_Select(context).getLst_Kelurahan_PP(me.getTertanggungModel().getKelurahan_kantor_tt());
            Phrase kdpos_kntr_tt = new Phrase(context.getString(R.string.kode_pos), normalFont);
            kdpos_kntr_tt.setTabSettings(new TabSettings(250f));
            kdpos_kntr_tt.add(Chunk.TABBING);
            kdpos_kntr_tt.add(new Chunk("     : " + me.getTertanggungModel().getKdpos_kantor_tt() + "\n", boldFont));
            document.add(kdpos_kntr_tt);

            Phrase telp1_kntr_tt = new Phrase(context.getString(R.string.telp_1), normalFont);
            telp1_kntr_tt.setTabSettings(new TabSettings(250f));
            telp1_kntr_tt.add(Chunk.TABBING);
            telp1_kntr_tt.add(new Chunk("     : " + me.getTertanggungModel().getKdtelp1_kantor_tt() + " - " + me.getTertanggungModel().getTelp1_kantor_tt() + "\n", boldFont));
            document.add(telp1_kntr_tt);

            Phrase telp2_kntr_tt = new Phrase(context.getString(R.string.telp_2), normalFont);
            telp2_kntr_tt.setTabSettings(new TabSettings(250f));
            telp2_kntr_tt.add(Chunk.TABBING);
            telp2_kntr_tt.add(new Chunk("     : " + me.getTertanggungModel().getKdtelp2_kantor_tt() + " - " + me.getTertanggungModel().getTelp2_kantor_tt() + "\n", boldFont));
            document.add(telp2_kntr_tt);

            Phrase alamat_tgl_tt = new Phrase(context.getString(R.string.alamat) + " " + "Tinggal", normalFont);
            alamat_tgl_tt.setTabSettings(new TabSettings(250f));
            alamat_tgl_tt.add(Chunk.TABBING);
            alamat_tgl_tt.add(new Chunk("     : " + me.getTertanggungModel().getAlamat_tinggal_tt() + "\n", boldFont));
            document.add(alamat_tgl_tt);

            ListDropDownString = new E_Select(context).getLst_Propinsi_PP();
            Phrase provinsi_tgl_tt = new Phrase(context.getString(R.string.provinsi), normalFont);
            provinsi_tgl_tt.setTabSettings(new TabSettings(250f));
            provinsi_tgl_tt.add(Chunk.TABBING);
            provinsi_tgl_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getPropinsi_tinggal_tt())) + "\n", boldFont));
            document.add(provinsi_tgl_tt);

            ListDropDownString = new E_Select(context).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_tinggal_tt());
            Phrase kabupaten_tgl_tt = new Phrase(context.getString(R.string.kabupaten), normalFont);
            kabupaten_tgl_tt.setTabSettings(new TabSettings(250f));
            kabupaten_tgl_tt.add(Chunk.TABBING);
            kabupaten_tgl_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKabupaten_tinggal_tt())) + "\n", boldFont));
            document.add(kabupaten_tgl_tt);

            ListDropDownString = new E_Select(context).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_tinggal_tt());
            Phrase kecamatan_tgl_tt = new Phrase(context.getString(R.string.kecamatan), normalFont);
            kecamatan_tgl_tt.setTabSettings(new TabSettings(250f));
            kecamatan_tgl_tt.add(Chunk.TABBING);
            kecamatan_tgl_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKecamatan_tinggal_tt())) + "\n", boldFont));
            document.add(kecamatan_tgl_tt);

            ListDropDownString = new E_Select(context).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_tinggal_tt());
            Phrase kelurahan_tgl_tt = new Phrase(context.getString(R.string.kelurahan), normalFont);
            kelurahan_tgl_tt.setTabSettings(new TabSettings(250f));
            kelurahan_tgl_tt.add(Chunk.TABBING);
            kelurahan_tgl_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKelurahan_tinggal_tt())) + "\n", boldFont));
            document.add(kelurahan_tgl_tt);

            ListDropDownString = new E_Select(context).getLst_Kelurahan_PP(me.getTertanggungModel().getKelurahan_tinggal_tt());
            Phrase kdpos_tgl_tt = new Phrase(context.getString(R.string.kode_pos), normalFont);
            kdpos_tgl_tt.setTabSettings(new TabSettings(250f));
            kdpos_tgl_tt.add(Chunk.TABBING);
            kdpos_tgl_tt.add(new Chunk("     : " + me.getTertanggungModel().getKdpos_tinggal_tt() + "\n", boldFont));
            document.add(kdpos_tgl_tt);

            Phrase telp1_tgl_tt = new Phrase(context.getString(R.string.telp_1), normalFont);
            telp1_tgl_tt.setTabSettings(new TabSettings(250f));
            telp1_tgl_tt.add(Chunk.TABBING);
            telp1_tgl_tt.add(new Chunk("     : " + me.getTertanggungModel().getKdtelp1_tinggal_tt() + " - " + me.getTertanggungModel().getTelp1_tinggal_tt() + "\n", boldFont));
            document.add(telp1_tgl_tt);

            Phrase telp2_tgl_tt = new Phrase(context.getString(R.string.telp_2), normalFont);
            telp2_tgl_tt.setTabSettings(new TabSettings(250f));
            telp2_tgl_tt.add(Chunk.TABBING);
            telp2_tgl_tt.add(new Chunk("     : " + me.getTertanggungModel().getKdtelp2_tinggal_tt() + " - " + me.getTertanggungModel().getTelp2_tinggal_tt() + "\n", boldFont));
            document.add(telp2_tgl_tt);

            Phrase fax_tgl_tt = new Phrase(context.getString(R.string.no_fax), normalFont);
            fax_tgl_tt.setTabSettings(new TabSettings(250f));
            fax_tgl_tt.add(Chunk.TABBING);
            fax_tgl_tt.add(new Chunk("     : " + me.getTertanggungModel().getKdfax_tinggal_tt() + " - " + me.getTertanggungModel().getFax_tinggal_tt() + "\n", boldFont));
            document.add(fax_tgl_tt);

            Phrase hp1_tt = new Phrase(context.getString(R.string.hp1), normalFont);
            hp1_tt.setTabSettings(new TabSettings(250f));
            hp1_tt.add(Chunk.TABBING);
            hp1_tt.add(new Chunk("     : " + me.getTertanggungModel().getHp1_tt() + "\n", boldFont));
            document.add(hp1_tt);

            Phrase hp2_tt = new Phrase(context.getString(R.string.hp2), normalFont);
            hp2_tt.setTabSettings(new TabSettings(250f));
            hp2_tt.add(Chunk.TABBING);
            hp2_tt.add(new Chunk("     : " + me.getTertanggungModel().getHp2_tt() + "\n", boldFont));
            document.add(hp2_tt);

            Phrase email_tt = new Phrase(context.getString(R.string.email), normalFont);
            email_tt.setTabSettings(new TabSettings(250f));
            email_tt.add(Chunk.TABBING);
            email_tt.add(new Chunk("     : " + me.getTertanggungModel().getEmail_tt() + "\n", boldFont));
            document.add(email_tt);

            Phrase npwp_tt = new Phrase(context.getString(R.string.npwp), normalFont);
            npwp_tt.setTabSettings(new TabSettings(250f));
            npwp_tt.add(Chunk.TABBING);
            npwp_tt.add(new Chunk("     : " + me.getTertanggungModel().getNpwp_tt() + "\n", boldFont));
            document.add(npwp_tt);

            Phrase pndptn_tt = new Phrase("Total pendapatan bersih per tahun", normalFont);
            pndptn_tt.setTabSettings(new TabSettings(250f));
            pndptn_tt.add(Chunk.TABBING);
            pndptn_tt.add(new Chunk("     : " + me.getTertanggungModel().getPenghasilan_thn_tt() + "\n", boldFont));
            document.add(pndptn_tt);

            if (!me.getTertanggungModel().getDana_gaji_tt().equals("")) {
                Phrase smbrdn_gaji = new Phrase(context.getString(R.string.sumber_dana), normalFont);
                smbrdn_gaji.setTabSettings(new TabSettings(250f));
                smbrdn_gaji.add(Chunk.TABBING);
                smbrdn_gaji.add(new Chunk("     : Gaji" + "\n", boldFont));
                document.add(smbrdn_gaji);
            }
            if (!me.getTertanggungModel().getDana_tabungan_tt().equals("")) {
                Phrase smbrdn_tbngn = new Phrase(context.getString(R.string.sumber_dana), normalFont);
                smbrdn_tbngn.setTabSettings(new TabSettings(250f));
                smbrdn_tbngn.add(Chunk.TABBING);
                smbrdn_tbngn.add(new Chunk("     : Tabungan" + "\n", boldFont));
                document.add(smbrdn_tbngn);
            }
            if (!me.getTertanggungModel().getDana_warisan_tt().equals("")) {
                Phrase smbrdn_wrsn = new Phrase(context.getString(R.string.sumber_dana), normalFont);
                smbrdn_wrsn.setTabSettings(new TabSettings(250f));
                smbrdn_wrsn.add(Chunk.TABBING);
                smbrdn_wrsn.add(new Chunk("     : Warisan" + "\n", boldFont));
                document.add(smbrdn_wrsn);
            }
            if (!me.getTertanggungModel().getDana_hibah_tt().equals("")) {
                Phrase smbrdn_hbh = new Phrase(context.getString(R.string.sumber_dana), normalFont);
                smbrdn_hbh.setTabSettings(new TabSettings(250f));
                smbrdn_hbh.add(Chunk.TABBING);
                smbrdn_hbh.add(new Chunk("     : Hibah" + "\n", boldFont));
                document.add(smbrdn_hbh);
            }
            if (!me.getTertanggungModel().getDana_lainnya_tt().equals("")) {
                Phrase smbrdn_lain = new Phrase(context.getString(R.string.sumber_dana), normalFont);
                smbrdn_lain.setTabSettings(new TabSettings(250f));
                smbrdn_lain.add(Chunk.TABBING);
                smbrdn_lain.add(new Chunk("     : Lainnya" + "\n", boldFont));
                document.add(smbrdn_lain);
            }
            if (!me.getTertanggungModel().getDana_lainnya_tt().equals("")) {
                Phrase smbrdn_ket = new Phrase(context.getString(R.string.sebutkan), normalFont);
                smbrdn_ket.setTabSettings(new TabSettings(250f));
                smbrdn_ket.add(Chunk.TABBING);
                smbrdn_ket.add(new Chunk("     : " + me.getPemegangPolisModel().getEdit_d_lainnya_pp() + "\n", boldFont));
                document.add(smbrdn_ket);
            }

            if (!me.getTertanggungModel().getTujuan_proteksi_tt().equals("")) {
                Phrase tujuan_p = new Phrase(context.getString(R.string.tujuan_pengajuan_asuransi), normalFont);
                tujuan_p.add(new Chunk("  : Proteksi" + "\n", boldFont));
                document.add(tujuan_p); // masih belum check tujuan asuransi
            }
            if (!me.getTertanggungModel().getTujuan_inves_tt().equals("")) {
                Phrase tujuan_i = new Phrase(context.getString(R.string.tujuan_pengajuan_asuransi), normalFont);
                tujuan_i.add(new Chunk("  : Investasi" + "\n", boldFont));
                document.add(tujuan_i);
            }
            if (!me.getTertanggungModel().getTujuan_lainnya_tt().equals("")) {
                Phrase tujuan_l = new Phrase(context.getString(R.string.tujuan_pengajuan_asuransi), normalFont);
                tujuan_l.add(new Chunk("  : Lainnya" + "\n", boldFont));
                document.add(tujuan_l);
            }
            if (!me.getTertanggungModel().getTujuan_lainnya_tt().equals("")) {
                Phrase tujuan_k = new Phrase(context.getString(R.string.sebutkan), normalFont);
                tujuan_k.setTabSettings(new TabSettings(250f));
                tujuan_k.add(Chunk.TABBING);
                tujuan_k.add(new Chunk( "     : " + me.getPemegangPolisModel().getEdit_t_lainnya() + "\n", boldFont));
                document.add(tujuan_k);
            }

            document.newPage();
            document.add(new Paragraph(context.getString(R.string.calon_pembayar_premi) + "\n\n", normalFont));

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_relasi, 0);
            Phrase cpp = new Phrase(context.getString(R.string.calon_pembayar_premi), normalFont);
            cpp.setTabSettings(new TabSettings(250f));
            cpp.add(Chunk.TABBING);
            cpp.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getCalonPembayarPremiModel().getInt_ket_cp()) + "\n", boldFont));
            document.add(cpp);

            Phrase nmprshn_cpp = new Phrase(context.getString(R.string.nama_perusahaan), normalFont);
            nmprshn_cpp.setTabSettings(new TabSettings(250f));
            nmprshn_cpp.add(Chunk.TABBING);
            nmprshn_cpp.add(new Chunk( "     : " + me.getCalonPembayarPremiModel().getNama_perush_cp() + "\n", boldFont));
            document.add(nmprshn_cpp);

            Phrase almt_cpp = new Phrase(context.getString(R.string.alamat_perusahaan), normalFont);
            almt_cpp.setTabSettings(new TabSettings(250f));
            almt_cpp.add(Chunk.TABBING);
            almt_cpp.add(new Chunk("     : " + me.getCalonPembayarPremiModel().getAlmt_perush_cp() + "\n", boldFont));
            document.add(almt_cpp);

            Phrase tgl_cpp = new Phrase(context.getString(R.string.tanggal_pendirian), normalFont);
            tgl_cpp.setTabSettings(new TabSettings(250f));
            tgl_cpp.add(Chunk.TABBING);
            tgl_cpp.add(new Chunk("     : " + me.getCalonPembayarPremiModel().getTgl_pendirian_cp() + "\n", boldFont));
            document.add(tgl_cpp);

            Phrase tmpt_cpp = new Phrase(context.getString(R.string.tempat_kedudukan), normalFont);
            tmpt_cpp.setTabSettings(new TabSettings(250f));
            tmpt_cpp.add(Chunk.TABBING);
            tmpt_cpp.add(new Chunk("     : " + me.getCalonPembayarPremiModel().getTmpt_kedudukan_cp() + "\n", boldFont));
            document.add(tmpt_cpp);

            Phrase kota_cpp = new Phrase(context.getString(R.string.kota), normalFont);
            kota_cpp.setTabSettings(new TabSettings(250f));
            kota_cpp.add(Chunk.TABBING);
            kota_cpp.add(new Chunk("     : " + me.getCalonPembayarPremiModel().getKota_perush_cp() + "\n", boldFont));
            document.add(kota_cpp);

            ListDropDownSPAJ = new E_Select(context).getLst_Propinsi();
            Phrase provinsi_cpp = new Phrase(context.getString(R.string.provinsi), normalFont);
            provinsi_cpp.setTabSettings(new TabSettings(250f));
            provinsi_cpp.add(Chunk.TABBING);
            provinsi_cpp.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getCalonPembayarPremiModel().getInt_propinsi_cp()) + "\n", boldFont));
            document.add(provinsi_cpp);

            Phrase kdpos_cpp = new Phrase(context.getString(R.string.kode_pos), normalFont);
            kdpos_cpp.setTabSettings(new TabSettings(250f));
            kdpos_cpp.add(Chunk.TABBING);
            kdpos_cpp.add(new Chunk("     : " + me.getCalonPembayarPremiModel().getKdpos_perush_cp() + "\n", boldFont));
            document.add(kdpos_cpp);

            Phrase noprshn_cpp = new Phrase(context.getString(R.string.nomor_telp_perusahaan), normalFont);
            noprshn_cpp.setTabSettings(new TabSettings(250f));
            noprshn_cpp.add(Chunk.TABBING);
            noprshn_cpp.add(new Chunk("     : " + me.getCalonPembayarPremiModel().getTelp_perush_cp() + "\n", boldFont));
            document.add(noprshn_cpp);

            Phrase nofax_cpp = new Phrase(context.getString(R.string.nomor_fax), normalFont);
            nofax_cpp.setTabSettings(new TabSettings(250f));
            nofax_cpp.add(Chunk.TABBING);
            nofax_cpp.add(new Chunk("     : " + me.getCalonPembayarPremiModel().getNofax_perush_cp() + "\n", boldFont));
            document.add(nofax_cpp);

            Phrase bidusaha_cpp = new Phrase(context.getString(R.string.bidang_usaha), normalFont);
            bidusaha_cpp.setTabSettings(new TabSettings(250f));
            bidusaha_cpp.add(Chunk.TABBING);
            bidusaha_cpp.add(new Chunk("     : " + me.getCalonPembayarPremiModel().getBid_usaha_cp() + "\n", boldFont));
            document.add(bidusaha_cpp);

            //bernard
            if (!me.getCalonPembayarPremiModel().getStr_gaji_cp().equals("")) {
                Phrase smbrdn_gaji = new Phrase(context.getString(R.string.sumber_pendapatan_rutin_bulan), normalFont);
                smbrdn_gaji.add(new Chunk("     : Gaji" + "\n", boldFont));
                document.add(smbrdn_gaji);
            }
            if (!me.getCalonPembayarPremiModel().getStr_penghsl_cp().equals("")) {
                Phrase smbrdn_pnghsln = new Phrase(context.getString(R.string.sumber_pendapatan_rutin_bulan), normalFont);
                smbrdn_pnghsln.add(new Chunk("     : Penghasilan Suami/Istri" + "\n", boldFont));
                document.add(smbrdn_pnghsln);
            }
            if (!me.getCalonPembayarPremiModel().getStr_ortu_cp().equals("")) {
                Phrase smbrdn_orangtua = new Phrase(context.getString(R.string.sumber_pendapatan_rutin_bulan), normalFont);
                smbrdn_orangtua.add(new Chunk("     : Orang Tua" + "\n", boldFont));
                document.add(smbrdn_orangtua);
            }
            if (!me.getCalonPembayarPremiModel().getStr_laba_cp().equals("")) {
                Phrase smbrdn_laba = new Phrase(context.getString(R.string.sumber_pendapatan_rutin_bulan), normalFont);
                smbrdn_laba.add(new Chunk("     : Laba Perusahaan" + "\n", boldFont));
                document.add(smbrdn_laba);
            }
            if (!me.getCalonPembayarPremiModel().getStr_hslusaha_cp().equals("")) {
                Phrase smbrdn_usaha = new Phrase(context.getString(R.string.sumber_pendapatan_rutin_bulan), normalFont);
                smbrdn_usaha.add(new Chunk("     : Hasil Usaha ~ " + me.getCalonPembayarPremiModel().getEdit_hslusaha_cp() + "\n", boldFont));
                document.add(smbrdn_usaha);
            }
            if (!me.getCalonPembayarPremiModel().getStr_hslinves_cp().equals("")) {
                Phrase smbrdn_investasi = new Phrase(context.getString(R.string.sumber_pendapatan_rutin_bulan), normalFont);
                smbrdn_investasi.add(new Chunk("     : Hasil Investasi ~ " + me.getCalonPembayarPremiModel().getEdit_hslinves_cp() + "\n", boldFont));
                document.add(smbrdn_investasi);
            }
            if (!me.getCalonPembayarPremiModel().getEdit_lainnya_cp().equals("")) {
                Phrase smbrdn_lainnya = new Phrase(context.getString(R.string.sumber_pendapatan_rutin_bulan), normalFont);
                smbrdn_lainnya.add(new Chunk("     : Lainnya ~ " + me.getCalonPembayarPremiModel().getEdit_lainnya_cp() + "\n", boldFont));
                document.add(smbrdn_lainnya);
            }

            if (!me.getCalonPembayarPremiModel().getStr_bonus_cp().equals("")) {
                Phrase smbrdn_bonus = new Phrase(context.getString(R.string.sumber_pendapatan_nonrutin_tahun), normalFont);
                smbrdn_bonus.add(new Chunk("     : Bonus" + "\n", boldFont));
                document.add(smbrdn_bonus);
            }
            if (!me.getCalonPembayarPremiModel().getStr_komisi_cp().equals("")) {
                Phrase smbrdn_komisi = new Phrase(context.getString(R.string.sumber_pendapatan_nonrutin_tahun), normalFont);
                smbrdn_komisi.add(new Chunk("     : Komisi" + "\n", boldFont));
                document.add(smbrdn_komisi);
            }
            if (!me.getCalonPembayarPremiModel().getStr_aset_cp().equals("")) {
                Phrase smbrdn_aset = new Phrase(context.getString(R.string.sumber_pendapatan_nonrutin_tahun), normalFont);
                smbrdn_aset.add(new Chunk("     : Penjualan Aset" + "\n", boldFont));
                document.add(smbrdn_aset);
            }
            if (!me.getCalonPembayarPremiModel().getStr_hadiah_cp().equals("")) {
                Phrase smbrdn_warisan = new Phrase(context.getString(R.string.sumber_pendapatan_nonrutin_tahun), normalFont);
                smbrdn_warisan.add(new Chunk("     : Hadiah/Warisan" + "\n", boldFont));
                document.add(smbrdn_warisan);
            }
            if (!me.getCalonPembayarPremiModel().getStr_hslinves_thn_cp().equals("")) {
                Phrase smbrdn_invest = new Phrase(context.getString(R.string.sumber_pendapatan_nonrutin_tahun), normalFont);
                smbrdn_invest.add(new Chunk("     : Hasil Investasi ~ " + me.getCalonPembayarPremiModel().getEdit_hslinves_thn_cp() + "\n", boldFont));
                document.add(smbrdn_invest);
            }
            if (!me.getCalonPembayarPremiModel().getEdit_lainnya_thn_cp().equals("")) {
                Phrase smbrdn_lain = new Phrase(context.getString(R.string.sumber_pendapatan_nonrutin_tahun), normalFont);
                smbrdn_lain.add(new Chunk("     : Lainnya ~ " + me.getCalonPembayarPremiModel().getEdit_lainnya_thn_cp() + "\n", boldFont));
                document.add(smbrdn_lain);
            }

            Phrase ttl_bulan = new Phrase(context.getString(R.string.jumlah_total_pendapatan_rutin_bulan) + "     : ", normalFont);
            ttl_bulan.add(new Chunk(me.getCalonPembayarPremiModel().getStr_spintotal_bln_cp() + "\n", boldFont));
            document.add(ttl_bulan);//dropdown

            Phrase ttl_tahun = new Phrase(context.getString(R.string.jumlah_total_pendapatan_nonrutin_tahun) + "     : ", normalFont);
            ttl_tahun.add(new Chunk(me.getCalonPembayarPremiModel().getStr_spintotal_thn_cp() + "\n", boldFont));
            document.add(ttl_tahun);//dropdown

            if (!me.getCalonPembayarPremiModel().getStr_proteksi_cp().equals("")) {
                Phrase tujuan_p = new Phrase(context.getString(R.string.tujuan_pengajuan_asuransi_cp), normalFont);
                tujuan_p.add(new Chunk("     : Proteksi" + "\n", boldFont));
                document.add(tujuan_p); // masih belum check tujuan asuransi
            }
            if (!me.getCalonPembayarPremiModel().getStr_pend_cp().equals("")) {
                Phrase tujuan_p = new Phrase(context.getString(R.string.tujuan_pengajuan_asuransi_cp), normalFont);
                tujuan_p.add(new Chunk("     : Pendidikan" + "\n", boldFont));
                document.add(tujuan_p); // masih belum check tujuan asuransi
            }
            if (!me.getCalonPembayarPremiModel().getStr_inves_cp().equals("")) {
                Phrase tujuan_i = new Phrase(context.getString(R.string.tujuan_pengajuan_asuransi_cp), normalFont);
                tujuan_i.add(new Chunk("     : Investasi" + "\n", boldFont));
                document.add(tujuan_i); // masih belum check tujuan asuransi
            }
            if (!me.getCalonPembayarPremiModel().getStr_tabungan_cp().equals("")) {
                Phrase tujuan_t = new Phrase(context.getString(R.string.tujuan_pengajuan_asuransi_cp), normalFont);
                tujuan_t.add(new Chunk("     : Tabungan" + "\n", boldFont));
                document.add(tujuan_t); // masih belum check tujuan asuransi
            }
            if (!me.getCalonPembayarPremiModel().getStr_pensiun_cp().equals("")) {
                Phrase tujuan_dp = new Phrase(context.getString(R.string.tujuan_pengajuan_asuransi_cp), normalFont);
                tujuan_dp.add(new Chunk("     : Dana Pensiun" + "\n", boldFont));
                document.add(tujuan_dp); // masih belum check tujuan asuransi
            }
            if (!me.getCalonPembayarPremiModel().getEdit_lainnya_tujuan_cp().equals("")) {
                Phrase tujuan_l = new Phrase(context.getString(R.string.tujuan_pengajuan_asuransi_cp), normalFont);
                tujuan_l.add(new Chunk("     : Lainnya ~ " + me.getCalonPembayarPremiModel().getEdit_lainnya_tujuan_cp() + "\n", boldFont));
                document.add(tujuan_l); // masih belum check tujuan asuransi
            }

            Phrase pkrjn_lain = new Phrase(context.getString(R.string.pekerjaanlain_diluar_pekerjaanutama) + "     : ", normalFont);
            pkrjn_lain.add(new Chunk(MethodSupport.getAdapterPositionArray(mContext.getResources().getStringArray(R.array.yatidak), me.getCalonPembayarPremiModel().getInt_spinbisnis_cp()) + "\n", boldFont));
            document.add(pkrjn_lain);//dropdown

            Phrase pkrjn_lain_ket = new Phrase(context.getString(R.string.keterangan) + "     : ", normalFont);
            pkrjn_lain_ket.add(new Chunk(me.getCalonPembayarPremiModel().getEditbisnis_cp() + "\n", boldFont));
            document.add(pkrjn_lain_ket);//dropdown

            //bernard2
//            Phrase Editpihak3_cp = new Phrase("" + "    : ", normalFont);
//            Editpihak3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getEditpihak3_cp() + "\n", boldFont));
//            document.add(Editpihak3_cp);

            int int_ket_cp = me.getCalonPembayarPremiModel().getInt_ket_cp();
            if(int_ket_cp == 40){
                //null gak ada paket
            } else {
                Phrase Edit_nmpihak3_cp = new Phrase("Nama Pihak ke Tiga" + "    : ", normalFont);
                Edit_nmpihak3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getEdit_nmpihak3_cp() + "\n", boldFont));
                document.add(Edit_nmpihak3_cp);

                Phrase Almt_phk3_cp = new Phrase("Alamat (Kecamatan/Kota/Kode Pos)" + "    : ", normalFont);
                Almt_phk3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getAlmt_phk3_cp() + "\n", boldFont));
                document.add(Almt_phk3_cp);

                Phrase Notlp_phk3_cp = new Phrase("No. telp. Rumah" + "    : ", normalFont);
                Notlp_phk3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getNotlp_phk3_cp() + "\n", boldFont));
                document.add(Notlp_phk3_cp);

                Phrase Notlp_kntrphk3_cp = new Phrase("No. telp. Kantor" + "    : ", normalFont);
                Notlp_kntrphk3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getNotlp_kntrphk3_cp() + "\n", boldFont));
                document.add(Notlp_kntrphk3_cp);

                Phrase Edit_emailphk3_cp = new Phrase("Alamat EMAIL" + "    : ", normalFont);
                Edit_emailphk3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getEdit_emailphk3_cp() + "\n", boldFont));
                document.add(Edit_emailphk3_cp);

//                Phrase Tmpt_kedudukan_cp = new Phrase("" + "    : ", normalFont);
//                Tmpt_kedudukan_cp.add(new Chunk(me.getCalonPembayarPremiModel().getTmpt_kedudukan_cp() + "\n", boldFont));
//                document.add(Tmpt_kedudukan_cp);

                Phrase Bidusaha_phk3_cp = new Phrase("Bidang Usaha" + "    : ", normalFont);
                Bidusaha_phk3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getBidusaha_phk3_cp() + "\n", boldFont));
                document.add(Bidusaha_phk3_cp);

                Phrase Str_spin_jns_pkrjaan_cp = new Phrase("Bidang / Jenis Pekerjaan" + "    : ", normalFont);
                Str_spin_jns_pkrjaan_cp.add(new Chunk(me.getCalonPembayarPremiModel().getStr_spin_jns_pkrjaan_cp() + "\n", boldFont));
                document.add(Str_spin_jns_pkrjaan_cp);

                Phrase Lain_jns_pkrjaan_cp = new Phrase("Keterangan " + "    : ", normalFont);
                Lain_jns_pkrjaan_cp.add(new Chunk(me.getCalonPembayarPremiModel().getLain_jns_pkrjaan_cp() + "\n", boldFont));
                document.add(Lain_jns_pkrjaan_cp);

                Phrase Instansi_phk3_cp = new Phrase("Instansi / Departemen" + "    : ", normalFont);
                Instansi_phk3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getInstansi_phk3_cp() + "\n", boldFont));
                document.add(Instansi_phk3_cp);

                Phrase Npwp_phk3_cp = new Phrase("NPWP" + "    : ", normalFont);
                Npwp_phk3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getNpwp_phk3_cp() + "\n", boldFont));
                document.add(Npwp_phk3_cp);

                Phrase Sumber_dana_phk3_cp = new Phrase("Sumber Dana" + "    : ", normalFont);
                Sumber_dana_phk3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getSumber_dana_phk3_cp() + "\n", boldFont));
                document.add(Sumber_dana_phk3_cp);

                Phrase Tujuan_phk3_cp = new Phrase("Tujuan Penggunaan Dana" + "    : ", normalFont);
                Tujuan_phk3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getTujuan_phk3_cp() + "\n", boldFont));
                document.add(Tujuan_phk3_cp);

                Phrase Alasan_phk3_cp = new Phrase("Apabila tidak dapat memberikan informasi atas Pihak Ketiga, mohon diberikan alasannya" + "    : ", normalFont);
                Alasan_phk3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getAlasan_phk3_cp() + "\n", boldFont));
                document.add(Alasan_phk3_cp);

//                Phrase Tgl_pendirian_cp = new Phrase("" + "    : ", normalFont);
//                Tgl_pendirian_cp.add(new Chunk(me.getCalonPembayarPremiModel().getTgl_pendirian_cp() + "\n", boldFont));
//                document.add(Tgl_pendirian_cp);

                Phrase Tgl_lhrphk3_cp = new Phrase("Tanggal Lahir" + "    : ", normalFont);
                Tgl_lhrphk3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getTgl_lhrphk3_cp() + "\n", boldFont));
                document.add(Tgl_lhrphk3_cp);

                Phrase Tmpt_lhrphk3_cp = new Phrase("Tempat Lahir" + "    : ", normalFont);
                Tmpt_lhrphk3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getTmpt_lhrphk3_cp() + "\n", boldFont));
                document.add(Tmpt_lhrphk3_cp);

//                Phrase Int_spinpihak3_cp = new Phrase("" + "    : ", normalFont);
//                Int_spinpihak3_cp.add(new Chunk(me.getCalonPembayarPremiModel().getInt_spinpihak3_cp() + "\n", boldFont));
//                document.add(Int_spinpihak3_cp);

                ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_warganegara, 0);
                Phrase Int_spin_kewrgn_cp = new Phrase("Kewarganegaraan" + "    : ", normalFont);
                Int_spin_kewrgn_cp.add(new Chunk(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getCalonPembayarPremiModel().getInt_spin_kewrgn_cp()) + "\n", boldFont));
                document.add(Int_spin_kewrgn_cp);

                ListDropDownSPAJ = new E_Select(context).getLst_Pekerjaan("");
                Phrase Spin_pekerjaan_cp = new Phrase("Deskripsi Pekerjaan" + "    : ", normalFont);
                Spin_pekerjaan_cp.add(new Chunk(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getCalonPembayarPremiModel().getSpin_pekerjaan_cp()) + "\n", boldFont));
                document.add(Spin_pekerjaan_cp);
            }

            document.newPage();
            document.add(new Paragraph(context.getString(R.string.usulan_asuransi) + "\n\n", normalFont));

            ListDropDownSPAJ = new E_Select(context).getLstChannelDist(JENIS_LOGIN, JENIS_LOGIN_BC, KODE_REG1, KODE_REG2);
            Phrase ch_ua = new Phrase(context.getString(R.string.channel_distribusi), normalFont);
            ch_ua.setTabSettings(new TabSettings(250f));
            ch_ua.add(Chunk.TABBING);
            ch_ua.add(new Chunk( "     : " + ListDropDownSPAJ.get(0).getValue() + "\n", boldFont));
            document.add(ch_ua);

            ListDropInt = new P_Select(context).selectListRencana(groupId);
            Phrase prdk_ua = new Phrase(context.getString(R.string.produk_yang_dipilih), normalFont);
            prdk_ua.setTabSettings(new TabSettings(250f));
            prdk_ua.add(Chunk.TABBING);
            prdk_ua.add(new Chunk("     : " + MethodSupport.getAdapterPosition(ListDropInt, me.getUsulanAsuransiModel().getKd_produk_ua()) + "\n", boldFont));
            document.add(prdk_ua);

            ListDropInt = new P_Select(context).selectListRencanaSub(groupId, me.getUsulanAsuransiModel().getKd_produk_ua());
            Phrase subprdk_ua = new Phrase(context.getString(R.string.sub_produk_yang_dipilih), normalFont);
            subprdk_ua.setTabSettings(new TabSettings(250f));
            subprdk_ua.add(Chunk.TABBING);
            subprdk_ua.add(new Chunk( "     : " + MethodSupport.getSubAdapterPosition(ListDropInt, me.getUsulanAsuransiModel().getSub_produk_ua()) + "\n", boldFont));
            document.add(subprdk_ua);

            try {
                if (MethodSupport.getSubAdapterPacket(ListDropInt, me.getUsulanAsuransiModel().getPaket_ua()).toString() != "null") {
                    ListDropInt = new P_Select(context).selectListPacket(groupId, me.getUsulanAsuransiModel().getKd_produk_ua());
                    document.add(new Paragraph(context.getString(R.string.paket) + "     : " + MethodSupport.getSubAdapterPacket(ListDropInt, me.getUsulanAsuransiModel().getPaket_ua())));
                } else {
                    // null bearti gak ada paket
                }
            } catch (Exception e) {

            }


            Phrase masa_prtnggn = new Phrase(context.getString(R.string.masa_pertanggungan), normalFont);
            masa_prtnggn.setTabSettings(new TabSettings(250f));
            masa_prtnggn.add(Chunk.TABBING);
            masa_prtnggn.add(new Chunk( "     : " + me.getUsulanAsuransiModel().getMasa_pertanggungan_ua() + "\n", boldFont));
            document.add(masa_prtnggn);

            Phrase mpp_ua = new Phrase(context.getString(R.string.masa_pembayaran_premi_ua), normalFont);
            mpp_ua.setTabSettings(new TabSettings(250f));
            mpp_ua.add(Chunk.TABBING);
            mpp_ua.add(new Chunk("     : " + me.getUsulanAsuransiModel().getMasa_pembayaran_ua() + "\n", boldFont));
            document.add(mpp_ua);

            Phrase cuti_premi_ua = new Phrase(context.getString(R.string.cuti_premi), normalFont);
            cuti_premi_ua.setTabSettings(new TabSettings(250f));
            cuti_premi_ua.add(Chunk.TABBING);
            cuti_premi_ua.add(new Chunk("     : " + me.getUsulanAsuransiModel().getCuti_premi_ua() + "\n", boldFont));
            document.add(cuti_premi_ua);


            ListDropDownString = new E_Select(context).getLst_Kurs(me.getUsulanAsuransiModel().getKd_produk_ua(), me.getUsulanAsuransiModel().getSub_produk_ua());
            Phrase kursup_ua = new Phrase(context.getString(R.string.kurs), normalFont);
            kursup_ua.setTabSettings(new TabSettings(250f));
            kursup_ua.add(Chunk.TABBING);
            kursup_ua.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, me.getUsulanAsuransiModel().getKurs_up_ua()) + "\n", boldFont));
            document.add(kursup_ua);

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_bentukpremi, 0);
            Phrase carabayar_ua = new Phrase(context.getString(R.string.cara_pembayaran_premi), normalFont);
            carabayar_ua.setTabSettings(new TabSettings(250f));
            carabayar_ua.add(Chunk.TABBING);
            carabayar_ua.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getUsulanAsuransiModel().getBentukbayar_ua()) + "\n", boldFont));
            document.add(carabayar_ua);

            if (me.getUsulanAsuransiModel().getUnitlink_opsipremi_ua() == 0) {
                Phrase opsi = new Phrase(context.getString(R.string.opsi_isi_premi), normalFont);
                opsi.setTabSettings(new TabSettings(250f));
                opsi.add(Chunk.TABBING);
                opsi.add(new Chunk("     : " + "ISI TOTAL PREMI DAN PILIH KOMBINASI" + "\n", boldFont));
                document.add(opsi);
            } else {
                Phrase opsi = new Phrase(context.getString(R.string.opsi_isi_premi), normalFont);
                opsi.setTabSettings(new TabSettings(250f));
                opsi.add(Chunk.TABBING);
                opsi.add(new Chunk("     : " + "ISI PREMI" + "\n", boldFont));
                document.add(opsi);
            }

            ListDropDownString = new E_Select(context).getLst_Kurs(me.getUsulanAsuransiModel().getKd_produk_ua(), me.getUsulanAsuransiModel().getSub_produk_ua());
            Phrase kurs_unitlink = new Phrase(context.getString(R.string.kurs), normalFont);
            kurs_unitlink.setTabSettings(new TabSettings(250f));
            kurs_unitlink.add(Chunk.TABBING);
            kurs_unitlink.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, me.getUsulanAsuransiModel().getUnitlink_kurs_ua()) + "\n", boldFont));
            document.add(kurs_unitlink);

            Phrase premistndrpokok = new Phrase(context.getString(R.string.premi_standart_pokok), normalFont);
            premistndrpokok.setTabSettings(new TabSettings(250f));
            premistndrpokok.add(Chunk.TABBING);
            premistndrpokok.add(new Chunk("     : " + NumberFormat.getNumberInstance(Locale.GERMAN).format(me.getUsulanAsuransiModel().getUnitlink_premistandard_ua()) + "\n", boldFont));
            document.add(premistndrpokok);

            ListDropDownString = new E_Select(context).getLst_KombinasiNew(me.getUsulanAsuransiModel().getKd_produk_ua(), me.getUsulanAsuransiModel().getSub_produk_ua());
            Phrase kombinasi = new Phrase(context.getString(R.string.kombinasi), normalFont);
            kombinasi.setTabSettings(new TabSettings(250f));
            kombinasi.add(Chunk.TABBING);
            kombinasi.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, me.getUsulanAsuransiModel().getUnitlink_kombinasi_ua()) + "\n", boldFont));
            document.add(kombinasi);

            ListDropDownString = new E_Select(context).getLst_Kurs(me.getUsulanAsuransiModel().getKd_produk_ua(), me.getUsulanAsuransiModel().getSub_produk_ua());
            Phrase kursttl_ua = new Phrase(context.getString(R.string.kurs), normalFont);
            kursttl_ua.setTabSettings(new TabSettings(250f));
            kursttl_ua.add(Chunk.TABBING);
            kursttl_ua.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, me.getUsulanAsuransiModel().getUnitlink_kurs_total_ua()) + "\n", boldFont));
            document.add(kursttl_ua);

            Phrase total_premi = new Phrase(context.getString(R.string.total_premi), normalFont);
            total_premi.setTabSettings(new TabSettings(250f));
            total_premi.add(Chunk.TABBING);
            total_premi.add(new Chunk("     : " + NumberFormat.getNumberInstance(Locale.GERMAN).format(me.getUsulanAsuransiModel().getUnitlink_total_ua()) + "\n", boldFont));
            document.add(total_premi);

            Phrase up_ua = new Phrase(context.getString(R.string.uang_pertanggungan_ua), normalFont);
            up_ua.setTabSettings(new TabSettings(250f));
            up_ua.add(Chunk.TABBING);
            up_ua.add(new Chunk("     : " + NumberFormat.getNumberInstance(Locale.GERMAN).format(me.getUsulanAsuransiModel().getUp_ua()) + "\n", boldFont));
            document.add(up_ua);

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_bentukpremi, 0);
            Phrase bentukbayar_ua = new Phrase(context.getString(R.string.bentuk_pembayaran_premi), normalFont);
            bentukbayar_ua.setTabSettings(new TabSettings(250f));
            bentukbayar_ua.add(Chunk.TABBING);
            bentukbayar_ua.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getUsulanAsuransiModel().getBentukbayar_ua()) + "\n", boldFont));
            document.add(bentukbayar_ua);

            Phrase mulai_berlaku = new Phrase(context.getString(R.string.mulai_berlaku), normalFont);
            mulai_berlaku.setTabSettings(new TabSettings(250f));
            mulai_berlaku.add(Chunk.TABBING);
            mulai_berlaku.add(new Chunk("     : " + me.getUsulanAsuransiModel().getAwalpertanggungan_ua() + "\n", boldFont));
            document.add(mulai_berlaku);

            Phrase akhir_berlaku = new Phrase(context.getString(R.string.akhir_berlaku), normalFont);
            akhir_berlaku.setTabSettings(new TabSettings(250f));
            akhir_berlaku.add(Chunk.TABBING);
            akhir_berlaku.add(new Chunk("     : " + GetTime.getEndDate(me.getUsulanAsuransiModel().getAwalpertanggungan_ua(), me.getUsulanAsuransiModel().getMasa_pertanggungan_ua()) + "\n", boldFont));
            document.add(akhir_berlaku);

            List<ModelAddRiderUA> ListRiderUA = me.getListRiderUA();
            for (int i = 0; i < ListRiderUA.size(); i++) {

                if (ListRiderUA.get(i).getKode_produk_rider() == 810) {
                    Phrase kdprdk_riderPA = new Phrase("kode produk rider", normalFont);
                    kdprdk_riderPA.setTabSettings(new TabSettings(250f));
                    kdprdk_riderPA.add(Chunk.TABBING);
                    kdprdk_riderPA.add(new Chunk("     : Personal Accident" + "\n", boldFont));
                    document.add(kdprdk_riderPA);
                }else {
                    Phrase kdprdk_rider = new Phrase("kode produk rider", normalFont);
                    kdprdk_rider.setTabSettings(new TabSettings(250f));
                    kdprdk_rider.add(Chunk.TABBING);
                    kdprdk_rider.add(new Chunk("     : " + new E_Select(context).getNamaProduk(ListRiderUA.get(i).getKode_produk_rider()) + "\n", boldFont));
                    document.add(kdprdk_rider);
                }

                Phrase subkdprdk_rider = new Phrase("kode subproduk rider", normalFont);
                subkdprdk_rider.setTabSettings(new TabSettings(250f));
                subkdprdk_rider.add(Chunk.TABBING);
                subkdprdk_rider.add(new Chunk("     : " + new E_Select(context).getNamaSubProduk(ListRiderUA.get(i).getKode_produk_rider(), ListRiderUA.get(i).getKode_subproduk_rider()) + "\n", boldFont));
                document.add(subkdprdk_rider);

                if (ListRiderUA.get(i).getTertanggung_rider() > 0) {
                    Phrase tt_rider = new Phrase("tertanggung rider", normalFont);
                    tt_rider.setTabSettings(new TabSettings(250f));
                    tt_rider.add(Chunk.TABBING);
                    tt_rider.add(new Chunk("     : " + ListRiderUA.get(i).getTertanggung_rider() + "\n", boldFont));
                    document.add(tt_rider);
                }

                if (ListRiderUA.get(i).getRate_rider() > 0) {
                    Phrase rate_rider = new Phrase("rate rider", normalFont);
                    rate_rider.setTabSettings(new TabSettings(250f));
                    rate_rider.add(Chunk.TABBING);
                    rate_rider.add(new Chunk("     : " + ListRiderUA.get(i).getRate_rider() + "\n", boldFont));
                    document.add(rate_rider);
                }

                if (ListRiderUA.get(i).getUnit_rider() > 0) {
                    Phrase unit_rider = new Phrase("unit rider", normalFont);
                    unit_rider.setTabSettings(new TabSettings(250f));
                    unit_rider.add(Chunk.TABBING);
                    unit_rider.add(new Chunk("     : " + ListRiderUA.get(i).getUnit_rider() + "\n", boldFont));
                    document.add(unit_rider);
                }

                if (ListRiderUA.get(i).getKlas_rider() > 0) {
                    Phrase klas_rider = new Phrase("klas rider", normalFont);
                    klas_rider.setTabSettings(new TabSettings(250f));
                    klas_rider.add(Chunk.TABBING);
                    klas_rider.add(new Chunk("     : " + ListRiderUA.get(i).getKlas_rider() + "\n", boldFont));
                    document.add(klas_rider);
                }

                if (ListRiderUA.get(i).getPersentase_rider() > 0) {
                    Phrase persentase_rider = new Phrase("persentase rider", normalFont);
                    persentase_rider.setTabSettings(new TabSettings(250f));
                    persentase_rider.add(Chunk.TABBING);
                    persentase_rider.add(new Chunk("     : " + ListRiderUA.get(i).getPersentase_rider() + "\n", boldFont));
                    document.add(persentase_rider);
                }

                if (ListRiderUA.get(i).getUp_rider() > 0) {
                    Phrase up_rider = new Phrase("up rider", normalFont);
                    up_rider.setTabSettings(new TabSettings(250f));
                    up_rider.add(Chunk.TABBING);
                    up_rider.add(new Chunk("     : " + ListRiderUA.get(i).getUp_rider() + "\n", boldFont));
                    document.add(up_rider);
                }

                if (ListRiderUA.get(i).getPremi_rider() > 0) {
                    Phrase premi_rider = new Phrase("premi rider", normalFont);
                    premi_rider.setTabSettings(new TabSettings(250f));
                    premi_rider.add(Chunk.TABBING);
                    premi_rider.add(new Chunk("     : " + ListRiderUA.get(i).getPremi_rider() + "\n", boldFont));
                    document.add(premi_rider);
                }

//                Phrase tglmulai_rider = new Phrase("tanggal mulai rider", normalFont);
//                tglmulai_rider.setTabSettings(new TabSettings(250f));
//                tglmulai_rider.add(Chunk.TABBING);
//                tglmulai_rider.add(new Chunk("     : " + ListRiderUA.get(i).getTglmulai_rider() + "\n", boldFont));
//                document.add(tglmulai_rider);

//                Phrase tglakhir_rider = new Phrase("tanggal akhir rider", normalFont);
//                tglakhir_rider.setTabSettings(new TabSettings(250f));
//                tglakhir_rider.add(Chunk.TABBING);
//                tglakhir_rider.add(new Chunk("     : " + ListRiderUA.get(i).getTglakhir_rider() + "\n", boldFont));
//                document.add(tglakhir_rider);

                if (ListRiderUA.get(i).getMasa_rider() > 0) {
                    Phrase masa_rider = new Phrase("masa rider", normalFont);
                    masa_rider.setTabSettings(new TabSettings(250f));
                    masa_rider.add(Chunk.TABBING);
                    masa_rider.add(new Chunk("     : " + ListRiderUA.get(i).getMasa_rider() + "\n", boldFont));
                    document.add(masa_rider);
                }

//                Phrase akhirbyr_rider = new Phrase("akhir bayar rider", normalFont);
//                akhirbyr_rider.setTabSettings(new TabSettings(250f));
//                akhirbyr_rider.add(Chunk.TABBING);
//                akhirbyr_rider.add(new Chunk("     : " + ListRiderUA.get(i).getAkhirbayar_rider() + "\n\n", boldFont));
//                document.add(akhirbyr_rider);
            }

//            List<ModelAskesUA> ListAskesUA = me.getListASkesUA();
//            for (int i = 0; i < ListAskesUA.size(); i++) {
//                jsonObject.put("peserta_askes", ListAskesUA.get(i).getPeserta_askes());
//                jsonObject.put("jekel_askes", ListAskesUA.get(i).getJekel_askes());
//                jsonObject.put("ttl_askes", ListAskesUA.get(i).getTtl_askes());
//                jsonObject.put("usia_askes", ListAskesUA.get(i).getUsia_askes());
//                jsonObject.put("hubungan_askes", ListAskesUA.get(i).getHubungan_askes());
//                jsonObject.put("produk_askes", ListAskesUA.get(i).getProduk_askes());
//                jsonObject.put("rider_askes", ListAskesUA.get(i).getRider_askes());
//                jsonObject.put("kode_produk_rider", ListAskesUA.get(i).getKode_produk_rider());
//                jsonObject.put("kode_subproduk_rider", ListAskesUA.get(i).getKode_subproduk_rider());
//                jsonObject.put("tinggi_askes", ListAskesUA.get(i).getTinggi_askes());
//                jsonObject.put("berat_askes", ListAskesUA.get(i).getBerat_askes());
//                jsonObject.put("warganegara_askes", ListAskesUA.get(i).getWarganegara_askes());
//                jsonObject.put("pekerjaan_askes", ListAskesUA.get(i).getPekerjaan_askes());
//            }

            List<ModelAskesUA> ListAskesUA = me.getListASkesUA();
            for (int i = 0; i < ListAskesUA.size(); i++) {
                Phrase peserta_askes = new Phrase("Peserta Asuransi Kesehatan", normalFont);
                peserta_askes.setTabSettings(new TabSettings(250f));
                peserta_askes.add(Chunk.TABBING);
                peserta_askes.add(new Chunk("     : " + ListAskesUA.get(i).getPeserta_askes() + "\n", boldFont));
                document.add(peserta_askes);

                if (ListAskesUA.get(i).getJekel_askes() == 0) {
                    Phrase jekel_askes_w = new Phrase("Jenis Kelamin", normalFont);
                    jekel_askes_w.setTabSettings(new TabSettings(250f));
                    jekel_askes_w.add(Chunk.TABBING);
                    jekel_askes_w.add(new Chunk("     : Wanita" + "\n", boldFont));
                    document.add(jekel_askes_w);
                } else {
                    Phrase jekel_askes_p = new Phrase("Jenis Kelamin", normalFont);
                    jekel_askes_p.setTabSettings(new TabSettings(250f));
                    jekel_askes_p.add(Chunk.TABBING);
                    jekel_askes_p.add(new Chunk("     : Pria" + "\n", boldFont));
                    document.add(jekel_askes_p);
                }

                Phrase ttl_askes = new Phrase("Tanggal Lahir", normalFont);
                ttl_askes.setTabSettings(new TabSettings(250f));
                ttl_askes.add(Chunk.TABBING);
                ttl_askes.add(new Chunk("     : " + ListAskesUA.get(i).getTtl_askes() + "\n", boldFont));
                document.add(ttl_askes);

                Phrase usia_askes = new Phrase("Usia", normalFont);
                usia_askes.setTabSettings(new TabSettings(250f));
                usia_askes.add(Chunk.TABBING);
                usia_askes.add(new Chunk("     : " + ListAskesUA.get(i).getUsia_askes() + "\n", boldFont));
                document.add(usia_askes);

                ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_relasi, 0);
                Phrase hubungan_askes = new Phrase("Hubungan dengan Pemegang Polis", normalFont);
                hubungan_askes.setTabSettings(new TabSettings(250f));
                hubungan_askes.add(Chunk.TABBING);
                hubungan_askes.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, ListAskesUA.get(i).getHubungan_askes()) + "\n", boldFont));
                document.add(hubungan_askes);

                Phrase kode_produk_rider = new Phrase("Jenis Produk", normalFont);
                kode_produk_rider.setTabSettings(new TabSettings(250f));
                kode_produk_rider.add(Chunk.TABBING);
                kode_produk_rider.add(new Chunk("     : " + new E_Select(context).getNamaProduk(ListAskesUA.get(i).getKode_produk_rider()) + "\n", boldFont));
                document.add(kode_produk_rider);

                Phrase kode_subproduk_rider = new Phrase("Jenis Rider", normalFont);
                kode_subproduk_rider.setTabSettings(new TabSettings(250f));
                kode_subproduk_rider.add(Chunk.TABBING);
                kode_subproduk_rider.add(new Chunk("     : " + new E_Select(context).getNamaSubProduk(ListAskesUA.get(i).getKode_produk_rider(), ListAskesUA.get(i).getKode_subproduk_rider()) + "\n", boldFont));
                document.add(kode_subproduk_rider);

                Phrase tinggi_askes = new Phrase("Tinggi Badan", normalFont);
                tinggi_askes.setTabSettings(new TabSettings(250f));
                tinggi_askes.add(Chunk.TABBING);
                tinggi_askes.add(new Chunk("     : " + ListAskesUA.get(i).getTinggi_askes() + "\n", boldFont));
                document.add(tinggi_askes);

                Phrase berat_askes = new Phrase("Berat Badan", normalFont);
                berat_askes.setTabSettings(new TabSettings(250f));
                berat_askes.add(Chunk.TABBING);
                berat_askes.add(new Chunk("     : " + ListAskesUA.get(i).getBerat_askes() + "\n", boldFont));
                document.add(berat_askes);

                ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_warganegara, 0);
                Phrase warganegara_askes = new Phrase("Warga Negara", normalFont);
                warganegara_askes.setTabSettings(new TabSettings(250f));
                warganegara_askes.add(Chunk.TABBING);
                warganegara_askes.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, ListAskesUA.get(i).getWarganegara_askes()) + "\n", boldFont));
                document.add(warganegara_askes);

                Phrase pekerjaan_askes = new Phrase("Pekerjaan", normalFont);
                pekerjaan_askes.setTabSettings(new TabSettings(250f));
                pekerjaan_askes.add(Chunk.TABBING);
                pekerjaan_askes.add(new Chunk("     : " + ListAskesUA.get(i).getPekerjaan_askes() + "\n\n", boldFont));
                document.add(pekerjaan_askes);
            }

            document.newPage();
            document.add(new Paragraph(context.getString(R.string.detil_investasi) + "\n\n", normalFont));

            Phrase premi_pokok = new Phrase(context.getString(R.string.premi_pokok), normalFont);
            premi_pokok.setTabSettings(new TabSettings(250f));
            premi_pokok.add(Chunk.TABBING);
            premi_pokok.add(new Chunk("     : " + NumberFormat.getNumberInstance(Locale.GERMAN).format(me.getDetilInvestasiModel().getPremi_pokok_di()) + "\n", boldFont));
            document.add(premi_pokok);

            if (me.getDetilInvestasiModel().getPremi_berkala_di() != 0) {
                Phrase premi_topup = new Phrase(context.getString(R.string.premi_top_up_berkala), normalFont);
                premi_topup.setTabSettings(new TabSettings(250f));
                premi_topup.add(Chunk.TABBING);
                premi_topup.add(new Chunk("     : " + NumberFormat.getNumberInstance(Locale.GERMAN).format(me.getDetilInvestasiModel().getPremi_berkala_di()) + "\n", boldFont));
                document.add(premi_topup);

                Phrase jenis_topup = new Phrase(context.getString(R.string.jenis_top_up), normalFont);
                jenis_topup.setTabSettings(new TabSettings(250f));
                jenis_topup.add(Chunk.TABBING);
                jenis_topup.add(new Chunk("     : " + "BERKALA" + "\n", boldFont));
                document.add(jenis_topup);
            }
            if (me.getDetilInvestasiModel().getPremi_tunggal_di() != 0) {
                Phrase premi_tunggal = new Phrase(context.getString(R.string.premi_top_up_tunggal), normalFont);
                premi_tunggal.setTabSettings(new TabSettings(250f));
                premi_tunggal.add(Chunk.TABBING);
                premi_tunggal.add(new Chunk("     : " + NumberFormat.getNumberInstance(Locale.GERMAN).format(me.getDetilInvestasiModel().getPremi_tunggal_di()) + "\n", boldFont));
                document.add(premi_tunggal);

                Phrase jenis_topup = new Phrase(context.getString(R.string.jenis_top_up), normalFont);
                jenis_topup.setTabSettings(new TabSettings(250f));
                jenis_topup.add(Chunk.TABBING);
                jenis_topup.add(new Chunk("     : " + "TUNGGAL" + "\n", boldFont));
                document.add(jenis_topup);
            }

            Phrase premi_tambahan = new Phrase(context.getString(R.string.premi_tambahan), normalFont);
            premi_tambahan.setTabSettings(new TabSettings(250f));
            premi_tambahan.add(Chunk.TABBING);
            premi_tambahan.add(new Chunk("     : " + NumberFormat.getNumberInstance(Locale.GERMAN).format(me.getDetilInvestasiModel().getPremi_tambahan_di()) + "\n", boldFont));
            document.add(premi_tambahan);

            Phrase jml = new Phrase(context.getString(R.string.jumlah), normalFont);
            jml.setTabSettings(new TabSettings(250f));
            jml.add(Chunk.TABBING);
            jml.add(new Chunk("     : " + NumberFormat.getNumberInstance(Locale.GERMAN).format(me.getDetilInvestasiModel().getPremi_pokok_di() + me.getDetilInvestasiModel().getPremi_berkala_di() + me.getDetilInvestasiModel().getPremi_tunggal_di()) + "\n", boldFont));
            document.add(jml);//tanpa premi tambahan di

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_bentukpremi, 0);
            Phrase bentukbayar_di = new Phrase(context.getString(R.string.bentuk_pembayaran_premi), normalFont);
            bentukbayar_di.setTabSettings(new TabSettings(250f));
            bentukbayar_di.add(Chunk.TABBING);
            bentukbayar_di.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getUsulanAsuransiModel().getBentukbayar_ua()) + "\n", boldFont));
            document.add(bentukbayar_di);

            ListDropDownBank = new E_Select(context).getBankPusat("", JENIS_LOGIN, JENIS_LOGIN_BC);
            Phrase bnk = new Phrase(context.getString(R.string.bank), normalFont);
            bnk.setTabSettings(new TabSettings(250f));
            bnk.add(Chunk.TABBING);
            bnk.add(new Chunk("     : " + MethodSupport.getAdapterBank(ListDropDownBank, me.getDetilInvestasiModel().getInvest_bank_di()) + "\n", boldFont));
            document.add(bnk);

            Phrase cab = new Phrase(context.getString(R.string.cabang), normalFont);
            cab.setTabSettings(new TabSettings(250f));
            cab.add(Chunk.TABBING);
            cab.add(new Chunk("     : " + me.getDetilInvestasiModel().getInvest_cabang_di() + "\n", boldFont));
            document.add(cab);

            Phrase kota_di = new Phrase(context.getString(R.string.kota), normalFont);
            kota_di.setTabSettings(new TabSettings(250f));
            kota_di.add(Chunk.TABBING);
            kota_di.add(new Chunk("     : " + me.getDetilInvestasiModel().getInvest_kota_di() + "\n", boldFont));
            document.add(kota_di);

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_jenis_tabungan, 0);
            Phrase jns_tabungan = new Phrase(context.getString(R.string.jenis_tabungan), normalFont);
            jns_tabungan.setTabSettings(new TabSettings(250f));
            jns_tabungan.add(Chunk.TABBING);
            jns_tabungan.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getDetilInvestasiModel().getInvest_jnstab_di()) + "\n", boldFont));
            document.add(jns_tabungan);

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_jenis_nasabah, 0);
            Phrase jns_nasabah = new Phrase(context.getString(R.string.jenis_nasabah), normalFont);
            jns_nasabah.setTabSettings(new TabSettings(250f));
            jns_nasabah.add(Chunk.TABBING);
            jns_nasabah.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getDetilInvestasiModel().getInvest_jnsnasabah_di()) + "\n", boldFont));
            document.add(jns_nasabah);

            Phrase no_rek = new Phrase(context.getString(R.string.no_rekening), normalFont);
            no_rek.setTabSettings(new TabSettings(250f));
            no_rek.add(Chunk.TABBING);
            no_rek.add(new Chunk("     : " + me.getDetilInvestasiModel().getInvest_norek_di() + "\n", boldFont));
            document.add(no_rek);

            Phrase atas_nama = new Phrase(context.getString(R.string.atas_nama), normalFont);
            atas_nama.setTabSettings(new TabSettings(250f));
            atas_nama.add(Chunk.TABBING);
            atas_nama.add(new Chunk("     : " + me.getDetilInvestasiModel().getInvest_nama_di() + "\n", boldFont));
            document.add(atas_nama);

            ListDropDownString = new E_Select(context).getLst_Kurs(me.getUsulanAsuransiModel().getKd_produk_ua(), me.getUsulanAsuransiModel().getSub_produk_ua());
            Phrase kurs_di = new Phrase(context.getString(R.string.kurs), normalFont);
            kurs_di.setTabSettings(new TabSettings(250f));
            kurs_di.add(Chunk.TABBING);
            kurs_di.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, me.getDetilInvestasiModel().getInvest_kurs_di()) + "\n", boldFont));
            document.add(kurs_di);

//            Phrase kuasa = new Phrase(context.getString(R.string.memberi_kuasa) + "     : ", normalFont);
//            kuasa.add(new Chunk(me.getDetilInvestasiModel().getInvest_berikuasa_di() + "\n", boldFont));
//            document.add(kuasa);

            Phrase tgl_surat = new Phrase(context.getString(R.string.tanggal_surat), normalFont);
            tgl_surat.setTabSettings(new TabSettings(250f));
            tgl_surat.add(Chunk.TABBING);
            tgl_surat.add(new Chunk("     : " + me.getDetilInvestasiModel().getInvest_tglkuasa_di() + "\n", boldFont));
            document.add(tgl_surat);

            Phrase ket = new Phrase(context.getString(R.string.keterangan), normalFont);
            ket.setTabSettings(new TabSettings(250f));
            ket.add(Chunk.TABBING);
            ket.add(new Chunk("     : " + me.getDetilInvestasiModel().getInvest_keterangan_di() + "\n\n", boldFont));
            document.add(ket);

            document.add(new Paragraph(context.getString(R.string.no_rekening_pp_untuk_transaksi_autodebet) + "\n\n"));

            ListDropDownBankCabang = new E_Select(context).getBankCab("", JENIS_LOGIN, JENIS_LOGIN_BC);
            Phrase bnk_autodebet = new Phrase(context.getString(R.string.bank), normalFont);
            bnk_autodebet.setTabSettings(new TabSettings(250f));
            bnk_autodebet.add(Chunk.TABBING);
            bnk_autodebet.add(new Chunk("     : " + MethodSupport.getAdapterBankCab(ListDropDownBankCabang, me.getDetilInvestasiModel().getAutodbt_bank_di()) + "\n", boldFont));
            document.add(bnk_autodebet);

//            document.add(new Paragraph(context.getString(R.string.cabang) + "     : ")); //gak ada di model nya
//            document.add(new Paragraph(context.getString(R.string.kota) + "     : ")); // gak ada di model nya

            ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_jenis_tabungan, 0);
            Phrase jns_tabungan_autodebet = new Phrase(context.getString(R.string.jenis_tabungan), normalFont);
            jns_tabungan_autodebet.setTabSettings(new TabSettings(250f));
            jns_tabungan_autodebet.add(Chunk.TABBING);
            jns_tabungan_autodebet.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getDetilInvestasiModel().getAutodbt_jnstab_di()) + "\n", boldFont));
            document.add(jns_tabungan_autodebet);

//            ListDropDownString = new E_Select(getContext()).getLst_Kurs(me.getUsulanAsuransiModel().getKd_produk_ua(), me.getUsulanAsuransiModel().getSub_produk_ua());
//            document.add(new Paragraph(getString(R.string.kurs) + "     : " + MethodSupport.getAdapterPositionString( ListDropDownString, me.getDetilInvestasiModel().getAutodbt_kurs_di())));

            Phrase kurs_di_1 = new Phrase(context.getString(R.string.kurs), normalFont);
            kurs_di_1.setTabSettings(new TabSettings(250f));
            kurs_di_1.add(Chunk.TABBING);
            kurs_di_1.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, me.getDetilInvestasiModel().getInvest_kurs_di()) + "\n", boldFont));
            document.add(kurs_di_1);

            Phrase norek_autodebet = new Phrase(context.getString(R.string.no_rekening), normalFont);
            norek_autodebet.setTabSettings(new TabSettings(250f));
            norek_autodebet.add(Chunk.TABBING);
            norek_autodebet.add(new Chunk("     : " + me.getDetilInvestasiModel().getAutodbt_norek_di() + "\n", boldFont));
            document.add(norek_autodebet);

            Phrase atasnama_autodebet = new Phrase(context.getString(R.string.atas_nama), normalFont);
            atasnama_autodebet.setTabSettings(new TabSettings(250f));
            atasnama_autodebet.add(Chunk.TABBING);
            atasnama_autodebet.add(new Chunk("     : " + me.getDetilInvestasiModel().getAutodbt_nama_di() + "\n", boldFont));
            document.add(atasnama_autodebet);

            Phrase tgl_debet = new Phrase(context.getString(R.string.tanggal_debet), normalFont);
            tgl_debet.setTabSettings(new TabSettings(250f));
            tgl_debet.add(Chunk.TABBING);
            tgl_debet.add(new Chunk("     : " + me.getDetilInvestasiModel().getAutodbt_tgldebet_di() + "\n", boldFont));
            document.add(tgl_debet);

            Phrase tgl_valid = new Phrase(context.getString(R.string.tanggal_valid), normalFont);
            tgl_valid.setTabSettings(new TabSettings(250f));
            tgl_valid.add(Chunk.TABBING);
            tgl_valid.add(new Chunk("     : " + me.getDetilInvestasiModel().getAutodbt_tglvalid_di() + "\n", boldFont));
            document.add(tgl_valid);

            Phrase active = new Phrase(context.getString(R.string.aktif), normalFont);
            active.setTabSettings(new TabSettings(250f));
            active.add(Chunk.TABBING);
            active.add(new Chunk("     : " + "YA" + "\n", boldFont));
            document.add(active);

//            Phrase autodebet_premipertama = new Phrase(context.getString(R.string.autodebet_premi_pertama) + "     : ", normalFont);
//            autodebet_premipertama.add(new Chunk(me.getDetilInvestasiModel().getAutodbt_premipertama_di() + "\n", boldFont));
//            document.add(autodebet_premipertama);

            Phrase dana_investasi = new Phrase(context.getString(R.string.dana_investasi_yang_dialokasikan_di), normalFont);
            dana_investasi.setTabSettings(new TabSettings(250f));
            dana_investasi.add(Chunk.TABBING);
            dana_investasi.add(new Chunk("     : " + "Setelah permintaan asuransi disetujui oleh Bagian Underwriting dan Calon Pemegang Polis telah setuju serta membayar ekstra premi (jika ada)." + "\n\n", boldFont));
            document.add(dana_investasi);

            List<ModelDataDitunjukDI> ListDataDItunjukDI = me.getListManfaatDI();
            for (int i = 0; i < ListDataDItunjukDI.size(); i++) {
                int a = i+1;
                Phrase nomor_manfaat = new Phrase("Penerima manfaat " + a + "\n\n", normalFont);
                document.add(nomor_manfaat);

                Phrase nama_manfaat = new Phrase(context.getString(R.string.nama), normalFont);
                nama_manfaat.setTabSettings(new TabSettings(250f));
                nama_manfaat.add(Chunk.TABBING);
                nama_manfaat.add(new Chunk("     : " + ListDataDItunjukDI.get(i).getNama() + "\n", boldFont));
                document.add(nama_manfaat);

                Phrase getmanfaat = new Phrase(context.getString(R.string.manfaat), normalFont);
                getmanfaat.setTabSettings(new TabSettings(250f));
                getmanfaat.add(Chunk.TABBING);
                getmanfaat.add(new Chunk("     : " + ListDataDItunjukDI.get(i).getManfaat() + "\n", boldFont));
                document.add(getmanfaat);

//                Phrase jekel_manfaat = new Phrase(context.getString(R.string.jenis_kelamin) + "     : ", normalFont);
//                getmanfaat.add(new Chunk(ListDataDItunjukDI.get(i).getJekel() + "\n", boldFont));
//                document.add(jekel_manfaat);

                if (ListDataDItunjukDI.get(i).getJekel() == 0) {
                    Phrase jekel_w = new Phrase(context.getString(R.string.jenis_kelamin), normalFont);
                    jekel_w.setTabSettings(new TabSettings(250f));
                    jekel_w.add(Chunk.TABBING);
                    jekel_w.add(new Chunk("     : Wanita" + "\n", boldFont));
                    document.add(jekel_w);
                } else {
                    Phrase jekel_p = new Phrase(context.getString(R.string.jenis_kelamin), normalFont);
                    jekel_p.setTabSettings(new TabSettings(250f));
                    jekel_p.add(Chunk.TABBING);
                    jekel_p.add(new Chunk("     : Pria" + "\n", boldFont));
                    document.add(jekel_p);
                }

                Phrase tgl_lhr = new Phrase(context.getString(R.string.tanggal_lahir), normalFont);
                tgl_lhr.setTabSettings(new TabSettings(250f));
                tgl_lhr.add(Chunk.TABBING);
                tgl_lhr.add(new Chunk("     : " + ListDataDItunjukDI.get(i).getTtl() + "\n", boldFont));
                document.add(tgl_lhr);

                ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_relasi, 0);
                Phrase hub_tt = new Phrase(context.getString(R.string.hubungan_dengan_tt), normalFont);
                hub_tt.setTabSettings(new TabSettings(250f));
                hub_tt.add(Chunk.TABBING);
                hub_tt.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, ListDataDItunjukDI.get(i).getHub_dgcalon_tt()) + "\n", boldFont));
                document.add(hub_tt);

                ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_warganegara, 0);
                Phrase wn_manfaat = new Phrase(context.getString(R.string.warga_negara), normalFont);
                wn_manfaat.setTabSettings(new TabSettings(250f));
                wn_manfaat.add(Chunk.TABBING);
                wn_manfaat.add(new Chunk("     : " + MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, ListDataDItunjukDI.get(i).getWarganegara()) + "\n\n", boldFont));
                document.add(wn_manfaat);
            }

            List<ModelJnsDanaDI> ListJnsDanaDI = me.getListDanaDI();
            if (me.getUsulanAsuransiModel().getKd_produk_ua() != 208 && me.getUsulanAsuransiModel().getKd_produk_ua() != 219
                    && me.getUsulanAsuransiModel().getKd_produk_ua() != 212) {
                for (int i = 0; i < ListJnsDanaDI.size(); i++) {
                    if (ListJnsDanaDI.get(i).getJenis_dana() == null || ListJnsDanaDI.get(i).getJenis_dana().equals(null)) {
                        Phrase jns_dana_1 = new Phrase(context.getString(R.string.jns_dana), normalFont);
                        jns_dana_1.setTabSettings(new TabSettings(250f));
                        jns_dana_1.add(Chunk.TABBING);
                        jns_dana_1.add(new Chunk("     : " + "00" + "\n", boldFont));
                        document.add(jns_dana_1);
                    } else {
                        String Dana = new E_Select(context).getNamaFund(ListJnsDanaDI.get(i).getJenis_dana());
                        Phrase jns_dana_2 = new Phrase(context.getString(R.string.jns_dana), normalFont);
                        jns_dana_2.setTabSettings(new TabSettings(250f));
                        jns_dana_2.add(Chunk.TABBING);
                        jns_dana_2.add(new Chunk("     : " + Dana + "\n", boldFont));
                        document.add(jns_dana_2);
                    }
//                    document.add(new Paragraph(getString(R.string.alokasi_investasi) + "     : " + ListJnsDanaDI.get(i).getJumlah_alokasi()));
                    Phrase alokasi_investasi = new Phrase(context.getString(R.string.alokasi_investasi), normalFont);
                    alokasi_investasi.setTabSettings(new TabSettings(250f));
                    alokasi_investasi.add(Chunk.TABBING);
                    alokasi_investasi.add(new Chunk("     : " + ListJnsDanaDI.get(i).getPersen_alokasi() + "\n", boldFont));
                    document.add(alokasi_investasi);
                }
            }

            document.newPage();
            document.add(new Paragraph(context.getString(R.string.detil_agen) + "\n\n", normalFont));

            Phrase tgl_spaj = new Phrase(context.getString(R.string.tanggal_spaj), normalFont);
            tgl_spaj.setTabSettings(new TabSettings(250f));
            tgl_spaj.add(Chunk.TABBING);
            tgl_spaj.add(new Chunk("     : " + me.getDetilAgenModel().getTgl_spaj_da() + "\n", boldFont));
            document.add(tgl_spaj);

            Phrase kd_reg = new Phrase(context.getString(R.string.kode_regional), normalFont);
            kd_reg.setTabSettings(new TabSettings(250f));
            kd_reg.add(Chunk.TABBING);
            kd_reg.add(new Chunk("     : " + me.getDetilAgenModel().getKd_regional_da() + "\n", boldFont));
            document.add(kd_reg);

            Phrase nm_reg = new Phrase(context.getString(R.string.nama_regional), normalFont);
            nm_reg.setTabSettings(new TabSettings(250f));
            nm_reg.add(Chunk.TABBING);
            nm_reg.add(new Chunk("     : " + me.getDetilAgenModel().getNm_regional_da() + "\n", boldFont));
            document.add(nm_reg);

            Phrase kd_pntp = new Phrase(context.getString(R.string.kode_penutup), normalFont);
            kd_pntp.setTabSettings(new TabSettings(250f));
            kd_pntp.add(Chunk.TABBING);
            kd_pntp.add(new Chunk("     : " + me.getDetilAgenModel().getKd_penutup_da() + "\n", boldFont));
            document.add(kd_pntp);

            Phrase nm_pntp = new Phrase(context.getString(R.string.nama_penutup), normalFont);
            nm_pntp.setTabSettings(new TabSettings(250f));
            nm_pntp.add(Chunk.TABBING);
            nm_pntp.add(new Chunk("     : " + me.getDetilAgenModel().getNm_pnutup_da() + "\n", boldFont));
            document.add(nm_pntp);

            Phrase kd_lead = new Phrase(context.getString(R.string.kode_leader_bc), normalFont);
            kd_lead.setTabSettings(new TabSettings(250f));
            kd_lead.add(Chunk.TABBING);
            kd_lead.add(new Chunk("     : " + me.getDetilAgenModel().getKd_leader_da() + "\n", boldFont));
            document.add(kd_lead);

            Phrase kd_ao = new Phrase(context.getString(R.string.kode_ao), normalFont);
            kd_ao.setTabSettings(new TabSettings(250f));
            kd_ao.add(Chunk.TABBING);
            kd_ao.add(new Chunk("     : " + me.getDetilAgenModel().getKd_ao_da() + "\n", boldFont));
            document.add(kd_ao);

            Phrase nm_agen = new Phrase("Nama BC", normalFont);
            nm_agen.setTabSettings(new TabSettings(250f));
            nm_agen.add(Chunk.TABBING);
            nm_agen.add(new Chunk("     : " + NAMA_AGEN + "\n", boldFont));
            document.add(nm_agen);

            Phrase email_pntp = new Phrase(context.getString(R.string.email_bc), normalFont);
            email_pntp.setTabSettings(new TabSettings(250f));
            email_pntp.add(Chunk.TABBING);
            email_pntp.add(new Chunk("     : " + me.getDetilAgenModel().getEmail_da() + "\n\n", boldFont));
            document.add(email_pntp);

//            Phrase pers_polis = new Phrase(context.getString(R.string.persetujuan_polis) + "     : ", normalFont);
//            pers_polis.add(new Chunk(me.getDetilAgenModel().getKd_regional_da() + "\n", boldFont));
//            document.add(pers_polis); //masih belum
            if(groupId != 40){
                Phrase nm_reff = new Phrase(context.getString(R.string.nama_refferal) , normalFont);
                nm_reff.setTabSettings(new TabSettings(250f));
                nm_reff.add(Chunk.TABBING);
                nm_reff.add(new Chunk("     : " + me.getDetilAgenModel().getNm_refferal_da() + "\n", boldFont));
                document.add(nm_reff);

                Phrase id_reff = new Phrase(context.getString(R.string.id_refferal), normalFont);
                id_reff.setTabSettings(new TabSettings(250f));
                id_reff.add(Chunk.TABBING);
                id_reff.add(new Chunk("     : " + me.getDetilAgenModel().getId_refferal_da() + "\n", boldFont));
                document.add(id_reff);

                Phrase cab_reff = new Phrase(context.getString(R.string.cab_referral), normalFont);
                cab_reff.setTabSettings(new TabSettings(250f));
                cab_reff.add(Chunk.TABBING);
                cab_reff.add(new Chunk("     : " + me.getDetilAgenModel().getNm_cabang_da() + "\n", boldFont));
                document.add(cab_reff);
            }else {
                Phrase id_mem = new Phrase(context.getString(R.string.idmember), normalFont);
                id_mem.setTabSettings(new TabSettings(250f));
                id_mem.add(Chunk.TABBING);
                id_mem.add(new Chunk("     : " + me.getDetilAgenModel().getId_member_da() + "\n", boldFont));
                document.add(id_mem);

                Phrase id_penempatan = new Phrase(context.getString(R.string.id_penempatan), normalFont);
                id_penempatan.setTabSettings(new TabSettings(250f));
                id_penempatan.add(Chunk.TABBING);
                id_penempatan.add(new Chunk("     : " + me.getDetilAgenModel().getId_penempatan_da() + "\n", boldFont));
                document.add(id_penempatan);

                Phrase id_sponsor = new Phrase(context.getString(R.string.id_sponsor), normalFont);
                id_sponsor.setTabSettings(new TabSettings(250f));
                id_sponsor.add(Chunk.TABBING);
                id_sponsor.add(new Chunk("     : " + me.getDetilAgenModel().getId_sponsor_da() + "\n", boldFont));
                document.add(id_sponsor);
            }

            if (me.getModelID().getJns_spaj() == 4){
                QUsio(context, me, document);
            }else {
                QU(context, me, document);

                QUkesehatan(context, me, document);

                if (!me.getModelUpdateQuisioner().getList_dkno9pp().isEmpty()) {
                    List<ModelDK_NO9ppUQ> List_dkno9pp = me.getModelUpdateQuisioner().getList_dkno9pp();
                    for (int i = 0; i < List_dkno9pp.size(); i++) {
                        Phrase ct_9pp = new Phrase("Calon Pemegang Polis", boldFont);
                        ct_9pp.setTabSettings(new TabSettings(250f));
                        ct_9pp.add(Chunk.TABBING);
                        ct_9pp.add(new Chunk("     : " + List_dkno9pp.get(i).getStr_cp() + "\n", boldFont));
                        document.add(ct_9pp);

                        if (List_dkno9pp.get(i).getKeadaan().equals("Masih Hidup")){
                            Phrase umur_9pp = new Phrase("Usia saat Terdiagnosa", normalFont);
                            umur_9pp.setTabSettings(new TabSettings(250f));
                            umur_9pp.add(Chunk.TABBING);
                            umur_9pp.add(new Chunk("     : " + List_dkno9pp.get(i).getUmur() + "\n", boldFont));
                            document.add(umur_9pp);
                        }else {
                            Phrase umur_9pp = new Phrase("Usia saat Meninggal", normalFont);
                            umur_9pp.setTabSettings(new TabSettings(250f));
                            umur_9pp.add(Chunk.TABBING);
                            umur_9pp.add(new Chunk("     : " + List_dkno9pp.get(i).getUmur() + "\n", boldFont));
                            document.add(umur_9pp);
                        }

                        Phrase keadaan_9pp = new Phrase("Kondisi", normalFont);
                        keadaan_9pp.setTabSettings(new TabSettings(250f));
                        keadaan_9pp.add(Chunk.TABBING);
                        keadaan_9pp.add(new Chunk("     : " + List_dkno9pp.get(i).getKeadaan() + "\n", boldFont));
                        document.add(keadaan_9pp);

                        Phrase penyebab_9pp = new Phrase("Keterangan", normalFont);
                        penyebab_9pp.setTabSettings(new TabSettings(250f));
                        penyebab_9pp.add(Chunk.TABBING);
                        penyebab_9pp.add(new Chunk("     : " + List_dkno9pp.get(i).getPenyebab() + "\n\n", boldFont));
                        document.add(penyebab_9pp);
                    }
                }

                if (!me.getModelUpdateQuisioner().getList_dkno9tt().isEmpty()) {
                    List<ModelDK_NO9ttUQ> List_dkno9tt = me.getModelUpdateQuisioner().getList_dkno9tt();
                    for (int i = 0; i < List_dkno9tt.size(); i++) {
                        Phrase ct_9tt = new Phrase("Calon Tertanggung", boldFont);
                        ct_9tt.setTabSettings(new TabSettings(250f));
                        ct_9tt.add(Chunk.TABBING);
                        ct_9tt.add(new Chunk("     : " + List_dkno9tt.get(i).getStr_ct() + "\n", boldFont));
                        document.add(ct_9tt);

                        if (List_dkno9tt.get(i).getKeadaan().equals("Masih Hidup")) {
                            Phrase umur_9tt = new Phrase("Usia saat Terdiagnosa", normalFont);
                            umur_9tt.setTabSettings(new TabSettings(250f));
                            umur_9tt.add(Chunk.TABBING);
                            umur_9tt.add(new Chunk("     : " + List_dkno9tt.get(i).getUmur() + "\n", boldFont));
                            document.add(umur_9tt);
                        }else {
                            Phrase umur_9tt = new Phrase("Usia saat Meninggal", normalFont);
                            umur_9tt.setTabSettings(new TabSettings(250f));
                            umur_9tt.add(Chunk.TABBING);
                            umur_9tt.add(new Chunk("     : " + List_dkno9tt.get(i).getUmur() + "\n", boldFont));
                            document.add(umur_9tt);
                        }

                        Phrase keadaan_9tt = new Phrase("Kondisi", normalFont);
                        keadaan_9tt.setTabSettings(new TabSettings(250f));
                        keadaan_9tt.add(Chunk.TABBING);
                        keadaan_9tt.add(new Chunk("     : " + List_dkno9tt.get(i).getKeadaan() + "\n", boldFont));
                        document.add(keadaan_9tt);

                        Phrase penyebab_9tt = new Phrase("Keterangan", normalFont);
                        penyebab_9tt.setTabSettings(new TabSettings(250f));
                        penyebab_9tt.add(Chunk.TABBING);
                        penyebab_9tt.add(new Chunk("     : " + List_dkno9tt.get(i).getPenyebab() + "\n\n", boldFont));
                        document.add(penyebab_9tt);
                    }
                }
            }

            PR(context ,me ,document);

            document.newPage();
            document.add(new Paragraph(context.getString(R.string.dokumen_pendukung) + "\n", normalFont));

            File file_bitmap = new File(
                    context.getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());

            Image image = Image.getInstance(file_bitmap.toString() + "/DP_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg");
            image.setCompressionLevel(9);

            float documentWidth = document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin();
            float documentHeight = document.getPageSize().getHeight() - document.topMargin() - document.bottomMargin();
            image.scaleToFit(documentWidth, documentHeight);
            image.setAbsolutePosition((PageSize.A4.getWidth() - image.getScaledWidth()) / 2, (PageSize.A4.getHeight() - image.getScaledHeight()) / 2);
            document.add(image);

//            document.newPage();
//            file_ttd = new File(getActivity().getFilesDir(), "ESPAJ/spaj_ttd");
//            File[] listTTD = file_ttd.listFiles();
//
//            for (File ttd: listTTD) {
//                Image imagettd = Image.getInstance("/data/user/0/id.co.ajsmsig.nb/files/ESPAJ/spaj_ttd/" + ttd.getName());
//                imagettd.scaleToFit(documentWidth, documentHeight);
//                document.add(imagettd);
//                document.newPage();
//            }

//            document.newPage();
//            document.add(new Paragraph(context.getString(R.string.questionnaire) + "\n\n", normalFont));
//
//            if (me.getModelID().getJns_spaj() == 3) {
//                Image imageQU1 = Image.getInstance(file_bitmap.toString() + "/QU_HAL1_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg");
//                imageQU1.setCompressionLevel(9);
//                Image imageQU2 = Image.getInstance(file_bitmap.toString() + "/QU_HAL2_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg");
//                imageQU2.setCompressionLevel(9);
//                Image imageQU3 = Image.getInstance(file_bitmap.toString() + "/QU_HAL3_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg");
//                imageQU3.setCompressionLevel(9);
//                Image imageQU4 = Image.getInstance(file_bitmap.toString() + "/QU_HAL4_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg");
//                imageQU4.setCompressionLevel(9);
//
//                imageQU1.scaleToFit(documentWidth, documentHeight);
//                imageQU1.setAbsolutePosition((PageSize.A4.getWidth() - image.getScaledWidth()) / 2, (PageSize.A4.getHeight() - image.getScaledHeight()) / 2);
//
//                imageQU2.scaleToFit(documentWidth, documentHeight);
////                imageQU2.setAbsolutePosition((PageSize.A4.getWidth() - image.getScaledWidth()) / 2,(PageSize.A4.getHeight() - image.getScaledHeight()) / 2);
//
//                imageQU3.scaleToFit(documentWidth, documentHeight);
//                imageQU3.setAbsolutePosition((PageSize.A4.getWidth() - image.getScaledWidth()) / 2, (PageSize.A4.getHeight() - image.getScaledHeight()) / 2);
//
//                imageQU4.scaleToFit(documentWidth, documentHeight);
//                imageQU4.setAbsolutePosition((PageSize.A4.getWidth() - image.getScaledWidth()) / 2, (PageSize.A4.getHeight() - image.getScaledHeight()) / 2);
//
//                document.add(imageQU1);
//                document.newPage();
//                document.add(imageQU2);
//                document.newPage();
//                document.add(imageQU3);
//                document.newPage();
//                document.add(imageQU4);

//                List<ModelMst_Answer> listQuisioner = me.getModelUpdateQuisioner().getListQuisioner();
//                for (int i = 0; i < listQuisioner.size(); i++) {
//
//                    Integer question_id = listQuisioner.get(i).getQuestion_id();
//
//                    if (question_id == 1) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("1." + " " + context.getString(R.string.qu1), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("1." + " " + context.getString(R.string.qu1), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("1." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 2) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_2 = new Phrase("2." + " " + context.getString(R.string.qu2), normalFont);
//                                qu_2.setTabSettings(new TabSettings(250f));
//                                qu_2.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_2);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_2 = new Phrase("2." + " " + context.getString(R.string.qu2), normalFont);
//                                qu_2.setTabSettings(new TabSettings(250f));
//                                qu_2.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_2);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("2." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 3) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_3 = new Phrase("3." + " " + context.getString(R.string.qu3), normalFont);
//                                qu_3.setTabSettings(new TabSettings(250f));
//                                qu_3.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_3);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_3 = new Phrase("3." + " " + context.getString(R.string.qu3), normalFont);
//                                qu_3.setTabSettings(new TabSettings(250f));
//                                qu_3.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_3);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("3." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 4) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_4 = new Phrase("4." + " " + context.getString(R.string.qu4), normalFont);
//                                qu_4.setTabSettings(new TabSettings(250f));
//                                qu_4.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_4);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_4 = new Phrase("4." + " " + context.getString(R.string.qu4), normalFont);
//                                qu_4.setTabSettings(new TabSettings(250f));
//                                qu_4.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_4);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("g : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("4." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 5) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_5 = new Phrase("5." + " " + context.getString(R.string.qu5), normalFont);
//                                qu_5.setTabSettings(new TabSettings(250f));
//                                qu_5.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_5);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_5 = new Phrase("5." + " " + context.getString(R.string.qu5), normalFont);
//                                qu_5.setTabSettings(new TabSettings(250f));
//                                qu_5.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_5);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("5." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 6) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_6 = new Phrase("6." + " " + context.getString(R.string.qu6), normalFont);
//                                qu_6.setTabSettings(new TabSettings(250f));
//                                qu_6.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_6);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_6 = new Phrase("6." + " " + context.getString(R.string.qu6), normalFont);
//                                qu_6.setTabSettings(new TabSettings(250f));
//                                qu_6.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_6);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("6." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 7) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_7 = new Phrase("7." + " " + context.getString(R.string.qu7), normalFont);
//                                qu_7.setTabSettings(new TabSettings(250f));
//                                qu_7.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_7);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_7 = new Phrase("7." + " " + context.getString(R.string.qu7), normalFont);
//                                qu_7.setTabSettings(new TabSettings(250f));
//                                qu_7.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_7);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("7." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 8) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_8 = new Phrase("8." + " " + context.getString(R.string.qu8), normalFont);
//                                qu_8.setTabSettings(new TabSettings(250f));
//                                qu_8.add(Chunk.TABBING);
//                                qu_8.add(new Chunk(" : Ya" + "\n\n" , boldFont));
//                                document.add(qu_8);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_8 = new Phrase("8." + " " + context.getString(R.string.qu8), normalFont);
//                                qu_8.setTabSettings(new TabSettings(250f));
//                                qu_8.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_8);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("8." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 9) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_8a = new Phrase("a." + " " + context.getString(R.string.qua), normalFont);
//                                qu_8a.setTabSettings(new TabSettings(250f));
//                                qu_8a.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_8a);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_8a = new Phrase("a." + " " + context.getString(R.string.qua), normalFont);
//                                qu_8a.setTabSettings(new TabSettings(250f));
//                                qu_8a.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_8a);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("8a." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 10) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_8b = new Phrase("b." + " " + context.getString(R.string.qub), normalFont);
//                                qu_8b.setTabSettings(new TabSettings(250f));
//                                qu_8b.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_8b);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_8b = new Phrase("b." + " " + context.getString(R.string.qub), normalFont);
//                                qu_8b.setTabSettings(new TabSettings(250f));
//                                qu_8b.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_8b);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("8b." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 11) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_9 = new Phrase("9." + " " + context.getString(R.string.qu9), normalFont);
//                                qu_9.setTabSettings(new TabSettings(250f));
//                                qu_9.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_9);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_9 = new Phrase("9." + " " + context.getString(R.string.qu9), normalFont);
//                                qu_9.setTabSettings(new TabSettings(250f));
//                                qu_9.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_9);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("9." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 12) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_10 = new Phrase("10." + " " + context.getString(R.string.qu10), normalFont);
//                                qu_10.setTabSettings(new TabSettings(250f));
//                                qu_10.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_10);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_10 = new Phrase("10." + " " + context.getString(R.string.qu10), normalFont);
//                                qu_10.setTabSettings(new TabSettings(250f));
//                                qu_10.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_10);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("10." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 13) {
//                        if (listQuisioner.get(i).getOption_group() == 0) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 ) {
//                                Phrase qu_11 = new Phrase("11a." + " " + context.getString(R.string.qu11a), normalFont);
//                                qu_11.setTabSettings(new TabSettings(250f));
//                                qu_11.add(Chunk.TABBING);
////                                qu_11.add(new Chunk("\n" + " " + "\n\n" , boldFont));
//                                document.add(qu_11);
//                                document.add(new Paragraph("\n" + "Tinggi Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 ) {
//                                document.add(new Paragraph("Berat Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("11a." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 14) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_11b = new Phrase("11b." + " " + context.getString(R.string.qu11b), normalFont);
//                                qu_11b.setTabSettings(new TabSettings(250f));
//                                qu_11b.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_11b);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_11b = new Phrase("11b." + " " + context.getString(R.string.qu11b), normalFont);
//                                qu_11b.setTabSettings(new TabSettings(250f));
//                                qu_11b.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_11b);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("11b." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 15) {
//                        if (listQuisioner.get(i).getOption_group() == 0) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 ) {
//                                Phrase qu_12 = new Phrase("12." + " " + context.getString(R.string.qu12), normalFont);
//                                qu_12.setTabSettings(new TabSettings(250f));
//                                qu_12.add(Chunk.TABBING);
////                                qu_11.add(new Chunk("\n" + " " + "\n\n" , boldFont));
//                                document.add(qu_12);
//                                document.add(new Paragraph("\n" + "Tinggi Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 ) {
//                                document.add(new Paragraph("Berat Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("12." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 16) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_13 = new Phrase("13." + " " + context.getString(R.string.qu13), normalFont);
//                                qu_13.setTabSettings(new TabSettings(250f));
//                                qu_13.add(Chunk.TABBING);
//                                qu_13.add(new Chunk("\n" + context.getString(R.string.qu13a) + "\n\n" , normalFont));
//                                document.add(qu_13);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_13 = new Phrase("13." + " " + context.getString(R.string.qu13), normalFont);
//                                qu_13.setTabSettings(new TabSettings(250f));
//                                qu_13.add(Chunk.TABBING);
////                                qu_13.add(new Chunk("\n" + context.getString(R.string.qu13a) + "\n\n" , normalFont));
//                                document.add(qu_13);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 ) {
////                                Phrase qu_11 = new Phrase("13." + " " + context.getString(R.string.qu11a), normalFont);
////                                qu_11.setTabSettings(new TabSettings(250f));
////                                qu_11.add(Chunk.TABBING);
//////                                qu_11.add(new Chunk("\n" + " " + "\n\n" , boldFont));
////                                document.add(qu_11);
//                                document.add(new Paragraph("\n" + "13." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 ) {
//                                document.add(new Paragraph("13." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("13." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 17) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_14 = new Phrase("14." + " " + context.getString(R.string.qu14), normalFont);
//                                qu_14.setTabSettings(new TabSettings(250f));
//                                qu_14.add(Chunk.TABBING);
//                                qu_14.add(new Chunk("\n" + context.getString(R.string.qu14a) + "\n\n" , normalFont));
//                                document.add(qu_14);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_14 = new Phrase("14." + " " + context.getString(R.string.qu14), normalFont);
//                                qu_14.setTabSettings(new TabSettings(250f));
//                                qu_14.add(Chunk.TABBING);
////                                qu_14.add(new Chunk("\n" + context.getString(R.string.qu14a) + "\n\n" , normalFont));
//                                document.add(qu_14);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 ) {
////                                Phrase qu_11 = new Phrase("9." + " " + context.getString(R.string.qu11a), normalFont);
////                                qu_11.setTabSettings(new TabSettings(250f));
////                                qu_11.add(Chunk.TABBING);
//////                                qu_11.add(new Chunk("\n" + " " + "\n\n" , boldFont));
////                                document.add(qu_11);
//                                document.add(new Paragraph("\n" + "14." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 ) {
//                                document.add(new Paragraph("14." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("14." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 18) {
//                        if (listQuisioner.get(i).getOption_group() == 0) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 ) {
//                                Phrase qu_11 = new Phrase("15." + " " + context.getString(R.string.qu15), normalFont);
//                                qu_11.setTabSettings(new TabSettings(250f));
//                                qu_11.add(Chunk.TABBING);
////                                qu_11.add(new Chunk("\n" + " " + "\n\n" , boldFont));
//                                document.add(qu_11);
//                                document.add(new Paragraph("\n" + "Tinggi Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 ) {
//                                document.add(new Paragraph("Berat Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("15." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 106){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase dk_i = new Phrase(context.getString(R.string.data_kesehatan) + "\n" + "1." + " " + context.getString(R.string.data_kesehatan1) , normalFont);
//                                dk_i.setTabSettings(new TabSettings(250f));
//                                dk_i.add(Chunk.TABBING);
//                                dk_i.add(new Chunk("\n" + "i." + " " +  context.getString(R.string.nyeri_dada) + "\n" , boldFont));
//                                document.add(dk_i);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase dk_i = new Phrase(context.getString(R.string.data_kesehatan) + "\n" + "1." + " " + context.getString(R.string.data_kesehatan1) , normalFont);
//                                dk_i.setTabSettings(new TabSettings(250f));
//                                dk_i.add(Chunk.TABBING);
//                                dk_i.add(new Chunk("\n" + "i." + " " +  context.getString(R.string.nyeri_dada) + "\n\n" , boldFont));
//                                document.add(dk_i);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("i." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 107){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase dk_ii = new Phrase("ii." + " " + context.getString(R.string.tekanan_darah_tinggi), boldFont);
//                                dk_ii.setTabSettings(new TabSettings(250f));
//                                dk_ii.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(dk_ii);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase dk_ii = new Phrase("ii." + " " + context.getString(R.string.tekanan_darah_tinggi), boldFont);
//                                dk_ii.setTabSettings(new TabSettings(250f));
//                                dk_ii.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(dk_ii);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("ii." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 108){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase dk_iii = new Phrase("iii." + " " + context.getString(R.string.stroke), boldFont);
//                                dk_iii.setTabSettings(new TabSettings(250f));
//                                dk_iii.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(dk_iii);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase dk_iii = new Phrase("iii." + " " + context.getString(R.string.stroke), boldFont);
//                                dk_iii.setTabSettings(new TabSettings(250f));
//                                dk_iii.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(dk_iii);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("iii." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 109){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("iv." + " " + context.getString(R.string.kelainan_jantung), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("iv." + " " + context.getString(R.string.kelainan_jantung), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("iv." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    }  else if (question_id == 110){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("v." + " " + context.getString(R.string.bising_jantung), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("v." + " " + context.getString(R.string.bising_jantung), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("v." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 111){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("vi." + " " + context.getString(R.string.asma), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("vi." + " " + context.getString(R.string.asma), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("vi." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 112){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("vii." + " " + context.getString(R.string.batuk_kronis), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("vii." + " " + context.getString(R.string.batuk_kronis), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("vii." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 113){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("viii." + " " + context.getString(R.string.sesak_napas), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("viii." + " " + context.getString(R.string.sesak_napas), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("viii." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 114){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("ix." + " " + context.getString(R.string.kelainan_paru_paru), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("ix." + " " + context.getString(R.string.kelainan_paru_paru), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("ix." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 115){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("x." + " " + context.getString(R.string.kencing_manis), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("x." + " " + context.getString(R.string.kencing_manis), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("x." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 116){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xi." + " " + context.getString(R.string.sakit_maag), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xi." + " " + context.getString(R.string.sakit_maag), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xi." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 117){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xii." + " " + context.getString(R.string.colitis), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xii." + " " + context.getString(R.string.colitis), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xii." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 118){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xiii." + " " + context.getString(R.string.diare_kronis), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xiii." + " " + context.getString(R.string.diare_kronis), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xiii." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 119){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xiv." + " " + context.getString(R.string.hepatitis), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xiv." + " " + context.getString(R.string.hepatitis), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xiv." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    }  else if (question_id == 120){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xv." + " " + context.getString(R.string.kelainan_pencernaan_lainnya), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xv." + " " + context.getString(R.string.kelainan_pencernaan_lainnya), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xv." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 121){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xvi." + " " + context.getString(R.string.pingsan), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xvi." + " " + context.getString(R.string.pingsan), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xvi." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 122){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xvii." + " " + context.getString(R.string.ayan), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xvii." + " " + context.getString(R.string.ayan), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xvii." + context.getString(R.string.keterangan) + " : " +listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 123){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xviii." + " " + context.getString(R.string.kelainan_syaraf_dan_mental), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xviii." + " " + context.getString(R.string.kelainan_syaraf_dan_mental), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xviii." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 124){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xix." + " " + context.getString(R.string.kanker), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xix." + " " + context.getString(R.string.kanker), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xix." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 125){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xx." + " " + context.getString(R.string.tumor), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xx." + " " + context.getString(R.string.tumor), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xx." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 126){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxi." + " " + context.getString(R.string.pembesaran_kelenjar), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxi." + " " + context.getString(R.string.pembesaran_kelenjar), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xxi." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 127){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxii." + " " + context.getString(R.string.pembesaran_kelenjar_getah_bening), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxii." + " " + context.getString(R.string.pembesaran_kelenjar_getah_bening), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xxii." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 128){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxiii." + " " + context.getString(R.string.anemia), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxiii." + " " + context.getString(R.string.anemia), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xxiii." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 129){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxiv." + " " + context.getString(R.string.pendarahan), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxiv." + " " + context.getString(R.string.pendarahan), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xiv." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 130){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxv." + " " + context.getString(R.string.kelainan_darah), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxv." + " " + context.getString(R.string.kelainan_darah), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xxv." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 131){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxvi." + " " + context.getString(R.string.kelainan_urine), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxvi." + " " + context.getString(R.string.kelainan_urine), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xxvi." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 132){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxvii." + " " + context.getString(R.string.ginjal), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxvii." + " " + context.getString(R.string.ginjal), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xxvii." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 133){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxviii." + " " + context.getString(R.string.kelainan_kandung_kemih), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxviii." + " " + context.getString(R.string.kelainan_kandung_kemih), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xxviii." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 134){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxvix." + " " + context.getString(R.string.arthritis), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxvix." + " " + context.getString(R.string.arthritis), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xxvix." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 135){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxx." + " " + context.getString(R.string.hiv), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxx." + " " + context.getString(R.string.hiv), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xxx." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 136){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxxi." + " " + context.getString(R.string.penyakit_operasi_cidera), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("xxxi." + " " + context.getString(R.string.penyakit_operasi_cidera), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("xxxi." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 137){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("2." + " " + context.getString(R.string.data_kesehatan2), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("2." + " " + context.getString(R.string.data_kesehatan2), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("2." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 139){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("3." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3a), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("3." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3a), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("3a." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 140){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("3b." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3b), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("3b." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3b), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("3b." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 141){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("3c." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3c), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("3c." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3c), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("3c." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 142){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("3d." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3d), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("3d." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3d), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("3d." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 143){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("4." + " " + context.getString(R.string.data_kesehatan4), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("4." + " " + context.getString(R.string.data_kesehatan4), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("4." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 144){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("5." + " " + context.getString(R.string.data_kesehatan5), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("5." + " " + context.getString(R.string.data_kesehatan5), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("5." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 145){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("6." + " " + context.getString(R.string.data_kesehatan6), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("6." + " " + context.getString(R.string.data_kesehatan6), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("6." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 146){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("7." + " " + context.getString(R.string.data_kesehatan7), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("7." + " " + context.getString(R.string.data_kesehatan7), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("7." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 147){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("7a." + " " + context.getString(R.string.data_kesehatan7a), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
////                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                                document.add(new Paragraph("7a." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("7a." + " " + context.getString(R.string.data_kesehatan7a), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
////                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                                document.add(new Paragraph("7a." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("7a." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 148){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("7b." + " " + context.getString(R.string.data_kesehatan7b), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
////                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                                document.add(new Paragraph("7b." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("7b." + " " + context.getString(R.string.data_kesehatan7b), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
////                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                                document.add(new Paragraph("7b." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("7b." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 149){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("7c." + " " + context.getString(R.string.data_kesehatan7c), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
////                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                                document.add(new Paragraph("7c." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("7c." + " " + context.getString(R.string.data_kesehatan7c), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
////                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                                document.add(new Paragraph("7c." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("7c." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 150){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("7d." + " " + context.getString(R.string.data_kesehatan7d), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
////                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                                document.add(new Paragraph("7d." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("7d." + " " + context.getString(R.string.data_kesehatan7d), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
////                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                                document.add(new Paragraph("7d." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("7d." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 152){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("8a." + " " + context.getString(R.string.data_kesehatan8) + "\n" + context.getString(R.string.data_kesehatan8a), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("8a." + " " + context.getString(R.string.data_kesehatan8) + "\n" + context.getString(R.string.data_kesehatan8a), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("8a." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 153){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("8b." + " " + context.getString(R.string.data_kesehatan8) + "\n" + context.getString(R.string.data_kesehatan8b), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("8b." + " " + context.getString(R.string.data_kesehatan8) + "\n" + context.getString(R.string.data_kesehatan8b), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("8b." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 154){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("8c." + " " + context.getString(R.string.data_kesehatan8) + "\n" + context.getString(R.string.data_kesehatan8c), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("8c." + " " + context.getString(R.string.data_kesehatan8) + "\n" + context.getString(R.string.data_kesehatan8c), normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("8c." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 155){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("9." + " " + context.getString(R.string.data_kesehatan9) + "\n" , normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase qu_1 = new Phrase("9." + " " + context.getString(R.string.data_kesehatan9) + "\n" , normalFont);
//                                qu_1.setTabSettings(new TabSettings(250f));
//                                qu_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(qu_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("9." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    }


//                List<ModelMst_Answer> listQuisioner = me.getModelUpdateQuisioner().getListQuisioner();
//                for (int i = 0; i < listQuisioner.size(); i++) {
//                    if(listQuisioner.get(i).getOption_group() == 1) {
//                        if(listQuisioner.get(i).getAnswer().equals("0")){
//                        } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                            document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                        } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                            document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                        }
//                    } else if (listQuisioner.get(i).getOption_group() == 2) {
//                        if(listQuisioner.get(i).getAnswer().equals("0")){
//
//                        } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                            document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                        } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                            document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                        }
//                    } else if (listQuisioner.get(i).getOption_group() == 0) {
//                        document.add(new Paragraph("answer : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                    }
//                }
//            } else if (me.getModelID().getJns_spaj() == 4) {
//                Image imageQU = Image.getInstance(file_bitmap.toString() + "/QU_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg");
//
//                imageQU.scaleToFit(documentWidth, documentHeight);
//                imageQU.setAbsolutePosition((PageSize.A4.getWidth() - image.getScaledWidth()) / 2, (PageSize.A4.getHeight() - image.getScaledHeight()) / 2);
//
//                document.add(imageQU);



//                List<ModelMst_Answer> listQuisioner = me.getModelUpdateQuisioner().getListQuisioner();
//                for (int i = 0; i < listQuisioner.size(); i++) {
//                    String question_valid_date = listQuisioner.get(i).getQuestion_valid_date();
//                    Integer question_type_id = listQuisioner.get(i).getQuestion_type_id();
//                    Integer question_id = listQuisioner.get(i).getQuestion_id();
//                    Integer option_type = listQuisioner.get(i).getOption_type();
//                    Integer option_group = listQuisioner.get(i).getOption_group();
//                    Integer option_order = listQuisioner.get(i).getOption_order();
//                    Integer answer_order = 1;
//                    String answer = listQuisioner.get(i).getAnswer();


//                    document.add(new Paragraph(question_valid_date));
//                    document.add(new Paragraph(question_type_id));
//                    if (question_id == 81){
//                        document.add(new Paragraph(question_id + " " + i));
//                        Phrase cab_reff = new Phrase(context.getString(R.string.no1_sio), normalFont);
//                        cab_reff.setTabSettings(new TabSettings(250f));
//                        cab_reff.add(Chunk.TABBING);
//                        cab_reff.add(new Chunk("\n" +"     : " + answer , boldFont));
//                        document.add(cab_reff);;
//                    }
//                    document.add(new Paragraph(option_type));
//                    document.add(new Paragraph(option_group));
//                    document.add(new Paragraph(option_order));
//                    document.add(new Paragraph(answer_order));
//                    document.add(new Paragraph(answer));
//                }

//                List<ModelMst_Answer> listQuisioner = me.getModelUpdateQuisioner().getListQuisioner();
//                for (int i = 0; i < listQuisioner.size(); i++) {
//
//                    Integer question_id = listQuisioner.get(i).getQuestion_id();
//
//                    if (question_id == 81) {
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_1 = new Phrase("1." + " " + context.getString(R.string.no1_sio), normalFont);
//                                sio_1.setTabSettings(new TabSettings(250f));
//                                sio_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(sio_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_1 = new Phrase("1." + " " + context.getString(R.string.no1_sio), normalFont);
//                                sio_1.setTabSettings(new TabSettings(250f));
//                                sio_1.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(sio_1);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("1." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 82){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_2 = new Phrase("2." + " " + context.getString(R.string.no2_sio), normalFont);
//                                sio_2.setTabSettings(new TabSettings(250f));
//                                sio_2.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(sio_2);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_2 = new Phrase("2." + " " + context.getString(R.string.no2_sio), normalFont);
//                                sio_2.setTabSettings(new TabSettings(250f));
//                                sio_2.add(Chunk.TABBING);
////                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
//                                document.add(sio_2);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("2." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 84){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3." + " " + context.getString(R.string.no3_sio), normalFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + "3a." + " " +  context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3." + " " + context.getString(R.string.no3_sio), normalFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + "3a." + " " +  context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("a." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 85){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3b." + " " + context.getString(R.string.no3b_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3b." + " " + context.getString(R.string.no3b_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("b." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 86){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3c." + " " + context.getString(R.string.no3c_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3c." + " " + context.getString(R.string.no3c_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("c." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 87){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3d." + " " + context.getString(R.string.no3d_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3d." + " " + context.getString(R.string.no3d_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("d." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    }  else if (question_id == 88){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3e." + " " + context.getString(R.string.no3e_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3e." + " " + context.getString(R.string.no3e_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("e." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 89){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3f." + " " + context.getString(R.string.no3f_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3f." + " " + context.getString(R.string.no3f_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("f." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 90){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3g." + " " + context.getString(R.string.no3g_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3g." + " " + context.getString(R.string.no3g_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("g." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 91){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3h." + " " + context.getString(R.string.no3h_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3h." + " " + context.getString(R.string.no3h_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("h." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 92){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3i." + " " + context.getString(R.string.no3i_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3i." + " " + context.getString(R.string.no3i_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("i." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 93){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3j." + " " + context.getString(R.string.no3j_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3j." + " " + context.getString(R.string.no3j_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("j." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 94){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3k." + " " + context.getString(R.string.no3k_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3k." + " " + context.getString(R.string.no3k_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("k." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 95){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3l." + " " + context.getString(R.string.no3l_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3l." + " " + context.getString(R.string.no3l_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("l." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 96){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3m." + " " + context.getString(R.string.no3m_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3m." + " " + context.getString(R.string.no3m_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("m." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 97){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3n." + " " + context.getString(R.string.no3n_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3n." + " " + context.getString(R.string.no3n_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("n." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    }  else if (question_id == 98){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3o." + " " + context.getString(R.string.no3o_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3o." + " " + context.getString(R.string.no3o_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("o." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 99){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3p." + " " + context.getString(R.string.no3p_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_3 = new Phrase("3p." + " " + context.getString(R.string.no3p_sio), boldFont);
//                                sio_3.setTabSettings(new TabSettings(250f));
//                                sio_3.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_3);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("p." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 100){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph(context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 101){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_4 = new Phrase("4." + " " + context.getString(R.string.no4_sio), normalFont);
//                                sio_4.setTabSettings(new TabSettings(250f));
//                                sio_4.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_4);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_4 = new Phrase("4." + " " + context.getString(R.string.no4_sio), normalFont);
//                                sio_4.setTabSettings(new TabSettings(250f));
//                                sio_4.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_4);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("4." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 102){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_5 = new Phrase("5." + " " + context.getString(R.string.no5_sio), normalFont);
//                                sio_5.setTabSettings(new TabSettings(250f));
//                                sio_5.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_5);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_5 = new Phrase("5." + " " + context.getString(R.string.no5_sio), normalFont);
//                                sio_5.setTabSettings(new TabSettings(250f));
//                                sio_5.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_5);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("5." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 103){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_6 = new Phrase("6." + " " + context.getString(R.string.no6_sio), normalFont);
//                                sio_6.setTabSettings(new TabSettings(250f));
//                                sio_6.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
//                                document.add(sio_6);
//                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                Phrase sio_6 = new Phrase("6." + " " + context.getString(R.string.no6_sio), normalFont);
//                                sio_6.setTabSettings(new TabSettings(250f));
//                                sio_6.add(Chunk.TABBING);
////                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
//                                document.add(sio_6);
//                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
//                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("6." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    } else if (question_id == 104){
//                        if (listQuisioner.get(i).getOption_group() == 1) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//                            } else if (listQuisioner.get(i).getOption_order() == 1 ) {
//                                Phrase sio_7 = new Phrase("7." + " " + context.getString(R.string.no7_sio), normalFont);
//                                sio_7.setTabSettings(new TabSettings(250f));
//                                sio_7.add(Chunk.TABBING);
//                                sio_7.add(new Chunk("\n" + "Calon Pemegang Polis " + "\n\n" , boldFont));
//                                document.add(sio_7);
//                                document.add(new Paragraph("Tinggi Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 ) {
//                                document.add(new Paragraph("Berat Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 2) {
//                            if (listQuisioner.get(i).getAnswer().equals("0")) {
//
//                            } else if (listQuisioner.get(i).getOption_order() == 1 ) {
//                                Phrase sio_7 = new Phrase("7." + " " + context.getString(R.string.no7_sio), normalFont);
//                                sio_7.setTabSettings(new TabSettings(250f));
//                                sio_7.add(Chunk.TABBING);
//                                sio_7.add(new Chunk("\n" + "Calon Tertanggung " + "\n\n" , boldFont));
//                                document.add(sio_7);
//                                document.add(new Paragraph("Tinggi Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            } else if (listQuisioner.get(i).getOption_order() == 2 ) {
//                                document.add(new Paragraph("Berat Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                            }
//                        } else if (listQuisioner.get(i).getOption_group() == 0) {
//                            document.add(new Paragraph("7." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
//                        }
//                    }
//                }
//            }

//            document.newPage();
//            file_foto = new File(context.getFilesDir(), "ESPAJ/spaj_foto");
//            File[] list = file_foto.listFiles();
//
//            for (File f : list) {
//                Image imageFoto = Image.getInstance(file_foto.toString() + "/" + f.getName());
//                imageFoto.setCompressionLevel(999999999);
//                imageFoto.scaleToFit(documentWidth, documentHeight);
//                document.add(imageFoto);
//                document.newPage();
//            }

//            document.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        if (document.isOpen()) {
            document.close();
        }

        return PDF;
    }

    public static String QUkesehatan (Context context, EspajModel me, Document document) {
        String QUkesehatan = "";
        Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN,14);
        Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN,14,
                Font.BOLD);
        try {
            document.newPage();
            document.add(new Paragraph(context.getString(R.string.questionnaire) + "\n\n", normalFont));
            if (me.getModelID().getJns_spaj() == 3) {
                List<ModelMst_Answer> listQuisioner = me.getModelUpdateQuisioner().getListQuisioner();
                for (int i = 0; i < listQuisioner.size(); i++) {
                    Integer question_id = listQuisioner.get(i).getQuestion_id();
                    if (question_id == 106) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase dk_i = new Phrase(context.getString(R.string.data_kesehatan) + "\n" + "1." + " " + context.getString(R.string.data_kesehatan1), normalFont);
                                dk_i.setTabSettings(new TabSettings(250f));
                                dk_i.add(Chunk.TABBING);
                                dk_i.add(new Chunk("\n" + "i." + " " + context.getString(R.string.nyeri_dada) + "\n", boldFont));
                                document.add(dk_i);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase dk_i = new Phrase(context.getString(R.string.data_kesehatan) + "\n" + "1." + " " + context.getString(R.string.data_kesehatan1), normalFont);
                                dk_i.setTabSettings(new TabSettings(250f));
                                dk_i.add(Chunk.TABBING);
                                dk_i.add(new Chunk("\n" + "i." + " " + context.getString(R.string.nyeri_dada) + "\n\n", boldFont));
                                document.add(dk_i);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("i. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 107) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase dk_ii = new Phrase("ii." + " " + context.getString(R.string.tekanan_darah_tinggi), boldFont);
                                dk_ii.setTabSettings(new TabSettings(250f));
                                dk_ii.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(dk_ii);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase dk_ii = new Phrase("ii." + " " + context.getString(R.string.tekanan_darah_tinggi), boldFont);
                                dk_ii.setTabSettings(new TabSettings(250f));
                                dk_ii.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(dk_ii);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("ii. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 108) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase dk_iii = new Phrase("iii." + " " + context.getString(R.string.stroke), boldFont);
                                dk_iii.setTabSettings(new TabSettings(250f));
                                dk_iii.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(dk_iii);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase dk_iii = new Phrase("iii." + " " + context.getString(R.string.stroke), boldFont);
                                dk_iii.setTabSettings(new TabSettings(250f));
                                dk_iii.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(dk_iii);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("iii. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 109) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("iv." + " " + context.getString(R.string.kelainan_jantung), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("iv." + " " + context.getString(R.string.kelainan_jantung), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("iv. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 110) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("v." + " " + context.getString(R.string.bising_jantung), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("v." + " " + context.getString(R.string.bising_jantung), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("v. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 111) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("vi." + " " + context.getString(R.string.asma), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("vi." + " " + context.getString(R.string.asma), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("vi. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 112) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("vii." + " " + context.getString(R.string.batuk_kronis), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("vii." + " " + context.getString(R.string.batuk_kronis), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("vii. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 113) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("viii." + " " + context.getString(R.string.sesak_napas), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("viii." + " " + context.getString(R.string.sesak_napas), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("viii. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 114) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("ix." + " " + context.getString(R.string.kelainan_paru_paru), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("ix." + " " + context.getString(R.string.kelainan_paru_paru), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("ix. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 115) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("x." + " " + context.getString(R.string.kencing_manis), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("x." + " " + context.getString(R.string.kencing_manis), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("x. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 116) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xi." + " " + context.getString(R.string.sakit_maag), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xi." + " " + context.getString(R.string.sakit_maag), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xi. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 117) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xii." + " " + context.getString(R.string.colitis), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xii." + " " + context.getString(R.string.colitis), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xii. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 118) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xiii." + " " + context.getString(R.string.diare_kronis), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xiii." + " " + context.getString(R.string.diare_kronis), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xiii. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 119) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xiv." + " " + context.getString(R.string.hepatitis), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xiv." + " " + context.getString(R.string.hepatitis), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xiv. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 120) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xv." + " " + context.getString(R.string.kelainan_pencernaan_lainnya), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xv." + " " + context.getString(R.string.kelainan_pencernaan_lainnya), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xv. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 121) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xvi." + " " + context.getString(R.string.pingsan), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xvi." + " " + context.getString(R.string.pingsan), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xvi. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 122) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xvii." + " " + context.getString(R.string.ayan), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xvii." + " " + context.getString(R.string.ayan), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xvii. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 123) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xviii." + " " + context.getString(R.string.kelainan_syaraf_dan_mental), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xviii." + " " + context.getString(R.string.kelainan_syaraf_dan_mental), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xviii. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 124) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xix." + " " + context.getString(R.string.kanker), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xix." + " " + context.getString(R.string.kanker), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xix. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 125) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xx." + " " + context.getString(R.string.tumor), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xx." + " " + context.getString(R.string.tumor), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xx. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 126) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxi." + " " + context.getString(R.string.pembesaran_kelenjar), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxi." + " " + context.getString(R.string.pembesaran_kelenjar), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xxi. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 127) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxii." + " " + context.getString(R.string.pembesaran_kelenjar_getah_bening), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxii." + " " + context.getString(R.string.pembesaran_kelenjar_getah_bening), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xxii. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 128) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxiii." + " " + context.getString(R.string.anemia), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxiii." + " " + context.getString(R.string.anemia), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xxiii. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 129) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxiv." + " " + context.getString(R.string.pendarahan), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxiv." + " " + context.getString(R.string.pendarahan), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xiv. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 130) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxv." + " " + context.getString(R.string.kelainan_darah), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxv." + " " + context.getString(R.string.kelainan_darah), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xxv. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 131) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxvi." + " " + context.getString(R.string.kelainan_urine), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxvi." + " " + context.getString(R.string.kelainan_urine), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xxvi. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 132) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxvii." + " " + context.getString(R.string.ginjal), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxvii." + " " + context.getString(R.string.ginjal), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xxvii. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 133) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxviii." + " " + context.getString(R.string.kelainan_kandung_kemih), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxviii." + " " + context.getString(R.string.kelainan_kandung_kemih), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xxviii. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 134) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxvix." + " " + context.getString(R.string.arthritis), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxvix." + " " + context.getString(R.string.arthritis), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xxvix. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 135) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxx." + " " + context.getString(R.string.hiv), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxx." + " " + context.getString(R.string.hiv), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xxx. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 136) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxxi." + " " + context.getString(R.string.penyakit_operasi_cidera), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("xxxi." + " " + context.getString(R.string.penyakit_operasi_cidera), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("xxxi. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 137) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("2." + " " + context.getString(R.string.data_kesehatan2), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("2." + " " + context.getString(R.string.data_kesehatan2), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("2. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 139) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("3." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3a), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("3." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3a), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("3a. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 140) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("3b." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3b), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("3b." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3b), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("3b. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 141) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("3c." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3c), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("3c." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3c), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("3c. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 142) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("3d." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3d), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("3d." + " " + context.getString(R.string.data_kesehatan3) + "\n" + context.getString(R.string.data_kesehatan3d), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("3d. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 143) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("4." + " " + context.getString(R.string.data_kesehatan4), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("4." + " " + context.getString(R.string.data_kesehatan4), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("4. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 144) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("5." + " " + context.getString(R.string.data_kesehatan5), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("5." + " " + context.getString(R.string.data_kesehatan5), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("5. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 145) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("6." + " " + context.getString(R.string.data_kesehatan6), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("6." + " " + context.getString(R.string.data_kesehatan6), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("6. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 146) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("7." + " " + context.getString(R.string.data_kesehatan7), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("7." + " " + context.getString(R.string.data_kesehatan7), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("7. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 147) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("7a." + " " + context.getString(R.string.data_kesehatan7a), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
                                document.add(new Paragraph("7a." + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("7a." + " " + context.getString(R.string.data_kesehatan7a), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                                document.add(new Paragraph("7a. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("7a. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 148) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("7b." + " " + context.getString(R.string.data_kesehatan7b), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
                                document.add(new Paragraph("7b. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("7b." + " " + context.getString(R.string.data_kesehatan7b), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                                document.add(new Paragraph("7b. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("7b. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 149) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("7c." + " " + context.getString(R.string.data_kesehatan7c), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
                                document.add(new Paragraph("7c. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("7c." + " " + context.getString(R.string.data_kesehatan7c), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                                document.add(new Paragraph("7c. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("7c. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 150) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("7d." + " " + context.getString(R.string.data_kesehatan7d), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
//                                document.add(new Paragraph(" : Ya" + "\n\n"));
                                document.add(new Paragraph("7d. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("7d." + " " + context.getString(R.string.data_kesehatan7d), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
//                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                                document.add(new Paragraph("7d. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("7d. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 152) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("8a." + " " + context.getString(R.string.data_kesehatan8) + "\n" + context.getString(R.string.data_kesehatan8a), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("8a." + " " + context.getString(R.string.data_kesehatan8) + "\n" + context.getString(R.string.data_kesehatan8a), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("8a. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 153) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("8b." + " " + context.getString(R.string.data_kesehatan8) + "\n" + context.getString(R.string.data_kesehatan8b), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("8b." + " " + context.getString(R.string.data_kesehatan8) + "\n" + context.getString(R.string.data_kesehatan8b), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("8b. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 154) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("8c." + " " + context.getString(R.string.data_kesehatan8) + "\n" + context.getString(R.string.data_kesehatan8c), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("8c." + " " + context.getString(R.string.data_kesehatan8) + "\n" + context.getString(R.string.data_kesehatan8c), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("8c. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 155) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("9." + " " + context.getString(R.string.data_kesehatan9) + "\n", normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("9." + " " + context.getString(R.string.data_kesehatan9) + "\n", normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 3) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung I : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 4) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung II : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 5) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung III : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("9. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    }
                }
            }

        }catch (DocumentException e){
            e.printStackTrace();
        }
        return QUkesehatan;
    }

    public static String QUsio (Context context, EspajModel me, Document document) {
        String QUsio = "";
        Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 14);
        Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 14,
                Font.BOLD);

        try{
            document.newPage();
            document.add(new Paragraph(context.getString(R.string.questionnaire) + "\n\n", normalFont));
            if (me.getModelID().getJns_spaj() == 4){
                List<ModelMst_Answer> listQuisioner = me.getModelUpdateQuisioner().getListQuisioner();
                for (int i = 0; i < listQuisioner.size(); i++) {

                    Integer question_id = listQuisioner.get(i).getQuestion_id();

                    if (question_id == 81) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_1 = new Phrase("1." + " " + context.getString(R.string.no1_sio), normalFont);
                                sio_1.setTabSettings(new TabSettings(250f));
                                sio_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(sio_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_1 = new Phrase("1." + " " + context.getString(R.string.no1_sio), normalFont);
                                sio_1.setTabSettings(new TabSettings(250f));
                                sio_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(sio_1);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("1. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 82){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_2 = new Phrase("2." + " " + context.getString(R.string.no2_sio), normalFont);
                                sio_2.setTabSettings(new TabSettings(250f));
                                sio_2.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(sio_2);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_2 = new Phrase("2." + " " + context.getString(R.string.no2_sio), normalFont);
                                sio_2.setTabSettings(new TabSettings(250f));
                                sio_2.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(sio_2);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("2. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 84){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3." + " " + context.getString(R.string.no3_sio), normalFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
                                sio_3.add(new Chunk("\n" + "3a." + " " +  context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3." + " " + context.getString(R.string.no3_sio), normalFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
                                sio_3.add(new Chunk("\n" + "3a." + " " +  context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("a. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 85){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3b." + " " + context.getString(R.string.no3b_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3b." + " " + context.getString(R.string.no3b_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("b. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 86){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3c." + " " + context.getString(R.string.no3c_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3c." + " " + context.getString(R.string.no3c_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("c. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 87){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3d." + " " + context.getString(R.string.no3d_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3d." + " " + context.getString(R.string.no3d_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("d. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    }  else if (question_id == 88){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3e." + " " + context.getString(R.string.no3e_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3e." + " " + context.getString(R.string.no3e_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("e. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 89){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3f." + " " + context.getString(R.string.no3f_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3f." + " " + context.getString(R.string.no3f_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("f. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 90){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3g." + " " + context.getString(R.string.no3g_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3g." + " " + context.getString(R.string.no3g_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("g. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 91){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3h." + " " + context.getString(R.string.no3h_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3h." + " " + context.getString(R.string.no3h_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("h. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 92){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3i." + " " + context.getString(R.string.no3i_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3i." + " " + context.getString(R.string.no3i_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("i. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 93){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3j." + " " + context.getString(R.string.no3j_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3j." + " " + context.getString(R.string.no3j_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("j. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 94){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3k." + " " + context.getString(R.string.no3k_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3k." + " " + context.getString(R.string.no3k_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("k. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 95){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3l." + " " + context.getString(R.string.no3l_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3l." + " " + context.getString(R.string.no3l_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("l. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 96){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3m." + " " + context.getString(R.string.no3m_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3m." + " " + context.getString(R.string.no3m_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("m. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 97){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3n." + " " + context.getString(R.string.no3n_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3n." + " " + context.getString(R.string.no3n_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("n. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    }  else if (question_id == 98){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3o." + " " + context.getString(R.string.no3o_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3o." + " " + context.getString(R.string.no3o_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("o. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 99){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3p." + " " + context.getString(R.string.no3p_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_3 = new Phrase("3p." + " " + context.getString(R.string.no3p_sio), boldFont);
                                sio_3.setTabSettings(new TabSettings(250f));
                                sio_3.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_3);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("p. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 100){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph(context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 101){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_4 = new Phrase("4." + " " + context.getString(R.string.no4_sio), normalFont);
                                sio_4.setTabSettings(new TabSettings(250f));
                                sio_4.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_4);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_4 = new Phrase("4." + " " + context.getString(R.string.no4_sio), normalFont);
                                sio_4.setTabSettings(new TabSettings(250f));
                                sio_4.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_4);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("4. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 102){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_5 = new Phrase("5." + " " + context.getString(R.string.no5_sio), normalFont);
                                sio_5.setTabSettings(new TabSettings(250f));
                                sio_5.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_5);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_5 = new Phrase("5." + " " + context.getString(R.string.no5_sio), normalFont);
                                sio_5.setTabSettings(new TabSettings(250f));
                                sio_5.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_5);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("5. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 103){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_6 = new Phrase("6." + " " + context.getString(R.string.no6_sio), normalFont);
                                sio_6.setTabSettings(new TabSettings(250f));
                                sio_6.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n" , boldFont));
                                document.add(sio_6);
                                document.add(new Paragraph("Calon Pemegang Polis : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase sio_6 = new Phrase("6." + " " + context.getString(R.string.no6_sio), normalFont);
                                sio_6.setTabSettings(new TabSettings(250f));
                                sio_6.add(Chunk.TABBING);
//                                sio_3.add(new Chunk("\n" + context.getString(R.string.no3a_sio) + "\n\n" , boldFont));
                                document.add(sio_6);
                                document.add(new Paragraph("Calon Pemegang Polis : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("Calon Tertanggung : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("6. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 104){
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 ) {
                                Phrase sio_7 = new Phrase("7." + " " + context.getString(R.string.no7_sio), normalFont);
                                sio_7.setTabSettings(new TabSettings(250f));
                                sio_7.add(Chunk.TABBING);
                                sio_7.add(new Chunk("\n" + "Calon Pemegang Polis " + "\n\n" , boldFont));
                                document.add(sio_7);
                                document.add(new Paragraph("Tinggi Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 ) {
                                document.add(new Paragraph("Berat Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 ) {
                                Phrase sio_7 = new Phrase("7." + " " + context.getString(R.string.no7_sio), normalFont);
                                sio_7.setTabSettings(new TabSettings(250f));
                                sio_7.add(Chunk.TABBING);
                                sio_7.add(new Chunk("\n" + "Calon Tertanggung " + "\n\n" , boldFont));
                                document.add(sio_7);
                                document.add(new Paragraph("Tinggi Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 ) {
                                document.add(new Paragraph("Berat Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("7. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    }
                }
            }
        }catch (DocumentException e){
            e.printStackTrace();
        }
        return QUsio;

    }

    public static String QU (Context context, EspajModel me, Document document) {
        String QU = "";
        Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 14);
        Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 14,
                Font.BOLD);
        try{
            document.newPage();
            document.add(new Paragraph(context.getString(R.string.questionnaire) + "\n\n", normalFont));
            if (me.getModelID().getJns_spaj() == 3) {
                List<ModelMst_Answer> listQuisioner = me.getModelUpdateQuisioner().getListQuisioner();
                for (int i = 0; i < listQuisioner.size(); i++) {

                    Integer question_id = listQuisioner.get(i).getQuestion_id();

                    if (question_id == 1) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("1." + " " + context.getString(R.string.qu1), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_1 = new Phrase("1." + " " + context.getString(R.string.qu1), normalFont);
                                qu_1.setTabSettings(new TabSettings(250f));
                                qu_1.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_1);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("1. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 2) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_2 = new Phrase("2." + " " + context.getString(R.string.qu2), normalFont);
                                qu_2.setTabSettings(new TabSettings(250f));
                                qu_2.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_2);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_2 = new Phrase("2." + " " + context.getString(R.string.qu2), normalFont);
                                qu_2.setTabSettings(new TabSettings(250f));
                                qu_2.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_2);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("2. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 3) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_3 = new Phrase("3." + " " + context.getString(R.string.qu3), normalFont);
                                qu_3.setTabSettings(new TabSettings(250f));
                                qu_3.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_3);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_3 = new Phrase("3." + " " + context.getString(R.string.qu3), normalFont);
                                qu_3.setTabSettings(new TabSettings(250f));
                                qu_3.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_3);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("3. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 4) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_4 = new Phrase("4." + " " + context.getString(R.string.qu4), normalFont);
                                qu_4.setTabSettings(new TabSettings(250f));
                                qu_4.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_4);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_4 = new Phrase("4." + " " + context.getString(R.string.qu4), normalFont);
                                qu_4.setTabSettings(new TabSettings(250f));
                                qu_4.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_4);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph("g : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("4. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 5) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_5 = new Phrase("5." + " " + context.getString(R.string.qu5), normalFont);
                                qu_5.setTabSettings(new TabSettings(250f));
                                qu_5.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_5);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_5 = new Phrase("5." + " " + context.getString(R.string.qu5), normalFont);
                                qu_5.setTabSettings(new TabSettings(250f));
                                qu_5.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_5);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("5. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 6) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_6 = new Phrase("6." + " " + context.getString(R.string.qu6), normalFont);
                                qu_6.setTabSettings(new TabSettings(250f));
                                qu_6.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_6);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_6 = new Phrase("6." + " " + context.getString(R.string.qu6), normalFont);
                                qu_6.setTabSettings(new TabSettings(250f));
                                qu_6.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_6);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("6. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 7) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_7 = new Phrase("7." + " " + context.getString(R.string.qu7), normalFont);
                                qu_7.setTabSettings(new TabSettings(250f));
                                qu_7.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_7);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_7 = new Phrase("7." + " " + context.getString(R.string.qu7), normalFont);
                                qu_7.setTabSettings(new TabSettings(250f));
                                qu_7.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_7);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("7. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 8) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_8 = new Phrase("8." + " " + context.getString(R.string.qu8), normalFont);
                                qu_8.setTabSettings(new TabSettings(250f));
                                qu_8.add(Chunk.TABBING);
//                                qu_8.add(new Chunk(" : Ya" + "\n\n", boldFont));
                                document.add(qu_8);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_8 = new Phrase("8." + " " + context.getString(R.string.qu8), normalFont);
                                qu_8.setTabSettings(new TabSettings(250f));
                                qu_8.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_8);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("8. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 9) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_8a = new Phrase("a." + " " + context.getString(R.string.qua), normalFont);
                                qu_8a.setTabSettings(new TabSettings(250f));
                                qu_8a.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_8a);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_8a = new Phrase("a." + " " + context.getString(R.string.qua), normalFont);
                                qu_8a.setTabSettings(new TabSettings(250f));
                                qu_8a.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_8a);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("8a. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 10) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_8b = new Phrase("b." + " " + context.getString(R.string.qub), normalFont);
                                qu_8b.setTabSettings(new TabSettings(250f));
                                qu_8b.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_8b);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_8b = new Phrase("b." + " " + context.getString(R.string.qub), normalFont);
                                qu_8b.setTabSettings(new TabSettings(250f));
                                qu_8b.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_8b);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("8b. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 11) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_9 = new Phrase("9." + " " + context.getString(R.string.qu9), normalFont);
                                qu_9.setTabSettings(new TabSettings(250f));
                                qu_9.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_9);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_9 = new Phrase("9." + " " + context.getString(R.string.qu9), normalFont);
                                qu_9.setTabSettings(new TabSettings(250f));
                                qu_9.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_9);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("9. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 12) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_10 = new Phrase("10." + " " + context.getString(R.string.qu10), normalFont);
                                qu_10.setTabSettings(new TabSettings(250f));
                                qu_10.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_10);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_10 = new Phrase("10." + " " + context.getString(R.string.qu10), normalFont);
                                qu_10.setTabSettings(new TabSettings(250f));
                                qu_10.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_10);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("10. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 13) {
                        if (listQuisioner.get(i).getOption_group() == 0) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1) {
                                Phrase qu_11 = new Phrase("11a." + " " + context.getString(R.string.qu11a), normalFont);
                                qu_11.setTabSettings(new TabSettings(250f));
                                qu_11.add(Chunk.TABBING);
//                                qu_11.add(new Chunk("\n" + " " + "\n\n" , boldFont));
                                document.add(qu_11);
                                document.add(new Paragraph("\n" + "Tinggi Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2) {
                                document.add(new Paragraph("Berat Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("11a. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 14) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_11b = new Phrase("11b." + " " + context.getString(R.string.qu11b), normalFont);
                                qu_11b.setTabSettings(new TabSettings(250f));
                                qu_11b.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_11b);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_11b = new Phrase("11b." + " " + context.getString(R.string.qu11b), normalFont);
                                qu_11b.setTabSettings(new TabSettings(250f));
                                qu_11b.add(Chunk.TABBING);
//                                sio_1.add(new Chunk("Calon Pemegang Polis : Ya" + "\n\n" , boldFont));
                                document.add(qu_11b);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("11b. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 15) {
                        if (listQuisioner.get(i).getOption_group() == 0) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1) {
                                Phrase qu_12 = new Phrase("12." + " " + context.getString(R.string.qu12), normalFont);
                                qu_12.setTabSettings(new TabSettings(250f));
                                qu_12.add(Chunk.TABBING);
//                                qu_11.add(new Chunk("\n" + " " + "\n\n" , boldFont));
                                document.add(qu_12);
                                document.add(new Paragraph("\n" + "Tinggi Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2) {
                                document.add(new Paragraph("Berat Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("12. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 16) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_13 = new Phrase("13." + " " + context.getString(R.string.qu13), normalFont);
                                qu_13.setTabSettings(new TabSettings(250f));
                                qu_13.add(Chunk.TABBING);
                                qu_13.add(new Chunk("\n" + context.getString(R.string.qu13a) + "\n\n", normalFont));
                                document.add(qu_13);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_13 = new Phrase("13." + " " + context.getString(R.string.qu13), normalFont);
                                qu_13.setTabSettings(new TabSettings(250f));
                                qu_13.add(Chunk.TABBING);
//                                qu_13.add(new Chunk("\n" + context.getString(R.string.qu13a) + "\n\n" , normalFont));
                                document.add(qu_13);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1) {
//                                Phrase qu_11 = new Phrase("13." + " " + context.getString(R.string.qu11a), normalFont);
//                                qu_11.setTabSettings(new TabSettings(250f));
//                                qu_11.add(Chunk.TABBING);
////                                qu_11.add(new Chunk("\n" + " " + "\n\n" , boldFont));
//                                document.add(qu_11);
                                document.add(new Paragraph("\n" + "13. " + context.getString(R.string.keterangan) + "Tidak" + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                            } else if (listQuisioner.get(i).getOption_order() == 2) {
                                document.add(new Paragraph("13. " + context.getString(R.string.keterangan) + "Ya" + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("13. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 17) {
                        if (listQuisioner.get(i).getOption_group() == 1) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_14 = new Phrase("14." + " " + context.getString(R.string.qu14), normalFont);
                                qu_14.setTabSettings(new TabSettings(250f));
                                qu_14.add(Chunk.TABBING);
                                qu_14.add(new Chunk("\n" + context.getString(R.string.qu14a) + "\n\n", normalFont));
                                document.add(qu_14);
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                Phrase qu_14 = new Phrase("14." + " " + context.getString(R.string.qu14), normalFont);
                                qu_14.setTabSettings(new TabSettings(250f));
                                qu_14.add(Chunk.TABBING);
//                                qu_14.add(new Chunk("\n" + context.getString(R.string.qu14a) + "\n\n" , normalFont));
                                document.add(qu_14);
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 2) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {

                            } else if (listQuisioner.get(i).getOption_order() == 1 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Ya" + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2 && listQuisioner.get(i).getAnswer().equals("1")) {
                                document.add(new Paragraph(" : Tidak" + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1) {
//                                Phrase qu_11 = new Phrase("9." + " " + context.getString(R.string.qu11a), normalFont);
//                                qu_11.setTabSettings(new TabSettings(250f));
//                                qu_11.add(Chunk.TABBING);
////                                qu_11.add(new Chunk("\n" + " " + "\n\n" , boldFont));
//                                document.add(qu_11);
                                document.add(new Paragraph("\n" + "14. " + context.getString(R.string.keterangan) + "Tidak" + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                            } else if (listQuisioner.get(i).getOption_order() == 2) {
                                document.add(new Paragraph("14. " + context.getString(R.string.keterangan) + "Ya" + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("14. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    } else if (question_id == 18) {
                        if (listQuisioner.get(i).getOption_group() == 0) {
                            if (listQuisioner.get(i).getAnswer().equals("0")) {
                            } else if (listQuisioner.get(i).getOption_order() == 1) {
                                Phrase qu_11 = new Phrase("15." + " " + context.getString(R.string.qu15), normalFont);
                                qu_11.setTabSettings(new TabSettings(250f));
                                qu_11.add(Chunk.TABBING);
//                                qu_11.add(new Chunk("\n" + " " + "\n\n" , boldFont));
                                document.add(qu_11);
                                document.add(new Paragraph("\n" + "Tinggi Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
                            } else if (listQuisioner.get(i).getOption_order() == 2) {
                                document.add(new Paragraph("Berat Badan : " + listQuisioner.get(i).getAnswer() + "\n\n"));
                            }
                        } else if (listQuisioner.get(i).getOption_group() == 0) {
                            document.add(new Paragraph("15. " + context.getString(R.string.keterangan) + " : " + listQuisioner.get(i).getAnswer() + "\n\n", normalFont));
                        }
                    }
                }
            }
        }catch (DocumentException e){
            e.printStackTrace();
        }


        return QU;
    }

    public static String PR (Context context, EspajModel me, Document document) {
        String PR = "";
        Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 14);
        Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 14,
                Font.BOLD);
        try{
            if (new E_Select(context).selectLinkNonLink(me.getUsulanAsuransiModel().getKd_produk_ua()) == 17) {
                document.newPage();
                document.add(new Paragraph(context.getString(R.string.profil_nasabah) + "\n\n", normalFont));

                if (me.getProfileResikoModel().getNilai_no1_pr() == 2) {
                    document.add(new Paragraph(context.getString(R.string.no1_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no2_1pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no1_pr() == 4) {
                    document.add(new Paragraph(context.getString(R.string.no1_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no3_1pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no1_pr() == 6) {
                    document.add(new Paragraph(context.getString(R.string.no1_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no4_1pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no1_pr() == 8) {
                    document.add(new Paragraph(context.getString(R.string.no1_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no5_1pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no1_pr() == 10) {
                    document.add(new Paragraph(context.getString(R.string.no1_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no6_1pr), boldFont));
                } else {
//                jsonpr.put("answer_id", 0);
                }

                if (me.getProfileResikoModel().getNilai_no7_pr() == 2) {
                    document.add(new Paragraph(context.getString(R.string.no7_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no8_7pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no7_pr() == 4) {
                    document.add(new Paragraph(context.getString(R.string.no7_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no9_7pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no7_pr() == 6) {
                    document.add(new Paragraph(context.getString(R.string.no7_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no10_7pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no7_pr() == 8) {
                    document.add(new Paragraph(context.getString(R.string.no7_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no11_7pr), boldFont));
                } else {
//                jsonpr.put("answer_id", 0);
                }

                if (me.getProfileResikoModel().getNilai_no12_pr() == 2) {
                    document.add(new Paragraph(context.getString(R.string.no12_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no13_12pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no12_pr() == 4) {
                    document.add(new Paragraph(context.getString(R.string.no12_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no14_12pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no12_pr() == 6) {
                    document.add(new Paragraph(context.getString(R.string.no12_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no15_12pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no12_pr() == 8) {
                    document.add(new Paragraph(context.getString(R.string.no12_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no16_12pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no12_pr() == 10) {
                    document.add(new Paragraph(context.getString(R.string.no12_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no17_12pr), boldFont));
                } else {
//                jsonpr.put("answer_id", 0);
                }

                if (me.getProfileResikoModel().getNilai_no18_pr() == 2) {
                    document.add(new Paragraph(context.getString(R.string.no18_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no19_18pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no18_pr() == 4) {
                    document.add(new Paragraph(context.getString(R.string.no18_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no20_18pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no18_pr() == 6) {
                    document.add(new Paragraph(context.getString(R.string.no18_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no21_18pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no18_pr() == 8) {
                    document.add(new Paragraph(context.getString(R.string.no18_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no22_18pr), boldFont));
                } else {
//                jsonpr.put("answer_id", 0);
                }

                if (me.getProfileResikoModel().getNilai_no23_pr() == 2) {
                    document.add(new Paragraph(context.getString(R.string.no23_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no24_23pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no23_pr() == 4) {
                    document.add(new Paragraph(context.getString(R.string.no23_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no25_23pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no23_pr() == 6) {
                    document.add(new Paragraph(context.getString(R.string.no23_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no26_23pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no23_pr() == 8) {
                    document.add(new Paragraph(context.getString(R.string.no23_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no27_23pr), boldFont));
                } else {
//                jsonpr.put("answer_id", 0);
                }

                if (me.getProfileResikoModel().getNilai_no28_pr() == 2) {
                    document.add(new Paragraph(context.getString(R.string.no28_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no29_28pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no28_pr() == 4) {
                    document.add(new Paragraph(context.getString(R.string.no28_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no30_28pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no28_pr() == 6) {
                    document.add(new Paragraph(context.getString(R.string.no28_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no31_28pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no28_pr() == 8) {
                    document.add(new Paragraph(context.getString(R.string.no28_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no32_28pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no28_pr() == 10) {
                    document.add(new Paragraph(context.getString(R.string.no28_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no33_28pr), boldFont));
                } else {
//                jsonpr.put("answer_id", 0);
                }

                if (me.getProfileResikoModel().getNilai_no34_pr() == 2) {
                    document.add(new Paragraph(context.getString(R.string.no34_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no35_34pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no34_pr() == 4) {
                    document.add(new Paragraph(context.getString(R.string.no34_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no36_34pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no34_pr() == 6) {
                    document.add(new Paragraph(context.getString(R.string.no34_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no37_34pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no34_pr() == 8) {
                    document.add(new Paragraph(context.getString(R.string.no34_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no38_34pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no34_pr() == 10) {
                    document.add(new Paragraph(context.getString(R.string.no34_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no39_34pr), boldFont));
                } else {
//                jsonpr.put("answer_id", 0);
                }

                if (me.getProfileResikoModel().getNilai_no40_pr() == 2) {
                    document.add(new Paragraph(context.getString(R.string.no40_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no41_40pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no40_pr() == 4) {
                    document.add(new Paragraph(context.getString(R.string.no40_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no42_40pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no40_pr() == 6) {
                    document.add(new Paragraph(context.getString(R.string.no40_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no43_40pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no40_pr() == 8) {
                    document.add(new Paragraph(context.getString(R.string.no40_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no44_40pr), boldFont));
                } else {
//                jsonpr.put("answer_id", 0);
                }

                if (me.getProfileResikoModel().getNilai_no45_pr() == 2) {
                    document.add(new Paragraph(context.getString(R.string.no45_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no46_45pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no45_pr() == 4) {
                    document.add(new Paragraph(context.getString(R.string.no45_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no47_45pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no45_pr() == 6) {
                    document.add(new Paragraph(context.getString(R.string.no45_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no48_45pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no45_pr() == 8) {
                    document.add(new Paragraph(context.getString(R.string.no45_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no49_45pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no45_pr() == 10) {
                    document.add(new Paragraph(context.getString(R.string.no45_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no50_45pr), boldFont));
                } else {
//                jsonpr.put("answer_id", 0);
                }

                if (me.getProfileResikoModel().getNilai_no51_pr() == 2) {
                    document.add(new Paragraph(context.getString(R.string.no51_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no52_51pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no51_pr() == 4) {
                    document.add(new Paragraph(context.getString(R.string.no51_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no53_51pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no51_pr() == 6) {
                    document.add(new Paragraph(context.getString(R.string.no51_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no54_51pr), boldFont));
                } else if (me.getProfileResikoModel().getNilai_no51_pr() == 8) {
                    document.add(new Paragraph(context.getString(R.string.no51_pr)));
                    document.add(new Paragraph("-    " + context.getString(R.string.no55_51pr), boldFont));
                } else {
//                jsonpr.put("answer_id", 0);
                }
            }
        }catch (DocumentException e){
            e.printStackTrace();
        }


        return PR;
    }
}
