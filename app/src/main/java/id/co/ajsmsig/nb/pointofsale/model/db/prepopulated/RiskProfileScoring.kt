package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by andreyyoshuamanik on 02/03/18.
 */
@Entity
class RiskProfileScoring(
        @PrimaryKey
        val id: Int,
        var type: String?,
        var startRange: Int?,
        var endRange: Int?,
        var description: String?,
        var imageName: String?
)