package id.co.ajsmsig.nb.espaj.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.AutoCompleteTextViewClickListener;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.method.ProgressDialogClass;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelCabangDa;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelReferralDa;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;
import id.co.ajsmsig.nb.util.Const;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Bernard on 29/05/2017.
 */

public class E_DetilAgenFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, AdapterView.OnItemClickListener, CompoundButton.OnCheckedChangeListener {


    public E_DetilAgenFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        SharedPreferences settings = getActivity().getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        JENIS_LOGIN = String.valueOf(settings.getInt("JENIS_LOGIN", 0));
        JENIS_LOGIN_BC = String.valueOf(settings.getInt("JENIS_LOGIN_BC", 2));
        NAMA_AGEN = settings.getString("NAMA_AGEN", "");
        KODE_AGEN = settings.getString("KODE_AGEN", "");
        NAMA_REG = settings.getString("NAMA_REG", "");
        KODE_LEADER = settings.getString("KODE_LEADER", "");
        KODE_REG1 = settings.getString("KODE_REG1", "");
        KODE_REG2 = settings.getString("KODE_REG2", "");
        KODE_REG3 = settings.getString("KODE_REG3", "");
        GROUPID = settings.getInt("GROUP_ID", 0);
        KODE_REGIONAL = KODE_REG1 + KODE_REG2 + KODE_REG3;
        EMAIL = settings.getString("EMAIL", null);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem menu_validasi = menu.findItem(R.id.menu_validasi);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.e_fragment_detil_agen_layout, container,
                false);
        ButterKnife.bind(this, view);
        me = E_MainActivity.e_bundle.getParcelable(getString(R.string.modelespaj));
        ((E_MainActivity) getActivity()).setSecondToolbar("Detil Agen (6/", 6);

        //autocompletetextview

        ac_pnytaan_da.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_pnytaan_da, this));
        ac_pnytaan_da.setOnClickListener(this);
        ac_pnytaan_da.setOnFocusChangeListener(this);
        ac_pnytaan_da.addTextChangedListener(new generalTextWatcher(ac_pnytaan_da));

        ac_nama_regional_penagihan_da.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_nama_regional_penagihan_da, this));
        ac_nama_regional_penagihan_da.setOnClickListener(this);
        ac_nama_regional_penagihan_da.setOnFocusChangeListener(this);
        ac_nama_regional_penagihan_da.addTextChangedListener(new generalTextWatcher(ac_nama_regional_penagihan_da));

        ac_bank_da.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_bank_da, this));
        ac_bank_da.setOnClickListener(this);
        ac_bank_da.setOnFocusChangeListener(this);
        ac_bank_da.addTextChangedListener(new generalTextWatcher(ac_bank_da));

        ac_status_da.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_status_da, this));
        ac_status_da.setOnClickListener(this);
        ac_status_da.setOnFocusChangeListener(this);
        ac_status_da.addTextChangedListener(new generalTextWatcher(ac_status_da));

        ac_nama_perusahaan_da.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_nama_perusahaan_da, this));
        ac_nama_perusahaan_da.setOnClickListener(this);
        ac_nama_perusahaan_da.setOnFocusChangeListener(this);
        ac_nama_perusahaan_da.addTextChangedListener(new generalTextWatcher(ac_nama_perusahaan_da));

        ac_khusus_mnc2_da.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_khusus_mnc2_da, this));
        ac_khusus_mnc2_da.setOnClickListener(this);
        ac_khusus_mnc2_da.setOnFocusChangeListener(this);
        ac_khusus_mnc2_da.addTextChangedListener(new generalTextWatcher(ac_khusus_mnc2_da));

        ac_nm_referral_da.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_nm_referral_da, this));
        ac_nm_referral_da.setOnClickListener(this);
        ac_nm_referral_da.setOnFocusChangeListener(this);
        ac_nm_referral_da.addTextChangedListener(new generalTextWatcher(ac_nm_referral_da));

        ac_nm_cabang_da.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_nm_cabang_da, this));
        ac_nm_cabang_da.setOnClickListener(this);
        ac_nm_cabang_da.setOnFocusChangeListener(this);
        ac_nm_cabang_da.addTextChangedListener(new generalTextWatcher(ac_nm_cabang_da));

        ac_nm_pnutup_da.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_nm_pnutup_da, this));
        ac_nm_pnutup_da.setOnClickListener(this);
        ac_nm_pnutup_da.setOnFocusChangeListener(this);
        ac_nm_pnutup_da.addTextChangedListener(new generalTextWatcher(ac_nm_pnutup_da));

        //button
//        prev_btn_da.setOnClickListener(this);
//        next_btn_da.setOnClickListener(this);

        //edittext

        et_tgl_spaj_da.addTextChangedListener(new generalTextWatcher(et_tgl_spaj_da));
        et_kd_regional_da.addTextChangedListener(new generalTextWatcher(et_kd_regional_da));
        et_nm_regional_da.addTextChangedListener(new generalTextWatcher(et_nm_regional_da));
        et_kd_penutup_da.addTextChangedListener(new generalTextWatcher(et_kd_penutup_da));
        et_kd_leader_da.addTextChangedListener(new generalTextWatcher(et_kd_leader_da));
        et_kd_ao_txt_da.addTextChangedListener(new generalTextWatcher(et_kd_ao_txt_da));
        et_email_da.addTextChangedListener(new generalTextWatcher(et_email_da));
        et_kode_regional_penagihan_da.addTextChangedListener(new generalTextWatcher(et_kode_regional_penagihan_da));
        et_nama_broker_da.addTextChangedListener(new generalTextWatcher(et_nama_broker_da));
        et_cari_bank_da.addTextChangedListener(new generalTextWatcher(et_cari_bank_da));
        et_no_rek_broker_da.addTextChangedListener(new generalTextWatcher(et_no_rek_broker_da));
        et_nik_msig_da.addTextChangedListener(new generalTextWatcher(et_nik_msig_da));
        et_nama_karyawan_da.addTextChangedListener(new generalTextWatcher(et_nama_karyawan_da));
        et_cabang_da.addTextChangedListener(new generalTextWatcher(et_cabang_da));
        et_departement_da.addTextChangedListener(new generalTextWatcher(et_departement_da));
        et_premi_ke_da.addTextChangedListener(new generalTextWatcher(et_premi_ke_da));
        et_potongan_da.addTextChangedListener(new generalTextWatcher(et_potongan_da));
        et_tanggal_proses_da.addTextChangedListener(new generalTextWatcher(et_tanggal_proses_da));
        et_cari_bank2_da.addTextChangedListener(new generalTextWatcher(et_cari_bank2_da));
        et_nik_karyawan_da.addTextChangedListener(new generalTextWatcher(et_nik_karyawan_da));
        et_id_refferal_da.addTextChangedListener(new generalTextWatcher(et_id_refferal_da));
        et_id_sponsor_da.addTextChangedListener(new generalTextWatcher(et_id_sponsor_da));
        et_id_member_da.addTextChangedListener(new generalTextWatcher(et_id_member_da));
        et_id_penempatan_da.addTextChangedListener(new generalTextWatcher(et_id_penempatan_da));

        //checkbox
        cb_tutupan_pribadi_da.setOnCheckedChangeListener(this);
        cb_broker_da.setOnCheckedChangeListener(this);
        //button di Activity
        btn_lanjut = getActivity().findViewById(R.id.btn_lanjut);
        btn_lanjut.setOnClickListener(this);
        btn_kembali = getActivity().findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(this);
        second_toolbar = ((E_MainActivity) getActivity()).getSecond_toolbar();
        iv_save = second_toolbar.findViewById(R.id.iv_save);
        iv_save.setOnClickListener(this);
        iv_next = second_toolbar.findViewById(R.id.iv_next);
        iv_next.setOnClickListener(this);
        iv_prev = second_toolbar.findViewById(R.id.iv_prev);
        iv_prev.setOnClickListener(this);
        restore();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        ProgressDialogClass.removeSimpleProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        MethodSupport.active_view(1, btn_lanjut);
        MethodSupport.active_view(1, iv_next);
        MethodSupport.active_view(1, btn_kembali);
        MethodSupport.active_view(1, iv_prev);
        ProgressDialogClass.removeSimpleProgressDialog();
        setAdapterPernyataan();
        setAdapterNamaReferral();
        setAdapterNamaReferralAAJI();
        setAdapterCabang();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_validasi:
                me.getDetilAgenModel().setValidation(Validation());
                loadview();
                MyTaskValidasi taskValidasi = new MyTaskValidasi();
                taskValidasi.execute();
                Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTaskValidasi extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_DETIL_AGEN);
            return null;
        }
    }

    private class MyTaskSavePage extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_DETIL_AGEN);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
//View di activity
            case R.id.btn_lanjut:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_next:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_save:
                loadview();
                MyTaskSavePage taskSavePage = new MyTaskSavePage();
                taskSavePage.execute();

                break;
            case R.id.iv_prev:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.btn_kembali:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            //drop down
            case R.id.ac_pnytaan_da:
                ac_pnytaan_da.showDropDown();
                break;
            case R.id.ac_nama_regional_penagihan_da:
                ac_nama_regional_penagihan_da.showDropDown();
                break;
            case R.id.ac_bank_da:
                ac_bank_da.showDropDown();
                break;
            case R.id.ac_nm_referral_da:
                ac_nm_referral_da.showDropDown();
                break;
            case R.id.ac_nm_cabang_da:
                ac_nm_cabang_da.showDropDown();
                break;
            case R.id.ac_status_da:
                ac_status_da.showDropDown();
                break;
            case R.id.ac_nama_perusahaan_da:
                ac_nama_perusahaan_da.showDropDown();
                break;
            case R.id.ac_khusus_mnc2_da:
                ac_khusus_mnc2_da.showDropDown();
                break;
            case R.id.ac_nm_pnutup_da:
                ac_nm_pnutup_da.showDropDown();
                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.ac_pnytaan_da:
                if (hasFocus) ac_pnytaan_da.showDropDown();
                break;
            case R.id.ac_nama_regional_penagihan_da:
                if (hasFocus) ac_nama_regional_penagihan_da.showDropDown();
                break;
            case R.id.ac_bank_da:
                if (hasFocus) ac_bank_da.showDropDown();
                break;
            case R.id.ac_nm_referral_da:
                if (hasFocus) ac_nm_referral_da.showDropDown();
                break;
            case R.id.ac_nm_cabang_da:
                if (hasFocus) ac_nm_cabang_da.showDropDown();
                break;
            case R.id.ac_status_da:
                if (hasFocus) ac_status_da.showDropDown();
                break;
            case R.id.ac_nama_perusahaan_da:
                if (hasFocus) ac_nama_perusahaan_da.showDropDown();
                break;
            case R.id.ac_khusus_mnc2_da:
                if (hasFocus) ac_khusus_mnc2_da.showDropDown();
                break;
            case R.id.ac_nm_pnutup_da:
                if (hasFocus) ac_nm_pnutup_da.showDropDown();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.ac_pnytaan_da:
//                ModelDropdownInt modelPernyataan = (ModelDropdownInt) parent.getAdapter().getItem(position);
//                pnytaan = modelPernyataan.getId();
                break;
            case R.id.ac_nama_regional_penagihan_da:
                ModelDropDownString modelNama_regional_penagihan = (ModelDropDownString) parent.getAdapter().getItem(position);
                Nm_Regional_Penagihan = modelNama_regional_penagihan.getId();
                break;
            case R.id.ac_bank_da:
                ModelDropdownInt modelBank = (ModelDropdownInt) parent.getAdapter().getItem(position);
                bank = modelBank.getId();
                break;
            case R.id.ac_status_da:
                ModelDropdownInt modelStatus = (ModelDropdownInt) parent.getAdapter().getItem(position);
                status = modelStatus.getId();
                break;
            case R.id.ac_nama_perusahaan_da:
                ModelDropDownString modelNama_perusahaan = (ModelDropDownString) parent.getAdapter().getItem(position);
                nm_prshaan = modelNama_perusahaan.getId();
                break;
            case R.id.ac_khusus_mnc2_da:
                ModelDropdownInt modelKhusus_mnc = (ModelDropdownInt) parent.getAdapter().getItem(position);
                khusus_mnc = modelKhusus_mnc.getId();
                break;
            case R.id.ac_nm_referral_da:
                ModelReferralDa modelReferralDa = (ModelReferralDa) parent.getAdapter().getItem(position);
//                if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 44) {
//                    et_id_refferal_da.setText("000000");
//                } else if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 51) {
//                    et_id_refferal_da.setText("014337");
//                } else if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 50) {
//                    et_id_refferal_da.setText("902004");
//                } else {//non Aktifin kalau data asli
                et_id_refferal_da.setText(modelReferralDa.getAGENT_CODE());
//                }
                ac_nm_cabang_da.setText(modelReferralDa.getNAMA_CABANG());
                id_cabang_da = modelReferralDa.getLCB_NO();
                break;
            case R.id.ac_nm_cabang_da:
                ModelCabangDa modelCabRef = (ModelCabangDa) parent.getAdapter().getItem(position);
                id_cabang_da = modelCabRef.getLCB_NO();
                break;
            case R.id.ac_nm_pnutup_da:
                ModelReferralDa modelReferralDaAAJI = (ModelReferralDa) parent.getAdapter().getItem(position);
                et_kd_penutup_da.setText(modelReferralDaAAJI.getAGENT_CODE());
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_tutupan_pribadi_da:
                cb_tutupan_pribadi = (isChecked) ? cb_tutupan_pribadi_da.getText().toString() : "";
                break;
            case R.id.cb_broker_da:
                cb_broker = (isChecked) ? cb_broker_da.getText().toString() : "";
                break;

        }
    }

    private class generalTextWatcher implements TextWatcher {
        private View view;

        generalTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                //autocompletetextview
                case R.id.ac_pnytaan_da:
                    if (StaticMethods.isTextWidgetEmpty(ac_pnytaan_da)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_pnytaan_da.setColorFilter(color);
                    tv_error_pnytaan_da.setVisibility(View.GONE);
                    break;
                case R.id.ac_nama_regional_penagihan_da:
                    if (StaticMethods.isTextWidgetEmpty(ac_nama_regional_penagihan_da)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
//                    iv_circle_nama_regional_penagihan_da.setColorFilter(color);
//                    tv_error_nama_regional_penagihan_da.setVisibility(View.GONE);
                    break;
                case R.id.ac_bank_da:
                    if (StaticMethods.isTextWidgetEmpty(ac_bank_da)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
//                    iv_circle_bank_da.setColorFilter(color);
//                    tv_error_bank_da.setVisibility(View.GONE);
                    break;
                case R.id.ac_status_da:
                    if (StaticMethods.isTextWidgetEmpty(ac_status_da)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_status_da.setColorFilter(color);
                    tv_error_status_da.setVisibility(View.GONE);
                    break;
                case R.id.ac_nama_perusahaan_da:
                    if (StaticMethods.isTextWidgetEmpty(ac_nama_perusahaan_da)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_nama_perusahaan_da.setColorFilter(color);
                    tv_error_nama_perusahaan_da.setVisibility(View.GONE);
                    break;
                case R.id.ac_khusus_mnc2_da:
                    if (StaticMethods.isTextWidgetEmpty(ac_khusus_mnc2_da)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_khusus_mnc2_da.setColorFilter(color);
                    tv_error_khusus_mnc2_da.setVisibility(View.GONE);
                    break;

                //editText
                case R.id.et_tgl_spaj_da:
                    Method_Validator.ValEditWatcher(et_tgl_spaj_da, iv_circle_tgl_spaj_da,
                            tv_error_tgl_spaj_da, getContext());
                    break;
                case R.id.et_kd_regional_da:
                    Method_Validator.ValEditWatcher(et_kd_regional_da, iv_circle_kd_regional_da,
                            tv_error_kd_regional_da, getContext());
                    break;
                case R.id.et_nm_regional_da:
                    Method_Validator.ValEditWatcherNonRegex(et_nm_regional_da, iv_circle_nm_regional_da,
                            tv_error_nm_regional_da, getContext());
                    break;
                case R.id.et_kd_penutup_da:
                    Method_Validator.ValEditWatcherNonRegex(et_kd_penutup_da, iv_circle_kd_penutup_da,
                            tv_error_kd_penutup_da, getContext());
                    break;
                case R.id.et_nik_msig_da:
                    Method_Validator.ValEditWatcher(et_nik_msig_da, iv_circle_nik_msig_da,
                            tv_error_nik_msig_da, getContext());
                    break;
                case R.id.et_nama_karyawan_da:
                    Method_Validator.ValEditWatcher(et_nama_karyawan_da, iv_circle_nama_karyawan_da,
                            tv_error_nama_karyawan_da, getContext());
                    break;
                case R.id.et_cabang_da:
                    Method_Validator.ValEditWatcher(et_cabang_da, iv_circle_cabang_da,
                            tv_error_cabang_da, getContext());
                    break;
                case R.id.et_nik_karyawan_da:
                    Method_Validator.ValEditWatcher(et_nik_karyawan_da, iv_circle_nik_karyawan_da,
                            tv_error_nik_karyawan_da, getContext());
                    break;
                case R.id.ac_nm_referral_da:
                    Method_Validator.ValEditWatcher(ac_nm_referral_da, iv_circle_nm_refferal_da,
                            tv_error_nm_refferal_da, getContext());

                    break;
                case R.id.ac_nm_cabang_da:
//                    Method_Validator.ValEditWatcher(ac_nm_cabang_da, iv_circle_nm_cabang_da,
//                            tv_error_nm_cabang_da, getContext());
                    break;
                case R.id.ac_nm_pnutup_da:
                    Method_Validator.ValEditWatcher(ac_nm_pnutup_da, iv_circle_nm_pnutup_da,
                            tv_error_nm_pnutup_da, getContext());
                    break;
                case R.id.et_id_refferal_da:
                    Method_Validator.ValEditWatcher(et_id_refferal_da, iv_circle_id_refferal_da,
                            tv_error_id_refferal_da, getContext());
                    break;
                case R.id.et_id_member_da:
                    Method_Validator.ValEditWatcher(et_id_member_da, iv_circle_id_member_da,
                            tv_error_id_member_da, getContext());
                    break;
                case R.id.et_id_penempatan_da:
                    Method_Validator.ValEditWatcher(et_id_penempatan_da, iv_circle_id_penempatan_da,
                            tv_error_id_penempatan_da, getContext());
                    break;
                case R.id.et_id_sponsor_da:
                    Method_Validator.ValEditWatcher(et_id_sponsor_da, iv_circle_sponsor_da,
                            tv_error_sponsor_da, getContext());
                    break;
            }
        }
    }

    private void loadview() {
        me.getDetilAgenModel().setTgl_spaj_da(et_tgl_spaj_da.getText().toString());
        me.getDetilAgenModel().setNm_regional_da(et_nm_regional_da.getText().toString());
        me.getDetilAgenModel().setKd_regional_da(et_kd_regional_da.getText().toString());
        me.getDetilAgenModel().setKd_leader_da(et_kd_leader_da.getText().toString());
        me.getDetilAgenModel().setNmlead_pnutup_da(nmlead_pnutup_da);
        me.getDetilAgenModel().setKd_penutup_da(et_kd_penutup_da.getText().toString());
        me.getDetilAgenModel().setNm_pnutup_da(ac_nm_pnutup_da.getText().toString());
        me.getDetilAgenModel().setCheck_kd_ao_da(check_kd_ao_da);
        me.getDetilAgenModel().setKd_ao_da(et_kd_ao_txt_da.getText().toString());
        me.getDetilAgenModel().setCheck_kd_pribadi_da(check_kd_pribadi_da);
        me.getDetilAgenModel().setKd_pnagihan_da(et_kode_regional_penagihan_da.getText().toString());
        me.getDetilAgenModel().setNm_Regional_Penagihan_da(Nm_Regional_Penagihan);
        me.getDetilAgenModel().setCheck_broker_da(check_broker_da);
        me.getDetilAgenModel().setBroker_da(broker_da);
        me.getDetilAgenModel().setNama_broker_da(et_nama_broker_da.getText().toString());
        me.getDetilAgenModel().setBank_da(bank);
        me.getDetilAgenModel().setNorek_da(et_no_rek_broker_da.getText().toString());
        me.getDetilAgenModel().setPnytaan_setuju_da(pnytaan);
        me.getDetilAgenModel().setId_sponsor_da(id_sponsor_da);
        me.getDetilAgenModel().setId_penempatan_da(id_penempatan_da);
        me.getDetilAgenModel().setId_refferal_da(et_id_refferal_da.getText().toString());
        me.getDetilAgenModel().setNm_refferal_da(ac_nm_referral_da.getText().toString());
        me.getDetilAgenModel().setNm_cabang_da(ac_nm_cabang_da.getText().toString());
        me.getDetilAgenModel().setId_cabang_da(id_cabang_da);
        me.getDetilAgenModel().setEmail_da(et_email_da.getText().toString());
        me.getDetilAgenModel().setId_member_da(et_id_member_da.getText().toString());
        me.getDetilAgenModel().setId_penempatan_da(et_id_penempatan_da.getText().toString());
        me.getDetilAgenModel().setId_sponsor_da(et_id_sponsor_da.getText().toString());
        E_MainActivity.e_bundle.putParcelable(getString(R.string.modelespaj), me);
    }

    private void restore() {
        try {

            if (me.getDetilAgenModel().getJENIS_LOGIN() == 2) {
                if (GROUPID == 40) {
                    cl_siap2u_da.setVisibility(View.VISIBLE);
                }else {
                    cl_siap2u_da.setVisibility(View.GONE);
                }
                cl_referensi_da.setVisibility(View.GONE);
            } else if (me.getDetilAgenModel().getJENIS_LOGIN() == 9) {
                cl_referensi_da.setVisibility(View.VISIBLE);
                cl_siap2u_da.setVisibility(View.GONE);
            }

            et_tgl_spaj_da.setText(me.getDetilAgenModel().getTgl_spaj_da());
            MethodSupport.active_view(0, et_tgl_spaj_da);
            et_nm_regional_da.setText(NAMA_REG);
            MethodSupport.active_view(0, et_nm_regional_da);
            et_kd_regional_da.setText(KODE_REGIONAL);
            MethodSupport.active_view(0, et_kd_regional_da);
//            if (me.getDetilAgenModel().getJENIS_LOGIN() == 2) {//bagian kondisi ini hanya untuk data tes
//                et_kd_penutup_da.setText("002744");
//            } else if (me.getDetilAgenModel().getJENIS_LOGIN() == 9) {
//                if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 44) {
//                    et_kd_penutup_da.setText("000000");
//                } else if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 51) {
//                    et_kd_penutup_da.setText("014337");
//                } else if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 50) {
//                    et_kd_penutup_da.setText("902004");
//                }
//                et_kd_penutup_da.setText(KODE_AGEN);
//            } else { //bagian kondisi ini hanya untuk data tes
            if (me.getUsulanAsuransiModel().getKd_produk_ua() == 208 && me.getUsulanAsuransiModel().getSub_produk_ua() == 5
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 208 && me.getUsulanAsuransiModel().getSub_produk_ua() == 6
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 208 && me.getUsulanAsuransiModel().getSub_produk_ua() == 7
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 208 && me.getUsulanAsuransiModel().getSub_produk_ua() == 8
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 219 && me.getUsulanAsuransiModel().getSub_produk_ua() == 1
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 219 && me.getUsulanAsuransiModel().getSub_produk_ua() == 2
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 219 && me.getUsulanAsuransiModel().getSub_produk_ua() == 3
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 219 && me.getUsulanAsuransiModel().getSub_produk_ua() == 4) {
                et_kd_penutup_da.setText(me.getDetilAgenModel().getKd_penutup_da());
                MethodSupport.active_view(0, et_kd_penutup_da);
                ac_nm_pnutup_da.setText(me.getDetilAgenModel().getNm_pnutup_da());
                MethodSupport.active_view(1, ac_nm_pnutup_da);
            }else {
                et_kd_penutup_da.setText(KODE_AGEN);
                MethodSupport.active_view(0, et_kd_penutup_da);
                ac_nm_pnutup_da.setText(NAMA_AGEN);
                MethodSupport.active_view(0, ac_nm_pnutup_da);
            }

            et_kd_leader_da.setText(KODE_LEADER);
            MethodSupport.active_view(0, et_kd_leader_da);
            et_kd_ao_txt_da.setText(KODE_AGEN);
            MethodSupport.active_view(0, et_kd_ao_txt_da);
            et_kode_regional_penagihan_da.setText(me.getDetilAgenModel().getKd_pnagihan_da());
            et_nama_broker_da.setText(me.getDetilAgenModel().getNama_broker_da());
            et_no_rek_broker_da.setText(me.getDetilAgenModel().getNorek_da());
            MethodSupport.active_view(0, et_id_refferal_da);
            //Aktifin hanya ketika tes
//            if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 44) {
//                et_id_refferal_da.setText("000000");
//            } else if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 51) {
//                et_id_refferal_da.setText("014337");
//            } else if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 50) {
//                et_id_refferal_da.setText("902004");
//            } else {//non Aktifin kalau data asli

            et_id_refferal_da.setText(me.getDetilAgenModel().getId_refferal_da());
//            }
            ac_nm_referral_da.setText(me.getDetilAgenModel().getNm_refferal_da());
            id_cabang_da = me.getDetilAgenModel().getId_cabang_da();
            ac_nm_cabang_da.setText(me.getDetilAgenModel().getNm_cabang_da());
            if (me.getDetilAgenModel().getJENIS_LOGIN_BC() != 50) {
                MethodSupport.active_view(0, ac_nm_cabang_da);
            }
            ac_nm_referral_da.setText(me.getDetilAgenModel().getNm_refferal_da());
            et_email_da.setText(me.getDetilAgenModel().getEmail_da());
            MethodSupport.active_view(0, et_email_da);

            Nm_Regional_Penagihan = me.getDetilAgenModel().getNm_Regional_Penagihan_da();

            bank = me.getDetilAgenModel().getBank_da();

            pnytaan = 0;

            ac_pnytaan_da.setText(MethodSupport.getAdapterPositionArray(getResources().getStringArray(R.array.pernyataan_da), pnytaan));
            MethodSupport.active_view(0, ac_pnytaan_da);

            et_id_member_da.setText(me.getDetilAgenModel().getId_member_da());
            MethodSupport.active_view(0, et_id_member_da);
            et_id_penempatan_da.setText(me.getDetilAgenModel().getId_penempatan_da());
            MethodSupport.active_view(0, et_id_penempatan_da);
            et_id_sponsor_da.setText(me.getDetilAgenModel().getId_sponsor_da());
            MethodSupport.active_view(0, et_id_sponsor_da);

            if (!me.getValidation()) {
                Validation();
            }
            if (me.getModelID().getFlag_aktif() == 1) {
                MethodSupport.disable(false, cl_child_da);
            }
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

//    nmlead_pnutup_da
//    check_kd_ao_da
//    check_kd_pribadi_da
//    Nm_Regional_Penagihan
//    check_broker_da
//    id_sponsor_da
//    id_sponsor_da

    private void load_setDropXml() throws IOException, XmlPullParserException {

    }

    private boolean Validation() {
        boolean val = true;
        int color = ContextCompat.getColor(getContext(), R.color.red);
        if (et_tgl_spaj_da.isShown() && et_tgl_spaj_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, et_tgl_spaj_da, tv_error_tgl_spaj_da, iv_circle_tgl_spaj_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_tgl_spaj_da.setVisibility(View.VISIBLE);
//            tv_error_tgl_spaj_da.setText(getString(R.string.error_field_required));
//            iv_circle_tgl_spaj_da.setColorFilter(color);
        }
        if (et_kd_regional_da.isShown() && et_kd_regional_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, et_kd_regional_da, tv_error_kd_regional_da, iv_circle_kd_regional_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_kd_regional_da.setVisibility(View.VISIBLE);
//            tv_error_kd_regional_da.setText(getString(R.string.error_field_required));
//            iv_circle_kd_regional_da.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_kd_regional_da.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_da, cl_child_da, et_kd_regional_da, tv_error_kd_regional_da, iv_circle_kd_regional_da, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_nm_regional_da.isShown() && et_nm_regional_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, et_nm_regional_da, tv_error_nm_regional_da, iv_circle_nm_regional_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nm_regional_da.setVisibility(View.VISIBLE);
//            tv_error_nm_regional_da.setText(getString(R.string.error_field_required));
//            iv_circle_nm_regional_da.setColorFilter(color);
        }
//        else {
//            if (!Method_Validator.ValEditRegex(et_nm_regional_da.getText().toString())) {
//                val = false;
//                MethodSupport.onFocusview(scroll_da, cl_child_da, et_nm_regional_da, tv_error_nm_regional_da, iv_circle_nm_regional_da, color, getContext(), getString(R.string.error_regex));
//            }
//        }
        if (et_kd_penutup_da.isShown() && et_kd_penutup_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, et_kd_penutup_da, tv_error_kd_penutup_da, iv_circle_kd_penutup_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_kd_penutup_da.setVisibility(View.VISIBLE);
//            tv_error_kd_penutup_da.setText(getString(R.string.error_field_required));
//            iv_circle_kd_penutup_da.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_kd_penutup_da.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_da, cl_child_da, et_kd_penutup_da, tv_error_kd_penutup_da, iv_circle_kd_penutup_da, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (ac_nm_pnutup_da.isShown() && ac_nm_pnutup_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, ac_nm_pnutup_da, tv_error_nm_pnutup_da, iv_circle_nm_pnutup_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nm_pnutup_da.setVisibility(View.VISIBLE);
//            tv_error_nm_pnutup_da.setText(getString(R.string.error_field_required));
//            iv_circle_nm_pnutup_da.setColorFilter(color);
        }

        if (et_nik_msig_da.isShown() && et_nik_msig_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, et_nik_msig_da, tv_error_nik_msig_da, iv_circle_nik_msig_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nik_msig_da.setVisibility(View.VISIBLE);
//            tv_error_nik_msig_da.setText(getString(R.string.error_field_required));
//            iv_circle_nik_msig_da.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_nik_msig_da.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_da, cl_child_da, et_nik_msig_da, tv_error_nik_msig_da, iv_circle_nik_msig_da, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_nama_karyawan_da.isShown() && et_nama_karyawan_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, et_nama_karyawan_da, tv_error_nama_karyawan_da, iv_circle_nama_karyawan_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nama_karyawan_da.setVisibility(View.VISIBLE);
//            tv_error_nama_karyawan_da.setText(getString(R.string.error_field_required));
//            iv_circle_nama_karyawan_da.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_nama_karyawan_da.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_da, cl_child_da, et_nama_karyawan_da, tv_error_nama_karyawan_da, iv_circle_nama_karyawan_da, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_cabang_da.isShown() && et_cabang_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, et_cabang_da, tv_error_cabang_da, iv_circle_cabang_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_cabang_da.setVisibility(View.VISIBLE);
//            tv_error_cabang_da.setText(getString(R.string.error_field_required));
//            iv_circle_cabang_da.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_cabang_da.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_da, cl_child_da, et_cabang_da, tv_error_cabang_da, iv_circle_cabang_da, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (ac_status_da.isShown() && ac_status_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, ac_status_da, tv_error_status_da, iv_circle_status_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_status_da.setVisibility(View.VISIBLE);
//            tv_error_status_da.setText(getString(R.string.error_field_required));
//            iv_circle_status_da.setColorFilter(color);
        }
        if (ac_nama_perusahaan_da.isShown() && ac_nama_perusahaan_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, ac_nama_perusahaan_da, tv_error_nama_perusahaan_da, iv_circle_nama_perusahaan_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nama_perusahaan_da.setVisibility(View.VISIBLE);
//            tv_error_nama_perusahaan_da.setText(getString(R.string.error_field_required));
//            iv_circle_nama_perusahaan_da.setColorFilter(color);
        }
        if (et_nik_karyawan_da.isShown() && et_nik_karyawan_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, et_nik_karyawan_da, tv_error_nik_karyawan_da, iv_circle_nik_karyawan_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nik_karyawan_da.setVisibility(View.VISIBLE);
//            tv_error_nik_karyawan_da.setText(getString(R.string.error_field_required));
//            iv_circle_nik_karyawan_da.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_nik_karyawan_da.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_da, cl_child_da, et_nik_karyawan_da, tv_error_nik_karyawan_da, iv_circle_nik_karyawan_da, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (ac_nm_referral_da.isShown() && ac_nm_referral_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, ac_nm_referral_da, tv_error_nm_refferal_da, iv_circle_nm_refferal_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nm_refferal_da.setVisibility(View.VISIBLE);
//            tv_error_nm_refferal_da.setText(getString(R.string.error_field_required));
//            iv_circle_nm_refferal_da.setColorFilter(color);
        }
        if (ac_nm_cabang_da.isShown() && ac_nm_cabang_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, ac_nm_cabang_da, tv_error_nm_cabang_da, iv_circle_nm_cabang_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nm_cabang_da.setVisibility(View.VISIBLE);
//            tv_error_nm_cabang_da.setText(getString(R.string.error_referral));
//            iv_circle_nm_cabang_da.setColorFilter(color);
        }
        if (et_id_refferal_da.isShown() && et_id_refferal_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, et_id_refferal_da, tv_error_id_refferal_da, iv_circle_id_refferal_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_id_refferal_da.setVisibility(View.VISIBLE);
//            tv_error_id_refferal_da.setText(getString(R.string.error_referral));
//            iv_circle_id_refferal_da.setColorFilter(color);
        }
        if (ac_khusus_mnc2_da.isShown() && ac_khusus_mnc2_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, ac_khusus_mnc2_da, tv_error_khusus_mnc2_da, iv_circle_khusus_mnc2_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_khusus_mnc2_da.setVisibility(View.VISIBLE);
//            tv_error_khusus_mnc2_da.setText(getString(R.string.error_field_required));
//            iv_circle_khusus_mnc2_da.setColorFilter(color);
        }
        if (ac_pnytaan_da.isShown() && ac_pnytaan_da.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_da, cl_child_da, ac_pnytaan_da, tv_error_pnytaan_da, iv_circle_pnytaan_da, color, getContext(), getString(R.string.error_field_required));
//            tv_error_pnytaan_da.setVisibility(View.VISIBLE);
//            tv_error_pnytaan_da.setText(getString(R.string.error_field_required));
//            iv_circle_pnytaan_da.setColorFilter(color);
        }
        return val;
    }

    private void setAdapterPernyataan() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.pernyataan_da));
        ac_pnytaan_da.setAdapter(Adapter);
    }

    private void setAdapterNamaReferral() {
        ArrayList<ModelReferralDa> Dropdown = new E_Select(getContext()).getLstReferral();
        ArrayAdapter<ModelReferralDa> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_nm_referral_da.setThreshold(1);
        ac_nm_referral_da.setAdapter(Adapter);
    }

    private void setAdapterNamaReferralAAJI() {
        ArrayList<ModelReferralDa> Dropdown = new E_Select(getContext()).getLstReferralAAJI();
        ArrayAdapter<ModelReferralDa> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_nm_pnutup_da.setThreshold(1);
        ac_nm_pnutup_da.setAdapter(Adapter);
    }

    private void setAdapterCabang() {
        ArrayList<ModelCabangDa> Dropdown = new E_Select(getContext()).getLstCabang();
        ArrayAdapter<ModelCabangDa> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_nm_cabang_da.setThreshold(1);
        ac_nm_cabang_da.setAdapter(Adapter);
    }


    private void onClickLanjut() {

/*        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Melakukan perpindahan halaman");
        progressDialog.setMessage("Mohon Menunggu...");
        progressDialog.show();
        progressDialog.setCancelable(false);*/
//        Runnable progressRunnable = new Runnable() {
//            @Override
//            public void run() {
//                MethodSupport.active_view(0, btn_lanjut);
//                MethodSupport.active_view(0, iv_next);
        //ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);

        me.getDetilAgenModel().setValidation(Validation());
        loadview();
        MyTask task = new MyTask();
        task.execute();
//                View cl_child_da = getActivity().findViewById(R.id.cl_child_da);
//                File file_bitmap = new File(getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//                if (!file_bitmap.exists()) {
//                    file_bitmap.mkdirs();
//                }

        //createJPG
        //String fileName = "DA_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
        //Context context = getActivity();
        //MethodSupport.onCreateBitmap(cl_child_da, scroll_da, file_bitmap, fileName, context);

//        ((E_MainActivity) getActivity()).setFragment(new E_DokumenPendukungFragment(), getResources().getString(R.string.dokumen_pendukung));
//                progressDialog.cancel();

//            }
//        };
//
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 1000);
    }

    private class MyTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_DETIL_AGEN);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ((E_MainActivity) getActivity()).setFragment(new E_DokumenPendukungFragment(), getResources().getString(R.string.dokumen_pendukung));
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    private class MyTaskBack extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_DETIL_AGEN);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ((E_MainActivity) getActivity()).setFragment(new E_DetilInvestasiFragment(), getResources().getString(R.string.detil_investasi));
        }
    }

    private void onClickBack() {

        loadview();
        MyTaskBack taskBack = new MyTaskBack();
        taskBack.execute();

//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setTitle("Melakukan perpindahan halaman");
//        progressDialog.setMessage("Mohon Menunggu...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
//        Runnable progressRunnable = new Runnable() {
//
//            @Override
//            public void run() {
//                MethodSupport.active_view(0, btn_kembali);
//                MethodSupport.active_view(0, iv_prev);
//                ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
//
//                loadview();
//                ((E_MainActivity) getActivity()).onSave();
//                ((E_MainActivity) getActivity()).setFragment(new E_DetilInvestasiFragment(), getResources().getString(R.string.detil_investasi));
//                progressDialog.cancel();
//            }
//        };
//
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 1000);
    }

    private ProgressDialog dialogmessage;

    public void startProgress() {
        dialogmessage = new ProgressDialog(getActivity());
        dialogmessage.setMessage("Mohon Menunggu ");
        dialogmessage.setTitle("Sedang Melakukan Perpindahan Halaman ...");
        dialogmessage.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialogmessage.show();
        dialogmessage.setCancelable(false);
        btn_lanjut.setEnabled(false);
        iv_next.setEnabled(false);
        iv_prev.setEnabled(false);
        btn_kembali.setEnabled(false);
        dialogmessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                btn_lanjut.setEnabled(true);
                iv_next.setEnabled(true);
                iv_prev.setEnabled(true);
                btn_kembali.setEnabled(true);
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (dialogmessage.getProgress() <= dialogmessage
                            .getMax()) {
                        Thread.sleep(10);
                        handle.sendMessage(handle.obtainMessage());
                        if (dialogmessage.getProgress() == dialogmessage
                                .getMax()) {
                            dialogmessage.dismiss();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    Handler handle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            dialogmessage.incrementProgressBy(1);
        }
    };

    @BindView(R.id.tv_pernyataan_penutup_da)
    TextView tv_pernyataan_penutup_da;
    @BindView(R.id.tv_error_tgl_spaj_da)
    TextView tv_error_tgl_spaj_da;
    @BindView(R.id.tv_error_kd_regional_da)
    TextView tv_error_kd_regional_da;
    @BindView(R.id.tv_error_nm_regional_da)
    TextView tv_error_nm_regional_da;
    @BindView(R.id.tv_error_kd_penutup_da)
    TextView tv_error_kd_penutup_da;
    @BindView(R.id.tv_error_nm_pnutup_da)
    TextView tv_error_nm_pnutup_da;
    @BindView(R.id.tv_error_kd_leader_da)
    TextView tv_error_kd_leader_da;
    @BindView(R.id.tv_error_kd_ao_txt_da)
    TextView tv_error_kd_ao_txt_da;
    @BindView(R.id.tv_error_email_da)
    TextView tv_error_email_da;
    @BindView(R.id.tv_pernyataan_dan_persetujuan_da)
    TextView tv_pernyataan_dan_persetujuan_da;
    @BindView(R.id.tv_error_pnytaan_da)
    TextView tv_error_pnytaan_da;
    @BindView(R.id.tv_referensi_da)
    TextView tv_referensi_da;
    @BindView(R.id.tv_error_id_refferal_da)
    TextView tv_error_id_refferal_da;
    @BindView(R.id.tv_error_nm_refferal_da)
    TextView tv_error_nm_refferal_da;
    @BindView(R.id.tv_error_nm_cabang_da)
    TextView tv_error_nm_cabang_da;
    @BindView(R.id.tv_error_nama_regional_penagihan_da)
    TextView tv_error_nama_regional_penagihan_da;
    @BindView(R.id.tv_error_nama_broker_da)
    TextView tv_error_nama_broker_da;
    @BindView(R.id.tv_error_bank_da)
    TextView tv_error_bank_da;
    @BindView(R.id.tv_error_kode_regional_penagihan_da)
    TextView tv_error_kode_regional_penagihan_da;
    @BindView(R.id.tv_error_cari_bank_da)
    TextView tv_error_cari_bank_da;
    @BindView(R.id.tv_error_no_rek_broker_da)
    TextView tv_error_no_rek_broker_da;
    @BindView(R.id.tv_info_khusus_karyawan_msig_pemegang_polis_da)
    TextView tv_info_khusus_karyawan_msig_pemegang_polis_da;
    @BindView(R.id.tv_error_nik_msig_da)
    TextView tv_error_nik_msig_da;
    @BindView(R.id.tv_error_nama_karyawan_da)
    TextView tv_error_nama_karyawan_da;
    @BindView(R.id.tv_error_cabang_da)
    TextView tv_error_cabang_da;
    @BindView(R.id.tv_error_departement_da)
    TextView tv_error_departement_da;
    @BindView(R.id.tv_error_premi_ke_da)
    TextView tv_error_premi_ke_da;
    @BindView(R.id.tv_error_potongan_da)
    TextView tv_error_potongan_da;
    @BindView(R.id.tv_error_tanggal_proses_da)
    TextView tv_error_tanggal_proses_da;
    @BindView(R.id.tv_info_khusus_produk_perusahaan_karyawan_ajsm_da)
    TextView tv_info_khusus_produk_perusahaan_karyawan_ajsm_da;
    @BindView(R.id.tv_error_cari_bank2_da)
    TextView tv_error_cari_bank2_da;
    @BindView(R.id.tv_error_nama_perusahaan_da)
    TextView tv_error_nama_perusahaan_da;
    @BindView(R.id.tv_error_nik_karyawan_da)
    TextView tv_error_nik_karyawan_da;
    @BindView(R.id.tv_khusus_mnc_da)
    TextView tv_khusus_mnc_da;
    @BindView(R.id.tv_error_khusus_mnc2_da)
    TextView tv_error_khusus_mnc2_da;
    @BindView(R.id.tv_error_status_da)
    TextView tv_error_status_da;
    @BindView(R.id.tv_error_id_penempatan_da)
    TextView tv_error_id_penempatan_da;
    @BindView(R.id.tv_error_id_member_da)
    TextView tv_error_id_member_da;
    @BindView(R.id.tv_error_sponsor_da)
    TextView tv_error_sponsor_da;

    @BindView(R.id.cl_pernyataan_penutup_da)
    ConstraintLayout cl_pernyataan_penutup_da;
    @BindView(R.id.cl_form_pernyataan_penutup_da)
    ConstraintLayout cl_form_pernyataan_penutup_da;
    @BindView(R.id.cl_tgl_spaj_da)
    ConstraintLayout cl_tgl_spaj_da;
    @BindView(R.id.cl_kd_regional_da)
    ConstraintLayout cl_kd_regional_da;
    @BindView(R.id.cl_nm_regional_da)
    ConstraintLayout cl_nm_regional_da;
    @BindView(R.id.cl_kd_penutup_da)
    ConstraintLayout cl_kd_penutup_da;
    @BindView(R.id.cl_nm_pnutup_da)
    ConstraintLayout cl_nm_pnutup_da;
    @BindView(R.id.cl_kd_leader_da)
    ConstraintLayout cl_kd_leader_da;
    @BindView(R.id.cl_kd_ao_txt_da)
    ConstraintLayout cl_kd_ao_txt_da;
    @BindView(R.id.cl_email_da)
    ConstraintLayout cl_email_da;
    @BindView(R.id.cl_pernyataan_dan_persetujuan_da)
    ConstraintLayout cl_pernyataan_dan_persetujuan_da;
    @BindView(R.id.cl_form_pernyataan_dan_persetujuan_da)
    ConstraintLayout cl_form_pernyataan_dan_persetujuan_da;
    @BindView(R.id.cl_pnytaan_da)
    ConstraintLayout cl_pnytaan_da;
    @BindView(R.id.cl_referensi_da)
    ConstraintLayout cl_referensi_da;
    @BindView(R.id.cl_siap2u_da)
    ConstraintLayout cl_siap2u_da;
    @BindView(R.id.cl_form_referensi_da)
    ConstraintLayout cl_form_referensi_da;
    @BindView(R.id.cl_id_refferal_da)
    ConstraintLayout cl_id_refferal_da;
    @BindView(R.id.cl_nm_refferal_da)
    ConstraintLayout cl_nm_refferal_da;
    @BindView(R.id.cl_pribadi_da)
    ConstraintLayout cl_pribadi_da;
    @BindView(R.id.cl_nama_regional_penagihan_da)
    ConstraintLayout cl_nama_regional_penagihan_da;
    @BindView(R.id.cl_kode_regional_penagihan_da)
    ConstraintLayout cl_kode_regional_penagihan_da;
    @BindView(R.id.cl_broker_da)
    ConstraintLayout cl_broker_da;
    @BindView(R.id.cl_nama_broker_da)
    ConstraintLayout cl_nama_broker_da;
    @BindView(R.id.cl_cari_bank_da)
    ConstraintLayout cl_cari_bank_da;
    @BindView(R.id.cl_bank_da)
    ConstraintLayout cl_bank_da;
    @BindView(R.id.cl_no_rek_broker_da)
    ConstraintLayout cl_no_rek_broker_da;
    @BindView(R.id.cl_info_khusus_karyawan_msig_pemegang_polis_da)
    ConstraintLayout cl_info_khusus_karyawan_msig_pemegang_polis_da;
    @BindView(R.id.cl_form_info_khusus_karyawan_msig_pemegang_polis_da)
    ConstraintLayout cl_form_info_khusus_karyawan_msig_pemegang_polis_da;
    @BindView(R.id.cl_nik_msig_da)
    ConstraintLayout cl_nik_msig_da;
    @BindView(R.id.cl_nama_karyawan_da)
    ConstraintLayout cl_nama_karyawan_da;
    @BindView(R.id.cl_cabang_da)
    ConstraintLayout cl_cabang_da;
    @BindView(R.id.cl_departement_da)
    ConstraintLayout cl_departement_da;
    @BindView(R.id.cl_premi_ke_da)
    ConstraintLayout cl_premi_ke_da;
    @BindView(R.id.cl_potongan_da)
    ConstraintLayout cl_potongan_da;
    @BindView(R.id.cl_tanggal_proses_da)
    ConstraintLayout cl_tanggal_proses_da;
    @BindView(R.id.cl_info_khusus_produk_perusahaan_karyawan_ajsm_da)
    ConstraintLayout cl_info_khusus_produk_perusahaan_karyawan_ajsm_da;
    @BindView(R.id.cl_form_info_khusus_produk_perusahaan_karyawan_ajsm_da)
    ConstraintLayout cl_form_info_khusus_produk_perusahaan_karyawan_ajsm_da;
    @BindView(R.id.cl_status_da)
    ConstraintLayout cl_status_da;
    @BindView(R.id.cl_cari_bank2_da)
    ConstraintLayout cl_cari_bank2_da;
    @BindView(R.id.cl_nama_perusahaan_da)
    ConstraintLayout cl_nama_perusahaan_da;
    @BindView(R.id.cl_nik_karyawan_da)
    ConstraintLayout cl_nik_karyawan_da;
    @BindView(R.id.cl_khusus_mnc_da)
    ConstraintLayout cl_khusus_mnc_da;
    @BindView(R.id.cl_form_khusus_mnc_da)
    ConstraintLayout cl_form_khusus_mnc_da;
    @BindView(R.id.cl_khusus_mnc2_da)
    ConstraintLayout cl_khusus_mnc2_da;
    @BindView(R.id.cl_child_da)
    ConstraintLayout cl_child_da;

//    @BindView(R.id.cl_bt_kembali_da)
//    ConstraintLayout cl_bt_kembali_da;
//    @BindView(R.id.cl_bt_lanjut_da)
//    ConstraintLayout cl_bt_lanjut_da;

    @BindView(R.id.iv_circle_tgl_spaj_da)
    ImageView iv_circle_tgl_spaj_da;
    @BindView(R.id.iv_circle_kd_regional_da)
    ImageView iv_circle_kd_regional_da;
    @BindView(R.id.iv_circle_nm_regional_da)
    ImageView iv_circle_nm_regional_da;
    @BindView(R.id.iv_circle_kd_penutup_da)
    ImageView iv_circle_kd_penutup_da;
    @BindView(R.id.iv_circle_nm_pnutup_da)
    ImageView iv_circle_nm_pnutup_da;
    @BindView(R.id.iv_circle_pnytaan_da)
    ImageView iv_circle_pnytaan_da;
    @BindView(R.id.iv_circle_nik_msig_da)
    ImageView iv_circle_nik_msig_da;
    @BindView(R.id.iv_circle_nama_karyawan_da)
    ImageView iv_circle_nama_karyawan_da;
    @BindView(R.id.iv_circle_cabang_da)
    ImageView iv_circle_cabang_da;
    @BindView(R.id.iv_circle_status_da)
    ImageView iv_circle_status_da;
    @BindView(R.id.iv_circle_nama_perusahaan_da)
    ImageView iv_circle_nama_perusahaan_da;
    @BindView(R.id.iv_circle_nik_karyawan_da)
    ImageView iv_circle_nik_karyawan_da;
    @BindView(R.id.iv_circle_khusus_mnc2_da)
    ImageView iv_circle_khusus_mnc2_da;
    @BindView(R.id.iv_circle_id_refferal_da)
    ImageView iv_circle_id_refferal_da;
    @BindView(R.id.iv_circle_nm_refferal_da)
    ImageView iv_circle_nm_refferal_da;
    @BindView(R.id.iv_circle_nm_cabang_da)
    ImageView iv_circle_nm_cabang_da;
    @BindView(R.id.iv_circle_sponsor_da)
    ImageView iv_circle_sponsor_da;
    @BindView(R.id.iv_circle_id_member_da)
    ImageView iv_circle_id_member_da;
    @BindView(R.id.iv_circle_id_penempatan_da)
    ImageView iv_circle_id_penempatan_da;

    @BindView(R.id.et_tgl_spaj_da)
    EditText et_tgl_spaj_da;
    @BindView(R.id.et_kd_regional_da)
    EditText et_kd_regional_da;
    @BindView(R.id.et_nm_regional_da)
    EditText et_nm_regional_da;
    @BindView(R.id.et_kd_penutup_da)
    EditText et_kd_penutup_da;
    @BindView(R.id.et_kd_leader_da)
    EditText et_kd_leader_da;
    @BindView(R.id.et_kd_ao_txt_da)
    EditText et_kd_ao_txt_da;
    @BindView(R.id.et_email_da)
    EditText et_email_da;
    @BindView(R.id.et_kode_regional_penagihan_da)
    EditText et_kode_regional_penagihan_da;
    @BindView(R.id.et_nama_broker_da)
    EditText et_nama_broker_da;
    @BindView(R.id.et_cari_bank_da)
    EditText et_cari_bank_da;
    @BindView(R.id.et_no_rek_broker_da)
    EditText et_no_rek_broker_da;
    @BindView(R.id.et_nik_msig_da)
    EditText et_nik_msig_da;
    @BindView(R.id.et_nama_karyawan_da)
    EditText et_nama_karyawan_da;
    @BindView(R.id.et_cabang_da)
    EditText et_cabang_da;
    @BindView(R.id.et_departement_da)
    EditText et_departement_da;
    @BindView(R.id.et_premi_ke_da)
    EditText et_premi_ke_da;
    @BindView(R.id.et_potongan_da)
    EditText et_potongan_da;
    @BindView(R.id.et_tanggal_proses_da)
    EditText et_tanggal_proses_da;
    @BindView(R.id.et_cari_bank2_da)
    EditText et_cari_bank2_da;
    @BindView(R.id.et_nik_karyawan_da)
    EditText et_nik_karyawan_da;

    @BindView(R.id.et_id_refferal_da)
    EditText et_id_refferal_da;//sudah diganti
    @BindView(R.id.et_id_member_da)
    EditText et_id_member_da;
    @BindView(R.id.et_id_penempatan_da)
    EditText et_id_penempatan_da;
    @BindView(R.id.et_id_sponsor_da)
    EditText et_id_sponsor_da;

    @BindView(R.id.ac_nm_referral_da)
    AutoCompleteTextView ac_nm_referral_da;//sudah diganti
    @BindView(R.id.ac_nm_cabang_da)
    AutoCompleteTextView ac_nm_cabang_da;//sudah diganti
    @BindView(R.id.ac_pnytaan_da)
    AutoCompleteTextView ac_pnytaan_da;
    @BindView(R.id.ac_nama_regional_penagihan_da)
    AutoCompleteTextView ac_nama_regional_penagihan_da;
    @BindView(R.id.ac_bank_da)
    AutoCompleteTextView ac_bank_da;
    @BindView(R.id.ac_status_da)
    AutoCompleteTextView ac_status_da;
    @BindView(R.id.ac_nama_perusahaan_da)
    AutoCompleteTextView ac_nama_perusahaan_da;
    @BindView(R.id.ac_khusus_mnc2_da)
    AutoCompleteTextView ac_khusus_mnc2_da;
    @BindView(R.id.ac_nm_pnutup_da)
    AutoCompleteTextView ac_nm_pnutup_da;

    @BindView(R.id.cb_tutupan_pribadi_da)
    CheckBox cb_tutupan_pribadi_da;
    @BindView(R.id.cb_broker_da)
    CheckBox cb_broker_da;

    @BindView(R.id.scroll_da)
    ScrollView scroll_da;

    private Button btn_lanjut, btn_kembali;
    private ImageView iv_save, iv_next, iv_prev;
    private Toolbar second_toolbar;

    View view;
    EspajModel me;

    private String cb_tutupan_pribadi = "";
    private String cb_broker = "";

    private String Nm_Regional_Penagihan;
    private int bank;
    private String nm_prshaan;
    private int khusus_mnc;
    private int pnytaan;
    private int status = -1;
    private int GROUPID;

    private String NAMA_AGEN, KODE_AGEN, NAMA_REG, KODE_REGIONAL, KODE_LEADER, KODE_REG1, KODE_REG2, KODE_REG3, EMAIL;
    private String JENIS_LOGIN;
    private String JENIS_LOGIN_BC;
    //not declare in layout
    private String nmlead_pnutup_da = "";
    private int check_kd_ao_da = 1;
    private int check_kd_pribadi_da = 1;
    private int check_broker_da = 1;
    private String broker_da = "";
    private String id_sponsor_da = "";
    private String id_penempatan_da = "";
    private String id_cabang_da = "";
    private File pdfFile;

    private ProgressDialog progressDialog;
}