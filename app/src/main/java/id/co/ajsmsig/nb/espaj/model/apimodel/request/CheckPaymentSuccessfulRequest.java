package id.co.ajsmsig.nb.espaj.model.apimodel.request;

public class CheckPaymentSuccessfulRequest {
    private String transactionno;

    public CheckPaymentSuccessfulRequest(String transactionno) {
        this.transactionno = transactionno;
    }

}
