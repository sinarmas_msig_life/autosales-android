package id.co.ajsmsig.nb.prop.model.modelCalcIllustration;

import java.io.Serializable;
import java.util.Date;

public class Prop_Model_S_ladiesMedExpense implements Serializable
{
	private static final long serialVersionUID = -4675656993202205558L;
	public int peserta;
    public String[] nama = new String[5 + 1]; 
    public Date[] tgl = new Date[5 + 1];
    public int[] usia = new int[5 + 1];
    public boolean changed; 
}
