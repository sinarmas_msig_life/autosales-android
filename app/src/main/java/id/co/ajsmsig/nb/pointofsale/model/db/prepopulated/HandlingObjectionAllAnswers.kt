package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.*
import android.databinding.ObservableBoolean

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */
class HandlingObjectionAllAnswers {

        @Embedded
        var handlingObjection: HandlingObjection? = null

        @Relation(entity = HandlingObjection::class, parentColumn = "id", entityColumn = "parentId")
        var answers: List<HandlingObjection>? = null

        @Ignore
        var selected: ObservableBoolean = ObservableBoolean(false)
}