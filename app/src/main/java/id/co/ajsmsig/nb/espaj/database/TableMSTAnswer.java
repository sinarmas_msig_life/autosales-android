package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelJnsDanaDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelMst_Answer;

/**
 * Created by Eriza_c on 30/09/2017.
 */

public class TableMSTAnswer {

    private Context context;

    public TableMSTAnswer(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(ModelMst_Answer me, int counter, String SPAJ_ID_TAB) {
        ContentValues cv = new ContentValues();
        cv.put("COUNTER", counter);
        cv.put("SPAJ_ID_TAB", SPAJ_ID_TAB);
        cv.put("QUESTION_VALID_DATE", me.getQuestion_valid_date());
        cv.put("QUESTION_TYPE_ID", me.getQuestion_type_id());
        cv.put("QUESTION_ID", me.getQuestion_id());
        cv.put("OPTION_TYPE", me.getOption_type());
        cv.put("OPTION_GROUP", me.getOption_group());
        cv.put("OPTION_ORDER", me.getOption_order());
        cv.put("ANSWER", me.getAnswer());
        return cv;
    }
    public ArrayList<ModelMst_Answer> getObjectArray(Cursor cursor) {
        ArrayList<ModelMst_Answer> List = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ModelMst_Answer msa = new ModelMst_Answer();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("COUNTER"))) {
                    msa.setCounter(cursor.getInt(cursor.getColumnIndexOrThrow("COUNTER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("QUESTION_VALID_DATE"))) {
                    msa.setQuestion_valid_date(cursor.getString(cursor.getColumnIndexOrThrow("QUESTION_VALID_DATE")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("QUESTION_TYPE_ID"))) {
                    msa.setQuestion_type_id(cursor.getInt(cursor.getColumnIndexOrThrow("QUESTION_TYPE_ID")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("QUESTION_ID"))) {
                    msa.setQuestion_id(cursor.getInt(cursor.getColumnIndexOrThrow("QUESTION_ID")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("OPTION_TYPE"))) {
                    msa.setOption_type(cursor.getInt(cursor.getColumnIndexOrThrow("OPTION_TYPE")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("OPTION_GROUP"))) {
                    msa.setOption_group(cursor.getInt(cursor.getColumnIndexOrThrow("OPTION_GROUP")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("OPTION_ORDER"))) {
                    msa.setOption_order(cursor.getInt(cursor.getColumnIndexOrThrow("OPTION_ORDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("ANSWER"))) {
                    msa.setAnswer(cursor.getString(cursor.getColumnIndexOrThrow("ANSWER")));
                }

                List.add(msa);
                cursor.moveToNext();
            }

            // always close the cursor
            cursor.close();
        }

        return List;
    }
}
