package id.co.ajsmsig.nb.pointofsale.model.db.dynamic.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductRider


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface ProductRiderDao: BaseDao<ProductRider> {

    @Query("select * from ProductRider")
    fun loadAllProductRiders(): LiveData<List<ProductRider>>

    @Query("select * from ProductRider where ProductLsbsId = :lsbsId")
    fun loadAllProductRidersWith(lsbsId: Int?): LiveData<List<ProductRider>>


}