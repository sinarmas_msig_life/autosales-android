package id.co.ajsmsig.nb.crm.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.adapter.AgencySearchLeadAdapter;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.model.AgencySearchModel;
import id.co.ajsmsig.nb.crm.model.hardcode.RequestCode;
import id.co.ajsmsig.nb.pointofsale.POSActivity;
import id.co.ajsmsig.nb.pointofsale.utils.Constants;
import id.co.ajsmsig.nb.util.Const;

public class AgencySearchLeadActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor>,
        SearchView.OnQueryTextListener, AgencySearchLeadAdapter.ClickListener{


    private String TAG = C_SearchLeadActivity.class.getSimpleName();
    // If non-null, this is the current filter the user has provided.
    private String mCurFilter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.layoutSearchNotFound)
    LinearLayout layoutSearchNotFound;

    private final int LOADER_ID = 420;
    private AgencySearchLeadAdapter mAdapter;
    private List<AgencySearchModel> leadList;
    private String agentCode;
    private String searchQuery;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agency_search_lead);
        ButterKnife.bind(this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.title_activity_search_lead);
            toolbar.setNavigationOnClickListener(view -> onBackPressed());
        }
        handleIntent(getIntent());

        Intent intent = getIntent();
        agentCode = intent.getStringExtra(Const.INTENT_KEY_AGENT_CODE);

        leadList = new ArrayList<>();
        mAdapter = new AgencySearchLeadAdapter(leadList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        mAdapter.setClickListener(this);
        recyclerView.setAdapter(mAdapter);
        //getSupportLoaderManager().initLoader(LOADER_ID,null,this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_lead, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        //Whether the search field should be iconified by default
        searchView.setIconifiedByDefault(true);

        //A true value will collapse the SearchView to an icon, while a false will expand it.
        searchView.setIconified(false);

        //Change searchview hint color to white
        searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + getResources().getString(R.string.search_hint) + "</font>"));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        //Change searchview close icon to white
        ImageView searchClose = searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
        if (searchClose != null) {
            searchClose.setImageResource(R.drawable.ic_action_search_close);
        }

        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    /**
     * To make searchview query result delivered to this activity instead of a new one
     *
     * @param intent
     */
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // Called when the action bar search text has changed.  Update
        // the search filter, and restart the loader to do a new query
        // with this filter.
        String newFilter = !TextUtils.isEmpty(query) ? query : null;
        // Don't do anything if the filter hasn't actually changed.
        // Prevents restarting the loader when restoring state.
        if (mCurFilter == null && newFilter == null) {
            return true;
        }
        if (mCurFilter != null && mCurFilter.equals(newFilter)) {
            return true;
        }
        mCurFilter = newFilter;


        getSupportLoaderManager().restartLoader(LOADER_ID, null, this);

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        searchQuery = newText;
        // Called when the action bar search text has changed.  Update
        // the search filter, and restart the loader to do a new query
        // with this filter.
        String newFilter = !TextUtils.isEmpty(newText) ? newText : null;
        // Don't do anything if the filter hasn't actually changed.
        // Prevents restarting the loader when restoring state.
        if (mCurFilter == null && newFilter == null) {
            return true;
        }
        if (mCurFilter != null && mCurFilter.equals(newFilter)) {
            return true;
        }
        mCurFilter = newFilter;

        //Put search query into bundle
        Bundle bundle = new Bundle();
        bundle.putString(Const.BUNDLE_KEY_LOADER_SEARCH_LEAD, newText);

        getSupportLoaderManager().restartLoader(LOADER_ID, bundle, this);

        return true;

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        C_Select select = new C_Select(AgencySearchLeadActivity.this);
        //Make sure the agent code is not null, therefore the lead search result only displayed from the logged in agent only
        if (agentCode != null) {
            String searchQuery = null;

            //Extract the search query from bundle
            if (bundle.getString("query") != null) {
                searchQuery = bundle.getString(Const.BUNDLE_KEY_LOADER_SEARCH_LEAD);
            }
            CursorLoader cursorLoader;

            switch (id) {
                case LOADER_ID:
                    cursor = select.searchAgencyLeadWithNameOf(searchQuery, agentCode); //Default sort is by date
                    break;
            }


            cursorLoader = new CursorLoader(AgencySearchLeadActivity.this) {
                @Override
                public Cursor loadInBackground() {
                    return cursor;
                }
            };

            return cursorLoader;
        }

        return null;
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor != null && cursor.getCount() > 0) {
            //Remove previous search lead history. If any.
            clearPreviousSearchResult();

            while (cursor.moveToNext()) {
                long id = cursor.getLong(cursor.getColumnIndexOrThrow("ID"));
                String leadName = cursor.getString(cursor.getColumnIndexOrThrow("LEAD_NAME"));
                int leadAge = cursor.getInt(cursor.getColumnIndexOrThrow("LEAD_AGE"));
                int leadGender = cursor.getInt(cursor.getColumnIndexOrThrow("LEAD_GENDER"));


                AgencySearchModel model = new AgencySearchModel(id, leadName, leadAge, leadGender);
                leadList.add(model);


            }
            mAdapter.notifyDataSetChanged();
            layoutSearchNotFound.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

        } else {
            layoutSearchNotFound.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // This is called when the last Cursor provided to onLoadFinished()
        // above is about to be closed.  We need to make sure we are no
        // longer using it.
    }


    /**
     * Remove previous search lead history. If any.
     */
    private void clearPreviousSearchResult() {
        if (leadList.size() > 0) {
            leadList.clear();
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Re-display search lead result when user navigate back to this activity
     */
    private void reloadSearchLead() {
        //Put search query into bundle
        Bundle bundle = new Bundle();
        bundle.putString(Const.BUNDLE_KEY_LOADER_SEARCH_LEAD, searchQuery);

        getSupportLoaderManager().restartLoader(LOADER_ID, bundle, this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (!TextUtils.isEmpty(searchQuery)) {
            clearPreviousSearchResult();
            reloadSearchLead();
        }

    }

    private void launchPointOfSales(long id, String leadName, int leadGender, int groupId) {
        Intent intent = new Intent(AgencySearchLeadActivity.this, POSActivity.class);
        intent.putExtra(Constants.Companion.getSL_TAB_ID(), id);
        intent.putExtra(Constants.Companion.getSL_NAME(), leadName);
        intent.putExtra(Constants.Companion.getSL_GENDER(), leadGender);
        intent.putExtra(Constants.Companion.getGROUP_ID(), groupId);
        startActivityForResult(intent, RequestCode.REQ_POS);
    }

    @Override
    public void onLeadSelected(int position, List<AgencySearchModel> leads) {
        AgencySearchModel model = leads.get(position);
        long id = model.getId();
        int leadAge = model.getLeadAge();
        String leadName = model.getLeadName();
        int leadGender = model.getLeadGender();

        launchPointOfSales(id,leadName,leadGender,Const.GROUP_ID_AGENCY);
    }
}
