package id.co.ajsmsig.nb.pointofsale.custom

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import id.co.ajsmsig.nb.R

/**
 * Created by andreyyoshuamanik on 27/03/18.
 */
class LoadingButton: RelativeLayout {

    constructor(context: Context) : super(context) {
        init()
    }
    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        init()
    }
    constructor(context: Context, attributeSet: AttributeSet, int: Int) : super(context, attributeSet, int){
        init()
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attributeSet: AttributeSet, int: Int, int1: Int) : super(context, attributeSet, int, int1) {
        init()
    }

    private fun init() {
        View.inflate(context, R.layout.pos_custom_loading_button, this)
        dismissProgress()
    }

    val imageView: ImageView
    get() = findViewById(R.id.imageView)

    private val progressBar: ProgressBar
    get() = findViewById(R.id.progressBar)

    fun showProgress() {
        progressBar.visibility = View.VISIBLE
        imageView.visibility = View.INVISIBLE
    }

    fun dismissProgress() {
        progressBar.visibility = View.INVISIBLE
        imageView.visibility = View.VISIBLE
    }
}