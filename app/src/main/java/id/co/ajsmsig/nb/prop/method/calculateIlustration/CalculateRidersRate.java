package id.co.ajsmsig.nb.prop.method.calculateIlustration;

import android.content.Context;
import android.util.Log;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.method.RiderAiJenis;
import id.co.ajsmsig.nb.prop.method.util.FormatNumber;
import id.co.ajsmsig.nb.prop.method.util.MathUtil;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductRiderModel;
import id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId;
import id.co.ajsmsig.nb.prop.model.hardCode.Proposal;

/**
 * Created by faiz_f on 06/09/2016.
 */
public class CalculateRidersRate {
    private static final String TAG = "CalcRiderRate";
    private P_MstDataProposalModel dataProposalModel;
    private P_MstProposalProductModel proposalProductModel;

    private Context context;
    //    private DBAlokasiInvestasi dbAlokasiInvestasi;
    private P_Select select;
    private String noProposal;
    private int li_usia;

    public CalculateRidersRate(Context context, String noProposalTab) {
        this.context = context;
        this.noProposal = noProposalTab;
        select = new P_Select(context);

        dataProposalModel = select.selectDataProposalByNoProposal(noProposalTab);
        proposalProductModel = select.selectProposalProductByNoProposal(noProposalTab);
    }

    public ArrayList<HashMap<String, Object>> getRiderPriceTag(int ai_th) {
        if (dataProposalModel == null) {
            Log.d(TAG, "getRiderPriceTag: dataproposal null");
        } else {
            if (dataProposalModel.getUmur_tt() == null) {
                Log.d(TAG, "getRiderPriceTag: umur tt null");
            }
        }

        li_usia = dataProposalModel.getUmur_tt() + ai_th - 1;
        int li_usia_pp = dataProposalModel.getUmur_pp() + ai_th - 1;

        HashMap<String, Object> params = new HashMap<>();
        params.put("liUsia", li_usia);
        params.put("li_usia_pp", li_usia_pp);
        params.put("no_proposal", noProposal);
        params.put("thn_ke", ai_th);

        ArrayList<HashMap<String, Object>> results = new ArrayList<>();
        ArrayList<HashMap<String, Object>> riderRates = select.selectRidersRate(params);

        Log.d(TAG, "getRiderPriceTag: rider rates size " + riderRates);
        for (HashMap<String, Object> riderRate : riderRates) {
            int riderId = (int) riderRate.get("RIDER_ID");
            int riderNumber = (int) riderRate.get("RIDER_NUMBER");
            double ldecRate = (double) riderRate.get("RATE");

            params.put("lku_id", proposalProductModel.getLku_id());
            params.put("rider_id", riderId);
            params.put("rider_number", riderNumber);
            params.put("thn_ke", ai_th);
            params.put("ldec_rate", ldecRate);

            Log.i(TAG, "getRiderPriceTag: params = " + params);

            double ldecTotal = calculateLdecTotal(params);
            String nama = RiderAiJenis.getNamaRiderForIlustrasi(riderId, riderNumber);

            Log.i(TAG, "getRiderPriceTag: ldecTotal = " + ldecTotal);

            HashMap<String, Object> result = new HashMap<>();
            result.put("NAMA", nama);
            result.put("LDECTOTAL", ldecTotal);
            results.add(result);
        }
        return results;

    }


    private double calculateLdecTotal(HashMap<String, Object> params) {
        int li_kali = select.selectLiKali(proposalProductModel.getCara_bayar());

        ArrayList<Double> listRateTambahan;

        String noProposal = (String) params.get("no_proposal");
        int riderId = (int) params.get("rider_id");
        int riderNumber = (int) params.get("rider_number");
        double ldec_rate = (double) params.get("ldec_rate");
        int ai_th = (int) params.get("thn_ke");


        P_MstProposalProductRiderModel riderModel = select.selectRider(noProposal, riderId, riderNumber);
        double ldec_temp;
        double ldec_total = 0;
        double persentase;
        int li_temp;

        double ldec_max = 2000000000.0;
        if (proposalProductModel.getLku_id().equals(Proposal.CUR_USD_CD)) {
            ldec_max = 200000.0;
        }
        double maxAmount;

        switch (riderId) {
//            case Prop_Model_RiderLsbsId.RIDER_PA://810
//                maxAmount = MathUtil.min(riderModel.getJml_unit() * proposalProductModel.getUp(), ldec_max);
//                ldec_temp = FormatNumber.round(((maxAmount / 1000) * ldec_rate) * 0.1, 2);
//                ldec_total += ldec_temp;
//                break;
            case Prop_Model_RiderLsbsId.RIDER_HCP://811
                ldec_temp = FormatNumber.round(ldec_rate * 0.1, 2);
                ldec_total += ldec_temp;
                break;
            case Prop_Model_RiderLsbsId.RIDER_TPD://812
                switch (riderNumber) {
//                    case 7:
//                        maxAmount = MathUtil.min(proposalProductModel.getUp(), ldec_max);
//                        ldec_temp = FormatNumber.round(((maxAmount / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
//                        break;
                    default:
                        break;
                }
                break;
            case Prop_Model_RiderLsbsId.RIDER_CI://813
                switch (riderNumber) {
//                    case 8:
////                        ldec_temp = (proposalProductModel.getPremi_pokok() * li_kali) * 2;
//                        ldec_max = 500000000;
//                        if (proposalProductModel.getLku_id().equals("02")) {
//                            ldec_max = 50000;
//                        }
//                        double minAmount = MathUtil.min(ldec_temp, ldec_max);
//                        ldec_temp = FormatNumber.round(((minAmount / 1000) * ldec_rate) * 0.1, 2);
//                        double ldec_persen_ci;
//                        if (proposalProductModel.getLsbs_id() == 120 || proposalProductModel.getLsbs_id() == 127 || proposalProductModel.getLsbs_id() == 128 || proposalProductModel.getLsbs_id() == 129 || proposalProductModel.getLsbs_id() == 202) {
//                            if (proposalProductModel.getLsbs_id() == 127 || proposalProductModel.getLsbs_id() == 128 || proposalProductModel.getLsbs_id() == 129) {
//                                ldec_persen_ci = 1;
//
//                            } else {
//                                ldec_persen_ci = (double) riderModel.getPersen_up() / 100.0;
//                            }
//                            ldec_max = 2000000000;
//                            if (proposalProductModel.getLku_id().equals("02")) {
//                                ldec_max = 200000;
//                            }
//                            minAmount = MathUtil.min(proposalProductModel.getUp() * ldec_persen_ci, ldec_max);
//                            ldec_temp = FormatNumber.round(((minAmount / 1000) * ldec_rate) * 0.1, 2);
//                        }
//
//                        ldec_total += ldec_temp;
//                        break;
                    default:
                        break;
                }
                break;
            case Prop_Model_RiderLsbsId.RIDER_WAIVER_TPD://814
                switch (riderNumber) {
//                    case 4:
//                        ldec_temp = FormatNumber.round((((proposalProductModel.getPremi()) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
//                        break;
//                    case 5:
//                        ldec_temp = FormatNumber.round((((proposalProductModel.getPremi()) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
//                        break;
                    default:
                        break;
                }
                break;
            case Prop_Model_RiderLsbsId.RIDER_PAYOR_TPD_DEATH://815
                switch (riderNumber) {
//                    case 4:
//                        ldec_temp = FormatNumber.round((((proposalProductModel.getPremi()) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
//                        break;
//                    case 5:
//                        ldec_temp = FormatNumber.round((((proposalProductModel.getPremi()) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
//                        break;
                    default:
                        break;

                }
                break;
            case Prop_Model_RiderLsbsId.RIDER_WAIVER_CI://816
                switch (riderNumber) {
//                    case 2:
//                        ldec_temp = FormatNumber.round((((proposalProductModel.getPremi()) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
//                        break;
//                    case 3:
//                        ldec_temp = FormatNumber.round((((proposalProductModel.getPremi()) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
//                        break;
                    default:
                        break;
                }
                break;
            case Prop_Model_RiderLsbsId.RIDER_PAYOR_CI://817:
                switch (riderNumber) {
//                    case 2:
//                        ldec_temp = FormatNumber.round((((proposalProductModel.getPremi()) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
//                        break;
//                    case 3:
//                        ldec_temp = FormatNumber.round((((proposalProductModel.getPremi()) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
//                        break;
                    default:
                        break;
                }
                break;
            case Prop_Model_RiderLsbsId.RIDER_TERM://818
                switch (riderNumber) {
//                    case 2:
//                        li_temp = 10;
//                        if (proposalProductModel.getLsbs_id() == 120 || proposalProductModel.getLsbs_id() == 202)
//                            li_temp = 70;
//                        if (ai_th <= li_temp) {
////                            double up_term = riderModel.getUp();
////                            if (proposalProductModel.getLsbs_id() == 128) {
////                                up_term = proposalProductModel.getUp();
////                            }
//
//                            ldec_temp = FormatNumber.round(((up_term / 1000) * ldec_rate) * 0.1, 2);
//                            ldec_total += ldec_temp;
//                        }
//                        break;
                    default:
                        break;
                }
                break;
            case Prop_Model_RiderLsbsId.RIDER_HCP_FAMILY://819
                ldec_temp = FormatNumber.round(ldec_rate * 0.1, 2);
                ldec_total += ldec_temp;

                listRateTambahan = select.selectListRiderRatePesertaTambahan(params);
                Log.e(TAG, "listRateTambahan: " + listRateTambahan);
                Log.e(TAG, "params: " + params);
                for (Double rate : listRateTambahan) {
                    ldec_temp = FormatNumber.round(rate * 0.09, 2);
                    ldec_total += ldec_temp;
                }

                break;
            case Prop_Model_RiderLsbsId.RIDER_EKA_SEHAT://823
                ldec_temp = FormatNumber.round((ldec_rate) * 0.12, 2);
                ldec_total += ldec_temp;

                listRateTambahan = select.selectListRiderRatePesertaTambahan(params);
                for (Double rate : listRateTambahan) {
                    ldec_temp = FormatNumber.round(rate * 0.117, 2);
                    ldec_total += ldec_temp;
                }

                break;
            case Prop_Model_RiderLsbsId.RIDER_EKA_SEHAT_INNER_LIMIT://825
                ldec_temp = FormatNumber.round((ldec_rate) * 0.12, 2);
                ldec_total += ldec_temp;

                listRateTambahan = select.selectListRiderRatePesertaTambahan(params);
                for (Double rate : listRateTambahan) {
                    ldec_temp = FormatNumber.round(rate * 0.117, 2);
                    ldec_total += ldec_temp;
                }
                break;
            case Prop_Model_RiderLsbsId.RIDER_HCP_PROVIDER://826
                ldec_temp = FormatNumber.round(ldec_rate * 0.1, 2);
                ldec_total += ldec_temp;

                listRateTambahan = select.selectListRiderRatePesertaTambahan(params);
                for (Double rate : listRateTambahan) {
                    ldec_temp = FormatNumber.round(rate * 0.09, 2);
                    ldec_total += ldec_temp;
                }
                break;
//            case Prop_Model_RiderLsbsId.RIDER_WAIVER_TPD_CI://827
//                ldec_temp = FormatNumber.round((((proposalProductModel.getPremi()) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                ldec_total += ldec_temp;
//                break;
//            case Prop_Model_RiderLsbsId.RIDER_PAYOR_TPD_CI_DEATH://828
//                ldec_temp = FormatNumber.round(((((proposalProductModel.getPremi())) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                ldec_total += ldec_temp;
//                break;
//            case Prop_Model_RiderLsbsId.RIDER_LADIES_INSURANCE://830
//                switch (proposalProductModel.getLku_id()) {
//                    case "01":
//                        ldec_max = 2000000000;
//                        break;
//                    case "02":
//                        ldec_max = 200000;
//                        break;
//                    default:
//                        break;
//                }
//                persentase = BigDecimal.valueOf(riderModel.getPersen_up()).divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_UP).doubleValue();
//                maxAmount = MathUtil.min(proposalProductModel.getUp() * persentase, ldec_max);
//                ldec_temp = FormatNumber.round(((maxAmount / 1000) * ldec_rate) * 0.1, 2);
//                ldec_total += ldec_temp;
//                break;
            case Prop_Model_RiderLsbsId.RIDER_HCP_LADIES://831 //
                listRateTambahan = select.selectListRiderRatePesertaTambahan(params);

                if (listRateTambahan.size() == 0) {
                    ldec_temp = FormatNumber.round(ldec_rate * 0.1, 2);
                    ldec_total += ldec_temp;
                } else {
                    int i = 1;
                    for (Double rate : listRateTambahan) {
                        switch (i) {
                            case 1:
                                ldec_temp = FormatNumber.round(rate * 0.1, 2);
                                break;
                            default:
                                ldec_temp = FormatNumber.round(rate * 0.09, 2);
                                break;
                        }
                        ldec_total += ldec_temp;
                        i++;
                    }
                }
                break;
            case Prop_Model_RiderLsbsId.RIDER_LADIES_MED_EXPENSE://832
                listRateTambahan = select.selectListRiderRatePesertaTambahan(params);

                if (listRateTambahan.size() == 0) {
                    ldec_temp = FormatNumber.round(ldec_rate * 0.12, 2);
                    ldec_total += ldec_temp;
                } else {
                    int i = 1;
                    for (Double rate : listRateTambahan) {
                        switch (i) {
                            case 1:
                                ldec_temp = FormatNumber.round(rate * 0.12, 2);
                                break;
                            default:
                                ldec_temp = FormatNumber.round(rate * 0.117, 2);
                                break;
                        }
                        ldec_total += ldec_temp;
                        i++;
                    }
                }
                break;
            case Prop_Model_RiderLsbsId.RIDER_LADIES_MED_EXPENSE_INNER_LIMIT://833
                listRateTambahan = select.selectListRiderRatePesertaTambahan(params);

                if (listRateTambahan.size() == 0) {
                    ldec_temp = FormatNumber.round(ldec_rate * 0.12, 2);
                    ldec_total += ldec_temp;
                } else {
                    int i = 1;
                    for (Double rate : listRateTambahan) {
                        switch (i) {
                            case 1:
                                ldec_temp = FormatNumber.round(rate * 0.12, 2);
                                break;
                            default:
                                ldec_temp = FormatNumber.round(rate * 0.117, 2);
                                break;
                        }
                        ldec_total += ldec_temp;
                        i++;
                    }
                }
                break;
            case Prop_Model_RiderLsbsId.RIDER_SCHOLARSHIP://835
                HashMap<String, Object> paramsRate = new HashMap<String, Object>();
                paramsRate.put("lsbsId", "835");
                paramsRate.put("liJenis", riderModel.getLsdbs_number());
                paramsRate.put("liUsia", li_usia);
                paramsRate.put("kursId", proposalProductModel.getLku_id());
//                TODO: cari query selectTableFaktor dibawah
//                BigDecimal rate_factor = eproposalManager.selectTableFactor(paramsRate);

//                ldec_temp = FormatNumber.round(((ldec_rate / 1000) * propModelMstProposalProductRider.getUp() * rate_factor_double) * 0.1, 2);
//                ldec_total += ldec_temp;
                break;
            case Prop_Model_RiderLsbsId.RIDER_BABY://836
                break;
//            case Prop_Model_RiderLsbsId.RIDER_EARLY_STAGE_CI99: //837
//                persentase = BigDecimal.valueOf(riderModel.getPersen_up()).divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_UP).doubleValue();
//                double upEsci99 = (proposalProductModel.getUp() * persentase);
//                double maxUp;
//                switch (proposalProductModel.getLku_id()) {
//                    case "01":
//                        maxUp = 2000000000.0;
//                        break;
//                    case "02:":
//                        maxUp = 200000.0;
//                        break;
//                    default:
//                        maxUp = 0.0;
//                        break;
//                }
//
//                double temp = MathUtil.min(upEsci99, maxUp);
//                ldec_temp = FormatNumber.round(((ldec_rate / 1000) * temp) * 0.1, 2);
//                ldec_total += ldec_temp;
//                break;
            default:
                break;

        }
        return ldec_total;
    }
}
