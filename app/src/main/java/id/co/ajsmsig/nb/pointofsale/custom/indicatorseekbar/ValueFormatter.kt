package id.co.ajsmsig.nb.pointofsale.custom.indicatorseekbar

/**
 * Created by andreyyoshuamanik on 02/03/18.
 */
interface ValueFormatter {

    fun format(progress: Float): String = progress.toInt().toString()
}