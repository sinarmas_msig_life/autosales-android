package id.co.ajsmsig.nb.espaj.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelJnsDanaDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelJnsDanaDI;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.method.ValueView;
import id.co.ajsmsig.nb.prop.model.DropboxModel;

/**
 * Created by Eriza on 9/30/2017.
 */

public class ListDanaAdapter extends RecyclerView.Adapter<ListDanaAdapter.ViewHolder> {
    private ArrayList<ModelJnsDanaDI> lst_Dana;
    private int resourceId;
    private Context context;

    public ArrayList<ModelJnsDanaDI> getlst_Dana() {
        return lst_Dana;
    }

    public ListDanaAdapter(Context context, int resourceId, ArrayList<ModelJnsDanaDI> lst_Dana) {
        this.context = context;
        this.lst_Dana = new ArrayList<>();
        this.lst_Dana.addAll(lst_Dana);
        this.resourceId = resourceId;

    }

    public void updateListTP(ArrayList<ModelJnsDanaDI> newList) {
        lst_Dana.clear();
        lst_Dana.addAll(newList);
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.p_fragment_list_peserta_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelJnsDanaDI list = lst_Dana.get(position);
        holder.tv_header.setText("Jenis Dana ke-" + (position + 1));
        String Dana = new E_Select(context).getNamaFund(list.getJenis_dana());

        String[][] values = new String[][]{
                new String[]{context.getString(R.string.jenis_dana_investasi), Dana},
                new String[]{context.getString(R.string.persentase), new BigDecimal(list.getPersen_alokasi()).toString()}
        };
        new ValueView(context).addToLinearLayout(holder.ll_tertanggung, values);
    }

    @Override
    public int getItemCount() {
        return lst_Dana.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_header)
        TextView tv_header;
        @BindView(R.id.ll_tertanggung)
        LinearLayout ll_tertanggung;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
