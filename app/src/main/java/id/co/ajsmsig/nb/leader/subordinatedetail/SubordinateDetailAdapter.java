package id.co.ajsmsig.nb.leader.subordinatedetail;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.leader.subordinate.Subordinate;

public class SubordinateDetailAdapter extends RecyclerView.Adapter<SubordinateDetailAdapter.MyViewHolder> {

    private OnItemPressed listener;
    private ArrayList<Subordinate> subordinateList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView agentName;
        public ConstraintLayout constraintLayoutDetail;
        public TextView tvAgentInitialNameDetail;

        public MyViewHolder(View view) {
            super(view);
            agentName = view.findViewById(R.id.tvAgentName);
            constraintLayoutDetail = view.findViewById(R.id.constraintLayoutDetail);
            tvAgentInitialNameDetail = view.findViewById(R.id.tvAgentInitialNameDetail);
        }

    }

    public SubordinateDetailAdapter(ArrayList<Subordinate> subordinateList, OnItemPressed listener) {
        this.subordinateList = subordinateList;
        this.listener = listener;
    }

    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_subordinate_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Subordinate model = subordinateList.get(position);
        holder.agentName.setText(model.getNAMAREFF());
        holder.tvAgentInitialNameDetail.setText(model.getInitialName());
        holder.constraintLayoutDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSubordinatePressed(holder.getAdapterPosition(), model);
            }
        });
    }


    @Override
    public int getItemCount() {
        return subordinateList.size();
    }

    public void refreshSubordinate(ArrayList<Subordinate> subordinates) {
        //Hapus list yg lama
        subordinateList.clear();
//            //Update dengan data yg baru
        subordinateList = subordinates;
//            //Refresh recyclerview
        notifyDataSetChanged();
    }

    interface OnItemPressed {
        void onSubordinatePressed(int position, Subordinate subordinate);
    }
}
