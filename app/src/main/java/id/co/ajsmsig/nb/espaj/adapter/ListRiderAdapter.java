package id.co.ajsmsig.nb.espaj.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.fragment.E_UsulanAsuransiFragment;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAddRiderUA;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;

/**
 * Created by Eriza on 9/30/2017.
 */

public class ListRiderAdapter extends RecyclerView.Adapter<ListRiderAdapter.ViewHolder> {
    private ArrayList<ModelAddRiderUA> list_Rider;
    private int resourceId;
    private Context context;
    private E_UsulanAsuransiFragment usulanAsuransi;
    private EspajModel me;

    public ArrayList<ModelAddRiderUA> getList_Rider() {
        return list_Rider;
    }

    public ListRiderAdapter(Context context, int resourceId, ArrayList<ModelAddRiderUA> list_Rider, EspajModel me, E_UsulanAsuransiFragment usulanAsuransi) {
        this.context = context;
        this.list_Rider = new ArrayList<>();
        this.list_Rider.addAll(list_Rider);
        this.resourceId = resourceId;
        this.usulanAsuransi = usulanAsuransi;
        this.me = me;

    }

    public void updateListTP(ArrayList<ModelAddRiderUA> newList) {
        list_Rider.clear();
        list_Rider.addAll(newList);
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.e_adapter_rider, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelAddRiderUA list = list_Rider.get(position);
        holder.tv_header.setText("Data Rider ke-" + (position + 1));
//        holder.btn_lanjut.setVisibility(View.GONE);
//        holder.btn_cancel.setVisibility(View.GONE);
        holder.cl_peserta.setVisibility(View.GONE);
        disable(holder.cl_peserta);
        disable(holder.cl_form_info_asuransi_tambahan);

        holder.ac_jns_produk.setText(new E_Select(context).getNamaProduk(list.getKode_produk_rider()));
        holder.iv_circle_jns_produk.setVisibility(View.GONE);
        holder.ac_jns_rider.setText(new E_Select(context).getNamaSubProduk(list.getKode_produk_rider(), list.getKode_subproduk_rider()));
        holder.iv_circle_jns_rider.setVisibility(View.GONE);

        if (list.getPersentase_rider() != 0) {
            holder.et_persentase.setText(String.valueOf(list.getPersentase_rider()));
            holder.cl_persentase.setVisibility(View.VISIBLE);
            holder.iv_circle_persentase.setVisibility(View.GONE);
        }

        if (list.getUnit_rider() != 0) {
            holder.et_unit.setText(String.valueOf(list.getUnit_rider()));
            holder.cl_unit.setVisibility(View.VISIBLE);
            holder.et_klas.setText(String.valueOf(list.getKlas_rider()));
            holder.cl_klas.setVisibility(View.VISIBLE);
            holder.iv_circle_unit.setVisibility(View.GONE);
        }

        if (list.getPeserta_askes().equals("")) {
            holder.cl_peserta.setVisibility(View.GONE);
        } else {
            holder.cl_peserta.setVisibility(View.VISIBLE);
            holder.et_nama.setText(list.getPeserta_askes());
            holder.et_tinggi.setText(String.valueOf(list.getTinggi_askes()));
            holder.et_bb.setText(String.valueOf(list.getBerat_askes()));
            holder.et_usia.setText(String.valueOf(list.getUsia_askes()));
            holder.et_tgl_lahir.setText(String.valueOf(list.getTtl_askes()));
            holder.et_pekerjaan.setText(list.getPekerjaan_askes());

            String tertanggung = new E_Select(context).getSubPesertaI(list.getKode_produk_rider(), list.getKode_subproduk_rider());
            if (tertanggung.contains("(TERTANGGUNG I)")) {
                ArrayList<ModelDropdownInt> ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_relasi, 1);
                holder.ac_hubungan_dengan_pp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, me.getPemegangPolisModel().getHubungan_pp()));
            } else {
                ArrayList<ModelDropdownInt> ListDropDownSPAJ = MethodSupport.getXML(context, R.xml.e_relasi, 1);
                holder.ac_hubungan_dengan_pp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, list.getHubungan_askes()));
            }
            ArrayList<ModelDropdownInt> ListDropDownSPAJ2 = MethodSupport.getXML(context, R.xml.e_warganegara, 0);
            holder.ac_warganegara.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ2, list.getWarganegara_askes()));

            MethodSupport.OnsetTwoRadio(holder.rg_jeniskel, list.getJekel_askes());

            holder.iv_menurider_dp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupMenu popup = new PopupMenu(view.getContext(), view);
                    popup.getMenuInflater().inflate(R.menu.menu_edit, popup.getMenu());
                    popup.show();
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            switch (menuItem.getItemId()) {
                                case R.id.edit:
                                    usulanAsuransi.goToForm(holder.getAdapterPosition());
                                    break;
                            }
                            return true;
                        }
                    });
                }
            });

        }


    }

    @Override
    public int getItemCount() {
        return list_Rider.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.cl_nama)
        RelativeLayout cl_nama;
        @BindView(R.id.et_nama)
        EditText et_nama;
        @BindView(R.id.iv_circle_nama)
        ImageView iv_circle_nama;
        @BindView(R.id.tv_error_nama)
        TextView tv_error_nama;

        @BindView(R.id.cl_jenis_kelamin)
        RelativeLayout cl_jenis_kelamin;
        @BindView(R.id.iv_circle_jenis_kelamin)
        ImageView iv_circle_jenis_kelamin;
        @BindView(R.id.rg_jeniskel)
        RadioGroup rg_jeniskel;
        @BindView(R.id.rb_pria)
        RadioButton rb_pria;
        @BindView(R.id.rb_wanita)
        RadioButton rb_wanita;
        @BindView(R.id.tv_error_jenis_kelamin)
        TextView tv_error_jenis_kelamin;

        @BindView(R.id.cl_tgl_lahir)
        RelativeLayout cl_tgl_lahir;
        @BindView(R.id.iv_circle_tgl_lahir)
        ImageView iv_circle_tgl_lahir;
        @BindView(R.id.et_tgl_lahir)
        EditText et_tgl_lahir;
        @BindView(R.id.tv_error_tgl_lahir)
        TextView tv_error_tgl_lahir;
        @BindView(R.id.iv_menurider_dp)
        ImageView iv_menurider_dp;

        @BindView(R.id.cl_usia)
        RelativeLayout cl_usia;
        @BindView(R.id.iv_circle_usia)
        ImageView iv_circle_usia;
        @BindView(R.id.et_usia)
        EditText et_usia;
        @BindView(R.id.tv_error_usia)
        TextView tv_error_usia;

        @BindView(R.id.cl_hubungan_dengan_pp)
        RelativeLayout cl_hubungan_dengan_pp;
        @BindView(R.id.iv_circle_hubungan_dengan_pp)
        ImageView iv_circle_hubungan_dengan_pp;
        @BindView(R.id.ac_hubungan_dengan_pp)
        AutoCompleteTextView ac_hubungan_dengan_pp;
        @BindView(R.id.tv_error_hubungan_dengan_pp)
        TextView tv_error_hubungan_dengan_pp;

        @BindView(R.id.cl_jns_produk)
        RelativeLayout cl_jns_produk;
        @BindView(R.id.iv_circle_jns_produk)
        ImageView iv_circle_jns_produk;
        @BindView(R.id.ac_jns_produk)
        AutoCompleteTextView ac_jns_produk;
        @BindView(R.id.tv_error_jns_produk)
        TextView tv_error_jns_produk;

        @BindView(R.id.cl_jns_rider)
        RelativeLayout cl_jns_rider;
        @BindView(R.id.iv_circle_jns_rider)
        ImageView iv_circle_jns_rider;
        @BindView(R.id.ac_jns_rider)
        AutoCompleteTextView ac_jns_rider;
        @BindView(R.id.tv_error_jns_rider)
        TextView tv_error_jns_rider;

        @BindView(R.id.cl_unit)
        RelativeLayout cl_unit;
        @BindView(R.id.iv_circle_unit)
        ImageView iv_circle_unit;
        @BindView(R.id.et_unit)
        EditText et_unit;
        @BindView(R.id.tv_error_unit)
        TextView tv_error_unit;

        @BindView(R.id.cl_klas)
        RelativeLayout cl_klas;
        @BindView(R.id.et_klas)
        EditText et_klas;
        @BindView(R.id.tv_error_klas)
        TextView tv_error_klas;

        @BindView(R.id.cl_persentase)
        RelativeLayout cl_persentase;
        @BindView(R.id.et_persentase)
        EditText et_persentase;
        @BindView(R.id.iv_circle_persentase)
        ImageView iv_circle_persentase;
        @BindView(R.id.tv_error_persentase)
        TextView tv_error_persentase;

        @BindView(R.id.cl_bb)
        RelativeLayout cl_bb;
        @BindView(R.id.et_bb)
        EditText et_bb;
        @BindView(R.id.iv_circle_bb)
        ImageView iv_circle_bb;
        @BindView(R.id.tv_error_bb)
        TextView tv_error_bb;

        @BindView(R.id.cl_tinggi)
        RelativeLayout cl_tinggi;
        @BindView(R.id.et_tinggi)
        EditText et_tinggi;
        @BindView(R.id.tv_error_tinggi)
        TextView tv_error_tinggi;

        @BindView(R.id.cl_pekerjaan)
        RelativeLayout cl_pekerjaan;
        @BindView(R.id.et_pekerjaan)
        EditText et_pekerjaan;
        @BindView(R.id.iv_circle_pekerjaan)
        ImageView iv_circle_pekerjaan;
        @BindView(R.id.tv_error_pekerjaan)
        TextView tv_error_pekerjaan;

        @BindView(R.id.cl_warganegara)
        RelativeLayout cl_warganegara;
        @BindView(R.id.iv_circle_warganegara)
        ImageView iv_circle_warganegara;
        @BindView(R.id.ac_warganegara)
        AutoCompleteTextView ac_warganegara;
        @BindView(R.id.tv_error_warganegara)
        TextView tv_error_warganegara;

        @BindView(R.id.tv_header)
        TextView tv_header;
        @BindView(R.id.cl_form_info_asuransi_tambahan)
        RelativeLayout cl_form_info_asuransi_tambahan;
        @BindView(R.id.cl_peserta)
        RelativeLayout cl_peserta;

        int position = 0;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            position = getAdapterPosition();
            usulanAsuransi.goToForm(position);

        }
    }

    private void disable(ViewGroup layout) {
        layout.setEnabled(false);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                disable((ViewGroup) child);
            } else {
                child.setEnabled(false);
            }
        }
    }
}
