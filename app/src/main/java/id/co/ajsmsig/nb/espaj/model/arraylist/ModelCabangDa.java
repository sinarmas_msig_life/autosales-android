package id.co.ajsmsig.nb.espaj.model.arraylist;

/**
 * Created by eriza on 14/11/2017.
 */

public class ModelCabangDa {
    private String NAMA_CABANG = "";
    private String LCB_NO = "";

    public ModelCabangDa() {
    }

    public ModelCabangDa(String NAMA_CABANG, String LCB_NO) {
        this.NAMA_CABANG = NAMA_CABANG;
        this.LCB_NO = LCB_NO;
    }

    public String getNAMA_CABANG() {
        return NAMA_CABANG;
    }

    public void setNAMA_CABANG(String NAMA_CABANG) {
        this.NAMA_CABANG = NAMA_CABANG;
    }

    public String getLCB_NO() {
        return LCB_NO;
    }

    public void setLCB_NO(String LCB_NO) {
        this.LCB_NO = LCB_NO;
    }

    public String toString() {
        return NAMA_CABANG;
    }
}
