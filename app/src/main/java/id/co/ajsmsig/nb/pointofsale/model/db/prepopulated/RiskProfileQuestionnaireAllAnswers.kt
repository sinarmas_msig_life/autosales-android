package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.*
import android.databinding.ObservableBoolean

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */
class RiskProfileQuestionnaireAllAnswers {

        @Embedded
        var question: RiskProfileQuestionnaire? = null

        @Relation(entity = RiskProfileQuestionnaire::class, parentColumn = "id", entityColumn = "parentId")
        var answers: List<RiskProfileQuestionnaire>? = null

        @Ignore
        var selected: ObservableBoolean = ObservableBoolean(false)
}