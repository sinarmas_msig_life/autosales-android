package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTTD;

/**
 * Created by rizky_c on 30/09/2017.
 */

public class TableTTD {

    private Context context;

    public TableTTD(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(ModelTTD me, int counter, String SPAJ_ID_TAB) {
        ContentValues cv = new ContentValues();
        cv.put("SPAJ_ID_TAB", SPAJ_ID_TAB);
        cv.put("COUNTER", counter);
        cv.put("FILE_TTD", me.getFile_ttd());
        cv.put("JUDUL_TTD", me.getJudul_ttd());
        cv.put("ID_TTD", me.getId_ttd());
        cv.put("NAMA_JELAS", me.getNama_jelas());
        cv.put("FLAG_DEFAULT", me.getFlag_default());
        cv.put("VALIDATION", me.getValidation());
        cv.put("FLAG_STATUS", me.getFlag_status());

        return cv;
    }
    public ArrayList<ModelTTD> getObjectArray(Cursor cursor) {

        ArrayList<ModelTTD> mes = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ModelTTD me = new ModelTTD();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("COUNTER"))) {
                    me.setCounter(cursor.getInt(cursor.getColumnIndexOrThrow("COUNTER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("FILE_TTD"))) {
                    me.setFile_ttd(cursor.getString(cursor.getColumnIndexOrThrow("FILE_TTD")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("JUDUL_TTD"))) {
                    me.setJudul_ttd(cursor.getString(cursor.getColumnIndexOrThrow("JUDUL_TTD")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("ID_TTD"))) {
                    me.setId_ttd(cursor.getString(cursor.getColumnIndexOrThrow("ID_TTD")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("NAMA_JELAS"))) {
                    me.setNama_jelas(cursor.getString(cursor.getColumnIndexOrThrow("NAMA_JELAS")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("FLAG_DEFAULT"))) {
                    me.setFlag_default(cursor.getInt(cursor.getColumnIndexOrThrow("FLAG_DEFAULT")));
                }
                if(!cursor.isNull(cursor.getColumnIndexOrThrow("VALIDATION"))){
                    int valid = cursor.getInt(cursor.getColumnIndexOrThrow("VALIDATION"));
//                boolean isValid = valid == 1 ? true : false;
                    boolean val;
                    val = valid == 1;
                    me.setValidation(val);
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("FLAG_STATUS"))) {
                    me.setFlag_status(cursor.getInt(cursor.getColumnIndexOrThrow("FLAG_STATUS")));
                }

                mes.add(me);
                cursor.moveToNext();
            }

            // always close the cursor
            cursor.close();
        }

        return mes;
    }
}