package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.ProductDetail


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface ProductDetailDao {

    @Query("select * from ProductDetail")
    fun loadAllProductDetails(): LiveData<List<ProductDetail>>

    @Query("select * from ProductDetail where productId = :productId")
    fun loadProductDetailWithProductId(productId: String?): LiveData<List<ProductDetail>>

}