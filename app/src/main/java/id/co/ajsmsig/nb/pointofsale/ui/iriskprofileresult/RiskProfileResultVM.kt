package id.co.ajsmsig.nb.pointofsale.ui.iriskprofileresult

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Observer
import android.databinding.ObservableField
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Analytics
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.RiskProfileScoring
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 03/03/18.
 */
class RiskProfileResultVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository, var sharedViewModel: SharedViewModel, val mainRepository: MainRepository): AndroidViewModel(application){

    var score: ObservableField<String> = ObservableField()
    var observableRiskProfile: MediatorLiveData<RiskProfileScoring> = MediatorLiveData()
    var riskProfileType: ObservableField<String> = ObservableField()
    var riskProfileDescription: ObservableField<String> = ObservableField()
    var riskProfileImageName: ObservableField<String> = ObservableField()

    init {
        var totalScore: Int = 0

        sharedViewModel.riskProfileQuestionnaireAnswers?.forEach { totalScore += it.value ?: 0 }

        score.set("$totalScore")

        val riskProfile = repository.getRiskProfileScoringFromScore(totalScore)
        riskProfile.observeForever(object : Observer<RiskProfileScoring> {
            override fun onChanged(t: RiskProfileScoring?) {
                t?.let {
                    riskProfileType.set(t.type)
                    riskProfileDescription.set(t.description)
                    riskProfileImageName.set(t.imageName)

                    observableRiskProfile.value = t


                    mainRepository.sendAnalytics(Analytics(
                            agentCode = sharedViewModel.agentCode,
                            agentName = sharedViewModel.agentName,
                            group = sharedViewModel.groupId,
                            event = "Risk Profile = ${it.type}",
                            timestamp = System.currentTimeMillis()
                    ))

                    riskProfile.removeObserver(this)
                }
            }

        })


    }
}