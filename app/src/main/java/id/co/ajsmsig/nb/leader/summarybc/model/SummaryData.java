
package id.co.ajsmsig.nb.leader.summarybc.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SummaryData implements Parcelable {

    private static int increment = 0;

    @SerializedName("msag_id")
    @Expose
    private int msagId;
    @SerializedName("inforce")
    @Expose
    private Integer inforce;
    @SerializedName("closing")
    @Expose
    private Integer closing;
    @SerializedName("agent_name")
    @Expose
    private String agentName;
    @SerializedName("submit")
    @Expose
    private Integer submit;
    @SerializedName("meet_and_presentation")
    @Expose
    private Integer meetAndPresentation;

    protected SummaryData(Parcel in) {
        agentName = in.readString();
        msagId = in.readInt();
        closing = in.readInt();
        submit = in.readInt();
        meetAndPresentation = in.readInt();
        inforce = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(msagId);
        dest.writeInt(inforce);
        dest.writeInt(closing);
        dest.writeInt(submit);
        dest.writeInt(meetAndPresentation);
        dest.writeString(agentName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SummaryData> CREATOR = new Creator<SummaryData>() {
        @Override
        public SummaryData createFromParcel(Parcel in) {
            return new SummaryData(in);
        }

        @Override
        public SummaryData[] newArray(int size) {
            return new SummaryData[size];
        }
    };

    public static DiffUtil.ItemCallback<SummaryData> DIFF_CALLBACK = new DiffUtil.ItemCallback<SummaryData>() {
        @Override
        public boolean areItemsTheSame(@NonNull SummaryData oldItem, @NonNull SummaryData newItem) {
            return oldItem.msagId == newItem.msagId;
        }

        @Override
        public boolean areContentsTheSame(SummaryData oldItem, SummaryData newItem) {
            return oldItem.equals(newItem);
        }
    };

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        SummaryData article = (SummaryData) obj;
        return article.msagId == this.msagId;
    }

    public int getMsagId() {
        return msagId;
    }

    public void setMsagId(int msagId) {
        this.msagId = msagId;
    }

    public Integer getInforce() {
        return inforce;
    }

    public void setInforce(Integer inforce) {
        this.inforce = inforce;
    }

    public Integer getClosing() {
        return closing;
    }

    public void setClosing(Integer closing) {
        this.closing = closing;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public Integer getSubmit() {
        return submit;
    }

    public void setSubmit(Integer submit) {
        this.submit = submit;
    }

    public Integer getMeetAndPresentation() {
        return meetAndPresentation;
    }

    public void setMeetAndPresentation(Integer meetAndPresentation) {
        this.meetAndPresentation = meetAndPresentation;
    }

}
