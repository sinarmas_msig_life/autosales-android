package id.co.ajsmsig.nb.leader.statusspaj;

import java.util.List;

public class StatusWithListSpajCount {

    private String statusName;
    private List<SpajData> statusSpajData;


    public StatusWithListSpajCount(String statusName, List<SpajData> statusSpajData) {
        this.statusName = statusName;
        this.statusSpajData = statusSpajData;
    }


    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public List<SpajData> getStatusSpajData() {
        return statusSpajData;
    }

    public void setStatusSpajData(List<SpajData> statusSpajData) {
        this.statusSpajData = statusSpajData;
    }

}
