/**
 * @author Eriza Siti Mulyani
 *
 */
package id.co.ajsmsig.nb.espaj.method;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

public class FormatNumber {

	/**
	 * Fungsi untuk pembulatan per desimal angka (xx angka dibelakang koma) (contoh: <b>round(3.1235, 2) = 3.12</b>)
	 * @param number nilai yang ingin dibulatkan
	 * @param decimalPlace jumlah desimal dibelakang koma
	 * @return Hasil setelah dibulatkan
	 */	
	public static double round(double number, int decimalPlace) {
		BigDecimal bd = new BigDecimal(number);
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}
	
	public static String StringCurency(double number) {
		NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.GERMANY);
//		DecimalFormat decimalFormatSymbols =(DecimalFormat) nf;
		String value = nf.format(number).trim();
		return (value.substring(0,value.lastIndexOf(",")));
	}

}
