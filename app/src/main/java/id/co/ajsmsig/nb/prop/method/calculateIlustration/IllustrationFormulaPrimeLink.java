package id.co.ajsmsig.nb.prop.method.calculateIlustration;

import android.content.Context;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.method.util.ArrUtil;
import id.co.ajsmsig.nb.prop.method.util.CommonUtil;
import id.co.ajsmsig.nb.prop.method.util.FormatNumber;
import id.co.ajsmsig.nb.prop.method.util.ProposalStringFormatter;
import id.co.ajsmsig.nb.prop.model.hardCode.Proposal;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.IllustrationResultVO;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.S_biaya;


public class IllustrationFormulaPrimeLink extends IllustrationFormula {

    public IllustrationFormulaPrimeLink(Context context) {
        super(context);

    }

    @Override
    public HashMap<String, Object> getIllustrationResult(String no_proposal) {
        IllustrationResultVO result = new IllustrationResultVO();
        ArrayList<LinkedHashMap<String, String>> mapList = new ArrayList<>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        ArrayList<HashMap<String, Object>> topupDrawList = getSelect().selectTopupAndWithdrawList(no_proposal);
        Integer defaultTopupDrawListSize = 50;
        String premiumTotal;
        String topup;
        String draw;
        
        int li_ke = 0, li_bagi = 1000, li_hal = 3;
        double[] ldec_bak ;
        double[] ldec_hasil_ppokok = new double[3 + 1];
        double[] ldec_hasil_ptu = new double[3 + 1];
        double[] ldec_hasil_wdraw = new double[3 + 1];
        double[] ldec_fph = {Proposal.DUMMY_ZERO, 0, 0, 0.9, 0.85, 0.8};
        double ldec_bawal = 100000;
        double[] ldec_man_non = new double[2 + 1];
        double ldec_bak_tu = 0.05;
        double ldec_bak_tut = 0.05;
        double ldec_akuisisi;
        double ldec_premi_invest;
        double[][] ldec_hasil_invest = new double[5 + 1][3 + 1];
        double[] ldec_tarik = { Proposal.DUMMY_ZERO, 0.04, 0.04, 0.03, 0.02, 0.01 };
        double ldec_wdraw;
        double[] ldec_premi_bulan = new double[12 + 1];
        double ldec_topup;
        double ldec_bass;
        double[][] ldec_bunga = new double[5 + 1][3 + 1];
        double[] ldec_bunga_avg = new double[3 + 1]; //= {0.06, 0.1, 0.08, 0.18, 0.12, 0.25} //{0.09, 0.06, 0.11, 0.15, 0.165, 0.25}  //fixed:0.09, 0.11, 0.15 (0.165); dynamic:0.08, 0.11, 0.18 (0.195)
        double ldec_fee = 0.020075;
        double ldec_coi = 0, ldec_mfc, ldec_sc, ldec_cost, ldec_ppokok, ldec_ptu, ldec_aph, ldec_ff, ldec_man_celaka, ldec_temp = 0; //, ldec_man[10+1] = {1, 1, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7}
        boolean[] lb_minus = { Proposal.DUMMY_FALSE, false, false, false };
        boolean lb_rider = false;
        String ls_sy = "", ls_temp; //ls_dpp = ' dari Premi Pokok', ls_sy = ''
        S_biaya lstr;
        double ldec_manfaat, ldec_premi_setahun = 0, ldec_coi24 = 0;
        ldec_mfc = 27500;
        
        Integer umur_tt = 0, bisnis_id = 0, bisnis_no = 0, ins_per = 0, lscb_kali=0, lscb_id = 0, lamaBayar = 0, cuti_premi = 0;
        double premi = 0, premi_komb = 0, premi_pokok = 0, up = 0;
        String kondisi_layak_jual = null, lscb_pay_mode = null, lku_id = "00", userMsagId = "";
        HashMap<String, Object> dataProposal = getSelect().selectDataProposal(no_proposal);
        if(!CommonUtil.isEmpty(dataProposal)) {
            bisnis_id = ((BigDecimal) dataProposal.get("LSBS_ID")).intValue();
            bisnis_no = ((BigDecimal) dataProposal.get("LSDBS_NUMBER")).intValue();
            umur_tt =  Integer.valueOf(dataProposal.get("UMUR_TT").toString());
            premi = Double.valueOf(dataProposal.get("PREMI").toString());
            premi_komb = Double.valueOf(dataProposal.get("PREMI_KOMB").toString());
            premi_pokok = Double.valueOf(dataProposal.get("PREMI_POKOK").toString());
            up =  Double.valueOf(dataProposal.get("UP").toString());
            ins_per =  Integer.valueOf(dataProposal.get("THN_MASA_KONTRAK").toString());
            kondisi_layak_jual = dataProposal.get("NAME_ELIGIBLE").toString();
            lscb_kali = Integer.valueOf(dataProposal.get("LSCB_KALI").toString());
            lscb_pay_mode = dataProposal.get("LSCB_PAY_MODE").toString();
            lscb_id = Integer.valueOf(dataProposal.get("LSCB_ID").toString());
            lku_id = dataProposal.get("LKU_ID").toString();
            cuti_premi = Integer.valueOf(dataProposal.get("THN_CUTI_PREMI").toString());
            lamaBayar = Integer.valueOf(dataProposal.get("THN_LAMA_BAYAR").toString());
            userMsagId = dataProposal.get("MSAG_ID").toString();
        }
        
        if(Proposal.CUR_USD_CD.equals(lku_id)){
            ldec_mfc = 2.75;
            li_bagi = 1;
        }
        for( int i = 1; i <= 12; i++ )
        {
            ldec_premi_bulan[ i ] = 0;
            if( i == 1 ) ldec_premi_bulan[ i ] = premi;
            if( Proposal.PAY_MODE_CD_TRIWULANAN == lscb_id )
            {
                if( i == 4 || i == 7 || i == 10 ) ldec_premi_bulan[ i ] = premi;
            }
            else if( Proposal.PAY_MODE_CD_SEMESTERAN == lscb_id )
            {
                if( i == 7 ) ldec_premi_bulan[ i ] = premi;
            }
            else if( Proposal.PAY_MODE_CD_BULANAN == lscb_id )
            {
                ldec_premi_bulan[ i ] = premi;
            }
            ldec_premi_setahun += ldec_premi_bulan[ i ];
        }
        //
        for(int i = 1 ; i <= 3 ; i++){
            ldec_hasil_invest[1][i] = 0;
            ldec_hasil_ppokok[i] = 0;
            ldec_hasil_ptu[i] = 0;
            ldec_hasil_wdraw[i] = 0;
        }
        //
        ldec_manfaat = up;
        //    Biaya Akuisisi & asumsi penarikan (1 tarik, 1-nya tdk tarik)
        lstr = getBiaya(bisnis_id, bisnis_no, no_proposal);
        ldec_bak = lstr.bak;
        ldec_bunga = lstr.bunga;
        
        ldec_bunga_avg = getSelect().selectBungaAvg( no_proposal );

        double[] np = new double[4];
        double[] celaka = new double[4];

        int j;
        ldec_fee = 0;
        for( int i = 1; i <= ins_per; i++ )
        {
//          surrender charge
            ldec_sc = 0;
            ldec_wdraw = 0;
            ldec_topup = 0;
            
            ldec_coi = getLdec_coi(dataProposal, i);  

//            if( istr_prop.cb_id == 0 ){ TODO: nanti coba cek disini klo beda
//                for(int k = 1; k <= ArrUtil.upperBound(istr_prop.rider_baru) ; k ++ ){
//                    if( istr_prop.rider_baru[k] > 0 ){
//                        ldec_temp = of_get_coi_134(k, i);
//                        ldec_coi += ldec_temp;
//                    }
//                }
//            }
            if(i <= ArrUtil.upperBound(lstr.topup)) ldec_topup = lstr.topup[i];
            if(i <= 5) ldec_sc = ldec_tarik[i];
            //
            ldec_cost = (ldec_coi + ldec_mfc);
            
            for(int k = 1 ; k <= 3; k++){
                if(i <= ArrUtil.upperBound(lstr.tarik)) ldec_wdraw = lstr.tarik[i];
                for(int li_bulan = 1 ; li_bulan <= 12 ; li_bulan++){
                    ldec_cost = (ldec_coi + ldec_mfc);
                    ldec_premi_invest = 0;
                    ldec_ppokok = 0;
                    ldec_ptu = 0;
                    ldec_ff = 0;
                    ldec_aph = 0;
                    if(i <= cuti_premi){
                        ldec_premi_invest = ldec_premi_bulan[li_bulan] * (premi_komb / 100);
                        ldec_premi_invest += ((ldec_premi_bulan[li_bulan] * (100 - premi_komb) / 100));
                        ldec_ppokok = ldec_premi_bulan[li_bulan] * (premi_komb / 100);
                        ldec_ptu = (ldec_premi_bulan[li_bulan] * (100 - premi_komb) / 100);                    
                    }
                    
                    if(li_bulan == 1) ldec_ptu += ldec_topup;
                    // Em@il Vito@18/05/2016=> Rumusan SIMAS PRIMELINK & SMiLe AssetPro(Agency)(134-X5/X6)
                    if(i == 1 && li_bulan == 1){
                        ldec_hasil_ppokok[k] = FormatNumber.round(( ldec_ppokok - ldec_cost ) * (Math.pow((1 + ldec_bunga_avg[k]),( ( double ) 1/12))), 2) ;
                    }else{
                        ldec_hasil_ppokok[k] = FormatNumber.round(( ldec_hasil_ppokok[k] - ldec_cost ) * (Math.pow((1 + ldec_bunga_avg[k]),( ( double ) 1/12))), 2) ;
                    }
                    ldec_hasil_ptu[k] = (ldec_ptu + ldec_hasil_ptu[k]) * (Math.pow((1 + ldec_bunga_avg[k]),( ( double ) 1/12)));
                    if(k == 1)
                    {
                        ldec_hasil_invest[1][k] = ldec_hasil_ppokok[k] + ldec_hasil_ptu[k];                     
                    }
                }

                    
                ldec_hasil_wdraw[k] += ldec_wdraw;
                ldec_hasil_invest[1][k] = ldec_hasil_ppokok[k] + ldec_hasil_ptu[k];
                ldec_hasil_invest[ 1 ][ k ] -= ( ldec_hasil_wdraw[k] * ( 1 + ldec_sc ) );
                 
                 
                if( ldec_hasil_invest[ 1 ][ k ] <= 0 &&  ( i <= 10 ) ){
                    lb_minus[k] = true;                
                }
            }
        

            //j = cepr01030101Form.getInsuredAge() + i;
            j = umur_tt + i;
            if( i <= 23 || j == 55 || j == 65 || j == 75 || j == 80 || j == 100 )
            {
                for( int k = 1; k <= 3; k++ )
                {
                    np[ k ] = FormatNumber.round( ldec_hasil_invest[ 1 ][ k ] / li_bagi, 0 );
                    celaka[ k ] = FormatNumber.round( ( ldec_hasil_invest[ 1 ][ k ] + ldec_manfaat ) / li_bagi, 0 );
                }

                if( i <= cuti_premi )
                {
                    premiumTotal = ProposalStringFormatter.convertToStringWithoutCent( ldec_premi_setahun / li_bagi );
                }
                else
                {
                    premiumTotal = "";
                }

                if( i < defaultTopupDrawListSize )
                {
                    topup = "0";
                    draw = "0";
                    
                    for(HashMap<String, Object> topupDraw : topupDrawList) {
                        Integer thn_ke = ((BigDecimal) topupDraw.get("THN_KE")).intValue();
                        if(i == thn_ke) {
                            BigDecimal topupAmount = (BigDecimal) topupDraw.get("TOPUP");
                            BigDecimal drawAmount = (BigDecimal) topupDraw.get("TARIK");
                            
                            topup = ProposalStringFormatter.convertToString(topupAmount.divide(new BigDecimal("1000")));
                            draw = ProposalStringFormatter.convertToString(drawAmount.divide(new BigDecimal("1000")));
                        }
                    }
                    
                    if( "0".equals( topup ) ) topup = "0.00";
                    if( "0".equals( draw ) ) draw = "0.00";
                }
                else
                {
                    topup = "0.00";
                    draw = "0.00";
                }

                String valueLow = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 1 ] ) );
                String valueMid = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 2 ] ) );
                String valueHi = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 3 ] ) );

                String benefitLow = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 1 ], np[ 1 ] );
                String benefitMid = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 2 ], np[ 2 ] );
                String benefitHi = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 3 ], np[ 3 ] );

                LinkedHashMap<String, String> map1 = new LinkedHashMap<>();
                map1.put( getContext().getString(R.string.yearNo), ProposalStringFormatter.convertToString( i ) );
                map1.put( getContext().getString(R.string.insuredAge), ProposalStringFormatter.convertToString( umur_tt + i ) );
                map1.put( getContext().getString(R.string.premiumTotal), premiumTotal );
                map1.put( getContext().getString(R.string.topupAssumption), topup );
                map1.put( getContext().getString(R.string.drawAssumption), draw );
                map1.put( getContext().getString(R.string.valueLow), valueLow );
                map1.put( getContext().getString(R.string.valueMid), valueMid );
                map1.put( getContext().getString(R.string.valueHi), valueHi );
                map1.put( getContext().getString(R.string.benefitLow), benefitLow );
                map1.put( getContext().getString(R.string.benefitMid), benefitMid );
                map1.put(getContext().getString(R.string.benefitHi), benefitHi);
                mapList.add( map1 );

            }
        }
        
        result.setValidityMsg( lb_minus[ 1 ] ? "Maaf, Proposal yang Anda buat Tidak Layak Jual." : "" );
        result.setIllustrationList( mapList );
        map.put("Illustration1", result);
        
        return map;
    }
    
}
