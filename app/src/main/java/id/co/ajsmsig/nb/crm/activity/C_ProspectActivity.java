package id.co.ajsmsig.nb.crm.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.fragment.C_ListActivityFragment;

public class C_ProspectActivity extends AppCompatActivity {
    private static final String TAG = C_ProspectActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_activity_prospect);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FragmentManager fragmentManager = getSupportFragmentManager();
        chooseFragmentPageProposal(fragmentManager, 1);
    }

    public void chooseFragmentPageProposal(FragmentManager fragmentManager, int position) {
        Fragment fragment;

        FragmentTransaction transaction;
        transaction = fragmentManager.beginTransaction();
        for (int i = 1; i <= position; i++) {
            switch (i) {
                case 1:
                    fragment = new C_ListActivityFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fl_container, fragment);
                    transaction.commit();
                    break;
            }
        }
//        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        } else {
            finish();
        }
    }

    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_prospect, menu);
//        return super.onCreateOptionsMenu(menu);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        int pageNum = pager.getCurrentItem();
//        switch (pageNum) {
//            case 0:
//                showMenuItem(menu, R.id.action_add_activity);
//                hideMenuItem(menu, R.id.action_add_proposal);
//                break;
//            case 1:
//                hideMenuItem(menu, R.id.action_add_activity);
//                showMenuItem(menu, R.id.action_add_proposal);
//                break;
//            default:
//                hideMenuItem(menu, R.id.action_add_activity);
//                hideMenuItem(menu, R.id.action_add_proposal);
//                break;
//        }
//        return super.onPrepareOptionsMenu(menu);
//    }

    private void hideMenuItem(Menu menu, int menuId) {
        MenuItem item = menu.findItem(menuId);
        item.setVisible(false);
    }

    private void showMenuItem(Menu menu, int menuId) {
        MenuItem item = menu.findItem(menuId);
        item.setVisible(true);
    }

}
