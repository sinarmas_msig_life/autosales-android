package id.co.ajsmsig.nb.prop.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import id.co.ajsmsig.nb.R;


public class P_AdapterListLinkIlustrasi extends ArrayAdapter<HashMap<String, String>> {
    private static final String TAG = "P_AdapterListLinkIlustrasi";
    private ArrayList<LinkedHashMap<String, String>> objects;
    private LayoutInflater layoutInflater;
    private int Resource;

    public P_AdapterListLinkIlustrasi(Context context, int resource, ArrayList<LinkedHashMap<String, String>> objects) {
        super(context, resource);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = resource;
        this.objects = objects;

    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {

        View v = convertView;
        ViewHolder holder;

        if (v == null) {
            holder = new ViewHolder();
            v = layoutInflater.inflate(Resource, null);
            holder.tv_isi_tahun = v.findViewById(R.id.tv_isi_tahun);
            holder.tv_isi_usia = v.findViewById(R.id.tv_isi_usia);
            holder.tv_isi_total_premi = v.findViewById(R.id.tv_isi_total_premi);
            holder.tv_isi_top_up = v.findViewById(R.id.tv_isi_top_up);
            holder.tv_isi_penarikan = v.findViewById(R.id.tv_isi_penarikan);
            holder.tv_isi_np_rendah = v.findViewById(R.id.tv_isi_np_rendah);
            holder.tv_isi_np_sedang = v.findViewById(R.id.tv_isi_np_sedang);
            holder.tv_isi_np_tinggi = v.findViewById(R.id.tv_isi_np_tinggi);
            holder.tv_isi_mm_rendah = v.findViewById(R.id.tv_isi_mm_rendah);
            holder.tv_isi_mm_sedang = v.findViewById(R.id.tv_isi_mm_sedang);
            holder.tv_isi_mm_tinggi = v.findViewById(R.id.tv_isi_mm_tinggi);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        HashMap<String, String> object =  getItem(position);

        assert object != null;
        holder.tv_isi_tahun.setText(object.get(getContext().getString(R.string.yearNo)));
        holder.tv_isi_usia.setText(object.get(getContext().getString(R.string.insuredAge)));
        holder.tv_isi_total_premi.setText(object.get(getContext().getString(R.string.premiumTotal)).replace(",", "."));

        String nilaiTopup = (object.get(getContext().getString(R.string.topupAssumption)).equals("0.00")) ? "0" : object.get(getContext().getString(R.string.topupAssumption));
        String nilaiDraw = (object.get(getContext().getString(R.string.drawAssumption)).equals("0.00")) ? "0" : object.get(getContext().getString(R.string.drawAssumption));

        holder.tv_isi_top_up.setText(nilaiTopup);
        holder.tv_isi_penarikan.setText(nilaiDraw);
        holder.tv_isi_np_rendah.setText(object.get(getContext().getString(R.string.valueLow)).replace(",", ".").replace(",", "."));
        holder.tv_isi_np_sedang.setText(object.get(getContext().getString(R.string.valueMid)).replace(",", "."));
        holder.tv_isi_np_tinggi.setText(object.get(getContext().getString(R.string.valueHi)).replace(",", "."));
        holder.tv_isi_mm_rendah.setText(object.get(getContext().getString(R.string.benefitLow)).replace(",", "."));
        holder.tv_isi_mm_sedang.setText(object.get(getContext().getString(R.string.benefitMid)).replace(",", "."));
        holder.tv_isi_mm_tinggi.setText(object.get(getContext().getString(R.string.benefitHi)).replace(",", "."));

        return v;

    }

    @Nullable
    @Override
    public HashMap<String, String> getItem(int position) {
        return objects.get(position);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    private static class ViewHolder {
        TextView tv_isi_tahun;
        TextView tv_isi_usia;
        TextView tv_isi_total_premi;
        TextView tv_isi_top_up;
        TextView tv_isi_penarikan;
        TextView tv_isi_np_rendah;
        TextView tv_isi_np_sedang;
        TextView tv_isi_np_tinggi;
        TextView tv_isi_mm_rendah;
        TextView tv_isi_mm_sedang;
        TextView tv_isi_mm_tinggi;
    }
}
