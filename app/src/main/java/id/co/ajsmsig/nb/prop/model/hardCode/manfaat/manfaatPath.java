package id.co.ajsmsig.nb.prop.model.hardCode.manfaat;
/*
 Created by faiz_f on 28/08/2017.
 */

import id.co.ajsmsig.nb.R;

public class manfaatPath {


    public static int getManfaatImagePath(int productCode, int plan){
        switch (productCode){
            case 208:
                switch (plan){
                    case 1:
                        return R.drawable.menu_proposal;
                    case 2:
                        return R.drawable.menu_proposal;
                    default:
                        throw new IllegalArgumentException("Untuk product ini belum di set");
                }
            case 801:
                switch (plan){
                    case 1:
                        return R.drawable.menu_proposal;
                    case 2:
                        return R.drawable.menu_proposal;
                    default:
                        throw new IllegalArgumentException("Untuk product ini belum di set");
                }
            default:
                throw new IllegalArgumentException("Untuk product ini belum di set");
        }
    }
}
