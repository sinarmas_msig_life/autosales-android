package id.co.ajsmsig.nb.crm.fragment.listspaj;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by Fajar.Azhar on 21/03/2018.
 */

public class ParentSPAJList extends ExpandableGroup<ChildSPAJList> {
    public ParentSPAJList(String groupTitle, List<ChildSPAJList> items) {
        super(groupTitle, items);
    }
}
