package id.co.ajsmsig.nb.pointofsale.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import id.co.ajsmsig.nb.di.AppController
import javax.inject.Singleton

/**
 * Created by andreyyoshuamanik on 26/02/18.
 */
@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    MainActivityModule::class,
    ServiceBuilderModule::class
])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(posApp: AppController)


}