package id.co.ajsmsig.nb.crm.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import id.co.ajsmsig.nb.BuildConfig;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.C_Delete;
import id.co.ajsmsig.nb.crm.database.C_Insert;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.fragment.C_RequestLoginDialogFragment;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.method.async.CrmRestClientUsage;
import id.co.ajsmsig.nb.crm.method.async.SpajApiRestClientUsage;
import id.co.ajsmsig.nb.crm.model.apiresponse.request.LoginRequest;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.LoginResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.userlogin.AGENTINFO;
import id.co.ajsmsig.nb.crm.model.apiresponse.userlogin.DATA;
import id.co.ajsmsig.nb.crm.model.apiresponse.userlogin.MENU;
import id.co.ajsmsig.nb.crm.model.apiresponse.userlogin.UserLoginResponse;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.prop.model.apimodel.request.ReserveVaRequest;
import id.co.ajsmsig.nb.prop.model.apimodel.response.ReserveVaResponse;
import id.co.ajsmsig.nb.util.AppConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 * Author: Faiz
 */
public class C_LoginActivity extends AppCompatActivity implements SpajApiRestClientUsage.AuthenticationListener {
    private final static String TAG = C_LoginActivity.class.getSimpleName();

    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.login_form)
    View login_form;
    @BindView(R.id.login_progress)
    View login_progress;

    public SpajApiRestClientUsage clientUsageGeneral;

//    private static final String TAG = C_LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Intent logIntent = new Intent(this, C_MainActivity.class);
        SharedPreferences sharedpreferences = getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
        if (sharedpreferences.getBoolean("IS_LOGIN", false)) {
            finish();
            startActivity(logIntent);
        }
//        else if (sharedpreferences.getBoolean("JENIS_LOGIN", false)) {
//            finish();
//            startActivity(log2Intent);
//        }
        else {
            ButterKnife.bind(this);
//            Button btn_sign_in = (Button) findViewById(R.id.btn_sign_in);
//            btn_sign_in.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    performLogin();
//                }
//            });
            findViewById(R.id.btn_sign_in).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    performLogin();
                }
            });
            findViewById(R.id.tv_request_login).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showRequestDialog();
                }
            });

        }

        clientUsageGeneral = new SpajApiRestClientUsage(this);
        clientUsageGeneral.setOnAuthenticationListener(this);
        printUsedEndpoint();
    }

    private void printUsedEndpoint() {
        Log.v("Endpoint", "Flavor : " + BuildConfig.FLAVOR);
        Log.v("Endpoint", "CRM Base URL " + AppConfig.getBaseUrlCRM());
        Log.v("Endpoint", "E-Proposal Base URL " + AppConfig.getBaseUrlEproposal());
        Log.v("Endpoint", "EWS Base URL " + AppConfig.getBaseUrlEWS());
        Log.v("Endpoint", "Proposal Base URL " + AppConfig.getBaseUrlProposal());
        Log.v("Endpoint", "SPAJ Bank Base URL " + AppConfig.getBaseUrlSPAJBank());
        Log.v("Endpoint", "SPAJ Base URL " + AppConfig.getBaseUrlSPAJ());
        Log.v("Endpoint", "WS Base URL " + AppConfig.getBaseUrlWs());
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void performLogin() {
        TextView tv_error_username = findViewById(R.id.tv_error_username);
        TextView tv_error_password = findViewById(R.id.tv_error_password);


        // Reset errors.
        tv_error_username.setVisibility(View.GONE);
        tv_error_password.setVisibility(View.GONE);

        // Store values at the time of the login attempt.
        String email = et_email.getText().toString();
        String password = et_password.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid password, if the user entered one.
        if (password.trim().length() == 0 && !isPasswordValid(password)) {
            tv_error_password.setText(getString(R.string.error_invalid_password));
            tv_error_password.setVisibility(View.VISIBLE);
            focusView = et_password;
            cancel = true;
        }

        // Check for a valid username.
        if (email.trim().length() == 0) {
            tv_error_username.setText(getString(R.string.error_field_required));
            tv_error_username.setVisibility(View.VISIBLE);
            focusView = et_email;
            cancel = true;
        }
//        else {
//            if (!email.contains("@") || !email.contains(".")) {
//                tv_error_username.setText(getString(R.string.error_invalid_email));
//                tv_error_username.setVisibility(View.VISIBLE);
//                focusView = et_email;
//                cancel = true;
//            }
//        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
//            mAuthTask = new UserLoginTask(email, password);
//            mAuthTask.execute(urlLogin);
//            SpajApiRestClientUsage ewsRestClientUsage = new SpajApiRestClientUsage();
//            ewsRestClientUsage.login(email, password);

//            clientUsageGeneral.performAuthentication(email, password);
            if(StaticMethods.isNetworkAvailable(this)){
                loginWith(email, password);
            }else {
                showDialogTryAgain(getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi));
                showProgress(false);
            }
        }
    }

    private void loginWith(String username, String password) {
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);

        //Make a POST request body
        LoginRequest loginRequest = new LoginRequest(username, password);
        String url = AppConfig.getBaseUrlEWS().concat("member/api/json/login");
        Call<List<UserLoginResponse>> call = api.loginWith(url, loginRequest);

        call.enqueue(new Callback<List<UserLoginResponse>>() {
            @Override
            public void onResponse(Call<List<UserLoginResponse>> call, Response<List<UserLoginResponse>> serverResponse) {
                if (serverResponse.body() != null) {
                    List<UserLoginResponse> body = serverResponse.body();

                    if (body != null && !body.isEmpty()) {

                        UserLoginResponse response = body.get(0);

                        boolean error = response.getERROR();
                        String message = response.getMESSAGE();

                        DATA data = response.getDATA();

                        if (!error && data != null) {
                            if (data.getAGENTINFO().getJENISLOGINBC()== 0 && data.getAGENTINFO().getJENISLOGIN() == 9) { // validasi khusus BC
                                processUserLoginAsUnknownErrorInformation();
                            } else if (data.getAGENTINFO().getGROUPID()== null) {
                                processUserLoginAsUnknownErrorInformationDetail();
                            } else {
                                processUserLoginAsSuccess(data, message);
                            }
                        } else if (error && message.equalsIgnoreCase("Email atau password salah. Mohon coba kembali !!!")) {
                            processUserLoginAsAuthenticationError();
                        } else {
                            processUserLoginAsUnknownError(message);
                        }

                    }

                }
            }

            @Override
            public void onFailure(Call<List<UserLoginResponse>> call, Throwable t) {
                Log.e(TAG, "Login error "+t.getMessage());
                Toast.makeText(C_LoginActivity.this, "Terjadi kesalahan dalam login "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });
    }
    private void processUserLoginAsAuthenticationError() {
        showProgress(false);
        TextView tv_error_password = findViewById(R.id.tv_error_password);
        tv_error_password.setText(R.string.error_login);
        tv_error_password.setVisibility(View.VISIBLE);
        et_password.requestFocus();
    }
    private void processUserLoginAsAuthenticationErrorInformation() {
        showProgress(false);
        TextView tv_error_password = findViewById(R.id.tv_error_password);
        tv_error_password.setText(R.string.error_login_information);
        tv_error_password.setVisibility(View.VISIBLE);
        et_password.requestFocus();
    }
    private void processUserLoginAsUnknownError(String message) {
        new AlertDialog.Builder(C_LoginActivity.this)
                .setTitle("Login gagal")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    dialog.dismiss();
                    processUserLoginAsAuthenticationError();
                })
                .show();
    }
    private void processUserLoginAsUnknownErrorInformation() {
        new AlertDialog.Builder(C_LoginActivity.this)
                .setTitle("Login gagal")
                .setMessage("Kendala ini terjadi karena id anda belum di aktifkan, mohon untuk email ke iriana@sinarmasmsiglife.co.id, "
                        +"untuk pengaktifan kode agen agar bisa akses autosales."
                        +"\n\n"+"Mohon untuk cc email ke itbcsupport@sinarmasmsiglife.co.id")
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    dialog.dismiss();
                    processUserLoginAsAuthenticationErrorInformation();
                })
                .show();
    }
    private void processUserLoginAsUnknownErrorInformationDetail() {
        new AlertDialog.Builder(C_LoginActivity.this)
                .setTitle("Aktivasi Login")
                .setMessage("Akun Autosales Anda belum diaktifkan, "
                        +"Mohon untuk email ke itbcsupport@sinarmasmsiglife.co.id")
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    dialog.dismiss();
                    processUserLoginAsAuthenticationErrorInformation();
                })
                .show();
    }

    private void processUserLoginAsSuccess(DATA data, String message) {
        if (data != null) {

            List<MENU> listMenu = data.getMENU();
            AGENTINFO agentInfo = data.getAGENTINFO();

            String msagId = data.getAGENTINFO().getAGENCODE();
            int roleId = data.getROLEID();

            SharedPreferences sharedpreferences = getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putInt("ROLE_ID", roleId);
            editor.apply();

            saveUserLoginInfoToSession(agentInfo);
            saveUserMenu(listMenu, msagId);
        }
    }

    private void saveUserLoginInfoToSession(AGENTINFO agentInfo) {


        String regionalCode1 = agentInfo.getREGIONALCODE1();
        String regionalCode2 = agentInfo.getREGIONALCODE2();
        String regionalCode3 = agentInfo.getREGIONALCODE3();
        int groupId = agentInfo.getGROUPID();
        String regionalName = agentInfo.getREGIONALNAME();
        int flagAdmin = agentInfo.getFLAGADMIN();
        String agentName = agentInfo.getAGENNAME();
        int jenisLoginBc = agentInfo.getJENISLOGINBC();
        String bankName = agentInfo.getBANKNAME();
        String regionalCode = agentInfo.getREGIONALCODE();
        int jenisLogin = agentInfo.getJENISLOGIN();
        String leaderCode = agentInfo.getLEADERCODE();
        String agentCode = agentInfo.getAGENCODE();
        String email = agentInfo.getAgentEmail();

        SharedPreferences sharedpreferences = getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("IS_LOGIN", true);
        editor.putString("USERNAME", et_email.getText().toString());
        editor.putString("PASSWORD", et_password.getText().toString());
        editor.putString("NAMA_AGEN", agentName);
        editor.putString("KODE_AGEN", agentCode);
        editor.putString("NAMA_REG", regionalName);
        editor.putString("KODE_LEADER", leaderCode);
        editor.putString("KODE_REG1", regionalCode1);
        editor.putString("KODE_REG2", regionalCode2);
        editor.putString("KODE_REG3", regionalCode3);
        editor.putString("KODE_REGIONAL", regionalCode1 + regionalCode2 + regionalCode3);
        editor.putInt("JENIS_LOGIN", jenisLogin);
        if (jenisLoginBc == 16 || jenisLoginBc == 2) {
            editor.putInt("JENIS_LOGIN_BC", 2);
        } else {
            editor.putInt("JENIS_LOGIN_BC", jenisLoginBc);
        }
        editor.putString("NAMA_BANK", bankName);
        editor.putInt("FLAG_ADMIN", flagAdmin);
        editor.putString("EMAIL", email);
        editor.putInt("GROUP_ID", groupId);
        editor.putInt(getString(R.string.SL_APP), getSL_APP(jenisLoginBc));
        editor.apply();

        Intent logIntent = new Intent(C_LoginActivity.this, C_MainActivity.class);
        startActivity(logIntent);
        showProgress(false);
        downloadActivityAndSubActivity(agentCode, jenisLoginBc);
        finish();
    }

    private void downloadActivityAndSubActivity(String agentCode, int jenisLoginBc) {
        if (StaticMethods.isNetworkAvailable(C_LoginActivity.this)) {

            if (!TextUtils.isEmpty(agentCode)) {
                CrmRestClientUsage crm = new CrmRestClientUsage(C_LoginActivity.this);
                crm.getActivityTypeForDropdown(agentCode, jenisLoginBc);
            }
        }
    }

    /**
     *
     * @param listMenu user latest menu list
     * @param agentCode = msag id
     */
    private void saveUserMenu(List<MENU> listMenu, String agentCode) {
        C_Insert insert = new C_Insert(C_LoginActivity.this);
        C_Select select = new C_Select(C_LoginActivity.this);
        C_Delete delete = new C_Delete(C_LoginActivity.this);

        int menuCount = select.getMenuCountForUser(agentCode);

        if (!listMenu.isEmpty()) {

            //There are no menu count for this user, add the menu list to the database
            if (menuCount == 0) {
                //Save user menu to database
                for (MENU menu : listMenu) {
                    String menuId = menu.getMENUID();
                    String menuName = menu.getMENUNAME();
                    insert.insertToTableLstUserMenu(menuId, menuName, agentCode);
                }

            } else {
                //User already have menu list on the database, delete the old data, replace it with the latest menu
                delete.removeAllMenuListFromUser(agentCode);

                //Save the latest menu to database
                for (MENU menu : listMenu) {
                    String menuId = menu.getMENUID();
                    String menuName = menu.getMENUNAME();
                    insert.insertToTableLstUserMenu(menuId, menuName, agentCode);
                }
            }

        }
    }

    private void showRequestDialog() {
        Bundle bundle = new Bundle();
        DialogFragment requestLoginDialogFragment = C_RequestLoginDialogFragment.newInstance(bundle);
        requestLoginDialogFragment.show(getSupportFragmentManager(), "dialog");
    }


    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        login_form.setVisibility(show ? View.GONE : View.VISIBLE);
        login_form.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                login_form.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        login_progress.setVisibility(show ? View.VISIBLE : View.GONE);
        login_progress.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                login_progress.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onAuthSuccess(int statusCode, Header[] headers, JSONArray response) {
        Log.d(TAG, "onSuccess: " + response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(0);
            SharedPreferences sharedpreferences = getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putBoolean("IS_LOGIN", true);
            editor.putString("USERNAME", et_email.getText().toString());
            editor.putString("PASSWORD", et_password.getText().toString());
            editor.putString("NAMA_AGEN", jsonObject.optString("NAMA_AGEN"));
            editor.putString("KODE_AGEN", jsonObject.optString("KODE_AGEN"));
            editor.putString("NAMA_REG", jsonObject.optString("NAMA_REG"));
            editor.putString("KODE_LEADER", jsonObject.optString("KODE_LEADER"));
            editor.putString("KODE_REG1", jsonObject.optString("KODE_REG1"));
            editor.putString("KODE_REG2", jsonObject.optString("KODE_REG2"));
            editor.putString("KODE_REG3", jsonObject.optString("KODE_REG3"));
            editor.putString("KODE_REGIONAL", jsonObject.optString("KODE_REG1") + jsonObject.optString("KODE_REG2") + jsonObject.optString("KODE_REG3"));
            editor.putInt("JENIS_LOGIN", jsonObject.optInt("JENIS_LOGIN"));
            if (jsonObject.getInt("JENIS_LOGIN_BC") == 16) {
                editor.putInt("JENIS_LOGIN_BC", 2);
            } else {
                editor.putInt("JENIS_LOGIN_BC", jsonObject.optInt("JENIS_LOGIN_BC"));
            }
            editor.putString("NAMA_BANK", jsonObject.optString("NAMA_BANK"));
            editor.putInt("FLAG_ADMIN", jsonObject.optInt("FLAG_ADMIN"));
            editor.putString("EMAIL", jsonObject.optString("EMAIL"));
            editor.putInt("GROUP_ID", jsonObject.optInt("GROUP_ID"));
            editor.putInt(getString(R.string.SL_APP), getSL_APP(jsonObject.optInt("JENIS_LOGIN_BC")));
            editor.apply();

        } catch (JSONException e) {
            Log.e(TAG, "onSuccess: error " + e);
        }
        Intent logIntent = new Intent(C_LoginActivity.this, C_MainActivity.class);
        startActivity(logIntent);
        showProgress(false);
        finish();
    }
//
//    private int getJenisLogin(int jenis_login) {
//        if (jenis_login == 2 ) {
//            //e-agency
//            Intent log2Intent = new Intent(C_LoginActivity.this, C_MainSiap2UActivity.class);
//            startActivity(log2Intent);
//            showProgress(false);
//            finish();
//
//        } else if (jenis_login == 9) {            //bancass
//            Intent logIntent = new Intent(C_LoginActivity.this, C_MainActivity.class);
//            startActivity(logIntent);
//            showProgress(false);
//            finish();
//            return 13;
//        } else {
//            Toast.makeText(C_LoginActivity.this, "NO_JENIS_LOGIN", Toast.LENGTH_SHORT).show();
//
//        }
//        return 0;
//    }

    private int getSL_APP(int jenis_login_bc) {
        switch (jenis_login_bc) {
            case 2: //bank sinarmas
                return 12;
            case 16: //bank sinarmas
                return 12;
            case 44://bjb
                return 14;
            case 50://bukopin
                return 2;
            case 51:// jatim
                return 15;
            case 56://BTN Syariah
                return 16;
            default:
                Toast.makeText(C_LoginActivity.this, "N0_SL_APP", Toast.LENGTH_SHORT).show();
                return 0;
        }
    }

    @Override
    public void onAuthFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        Log.d(TAG, "onFailure: json object" + throwable.getMessage());
        /*disini onfailure koneksi*/
        showDialogTryAgain(getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi));
        showProgress(false);
    }

    @Override
    public void onAuthFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        Log.d(TAG, "onFailure:  string" + responseString);
        /*disni onfailure salah password*/
        showProgress(false);
        TextView tv_error_password = findViewById(R.id.tv_error_password);
        tv_error_password.setText(R.string.error_login);
        tv_error_password.setVisibility(View.VISIBLE);
        et_password.requestFocus();
    }

    private void showDialogTryAgain(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(C_LoginActivity.this);
        AlertDialog alertDialog;

        alertDialogBuilder.setTitle(title);
//                    alertDialogBuilder.setMessage(getString(R.string.solusi_masalah_koneksi));
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(getString(R.string.coba_kembali), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}


