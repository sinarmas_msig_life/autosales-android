package id.co.ajsmsig.nb.prop.method;
/*
 *Created by faiz_f on 18/05/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.DBHelper;

public class QueryUtil {
    private DBHelper dbHelper;

    public QueryUtil(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.update_data_preferences), Context.MODE_PRIVATE);
        int version = sharedPreferences.getInt(context.getString(R.string.lav_data_id), 0);
        this.dbHelper = new DBHelper(context, version);
    }

    public Cursor query (@NonNull String tableName,
                         @Nullable String[] projection,
                         @Nullable String selection,
                         @Nullable String[]selectionArgs,
                         @Nullable String sortOrder) {

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(tableName);

        return qb.query(db,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);
    }

    public long insert(@NonNull String tableName, @NonNull ContentValues values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.insert(tableName, "", values);
    }

    public int update(@NonNull String tableName, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        return db.update(tableName, values, selection, selectionArgs);
    }

    public int delete(@NonNull String tableName, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        return db.delete(tableName, selection, selectionArgs);
    }




}
