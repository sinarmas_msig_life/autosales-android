package id.co.ajsmsig.nb.crm.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.adapter.C_LeadPagerAdapter;
import id.co.ajsmsig.nb.crm.database.C_Update;
import id.co.ajsmsig.nb.crm.fragment.C_ListActivityFragment;
import id.co.ajsmsig.nb.crm.fragment.C_ListProposalFragment;
import id.co.ajsmsig.nb.crm.fragment.C_ListSpajFragment;
import id.co.ajsmsig.nb.crm.model.hardcode.RequestCode;
import id.co.ajsmsig.nb.pointofsale.POSActivity;
import id.co.ajsmsig.nb.pointofsale.utils.Constants;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.util.Const;

public class C_LeadActivity extends AppCompatActivity {
    private static final String TAG = C_LeadActivity.class.getSimpleName();

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tab_layout)
    TabLayout tab_layout;
    CheckBox cb_star;

    private long SL_TAB_ID;
    private long SL_ID;
    private String SL_NAME = "";
    private int SL_GENDER = 0;
    private int GROUP_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_activity_lead);

        ButterKnife.bind(this);
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        GROUP_ID = sharedPreferences.getInt("GROUP_ID", 0);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }

        initView();

    }

    private void initView() {

        final Bundle bundle = getIntent().getExtras();
        SL_TAB_ID = bundle.getLong(getString(R.string.SL_TAB_ID), -1);
        SL_ID = bundle.getLong(Const.INTENT_KEY_SL_ID);
        cb_star = toolbar.findViewById(R.id.cb_star);

        QueryUtil queryUtil = new QueryUtil(this);
        String[] projection = new String[]{getString(R.string.SL_NAME), getString(R.string.SL_FAV),
                getString(R.string.SL_GENDER)};
        String selection = "SL_TAB_ID = ? OR SL_ID = ?";
        String[] selectionArgs = new String[]{String.valueOf(SL_TAB_ID), String.valueOf(SL_ID)};

        Cursor cursor = queryUtil.query(
                getString(R.string.TABLE_SIMBAK_LEAD),
                projection,
                selection,
                selectionArgs,
                null
        );


        if (cursor.moveToFirst()) {
            SL_GENDER = cursor.getInt(cursor.getColumnIndexOrThrow(getString(R.string.SL_NAME)));
            SL_NAME = cursor.getString(cursor.getColumnIndexOrThrow(getString(R.string.SL_NAME)));
            setActivityTitle(SL_NAME);
            int fav;
            if (cursor.isNull(cursor.getColumnIndexOrThrow(getString(R.string.SL_FAV)))) {
                fav = 0;
            } else {
                fav = cursor.getInt(cursor.getColumnIndexOrThrow(getString(R.string.SL_FAV)));
            }

            cb_star.setChecked(fav == 1);
        }
        cursor.close();

        cb_star.setOnCheckedChangeListener((buttonView, isChecked) -> {
            C_Update update = new C_Update(C_LeadActivity.this);

            if (isChecked) {
                Toast.makeText(C_LeadActivity.this, SL_NAME + " ditandai sebagai lead favorit", Toast.LENGTH_SHORT).show();
                update.markAsFavoriteLead(String.valueOf(SL_TAB_ID), String.valueOf(SL_ID), true);
            } else {
                Toast.makeText(C_LeadActivity.this, SL_NAME + " dihapus dari lead favorit", Toast.LENGTH_SHORT).show();
                update.markAsFavoriteLead(String.valueOf(SL_TAB_ID), String.valueOf(SL_ID),false);
            }


        });


        CharSequence[] titles = new CharSequence[]{
                "AKTIVITAS",
                "PROPOSAL",
                "ESPAJ"
        };
        Fragment[] fragments = new Fragment[]{
                new C_ListActivityFragment(),
                new C_ListProposalFragment(),
                new C_ListSpajFragment()
        };
        C_LeadPagerAdapter adapter = new C_LeadPagerAdapter(getSupportFragmentManager(), fragments, titles);

        // Assigning ViewPager View and setting the adapter
        pager.setAdapter(adapter);
        tab_layout.setupWithViewPager(pager, true);

        tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                invalidateOptionsMenu();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setActivityTitle(String title) {
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lead, menu);
        MenuItem action_add_pos = menu.findItem(R.id.action_add_pos);
        action_add_pos.setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        Bundle bundle;
        switch (item.getItemId()) {
            case R.id.action_edit_lead:
                bundle = new Bundle();
                bundle.putLong(Const.INTENT_KEY_SL_TAB_ID, SL_TAB_ID);
                bundle.putBoolean(Const.INTENT_KEY_EDIT_MODE, true);
                intent = new Intent(this, C_FormSimpleLeadActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, RequestCode.EDIT_FORM_LEAD);

                break;
            case R.id.action_add_activity:
                intent = new Intent(this, C_FormAktifitasActivity.class);
                bundle = new Bundle();
                bundle.putLong(Const.INTENT_KEY_SL_TAB_ID, SL_TAB_ID);
                bundle.putBoolean(Const.INTENT_KEY_ADD_ACTIVITY_MODE, true);
                bundle.putString(Const.INTENT_KEY_SL_NAME, SL_NAME);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
                break;
            case R.id.action_add_proposal:
                intent = new Intent(this, P_MainActivity.class);
                intent.putExtra(Const.INTENT_KEY_ADD_PROPOSAL_MODE, true);
                intent.putExtra(Const.INTENT_KEY_SL_TAB_ID, SL_TAB_ID);
                startActivity(intent);
                break;
            case R.id.action_add_pos:
                intent = new Intent(this, POSActivity.class);
                intent.putExtra(Constants.Companion.getSL_TAB_ID(), SL_TAB_ID);
                intent.putExtra(Constants.Companion.getSL_NAME(), SL_NAME);
                intent.putExtra(Constants.Companion.getSL_GENDER(), SL_GENDER);
                intent.putExtra(Constants.Companion.getGROUP_ID(), GROUP_ID);
                startActivityForResult(intent, RequestCode.REQ_POS);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        if (requestCode == RequestCode.EDIT_FORM_LEAD) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                setActivityTitle(bundle.getString(getString(R.string.title)));
            }
        } else if (requestCode == RequestCode.REQ_POS) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                ArrayList<Integer> List_db = new ArrayList<Integer>();
                Bundle bundle = data.getExtras();
                Intent intent = new Intent(this, P_MainActivity.class);
                intent.putExtra(Const.INTENT_KEY_ADD_PROPOSAL_MODE_POS, true);
                intent.putExtra(Const.INTENT_KEY_SL_TAB_ID, SL_TAB_ID);
                intent.putExtra(Constants.Companion.getLSBS_ID(), bundle.getInt(Constants.Companion.getLSBS_ID()));
                intent.putExtra("value", bundle.getIntegerArrayList("value"));
                startActivity(intent);
            }
        }
    }
}
