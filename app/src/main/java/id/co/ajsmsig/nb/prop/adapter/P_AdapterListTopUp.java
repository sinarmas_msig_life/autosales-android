package id.co.ajsmsig.nb.prop.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.prop.fragment.P_TopUpFragment;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductTopUpModel;


public class P_AdapterListTopUp extends ArrayAdapter<P_MstProposalProductTopUpModel> {
    private ArrayList<P_MstProposalProductTopUpModel> mstProposalProductTopUpModels;
    private int resourceId;
    private BigDecimal totalTP = BigDecimal.ZERO, totalTR = BigDecimal.ZERO;
    private P_TopUpFragment topUpFragment;

    public ArrayList<P_MstProposalProductTopUpModel> getMstProposalProductTopUpModels() {
        return mstProposalProductTopUpModels;
    }

    public BigDecimal getTotalTP() {
        return totalTP;
    }

    public void setTotalTP(BigDecimal totalTP) {
        this.totalTP = totalTP;
    }

    public BigDecimal getTotalTR() {
        return totalTR;
    }

    public void setTotalTR(BigDecimal totalTR) {
        this.totalTR = totalTR;
    }

    public P_AdapterListTopUp(Context context, int resourceId,
                              ArrayList<P_MstProposalProductTopUpModel> mstProposalProductTopUpModels, P_TopUpFragment topUpFragment) {
        super(context, resourceId, mstProposalProductTopUpModels);
        this.mstProposalProductTopUpModels = new ArrayList<>();
        this.mstProposalProductTopUpModels.addAll(mstProposalProductTopUpModels);
        this.resourceId = resourceId;
        this.topUpFragment = topUpFragment;
    }

    public void updateListTP(ArrayList<P_MstProposalProductTopUpModel> newList) {
        mstProposalProductTopUpModels.clear();
        mstProposalProductTopUpModels.addAll(newList);
        setTotalTP(BigDecimal.ZERO);
        setTotalTR(BigDecimal.ZERO);
        topUpFragment.setTotal(getTotalTP(), getTotalTR());
        this.notifyDataSetChanged();
    }

    private class ViewHolder {
        TextView tv_tp;
        TextView tv_tr;
        CheckBox cb;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

//        ViewHolder holder = null;
        final ViewHolder holder;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(resourceId, null);

            holder = new ViewHolder();
            holder.tv_tp = convertView.findViewById(R.id.tv_tp);
            holder.tv_tr = convertView.findViewById(R.id.tv_tr);
            holder.cb = convertView.findViewById(R.id.cb);
            convertView.setTag(holder);

            holder.cb.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    P_MstProposalProductTopUpModel mstProposalProductTopUpModel = (P_MstProposalProductTopUpModel) cb.getTag();
//                    Toast.makeText(getContext().getApplicationContext(),
//                            "Clicked on Checkbox: " + cb.getText() +
//                                    " is " + cb.isChecked(),
//                            Toast.LENGTH_LONG).show();
                    mstProposalProductTopUpModel.setSelected(cb.isChecked());

                    if (mstProposalProductTopUpModel.getSelected()) {
                        totalTP = totalTP.add(new BigDecimal(mstProposalProductTopUpModel.getTopup()));
                        totalTR = totalTR.add(new BigDecimal(mstProposalProductTopUpModel.getTarik()));
                    } else {
                        totalTP = totalTP.subtract(new BigDecimal(mstProposalProductTopUpModel.getTopup()));
                        totalTR = totalTR.subtract(new BigDecimal(mstProposalProductTopUpModel.getTarik()));
                    }
                    topUpFragment.setTotal(totalTP, totalTR);
                }
            });

        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        P_MstProposalProductTopUpModel mstProposalProductTopUpModel = mstProposalProductTopUpModels.get(position);
        holder.tv_tp.setText(StaticMethods.toMoney(mstProposalProductTopUpModel.getTopup()));
        holder.tv_tr.setText(StaticMethods.toMoney(mstProposalProductTopUpModel.getTarik()));
        holder.cb.setText(String.valueOf(mstProposalProductTopUpModel.getThn_ke()));
        holder.cb.setChecked(mstProposalProductTopUpModel.getSelected());
        holder.cb.setTag(mstProposalProductTopUpModel);

        return convertView;

    }
}
