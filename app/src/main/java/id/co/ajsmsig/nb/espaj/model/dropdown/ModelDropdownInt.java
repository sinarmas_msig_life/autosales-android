/**
 * @author Eriza Siti Mulyani
 *
 */
package id.co.ajsmsig.nb.espaj.model.dropdown;

public class ModelDropdownInt {
	private String value = "";
	private int id;

	public ModelDropdownInt() {
	}

	public ModelDropdownInt(String value, int id) {
		super();
		this.value = value;
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String toString() {
		  return value;
	  }
	
}