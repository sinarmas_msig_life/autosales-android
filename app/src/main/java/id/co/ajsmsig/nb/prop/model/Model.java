package id.co.ajsmsig.nb.prop.model;
/*
 Created by faiz_f on 05/06/2017.
 */

public class Model {
    private String text;
    private Boolean isSelected;

    public Model(String text, Boolean isSelected) {
        this.text = text;
        this.isSelected = isSelected;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }
}
