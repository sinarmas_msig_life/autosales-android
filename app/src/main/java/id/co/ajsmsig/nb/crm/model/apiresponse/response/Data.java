
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("reg_spaj")
    @Expose
    private String regSpaj;
    @SerializedName("no_blanko")
    @Expose
    private String noBlanko;
    @SerializedName("gadget_tmp")
    @Expose
    private Object gadgetTmp;
    @SerializedName("gadget_spaj")
    @Expose
    private Object gadgetSpaj;
    @SerializedName("no_polis")
    @Expose
    private String noPolis;
    @SerializedName("lsbs_id")
    @Expose
    private String lsbsId;
    @SerializedName("id_user_penutup")
    @Expose
    private String idUserPenutup;
    @SerializedName("user_penutup")
    @Expose
    private String userPenutup;
    @SerializedName("lca_id")
    @Expose
    private String lcaId;
    @SerializedName("lspd_id")
    @Expose
    private String lspdId;
    @SerializedName("pemegang")
    @Expose
    private String pemegang;
    @SerializedName("ttl_pemegang")
    @Expose
    private String ttlPemegang;
    @SerializedName("tanggal_input")
    @Expose
    private String tanggalInput;

    public String getRegSpaj() {
        return regSpaj;
    }

    public void setRegSpaj(String regSpaj) {
        this.regSpaj = regSpaj;
    }

    public String getNoBlanko() {
        return noBlanko;
    }

    public void setNoBlanko(String noBlanko) {
        this.noBlanko = noBlanko;
    }

    public Object getGadgetTmp() {
        return gadgetTmp;
    }

    public void setGadgetTmp(Object gadgetTmp) {
        this.gadgetTmp = gadgetTmp;
    }

    public Object getGadgetSpaj() {
        return gadgetSpaj;
    }

    public void setGadgetSpaj(Object gadgetSpaj) {
        this.gadgetSpaj = gadgetSpaj;
    }

    public String getNoPolis() {
        return noPolis;
    }

    public void setNoPolis(String noPolis) {
        this.noPolis = noPolis;
    }

    public String getLsbsId() {
        return lsbsId;
    }

    public void setLsbsId(String lsbsId) {
        this.lsbsId = lsbsId;
    }

    public String getIdUserPenutup() {
        return idUserPenutup;
    }

    public void setIdUserPenutup(String idUserPenutup) {
        this.idUserPenutup = idUserPenutup;
    }

    public String getUserPenutup() {
        return userPenutup;
    }

    public void setUserPenutup(String userPenutup) {
        this.userPenutup = userPenutup;
    }

    public String getLcaId() {
        return lcaId;
    }

    public void setLcaId(String lcaId) {
        this.lcaId = lcaId;
    }

    public String getLspdId() {
        return lspdId;
    }

    public void setLspdId(String lspdId) {
        this.lspdId = lspdId;
    }

    public String getPemegang() {
        return pemegang;
    }

    public void setPemegang(String pemegang) {
        this.pemegang = pemegang;
    }

    public String getTtlPemegang() {
        return ttlPemegang;
    }

    public void setTtlPemegang(String ttlPemegang) {
        this.ttlPemegang = ttlPemegang;
    }

    public String getTanggalInput() {
        return tanggalInput;
    }

    public void setTanggalInput(String tanggalInput) {
        this.tanggalInput = tanggalInput;
    }

}
