package id.co.ajsmsig.nb.prop.database;
/*
 Created by faiz_f on 03/10/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.DBHelper;
import id.co.ajsmsig.nb.crm.model.form.SimbakActivityModel;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;

import static id.co.ajsmsig.nb.prop.model.hardCode.P_FragmentPagePosition.DATA_PROPOSAL_PAGE;

public class P_Update {
    private final static String TAG = P_Select.class.getSimpleName();
    private DBHelper dbHelper;
    private Context context;

    //Table E_LST_VA
    private final String TABLE_NAME_E_LST_VA = "E_LST_VA";
    private final String COLUMN_NO_VA = "NO_VA";
    private final String COLUMN_FLAG_AKTIF = "FLAG_AKTIF";
    private final int FLAG_USED = 0;
    private final String TABLE_NAME_MST_DATA_PROPOSAL = "MST_DATA_PROPOSAL";
    private final int FLAG_AVAILABLE = 1;

    public P_Update(Context context) {
        this.context = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.update_data_preferences), Context.MODE_PRIVATE);
        int version = sharedPreferences.getInt(context.getString(R.string.lav_data_id), 0);
        this.dbHelper = new DBHelper(context, version);
    }

    public void updateNoProposal(String noProposalTab, String noProposal) {
        ContentValues cv = new ContentValues();
        cv.put(context.getString(R.string.NO_PROPOSAL), noProposal);
        String tableName;
        String selection = context.getString(R.string.NO_PROPOSAL_TAB) + "=?";
        String[] selectionArgs = new String[]{noProposalTab};

        updateTabel(context.getString(R.string.MST_DATA_PROPOSAL), cv, selection, selectionArgs);
        updateTabel(context.getString(R.string.MST_PROPOSAL_PRODUCT), cv, selection, selectionArgs);
        updateTabel(context.getString(R.string.MST_PROPOSAL_PRODUCT_ULINK), cv, selection, selectionArgs);
        updateTabel(context.getString(R.string.MST_PROPOSAL_PRODUCT_RIDER), cv, selection, selectionArgs);
        updateTabel(context.getString(R.string.MST_PROPOSAL_PRODUCT_PESERTA), cv, selection, selectionArgs);
        updateTabel(context.getString(R.string.MST_PROPOSAL_PRODUCT_TOPUP), cv, selection, selectionArgs);

    }

    public void updateFlagFinish(String noProposalTab, int flagValue) {
        ContentValues cv = new ContentValues();
        cv.put(context.getString(R.string.FLAG_FINISH), flagValue);
        String tableName = context.getString(R.string.MST_DATA_PROPOSAL);
        String selection = context.getString(R.string.NO_PROPOSAL_TAB) + "=?";
        String[] selectionArgs = new String[]{noProposalTab};

        updateTabel(tableName, cv, selection, selectionArgs);
    }


    private void updateTabel(String tableName, ContentValues cv, String selection, String[] selectionArgs){
        new QueryUtil(context).update(tableName, cv, selection, selectionArgs);
    }

    /**
     * Mark a virtual account already been used.
     * So it can't be used for the second time for a different customer
     */
    public void updateVirtualAccountIsUsed(String selectedVirtualAccount) {
        //Update the flag '1' to '0'
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_FLAG_AKTIF,FLAG_USED);

        String selection = COLUMN_NO_VA + " = ?";
        String[] selectionArgs = new String[]{selectedVirtualAccount};

        updateTabel(TABLE_NAME_E_LST_VA, contentValues, selection, selectionArgs);
    }

    public void updateProposal(Long id, P_MstDataProposalModel p_mstDataProposalModel) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(context.getString(R.string.NO_PROPOSAL_TAB), p_mstDataProposalModel.getNo_proposal_tab());
        cv.put(context.getString(R.string.NO_PROPOSAL), p_mstDataProposalModel.getNo_proposal());
        cv.put(context.getString(R.string.NAMA_PP), p_mstDataProposalModel.getNama_pp());
        cv.put(context.getString(R.string.TGL_LAHIR_PP), p_mstDataProposalModel.getTgl_lahir_pp());
        cv.put(context.getString(R.string.UMUR_PP), p_mstDataProposalModel.getUmur_pp());
        cv.put(context.getString(R.string.SEX_PP), p_mstDataProposalModel.getSex_pp());
        cv.put(context.getString(R.string.NAMA_TT), p_mstDataProposalModel.getNama_tt());
        cv.put(context.getString(R.string.TGL_LAHIR_TT), p_mstDataProposalModel.getTgl_lahir_tt());
        cv.put(context.getString(R.string.UMUR_TT), p_mstDataProposalModel.getUmur_tt());
        cv.put(context.getString(R.string.SEX_TT), p_mstDataProposalModel.getSex_tt());
        cv.put(context.getString(R.string.NAMA_AGEN), p_mstDataProposalModel.getNama_agen());
        cv.put(context.getString(R.string.MSAG_ID), p_mstDataProposalModel.getMsag_id());
        cv.put(context.getString(R.string.TGL_INPUT), p_mstDataProposalModel.getTgl_input());
        cv.put(context.getString(R.string.FLAG_AKTIF), p_mstDataProposalModel.getFlag_aktif());
        cv.put(context.getString(R.string.FLAG_TEST), p_mstDataProposalModel.getFlag_test());
        cv.put(context.getString(R.string.NAMA_USER), p_mstDataProposalModel.getNama_user());
        cv.put(context.getString(R.string.KODE_AGEN), p_mstDataProposalModel.getKode_agen());
        cv.put(context.getString(R.string.LSTB_ID), p_mstDataProposalModel.getLstb_id());
        cv.put(context.getString(R.string.ID_DIST), p_mstDataProposalModel.getId_dist());
        cv.put(context.getString(R.string.JENIS_ID), p_mstDataProposalModel.getJenis_id());
        cv.put(context.getString(R.string.LEAD_ID), p_mstDataProposalModel.getLead_id());
        cv.put(context.getString(R.string.LEAD_TAB_ID), p_mstDataProposalModel.getLead_tab_id());
        cv.put(context.getString(R.string.PROSPECT_ID), p_mstDataProposalModel.getProspect_id());
        cv.put(context.getString(R.string.FLAG_TT_CALON_BAYI), p_mstDataProposalModel.getFlag_tt_calon_bayi());
        cv.put(context.getString(R.string.FLAG_FINISH), p_mstDataProposalModel.getFlag_finish());
        cv.put(context.getString(R.string.FLAG_STAR), p_mstDataProposalModel.getFlag_star());
        cv.put(context.getString(R.string.LAST_PAGE_POSITION), p_mstDataProposalModel.getLast_page_position());
        cv.put(context.getString(R.string.FLAG_PROPER), p_mstDataProposalModel.getFlag_proper());
        cv.put(context.getString(R.string.FLAG_PACKET), p_mstDataProposalModel.getFlag_packet());
        cv.put(context.getString(R.string.va), p_mstDataProposalModel.getVirtual_acc());
        cv.put(context.getString(R.string.noid_member), p_mstDataProposalModel.getNoid());
        cv.put(context.getString(R.string.flag_pos), p_mstDataProposalModel.getFlag_pos());

        db.update(TABLE_NAME_MST_DATA_PROPOSAL, cv, "_id = ?", new String[]{String.valueOf(id)});
    }

    public void updateProposalValueWith(long proposalId, int lastPagePosition) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cvUpdate = new ContentValues();
        cvUpdate.put(context.getString(R.string.LAST_PAGE_POSITION), lastPagePosition);
        cvUpdate.put(context.getString(R.string.FLAG_FINISH), 0);
        cvUpdate.put(context.getString(R.string.FLAG_PROPER), 0);

        db.update(TABLE_NAME_MST_DATA_PROPOSAL, cvUpdate, "_id = ?", new String[]{String.valueOf(proposalId)});
    }

    public void updateProposalStatusWith(String noProposal, int flagFinish, long proposalId) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cvUpdate = new ContentValues();
        cvUpdate.put(context.getString(R.string.NO_PROPOSAL), noProposal);
        cvUpdate.put(context.getString(R.string.FLAG_FINISH), flagFinish);

        db.update(TABLE_NAME_MST_DATA_PROPOSAL, cvUpdate, "_id = ?", new String[]{String.valueOf(proposalId)});
    }
}
