package id.co.ajsmsig.nb.crm.adapter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.C_SearchLeadModel;

/**
 * Created by Fajar.Azhar on 09/03/2018.
 */

public class C_SearchLeadAdapter extends RecyclerView.Adapter<C_SearchLeadAdapter.LeadVH> {
    private List<C_SearchLeadModel> leadList;
    private ClickListener clickListener;

    public C_SearchLeadAdapter(List<C_SearchLeadModel> leadList) {
        this.leadList = leadList;
    }

    public class LeadVH extends RecyclerView.ViewHolder {
        private ImageView imgProfile;
        private TextView tvName;
        private TextView tvAge;
        private ConstraintLayout layoutLeads;

        public LeadVH(View view) {
            super(view);
            layoutLeads = view.findViewById(R.id.layoutLeads);
            imgProfile = view.findViewById(R.id.imgProfile);
            tvName = view.findViewById(R.id.tvName);
            tvAge = view.findViewById(R.id.tvAge);
        }

    }
    @Override
    public LeadVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.c_search_lead_list, parent, false);
        return new LeadVH(itemView);
    }

    @Override
    public void onBindViewHolder(final LeadVH holder, int position) {
        C_SearchLeadModel model = leadList.get(position);

        if (model.getCostumerAge() > 0) {
            holder.tvAge.setVisibility(View.VISIBLE);
            String customerAge = String.valueOf(model.getCostumerAge()).concat(" tahun");
            holder.tvAge.setText(customerAge);
        } else {
            holder.tvAge.setVisibility(View.INVISIBLE);
        }
     

        holder.tvName.setText(model.getCostumerName());

        holder.layoutLeads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onLeadSelected(holder.getAdapterPosition(),leadList);
            }
        });
    }

    @Override
    public int getItemCount() {
        return leadList.size();
    }

    public void setClickListener (ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onLeadSelected(int position, List<C_SearchLeadModel> leads);
    }
}
