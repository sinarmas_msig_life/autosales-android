package id.co.ajsmsig.nb.prop.database;
/*
 *Created by faiz_f on 10/05/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductPesertaModel;

public class TableProposalProductPeserta {
    private Context context;

    public TableProposalProductPeserta(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(P_MstProposalProductPesertaModel p_mstProposalProductPesertaModel) {
        ContentValues cv = new ContentValues();
//        if(p_mstProposalProductPesertaModel.getNo_proposal_tab()==null){
        cv.put(context.getString(R.string.NO_PROPOSAL_TAB), p_mstProposalProductPesertaModel.getNo_proposal_tab());
//        }
        cv.put(context.getString(R.string.NO_PROPOSAL), p_mstProposalProductPesertaModel.getNo_proposal());
        cv.put(context.getString(R.string.LSBS_ID), p_mstProposalProductPesertaModel.getLsbs_id());
        cv.put(context.getString(R.string.LSDBS_NUMBER), p_mstProposalProductPesertaModel.getLsdbs_number());
        cv.put(context.getString(R.string.NAMA_PESERTA), p_mstProposalProductPesertaModel.getNama_peserta());
        cv.put(context.getString(R.string.TGL_LAHIR), p_mstProposalProductPesertaModel.getTgl_lahir());
        cv.put(context.getString(R.string.USIA), p_mstProposalProductPesertaModel.getUsia());
        cv.put(context.getString(R.string.LSRE_ID), p_mstProposalProductPesertaModel.getLsre_id());
        cv.put(context.getString(R.string.PESERTA_KE), p_mstProposalProductPesertaModel.getPeserta_ke());
        return cv;
    }

    public ArrayList<P_MstProposalProductPesertaModel> getObjectArray(Cursor cursor, boolean androidTimeFormat) {
        ArrayList<P_MstProposalProductPesertaModel> p_mstProposalProductPesertaModels = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                P_MstProposalProductPesertaModel p_mstProposalProductPesertaModel = new P_MstProposalProductPesertaModel();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB)))) {
                    p_mstProposalProductPesertaModel.setNo_proposal_tab(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL)))) {
                    p_mstProposalProductPesertaModel.setNo_proposal(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LSBS_ID)))) {
                    p_mstProposalProductPesertaModel.setLsbs_id(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LSBS_ID))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LSDBS_NUMBER)))) {
                    p_mstProposalProductPesertaModel.setLsdbs_number(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LSDBS_NUMBER))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NAMA_PESERTA)))) {
                    p_mstProposalProductPesertaModel.setNama_peserta(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NAMA_PESERTA))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.TGL_LAHIR)))) {
                    String tglLahir = cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.TGL_LAHIR)));
                    if (androidTimeFormat) {
                        p_mstProposalProductPesertaModel.setTgl_lahir(tglLahir);
                    } else {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                        Calendar calendar = StaticMethods.toCalendar(tglLahir);
                        p_mstProposalProductPesertaModel.setTgl_lahir(sdf.format(calendar.getTime()));
                    }
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.USIA)))) {
                    p_mstProposalProductPesertaModel.setUsia(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.USIA))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LSRE_ID)))) {
                    p_mstProposalProductPesertaModel.setLsre_id(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LSRE_ID))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.PESERTA_KE)))) {
                    p_mstProposalProductPesertaModel.setPeserta_ke(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.PESERTA_KE))));
                }
                p_mstProposalProductPesertaModels.add(p_mstProposalProductPesertaModel);
                cursor.moveToNext();
            }

            // always close the cursor
            cursor.close();
        }

        return p_mstProposalProductPesertaModels;
    }
}
