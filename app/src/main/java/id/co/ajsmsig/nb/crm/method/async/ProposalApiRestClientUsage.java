package id.co.ajsmsig.nb.crm.method.async;
/*
 Created by Eriza on 03/03/2018.
 */

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.DataVersionResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.ListVersion;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.Result;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.SaveProposalDataResponse;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.database.P_Update;
import id.co.ajsmsig.nb.prop.method.P_StaticMethods;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;
import id.co.ajsmsig.nb.util.AppConfig;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ProposalApiRestClientUsage {
    private static final String TAG = ProposalApiRestClientUsage.class.getSimpleName();

    private Context context;
    private ProposalApiRestClientUsage.ListenerInfo mListenerInfo;

    private static class ListenerInfo {
        DownloadPdfProposalListener mDownloadPdfProposalListener;
        DownloadPdfProposalSIAP2UListener mDownloadPdfProposalSIAP2UListener;
        StempelListener stempelListener;
        UploadProposalListener mUploadProposalListener;
        CheckLastDataVersionListener mCheckLastDataVersionListener;
        DownloadNewDataVersionListener mDownloadNewDataVersionListener;
    }

    public ProposalApiRestClientUsage(Context context) {
        this.context = context;
    }

    //set All Listener Here
    private ListenerInfo getListenerInfo() {
        if (mListenerInfo != null) {
            return mListenerInfo;
        }
        mListenerInfo = new ListenerInfo();
        return mListenerInfo;
    }

    public void setOnDownloadPDFProposalListener(DownloadPdfProposalListener l) {
        getListenerInfo().mDownloadPdfProposalListener = l;
    }

    public void setOnDownloadPDFProposalSIAP2UListener(DownloadPdfProposalSIAP2UListener l) {
        getListenerInfo().mDownloadPdfProposalSIAP2UListener = l;
    }

    public void setonStempelProposalListener(StempelListener l) {
        getListenerInfo().stempelListener = l;
    }

    public void setOnUploadProposalListener(UploadProposalListener l) {
        getListenerInfo().mUploadProposalListener = l;
    }

    public void setOnCheckLastDataVersionListener(CheckLastDataVersionListener l) {
        getListenerInfo().mCheckLastDataVersionListener = l;
    }

    public void setOnDownloadNewDataVersionListener(DownloadNewDataVersionListener l) {
        getListenerInfo().mDownloadNewDataVersionListener = l;
    }

    public interface DownloadPdfProposalListener {
        void onDownloadPDFProposalSuccess();

        void onDownloadPDFProposalFailure(String title, String message);
    }

    public interface DownloadPdfProposalSIAP2UListener {
        void onDownloadPDFProposalSIAP2USuccess();

        void onDownloadPDFProposalSIAP2UFailure(String title, String message);
    }

    public interface StempelListener {
        void onStempelSuccess(int statusCode, Header[] headers, JSONObject response);

        void onStempelFailure(String title, String message);
    }

    public interface UploadProposalListener {
        void onUploadProposalSuccess(String noProposal, String message);

        void onUploadProposalFailure(String message);

        void onUploadProposalSuccessBuatSPAJ(String noProposal, String message);
    }

    public interface CheckLastDataVersionListener {
        void onCheckLastDataVersionSuccess(String updateVersion, ContentValues cvVersion);

        void onCheckLastDataVersionUpToDate(String message);

        void onCheckLastDataVersionFailureConnection(String message);
    }

    public interface DownloadNewDataVersionListener {
        void onProgressDownloadNewDataVersion(final long bytesWritten, final long totalSize, String downloadVersion);

        void onDownloadNewDataVersionSuccess(final File file, final ContentValues cvLatestVersion);

        void onDownloadNewDataVersionFailure(String title, String message);
    }

    //Set All Method Below Listener
    public boolean performDownloadIdMember(final String noId, final boolean savePdfToSDCard) {
        final boolean result;
        final ProposalApiRestClientUsage.ListenerInfo li = mListenerInfo;
        if (li != null && li.mDownloadPdfProposalSIAP2UListener != null) {
            EproposalRestClient.get("api/smws/gen_proposal_siap2u/" + noId, null, 25000, new FileAsyncHttpResponseHandler(context) {
                @Override
                public void onSuccess(int statusCode, Header[] headers, File file) {
                    Log.d(TAG, "onSuccess: " + file.getPath());
                    if (file.exists()) {
                        if (savePdfToSDCard) {
                            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_preferences), Context.MODE_PRIVATE);
                            String kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
                            String pdfName = kodeAgen + "_" + noId + ".pdf";
                            File pdfFile = P_StaticMethods.getFileForPdf(pdfName);
                            pdfFile.getParentFile().mkdirs();
                            try {
                                FileChannel src = new FileInputStream(file).getChannel();
                                FileChannel dst = new FileOutputStream(pdfFile).getChannel();
                                dst.transferFrom(src, 0, src.size());
                                src.close();
                                dst.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } finally {
                                file.delete();
                            }
                            if (pdfFile.exists()) {
                                li.mDownloadPdfProposalSIAP2UListener.onDownloadPDFProposalSIAP2USuccess();
                                StaticMethods.openPDF(context, pdfFile);
                            }
                        } else {
                            li.mDownloadPdfProposalSIAP2UListener.onDownloadPDFProposalSIAP2USuccess();
                        }
                    } else {
                        li.mDownloadPdfProposalSIAP2UListener.onDownloadPDFProposalSIAP2UFailure("Error", "filenya tidak ada");
////                        Log.i(TAG, "onSuccess: check server filenya gak ada");
////                    showDialogTryAgain(getString(R.string.masalah_server), getString(R.string.solusi_masalah_server));
                    }
                }
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, final File file) {

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

                }
            });
            result = true;
        } else {
            result = false;
        }
        return result;
    }


    public void uploadProposal(String noProposalTab) {
        P_ProposalModel proposalModel = new P_Select(context).getAllDataProposalForUpload(noProposalTab);

//        performUploadProposal(proposalModel);
        PerformUploadProposalRetrofit(proposalModel);
    }

    public void uploadProposalBuatSPAJ(String noProposalTab) {
        P_ProposalModel proposalModel = new P_Select(context).getAllDataProposalForUpload(noProposalTab);

//        performUploadProposal(proposalModel);
        PerformUploadProposalRetrofitBuatSPAJ(proposalModel);
    }

    public boolean getDownloadPdfProposal(final String noProposal, final boolean savePdfToSDCard){

        final ProposalApiRestClientUsage.ListenerInfo li = mListenerInfo;
        String url_path = "";
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);

        url_path = "api/proposal/generatePdf/";
        String url = AppConfig.getBaseUrlEproposal().concat(url_path).concat(noProposal);
        Call<ResponseBody> call = api.performDownloadPdfProposalRetrofit(url);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "server contacted and has file");
                        if (savePdfToSDCard) {
                            boolean test = writeResponseBodyToDisk(response.body(), noProposal);
                        } else {
                            li.mDownloadPdfProposalListener.onDownloadPDFProposalSuccess();
                        }
                } else {
                    Log.d(TAG, "server contact failed");
                    li.mDownloadPdfProposalListener.onDownloadPDFProposalFailure("Error", "filenya tidak ada");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "error");
                li.mDownloadPdfProposalListener.onDownloadPDFProposalFailure(context.getString(R.string.masalah_koneksi), t.getMessage());
            }
        });
        return false;
    }

    private boolean writeResponseBodyToDisk(ResponseBody body, String noProposal) {
        try {
            // todo change the file location/name according to your needs
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_preferences), Context.MODE_PRIVATE);
            String kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
            String pdfName = kodeAgen + "_" + noProposal + ".pdf";
            File pdfFile = P_StaticMethods.getFileForPdf(pdfName);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(pdfFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
                if (pdfFile.exists()) {
                    StaticMethods.openPDF(context, pdfFile);
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    public boolean performDownloadPdfProposal(final String noProposal, final boolean savePdfToSDCard) {
        final boolean result;
        final ProposalApiRestClientUsage.ListenerInfo li = mListenerInfo;
        if (li != null && li.mDownloadPdfProposalListener != null) {
            Log.v(TAG, "Proposal path : "+AppConfig.getBaseUrlEproposal()+"api/proposal/generatePdf/"+noProposal);
            EproposalRestClient.get("api/proposal/generatePdf/" + noProposal, null, 25000, new FileAsyncHttpResponseHandler(context) {
                @Override
                public void onSuccess(int statusCode, Header[] headers, final File file) {
                    Log.d(TAG, "onSuccess: " + file.getPath());
                    if (file.exists()) {
                        if (savePdfToSDCard) {
                            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_preferences), Context.MODE_PRIVATE);
                            String kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
                            String pdfName = kodeAgen + "_" + noProposal + ".pdf";
                            File pdfFile = P_StaticMethods.getFileForPdf(pdfName);
                            pdfFile.getParentFile().mkdirs();
                            try {
                                FileChannel src = new FileInputStream(file).getChannel();
                                FileChannel dst = new FileOutputStream(pdfFile).getChannel();
                                dst.transferFrom(src, 0, src.size());
                                src.close();
                                dst.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } finally {
                                file.delete();
                            }
                            if (pdfFile.exists()) {
                                li.mDownloadPdfProposalListener.onDownloadPDFProposalSuccess();
                                StaticMethods.openPDF(context, pdfFile);
                            }
                        } else {
                            li.mDownloadPdfProposalListener.onDownloadPDFProposalSuccess();
                        }
                    } else {
                        Log.v(TAG, "Unable to download proposal PDF. File not exist");
                        li.mDownloadPdfProposalListener.onDownloadPDFProposalFailure("Error", "filenya tidak ada");
//                        Log.i(TAG, "onSuccess: check server filenya gak ada");
//                    showDialogTryAgain(getString(R.string.masalah_server), getString(R.string.solusi_masalah_server));
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                    Log.v(TAG, "Unable to download proposal PDF "+throwable.getMessage());
                    li.mDownloadPdfProposalListener.onDownloadPDFProposalFailure(context.getString(R.string.masalah_koneksi), throwable.getMessage() + statusCode);
                }

            });
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    public void UploadStempelProposal(final EspajModel me) {
        final boolean result;
        final ProposalApiRestClientUsage.ListenerInfo li = mListenerInfo;
        if (li != null && li.stempelListener != null) {
            try {
                String spajJson = "";
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("no_proposal", me.getModelID().getProposal());
                jsonObject.put("spaj", me.getModelID().getSPAJ_ID());

                JSONObject JSONFINAL = new JSONObject();
                JSONFINAL.put("data", jsonObject);
                spajJson = JSONFINAL.toString();
                StaticMethods.backUpToJsonFile(spajJson, "SpajJson", "SpajJson");

                StringEntity entity = new StringEntity(spajJson);
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                ProposalApiRestClient.post(context, "proposal/api/eproposal/stempel_pdf_proposal/", entity, "application/json", 60000, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        li.stempelListener.onStempelSuccess(statusCode, headers, response);

                    }

                    @Override
                    public void onFailure(final int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        li.stempelListener.onStempelFailure("Gagal", throwable.getMessage() + errorResponse);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        li.stempelListener.onStempelFailure("Gagal", throwable.getMessage() + errorResponse);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        li.stempelListener.onStempelFailure("Gagal", throwable.getMessage() + responseString);
                    }

                });
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "login: error is " + e.getMessage());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean performCheckLastDataVersionListener() {
        final boolean result;
        final ProposalApiRestClientUsage.ListenerInfo li = mListenerInfo;
        if (li != null && li.mCheckLastDataVersionListener != null) {

            String currentVersion = new C_Select(context).getCurrentDatabaseSchemaVersion();
            //Check if the version is null or empty string
            if (TextUtils.isEmpty(currentVersion)) {
                currentVersion = "1.0.0";
            }
            ProposalApiRestClient.get("proposal/api/eproposal/get_list_update/" + currentVersion, null, 10000, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d(TAG, "onSuccess object: " + response);
                    try {
                        if (response.has(context.getString(R.string.flag))) {
                            if (response.getString(context.getString(R.string.flag)).equals(context.getString(R.string.success))) {
                                JSONArray listVersion = response.getJSONArray(context.getString(R.string.list_version));
                                JSONObject object = listVersion.getJSONObject(0);
                                ContentValues cvLatestVersion = new ContentValues();
                                cvLatestVersion.put(context.getString(R.string.data_version), object.getString(context.getString(R.string.DATA_VERSION)));
                                cvLatestVersion.put(context.getString(R.string.lav_data_id), object.getInt(context.getString(R.string.LAV_DATA_ID)));
                                cvLatestVersion.put(context.getString(R.string.last_update), StaticMethods.getTodayDate());
                                Log.d(TAG, "onSuccess: cv" + cvLatestVersion);
                                li.mCheckLastDataVersionListener.onCheckLastDataVersionSuccess(object.getString(context.getString(R.string.DATA_VERSION)), cvLatestVersion);
                            } else {
                                li.mCheckLastDataVersionListener.onCheckLastDataVersionUpToDate("Data Up Todate");
                            }
                        } else {
                            li.mCheckLastDataVersionListener.onCheckLastDataVersionFailureConnection("koneksi gagal");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(final int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Log.d(TAG, "onFailure: " + errorResponse);
                    li.mCheckLastDataVersionListener.onCheckLastDataVersionFailureConnection(throwable.getMessage());
//                    showDialogTryAgain(getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi));
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d(TAG, "onFailure: " + responseString);

                }
            });
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    public boolean getLastDataVersion() {
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        final boolean result;
        String url_path = "";
        final ProposalApiRestClientUsage.ListenerInfo li = mListenerInfo;
        if (li != null && li.mCheckLastDataVersionListener != null) {

            String currentVersion = new C_Select(context).getCurrentDatabaseSchemaVersion();
            //Check if the version is null or empty string
            if (TextUtils.isEmpty(currentVersion)) {
                currentVersion = "1.0.0";
            }
            url_path = "proposal/api/eproposal/get_list_update/";
            String url = AppConfig.getBaseUrlProposal().concat(url_path).concat(currentVersion);
            Call<DataVersionResponse> call = api.getLastDataVersion(url);
            call.enqueue(new Callback<DataVersionResponse>() {
                @Override
                public void onResponse(Call<DataVersionResponse> call, Response<DataVersionResponse> response) {
                    if (response.body() != null) {
                        DataVersionResponse body = response.body();
                        String flag = body.getFlag();

                        if (flag.equalsIgnoreCase("success")) {
                            List<ListVersion> listVersions = body.getListVersion();

                            if (!listVersions.isEmpty()) {
                                ListVersion version = listVersions.get(0);

                                String dataVersion = version.getDATAVERSION();
                                int lavDataId = version.getLAVDATAID();

                                ContentValues cvLatestVersion = new ContentValues();
                                cvLatestVersion.put("data_version", dataVersion);
                                cvLatestVersion.put("lav_data_id", lavDataId);
                                cvLatestVersion.put("list_version", StaticMethods.getTodayDate());

                                li.mCheckLastDataVersionListener.onCheckLastDataVersionSuccess(dataVersion, cvLatestVersion);
                            }

                        } else {
                            li.mCheckLastDataVersionListener.onCheckLastDataVersionUpToDate("Data Up Todate");
                        }


                    } else {
                        li.mCheckLastDataVersionListener.onCheckLastDataVersionFailureConnection("koneksi gagal");
                    }
                }

                @Override
                public void onFailure(Call<DataVersionResponse> call, Throwable t) {
                    li.mCheckLastDataVersionListener.onCheckLastDataVersionFailureConnection(t.getMessage());
                }
            });

            result = true;
        } else {
            result = false;

        }

        return result;

    }


    public boolean performDownloadDataUpdate(final String downloadVersion, final ContentValues cvLatestVersion) {
        final boolean result;
        final ProposalApiRestClientUsage.ListenerInfo li = mListenerInfo;
        if (li != null && li.mDownloadNewDataVersionListener != null) {

            ProposalApiRestClient.get("proposal/api/eproposal/get_data_update/" + downloadVersion, null, 15000, new FileAsyncHttpResponseHandler(context) {

                @Override
                public void onProgress(final long bytesWritten, final long totalSize) {
                    li.mDownloadNewDataVersionListener.onProgressDownloadNewDataVersion(bytesWritten, totalSize, downloadVersion);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, final File file) {
                    Log.d(TAG, "onSuccess: " + file.getPath());
                    if (file.exists()) {
                        li.mDownloadNewDataVersionListener.onDownloadNewDataVersionSuccess(file, cvLatestVersion);
                    } else {
                        li.mDownloadNewDataVersionListener.onDownloadNewDataVersionFailure("Error", "File tidak exist");
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                    li.mDownloadNewDataVersionListener.onDownloadNewDataVersionFailure(context.getString(R.string.masalah_koneksi), context.getString(R.string.solusi_masalah_koneksi));
                }
            });
            result = true;
        } else {
            result = false;
        }
        return result;
    }



    private void performUploadProposal(final P_ProposalModel proposalModel) {
        final boolean result;
        final ListenerInfo li = mListenerInfo;

        try {
            Gson gson = new Gson();
            final String proposalJson = gson.toJson(proposalModel);
            StaticMethods.backUpToJsonFile(proposalJson, "PropJson", "PropJson");

            StringEntity entity = new StringEntity(proposalJson);
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            ProposalApiRestClient.post(context, "proposal/api/eproposal/save_proposal_data", entity, "application/json", 15000, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        P_Update update = new P_Update(context);

                        String flagUpload = response.getString(context.getString(R.string.flag));
                        if (flagUpload.equals(context.getString(R.string.success))) {
                            JSONObject result = response.getJSONObject("result");
                            String noProposal = result.optString(context.getString(R.string.NO_PROPOSAL).toLowerCase());

                            long id = proposalModel.getMst_data_proposal().getId();
                            //Update no proposal with id from server and set flag finish to 1
                            update.updateProposalStatusWith(noProposal, 1, id);

                            if (li != null && li.mUploadProposalListener != null)
                                li.mUploadProposalListener.onUploadProposalSuccess(noProposal, context.getString(R.string.success));
                        } else {
                            if (li != null && li.mUploadProposalListener != null)
                                li.mUploadProposalListener.onUploadProposalFailure("Upload Failed");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(final int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (li != null && li.mUploadProposalListener != null)
                        li.mUploadProposalListener.onUploadProposalFailure(throwable.getMessage() + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    if (li != null && li.mUploadProposalListener != null)
                        li.mUploadProposalListener.onUploadProposalFailure(throwable.getMessage() + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (li != null && li.mUploadProposalListener != null)
                        li.mUploadProposalListener.onUploadProposalFailure(throwable.getMessage() + responseString);
                }

            });
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "login: error is " + e.getMessage());
        }
    }

    private void PerformUploadProposalRetrofit (final P_ProposalModel proposalModel) {
        final boolean result;
        final ListenerInfo li = mListenerInfo;

        Gson gson = new Gson();
        final String proposalJson = gson.toJson(proposalModel);
        StaticMethods.backUpToJsonFile(proposalJson, "PropJson", "PropJson");

        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        String url_path = "proposal/api/eproposal/save_proposal_data";
        String url = AppConfig.getBaseUrlProposal().concat(url_path);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), proposalJson);
        Call<SaveProposalDataResponse> call = api.PerformUploadProposalRetrofit(url, requestBody);

        call.enqueue(new Callback<SaveProposalDataResponse>() {
            @Override
            public void onResponse(Call<SaveProposalDataResponse> call, Response<SaveProposalDataResponse> response) {
                try {
                    SaveProposalDataResponse body = response.body();
                    Result result;
                    String flag;

                    P_Update update = new P_Update(context);

                    flag = body.getFlag();
                    if (flag.equals(context.getString(R.string.success))) {
                        result = body.getResult();
                        String noProposal = result.getNoProposal();
                        long id = proposalModel.getMst_data_proposal().getId();
                        //Update no proposal with id from server and set flag finish to 1
                        update.updateProposalStatusWith(noProposal, 1, id);

                        if (li != null && li.mUploadProposalListener != null)
                            li.mUploadProposalListener.onUploadProposalSuccess(noProposal, context.getString(R.string.success));
                    } else {
                        if (li != null && li.mUploadProposalListener != null)
                            li.mUploadProposalListener.onUploadProposalFailure("Upload Failed");
                    }

                }catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SaveProposalDataResponse> call, Throwable t) {
                if (li != null && li.mUploadProposalListener != null){
                    li.mUploadProposalListener.onUploadProposalFailure(t.getMessage());
                }
            }
        });
    }

    private void PerformUploadProposalRetrofitBuatSPAJ (final P_ProposalModel proposalModel) {
        final boolean result;
        final ListenerInfo li = mListenerInfo;

        Gson gson = new Gson();
        final String proposalJson = gson.toJson(proposalModel);
        StaticMethods.backUpToJsonFile(proposalJson, "PropJson", "PropJson");

        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        String url_path = "proposal/api/eproposal/save_proposal_data";
        String url = AppConfig.getBaseUrlProposal().concat(url_path);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), proposalJson);
        Call<SaveProposalDataResponse> call = api.PerformUploadProposalRetrofit(url, requestBody);

        call.enqueue(new Callback<SaveProposalDataResponse>() {
            @Override
            public void onResponse(Call<SaveProposalDataResponse> call, Response<SaveProposalDataResponse> response) {
                try {
                    SaveProposalDataResponse body = response.body();
                    Result result;
                    String flag;

                    P_Update update = new P_Update(context);

                    flag = body.getFlag();
                    if (flag.equals(context.getString(R.string.success))) {
                        result = body.getResult();
                        String noProposal = result.getNoProposal();
                        long id = proposalModel.getMst_data_proposal().getId();
                        //Update no proposal with id from server and set flag finish to 1
                        update.updateProposalStatusWith(noProposal, 1, id);
                        if (li != null && li.mUploadProposalListener != null)
                            li.mUploadProposalListener.onUploadProposalSuccessBuatSPAJ(noProposal, context.getString(R.string.success));
                    } else {

                    }

                }catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SaveProposalDataResponse> call, Throwable t) {
                if (li != null && li.mUploadProposalListener != null){
                    li.mUploadProposalListener.onUploadProposalFailure(t.getMessage());
                }
            }
        });
    }

}
