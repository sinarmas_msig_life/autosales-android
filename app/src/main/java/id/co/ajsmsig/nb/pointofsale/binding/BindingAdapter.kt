package id.co.ajsmsig.nb.pointofsale.binding

import android.arch.lifecycle.MediatorLiveData
import android.databinding.BindingAdapter
import android.databinding.DataBindingUtil
import android.databinding.InverseBindingAdapter
import android.databinding.InverseBindingListener
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.pointofsale.custom.LoadingButton
import id.co.ajsmsig.nb.pointofsale.custom.indicatorseekbar.IndicatorSeekBar
import id.co.ajsmsig.nb.pointofsale.*
import id.co.ajsmsig.nb.databinding.*
import id.co.ajsmsig.nb.pointofsale.handler.SelectionHandler
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.*
import id.co.ajsmsig.nb.pointofsale.utils.Constants
import id.co.ajsmsig.nb.pointofsale.utils.SeekbarValueFormatter

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */
@BindingAdapter("app:questionnaires")
fun setLayoutFromQuestionnaires(viewGroup: ViewGroup, questionnaires: MediatorLiveData<List<RiskProfileQuestionnaireAllAnswers>>) {
    viewGroup.removeAllViews()

    questionnaires.observeForever {
        val inflater = LayoutInflater.from(viewGroup.context)
        it?.let {

            it.forEachIndexed { index, riskProfileQuestionnaire ->
                val dataBinding = DataBindingUtil.inflate<PosRowQuestionBinding>(inflater, R.layout.pos_row_question, viewGroup, true)
                dataBinding.vm = riskProfileQuestionnaire
                dataBinding.index = (index + 1).toString()
            }
        }
    }
}


@BindingAdapter("app:childQuestionnaires")
fun setLayoutFromChildQuestionnaires(radioGroup: RadioGroup, questionnaires: List<RiskProfileQuestionnaire>?) {
    radioGroup.removeAllViews()

    questionnaires?.let {
            val inflater = LayoutInflater.from(radioGroup.context)

            it.forEach {
                val dataBinding = DataBindingUtil.inflate<PosRowAnswerBinding>(inflater, R.layout.pos_row_answer, radioGroup, true)
                (dataBinding.root as RadioButton).id = it.id
                dataBinding.vm = it
            }

            radioGroup.setOnCheckedChangeListener { group, checkedId ->
                it.forEach { it.selected.set(it.id == checkedId) }
            }

            it.firstOrNull{it.selected.get()}?.let {
                radioGroup.check(it.id)
            }
        }

}

@BindingAdapter("app:entries")
fun setLayoutFromHandlingObjection(viewGroup: ViewGroup, handlingObjection: HandlingObjectionAllAnswers?) {
    viewGroup.removeAllViews()

    handlingObjection?.let{
        val inflater = LayoutInflater.from(viewGroup.context)

        it.answers?.let {
            it.forEach {
                val dataBinding = DataBindingUtil.inflate<PosRowHandlingObjectionAnswerBinding>(inflater, R.layout.pos_row_handling_objection_answer, viewGroup, true)
                dataBinding.vm = it
            }

        }
    }
}

@BindingAdapter("app:entries")
fun setLayoutFromInputs(viewGroup: ViewGroup, inputs: List<InputAllChoices>?) {
    viewGroup.removeAllViews()

    inputs?.let {
        val inflater = LayoutInflater.from(viewGroup.context)

        it.forEach {
            when (it.input?.type) {

                Constants.RADIOBUTTON -> {
                    val dataBinding = DataBindingUtil.inflate<PosRowInputRadiogroupBinding>(inflater, R.layout.pos_row_input_radiogroup, viewGroup, true)
                    dataBinding.vm = it

                    dataBinding.radioGroup.setOnCheckedChangeListener { group, checkedId ->
                        it.selectedChoice.set(it.choices?.firstOrNull { it.id == checkedId })
                    }
                }
                Constants.DROPDOWN -> {
                    val dataBinding = DataBindingUtil.inflate<PosRowInputDropdownBinding>(inflater, R.layout.pos_row_input_dropdown, viewGroup, true)
                    dataBinding.vm = it


                    dataBinding.spinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
                        override fun onNothingSelected(parent: AdapterView<*>?) {
                        }

                        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                            it.selectedChoice.set(it.choices?.get(position))
                        }

                    }

                }
                else -> {

                    val dataBinding = DataBindingUtil.inflate<PosRowInputSliderBinding>(inflater, R.layout.pos_row_input_slider, viewGroup, true)
                    dataBinding.vm = it

                    dataBinding.seekBar.formatter = SeekbarValueFormatter(it.input?.range?.toLong() ?: 1)

                    val totalTickNum = ((it.input?.max ?: 0) - (it.input?.min ?: 0)) / (it.input?.range ?: 1)
                    val totalTickNumInt = totalTickNum.toInt()
                    dataBinding.seekBar.builder.setTickNum(totalTickNumInt)
                    dataBinding.seekBar.setOnSeekChangeListener(object : IndicatorSeekBar.OnSeekBarChangeListener {
                        override fun onStartTrackingTouch(seekBar: IndicatorSeekBar?, thumbPosOnTick: Int) {
                        }
                        override fun onSectionChanged(seekBar: IndicatorSeekBar?, thumbPosOnTick: Int, textBelowTick: String?, fromUserTouch: Boolean) {
                        }
                        override fun onStopTrackingTouch(seekBar: IndicatorSeekBar?) {
                        }
                        override fun onProgressChanged(seekBar: IndicatorSeekBar?, progress: Int, progressFloat: Float, fromUserTouch: Boolean) {
                            if (fromUserTouch || it.selectedValue.get() == null) {
                                val prog = Math.round(progressFloat / (it.input?.range?.toFloat()
                                        ?: 0F)).toDouble() * (it.input?.range ?: 0)
                                it.selectedValue.set(prog.toLong())

                                // For usia anak pengaruh ke pendidikan yg ditempuhnya
                                if (it.input?.description?.toLowerCase()?.contains("usia anak") == true) {
                                    inputs.forEach {
                                        val input = it
                                        input.choices?.forEach {
                                            if (it.minAge != null && it.maxAge != null) {

                                                if (it.minAge!! <= prog && it.maxAge!! >= prog) {
                                                    it.selected.set(true)
                                                    input.selectedChoice.set(it)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    })
                }
            }
        }
    }
}


@BindingAdapter("realValueAttrChanged")
fun setListener(spinner: Spinner, listener: InverseBindingListener?) {
    if (listener != null) {
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                listener.onChange()
            }
        }
    }
}

@BindingAdapter("realValue")
fun setRealValue(view: Spinner, value: Any?) {
    if (view.selectedItem != value) {
        for (i in 0 until (view.adapter?.count ?: 0)) {
            if (view.adapter.getItem(i) == value) {
                view.setSelection(i)
                break
            }
        }
    }
}

@InverseBindingAdapter(attribute = "realValue")
fun getRealValue(spinner: Spinner): Any {
    return spinner.selectedItem
}


@InverseBindingAdapter(attribute = "stringValue")
fun getStringValue(spinner: Spinner): String {
    return spinner.selectedItem.toString()
}



@BindingAdapter("recommendationSummaryFields", "inputs", requireAll = false)
fun setLayoutFromRecommendationSummayFields(viewGroup: ViewGroup, field: MediatorLiveData<List<RecommendationSummaryField>>, inputs: HashMap<String, Any?>) {
    viewGroup.removeAllViews()
    field.observeForever {
        it?.let {
            val inflater = LayoutInflater.from(viewGroup.context)

            it.forEach {
                val dataBinding = DataBindingUtil.inflate<PosRowRecommendationSummaryFieldBinding>(inflater, R.layout.pos_row_recommendation_summary_field, viewGroup, true)
                dataBinding.vm = it
                val value = inputs[it.inputSourceName]?.toString()
                dataBinding.root.visibility = if (value == null || value == "") View.GONE else View.VISIBLE
                dataBinding.editText.setText(value)
                dataBinding.editText.id = it.id
            }
        }
    }
}

@BindingAdapter("app:entries", "handler")
fun setPageOptionToButtons(viewGroup: ViewGroup, pageOptions: MediatorLiveData<List<PageOption>>, handler: SelectionHandler) {
    viewGroup.removeAllViews()

    pageOptions.observeForever {
        it?.let {
            val inflater = LayoutInflater.from(viewGroup.context)
            it.forEach {
                val dataBinding = DataBindingUtil.inflate<PosRowPageOptionToButtonBinding>(inflater, R.layout.pos_row_page_option_to_button, viewGroup, true)
                if (it.title == "Pilihan Asuransi Tambahan") {
                    dataBinding.root.id = 4000
                }
                dataBinding.vm = it
                dataBinding.handler = handler
            }
        }
    }
}


@BindingAdapter("android:onClick")
fun setOnClick(view: LoadingButton, listener: View.OnClickListener) {
    view.imageView.setOnClickListener { listener.onClick(view) }
}