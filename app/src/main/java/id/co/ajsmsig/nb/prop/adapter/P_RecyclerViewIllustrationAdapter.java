package id.co.ajsmsig.nb.prop.adapter;
/*
 Created by faiz_f on 12/09/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import id.co.ajsmsig.nb.R;

public class P_RecyclerViewIllustrationAdapter extends RecyclerView.Adapter<P_RecyclerViewIllustrationAdapter.ViewHolder> {
    private static final int TYPE_FULL = 0;
    private static final int TYPE_NOT_FULL = 1;


    private List<String> mData;
    //    private String[] mData = new String[0];
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public P_RecyclerViewIllustrationAdapter(Context context, List<String> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View itemView = mInflater.inflate(R.layout.p_rv_illustration_item, parent, false);
        itemView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                final ViewGroup.LayoutParams lp = itemView.getLayoutParams();
                if (lp instanceof StaggeredGridLayoutManager.LayoutParams) {
                    StaggeredGridLayoutManager.LayoutParams sglp =
                            (StaggeredGridLayoutManager.LayoutParams) lp;

                    switch (viewType) {
                        case TYPE_FULL:
                            sglp.setFullSpan(true);
                            break;
                        case TYPE_NOT_FULL:
                            sglp.setFullSpan(false);
                            sglp.width = 200;
                            break;
                    }
                    itemView.setLayoutParams(sglp);

                    itemView.setLayoutParams(sglp);
                    final StaggeredGridLayoutManager lm =
                            (StaggeredGridLayoutManager) ((RecyclerView) parent).getLayoutManager();
                    lm.invalidateSpanAssignments();
                }
                itemView.getViewTreeObserver().removeOnPreDrawListener(this);

                return true;
            }
        });


        return new ViewHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        final int modeEleven = (position + 1) % 11;
        switch (modeEleven) {
            case 0:
                return TYPE_FULL;
            case 1:
                return TYPE_NOT_FULL;
            case 2:
                return TYPE_NOT_FULL;
            case 3:
                return TYPE_NOT_FULL;
            case 4:
                return TYPE_NOT_FULL;
            case 5:
                return TYPE_NOT_FULL;
            case 6:
                return TYPE_FULL;
            case 7:
                return TYPE_FULL;
            case 8:
                return TYPE_FULL;
            case 9:
                return TYPE_FULL;
            case 10:
                return TYPE_FULL;
        }
        return TYPE_FULL;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.myTextView.setText(mData.get(position));
//        switch (position + 1) {
//            case 1:
//                LinearLayout.LayoutParams Params1 = new LinearLayout.LayoutParams(10, ViewGroup.LayoutParams.WRAP_CONTENT);
//                holder.myTextView.setLayoutParams(Params1);
//
//                break;
//            default:
//                break;
//        }
    }


    // total number of cells
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView myTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.tv_value);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
