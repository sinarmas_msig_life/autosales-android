package id.co.ajsmsig.nb.prop.model.modelCalcIllustration;

/**
 * Created by rizky_c on 28/08/2017.
 */

public class ProductCodePlanModel {
    private int productCode;
    private int plan;

    public ProductCodePlanModel(int productCode, int plan) {
        this.productCode = productCode;
        this.plan = plan;
    }

    public int getProductCode() {

        return productCode;
    }

    public void setProductCode(int productCode) {

        this.productCode = productCode;
    }

    public int getPlan() {

        return plan;
    }

    public void setPlan(int plan) {

        this.plan = plan;
    }
}
