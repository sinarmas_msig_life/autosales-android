package id.co.ajsmsig.nb.crm.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;

/*
 Created by faiz_f on 08/06/2017.
 */

public class AktivitasSimpleCursorAdapter extends SimpleCursorAdapter {
    private final String LABEL_COLUMN = "LABEL";

    public AktivitasSimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        super.bindView(view, context, cursor);
        TextView tv_tanggal_aktivitas = view.findViewById(R.id.tv_tanggal_aktivitas);
        ImageView iv_circle = view.findViewById(R.id.iv_circle);
        TextView tvActivityName = view.findViewById(R.id.tv_aktivitas);

        Calendar calendar = StaticMethods.toCalendarTime(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SLA_SDATE))));
        String tanggal = StaticMethods.toStringDate(calendar,4);
        tv_tanggal_aktivitas.setText(tanggal);

        int SLA_LAST_POS = cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.SLA_LAST_POS)));

        if (SLA_LAST_POS == 1) {
//            iv_circle.setColorFilter(ContextCompat.getColor(context, R.color.red));
            iv_circle.setVisibility(View.VISIBLE);
        } else {
            iv_circle.setVisibility(View.INVISIBLE);
        }


        //Add "terima nasabah" prefix when new lead is created
        String label = cursor.getString(cursor.getColumnIndexOrThrow(LABEL_COLUMN));
        if (label.equalsIgnoreCase("Menghubungi Nasabah")) {
            label += " Terima nasabah";

        }
        tvActivityName.setText(label);

    }
}
