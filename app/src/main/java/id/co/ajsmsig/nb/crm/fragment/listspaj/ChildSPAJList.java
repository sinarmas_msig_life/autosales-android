package id.co.ajsmsig.nb.crm.fragment.listspaj;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by Fajar.Azhar on 21/03/2018.
 */

public class ChildSPAJList implements Parcelable, Comparable<ChildSPAJList> {
    private long _id;
    private String spajIdTab;
    private String noProposalTab;
    private String namaPp;
    private String namaTt;
    private String premiPokokDi;
    private String kdProdukUa;
    private String virtualAcc;
    private String spajId;
    private String flagDokumen;
    private String subProdukUa;
    private Date inputDate;
    private int month;

    public ChildSPAJList(long _id, String spajIdTab, String noProposalTab, String namaPp, String namaTt, String premiPokokDi, String kdProdukUa, String virtualAcc, String spajId, String flagDokumen, String subProdukUa, Date inputDate, int month) {
        this._id = _id;
        this.spajIdTab = spajIdTab;
        this.noProposalTab = noProposalTab;
        this.namaPp = namaPp;
        this.namaTt = namaTt;
        this.premiPokokDi = premiPokokDi;
        this.kdProdukUa = kdProdukUa;
        this.virtualAcc = virtualAcc;
        this.spajId = spajId;
        this.flagDokumen = flagDokumen;
        this.subProdukUa = subProdukUa;
        this.inputDate = inputDate;
        this.month = month;
    }
    public Date getInputDate() {
        return inputDate;
    }

    public void setInputDate(Date inputDate) {
        this.inputDate = inputDate;
    }
    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getSpajIdTab() {
        return spajIdTab;
    }

    public void setSpajIdTab(String spajIdTab) {
        this.spajIdTab = spajIdTab;
    }

    public String getNoProposalTab() {
        return noProposalTab;
    }

    public void setNoProposalTab(String noProposalTab) {
        this.noProposalTab = noProposalTab;
    }

    public String getNamaPp() {
        return namaPp;
    }

    public void setNamaPp(String namaPp) {
        this.namaPp = namaPp;
    }

    public String getNamaTt() {
        return namaTt;
    }

    public void setNamaTt(String namaTt) {
        this.namaTt = namaTt;
    }

    public String getPremiPokokDi() {
        return premiPokokDi;
    }

    public void setPremiPokokDi(String premiPokokDi) {
        this.premiPokokDi = premiPokokDi;
    }

    public String getKdProdukUa() {
        return kdProdukUa;
    }

    public void setKdProdukUa(String kdProdukUa) {
        this.kdProdukUa = kdProdukUa;
    }

    public String getVirtualAcc() {
        return virtualAcc;
    }

    public void setVirtualAcc(String virtualAcc) {
        this.virtualAcc = virtualAcc;
    }

    public String getSpajId() {
        return spajId;
    }

    public void setSpajId(String spajId) {
        this.spajId = spajId;
    }

    public String getFlagDokumen() {
        return flagDokumen;
    }

    public void setFlagDokumen(String flagDokumen) {
        this.flagDokumen = flagDokumen;
    }

    public String getSubProdukUa() {
        return subProdukUa;
    }

    public void setSubProdukUa(String subProdukUa) {
        this.subProdukUa = subProdukUa;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public ChildSPAJList(Parcel in) {
        _id = in.readLong();
        spajIdTab = in.readString();
        noProposalTab = in.readString();
        namaPp = in.readString();
        namaTt = in.readString();
        premiPokokDi = in.readString();
        kdProdukUa = in.readString();
        virtualAcc = in.readString();
        spajId = in.readString();
        flagDokumen = in.readString();
        subProdukUa = in.readString();
        inputDate = new Date(in.readLong());
        month = in.readInt();
    }

    public static final Creator<ChildSPAJList> CREATOR = new Creator<ChildSPAJList>() {
        @Override
        public ChildSPAJList createFromParcel(Parcel in) {
            return new ChildSPAJList(in);
        }

        @Override
        public ChildSPAJList[] newArray(int size) {
            return new ChildSPAJList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(_id);
        parcel.writeString(spajIdTab);
        parcel.writeString(noProposalTab);
        parcel.writeString(namaPp);
        parcel.writeString(namaTt);
        parcel.writeString(premiPokokDi);
        parcel.writeString(kdProdukUa);
        parcel.writeString(virtualAcc);
        parcel.writeString(spajId);
        parcel.writeString(flagDokumen);
        parcel.writeString(subProdukUa);
        parcel.writeLong(inputDate.getTime());
        parcel.writeInt(month);
    }

    @Override
    public int compareTo(@NonNull ChildSPAJList o) {
        return namaPp.compareToIgnoreCase(o.namaPp);
    }

    @Override
    public String toString() {
        return this.inputDate.toString();
    }
}
