package id.co.ajsmsig.nb.prop.method.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class CommonUtil {
	/**
	 *@author Deddy
	 *@since Mar 10, 2014
	 *@description Class ini untuk menampung semua fungsi tambahan yg dapat digunakan untuk kepentingan Web Services.
	 */
//	private String convertFileToString(File file) throws IOException{
//        byte[] bytes = File.readAllBytes(file.toPath());   
//        return new String(Base64.encode(bytes));
//    }
	
	public static boolean isEmpty(Object cek) {
		if(cek==null) return true;
		else	if(cek instanceof String) {
			String tmp = (String) cek;
            return tmp.trim().equals("");
		}else if(cek instanceof List) {
			List tmp = (List) cek;
			return tmp.isEmpty();
		}else if(cek instanceof Map){
			return ((Map) cek).isEmpty();
		}else return !(cek instanceof Integer) && !(cek instanceof Long) && !(cek instanceof Double) && !(cek instanceof Float) && !(cek instanceof BigDecimal) && !(cek instanceof Date);
    }

}
