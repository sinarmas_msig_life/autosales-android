package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelCabangDa;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelReferralDa;

/**
 * Created by eriza on 14/11/2017.
 */

public class TableLST_Reff {
    private Context context;

    public TableLST_Reff(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(ModelReferralDa me) {
        ContentValues cv = new ContentValues();
        cv.put("NAMA_REFF", me.getNAMA_REFF());
        cv.put("AGENT_CODE", me.getAGENT_CODE());
        cv.put("NAMA_CABANG", me.getNAMA_CABANG());
        cv.put("LCB_NO", me.getLCB_NO());
        cv.put("FLAG_LISENSI", me.getFLAG_LISENSI());
        return cv;
    }

    public ArrayList<ModelReferralDa> getObjectArray(Cursor cursor) {
        ArrayList<ModelReferralDa> List = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ModelReferralDa msa = new ModelReferralDa();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("NAMA_REFF"))) {
                    msa.setNAMA_REFF(cursor.getString(cursor.getColumnIndexOrThrow("NAMA_REFF")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("AGENT_CODE"))) {
                    msa.setAGENT_CODE(cursor.getString(cursor.getColumnIndexOrThrow("AGENT_CODE")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("NAMA_CABANG"))) {
                    msa.setNAMA_CABANG(cursor.getString(cursor.getColumnIndexOrThrow("NAMA_CABANG")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("LCB_NO"))) {
                    msa.setLCB_NO(cursor.getString(cursor.getColumnIndexOrThrow("LCB_NO")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("FLAG_LISENSI"))) {
                    msa.setFLAG_LISENSI(cursor.getInt(cursor.getColumnIndexOrThrow("FLAG_LISENSI")));
                }
                List.add(msa);
                cursor.moveToNext();
            }
            // always close the cursor
            cursor.close();
        }

        return List;
    }

    public ArrayList<ModelCabangDa> getObjectArrayCab(Cursor cursor) {
        ArrayList<ModelCabangDa> List = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ModelCabangDa msa = new ModelCabangDa();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("NAMA_CABANG"))) {
                    msa.setNAMA_CABANG(cursor.getString(cursor.getColumnIndexOrThrow("NAMA_CABANG")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("LCB_NO"))) {
                    msa.setLCB_NO(cursor.getString(cursor.getColumnIndexOrThrow("LCB_NO")));
                }
                List.add(msa);
                cursor.moveToNext();
            }
            // always close the cursor
            cursor.close();
        }

        return List;
    }
}
