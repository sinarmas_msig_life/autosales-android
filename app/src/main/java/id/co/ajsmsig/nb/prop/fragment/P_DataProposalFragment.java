package id.co.ajsmsig.nb.prop.fragment;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.AutoCompleteTextViewClickListener;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.MemberModel;
import id.co.ajsmsig.nb.crm.model.hardcode.RequestCode;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.prop.activity.P_FormDataProposalActivity;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.database.TableDataProposal;
import id.co.ajsmsig.nb.prop.method.P_StaticMethods;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.method.ValueView;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductRiderModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductTopUpModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductUlinkModel;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;
import id.co.ajsmsig.nb.prop.model.hardCode.P_InsuredAgeFromFlag;
import id.co.ajsmsig.nb.util.MessageEvent;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static id.co.ajsmsig.nb.crm.method.StaticMethods.toCalendar;
import static id.co.ajsmsig.nb.crm.method.StaticMethods.toStringDate;
import static id.co.ajsmsig.nb.prop.method.P_StaticMethods.getDefaultTPValue;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FragmentPagePosition.DATA_PROPOSAL_PAGE;

/**
 * Created by faiz_f on 03/02/2017.
 */

public class P_DataProposalFragment extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, AdapterView.OnItemClickListener, View.OnFocusChangeListener {
    private final static String TAG = P_DataProposalFragment.class.getSimpleName();

    private P_ProposalModel p_proposalModel;
    private P_MstDataProposalModel p_mstDataProposalModel;
    private P_MstProposalProductModel p_mstProposalProductModel;
    private ArrayList<P_MstProposalProductUlinkModel> p_mstProposalProductUlinkModels;
    private ArrayList<P_MstProposalProductRiderModel> p_mstProposalProductRiderModels;
    private ArrayList<P_MstProposalProductTopUpModel> p_mstProposalProductTopUpModels;
    private MemberModel memberModel;
    private DropboxModel dRencana = new DropboxModel();
    private DropboxModel dRencanaSub = new DropboxModel();
    private DropboxModel dPacket = new DropboxModel(); //tambahan untuk paket smile link ultimate
    private P_Select select;
    private QueryUtil queryUtil;
    private boolean isSubProductSelected;

    private int groupId;
    private int flagPacket;
    private String kodeRegional = "";

    public P_DataProposalFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        groupId = sharedPreferences.getInt("GROUP_ID", 0);
        kodeRegional = sharedPreferences.getString("KODE_REGIONAL", "");
        queryUtil = new QueryUtil(getContext());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.p_fragment_data_proposal, container, false);
        ButterKnife.bind(this, view);

        ((P_MainActivity) getActivity()).setSecondToolbarTitle(getString(R.string.fragment_title_data_proposal));

        select = new P_Select(getContext());
        p_proposalModel = P_MainActivity.myBundle.getParcelable(getString(R.string.proposal_model));
        assert p_proposalModel != null;
        p_mstDataProposalModel = p_proposalModel.getMst_data_proposal();
        p_mstProposalProductModel = p_proposalModel.getMst_proposal_product();
        p_mstProposalProductUlinkModels = p_proposalModel.getMst_proposal_product_ulink();
        p_mstProposalProductRiderModels = p_proposalModel.getMst_proposal_product_rider();
        p_mstProposalProductTopUpModels = p_proposalModel.getMst_proposal_product_topup();

//        p_mstDataProposalModel = p_proposalModel != null ? p_proposalModel.getMst_data_proposal() : null;
//        p_mstProposalProductModel = p_proposalModel != null ? p_proposalModel.getMst_proposal_product() : null;
//        p_mstProposalProductUlinkModels = p_proposalModel != null ? p_proposalModel.getMst_proposal_product_ulink() : null;
//        p_mstProposalProductRiderModels = p_proposalModel != null ? p_proposalModel.getMst_proposal_product_rider() : null;
//        p_mstProposalProductTopUpModels = p_proposalModel != null ? p_proposalModel.getMst_proposal_product_topup() : null;

        ac_rencana.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_rencana, this));
        ac_rencana.setOnClickListener(this);
        ac_rencana.setOnFocusChangeListener(this);
        ac_rencana.addTextChangedListener(new MTextWatcher(ac_rencana));
        ac_rencana_sub.setOnClickListener(this);
        ac_rencana_sub.setOnFocusChangeListener(this);
        ac_rencana_sub.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_rencana_sub, this));
        ac_rencana_sub.addTextChangedListener(new MTextWatcher(ac_rencana_sub));
        ac_packet.setOnClickListener(this);
        ac_packet.setOnFocusChangeListener(this);
        ac_packet.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_packet, this));
        ac_packet.addTextChangedListener(new MTextWatcher(ac_packet));

        iv_pp.setOnClickListener(this);
        tv_ubah_pp.setOnClickListener(this);
        iv_tt.setOnClickListener(this);
        tv_ubah_tt.setOnClickListener(this);
        cb_same.setOnCheckedChangeListener(this);
//        btn_lanjut.setOnClickListener(this);
//        btn_kembali.setOnClickListener(this);

        Button btn_lanjut = getActivity().findViewById(R.id.btn_lanjut);
        btn_lanjut.setText(getString(R.string.lanjut));
        btn_lanjut.setOnClickListener(this);

        Button btn_kembali = getActivity().findViewById(R.id.btn_kembali);
        btn_kembali.setText(getString(R.string.kembali));
        btn_kembali.setOnClickListener(this);

        updateView();
        return view;
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.menu_va_proposal, menu);
//    }

    @Override
    public void onResume() {
        setAdapter();
        if (p_mstProposalProductModel.getLsbs_id() != null && p_mstProposalProductModel.getLsbs_id() != 0 ) {
            setRencanaSubAdapter();
            if (p_mstProposalProductModel.getLsdbs_number() != null) {
                if (new P_Select(getContext()).getLstPacket(p_mstProposalProductModel.getLsbs_id(), p_mstProposalProductModel.getLsdbs_number()).size() > 0) {
                    if(groupId == 40){
                        setPacketAdapter();
                    }
                }
            }
        }
//        if (p_mstProposalProductModel.getLsdbs_number() != null && p_mstProposalProductModel.getLsbs_id() != null) {
//            setPacketAdapter();
//        }

//        if (groupId == 40) {
//            cl_packet.setVisibility(View.VISIBLE);
//            setPacketAdapter();
//        } else {
//            cl_packet.setVisibility(View.GONE);
//        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: status");
//        saveToMyBundle();
    }

    public void setAdapter() {
        ArrayList<DropboxModel> dRencanas = new P_Select(getContext()).selectListRencana(groupId);
        ArrayAdapter<DropboxModel> mRencanaAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, dRencanas);
        ac_rencana.setAdapter(mRencanaAdapter);
    }

    public void setRencanaSubAdapter() {
        cl_rencana_sub.setVisibility(View.VISIBLE);
        ArrayList<DropboxModel> dRencanaSubs = new P_Select(getContext()).selectListRencanaSub(groupId, dRencana.getId());
        ArrayAdapter<DropboxModel> mRencanaSubAdapter = new ArrayAdapter<DropboxModel>(getContext(),
                android.R.layout.simple_dropdown_item_1line, dRencanaSubs);
        ac_rencana_sub.setAdapter(mRencanaSubAdapter);
    }

    public void setPacketAdapter() { // paket adapter
        cl_packet.setVisibility(View.VISIBLE);
        ArrayList<DropboxModel> dPackets = new P_Select(getContext()).selectListPacket(groupId, dRencana.getId());
        ArrayAdapter<DropboxModel> mPacketAdapter = new ArrayAdapter<DropboxModel>(getContext(),
                android.R.layout.simple_dropdown_item_1line, dPackets);
        ac_packet.setAdapter(mPacketAdapter);
    }

    public void showToast(String yuhu) {
        Toast.makeText(getContext(), yuhu, Toast.LENGTH_SHORT).show();
    }


    private void updateView() {
        ((P_MainActivity) getActivity()).getSecond_toolbar().setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_save:
                        saveToMyBundle();
                        P_MainActivity.saveState(getContext(), true);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
        Log.d(TAG, "updateView: namapp = " + p_mstDataProposalModel.getNama_pp());

        updateViewPP(p_mstDataProposalModel);
        updateViewTT(p_mstDataProposalModel);
        updateViewAG(p_mstDataProposalModel);

        if (p_mstDataProposalModel.getFlag_tt_calon_bayi() != null) {
            cb_calon_bayi.setChecked(p_mstDataProposalModel.getFlag_tt_calon_bayi() == 1);
        }

        if(groupId == 40){ //SIAP2U
            MethodSupport.active_view(0, ac_rencana);
            MethodSupport.active_view(0, ac_rencana_sub);
            MethodSupport.active_view(0, ac_packet);
        }else {
            MethodSupport.active_view(1, ac_rencana);
            MethodSupport.active_view(1, ac_rencana_sub);
            MethodSupport.active_view(1, ac_packet);
        }

        P_Select p_select = new P_Select(getContext());
//        if (new P_Select(getContext()).getLstPacket(p_mstProposalProductModel.getLsbs_id(), p_mstProposalProductModel.getLsdbs_number()).size() > 0) {
//            cl_packet.setVisibility(View.VISIBLE);
//        } else {
//            cl_packet.setVisibility(View.GONE);
//        }

//        if (p_mstProposalProductModel.getLsdbs_number() != null && p_mstProposalProductModel.getLsbs_id() != null) {
//            cl_packet.setVisibility(View.VISIBLE);
//        }

        if (p_mstProposalProductModel.getLsbs_id() != null && p_mstProposalProductModel.getLsbs_id() != 0) {
            dRencana = p_select.selectRencana(p_mstProposalProductModel.getLsbs_id(), groupId);
            ac_rencana.setText(dRencana.getLabel());
            if (dRencana.getId() == 208 ) {
                fl_calon_bayi.setVisibility(View.VISIBLE);
            } else {
                fl_calon_bayi.setVisibility(View.GONE);
            }
        }
        if (p_mstProposalProductModel.getLsdbs_number() != null) {
            dRencanaSub = p_select.selectRencanaSub(p_mstProposalProductModel.getLsbs_id(), p_mstProposalProductModel.getLsdbs_number());
            ac_rencana_sub.setText(dRencanaSub.getLabel());
            if(p_mstDataProposalModel.getFlag_packet() != null){
                dPacket = p_select.selectPacket(p_mstProposalProductModel.getLsbs_id(), p_mstProposalProductModel.getLsdbs_number(), p_mstDataProposalModel.getFlag_packet());
                ac_packet.setText(dPacket.getLabel());
            }
            if (dRencanaSub.getIdSecond() >= 21 && dRencana.getId() == 208 && dRencanaSub.getIdSecond() <= 24 ) {
                fl_calon_bayi.setVisibility(View.GONE);
            } else if (dRencana.getId() != 208 ) {
                fl_calon_bayi.setVisibility(View.GONE);
            } else if (dRencanaSub.getIdSecond() <= 20 && dRencana.getId() == 208 && dRencanaSub.getIdSecond() >= 25) {
                fl_calon_bayi.setVisibility(View.VISIBLE);
            }
        }

//            dPacket = p_select.selectPacket(p_mstProposalProductModel.getLsbs_id(), p_mstProposalProductModel.getLsdbs_number());
//            ac_packet.setText(dPacket.getLabel());


    }

    private void updateViewPP(P_MstDataProposalModel p_mstDataProposalModel) {
        if (p_mstDataProposalModel.getNama_pp() != null) {
            tv_isi_pp.setVisibility(View.GONE);
            iv_pp.setVisibility(View.GONE);
            if(groupId == 40){
                tv_ubah_pp.setVisibility(View.GONE);
                tv_ubah_pp.setEnabled(false);
            }else{
                tv_ubah_pp.setVisibility(View.VISIBLE);
                tv_ubah_pp.setEnabled(true);
            }
            ll_pp.setVisibility(View.VISIBLE);

            String[][] values = new String[][]{
                    new String[]{getString(R.string.nama), p_mstDataProposalModel.getNama_pp()},
                    new String[]{getString(R.string.tanggal_lahir), p_mstDataProposalModel.getTgl_lahir_pp()},
                    new String[]{getString(R.string.umur), String.valueOf(p_mstDataProposalModel.getUmur_pp())},
                    new String[]{getString(R.string.jenis_kelamin), P_StaticMethods.getSexLabel(getContext(), p_mstDataProposalModel.getSex_pp())}
            };
            new ValueView(getContext()).addToLinearLayout(ll_pp, values);
            fl_same.setVisibility(View.VISIBLE);

            boolean samePerson = P_StaticMethods.isPPandTTSamePerson(p_mstDataProposalModel);
            ContentValues cvvv = new TableDataProposal(getContext()).getContentValues(p_mstDataProposalModel);
            Log.d(TAG, "updateViewPP: cvvv" + cvvv);
            Log.d(TAG, "updateViewPP: samePerson" + samePerson);
            if (samePerson) {
                cb_same.setChecked(true);
            } else {
                cb_same.setOnCheckedChangeListener(null);
                cb_same.setChecked(false);
                cb_same.setOnCheckedChangeListener(this);
            }

        } else {
            tv_isi_pp.setVisibility(View.VISIBLE);
            iv_pp.setVisibility(View.VISIBLE);
            tv_ubah_pp.setVisibility(View.GONE);
            ll_pp.setVisibility(View.GONE);
            fl_same.setVisibility(View.GONE);
            cb_same.setChecked(false);
        }
    }

    private void updateViewTT(P_MstDataProposalModel p_mstDataProposalModel) {
        if (p_mstDataProposalModel.getNama_tt() != null) {
            tv_isi_tt.setVisibility(View.GONE);
            iv_tt.setVisibility(View.GONE);
            tv_ubah_tt.setVisibility(View.VISIBLE);
            ll_tt.setVisibility(View.VISIBLE);

            String[][] values = new String[][]{
                    new String[]{getString(R.string.nama), p_mstDataProposalModel.getNama_tt()},
                    new String[]{getString(R.string.tanggal_lahir), p_mstDataProposalModel.getTgl_lahir_tt()},
                    new String[]{getString(R.string.umur), String.valueOf(p_mstDataProposalModel.getUmur_tt())},
                    new String[]{getString(R.string.jenis_kelamin), P_StaticMethods.getSexLabel(getContext(), p_mstDataProposalModel.getSex_tt())}
            };
            new ValueView(getContext()).addToLinearLayout(ll_tt, values);

            boolean samePerson = P_StaticMethods.isPPandTTSamePerson(p_mstDataProposalModel);
            if (samePerson) {
                cb_same.setChecked(true);
            } else {
                cb_same.setOnCheckedChangeListener(null);
                cb_same.setChecked(false);
                cb_same.setOnCheckedChangeListener(this);
            }
        } else {
            tv_isi_tt.setVisibility(View.VISIBLE);
            iv_tt.setVisibility(View.VISIBLE);
            tv_ubah_tt.setVisibility(View.GONE);
            ll_tt.setVisibility(View.GONE);
        }
    }

    private void updateViewAG(P_MstDataProposalModel p_mstDataProposalModel) {
        if (p_mstDataProposalModel.getNama_agen() != null) {
            tv_isi_ag.setVisibility(View.GONE);
            iv_ag.setVisibility(View.GONE);
//            tv_ubah_ag.setVisibility(View.VISIBLE);
            ll_ag.setVisibility(View.VISIBLE);

            String[][] values = new String[][]{
                    new String[]{getString(R.string.nama), p_mstDataProposalModel.getNama_agen()},
                    new String[]{getString(R.string.kode), p_mstDataProposalModel.getKode_agen()},
                    new String[]{getString(R.string.tanggal_input), String.valueOf(p_mstDataProposalModel.getTgl_input())}
            };
            new ValueView(getContext()).addToLinearLayout(ll_ag, values);
        } else {
            tv_isi_ag.setVisibility(View.VISIBLE);
            iv_ag.setVisibility(View.VISIBLE);
//            tv_ubah_ag.setVisibility(View.GONE);
            ll_ag.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_pp:
                goToForm(0);
                break;
            case R.id.tv_ubah_pp:
                goToForm(0);
                break;
            case R.id.iv_tt:
                goToForm(1);
                break;
            case R.id.tv_ubah_tt:
                goToForm(1);
                break;
            case R.id.ac_rencana:
                ac_rencana.showDropDown();
                break;
            case R.id.ac_rencana_sub:
                ac_rencana_sub.showDropDown();
                break;
            case R.id.ac_packet:
                ac_packet.showDropDown();
                break;
            case R.id.btn_lanjut:
                clickLanjut();
                break;
            case R.id.btn_kembali:
                ((P_MainActivity) getActivity()).onKembaliPressed();
                break;
            default:
                break;
        }
    }


    private void clickLanjut() {
        View errorRelativeView = null;
        String errorMessage = "";
        boolean success = true;

        tv_error_rencana.setVisibility(View.GONE);

        if (p_mstDataProposalModel.getNama_tt() == null) {
            errorRelativeView = cl_tt;
            errorMessage = "Isi data tertanggung";
            success = false;
        }

        if (p_mstDataProposalModel.getSex_tt() == null) {
            errorRelativeView = cl_tt;
            errorMessage = "Pilih jenis kelamin tertanggung";
            success = false;
        } else {
            if (fl_calon_bayi.isShown()) { //StaticMethods.isViewVisible(fl_calon_bayi
                if (cb_calon_bayi.isChecked()) {
                    if (p_mstDataProposalModel.getSex_tt() == 1) {
                        errorRelativeView = fl_calon_bayi;
                        errorMessage = "Jenis kelamin tertanggung harus wanita";
                        success = false;
                    }
                }
            }
        }

        if (p_mstDataProposalModel.getTgl_lahir_tt() == null) {
            errorRelativeView = cl_tt;
            errorMessage = "Isi tanggal lahir tertanggung";
            success = false;
        }


        if (p_mstDataProposalModel.getNama_pp() == null) {
            errorRelativeView = cl_pp;
            errorMessage = "Isi data pemegang polis";
            success = false;
        }

        if (p_mstDataProposalModel.getSex_pp() == null) {
            errorRelativeView = cl_pp;
            errorMessage = "Pilih jenis kelamin pemegang polis";
            success = false;
        }

        if (p_mstDataProposalModel.getTgl_lahir_pp() == null) {
            errorRelativeView = cl_pp;
            errorMessage = "Isi tanggal lahir pemegang polis";
            success = false;
        }

        if (ac_packet.getText().toString().trim().length() == 0 && ac_packet.isShown()) {
            iv_circle_packet.setColorFilter(ContextCompat.getColor(getContext(), R.color.red));
            tv_error_packet.setText(R.string.field_requirement);
            tv_error_packet.setVisibility(View.VISIBLE);
            success = false;
        } else {
            if (ac_rencana.getText().toString().trim().length() == 0) {
                iv_circle_rencana.setColorFilter(ContextCompat.getColor(getContext(), R.color.red));
                tv_error_rencana.setText(R.string.field_requirement);
                tv_error_rencana.setVisibility(View.VISIBLE);
                success = false;
            } else {
                if (ac_rencana_sub.getText().toString().trim().length() == 0) {
                    iv_circle_rencana_sub.setColorFilter(ContextCompat.getColor(getContext(), R.color.red));
                    tv_error_rencana_sub.setText(R.string.field_requirement);
                    tv_error_rencana_sub.setVisibility(View.VISIBLE);
                    success = false;
                } else {
                    if (p_mstDataProposalModel.getTgl_lahir_pp() != null || p_mstDataProposalModel.getTgl_lahir_tt() != null) {
                        int psetId = select.selectPsetIdProductSetup(dRencanaSub.getId(), dRencanaSub.getIdSecond());
                        HashMap<String, Integer> ageBound = select.selectAgeBound(psetId);
                        int holderAgeFrom = ageBound.get(getString(R.string.HOLDER_AGE_FROM)); //data masih kosong di local nya
                        int holderAgeTo = ageBound.get(getString(R.string.HOLDER_AGE_TO));
                        int insuredAgeFrom = ageBound.get(getString(R.string.INSURED_AGE_FROM));
                        int insuredAgeTo = ageBound.get(getString(R.string.INSURED_AGE_TO));
                        int insuredAgeFromFlag = ageBound.get(getString(R.string.INSURED_AGE_FROM_FLAG));

                        if (psetId == 1260){
                            if(p_mstDataProposalModel.getFlag_packet() == 35){
                                insuredAgeTo = insuredAgeTo - 5;
                            }
                        }

                        Log.d(TAG, "clickLanjut: fl_calon_bayi visible " + fl_calon_bayi.isShown()); //StaticMethods.isViewVisible(fl_calon_bayi))
                        Log.d(TAG, "clickLanjut: cb_calon_bayi is selectd " + cb_calon_bayi.isChecked());

                        if (p_mstDataProposalModel.getTgl_lahir_tt() != null) {
                            Calendar tglLahirTT = StaticMethods.toCalendar(p_mstDataProposalModel.getTgl_lahir_tt());
                            if (fl_calon_bayi.isShown() && cb_calon_bayi.isChecked()) { //StaticMethods.isViewVisible(fl_calon_bayi)
                                Log.d(TAG, "clickLanjut: calon bayi visible dan selcted");
                                insuredAgeFrom = 18;
                                insuredAgeTo = 40;
                                insuredAgeFromFlag = P_InsuredAgeFromFlag.YEAR;
                            }
                            switch (insuredAgeFromFlag) {
                                case P_InsuredAgeFromFlag.YEAR:
                                    if (p_mstDataProposalModel.getUmur_tt() < insuredAgeFrom) {
                                        errorRelativeView = cl_tt;
                                        errorMessage = getString(R.string.tt_umur_min).replace("#{tahun}", String.valueOf(insuredAgeFrom));
                                        success = false;
                                    }
                                    break;
                                case P_InsuredAgeFromFlag.DATE:
                                    int umurTTHari = StaticMethods.onCountHari(tglLahirTT);
                                    if (umurTTHari < insuredAgeFrom) {
                                        errorRelativeView = cl_tt;
                                        errorMessage = getString(R.string.tt_umur_min_zero).replace("#{hari}", String.valueOf(insuredAgeFrom));
                                        success = false;
                                    }
                                    break;
                                case P_InsuredAgeFromFlag.MONTH:
                                    int umurTTBulan = StaticMethods.onCountBulan(tglLahirTT);
                                    if(umurTTBulan < insuredAgeFrom) {
                                        errorRelativeView = cl_tt;
                                        errorMessage = getString(R.string.tt_umur_min_bulan).replace("#{bulan}", String.valueOf(insuredAgeFrom));
                                        success = false;
                                    }
                                    break;
                                default:
                                    break;
                            }

                            Log.d(TAG, "clickLanjut: umur tt = " + p_mstDataProposalModel.getUmur_tt() + " dan batas umur tt = " + insuredAgeTo);
                            if (p_mstDataProposalModel.getUmur_tt() > insuredAgeTo) {
                                Log.d(TAG, "clickLanjut dalam: umur tt = " + p_mstDataProposalModel.getUmur_tt() + " dan batas umur tt = " + insuredAgeTo);
                                errorRelativeView = cl_tt;
                                errorMessage = getString(R.string.tt_umur_max).replace("#{tahun}", String.valueOf(insuredAgeTo));
                                success = false;
                            }
                        }

                        if (p_mstDataProposalModel.getTgl_lahir_pp() != null) {
                            if (p_mstDataProposalModel.getUmur_pp() < holderAgeFrom) {
                                errorRelativeView = cl_pp;
                                errorMessage = getString(R.string.pp_umur_min).replace("#{tahun}", String.valueOf(holderAgeFrom));
                                success = false;
                            } else if (p_mstDataProposalModel.getUmur_pp() > holderAgeTo) {
                                errorRelativeView = cl_pp;
                                errorMessage = getString(R.string.pp_umur_max).replace("#{tahun}", String.valueOf(holderAgeTo));
                                success = false;
                            }
                        }
                    }
                }
            }
        }
        if (success) {
            if (p_mstProposalProductModel.getLsdbs_number() == null) {
                goToNextPage();
            } else {
                if (p_mstProposalProductModel.getLsbs_id().equals(dRencana.getId()) && p_mstProposalProductModel.getLsdbs_number().equals(dRencanaSub.getIdSecond())) {
                    goToNextPage();
                } else {
                    showDialogValidation();
                }
            }
        } else {
            Log.d(TAG, "clickOk: false");
//            isClickAble = true;
            setRelativeLayoutError(errorRelativeView, errorMessage);
        }
    }

    private void goToNextPage() {
//        if(groupId != 40){

        int psetId = select.selectPsetIdProductSetup(dRencanaSub.getId(), dRencanaSub.getIdSecond());

//        if (p_mstProposalProductModel.getFlag_packet() != 0 || groupId == 40) {
//            HashMap<String, Integer> productSetup = select.selectProductSetupPacket(p_mstProposalProductModel.getLsbs_id(), p_mstProposalProductModel.getLsdbs_number(), p_mstProposalProductModel.getFlag_packet());
//            p_mstProposalProductModel.setThn_cuti_premi(productSetup.get(getContext().getString(R.string.INSURED_PERIOD)));
//
//            List<DropboxModel> list_invest = select.selectListJenisInvest(psetId, p_mstProposalProductModel.getLku_id(),"0");
//                P_MstProposalProductUlinkModel  mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
//                mstProposalProductUlinkModel.setLji_id(list_invest.get(0).getIdString());
//                mstProposalProductUlinkModel.setMdu_persen(100);
//                p_mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
//
//
//            saveToMyBundle();
//            Fragment fragment = new P_ResultFragment();
//            ((P_MainActivity) getActivity()).setFragmentBackStack(fragment, "Result Fragment");
//        }else {

        saveToMyBundle();
        ((P_MainActivity)getActivity()).calculateMaxPage(psetId, dRencanaSub.getId(), dRencanaSub.getIdSecond(), p_mstDataProposalModel.getFlag_tt_calon_bayi());
        Fragment fragment = new P_DetailProposalFragment();
        ((P_MainActivity) getActivity()).setFragmentBackStack(fragment, "Detail Produk");
//        }

    }

    private void showDialogValidation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Konfirmasi");
        builder.setMessage(getString(R.string.message_konfirmasi_beda_produk));

        builder.setPositiveButton("IYA", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                p_mstProposalProductModel = new P_MstProposalProductModel();
                p_mstProposalProductUlinkModels = new ArrayList<>();
                p_mstProposalProductRiderModels = new ArrayList<>();
                p_mstProposalProductTopUpModels = getDefaultTPValue();

                goToNextPage();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    public void saveToMyBundle() {
        p_mstDataProposalModel.setLast_page_position(DATA_PROPOSAL_PAGE);
        if (dRencana.getId() != null) {
            p_mstProposalProductModel.setLsbs_id(dRencana.getId());
        }

        if (ac_rencana_sub.getText().toString().trim().length() != 0) {
            p_mstProposalProductModel.setLsdbs_number(dRencanaSub.getIdSecond());
        }

        if (ac_packet.getText().toString().trim().length() != 0) {
            p_mstDataProposalModel.setFlag_packet(dPacket.getFlagPacket());
        }

        if (fl_calon_bayi.isShown()) { //StaticMethods.isViewVisible(fl_calon_bayi)
            if (cb_calon_bayi.isChecked()) {
                p_mstDataProposalModel.setFlag_tt_calon_bayi(1);
            } else {
                p_mstDataProposalModel.setFlag_tt_calon_bayi(0);
            }
        } else {
            p_mstDataProposalModel.setFlag_tt_calon_bayi(0);
        }

        int lsgbId = select.selectLsgbId(p_mstProposalProductModel.getLsbs_id());

        if (lsgbId == 17) {
            if (p_mstDataProposalModel.getUmur_tt() == 0) {
                p_mstDataProposalModel.setUmur_tt(1);
            }
        } else if (p_mstProposalProductModel.getLsbs_id() == 208){
            if (p_mstDataProposalModel != null) {
                if (p_mstDataProposalModel.getTgl_lahir_tt() != null) {
                    Calendar calendar = toCalendar(p_mstDataProposalModel.getTgl_lahir_tt());
                    p_mstDataProposalModel.setUmur_tt(Integer.valueOf(StaticMethods.onCountUsia(calendar)));
                }
            }
        } else {
            if (p_mstDataProposalModel != null) {
                if (p_mstDataProposalModel.getTgl_lahir_tt() != null) {
                    Calendar calendar = toCalendar(p_mstDataProposalModel.getTgl_lahir_tt());
                    p_mstDataProposalModel.setUmur_tt(Integer.valueOf(StaticMethods.onCountUsia(calendar)));
                }
            }
        }


        p_proposalModel.setMst_data_proposal(p_mstDataProposalModel);
        p_proposalModel.setMst_proposal_product(p_mstProposalProductModel);
        p_proposalModel.setMst_proposal_product_rider(p_mstProposalProductRiderModels);
        p_proposalModel.setMst_proposal_product_ulink(p_mstProposalProductUlinkModels);
        p_proposalModel.setMst_proposal_product_topup(p_mstProposalProductTopUpModels);
        P_MainActivity.myBundle.putParcelable(getString(R.string.proposal_model), p_proposalModel);

    }

    public void setRelativeLayoutError(View relLayout, String errorMessage) {
        if (relLayout != null) {
//            relLayout.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));
//            relLayout.requestFocus();

            final Snackbar snack = Snackbar.make(getView(), errorMessage, Snackbar.LENGTH_LONG);
            View view = snack.getView();
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snack.dismiss();
                }
            });
            view.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            view.setBackgroundColor(Color.WHITE);
            TextView tv = view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
            tv.setGravity(Gravity.CENTER);
            snack.show();

            view.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));
            view.requestFocus();
        }
    }


//    public void setFragment(Fragment fragment, String fragmentTag) {
//        if (fragment != null) {
//            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
//            fragmentTransaction.replace(R.id.fl_container, fragment);
//            fragmentTransaction.addToBackStack(fragmentTag);
//            fragmentTransaction.commit();
//        }
//    }


    public void goToForm(int fieldType) {
        Intent intent = new Intent(getContext(), P_FormDataProposalActivity.class);
        intent.putExtra(getString(R.string.FIELD_TYPE), fieldType);//1 itu pp
        intent.putExtra(getString(R.string.MST_DATA_PROPOSAL), p_mstDataProposalModel);
        startActivityForResult(intent, RequestCode.FILL_FORM_DATA_PROPOSAL);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == RequestCode.FILL_FORM_DATA_PROPOSAL) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                P_MstDataProposalModel updateDataProposal = data.getParcelableExtra(getString(R.string.MST_DATA_PROPOSAL));
                switch (data.getIntExtra(getString(R.string.FIELD_TYPE), 0)) {
                    case 0:
                        p_mstDataProposalModel.setNama_pp(updateDataProposal.getNama_pp());
                        p_mstDataProposalModel.setTgl_lahir_pp(updateDataProposal.getTgl_lahir_pp());
                        p_mstDataProposalModel.setUmur_pp(updateDataProposal.getUmur_pp());
                        p_mstDataProposalModel.setSex_pp(updateDataProposal.getSex_pp());
                        updateViewPP(p_mstDataProposalModel);
                        break;
                    case 1:
                        p_mstDataProposalModel.setNama_tt(updateDataProposal.getNama_tt());
                        p_mstDataProposalModel.setTgl_lahir_tt(updateDataProposal.getTgl_lahir_tt());
                        p_mstDataProposalModel.setUmur_tt(updateDataProposal.getUmur_tt());
                        p_mstDataProposalModel.setSex_tt(updateDataProposal.getSex_tt());
                        updateViewTT(p_mstDataProposalModel);
                        break;
//                    case 2:
//                        updateViewAG(p_mstDataProposalModel);
//                        break;
                    default:
                        throw new IllegalArgumentException("Field Type belum diset");
                }
            }
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            p_mstDataProposalModel.setNama_tt(p_mstDataProposalModel.getNama_pp());
            p_mstDataProposalModel.setTgl_lahir_tt(p_mstDataProposalModel.getTgl_lahir_pp());
            p_mstDataProposalModel.setUmur_tt(p_mstDataProposalModel.getUmur_pp());
            p_mstDataProposalModel.setSex_tt(p_mstDataProposalModel.getSex_pp());
        } else {
            p_mstDataProposalModel.setNama_tt(null);
            p_mstDataProposalModel.setTgl_lahir_tt(null);
            p_mstDataProposalModel.setUmur_tt(null);
            p_mstDataProposalModel.setSex_tt(null);
        }
        updateViewTT(p_mstDataProposalModel);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String test = "SMART PLAN PROTECTION";
        switch (view.getId()) {
            case R.id.ac_rencana:
                DropboxModel chooseItem = (DropboxModel) parent.getAdapter().getItem(position);
                if (chooseItem.getId() == 208 && !chooseItem.getLabel().equals(test)) {
                    fl_calon_bayi.setVisibility(View.VISIBLE);
                } else if (chooseItem.getId() != 208 ) {
                    fl_calon_bayi.setVisibility(View.GONE);
                } else if (chooseItem.getId() == 208 && chooseItem.getLabel().equals(test) ) {
                    fl_calon_bayi.setVisibility(View.GONE);
                }

                if (!dRencana.equals(chooseItem)) {
                    dRencana = chooseItem;
                    setRencanaSubAdapter();
                    ac_rencana_sub.setText("");
//                    if (new P_Select(getContext()).getLstPacket(p_mstProposalProductModel.getLsbs_id(), p_mstProposalProductModel.getLsdbs_number()).size() > 0) {
//                        setPacketAdapter();
//                        ac_packet.setText("");
//                    }
                }
                int lsgbId = select.selectLsgbId(p_mstProposalProductModel.getLsbs_id());
                if (lsgbId == 17) {
                    if (p_mstDataProposalModel.getUmur_tt() == 0) {
                        p_mstDataProposalModel.setUmur_tt(1);
                    }
                } else if (p_mstProposalProductModel.getLsbs_id() == 208){
                    if (p_mstDataProposalModel != null) {
                        if (p_mstDataProposalModel.getTgl_lahir_tt() != null) {
                            Calendar calendar = toCalendar(p_mstDataProposalModel.getTgl_lahir_tt());
                            p_mstDataProposalModel.setUmur_tt(Integer.valueOf(StaticMethods.onCountUsia(calendar)));
                        }
                    }
                }else {
                    if (p_mstDataProposalModel != null) {
                        if (p_mstDataProposalModel.getTgl_lahir_tt() != null) {
                            Calendar calendar = toCalendar(p_mstDataProposalModel.getTgl_lahir_tt());
                            p_mstDataProposalModel.setUmur_tt(Integer.valueOf(StaticMethods.onCountUsia(calendar)));
                        }
                    }
                }
                updateViewTT(p_mstDataProposalModel);

                //Pass the product id and product name to P_MainActivity via event bus
                String productId = dRencana.getId().toString();
                String productName = dRencana.getLabel();
                //Post the event bus
                EventBus.getDefault().post(new MessageEvent(productId, productName));

                break;
            case R.id.ac_rencana_sub:
                dRencanaSub = (DropboxModel) parent.getAdapter().getItem(position);
                if (dRencanaSub.getIdSecond() >= 21 && dRencana.getId() == 208 && dRencanaSub.getIdSecond() <= 24 ) {
                    fl_calon_bayi.setVisibility(View.GONE);
                }
                else if (dRencana.getId() != 208) {
                    fl_calon_bayi.setVisibility(View.GONE);
                }
                else if (dRencanaSub.getIdSecond() <= 20 && dRencana.getId() == 208 && dRencanaSub.getIdSecond() >= 25) {
                    fl_calon_bayi.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.ac_packet:
                dPacket = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            default:
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
    /**
     * Invoked when :
     * 1. user select "gunakan va" from the dialog, therefore make the product dropdown unable to changed again
     * 2. the selected proposal already has a va, therefore make the product dropdown unable to changed again
     * @param event
     */

    @Subscribe
    public void onEvent(MessageEvent event) {
        boolean shouldDisableProductReselect = event.isShouldDisableProductReselect();
        if (shouldDisableProductReselect) {
            ac_rencana.setEnabled(false);
            ac_rencana.setClickable(false);
            ac_rencana.setFocusable(false);
            ac_rencana.setFocusableInTouchMode(false);
        }
    }



    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.ac_rencana:
                if (hasFocus) ac_rencana.showDropDown();
                break;
            case R.id.ac_rencana_sub:
                if (hasFocus) ac_rencana_sub.showDropDown();
                break;
            case R.id.ac_packet:
                if (hasFocus) ac_packet.showDropDown();
                break;
            default:
                break;
        }
    }

    private class MTextWatcher implements TextWatcher {
        private View view;

        MTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                case R.id.ac_rencana:
                    if (StaticMethods.isTextWidgetEmpty(ac_rencana)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_rencana.setColorFilter(color);
                    tv_error_rencana.setVisibility(View.GONE);
                    break;
                case R.id.ac_rencana_sub:
                    if (StaticMethods.isTextWidgetEmpty(ac_rencana_sub)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_rencana_sub.setColorFilter(color);
                    tv_error_rencana_sub.setVisibility(View.GONE);
                    break;
                case R.id.ac_packet:
                    if (StaticMethods.isTextWidgetEmpty(ac_packet)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_packet.setColorFilter(color);
                    tv_error_packet.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }
    }

    @BindView(R.id.iv_circle_rencana)
    ImageView iv_circle_rencana;
    @BindView(R.id.iv_circle_rencana_sub)
    ImageView iv_circle_rencana_sub;
    @BindView(R.id.iv_circle_packet)
    ImageView iv_circle_packet;

    @BindView(R.id.tv_ubah_pp)
    TextView tv_ubah_pp;
    @BindView(R.id.tv_isi_pp)
    TextView tv_isi_pp;
    @BindView(R.id.iv_pp)
    ImageView iv_pp;
    @BindView(R.id.ll_pp)
    LinearLayout ll_pp;
    @BindView(R.id.tv_ubah_tt)
    TextView tv_ubah_tt;
    @BindView(R.id.tv_isi_tt)
    TextView tv_isi_tt;
    @BindView(R.id.iv_tt)
    ImageView iv_tt;
    @BindView(R.id.ll_tt)
    LinearLayout ll_tt;
    @BindView(R.id.tv_ubah_ag)
    TextView tv_ubah_ag;
    @BindView(R.id.tv_isi_ag)
    TextView tv_isi_ag;
    @BindView(R.id.ll_ag)
    LinearLayout ll_ag;
    @BindView(R.id.iv_ag)
    ImageView iv_ag;
    @BindView(R.id.ac_rencana)
    AutoCompleteTextView ac_rencana;
    @BindView(R.id.tv_error_rencana)
    TextView tv_error_rencana;
    @BindView(R.id.cl_rencana_sub)
    ConstraintLayout cl_rencana_sub;
    @BindView(R.id.ac_rencana_sub)
    AutoCompleteTextView ac_rencana_sub;
    @BindView(R.id.tv_error_rencana_sub)
    TextView tv_error_rencana_sub;
    @BindView(R.id.cb_same)
    CheckBox cb_same;
    @BindView(R.id.fl_same)
    FrameLayout fl_same;
    @BindView(R.id.cb_calon_bayi)
    CheckBox cb_calon_bayi;
    @BindView(R.id.fl_calon_bayi)
    FrameLayout fl_calon_bayi;
    @BindView(R.id.btn_lanjut)
    Button btn_lanjut;
    @BindView(R.id.btn_kembali)
    Button btn_kembali;
    @BindView(R.id.cl_pp)
    ConstraintLayout cl_pp;
    @BindView(R.id.cl_tt)
    ConstraintLayout cl_tt;
    @BindView(R.id.cl_rencana_layout)
    ConstraintLayout cl_rencana_layout;

    @BindView(R.id.scoll_data_proposal)
    ScrollView scoll_data_proposal;

    @BindView(R.id.cl_packet)
    ConstraintLayout cl_packet;
    @BindView(R.id.ac_packet)
    AutoCompleteTextView ac_packet;
    @BindView(R.id.tv_error_packet)
    TextView tv_error_packet;
}
