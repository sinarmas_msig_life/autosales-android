package id.co.ajsmsig.nb.crm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.form.SimbakAddressModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakLeadModel;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;

public class C_FormAlamatActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = C_FormAlamatActivity.class.getSimpleName();
    @BindView(R.id.et_alamat)
    EditText et_alamat;
    @BindView(R.id.et_kode_pos)
    EditText et_kode_pos;
    @BindView(R.id.et_kota)
    EditText et_kota;
    @BindView(R.id.et_telp1)
    EditText et_telp1;
    @BindView(R.id.et_telp2)
    EditText et_telp2;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_fax)
    EditText et_fax;
    @BindView(R.id.btn_ok)
    Button btn_ok;
    @BindView(R.id.btn_kembali)
    Button btn_kembali;

    @BindView(R.id.iv_circle_alamat)
    ImageView iv_circle_alamat;
    @BindView(R.id.iv_circle_kode_pos)
    ImageView iv_circle_kode_pos;
    @BindView(R.id.iv_circle_kota)
    ImageView iv_circle_kota;
    @BindView(R.id.iv_circle_telp1)
    ImageView iv_circle_telp1;
    @BindView(R.id.iv_circle_telp2)
    ImageView iv_circle_telp2;
    @BindView(R.id.iv_circle_fax)
    ImageView iv_circle_fax;
    @BindView(R.id.iv_circle_email)
    ImageView iv_circle_email;

    @BindView(R.id.tv_error_alamat)
    TextView tv_error_alamat;
    @BindView(R.id.tv_error_kode_pos)
    TextView tv_error_kode_pos;
    @BindView(R.id.tv_error_kota)
    TextView tv_error_kota;
    @BindView(R.id.tv_error_telp1)
    TextView tv_error_telp1;
    @BindView(R.id.tv_error_telp2)
    TextView tv_error_telp2;
    @BindView(R.id.tv_error_fax)
    TextView tv_error_fax;
    @BindView(R.id.tv_error_email)
    TextView tv_error_email;

    @BindView(R.id.cl_form_info_umum)
    ConstraintLayout cl_form_info_umum;

    @BindView(R.id.scroll_alamat)
    ScrollView scroll_alamat;

    @BindView(R.id.cl_child_alamat)
    ConstraintLayout cl_child_alamat;


    private SimbakAddressModel simbakAddressModel;
    private SimbakLeadModel simbakLeadModel;
    private int type = 0;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_activity_form_alamat);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        simbakLeadModel = intent.getParcelableExtra(getString(R.string.SIMBAK_LEAD));
        type = intent.getIntExtra(getString(R.string.type), 0);

        et_alamat.addTextChangedListener(new MTextWatcher(et_alamat));
        et_kode_pos.addTextChangedListener(new MTextWatcher(et_kode_pos));
        et_kota.addTextChangedListener(new MTextWatcher(et_kota));
        et_telp1.addTextChangedListener(new MTextWatcher(et_telp1));
        et_telp2.addTextChangedListener(new MTextWatcher(et_telp2));
        et_email.addTextChangedListener(new MTextWatcher(et_email));
        et_fax.addTextChangedListener(new MTextWatcher(et_fax));

        btn_ok.setOnClickListener(this);
        btn_kembali.setOnClickListener(this);
                setView();
//        restore();
    }



    private void setView() {
        if(simbakAddressModel.getSLA2_DETAIL()!= null)
            et_alamat.setText(simbakAddressModel.getSLA2_DETAIL());
        if(simbakAddressModel.getSLA2_POSTCODE()!=null)
            et_kode_pos.setText(simbakAddressModel.getSLA2_POSTCODE());
        if(simbakAddressModel.getSLA2_CITY()!=null)
            et_kota.setText(simbakAddressModel.getSLA2_CITY());
        if(simbakAddressModel.getSLA2_PHONE1()!=null)
            et_telp1.setText(simbakAddressModel.getSLA2_PHONE1());
        if(simbakAddressModel.getSLA2_PHONE2()!=null)
            et_telp1.setText(simbakAddressModel.getSLA2_PHONE2());
        if(simbakAddressModel.getSLA2_EMAIL()!=null)
            et_email.setText(simbakAddressModel.getSLA2_EMAIL());
        if(simbakAddressModel.getSLA2_FAX()!=null)
            et_fax.setText(simbakAddressModel.getSLA2_FAX());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_ok:
//                clickOk();
                load();
                if (Validation()) {
                    intent = new Intent();
                    intent.putExtra(getString(R.string.SIMBAK_LEAD), simbakLeadModel);
                    intent.putExtra(getString(R.string.type), type);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
            case R.id.btn_kembali:
                et_alamat.setText("");
                et_kode_pos.setText("");
                et_kota.setText("");
                et_telp1.setText("");
                et_telp2.setText("");
                et_email.setText("");
                et_fax.setText("");
                load();
                intent = new Intent();
                intent.putExtra(getString(R.string.SIMBAK_LEAD), simbakLeadModel);
                intent.putExtra(getString(R.string.type), type);
                setResult(RESULT_OK, intent);
                finish();
        }
    }

    private void load(){
        switch (type) {
            //Alamat Rumah
            case 1: //rumah
                simbakLeadModel.getHome().setSLA2_DETAIL(et_alamat.getText().toString());
                simbakLeadModel.getHome().setSLA2_CITY(et_kota.getText().toString());
                simbakLeadModel.getHome().setSLA2_POSTCODE(et_kode_pos.getText().toString());
                simbakLeadModel.getHome().setSLA2_PHONE1(et_telp1.getText().toString());
                simbakLeadModel.getHome().setSLA2_PHONE2(et_telp1.getText().toString());
                simbakLeadModel.getHome().setSLA2_EMAIL(et_email.getText().toString());
                simbakLeadModel.getHome().setSLA2_FAX(et_fax.getText().toString());
                break;
            case 2: //kantor
                simbakLeadModel.getHome().setSLA2_DETAIL(et_alamat.getText().toString());
                simbakLeadModel.getHome().setSLA2_CITY(et_kota.getText().toString());
                simbakLeadModel.getHome().setSLA2_POSTCODE(et_kode_pos.getText().toString());
                simbakLeadModel.getHome().setSLA2_PHONE1(et_telp1.getText().toString());
                simbakLeadModel.getHome().setSLA2_PHONE2(et_telp1.getText().toString());
                simbakLeadModel.getHome().setSLA2_EMAIL(et_email.getText().toString());
                simbakLeadModel.getHome().setSLA2_FAX(et_fax.getText().toString());
                break;
            case 3: //alamat lain
                simbakLeadModel.getHome().setSLA2_DETAIL(et_alamat.getText().toString());
                simbakLeadModel.getHome().setSLA2_CITY(et_kota.getText().toString());
                simbakLeadModel.getHome().setSLA2_POSTCODE(et_kode_pos.getText().toString());
                simbakLeadModel.getHome().setSLA2_PHONE1(et_telp1.getText().toString());
                simbakLeadModel.getHome().setSLA2_PHONE2(et_telp1.getText().toString());
                simbakLeadModel.getHome().setSLA2_EMAIL(et_email.getText().toString());
                simbakLeadModel.getHome().setSLA2_FAX(et_fax.getText().toString());
                break;
        }
    }

    private void restore(){
        switch (type) {
            case 1: //rumah
                et_alamat.setText(simbakLeadModel.getHome().getSLA2_DETAIL());
                et_kota.setText(simbakLeadModel.getHome().getSLA2_CITY());
                et_kode_pos.setText(simbakLeadModel.getHome().getSLA2_POSTCODE());
                et_telp1.setText(simbakLeadModel.getHome().getSLA2_PHONE1());
                et_telp2.setText(simbakLeadModel.getHome().getSLA2_PHONE2());
                et_email.setText(simbakLeadModel.getHome().getSLA2_EMAIL());
                et_fax.setText(simbakLeadModel.getHome().getSLA2_FAX());
                if (!simbakLeadModel.getHome().getSLA2_DETAIL().equals("")) {
                    btn_kembali.setText("HAPUS");
                } else {
                    btn_kembali.setText("BATAL");
                }
                break;
            case 2: //rumah
                et_alamat.setText(simbakLeadModel.getHome().getSLA2_DETAIL());
                et_kota.setText(simbakLeadModel.getHome().getSLA2_CITY());
                et_kode_pos.setText(simbakLeadModel.getHome().getSLA2_POSTCODE());
                et_telp1.setText(simbakLeadModel.getHome().getSLA2_PHONE1());
                et_telp2.setText(simbakLeadModel.getHome().getSLA2_PHONE2());
                et_email.setText(simbakLeadModel.getHome().getSLA2_EMAIL());
                et_fax.setText(simbakLeadModel.getHome().getSLA2_FAX());
                if (!simbakLeadModel.getHome().getSLA2_DETAIL().equals("")) {
                    btn_kembali.setText("HAPUS");
                } else {
                    btn_kembali.setText("BATAL");
                }
                break;
            case 3: //rumah
                et_alamat.setText(simbakLeadModel.getHome().getSLA2_DETAIL());
                et_kota.setText(simbakLeadModel.getHome().getSLA2_CITY());
                et_kode_pos.setText(simbakLeadModel.getHome().getSLA2_POSTCODE());
                et_telp1.setText(simbakLeadModel.getHome().getSLA2_PHONE1());
                et_telp2.setText(simbakLeadModel.getHome().getSLA2_PHONE2());
                et_email.setText(simbakLeadModel.getHome().getSLA2_EMAIL());
                et_fax.setText(simbakLeadModel.getHome().getSLA2_FAX());
                if (!simbakLeadModel.getHome().getSLA2_DETAIL().equals("")) {
                    btn_kembali.setText("HAPUS");
                } else {
                    btn_kembali.setText("BATAL");
                }
                break;
        }
    }

    private boolean Validation() {
        boolean val = true;

        int color = ContextCompat.getColor(this, R.color.red);
        if (cl_form_info_umum.getVisibility() == View.VISIBLE) {

            if (cl_form_info_umum.getVisibility() == View.VISIBLE) {
                if (et_alamat.getText().toString().trim().length() == 0) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_alamat, tv_error_alamat, iv_circle_alamat, color, this, getString(R.string.error_field_required));
//                tv_error_alamat.setText(R.string.field_requirement);
//                tv_error_alamat.setVisibility(View.VISIBLE);
//                iv_circle_alamat.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
                } else {
                    if (!Method_Validator.ValEditRegex(et_alamat.getText().toString())) {
                        val = false;
                        MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_alamat, tv_error_alamat, iv_circle_alamat, color, this, getString(R.string.error_regex));
                    }
                }
                if (et_kota.getText().toString().trim().length() == 0) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_kota, tv_error_kota, iv_circle_kota, color, this, getString(R.string.error_field_required));
//                tv_error_kota.setText(R.string.field_requirement);
//                tv_error_kota.setVisibility(View.VISIBLE);
//                iv_circle_kota.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
                } else {
                    if (!Method_Validator.ValEditRegex(et_kota.getText().toString())) {
                        val = false;
                        MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_kota, tv_error_kota, iv_circle_kota, color, this, getString(R.string.error_regex));
                    }
                }
            }
            if (et_email.getText().toString().trim().length() != 0) {
                if (Method_Validator.emailValidator(et_email.getText().toString()) == false) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_email, tv_error_email, iv_circle_email, color, this, getString(R.string.error_field_required));
//                    tv_error_email.setText(R.string.false_format);
//                    tv_error_email.setVisibility(View.VISIBLE);
//                    iv_circle_email.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
                }
            }
        }
        return val;
    }

    private void clickOk() {
        boolean isSave = true;

        if(et_alamat.getText().toString().trim().length()==0){
            isSave = false;
            tv_error_alamat.setText(R.string.field_requirement);
            tv_error_alamat.setVisibility(View.VISIBLE);
            iv_circle_alamat.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
        }

//        if(isSave){
//            if(et_alamat.getText().toString().trim().length()!=0)
//                simbakAddressModel.setSLA2_DETAIL(et_alamat.getText().toString());
//            if(et_kode_pos.getText().toString().trim().length()!=0)
//                simbakAddressModel.setSLA2_POSTCODE(et_kode_pos.getText().toString());
//            if(et_kota.getText().toString().trim().length()!=0)
//                simbakAddressModel.setSLA2_CITY(et_kota.getText().toString());
//            if(et_telp1.getText().toString().trim().length()!=0)
//                simbakAddressModel.setSLA2_PHONE1(et_telp1.getText().toString());
//            if(et_telp2.getText().toString().trim().length()!=0)
//                simbakAddressModel.setSLA2_PHONE2(et_telp2.getText().toString());
//            if(et_email.getText().toString().trim().length()!=0)
//                simbakAddressModel.setSLA2_EMAIL(et_email.getText().toString());
//            if(et_fax.getText().toString().trim().length()!=0)
//                simbakAddressModel.setSLA2_FAX(et_fax.getText().toString());


            Intent intent = new Intent();
            intent.putExtra(getString(R.string.SIMBAK_LEAD), simbakLeadModel);
            setResult(RESULT_OK, intent);
            finish();
//        }
    }


    private class MTextWatcher implements TextWatcher {
        private View view;

        MTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                case R.id.et_alamat:
                    if (StaticMethods.isTextWidgetEmpty(et_alamat)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_alamat.setColorFilter(color);
                    tv_error_alamat.setVisibility(View.GONE);
                    break;
                case R.id.et_kode_pos:
                    if (StaticMethods.isTextWidgetEmpty(et_kode_pos)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_kode_pos.setColorFilter(color);
                    tv_error_kode_pos.setVisibility(View.GONE);
                    break;
                case R.id.et_kota:
                    if (StaticMethods.isTextWidgetEmpty(et_kota)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_kota.setColorFilter(color);
                    tv_error_kota.setVisibility(View.GONE);
                    break;
                case R.id.et_telp1:
                    if (StaticMethods.isTextWidgetEmpty(et_telp1)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_telp1.setColorFilter(color);
                    tv_error_telp1.setVisibility(View.GONE);
                    break;
                case R.id.et_telp2:
                    if (StaticMethods.isTextWidgetEmpty(et_telp2)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_telp2.setColorFilter(color);
                    tv_error_telp2.setVisibility(View.GONE);
                    break;
                case R.id.et_email:
                    if (StaticMethods.isTextWidgetEmpty(et_email)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_email.setColorFilter(color);
                    tv_error_email.setVisibility(View.GONE);
                    break;
                case R.id.et_fax:
                    if (StaticMethods.isTextWidgetEmpty(et_fax)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_fax.setColorFilter(color);
                    tv_error_fax.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }
    }
}
