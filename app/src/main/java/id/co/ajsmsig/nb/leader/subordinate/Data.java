
package id.co.ajsmsig.nb.leader.subordinate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("NAMA_REFF")
    @Expose
    private String nAMAREFF;
    @SerializedName("NM_BANK")
    @Expose
    private String nMBANK;
    @SerializedName("JENIS")
    @Expose
    private String jENIS;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param jENIS
     * @param nAMAREFF
     * @param nMBANK
     */
    public Data(String nAMAREFF, String nMBANK, String jENIS) {
        super();
        this.nAMAREFF = nAMAREFF;
        this.nMBANK = nMBANK;
        this.jENIS = jENIS;
    }

    public String getNAMAREFF() {
        return nAMAREFF;
    }

    public void setNAMAREFF(String nAMAREFF) {
        this.nAMAREFF = nAMAREFF;
    }

    public String getNMBANK() {
        return nMBANK;
    }

    public void setNMBANK(String nMBANK) {
        this.nMBANK = nMBANK;
    }

    public String getJENIS() {
        return jENIS;
    }

    public void setJENIS(String jENIS) {
        this.jENIS = jENIS;
    }

}
