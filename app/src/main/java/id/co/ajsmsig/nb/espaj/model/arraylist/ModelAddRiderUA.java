package id.co.ajsmsig.nb.espaj.model.arraylist;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 30/08/2017.
 */

public class ModelAddRiderUA implements Parcelable {
    private int counter = 0;
    private int kode_produk_rider = 0;
    private int kode_subproduk_rider = 0;
    private int tertanggung_rider = 0;
    private double rate_rider = (double) 0;
    private int unit_rider = 0;
    private int klas_rider = 0;
    private int persentase_rider = 0;
    private double up_rider = (double) 0;
    private double premi_rider = (double) 0;
    private String tglmulai_rider = "";
    private String tglakhir_rider = "";
    private int masa_rider = 0;
    private String akhirbayar_rider = "";
    private String peserta_askes = "";
    private int jekel_askes = 0;
    private String ttl_askes = "";
    private int usia_askes = 0;
    private int hubungan_askes = 0;
    private int produk_askes = 0;
    private int rider_askes = 0;
    private int tinggi_askes = 0;
    private int berat_askes = 0;
    private int warganegara_askes = 0;
    private String pekerjaan_askes = "";
    private Boolean val_rider = true;

    public ModelAddRiderUA() {
    }

    protected ModelAddRiderUA(Parcel in) {
        counter = in.readInt();
        kode_produk_rider = in.readInt();
        kode_subproduk_rider = in.readInt();
        tertanggung_rider = in.readInt();
        rate_rider = in.readDouble();
        unit_rider = in.readInt();
        klas_rider = in.readInt();
        persentase_rider = in.readInt();
        up_rider = in.readDouble();
        premi_rider = in.readDouble();
        tglmulai_rider = in.readString();
        tglakhir_rider = in.readString();
        masa_rider = in.readInt();
        akhirbayar_rider = in.readString();
        peserta_askes = in.readString();
        jekel_askes = in.readInt();
        ttl_askes = in.readString();
        usia_askes = in.readInt();
        hubungan_askes = in.readInt();
        produk_askes = in.readInt();
        rider_askes = in.readInt();
        tinggi_askes = in.readInt();
        berat_askes = in.readInt();
        warganegara_askes = in.readInt();
        pekerjaan_askes = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(counter);
        dest.writeInt(kode_produk_rider);
        dest.writeInt(kode_subproduk_rider);
        dest.writeInt(tertanggung_rider);
        dest.writeDouble(rate_rider);
        dest.writeInt(unit_rider);
        dest.writeInt(klas_rider);
        dest.writeInt(persentase_rider);
        dest.writeDouble(up_rider);
        dest.writeDouble(premi_rider);
        dest.writeString(tglmulai_rider);
        dest.writeString(tglakhir_rider);
        dest.writeInt(masa_rider);
        dest.writeString(akhirbayar_rider);
        dest.writeString(peserta_askes);
        dest.writeInt(jekel_askes);
        dest.writeString(ttl_askes);
        dest.writeInt(usia_askes);
        dest.writeInt(hubungan_askes);
        dest.writeInt(produk_askes);
        dest.writeInt(rider_askes);
        dest.writeInt(tinggi_askes);
        dest.writeInt(berat_askes);
        dest.writeInt(warganegara_askes);
        dest.writeString(pekerjaan_askes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelAddRiderUA> CREATOR = new Creator<ModelAddRiderUA>() {
        @Override
        public ModelAddRiderUA createFromParcel(Parcel in) {
            return new ModelAddRiderUA(in);
        }

        @Override
        public ModelAddRiderUA[] newArray(int size) {
            return new ModelAddRiderUA[size];
        }
    };

    public ModelAddRiderUA(int counter, int kode_produk_rider, int kode_subproduk_rider, int tertanggung_rider, double rate_rider, int unit_rider, int klas_rider, int persentase_rider, double up_rider, double premi_rider, String tglmulai_rider, String tglakhir_rider, int masa_rider, String akhirbayar_rider, String peserta_askes, int jekel_askes, String ttl_askes, int usia_askes, int hubungan_askes, int produk_askes, int rider_askes, int tinggi_askes, int berat_askes, int warganegara_askes, String pekerjaan_askes, Boolean val_rider) {
        this.counter = counter;
        this.kode_produk_rider = kode_produk_rider;
        this.kode_subproduk_rider = kode_subproduk_rider;
        this.tertanggung_rider = tertanggung_rider;
        this.rate_rider = rate_rider;
        this.unit_rider = unit_rider;
        this.klas_rider = klas_rider;
        this.persentase_rider = persentase_rider;
        this.up_rider = up_rider;
        this.premi_rider = premi_rider;
        this.tglmulai_rider = tglmulai_rider;
        this.tglakhir_rider = tglakhir_rider;
        this.masa_rider = masa_rider;
        this.akhirbayar_rider = akhirbayar_rider;
        this.peserta_askes = peserta_askes;
        this.jekel_askes = jekel_askes;
        this.ttl_askes = ttl_askes;
        this.usia_askes = usia_askes;
        this.hubungan_askes = hubungan_askes;
        this.produk_askes = produk_askes;
        this.rider_askes = rider_askes;
        this.tinggi_askes = tinggi_askes;
        this.berat_askes = berat_askes;
        this.warganegara_askes = warganegara_askes;
        this.pekerjaan_askes = pekerjaan_askes;
        this.val_rider = val_rider;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public int getKode_produk_rider() {
        return kode_produk_rider;
    }

    public void setKode_produk_rider(int kode_produk_rider) {
        this.kode_produk_rider = kode_produk_rider;
    }

    public int getKode_subproduk_rider() {
        return kode_subproduk_rider;
    }

    public void setKode_subproduk_rider(int kode_subproduk_rider) {
        this.kode_subproduk_rider = kode_subproduk_rider;
    }

    public int getTertanggung_rider() {
        return tertanggung_rider;
    }

    public void setTertanggung_rider(int tertanggung_rider) {
        this.tertanggung_rider = tertanggung_rider;
    }

    public double getRate_rider() {
        return rate_rider;
    }

    public void setRate_rider(double rate_rider) {
        this.rate_rider = rate_rider;
    }

    public int getUnit_rider() {
        return unit_rider;
    }

    public void setUnit_rider(int unit_rider) {
        this.unit_rider = unit_rider;
    }

    public int getKlas_rider() {
        return klas_rider;
    }

    public void setKlas_rider(int klas_rider) {
        this.klas_rider = klas_rider;
    }

    public int getPersentase_rider() {
        return persentase_rider;
    }

    public void setPersentase_rider(int persentase_rider) {
        this.persentase_rider = persentase_rider;
    }

    public double getUp_rider() {
        return up_rider;
    }

    public void setUp_rider(double up_rider) {
        this.up_rider = up_rider;
    }

    public double getPremi_rider() {
        return premi_rider;
    }

    public void setPremi_rider(double premi_rider) {
        this.premi_rider = premi_rider;
    }

    public String getTglmulai_rider() {
        return tglmulai_rider;
    }

    public void setTglmulai_rider(String tglmulai_rider) {
        this.tglmulai_rider = tglmulai_rider;
    }

    public String getTglakhir_rider() {
        return tglakhir_rider;
    }

    public void setTglakhir_rider(String tglakhir_rider) {
        this.tglakhir_rider = tglakhir_rider;
    }

    public int getMasa_rider() {
        return masa_rider;
    }

    public void setMasa_rider(int masa_rider) {
        this.masa_rider = masa_rider;
    }

    public String getAkhirbayar_rider() {
        return akhirbayar_rider;
    }

    public void setAkhirbayar_rider(String akhirbayar_rider) {
        this.akhirbayar_rider = akhirbayar_rider;
    }

    public String getPeserta_askes() {
        return peserta_askes;
    }

    public void setPeserta_askes(String peserta_askes) {
        this.peserta_askes = peserta_askes;
    }

    public int getJekel_askes() {
        return jekel_askes;
    }

    public void setJekel_askes(int jekel_askes) {
        this.jekel_askes = jekel_askes;
    }

    public String getTtl_askes() {
        return ttl_askes;
    }

    public void setTtl_askes(String ttl_askes) {
        this.ttl_askes = ttl_askes;
    }

    public int getUsia_askes() {
        return usia_askes;
    }

    public void setUsia_askes(int usia_askes) {
        this.usia_askes = usia_askes;
    }

    public int getHubungan_askes() {
        return hubungan_askes;
    }

    public void setHubungan_askes(int hubungan_askes) {
        this.hubungan_askes = hubungan_askes;
    }

    public int getProduk_askes() {
        return produk_askes;
    }

    public void setProduk_askes(int produk_askes) {
        this.produk_askes = produk_askes;
    }

    public int getRider_askes() {
        return rider_askes;
    }

    public void setRider_askes(int rider_askes) {
        this.rider_askes = rider_askes;
    }

    public int getTinggi_askes() {
        return tinggi_askes;
    }

    public void setTinggi_askes(int tinggi_askes) {
        this.tinggi_askes = tinggi_askes;
    }

    public int getBerat_askes() {
        return berat_askes;
    }

    public void setBerat_askes(int berat_askes) {
        this.berat_askes = berat_askes;
    }

    public int getWarganegara_askes() {
        return warganegara_askes;
    }

    public void setWarganegara_askes(int warganegara_askes) {
        this.warganegara_askes = warganegara_askes;
    }

    public String getPekerjaan_askes() {
        return pekerjaan_askes;
    }

    public void setPekerjaan_askes(String pekerjaan_askes) {
        this.pekerjaan_askes = pekerjaan_askes;
    }

    public Boolean getVal_rider() {
        return val_rider;
    }

    public void setVal_rider(Boolean val_rider) {
        this.val_rider = val_rider;
    }

    public static Creator<ModelAddRiderUA> getCREATOR() {
        return CREATOR;
    }
}