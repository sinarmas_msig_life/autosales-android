package id.co.ajsmsig.nb.crm.fragment.listlead;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Fajar.Azhar on 21/03/2018.
 */

public class ChildLeadList implements Parcelable {

    private long slTabId;
    private long slId;
    private String slName;
    private String slNameFirstNameInitial;
    private String slNameLastNameInitial;
    private String slCreatedDate;
    private int slUmur;
    private int slLastPos;
    private boolean isFavoriteLead;
    private String slCurrentBranch;

    public ChildLeadList(long slTabId, long slId,String slCreatedDate, String slName, String slNameFirstNameInitial, String slNameLastNameInitial, int slUmur, int slLastPos, boolean isFavoriteLead, String slCurrentBranch) {
        this.slTabId = slTabId;
        this.slId = slId;
        this.slCreatedDate = slCreatedDate;
        this.slLastPos = slLastPos;
        this.slName = slName;
        this.slNameFirstNameInitial = slNameFirstNameInitial;
        this.slNameLastNameInitial = slNameLastNameInitial;
        this.slUmur = slUmur;
        this.slLastPos = slLastPos;
        this.isFavoriteLead = isFavoriteLead;
        this.slCurrentBranch = slCurrentBranch;
    }


    public String getSlNameFirstNameInitial() {
        return slNameFirstNameInitial;
    }

    public String getSlNameLastNameInitial() {
        return slNameLastNameInitial;
    }
    public ChildLeadList(Parcel in) {
        slName = in.readString();
    }

    public String getSlCreatedDate() {
        return slCreatedDate;
    }



    public int getSlLastPos() {
        return slLastPos;
    }


    public String getslCurrentBranch() {
        return slCurrentBranch;
    }

    public void setslCurrentBranch(String slCurrentBranch) {
        this.slCurrentBranch = slCurrentBranch;
    }

    public boolean isFavoriteLead() {
        return isFavoriteLead;
    }

    public void setFavoriteLead(boolean favoriteLead) {
        isFavoriteLead = favoriteLead;
    }

    public long getSlTabId() {
        return slTabId;
    }

    public long getSlId() {
        return slId;
    }

    public String getSlName() {
        return slName;
    }

    public void setSlName(String slName) {
        this.slName = slName;
    }

    public int getslUmur() {
        return slUmur;
    }

    public void setslUmur(int slUmur) {
        this.slUmur = slUmur;
    }


    public String getTitle() {
        return slName;
    }

    public void setTitle(String Title) {
        this.slName = Title;
    }


    public static final Creator<ChildLeadList> CREATOR = new Creator<ChildLeadList>() {
        @Override
        public ChildLeadList createFromParcel(Parcel in) {
            return null;
        }

        @Override
        public ChildLeadList[] newArray(int size) {
            return new ChildLeadList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(slName);
    }
}
