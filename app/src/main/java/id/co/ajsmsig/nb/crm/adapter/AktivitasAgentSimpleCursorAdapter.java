package id.co.ajsmsig.nb.crm.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;

/*
 Created by faiz_f on 08/06/2017.
 */

public class AktivitasAgentSimpleCursorAdapter extends SimpleCursorAdapter {
    public AktivitasAgentSimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        super.bindView(view, context, cursor);
        TextView tv_tanggal_aktivitas = view.findViewById(R.id.tv_tanggal_aktivitas);
        Calendar calendar = StaticMethods.toCalendarTime(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SLA_SDATE))));
        String tanggal = StaticMethods.toStringDate(calendar,4);
        String value = tanggal + " * " + cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.SL_NAME)));
        tv_tanggal_aktivitas.setText(value);
    }
}
