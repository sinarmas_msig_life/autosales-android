package id.co.ajsmsig.nb.pointofsale.utils

import id.co.ajsmsig.nb.pointofsale.custom.indicatorseekbar.ValueFormatter

/**
 * Created by andreyyoshuamanik on 02/03/18.
 */
class SeekbarValueFormatter (var range: Long = 1): ValueFormatter {

    override fun format(progress: Float): String {
        if (progress >= 1000000000) {
//            val i = progress.toLong() / 10000000 * 10000000
//            val progress = i.toFloat()
//            Log.i("INFO", "$progress")
            var digits = 3
            if (progress % 1000000000 == 0F) {
                digits = 0
            } else if (progress % 100000000 == 0F) {
                digits = 1
            } else if (progress % 10000000 == 0F) {
                digits = 2
            }
            if (range >= 1000000000) {
                digits = 0
            }
            return "${(progress / 1000000000).format(digits)} Milyar"
        } else if (progress >= 1000000) {
            return "${(progress / 1000000).format(0)} Juta"
        } else {
            return super.format(progress)
        }
    }
}