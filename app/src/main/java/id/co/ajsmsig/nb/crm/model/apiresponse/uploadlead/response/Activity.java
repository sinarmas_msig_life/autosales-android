
package id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Activity {

    @SerializedName("state")
    @Expose
    private Boolean state;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private Data__ data;
    @SerializedName("sla_tab_id")
    @Expose
    private String slaTabId;

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data__ getData() {
        return data;
    }

    public void setData(Data__ data) {
        this.data = data;
    }

    public String getSlaTabId() {
        return slaTabId;
    }

    public void setSlaTabId(String slaTabId) {
        this.slaTabId = slaTabId;
    }

}
