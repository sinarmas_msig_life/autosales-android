package id.co.ajsmsig.nb.crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.BcBranch;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.DownloadLeadResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.Home;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.LeadActivityList;
import id.co.ajsmsig.nb.crm.model.form.SimbakAccountModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakActivityModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakAddressModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakLeadModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakLinkAccToLeadModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakLinkUsrToLeadModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakProductModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakSpajModel;
import id.co.ajsmsig.nb.crm.model.hardcode.CrmTableCode;

/**
 * Created by faiz_f on 17/03/2017.
 */

public class C_Insert {
    private Context context;
    private DBHelper dbHelper;
    private final String TAG = getClass().getSimpleName();

    private final String TABLE_NAME_SIMBAK_LIST_ACTIVITY = "SIMBAK_LIST_ACTIVITY";
    private final String TABLE_NAME_SIMBAK_LEAD = "SIMBAK_LEAD";
    private final String TABLE_NAME_SIMBAK_AGENT_BRANCH = "SIMBAK_AGENT_BRANCH";
    private final String TABLE_SIMBAK_LINK_USR_TO_LEAD = "SIMBAK_LINK_USR_TO_LEAD";
    private final String TABLE_NAME_SIMBAK_LIST_ADDRESS = "SIMBAK_LIST_ADDRESS";
    private final String TABLE_NAME_SIMBAK_LINK_USR_TO_LEAD = "SIMBAK_LINK_USR_TO_LEAD";
    private final String TABLE_NAME_LST_USER_MENU = "LST_USER_MENU";
    private final String TABLE_NAME_SLS_SPAJ_TEMP = "SIMBAK_LIST_SPAJ";
    private final String TABLE_NAME_LST_AGENCY_LEAD = "LST_AGENCY_LEAD";

    private final String LRB_ID_COLUMN = "lrb_id";
    private final String MSAG_ID_COLUMN = "msag_id";
    private final String AGENT_NAME_COLUMN = "agent_name";
    private final String AJS_CABANG_COLUMN = "ajs_cbng";
    private final String SIMAS_CABANG_COLUMN = "simas_cbng";
    private final String CABANG_COLUMN = "cabang";
    private final String WILAYAH_COLUMN = "wilayah";


    public C_Insert(Context context) {
        this.context = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.update_data_preferences), Context.MODE_PRIVATE);
        int version = sharedPreferences.getInt(context.getString(R.string.lav_data_id), 0);
        this.dbHelper = new DBHelper(context, version);
    }


    public long insertToSimbakLead(SimbakLeadModel simbakLeadModel) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        if (simbakLeadModel.getSL_TAB_ID() != null)
            contentValues.put(context.getString(R.string.SL_TAB_ID), simbakLeadModel.getSL_TAB_ID());
        if (simbakLeadModel.getSL_ID() != null)
            contentValues.put(context.getString(R.string.SL_ID), simbakLeadModel.getSL_ID());
        if (simbakLeadModel.getSL_NAME() != null)
            contentValues.put(context.getString(R.string.SL_NAME), simbakLeadModel.getSL_NAME());
        if (simbakLeadModel.getSL_CREATED() != null)
            contentValues.put(context.getString(R.string.SL_CREATED), simbakLeadModel.getSL_CREATED());
        if (simbakLeadModel.getSL_CRTD_DATE() != null)
            contentValues.put(context.getString(R.string.SL_CRTD_DATE), simbakLeadModel.getSL_CRTD_DATE());
        if (simbakLeadModel.getSL_UPDATED() != null)
            contentValues.put(context.getString(R.string.SL_UPDATED), simbakLeadModel.getSL_UPDATED());
        if (simbakLeadModel.getSL_UPTD_DATE() != null)
            contentValues.put(context.getString(R.string.SL_UPTD_DATE), simbakLeadModel.getSL_UPTD_DATE());
        if (simbakLeadModel.getSL_RATE() != null)
            contentValues.put(context.getString(R.string.SL_RATE), simbakLeadModel.getSL_RATE());
        if (simbakLeadModel.getSL_REMARK() != null)
            contentValues.put(context.getString(R.string.SL_REMARK), simbakLeadModel.getSL_REMARK());
        if (simbakLeadModel.getSL_INIT() != null)
            contentValues.put(context.getString(R.string.SL_INIT), simbakLeadModel.getSL_INIT());
        if (simbakLeadModel.getSL_MOTHER() != null)
            contentValues.put(context.getString(R.string.SL_MOTHER), simbakLeadModel.getSL_MOTHER());
        if (simbakLeadModel.getSL_CITIZEN() != null)
            contentValues.put(context.getString(R.string.SL_CITIZEN), simbakLeadModel.getSL_CITIZEN());
        if (simbakLeadModel.getSL_BPLACE() != null)
            contentValues.put(context.getString(R.string.SL_BPLACE), simbakLeadModel.getSL_BPLACE());
        if (simbakLeadModel.getSL_UMUR() != null)
            contentValues.put(context.getString(R.string.SL_UMUR), simbakLeadModel.getSL_UMUR());
        if (simbakLeadModel.getSL_IDNO() != null)
            contentValues.put(context.getString(R.string.SL_IDNO), simbakLeadModel.getSL_IDNO());
        if (simbakLeadModel.getSL_BDATE() != null)
            contentValues.put(context.getString(R.string.SL_BDATE), simbakLeadModel.getSL_BDATE());
        if (simbakLeadModel.getSL_RES_PREMIUM() != null)
            contentValues.put(context.getString(R.string.SL_RES_PREMIUM), simbakLeadModel.getSL_RES_PREMIUM());
        if (simbakLeadModel.getSL_BAC_REFF() != null)
            contentValues.put(context.getString(R.string.SL_BAC_REFF), simbakLeadModel.getSL_BAC_REFF());
        if (simbakLeadModel.getSL_BAC_REFF_NAME() != null)
            contentValues.put(context.getString(R.string.SL_BAC_REFF_NAME), simbakLeadModel.getSL_BAC_REFF_NAME());
        if (simbakLeadModel.getSL_PEND_ID() != null)
            contentValues.put(context.getString(R.string.SL_PEND_ID), simbakLeadModel.getSL_PEND_ID());
        if (simbakLeadModel.getSL_APPROVE() != null)
            contentValues.put(context.getString(R.string.SL_APPROVE), simbakLeadModel.getSL_APPROVE());
        if (simbakLeadModel.getSL_REQUEST() != null)
            contentValues.put(context.getString(R.string.SL_REQUEST), simbakLeadModel.getSL_REQUEST());
        if (simbakLeadModel.getSL_ALASAN() != null)
            contentValues.put(context.getString(R.string.SL_ALASAN), simbakLeadModel.getSL_ALASAN());
        if (simbakLeadModel.getSL_NO_NASABAH() != null)
            contentValues.put(context.getString(R.string.SL_NO_NASABAH), simbakLeadModel.getSL_NO_NASABAH());
        if (simbakLeadModel.getSL_APPROVER() != null)
            contentValues.put(context.getString(R.string.SL_APPROVER), simbakLeadModel.getSL_APPROVER());
        if (simbakLeadModel.getSL_BAC_CURR_BRANCH() != null)
            contentValues.put(context.getString(R.string.SL_BAC_CURR_BRANCH), simbakLeadModel.getSL_BAC_CURR_BRANCH());
        if (simbakLeadModel.getSL_ROOMOUT() != null)
            contentValues.put(context.getString(R.string.SL_ROOMOUT), simbakLeadModel.getSL_ROOMOUT());
        if (simbakLeadModel.getSL_ROOMIN() != null)
            contentValues.put(context.getString(R.string.SL_ROOMIN), simbakLeadModel.getSL_ROOMIN());
        if (simbakLeadModel.getSL_OCCUPATION() != null)
            contentValues.put(context.getString(R.string.SL_OCCUPATION), simbakLeadModel.getSL_OCCUPATION());
        if (simbakLeadModel.getSL_IDEXP() != null)
            contentValues.put(context.getString(R.string.SL_IDEXP), simbakLeadModel.getSL_IDEXP());
        if (simbakLeadModel.getSL_NPWP() != null)
            contentValues.put(context.getString(R.string.SL_NPWP), simbakLeadModel.getSL_NPWP());
        if (simbakLeadModel.getSL_OWNERID() != null)
            contentValues.put(context.getString(R.string.SL_OWNERID), simbakLeadModel.getSL_OWNERID());
        if (simbakLeadModel.getSL_CIF() != null)
            contentValues.put(context.getString(R.string.SL_CIF), simbakLeadModel.getSL_CIF());
        if (simbakLeadModel.getSL_PINBB() != null)
            contentValues.put(context.getString(R.string.SL_PINBB), simbakLeadModel.getSL_PINBB());
        if (simbakLeadModel.getSL_OLD_ID() != null)
            contentValues.put(context.getString(R.string.SL_OLD_ID), simbakLeadModel.getSL_OLD_ID());
        if (simbakLeadModel.getSL_APP() != null)
            contentValues.put(context.getString(R.string.SL_APP), simbakLeadModel.getSL_APP());
        if (simbakLeadModel.getSL_ACTIVE() != null)
            contentValues.put(context.getString(R.string.SL_ACTIVE), simbakLeadModel.getSL_ACTIVE());
        if (simbakLeadModel.getSL_SRC() != null)
            contentValues.put(context.getString(R.string.SL_SRC), simbakLeadModel.getSL_SRC());
        if (simbakLeadModel.getSL_CHAR() != null)
            contentValues.put(context.getString(R.string.SL_CHAR), simbakLeadModel.getSL_CHAR());
        if (simbakLeadModel.getSL_STATE() != null)
            contentValues.put(context.getString(R.string.SL_STATE), simbakLeadModel.getSL_STATE());
        if (simbakLeadModel.getSL_CORP_POS() != null)
            contentValues.put(context.getString(R.string.SL_CORP_POS), simbakLeadModel.getSL_CORP_POS());
        if (simbakLeadModel.getSL_GENDER() != null)
            contentValues.put(context.getString(R.string.SL_GENDER), simbakLeadModel.getSL_GENDER());
        if (simbakLeadModel.getSL_RELIGION() != null)
            contentValues.put(context.getString(R.string.SL_RELIGION), simbakLeadModel.getSL_RELIGION());
        if (simbakLeadModel.getSL_PERSONAL_REL() != null)
            contentValues.put(context.getString(R.string.SL_PERSONAL_REL), simbakLeadModel.getSL_PERSONAL_REL());
        if (simbakLeadModel.getSL_LAST_EDU() != null)
            contentValues.put(context.getString(R.string.SL_LAST_EDU), simbakLeadModel.getSL_LAST_EDU());
        if (simbakLeadModel.getSL_POLIS_PURPOSE() != null)
            contentValues.put(context.getString(R.string.SL_POLIS_PURPOSE), simbakLeadModel.getSL_POLIS_PURPOSE());
        if (simbakLeadModel.getSL_PAYMENT_SOURCE() != null)
            contentValues.put(context.getString(R.string.SL_PAYMENT_SOURCE), simbakLeadModel.getSL_PAYMENT_SOURCE());
        if (simbakLeadModel.getSL_OTHER_TYPE() != null)
            contentValues.put(context.getString(R.string.SL_OTHER_TYPE), simbakLeadModel.getSL_OTHER_TYPE());
        if (simbakLeadModel.getSL_CSTMR_FUND_TYPE() != null)
            contentValues.put(context.getString(R.string.SL_CSTMR_FUND_TYPE), simbakLeadModel.getSL_CSTMR_FUND_TYPE());
        if (simbakLeadModel.getSL_IDTYPE() != null)
            contentValues.put(context.getString(R.string.SL_IDTYPE), simbakLeadModel.getSL_IDTYPE());
        if (simbakLeadModel.getSL_SALARY() != null)
            contentValues.put(context.getString(R.string.SL_SALARY), simbakLeadModel.getSL_SALARY());
        if (simbakLeadModel.getSL_BUYER_FUND_TYPE() != null)
            contentValues.put(context.getString(R.string.SL_BUYER_FUND_TYPE), simbakLeadModel.getSL_BUYER_FUND_TYPE());
        if (simbakLeadModel.getSL_BAC_FIRST_BRANCH() != null)
            contentValues.put(context.getString(R.string.SL_BAC_FIRST_BRANCH), simbakLeadModel.getSL_BAC_FIRST_BRANCH());
        if (simbakLeadModel.getSL_MALL_GUESTCODE() != null)
            contentValues.put(context.getString(R.string.SL_MALL_GUESTCODE), simbakLeadModel.getSL_MALL_GUESTCODE());
        if (simbakLeadModel.getSL_MALL_MALLCODE() != null)
            contentValues.put(context.getString(R.string.SL_MALL_MALLCODE), simbakLeadModel.getSL_MALL_MALLCODE());
        if (simbakLeadModel.getSL_MALL_ROOMCODE() != null)
            contentValues.put(context.getString(R.string.SL_MALL_ROOMCODE), simbakLeadModel.getSL_MALL_ROOMCODE());
        if (simbakLeadModel.getSL_CORRESPONDENT() != null)
            contentValues.put(context.getString(R.string.SL_CORRESPONDENT), simbakLeadModel.getSL_CORRESPONDENT());
        if (simbakLeadModel.getSL_BAC_REFF_TITLE() != null)
            contentValues.put(context.getString(R.string.SL_BAC_REFF_TITLE), simbakLeadModel.getSL_BAC_REFF_TITLE());
        if (simbakLeadModel.getSL_MARRIAGE() != null)
            contentValues.put(context.getString(R.string.SL_MARRIAGE), simbakLeadModel.getSL_MARRIAGE());
        if (simbakLeadModel.getSL_OWNERTYPE() != null)
            contentValues.put(context.getString(R.string.SL_OWNERTYPE), simbakLeadModel.getSL_OWNERTYPE());
        if (simbakLeadModel.getSL_TYPE() != null)
            contentValues.put(context.getString(R.string.SL_TYPE), simbakLeadModel.getSL_TYPE());
        if (simbakLeadModel.getSL_LAST_POS() != null)
            contentValues.put(context.getString(R.string.SL_LAST_POS), simbakLeadModel.getSL_LAST_POS());
        if (simbakLeadModel.getSL_FAV() != null)
            contentValues.put(context.getString(R.string.SL_FAV), simbakLeadModel.getSL_FAV());
        if (simbakLeadModel.getSL_NAMA_CABANG() != null)
            contentValues.put(context.getString(R.string.SL_NAMA_CABANG), simbakLeadModel.getSL_NAMA_CABANG());
        if (simbakLeadModel.getUTL_USER_ID() != null)
            contentValues.put(context.getString(R.string.UTL_USER_ID), simbakLeadModel.getUTL_USER_ID());

        long rowInserted = simbakLeadModel.getSL_TAB_ID();
        db.insert(context.getString(R.string.TABLE_SIMBAK_LEAD), null, contentValues);
        db.close();

        return rowInserted;
    }

    public long insertToSimbakListAddress(SimbakAddressModel simbakAddressModel) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long rowInserted;

        ContentValues contentValues = new ContentValues();
        contentValues.put(context.getString(R.string.SLA2_TAB_ID), simbakAddressModel.getSLA2_TAB_ID());
        contentValues.put(context.getString(R.string.SLA2_ID), simbakAddressModel.getSLA2_ID());
        contentValues.put(context.getString(R.string.SLA2_INST_ID), simbakAddressModel.getSLA2_INST_ID());
        contentValues.put(context.getString(R.string.SLA2_INST_TAB_ID), simbakAddressModel.getSLA2_INST_TAB_ID());
        contentValues.put(context.getString(R.string.SLA2_INST_TYPE), simbakAddressModel.getSLA2_INST_TYPE());
        contentValues.put(context.getString(R.string.SLA2_ADDR_TYPE), simbakAddressModel.getSLA2_ADDR_TYPE());
        contentValues.put(context.getString(R.string.SLA2_DETAIL), simbakAddressModel.getSLA2_DETAIL());
        contentValues.put(context.getString(R.string.SLA2_POSTCODE), simbakAddressModel.getSLA2_POSTCODE());
        contentValues.put(context.getString(R.string.SLA2_CITY), simbakAddressModel.getSLA2_CITY());
        contentValues.put(context.getString(R.string.SLA2_STATE), simbakAddressModel.getSLA2_STATE());
        contentValues.put(context.getString(R.string.SLA2_HP1), simbakAddressModel.getSLA2_HP1());
        contentValues.put(context.getString(R.string.SLA2_HP2), simbakAddressModel.getSLA2_HP2());
        contentValues.put(context.getString(R.string.SLA2_PHONECODE), simbakAddressModel.getSLA2_PHONECODE());
        contentValues.put(context.getString(R.string.SLA2_PHONE1), simbakAddressModel.getSLA2_PHONE1());
        contentValues.put(context.getString(R.string.SLA2_PHONE2), simbakAddressModel.getSLA2_PHONE2());
        contentValues.put(context.getString(R.string.SLA2_EMAIL), simbakAddressModel.getSLA2_EMAIL());
        contentValues.put(context.getString(R.string.SLA2_FAXCODE), simbakAddressModel.getSLA2_FAXCODE());
        contentValues.put(context.getString(R.string.SLA2_FAX), simbakAddressModel.getSLA2_FAX());
        contentValues.put(context.getString(R.string.SLA2_LAT), simbakAddressModel.getSLA2_LAT());
        contentValues.put(context.getString(R.string.SLA2_LON), simbakAddressModel.getSLA2_LON());
        contentValues.put(context.getString(R.string.SLA2_OLD_ID), simbakAddressModel.getSLA2_OLD_ID());

        rowInserted = db.insert(context.getString(R.string.TABLE_SIMBAK_LIST_ADDRESS), null, contentValues);
        db.close();
        return rowInserted;
    }

    public long insertToSimbakAccount(SimbakAccountModel simbakAccountModel) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long rowInserted;


        ContentValues contentValues = new ContentValues();
//        if (simbakAccountModel.getSA3_TAB_ID() != null) {
        contentValues.put(context.getString(R.string.SA3_TAB_ID), simbakAccountModel.getSA3_TAB_ID());
//        }
//        if (simbakAccountModel.getSA3_ID() != null) {
        contentValues.put(context.getString(R.string.SA3_ID), simbakAccountModel.getSA3_ID());
//        }
//        if (simbakAccountModel.getSA3_NAME() != null) {
        contentValues.put(context.getString(R.string.SA3_NAME), simbakAccountModel.getSA3_NAME());
//        }
//        if (simbakAccountModel.getSA3_BDATE() != null) {
        contentValues.put(context.getString(R.string.SA3_BDATE), simbakAccountModel.getSA3_BDATE());
//        }
//        if (simbakAccountModel.getSA3_BPLACE() != null) {
        contentValues.put(context.getString(R.string.SA3_BPLACE), simbakAccountModel.getSA3_BPLACE());
//        }
//        if (simbakAccountModel.getSA3_REMARK() != null) {
        contentValues.put(context.getString(R.string.SA3_REMARK), simbakAccountModel.getSA3_REMARK());
//        }
//        if (simbakAccountModel.getSA3_CREATE() != null) {
        contentValues.put(context.getString(R.string.SA3_CREATE), simbakAccountModel.getSA3_CREATE());
//        }
//        if (simbakAccountModel.getSA3_CRTDID() != null) {
        contentValues.put(context.getString(R.string.SA3_CRTDID), simbakAccountModel.getSA3_CRTDID());
//        }
//        if (simbakAccountModel.getSA3_UPDATE() != null) {
        contentValues.put(context.getString(R.string.SA3_UPDATE), simbakAccountModel.getSA3_UPDATE());
//        }
//        if (simbakAccountModel.getSA3_UPTDID() != null) {
        contentValues.put(context.getString(R.string.SA3_UPTDID), simbakAccountModel.getSA3_UPTDID());
//        }
//        if (simbakAccountModel.getSA3_NOSTAFF() != null) {
        contentValues.put(context.getString(R.string.SA3_NOSTAFF), simbakAccountModel.getSA3_NOSTAFF());
//        }
//        if (simbakAccountModel.getSA3_NPWP_EXP() != null) {
        contentValues.put(context.getString(R.string.SA3_NPWP_EXP), simbakAccountModel.getSA3_NPWP_EXP());
//        }
//        if (simbakAccountModel.getSA3_NPWP_DATE() != null) {
        contentValues.put(context.getString(R.string.SA3_NPWP_DATE), simbakAccountModel.getSA3_NPWP_DATE());
//        }
//        if (simbakAccountModel.getSA3_SIUP() != null) {
        contentValues.put(context.getString(R.string.SA3_SIUP), simbakAccountModel.getSA3_SIUP());
//        }
//        if (simbakAccountModel.getSA3_SIUP_DATE() != null) {
        contentValues.put(context.getString(R.string.SA3_SIUP_DATE), simbakAccountModel.getSA3_SIUP_DATE());
//        }
//        if (simbakAccountModel.getSA3_SIUP_EXP() != null) {
        contentValues.put(context.getString(R.string.SA3_SIUP_EXP), simbakAccountModel.getSA3_SIUP_EXP());
//        }
//        if (simbakAccountModel.getSA3_NPWP() != null) {
        contentValues.put(context.getString(R.string.SA3_NPWP), simbakAccountModel.getSA3_NPWP());
//        }
//        if (simbakAccountModel.getSA3_OLD_ID() != null) {
        contentValues.put(context.getString(R.string.SA3_OLD_ID), simbakAccountModel.getSA3_OLD_ID());
//        }
//        if (simbakAccountModel.getSA3_ABBRID() != null) {
        contentValues.put(context.getString(R.string.SA3_ABBRID), simbakAccountModel.getSA3_ABBRID());
//        }
//        if (simbakAccountModel.getSA3_SRC() != null) {
        contentValues.put(context.getString(R.string.SA3_SRC), simbakAccountModel.getSA3_SRC());
//        }
//        if (simbakAccountModel.getSA3_APP() != null) {
        contentValues.put(context.getString(R.string.SA3_APP), simbakAccountModel.getSA3_APP());
//        }
//        if (simbakAccountModel.getSA3_ACTIVE() != null) {
        contentValues.put(context.getString(R.string.SA3_ACTIVE), simbakAccountModel.getSA3_ACTIVE());
//        }
//        if (simbakAccountModel.getSA3_ASSET() != null) {
        contentValues.put(context.getString(R.string.SA3_ASSET), simbakAccountModel.getSA3_ASSET());
//        }
//        if (simbakAccountModel.getSA3_INCOME() != null) {
        contentValues.put(context.getString(R.string.SA3_INCOME), simbakAccountModel.getSA3_INCOME());
//        }
//        if (simbakAccountModel.getSA3_SRC2() != null) {
        contentValues.put(context.getString(R.string.SA3_SRC2), simbakAccountModel.getSA3_SRC2());
//        }
//        if (simbakAccountModel.getSA3_VIA() != null) {
        contentValues.put(context.getString(R.string.SA3_VIA), simbakAccountModel.getSA3_VIA());
//        }
//        if (simbakAccountModel.getSA3_RAW() != null) {
        contentValues.put(context.getString(R.string.SA3_RAW), simbakAccountModel.getSA3_RAW());
//        }
//        if (simbakAccountModel.getSA3_LEADER() != null) {
        contentValues.put(context.getString(R.string.SA3_LEADER), simbakAccountModel.getSA3_LEADER());
//        }
//        if (simbakAccountModel.getSA3_EST_PREMI() != null) {
        contentValues.put(context.getString(R.string.SA3_EST_PREMI), simbakAccountModel.getSA3_EST_PREMI());
//        }
//        if (simbakAccountModel.getSA3_KOMPETITOR() != null) {
        contentValues.put(context.getString(R.string.SA3_KOMPETITOR), simbakAccountModel.getSA3_KOMPETITOR());
//        }
//        if (simbakAccountModel.getSA3_REFF_NAME() != null) {
        contentValues.put(context.getString(R.string.SA3_REFF_NAME), simbakAccountModel.getSA3_REFF_NAME());
//        }
//        if (simbakAccountModel.getSA3_EST_BUDGET() != null) {
        contentValues.put(context.getString(R.string.SA3_EST_BUDGET), simbakAccountModel.getSA3_EST_BUDGET());
//        }
//        if (simbakAccountModel.getSA3_EFEKTIF_DATE() != null) {
        contentValues.put(context.getString(R.string.SA3_EFEKTIF_DATE), simbakAccountModel.getSA3_EFEKTIF_DATE());
//        }
        //        if (simbakAccountModel.getSA3_CATID() != null) {
        contentValues.put(context.getString(R.string.SA3_CATID), simbakAccountModel.getSA3_CATID());
//        }
//        if (simbakAccountModel.getSA3_LAST_POS() != null) {
        contentValues.put(context.getString(R.string.SA3_LAST_POS), simbakAccountModel.getSA3_LAST_POS());
//        }
//        if (simbakAccountModel.getSA3_FAV() != null) {
        contentValues.put(context.getString(R.string.SA3_FAV), simbakAccountModel.getSA3_FAV());
//        }
        rowInserted = db.insert(context.getString(R.string.TABLE_SIMBAK_ACCOUNT), null, contentValues);
        db.close();
        return rowInserted;
    }

    public long insertToSimbakLinkUsrToLead(SimbakLinkUsrToLeadModel simbakLinkUsrToLeadModel) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long rowInserted;


        ContentValues contentValues = new ContentValues();
//        if (simbakLinkUsrToLeadModel.getUTL_LEAD_ID() != null) {
        contentValues.put(context.getString(R.string.UTL_LEAD_ID), simbakLinkUsrToLeadModel.getUTL_LEAD_ID());
//        }
//        if (simbakLinkUsrToLeadModel.getUTL_USER_ID() != null) {
        contentValues.put(context.getString(R.string.UTL_USER_ID), simbakLinkUsrToLeadModel.getUTL_USER_ID());
//        }


        rowInserted = db.insert(context.getString(R.string.TABLE_SIMBAK_LINK_USR_TO_LEAD), null, contentValues);
        db.close();
        return rowInserted;
    }

    public long insertToSimbakLinkAccToLead(SimbakLinkAccToLeadModel simbakLinkAccToLeadModel) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long rowInserted;

        ContentValues contentValues = new ContentValues();
//        if (simbakLinkAccToLeadModel.getATL_ACC_ID() != null) {
        contentValues.put(context.getString(R.string.ATL_ACC_ID), simbakLinkAccToLeadModel.getATL_ACC_ID());
//        }
//        if (simbakLinkAccToLeadModel.getATL_LEAD_ID() != null) {
        contentValues.put(context.getString(R.string.ATL_LEAD_ID), simbakLinkAccToLeadModel.getATL_LEAD_ID());
//        }

        rowInserted = db.insert(context.getString(R.string.TABLE_SIMBAK_LINK_ACC_TO_LEAD), null, contentValues);
        db.close();
        return rowInserted;
    }

    public long insertToSimbakListProduct(SimbakProductModel simbakProductModel) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long rowInserted;

        ContentValues contentValues = new ContentValues();
//        if (simbakProductModel.getSLP_ID() != null) {
        contentValues.put(context.getString(R.string.SLP_TAB_ID), simbakProductModel.getSLP_TAB_ID());
//        }
//        if (simbakProductModel.getSLP_INST_ID() != null) {
        contentValues.put(context.getString(R.string.SLP_ID), simbakProductModel.getSLP_ID());
//        }
//        if (simbakProductModel.getSLP_INST_TYPE() != null) {
        contentValues.put(context.getString(R.string.SLP_INST_ID), simbakProductModel.getSLP_INST_ID());
//        }
//        if (simbakProductModel.getSLP_LSBS_ID() != null) {
        contentValues.put(context.getString(R.string.SLP_INST_TAB_ID), simbakProductModel.getSLP_INST_TAB_ID());
//        }
//        if (simbakProductModel.getSLP_LSBS_NO() != null) {
        contentValues.put(context.getString(R.string.SLP_LSBS_ID), simbakProductModel.getSLP_LSBS_ID());
//        }
//        if (simbakProductModel.getSLP_NAME() != null) {
        contentValues.put(context.getString(R.string.SLP_LSBS_NO), simbakProductModel.getSLP_LSBS_NO());
//        }
//        if (simbakProductModel.getSLP_CORP_STAGE() != null) {
        contentValues.put(context.getString(R.string.SLP_NAME), simbakProductModel.getSLP_NAME());
//        }
//        if (simbakProductModel.getSLP_ACTIVE() != null) {
        contentValues.put(context.getString(R.string.SLP_LAST_PREMI), simbakProductModel.getSLP_LAST_PREMI());
//        }
//        if (simbakProductModel.getSLP_BRANCH() != null) {
        contentValues.put(context.getString(R.string.SLP_OLD_ID), simbakProductModel.getSLP_OLD_ID());
//        }
//        if (simbakProductModel.getSLP_CAMPAIGN() != null) {
        contentValues.put(context.getString(R.string.SLP_INST_TYPE), simbakProductModel.getSLP_INST_TYPE());
//        }
//        if (simbakProductModel.getSLP_LAST_PREMI() != null) {
        contentValues.put(context.getString(R.string.SLP_CORP_STAGE), simbakProductModel.getSLP_CORP_STAGE());
//        }
//        if (simbakProductModel.getSLP_OLD_ID() != null) {
        contentValues.put(context.getString(R.string.SLP_ACTIVE), simbakProductModel.getSLP_ACTIVE());
//        }
        //        if (simbakProductModel.getSLP_LAST_PREMI() != null) {
        contentValues.put(context.getString(R.string.SLP_BRANCH), simbakProductModel.getSLP_BRANCH());
//        }
//        if (simbakProductModel.getSLP_OLD_ID() != null) {
        contentValues.put(context.getString(R.string.SLP_CAMPAIGN), simbakProductModel.getSLP_CAMPAIGN());
//        }
        //        if (simbakProductModel.getSLP_LAST_PREMI() != null) {
        contentValues.put(context.getString(R.string.SLP_CREATED), simbakProductModel.getSLP_CREATED());
//        }
//        if (simbakProductModel.getSLP_OLD_ID() != null) {
        contentValues.put(context.getString(R.string.SLP_CRTD_ID), simbakProductModel.getSLP_CRTD_ID());
//        }
        //        if (simbakProductModel.getSLP_LAST_PREMI() != null) {
        contentValues.put(context.getString(R.string.SLP_UPDATED), simbakProductModel.getSLP_UPDATED());
//        }
//        if (simbakProductModel.getSLP_OLD_ID() != null) {
        contentValues.put(context.getString(R.string.SLP_UPDTD_ID), simbakProductModel.getSLP_UPDTD_ID());
//        }

        rowInserted = db.insert(context.getString(R.string.TABLE_SIMBAK_LIST_PRODUCT), null, contentValues);
        db.close();
        return rowInserted;
    }

    public long insertToSimbakListActivity(SimbakActivityModel model) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(context.getString(R.string.SLA_TAB_ID), model.getSLA_TAB_ID());
        contentValues.put(context.getString(R.string.SLA_ID), model.getSLA_ID());
        contentValues.put(context.getString(R.string.SLA_INST_ID), model.getSLA_INST_ID());
        contentValues.put(context.getString(R.string.SLA_INST_TAB_ID), model.getSLA_INST_TAB_ID());
        contentValues.put(context.getString(R.string.SLA_NAME), model.getSLA_NAME());
        contentValues.put(context.getString(R.string.SLA_DETAIL), model.getSLA_DETAIL());
        contentValues.put(context.getString(R.string.SLA_PREMI), model.getSLA_PREMI());
        contentValues.put(context.getString(R.string.SLA_IDATE), model.getSLA_IDATE());
        contentValues.put(context.getString(R.string.SLA_SDATE), model.getSLA_SDATE());
        contentValues.put(context.getString(R.string.SLA_EDATE), model.getSLA_EDATE());
        contentValues.put(context.getString(R.string.SLA_REMINDER_DATE), model.getSLA_REMINDER_DATE());
        contentValues.put(context.getString(R.string.SLA_CRTD_DATE), model.getSLA_CRTD_DATE());
        contentValues.put(context.getString(R.string.SLA_CRTD_ID), model.getSLA_CRTD_ID());
        contentValues.put(context.getString(R.string.SLA_OWNER_ID), model.getSLA_OWNER_ID());
        contentValues.put(context.getString(R.string.SLA_UPDTD_DATE), model.getSLA_UPDTD_DATE());
        contentValues.put(context.getString(R.string.SLA_UPDTD_ID), model.getSLA_UPDTD_ID());
        contentValues.put(context.getString(R.string.SLA_LON), model.getSLA_LON());
        contentValues.put(context.getString(R.string.SLA_LAT), model.getSLA_LAT());
        contentValues.put(context.getString(R.string.SLA_NEXT_ID), model.getSLA_NEXT_ID());
        contentValues.put(context.getString(R.string.SLA_REFF_ID), model.getSLA_REFF_ID());
        contentValues.put(context.getString(R.string.SLA_OLD_ID), model.getSLA_OLD_ID());
        contentValues.put(context.getString(R.string.SLA_INST_TYPE), model.getSLA_INST_TYPE());
        contentValues.put(context.getString(R.string.SLA_ACTIVE), model.getSLA_ACTIVE());
        contentValues.put(context.getString(R.string.SLA_TYPE), model.getSLA_TYPE());
        contentValues.put(context.getString(R.string.SLA_SUBTYPE), model.getSLA_SUBTYPE());
        contentValues.put(context.getString(R.string.SLA_CORP_STAGE), model.getSLA_CORP_STAGE());
        contentValues.put(context.getString(R.string.SLA_STATUS), model.getSLA_STATUS());
        contentValues.put(context.getString(R.string.SLA_REMINDER_TYPE), model.getSLA_REMINDER_TYPE());
        contentValues.put(context.getString(R.string.SLA_OWNER_TYPE), model.getSLA_OWNER_TYPE());
        contentValues.put(context.getString(R.string.SLA_OLD_TYPE), model.getSLA_OLD_TYPE());
        contentValues.put(context.getString(R.string.SLA_OLD_SUBTYPE), model.getSLA_OLD_SUBTYPE());
        contentValues.put(context.getString(R.string.SLA_USER_ID), model.getSLA_USER_ID());
        contentValues.put(context.getString(R.string.SLA_RPT_CAT), model.getSLA_RPT_CAT());
        contentValues.put(context.getString(R.string.SLA_NXT_ACTION), model.getSLA_NXT_ACTION());
        contentValues.put(context.getString(R.string.SLA_CURR_LVL), model.getSLA_CURR_LVL());
        contentValues.put(context.getString(R.string.SLA_CORP_COMMEN), model.getSLA_CORP_COMMEN());
        contentValues.put(context.getString(R.string.SLA_PREMI2), model.getSLA_PREMI2());
        contentValues.put(context.getString(R.string.SLA_PREMI3), model.getSLA_PREMI3());
        contentValues.put(context.getString(R.string.SLA_LAST_POS), model.getSLA_LAST_POS());
        contentValues.put(context.getString(R.string.SLA_SRC), model.getSLA_SRC());
        contentValues.put(context.getString(R.string.SL_ID_), model.getSL_ID_());
        contentValues.put(context.getString(R.string.SL_NAME), model.getSL_NAME());
        db.insert(context.getString(R.string.TABLE_SIMBAK_LIST_ACTIVITY), null, contentValues);
        db.close();
        return model.getSLA_TAB_ID();
    }

    public long insertToSimbakListSPAJ(SimbakSpajModel simbakSpajModel) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long rowInserted;


        ContentValues contentValues = new ContentValues();
//        if (simbakSpajModel.getSLS_TAB_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_TAB_ID), simbakSpajModel.getSLS_TAB_ID());
//        }
        //        if (simbakSpajModel.getSLS_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_ID), simbakSpajModel.getSLS_ID());
//        }
        //        if (simbakSpajModel.getSLS_INST_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_INST_ID), simbakSpajModel.getSLS_INST_ID());
//        }
        //        if (simbakSpajModel.getSLS_INST_TAB_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_INST_TAB_ID), simbakSpajModel.getSLS_INST_TAB_ID());
//        }
        //        if (simbakSpajModel.getSLS_PROPOSAL() != null) {
        contentValues.put(context.getString(R.string.SLS_PROPOSAL), simbakSpajModel.getSLS_PROPOSAL());
//        }
        //        if (simbakSpajModel.getSLS_SPAJ() != null) {
        contentValues.put(context.getString(R.string.SLS_SPAJ), simbakSpajModel.getSLS_SPAJ());
//        }
//        if (simbakSpajModel.getSLS_POLIS() != null) {
        contentValues.put(context.getString(R.string.SLS_POLIS), simbakSpajModel.getSLS_POLIS());
//        }
        //        if (simbakSpajModel.getSLS_OLD_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_OLD_ID), simbakSpajModel.getSLS_OLD_ID());
//        }
//        if (simbakSpajModel.getSLS_INST_TYPE() != null) {
        contentValues.put(context.getString(R.string.SLS_INST_TYPE), simbakSpajModel.getSLS_INST_TYPE());
//        }
        //        if (simbakSpajModel.getSLS_USED() != null) {
        contentValues.put(context.getString(R.string.SLS_USED), simbakSpajModel.getSLS_USED());
//        }
        //        if (simbakSpajModel.getSLS_PROP_IN() != null) {
        contentValues.put(context.getString(R.string.SLS_PROP_IN), simbakSpajModel.getSLS_PROP_IN());
//        }
        //        if (simbakSpajModel.getSLS_SPAJ_IN() != null) {
        contentValues.put(context.getString(R.string.SLS_SPAJ_IN), simbakSpajModel.getSLS_SPAJ_IN());
//        }
        //        if (simbakSpajModel.getSLS_SPAJ_TMP() != null) {
        contentValues.put(context.getString(R.string.SLS_SPAJ_TMP), simbakSpajModel.getSLS_SPAJ_TMP());
//        }
        //        if (simbakSpajModel.getSLS_SPAJ_TMP_IN() != null) {
        contentValues.put(context.getString(R.string.SLS_SPAJ_TMP_IN), simbakSpajModel.getSLS_SPAJ_TMP_IN());
//        }
        //        if (simbakSpajModel.getSLS_CREATED() != null) {
        contentValues.put(context.getString(R.string.SLS_CREATED), simbakSpajModel.getSLS_CREATED());
//        }
        //        if (simbakSpajModel.getSLS_CRTD_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_CRTD_ID), simbakSpajModel.getSLS_CRTD_ID());
//        }
        //        if (simbakSpajModel.getSLS_UPDATED() != null) {
        contentValues.put(context.getString(R.string.SLS_UPDATED), simbakSpajModel.getSLS_UPDATED());
//        }
        //        if (simbakSpajModel.getSLS_UPDTD_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_UPDTD_ID), simbakSpajModel.getSLS_UPDTD_ID());
//        }


        rowInserted = db.insert(context.getString(R.string.TABLE_SIMBAK_LIST_SPAJ), null, contentValues);
        db.close();
        return rowInserted;
    }


    public boolean insertToTableJsonDropdown(String tableName,
                                             int id,
                                             String label,
                                             String labelEng,
                                             int level1,
                                             int level2,
                                             int level3,
                                             int level4,
                                             int jn_bank) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Long rowInserted;

        ContentValues contentValues = new ContentValues();
        contentValues.put(context.getString(R.string.ID), id);
        contentValues.put(context.getString(R.string.LABEL), label);
        contentValues.put(context.getString(R.string.LABEL_ENG), labelEng);
        contentValues.put("LEVEL1", level1);
        contentValues.put("LEVEL2", level2);
        contentValues.put("LEVEL3", level3);
        contentValues.put("LEVEL4", level4);
        contentValues.put("JN_BANK", jn_bank);

        rowInserted = db.insert(tableName, null, contentValues);
        db.close();
        return rowInserted != -1;
    }

    public boolean insertToTableJsonDropdown(String tableName,
                                             int id,
                                             String label,
                                             String labelEng,
                                             int type,
                                             int nextLevel,
                                             int jn_bank) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Long rowInserted;

        ContentValues contentValues = new ContentValues();
        contentValues.put(context.getString(R.string.ID), id);
        contentValues.put(context.getString(R.string.LABEL), label);
        contentValues.put(context.getString(R.string.LABEL_ENG), labelEng);
        contentValues.put(context.getString(R.string.TYPE), type);
        contentValues.put("NEXT_LEVEL", nextLevel);
        contentValues.put("JN_BANK", jn_bank);

        rowInserted = db.insert(tableName, null, contentValues);
        db.close();
        return rowInserted != -1;
    }

    public boolean insertToTableJsonDropdown(String tableName, int id, String label) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Long rowInserted = (long) -1;

        ContentValues contentValues = new ContentValues();
        contentValues.put(context.getString(R.string.ID), id);
        contentValues.put(context.getString(R.string.LABEL), label);

        rowInserted = db.insert(tableName, null, contentValues);
        db.close();
        return rowInserted != -1;
    }

    public boolean insertToTableJsonDropdown(String tableName, int id, String label, String labelEng) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Long rowInserted = (long) -1;

        ContentValues contentValues = new ContentValues();
        contentValues.put(context.getString(R.string.ID), id);
        contentValues.put(context.getString(R.string.LABEL), label);
        contentValues.put(context.getString(R.string.LABEL_ENG), labelEng);

        rowInserted = db.insert(tableName, null, contentValues);
        db.close();
        return rowInserted != -1;
    }

    public boolean insertToTableJsonDropdown(String tableName, String id, String label, String cabangId) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Long rowInserted = (long) -1;

        ContentValues contentValues = new ContentValues();
        contentValues.put(context.getString(R.string.ID), id);
        contentValues.put(context.getString(R.string.LABEL), label);
        contentValues.put(context.getString(R.string.CABANG_ID), cabangId);

        rowInserted = db.insert(tableName, null, contentValues);
        db.close();
        return rowInserted != -1;
    }

    public void insertAgentBranchToDatabase(List<BcBranch> branches) {
        if (branches != null && branches.size() > 0) {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.beginTransaction();
            try {
                ContentValues contentValues = new ContentValues();
                for (BcBranch model : branches) {
                    Log.v(TAG, "Inserting agent branch to database");

                    String ajsCabang = model.getAjsCbng().trim();

                    contentValues.put(LRB_ID_COLUMN, model.getLrbId().trim());
                    contentValues.put(MSAG_ID_COLUMN, model.getMsagId().trim());
                    contentValues.put(AGENT_NAME_COLUMN, model.getAgentName().trim());
                    contentValues.put(AJS_CABANG_COLUMN, ajsCabang);
                    contentValues.put(SIMAS_CABANG_COLUMN, model.getSimasCbng().trim());
                    contentValues.put(CABANG_COLUMN, model.getCabang().trim());
                    contentValues.put(WILAYAH_COLUMN, model.getWilayah().trim());

                    db.insert(TABLE_NAME_SIMBAK_AGENT_BRANCH, null, contentValues);
                }
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }

        }
    }

    public void insertActivityListToDatabase(List<LeadActivityList> activities) {
        if (activities != null && activities.size() > 0) {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.beginTransaction();
            try {
                ContentValues contentValues = new ContentValues();
                for (LeadActivityList activity : activities) {


                    contentValues.put("SLA_TAB_ID", activity.getSLAID()); //
                    contentValues.put("SLA_ID", activity.getSLAID());
                    contentValues.put("SLA_INST_ID", activity.getSLAINSTID());
                    contentValues.put("SLA_INST_TAB_ID", activity.getSL_TAB_ID());
                    contentValues.put("SLA_NAME", activity.getSLANAME());
                    contentValues.put("SLA_DETAIL", activity.getSLADETAIL());
                    contentValues.put("SLA_PREMI", activity.getSLAPREMI());
                    contentValues.put("SLA_IDATE", activity.getSLAIDATE());
                    contentValues.put("SLA_SDATE", activity.getSLASDATE());
                    contentValues.put("SLA_EDATE", activity.getSLAEDATE());
                    contentValues.put("SLA_REMINDER_DATE", activity.getSLAREMINDERDATE());
                    contentValues.put("SLA_CRTD_DATE", activity.getSLACRTDDATE());
                    contentValues.put("SLA_CRTD_ID", activity.getSLACRTDID());
                    contentValues.put("SLA_OWNER_ID", activity.getSLAOWNERID());
                    contentValues.put("SLA_UPDTD_DATE", activity.getSLAUPDTDDATE());
                    contentValues.put("SLA_UPDTD_ID", activity.getSLAUPDTDID());
                    contentValues.put("SLA_LON", activity.getSLALON());
                    contentValues.put("SLA_LAT", activity.getSLALAT());
                    contentValues.put("SLA_NEXT_ID", activity.getSLANEXTID());
                    contentValues.put("SLA_REFF_ID", activity.getSLAREFFID());
                    contentValues.put("SLA_OLD_ID", "");
                    contentValues.put("SLA_INST_TYPE", activity.getSLAINSTTYPE());
                    contentValues.put("SLA_ACTIVE", activity.getSLAACTIVE());
                    contentValues.put("SLA_TYPE", activity.getSLATYPE());
                    contentValues.put("SLA_SUBTYPE", activity.getSLASUBTYPE());
                    contentValues.put("SLA_CORP_STAGE", activity.getSLACORPSTAGE());
                    contentValues.put("SLA_STATUS", activity.getSLASTATUS());
                    contentValues.put("SLA_REMINDER_TYPE", activity.getSLAREMINDERTYPE());
                    contentValues.put("SLA_OWNER_TYPE", activity.getSLAOWNERTYPE());
                    contentValues.put("SLA_OLD_TYPE", "");
                    contentValues.put("SLA_OLD_SUBTYPE", activity.getSLASUBTYPE());
                    contentValues.put("SLA_USER_ID", activity.getSLAUSERID());
                    contentValues.put("SLA_RPT_CAT", activity.getSLARPTCAT());
                    contentValues.put("SLA_NXT_ACTION", "");
                    contentValues.put("SLA_CURR_LVL", activity.getSLACURRLVL());
                    contentValues.put("SLA_CORP_COMMEN", activity.getSLACORPCOMMEN());
                    contentValues.put("SLA_PREMI2", activity.getSLAPREMI2());
                    contentValues.put("SLA_PREMI3", activity.getSLAPREMI3());
                    contentValues.put("SLA_LAST_POS", 0);
                    contentValues.put("SLA_SRC", "");
                    contentValues.put("SL_NAME", activity.getSL_NAME());
                    contentValues.put("SL_ID_", activity.getSL_ID());
                    Log.v(TAG, "Inserting activity list to database. SL NAME " + activity.getSL_NAME() + " SL TAB ID is " + activity.getSL_TAB_ID());

                    db.insert(TABLE_NAME_SIMBAK_LIST_ACTIVITY, null, contentValues);
                }
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }

        }
    }

    public void insertActivityToDatabase(LeadActivityList activity) {


        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {

            ContentValues contentValues = new ContentValues();
            contentValues.put("SLA_TAB_ID", activity.getSLAID());
            contentValues.put("SLA_ID", activity.getSLAID());
            contentValues.put("SLA_INST_ID", activity.getSLAINSTID()); // For BSIM, this inst id may contains product id not lead id, depends on INST_TYPE
            contentValues.put("SLA_INST_TAB_ID", activity.getSL_TAB_ID());
            contentValues.put("SLA_NAME", activity.getSLANAME());
            contentValues.put("SLA_DETAIL", activity.getSLADETAIL());
            contentValues.put("SLA_PREMI", activity.getSLAPREMI());
            contentValues.put("SLA_IDATE", activity.getSLAIDATE());
            contentValues.put("SLA_SDATE", activity.getSLASDATE());
            contentValues.put("SLA_EDATE", activity.getSLAEDATE());
            contentValues.put("SLA_REMINDER_DATE", activity.getSLAREMINDERDATE());
            contentValues.put("SLA_CRTD_DATE", activity.getSLACRTDDATE());
            contentValues.put("SLA_CRTD_ID", activity.getSLACRTDID());
            contentValues.put("SLA_OWNER_ID", activity.getSLAOWNERID());
            contentValues.put("SLA_UPDTD_DATE", activity.getSLAUPDTDDATE());
            contentValues.put("SLA_UPDTD_ID", activity.getSLAUPDTDID());
            contentValues.put("SLA_LON", activity.getSLALON());
            contentValues.put("SLA_LAT", activity.getSLALAT());
            contentValues.put("SLA_NEXT_ID", activity.getSLANEXTID());
            contentValues.put("SLA_REFF_ID", activity.getSLAREFFID());
            contentValues.put("SLA_OLD_ID", "");
            contentValues.put("SLA_INST_TYPE", activity.getSLAINSTTYPE());
            contentValues.put("SLA_ACTIVE", activity.getSLAACTIVE());
            contentValues.put("SLA_TYPE", activity.getSLATYPE());
            contentValues.put("SLA_SUBTYPE", activity.getSLASUBTYPE());
            contentValues.put("SLA_CORP_STAGE", activity.getSLACORPSTAGE());
            contentValues.put("SLA_STATUS", activity.getSLASTATUS());
            contentValues.put("SLA_REMINDER_TYPE", activity.getSLAREMINDERTYPE());
            contentValues.put("SLA_OWNER_TYPE", activity.getSLAOWNERTYPE());
            contentValues.put("SLA_OLD_TYPE", "");
            contentValues.put("SLA_OLD_SUBTYPE", activity.getSLASUBTYPE());
            contentValues.put("SLA_USER_ID", activity.getSLAUSERID());
            contentValues.put("SLA_RPT_CAT", activity.getSLARPTCAT());
            contentValues.put("SLA_NXT_ACTION", "");
            contentValues.put("SLA_CURR_LVL", activity.getSLACURRLVL());
            contentValues.put("SLA_CORP_COMMEN", activity.getSLACORPCOMMEN());
            contentValues.put("SLA_PREMI2", activity.getSLAPREMI2());
            contentValues.put("SLA_PREMI3", activity.getSLAPREMI3());
            contentValues.put("SLA_LAST_POS", 0);
            contentValues.put("SLA_SRC", "");
            contentValues.put("SL_NAME", activity.getSL_NAME());
            contentValues.put("SL_ID_", activity.getSL_ID());

            db.insert(TABLE_NAME_SIMBAK_LIST_ACTIVITY, null, contentValues);

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }


    }

    public List<String> insertSimbakLeadToDatabase(List<DownloadLeadResponse> leadList) {
        List<String> insertedLeadTabIdList = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            for (DownloadLeadResponse lead : leadList) {

                String slTabId = lead.getSL_TAB_ID();
                String slId = lead.getSLID();

                //Clean case. When lead is having SL_ID and SL_TAB_ID (both is not null or empty)
                if (!TextUtils.isEmpty(slTabId) && !TextUtils.isEmpty(slId)) {
                    contentValues.put("SL_ID", slId);
                    contentValues.put("SL_NAME", lead.getSLNAME());
                    contentValues.put("SL_APP", lead.getSLAPP());
                    contentValues.put("SL_ACTIVE", lead.getSLACTIVE());
                    contentValues.put("SL_UMUR", lead.getSLUMUR());
                    contentValues.put("SL_GENDER", lead.getSLGENDER());
                    contentValues.put("SL_LAST_POS", 0);
                    contentValues.put("SL_UPTD_DATE", lead.getSLUPTDDATE());
                    contentValues.put("SL_SRC", lead.getSLSRC());
                    contentValues.put("SL_TYPE", lead.getSLTYPE());
                    contentValues.put("SL_BAC_REFF", lead.getSLBACREFF());
                    contentValues.put("SL_BAC_REFF_NAME", lead.getSLBACREFFNAME());
                    contentValues.put("SL_BAC_FIRST_BRANCH", lead.getSLBACFIRSTBRANCH());
                    contentValues.put("SL_BAC_CURR_BRANCH", lead.getSLBACCURRBRANCH());
                    contentValues.put("NAMA_CABANG", lead.getNAMACABANG());
                    contentValues.put("SL_CREATED", lead.getSLCREATED());
                    contentValues.put("SL_UPDATED", lead.getSLUPDATED());
                    contentValues.put("SL_TAB_ID", slTabId);
                    contentValues.put("SL_FAV", 0);
                    contentValues.put("UTL_USER_ID", lead.getUTL_USER_ID());

                    db.insert(TABLE_NAME_SIMBAK_LEAD, null, contentValues);

                    insertedLeadTabIdList.add(slTabId);

                    Log.v(TAG, "Clean case " + lead.getSLNAME());
                } else if (TextUtils.isEmpty(slTabId) && !TextUtils.isEmpty(slId)) {
                    //Not a clean case. When lead is having only SL_ID. So we should fill SL_TAB_ID value with SL_ID

                    contentValues.put("SL_ID", slId);
                    contentValues.put("SL_NAME", lead.getSLNAME());
                    contentValues.put("SL_APP", lead.getSLAPP());
                    contentValues.put("SL_ACTIVE", lead.getSLACTIVE());
                    contentValues.put("SL_UMUR", lead.getSLUMUR());
                    contentValues.put("SL_GENDER", lead.getSLGENDER());
                    contentValues.put("SL_LAST_POS", 0);
                    contentValues.put("SL_UPTD_DATE", lead.getSLUPTDDATE());
                    contentValues.put("SL_SRC", lead.getSLSRC());
                    contentValues.put("SL_TYPE", lead.getSLTYPE());
                    contentValues.put("SL_BAC_REFF", lead.getSLBACREFF());
                    contentValues.put("SL_BAC_REFF_NAME", lead.getSLBACREFFNAME());
                    contentValues.put("SL_BAC_FIRST_BRANCH", lead.getSLBACFIRSTBRANCH());
                    contentValues.put("SL_BAC_CURR_BRANCH", lead.getSLBACCURRBRANCH());
                    contentValues.put("NAMA_CABANG", lead.getNAMACABANG());
                    contentValues.put("SL_CREATED", lead.getSLCREATED());
                    contentValues.put("SL_UPDATED", lead.getSLUPDATED());
                    contentValues.put("SL_TAB_ID", slId);
                    contentValues.put("SL_FAV", 0);
                    contentValues.put("UTL_USER_ID", lead.getUTL_USER_ID());

                    db.insert(TABLE_NAME_SIMBAK_LEAD, null, contentValues);

                    insertedLeadTabIdList.add(slTabId);
                    Log.v(TAG, "Not clean case. SL TAB ID is " + slId);
                }

            }

            db.setTransactionSuccessful();

            return insertedLeadTabIdList;
        } finally {
            db.endTransaction();
        }

    }

    public void insertAddressToDatabase(List<DownloadLeadResponse> leadList, List<String> insertedLeadTabIdList) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();


            for (int i = 0; i < leadList.size(); i++) {
                DownloadLeadResponse lead = leadList.get(i);

                if (lead.getHome() != null) {

                    String slTabId = insertedLeadTabIdList.get(i);

                    Home home = lead.getHome();

                    contentValues.put("SLA2_ID", home.getSLA2ID()); //
                    contentValues.put("SLA2_INST_ID", home.getSLA2INSTID());
                    contentValues.put("SLA2_INST_TAB_ID", slTabId);
                    contentValues.put("SLA2_INST_TYPE", home.getSLA2INSTTYPE());
                    contentValues.put("SLA2_ADDR_TYPE", home.getSLA2ADDRTYPE());
                    contentValues.put("SLA2_DETAIL", home.getSLA2DETAIL());
                    contentValues.put("SLA2_POSTCODE", home.getSLA2POSTCODE());
                    contentValues.put("SLA2_CITY", home.getSLA2CITY());
                    contentValues.put("SLA2_STATE", home.getSLA2STATE());
                    contentValues.put("SLA2_HP1", home.getSLA2HP1());
                    contentValues.put("SLA2_HP2", home.getSLA2HP2());
                    contentValues.put("SLA2_PHONECODE", home.getSLA2PHONECODE()); //
                    contentValues.put("SLA2_PHONE1", home.getSLA2PHONE1());
                    contentValues.put("SLA2_PHONE2", home.getSLA2PHONE2());
                    contentValues.put("SLA2_EMAIL", home.getSLA2EMAIL());
                    contentValues.put("SLA2_FAXCODE", home.getSLA2FAXCODE());
                    contentValues.put("SLA2_FAX", home.getSLA2FAX());
                    contentValues.put("SLA2_LAT", home.getSLA2LAT());
                    contentValues.put("SLA2_LON", home.getSLA2LON());
                    contentValues.put("SLA2_OLD_ID", home.getSLA2OLDID());
                    contentValues.put("SLA2_TAB_ID", home.getSLA2TABID());
                    contentValues.put("SLA2_LSPR_ID", home.getSLA2LSPRID());
                    contentValues.put("SLA2_LSKA_ID", home.getSLA2LSKAID());
                    contentValues.put("SLA2_LSKC_ID", home.getSLA2LSKCID());
                    contentValues.put("SLA2_LSKL_ID", home.getSLA2LSKLID());

                    Log.v(TAG, "Inserting lead " + lead.getSLNAME() + " lead tab id " + lead.getSL_TAB_ID());
                    db.insert(TABLE_NAME_SIMBAK_LIST_ADDRESS, null, contentValues);
                }
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

    }

    public void insertSimbakLinkUserLeadToDatabase(List<DownloadLeadResponse> leadList, String msagId, List<String> insertedLeadTabIdList) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();


            for (int i = 0; i < leadList.size(); i++) {

                String slId = insertedLeadTabIdList.get(i);
                // String slTabId = String.valueOf(slId);

                contentValues.put("UTL_LEAD_ID", slId);
                contentValues.put("UTL_USER_ID", msagId);

                //Log.v(TAG, "Inserting simbak link to user lead. SL tab id " + slTabId + " msag id "+msagId);
                db.insert(TABLE_SIMBAK_LINK_USR_TO_LEAD, null, contentValues);

            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void insertToTableLinkUsrToLead(String slTabId, String msagId) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("UTL_LEAD_ID", slTabId);
        contentValues.put("UTL_USER_ID", msagId);

        db.insert(TABLE_NAME_SIMBAK_LINK_USR_TO_LEAD, null, contentValues);
        db.close();
    }

    public void insertToTableLstUserMenu(String menuId, String menuName, String msagId) {
        if (!TextUtils.isEmpty(menuId) && !TextUtils.isEmpty(menuName) && !TextUtils.isEmpty(msagId)) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put("MENU_ID", menuId);
            contentValues.put("MENU_NAME", menuName);
            contentValues.put("MSAG_ID", msagId);

            db.insert(TABLE_NAME_LST_USER_MENU, null, contentValues);
            db.close();
        }
    }

    public void insertToSimbakSPAJ(String spajIdTab, String slaTabId, String kodeAgen) {
        if (!TextUtils.isEmpty(spajIdTab) && !TextUtils.isEmpty(slaTabId) && !TextUtils.isEmpty(kodeAgen)) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues cvSimbakSPAJ = new ContentValues();
            cvSimbakSPAJ.put("SLS_SPAJ_TMP", spajIdTab);
            cvSimbakSPAJ.put(context.getString(R.string.SLS_CRTD_ID), kodeAgen);
            cvSimbakSPAJ.put(context.getString(R.string.SLS_CREATED), StaticMethods.getTodayDateTime());
            cvSimbakSPAJ.put(context.getString(R.string.SLS_UPDTD_ID), kodeAgen);
            cvSimbakSPAJ.put(context.getString(R.string.SLS_UPDATED), StaticMethods.getTodayDateTime());
            cvSimbakSPAJ.put(context.getString(R.string.SLS_INST_TAB_ID), slaTabId);
            cvSimbakSPAJ.put(context.getString(R.string.SLS_INST_TYPE), CrmTableCode.ACTIVITY);

            db.insert(TABLE_NAME_SLS_SPAJ_TEMP, null, cvSimbakSPAJ);
            db.close();
        }

    }

    public void insertToLstAgencyLead(long id, String leadName, int leadAge, int leadGender, int leadFavorite, String agentCode, String createdDate, String updateDate) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("ID", id);
        contentValues.put("LEAD_NAME", leadName);
        contentValues.put("LEAD_AGE", leadAge);
        contentValues.put("LEAD_GENDER", leadGender);
        contentValues.put("LEAD_FAVORITE", leadFavorite);
        contentValues.put("AGENT_CODE", agentCode);
        contentValues.put("CREATED_DATE", createdDate);
        contentValues.put("UPDATE_DATE", updateDate);

        db.insert(TABLE_NAME_LST_AGENCY_LEAD, null, contentValues);
        db.close();


    }
}

