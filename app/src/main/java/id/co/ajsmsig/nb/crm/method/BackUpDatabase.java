package id.co.ajsmsig.nb.crm.method;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.Calendar;

import id.co.ajsmsig.nb.R;

/**
 * Created by faiz_f on 26/08/2016.
 */
public class BackUpDatabase {
    private Context context;

    public BackUpDatabase(Context context) {
        this.context = context;
    }

    public String backUpDatabase() {
        String yuhuu;
        try {
            File sd = Environment.getExternalStorageDirectory();


            if (sd.canWrite()) {
                Calendar c = Calendar.getInstance();

                String currentDBPath = context.getDatabasePath(context.getString(R.string.DATABASE_NAME)).toString();
                String backupDBPath = "DBAutoSales/DBAutoSales "+ StaticMethods.toStringDate(c, 3) +".db";
                File currentDB = context.getDatabasePath(context.getString(R.string.DATABASE_NAME));
                File backupDB = new File(sd, backupDBPath);
                backupDB.getParentFile().mkdirs();

                if (currentDB.exists()) {
                    yuhuu = currentDBPath;
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                } else {
                    yuhuu = "not exist";
                }
            } else {
                yuhuu = "can't write";
            }

        } catch (Exception e) {
            yuhuu = e.toString();
        }
        Log.e("BackUpDb", "backup to: " + yuhuu);
        return yuhuu;
    }
}