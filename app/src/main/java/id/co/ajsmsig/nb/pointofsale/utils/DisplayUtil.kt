package id.co.ajsmsig.nb.pointofsale.utils

import android.content.res.Resources


/**
 * Created by andreyyoshuamanik on 08/02/18.
 */

object DisplayUtil {
    val displayHeight: Int
    get() = Resources.getSystem().displayMetrics.heightPixels

    val displayWidth: Int
    get() = Resources.getSystem().displayMetrics.widthPixels

    val density: Float
    get() = Resources.getSystem().displayMetrics.density

    val widthDp: Float
    get() = DisplayUtil.displayWidth / DisplayUtil.density

    val heightDp: Float
    get() = DisplayUtil.displayHeight / DisplayUtil.density

    val screenSize: String
    get() {

        when (density) {
            0.75F ->  return "LDPI"
            1.0F -> return "MDPI"
            1.5F -> return "HDPI"
            2.0F -> return "XHDPI"
            3.0F -> return "XXHDPI"
            4.0F -> return "XXXHDPI"
            else -> return "${density}"

        }
    }
}