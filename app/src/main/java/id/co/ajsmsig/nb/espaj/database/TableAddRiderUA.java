package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAddRiderUA;

/**
 * Created by rizky_c on 30/09/2017.
 */

public class TableAddRiderUA {
    private Context context;

    public TableAddRiderUA(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(ModelAddRiderUA me, int counter, String SPAJ_ID_TAB, int kode_produk) {
        ContentValues cv = new ContentValues();
        cv.put("SPAJ_ID_TAB", SPAJ_ID_TAB);
        cv.put("COUNTER", counter);
        cv.put("KODE_PRODUK_RIDER", kode_produk);
        cv.put("KODE_SUBPRODUK_RIDER", me.getKode_subproduk_rider());
        cv.put("UNIT_RIDER", me.getUnit_rider());
        cv.put("KLAS_RIDER", me.getKlas_rider());
        cv.put("RATE_RIDER", me.getRate_rider());
        cv.put("TERTANGGUNG_RIDER", me.getTertanggung_rider());
        cv.put("PERSENTASE_RIDER", me.getPersentase_rider());
        cv.put("UP_RIDER", me.getUp_rider());
        cv.put("PREMI_RIDER", me.getPremi_rider());
        cv.put("TGLMULAI_RIDER", me.getTglmulai_rider());
        cv.put("TGLAKHIR_RIDER", me.getTglakhir_rider());
        cv.put("MASA_RIDER", me.getMasa_rider());
        cv.put("AKHIRBAYAR_RIDER", me.getAkhirbayar_rider());
        cv.put("PESERTA_ASKES", me.getPeserta_askes());
        cv.put("JEKEL_ASKES", me.getJekel_askes());
        cv.put("TTL_ASKES", me.getTtl_askes());
        cv.put("USIA_ASKES", me.getUsia_askes());
        cv.put("HUBUNGAN_ASKES", me.getHubungan_askes());
        cv.put("PRODUK_ASKES", me.getProduk_askes());
        cv.put("RIDER_ASKES", me.getRider_askes());
        cv.put("TINGGI_ASKES", me.getTinggi_askes());
        cv.put("BERAT_ASKES", me.getBerat_askes());
        cv.put("WARGANEGARA_ASKES", me.getWarganegara_askes());
        cv.put("PEKERJAAN_ASKES", me.getPekerjaan_askes());

        return cv;
    }

    public ArrayList<ModelAddRiderUA> getObjectArray(Cursor cursor) {
        ArrayList<ModelAddRiderUA> mes = new ArrayList<ModelAddRiderUA>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ModelAddRiderUA me = new ModelAddRiderUA();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("COUNTER"))) {
                    me.setCounter(cursor.getInt(cursor.getColumnIndexOrThrow("COUNTER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("KODE_PRODUK_RIDER"))) {
                    me.setKode_produk_rider(cursor.getInt(cursor.getColumnIndexOrThrow("KODE_PRODUK_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("KODE_SUBPRODUK_RIDER"))) {
                    me.setKode_subproduk_rider(cursor.getInt(cursor.getColumnIndexOrThrow("KODE_SUBPRODUK_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("TERTANGGUNG_RIDER"))) {
                    me.setTertanggung_rider(cursor.getInt(cursor.getColumnIndexOrThrow("TERTANGGUNG_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("RATE_RIDER"))) {
                    me.setRate_rider(cursor.getDouble(cursor.getColumnIndexOrThrow("RATE_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("UNIT_RIDER"))) {
                    me.setUnit_rider(cursor.getInt(cursor.getColumnIndexOrThrow("UNIT_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("KLAS_RIDER"))) {
                    me.setKlas_rider(cursor.getInt(cursor.getColumnIndexOrThrow("KLAS_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("PERSENTASE_RIDER"))) {
                    me.setPersentase_rider(cursor.getInt(cursor.getColumnIndexOrThrow("PERSENTASE_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("UP_RIDER"))) {
                    me.setUp_rider(cursor.getDouble(cursor.getColumnIndexOrThrow("UP_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("PREMI_RIDER"))) {
                    me.setPremi_rider(cursor.getDouble(cursor.getColumnIndexOrThrow("PREMI_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("TGLMULAI_RIDER"))) {
                    me.setTglmulai_rider(cursor.getString(cursor.getColumnIndexOrThrow("TGLMULAI_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("TGLAKHIR_RIDER"))) {
                    me.setTglakhir_rider(cursor.getString(cursor.getColumnIndexOrThrow("TGLAKHIR_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("MASA_RIDER"))) {
                    me.setMasa_rider(cursor.getInt(cursor.getColumnIndexOrThrow("MASA_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("AKHIRBAYAR_RIDER"))) {
                    me.setAkhirbayar_rider(cursor.getString(cursor.getColumnIndexOrThrow("AKHIRBAYAR_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("PESERTA_ASKES"))) {
                    me.setPeserta_askes(cursor.getString(cursor.getColumnIndexOrThrow("PESERTA_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("JEKEL_ASKES"))) {
                    me.setJekel_askes(cursor.getInt(cursor.getColumnIndexOrThrow("JEKEL_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("TTL_ASKES"))) {
                    me.setTtl_askes(cursor.getString(cursor.getColumnIndexOrThrow("TTL_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("USIA_ASKES"))) {
                    me.setUsia_askes(cursor.getInt(cursor.getColumnIndexOrThrow("USIA_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("HUBUNGAN_ASKES"))) {
                    me.setHubungan_askes(cursor.getInt(cursor.getColumnIndexOrThrow("HUBUNGAN_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("PRODUK_ASKES"))) {
                    me.setProduk_askes(cursor.getInt(cursor.getColumnIndexOrThrow("PRODUK_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("RIDER_ASKES"))) {
                    me.setRider_askes(cursor.getInt(cursor.getColumnIndexOrThrow("RIDER_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("TINGGI_ASKES"))) {
                    me.setTinggi_askes(cursor.getInt(cursor.getColumnIndexOrThrow("TINGGI_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("BERAT_ASKES"))) {
                    me.setBerat_askes(cursor.getInt(cursor.getColumnIndexOrThrow("BERAT_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("WARGANEGARA_ASKES"))) {
                    me.setWarganegara_askes(cursor.getInt(cursor.getColumnIndexOrThrow("WARGANEGARA_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("PEKERJAAN_ASKES"))) {
                    me.setPekerjaan_askes(cursor.getString(cursor.getColumnIndexOrThrow("PEKERJAAN_ASKES")));
                }
                mes.add(me);
                cursor.moveToNext();
            }
            // always close the cursor
            cursor.close();
        }

        return mes;
    }
}

