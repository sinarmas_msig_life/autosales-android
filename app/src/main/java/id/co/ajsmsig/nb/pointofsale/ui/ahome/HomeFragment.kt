package id.co.ajsmsig.nb.pointofsale.ui.ahome


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.ajsmsig.nb.pointofsale.handler.*


import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosFragmentHomeBinding
import id.co.ajsmsig.nb.pointofsale.ui.fragment.BaseFragment
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : BaseFragment(), SelectionHandler {

    private lateinit var dataBinding: PosFragmentHomeBinding


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeVM::class.java)
        subscribeUi(viewModel)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_home, container, false)

        return dataBinding.root
    }

    private fun subscribeUi(viewModel: HomeVM) {
        dataBinding.vm = viewModel
        dataBinding.handler = this

        viewModel.page = page

        // No subtitle for this page
        mainVM.hideSubtitle()
    }

    override fun onClickSelection(selection: PageOption) {
        navigationController.optionSelected(selection)
    }


}// Required empty public constructor