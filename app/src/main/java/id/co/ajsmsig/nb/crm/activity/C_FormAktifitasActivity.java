package id.co.ajsmsig.nb.crm.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.C_Insert;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.database.C_Update;
import id.co.ajsmsig.nb.crm.database.SimbakActivityTable;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.method.async.CrmRestClientUsage;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.CheckForSPAJNumberValidityResponse;
import id.co.ajsmsig.nb.crm.model.form.SimbakActivityModel;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.DateUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class C_FormAktifitasActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener, LoaderManager.LoaderCallbacks<Cursor> {
    private final String TAG = C_FormAktifitasActivity.class.getSimpleName();
    private SimbakActivityModel simbakActivityModel = new SimbakActivityModel();
    private DropboxModel dAktifitas = new DropboxModel();
    private DropboxModel dSubAktifitas = new DropboxModel();
    private String kodeAgen;
    private int jnsLoginBC;
    private Calendar calInputDate;
    private final int LOADER_ID = 400;
    private boolean isAllowedToDoClosing;
    private int subActivityId;
    private int activityId;
    private final int KUNJUNGAN_DAN_PRESENTASI_ID = 51;
    private final int CLOSING = 45;
    private boolean isAddActivityMode;
    private String slaId;
    private String slaTabId;
    private String slName;
    private long slTabId;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_activity_form_aktifitas);
        ButterKnife.bind(this);
        setListenerGeneral();


        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
        jnsLoginBC = sharedPreferences.getInt("JENIS_LOGIN_BC", 0);

        /*Tanggal input diset defaultnya dulu*/
        calInputDate = Calendar.getInstance();

        et_tanggal_input.setText(StaticMethods.toStringDate(calInputDate, 2));
        et_tanggal_pelaksanaan.setText(StaticMethods.toStringDate(calInputDate, 2));

        DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(getApplicationContext());
        et_waktu_input.setText(timeFormat.format(calInputDate.getTime()));
        et_waktu_pelaksanaan.setText(timeFormat.format(calInputDate.getTime()));

        Bundle bundle = getIntent().getExtras();
        if (bundle == null) bundle = new Bundle();

        isAddActivityMode = bundle.getBoolean(Const.INTENT_KEY_ADD_ACTIVITY_MODE);
        slaId = bundle.getString(Const.INTENT_KEY_SLA_ID);
        slaTabId = bundle.getString(Const.INTENT_KEY_SLA_TAB_ID);
        slTabId = bundle.getLong(Const.INTENT_KEY_SL_TAB_ID);
        slName = bundle.getString(Const.INTENT_KEY_SL_NAME);


        setToolbarTitle("Tambah aktivitas");

        if (!isAddActivityMode) {
            fillView();
            setToolbarTitle("Edit aktivitas");
        }

        setAdapter();
        getSupportLoaderManager().initLoader(LOADER_ID, null, this);


    }

    private void setToolbarTitle(String title) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(view -> onBackPressed());
            getSupportActionBar().setTitle(title);
        }
    }

    public void setAdapter() {
        final C_Select c_select = new C_Select(this);

        String selection = getString(R.string.jn_bank) + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(jnsLoginBC)};

        if (jnsLoginBC == 2 || jnsLoginBC == 16) {
            selection = "JN_BANK = ? or JN_BANK = ?";
            selectionArgs = new String[]{String.valueOf(2), String.valueOf(16)};
        }

        ArrayList<DropboxModel> dropboxModels = c_select.selectDropDownList(getString(R.string.TABLE_JSON_ACTIVITY), null, selection, selectionArgs, null);
        ArrayAdapter<DropboxModel> adAktifitas = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, dropboxModels);

        ac_aktifitas.setAdapter(adAktifitas);
        ac_aktifitas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dAktifitas = (DropboxModel) parent.getAdapter().getItem(position);
                activityId = dAktifitas.getId();
                String selection = getString(R.string.TYPE) + " = ? AND ID IN (4,5,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45, 46,47)"; //tanpa closing id = 45
                String[] selectionArgs = new String[]{String.valueOf(dAktifitas.getId())};
                ArrayList<DropboxModel> dropboxModels = c_select.selectDropDownListSubAktivity(getString(R.string.TABLE_JSON_SUB_ACTIVITY), null, selection, selectionArgs, null);
                ArrayAdapter<DropboxModel> acSubAct = new ArrayAdapter<>(C_FormAktifitasActivity.this, android.R.layout.simple_dropdown_item_1line, dropboxModels);
                ac_hasil_aktifitas.setText("");
                ac_hasil_aktifitas.setAdapter(acSubAct);
            }
        });

        if (simbakActivityModel.getSLA_TYPE() != null) {
            selection = getString(R.string.TYPE) + " = ? AND ID IN (4,5,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47)";
            selectionArgs = new String[]{String.valueOf(dAktifitas.getId())};
            dropboxModels = c_select.selectDropDownListSubAktivity(getString(R.string.TABLE_JSON_SUB_ACTIVITY), null, selection, selectionArgs, null);
            ArrayAdapter<DropboxModel> acSubAct = new ArrayAdapter<>(this,
                    android.R.layout.simple_dropdown_item_1line, dropboxModels);

            ac_hasil_aktifitas.setAdapter(acSubAct);
        }
        final String finalSelection = selection;
        ac_hasil_aktifitas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dSubAktifitas = (DropboxModel) parent.getAdapter().getItem(position);
                subActivityId = dSubAktifitas.getId();

                if (shouldShowSPAJNumberTextField()) {

                    if (isAllowedToDoClosing) {
                        cl_no_spaj.setVisibility(View.VISIBLE);
                        //Hide description text field when user choose closing activity
                        cl_deskripsi.setVisibility(View.GONE);
                    }

                } else {
                    cl_no_spaj.setVisibility(View.GONE);
                    //Show description text field when user choose non-closing activity
                    cl_deskripsi.setVisibility(View.VISIBLE);
                }

            }
        });

    }

    /**
     * Displayed when user requested to add closing activity but there are no Kunjungan dan Presentasi Nasabah Tertarik (buat proposal)" activity found on his activity list
     */
    private void showClosingActivityFailedDialog() {
        new AlertDialog.Builder(C_FormAktifitasActivity.this)
                .setTitle("Tidak dapat melakukan aktivitas closing")
                .setMessage("Untuk dapat melakukan aktivitas closing, Anda harus menambahkan aktivitas \"Kunjungan dan Presentasi Nasabah Tertarik (buat proposal)\" terlebih dahulu")
                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                .show();
    }

    /**
     * Show or hide NO SPAJ text field
     */
    private boolean shouldShowSPAJNumberTextField() {
        return (dAktifitas.getId() == 51 || dAktifitas.getLabel().equalsIgnoreCase("Kunjungan dan Presentasi"))
                &&
                (dSubAktifitas.getId() == 45 || dSubAktifitas.getLabel().equalsIgnoreCase("Closing"));
    }


    private void setListenerGeneral() {
        et_tanggal_input.addTextChangedListener(new MTextWatcher(et_tanggal_input));
        et_tanggal_pelaksanaan.addTextChangedListener(new MTextWatcher(et_tanggal_pelaksanaan));
        ac_aktifitas.addTextChangedListener(new MTextWatcher(ac_aktifitas));
        ac_hasil_aktifitas.addTextChangedListener(new MTextWatcher(ac_hasil_aktifitas));
        et_no_spaj.addTextChangedListener(new MTextWatcher(et_no_spaj));
        et_deskripsi.addTextChangedListener(new MTextWatcher(et_deskripsi));
        et_tanggal_pelaksanaan.setOnClickListener(this);
        et_waktu_pelaksanaan.setOnClickListener(this);
        ac_aktifitas.setOnClickListener(this);
        ac_hasil_aktifitas.setOnClickListener(this);
        et_tanggal_pelaksanaan.setOnFocusChangeListener(this);
        et_waktu_pelaksanaan.setOnFocusChangeListener(this);
        ac_aktifitas.setOnFocusChangeListener(this);
        ac_hasil_aktifitas.setOnFocusChangeListener(this);


        btn_ok.setOnClickListener(this);
    }

    private void fillView() {
        C_Select c_select = new C_Select(this);

        Cursor cursor = c_select.getActivityInfo(slaTabId, slaId);
        simbakActivityModel = new SimbakActivityTable(this).getObject(cursor);


        if (simbakActivityModel.getSLA_IDATE() != null) {
            Calendar calInput = StaticMethods.toCalendarTime(simbakActivityModel.getSLA_IDATE());
            et_tanggal_input.setText(StaticMethods.toStringDate(calInput, 2));
            DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(getApplicationContext());
            et_waktu_input.setText(timeFormat.format(calInput.getTime()));
        }

        DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(getApplicationContext());
        if (simbakActivityModel.getSLA_SDATE() != null) {
            Calendar calendar = StaticMethods.toCalendar(simbakActivityModel.getSLA_SDATE());

            et_tanggal_pelaksanaan.setText(StaticMethods.toStringDate(calendar));
            et_waktu_pelaksanaan.setText(timeFormat.format(calendar.getTime()));
        }

        if (simbakActivityModel.getSLA_TYPE() != null) {
            dAktifitas = c_select.selectDropDown(getString(R.string.TABLE_JSON_ACTIVITY), simbakActivityModel.getSLA_TYPE());
            ac_aktifitas.setText(dAktifitas.getLabel());
            if (simbakActivityModel.getSLA_SUBTYPE() == null) {
                cl_hasil_aktifitas.setVisibility(View.GONE);
            }
            if (simbakActivityModel.getSLA_TYPE() == 50) {
                setEnableSpecificView(false);
            }

        }

        if (simbakActivityModel.getSLA_SUBTYPE() != null) {
            dSubAktifitas = c_select.selectDropDown(getString(R.string.TABLE_JSON_SUB_ACTIVITY), simbakActivityModel.getSLA_SUBTYPE());
            ac_hasil_aktifitas.setText(dSubAktifitas.getLabel());
            if (simbakActivityModel.getSLA_SUBTYPE() == 3) {
                setEnableSpecificView(false);
            }
        }

        if (simbakActivityModel.getSLA_DETAIL() != null) {
            et_deskripsi.setText(simbakActivityModel.getSLA_DETAIL());
        }

        Integer slaType = simbakActivityModel.getSLA_TYPE();
        Integer slaSubType = simbakActivityModel.getSLA_SUBTYPE();

        if (slaType != null && slaType == KUNJUNGAN_DAN_PRESENTASI_ID && slaSubType != null && slaSubType == CLOSING) {
            String activityCreatedDate = simbakActivityModel.getSLA_CRTD_DATE();

            if (isAllowedToEditClosingActivity(activityCreatedDate)) {
                setEnableSpecificView(true);
            } else {
                //Ketika edit aktivitas dilakukan lebih dari tanggal 4
                setEnableSpecificView(false);
                new AlertDialog.Builder(C_FormAktifitasActivity.this)
                        .setTitle("Sudah melewati masa edit")
                        .setMessage("Maksimal waktu edit untuk aktivitas closing ini adalah H+4 dari bulan aktivitas pertama kali dibuat")
                        .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                        .show();

            }
        }

    }

    private void setEnableSpecificView(boolean shouldEnable) {
        et_tanggal_pelaksanaan.setEnabled(shouldEnable);
        et_waktu_pelaksanaan.setEnabled(shouldEnable);
        ac_aktifitas.setEnabled(shouldEnable);
        ac_hasil_aktifitas.setEnabled(shouldEnable);
        et_deskripsi.setEnabled(shouldEnable);
        et_no_spaj.setEnabled(shouldEnable);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                clickOk();
                break;
            case R.id.et_tanggal_pelaksanaan:
                String activityCreatedDate = simbakActivityModel.getSLA_CRTD_DATE();

                if (!isAddActivityMode && isClosingActivity() && isAllowedToEditClosingActivity(activityCreatedDate)) {
                    showDatePickerDialogForClosingActivity();
                } else {
                    showDatePickerDialog();
                }

                break;
            case R.id.et_waktu_pelaksanaan:
                showTimeDialog(R.id.et_waktu_pelaksanaan);
                break;
            case R.id.ac_aktifitas:
                ac_aktifitas.showDropDown();
                break;
            case R.id.ac_hasil_aktifitas:
                ac_hasil_aktifitas.showDropDown();
                break;
            default:
                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.et_waktu_pelaksanaan:
                if (hasFocus)
                    showTimeDialog(R.id.et_waktu_pelaksanaan);
                break;
            case R.id.ac_aktifitas:
                if (hasFocus)
                    ac_aktifitas.showDropDown();
                break;
            case R.id.ac_hasil_aktifitas:
                if (hasFocus)
                    ac_hasil_aktifitas.showDropDown();
                break;
            default:
                break;
        }
    }

    private void clickOk() {
        boolean isOk = true;
        boolean isSpajNumberIncluded = false;
        String spajNumber = null;

        if (cl_no_spaj.getVisibility() == View.VISIBLE) {
            spajNumber = et_no_spaj.getText().toString().trim();
            if (TextUtils.isEmpty(spajNumber)) {
                et_no_spaj.setError(getString(R.string.err_msg_spaj_number_empty));
                et_no_spaj.requestFocus();
                isOk = false;
            } else {
                et_no_spaj.setError(null);
                isOk = true;
                isSpajNumberIncluded = true;
            }

        }

        if (et_tanggal_input.getText().toString().trim().length() == 0) {
            isOk = false;
            tv_error_tanggal_input.setText(R.string.field_requirement);
            tv_error_tanggal_input.setVisibility(View.VISIBLE);
            iv_circle_tanggal_input.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
        }

        if (et_tanggal_pelaksanaan.getText().toString().trim().length() == 0) {
            isOk = false;
            tv_error_tanggal_pelaksanaan.setText(R.string.field_requirement);
            tv_error_tanggal_pelaksanaan.setVisibility(View.VISIBLE);
            iv_circle_tanggal_pelaksanaan.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
        }

        if (ac_aktifitas.getText().toString().trim().length() == 0) {
            isOk = false;
            tv_error_aktifitas.setText(R.string.field_requirement);
            tv_error_aktifitas.setVisibility(View.VISIBLE);
            iv_circle_aktifitas.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
        }

        if (cl_hasil_aktifitas.getVisibility() == View.VISIBLE) {
            if (ac_hasil_aktifitas.getText().toString().trim().length() == 0) {
                isOk = false;
                tv_error_hasil_aktifitas.setText(R.string.field_requirement);
                tv_error_hasil_aktifitas.setVisibility(View.VISIBLE);
                iv_circle_hasil_aktifitas.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
            }
        }

        if (isSpajNumberIncluded) {
            checkForSPAJNumberValidity(AppConfig.getBaseUrlCRM().concat("v2/toll/check_spaj/?"), kodeAgen, spajNumber);
        } else {

            //Show closing activity failed dialog only when user select closing activity but not allowed to do closing
            if (!isAllowedToDoClosing && activityId == 51 && subActivityId == 45) {
                showClosingActivityFailedDialog();
                return;
            }

            if (isOk) {
                saveAktifitas(false);
                finish();
            }
        }

    }

    /**
     * Check whether SPAJ Number is existed previously on the server
     *
     * @param url
     * @param agentCode
     * @param regSpaj
     */
    private void checkForSPAJNumberValidity(String url, String agentCode, String regSpaj) {

        //Invoked when user add/edit activity beyond predefined time rules
        if (!isAllowedToMakeClosingActivity()) {

            new AlertDialog.Builder(C_FormAktifitasActivity.this)
                    .setTitle(R.string.dialog_title_exceeding_input_date)
                    .setMessage(R.string.dialog_msg_exceeding_input_date)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .show();
            return;

        }

        final ProgressDialog progressDialog = new ProgressDialog(C_FormAktifitasActivity.this);
        progressDialog.setTitle(getString(R.string.dialog_title_validating));
        progressDialog.setMessage(getString(R.string.dialog_msg_validating_spaj_number));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<CheckForSPAJNumberValidityResponse> call = api.checkSPAJValidity(url, agentCode, regSpaj);
        call.enqueue(new Callback<CheckForSPAJNumberValidityResponse>() {
            @Override
            public void onResponse(Call<CheckForSPAJNumberValidityResponse> call, Response<CheckForSPAJNumberValidityResponse> response) {
                if (response.body() != null) {
                    boolean state = response.body().isState();
                    String message = response.body().getMsg();

                    //Check if SPAJ number state is exist
                    if (state) {

                        new AlertDialog.Builder(C_FormAktifitasActivity.this)
                                .setTitle(R.string.dialog_title_closing_success)
                                .setMessage(message)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        saveAktifitas(true);
                                        dialogInterface.dismiss();
                                    }
                                })
                                .setCancelable(false)
                                .show();

                    } else {
                        Toast.makeText(C_FormAktifitasActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                }

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<CheckForSPAJNumberValidityResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.v(TAG, "Checking SPAJ number validity failed. Response : " + t.getMessage());
                Toast.makeText(C_FormAktifitasActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }

    private void saveAktifitas(boolean isManualInputClosing) {
        C_Insert insert = new C_Insert(C_FormAktifitasActivity.this);
        C_Update update = new C_Update(C_FormAktifitasActivity.this);
        C_Select select = new C_Select(C_FormAktifitasActivity.this);

        simbakActivityModel.setSLA_UPDTD_ID(kodeAgen);
        simbakActivityModel.setSLA_UPDTD_DATE(StaticMethods.getTodayDateTime());
        simbakActivityModel.setSLA_ACTIVE(1);
        simbakActivityModel.setSLA_LAST_POS(1);
        simbakActivityModel.setSLA_SDATE(et_tanggal_pelaksanaan.getText().toString() + " " + et_waktu_pelaksanaan.getText().toString());

        simbakActivityModel.setSLA_TYPE(dAktifitas.getId());
        simbakActivityModel.setSLA_SUBTYPE(dSubAktifitas.getId());

        if (isManualInputClosing) {
            String spajNumber = "Closing SPAJ kertas " + et_no_spaj.getText().toString().trim();
            simbakActivityModel.setSLA_DETAIL(spajNumber);
        } else {
            simbakActivityModel.setSLA_DETAIL(et_deskripsi.getText().toString());
        }

        simbakActivityModel.setSLA_SRC(select.getSlType(slaTabId));
        simbakActivityModel.setSL_NAME(slName);


        if (isAddActivityMode) {
            simbakActivityModel.setSLA_IDATE(StaticMethods.getTodayDateTime());
            simbakActivityModel.setSLA_CRTD_ID(kodeAgen);
            simbakActivityModel.setSLA_CRTD_DATE(StaticMethods.getTodayDateTime());
            simbakActivityModel.setSLA_INST_TAB_ID(slTabId);
            simbakActivityModel.setSLA_INST_TYPE(2);
            simbakActivityModel.setSL_ID_(null);
            long timestamp = DateUtils.generateTimeStamp();
            simbakActivityModel.setSLA_TAB_ID(timestamp);
            insert.insertToSimbakListActivity(simbakActivityModel);


            if (isManualInputClosing) {
                String spaj = et_no_spaj.getText().toString().trim();
                insert.insertToSimbakSPAJ(spaj, String.valueOf(timestamp), kodeAgen);
            }

            setResult(RESULT_OK);

        } else {
            simbakActivityModel.setSLA_UPDTD_DATE(StaticMethods.getTodayDateTime());
            simbakActivityModel.setSLA_LAST_POS(1); // Update this to server
            update.updateActivity(simbakActivityModel);

            setResult(RESULT_OK);
        }


        uploadUnsynchronizedActivity();
        finish();
    }


    /**
     * upload un-synchronized activity data to server
     */
    private void uploadUnsynchronizedActivity() {
        Log.v(TAG, "upload un-synchronized activity data to server");
        List<Long> listIdActs = new C_Select(C_FormAktifitasActivity.this).selectListActivityIdWhichNeedToBeUpdate();

        if (!listIdActs.isEmpty()) {

            if (StaticMethods.isNetworkAvailable(C_FormAktifitasActivity.this)) {

                CrmRestClientUsage crm = new CrmRestClientUsage(C_FormAktifitasActivity.this);
                crm.createUploadActivityRequestBodyFrom(listIdActs);
                crm.getActivityTypeForDropdown(kodeAgen, jnsLoginBC);
            }

        }

    }

    private void showDatePickerDialog() {
        TimeZone timeZone = TimeZone.getTimeZone("GMT");
        Calendar calendar = Calendar.getInstance(timeZone);

        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            calendar.setFirstDayOfWeek(Calendar.MONDAY);

            refreshDate(calendar);

        };

        long activityInputStartDate = getActivityInputStartDate();
        long activityInputForLastWeek = getActivityInputStartDateForLastWeek();
        long currentDate = System.currentTimeMillis();

        DatePickerDialog dialog = new DatePickerDialog(C_FormAktifitasActivity.this, date,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        if (isMonday()) {
            dialog.getDatePicker().setMinDate(currentDate);
        } else if (!isMonday() && !isSunday()) {
            dialog.getDatePicker().setMinDate(activityInputStartDate);
        } else if (isSunday()) {
            dialog.getDatePicker().setMinDate(activityInputForLastWeek);
        }

       /* long activityInputStartMillis = getAddActivityStartDate();
        long activityInputEndMillis = getAddActivityEndDate();

        dialog.getDatePicker().setMinDate(activityInputStartMillis);
        dialog.getDatePicker().setMaxDate(activityInputEndMillis);*/

        dialog.show();

    }

    private void showDatePickerDialogForClosingActivity() {
        TimeZone timeZone = TimeZone.getTimeZone("GMT");

        Calendar calendar = Calendar.getInstance(timeZone);

        Calendar startDateCalendar = Calendar.getInstance(timeZone);
        int currentDay = startDateCalendar.get(Calendar.DATE);
        int currentMonth = startDateCalendar.get(Calendar.MONTH); //Month is zero based, so we add it by 1
        int previousMonth = currentMonth - 1;

        //Set to 1st date on the previous month
        startDateCalendar.set(Calendar.MONTH, previousMonth);
        startDateCalendar.set(Calendar.DATE, 1);
        startDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        startDateCalendar.set(Calendar.MINUTE, 1);
        startDateCalendar.set(Calendar.SECOND, 0);

        long firstDayLastMonthMillis = startDateCalendar.getTimeInMillis();

        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            calendar.setFirstDayOfWeek(Calendar.MONDAY);

            refreshDate(calendar);

        };
        DatePickerDialog dialog = new DatePickerDialog(C_FormAktifitasActivity.this, date,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMinDate(firstDayLastMonthMillis);


        dialog.show();

    }

    private void refreshDate(Calendar calendar) {
        String format = "yyyy-MM-dd";

        DateFormat simpleDateFormat = new SimpleDateFormat(format);
        Date date = calendar.getTime();

        String dateText = simpleDateFormat.format(date);

        et_tanggal_pelaksanaan.setText(dateText);
    }

    /**
     * Get start date time millis of current week (day : Monday) // kalau senin
     *
     * @return
     */
    private long getActivityInputStartDate() {

        Calendar calendar = Calendar.getInstance();
        //Get maximum days count in current month
        int currentWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

        //Add it by 4 days, set the hours, minute, and second to 0.
        //So the converted millis will produce ex: Wed Apr 04 2018 00:00:00
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, currentWeekOfYear);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        //Get the 4th day on next month millis
        long startDate = calendar.getTimeInMillis();

        return startDate;
    }

    /**
     * Get start date time millis of current week (day : Sunday) // kalau minggu
     *
     * @return
     */
    private long getActivityInputStartDateForLastWeek() {

        Calendar calendar = Calendar.getInstance();
        //Get maximum days count in current month
        int currentWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

        //Add it by 4 days, set the hours, minute, and second to 0.
        //So the converted millis will produce ex: Wed Apr 04 2018 00:00:00
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, currentWeekOfYear - 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        //Get the 4th day on next month millis
        long startDate = calendar.getTimeInMillis();

        return startDate;
    }

    /**
     * Get end date time millis of current week (day : Sunday)
     *
     * @return
     */
    private long getActivityInputEndDate() {

        Calendar calendar = Calendar.getInstance();
        //Get maximum days count in current month
        int currentWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

        //Add it by 4 days, set the hours, minute, and second to 0.
        //So the converted millis will produce ex: Wed Apr 04 2018 00:00:00
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, currentWeekOfYear);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        //Get the 4th day on next month millis
        long startDate = calendar.getTimeInMillis();

        return startDate;
    }

    private boolean isSunday() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        return day == Calendar.SUNDAY;
    }

    private boolean isMonday() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        return day == Calendar.MONDAY;
    }

    private boolean isClosingActivity() {
        if (simbakActivityModel.getSLA_SUBTYPE() != null) {
            int activitySubType = simbakActivityModel.getSLA_SUBTYPE();
            return activitySubType == CLOSING;
        }
        return false;
    }

    /*private long getAddActivityStartDate() {
        //Get current date
        Calendar calendar = Calendar.getInstance();
        int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, weekOfYear);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTimeInMillis();
    }

    private long getAddActivityEndDate() {
        //Get current date
        Calendar calendar = Calendar.getInstance();

        //Set one week after now
        calendar.add(Calendar.WEEK_OF_YEAR, +1);
        int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, weekOfYear);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        return calendar.getTimeInMillis();
    }*/

    private Calendar getMinDateRuleUpdate() {
        Calendar minDate = Calendar.getInstance();
        minDate.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        minDate.set(Calendar.YEAR, calInputDate.get(Calendar.YEAR));
        minDate.set(Calendar.WEEK_OF_YEAR, calInputDate.get(Calendar.WEEK_OF_YEAR));

        return minDate;
    }

    private Calendar getMaxDateRuleUpdate() {
        Calendar maxDate = Calendar.getInstance();
        maxDate.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        maxDate.set(Calendar.WEEK_OF_YEAR, maxDate.get(Calendar.WEEK_OF_YEAR) + 1);
        maxDate.set(Calendar.YEAR, calInputDate.get(Calendar.YEAR));
        return maxDate;
    }

    /**
     * Rules for Bank Sinarmas
     * Return a max date for one week after current date
     * Maximum is Sunday
     * Previous week activities will be locked every Monday
     *
     * @return
     */
    private long getMaxDate() {
        //Get current date
        Calendar calendar = Calendar.getInstance();

        //Set one week after now
        calendar.add(Calendar.WEEK_OF_YEAR, +1);
        int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, weekOfYear);

        return calendar.getTimeInMillis();
    }

    /**
     * Rules for Bank Sinarmas
     * Check if user is allowed to make closing activity at the current date
     * Rules : SPAJ is able to input and edit on the present month with maximum input for the fourth day of next month
     *
     * @return
     */
    private boolean isAllowedToMakeClosingActivity() {
        Calendar calendar = Calendar.getInstance();

        //Get current millis
        long currentTimeMillis = calendar.getTimeInMillis();
        //Get maximum days count in current month
        int daysCountInCurrentMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        //Add it by 4 days, set the hours, minute, and second to 0.
        //So the converted millis will produce ex: Wed Apr 04 2018 00:00:00
        calendar.set(Calendar.DATE, daysCountInCurrentMonth + 4);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        //Get the 4th day on next month millis
        long nextMonthAt4thDayMillis = calendar.getTimeInMillis();


        //Will return true if current datetime is before deadline date
        return currentTimeMillis < nextMonthAt4thDayMillis;
    }

    /**
     * Allow edit if only activity created millis is before activity cutoff millis (D+4 on the next month)
     */
    private boolean isAllowedToEditClosingActivity(String activityCreatedDate) {
        if (!TextUtils.isEmpty(activityCreatedDate)) {
            long activityCreatedDateMillis = DateUtils.getMillisFrom(activityCreatedDate);
            long activityCutoffMillis = getMillis4DayAfterCurrentMonth(activityCreatedDateMillis);
            long currentMillis = System.currentTimeMillis();

            return ((activityCreatedDateMillis < currentMillis) && (currentMillis < activityCutoffMillis));
        }

        return false;
    }

    private long getMillis4DayAfterCurrentMonth(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        int nextMonth = calendar.get(Calendar.MONTH) + 1;

        //Add it by 4 days, set the hours, minute, and second to 0.
        calendar.set(Calendar.DATE, 4);
        calendar.set(Calendar.MONTH, nextMonth);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        //Get the 4th day on next month millis
        return calendar.getTimeInMillis();
    }

    public void showTimeDialog(final int id) {
        Log.d(TAG, "showTimeDialog: called");
        TimePickerDialog.OnTimeSetListener time = new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);

                DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(getApplicationContext());

                switch (id) {
                    case R.id.et_waktu_pelaksanaan:
                        et_waktu_pelaksanaan.setText(timeFormat.format(calendar.getTime()));
                        break;
                    default:
                        break;
                }

            }
        };


        StringBuilder sTime;
        int hourOfDay = 9;
        int minute = 0;
        switch (id) {
            case R.id.et_waktu_pelaksanaan:
                sTime = new StringBuilder(et_waktu_pelaksanaan.getText().toString());
                break;
            default:
                sTime = new StringBuilder();
                break;
        }

        if (sTime.length() != 0) {
            hourOfDay = Integer.parseInt(sTime.substring(0, 2));
            minute = Integer.parseInt(sTime.substring(3));
        }


        TimePickerDialog timePickerDialog = new TimePickerDialog(C_FormAktifitasActivity.this, time, hourOfDay, minute, true);
        timePickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
//                isClickAble = true;
            }
        });

        timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
//                isClickAble = true;
            }
        });

        timePickerDialog.show();


    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader cursorLoader;

        switch (id) {
            case LOADER_ID:
                cursor = new C_Select(C_FormAktifitasActivity.this).checkIfActivityContainsBuatProposal(String.valueOf(slTabId));
                break;
        }


        cursorLoader = new CursorLoader(C_FormAktifitasActivity.this) {
            @Override
            public Cursor loadInBackground() {
                return cursor;
            }
        };

        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data.getCount() > 0) {

            while (data.moveToNext()) {
                String activityName = data.getString(data.getColumnIndexOrThrow("LABEL"));

                //Make sure activiy name not null or empty
                if (!TextUtils.isEmpty(activityName)) {
                    //Only allowed closing activity if user previously made a 'kunjungan dan presentasi nasabah tertarik (buat proposal)' activity
                    if (activityName.equalsIgnoreCase("Nasabah Tertarik (buat proposal)")) {
                        isAllowedToDoClosing = true;
                    }
                }

            }

        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    private class MTextWatcher implements TextWatcher {
        private View view;

        MTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                case R.id.et_tanggal_input:
                    if (StaticMethods.isTextWidgetEmpty(et_tanggal_input)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_tanggal_input.setColorFilter(color);
                    tv_error_tanggal_input.setVisibility(View.GONE);
                    break;
                case R.id.et_tanggal_pelaksanaan:
                    if (StaticMethods.isTextWidgetEmpty(et_tanggal_pelaksanaan)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_tanggal_pelaksanaan.setColorFilter(color);
                    tv_error_tanggal_pelaksanaan.setVisibility(View.GONE);
                    break;
                case R.id.ac_aktifitas:
                    if (StaticMethods.isTextWidgetEmpty(ac_aktifitas)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_aktifitas.setColorFilter(color);
                    tv_error_aktifitas.setVisibility(View.GONE);
                    break;
                case R.id.ac_hasil_aktifitas:
                    if (StaticMethods.isTextWidgetEmpty(ac_hasil_aktifitas)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_hasil_aktifitas.setColorFilter(color);
                    tv_error_hasil_aktifitas.setVisibility(View.GONE);
                    break;

                case R.id.et_no_spaj:
                    if (StaticMethods.isTextWidgetEmpty(et_no_spaj)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_no_spaj.setColorFilter(color);
                    tv_error_no_spaj.setVisibility(View.GONE);
                    break;
                case R.id.et_deskripsi:
                    if (StaticMethods.isTextWidgetEmpty(et_deskripsi)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_deskripsi.setColorFilter(color);
                    tv_error_deskripsi.setVisibility(View.GONE);
                    break;

                default:
                    break;
            }
        }
    }

    @BindView(R.id.cl_hasil_aktifitas)
    ConstraintLayout cl_hasil_aktifitas;
    @BindView(R.id.cl_no_spaj)
    ConstraintLayout cl_no_spaj;

    @BindView(R.id.et_tanggal_input)
    EditText et_tanggal_input;
    @BindView(R.id.et_waktu_input)
    EditText et_waktu_input;
    @BindView(R.id.et_tanggal_pelaksanaan)
    EditText et_tanggal_pelaksanaan;
    @BindView(R.id.et_waktu_pelaksanaan)
    EditText et_waktu_pelaksanaan;
    @BindView(R.id.ac_aktifitas)
    AutoCompleteTextView ac_aktifitas;
    @BindView(R.id.ac_hasil_aktifitas)
    AutoCompleteTextView ac_hasil_aktifitas;
    @BindView(R.id.et_deskripsi)
    EditText et_deskripsi;
    @BindView(R.id.et_no_spaj)
    EditText et_no_spaj;
    @BindView(R.id.btn_ok)
    Button btn_ok;

    @BindView(R.id.iv_circle_tanggal_input)
    ImageView iv_circle_tanggal_input;
    @BindView(R.id.iv_circle_tanggal_pelaksanaan)
    ImageView iv_circle_tanggal_pelaksanaan;
    @BindView(R.id.iv_circle_aktifitas)
    ImageView iv_circle_aktifitas;
    @BindView(R.id.iv_circle_hasil_aktifitas)
    ImageView iv_circle_hasil_aktifitas;
    @BindView(R.id.iv_circle_pengingat)
    ImageView iv_circle_pengingat;
    @BindView(R.id.iv_circle_agen_ref)
    ImageView iv_circle_agen_ref;
    @BindView(R.id.iv_circle_deskripsi)
    ImageView iv_circle_deskripsi;
    @BindView(R.id.iv_circle_no_spaj)
    ImageView iv_circle_no_spaj;

    @BindView(R.id.tv_error_tanggal_input)
    TextView tv_error_tanggal_input;
    @BindView(R.id.tv_error_tanggal_pelaksanaan)
    TextView tv_error_tanggal_pelaksanaan;
    @BindView(R.id.tv_error_aktifitas)
    TextView tv_error_aktifitas;
    @BindView(R.id.tv_error_hasil_aktifitas)
    TextView tv_error_hasil_aktifitas;
    @BindView(R.id.tv_error_pengingat)
    TextView tv_error_pengingat;
    @BindView(R.id.tv_error_agen_ref)
    TextView tv_error_agen_ref;
    @BindView(R.id.tv_error_deskripsi)
    TextView tv_error_deskripsi;
    @BindView(R.id.tv_error_no_spaj)
    TextView tv_error_no_spaj;
    @BindView(R.id.cl_deskripsi)
    ConstraintLayout cl_deskripsi;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
}
