package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReferralResponse {

    @SerializedName("NAMA_REFF")
    @Expose
    private String nAMAREFF;
    @SerializedName("NAMA_CABANG")
    @Expose
    private String nAMACABANG;
    @SerializedName("FLAG_LISENSI")
    @Expose
    private Integer fLAGLISENSI;
    @SerializedName("LCB_NO")
    @Expose
    private String lCBNO;
    @SerializedName("AGENT_CODE")
    @Expose
    private String aGENTCODE;

    public String getNAMAREFF() {
        return nAMAREFF;
    }

    public void setNAMAREFF(String nAMAREFF) {
        this.nAMAREFF = nAMAREFF;
    }

    public String getNAMACABANG() {
        return nAMACABANG;
    }

    public void setNAMACABANG(String nAMACABANG) {
        this.nAMACABANG = nAMACABANG;
    }

    public Integer getFLAGLISENSI() {
        return fLAGLISENSI;
    }

    public void setFLAGLISENSI(Integer fLAGLISENSI) {
        this.fLAGLISENSI = fLAGLISENSI;
    }

    public String getLCBNO() {
        return lCBNO;
    }

    public void setLCBNO(String lCBNO) {
        this.lCBNO = lCBNO;
    }

    public String getAGENTCODE() {
        return aGENTCODE;
    }

    public void setAGENTCODE(String aGENTCODE) {
        this.aGENTCODE = aGENTCODE;
    }

}
