package id.co.ajsmsig.nb.pointofsale

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import id.co.ajsmsig.nb.pointofsale.api.APIService
import id.co.ajsmsig.nb.databinding.PosActivityPosBinding
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.utils.Constants
import id.co.ajsmsig.nb.pointofsale.ui.common.NavigationController
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.MainVM
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.PageViewModelFactory
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import javax.inject.Inject
import android.R.attr.bitmap
import android.graphics.Bitmap
import android.provider.MediaStore
import id.co.ajsmsig.nb.R


class POSActivity : AppCompatActivity(), HasSupportFragmentInjector {


    @Inject lateinit var navigationController: NavigationController
    @Inject lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject lateinit var viewModelFactory: PageViewModelFactory
    @Inject lateinit var sharedViewModel: SharedViewModel
    @Inject lateinit var mainRepository: MainRepository
    @Inject lateinit var appExecutors: AppExecutors
    @Inject lateinit var apiService: APIService
    @Inject lateinit var prePopulatedRepository: PrePopulatedRepository


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainVM::class.java)

        val dataBinding = DataBindingUtil.setContentView<PosActivityPosBinding>(this, R.layout.pos_activity_pos)
        dataBinding.vm = viewModel

        supportFragmentManager.addOnBackStackChangedListener {
            val backstackCount = supportFragmentManager.backStackEntryCount
            viewModel.isHome.set(backstackCount == 1)
        }

//        Toast.makeText(this, DisplayUtil.screenSize, Toast.LENGTH_SHORT).show()



        if (intent != null && intent.extras != null && intent.extras?.getString(Constants.SL_NAME) != null) {

            // Get data sent by AutoSales App
            val leadID = intent.extras.getLong(Constants.SL_TAB_ID, 1)
            val leadName = intent.extras.getString(Constants.SL_NAME, "Andrey Yoshua")
            val leadGender = intent.extras.getInt(Constants.SL_GENDER, 1)
            val agentBankId = intent.extras.getInt(Constants.GROUP_ID, 38)
            val agentCode = intent.extras.getString(Constants.AGENT_CODE, "Andrey")
            val agentName = intent.extras.getString(Constants.AGENT_NAME, "Andrey Yoshua")

            sharedViewModel.leadId = leadID
            sharedViewModel.leadName = leadName
            sharedViewModel.leadGender = leadGender
            sharedViewModel.groupId = agentBankId
            sharedViewModel.agentCode = agentCode
            sharedViewModel.agentName = agentName
        }

       /* sharedViewModel.leadName = "Andrey Yoshua"
        sharedViewModel.leadAge = 17
        sharedViewModel.leadId = 23
        sharedViewModel.groupId = 38*/


        // GROUP ID SEMENTARA UNTUK BUKOPIN SYARIAH


        sharedViewModel.getUserRecord {


            if (sharedViewModel.lastSavedPages?.isNotEmpty() == true) {

                navigationController.removeAllPage()
                for (page in sharedViewModel.lastSavedPages!!) {
                    navigationController.goToPageId(page.id)
                }
            }

        }

        navigationController.goToHome()

    }

    fun back(view: View) {
        navigationController.back()
    }

    fun home(view: View) {
        navigationController.home()
    }

    fun next(view: View) {
        navigationController.next()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_pos, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.handling_objection -> {
                navigationController.goToHandlingObjectionList()
            }
            R.id.autosales -> {
                finish()
            }
            R.id.exit -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1){
            finish()
            return
        }

        super.onBackPressed()
    }

    fun compileBackIntent(): Intent {

        val intent = Intent()
        val parentIds = ArrayList<Int?>()
        val answerIds = ArrayList<Int?>()
        val values = ArrayList<Int?>()
        sharedViewModel.riskProfileQuestionnaireAnswers?.forEach {
            parentIds.add(it.parentId)
            answerIds.add(it.id)
            values.add(it.value)
        }

        Log.i("INFO", "$parentIds\n$answerIds\n$values")
        intent.putIntegerArrayListExtra("question_id", parentIds)
        intent.putIntegerArrayListExtra("answer_id", answerIds)
        intent.putIntegerArrayListExtra("value", values)
        intent.putExtra(Constants.SL_TAB_ID, sharedViewModel.leadId)
        intent.putExtra(Constants.LSBS_ID, sharedViewModel.selectedProduct?.LsbsId)

        return  intent
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return dispatchingAndroidInjector
    }

    override fun onPause() {
        super.onPause()

        appExecutors.diskIO().execute { sharedViewModel.save() }
    }

}
