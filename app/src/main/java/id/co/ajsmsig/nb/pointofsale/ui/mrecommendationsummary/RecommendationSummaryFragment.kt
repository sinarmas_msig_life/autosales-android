package id.co.ajsmsig.nb.pointofsale.ui.mrecommendationsummary


import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ScrollView
import android.widget.Toast
import id.co.ajsmsig.nb.pointofsale.AppExecutors
import id.co.ajsmsig.nb.pointofsale.POSActivity
import id.co.ajsmsig.nb.R

import id.co.ajsmsig.nb.databinding.PosFragmentRecommendationSummaryBinding
import id.co.ajsmsig.nb.pointofsale.ui.fragment.BaseFragment
import id.co.ajsmsig.nb.pointofsale.utils.isExternalStorageWritable
import id.co.ajsmsig.nb.pointofsale.utils.share
import id.co.ajsmsig.nb.pointofsale.utils.*
import kotlinx.android.synthetic.main.pos_fragment_recommendation_summary.*
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class RecommendationSummaryFragment : BaseFragment() {


    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var dataBinding: PosFragmentRecommendationSummaryBinding
    lateinit var viewModel: RecommendationSummaryVM

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private val PERMISSIONS_STORAGE = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
    private val REQUEST_STORAGE: Int = 1



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RecommendationSummaryVM::class.java)
        subscribeUI(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_recommendation_summary, container, false)

        return dataBinding.root
    }


    fun subscribeUI(viewModel: RecommendationSummaryVM) {
        dataBinding.vm = viewModel
        dataBinding.sharedViewModel = sharedViewModel
        dataBinding.fragment = this

        viewModel.page = page

        mainVM.subtitle.set(sharedViewModel.selectedNeedAnalysis?.title)
    }

    /**
     * Requests the Contacts permissions.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    private fun requestStoragePermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example, if the request has been denied previously.
            Log.i("INFO",
                    "Displaying contacts permission rationale to provide additional context.")

            // Display a SnackBar with an explanation and a button to trigger the request.

            Snackbar.make(dataBinding.root, R.string.permission_external_storage,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, {
                        ActivityCompat.requestPermissions(activity!!, PERMISSIONS_STORAGE, REQUEST_STORAGE)
                    })
                    .show()
        } else {
            ActivityCompat.requestPermissions(activity!!, PERMISSIONS_STORAGE, REQUEST_STORAGE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_STORAGE) {

            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                generatePdf()
            }
        }
    }

    fun generatePdf(view: View? = null) {


        // Verify that all required contact permissions have been granted.
        if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Contacts permissions have not been granted.
            Log.i("INFO", "Storage permissions has NOT been granted. Requesting permissions.")
            requestStoragePermissions()

        } else {
            handlingObjectionButton?.visibility = View.GONE
            downloadButton.visibility = View.GONE
            eProposalButton.visibility = View.GONE
            createPdf()
            handlingObjectionButton?.visibility = View.VISIBLE
            downloadButton?.visibility = View.VISIBLE
            eProposalButton?.visibility = View.VISIBLE

            sharedViewModel.userRecord?.complete = true

            viewModel.sendFinishCycleEvent(sharedViewModel.startFromLifeStage)
        }
    }

    private fun createPdf() {

        if (!isExternalStorageWritable()) {
            Toast.makeText(activity, "Your storage is not available", Toast.LENGTH_LONG).show()
            return
        }


        val bitmap = getBitmapScreenshotOfThis()

        val filePath = PdfUtil.createPdfFileFromBitmap(bitmap, "${sharedViewModel.leadName?.replace(" ", "")}")
        try {
            share(filePath)
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }


    private fun getBitmapScreenshotOfThis(): Bitmap {
        val top = activity!!.findViewById<FrameLayout>(R.id.header)
        val bottom = dataBinding.root
        val bottomHeight = (bottom as ScrollView).getChildAt(0).height

        val bitmap = Bitmap.createBitmap(top.width, top.height + bottomHeight,
                Bitmap.Config.ARGB_8888)
        val bigCanvas = Canvas(bitmap)

        val paint = Paint()

        top.isDrawingCacheEnabled = true
        top.buildDrawingCache(true)
        top.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
        bigCanvas.drawBitmap(top.drawingCache, 0f, 0f, paint)
        top.isDrawingCacheEnabled = false
        top.destroyDrawingCache()


        val bottomBitmap = Bitmap.createBitmap(bottom.width, bottomHeight, Bitmap.Config.ARGB_8888)

        val bottomCanvas = Canvas(bottomBitmap)
        bottom.draw(bottomCanvas)

        bigCanvas.drawBitmap(bottomBitmap, 0f, top.height.toFloat(), paint)

        return bitmap
    }

    fun goToHandlingObjection() {
        val task = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<Unit, Unit, Unit>() {
            override fun doInBackground(vararg params: Unit?) {
                if (isUserNotCreated()) sharedViewModel.save()
            }

            override fun onPostExecute(result: Unit?) {
                super.onPostExecute(result)

                val hasGeneratePdfAndSaveToContinue = showAlertAboutSavingPDF { if (it) { generatePdf() } }
                if (hasGeneratePdfAndSaveToContinue) {
                    navigationController.goToHandlingObjectionList()
                }
            }
        }
        task.execute()
    }

    fun goToEProposal() {
        val task = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<Unit, Unit, Unit>() {
            override fun doInBackground(vararg params: Unit?) {
                if (isUserNotCreated()) sharedViewModel.save()
            }

            override fun onPostExecute(result: Unit?) {
                super.onPostExecute(result)

                val hasGeneratePdfAndSaveToContinue = showAlertAboutSavingPDF { if (it) { generatePdf() } }
                if (hasGeneratePdfAndSaveToContinue) {
                    activity?.setResult(RESULT_OK, (activity as POSActivity).compileBackIntent())
                    activity?.finish()
                }
            }
        }
        task.execute()
    }

    private fun isUserNotCreated(): Boolean {
        return sharedViewModel.userRecord == null
    }

    private fun showAlertAboutSavingPDF(listener: (Boolean) -> Unit): Boolean {
        if (sharedViewModel.userRecord?.complete == false) {
            AlertDialog.Builder(activity).setTitle(R.string.finish_flow_message)
                    .setPositiveButton(R.string.simpan, { dialog, which ->
                        listener(true)
                    })
                    .setNegativeButton(R.string.tidak_simpan, { dialog, which ->
                        listener(false)
                    }).create().show()
        }
        return sharedViewModel.userRecord?.complete == true
    }

    override fun onPause() {
        super.onPause()
        sharedViewModel.userRecord?.complete = sharedViewModel.userRecord?.complete == true
    }

}// Required empty public constructor
