package id.co.ajsmsig.nb.pointofsale.utils

import android.util.Log

/**
 * Created by andreyyoshuamanik on 21/03/18.
 */
class Calculator(private val mathEval: MathEval, private val formula: String? = "") {

    fun calculate(): MathEval {
        val formulas = formula?.split(";")
        formulas?.forEach {
            val formulaAndResultVariableName = it.replace("\\s+","").replace(" ", "").replace("\n", "")
            if (formulaAndResultVariableName == "") return@forEach
            val resultVariableName = formulaAndResultVariableName.split("=").first()
            val formula = formulaAndResultVariableName.split("=").last()

            val tempResult = mathEval.evaluate(formula)
            Log.i("INFO", "$formulaAndResultVariableName\n$resultVariableName = $tempResult")

            mathEval.setVariable(resultVariableName, tempResult)
        }
        return mathEval
    }
}