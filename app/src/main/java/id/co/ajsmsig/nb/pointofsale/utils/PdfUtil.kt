package id.co.ajsmsig.nb.pointofsale.utils

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.pdf.PdfDocument
import android.os.Build
import android.os.Environment
import android.util.Log
import java.io.File
import java.io.FileOutputStream

/**
 * Created by andreyyoshuamanik on 10/03/18.
 */

object PdfUtil {

    @SuppressLint("NewApi")
    @Throws(Throwable::class)
    fun createPdfFileFromBitmap(bitmap: Bitmap, fileName: String): File? {

        try {

            // create a new document
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

                val dirFile = getPublicPdfFolder("saved_pdf")
                val targetPdf = "$fileName.pdf"
                val filePath = File(dirFile?.path, targetPdf)
                val document = PdfDocument()

                // create a page description
                val pageInfo = PdfDocument.PageInfo.Builder(bitmap.width, bitmap.height, 1).create()

                // start a page
                val page = document.startPage(pageInfo)

                page.canvas.drawBitmap(bitmap, 0.toFloat(), 0.toFloat(), null)
                document.finishPage(page)

                try {
                    document.writeTo(FileOutputStream(filePath))

                } catch (e: Throwable) {
                    e.printStackTrace()
                    throw e
                }

                document.close()
                return filePath
            } else {
                val dirFile = getPublicPdfFolder("saved_pdf")
                val targetPdf = "$fileName.png"
                val filePath = File(dirFile?.path, targetPdf)

                val outputStream = FileOutputStream(filePath)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
                outputStream.flush()
                outputStream.close()
                return filePath
            }


        } catch (ex: Exception) {
            return null
        }
    }

    private fun getPublicPdfFolder(pdfFolder: String): File? {
        // Get the directory for the user's public pictures directory.
//        val docsFolder = File(Environment.getExternalStorageDirectory(), "/Downloads");
//        var isPresent = true;
//        if (!docsFolder.exists()) {
//            isPresent = docsFolder.mkdir();
//        }
//        if (isPresent) {
//            val file = File(docsFolder.getAbsolutePath(), pdfFolder);
//            return file
//        } else {
//            // Failure
//            return null
//        }

        val file = File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), pdfFolder)
        if (!file.mkdirs()) {
            Log.e("INFO", "Directory not created")
        }

        if (!file.exists()) return null
        return file
    }


}