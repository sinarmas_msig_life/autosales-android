package id.co.ajsmsig.nb.pointofsale.api

import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Analytics
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Product
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductBenefit
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductRider
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by andreyyoshuamanik on 06/03/18.
 */
interface APIService {

    @GET("DProduct")
    fun getProductsWithGroupId(@Query("groupId") groupId: String): LiveData<ApiResponse<List<Product>>>

    @GET("DProduct")
    fun getProductsWithGroupId(@Query("groupId") groupId: String, @Query("LsbsId") lsbsId: String): LiveData<ApiResponse<List<Product>>>

    @GET("DRider")
    fun getRidersWith(@Query("groupId") groupId: String, @Query("LsbsId") lsbsId: String): LiveData<ApiResponse<List<ProductRider>>>

    @GET("DBenefit")
    fun getBenefitsWith(@Query("groupId") groupId: String, @Query("LsbsId") lsbsId: String): LiveData<ApiResponse<List<ProductBenefit>>>

    @POST("DAnalytics")
    fun sendAnalytics(@Body data: List<Analytics>): LiveData<ApiResponse<String>>
}