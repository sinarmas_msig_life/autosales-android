package id.co.ajsmsig.nb.leader.statusspajdetail;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.DateUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StatusSpajDetailActivity extends AppCompatActivity {

    private ArrayList<Status> dataListSpaj = new ArrayList<>();
    private StatusSpajDetailAdapter mAdapter;
    private String spajNo;

    @BindView(R.id.toolbarStatusSpaj)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewDetail)
    RecyclerView recyclerView;

    @BindView(R.id.layoutNoInternetConnection)
    ConstraintLayout layoutNoInternetConnection;

    @BindView(R.id.progressBarDetail)
    ProgressBar progressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_detail);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        String spajNo = intent.getStringExtra(Const.INTENT_KEY_SPAJ_NO_TEMP);
        initToolbar(spajNo);

        if (StaticMethods.isNetworkAvailable(this)) {
            hideNoInternetConnection();
            initRecyclerView();
            getSpajStatus(spajNo);
        } else {
            showNoInternetConnection();
            Toast.makeText(StatusSpajDetailActivity.this,"Anda tidak terkoneksi ke internet. Mohon pastikan Anda terkoneksi dengan internet",Toast.LENGTH_SHORT).show();
        }
    }

    private void showNoInternetConnection() {
        layoutNoInternetConnection.setVisibility(View.VISIBLE);
    }

    private void hideNoInternetConnection(){
        layoutNoInternetConnection.setVisibility(View.GONE);
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoading(){
        progressBar.setVisibility(View.GONE);
    }

    private void showRecyclerView(){
        recyclerView.setVisibility(View.VISIBLE);

    }

    private void hideRecyclerView(){
        recyclerView.setVisibility(View.GONE);
    }

    private void initToolbar(String toolbarTitle) {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            toolbarTitle = "Status SPAJ " + toolbarTitle;
            getSupportActionBar().setTitle(toolbarTitle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                onBackPressed();
            });
        }
    }

    private void initRecyclerView() {
        mAdapter = new StatusSpajDetailAdapter(dataListSpaj);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    private void getSpajStatus(String spajNo) {
        showLoading();
        String url = AppConfig.getBaseUrlSPAJ().concat("spaj/api/json/trackspajstatus").concat("/").concat(spajNo);
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<GetSpajDetailResponse> call = api.getDetailSpaj(url);
        call.enqueue(new Callback<GetSpajDetailResponse>() {
            @Override
            public void onResponse(Call<GetSpajDetailResponse> call, Response<GetSpajDetailResponse> response) {
                GetSpajDetailResponse responsBody = response.body();

                if (responsBody != null) {

                    boolean error = responsBody.isError();
                    String message = responsBody.getMessage();
                    List<Status> data = responsBody.getData().getStatus();

                    if (!error) {
                        displaySpajStatus(data);
                    } else {
                        showErrorRetrieveSpajStatus(message);
                    }

                }
                hideLoading();
                showRecyclerView();
            }

            @Override
            public void onFailure(Call<GetSpajDetailResponse> call, Throwable t) {
                hideLoading();
                showNoInternetConnection();
            }
        });
    }

    @OnClick(R.id.btnRetry)
    void onBtnRetryPressed() {

        Intent intent = getIntent();
        String spajNo = intent.getStringExtra(Const.INTENT_KEY_SPAJ_NO_TEMP);

        if (StaticMethods.isNetworkAvailable(StatusSpajDetailActivity.this)) {
            initRecyclerView();
            getSpajStatus(spajNo);
            hideNoInternetConnection();

        } else {
            showNoInternetConnection();
            Toast.makeText(StatusSpajDetailActivity.this, "Anda tidak terkoneksi ke internet. Mohon pastikan Anda terkoneksi dengan internet", Toast.LENGTH_SHORT).show();
        }

    }

    private void displaySpajStatus(List<Status> statuses) {
        if (statuses.size() > 0) {

            for (Status status : statuses) {

                status.setMspsDate(DateUtils.getSPAJProcessDate(status.getMspsDate()));

                int lssaId = status.getLssaId();
                if (lssaId == 2 || lssaId == 3 || lssaId == 4 || lssaId == 9 || lssaId == 10 || lssaId == 15 || lssaId == 18) {
                    status.setPending(true);
                }
                int lsspId = status.getLsspId();
                if (lsspId == 1) {
                    status.setInforce(true);
                } else if (lsspId == 10){
                    status.setInforce(false);
                } else {
                    status.setInforce(false);
                }

            }

            mAdapter.refreshSpajDetail(new ArrayList<>(statuses));
        }

    }

    private void showErrorRetrieveSpajStatus(String message)

    {
        new AlertDialog.Builder(this)
                .setTitle("Tidak dapat menampilkan status spaj")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                .show();
    }
}
