package id.co.ajsmsig.nb.crm.model.form;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by faiz_f on 11/04/2017.
 */

public class SimbakLeadModel implements Parcelable {
    private Long SL_TAB_ID;
    private Long SL_ID;
    private String SL_NAME;
    private String SL_CREATED;
    private String SL_CRTD_DATE;
    private String SL_UPDATED;
    private String SL_UPTD_DATE;
    private Integer SL_RATE;
    private String SL_REMARK;
    private Integer SL_INIT;
    private String SL_MOTHER;
    private Integer SL_CITIZEN;
    private String SL_BPLACE;
    private String SL_IDNO;
    private String SL_BDATE;
    private Integer SL_UMUR;
    private Integer SL_RES_PREMIUM;
    private String SL_BAC_REFF;
    private String SL_BAC_REFF_NAME;
    private String SL_PEND_ID;
    private String SL_APPROVE;
    private String SL_REQUEST;
    private String SL_ALASAN;
    private String SL_NO_NASABAH;
    private String SL_APPROVER;
    private String SL_BAC_CURR_BRANCH;
    private String SL_ROOMOUT;
    private String SL_ROOMIN;
    private String SL_OCCUPATION;
    private String SL_IDEXP;
    private String SL_NPWP;
    private String SL_OWNERID;
    private String SL_CIF;
    private String SL_PINBB;
    private String SL_OLD_ID;
    private Integer SL_APP;
    private Integer SL_ACTIVE;
    private Integer SL_SRC;
    private Integer SL_CHAR;
    private Integer SL_STATE;
    private Integer SL_CORP_POS;
    private Integer SL_GENDER;
    private Integer SL_RELIGION;
    private Integer SL_PERSONAL_REL;
    private Integer SL_LAST_EDU;
    private Integer SL_POLIS_PURPOSE;
    private Integer SL_PAYMENT_SOURCE;
    private Integer SL_OTHER_TYPE;
    private Integer SL_CSTMR_FUND_TYPE;
    private Integer SL_IDTYPE;
    private Integer SL_SALARY;
    private Integer SL_BUYER_FUND_TYPE;
    private String SL_BAC_FIRST_BRANCH;
    private Integer SL_MALL_GUESTCODE;
    private Integer SL_MALL_MALLCODE;
    private Integer SL_MALL_ROOMCODE;
    private Integer SL_CORRESPONDENT;
    private Integer SL_BAC_REFF_TITLE;
    private Integer SL_MARRIAGE;
    private Integer SL_OWNERTYPE;
    private Integer SL_TYPE;
    private Integer SL_LAST_POS;
    private Integer SL_FAV;
    private String NAMA_CABANG;
    private String UTL_USER_ID;
    private SimbakAccountModel account;
    private SimbakAddressModel home;
    private SimbakAddressModel office;
    private SimbakAddressModel other;
    private ArrayList<SimbakActivityModel> LIST_ACTIVITY = null;



    public SimbakLeadModel() {

    }

    protected SimbakLeadModel(Parcel in) {
        SL_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SL_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SL_NAME = in.readString();
        SL_CREATED = in.readString();
        SL_CRTD_DATE = in.readString();
        SL_UPDATED = in.readString();
        SL_UPTD_DATE = in.readString();
        SL_RATE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_REMARK = in.readString();
        SL_INIT = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_MOTHER = in.readString();
        SL_CITIZEN = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_BPLACE = in.readString();
        SL_IDNO = in.readString();
        SL_BDATE = in.readString();
        SL_UMUR = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_RES_PREMIUM = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_BAC_REFF = in.readString();
        SL_BAC_REFF_NAME = in.readString();
        SL_PEND_ID = in.readString();
        SL_APPROVE = in.readString();
        SL_REQUEST = in.readString();
        SL_ALASAN = in.readString();
        SL_NO_NASABAH = in.readString();
        SL_APPROVER = in.readString();
        SL_BAC_CURR_BRANCH = in.readString();
        SL_ROOMOUT = in.readString();
        SL_ROOMIN = in.readString();
        SL_OCCUPATION = in.readString();
        SL_IDEXP = in.readString();
        SL_NPWP = in.readString();
        SL_OWNERID = in.readString();
        SL_CIF = in.readString();
        SL_PINBB = in.readString();
        SL_OLD_ID = in.readString();
        SL_APP = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_ACTIVE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_SRC = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_CHAR = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_STATE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_CORP_POS = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_GENDER = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_RELIGION = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_PERSONAL_REL = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_LAST_EDU = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_POLIS_PURPOSE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_PAYMENT_SOURCE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_OTHER_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_CSTMR_FUND_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_IDTYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_SALARY = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_BUYER_FUND_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_BAC_FIRST_BRANCH = in.readString();
        SL_MALL_GUESTCODE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_MALL_MALLCODE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_MALL_ROOMCODE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_CORRESPONDENT = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_BAC_REFF_TITLE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_MARRIAGE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_OWNERTYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_LAST_POS = (Integer) in.readValue(Integer.class.getClassLoader());
        SL_FAV = (Integer) in.readValue(Integer.class.getClassLoader());
        account = in.readParcelable(SimbakAccountModel.class.getClassLoader());
        home = in.readParcelable(SimbakAddressModel.class.getClassLoader());
        office = in.readParcelable(SimbakAddressModel.class.getClassLoader());
        other = in.readParcelable(SimbakAddressModel.class.getClassLoader());
        LIST_ACTIVITY = in.createTypedArrayList(SimbakActivityModel.CREATOR);
        NAMA_CABANG = in.readString();
        UTL_USER_ID = in.readString();
    }

    public static final Creator<SimbakLeadModel> CREATOR = new Creator<SimbakLeadModel>() {
        @Override
        public SimbakLeadModel createFromParcel(Parcel in) {
            return new SimbakLeadModel(in);
        }

        @Override
        public SimbakLeadModel[] newArray(int size) {
            return new SimbakLeadModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(SL_TAB_ID);
        dest.writeValue(SL_ID);
        dest.writeString(SL_NAME);
        dest.writeString(SL_CREATED);
        dest.writeString(SL_CRTD_DATE);
        dest.writeString(SL_UPDATED);
        dest.writeString(SL_UPTD_DATE);
        dest.writeValue(SL_RATE);
        dest.writeString(SL_REMARK);
        dest.writeValue(SL_INIT);
        dest.writeString(SL_MOTHER);
        dest.writeValue(SL_CITIZEN);
        dest.writeString(SL_BPLACE);
        dest.writeString(SL_IDNO);
        dest.writeString(SL_BDATE);
        dest.writeValue(SL_UMUR);
        dest.writeValue(SL_RES_PREMIUM);
        dest.writeString(SL_BAC_REFF);
        dest.writeString(SL_BAC_REFF_NAME);
        dest.writeString(SL_PEND_ID);
        dest.writeString(SL_APPROVE);
        dest.writeString(SL_REQUEST);
        dest.writeString(SL_ALASAN);
        dest.writeString(SL_NO_NASABAH);
        dest.writeString(SL_APPROVER);
        dest.writeString(SL_BAC_CURR_BRANCH);
        dest.writeString(SL_ROOMOUT);
        dest.writeString(SL_ROOMIN);
        dest.writeString(SL_OCCUPATION);
        dest.writeString(SL_IDEXP);
        dest.writeString(SL_NPWP);
        dest.writeString(SL_OWNERID);
        dest.writeString(SL_CIF);
        dest.writeString(SL_PINBB);
        dest.writeString(SL_OLD_ID);
        dest.writeValue(SL_APP);
        dest.writeValue(SL_ACTIVE);
        dest.writeValue(SL_SRC);
        dest.writeValue(SL_CHAR);
        dest.writeValue(SL_STATE);
        dest.writeValue(SL_CORP_POS);
        dest.writeValue(SL_GENDER);
        dest.writeValue(SL_RELIGION);
        dest.writeValue(SL_PERSONAL_REL);
        dest.writeValue(SL_LAST_EDU);
        dest.writeValue(SL_POLIS_PURPOSE);
        dest.writeValue(SL_PAYMENT_SOURCE);
        dest.writeValue(SL_OTHER_TYPE);
        dest.writeValue(SL_CSTMR_FUND_TYPE);
        dest.writeValue(SL_IDTYPE);
        dest.writeValue(SL_SALARY);
        dest.writeValue(SL_BUYER_FUND_TYPE);
        dest.writeString(SL_BAC_FIRST_BRANCH);
        dest.writeValue(SL_MALL_GUESTCODE);
        dest.writeValue(SL_MALL_MALLCODE);
        dest.writeValue(SL_MALL_ROOMCODE);
        dest.writeValue(SL_CORRESPONDENT);
        dest.writeValue(SL_BAC_REFF_TITLE);
        dest.writeValue(SL_MARRIAGE);
        dest.writeValue(SL_OWNERTYPE);
        dest.writeValue(SL_TYPE);
        dest.writeValue(SL_LAST_POS);
        dest.writeValue(SL_FAV);
        dest.writeParcelable(account, flags);
        dest.writeParcelable(home, flags);
        dest.writeParcelable(office, flags);
        dest.writeParcelable(other, flags);
        dest.writeTypedList(LIST_ACTIVITY);
        dest.writeString(NAMA_CABANG);
        dest.writeString(UTL_USER_ID);
    }

    public Long getSL_TAB_ID() {
        return SL_TAB_ID;
    }

    public void setSL_TAB_ID(Long SL_TAB_ID) {
        this.SL_TAB_ID = SL_TAB_ID;
    }

    public Long getSL_ID() {
        return SL_ID;
    }

    public void setSL_ID(Long SL_ID) {
        this.SL_ID = SL_ID;
    }

    public String getSL_NAME() {
        return SL_NAME;
    }

    public void setSL_NAME(String SL_NAME) {
        this.SL_NAME = SL_NAME;
    }

    public String getSL_CREATED() {
        return SL_CREATED;
    }

    public void setSL_CREATED(String SL_CREATED) {
        this.SL_CREATED = SL_CREATED;
    }

    public String getSL_CRTD_DATE() {
        return SL_CRTD_DATE;
    }

    public void setSL_CRTD_DATE(String SL_CRTD_DATE) {
        this.SL_CRTD_DATE = SL_CRTD_DATE;
    }

    public String getSL_UPDATED() {
        return SL_UPDATED;
    }

    public void setSL_UPDATED(String SL_UPDATED) {
        this.SL_UPDATED = SL_UPDATED;
    }

    public String getSL_UPTD_DATE() {
        return SL_UPTD_DATE;
    }

    public void setSL_UPTD_DATE(String SL_UPTD_DATE) {
        this.SL_UPTD_DATE = SL_UPTD_DATE;
    }

    public Integer getSL_RATE() {
        return SL_RATE;
    }

    public void setSL_RATE(Integer SL_RATE) {
        this.SL_RATE = SL_RATE;
    }

    public String getSL_REMARK() {
        return SL_REMARK;
    }

    public void setSL_REMARK(String SL_REMARK) {
        this.SL_REMARK = SL_REMARK;
    }

    public Integer getSL_INIT() {
        return SL_INIT;
    }

    public void setSL_INIT(Integer SL_INIT) {
        this.SL_INIT = SL_INIT;
    }

    public String getSL_MOTHER() {
        return SL_MOTHER;
    }

    public void setSL_MOTHER(String SL_MOTHER) {
        this.SL_MOTHER = SL_MOTHER;
    }

    public Integer getSL_CITIZEN() {
        return SL_CITIZEN;
    }

    public void setSL_CITIZEN(Integer SL_CITIZEN) {
        this.SL_CITIZEN = SL_CITIZEN;
    }

    public String getSL_BPLACE() {
        return SL_BPLACE;
    }

    public void setSL_BPLACE(String SL_BPLACE) {
        this.SL_BPLACE = SL_BPLACE;
    }

    public String getSL_IDNO() {
        return SL_IDNO;
    }

    public void setSL_IDNO(String SL_IDNO) {
        this.SL_IDNO = SL_IDNO;
    }

    public String getSL_BDATE() {
        return SL_BDATE;
    }

    public void setSL_BDATE(String SL_BDATE) {
        this.SL_BDATE = SL_BDATE;
    }

    public Integer getSL_UMUR() {
        return SL_UMUR;
    }

    public void setSL_UMUR(Integer SL_UMUR) {
        this.SL_UMUR = SL_UMUR;
    }

    public Integer getSL_RES_PREMIUM() {
        return SL_RES_PREMIUM;
    }

    public void setSL_RES_PREMIUM(Integer SL_RES_PREMIUM) {
        this.SL_RES_PREMIUM = SL_RES_PREMIUM;
    }

    public String getSL_BAC_REFF() {
        return SL_BAC_REFF;
    }

    public void setSL_BAC_REFF(String SL_BAC_REFF) {
        this.SL_BAC_REFF = SL_BAC_REFF;
    }

    public String getSL_BAC_REFF_NAME() {
        return SL_BAC_REFF_NAME;
    }

    public void setSL_BAC_REFF_NAME(String SL_BAC_REFF_NAME) {
        this.SL_BAC_REFF_NAME = SL_BAC_REFF_NAME;
    }

    public String getSL_PEND_ID() {
        return SL_PEND_ID;
    }

    public void setSL_PEND_ID(String SL_PEND_ID) {
        this.SL_PEND_ID = SL_PEND_ID;
    }

    public String getSL_APPROVE() {
        return SL_APPROVE;
    }

    public void setSL_APPROVE(String SL_APPROVE) {
        this.SL_APPROVE = SL_APPROVE;
    }

    public String getSL_REQUEST() {
        return SL_REQUEST;
    }

    public void setSL_REQUEST(String SL_REQUEST) {
        this.SL_REQUEST = SL_REQUEST;
    }

    public String getSL_ALASAN() {
        return SL_ALASAN;
    }

    public void setSL_ALASAN(String SL_ALASAN) {
        this.SL_ALASAN = SL_ALASAN;
    }

    public String getSL_NO_NASABAH() {
        return SL_NO_NASABAH;
    }

    public void setSL_NO_NASABAH(String SL_NO_NASABAH) {
        this.SL_NO_NASABAH = SL_NO_NASABAH;
    }

    public String getSL_APPROVER() {
        return SL_APPROVER;
    }

    public void setSL_APPROVER(String SL_APPROVER) {
        this.SL_APPROVER = SL_APPROVER;
    }

    public String getSL_BAC_CURR_BRANCH() {
        return SL_BAC_CURR_BRANCH;
    }

    public void setSL_BAC_CURR_BRANCH(String SL_BAC_CURR_BRANCH) {
        this.SL_BAC_CURR_BRANCH = SL_BAC_CURR_BRANCH;
    }

    public String getSL_ROOMOUT() {
        return SL_ROOMOUT;
    }

    public void setSL_ROOMOUT(String SL_ROOMOUT) {
        this.SL_ROOMOUT = SL_ROOMOUT;
    }

    public String getSL_ROOMIN() {
        return SL_ROOMIN;
    }

    public void setSL_ROOMIN(String SL_ROOMIN) {
        this.SL_ROOMIN = SL_ROOMIN;
    }

    public String getSL_OCCUPATION() {
        return SL_OCCUPATION;
    }

    public void setSL_OCCUPATION(String SL_OCCUPATION) {
        this.SL_OCCUPATION = SL_OCCUPATION;
    }

    public String getSL_IDEXP() {
        return SL_IDEXP;
    }

    public void setSL_IDEXP(String SL_IDEXP) {
        this.SL_IDEXP = SL_IDEXP;
    }

    public String getSL_NPWP() {
        return SL_NPWP;
    }

    public void setSL_NPWP(String SL_NPWP) {
        this.SL_NPWP = SL_NPWP;
    }

    public String getSL_OWNERID() {
        return SL_OWNERID;
    }

    public void setSL_OWNERID(String SL_OWNERID) {
        this.SL_OWNERID = SL_OWNERID;
    }

    public String getSL_CIF() {
        return SL_CIF;
    }

    public void setSL_CIF(String SL_CIF) {
        this.SL_CIF = SL_CIF;
    }

    public String getSL_PINBB() {
        return SL_PINBB;
    }

    public void setSL_PINBB(String SL_PINBB) {
        this.SL_PINBB = SL_PINBB;
    }

    public String getSL_OLD_ID() {
        return SL_OLD_ID;
    }

    public void setSL_OLD_ID(String SL_OLD_ID) {
        this.SL_OLD_ID = SL_OLD_ID;
    }

    public Integer getSL_APP() {
        return SL_APP;
    }

    public void setSL_APP(Integer SL_APP) {
        this.SL_APP = SL_APP;
    }

    public Integer getSL_ACTIVE() {
        return SL_ACTIVE;
    }

    public void setSL_ACTIVE(Integer SL_ACTIVE) {
        this.SL_ACTIVE = SL_ACTIVE;
    }

    public Integer getSL_SRC() {
        return SL_SRC;
    }

    public void setSL_SRC(Integer SL_SRC) {
        this.SL_SRC = SL_SRC;
    }

    public Integer getSL_CHAR() {
        return SL_CHAR;
    }

    public void setSL_CHAR(Integer SL_CHAR) {
        this.SL_CHAR = SL_CHAR;
    }

    public Integer getSL_STATE() {
        return SL_STATE;
    }

    public void setSL_STATE(Integer SL_STATE) {
        this.SL_STATE = SL_STATE;
    }

    public Integer getSL_CORP_POS() {
        return SL_CORP_POS;
    }

    public void setSL_CORP_POS(Integer SL_CORP_POS) {
        this.SL_CORP_POS = SL_CORP_POS;
    }

    public Integer getSL_GENDER() {
        return SL_GENDER;
    }

    public void setSL_GENDER(Integer SL_GENDER) {
        this.SL_GENDER = SL_GENDER;
    }

    public Integer getSL_RELIGION() {
        return SL_RELIGION;
    }

    public void setSL_RELIGION(Integer SL_RELIGION) {
        this.SL_RELIGION = SL_RELIGION;
    }

    public Integer getSL_PERSONAL_REL() {
        return SL_PERSONAL_REL;
    }

    public void setSL_PERSONAL_REL(Integer SL_PERSONAL_REL) {
        this.SL_PERSONAL_REL = SL_PERSONAL_REL;
    }

    public Integer getSL_LAST_EDU() {
        return SL_LAST_EDU;
    }

    public void setSL_LAST_EDU(Integer SL_LAST_EDU) {
        this.SL_LAST_EDU = SL_LAST_EDU;
    }

    public Integer getSL_POLIS_PURPOSE() {
        return SL_POLIS_PURPOSE;
    }

    public void setSL_POLIS_PURPOSE(Integer SL_POLIS_PURPOSE) {
        this.SL_POLIS_PURPOSE = SL_POLIS_PURPOSE;
    }

    public Integer getSL_PAYMENT_SOURCE() {
        return SL_PAYMENT_SOURCE;
    }

    public void setSL_PAYMENT_SOURCE(Integer SL_PAYMENT_SOURCE) {
        this.SL_PAYMENT_SOURCE = SL_PAYMENT_SOURCE;
    }

    public Integer getSL_OTHER_TYPE() {
        return SL_OTHER_TYPE;
    }

    public void setSL_OTHER_TYPE(Integer SL_OTHER_TYPE) {
        this.SL_OTHER_TYPE = SL_OTHER_TYPE;
    }

    public Integer getSL_CSTMR_FUND_TYPE() {
        return SL_CSTMR_FUND_TYPE;
    }

    public void setSL_CSTMR_FUND_TYPE(Integer SL_CSTMR_FUND_TYPE) {
        this.SL_CSTMR_FUND_TYPE = SL_CSTMR_FUND_TYPE;
    }

    public Integer getSL_IDTYPE() {
        return SL_IDTYPE;
    }

    public void setSL_IDTYPE(Integer SL_IDTYPE) {
        this.SL_IDTYPE = SL_IDTYPE;
    }

    public Integer getSL_SALARY() {
        return SL_SALARY;
    }

    public void setSL_SALARY(Integer SL_SALARY) {
        this.SL_SALARY = SL_SALARY;
    }

    public Integer getSL_BUYER_FUND_TYPE() {
        return SL_BUYER_FUND_TYPE;
    }

    public void setSL_BUYER_FUND_TYPE(Integer SL_BUYER_FUND_TYPE) {
        this.SL_BUYER_FUND_TYPE = SL_BUYER_FUND_TYPE;
    }

    public String getSL_BAC_FIRST_BRANCH() {
        return SL_BAC_FIRST_BRANCH;
    }

    public void setSL_BAC_FIRST_BRANCH(String SL_BAC_FIRST_BRANCH) {
        this.SL_BAC_FIRST_BRANCH = SL_BAC_FIRST_BRANCH;
    }

    public Integer getSL_MALL_GUESTCODE() {
        return SL_MALL_GUESTCODE;
    }

    public void setSL_MALL_GUESTCODE(Integer SL_MALL_GUESTCODE) {
        this.SL_MALL_GUESTCODE = SL_MALL_GUESTCODE;
    }

    public Integer getSL_MALL_MALLCODE() {
        return SL_MALL_MALLCODE;
    }

    public void setSL_MALL_MALLCODE(Integer SL_MALL_MALLCODE) {
        this.SL_MALL_MALLCODE = SL_MALL_MALLCODE;
    }

    public Integer getSL_MALL_ROOMCODE() {
        return SL_MALL_ROOMCODE;
    }

    public void setSL_MALL_ROOMCODE(Integer SL_MALL_ROOMCODE) {
        this.SL_MALL_ROOMCODE = SL_MALL_ROOMCODE;
    }

    public Integer getSL_CORRESPONDENT() {
        return SL_CORRESPONDENT;
    }

    public void setSL_CORRESPONDENT(Integer SL_CORRESPONDENT) {
        this.SL_CORRESPONDENT = SL_CORRESPONDENT;
    }

    public Integer getSL_BAC_REFF_TITLE() {
        return SL_BAC_REFF_TITLE;
    }

    public void setSL_BAC_REFF_TITLE(Integer SL_BAC_REFF_TITLE) {
        this.SL_BAC_REFF_TITLE = SL_BAC_REFF_TITLE;
    }

    public Integer getSL_MARRIAGE() {
        return SL_MARRIAGE;
    }

    public void setSL_MARRIAGE(Integer SL_MARRIAGE) {
        this.SL_MARRIAGE = SL_MARRIAGE;
    }

    public Integer getSL_OWNERTYPE() {
        return SL_OWNERTYPE;
    }

    public void setSL_OWNERTYPE(Integer SL_OWNERTYPE) {
        this.SL_OWNERTYPE = SL_OWNERTYPE;
    }

    public Integer getSL_TYPE() {
        return SL_TYPE;
    }

    public void setSL_TYPE(Integer SL_TYPE) {
        this.SL_TYPE = SL_TYPE;
    }

    public Integer getSL_LAST_POS() {
        return SL_LAST_POS;
    }

    public void setSL_LAST_POS(Integer SL_LAST_POS) {
        this.SL_LAST_POS = SL_LAST_POS;
    }

    public Integer getSL_FAV() {
        return SL_FAV;
    }

    public void setSL_FAV(Integer SL_FAV) {
        this.SL_FAV = SL_FAV;
    }

    public SimbakAccountModel getAccount() {
        return account;
    }

    public void setAccount(SimbakAccountModel account) {
        this.account = account;
    }

    public SimbakAddressModel getHome() {
        return home;
    }

    public void setHome(SimbakAddressModel home) {
        this.home = home;
    }

    public SimbakAddressModel getOffice() {
        return office;
    }

    public void setOffice(SimbakAddressModel office) {
        this.office = office;
    }

    public SimbakAddressModel getOther() {
        return other;
    }

    public void setOther(SimbakAddressModel other) {
        this.other = other;
    }

    public ArrayList<SimbakActivityModel> getLIST_ACTIVITY() {
        return LIST_ACTIVITY;
    }

    public String getSL_NAMA_CABANG() {
        return NAMA_CABANG;
    }

    public void setSL_NAMA_CABANG(String SL_NAMA_CABANG) {
        this.NAMA_CABANG = SL_NAMA_CABANG;
    }

    public void setLIST_ACTIVITY(ArrayList<SimbakActivityModel> LIST_ACTIVITY) {
        this.LIST_ACTIVITY = LIST_ACTIVITY;
    }

    public String getUTL_USER_ID() {
        return UTL_USER_ID;
    }

}
