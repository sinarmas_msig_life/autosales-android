package id.co.ajsmsig.nb.espaj.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.DBHelper;
import id.co.ajsmsig.nb.espaj.model.EspajModel;

public class E_Delete {
    private Context context;
    private DBHelper dbHelper;

    public E_Delete(Context context) {
        this.context = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.update_data_preferences), Context.MODE_PRIVATE);
        int version = sharedPreferences.getInt(context.getString(R.string.lav_data_id), 0);
        this.dbHelper = new DBHelper(context, version);
    }

    public void deleteAllBranchWithBranchName(String branchName){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("DELETE FROM E_LST_REFF WHERE NAMA_CABANG = '"+branchName+"'");
    }

    public void deleteListSpajDraft(String spajIdTab) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("DELETE FROM E_MST_SPAJ WHERE SPAJ_ID_TAB = '"+spajIdTab+"'");
    }

    public void deleteListDK9pp(String spajIdTab) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("DELETE FROM E_TABEL_NO9PPUQ WHERE SPAJ_ID_TAB = '"+spajIdTab+"'");
    }

    public void deleteListDK9tt(String spajIdTab) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("DELETE FROM E_TABEL_NO9TTUQ WHERE SPAJ_ID_TAB = '"+spajIdTab+"'");
    }

    public void deleteLstRef() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("DELETE FROM E_LST_REFF");
    }
}
