package id.co.ajsmsig.nb.crm.model.apiresponse.request;

public class UpdatePasswordRequest {
    private String agent_code;
    private String new_password;
    private String jenis_login;

    public UpdatePasswordRequest(String agent_code, String new_password, String jenis_login) {
        this.agent_code = agent_code;
        this.new_password = new_password;
        this.jenis_login = jenis_login;
    }


}
