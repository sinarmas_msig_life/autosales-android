package id.co.ajsmsig.nb.crm.method.async;
/*
 Created by faiz_f on 13/06/2017.
 */

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.HttpEntity;
import id.co.ajsmsig.nb.util.AppConfig;

public class EproposalRestClient {

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, int timeOut, AsyncHttpResponseHandler responseHandler) {
        client.setTimeout(timeOut);
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, int timeOut, AsyncHttpResponseHandler responseHandler) {
        client.setTimeout(timeOut);
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(Context context, String url, HttpEntity entity, String contentType, int timeOut, AsyncHttpResponseHandler responseHandler) {
        client.setTimeout(timeOut);
        client.post(context, getAbsoluteUrl(url), entity, contentType, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return AppConfig.getBaseUrlEproposal() + relativeUrl;
    }
}