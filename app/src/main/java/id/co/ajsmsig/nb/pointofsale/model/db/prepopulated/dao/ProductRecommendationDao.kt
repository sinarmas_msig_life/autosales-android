package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.ProductRecommendation


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface ProductRecommendationDao {

    @Query("select * from ProductRecommendation " +
            "where ProductRecommendation.productGroupId = :groupId and productId != '' group by productId order by `order`")
    fun loadAllProductRecommendationsWithGroupId(groupId: Int?): LiveData<List<ProductRecommendation>>

    @Query("select * from ProductRecommendation " +
            "where ProductRecommendation.productGroupId = :groupId and productId != '' and riskProfileScoringId = :riskProfileId group by productId order by `order`")
    fun loadProductRecommendationsWithGroupId(groupId: Int?, riskProfileId: Int?): LiveData<List<ProductRecommendation>>

    @Query("select * from ProductRecommendation " +
            "where ProductRecommendation.productGroupId = :groupId and productId != '' and pageOptionId = :selectedNeedId and riskProfileScoringId = :riskProfileId group by productId order by `order`")
    fun loadProductRecommendationsWithGroupId(groupId: Int?, riskProfileId: Int?, selectedNeedId: Int?): LiveData<List<ProductRecommendation>>

    @Query("select * from ProductRecommendation " +
            "where ProductRecommendation.productGroupId = :groupId " +
            "and productId != '' and pageOptionId = :selectedNeedId " +
            "and riskProfileScoringId = :riskProfileId " +
            "and (maxAge isnull or maxAge >= :age) group by productId order by `order`")
    fun loadProductRecommendationsWithGroupId(groupId: Int?, riskProfileId: Int?, selectedNeedId: Int?, age: Int?): LiveData<List<ProductRecommendation>>

    @Query("select * from ProductRecommendation " +
            "where ProductRecommendation.productGroupId = :groupId and productId != '' and pageOptionId = :selectedNeedId group by productId order by `order`")
    fun loadProductRecommendationsWithGroupIdAndSelectedNeedId(groupId: Int?, selectedNeedId: Int?): LiveData<List<ProductRecommendation>>


    // TODO
    @Query("select * from ProductRecommendation " +
            "where ProductRecommendation.productGroupId = :groupId and productId != '' and pageOptionId = :selectedPriorityNeedId group by productId order by `order`")
    fun loadProductRecommendationsWithGroupIdAndSelectedPriorityNeedId(groupId: Int?, selectedPriorityNeedId: Int?): LiveData<List<ProductRecommendation>>



}