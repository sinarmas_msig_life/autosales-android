package id.co.ajsmsig.nb.crm.database;
/*
 *Created by faiz_f on 08/05/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.HashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.form.SimbakSpajModel;

public class SimbakSpajTable {
    private Context context;

    public SimbakSpajTable(Context context) {
        this.context = context;
    }

    public HashMap<String, String> getProjectionMap(){
        HashMap<String, String> projectionMap = new HashMap<>();
        projectionMap.put(context.getString(R.string.SLS_TAB_ID),  context.getString(R.string.SLS_TAB_ID) +" AS " +  context.getString(R.string._ID));
        projectionMap.put(context.getString(R.string.SLS_ID),  context.getString(R.string.SLS_ID));
        projectionMap.put(context.getString(R.string.SLS_INST_ID),  context.getString(R.string.SLS_INST_ID));
        projectionMap.put(context.getString(R.string.SLS_INST_TAB_ID),  context.getString(R.string.SLS_INST_TAB_ID));
        projectionMap.put(context.getString(R.string.SLS_PROPOSAL),  context.getString(R.string.SLS_PROPOSAL));
        projectionMap.put(context.getString(R.string.SLS_SPAJ),  context.getString(R.string.SLS_SPAJ));
        projectionMap.put(context.getString(R.string.SLS_POLIS),  context.getString(R.string.SLS_POLIS));
        projectionMap.put(context.getString(R.string.SLS_INST_TYPE),  context.getString(R.string.SLS_INST_TYPE));
        projectionMap.put(context.getString(R.string.SLS_USED),  context.getString(R.string.SLS_USED));
        projectionMap.put(context.getString(R.string.SLS_PROP_IN),  context.getString(R.string.SLS_PROP_IN));
        projectionMap.put(context.getString(R.string.SLS_SPAJ_IN),  context.getString(R.string.SLS_SPAJ_IN));
        projectionMap.put(context.getString(R.string.SLS_SPAJ_TMP),  context.getString(R.string.SLS_SPAJ_TMP));
        projectionMap.put(context.getString(R.string.SLS_SPAJ_TMP_IN),  context.getString(R.string.SLS_SPAJ_TMP_IN));
        projectionMap.put(context.getString(R.string.SLS_CREATED),  context.getString(R.string.SLS_CREATED));
        projectionMap.put(context.getString(R.string.SLS_CRTD_ID),  context.getString(R.string.SLS_CRTD_ID));
        projectionMap.put(context.getString(R.string.SLS_UPDATED),  context.getString(R.string.SLS_UPDATED));
        projectionMap.put(context.getString(R.string.SLS_UPDTD_ID),  context.getString(R.string.SLS_UPDTD_ID));

        return projectionMap;
    }

    public ContentValues toContentValues (SimbakSpajModel simbakSpajModel){
        ContentValues contentValues = new ContentValues();
//        if (simbakSpajModel.getSLS_TAB_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_TAB_ID), simbakSpajModel.getSLS_TAB_ID());
//        }
        //        if (simbakSpajModel.getSLS_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_ID), simbakSpajModel.getSLS_ID());
//        }
        //        if (simbakSpajModel.getSLS_INST_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_INST_ID), simbakSpajModel.getSLS_INST_ID());
//        }
        //        if (simbakSpajModel.getSLS_INST_TAB_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_INST_TAB_ID), simbakSpajModel.getSLS_INST_TAB_ID());
//        }
        //        if (simbakSpajModel.getSLS_PROPOSAL() != null) {
        contentValues.put(context.getString(R.string.SLS_PROPOSAL), simbakSpajModel.getSLS_PROPOSAL());
//        }
        //        if (simbakSpajModel.getSLS_SPAJ() != null) {
        contentValues.put(context.getString(R.string.SLS_SPAJ), simbakSpajModel.getSLS_SPAJ());
//        }
//        if (simbakSpajModel.getSLS_POLIS() != null) {
        contentValues.put(context.getString(R.string.SLS_POLIS), simbakSpajModel.getSLS_POLIS());
//        }
        //        if (simbakSpajModel.getSLS_OLD_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_OLD_ID), simbakSpajModel.getSLS_OLD_ID());
//        }
//        if (simbakSpajModel.getSLS_INST_TYPE() != null) {
        contentValues.put(context.getString(R.string.SLS_INST_TYPE), simbakSpajModel.getSLS_INST_TYPE());
//        }
        //        if (simbakSpajModel.getSLS_USED() != null) {
        contentValues.put(context.getString(R.string.SLS_USED), simbakSpajModel.getSLS_USED());
//        }
        //        if (simbakSpajModel.getSLS_PROP_IN() != null) {
        contentValues.put(context.getString(R.string.SLS_PROP_IN), simbakSpajModel.getSLS_PROP_IN());
//        }
        //        if (simbakSpajModel.getSLS_SPAJ_IN() != null) {
        contentValues.put(context.getString(R.string.SLS_SPAJ_IN), simbakSpajModel.getSLS_SPAJ_IN());
//        }
        //        if (simbakSpajModel.getSLS_SPAJ_TMP() != null) {
        contentValues.put(context.getString(R.string.SLS_SPAJ_TMP), simbakSpajModel.getSLS_SPAJ_TMP());
//        }
        //        if (simbakSpajModel.getSLS_SPAJ_TMP_IN() != null) {
        contentValues.put(context.getString(R.string.SLS_SPAJ_TMP_IN), simbakSpajModel.getSLS_SPAJ_TMP_IN());
//        }
        //        if (simbakSpajModel.getSLS_CREATED() != null) {
        contentValues.put(context.getString(R.string.SLS_CREATED), simbakSpajModel.getSLS_CREATED());
//        }
        //        if (simbakSpajModel.getSLS_CRTD_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_CRTD_ID), simbakSpajModel.getSLS_CRTD_ID());
//        }
        //        if (simbakSpajModel.getSLS_UPDATED() != null) {
        contentValues.put(context.getString(R.string.SLS_UPDATED), simbakSpajModel.getSLS_UPDATED());
//        }
        //        if (simbakSpajModel.getSLS_UPDTD_ID() != null) {
        contentValues.put(context.getString(R.string.SLS_UPDTD_ID), simbakSpajModel.getSLS_UPDTD_ID());
//        }

        return contentValues;
    }
    
    public SimbakSpajModel getObject(Cursor cursor){
        SimbakSpajModel simbakSpajModel = null;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            simbakSpajModel = new SimbakSpajModel();
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_TAB_ID))))
                simbakSpajModel.setSLS_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLS_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_ID))))
                simbakSpajModel.setSLS_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLS_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_INST_ID))))
                simbakSpajModel.setSLS_INST_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLS_INST_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_INST_TAB_ID))))
                simbakSpajModel.setSLS_INST_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLS_INST_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_PROPOSAL))))
                simbakSpajModel.setSLS_PROPOSAL(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_PROPOSAL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ))))
                simbakSpajModel.setSLS_SPAJ(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_POLIS))))
                simbakSpajModel.setSLS_POLIS(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_POLIS))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_OLD_ID))))
                simbakSpajModel.setSLS_OLD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_OLD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_INST_TYPE))))
                simbakSpajModel.setSLS_INST_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLS_INST_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_USED))))
                simbakSpajModel.setSLS_USED(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLS_USED))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_PROP_IN))))
                simbakSpajModel.setSLS_PROP_IN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_PROP_IN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ_IN))))
                simbakSpajModel.setSLS_SPAJ_IN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ_IN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ_TMP_IN))))
                simbakSpajModel.setSLS_SPAJ_TMP_IN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ_TMP_IN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_CREATED))))
                simbakSpajModel.setSLS_CREATED(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_CREATED))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_CRTD_ID))))
                simbakSpajModel.setSLS_CRTD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_CRTD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_UPDATED))))
                simbakSpajModel.setSLS_UPDATED(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_UPDATED))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_UPDTD_ID))))
                simbakSpajModel.setSLS_UPDTD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_UPDTD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ_TMP))))
                simbakSpajModel.setSLS_SPAJ_TMP(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ_TMP))));

            cursor.close();
        }
        return simbakSpajModel;
    }

    public ArrayList<SimbakSpajModel> getArrayObject(Cursor cursor){
        ArrayList<SimbakSpajModel> spajModels = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                SimbakSpajModel simbakSpajModel = new SimbakSpajModel();
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_TAB_ID))))
                    simbakSpajModel.setSLS_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLS_TAB_ID))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_ID))))
                    simbakSpajModel.setSLS_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLS_ID))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_INST_ID))))
                    simbakSpajModel.setSLS_INST_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLS_INST_ID))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_INST_TAB_ID))))
                    simbakSpajModel.setSLS_INST_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLS_INST_TAB_ID))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_PROPOSAL))))
                    simbakSpajModel.setSLS_PROPOSAL(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_PROPOSAL))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ))))
                    simbakSpajModel.setSLS_SPAJ(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_POLIS))))
                    simbakSpajModel.setSLS_POLIS(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_POLIS))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_OLD_ID))))
                    simbakSpajModel.setSLS_OLD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_OLD_ID))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_INST_TYPE))))
                    simbakSpajModel.setSLS_INST_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLS_INST_TYPE))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_USED))))
                    simbakSpajModel.setSLS_USED(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLS_USED))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_PROP_IN))))
                    simbakSpajModel.setSLS_PROP_IN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_PROP_IN))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ_IN))))
                    simbakSpajModel.setSLS_SPAJ_IN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ_IN))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ_TMP_IN))))
                    simbakSpajModel.setSLS_SPAJ_TMP_IN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_SPAJ_TMP_IN))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_CREATED))))
                    simbakSpajModel.setSLS_CREATED(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_CREATED))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_CRTD_ID))))
                    simbakSpajModel.setSLS_CRTD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_CRTD_ID))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_UPDATED))))
                    simbakSpajModel.setSLS_UPDATED(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_UPDATED))));
                if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLS_UPDTD_ID))))
                    simbakSpajModel.setSLS_UPDTD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLS_UPDTD_ID))));

                spajModels.add(simbakSpajModel);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return spajModels;
    }
}
