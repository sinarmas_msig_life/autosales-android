package id.co.ajsmsig.nb.pointofsale.ui.cpriorityneed

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Observer
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.ProductRecommendation
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
class PriorityNeedVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository, var sharedViewModel: SharedViewModel): AndroidViewModel(application) {


    var observablePageOptions: MediatorLiveData<List<PageOption>> = MediatorLiveData()
    var row = 4
    var column = 3

    var page: Page? = null
    set(value) {

        val pageOptions = repository.getPageOptionsFromPage(value, sharedViewModel.selectedLifeStage?.id)
        observablePageOptions.addSource(pageOptions, {
            it?.let {
                observablePageOptions.value = it

                // Disable some page option when no recommendation product found
                it.forEach {
                    val pageOption = it
                    val nextPage = repository.getNextPageFrom(value, pageOption)

                    nextPage.observeForever(object : Observer<Page> {
                        override fun onChanged(t: Page?) {


                            val needAnalysis = repository.getPageOptionsFromPage(t, pageOption.id)
                            needAnalysis.observeForever(Observer {
                                it?.let {
                                    val needAnalysises = it
                                    if (needAnalysises.size == 1) {
                                        val needAnalysis = needAnalysises.firstOrNull()

                                        val productRecommendations = repository.getProductRecommendationWithGroupIdAndSelectedNeedAnalysisId(sharedViewModel.groupId, needAnalysis?.id)
                                        productRecommendations.observeForever(object : Observer<List<ProductRecommendation>> {
                                            override fun onChanged(t: List<ProductRecommendation>?) {

                                                pageOption.disabled.set(t?.isEmpty() == true)
                                                productRecommendations.removeObserver(this)
                                            }

                                        })
                                    }
                                }
                            })
                            nextPage.removeObserver(this)
                        }

                    })
                }
            }
        })
    }
}