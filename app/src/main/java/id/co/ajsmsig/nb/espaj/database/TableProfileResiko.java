package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.ProfileResikoModel;

/**
 * Created by Bernard on 05/09/2017.
 */

public class TableProfileResiko {
    private Context context;

    public TableProfileResiko(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(EspajModel me){
        ContentValues cv = new ContentValues();
        cv.put("NILAI_NO1_PR", me.getProfileResikoModel().getNilai_no1_pr());
        cv.put("NILAI_NO7_PR", me.getProfileResikoModel().getNilai_no7_pr());
        cv.put("NILAI_NO12_PR", me.getProfileResikoModel().getNilai_no12_pr());
        cv.put("NILAI_NO18_PR", me.getProfileResikoModel().getNilai_no18_pr());
        cv.put("NILAI_NO23_PR", me.getProfileResikoModel().getNilai_no23_pr());
        cv.put("NILAI_NO28_PR", me.getProfileResikoModel().getNilai_no28_pr());
        cv.put("NILAI_NO34_PR", me.getProfileResikoModel().getNilai_no34_pr());
        cv.put("NILAI_NO40_PR", me.getProfileResikoModel().getNilai_no40_pr());
        cv.put("NILAI_NO45_PR", me.getProfileResikoModel().getNilai_no45_pr());
        cv.put("NILAI_NO51_PR", me.getProfileResikoModel().getNilai_no51_pr());
        cv.put("ANSWER_NO1", me.getProfileResikoModel().getAnswer_no1());
        cv.put("ANSWER_NO7", me.getProfileResikoModel().getAnswer_no7());
        cv.put("ANSWER_NO12", me.getProfileResikoModel().getAnswer_no12());
        cv.put("ANSWER_NO18", me.getProfileResikoModel().getAnswer_no18());
        cv.put("ANSWER_NO23", me.getProfileResikoModel().getAnswer_no23());
        cv.put("ANSWER_NO28", me.getProfileResikoModel().getAnswer_no28());
        cv.put("ANSWER_NO34", me.getProfileResikoModel().getAnswer_no34());
        cv.put("ANSWER_NO40", me.getProfileResikoModel().getAnswer_no40());
        cv.put("ANSWER_NO45", me.getProfileResikoModel().getAnswer_no45());
        cv.put("ANSWER_NO51", me.getProfileResikoModel().getAnswer_no51());
        cv.put("VALIDATION", me.getProfileResikoModel().getValidation());

        return cv;
    }

    public ContentValues getContentValuesPOS(ArrayList<Integer> list_value) {
        ContentValues cv = new ContentValues();
        cv.put("NILAI_NO1_PR", list_value.get(0));
        cv.put("NILAI_NO7_PR", list_value.get(1));
        cv.put("NILAI_NO12_PR", list_value.get(2));
        cv.put("NILAI_NO18_PR", list_value.get(3));
        cv.put("NILAI_NO23_PR", list_value.get(4));
        cv.put("NILAI_NO28_PR", list_value.get(5));
        cv.put("NILAI_NO34_PR", list_value.get(6));
        cv.put("NILAI_NO40_PR", list_value.get(7));
        cv.put("NILAI_NO45_PR", list_value.get(8));
        cv.put("NILAI_NO51_PR", list_value.get(9));
        return cv;
    }

    public ProfileResikoModel getObject (Cursor cursor){
        ProfileResikoModel profileResikoModel= new ProfileResikoModel();
        if (cursor != null && cursor.getCount()>0){
            cursor.moveToFirst();
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NILAI_NO1_PR"))){
                profileResikoModel.setNilai_no1_pr(cursor.getInt(cursor.getColumnIndexOrThrow("NILAI_NO1_PR")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NILAI_NO7_PR"))){
                profileResikoModel.setNilai_no7_pr(cursor.getInt(cursor.getColumnIndexOrThrow("NILAI_NO7_PR")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NILAI_NO12_PR"))){
                profileResikoModel.setNilai_no12_pr(cursor.getInt(cursor.getColumnIndexOrThrow("NILAI_NO12_PR")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NILAI_NO18_PR"))){
                profileResikoModel.setNilai_no18_pr(cursor.getInt(cursor.getColumnIndexOrThrow("NILAI_NO18_PR")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NILAI_NO23_PR"))){
                profileResikoModel.setNilai_no23_pr(cursor.getInt(cursor.getColumnIndexOrThrow("NILAI_NO23_PR")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NILAI_NO28_PR"))){
                profileResikoModel.setNilai_no28_pr(cursor.getInt(cursor.getColumnIndexOrThrow("NILAI_NO28_PR")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NILAI_NO34_PR"))){
                profileResikoModel.setNilai_no34_pr(cursor.getInt(cursor.getColumnIndexOrThrow("NILAI_NO34_PR")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NILAI_NO40_PR"))){
                profileResikoModel.setNilai_no40_pr(cursor.getInt(cursor.getColumnIndexOrThrow("NILAI_NO40_PR")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NILAI_NO45_PR"))){
                profileResikoModel.setNilai_no45_pr(cursor.getInt(cursor.getColumnIndexOrThrow("NILAI_NO45_PR")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NILAI_NO51_PR"))){
                profileResikoModel.setNilai_no51_pr(cursor.getInt(cursor.getColumnIndexOrThrow("NILAI_NO51_PR")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ANSWER_NO1"))){
                profileResikoModel.setAnswer_no1(cursor.getString(cursor.getColumnIndexOrThrow("ANSWER_NO1")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ANSWER_NO7"))){
                profileResikoModel.setAnswer_no7(cursor.getString(cursor.getColumnIndexOrThrow("ANSWER_NO7")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ANSWER_NO12"))){
                profileResikoModel.setAnswer_no12(cursor.getString(cursor.getColumnIndexOrThrow("ANSWER_NO12")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ANSWER_NO18"))){
                profileResikoModel.setAnswer_no18(cursor.getString(cursor.getColumnIndexOrThrow("ANSWER_NO18")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ANSWER_NO23"))){
                profileResikoModel.setAnswer_no23(cursor.getString(cursor.getColumnIndexOrThrow("ANSWER_NO23")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ANSWER_NO28"))){
                profileResikoModel.setAnswer_no28(cursor.getString(cursor.getColumnIndexOrThrow("ANSWER_NO28")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ANSWER_NO34"))){
                profileResikoModel.setAnswer_no34(cursor.getString(cursor.getColumnIndexOrThrow("ANSWER_NO34")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ANSWER_NO40"))){
                profileResikoModel.setAnswer_no40(cursor.getString(cursor.getColumnIndexOrThrow("ANSWER_NO40")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ANSWER_NO45"))){
                profileResikoModel.setAnswer_no45(cursor.getString(cursor.getColumnIndexOrThrow("ANSWER_NO45")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ANSWER_NO51"))){
                profileResikoModel.setAnswer_no51(cursor.getString(cursor.getColumnIndexOrThrow("ANSWER_NO51")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("VALIDATION"))){
                int valid = cursor.getInt(cursor.getColumnIndexOrThrow("VALIDATION"));
//                boolean isValid = valid == 1 ? true : false;
                boolean val;
                val = valid == 1;
                profileResikoModel.setValidation(val);
            }
            cursor.close();
        }
        return profileResikoModel;
    }
}
