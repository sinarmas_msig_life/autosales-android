package id.co.ajsmsig.nb.pointofsale.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosRowProductRecommendationOptionBinding
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.ProductRecommendation

/**
 * Created by andreyyoshuamanik on 21/02/18.
 */
class ProductRecommendationOptionAdapter: RecyclerView.Adapter<ProductRecommendationOptionAdapter.OptionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionViewHolder {

        val dataBinding = DataBindingUtil.inflate<PosRowProductRecommendationOptionBinding>(LayoutInflater.from(parent.context), R.layout.pos_row_product_recommendation_option, parent, false)
        return OptionViewHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        options?.let {
            holder.bind(it[position])
        }
    }

    var options: Array<ProductRecommendation>? = null
    set(value) {
        field = value

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return options?.size ?: 0
    }

    inner class OptionViewHolder(private val dataBinding: PosRowProductRecommendationOptionBinding): RecyclerView.ViewHolder(dataBinding.root) {

        fun bind(option: ProductRecommendation) {
            dataBinding.vm = option
            dataBinding.viewHolder = this
            option.product.observeForever {
                dataBinding.product = it
            }
        }

        fun onClickOption(option: ProductRecommendation) {

            options?.forEach {
                it.selected.set(option == it)
            }

        }
    }
}