package id.co.ajsmsig.nb.espaj.model.arraylist;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 30/08/2017.
 */

public class ModelTTD implements Parcelable{

    private int counter = 0;
    private String id_ttd = "";
    private String file_ttd = "";
    private String judul_ttd="";
    private String nama_jelas="";
    private Boolean validation = false;
    private int flag_default = 0;
    private int flag_status = 0;

    public ModelTTD() {
    }

    protected ModelTTD(Parcel in) {
        counter = in.readInt();
        id_ttd = in.readString();
        file_ttd = in.readString();
        judul_ttd = in.readString();
        nama_jelas = in.readString();
        byte tmpValidation = in.readByte();
        validation = tmpValidation == 0 ? null : tmpValidation == 1;
        flag_default = in.readInt();
        flag_status = in.readInt();
    }

    public static final Creator<ModelTTD> CREATOR = new Creator<ModelTTD>() {
        @Override
        public ModelTTD createFromParcel(Parcel in) {
            return new ModelTTD(in);
        }

        @Override
        public ModelTTD[] newArray(int size) {
            return new ModelTTD[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(counter);
        parcel.writeString(id_ttd);
        parcel.writeString(file_ttd);
        parcel.writeString(judul_ttd);
        parcel.writeString(nama_jelas);
        parcel.writeByte((byte) (validation == null ? 0 : validation ? 1 : 2));
        parcel.writeInt(flag_default);
        parcel.writeInt(flag_status);
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getId_ttd() {
        return id_ttd;
    }

    public void setId_ttd(String id_ttd) {
        this.id_ttd = id_ttd;
    }

    public String getFile_ttd() {
        return file_ttd;
    }

    public void setFile_ttd(String file_ttd) {
        this.file_ttd = file_ttd;
    }

    public String getJudul_ttd() {
        return judul_ttd;
    }

    public void setJudul_ttd(String judul_ttd) {
        this.judul_ttd = judul_ttd;
    }

    public String getNama_jelas() {
        return nama_jelas;
    }

    public void setNama_jelas(String nama_jelas) {
        this.nama_jelas = nama_jelas;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public int getFlag_default() {
        return flag_default;
    }

    public void setFlag_default(int flag_default) {
        this.flag_default = flag_default;
    }

    public int getFlag_status() {
        return flag_status;
    }

    public void setFlag_status(int flag_status) {
        this.flag_status = flag_status;
    }

    public static Creator<ModelTTD> getCREATOR() {
        return CREATOR;
    }
}
