
package id.co.ajsmsig.nb.crm.model.apiresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DownloadSubActivityTypeForDropdownResponse {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("saved_id")
    @Expose
    private Integer savedId;
    @SerializedName("indonesia")
    @Expose
    private String indonesia;
    @SerializedName("english")
    @Expose
    private String english;
    @SerializedName("appCode")
    @Expose
    private Integer appCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSavedId() {
        return savedId;
    }

    public void setSavedId(Integer savedId) {
        this.savedId = savedId;
    }

    public String getIndonesia() {
        return indonesia;
    }

    public void setIndonesia(String indonesia) {
        this.indonesia = indonesia;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public Integer getAppCode() {
        return appCode;
    }

    public void setAppCode(Integer appCode) {
        this.appCode = appCode;
    }

}
