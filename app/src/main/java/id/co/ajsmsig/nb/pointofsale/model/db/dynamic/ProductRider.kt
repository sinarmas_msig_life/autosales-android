package id.co.ajsmsig.nb.pointofsale.model.db.dynamic

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class ProductRider(
        @PrimaryKey
        val Id: Int = 0,
        var Nama: String?,
        var ProductLsbsId: Int?,
        var Image: String?)


