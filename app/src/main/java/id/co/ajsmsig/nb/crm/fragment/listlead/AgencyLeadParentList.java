package id.co.ajsmsig.nb.crm.fragment.listlead;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class AgencyLeadParentList extends ExpandableGroup<AgencyLeadChildList> {
    public AgencyLeadParentList(String groupTitle, List<AgencyLeadChildList> items) {
        super(groupTitle, items);
    }
}
