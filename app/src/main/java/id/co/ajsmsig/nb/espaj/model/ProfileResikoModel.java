package id.co.ajsmsig.nb.espaj.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 30/08/2017.
 */

public class ProfileResikoModel implements Parcelable{

    private int nilai_no1_pr = 0;
    private int nilai_no7_pr = 0;
    private int nilai_no12_pr = 0;
    private int nilai_no18_pr = 0;
    private int nilai_no23_pr = 0;
    private int nilai_no28_pr = 0;
    private int nilai_no34_pr = 0;
    private int nilai_no40_pr = 0;
    private int nilai_no45_pr = 0;
    private int nilai_no51_pr = 0;

    //1
    private String answer_no1 = "";
    //2
    private String answer_no7 = "";
    //3
    private String answer_no12 = "";
    //4
    private String answer_no18 = "";
    //5
    private String answer_no23 = "";
    //6
    private String answer_no28 = "";
    //7
    private String answer_no34 = "";
    //8
    private String answer_no40 = "";
    //9
    private String answer_no45 = "";
    //10
    private String answer_no51 = "";

    private Boolean Validation = false;

    public ProfileResikoModel() {
    }

    public ProfileResikoModel(int nilai_no1_pr, int nilai_no7_pr, int nilai_no12_pr, int nilai_no18_pr, int nilai_no23_pr, int nilai_no28_pr, int nilai_no34_pr, int nilai_no40_pr, int nilai_no45_pr, int nilai_no51_pr, String answer_no1, String answer_no7, String answer_no12, String answer_no18, String answer_no23, String answer_no28, String answer_no34, String answer_no40, String answer_no45, String answer_no51) {
        this.nilai_no1_pr = nilai_no1_pr;
        this.nilai_no7_pr = nilai_no7_pr;
        this.nilai_no12_pr = nilai_no12_pr;
        this.nilai_no18_pr = nilai_no18_pr;
        this.nilai_no23_pr = nilai_no23_pr;
        this.nilai_no28_pr = nilai_no28_pr;
        this.nilai_no34_pr = nilai_no34_pr;
        this.nilai_no40_pr = nilai_no40_pr;
        this.nilai_no45_pr = nilai_no45_pr;
        this.nilai_no51_pr = nilai_no51_pr;
        this.answer_no1 = answer_no1;
        this.answer_no7 = answer_no7;
        this.answer_no12 = answer_no12;
        this.answer_no18 = answer_no18;
        this.answer_no23 = answer_no23;
        this.answer_no28 = answer_no28;
        this.answer_no34 = answer_no34;
        this.answer_no40 = answer_no40;
        this.answer_no45 = answer_no45;
        this.answer_no51 = answer_no51;
    }

    protected ProfileResikoModel(Parcel in) {
        nilai_no1_pr = in.readInt();
        nilai_no7_pr = in.readInt();
        nilai_no12_pr = in.readInt();
        nilai_no18_pr = in.readInt();
        nilai_no23_pr = in.readInt();
        nilai_no28_pr = in.readInt();
        nilai_no34_pr = in.readInt();
        nilai_no40_pr = in.readInt();
        nilai_no45_pr = in.readInt();
        nilai_no51_pr = in.readInt();
        answer_no1 = in.readString();
        answer_no7 = in.readString();
        answer_no12 = in.readString();
        answer_no18 = in.readString();
        answer_no23 = in.readString();
        answer_no28 = in.readString();
        answer_no34 = in.readString();
        answer_no40 = in.readString();
        answer_no45 = in.readString();
        answer_no51 = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(nilai_no1_pr);
        dest.writeInt(nilai_no7_pr);
        dest.writeInt(nilai_no12_pr);
        dest.writeInt(nilai_no18_pr);
        dest.writeInt(nilai_no23_pr);
        dest.writeInt(nilai_no28_pr);
        dest.writeInt(nilai_no34_pr);
        dest.writeInt(nilai_no40_pr);
        dest.writeInt(nilai_no45_pr);
        dest.writeInt(nilai_no51_pr);
        dest.writeString(answer_no1);
        dest.writeString(answer_no7);
        dest.writeString(answer_no12);
        dest.writeString(answer_no18);
        dest.writeString(answer_no23);
        dest.writeString(answer_no28);
        dest.writeString(answer_no34);
        dest.writeString(answer_no40);
        dest.writeString(answer_no45);
        dest.writeString(answer_no51);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProfileResikoModel> CREATOR = new Creator<ProfileResikoModel>() {
        @Override
        public ProfileResikoModel createFromParcel(Parcel in) {
            return new ProfileResikoModel(in);
        }

        @Override
        public ProfileResikoModel[] newArray(int size) {
            return new ProfileResikoModel[size];
        }
    };

    public int getNilai_no1_pr() {
        return nilai_no1_pr;
    }

    public void setNilai_no1_pr(int nilai_no1_pr) {
        this.nilai_no1_pr = nilai_no1_pr;
    }

    public int getNilai_no7_pr() {
        return nilai_no7_pr;
    }

    public void setNilai_no7_pr(int nilai_no7_pr) {
        this.nilai_no7_pr = nilai_no7_pr;
    }

    public int getNilai_no12_pr() {
        return nilai_no12_pr;
    }

    public void setNilai_no12_pr(int nilai_no12_pr) {
        this.nilai_no12_pr = nilai_no12_pr;
    }

    public int getNilai_no18_pr() {
        return nilai_no18_pr;
    }

    public void setNilai_no18_pr(int nilai_no18_pr) {
        this.nilai_no18_pr = nilai_no18_pr;
    }

    public int getNilai_no23_pr() {
        return nilai_no23_pr;
    }

    public void setNilai_no23_pr(int nilai_no23_pr) {
        this.nilai_no23_pr = nilai_no23_pr;
    }

    public int getNilai_no28_pr() {
        return nilai_no28_pr;
    }

    public void setNilai_no28_pr(int nilai_no28_pr) {
        this.nilai_no28_pr = nilai_no28_pr;
    }

    public int getNilai_no34_pr() {
        return nilai_no34_pr;
    }

    public void setNilai_no34_pr(int nilai_no34_pr) {
        this.nilai_no34_pr = nilai_no34_pr;
    }

    public int getNilai_no40_pr() {
        return nilai_no40_pr;
    }

    public void setNilai_no40_pr(int nilai_no40_pr) {
        this.nilai_no40_pr = nilai_no40_pr;
    }

    public int getNilai_no45_pr() {
        return nilai_no45_pr;
    }

    public void setNilai_no45_pr(int nilai_no45_pr) {
        this.nilai_no45_pr = nilai_no45_pr;
    }

    public int getNilai_no51_pr() {
        return nilai_no51_pr;
    }

    public void setNilai_no51_pr(int nilai_no51_pr) {
        this.nilai_no51_pr = nilai_no51_pr;
    }

    public String getAnswer_no1() {
        return answer_no1;
    }

    public void setAnswer_no1(String answer_no1) {
        this.answer_no1 = answer_no1;
    }

    public String getAnswer_no7() {
        return answer_no7;
    }

    public void setAnswer_no7(String answer_no7) {
        this.answer_no7 = answer_no7;
    }

    public String getAnswer_no12() {
        return answer_no12;
    }

    public void setAnswer_no12(String answer_no12) {
        this.answer_no12 = answer_no12;
    }

    public String getAnswer_no18() {
        return answer_no18;
    }

    public void setAnswer_no18(String answer_no18) {
        this.answer_no18 = answer_no18;
    }

    public String getAnswer_no23() {
        return answer_no23;
    }

    public void setAnswer_no23(String answer_no23) {
        this.answer_no23 = answer_no23;
    }

    public String getAnswer_no28() {
        return answer_no28;
    }

    public void setAnswer_no28(String answer_no28) {
        this.answer_no28 = answer_no28;
    }

    public String getAnswer_no34() {
        return answer_no34;
    }

    public void setAnswer_no34(String answer_no34) {
        this.answer_no34 = answer_no34;
    }

    public String getAnswer_no40() {
        return answer_no40;
    }

    public void setAnswer_no40(String answer_no40) {
        this.answer_no40 = answer_no40;
    }

    public String getAnswer_no45() {
        return answer_no45;
    }

    public void setAnswer_no45(String answer_no45) {
        this.answer_no45 = answer_no45;
    }

    public String getAnswer_no51() {
        return answer_no51;
    }

    public void setAnswer_no51(String answer_no51) {
        this.answer_no51 = answer_no51;
    }

    public Boolean getValidation() {
        return Validation;
    }

    public void setValidation(Boolean validation) {
        Validation = validation;
    }

    public static Creator<ProfileResikoModel> getCREATOR() {
        return CREATOR;
    }
}
