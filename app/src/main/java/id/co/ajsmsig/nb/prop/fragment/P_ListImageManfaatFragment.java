package id.co.ajsmsig.nb.prop.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.activity.P_FullImageScreenManfaat;
import id.co.ajsmsig.nb.prop.adapter.P_ImageAdapter;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductRiderModel;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.ProductCodePlanModel;


public class P_ListImageManfaatFragment extends Fragment {
    @BindView(R.id.gv_list_manfaat)
    GridView gv_list_manfaat;

    private ArrayList<HashMap<String, String>> imagePaths;

    private P_ProposalModel proposalModel;
    private P_ImageAdapter imageAdapter;

    private ArrayList<ProductCodePlanModel> productCodePlanModels;

//    private OnFragmentInteractionListener mListener;

    public P_ListImageManfaatFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getActivity().getIntent().getExtras();
        proposalModel = bundle.getParcelable(getString(R.string.proposal_model));

    }

//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        View itemView = convertView;
//        if (itemView==null)
//            itemView = LayoutInflater.from(mContext).inflate(R.layout.item_view, parent, false);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.p_fragment_list_image_manfaat, container, false);
        ButterKnife.bind(this, view);

        JSONObject mapListImageManfaat = null;
        try {
            mapListImageManfaat = new JSONObject(getJSONString(getContext()));
            Log.d("TAG", "getPath: " + mapListImageManfaat.toString(2));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        imagePaths = new ArrayList<>();

        /*TODO jika semua product udah ditambahkan hilangkan komen dibawah*/
        imagePaths.add(getPath(mapListImageManfaat, proposalModel.getMst_proposal_product().getLsbs_id(), proposalModel.getMst_proposal_product().getLsdbs_number()));

        for (P_MstProposalProductRiderModel riderModel : proposalModel.getMst_proposal_product_rider()) {
            imagePaths.add(getPath(mapListImageManfaat, riderModel.getLsbs_id(), riderModel.getLsdbs_number()));

        }
        Log.d("TAG", "onCreateView: ");
        imageAdapter = new P_ImageAdapter(getContext(), imagePaths);
        gv_list_manfaat.setAdapter(imageAdapter);

        gv_list_manfaat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                // Sending image id to FullScreenActivity
                Intent i = new Intent(getActivity().getApplicationContext(), P_FullImageScreenManfaat.class);
                // passing array index
                Log.d("TAG", "onItemClick: jumlah arrray grid is " + imagePaths.size());
                Bundle bundle = new Bundle();
                bundle.putInt("id", position);
                bundle.putSerializable("array", imagePaths);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        return view;
    }

    public HashMap<String, String> getPath(JSONObject jsonObject, int productCode, int plan) {
        HashMap<String, String> result = new HashMap<>();

        try {
            JSONObject productCodeJson = jsonObject.getJSONObject(String.valueOf(productCode));
            JSONObject planObj = productCodeJson.getJSONObject(String.valueOf(plan));
            String path = planObj.getString("path");
            String name = planObj.getString("name");
            result.put("path", path);
            result.put("name", name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
    private String getJSONString(Context context)
    {
        StringBuilder stringBuilder = new StringBuilder();
        try
        {
            AssetManager assetManager = context.getAssets();
            if (proposalModel.getMst_proposal_product().getLsbs_id() == 224 || proposalModel.getMst_proposal_product().getLsbs_id() == 215) {
                InputStream in = assetManager.open("map_manfaat_product_syariah.txt");
                InputStreamReader isr = new InputStreamReader(in);
                char[] inputBuffer = new char[100];

                int charRead;
                while ((charRead = isr.read(inputBuffer)) > 0) {
                    String readString = String.copyValueOf(inputBuffer, 0, charRead);
                    stringBuilder.append(readString);
                }
            } else {
                InputStream in = assetManager.open("map_manfaat_product.txt");
                InputStreamReader isr = new InputStreamReader(in);
                char[] inputBuffer = new char[100];

                int charRead;
                while ((charRead = isr.read(inputBuffer)) > 0) {
                    String readString = String.copyValueOf(inputBuffer, 0, charRead);
                    stringBuilder.append(readString);
                }
            }
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
        }

        return stringBuilder.toString();
    }

    public void execute() {
    }


//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
}
