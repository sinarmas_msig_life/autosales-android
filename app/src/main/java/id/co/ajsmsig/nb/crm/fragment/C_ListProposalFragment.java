package id.co.ajsmsig.nb.crm.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.adapter.ProposalListAdapter;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.model.ProposalListModel;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.util.Const;

/*
 * Created by faiz_f on 03/02/2017.
 */

public class C_ListProposalFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, ProposalListAdapter.ClickListener {
    private static final String TAG = C_ListProposalFragment.class.getSimpleName();
    private boolean isClick = true;
    private long SL_TAB_ID;
    private long SLP_TAB_ID;
    private LinearLayout layoutSearchNotFound;
    private ListView listView;
    private final int LOADER_DISPLAY_LIST_PROPOSAL = 200;
    private ProposalListAdapter mAdapter;
    private Cursor cursor;
    private List<ProposalListModel> proposals;
    private RecyclerView recyclerView;

    public C_ListProposalFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getActivity().getIntent().getExtras();
        SL_TAB_ID = bundle.getLong(getString(R.string.SL_TAB_ID), -1);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.c_fragment_list_proposal, container, false);

        layoutSearchNotFound = view.findViewById(R.id.layoutSearchNotFound);
        recyclerView = view.findViewById(R.id.recyclerViewProposal);

        getActivity().getSupportLoaderManager().initLoader(LOADER_DISPLAY_LIST_PROPOSAL, null, this);
        initRecyclerView();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getSupportLoaderManager().restartLoader(LOADER_DISPLAY_LIST_PROPOSAL, null, this);
    }

    private void clearPreviousResult() {
        if (proposals!= null && proposals.size() >0) {
            proposals.clear();
        }
    }

    /**
     * Init recyclerview to display proposal list
     */
    private void initRecyclerView() {
        proposals = new ArrayList<>();
        mAdapter = new ProposalListAdapter(proposals);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setClickListener(this);
    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader cursorLoader;
        switch (id) {
            case LOADER_DISPLAY_LIST_PROPOSAL:
                cursor = new C_Select(getActivity()).getProposalList(String.valueOf(SL_TAB_ID));
                break;
        }

        cursorLoader = new CursorLoader(getContext()) {
            @Override
            public Cursor loadInBackground() {
                return cursor;
            }
        };

        return cursorLoader;
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case LOADER_DISPLAY_LIST_PROPOSAL:

                if (cursor.getCount() > 0) {
                    clearPreviousResult();
                    List<ProposalListModel> proposals = new ArrayList<>();
                    while (cursor.moveToNext()) {
                        long id = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                        String noProposalTab = cursor.getString(cursor.getColumnIndexOrThrow("NO_PROPOSAL_TAB"));
                        String noProposal = cursor.getString(cursor.getColumnIndexOrThrow("NO_PROPOSAL"));
                        String namaPp = "Pemegang polis : "+cursor.getString(cursor.getColumnIndexOrThrow("NAMA_PP"));
                        String namaTt = "Tertanggung : "+cursor.getString(cursor.getColumnIndexOrThrow("NAMA_TT"));
                        long premi = cursor.getLong(cursor.getColumnIndexOrThrow("PREMI"));
                        long uangPertanggungan = cursor.getLong(cursor.getColumnIndexOrThrow("UP"));
                        String lsdbsName = cursor.getString(cursor.getColumnIndexOrThrow("LSDBS_NAME"));

                        ProposalListModel proposal = new ProposalListModel(id, noProposalTab, noProposal, namaPp, namaTt, premi, uangPertanggungan, lsdbsName);
                        proposals.add(proposal);
                    }

                    layoutSearchNotFound.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    mAdapter.addAll(proposals);
                } else {
                    layoutSearchNotFound.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void onProposalSelected(int position, ProposalListModel model) {
        long id = model.getId();
        String noProposalTab = model.getNoProposalTab();
        Intent intent = new Intent(getContext(), P_MainActivity.class);
        intent.putExtra(Const.INTENT_KEY_SL_TAB_ID, id);
        intent.putExtra(Const.INTENT_KEY_NO_PROPOSAL_TAB, noProposalTab);
        startActivity(intent);
    }
}

