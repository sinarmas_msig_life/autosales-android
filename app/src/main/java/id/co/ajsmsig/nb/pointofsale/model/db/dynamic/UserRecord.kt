package id.co.ajsmsig.nb.pointofsale.model.db.dynamic

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by andreyyoshuamanik on 10/03/18.
 */
@Entity
class UserRecord(
        @PrimaryKey(autoGenerate = true)
        var id: Long,
        var userId: Long? = null,
        var groupId: Int? = null,
        var savedJson: String? = null,
        var complete: Boolean = false
)