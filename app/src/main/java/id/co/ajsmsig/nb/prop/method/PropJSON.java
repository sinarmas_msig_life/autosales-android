package id.co.ajsmsig.nb.prop.method;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductPesertaModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductRiderModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductTopUpModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductUlinkModel;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;

/**
  Created by faiz_f on 14/12/2016.
 */

public class PropJSON {
    private static final String TAG  = PropJSON.class.getSimpleName();
    private Context context;

    public PropJSON(Context context) {
        this.context = context;
    }

    private ArrayList<P_MstProposalProductTopUpModel> filterTopUp(ArrayList<P_MstProposalProductTopUpModel> topUpModels) {
        Iterator<P_MstProposalProductTopUpModel> it = topUpModels.iterator();
        while (it.hasNext()) {
            if (!it.next().getSelected()) {
                it.remove();
            }
        }

        return topUpModels;
    }

    private ArrayList<P_MstProposalProductPesertaModel> getListPesertaFromRider(ArrayList<P_MstProposalProductRiderModel> riderModels) {
        ArrayList<P_MstProposalProductPesertaModel> allPesertaModels = new ArrayList<>();
        for (P_MstProposalProductRiderModel riderModel : riderModels) {
            ArrayList<P_MstProposalProductPesertaModel> pesertaModels = riderModel.getP_mstProposalProductPesertaModels();
            if (pesertaModels != null) {
                for(P_MstProposalProductPesertaModel pesertaModel : pesertaModels){
                    allPesertaModels.add(pesertaModel);
                }
            }
        }
        return allPesertaModels;
    }

    public JSONObject getJSONForPrimaryNoProposal(P_ProposalModel source) throws JSONException {
        P_MstDataProposalModel dataProposalModel = source.getMst_data_proposal();
        P_MstProposalProductModel productModel = source.getMst_proposal_product();
        ArrayList<P_MstProposalProductTopUpModel> topUpModels =
                filterTopUp(source.getMst_proposal_product_topup());
        ArrayList<P_MstProposalProductUlinkModel> ulinkModels =
                source.getMst_proposal_product_ulink();
        ArrayList<P_MstProposalProductRiderModel> riderModels =
                source.getMst_proposal_product_rider();
        ArrayList<P_MstProposalProductPesertaModel> pesertaModels =
                getListPesertaFromRider(riderModels);

        JSONObject allDataObject = new JSONObject();

        if (dataProposalModel != null) {
            JSONObject dataProposalObject = getJsonObjectForDataProposal(dataProposalModel);
            allDataObject.put(context.getString(R.string.MST_DATA_PROPOSAL).toLowerCase(), dataProposalObject);
        }
        if (productModel != null) {
            JSONObject detailProposalObject = getJsonObjectForProposalProduct(productModel);
            allDataObject.put(context.getString(R.string.MST_PROPOSAL_PRODUCT).toLowerCase(), detailProposalObject);
        }
        if (!topUpModels.isEmpty()) {
            JSONArray topUpArray = getJsonArrayForTopUp(topUpModels);
            allDataObject.put(context.getString(R.string.MST_PROPOSAL_PRODUCT_TOPUP).toLowerCase(), topUpArray);
        }
        if (!ulinkModels.isEmpty()) {
            JSONArray uLinkArray = getJsonArrayForULink(ulinkModels);
            allDataObject.put(context.getString(R.string.MST_PROPOSAL_PRODUCT_ULINK).toLowerCase(), uLinkArray);
        }
        if (!riderModels.isEmpty()) {
            JSONArray riderArray = getJsonArrayForRider(riderModels);
            allDataObject.put(context.getString(R.string.MST_PROPOSAL_PRODUCT_RIDER).toLowerCase(), riderArray);
        }
        if (!pesertaModels.isEmpty()) {
            JSONArray pesertaArray = getJsonArrayForPeserta(pesertaModels);
            allDataObject.put(context.getString(R.string.MST_PROPOSAL_PRODUCT_PESERTA).toLowerCase(), pesertaArray);
        }
        Log.d(TAG, "getJSONForPrimaryNoProposal: " + allDataObject.toString());
        return allDataObject;
    }

    private JSONObject getJsonObjectForDataProposal(P_MstDataProposalModel propModelMstDataProposal) throws JSONException {
        JSONObject dataProposalObject = new JSONObject();

        dataProposalObject.put(context.getString(R.string.NO_PROPOSAL).toLowerCase(),
                propModelMstDataProposal.getNo_proposal());
        dataProposalObject.put(context.getString(R.string.NAMA_PP).toLowerCase(),
                propModelMstDataProposal.getNama_pp());
        dataProposalObject.put(context.getString(R.string.TGL_LAHIR_PP).toLowerCase(),
                propModelMstDataProposal.getTgl_lahir_pp());
        dataProposalObject.put(context.getString(R.string.UMUR_PP).toLowerCase(),
                propModelMstDataProposal.getUmur_pp());
        dataProposalObject.put(context.getString(R.string.SEX_PP).toLowerCase(),
                propModelMstDataProposal.getSex_pp());
        dataProposalObject.put(context.getString(R.string.NAMA_TT).toLowerCase(),
                propModelMstDataProposal.getNama_tt());
        dataProposalObject.put(context.getString(R.string.TGL_LAHIR_TT).toLowerCase(),
                propModelMstDataProposal.getTgl_lahir_tt());
        dataProposalObject.put(context.getString(R.string.UMUR_TT).toLowerCase(),
                propModelMstDataProposal.getUmur_tt());
        dataProposalObject.put(context.getString(R.string.SEX_TT).toLowerCase(),
                propModelMstDataProposal.getSex_tt());
        dataProposalObject.put(context.getString(R.string.NAMA_AGEN).toLowerCase(),
                propModelMstDataProposal.getNama_agen());
        dataProposalObject.put(context.getString(R.string.MSAG_ID).toLowerCase(),
                propModelMstDataProposal.getMsag_id());
        dataProposalObject.put(context.getString(R.string.TGL_INPUT).toLowerCase(),
                propModelMstDataProposal.getTgl_input());
        dataProposalObject.put(context.getString(R.string.FLAG_AKTIF).toLowerCase(),
                1);
        dataProposalObject.put(context.getString(R.string.FLAG_TEST).toLowerCase(),
                propModelMstDataProposal.getFlag_test());
        dataProposalObject.put(context.getString(R.string.NAMA_USER).toLowerCase(),
                propModelMstDataProposal.getNama_user());
        dataProposalObject.put(context.getString(R.string.KODE_AGEN).toLowerCase(),
                propModelMstDataProposal.getKode_agen());
        dataProposalObject.put(context.getString(R.string.LEAD_ID).toLowerCase(),
                propModelMstDataProposal.getLead_id());
        return dataProposalObject;
    }

    private JSONObject getJsonObjectForProposalProduct(P_MstProposalProductModel propModelMstProposalProduct) throws JSONException {
        JSONObject proposalProductObject = new JSONObject();

        proposalProductObject.put(context.getString(R.string.NO_PROPOSAL).toLowerCase(),
                propModelMstProposalProduct.getNo_proposal());
        proposalProductObject.put(context.getString(R.string.LSBS_ID).toLowerCase(),
                propModelMstProposalProduct.getLsbs_id());
        proposalProductObject.put(context.getString(R.string.LSDBS_NUMBER).toLowerCase(),
                propModelMstProposalProduct.getLsdbs_number());
        proposalProductObject.put(context.getString(R.string.LKU_ID).toLowerCase(),
                propModelMstProposalProduct.getLku_id());
        proposalProductObject.put(context.getString(R.string.GRP_PRODUCT_NAME).toLowerCase(),
                propModelMstProposalProduct.getGrp_product_name());
        proposalProductObject.put(context.getString(R.string.PREMI).toLowerCase(),
                propModelMstProposalProduct.getPremi());
        proposalProductObject.put(context.getString(R.string.PREMI_KOMB).toLowerCase(),
                propModelMstProposalProduct.getPremi_komb());
        proposalProductObject.put(context.getString(R.string.PREMI_POKOK).toLowerCase(),
                propModelMstProposalProduct.getPremi_pokok());
        proposalProductObject.put(context.getString(R.string.PREMI_TOPUP).toLowerCase(),
                propModelMstProposalProduct.getPremi_topup());
        proposalProductObject.put(context.getString(R.string.UP).toLowerCase(),
                propModelMstProposalProduct.getUp());
        proposalProductObject.put(context.getString(R.string.CARA_BAYAR).toLowerCase(),
                propModelMstProposalProduct.getCara_bayar());
        proposalProductObject.put(context.getString(R.string.THN_CUTI_PREMI).toLowerCase(),
                propModelMstProposalProduct.getThn_cuti_premi());
        proposalProductObject.put(context.getString(R.string.THN_MASA_KONTRAK).toLowerCase(),
                propModelMstProposalProduct.getThn_masa_kontrak());
        proposalProductObject.put(context.getString(R.string.THN_LAMA_BAYAR).toLowerCase(),
                propModelMstProposalProduct.getThn_lama_bayar());
        proposalProductObject.put(context.getString(R.string.INV_FIX).toLowerCase(),
                propModelMstProposalProduct.getInv_fix());
        proposalProductObject.put(context.getString(R.string.INV_DYNAMIC).toLowerCase(),
                propModelMstProposalProduct.getInv_dynamic());
        proposalProductObject.put(context.getString(R.string.INV_AGGRESSIVE).toLowerCase(),
                propModelMstProposalProduct.getInv_aggressive());
        proposalProductObject.put(context.getString(R.string.LJI_FIX).toLowerCase(),
                propModelMstProposalProduct.getLji_fix());
        proposalProductObject.put(context.getString(R.string.LJI_DYNAMIC).toLowerCase(),
                propModelMstProposalProduct.getLji_dynamic());
        proposalProductObject.put(context.getString(R.string.LJI_AGGRESIVE).toLowerCase(),
                propModelMstProposalProduct.getLji_aggresive());

        return proposalProductObject;
    }

    private JSONArray getJsonArrayForTopUp(ArrayList<P_MstProposalProductTopUpModel> propModelMstProposalProductTopUpArrayList) throws JSONException {
        JSONArray topUPArray = new JSONArray();

        for (int i = 0; i < propModelMstProposalProductTopUpArrayList.size(); i++) {
            P_MstProposalProductTopUpModel propModelMstProposalProductTopUp = propModelMstProposalProductTopUpArrayList.get(i);
            JSONObject objectItem = new JSONObject();

            objectItem.put(context.getString(R.string.NO_PROPOSAL).toLowerCase(),
                    propModelMstProposalProductTopUp.getNo_proposal());
            objectItem.put(context.getString(R.string.THN_KE).toLowerCase(),
                    propModelMstProposalProductTopUp.getThn_ke());
            objectItem.put(context.getString(R.string.TOPUP).toLowerCase(),
                    propModelMstProposalProductTopUp.getTopup());
            objectItem.put(context.getString(R.string.TARIK).toLowerCase(),
                    propModelMstProposalProductTopUp.getTarik());

            topUPArray.put(objectItem);
        }

        return topUPArray;
    }

    private JSONArray getJsonArrayForULink(ArrayList<P_MstProposalProductUlinkModel> propModelMstProposalProductUlinkArrayList) throws JSONException {
        JSONArray uLinkArray = new JSONArray();

        for (int i = 0; i < propModelMstProposalProductUlinkArrayList.size(); i++) {
            P_MstProposalProductUlinkModel propModelMstProposalProductUlink = propModelMstProposalProductUlinkArrayList.get(i);
            JSONObject objectItem = new JSONObject();

            objectItem.put(context.getString(R.string.NO_PROPOSAL).toLowerCase(),
                    propModelMstProposalProductUlink.getNo_proposal());
            objectItem.put(context.getString(R.string.LJI_ID).toLowerCase(),
                    propModelMstProposalProductUlink.getLji_id());
            objectItem.put(context.getString(R.string.MDU_PERSEN).toLowerCase(),
                    propModelMstProposalProductUlink.getMdu_persen());

            uLinkArray.put(objectItem);
        }

        return uLinkArray;
    }

    private JSONArray getJsonArrayForRider(
            ArrayList<P_MstProposalProductRiderModel> propModelMstProposalProductRiderArrayList)
            throws JSONException {
        JSONArray riderArray = new JSONArray();

        for (int i = 0; i < propModelMstProposalProductRiderArrayList.size(); i++) {
            P_MstProposalProductRiderModel propModelMstProposalProductRider = propModelMstProposalProductRiderArrayList.get(i);
            JSONObject objectItem = new JSONObject();

            objectItem.put(context.getString(R.string.NO_PROPOSAL).toLowerCase(),
                    propModelMstProposalProductRider.getNo_proposal());
            objectItem.put(context.getString(R.string.LSBS_ID).toLowerCase(),
                    propModelMstProposalProductRider.getLsbs_id());
            objectItem.put(context.getString(R.string.LSDBS_NUMBER).toLowerCase(),
                    propModelMstProposalProductRider.getLsdbs_number());
            objectItem.put(context.getString(R.string.LKU_ID).toLowerCase(),
                    propModelMstProposalProductRider.getLku_id());
            objectItem.put(context.getString(R.string.PREMI).toLowerCase(),
                    propModelMstProposalProductRider.getPremi());
            objectItem.put(context.getString(R.string.UP).toLowerCase(),
                    propModelMstProposalProductRider.getUp());
            objectItem.put(context.getString(R.string.PERSEN_UP).toLowerCase(),
                    propModelMstProposalProductRider.getPersen_up());
            objectItem.put(context.getString(R.string.KELAS).toLowerCase(),
                    propModelMstProposalProductRider.getKelas());
            objectItem.put(context.getString(R.string.JML_UNIT).toLowerCase(),
                    propModelMstProposalProductRider.getJml_unit());
            objectItem.put(context.getString(R.string.BULAN_KE).toLowerCase(),
                    propModelMstProposalProductRider.getBulan_ke());

            riderArray.put(objectItem);
        }

        return riderArray;
    }

    private JSONArray getJsonArrayForPeserta(
            ArrayList<P_MstProposalProductPesertaModel> propModelMstProposalProductPesertaArrayList)
            throws JSONException {
        JSONArray pesertaArray = new JSONArray();

        for (int i = 0; i < propModelMstProposalProductPesertaArrayList.size(); i++) {
            P_MstProposalProductPesertaModel propModelMstProposalProductPeserta = propModelMstProposalProductPesertaArrayList.get(i);
            JSONObject objectItem = new JSONObject();

            objectItem.put(context.getString(R.string.NO_PROPOSAL).toLowerCase(),
                    propModelMstProposalProductPeserta.getNo_proposal());
            objectItem.put(context.getString(R.string.LSBS_ID).toLowerCase(),
                    propModelMstProposalProductPeserta.getLsbs_id());
            objectItem.put(context.getString(R.string.LSDBS_NUMBER).toLowerCase(),
                    propModelMstProposalProductPeserta.getLsdbs_number());
            objectItem.put(context.getString(R.string.NAMA_PESERTA).toLowerCase(),
                    propModelMstProposalProductPeserta.getNama_peserta());
            objectItem.put(context.getString(R.string.TGL_LAHIR).toLowerCase(),
                    propModelMstProposalProductPeserta.getTgl_lahir());
            objectItem.put(context.getString(R.string.USIA).toLowerCase(),
                    propModelMstProposalProductPeserta.getUsia());
            objectItem.put(context.getString(R.string.LSRE_ID).toLowerCase(),
                    propModelMstProposalProductPeserta.getLsre_id());
            objectItem.put(context.getString(R.string.PESERTA_KE).toLowerCase(),
                    propModelMstProposalProductPeserta.getPeserta_ke());

            pesertaArray.put(objectItem);
        }

        return pesertaArray;
    }


}
