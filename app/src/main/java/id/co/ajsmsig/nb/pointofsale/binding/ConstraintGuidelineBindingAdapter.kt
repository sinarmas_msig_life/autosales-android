package id.co.ajsmsig.nb.pointofsale.binding

import android.databinding.BindingAdapter
import android.support.annotation.DimenRes
import android.support.constraint.ConstraintLayout
import android.support.constraint.Guideline

/**
 * Created by andreyyoshuamanik on 11/03/18.
 */


@BindingAdapter("app:guideline_percent")
fun setGuidelinePercent(view: Guideline, value: Float) {
    (view.layoutParams as ConstraintLayout.LayoutParams).guidePercent = value
}


@BindingAdapter("app:guideline_begin")
fun setGuidelineBegin(view: Guideline, @DimenRes value: Float) {
    (view.layoutParams as ConstraintLayout.LayoutParams).guideBegin = value.toInt()
}