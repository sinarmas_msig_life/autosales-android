package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelFoto;

/**
 * Created by rizky_c on 30/09/2017.
 */

public class TableDP {


    private Context context;

    public TableDP(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(ModelFoto me, int counter, String SPAJ_ID_TAB) {
        ContentValues cv = new ContentValues();
        cv.put("SPAJ_ID_TAB", SPAJ_ID_TAB);
        cv.put("COUNTER", counter);
        cv.put("FILE_FOTO_DP", me.getFile_foto());
        cv.put("JUDUL_FOTO", me.getJudul_foto());
        cv.put("ID_FOTO", me.getId_foto());
        cv.put("FLAG_DEFAULT", me.getFlag_default());
        cv.put("VALIDATION", me.getValidation());
        cv.put("FLAG_STATUS", me.getFlag_default());

        return cv;
    }
    public ArrayList<ModelFoto> getObjectArray(Cursor cursor) {

        ArrayList<ModelFoto> mes = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ModelFoto me = new ModelFoto();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("COUNTER"))) {
                    me.setCounter(cursor.getInt(cursor.getColumnIndexOrThrow("COUNTER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("FILE_FOTO_DP"))) {
                    me.setFile_foto(cursor.getString(cursor.getColumnIndexOrThrow("FILE_FOTO_DP")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("JUDUL_FOTO"))) {
                    me.setJudul_foto(cursor.getString(cursor.getColumnIndexOrThrow("JUDUL_FOTO")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("ID_FOTO"))) {
                    me.setId_foto(cursor.getString(cursor.getColumnIndexOrThrow("ID_FOTO")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("FLAG_DEFAULT"))) {
                    me.setFlag_default(cursor.getInt(cursor.getColumnIndexOrThrow("FLAG_DEFAULT")));
                }
                if(!cursor.isNull(cursor.getColumnIndexOrThrow("VALIDATION"))){
                    int valid = cursor.getInt(cursor.getColumnIndexOrThrow("VALIDATION"));
//                boolean isValid = valid == 1 ? true : false;
                    boolean val;
                    val = valid == 1;
                    me.setValidation(val);
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("FLAG_STATUS"))) {
                    me.setFlag_status(cursor.getInt(cursor.getColumnIndexOrThrow("FLAG_STATUS")));
                }

                mes.add(me);
                cursor.moveToNext();
            }

            // always close the cursor
            cursor.close();
        }

        return mes;
    }

}
