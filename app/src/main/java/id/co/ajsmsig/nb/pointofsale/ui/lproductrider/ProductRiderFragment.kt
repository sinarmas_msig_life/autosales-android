package id.co.ajsmsig.nb.pointofsale.ui.lproductrider


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosFragmentProductRiderBinding
import id.co.ajsmsig.nb.pointofsale.ui.fragment.BaseFragment


/**
 * A simple [BaseFragment] subclass.
 */
class ProductRiderFragment : BaseFragment() {

    private lateinit var dataBinding: PosFragmentProductRiderBinding

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductRiderVM::class.java)
        subscribeUi(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_product_rider, container, false)

        return dataBinding.root
    }

    fun subscribeUi(viewModel: ProductRiderVM) {
        dataBinding.vm = viewModel
        viewModel.page = page
    }

}// Required empty public constructor
