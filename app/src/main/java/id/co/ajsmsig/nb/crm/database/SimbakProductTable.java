package id.co.ajsmsig.nb.crm.database;
/*
 *Created by faiz_f on 08/05/2017.
 */

import android.content.ContentValues;
import android.content.Context;

import java.util.HashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.form.SimbakProductModel;

public class SimbakProductTable {
    private Context context;

    public SimbakProductTable(Context context) {
        this.context = context;
    }

    public HashMap<String, String> getProjectionMap() {
        HashMap<String, String> projectionMap = new HashMap<>();
        projectionMap.put(context.getString(R.string.SLP_TAB_ID), context.getString(R.string.SLP_TAB_ID) + " AS " + context.getString(R.string._ID));
        projectionMap.put(context.getString(R.string.SLP_ID), context.getString(R.string.SLP_ID));
        projectionMap.put(context.getString(R.string.SLP_INST_TAB_ID), context.getString(R.string.SLP_INST_TAB_ID));
        projectionMap.put(context.getString(R.string.SLP_INST_ID), context.getString(R.string.SLP_INST_ID));
        projectionMap.put(context.getString(R.string.SLP_INST_TYPE), context.getString(R.string.SLP_INST_TYPE));
        projectionMap.put(context.getString(R.string.SLP_LSBS_ID), context.getString(R.string.SLP_LSBS_ID));
        projectionMap.put(context.getString(R.string.SLP_LSBS_NO), context.getString(R.string.SLP_LSBS_NO));
        projectionMap.put(context.getString(R.string.SLP_NAME), context.getString(R.string.SLP_NAME));
        projectionMap.put(context.getString(R.string.SLP_LAST_PREMI), context.getString(R.string.SLP_LAST_PREMI));
        projectionMap.put(context.getString(R.string.SLP_OLD_ID), context.getString(R.string.SLP_OLD_ID));
        projectionMap.put(context.getString(R.string.SLP_CORP_STAGE), context.getString(R.string.SLP_CORP_STAGE));
        projectionMap.put(context.getString(R.string.SLP_ACTIVE), context.getString(R.string.SLP_ACTIVE));
        projectionMap.put(context.getString(R.string.SLP_BRANCH), context.getString(R.string.SLP_BRANCH));
        projectionMap.put(context.getString(R.string.SLP_CAMPAIGN), context.getString(R.string.SLP_CAMPAIGN));
        projectionMap.put(context.getString(R.string.SLP_CREATED), context.getString(R.string.SLP_CREATED));
        projectionMap.put(context.getString(R.string.SLP_CRTD_ID), context.getString(R.string.SLP_CRTD_ID));
        projectionMap.put(context.getString(R.string.SLP_UPDATED), context.getString(R.string.SLP_UPDATED));
        projectionMap.put(context.getString(R.string.SLP_UPDTD_ID), context.getString(R.string.SLP_UPDTD_ID));
        projectionMap.put(context.getString(R.string.SLP_CLOSED), context.getString(R.string.SLP_CLOSED));

        return projectionMap;
    }

    public HashMap<String, String> getProjectionMapLeadProjectGroup() {
        HashMap<String, String> projectionMap = new HashMap<>();
        projectionMap.put(context.getString(R.string.SLP_TAB_ID), context.getString(R.string.SLP_TAB_ID) + " AS " + context.getString(R.string._ID));
        projectionMap.put(context.getString(R.string.SLP_ID), context.getString(R.string.SLP_ID));
        projectionMap.put(context.getString(R.string.SLP_INST_TAB_ID), context.getString(R.string.SLP_INST_TAB_ID));
        projectionMap.put(context.getString(R.string.SLP_INST_ID), context.getString(R.string.SLP_INST_ID));
        projectionMap.put(context.getString(R.string.SLP_INST_TYPE), context.getString(R.string.SLP_INST_TYPE));
        projectionMap.put(context.getString(R.string.SLP_LSBS_ID), context.getString(R.string.SLP_LSBS_ID));
        projectionMap.put(context.getString(R.string.SLP_LSBS_NO), context.getString(R.string.SLP_LSBS_NO));
        projectionMap.put(context.getString(R.string.SLP_NAME), context.getString(R.string.SLP_NAME));
        projectionMap.put(context.getString(R.string.SLP_LAST_PREMI), context.getString(R.string.SLP_LAST_PREMI));
        projectionMap.put(context.getString(R.string.SLP_OLD_ID), context.getString(R.string.SLP_OLD_ID));
        projectionMap.put(context.getString(R.string.SLP_CORP_STAGE), context.getString(R.string.SLP_CORP_STAGE));
        projectionMap.put(context.getString(R.string.SLP_ACTIVE), context.getString(R.string.SLP_ACTIVE));
        projectionMap.put(context.getString(R.string.SLP_BRANCH), context.getString(R.string.SLP_BRANCH));
        projectionMap.put(context.getString(R.string.SLP_CAMPAIGN), context.getString(R.string.SLP_CAMPAIGN));
        projectionMap.put(context.getString(R.string.SLP_CREATED), context.getString(R.string.SLP_CREATED));
        projectionMap.put(context.getString(R.string.SLP_CRTD_ID), context.getString(R.string.SLP_CRTD_ID));
        projectionMap.put(context.getString(R.string.SLP_UPDATED), context.getString(R.string.SLP_UPDATED));
        projectionMap.put(context.getString(R.string.SLP_UPDTD_ID), context.getString(R.string.SLP_UPDTD_ID));


        return projectionMap;
    }

    public ContentValues toContentValues(SimbakProductModel simbakProductModel) {
        ContentValues contentValues = new ContentValues();
        if (simbakProductModel.getSLP_TAB_ID() != null)
            contentValues.put(context.getString(R.string.SLP_TAB_ID), simbakProductModel.getSLP_TAB_ID());

        if (simbakProductModel.getSLP_ID() != null)
            contentValues.put(context.getString(R.string.SLP_ID), simbakProductModel.getSLP_ID());

        if (simbakProductModel.getSLP_INST_ID() != null)
            contentValues.put(context.getString(R.string.SLP_INST_ID), simbakProductModel.getSLP_INST_ID());

        if (simbakProductModel.getSLP_INST_TAB_ID() != null)
            contentValues.put(context.getString(R.string.SLP_INST_TAB_ID), simbakProductModel.getSLP_INST_TAB_ID());

        if (simbakProductModel.getSLP_LSBS_ID() != null)
            contentValues.put(context.getString(R.string.SLP_LSBS_ID), simbakProductModel.getSLP_LSBS_ID());

        if (simbakProductModel.getSLP_LSBS_NO() != null)
            contentValues.put(context.getString(R.string.SLP_LSBS_NO), simbakProductModel.getSLP_LSBS_NO());

        if (simbakProductModel.getSLP_NAME() != null)
            contentValues.put(context.getString(R.string.SLP_NAME), simbakProductModel.getSLP_NAME());

        if (simbakProductModel.getSLP_LAST_PREMI() != null)
            contentValues.put(context.getString(R.string.SLP_LAST_PREMI), simbakProductModel.getSLP_LAST_PREMI());

        if (simbakProductModel.getSLP_OLD_ID() != null)
            contentValues.put(context.getString(R.string.SLP_OLD_ID), simbakProductModel.getSLP_OLD_ID());

        if (simbakProductModel.getSLP_INST_TYPE() != null)
            contentValues.put(context.getString(R.string.SLP_INST_TYPE), simbakProductModel.getSLP_INST_TYPE());

        if (simbakProductModel.getSLP_CORP_STAGE() != null)
            contentValues.put(context.getString(R.string.SLP_CORP_STAGE), simbakProductModel.getSLP_CORP_STAGE());

        if (simbakProductModel.getSLP_ACTIVE() != null)
            contentValues.put(context.getString(R.string.SLP_ACTIVE), simbakProductModel.getSLP_ACTIVE());

        if (simbakProductModel.getSLP_BRANCH() != null)
            contentValues.put(context.getString(R.string.SLP_BRANCH), simbakProductModel.getSLP_BRANCH());

        if (simbakProductModel.getSLP_CAMPAIGN() != null)
            contentValues.put(context.getString(R.string.SLP_CAMPAIGN), simbakProductModel.getSLP_CAMPAIGN());

        if (simbakProductModel.getSLP_CREATED() != null)
            contentValues.put(context.getString(R.string.SLP_CREATED), simbakProductModel.getSLP_CREATED());

        if (simbakProductModel.getSLP_CRTD_ID() != null)
            contentValues.put(context.getString(R.string.SLP_CRTD_ID), simbakProductModel.getSLP_CRTD_ID());

        if (simbakProductModel.getSLP_UPDATED() != null)
            contentValues.put(context.getString(R.string.SLP_UPDATED), simbakProductModel.getSLP_UPDATED());

        if (simbakProductModel.getSLP_UPDTD_ID() != null)
            contentValues.put(context.getString(R.string.SLP_UPDTD_ID), simbakProductModel.getSLP_UPDTD_ID());
        if (simbakProductModel.getSLP_CLOSED() != null)
            contentValues.put(context.getString(R.string.SLP_CLOSED), simbakProductModel.getSLP_CLOSED());


        return contentValues;
    }
}
