package id.co.ajsmsig.nb.pointofsale.model.db.dynamic.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Analytics
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductBenefit


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface AnalyticsDao: BaseDao<Analytics> {

    @Query("select * from Analytics")
    fun loadAllAnalytics(): LiveData<List<Analytics>>


    @Query("select * from Analytics where isSent = 0")
    fun loadUnsentAnalytics(): LiveData<List<Analytics>>


}