package id.co.ajsmsig.nb.crm.model.form;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
  Created by faiz_f on 17/04/2017.
 */

public class SimbakProductModel implements Parcelable {
    private Long SLP_TAB_ID;
    private Long SLP_ID;
    private Long SLP_INST_ID;
    private Long SLP_INST_TAB_ID;
    private String SLP_LSBS_ID;
    private String SLP_LSBS_NO;
    private String SLP_NAME;
    private Integer SLP_LAST_PREMI;
    private String SLP_OLD_ID;
    private Integer SLP_INST_TYPE;
    private Integer SLP_CORP_STAGE;
    private Integer SLP_ACTIVE;
    private String SLP_BRANCH;
    private Integer SLP_CAMPAIGN;
    private String SLP_CREATED;
    private String SLP_CRTD_ID;
    private String SLP_UPDATED;
    private String SLP_UPDTD_ID;
    private Integer SLP_CLOSED;
    private Integer SLP_LAST_PROCESS_LOCATION;
    private ArrayList<SimbakActivityModel> LIST_ACTIVITY = null;

    public SimbakProductModel() {
    }


    protected SimbakProductModel(Parcel in) {
        SLP_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLP_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLP_INST_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLP_INST_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLP_LSBS_ID = in.readString();
        SLP_LSBS_NO = in.readString();
        SLP_NAME = in.readString();
        SLP_LAST_PREMI = (Integer) in.readValue(Integer.class.getClassLoader());
        SLP_OLD_ID = in.readString();
        SLP_INST_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLP_CORP_STAGE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLP_ACTIVE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLP_BRANCH = in.readString();
        SLP_CAMPAIGN = (Integer) in.readValue(Integer.class.getClassLoader());
        SLP_CREATED = in.readString();
        SLP_CRTD_ID = in.readString();
        SLP_UPDATED = in.readString();
        SLP_UPDTD_ID = in.readString();
        SLP_CLOSED = (Integer) in.readValue(Integer.class.getClassLoader());
        SLP_LAST_PROCESS_LOCATION = (Integer) in.readValue(Integer.class.getClassLoader());
        LIST_ACTIVITY = in.createTypedArrayList(SimbakActivityModel.CREATOR);
    }



    public static final Creator<SimbakProductModel> CREATOR = new Creator<SimbakProductModel>() {
        @Override
        public SimbakProductModel createFromParcel(Parcel in) {
            return new SimbakProductModel(in);
        }

        @Override
        public SimbakProductModel[] newArray(int size) {
            return new SimbakProductModel[size];
        }
    };

    public Long getSLP_TAB_ID() {
        return SLP_TAB_ID;
    }

    public void setSLP_TAB_ID(Long SLP_TAB_ID) {
        this.SLP_TAB_ID = SLP_TAB_ID;
    }

    public Long getSLP_ID() {
        return SLP_ID;
    }

    public void setSLP_ID(Long SLP_ID) {
        this.SLP_ID = SLP_ID;
    }

    public Long getSLP_INST_ID() {
        return SLP_INST_ID;
    }

    public void setSLP_INST_ID(Long SLP_INST_ID) {
        this.SLP_INST_ID = SLP_INST_ID;
    }

    public Long getSLP_INST_TAB_ID() {
        return SLP_INST_TAB_ID;
    }

    public void setSLP_INST_TAB_ID(Long SLP_INST_TAB_ID) {
        this.SLP_INST_TAB_ID = SLP_INST_TAB_ID;
    }

    public String getSLP_LSBS_ID() {
        return SLP_LSBS_ID;
    }

    public void setSLP_LSBS_ID(String SLP_LSBS_ID) {
        this.SLP_LSBS_ID = SLP_LSBS_ID;
    }

    public String getSLP_LSBS_NO() {
        return SLP_LSBS_NO;
    }

    public void setSLP_LSBS_NO(String SLP_LSBS_NO) {
        this.SLP_LSBS_NO = SLP_LSBS_NO;
    }

    public String getSLP_NAME() {
        return SLP_NAME;
    }

    public void setSLP_NAME(String SLP_NAME) {
        this.SLP_NAME = SLP_NAME;
    }

    public Integer getSLP_LAST_PREMI() {
        return SLP_LAST_PREMI;
    }

    public void setSLP_LAST_PREMI(Integer SLP_LAST_PREMI) {
        this.SLP_LAST_PREMI = SLP_LAST_PREMI;
    }

    public String getSLP_OLD_ID() {
        return SLP_OLD_ID;
    }

    public void setSLP_OLD_ID(String SLP_OLD_ID) {
        this.SLP_OLD_ID = SLP_OLD_ID;
    }

    public Integer getSLP_INST_TYPE() {
        return SLP_INST_TYPE;
    }

    public void setSLP_INST_TYPE(Integer SLP_INST_TYPE) {
        this.SLP_INST_TYPE = SLP_INST_TYPE;
    }

    public Integer getSLP_CORP_STAGE() {
        return SLP_CORP_STAGE;
    }

    public void setSLP_CORP_STAGE(Integer SLP_CORP_STAGE) {
        this.SLP_CORP_STAGE = SLP_CORP_STAGE;
    }

    public Integer getSLP_ACTIVE() {
        return SLP_ACTIVE;
    }

    public void setSLP_ACTIVE(Integer SLP_ACTIVE) {
        this.SLP_ACTIVE = SLP_ACTIVE;
    }

    public String getSLP_BRANCH() {
        return SLP_BRANCH;
    }

    public void setSLP_BRANCH(String SLP_BRANCH) {
        this.SLP_BRANCH = SLP_BRANCH;
    }

    public Integer getSLP_CAMPAIGN() {
        return SLP_CAMPAIGN;
    }

    public void setSLP_CAMPAIGN(Integer SLP_CAMPAIGN) {
        this.SLP_CAMPAIGN = SLP_CAMPAIGN;
    }

    public String getSLP_CREATED() {
        return SLP_CREATED;
    }

    public void setSLP_CREATED(String SLP_CREATED) {
        this.SLP_CREATED = SLP_CREATED;
    }

    public String getSLP_CRTD_ID() {
        return SLP_CRTD_ID;
    }

    public void setSLP_CRTD_ID(String SLP_CRTD_ID) {
        this.SLP_CRTD_ID = SLP_CRTD_ID;
    }

    public String getSLP_UPDATED() {
        return SLP_UPDATED;
    }

    public void setSLP_UPDATED(String SLP_UPDATED) {
        this.SLP_UPDATED = SLP_UPDATED;
    }

    public String getSLP_UPDTD_ID() {
        return SLP_UPDTD_ID;
    }

    public Integer getSLP_CLOSED() {
        return SLP_CLOSED;
    }

    public void setSLP_CLOSED(Integer SLP_CLOSED) {
        this.SLP_CLOSED = SLP_CLOSED;
    }

    public void setSLP_UPDTD_ID(String SLP_UPDTD_ID) {
        this.SLP_UPDTD_ID = SLP_UPDTD_ID;
    }

    public Integer getSLP_LAST_PROCESS_LOCATION() {
        return SLP_LAST_PROCESS_LOCATION;
    }

    public void setSLP_LAST_PROCESS_LOCATION(Integer SLP_LAST_PROCESS_LOCATION) {
        this.SLP_LAST_PROCESS_LOCATION = SLP_LAST_PROCESS_LOCATION;
    }

    public ArrayList<SimbakActivityModel> getLIST_ACTIVITY() {
        return LIST_ACTIVITY;
    }

    public void setLIST_ACTIVITY(ArrayList<SimbakActivityModel> LIST_ACTIVITY) {
        this.LIST_ACTIVITY = LIST_ACTIVITY;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(SLP_TAB_ID);
        dest.writeValue(SLP_ID);
        dest.writeValue(SLP_INST_ID);
        dest.writeValue(SLP_INST_TAB_ID);
        dest.writeString(SLP_LSBS_ID);
        dest.writeString(SLP_LSBS_NO);
        dest.writeString(SLP_NAME);
        dest.writeValue(SLP_LAST_PREMI);
        dest.writeString(SLP_OLD_ID);
        dest.writeValue(SLP_INST_TYPE);
        dest.writeValue(SLP_CORP_STAGE);
        dest.writeValue(SLP_ACTIVE);
        dest.writeString(SLP_BRANCH);
        dest.writeValue(SLP_CAMPAIGN);
        dest.writeString(SLP_CREATED);
        dest.writeString(SLP_CRTD_ID);
        dest.writeString(SLP_UPDATED);
        dest.writeString(SLP_UPDTD_ID);
        dest.writeValue(SLP_CLOSED);
        dest.writeValue(SLP_LAST_PROCESS_LOCATION);
        dest.writeTypedList(LIST_ACTIVITY);
    }

}
