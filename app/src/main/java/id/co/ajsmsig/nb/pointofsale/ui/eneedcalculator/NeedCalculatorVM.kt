package id.co.ajsmsig.nb.pointofsale.ui.eneedcalculator

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import android.databinding.ObservableField
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Analytics
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.InputAllChoices
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */
class NeedCalculatorVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository, var sharedViewModel: SharedViewModel, val mainRepository: MainRepository):AndroidViewModel(application) {


    val observableInputs = MediatorLiveData<List<InputAllChoices>>()
    val inputs: ObservableField<List<InputAllChoices>> = ObservableField()

    var page: Page? = null
    set(value) {

        sharedViewModel.selectedNeedAnalysis?.let {

            mainRepository.sendAnalytics(Analytics(
                    agentCode = sharedViewModel.agentCode,
                    agentName = sharedViewModel.agentName,
                    group = sharedViewModel.groupId,
                    event = "Need Analytics = ${it.title}",
                    timestamp = System.currentTimeMillis()
            ))
            observableInputs.addSource(repository.getInputForPage(value, it.id), observableInputs::setValue)
        }
    }

}