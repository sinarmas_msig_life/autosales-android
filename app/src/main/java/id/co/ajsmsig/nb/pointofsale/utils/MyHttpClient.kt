package id.co.ajsmsig.nb.pointofsale.utils

import com.downloader.httpclient.HttpClient
import com.downloader.request.DownloadRequest
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLConnection
import java.util.*
import com.downloader.Constants

class MytHttpClient : HttpClient {

    lateinit var connection: URLConnection

    override fun getResponseHeader(name: String?): String {

        return connection.getHeaderField(name)
    }

    override fun clone(): HttpClient {
        return MytHttpClient()
    }

    override fun getResponseCode(): Int {
        var responseCode = 0
        if (connection is HttpURLConnection) {
            responseCode = (connection as HttpURLConnection).responseCode
        }
        return responseCode
    }

    override fun getContentLength(): Long {

        val length = connection.getHeaderField("Content-Length")
        try {
            return length.toLong()
        } catch (e: NumberFormatException) {
            return -1
        }
    }

    override fun connect(request: DownloadRequest?) {
        connection = URL(request?.url).openConnection()
        connection.useCaches = false

        request ?: return

        connection.readTimeout = request.readTimeout
        connection.connectTimeout = request.connectTimeout
        val range = String.format(Locale.ENGLISH,
                "bytes=%d-", request.downloadedBytes)
        connection.addRequestProperty(Constants.RANGE, range)
        connection.addRequestProperty(Constants.USER_AGENT, request.userAgent)
        addHeaders(request)
        connection.connect()
    }

    override fun getInputStream(): InputStream {

        return connection.getInputStream()
    }

    override fun close() {

    }



    fun addHeaders(request: DownloadRequest?) {
        val headers = request?.headers
        if (headers != null) {
            val entries = headers.entries
            for (entry in entries) {
                val name = entry.key
                val list = entry.value
                if (list != null) {
                    for (value in list) {
                        connection.addRequestProperty(name, value)
                    }
                }
            }
        }
    }

}
