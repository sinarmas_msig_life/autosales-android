
package id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.bancass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meetandpre {

    @SerializedName("last_month")
    @Expose
    private Integer lastMonth;
    @SerializedName("current_month")
    @Expose
    private Integer currentMonth;
    @SerializedName("today")
    @Expose
    private Integer today;

    public Integer getLastMonth() {
        return lastMonth;
    }

    public void setLastMonth(Integer lastMonth) {
        this.lastMonth = lastMonth;
    }

    public Integer getCurrentMonth() {
        return currentMonth;
    }

    public void setCurrentMonth(Integer currentMonth) {
        this.currentMonth = currentMonth;
    }

    public Integer getToday() {
        return today;
    }

    public void setToday(Integer today) {
        this.today = today;
    }

}
