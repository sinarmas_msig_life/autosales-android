package id.co.ajsmsig.nb.espaj.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.graphics.pdf.PdfRenderer;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.print.pdf.PrintedPdfDocument;
import android.view.View;
import android.widget.Button;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.espaj.adapter.Adapter_Pdf;
import id.co.ajsmsig.nb.espaj.method.ExtendedViewPager;
import id.co.ajsmsig.nb.espaj.method.FormatNumber;
import id.co.ajsmsig.nb.espaj.model.EspajModel;

/**
 * Created by eriza on 02/11/2017.
 */
@SuppressLint("NewApi")
public class E_PdfReader extends Activity implements View.OnClickListener {

    private EspajModel me;
    private File file;
    private ExtendedViewPager pager;
    private PdfRenderer renderer;
    private String alamat_1 = "";
    private String alamat_2 = "";
    private Button btn_kembali, btn_lanjut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.e_activity_pdfreader);
//            file = intent.getParcelableExtra(getString(R.string.file_output));
        me = E_MainActivity.e_bundle.getParcelable(getString(R.string.modelespaj));
        btn_kembali = findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(this);
        btn_lanjut = findViewById(R.id.btn_lanjut);
        btn_lanjut.setOnClickListener(this);
        btn_lanjut.setText("CETAK");

        try {
            FileOutputStream outStream = null;
            Font font = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED, 0.8f, Font.NORMAL, BaseColor.BLACK);
            BaseFont arial_narow = font.getBaseFont();
            File myFilesDir = new File(getFilesDir() + "/ESPAJ/SERTIFIKAT/");
            if (!myFilesDir.exists()) {
                myFilesDir.mkdirs();
            }
            file = new File(myFilesDir, "SERTIFIKAT" + me.getModelID().getSertifikat() + ".PDF");
            PdfReader reader = new PdfReader(this.getAssets().open("Sertifikat.pdf"));
            outStream = new FileOutputStream(file);
            PdfStamper stamper = new PdfStamper(reader, outStream);
            PdfContentByte content = stamper.getOverContent(1);
            content.beginText();
            content.setFontAndSize(arial_narow, 8f);
            if (me.getPemegangPolisModel().getAlamat_pp().length() > 46) {
                alamat_1 = me.getPemegangPolisModel().getAlamat_pp().substring(0, 45);
                alamat_2 = me.getPemegangPolisModel().getAlamat_pp().substring(46, me.getPemegangPolisModel().getAlamat_pp().length());
            } else {
                alamat_1 = me.getPemegangPolisModel().getAlamat_pp();
            }
//								alamat_1 = modelespaj.getModelPemegangPolis().getAlamat_pp().substring(0,40);
//								alamat_2 = modelespaj.getModelPemegangPolis().getAlamat_pp().substring(41,80);
            content.showTextAligned(PdfContentByte.ALIGN_LEFT, me.getModelID().getSertifikat(), 115, 744, 0);
            content.showTextAligned(PdfContentByte.ALIGN_LEFT, me.getPemegangPolisModel().getNama_pp(), 115, 730, 0);
            content.showTextAligned(PdfContentByte.ALIGN_LEFT, me.getPemegangPolisModel().getTtl_pp() + " / " + me.getPemegangPolisModel().getUsia_pp() + " Tahun", 115, 717, 0);
            content.showTextAligned(PdfContentByte.ALIGN_LEFT, me.getUsulanAsuransiModel().getAwalpertanggungan_ua(), 115, 700, 0);
            content.showTextAligned(PdfContentByte.ALIGN_LEFT, me.getUsulanAsuransiModel().getAwalpertanggungan_ua() + " s/d " + me.getUsulanAsuransiModel().getAkhirpertanggungan_ua(), 115, 683, 0);
            content.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat_1, 115, 670, 0);
            if (alamat_2.equals("")) {
                content.showTextAligned(PdfContentByte.ALIGN_LEFT, me.getPemegangPolisModel().getKota_pp() + " , " + me.getPemegangPolisModel().getKdpos_pp(), 115, 660, 0);
            } else {
                content.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat_2, 115, 660, 0);
                content.showTextAligned(PdfContentByte.ALIGN_LEFT, me.getPemegangPolisModel().getKota_pp() + " , " + me.getPemegangPolisModel().getKdpos_pp(), 115, 650, 0);
            }
            content.showTextAligned(PdfContentByte.ALIGN_LEFT, me.getTertanggungModel().getNama_tt(), 398, 744, 0);
            content.showTextAligned(PdfContentByte.ALIGN_LEFT, me.getTertanggungModel().getTtl_tt() + " / " + me.getTertanggungModel().getUsia_tt() + " Tahun", 398, 730, 0);
            content.showTextAligned(PdfContentByte.ALIGN_LEFT, "Rp. " + FormatNumber.StringCurency(me.getUsulanAsuransiModel().getUnitlink_premistandard_ua()) + ",- per bulan", 398, 700, 0);
            content.showTextAligned(PdfContentByte.ALIGN_LEFT, "Dibayarkan sekaligus Rp. " + FormatNumber.StringCurency(me.getUsulanAsuransiModel().getUp_ua()) + "; Atau", 398, 685, 0);
            content.showTextAligned(PdfContentByte.ALIGN_LEFT, "Dibayarkan bulanan Rp. " + FormatNumber.StringCurency(me.getUsulanAsuransiModel().getUnitlink_premistandard_ua() * 10) + " selama 5 tahun", 398, 673, 0);

            content.showTextAligned(PdfContentByte.ALIGN_LEFT, me.getUsulanAsuransiModel().getAwalpertanggungan_ua(), 430, 638, 0);
            content.endText();
            stamper.close();
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                renderer = new PdfRenderer(ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY));
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        pager = findViewById(R.id.pager);
        pager.setAdapter(new Adapter_Pdf(this, renderer));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_kembali:
                finish();
                break;
            case R.id.btn_lanjut:
                PrintManager printManager = (PrintManager) this.getSystemService(PRINT_SERVICE);
                String jobName = getString(R.string.app_name) + " Document";
                printManager.print(jobName, new MyPrintDocumentAdapter(this),
                        null);
                break;
        }
    }

    public class MyPrintDocumentAdapter extends PrintDocumentAdapter{
        Context context;
        private int pageHeight;
        private int pageWidth;
        public PdfDocument myPdfDocument;
        public int totalpages = 2;

        public MyPrintDocumentAdapter(Context context)
        {
            this.context = context;
        }

        @Override
        public void onLayout(PrintAttributes oldAttributes,
                             PrintAttributes newAttributes,
                             CancellationSignal cancellationSignal,
                             LayoutResultCallback callback,
                             Bundle metadata) {

            myPdfDocument = new PrintedPdfDocument(context, newAttributes);

            pageHeight =
                    newAttributes.getMediaSize().getHeightMils()/1000 * 72;
            pageWidth =
                    newAttributes.getMediaSize().getWidthMils()/1000 * 72;

            if (cancellationSignal.isCanceled() ) {
                callback.onLayoutCancelled();
                return;
            }

            if (totalpages > 0) {
                PrintDocumentInfo.Builder builder = new PrintDocumentInfo
                        .Builder("print_output.pdf")
                        .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                        .setPageCount(totalpages);

                PrintDocumentInfo info = builder.build();
                callback.onLayoutFinished(info, true);
            } else {
                callback.onLayoutFailed("Page count is zero.");
            }

        }

        @Override
        public void onWrite(final PageRange[] pageRanges,
                            final ParcelFileDescriptor destination,
                            final CancellationSignal cancellationSignal,
                            final WriteResultCallback callback) {

            FileInputStream input = null;
            FileOutputStream output = null;

            try {
                input = new FileInputStream(file);
                output = new FileOutputStream(destination.getFileDescriptor());

                byte[] buf = new byte [1024];
                int bytesRead;

                while ((bytesRead = input.read(buf)) > 0){
                    output.write(buf, 0, bytesRead);
                }

                callback.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES});

            } catch (FileNotFoundException ee) {

            } catch (Exception e){

            }finally {
                try {
                    input.close();
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

    }
}
