package id.co.ajsmsig.nb.crm.database;
/*
 *Created by faiz_f on 08/05/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.HashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.form.SimbakLeadModel;

public class SimbakLeadTable {
    private Context context;

    public SimbakLeadTable(Context context) {
        this.context = context;
    }

    public HashMap<String, String> getProjectionMap() {
        HashMap<String, String> projectionMap = new HashMap<>();
        projectionMap.put(context.getString(R.string.SL_TAB_ID), context.getString(R.string.SL_TAB_ID) + " AS " + context.getString(R.string._ID));
        projectionMap.put(context.getString(R.string.SL_ID), context.getString(R.string.SL_ID));
        projectionMap.put(context.getString(R.string.SL_NAME), context.getString(R.string.SL_NAME));
        projectionMap.put(context.getString(R.string.SL_APP), context.getString(R.string.SL_APP));
        projectionMap.put(context.getString(R.string.SL_CREATED), context.getString(R.string.SL_CREATED));
        projectionMap.put(context.getString(R.string.SL_CRTD_DATE), context.getString(R.string.SL_CRTD_DATE));
        projectionMap.put(context.getString(R.string.SL_UPDATED), context.getString(R.string.SL_UPDATED));
        projectionMap.put(context.getString(R.string.SL_UPTD_DATE), context.getString(R.string.SL_UPTD_DATE));
        projectionMap.put(context.getString(R.string.SL_ACTIVE), context.getString(R.string.SL_ACTIVE));
        projectionMap.put(context.getString(R.string.SL_RATE), context.getString(R.string.SL_RATE));
        projectionMap.put(context.getString(R.string.SL_REMARK), context.getString(R.string.SL_REMARK));
        projectionMap.put(context.getString(R.string.SL_INIT), context.getString(R.string.SL_INIT));
        projectionMap.put(context.getString(R.string.SL_SRC), context.getString(R.string.SL_SRC));
        projectionMap.put(context.getString(R.string.SL_CHAR), context.getString(R.string.SL_CHAR));
        projectionMap.put(context.getString(R.string.SL_STATE), context.getString(R.string.SL_STATE));
        projectionMap.put(context.getString(R.string.SL_MOTHER), context.getString(R.string.SL_MOTHER));
        projectionMap.put(context.getString(R.string.SL_CITIZEN), context.getString(R.string.SL_CITIZEN));
        projectionMap.put(context.getString(R.string.SL_CORP_POS), context.getString(R.string.SL_CORP_POS));
        projectionMap.put(context.getString(R.string.SL_GENDER), context.getString(R.string.SL_GENDER));
        projectionMap.put(context.getString(R.string.SL_RELIGION), context.getString(R.string.SL_RELIGION));
        projectionMap.put(context.getString(R.string.SL_PERSONAL_REL), context.getString(R.string.SL_PERSONAL_REL));
        projectionMap.put(context.getString(R.string.SL_BPLACE), context.getString(R.string.SL_BPLACE));
        projectionMap.put(context.getString(R.string.SL_LAST_EDU), context.getString(R.string.SL_LAST_EDU));
        projectionMap.put(context.getString(R.string.SL_POLIS_PURPOSE), context.getString(R.string.SL_POLIS_PURPOSE));
        projectionMap.put(context.getString(R.string.SL_PAYMENT_SOURCE), context.getString(R.string.SL_PAYMENT_SOURCE));
        projectionMap.put(context.getString(R.string.SL_OTHER_TYPE), context.getString(R.string.SL_OTHER_TYPE));
        projectionMap.put(context.getString(R.string.SL_IDNO), context.getString(R.string.SL_IDNO));
        projectionMap.put(context.getString(R.string.SL_BDATE), context.getString(R.string.SL_BDATE));
        projectionMap.put(context.getString(R.string.SL_CSTMR_FUND_TYPE), context.getString(R.string.SL_CSTMR_FUND_TYPE));
        projectionMap.put(context.getString(R.string.SL_RES_PREMIUM), context.getString(R.string.SL_RES_PREMIUM));
        projectionMap.put(context.getString(R.string.SL_SALARY), context.getString(R.string.SL_SALARY));
        projectionMap.put(context.getString(R.string.SL_BAC_REFF), context.getString(R.string.SL_BAC_REFF));
        projectionMap.put(context.getString(R.string.SL_BAC_REFF_NAME), context.getString(R.string.SL_BAC_REFF_NAME));
        projectionMap.put(context.getString(R.string.SL_BUYER_FUND_TYPE), context.getString(R.string.SL_BUYER_FUND_TYPE));
        projectionMap.put(context.getString(R.string.SL_PEND_ID), context.getString(R.string.SL_PEND_ID));
        projectionMap.put(context.getString(R.string.SL_APPROVE), context.getString(R.string.SL_APPROVE));
        projectionMap.put(context.getString(R.string.SL_REQUEST), context.getString(R.string.SL_REQUEST));
        projectionMap.put(context.getString(R.string.SL_ALASAN), context.getString(R.string.SL_ALASAN));
        projectionMap.put(context.getString(R.string.SL_NO_NASABAH), context.getString(R.string.SL_NO_NASABAH));
        projectionMap.put(context.getString(R.string.SL_APPROVER), context.getString(R.string.SL_APPROVER));
        projectionMap.put(context.getString(R.string.SL_BAC_FIRST_BRANCH), context.getString(R.string.SL_BAC_FIRST_BRANCH));
        projectionMap.put(context.getString(R.string.SL_BAC_CURR_BRANCH), context.getString(R.string.SL_BAC_CURR_BRANCH));
        projectionMap.put(context.getString(R.string.SL_MALL_GUESTCODE), context.getString(R.string.SL_MALL_GUESTCODE));
        projectionMap.put(context.getString(R.string.SL_MALL_MALLCODE), context.getString(R.string.SL_MALL_MALLCODE));
        projectionMap.put(context.getString(R.string.SL_MALL_ROOMCODE), context.getString(R.string.SL_MALL_ROOMCODE));
        projectionMap.put(context.getString(R.string.SL_ROOMOUT), context.getString(R.string.SL_ROOMOUT));
        projectionMap.put(context.getString(R.string.SL_CORRESPONDENT), context.getString(R.string.SL_CORRESPONDENT));
        projectionMap.put(context.getString(R.string.SL_ROOMIN), context.getString(R.string.SL_ROOMIN));
        projectionMap.put(context.getString(R.string.SL_BAC_REFF_TITLE), context.getString(R.string.SL_BAC_REFF_TITLE));
        projectionMap.put(context.getString(R.string.SL_OCCUPATION), context.getString(R.string.SL_OCCUPATION));
        projectionMap.put(context.getString(R.string.SL_IDEXP), context.getString(R.string.SL_IDEXP));
        projectionMap.put(context.getString(R.string.SL_NPWP), context.getString(R.string.SL_NPWP));
        projectionMap.put(context.getString(R.string.SL_MARRIAGE), context.getString(R.string.SL_MARRIAGE));
        projectionMap.put(context.getString(R.string.SL_OWNERID), context.getString(R.string.SL_OWNERID));
        projectionMap.put(context.getString(R.string.SL_OWNERTYPE), context.getString(R.string.SL_OWNERTYPE));
        projectionMap.put(context.getString(R.string.SL_CIF), context.getString(R.string.SL_CIF));
        projectionMap.put(context.getString(R.string.SL_PINBB), context.getString(R.string.SL_PINBB));
        projectionMap.put(context.getString(R.string.SL_OLD_ID), context.getString(R.string.SL_OLD_ID));
        projectionMap.put(context.getString(R.string.SL_TYPE), context.getString(R.string.SL_TYPE));
        projectionMap.put(context.getString(R.string.SL_LAST_POS), context.getString(R.string.SL_LAST_POS));
        projectionMap.put(context.getString(R.string.SL_FAV), context.getString(R.string.SL_FAV));
        projectionMap.put(context.getString(R.string.SL_NAMA_CABANG), context.getString(R.string.SL_NAMA_CABANG));

        return projectionMap;
    }

    public HashMap<String, String> getProjectionMapJoinSimbakLinkUsrToLead() {
        HashMap<String, String> projectionMap = new HashMap<>();
        projectionMap.put(context.getString(R.string.SL_TAB_ID), context.getString(R.string.SL_TAB_ID) + " AS " + context.getString(R.string._ID));
        projectionMap.put(context.getString(R.string.SL_ID), context.getString(R.string.SL_ID));
        projectionMap.put(context.getString(R.string.SL_NAME), context.getString(R.string.SL_NAME));
        projectionMap.put(context.getString(R.string.SL_BDATE), context.getString(R.string.SL_BDATE));
        projectionMap.put(context.getString(R.string.SL_UMUR), context.getString(R.string.SL_UMUR));
        projectionMap.put(context.getString(R.string.SL_LAST_POS), context.getString(R.string.SL_LAST_POS));
        projectionMap.put(context.getString(R.string.SL_BAC_CURR_BRANCH), context.getString(R.string.SL_BAC_CURR_BRANCH));
        projectionMap.put(context.getString(R.string.SL_FAV), context.getString(R.string.SL_FAV));
        projectionMap.put(context.getString(R.string.SL_NAMA_CABANG), context.getString(R.string.SL_NAMA_CABANG));
        return projectionMap;
    }

    public HashMap<String, String> getProjectionMapProfile() {
        HashMap<String, String> projectionMap = new HashMap<>();
        projectionMap.put(context.getString(R.string.SL_TAB_ID), context.getString(R.string.SL_TAB_ID) + " AS " + context.getString(R.string._ID));
        projectionMap.put(context.getString(R.string.SL_NAME), context.getString(R.string.SL_NAME));
        projectionMap.put(context.getString(R.string.SL_BDATE), context.getString(R.string.SL_BDATE));

        return projectionMap;
    }

    public ContentValues toContentValues(SimbakLeadModel simbakLeadModel) {
        ContentValues contentValues = new ContentValues();

        if (simbakLeadModel.getSL_TAB_ID() != null)
            contentValues.put(context.getString(R.string.SL_TAB_ID), simbakLeadModel.getSL_TAB_ID());
        if (simbakLeadModel.getSL_ID() != null)
            contentValues.put(context.getString(R.string.SL_ID), simbakLeadModel.getSL_ID());
        if (simbakLeadModel.getSL_NAME() != null)
            contentValues.put(context.getString(R.string.SL_NAME), simbakLeadModel.getSL_NAME());
        if (simbakLeadModel.getSL_CREATED() != null)
            contentValues.put(context.getString(R.string.SL_CREATED), simbakLeadModel.getSL_CREATED());
        if (simbakLeadModel.getSL_CRTD_DATE() != null)
            contentValues.put(context.getString(R.string.SL_CRTD_DATE), simbakLeadModel.getSL_CRTD_DATE());
        if (simbakLeadModel.getSL_UPDATED() != null)
            contentValues.put(context.getString(R.string.SL_UPDATED), simbakLeadModel.getSL_UPDATED());
        if (simbakLeadModel.getSL_UPTD_DATE() != null)
            contentValues.put(context.getString(R.string.SL_UPTD_DATE), simbakLeadModel.getSL_UPTD_DATE());
        if (simbakLeadModel.getSL_RATE() != null)
            contentValues.put(context.getString(R.string.SL_RATE), simbakLeadModel.getSL_RATE());
        if (simbakLeadModel.getSL_REMARK() != null)
            contentValues.put(context.getString(R.string.SL_REMARK), simbakLeadModel.getSL_REMARK());
        if (simbakLeadModel.getSL_INIT() != null)
            contentValues.put(context.getString(R.string.SL_INIT), simbakLeadModel.getSL_INIT());
        if (simbakLeadModel.getSL_MOTHER() != null)
            contentValues.put(context.getString(R.string.SL_MOTHER), simbakLeadModel.getSL_MOTHER());
        if (simbakLeadModel.getSL_CITIZEN() != null)
            contentValues.put(context.getString(R.string.SL_CITIZEN), simbakLeadModel.getSL_CITIZEN());
        if (simbakLeadModel.getSL_BPLACE() != null)
            contentValues.put(context.getString(R.string.SL_BPLACE), simbakLeadModel.getSL_BPLACE());
        if (simbakLeadModel.getSL_UMUR() != null)
            contentValues.put(context.getString(R.string.SL_UMUR), simbakLeadModel.getSL_UMUR());
        if (simbakLeadModel.getSL_IDNO() != null)
            contentValues.put(context.getString(R.string.SL_IDNO), simbakLeadModel.getSL_IDNO());
        if (simbakLeadModel.getSL_BDATE() != null)
            contentValues.put(context.getString(R.string.SL_BDATE), simbakLeadModel.getSL_BDATE());
        if (simbakLeadModel.getSL_RES_PREMIUM() != null)
            contentValues.put(context.getString(R.string.SL_RES_PREMIUM), simbakLeadModel.getSL_RES_PREMIUM());
        if (simbakLeadModel.getSL_BAC_REFF() != null)
            contentValues.put(context.getString(R.string.SL_BAC_REFF), simbakLeadModel.getSL_BAC_REFF());
        if (simbakLeadModel.getSL_BAC_REFF_NAME() != null)
            contentValues.put(context.getString(R.string.SL_BAC_REFF_NAME), simbakLeadModel.getSL_BAC_REFF_NAME());
        if (simbakLeadModel.getSL_PEND_ID() != null)
            contentValues.put(context.getString(R.string.SL_PEND_ID), simbakLeadModel.getSL_PEND_ID());
        if (simbakLeadModel.getSL_APPROVE() != null)
            contentValues.put(context.getString(R.string.SL_APPROVE), simbakLeadModel.getSL_APPROVE());
        if (simbakLeadModel.getSL_REQUEST() != null)
            contentValues.put(context.getString(R.string.SL_REQUEST), simbakLeadModel.getSL_REQUEST());
        if (simbakLeadModel.getSL_ALASAN() != null)
            contentValues.put(context.getString(R.string.SL_ALASAN), simbakLeadModel.getSL_ALASAN());
        if (simbakLeadModel.getSL_NO_NASABAH() != null)
            contentValues.put(context.getString(R.string.SL_NO_NASABAH), simbakLeadModel.getSL_NO_NASABAH());
        if (simbakLeadModel.getSL_APPROVER() != null)
            contentValues.put(context.getString(R.string.SL_APPROVER), simbakLeadModel.getSL_APPROVER());
        if (simbakLeadModel.getSL_BAC_CURR_BRANCH() != null)
            contentValues.put(context.getString(R.string.SL_BAC_CURR_BRANCH), simbakLeadModel.getSL_BAC_CURR_BRANCH());
        if (simbakLeadModel.getSL_ROOMOUT() != null)
            contentValues.put(context.getString(R.string.SL_ROOMOUT), simbakLeadModel.getSL_ROOMOUT());
        if (simbakLeadModel.getSL_ROOMIN() != null)
            contentValues.put(context.getString(R.string.SL_ROOMIN), simbakLeadModel.getSL_ROOMIN());
        if (simbakLeadModel.getSL_OCCUPATION() != null)
            contentValues.put(context.getString(R.string.SL_OCCUPATION), simbakLeadModel.getSL_OCCUPATION());
        if (simbakLeadModel.getSL_IDEXP() != null)
            contentValues.put(context.getString(R.string.SL_IDEXP), simbakLeadModel.getSL_IDEXP());
        if (simbakLeadModel.getSL_NPWP() != null)
            contentValues.put(context.getString(R.string.SL_NPWP), simbakLeadModel.getSL_NPWP());
        if (simbakLeadModel.getSL_OWNERID() != null)
            contentValues.put(context.getString(R.string.SL_OWNERID), simbakLeadModel.getSL_OWNERID());
        if (simbakLeadModel.getSL_CIF() != null)
            contentValues.put(context.getString(R.string.SL_CIF), simbakLeadModel.getSL_CIF());
        if (simbakLeadModel.getSL_PINBB() != null)
            contentValues.put(context.getString(R.string.SL_PINBB), simbakLeadModel.getSL_PINBB());
        if (simbakLeadModel.getSL_OLD_ID() != null)
            contentValues.put(context.getString(R.string.SL_OLD_ID), simbakLeadModel.getSL_OLD_ID());
        if (simbakLeadModel.getSL_APP() != null)
            contentValues.put(context.getString(R.string.SL_APP), simbakLeadModel.getSL_APP());
        if (simbakLeadModel.getSL_ACTIVE() != null)
            contentValues.put(context.getString(R.string.SL_ACTIVE), simbakLeadModel.getSL_ACTIVE());
        if (simbakLeadModel.getSL_SRC() != null)
            contentValues.put(context.getString(R.string.SL_SRC), simbakLeadModel.getSL_SRC());
        if (simbakLeadModel.getSL_CHAR() != null)
            contentValues.put(context.getString(R.string.SL_CHAR), simbakLeadModel.getSL_CHAR());
        if (simbakLeadModel.getSL_STATE() != null)
            contentValues.put(context.getString(R.string.SL_STATE), simbakLeadModel.getSL_STATE());
        if (simbakLeadModel.getSL_CORP_POS() != null)
            contentValues.put(context.getString(R.string.SL_CORP_POS), simbakLeadModel.getSL_CORP_POS());
        if (simbakLeadModel.getSL_GENDER() != null)
            contentValues.put(context.getString(R.string.SL_GENDER), simbakLeadModel.getSL_GENDER());
        if (simbakLeadModel.getSL_RELIGION() != null)
            contentValues.put(context.getString(R.string.SL_RELIGION), simbakLeadModel.getSL_RELIGION());
        if (simbakLeadModel.getSL_PERSONAL_REL() != null)
            contentValues.put(context.getString(R.string.SL_PERSONAL_REL), simbakLeadModel.getSL_PERSONAL_REL());
        if (simbakLeadModel.getSL_LAST_EDU() != null)
            contentValues.put(context.getString(R.string.SL_LAST_EDU), simbakLeadModel.getSL_LAST_EDU());
        if (simbakLeadModel.getSL_POLIS_PURPOSE() != null)
            contentValues.put(context.getString(R.string.SL_POLIS_PURPOSE), simbakLeadModel.getSL_POLIS_PURPOSE());
        if (simbakLeadModel.getSL_PAYMENT_SOURCE() != null)
            contentValues.put(context.getString(R.string.SL_PAYMENT_SOURCE), simbakLeadModel.getSL_PAYMENT_SOURCE());
        if (simbakLeadModel.getSL_OTHER_TYPE() != null)
            contentValues.put(context.getString(R.string.SL_OTHER_TYPE), simbakLeadModel.getSL_OTHER_TYPE());
        if (simbakLeadModel.getSL_CSTMR_FUND_TYPE() != null)
            contentValues.put(context.getString(R.string.SL_CSTMR_FUND_TYPE), simbakLeadModel.getSL_CSTMR_FUND_TYPE());
        if (simbakLeadModel.getSL_IDTYPE() != null)
            contentValues.put(context.getString(R.string.SL_IDTYPE), simbakLeadModel.getSL_IDTYPE());
        if (simbakLeadModel.getSL_SALARY() != null)
            contentValues.put(context.getString(R.string.SL_SALARY), simbakLeadModel.getSL_SALARY());
        if (simbakLeadModel.getSL_BUYER_FUND_TYPE() != null)
            contentValues.put(context.getString(R.string.SL_BUYER_FUND_TYPE), simbakLeadModel.getSL_BUYER_FUND_TYPE());
        if (simbakLeadModel.getSL_BAC_FIRST_BRANCH() != null)
            contentValues.put(context.getString(R.string.SL_BAC_FIRST_BRANCH), simbakLeadModel.getSL_BAC_FIRST_BRANCH());
        if (simbakLeadModel.getSL_MALL_GUESTCODE() != null)
            contentValues.put(context.getString(R.string.SL_MALL_GUESTCODE), simbakLeadModel.getSL_MALL_GUESTCODE());
        if (simbakLeadModel.getSL_MALL_MALLCODE() != null)
            contentValues.put(context.getString(R.string.SL_MALL_MALLCODE), simbakLeadModel.getSL_MALL_MALLCODE());
        if (simbakLeadModel.getSL_MALL_ROOMCODE() != null)
            contentValues.put(context.getString(R.string.SL_MALL_ROOMCODE), simbakLeadModel.getSL_MALL_ROOMCODE());
        if (simbakLeadModel.getSL_CORRESPONDENT() != null)
            contentValues.put(context.getString(R.string.SL_CORRESPONDENT), simbakLeadModel.getSL_CORRESPONDENT());
        if (simbakLeadModel.getSL_BAC_REFF_TITLE() != null)
            contentValues.put(context.getString(R.string.SL_BAC_REFF_TITLE), simbakLeadModel.getSL_BAC_REFF_TITLE());
        if (simbakLeadModel.getSL_MARRIAGE() != null)
            contentValues.put(context.getString(R.string.SL_MARRIAGE), simbakLeadModel.getSL_MARRIAGE());
        if (simbakLeadModel.getSL_OWNERTYPE() != null)
            contentValues.put(context.getString(R.string.SL_OWNERTYPE), simbakLeadModel.getSL_OWNERTYPE());
        if (simbakLeadModel.getSL_TYPE() != null)
            contentValues.put(context.getString(R.string.SL_TYPE), simbakLeadModel.getSL_TYPE());
        if (simbakLeadModel.getSL_LAST_POS() != null)
            contentValues.put(context.getString(R.string.SL_LAST_POS), simbakLeadModel.getSL_LAST_POS());
        if (simbakLeadModel.getSL_FAV() != null)
            contentValues.put(context.getString(R.string.SL_FAV), simbakLeadModel.getSL_FAV());
        if (simbakLeadModel.getSL_NAMA_CABANG() != null) {
            contentValues.put(context.getString(R.string.SL_NAMA_CABANG), simbakLeadModel.getSL_NAMA_CABANG());
        }

        return contentValues;
    }

    public SimbakLeadModel getObject(Cursor cursor) {
        SimbakLeadModel simbakLeadModel = new SimbakLeadModel();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_TAB_ID))))
                simbakLeadModel.setSL_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SL_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ID))))
                simbakLeadModel.setSL_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SL_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NAME))))
                simbakLeadModel.setSL_NAME(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NAME))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_APP))))
                simbakLeadModel.setSL_APP(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_APP))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CREATED))))
                simbakLeadModel.setSL_CREATED(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_CREATED))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CRTD_DATE))))
                simbakLeadModel.setSL_CRTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_CRTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_UPDATED))))
                simbakLeadModel.setSL_UPDATED(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_UPDATED))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_UPTD_DATE))))
                simbakLeadModel.setSL_UPTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_UPTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ACTIVE))))
                simbakLeadModel.setSL_ACTIVE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_ACTIVE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_RATE))))
                simbakLeadModel.setSL_RATE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_RATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_REMARK))))
                simbakLeadModel.setSL_REMARK(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_REMARK))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_INIT))))
                simbakLeadModel.setSL_INIT(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_INIT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_SRC))))
                simbakLeadModel.setSL_SRC(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_SRC))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CHAR))))
                simbakLeadModel.setSL_CHAR(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CHAR))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_STATE))))
                simbakLeadModel.setSL_STATE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_STATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MOTHER))))
                simbakLeadModel.setSL_MOTHER(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_MOTHER))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CITIZEN))))
                simbakLeadModel.setSL_CITIZEN(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CITIZEN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CORP_POS))))
                simbakLeadModel.setSL_CORP_POS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CORP_POS))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_GENDER))))
                simbakLeadModel.setSL_GENDER(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_GENDER))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_RELIGION))))
                simbakLeadModel.setSL_RELIGION(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_RELIGION))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PERSONAL_REL))))
                simbakLeadModel.setSL_PERSONAL_REL(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_PERSONAL_REL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BPLACE))))
                simbakLeadModel.setSL_BPLACE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BPLACE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_LAST_EDU))))
                simbakLeadModel.setSL_LAST_EDU(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_LAST_EDU))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_POLIS_PURPOSE))))
                simbakLeadModel.setSL_POLIS_PURPOSE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_POLIS_PURPOSE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PAYMENT_SOURCE))))
                simbakLeadModel.setSL_PAYMENT_SOURCE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_PAYMENT_SOURCE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OTHER_TYPE))))
                simbakLeadModel.setSL_OTHER_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_OTHER_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_IDNO))))
                simbakLeadModel.setSL_IDNO(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_IDNO))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BDATE))))
                simbakLeadModel.setSL_BDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BDATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_UMUR))))
                simbakLeadModel.setSL_UMUR(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_UMUR))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CSTMR_FUND_TYPE))))
                simbakLeadModel.setSL_CSTMR_FUND_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CSTMR_FUND_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_IDTYPE))))
                simbakLeadModel.setSL_IDTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_IDTYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_RES_PREMIUM))))
                simbakLeadModel.setSL_RES_PREMIUM(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_RES_PREMIUM))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_SALARY))))
                simbakLeadModel.setSL_SALARY(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_SALARY))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF))))
                simbakLeadModel.setSL_BAC_REFF(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF_NAME))))
                simbakLeadModel.setSL_BAC_REFF_NAME(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF_NAME))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BUYER_FUND_TYPE))))
                simbakLeadModel.setSL_BUYER_FUND_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_BUYER_FUND_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PEND_ID))))
                simbakLeadModel.setSL_PEND_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_PEND_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_APPROVE))))
                simbakLeadModel.setSL_APPROVE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_APPROVE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_REQUEST))))
                simbakLeadModel.setSL_REQUEST(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_REQUEST))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ALASAN))))
                simbakLeadModel.setSL_ALASAN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_ALASAN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NO_NASABAH))))
                simbakLeadModel.setSL_NO_NASABAH(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NO_NASABAH))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_APPROVER))))
                simbakLeadModel.setSL_APPROVER(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_APPROVER))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_FIRST_BRANCH))))
                simbakLeadModel.setSL_BAC_FIRST_BRANCH(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_FIRST_BRANCH))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_CURR_BRANCH))))
                simbakLeadModel.setSL_BAC_CURR_BRANCH(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_BAC_CURR_BRANCH))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MALL_GUESTCODE))))
                simbakLeadModel.setSL_MALL_GUESTCODE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MALL_GUESTCODE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MALL_MALLCODE))))
                simbakLeadModel.setSL_MALL_MALLCODE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MALL_MALLCODE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MALL_ROOMCODE))))
                simbakLeadModel.setSL_MALL_ROOMCODE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MALL_ROOMCODE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ROOMOUT))))
                simbakLeadModel.setSL_ROOMOUT(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_ROOMOUT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CORRESPONDENT))))
                simbakLeadModel.setSL_CORRESPONDENT(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_CORRESPONDENT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_ROOMIN))))
                simbakLeadModel.setSL_ROOMIN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_ROOMIN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF_TITLE))))
                simbakLeadModel.setSL_BAC_REFF_TITLE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_BAC_REFF_TITLE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OCCUPATION))))
                simbakLeadModel.setSL_OCCUPATION(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_OCCUPATION))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_IDEXP))))
                simbakLeadModel.setSL_IDEXP(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_IDEXP))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NPWP))))
                simbakLeadModel.setSL_NPWP(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NPWP))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_MARRIAGE))))
                simbakLeadModel.setSL_MARRIAGE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_MARRIAGE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OWNERID))))
                simbakLeadModel.setSL_OWNERID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_OWNERID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OWNERTYPE))))
                simbakLeadModel.setSL_OWNERTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_OWNERTYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_CIF))))
                simbakLeadModel.setSL_CIF(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_CIF))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_PINBB))))
                simbakLeadModel.setSL_PINBB(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_PINBB))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_OLD_ID))))
                simbakLeadModel.setSL_OLD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_OLD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_TYPE))))
                simbakLeadModel.setSL_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_LAST_POS))))
                simbakLeadModel.setSL_LAST_POS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_LAST_POS))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_FAV))))
                simbakLeadModel.setSL_FAV(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SL_FAV))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SL_NAMA_CABANG))))
                simbakLeadModel.setSL_NAMA_CABANG(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SL_NAMA_CABANG))));
            cursor.close();
        }
        return simbakLeadModel;
    }
}
