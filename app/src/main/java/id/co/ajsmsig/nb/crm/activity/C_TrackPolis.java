package id.co.ajsmsig.nb.crm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.adapter.C_TrackPolisAdapter;
import id.co.ajsmsig.nb.crm.model.TrackPolisModel;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.PolisData;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.TrackPolisResponse;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.DateUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class C_TrackPolis extends AppCompatActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.layoutLoading)
    RelativeLayout layoutLoading;
    @BindView(R.id.layoutSPAJOnProcess)
    RelativeLayout layoutSPAJOnProcess;
    @BindView(R.id.layoutNoInternetConnection)
    RelativeLayout layoutNoInternetConnection;

    private C_TrackPolisAdapter mAdapter;
    private List<TrackPolisModel> polisList;
    private final String TAG = getClass().getSimpleName();
    private String spajNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c__track_polis);
        ButterKnife.bind(this);

        initView();
        initRecyclerView();

        Intent intent = getIntent();
        spajNumber = intent.getStringExtra(Const.INTENT_KEY_SPAJ_NO_TEMP);
        String ppName = intent.getStringExtra(Const.INTENT_KEY_SPAJ_PP_NAME);
        if (!TextUtils.isEmpty(spajNumber)) {
            String subtitle = ppName.concat(" (").concat(spajNumber).concat(")");
            getSupportActionBar().setSubtitle(subtitle);
            trackPolisWithNoTemp(spajNumber);
        }

    }

    /**
     * Re-perform api call to track polis when button is pressed
     */
    @OnClick(R.id.btnRetry)
    void onButtonRetryPressed() {
        if (!TextUtils.isEmpty(spajNumber)) {
            trackPolisWithNoTemp(spajNumber);
        }
    }

    /**
     * Init activity view
     */
    private void initView() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(R.string.activity_title_track_polis);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Init recyclerview to display spaj progress
     */
    private void initRecyclerView() {
        polisList = new ArrayList<>();
        mAdapter = new C_TrackPolisAdapter(polisList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(mAdapter);
    }


    /**
     * Do api call to track SPAJ progress by specifying a SPAJ number
     *
     * @param spajNumber
     */
    private void trackPolisWithNoTemp(String spajNumber) {
        layoutNoInternetConnection.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        layoutLoading.setVisibility(View.VISIBLE);

        // Example of production URL
        //String url = "https://spaj-api.sinarmasmsiglife.co.id/".concat("spaj/api/json/tracking/").concat("201803012839");
        String url = AppConfig.getBaseUrlSPAJ().concat("spaj/api/json/tracking/").concat(spajNumber);

        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);

        Call<TrackPolisResponse> call = api.trackPolisProgress(url);
        call.enqueue(new Callback<TrackPolisResponse>() {
            @Override
            public void onResponse(@NonNull Call<TrackPolisResponse> call, @NonNull Response<TrackPolisResponse> response) {
                layoutLoading.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                List<PolisData> polisProgressList = new ArrayList<>();
                String error = "";

                if (response.body() != null) {
                    TrackPolisResponse body = response.body();

                    if (body.getError() != null) error = body.getError();
                    if (body.getData() != null) polisProgressList = body.getData();


                    if (error.equalsIgnoreCase("")) {
                        processSPAJProgress(polisProgressList);
                    } else {
                        layoutSPAJOnProcess.setVisibility(View.VISIBLE);
                    }

                }
            }

            @Override
            public void onFailure(Call<TrackPolisResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    //Show no internet connection layout!
                    layoutLoading.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    layoutNoInternetConnection.setVisibility(View.VISIBLE);
                } else {
                    layoutLoading.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    Log.e(TAG, "Terjadi kesalahan : " + t.getMessage());
                    Toast.makeText(C_TrackPolis.this, "Terjadi kesalahan. " + t.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    /**
     * Parse the response into data model
     *
     * @param polisProgressList
     */
    private void processSPAJProgress(List<PolisData> polisProgressList) {
        if (!polisProgressList.isEmpty()) {

            for (PolisData polis : polisProgressList) {
                int lssaId = polis.getLSSAID();
                String mspsDesc = polis.getMSPSDESC();
                String mspsDate = polis.getMSPSDATE();
                String statusPolis = polis.getSTATUSPOLIS();
                String statusAccept = polis.getSTATUSACCEPT();
                String regSpaj = polis.getREGSPAJ();
                int lsspId = polis.getLSSPID();

                //Translate to bahasa
                if (statusPolis.equalsIgnoreCase("POLICY IS BEING PROCESSED")) {
                    statusPolis = "Polis sedang diproses";
                }

                //Convert dd/MM/yyyy HH:mm:ss date format to a more readable one
                mspsDate = DateUtils.getReadableDateFrom(mspsDate);

                TrackPolisModel model = new TrackPolisModel(lssaId, mspsDesc, mspsDate, statusPolis, statusAccept, regSpaj, lsspId);
                polisList.add(model);
            }

            mAdapter.notifyDataSetChanged();
            layoutSPAJOnProcess.setVisibility(View.GONE);
        }
    }
}
