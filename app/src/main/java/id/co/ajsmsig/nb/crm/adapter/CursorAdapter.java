package id.co.ajsmsig.nb.crm.adapter;
/*
 Created by faiz_f on 09/06/2017.
 */


import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public class CursorAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    Cursor dataCursor;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return (dataCursor == null) ? 0 : dataCursor.getCount();
    }


    public void changeCursor(Cursor cursor) {
        Cursor old = swapCursor(cursor);
        if (old != null) {
            old.close();
        }
    }

    public Cursor swapCursor(Cursor cursor) {
        if (dataCursor == cursor) {
            return null;
        }
        Cursor oldCursor = dataCursor;
        this.dataCursor = cursor;
        if (cursor != null) {
            this.notifyDataSetChanged();
        }
        return oldCursor;
    }

    private Object getItem(int position) {
        dataCursor.moveToPosition(position);
        // Load data from dataCursor and return it...
        return dataCursor;
    }

}
