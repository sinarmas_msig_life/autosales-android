package id.co.ajsmsig.nb.espaj.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.BuildConfig;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.method.DrawingView;
import id.co.ajsmsig.nb.espaj.method.GetTime;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.method.ProgressDialogClass;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDataDitunjukDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelFoto;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTTD;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;
import id.co.ajsmsig.nb.util.Const;
import id.zelory.compressor.Compressor;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Bernard on 30/05/2017.
 */


/*Change AppCompatActivity into Fragment*/
public class E_DokumenPendukungFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, CompoundButton.OnCheckedChangeListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        SharedPreferences settings = getActivity().getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        JENIS_LOGIN = settings.getInt("JENIS_LOGIN", 0);
        JENIS_LOGIN_BC = settings.getInt("JENIS_LOGIN_BC", 2);
        NAMA_AGEN = settings.getString("NAMA_AGEN", "");
        GROUP_ID = settings.getInt("GROUP_ID", 0);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem menu_validasi = menu.findItem(R.id.menu_validasi);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.e_fragment_dokumen_pendukung_layout, container,
                false);
        me = E_MainActivity.e_bundle.getParcelable(getString(R.string.modelespaj));
        ((E_MainActivity) getActivity()).setSecondToolbar("Dokumen Pendukung (7/", 7);
        ButterKnife.bind(this, view);

        file_foto = new File(getActivity().getFilesDir(), "ESPAJ/spaj_foto");
        if (!file_foto.exists()) {
            file_foto.mkdirs();
        }

        file_ttd = new File(getActivity().getFilesDir(), "ESPAJ/spaj_ttd");
        if (!file_ttd.exists()) {
            file_ttd.mkdirs();
        }
        add_ttd_dp.setOnClickListener(this);
        add_foto_dp.setOnClickListener(this);

        ListTTD = new ArrayList<ModelTTD>();
        ListFoto = new ArrayList<ModelFoto>();
//button di Activity
        btn_lanjut = getActivity().findViewById(R.id.btn_lanjut);
        btn_lanjut.setOnClickListener(this);
        btn_kembali = getActivity().findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(this);
        second_toolbar = ((E_MainActivity) getActivity()).getSecond_toolbar();
        iv_save = second_toolbar.findViewById(R.id.iv_save);
        iv_save.setOnClickListener(this);
        iv_next = second_toolbar.findViewById(R.id.iv_next);
        iv_next.setOnClickListener(this);
        iv_prev = second_toolbar.findViewById(R.id.iv_prev);
        iv_prev.setOnClickListener(this);

        check_pernyataan.setOnCheckedChangeListener(this);

        if (new E_Select(getContext()).getLine_Bus(me.getUsulanAsuransiModel().getKd_produk_ua()) == 3) { //SYARIAH
            wv_pernyataan_asuransi.loadUrl("file:///android_asset/pernyataan_asuransi_syariah.html");
        } else {
            wv_pernyataan_asuransi.loadUrl("file:///android_asset/pernyataan_asuransi.html");
        }

        restore();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        ProgressDialogClass.removeSimpleProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        MethodSupport.active_view(1, btn_lanjut);
        MethodSupport.active_view(1, iv_next);
        MethodSupport.active_view(1, btn_kembali);
        MethodSupport.active_view(1, iv_prev);
        ProgressDialogClass.removeSimpleProgressDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_validasi:
                me.getDokumenPendukungModel().setValidation(Validation());
                me.getModelID().setVal_dp(Validation());
                loadview(count_ft, count_ttd);
                MyTaskValidasi taskValidasi = new MyTaskValidasi();
                taskValidasi.execute();
                Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTaskValidasi extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_DOKUMEN_PENDUKUNG);
            return null;
        }
    }

    private class MyTaskSavePage extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_DOKUMEN_PENDUKUNG);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    private void restore() {
        ListManfaat = me.getListManfaatDI();
        count_ft = 0;
        count_ttd = 0;
        ListFoto = me.getDokumenPendukungModel().getListFoto();
        if (ListFoto.isEmpty()) {
            count_ft++;
            inflateAddFoto(count_ft, "", "BUKTI IDENTITAS PEMEGANG POLIS", "FT_BUKTI_PP", 0);
            if (me.getPemegangPolisModel().getHubungan_pp() != 1) {
                count_ft++;
                inflateAddFoto(count_ft, "", "BUKTI IDENTITAS TERTANGGUNG UTAMA", "FT_BUKTI_TT", 1);
            }
            count_ft++;
            inflateAddFoto(count_ft, "", "BUKTI SETOR BANK - 1", "FT_BSB_1", 1);
            if (me.getUsulanAsuransiModel().getBentukbayar_ua() == 2) {
                if (me.getUsulanAsuransiModel().getCarabayar_ua() != 0) {
                    count_ft++;
                    inflateAddFoto(count_ft, "", "SURAT KUASA DEBET-HAL 1", "FT_SURATKUASA_1", 1);
                    count_ft++;
                    inflateAddFoto(count_ft, "", "ACCOUNT STATEMENT/BUKU TABUNGAN", "FT_ACCSTATEMENT", 1);

                }

            } else if (GROUP_ID == 40) {
                if (me.getUsulanAsuransiModel().getBentukbayar_ua() != 0) {
                    count_ft++;
                    inflateAddFoto(count_ft, "", "SURAT KUASA DEBET-HAL 1", "FT_SURATKUASA_1", 1);
                    count_ft++;
                    inflateAddFoto(count_ft, "", "ACCOUNT STATEMENT/BUKU TABUNGAN", "FT_ACCSTATEMENT", 1);
                }

            }

            if (GROUP_ID == 40) {

                for (int i = 0; i < ListManfaat.size(); i++) {
                    if (ListManfaat.get(i).getHub_dgcalon_tt() != 1 && ListManfaat.get(i).getHub_dgcalon_tt() != 7 &&
                            ListManfaat.get(i).getHub_dgcalon_tt() != 4 && ListManfaat.get(i).getHub_dgcalon_tt() != 2 &&
                            ListManfaat.get(i).getHub_dgcalon_tt() != 5) {
                        count_ft++;
                        inflateAddFoto(count_ft, "", "SURAT PERNYATAAN HUBUNGAN-HAL 1", "FT_SP_HUB_1", 1);
                    }
                }
            }
        } else {
            for (int i = 0; i < ListFoto.size(); i++) {
                count_ft++;
                inflateAddFoto(count_ft, ListFoto.get(i).getFile_foto(),
                        ListFoto.get(i).getJudul_foto(), ListFoto.get(i).getId_foto(), ListFoto.get(i).getFlag_default());
            }
        }
        ListTTD = me.getDokumenPendukungModel().getListTTD();
        if (ListTTD.isEmpty()) {
            count_ttd++;
            inflateAddTTD(count_ttd, "", "CALON PEMEGANG POLIS", me.getPemegangPolisModel().getNama_pp(), 1, "TTD_PP");
            if (me.getPemegangPolisModel().getHubungan_pp() != 1) {
                count_ttd++;
                inflateAddTTD(count_ttd, "", "CALON TERTANGGUNG UTAMA", me.getTertanggungModel().getNama_tt(), 1, "TTD_TU");
            }
            count_ttd++;
            inflateAddTTD(count_ttd, "", "AGEN BC", NAMA_AGEN, 1, "TTD_AGEN");
            if (me.getUsulanAsuransiModel().getKd_produk_ua() == 208 && me.getUsulanAsuransiModel().getSub_produk_ua() == 5
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 208 && me.getUsulanAsuransiModel().getSub_produk_ua() == 6
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 208 && me.getUsulanAsuransiModel().getSub_produk_ua() == 7
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 208 && me.getUsulanAsuransiModel().getSub_produk_ua() == 8
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 219 && me.getUsulanAsuransiModel().getSub_produk_ua() == 1
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 219 && me.getUsulanAsuransiModel().getSub_produk_ua() == 2
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 219 && me.getUsulanAsuransiModel().getSub_produk_ua() == 3
                    || me.getUsulanAsuransiModel().getKd_produk_ua() == 219 && me.getUsulanAsuransiModel().getSub_produk_ua() == 4) {
                count_ttd++;
                inflateAddTTD(count_ttd, "", "AGEN PENUTUP", me.getDetilAgenModel().getNm_pnutup_da(), 1, "TTD_PENUTUP");
                count_ttd++;
                inflateAddTTD(count_ttd, "", "AGEN REFERRAL", me.getDetilAgenModel().getNm_refferal_da(), 1, "TTD_REF");
            }
        } else {
            for (int i = 0; i < ListTTD.size(); i++) {
                count_ttd++;
                inflateAddTTD(count_ttd, ListTTD.get(i).getFile_ttd(),
                        ListTTD.get(i).getJudul_ttd(), ListTTD.get(i).getNama_jelas(), ListTTD.get(i).getFlag_default(), ListTTD.get(i).getId_ttd());
            }
        }

        if (!me.getValidation()) {
            Validation();
        }
//            if (me.getModelID().getFlag_aktif() == 1) {
//                MethodSupport.disable(false, cl_child_dp);
//            }
    }

    private void loadview(int count_ft, int count_ttd) {
        AddModelFoto(count_ft);
        AddModelTTD(count_ttd);
        E_MainActivity.e_bundle.putParcelable(getString(R.string.modelespaj), me);
    }

    private void inflateAddFoto(final int count_ft2, String file, String Ket, String judul, final int flag_default) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.e_adapter_foto, null);
        file_foto_dp[count_ft2] = new File(file);
        flag_foto_dp[count_ft2] = flag_default;

        tv_nomor_foto_dp[count_ft2] = rowView.findViewById(R.id.tv_nomorfoto_dp);
        tv_nomor_foto_dp[count_ft2].setText(String.valueOf(count_ft2));
        tv_nomor_foto_dp[count_ft2].setVisibility(View.GONE);

        ac_judul_foto_dp[count_ft2] = rowView.findViewById(R.id.ac_judul_foto_dp);
        try {
            setAdapterJudulFoto(ac_judul_foto_dp[count_ft2]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        judul_foto_dp[count_ft2] = judul;
        ListDropDownSPAJ = MethodSupport.getXMLString(getContext(), R.xml.e_judul_foto, 0);
        ac_judul_foto_dp[count_ft2].setText(Ket);
        if (flag_default == 1) {
            MethodSupport.active_view(0, ac_judul_foto_dp[count_ft2]);
        }

        foto_dp[count_ft2] = rowView.findViewById(R.id.foto_dp);
        foto_dp[count_ft2].setImageBitmap(decodeFileThumbnail(file));
        foto_dp[count_ft2].invalidate();

        iv_menufoto_dp[count_ft2] = rowView.findViewById(R.id.iv_menufoto_dp);
        //setonclick
        foto_dp[count_ft2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (file_foto_dp[count_ft2].exists()) {
                    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View popupSet = inflater.inflate(R.layout.e_popup_image, null);
                    ImageView foto = popupSet.findViewById(R.id.foto);

//				foto.setImageBitmap(decodeFile(file_foto));
                    Bitmap myBitmap = BitmapFactory.decodeFile(file_foto_dp[count_ft2].getAbsolutePath());
                    foto.setImageBitmap(myBitmap);
                    foto.invalidate();

                    final PopupWindow pw = new PopupWindow(popupSet, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT, true);
                    foto.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            pw.dismiss();
                        }
                    });
                    pw.showAtLocation(popupSet, Gravity.CENTER, 0, 0);
                }
            }
        });
        ac_judul_foto_dp[count_ft2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ac_judul_foto_dp[count_ft2].setText("");
                ac_judul_foto_dp[count_ft2].showDropDown();
            }
        });
        ac_judul_foto_dp[count_ft2].setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasfocus) {
                if (hasfocus) ac_judul_foto_dp[count_ft2].setText("");
                ac_judul_foto_dp[count_ft2].showDropDown();
            }
        });
        ac_judul_foto_dp[count_ft2].setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                ModelDropDownString modelJudul = (ModelDropDownString) parent.getAdapter().getItem(i);
                judul_foto_dp[count_ft2] = modelJudul.getId();
            }
        });
        iv_menufoto_dp[count_ft2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(getActivity(), view);
                popup.getMenuInflater().inflate(R.menu.menu_popupfoto, popup.getMenu());
                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.camera:
                                final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                                if (setImageUri(count_ft2) != null) {
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri(count_ft2));
                                } else {
                                    intent.putExtra("return-data", true);

                                }
                                startActivityForResult(intent, TAKE_PHOTO_CODE);
                                temp_count_img = count_ft2;
                                break;
                            case R.id.gallery:
                                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                                startActivityForResult(i,
                                        PICK_FROM_GALLERY);
                                temp_count_img = count_ft2;

                                break;
                            case R.id.delete:
                                if (flag_default == 1) {
                                    MethodSupport.Alert(getActivity(), getString(R.string.title_error), getString(R.string.msg_default_dok), 0);
                                } else {
                                    if (count_ft != 1) {
                                        AlertDialog.Builder img_dialog = new AlertDialog.Builder(getActivity());
                                        img_dialog.setTitle("KONFIRMASI. . .");
                                        img_dialog.setMessage("Apakah Anda Ingin Menghapus Foto ini? ");
                                        img_dialog.setPositiveButton("YA", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                count_ft--;
                                                ((ViewGroup) rowView.getParent()).removeView(rowView);
                                                if (count_ft != 0) {
                                                    tv_nomor_foto_dp[count_ft].setText(String.valueOf(count_ft));
                                                }
                                                cl_form_foto_dp.invalidate();

                                            }
                                        });
                                        img_dialog.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                        img_dialog.show();
                                    } else {
                                        MethodSupport.Alert(getActivity(), getString(R.string.title_error), getString(R.string.msg_foto_min), 0);
                                    }
                                }
                                break;
                        }
                        return true;
                    }
                });
            }
        });

        cl_form_foto_dp.addView(rowView, cl_form_foto_dp.getChildCount() - 1);
    }

    private void inflateAddTTD(final int count_ttd2, String file, String Ket, String nama, final int flag_default, String judul) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowTtd = inflater.inflate(R.layout.e_adapter_ttd, null);
        ttd_dp[count_ttd2] = rowTtd.findViewById(R.id.ttd_dp);
        flag_ttd_dp[count_ttd2] = flag_default;

        file_ttd_dp[count_ttd2] = new File(file);
        if (file_ttd_dp[count_ttd2].exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(file_ttd_dp[count_ttd2].getAbsolutePath());
            ttd_dp[count_ttd2].setImageBitmap(myBitmap);
            ttd_dp[count_ttd2].invalidate();
        }
        hapus_ttd_dp[count_ttd2] = rowTtd.findViewById(R.id.hapus_ttd_dp);
        tambah_ttd_dp[count_ttd2] = rowTtd.findViewById(R.id.tambah_ttd_dp);
        tv_nomor_ttd_dp[count_ttd2] = rowTtd.findViewById(R.id.tv_nomorttd_dp);
        tv_nomor_ttd_dp[count_ttd2].setText(String.valueOf(count_ttd2));
        tv_nomor_ttd_dp[count_ttd2].setVisibility(View.GONE);

        ac_judul_ttd_dp[count_ttd2] = rowTtd.findViewById(R.id.ac_judul_tt_dp);
        try {
            setAdapterJudulTTD(ac_judul_ttd_dp[count_ttd2]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        judul_ttd_dp[count_ttd2] = judul;
        ListDropDownSPAJ = MethodSupport.getXMLString(getContext(), R.xml.e_judul_ttd, 0);
        ac_judul_ttd_dp[count_ttd2].setText(Ket);
        if (flag_default == 1) {
            MethodSupport.active_view(0, ac_judul_ttd_dp[count_ttd2]);
        }

        nama_ttd_dp[count_ttd2] = rowTtd.findViewById(R.id.nama_ttd_dp);
        nama_ttd_dp[count_ttd2].setText(nama);
        if (flag_default == 1) {
            MethodSupport.active_view(0, nama_ttd_dp[count_ttd2]);
            MethodSupport.active_view(0, ac_judul_ttd_dp[count_ttd2]);
        }
        if (judul.equals("TTD_REF")) {
            nama_ttd_dp[count_ttd2].setText(me.getDetilAgenModel().getNm_refferal_da());
        }
        if (judul.equals("TTD_AGEN")) {
            nama_ttd_dp[count_ttd2].setText(NAMA_AGEN);
        }
        if (judul.equals("TTD_PENUTUP")) {
            nama_ttd_dp[count_ttd2].setText(me.getDetilAgenModel().getNm_pnutup_da());
        }

        //setonclick
        ac_judul_ttd_dp[count_ttd2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ac_judul_ttd_dp[count_ttd2].setText("");
                ac_judul_ttd_dp[count_ttd2].showDropDown();
            }
        });
        ac_judul_ttd_dp[count_ttd2].setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasfocus) {
                if (hasfocus) ac_judul_ttd_dp[count_ttd2].setText("");
                ac_judul_ttd_dp[count_ttd2].showDropDown();
            }
        });

        ac_judul_ttd_dp[count_ttd2].setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                ModelDropDownString modelJudul = (ModelDropDownString) parent.getAdapter().getItem(i);
                judul_ttd_dp[count_ttd2] = modelJudul.getId();
            }
        });
        tambah_ttd_dp[count_ttd2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupttd(count_ttd2);
            }
        });
        hapus_ttd_dp[count_ttd2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag_default == 1) {
                    MethodSupport.Alert(getActivity(), getString(R.string.title_error), getString(R.string.msg_default_dok), 0);
                } else {
                    if (count_ttd != 1) {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                        dialog.setTitle("KONFIRMASI. . .");
                        dialog.setMessage("Apakah Anda Ingin Menghapus Tanda Tangan ini? ");
                        dialog.setPositiveButton("YA", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                count_ttd--;
                                ((ViewGroup) rowTtd.getParent()).removeView(rowTtd);
                                if (count_ttd != 0) {
                                    tv_nomor_ttd_dp[count_ttd].setText(String.valueOf(count_ttd));
                                }
                            }
                        });
                        dialog.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    } else {
                        MethodSupport.Alert(getActivity(), getString(R.string.title_error), getString(R.string.msg_ttd_min), 0);
                    }
                }

            }
        });
        cl_form_tanda_tangan_dp.addView(rowTtd, cl_form_tanda_tangan_dp.getChildCount() - 1);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //View di activity
            case R.id.btn_lanjut:
                onClickLanjut(count_ft, count_ttd);
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_next:
                onClickLanjut(count_ft, count_ttd);
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_save:
                loadview(count_ft, count_ttd);
                MyTaskSavePage taskSavePage = new MyTaskSavePage();
                taskSavePage.execute();

                break;
            case R.id.iv_prev:
                onClickBack(count_ft, count_ttd);
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.btn_kembali:
                onClickBack(count_ft, count_ttd);
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.add_foto_dp:
                if (count_ft < 25) {
                    if (count_ft > 0) {
                        AddModelFoto(count_ft);
                    }
                    count_ft++;
                    inflateAddFoto(count_ft, "", "", "", 0);
                    view.setVisibility(View.VISIBLE);
                } else {
                    MethodSupport.Alert(getActivity(), getString(R.string.title_error), getString(R.string.msg_foto), 0);
                }
                break;
            case R.id.add_ttd_dp:
                if (count_ttd < 15) {
                    if (count_ttd > 0) {
                        AddModelTTD(count_ttd);
                    }
                    count_ttd++;
                    inflateAddTTD(count_ttd, "", "", "", 0, "");
                    view.setVisibility(View.VISIBLE);
                } else {
                    MethodSupport.Alert(getActivity(), getString(R.string.title_error), getString(R.string.msg_ttd), 0);
                }
                break;
        }
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (check_pernyataan.isChecked() == false) {
            iv_circle_pernyataan.setColorFilter(ContextCompat.getColor(getContext(), R.color.orange));
        } else {
            iv_circle_pernyataan.setColorFilter(ContextCompat.getColor(getContext(), R.color.green));
        }
    }

    private void popupttd(final int counter3) {

        LayoutInflater inflater = (LayoutInflater) getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupSet = inflater.inflate(R.layout.e_popup_ttd, null);
        Button close = popupSet.findViewById(R.id.Cancel);
        Button set = popupSet.findViewById(R.id.save);
        drawView = popupSet.findViewById(R.id.drawing);

        final PopupWindow pw = new PopupWindow(popupSet, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        set.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                //save drawing
                AlertDialog.Builder saveDialog = new AlertDialog.Builder(getActivity());
                saveDialog.setTitle("Set Tanda Tangan");
                saveDialog.setMessage("Simpan tanda Tangan?");
                saveDialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        drawView.setDrawingCacheEnabled(true);
                        savedraw(counter3);
                        pw.dismiss();
                        showDrawingFile(counter3);

                    }
                });
                saveDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                saveDialog.show();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pw.dismiss();
            }
        });

        pw.showAtLocation(popupSet, Gravity.CENTER, 0, 0);
    }

    public void savedraw(int counter3) {
        drawView.setDrawingCacheEnabled(true);
        Bitmap bitmap = drawView.getDrawingCache();

//		File sdCard= Environment.getExternalStorageDirectory();
        file_ttd_dp[counter3] = new File(file_ttd, "ttd" + GetTime.getCurrentDate("yyyyMMddhhmmss") + ".PNG");
        FileOutputStream ostream = null;
        try {
            ostream = new FileOutputStream(file_ttd_dp[counter3]);
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, ostream);

        try {
            ostream.close();
        } catch (IOException e) {

            e.printStackTrace();
        }
        drawView.invalidate();
        drawView.setDrawingCacheEnabled(false);
    }

    private void showDrawingFile(int counter3) {
//		File f = new File( "/sdcard/data/data/id.co.ajsmsig.espaj/ESPAJ/spaj_ttd/"+GetTime.getCurrentDate("yyyyMMddhhmmss")+".jpg");
        System.out.println(file_ttd_dp[counter3]);
        if (file_ttd_dp[counter3].exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(file_ttd_dp[counter3].getAbsolutePath());
            ttd_dp[counter3].setImageBitmap(myBitmap);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        int size_img = 0;

        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
            selectedImagePath = getImagePath();
            file_foto_dp[temp_count_img] = new File(file_foto, "foto" + GetTime.getCurrentDate("yyyyMMddhhmmss") + ".JPG");
            FileOutputStream fos = null;
            if (!file_foto_dp[temp_count_img].exists()) {
                try {
                    Bitmap bitmap = rotateBitmap(selectedImagePath);
                    fos = new FileOutputStream(file_foto_dp[temp_count_img]);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    fos.flush();
                    fos.close();
                } catch (FileNotFoundException e) {
                    throw new RuntimeException(e);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            size_img = (int) (Math.log10(file_foto_dp[temp_count_img].length()) / Math.log10(1024));
            if (size_img > 1) {
                file_foto_dp[temp_count_img] = ResizeImage(file_foto_dp[temp_count_img]);
            }
            foto_dp[temp_count_img].setImageBitmap(decodeFileThumbnail(file_foto_dp[temp_count_img].getAbsolutePath()));
        } else if (requestCode == PICK_FROM_GALLERY && data != null) {
            Uri selectedImageUri = data.getData();
            selectedImagePath = getPath(selectedImageUri);
            System.out.println("Image Path : " + selectedImagePath);
            file_foto_dp[temp_count_img] = new File(file_foto, "foto" + GetTime.getCurrentDate("yyyyMMddhhmmss") + ".JPG");
            FileOutputStream fos = null;
            if (!file_foto_dp[temp_count_img].exists()) {
                try {
                    Bitmap bitmap = rotateBitmap(selectedImagePath);
                    fos = new FileOutputStream(file_foto_dp[temp_count_img]);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    fos.flush();
                    fos.close();
                } catch (FileNotFoundException e) {
                    throw new RuntimeException(e);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            size_img = (int) (Math.log10(file_foto_dp[temp_count_img].length()) / Math.log10(1024));
            if (size_img > 1) {
                file_foto_dp[temp_count_img] = ResizeImage(file_foto_dp[temp_count_img]);
            }
            foto_dp[temp_count_img].setImageBitmap(decodeFileThumbnail(file_foto_dp[temp_count_img].getAbsolutePath()));
        }
    }

    //rotate image in ImageView
    public static Bitmap rotateBitmap(String file) {
        //	Bitmap resizedBitmap = null;
        Bitmap bitmap = BitmapFactory.decodeFile(file);
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        if (bitmap.getWidth() > bitmap.getHeight()) {
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        }
        return bitmap;
    }

    //rotate image in ImageView Thumbnail
    public Bitmap rotateBitmapThumbnail(String file) {
        Bitmap resizedBitmap = null;
        Bitmap bitmap = decodeFileThumbnail(file);
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        matrix.postScale(-1, 1);
        resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        return resizedBitmap;
    }

    //method copy gambar
    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }
        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }
    }

    //methode ambil image dari galeri
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    //Method camera
    public Uri setImageUri(int counter2) {
        FileOutputStream out = null;
        // Store image in dcim
        file_foto_dp[counter2] = new File(Environment.getExternalStorageDirectory() + "/android/data/ESPAJ/spaj_foto/" + GetTime.getCurrentDate("yyyyMMddhhmmss") + ".JPG");
//        Uri imgUri = Uri.fromFile(file_foto_dp[counter2]);
        Uri imgUri = FileProvider.getUriForFile(getActivity(),
                BuildConfig.APPLICATION_ID + ".provider",
                file_foto_dp[counter2]);
        this.imgPath = file_foto_dp[counter2].getAbsolutePath();
        return imgUri;
    }

    public String getImagePath() {
        return imgPath;
    }

    //decode memory image
    public Bitmap decodeFileThumbnail(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 250;
            // Find the correct scale value. It should be the power of 2.
            int scale = 4;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }

    private void setAdapterJudulFoto(AutoCompleteTextView auto) throws IOException, XmlPullParserException {
//        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
//                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.judul_foto));
//        auto.setAdapter(Adapter);
//        Adapter.notifyDataSetChanged();
        MethodSupport.load_setDropXmlString(R.xml.e_judul_foto, auto, getContext(), 0);
    }

    private void setAdapterJudulTTD(AutoCompleteTextView auto) throws IOException, XmlPullParserException {
//        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
//                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.judul_ttd));
//        auto.setAdapter(Adapter);
//        Adapter.notifyDataSetChanged();
        MethodSupport.load_setDropXmlString(R.xml.e_judul_ttd, auto, getContext(), 0);
    }

    private void onClickLanjut(final int count_ft, final int count_ttd) {
        loadview(count_ft, count_ttd);

        if (!val_page()) {
            MethodSupport.Alert(getActivity(), getString(R.string.title_error), getString(R.string.error_same_name), 0);

        } else {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setTitle("Melakukan perpindahan halaman");
//            progressDialog.setMessage("Mohon Menunggu...");
//            progressDialog.show();
//            progressDialog.setCancelable(false);
//            Runnable progressRunnable = new Runnable() {
//                @Override
//                public void run() {
//                    MethodSupport.active_view(0, btn_lanjut);
//                    MethodSupport.active_view(0, iv_next);
//                    ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);

                    me.getDokumenPendukungModel().setValidation(Validation());
                    me.getModelID().setVal_dp(Validation());
                    MyTask task = new MyTask();
                    task.execute();

//                    me.getDokumenPendukungModel().setValidation(Validation());
//                    ((E_MainActivity) getActivity()).onSave();
//                    View cl_child_dp = getActivity().findViewById(R.id.cl_child_dp);
//                    File file_bitmap = new File(
//                            getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//                    if (!file_bitmap.exists()) {
//                        file_bitmap.mkdirs();
//                    }
//                    String fileName = "DP_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
//                    Context context = getActivity();
//                    MethodSupport.onCreateBitmap(cl_child_dp, scroll_dp, file_bitmap, fileName, context);
//
//                    if (me.getModelID().getJns_spaj() == 3) {
//                        ((E_MainActivity) getActivity()).setFragment(new E_QuestionnaireFragment(), getResources().getString(R.string.questionnaire));
//                    } else if (me.getModelID().getJns_spaj() == 4) {
//                        ((E_MainActivity) getActivity()).setFragment(new E_UpdateQuisionerSIOFragment(), getResources().getString(R.string.questionnaire));
//                    } else {
//                        if (new E_Select(getContext()).selectLinkNonLink(me.getUsulanAsuransiModel().getKd_produk_ua()) == 17) {
//                            ((E_MainActivity) getActivity()).setFragment(new E_ProfileResikoFragment(), getResources().getString(R.string.profil_nasabah));
//                        } else {
//                            Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
//                        }
//                    }
//                    progressDialog.cancel();
//                }
//            };
//            Handler pdCanceller = new Handler();
//            pdCanceller.postDelayed(progressRunnable, 3000);
        }
    }

    private class MyTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_DOKUMEN_PENDUKUNG);
            View cl_child_dp = getActivity().findViewById(R.id.cl_child_dp);
            File file_bitmap = new File(
                    getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
            if (!file_bitmap.exists()) {
                file_bitmap.mkdirs();
            }
            String fileName = "DP_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
            Context context = getActivity();
            MethodSupport.onCreateBitmap(cl_child_dp, scroll_dp, file_bitmap, fileName, context);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (me.getModelID().getJns_spaj() == 3) {
                ((E_MainActivity) getActivity()).setFragment(new E_QuestionnaireFragment(), getResources().getString(R.string.questionnaire));
            } else if (me.getModelID().getJns_spaj() == 4) {
                ((E_MainActivity) getActivity()).setFragment(new E_UpdateQuisionerSIOFragment(), getResources().getString(R.string.questionnaire));
            } else {
                if (new E_Select(getContext()).selectLinkNonLink(me.getUsulanAsuransiModel().getKd_produk_ua()) == 17) {
                    ((E_MainActivity) getActivity()).setFragment(new E_ProfileResikoFragment(), getResources().getString(R.string.profil_nasabah));
                } else {
                    Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
                }
            }
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    private class MyTaskBack extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_DOKUMEN_PENDUKUNG);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ((E_MainActivity) getActivity()).setFragment(new E_DetilAgenFragment(), getResources().getString(R.string.detil_agen));
        }
    }

    private void onClickBack(final int count_ft, final int count_ttd) {

        loadview(count_ft, count_ttd);
        MyTaskBack taskBack = new MyTaskBack();
        taskBack.execute();

//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setTitle("Melakukan perpindahan halaman");
//        progressDialog.setMessage("Mohon Menunggu...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
//        Runnable progressRunnable = new Runnable() {
//
//            @Override
//            public void run() {
//                MethodSupport.active_view(0, btn_kembali);
//                MethodSupport.active_view(0, iv_prev);
//                ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
//
//                loadview(count_ft, count_ttd);
//                ((E_MainActivity) getActivity()).onSave();
//                ((E_MainActivity) getActivity()).setFragment(new E_DetilAgenFragment(), getResources().getString(R.string.detil_agen));
//                progressDialog.cancel();
//            }
//        };
//
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 1000);
    }

    private void onClickBackDokumenPendukung(int count_ft2, int count_ttd) {
        MethodSupport.active_view(0, btn_kembali);
        MethodSupport.active_view(0, iv_prev);
        ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);

        loadview(count_ft2, count_ttd);
        ((E_MainActivity) getActivity()).onSave(Const.PAGE_DOKUMEN_PENDUKUNG);
        ((E_MainActivity) getActivity()).setFragment(new E_DokumenPendukungFragment(), getResources().getString(R.string.dokumen_pendukung));
    }

    private ProgressDialog dialogmessage;

    public void startProgress() {
        dialogmessage = new ProgressDialog(getActivity());
        dialogmessage.setMessage("Mohon Menunggu ");
        dialogmessage.setTitle("Sedang Melakukan Perpindahan Halaman ...");
        dialogmessage.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialogmessage.show();
        dialogmessage.setCancelable(false);
        btn_lanjut.setEnabled(false);
        iv_next.setEnabled(false);
        iv_prev.setEnabled(false);
        btn_kembali.setEnabled(false);
        dialogmessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                btn_lanjut.setEnabled(true);
                iv_next.setEnabled(true);
                iv_prev.setEnabled(true);
                btn_kembali.setEnabled(true);
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (dialogmessage.getProgress() <= dialogmessage
                            .getMax()) {
                        Thread.sleep(10);
                        handle.sendMessage(handle.obtainMessage());
                        if (dialogmessage.getProgress() == dialogmessage
                                .getMax()) {
                            dialogmessage.dismiss();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    Handler handle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            dialogmessage.incrementProgressBy(1);
        }
    };

    private void AddModelFoto(int count_ft) {
        if (!me.getDokumenPendukungModel().getListFoto().isEmpty() && !ListFoto.isEmpty()) {
            me.getDokumenPendukungModel().getListFoto().clear();
            ListFoto.clear();
        }
        if (count_ft != 0) {
            for (int i = 0; i < count_ft; i++) {
                ModelFoto modelFoto = new ModelFoto();
                modelFoto.setCounter(i + 1);
                modelFoto.setFile_foto(file_foto_dp[i + 1].toString());
                if(judul_foto_dp[i + 1].equals("FT_BUKTI_PP")) {
                    ac_judul_foto_dp[i + 1].setText("BUKTI IDENTITAS PEMEGANG POLIS");
                    modelFoto.setJudul_foto(ac_judul_foto_dp[i + 1].getText().toString());

                }else {
                    modelFoto.setJudul_foto(ac_judul_foto_dp[i + 1].getText().toString());
                }
                modelFoto.setId_foto(judul_foto_dp[i + 1]);
                modelFoto.setFlag_default(flag_foto_dp[i + 1]);
                ListFoto.add(modelFoto);
            }
        }
        me.getDokumenPendukungModel().setListFoto(ListFoto);
    }

    private void AddModelTTD(int count_ttd) {
        if (!me.getDokumenPendukungModel().getListTTD().isEmpty() && !ListTTD.isEmpty()) {
            me.getDokumenPendukungModel().getListTTD().clear();
            ListTTD.clear();
        }
        if (count_ttd != 0) {
            for (int i = 0; i < count_ttd; i++) {
                ModelTTD modelTTD = new ModelTTD();
                modelTTD.setCounter(i + 1);
                modelTTD.setFile_ttd(file_ttd_dp[i + 1].toString());
                modelTTD.setJudul_ttd(ac_judul_ttd_dp[i + 1].getText().toString());
                modelTTD.setNama_jelas(nama_ttd_dp[i + 1].getText().toString());
                modelTTD.setId_ttd(judul_ttd_dp[i + 1]);
                modelTTD.setFlag_default(flag_ttd_dp[i + 1]);
                ListTTD.add(modelTTD);
            }
        }
        me.getDokumenPendukungModel().setListTTD(ListTTD);
    }

    private boolean val_page() {
        boolean val = true;
        List<String> List_db = new ArrayList<String>();
        for (int i = 0; i < ListFoto.size(); i++) {
            List_db.add(ListFoto.get(i).getJudul_foto());
        }
        Set set = new HashSet<>(List_db);

        if (List_db.size() > set.size()) {
            tv_error_foto_dp.setVisibility(View.VISIBLE);
            tv_error_foto_dp.setText(getString(R.string.error_same_name));
            val = false;
            MethodSupport.disable(true, cl_child_dp);
        } else {
            tv_error_foto_dp.setVisibility(View.GONE);
        }

        List_db = new ArrayList<String>();
        for (int i = 0; i < ListTTD.size(); i++) {
            List_db.add(ListTTD.get(i).getId_ttd());
        }

        set = new HashSet<>(List_db);

        if (List_db.size() > set.size()) {
            val = false;
            MethodSupport.disable(true, cl_child_dp);
        }

        return val;
    }

    private boolean Validation() {
        boolean val = true;
//        if (!check_pernyataan.isChecked()) {
//            iv_circle_pernyataan.setColorFilter(ContextCompat.getColor(getContext(), R.color.red));
//            val = false;
//        }
        for (int i = 0; i < ListFoto.size(); i++) {
            if (ListFoto.get(i).getFile_foto().length() == 0 || ListFoto.get(i).getJudul_foto().length() == 0) {
                tv_error_foto_dp.setVisibility(View.VISIBLE);
                tv_error_foto_dp.setText(getString(R.string.error_field_required));
                val = false;
                break;
            } else {
                tv_error_foto_dp.setVisibility(View.GONE);
            }
        }

        for (int i = 0; i < ListTTD.size(); i++) {
            if (ListTTD.get(i).getFile_ttd().length() == 0 || ListTTD.get(i).getJudul_ttd().length() == 0) {
                tv_error_ttd_dp.setVisibility(View.VISIBLE);
                tv_error_ttd_dp.setText(getString(R.string.error_field_required));
                val = false;
                break;
            } else {
                tv_error_ttd_dp.setVisibility(View.GONE);
            }
        }
        List<String> List_db = new ArrayList<String>();
        for (int i = 0; i < ListFoto.size(); i++) {
            List_db.add(ListFoto.get(i).getJudul_foto());
        }

        Set set = new HashSet<>(List_db);

        if (List_db.size() > set.size()) {
            tv_error_foto_dp.setVisibility(View.VISIBLE);
            tv_error_foto_dp.setText(getString(R.string.error_same_name));
            val = false;
        } else {
            tv_error_foto_dp.setVisibility(View.GONE);
        }

        return val;
    }


    public String getReadableFileSize(long size) {
        String str_file = "";
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        str_file = new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
        return str_file;
    }

    private File ResizeImage(File file_image) {
        File file = null;
        try {
            file = new Compressor(getContext())
                    .setMaxWidth(600)
                    .setMaxHeight(800)
                    .setQuality(100)
                    .setCompressFormat(Bitmap.CompressFormat.PNG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getPath())
                    .compressToFile(file_image);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i("tes", String.format("Ukuran Gambar %s", getReadableFileSize(file_foto_dp[temp_count_img].length())));
        return file;
    }

    @BindView(R.id.tv_foto_dp)
    TextView tv_foto_dp;
    @BindView(R.id.tv_pernyataan_dp)
    TextView tv_pernyataan_dp;
    @BindView(R.id.tv_tanda_tangan_dp)
    TextView tv_tanda_tangan_dp;
    @BindView(R.id.add_foto_dp)
    TextView add_foto_dp;
    @BindView(R.id.add_ttd_dp)
    TextView add_ttd_dp;
    @BindView(R.id.tv_error_foto_dp)
    TextView tv_error_foto_dp;
    @BindView(R.id.tv_error_ttd_dp)
    TextView tv_error_ttd_dp;

    @BindView(R.id.wv_pernyataan_asuransi)
    WebView wv_pernyataan_asuransi;

    @BindView(R.id.cl_foto_dp)
    ConstraintLayout cl_foto_dp;
    @BindView(R.id.cl_pernyataan_dp)
    ConstraintLayout cl_pernyataan_dp;
    @BindView(R.id.cl_form_pernyataan_dp)
    ConstraintLayout cl_form_pernyataan_dp;
    @BindView(R.id.cl_pernyataan_ket_dp)
    ConstraintLayout cl_pernyataan_ket_dp;
    @BindView(R.id.cl_cb_pernyataan_persetujuan_dp)
    ConstraintLayout cl_cb_pernyataan_peretujuan_dp;
    @BindView(R.id.cl_tanda_tangan_dp)
    ConstraintLayout cl_tanda_tangan_dp;
    @BindView(R.id.cl_child_dp)
    ConstraintLayout cl_child_dp;

    @BindView(R.id.cl_form_foto_dp)
    LinearLayout cl_form_foto_dp;
    @BindView(R.id.cl_form_tanda_tangan_dp)
    LinearLayout cl_form_tanda_tangan_dp;


    @BindView(R.id.iv_circle_pernyataan)
    ImageView iv_circle_pernyataan;

    @BindView(R.id.check_pernyataan)
    CheckBox check_pernyataan;

    @BindView(R.id.scroll_dp)
    ScrollView scroll_dp;

    private Button btn_lanjut, btn_kembali;
    private ImageView iv_save, iv_next, iv_prev;
    private Toolbar second_toolbar;

    private View mExclusiveEmptyView;

    private File file_ttd;
    private File file_foto;

    private int count_ft = 0;
    private int count_ttd = 0;
    private int temp_count_img = 0;
    private int TAKE_PHOTO_CODE = 0;
    private int PICK_FROM_GALLERY = 1;
    private int int_pernyataan = 0;

    private DrawingView drawView;

    private String imgPath = "";
    private String selectedImagePath = "";

    private int JENIS_LOGIN;
    private int JENIS_LOGIN_BC;
    private int GROUP_ID;

    private ArrayList<ModelTTD> ListTTD;
    private ArrayList<ModelFoto> ListFoto;
    private ArrayList<ModelDropDownString> ListDropDownSPAJ;
    private ArrayList<ModelDataDitunjukDI> ListManfaat;

    private String NAMA_AGEN;

    private EspajModel me;

    private String[] jdl_ttd = new String[]{"TTD_dp", "TTD_TU", "TTD_AGEN", "TTD_REF", "TTD_TT1", "TTD_TT2", "TTD_TT3", "TTD_dp_PROP", "TTD_AGEN_PROP"};
    private String[] jdl_foto = new String[]{"FT_BUKTI_PP", "FT_BUKTI_TT", "FT_ASURANSI_1", "FT_ASURANSI_2", "FT_SURATKUASA_1", "FT_SURATKUASA_2",
            "FT_BSB_1", "FT_BSB_2", "FT_BUKTI_TT1", "FT_BUKTI_TT2", "FT_BUKTI_TT3",
            "FT_PROP_1", "FT_PROP_2", "FT_PROP_3", "FT_PROP_4", "FT_PROP_5", "FT_PROP_6", "FT_PROP_7", "FT_PROP_8", "FT_PROP_9", "FT_PROP_10",
            "FT_PROP_11", "FT_PROP_12", "FT_PROP_13", "FT_PROP_14", "FT_PROP_15", "FT_LAIN_1", "FT_LAIN_2", "FT_LAIN_3", "FT_SP_PROVIDER_1",
            "FT_SP_PROVIDER_2", "FT_SP_HUB_1", "FT_SP_HUB_2", "FT_FUBAH", "FT_MEMO", "FT_ACCSTATEMENT", "PROFIL_NASABAH_1", "PROFIL_NASABAH_2"};

    private String judul_foto_dp1, judul_foto_dp2, judul_foto_dp3, judul_foto_dp4, judul_foto_dp5, judul_foto_dp6, judul_foto_dp7, judul_foto_dp8, judul_foto_dp9, judul_foto_dp10, judul_foto_dp11, judul_foto_dp12, judul_foto_dp13, judul_foto_dp14, judul_foto_dp15,
            judul_foto_dp16, judul_foto_dp17, judul_foto_dp18, judul_foto_dp19, judul_foto_dp20, judul_foto_dp21, judul_foto_dp22, judul_foto_dp23, judul_foto_dp24, judul_foto_dp25, judul_foto_dp26,
            judul_ttd_dp1, judul_ttd_dp2, judul_ttd_dp3, judul_ttd_dp4, judul_ttd_dp5, judul_ttd_dp6, judul_ttd_dp7, judul_ttd_dp8, judul_ttd_dp9, judul_ttd_dp10, judul_ttd_dp11, judul_ttd_dp12, judul_ttd_dp13, judul_ttd_dp14, judul_ttd_dp15;

    private String[] judul_foto_dp = {judul_foto_dp1, judul_foto_dp2, judul_foto_dp3, judul_foto_dp4, judul_foto_dp5, judul_foto_dp6, judul_foto_dp7, judul_foto_dp8, judul_foto_dp9, judul_foto_dp10, judul_foto_dp11, judul_foto_dp12, judul_foto_dp13, judul_foto_dp14, judul_foto_dp15,
            judul_foto_dp16, judul_foto_dp17, judul_foto_dp18, judul_foto_dp19, judul_foto_dp20, judul_foto_dp21, judul_foto_dp22, judul_foto_dp23, judul_foto_dp24, judul_foto_dp25, judul_foto_dp26};

    private String[] judul_ttd_dp = {judul_ttd_dp1, judul_ttd_dp2, judul_ttd_dp3, judul_ttd_dp4, judul_ttd_dp5, judul_ttd_dp6, judul_ttd_dp7, judul_ttd_dp8, judul_ttd_dp9, judul_ttd_dp10, judul_ttd_dp11, judul_ttd_dp12, judul_ttd_dp13, judul_ttd_dp14, judul_ttd_dp15};

    private int flag_foto_dp1, flag_foto_dp2, flag_foto_dp3, flag_foto_dp4, flag_foto_dp5, flag_foto_dp6, flag_foto_dp7, flag_foto_dp8, flag_foto_dp9, flag_foto_dp10, flag_foto_dp11, flag_foto_dp12, flag_foto_dp13, flag_foto_dp14, flag_foto_dp15,
            flag_foto_dp16, flag_foto_dp17, flag_foto_dp18, flag_foto_dp19, flag_foto_dp20, flag_foto_dp21, flag_foto_dp22, flag_foto_dp23, flag_foto_dp24, flag_foto_dp25, flag_foto_dp26,
            flag_ttd_dp1, flag_ttd_dp2, flag_ttd_dp3, flag_ttd_dp4, flag_ttd_dp5, flag_ttd_dp6, flag_ttd_dp7, flag_ttd_dp8, flag_ttd_dp9, flag_ttd_dp10, flag_ttd_dp11, flag_ttd_dp12, flag_ttd_dp13, flag_ttd_dp14, flag_ttd_dp15;

    private int[] flag_foto_dp = {flag_foto_dp1, flag_foto_dp2, flag_foto_dp3, flag_foto_dp4, flag_foto_dp5, flag_foto_dp6, flag_foto_dp7, flag_foto_dp8, flag_foto_dp9, flag_foto_dp10, flag_foto_dp11, flag_foto_dp12, flag_foto_dp13, flag_foto_dp14, flag_foto_dp15,
            flag_foto_dp16, flag_foto_dp17, flag_foto_dp18, flag_foto_dp19, flag_foto_dp20, flag_foto_dp21, flag_foto_dp22, flag_foto_dp23, flag_foto_dp24, flag_foto_dp25, flag_foto_dp26};

    private int[] flag_ttd_dp = {flag_ttd_dp1, flag_ttd_dp2, flag_ttd_dp3, flag_ttd_dp4, flag_ttd_dp5, flag_ttd_dp6, flag_ttd_dp7, flag_ttd_dp8, flag_ttd_dp9, flag_ttd_dp10, flag_ttd_dp11, flag_ttd_dp12, flag_ttd_dp13, flag_ttd_dp14, flag_ttd_dp15};


    private ImageView foto_dp1, foto_dp2, foto_dp3, foto_dp4, foto_dp5, foto_dp6, foto_dp7, foto_dp8, foto_dp9, foto_dp10, foto_dp11, foto_dp12, foto_dp13, foto_dp14, foto_dp15,
            foto_dp16, foto_dp17, foto_dp18, foto_dp19, foto_dp20, foto_dp21, foto_dp22, foto_dp23, foto_dp24, foto_dp25, foto_dp26,
            iv_menufoto_dp1, iv_menufoto_dp2, iv_menufoto_dp3, iv_menufoto_dp4, iv_menufoto_dp5, iv_menufoto_dp6, iv_menufoto_dp7, iv_menufoto_dp8, iv_menufoto_dp9, iv_menufoto_dp10, iv_menufoto_dp11, iv_menufoto_dp12, iv_menufoto_dp13, iv_menufoto_dp14, iv_menufoto_dp15,
            iv_menufoto_dp16, iv_menufoto_dp17, iv_menufoto_dp18, iv_menufoto_dp19, iv_menufoto_dp20, iv_menufoto_dp21, iv_menufoto_dp22, iv_menufoto_dp23, iv_menufoto_dp24, iv_menufoto_dp25, iv_menufoto_dp26,
            ttd_dp1, ttd_dp2, ttd_dp3, ttd_dp4, ttd_dp5, ttd_dp6, ttd_dp7, ttd_dp8, ttd_dp9, ttd_dp10, ttd_dp11, ttd_dp12, ttd_dp13, ttd_dp14, ttd_dp15;

    private ImageView[] foto_dp = {foto_dp1, foto_dp2, foto_dp3, foto_dp4, foto_dp5, foto_dp6, foto_dp7, foto_dp8, foto_dp9, foto_dp10, foto_dp11, foto_dp12, foto_dp13, foto_dp14, foto_dp15,
            foto_dp16, foto_dp17, foto_dp18, foto_dp19, foto_dp20, foto_dp21, foto_dp22, foto_dp23, foto_dp24, foto_dp25, foto_dp26};

    private ImageView[] iv_menufoto_dp = {iv_menufoto_dp1, iv_menufoto_dp2, iv_menufoto_dp3, iv_menufoto_dp4, iv_menufoto_dp5, iv_menufoto_dp6, iv_menufoto_dp7, iv_menufoto_dp8, iv_menufoto_dp9, iv_menufoto_dp10, iv_menufoto_dp11, iv_menufoto_dp12, iv_menufoto_dp13, iv_menufoto_dp14, iv_menufoto_dp15,
            iv_menufoto_dp16, iv_menufoto_dp17, iv_menufoto_dp18, iv_menufoto_dp19, iv_menufoto_dp20, iv_menufoto_dp21, iv_menufoto_dp22, iv_menufoto_dp23, iv_menufoto_dp24, iv_menufoto_dp25, iv_menufoto_dp26};

    private ImageView[] ttd_dp = {ttd_dp1, ttd_dp2, ttd_dp3, ttd_dp4, ttd_dp5, ttd_dp6, ttd_dp7, ttd_dp8, ttd_dp9, ttd_dp10, ttd_dp11, ttd_dp12, ttd_dp13, ttd_dp14, ttd_dp15};

    private TextView tv_nomor_foto_dp1, tv_nomor_foto_dp2, tv_nomor_foto_dp3, tv_nomor_foto_dp4, tv_nomor_foto_dp5, tv_nomor_foto_dp6, tv_nomor_foto_dp7, tv_nomor_foto_dp8, tv_nomor_foto_dp9, tv_nomor_foto_dp10, tv_nomor_foto_dp11, tv_nomor_foto_dp12, tv_nomor_foto_dp13, tv_nomor_foto_dp14, tv_nomor_foto_dp15,
            tv_nomor_foto_dp16, tv_nomor_foto_dp17, tv_nomor_foto_dp18, tv_nomor_foto_dp19, tv_nomor_foto_dp20, tv_nomor_foto_dp21, tv_nomor_foto_dp22, tv_nomor_foto_dp23, tv_nomor_foto_dp24, tv_nomor_foto_dp25, tv_nomor_foto_dp26,
            tv_nomor_ttd_dp1, tv_nomor_ttd_dp2, tv_nomor_ttd_dp3, tv_nomor_ttd_dp4, tv_nomor_ttd_dp5, tv_nomor_ttd_dp6, tv_nomor_ttd_dp7, tv_nomor_ttd_dp8, tv_nomor_ttd_dp9, tv_nomor_ttd_dp10, tv_nomor_ttd_dp11, tv_nomor_ttd_dp12, tv_nomor_ttd_dp13, tv_nomor_ttd_dp14, tv_nomor_ttd_dp15,
            hapus_ttd_dp1, hapus_ttd_dp2, hapus_ttd_dp3, hapus_ttd_dp4, hapus_ttd_dp5, hapus_ttd_dp6, hapus_ttd_dp7, hapus_ttd_dp8, hapus_ttd_dp9, hapus_ttd_dp10, hapus_ttd_dp11, hapus_ttd_dp12, hapus_ttd_dp13, hapus_ttd_dp14, hapus_ttd_dp15,
            tambah_ttd_dp1, tambah_ttd_dp2, tambah_ttd_dp3, tambah_ttd_dp4, tambah_ttd_dp5, tambah_ttd_dp6, tambah_ttd_dp7, tambah_ttd_dp8, tambah_ttd_dp9, tambah_ttd_dp10, tambah_ttd_dp11, tambah_ttd_dp12, tambah_ttd_dp13, tambah_ttd_dp14, tambah_ttd_dp15;

    private TextView[] tv_nomor_foto_dp = {tv_nomor_foto_dp1, tv_nomor_foto_dp2, tv_nomor_foto_dp3, tv_nomor_foto_dp4, tv_nomor_foto_dp5, tv_nomor_foto_dp6, tv_nomor_foto_dp7, tv_nomor_foto_dp8, tv_nomor_foto_dp9, tv_nomor_foto_dp10, tv_nomor_foto_dp11, tv_nomor_foto_dp12, tv_nomor_foto_dp13, tv_nomor_foto_dp14, tv_nomor_foto_dp15,
            tv_nomor_foto_dp16, tv_nomor_foto_dp17, tv_nomor_foto_dp18, tv_nomor_foto_dp19, tv_nomor_foto_dp20, tv_nomor_foto_dp21, tv_nomor_foto_dp22, tv_nomor_foto_dp23, tv_nomor_foto_dp24, tv_nomor_foto_dp25, tv_nomor_foto_dp26};

    private TextView[] tv_nomor_ttd_dp = {tv_nomor_ttd_dp1, tv_nomor_ttd_dp2, tv_nomor_ttd_dp3, tv_nomor_ttd_dp4, tv_nomor_ttd_dp5, tv_nomor_ttd_dp6, tv_nomor_ttd_dp7, tv_nomor_ttd_dp8, tv_nomor_ttd_dp9, tv_nomor_ttd_dp10, tv_nomor_ttd_dp11, tv_nomor_ttd_dp12, tv_nomor_ttd_dp13, tv_nomor_ttd_dp14, tv_nomor_ttd_dp15};

    private TextView[] hapus_ttd_dp = {hapus_ttd_dp1, hapus_ttd_dp2, hapus_ttd_dp3, hapus_ttd_dp4, hapus_ttd_dp5, hapus_ttd_dp6, hapus_ttd_dp7, hapus_ttd_dp8, hapus_ttd_dp9, hapus_ttd_dp10, hapus_ttd_dp11, hapus_ttd_dp12, hapus_ttd_dp13, hapus_ttd_dp14, hapus_ttd_dp15};

    private TextView[] tambah_ttd_dp = {tambah_ttd_dp1, tambah_ttd_dp2, tambah_ttd_dp3, tambah_ttd_dp4, tambah_ttd_dp5, tambah_ttd_dp6, tambah_ttd_dp7, tambah_ttd_dp8, tambah_ttd_dp9, tambah_ttd_dp10, tambah_ttd_dp11, tambah_ttd_dp12, tambah_ttd_dp13, tambah_ttd_dp14, tambah_ttd_dp15};

    private MaterialBetterSpinner ac_judul_foto_dp1, ac_judul_foto_dp2, ac_judul_foto_dp3, ac_judul_foto_dp4, ac_judul_foto_dp5, ac_judul_foto_dp6, ac_judul_foto_dp7, ac_judul_foto_dp8, ac_judul_foto_dp9, ac_judul_foto_dp10, ac_judul_foto_dp11, ac_judul_foto_dp12, ac_judul_foto_dp13, ac_judul_foto_dp14, ac_judul_foto_dp15,
            ac_judul_foto_dp16, ac_judul_foto_dp17, ac_judul_foto_dp18, ac_judul_foto_dp19, ac_judul_foto_dp20, ac_judul_foto_dp21, ac_judul_foto_dp22, ac_judul_foto_dp23, ac_judul_foto_dp24, ac_judul_foto_dp25, ac_judul_foto_dp26,
            ac_judul_ttd_dp1, ac_judul_ttd_dp2, ac_judul_ttd_dp3, ac_judul_ttd_dp4, ac_judul_ttd_dp5, ac_judul_ttd_dp6, ac_judul_ttd_dp7, ac_judul_ttd_dp8, ac_judul_ttd_dp9, ac_judul_ttd_dp10, ac_judul_ttd_dp11, ac_judul_ttd_dp12, ac_judul_ttd_dp13, ac_judul_ttd_dp14, ac_judul_ttd_dp15;

    private MaterialBetterSpinner[] ac_judul_foto_dp = {ac_judul_foto_dp1, ac_judul_foto_dp2, ac_judul_foto_dp3, ac_judul_foto_dp4, ac_judul_foto_dp5, ac_judul_foto_dp6, ac_judul_foto_dp7, ac_judul_foto_dp8, ac_judul_foto_dp9, ac_judul_foto_dp10, ac_judul_foto_dp11, ac_judul_foto_dp12, ac_judul_foto_dp13, ac_judul_foto_dp14, ac_judul_foto_dp15,
            ac_judul_foto_dp16, ac_judul_foto_dp17, ac_judul_foto_dp18, ac_judul_foto_dp19, ac_judul_foto_dp20, ac_judul_foto_dp21, ac_judul_foto_dp22, ac_judul_foto_dp23, ac_judul_foto_dp24, ac_judul_foto_dp25, ac_judul_foto_dp26};

    private MaterialBetterSpinner[] ac_judul_ttd_dp = {ac_judul_ttd_dp1, ac_judul_ttd_dp2, ac_judul_ttd_dp3, ac_judul_ttd_dp4, ac_judul_ttd_dp5, ac_judul_ttd_dp6, ac_judul_ttd_dp7, ac_judul_ttd_dp8, ac_judul_ttd_dp9, ac_judul_ttd_dp10, ac_judul_ttd_dp11, ac_judul_ttd_dp12, ac_judul_ttd_dp13, ac_judul_ttd_dp14, ac_judul_ttd_dp15};

    private EditText nama_ttd_dp1, nama_ttd_dp2, nama_ttd_dp3, nama_ttd_dp4, nama_ttd_dp5, nama_ttd_dp6, nama_ttd_dp7, nama_ttd_dp8, nama_ttd_dp9, nama_ttd_dp10, nama_ttd_dp11, nama_ttd_dp12, nama_ttd_dp13, nama_ttd_dp14, nama_ttd_dp15;

    private EditText[] nama_ttd_dp = {nama_ttd_dp1, nama_ttd_dp2, nama_ttd_dp3, nama_ttd_dp4, nama_ttd_dp5, nama_ttd_dp6, nama_ttd_dp7, nama_ttd_dp8, nama_ttd_dp9, nama_ttd_dp10, nama_ttd_dp11, nama_ttd_dp12, nama_ttd_dp13, nama_ttd_dp14, nama_ttd_dp15};

    private File file_foto_dp1, file_foto_dp2, file_foto_dp3, file_foto_dp4, file_foto_dp5, file_foto_dp6, file_foto_dp7, file_foto_dp8, file_foto_dp9, file_foto_dp10, file_foto_dp11, file_foto_dp12, file_foto_dp13, file_foto_dp14, file_foto_dp15,
            file_foto_dp16, file_foto_dp17, file_foto_dp18, file_foto_dp19, file_foto_dp20, file_foto_dp21, file_foto_dp22, file_foto_dp23, file_foto_dp24, file_foto_dp25, file_foto_dp26;

    private File[] file_foto_dp = {file_foto_dp1, file_foto_dp2, file_foto_dp3, file_foto_dp4, file_foto_dp5, file_foto_dp6, file_foto_dp7, file_foto_dp8, file_foto_dp9, file_foto_dp10, file_foto_dp11, file_foto_dp12, file_foto_dp13, file_foto_dp14, file_foto_dp15,
            file_foto_dp16, file_foto_dp17, file_foto_dp18, file_foto_dp19, file_foto_dp20, file_foto_dp21, file_foto_dp22, file_foto_dp23, file_foto_dp24, file_foto_dp25, file_foto_dp26};

    private File file_ttd_dp1, file_ttd_dp2, file_ttd_dp3, file_ttd_dp4, file_ttd_dp5, file_ttd_dp6, file_ttd_dp7, file_ttd_dp8, file_ttd_dp9, file_ttd_dp10, file_ttd_dp11, file_ttd_dp12, file_ttd_dp13, file_ttd_dp14, file_ttd_dp15;

    private File[] file_ttd_dp = {file_ttd_dp1, file_ttd_dp2, file_ttd_dp3, file_ttd_dp4, file_ttd_dp5, file_ttd_dp6, file_ttd_dp7, file_ttd_dp8, file_ttd_dp9, file_ttd_dp10, file_ttd_dp11, file_ttd_dp12, file_ttd_dp13, file_ttd_dp14, file_ttd_dp15};

    private ProgressDialog progressDialog;


}