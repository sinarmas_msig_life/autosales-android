package id.co.ajsmsig.nb.prop.method;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductTopUpModel;

/**
 * Created by faiz_f on 05/04/2017.
 */

public class P_StaticMethods {
    public static final String TAG = P_StaticMethods.class.getSimpleName();

    public static String getSexLabel(Context context, Integer sexId) {
        String theSex = "";
        if (sexId != null) {
            if (sexId == 1) {
                theSex = context.getString(R.string.laki_laki);
            } else if (sexId == 0) {
                theSex = context.getString(R.string.perempuan);
            }
        }
        return theSex;
    }

    public static Boolean isPPandTTSamePerson(P_MstDataProposalModel p_mstDataProposalModel) {
        boolean isSame = true;
        if (p_mstDataProposalModel.getNama_pp() != null) {
            if (!p_mstDataProposalModel.getNama_pp().equals(p_mstDataProposalModel.getNama_tt())) {
                isSame = false;
            }
        }
        if (p_mstDataProposalModel.getTgl_lahir_pp() != null) {
            if (!p_mstDataProposalModel.getTgl_lahir_pp().equals(p_mstDataProposalModel.getTgl_lahir_tt())) {
                isSame = false;
            }
        }
        if (p_mstDataProposalModel.getSex_pp() != null) {
            if (!p_mstDataProposalModel.getSex_pp().equals(p_mstDataProposalModel.getSex_tt())) {
                isSame = false;
            }
        }

        return isSame;
    }

    public static ArrayList<DropboxModel> getRangeNumberDropBox(int start, int end, int jump, String specialChar) {
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();
        for (int i = start; i <= end; i += jump) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setId(i);
            dropboxModel.setLabel(i + specialChar);
            dropboxModels.add(dropboxModel);

        }
        return dropboxModels;
    }

    public static ArrayList<DropboxModel> getRangeNumberDropBox(int start, int end, int jump) {
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();
        for (int i = start; i <= end; i += jump) {
            DropboxModel dropboxModel = new DropboxModel();
            dropboxModel.setId(i);
            dropboxModel.setLabel(String.valueOf(i));
            dropboxModels.add(dropboxModel);

        }
        return dropboxModels;
    }


    public static String getErrorChooseDropDownVerR(String lkuId, int s, BigDecimal up) {
        String error = "";
        switch (lkuId) {
            case "01":
                if (up.compareTo(BigDecimal.valueOf(50000000)) <= 0 && s > 1) {
                    error = getErrorTextHospitalProtectionFamily("50 juta", "R-100");
                } else if (up.compareTo(BigDecimal.valueOf(100000000)) <= 0 && s > 2) {
                    error = getErrorTextHospitalProtectionFamily("100 juta", "R-200");
                } else if (up.compareTo(BigDecimal.valueOf(150000000L)) <= 0 && s > 3) {
                    error = getErrorTextHospitalProtectionFamily("150 juta", "R-300");
                } else if (up.compareTo(BigDecimal.valueOf(200000000L)) <= 0 && s > 4) {
                    error = getErrorTextHospitalProtectionFamily("200 juta", "R-400");
                } else if (up.compareTo(BigDecimal.valueOf(250000000L)) <= 0 && s > 5) {
                    error = getErrorTextHospitalProtectionFamily("250 juta", "R-500");
                } else if (up.compareTo(BigDecimal.valueOf(300000000L)) <= 0 && s > 6) {
                    error = getErrorTextHospitalProtectionFamily("300 juta", "R-600");
                } else if (up.compareTo(BigDecimal.valueOf(350000000L)) <= 0 && s > 7) {
                    error = getErrorTextHospitalProtectionFamily("350 juta", "R-700");
                } else if (up.compareTo(BigDecimal.valueOf(400000000L)) <= 0 && s > 8) {
                    error = getErrorTextHospitalProtectionFamily("400 juta", "R-800");
                } else if (up.compareTo(BigDecimal.valueOf(450000000L)) <= 0 && s > 9) {
                    error = getErrorTextHospitalProtectionFamily("450 juta", "R-900");
                } else if (up.compareTo(BigDecimal.valueOf(500000000L)) <= 0 && s > 10) {
                    error = getErrorTextHospitalProtectionFamily("500 juta", "R-1000");
                }
                break;
            case "02":
                if (up.compareTo(BigDecimal.valueOf(5000L)) <= 0 && s > 1) {
                    error = getErrorTextHospitalProtectionFamily("US$ 5000", "D-10");
                } else if (up.compareTo(BigDecimal.valueOf(10000L)) <= 0 && s > 2) {
                    error = getErrorTextHospitalProtectionFamily("US$ 10000", "D-20");
                } else if (up.compareTo(BigDecimal.valueOf(15000L)) <= 0 && s > 3) {
                    error = getErrorTextHospitalProtectionFamily("US$ 15000", "D-30");
                } else if (up.compareTo(BigDecimal.valueOf(20000L)) <= 0 && s > 4) {
                    error = getErrorTextHospitalProtectionFamily("US$ 20000", "D-40");
                } else if (up.compareTo(BigDecimal.valueOf(25000L)) <= 0 && s > 5) {
                    error = getErrorTextHospitalProtectionFamily("US$ 25000", "D-50");
                } else if (up.compareTo(BigDecimal.valueOf(30000L)) <= 0 && s > 6) {
                    error = getErrorTextHospitalProtectionFamily("US$ 30000", "D-60");
                } else if (up.compareTo(BigDecimal.valueOf(35000L)) <= 0 && s > 7) {
                    error = getErrorTextHospitalProtectionFamily("US$ 35000", "D-70");
                } else if (up.compareTo(BigDecimal.valueOf(40000L)) <= 0 && s > 8) {
                    error = getErrorTextHospitalProtectionFamily("US$ 40000", "D-80");
                } else if (up.compareTo(BigDecimal.valueOf(45000L)) <= 0 && s > 9) {
                    error = getErrorTextHospitalProtectionFamily("US$ 45000", "D-90");
                } else if (up.compareTo(BigDecimal.valueOf(50000L)) <= 0 && s > 10) {
                    error = getErrorTextHospitalProtectionFamily("US$ 50000", "D-100");
                }
                break;
            default:
                break;
        }

        return error;
    }

    private static String getErrorTextHospitalProtectionFamily(String nilai, String plan) {
        return "Untuk UP dibawah " + nilai + " juta hanya boleh Plan " + plan + " atau dibawahnya";
    }

    public static String getHeaderPeserta(Context context, boolean isRiderLadies, int position) {
        String header;
        if (isRiderLadies) {
            if (position == 0) {
                header = context.getString(R.string.tertanggung_utama);
            } else {
                header = context.getString(R.string.tertanggung_tambahan) + " " + position;
            }
        } else {
            header = context.getString(R.string.tertanggung_tambahan) + " " + (position + 1);
        }
        return header;
    }

    public static ArrayList<P_MstProposalProductTopUpModel> getDefaultTPValue() {
        ArrayList<P_MstProposalProductTopUpModel> tpListDummy = new ArrayList<>();
        for (int i = 1; i <= 50; i++) {
            P_MstProposalProductTopUpModel mstProposalProductTopUpModel = new P_MstProposalProductTopUpModel();
            mstProposalProductTopUpModel.setThn_ke(i);
            mstProposalProductTopUpModel.setTopup("0");
            mstProposalProductTopUpModel.setTarik("0");
            mstProposalProductTopUpModel.setSelected(false);
            tpListDummy.add(mstProposalProductTopUpModel);
        }

        return tpListDummy;
    }

    public static File getFileForPdf(String pdfName) {
        File root = Environment.getExternalStorageDirectory();
        File dir = new File(root.getAbsolutePath() + "/AutoSales/pdf");
        if (!dir.exists()) {
            boolean createDir = dir.mkdirs();
            Log.d("TAG", "createDir: " + createDir);
        }
        return new File(dir, pdfName);
    }

    public static File getFileProposalPdf(Context context,  String noProposal) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_preferences), Context.MODE_PRIVATE);
        String kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
        String name = kodeAgen + "_" + noProposal + ".pdf";
        File root = Environment.getExternalStorageDirectory();
        File dir = new File(root.getAbsolutePath() + "/AutoSales/pdf");
        if (!dir.exists()) {
            boolean createDir = dir.mkdirs();
            Log.d("TAG", "createDir: " + createDir);
        }
        return new File(dir, name);
    }


    public static BigDecimal getPremi(Context context, HashMap<String, Object> params) {
        BigDecimal premi = BigDecimal.ZERO;
        P_Select select = new P_Select(context);
        HashMap<String, Object> premiRule = select.selectPremi(params);

        Double lscbKaliOld = (Double) premiRule.get(context.getString(R.string.LSCB_KALI_OLD));
        Integer lscbKali = (Integer) premiRule.get(context.getString(R.string.LSCB_KALI));
        String lpfQuery = (String) premiRule.get(context.getString(R.string.LPF_QUERY));
        if(lscbKaliOld!=null){
            params.put(context.getString(R.string.LPF_QUERY), lpfQuery);
            params.put(context.getString(R.string.LSCB_KALI), lscbKali);
            params.put(context.getString(R.string.LSCB_KALI_OLD), lscbKaliOld);
            premi = select.executeQueryPremi(params);
        }

        return premi;
    }

    public static BigDecimal getUP(Context context, HashMap<String, Object> params) {
        P_Select select = new P_Select(context);
//        Log.d("GetMinUp", "params getMinUP: "+params);
        HashMap<String, Object> hashMap = select.selectUp(params);
//        Log.d("GetMinUp", "hashmap getMinUP: "+hashMap);
        BigDecimal up = (BigDecimal) hashMap.get(context.getString(R.string.UP));
        if (up != null) {
            if (up.compareTo(BigDecimal.ZERO) == 0) up = null;
        }
        Integer lpfUP = (Integer) hashMap.get(context.getString(R.string.LPF_UP));
        if (lpfUP == null) lpfUP = 0;
        String lpfQuery = (String) hashMap.get(context.getString(R.string.LPF_QUERY));
        int lscbKali = (int) hashMap.get(context.getString(R.string.LSCB_KALI));


        if (lpfUP != 0) {
            Log.d(TAG, "getUP: ");
            params.put(context.getString(R.string.LPF_QUERY), lpfQuery);
            params.put(context.getString(R.string.LSCB_KALI), lscbKali);
            up = select.executeQueryUp(params);
        }

        return up;
    }

    public static BigDecimal getMinUP(Context context, HashMap<String, Object> params) {
        P_Select select = new P_Select(context);
//        Log.d("GetMinUp", "params getMinUP: "+params);
        HashMap<String, Object> hashMap = select.selectMinUp(params);
//        Log.d("GetMinUp", "hashmap getMinUP: "+hashMap);
        BigDecimal minimumUp = (BigDecimal) hashMap.get(context.getString(R.string.MIN_UP));
        Integer lpfMinUP = (Integer) hashMap.get(context.getString(R.string.LPF_MIN_UP));
        if (lpfMinUP == null) lpfMinUP = 0;
        String lpfQuery = (String) hashMap.get(context.getString(R.string.LPF_QUERY));
        int lscbKali = (int) hashMap.get(context.getString(R.string.LSCB_KALI));


        if (lpfMinUP != 0) {
            params.put(context.getString(R.string.LPF_QUERY), lpfQuery);
            params.put(context.getString(R.string.LSCB_KALI), lscbKali);
            minimumUp = select.executeQueryMinUp(params);
        }

        return minimumUp;
    }


    public static BigDecimal getMaxUP(Context context, HashMap<String, Object> params) {
        P_Select select = new P_Select(context);
        HashMap<String, Object> hashMap = select.selectMaxUp(params);

        BigDecimal maximumUp = (BigDecimal) hashMap.get(context.getString(R.string.MAX_UP));
        Integer lpfMaxUP = (Integer) hashMap.get(context.getString(R.string.LPF_MAX_UP));
        if (lpfMaxUP == null) lpfMaxUP = 0;
        String lpfQuery = (String) hashMap.get(context.getString(R.string.LPF_QUERY));
        Log.d("TAG", "getMaxUP: query = " + lpfQuery);
        int lscbKali = (int) hashMap.get(context.getString(R.string.LSCB_KALI));

        if (lpfMaxUP != 0) {
            params.put(context.getString(R.string.LPF_QUERY), lpfQuery);
            params.put(context.getString(R.string.LSCB_KALI), lscbKali);
            maximumUp = select.executeQueryMaxUp(params);
        }

        return maximumUp;
    }

    public static ArrayList<DropboxModel> getRiderBabyPlanNumbers(){
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();
        DropboxModel plan1 = new DropboxModel("Plan 1", 4);
        DropboxModel plan2 = new DropboxModel("Plan 2", 5);
        DropboxModel plan3 = new DropboxModel("Plan 3", 6);

        dropboxModels.add(plan1);
        dropboxModels.add(plan2);
        dropboxModels.add(plan3);
        return dropboxModels;
    }

    public static DropboxModel getRiderBabyPlanNumber(int lsdbsNumber){
        switch (lsdbsNumber){
            case 4:
                return new DropboxModel("Plan 1", 4);
            case 5:
                return new DropboxModel("Plan 2", 5);
            case 6:
                return new DropboxModel("Plan 3", 6);
            default:
                throw  new IllegalArgumentException("Gak bener nih lsdbs Numbernya");
        }
    }

    public static ArrayList<DropboxModel> getRiderWaiver556065s(){
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();
        DropboxModel plan55 = new DropboxModel("55", 1);
        DropboxModel plan60 = new DropboxModel("60", 2);
        DropboxModel plan65 = new DropboxModel("65", 3);

        dropboxModels.add(plan55);
        dropboxModels.add(plan60);
        dropboxModels.add(plan65);
        return dropboxModels;
    }

    public static DropboxModel getRiderWaiver556065(int lsdbsNumber){
        switch (lsdbsNumber){
            case 1:
                return new DropboxModel("55", 1);
            case 2:
                return new DropboxModel("60", 2);
            case 3:
                return new DropboxModel("65", 3);
            default:
                throw  new IllegalArgumentException("Gak bener nih lsdbs Numbernya");
        }
    }


}
