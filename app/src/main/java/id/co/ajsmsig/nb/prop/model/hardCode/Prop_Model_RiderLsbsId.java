package id.co.ajsmsig.nb.prop.model.hardCode;


public class Prop_Model_RiderLsbsId {

	public static final int RIDER_PA = 810;
    public static final int RIDER_HCP = 811;
    public static final int RIDER_TPD = 812;
    public static final int RIDER_CI = 813;
    public static final int RIDER_WAIVER_TPD = 814;
    public static final int RIDER_PAYOR_TPD_DEATH = 815;
    public static final int RIDER_WAIVER_CI = 816;
    public static final int RIDER_PAYOR_CI = 817;
    public static final int RIDER_TERM = 818;
    public static final int RIDER_HCP_FAMILY = 819;
    public static final int RIDER_EKA_SEHAT = 823;
    public static final int RIDER_EKA_SEHAT_INNER_LIMIT = 825;
    public static final int RIDER_HCP_PROVIDER = 826;
    public static final int RIDER_WAIVER_TPD_CI = 827;
    public static final int RIDER_PAYOR_TPD_CI_DEATH = 828;
    public static final int RIDER_LADIES_INSURANCE = 830;
    public static final int RIDER_HCP_LADIES = 831;
    public static final int RIDER_LADIES_MED_EXPENSE = 832;
    public static final int RIDER_LADIES_MED_EXPENSE_INNER_LIMIT = 833;
    public static final int RIDER_SCHOLARSHIP = 835;
    public static final int RIDER_BABY = 836;
    public static final int RIDER_EARLY_STAGE_CI99 = 837;
    public static final int RIDER_MEDICAL_PLUS_AC = 838;
    public static final int RIDER_MEDICAL_PLUS_IL = 839;
    public static final int RIDER_SIMAS_PRIMA = 845;
    public static final int RIDER_MEDICAL_EXTRA = 848;
	
}
