package id.co.ajsmsig.nb.prop.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.AutoCompleteTextViewClickListener;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductUlinkModel;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;
import id.co.ajsmsig.nb.prop.model.form.PilihanInvestasiModel;

import static id.co.ajsmsig.nb.prop.model.hardCode.P_FieldSettingFlag.TOMBOL_TOPUP_PENARIKAN;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FragmentPagePosition.DATA_PROPOSAL_PAGE;

/**
 * Created by faiz_f on 03/02/2017.
 */

public class P_PilihanInvestasiFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener, View.OnFocusChangeListener {
    private static final String TAG = P_PilihanInvestasiFragment.class.getSimpleName();


    private P_ProposalModel proposalModel;
    private P_MstDataProposalModel mstDataProposalModel;
    private P_MstProposalProductModel mstProposalProductModel;
    private ArrayList<P_MstProposalProductUlinkModel> mstProposalProductUlinkModels;
    private int psetId;
    private P_Select select;
    private ArrayList<PilihanInvestasiModel> pilihanInvestasiModels;
    private int flagInvestFund;

    private DropboxModel dFix = new DropboxModel();
    private DropboxModel dFixPercent = new DropboxModel();
    private DropboxModel dFix2 = new DropboxModel();
    private DropboxModel dFixPercent2 = new DropboxModel();
    private DropboxModel dDynamic = new DropboxModel();
    private DropboxModel dDynamicPercent = new DropboxModel();
    private DropboxModel dDynamic2 = new DropboxModel();
    private DropboxModel dDynamicPercent2 = new DropboxModel();
    private DropboxModel dAggressive = new DropboxModel();
    private DropboxModel dAggressivePercent = new DropboxModel();
    private DropboxModel dAggressive2 = new DropboxModel();
    private DropboxModel dAggressivePercent2 = new DropboxModel();
    private DropboxModel dAggressive3 = new DropboxModel();
    private DropboxModel dAggressivePercent3 = new DropboxModel();
    private DropboxModel dCash = new DropboxModel();
    private DropboxModel dCashPercent = new DropboxModel();
    private DropboxModel dCash2 = new DropboxModel();
    private DropboxModel dCashPercent2 = new DropboxModel();

    private ArrayList<DropboxModel> listJenisInvestFix;
    private ArrayList<DropboxModel> listJenisInvestDynamic;
    private ArrayList<DropboxModel> listJenisInvestAggressive;
    private ArrayList<DropboxModel> listJenisInvestCash;


    public P_PilihanInvestasiFragment() {
    }

    private void loadMyBundle() {
        select = new P_Select(getContext());

        proposalModel = P_MainActivity.myBundle.getParcelable(getString(R.string.proposal_model));
        mstDataProposalModel = proposalModel != null ? proposalModel.getMst_data_proposal() : null;
        mstProposalProductModel = proposalModel != null ? proposalModel.getMst_proposal_product() : null;
        mstProposalProductUlinkModels = proposalModel != null ? proposalModel.getMst_proposal_product_ulink() : null;
        if (mstProposalProductModel != null) {
            psetId = select.selectPsetIdProductSetup(mstProposalProductModel.getLsbs_id(), mstProposalProductModel.getLsdbs_number());
        }

        pilihanInvestasiModels = new ArrayList<>();
        flagInvestFund = select.selectFlagInvestFund(psetId, mstProposalProductModel.getLku_id(), mstProposalProductModel.getCara_bayar());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.p_fragment_pilihan_investasi, container, false);
        ButterKnife.bind(this, view);
        initiateView();

        return view;
    }

    private void initiateView() {
        ((P_MainActivity) getActivity()).setSecondToolbarTitle(getString(R.string.fragment_title_investasi_proposal));
        ((P_MainActivity) getActivity()).getSecond_toolbar().setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_save:
                        saveToMyBundle();
                        P_MainActivity.saveState(getContext(), true);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
        loadMyBundle();
        setListener();


        listJenisInvestFix = select.selectListJenisInvest(psetId, mstProposalProductModel.getLku_id(), getString(R.string.fix));

        listJenisInvestDynamic = select.selectListJenisInvest(psetId, mstProposalProductModel.getLku_id(), getString(R.string.dynamic));

        listJenisInvestAggressive = select.selectListJenisInvest(psetId, mstProposalProductModel.getLku_id(), getString(R.string.aggressive));

        listJenisInvestCash = select.selectListJenisInvest(psetId, mstProposalProductModel.getLku_id(), getString(R.string.money_market));

        switch (flagInvestFund) {
            case 0:
                /*8 baris*/

                /* untuk equity bakti peduli */
                if (psetId == 27 || psetId == 1430 || psetId == 1431 || psetId == 987 || psetId == 989 || psetId == 990 || psetId == 986 ||
                        psetId == 988 || psetId == 992 || psetId == 967) {
                    setFillDataBaris9();
                } else if (psetId == 1297){
                    setFillDataSIAP2U();
                } else {
                    setFillDataBaris8();
                }
                break;
            case 1:
                /*cuma 4 baris*/
                /*untuk equity bakti peduli*/
                if ( psetId == 1065 ){
                    setFillDataBaris4();
                } else {
                    if (mstDataProposalModel.getFlag_packet() != null) {
                        setFillDataSIAP2U();
                    } else {
                        setFillDataBaris4();
                    }
                }
                break;
            case 2:
//tinggal di set di local
                setFillDataSIAP2U();
                break;
            case 3:
                setFillDataBaris9();
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initiateAdapter();
    }

    private void initiateAdapter() {
        switch (flagInvestFund) {
            case 0:
                /*8 baris*/
                /* untuk equity bakti peduli */
                if (psetId ==  27 || psetId == 1430 || psetId == 1431 || psetId == 987 || psetId == 989 || psetId == 990 || psetId == 986 ||
                    psetId == 988 || psetId == 992 || psetId == 967) {
                    setDefaultDataBaris9();
                } else if (psetId == 1297){
                    setDefaultSIAP2U();
                } else {
                    setDefaultDataBaris8();
                }
                break;
            case 1:
                /*cuma 4 baris*/
                /* untuk equity bakti peduli */
                if (psetId == 1065 ){
                    setDefaultDataBaris4();
                } else {
                    if (mstDataProposalModel.getFlag_packet() != null) {
                        setDefaultSIAP2U();//sekarang masih hardcode dlu, nanti pakai case yang 2
                    } else {
                        setDefaultDataBaris4();
                    }
                }
                break;
            case 2:
//                cuma 1 baris
                setDefaultSIAP2U(); // belum di set di local
                break;
            case 3:
                setDefaultDataBaris9();
                break;
            default:
                break;
        }


    }

    private ArrayList<DropboxModel> getDropboxNilaiInvestasi() {
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();
        for (int i = 0; i <= 100; i = i + 10) {
            DropboxModel propModelSpinner = new DropboxModel();
            propModelSpinner.setLabel(i + "%");
            propModelSpinner.setId(i);
            dropboxModels.add(propModelSpinner);
        }
        return dropboxModels;
    }

    private void setDefaultDataBaris4() {


        setDefaulPerBaris(listJenisInvestFix, 0, ac_fix, ac_fix_percent, dFix, dFixPercent);


        setDefaulPerBaris(listJenisInvestDynamic, 0, ac_dynamic, ac_dynamic_percent, dDynamic, dDynamicPercent);

        /*set default aggressive*/
        if (listJenisInvestAggressive.size() > 0) {


            setDefaulPerBaris(listJenisInvestAggressive, 0, ac_aggresive, ac_aggresive_percent, dAggressive, dAggressivePercent);
        } else {
            tv_aggresive.setVisibility(View.GONE);
            cl_aggresive.setVisibility(View.GONE);
        }

        /*set default cash*/
        if (listJenisInvestCash.size() > 0) {
//

            setDefaulPerBaris(listJenisInvestCash, 0, ac_money_market, ac_money_market_percent, dCash, dCashPercent);
        } else {
            tv_money_market.setVisibility(View.GONE);
            cl_money_market.setVisibility(View.GONE);
        }
    }

    public void setDefaultDataBaris8() {

        /*set default fix*/
        setDefaultBaris8(
                listJenisInvestFix, cl_fix2,
                ac_fix, ac_fix_percent,
                ac_fix2, ac_fix_percent2,
                dFix, dFixPercent, dFix2, dFixPercent2);

        /*set default dynamix*/
        setDefaultBaris8(
                listJenisInvestDynamic, cl_dynamic2,
                ac_dynamic, ac_dynamic_percent,
                ac_dynamic2, ac_dynamic_percent2,
                dDynamic, dDynamicPercent, dDynamic2, dDynamicPercent2);

        /*set default aggressive*/
        if (listJenisInvestAggressive.size() > 0) {
            setDefaultBaris8(
                    listJenisInvestAggressive, cl_aggresive2,
                    ac_aggresive, ac_aggresive_percent,
                    ac_aggresive2, ac_aggresive_percent2,
                    dAggressive, dAggressivePercent, dAggressive2, dAggressivePercent2);
        } else {
            tv_aggresive.setVisibility(View.GONE);
            cl_aggresive.setVisibility(View.GONE);
            cl_aggresive2.setVisibility(View.GONE);
        }


        /*set default cash*/
        if (listJenisInvestCash.size() > 0) {
            setDefaultBaris8(
                    listJenisInvestCash, cl_money_market2,
                    ac_money_market, ac_money_market_percent,
                    ac_money_market2, ac_money_market_percent2,
                    dCash, dCashPercent, dCash2, dCashPercent2);
        }
        else {
            tv_money_market.setVisibility(View.GONE);
            cl_money_market.setVisibility(View.GONE);
            cl_money_market2.setVisibility(View.GONE);
        }
    }

    public void setDefaultDataBaris9() {
        /*set default fix*/
        setDefaultBaris8(
                listJenisInvestFix, cl_fix2,
                ac_fix, ac_fix_percent,
                ac_fix2, ac_fix_percent2,
                dFix, dFixPercent, dFix2, dFixPercent2);

        /*set default dynamix*/
        setDefaultBaris8(
                listJenisInvestDynamic, cl_dynamic2,
                ac_dynamic, ac_dynamic_percent,
                ac_dynamic2, ac_dynamic_percent2,
                dDynamic, dDynamicPercent, dDynamic2, dDynamicPercent2);

        /*set default aggressive*/
        if (listJenisInvestAggressive.size() > 0) {
            setDefaultBaris9(
                    listJenisInvestAggressive, cl_aggresive2, cl_aggressive3,
                    ac_aggresive, ac_aggresive_percent,
                    ac_aggresive2, ac_aggresive_percent2, ac_aggresive3, ac_aggresive_percent3,
                    dAggressive, dAggressivePercent, dAggressive2, dAggressivePercent2, dAggressive3, dAggressivePercent3);
        } else {
            tv_aggresive.setVisibility(View.GONE);
            cl_aggresive.setVisibility(View.GONE);
            cl_aggresive2.setVisibility(View.GONE);
        }


        /*set default cash*/
        if (listJenisInvestCash.size() > 0) {
            setDefaultBaris8(
                    listJenisInvestCash, cl_money_market2,
                    ac_money_market, ac_money_market_percent,
                    ac_money_market2, ac_money_market_percent2,
                    dCash, dCashPercent, dCash2, dCashPercent2);
        }
        else {
            tv_money_market.setVisibility(View.GONE);
            cl_money_market.setVisibility(View.GONE);
            cl_money_market2.setVisibility(View.GONE);
        }
    }

    public void setFillDataBaris9() {


        for (P_MstProposalProductUlinkModel mstProposalProductUlinkModel : mstProposalProductUlinkModels) {
//            boolean foundSameLjiId = false;
            DropboxModel infoInvest = select.selectInfoInvest(mstProposalProductUlinkModel.getLji_id());

            if (infoInvest.getLabelSecond().equals(getString(R.string.fix))) {
                setValueBaris8(
                        listJenisInvestFix, mstProposalProductUlinkModel,
                        ac_fix, ac_fix_percent, ac_fix2, ac_fix_percent2,
                        dFix, dFixPercent, dFix2, dFixPercent2);
            } else if (infoInvest.getLabelSecond().equals(getString(R.string.dynamic))) {
                setValueBaris8(
                        listJenisInvestDynamic, mstProposalProductUlinkModel,
                        ac_dynamic, ac_dynamic_percent, ac_dynamic2, ac_dynamic_percent2,
                        dDynamic, dDynamicPercent, dDynamic2, dDynamicPercent2);
            } else if (infoInvest.getLabelSecond().equals(getString(R.string.aggressive))) {
                setValueBaris9(
                        listJenisInvestAggressive, mstProposalProductUlinkModel,
                        ac_aggresive, ac_aggresive_percent, ac_aggresive2, ac_aggresive_percent2, ac_aggresive3, ac_aggresive_percent3,
                        dAggressive, dAggressivePercent, dAggressive2, dAggressivePercent2, dAggressive3, dAggressivePercent3);
            } else if (infoInvest.getLabelSecond().equals(getString(R.string.money_market))) {
                setValueBaris8(
                        listJenisInvestCash, mstProposalProductUlinkModel,
                        ac_money_market, ac_money_market_percent, ac_money_market2, ac_money_market_percent2,
                        dCash, dCashPercent, dCash2, dCashPercent2);
            } else {
//                do nothing
            }
        }
    }

    public void setFillDataBaris8() {

        for (P_MstProposalProductUlinkModel mstProposalProductUlinkModel : mstProposalProductUlinkModels) {
//            boolean foundSameLjiId = false;
            DropboxModel infoInvest = select.selectInfoInvest(mstProposalProductUlinkModel.getLji_id());

            if (infoInvest.getLabelSecond().equals(getString(R.string.fix))) {
                setValueBaris8(
                        listJenisInvestFix, mstProposalProductUlinkModel,
                        ac_fix, ac_fix_percent, ac_fix2, ac_fix_percent2,
                        dFix, dFixPercent, dFix2, dFixPercent2);
            } else if (infoInvest.getLabelSecond().equals(getString(R.string.dynamic))) {
                setValueBaris8(
                        listJenisInvestDynamic, mstProposalProductUlinkModel,
                        ac_dynamic, ac_dynamic_percent, ac_dynamic2, ac_dynamic_percent2,
                        dDynamic, dDynamicPercent, dDynamic2, dDynamicPercent2);
            } else if (infoInvest.getLabelSecond().equals(getString(R.string.aggressive))) {
                setValueBaris9(
                        listJenisInvestAggressive, mstProposalProductUlinkModel,
                        ac_aggresive, ac_aggresive_percent, ac_aggresive2, ac_aggresive_percent2, ac_aggresive3, ac_aggresive_percent3,
                        dAggressive, dAggressivePercent, dAggressive2, dAggressivePercent2, dAggressive3, dAggressivePercent3);
            } else if (infoInvest.getLabelSecond().equals(getString(R.string.money_market))) {
                setValueBaris8(
                        listJenisInvestCash, mstProposalProductUlinkModel,
                        ac_money_market, ac_money_market_percent, ac_money_market2, ac_money_market_percent2,
                        dCash, dCashPercent, dCash2, dCashPercent2);
            } else {
//                do nothing
            }

//            int counter;
//            counter = 0;
//            for (DropboxModel jenisInvestFix : listJenisInvestFix) {
//                if (mstProposalProductUlinkModel.getLji_id().equals(jenisInvestFix.getIdString())) {
//                    foundSameLjiId = true;
//                    switch (counter) {
//                        case 0:
////                            dFix = jenisInvestFix;
////                            ac_fix.setText(dFix.getLabel());
////
////                            dFixPercent.setId(mstProposalProductUlinkModel.getMdu_persen());
////                            dFixPercent.setLabel(mstProposalProductUlinkModel.getMdu_persen() + "%");
////                            ac_fix_percent.setText(dFixPercent.getLabel());
//                            setValueIntoView(ac_fix, ac_fix_percent, jenisInvestFix, dFix, dFixPercent, mstProposalProductUlinkModel.getMdu_persen());
//
//                            break;
//                        case 1:
////                            dFix2 = jenisInvestFix;
////                            ac_fix2.setText(dFix2.getLabel());
////
////                            dFixPercent2.setId(mstProposalProductUlinkModel.getMdu_persen());
////                            dFixPercent2.setLabel(mstProposalProductUlinkModel.getMdu_persen() + "%");
////                            ac_fix_percent2.setText(dFixPercent2.getLabel());
//                            setValueIntoView(ac_fix2, ac_fix_percent2, jenisInvestFix, dFix2, dFixPercent2, mstProposalProductUlinkModel.getMdu_persen());
//                            break;
//                        default:
//                            throw new IllegalArgumentException("Wrong Counter");
//                    }
//                    break;
//                }
//                counter++;
//            }
//            if (foundSameLjiId) {
//                continue;
//            }
//            counter = 0;
//            for (DropboxModel jenisInvestDynamic : listJenisInvestDynamic) {
//                if (mstProposalProductUlinkModel.getLji_id().equals(jenisInvestDynamic.getIdString())) {
//                    foundSameLjiId = true;
//                    switch (counter) {
//                        case 0:
//                            dDynamic = jenisInvestDynamic;
//                            ac_dynamic.setText(dDynamic.getLabel());
//
//                            dDynamicPercent.setId(mstProposalProductUlinkModel.getMdu_persen());
//                            dDynamicPercent.setLabel(mstProposalProductUlinkModel.getMdu_persen() + "%");
//                            ac_dynamic_percent.setText(dDynamicPercent.getLabel());
//                            break;
//                        case 1:
//                            dDynamic2 = jenisInvestDynamic;
//                            ac_dynamic2.setText(dDynamic2.getLabel());
//
//                            dDynamicPercent2.setId(mstProposalProductUlinkModel.getMdu_persen());
//                            dDynamicPercent2.setLabel(mstProposalProductUlinkModel.getMdu_persen() + "%");
//                            ac_dynamic_percent2.setText(dDynamicPercent2.getLabel());
//                            break;
//                        default:
//                            throw new IllegalArgumentException("Wrong Counter");
//                    }
//                    break;
//                }
//                counter++;
//            }
//            if (foundSameLjiId) {
//                continue;
//            }
//            counter = 0;
//            for (DropboxModel jenisInvestAggressive : listJenisInvestAggressive) {
//                Log.d(TAG, "setFillDataBaris8: jiv lji id" + jenisInvestAggressive.getIdString());
//                Log.d(TAG, "setFillDataBaris8: mst lji id" + mstProposalProductUlinkModel.getLji_id());
//
//                if (mstProposalProductUlinkModel.getLji_id().equals(jenisInvestAggressive.getIdString())) {
//                    switch (counter) {
//                        case 0:
//                            dAggressive = jenisInvestAggressive;
//                            ac_aggresive.setText(dAggressive.getLabel());
//
//                            dAggressivePercent.setId(mstProposalProductUlinkModel.getMdu_persen());
//                            dAggressivePercent.setLabel(mstProposalProductUlinkModel.getMdu_persen() + "%");
//                            ac_aggresive_percent.setText(dAggressivePercent.getLabel());
//                            break;
//                        case 1:
//                            dAggressive2 = jenisInvestAggressive;
//                            ac_aggresive2.setText(dAggressive2.getLabel());
//
//                            dAggressivePercent2.setId(mstProposalProductUlinkModel.getMdu_persen());
//                            dAggressivePercent2.setLabel(mstProposalProductUlinkModel.getMdu_persen() + "%");
//                            ac_aggresive_percent2.setText(dAggressivePercent2.getLabel());
//                            break;
//                        default:
//                            throw new IllegalArgumentException("Wrong Counter");
//                    }
//                    break;
//                }
//                counter++;
//            }
        }
    }

    private void setView8Baris() {

    }

    private void setValueBaris9(ArrayList<DropboxModel> listJenisInvest,
                                P_MstProposalProductUlinkModel ulinkModel,
                                AutoCompleteTextView ac,
                                AutoCompleteTextView ac_percent,
                                AutoCompleteTextView ac2,
                                AutoCompleteTextView ac_percent2,
                                AutoCompleteTextView ac3,
                                AutoCompleteTextView ac_percent3,
                                DropboxModel dbox,
                                DropboxModel dboxPercent,
                                DropboxModel dbox2,
                                DropboxModel dboxPercent2,
                                DropboxModel dbox3,
                                DropboxModel dboxPercent3){
        int counter = 0;
        for (DropboxModel jenisInvest : listJenisInvest) {
            if (ulinkModel.getLji_id().equals(jenisInvest.getIdString())) {
                switch (counter) {
                    case 0:
//                            dFix = jenisInvestFix;
//                            ac_fix.setText(dFix.getLabel());
//
//                            dFixPercent.setId(mstProposalProductUlinkModel.getMdu_persen());
//                            dFixPercent.setLabel(mstProposalProductUlinkModel.getMdu_persen() + "%");
//                            ac_fix_percent.setText(dFixPercent.getLabel());
                        setValueIntoView(ac, ac_percent, jenisInvest, dbox, dboxPercent, ulinkModel.getMdu_persen());

                        break;
                    case 1:
//                            dFix2 = jenisInvestFix;
//                            ac_fix2.setText(dFix2.getLabel());
//
//                            dFixPercent2.setId(mstProposalProductUlinkModel.getMdu_persen());
//                            dFixPercent2.setLabel(mstProposalProductUlinkModel.getMdu_persen() + "%");
//                            ac_fix_percent2.setText(dFixPercent2.getLabel());
                        setValueIntoView(ac2, ac_percent2, jenisInvest, dbox2, dboxPercent2, ulinkModel.getMdu_persen());
                        break;
                    case 2:
                        setValueIntoView(ac3, ac_percent3, jenisInvest, dbox3, dboxPercent3, ulinkModel.getMdu_persen());
                        break;
                    default:
                        throw new IllegalArgumentException("Wrong Counter");
                }
                break;
            }
            counter++;
        }

    }

    private void setValueBaris8(ArrayList<DropboxModel> listJenisInvest,
                                P_MstProposalProductUlinkModel ulinkModel,
                                AutoCompleteTextView ac,
                                AutoCompleteTextView ac_percent,
                                AutoCompleteTextView ac2,
                                AutoCompleteTextView ac_percent2,
                                DropboxModel dbox,
                                DropboxModel dboxPercent,
                                DropboxModel dbox2,
                                DropboxModel dboxPercent2) {
        int counter = 0;
        for (DropboxModel jenisInvest : listJenisInvest) {
            if (ulinkModel.getLji_id().equals(jenisInvest.getIdString())) {
                switch (counter) {
                    case 0:
//                            dFix = jenisInvestFix;
//                            ac_fix.setText(dFix.getLabel());
//
//                            dFixPercent.setId(mstProposalProductUlinkModel.getMdu_persen());
//                            dFixPercent.setLabel(mstProposalProductUlinkModel.getMdu_persen() + "%");
//                            ac_fix_percent.setText(dFixPercent.getLabel());
                        setValueIntoView(ac, ac_percent, jenisInvest, dbox, dboxPercent, ulinkModel.getMdu_persen());

                        break;
                    case 1:
//                            dFix2 = jenisInvestFix;
//                            ac_fix2.setText(dFix2.getLabel());
//
//                            dFixPercent2.setId(mstProposalProductUlinkModel.getMdu_persen());
//                            dFixPercent2.setLabel(mstProposalProductUlinkModel.getMdu_persen() + "%");
//                            ac_fix_percent2.setText(dFixPercent2.getLabel());
                        setValueIntoView(ac2, ac_percent2, jenisInvest, dbox2, dboxPercent2, ulinkModel.getMdu_persen());
                        break;
                    default:
                        throw new IllegalArgumentException("Wrong Counter");
                }
                break;
            }
            counter++;
        }
    }

    private void setDefaultBaris9(ArrayList<DropboxModel> listJenisInvest,
                                  ConstraintLayout cl2,
                                  ConstraintLayout cl3,
                                  AutoCompleteTextView ac,
                                  AutoCompleteTextView ac_percent,
                                  AutoCompleteTextView ac2,
                                  AutoCompleteTextView ac_percent2,
                                  AutoCompleteTextView ac3,
                                  AutoCompleteTextView ac_percent3,
                                  DropboxModel dbox,
                                  DropboxModel dboxPercent,
                                  DropboxModel dbox2,
                                  DropboxModel dboxPercent2,
                                  DropboxModel dbox3,
                                  DropboxModel dboxPercent3){
        int counter = 0;
        for (DropboxModel jenisInvest : listJenisInvest) {
            ArrayList<DropboxModel> listInvest = new ArrayList<>();
            Log.d(TAG, "setDefaultDataBaris8: ljiId = " + jenisInvest.getIdString() + " label = " + jenisInvest.getLabel());
            listInvest.add(jenisInvest);
            switch (counter) {
                case 0:
                    setDefaulPerBaris(listInvest, 0, ac, ac_percent, dbox, dboxPercent);
                    break;
                case 1:
                    cl2.setVisibility(View.VISIBLE);
                    setDefaulPerBaris(listInvest, 0, ac2, ac_percent2, dbox2, dboxPercent2);
                    break;
                case 2:
                    cl3.setVisibility(View.VISIBLE);
                    setDefaulPerBaris(listInvest, 0, ac3, ac_percent3, dbox3, dboxPercent3);
                    break;
                default:
                    throw new IllegalArgumentException("Wrong Counter");
            }
            counter++;
        }
    }

    private void setDefaultBaris8(ArrayList<DropboxModel> listJenisInvest,
                                  ConstraintLayout cl2,
                                  AutoCompleteTextView ac,
                                  AutoCompleteTextView ac_percent,
                                  AutoCompleteTextView ac2,
                                  AutoCompleteTextView ac_percent2,
                                  DropboxModel dbox,
                                  DropboxModel dboxPercent,
                                  DropboxModel dbox2,
                                  DropboxModel dboxPercent2) {
        int counter = 0;
        for (DropboxModel jenisInvest : listJenisInvest) {
            ArrayList<DropboxModel> listInvest = new ArrayList<>();
            Log.d(TAG, "setDefaultDataBaris8: ljiId = " + jenisInvest.getIdString() + " label = " + jenisInvest.getLabel());
            listInvest.add(jenisInvest);
            switch (counter) {
                case 0:
                    setDefaulPerBaris(listInvest, 0, ac, ac_percent, dbox, dboxPercent);
                    break;
                case 1:
                    cl2.setVisibility(View.VISIBLE);
                    setDefaulPerBaris(listInvest, 0, ac2, ac_percent2, dbox2, dboxPercent2);
                    break;
                default:
                    throw new IllegalArgumentException("Wrong Counter");
            }
            counter++;
        }
    }
    private void setValueIntoView(AutoCompleteTextView ac,
                                  AutoCompleteTextView ac_percent,
                                  DropboxModel infoInvest,
                                  DropboxModel dbox,
                                  DropboxModel dboxPercent,
                                  int percentRound) {
        dbox.setIdString(infoInvest.getIdString());
        dbox.setLabel(infoInvest.getLabel());
        dbox.setLabelSecond(infoInvest.getLabelSecond());

        ac.setText(dbox.getLabel());

        dboxPercent.setId(percentRound);
        String percentValue = percentRound + "%";
        ac_percent.setText(percentValue);
    }


    private void setFillDataBaris4() {
//        String percentValue;
        for (P_MstProposalProductUlinkModel mstProposalProductUlinkModel : mstProposalProductUlinkModels) {
            DropboxModel infoInvest = select.selectInfoInvest(mstProposalProductUlinkModel.getLji_id());
            if (infoInvest.getLabelSecond().equals(getString(R.string.fix))) {
//                    dFix = infoInvest;
//                    ac_fix.setText(dFix.getLabel());
//                    dFixPercent.setId(mstProposalProductUlinkModel.getMdu_persen());
//                    percentValue = mstProposalProductUlinkModel.getMdu_persen() + "%";
//                    ac_fix_percent.setText(percentValue);

                setValueIntoView(ac_fix,
                        ac_fix_percent,
                        infoInvest,
                        dFix,
                        dFixPercent,
                        mstProposalProductUlinkModel.getMdu_persen());

            } else if (infoInvest.getLabelSecond().equals(getString(R.string.dynamic))) {//                    dDynamic = infoInvest;
//                    ac_dynamic.setText(dDynamic.getLabel());
//                    dDynamicPercent.setId(mstProposalProductUlinkModel.getMdu_persen());
//                    percentValue = mstProposalProductUlinkModel.getMdu_persen() + "%";
//                    ac_dynamic_percent.setText(percentValue);

                setValueIntoView(ac_dynamic, ac_dynamic_percent, infoInvest, dDynamic, dDynamicPercent, mstProposalProductUlinkModel.getMdu_persen());

            } else if (infoInvest.getLabelSecond().equals(getString(R.string.aggressive))) {//                    dAggressive = infoInvest;
//                    ac_aggresive.setText(dAggressive.getLabel());
//                    dAggressivePercent.setId(mstProposalProductUlinkModel.getMdu_persen());
//                    percentValue = mstProposalProductUlinkModel.getMdu_persen() + "%";
//                    ac_aggresive_percent.setText(percentValue);

                setValueIntoView(ac_aggresive, ac_aggresive_percent, infoInvest, dAggressive, dAggressivePercent, mstProposalProductUlinkModel.getMdu_persen());

            } else if (infoInvest.getLabelSecond().equals(getString(R.string.money_market))) {

                setValueIntoView(ac_money_market, ac_money_market_percent, infoInvest, dCash, dCashPercent, mstProposalProductUlinkModel.getMdu_persen());

            } else {
//                do nothing
            }
        }
    }

    private void setFillDataSIAP2U() {
        for (P_MstProposalProductUlinkModel mstProposalProductUlinkModel : mstProposalProductUlinkModels) {
            DropboxModel infoInvest = select.selectInfoInvest(mstProposalProductUlinkModel.getLji_id());

            if (infoInvest.getLabelSecond().equals(getString(R.string.aggressive))) {//                    dAggressive = infoInvest;
//                    ac_aggresive.setText(dAggressive.getLabel());
//                    dAggressivePercent.setId(mstProposalProductUlinkModel.getMdu_persen());
//                    percentValue = mstProposalProductUlinkModel.getMdu_persen() + "%";
//                    ac_aggresive_percent.setText(percentValue);

                setValueIntoView(ac_aggresive, ac_aggresive_percent, infoInvest, dAggressive, dAggressivePercent, mstProposalProductUlinkModel.getMdu_persen());

            }

        }
    }

    private void setDefaulPerBaris(ArrayList<DropboxModel> listJenisInvest, int position,
                                   AutoCompleteTextView ac, AutoCompleteTextView ac_percent,
                                   DropboxModel dbox, DropboxModel dboxPercent) {
        if (dbox.getIdString() == null) {
            DropboxModel jenisInvest = listJenisInvest.get(position);
            dbox.setIdString(jenisInvest.getIdString());
            dbox.setLabel(jenisInvest.getLabel());
            dbox.setLabelSecond(jenisInvest.getLabelSecond());

            ac.setText(dbox.getLabel());

            if (listJenisInvest.size() == 1) {
                if(mstDataProposalModel.getFlag_packet() != null){
                    dboxPercent.setId(100);
                    ac_percent.setText("100%");
                }else if (psetId == 1297){
                    dboxPercent.setId(100);
                    ac_percent.setText("100%");
                }else{
                    dboxPercent.setId(0);
                    ac_percent.setText("0%");
                }

            } else {
                dboxPercent.setId(0);
                String percentValue = 0 + "%";
                ac_percent.setText(percentValue);
            }
        }
        ArrayAdapter<DropboxModel> aaInvest;
        aaInvest = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, listJenisInvest);
        ac.setAdapter(aaInvest);
        ArrayList<DropboxModel> listPercent = getDropboxNilaiInvestasi();
        aaInvest = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, listPercent);
        ac_percent.setAdapter(aaInvest);
    }

    private void setDefaultSIAP2U() {
        tv_fix.setVisibility(View.GONE);
        cl_fix.setVisibility(View.GONE);

        tv_dynamic.setVisibility(View.GONE);
        cl_dynamic.setVisibility(View.GONE);

        tv_money_market.setVisibility(View.GONE);
        cl_money_market.setVisibility(View.GONE);

        ac_aggresive.setEnabled(false);
        ac_aggresive_percent.setEnabled(false);

        setDefaulPerBaris(listJenisInvestAggressive, 0, ac_aggresive, ac_aggresive_percent, dAggressive, dAggressivePercent);
    }

    public void setListener() {
//        btn_lanjut.setOnClickListener(this);
//        btn_kembali.setOnClickListener(this);
        getActivity().findViewById(R.id.btn_lanjut).setOnClickListener(this);
        getActivity().findViewById(R.id.btn_kembali).setOnClickListener(this);

        ac_fix.setOnClickListener(this);
        ac_fix.setOnFocusChangeListener(this);
        ac_fix.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_fix, this));
        ac_fix_percent.setOnClickListener(this);
        ac_fix_percent.setOnFocusChangeListener(this);
        ac_fix_percent.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_fix_percent, this));
        ac_fix2.setOnClickListener(this);
        ac_fix2.setOnFocusChangeListener(this);
        ac_fix2.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_fix2, this));
        ac_fix_percent2.setOnClickListener(this);
        ac_fix_percent2.setOnFocusChangeListener(this);
        ac_fix_percent2.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_fix_percent2, this));

        ac_dynamic.setOnClickListener(this);
        ac_dynamic.setOnFocusChangeListener(this);
        ac_dynamic.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_dynamic, this));
        ac_dynamic_percent.setOnClickListener(this);
        ac_dynamic_percent.setOnFocusChangeListener(this);
        ac_dynamic_percent.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_dynamic_percent, this));
        ac_dynamic2.setOnClickListener(this);
        ac_dynamic2.setOnFocusChangeListener(this);
        ac_dynamic2.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_dynamic2, this));
        ac_dynamic_percent2.setOnClickListener(this);
        ac_dynamic_percent2.setOnFocusChangeListener(this);
        ac_dynamic_percent2.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_dynamic_percent2, this));

        ac_aggresive.setOnClickListener(this);
        ac_aggresive.setOnFocusChangeListener(this);
        ac_aggresive.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_aggresive, this));
        ac_aggresive_percent.setOnClickListener(this);
        ac_aggresive_percent.setOnFocusChangeListener(this);
        ac_aggresive_percent.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_aggresive_percent, this));
        ac_aggresive2.setOnClickListener(this);
        ac_aggresive2.setOnFocusChangeListener(this);
        ac_aggresive2.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_aggresive2, this));
        ac_aggresive3.setOnClickListener(this);
        ac_aggresive3.setOnFocusChangeListener(this);
        ac_aggresive3.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_aggresive3, this));
        ac_aggresive_percent2.setOnClickListener(this);
        ac_aggresive_percent2.setOnFocusChangeListener(this);
        ac_aggresive_percent2.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_aggresive_percent2, this));
        ac_aggresive_percent3.setOnClickListener(this);
        ac_aggresive_percent3.setOnFocusChangeListener(this);
        ac_aggresive_percent3.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_aggresive_percent3, this));


        ac_money_market.setOnClickListener(this);
        ac_money_market.setOnFocusChangeListener(this);
        ac_money_market.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_money_market, this));
        ac_money_market_percent.setOnClickListener(this);
        ac_money_market_percent.setOnFocusChangeListener(this);
        ac_money_market_percent.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_money_market_percent, this));
        ac_money_market2.setOnClickListener(this);
        ac_money_market2.setOnFocusChangeListener(this);
        ac_money_market2.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_money_market2, this));
        ac_money_market_percent2.setOnClickListener(this);
        ac_money_market_percent2.setOnFocusChangeListener(this);
        ac_money_market_percent2.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_money_market_percent2, this));
    }

    private void attemptContinue() {
        mstProposalProductUlinkModels = new ArrayList<>();
        int total = 0;
        switch (flagInvestFund) {
            case 0:
                switch (mstProposalProductModel.getLku_id()) {
                    case "01":
                        /*untuk fund equity bakti peduli*/
                        if (psetId == 27 || psetId == 1430 || psetId == 1431 || psetId == 987 || psetId == 989 || psetId == 990 || psetId == 986 ||
                            psetId == 988 || psetId == 992 || psetId == 967){
                            total = dFixPercent.getId() + dDynamicPercent.getId() + dAggressivePercent.getId() + dFixPercent2.getId() + dDynamicPercent2.getId() + dAggressivePercent2.getId() + dAggressivePercent3.getId();
                            if (listJenisInvestCash.size() > 0) {
                                total += dCashPercent.getId();
                            }
                            if (listJenisInvestCash.size() > 1) {
                                total += dCashPercent2.getId();
                            }
                        } else if (psetId == 1297) {

                            total = dAggressivePercent.getId();

                        } else if (psetId == 1504 || psetId == 1505) { // bukopin syariah
                            total = dFixPercent.getId() + dDynamicPercent.getId() + dAggressivePercent.getId();
                            if (listJenisInvestCash.size() > 0) {
                                total += dCashPercent.getId();
                            }
                            if (listJenisInvestCash.size() > 1) {
                                total += dCashPercent2.getId();
                            }
                        } else {
                            total = dFixPercent.getId() + dDynamicPercent.getId() + dAggressivePercent.getId() + dFixPercent2.getId() + dDynamicPercent2.getId() + dAggressivePercent2.getId();
                            if (listJenisInvestCash.size() > 0) {
                                total += dCashPercent.getId();
                            }
                            if (listJenisInvestCash.size() > 1) {
                                total += dCashPercent2.getId();
                            }
                        }

                        break;
                    case "02":
//                        total = dFixPercent.getId() + dDynamicPercent.getId() + dFixPercent2.getId() + dDynamicPercent2.getId();
                        total = dFixPercent.getId() + dDynamicPercent.getId() ;
                        if (listJenisInvestFix.size() > 1){
                            total += dFixPercent2.getId();
                        }
                        if (listJenisInvestDynamic.size() > 1){
                            total += dDynamicPercent2.getId();
                        }
                        break;
                }
                break;
            case 1:
                switch (mstProposalProductModel.getLku_id()) {

                    case "01":
                        /*untuk fund equity bakti peduli*/
                        if (psetId == 1065 ){
                            total = dFixPercent.getId() + dDynamicPercent.getId() + dAggressivePercent.getId();
                            if (listJenisInvestCash.size() > 0) {
                                total += dCashPercent.getId();
                            }
                            if (listJenisInvestCash.size() > 1) {
                                total += dCashPercent2.getId();
                            }
                        } else {
                            if (mstDataProposalModel.getFlag_packet() != null) {
                                total = dAggressivePercent.getId();
                            } else {
                                total = dFixPercent.getId() + dDynamicPercent.getId() + dAggressivePercent.getId();
                            }

                            if (listJenisInvestCash.size() > 0) {
                                total += dCashPercent.getId();
                            }
                        }
                        break;
                    case "02":
                        total = dFixPercent.getId() + dDynamicPercent.getId();
                        break;
                }
                break;
            case 2:
                switch (mstProposalProductModel.getLku_id()) {
                    case "01":
                        if (mstDataProposalModel.getFlag_packet() != null) {
                            total = dAggressivePercent.getId();
                        }

                        if (listJenisInvestCash.size() > 0) {
                            total += dCashPercent.getId();
                        }
                        break;
                }
                break;
            case 3:
                switch (mstProposalProductModel.getLku_id()) {
                    case "01":
                        total = dFixPercent.getId() + dDynamicPercent.getId() + dAggressivePercent.getId() + dFixPercent2.getId() + dDynamicPercent2.getId() + dAggressivePercent2.getId() + dAggressivePercent3.getId();
                        if (listJenisInvestCash.size() > 0) {
                            total += dCashPercent.getId();
                        }
                        if (listJenisInvestCash.size() > 1) {
                            total += dCashPercent2.getId();
                        }
                        break;
                }
                break;
            default:

                break;
        }


        if (total == 100) {
            goToNextPage();
//            Toast.makeText(getContext(), "Sukses", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "Total Investasi Harus 100%", Toast.LENGTH_SHORT).show();
            tv_note.getParent().requestChildFocus(tv_note, tv_note);
//            tv_note.setFocusable(true);
//            tv_note.requestFocus();
            tv_note.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
            tv_note.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));
        }
    }

    private void goToNextPage() {
        /*first check if there are any possible fund*/
        QueryUtil queryUtil = new QueryUtil(getContext());

        Fragment fragment;
        ArrayList<SparseIntArray> widgetView = select.selectLstProposalField(psetId);
        SparseIntArray display = widgetView.get(0);
        if (display.get(TOMBOL_TOPUP_PENARIKAN) == 1) {
            fragment = new P_TopUpFragment();
        } else {
            if (mstDataProposalModel.getFlag_packet() != null) { //SIAP2U
                Cursor riderCursor = queryUtil.query(
                        getString(R.string.e_lst_packet_det_left_join_e_lst_packet_rider),
                        new String[]{"RIDER_ID"},
                        getString(R.string.LSBS_ID) + " = ? AND " + getString(R.string.LSDBS_NUMBER) + " = ?",
                        new String[]{String.valueOf(mstProposalProductModel.getLsbs_id()), String.valueOf(mstProposalProductModel.getLsdbs_number())},
                        null
                );
                int riderCount = riderCursor.getCount();
                if (riderCount > 0) {
                    fragment = new P_RiderFragment();
                } else {
                    saveToMyBundle();
                    P_MainActivity.saveState(getContext(), true);
                    fragment = new P_ResultFragment();
                }
            } else {
                Cursor riderCursor = queryUtil.query(
                        getString(R.string.TABLE_LST_BISNIS_RIDER),
                        new String[]{"RIDER_ID"},
                        getString(R.string.LSBS_ID) + " = ? AND " + getString(R.string.LSDBS_NUMBER) + " = ?",
                        new String[]{String.valueOf(mstProposalProductModel.getLsbs_id()), String.valueOf(mstProposalProductModel.getLsdbs_number())},
                        null
                );
                int riderCount = riderCursor.getCount();
                if (riderCount > 0) {
                    fragment = new P_RiderFragment();
                } else {
                    saveToMyBundle();
                    P_MainActivity.saveState(getContext(), true);
                    fragment = new P_ResultFragment();
                }
            }

        }

        ((P_MainActivity) getActivity()).setFragmentBackStack(fragment, "fragment");
    }

    @Override
    public void onPause() {
        super.onPause();
        saveToMyBundle();
    }

    public void saveToMyBundle() {
        mstDataProposalModel.setLast_page_position(DATA_PROPOSAL_PAGE);//semua kembali keawal

        ArrayList<P_MstProposalProductUlinkModel> mstProposalProductUlinkModels = new ArrayList<>();
        P_MstProposalProductUlinkModel mstProposalProductUlinkModel;
        switch (flagInvestFund) {
            case 0:
                switch (mstProposalProductModel.getLku_id()) {
                    case "01":
                        if (dFixPercent.getId() != null && dFixPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dFix.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dFixPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dDynamicPercent.getId() != null && dDynamicPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dDynamic.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dDynamicPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dAggressivePercent.getId() != null && dAggressivePercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dAggressive.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dAggressivePercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dCashPercent.getId() != null && dCashPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dCash.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dCashPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dFixPercent2.getId() != null && dFixPercent2.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dFix2.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dFixPercent2.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dDynamicPercent2.getId() != null && dDynamicPercent2.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dDynamic2.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dDynamicPercent2.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dAggressivePercent2.getId() != null && dAggressivePercent2.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dAggressive2.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dAggressivePercent2.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dAggressivePercent3.getId() != null && dAggressivePercent3.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dAggressive3.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dAggressivePercent3.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dCashPercent2.getId() != null && dCashPercent2.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dCash2.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dCashPercent2.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        break;
                    case "02":
                        if (dFixPercent.getId() != null && dFixPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dFix.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dFixPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dDynamicPercent.getId() != null && dDynamicPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dDynamic.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dDynamicPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }

                        break;
                }
                break;
            case 1:
                switch (mstProposalProductModel.getLku_id()) {
                    case "01":
                        if (dFixPercent.getId() != null && dFixPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dFix.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dFixPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dDynamicPercent.getId() != null && dDynamicPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dDynamic.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dDynamicPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dAggressivePercent.getId() != null && dAggressivePercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dAggressive.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dAggressivePercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dCashPercent.getId() != null && dCashPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dCash.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dCashPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }

                        break;
                    case "02":
                        if (dFixPercent.getId() != null && dFixPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dFix.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dFixPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dDynamicPercent.getId() != null && dDynamicPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dDynamic.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dDynamicPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        break;
                }
                break;
            case 2:
                switch (mstProposalProductModel.getLku_id()) {
                    case "01":
                        if (dFixPercent.getId() != null && dFixPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dFix.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dFixPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dDynamicPercent.getId() != null && dDynamicPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dDynamic.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dDynamicPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dAggressivePercent.getId() != null && dAggressivePercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dAggressive.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dAggressivePercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dCashPercent.getId() != null && dCashPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dCash.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dCashPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        break;
                }
                break;
            case 3:
                switch (mstProposalProductModel.getLku_id()) {
                    case "01":
                        if (dFixPercent.getId() != null && dFixPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dFix.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dFixPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dDynamicPercent.getId() != null && dDynamicPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dDynamic.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dDynamicPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dAggressivePercent.getId() != null && dAggressivePercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dAggressive.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dAggressivePercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dCashPercent.getId() != null && dCashPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dCash.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dCashPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dFixPercent2.getId() != null && dFixPercent2.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dFix2.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dFixPercent2.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dDynamicPercent2.getId() != null && dDynamicPercent2.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dDynamic2.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dDynamicPercent2.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dAggressivePercent2.getId() != null && dAggressivePercent2.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dAggressive2.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dAggressivePercent2.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dAggressivePercent3.getId() != null && dAggressivePercent3.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dAggressive3.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dAggressivePercent3.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dCashPercent2.getId() != null && dCashPercent2.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dCash2.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dCashPercent2.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        break;
                    case "02":
                        if (dFixPercent.getId() != null && dFixPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dFix.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dFixPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }
                        if (dDynamicPercent.getId() != null && dDynamicPercent.getId() != 0) {
                            mstProposalProductUlinkModel = new P_MstProposalProductUlinkModel();
                            mstProposalProductUlinkModel.setLji_id(dDynamic.getIdString());
                            mstProposalProductUlinkModel.setMdu_persen(dDynamicPercent.getId());
                            mstProposalProductUlinkModels.add(mstProposalProductUlinkModel);
                        }

                        break;
                }
                break;
            default:

                break;
        }

        proposalModel.setMst_data_proposal(mstDataProposalModel);
        proposalModel.setMst_proposal_product_ulink(mstProposalProductUlinkModels);

        P_MainActivity.myBundle.putParcelable(getString(R.string.proposal_model), proposalModel);
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.menu_va_proposal, menu);
//    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.ac_fix:
                dFix = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_fix_percent:
                dFixPercent = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_fix2:
                dFix2 = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_fix_percent2:
                dFixPercent2 = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_dynamic:
                dDynamic = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_dynamic_percent:
                dDynamicPercent = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_dynamic2:
                dDynamic2 = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_dynamic_percent2:
                dDynamicPercent2 = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_aggresive:
                dAggressive = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_aggresive_percent:
                dAggressivePercent = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_aggresive2:
                dAggressive2 = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_aggresive_percent2:
                dAggressivePercent2 = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_aggresive3:
                dAggressive3 = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_aggresive_percent3:
                dAggressivePercent3 = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_money_market:
                dCash = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_money_market_percent:
                dCashPercent = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_money_market2:
                dCash2 = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_money_market_percent2:
                dCashPercent2 = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            default:
                throw new IllegalArgumentException("Salah id pada OnItemClick");
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.ac_fix:
                if (hasFocus)
                    ac_fix.showDropDown();
                break;
            case R.id.ac_fix_percent:
                if (hasFocus)
                    ac_fix_percent.showDropDown();
                break;
            case R.id.ac_fix2:
                if (hasFocus)
                    ac_fix2.showDropDown();
                break;
            case R.id.ac_fix_percent2:
                if (hasFocus)
                    ac_fix_percent2.showDropDown();
                break;
            case R.id.ac_dynamic:
                if (hasFocus)
                    ac_dynamic.showDropDown();
                break;
            case R.id.ac_dynamic_percent:
                if (hasFocus)
                    ac_dynamic_percent.showDropDown();
                break;
            case R.id.ac_dynamic2:
                if (hasFocus)
                    ac_dynamic2.showDropDown();
                break;
            case R.id.ac_dynamic_percent2:
                if (hasFocus)
                    ac_dynamic_percent2.showDropDown();
                break;
            case R.id.ac_aggresive:
                if (hasFocus)
                    ac_aggresive.showDropDown();
                break;
            case R.id.ac_aggresive_percent:
                if (hasFocus)
                    ac_aggresive_percent.showDropDown();
                break;
            case R.id.ac_aggresive2:
                if (hasFocus)
                    ac_aggresive2.showDropDown();
                break;
            case R.id.ac_aggresive_percent2:
                if (hasFocus)
                    ac_aggresive_percent2.showDropDown();
                break;
            case R.id.ac_aggresive3:
                if (hasFocus)
                    ac_aggresive3.showDropDown();
                break;
            case R.id.ac_aggresive_percent3:
                if (hasFocus)
                    ac_aggresive_percent3.showDropDown();
                break;
            case R.id.ac_money_market:
                if (hasFocus) ac_money_market.showDropDown();
                break;
            case R.id.ac_money_market_percent:
                if (hasFocus) ac_money_market_percent.showDropDown();
                break;
            case R.id.ac_money_market2:
                if (hasFocus) ac_money_market2.showDropDown();
                break;
            case R.id.ac_money_market_percent2:
                if (hasFocus) ac_money_market_percent2.showDropDown();
                break;
            default:
                throw new IllegalArgumentException("Wrong id Onclick");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_lanjut:
                attemptContinue();
                break;
            case R.id.btn_kembali:
                ((P_MainActivity) getActivity()).onKembaliPressed();
                break;
            case R.id.ac_fix:
                ac_fix.showDropDown();
                break;
            case R.id.ac_fix_percent:
                ac_fix_percent.showDropDown();
                break;
            case R.id.ac_fix2:
                ac_fix2.showDropDown();
                break;
            case R.id.ac_fix_percent2:
                ac_fix_percent2.showDropDown();
                break;
            case R.id.ac_dynamic:
                ac_dynamic.showDropDown();
                break;
            case R.id.ac_dynamic_percent:
                ac_dynamic_percent.showDropDown();
                break;
            case R.id.ac_dynamic2:
                ac_dynamic2.showDropDown();
                break;
            case R.id.ac_dynamic_percent2:
                ac_dynamic_percent2.showDropDown();
                break;
            case R.id.ac_aggresive:
                ac_aggresive.showDropDown();
                break;
            case R.id.ac_aggresive_percent:
                ac_aggresive_percent.showDropDown();
                break;
            case R.id.ac_aggresive2:
                ac_aggresive2.showDropDown();
                break;
            case R.id.ac_aggresive_percent2:
                ac_aggresive_percent2.showDropDown();
                break;
            case R.id.ac_aggresive3:
                ac_aggresive3.showDropDown();
                break;
            case R.id.ac_aggresive_percent3:
                ac_aggresive_percent3.showDropDown();
                break;
            case R.id.ac_money_market:
                ac_money_market.showDropDown();
                break;
            case R.id.ac_money_market_percent:
                ac_money_market_percent.showDropDown();
                break;
            case R.id.ac_money_market2:
                ac_money_market2.showDropDown();
                break;
            case R.id.ac_money_market_percent2:
                ac_money_market_percent2.showDropDown();
                break;
            default:
                throw new IllegalArgumentException("Wrong id Onclick");
        }
    }


    @BindView(R.id.ll_form)
    LinearLayout ll_form_investasi;

    @BindView(R.id.ac_fix)
    AutoCompleteTextView ac_fix;
    @BindView(R.id.ac_fix_percent)
    AutoCompleteTextView ac_fix_percent;
    @BindView(R.id.ac_fix2)
    AutoCompleteTextView ac_fix2;
    @BindView(R.id.ac_fix_percent2)
    AutoCompleteTextView ac_fix_percent2;
    @BindView(R.id.ac_dynamic)
    AutoCompleteTextView ac_dynamic;
    @BindView(R.id.ac_dynamic_percent)
    AutoCompleteTextView ac_dynamic_percent;
    @BindView(R.id.ac_dynamic2)
    AutoCompleteTextView ac_dynamic2;
    @BindView(R.id.ac_dynamic_percent2)
    AutoCompleteTextView ac_dynamic_percent2;
    @BindView(R.id.ac_aggresive)
    AutoCompleteTextView ac_aggresive;
    @BindView(R.id.ac_aggresive_percent)
    AutoCompleteTextView ac_aggresive_percent;
    @BindView(R.id.ac_aggresive2)
    AutoCompleteTextView ac_aggresive2;
    @BindView(R.id.ac_aggresive_percent2)
    AutoCompleteTextView ac_aggresive_percent2;
    @BindView(R.id.ac_aggresive3)
    AutoCompleteTextView ac_aggresive3;
    @BindView(R.id.ac_aggresive_percent3)
    AutoCompleteTextView ac_aggresive_percent3;
    @BindView(R.id.ac_money_market)
    AutoCompleteTextView ac_money_market;
    @BindView(R.id.ac_money_market_percent)
    AutoCompleteTextView ac_money_market_percent;
    @BindView(R.id.ac_money_market2)
    AutoCompleteTextView ac_money_market2;
    @BindView(R.id.ac_money_market_percent2)
    AutoCompleteTextView ac_money_market_percent2;


    @BindView(R.id.cl_fix)
    ConstraintLayout cl_fix;
    @BindView(R.id.cl_fix2)
    ConstraintLayout cl_fix2;
    @BindView(R.id.cl_dynamic)
    ConstraintLayout cl_dynamic;
    @BindView(R.id.cl_dynamic2)
    ConstraintLayout cl_dynamic2;
    @BindView(R.id.cl_aggresive)
    ConstraintLayout cl_aggresive;
    @BindView(R.id.cl_aggresive2)
    ConstraintLayout cl_aggresive2;
    @BindView(R.id.cl_aggresive3)
    ConstraintLayout cl_aggressive3;
    @BindView(R.id.cl_money_market)
    ConstraintLayout cl_money_market;
    @BindView(R.id.cl_money_market2)
    ConstraintLayout cl_money_market2;

    @BindView(R.id.tv_fix)
    TextView tv_fix;
    @BindView(R.id.tv_dynamic)
    TextView tv_dynamic;
    @BindView(R.id.tv_note)
    TextView tv_note;
    @BindView(R.id.tv_aggresive)
    TextView tv_aggresive;
    @BindView(R.id.tv_money_market)
    TextView tv_money_market;
    @BindView(R.id.btn_lanjut)
    Button btn_lanjut;
    @BindView(R.id.btn_kembali)
    Button btn_kembali;


}
