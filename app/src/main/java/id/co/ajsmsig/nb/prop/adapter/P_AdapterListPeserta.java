package id.co.ajsmsig.nb.prop.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.method.P_StaticMethods;
import id.co.ajsmsig.nb.prop.method.ValueView;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductPesertaModel;


public class P_AdapterListPeserta extends ArrayAdapter<P_MstProposalProductPesertaModel> {
    private ArrayList<P_MstProposalProductPesertaModel> mstProposalProductPesertaModels;
    private int resourceId;
    private boolean isRiderLadies;

    public ArrayList<P_MstProposalProductPesertaModel> getMstProposalProductPesertaModel() {
        return mstProposalProductPesertaModels;
    }


    public P_AdapterListPeserta(Context context, int resourceId,
                                ArrayList<P_MstProposalProductPesertaModel> mstProposalProductPesertaModels,
                                boolean isRiderLadies) {
        super(context, resourceId, mstProposalProductPesertaModels);
        this.mstProposalProductPesertaModels = new ArrayList<>();
        this.mstProposalProductPesertaModels.addAll(mstProposalProductPesertaModels);
        this.resourceId = resourceId;
        this.isRiderLadies = isRiderLadies;

    }

    public void updateListTP(ArrayList<P_MstProposalProductPesertaModel> newList) {
        mstProposalProductPesertaModels.clear();
        mstProposalProductPesertaModels.addAll(newList);

        this.notifyDataSetChanged();
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.p_fragment_list_peserta_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        P_MstProposalProductPesertaModel pesertaModel = mstProposalProductPesertaModels.get(position);
        String header = P_StaticMethods.getHeaderPeserta(getContext(), isRiderLadies, position);
        holder.tv_header.setText(header);

        DropboxModel dHubungan = new P_Select(getContext()).selectHubunganTtTamb(pesertaModel.getLsre_id());
        String[][] values = new String[][]{
                new String[]{getContext().getString(R.string.nama), pesertaModel.getNama_peserta()},
                new String[]{getContext().getString(R.string.tanggal_lahir), pesertaModel.getTgl_lahir()},
                new String[]{getContext().getString(R.string.umur), String.valueOf(pesertaModel.getUsia())},
                new String[]{getContext().getString(R.string.hubungan), dHubungan.getLabel()}
        };
        new ValueView(getContext()).addToLinearLayout(holder.ll_tertanggung, values);

        return convertView;

    }

    public static class ViewHolder {
        @BindView(R.id.tv_header)
        TextView tv_header;
        @BindView(R.id.ll_tertanggung)
        LinearLayout ll_tertanggung;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
