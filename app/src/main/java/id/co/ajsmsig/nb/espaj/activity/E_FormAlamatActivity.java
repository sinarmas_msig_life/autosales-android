package id.co.ajsmsig.nb.espaj.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.model.EspajModel;

/**
 * Created by Bernard on 12/07/2017.
 */

public class E_FormAlamatActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = E_FormAlamatActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.e_activity_form_alamat);
        ActionBar toolbar = getSupportActionBar();
        toolbar.setDisplayHomeAsUpEnabled(true);
        toolbar.setDisplayShowHomeEnabled(true);
        ButterKnife.bind(this);

        intent = getIntent();
        flag_input = intent.getIntExtra(getString(R.string.flag), 0);
        me = intent.getParcelableExtra(getString(R.string.modelespaj));
        if (flag_input == 7 || flag_input == 8){
            toolbar.setTitle("Form isi Hp");
            cl_form_info_hp.setVisibility(View.VISIBLE);
            cl_form_info_umum.setVisibility(View.GONE);
        }else {
            toolbar.setTitle("Form Alamat");
            cl_form_info_hp.setVisibility(View.GONE);
            cl_form_info_umum.setVisibility(View.VISIBLE);
        }

        et_alamat.addTextChangedListener(new MTextWatcher(et_alamat));
        et_kota.addTextChangedListener(new MTextWatcher(et_kota));
        et_hp1.addTextChangedListener(new MTextWatcher(et_hp1));
        et_email.addTextChangedListener(new MTextWatcher(et_email));

        btn_lanjut.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);


        restore();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_lanjut:
                load();
                if (Validation()) {
                    intent = new Intent();
                    intent.putExtra(getString(R.string.modelespaj), me);
                    intent.putExtra(getString(R.string.flag), flag_input);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
            case R.id.btn_cancel:
                et_alamat.setText("");
                et_kota.setText("");
                et_kode_pos.setText("");
                et_kdtelp1.setText("");
                et_telp1.setText("");
                et_kdtelp2.setText("");
                et_telp2.setText("");
                et_kdfax.setText("");
                et_fax.setText("");
                load();
                intent = new Intent();
                intent.putExtra(getString(R.string.modelespaj), me);
                intent.putExtra(getString(R.string.flag), flag_input);
                setResult(RESULT_OK, intent);
                finish();
                break;

        }
    }

    private void load() {
        switch (flag_input) {
            //PP
            case 1: //rumah
                me.getPemegangPolisModel().setAlamat_pp(et_alamat.getText().toString());
                me.getPemegangPolisModel().setKota_pp(et_kota.getText().toString());
                me.getPemegangPolisModel().setKdpos_pp(et_kode_pos.getText().toString());
                me.getPemegangPolisModel().setKdtelp1_pp(et_kdtelp1.getText().toString());
                me.getPemegangPolisModel().setTelp1_pp(et_telp1.getText().toString());
                me.getPemegangPolisModel().setKdtelp2_pp(et_kdtelp2.getText().toString());
                me.getPemegangPolisModel().setTelp2_pp(et_telp2.getText().toString());
                break;
            case 2: //kantor
                me.getPemegangPolisModel().setAlamat_kantor_pp(et_alamat.getText().toString());
                me.getPemegangPolisModel().setKota_kantor_pp(et_kota.getText().toString());
                me.getPemegangPolisModel().setKdpos_kantor_pp(et_kode_pos.getText().toString());
                me.getPemegangPolisModel().setKdtelp1_kantor_pp(et_kdtelp1.getText().toString());
                me.getPemegangPolisModel().setTelp1_kantor_pp(et_telp1.getText().toString());
                me.getPemegangPolisModel().setKdtelp2_kantor_pp(et_kdtelp2.getText().toString());
                me.getPemegangPolisModel().setTelp2_kantor_pp(et_telp2.getText().toString());
                me.getPemegangPolisModel().setKdfax_kantor_pp(et_kdfax.getText().toString());
                me.getPemegangPolisModel().setFax_kantor_pp(et_fax.getText().toString());
                break;
            case 3: //tempat tinggal
                me.getPemegangPolisModel().setAlamat_tinggal_pp(et_alamat.getText().toString());
                me.getPemegangPolisModel().setKota_tinggal_pp(et_kota.getText().toString());
                me.getPemegangPolisModel().setKdpos_tinggal_pp(et_kode_pos.getText().toString());
                me.getPemegangPolisModel().setKdtelp1_tinggal_pp(et_kdtelp1.getText().toString());
                me.getPemegangPolisModel().setTelp1_tinggal_pp(et_telp1.getText().toString());
                me.getPemegangPolisModel().setKdtelp2_tinggal_pp(et_kdtelp2.getText().toString());
                me.getPemegangPolisModel().setTelp2_tinggal_pp(et_telp2.getText().toString());
                me.getPemegangPolisModel().setKdfax_tinggal_pp(et_kdfax.getText().toString());
                me.getPemegangPolisModel().setFax_tinggal_pp(et_fax.getText().toString());
                break;
            //TT
            case 4: //rumah
                me.getTertanggungModel().setAlamat_tt(et_alamat.getText().toString());
                me.getTertanggungModel().setKota_tt(et_kota.getText().toString());
                me.getTertanggungModel().setKdpos_tt(et_kode_pos.getText().toString());
                me.getTertanggungModel().setKdtelp1_tt(et_kdtelp1.getText().toString());
                me.getTertanggungModel().setTelp1_tt(et_telp1.getText().toString());
                me.getTertanggungModel().setKdtelp2_tt(et_kdtelp2.getText().toString());
                me.getTertanggungModel().setTelp2_tt(et_telp2.getText().toString());
                break;
            case 5: //kantor
                me.getTertanggungModel().setAlamat_kantor_tt(et_alamat.getText().toString());
                me.getTertanggungModel().setKota_kantor_tt(et_kota.getText().toString());
                me.getTertanggungModel().setKdpos_kantor_tt(et_kode_pos.getText().toString());
                me.getTertanggungModel().setKdtelp1_kantor_tt(et_kdtelp1.getText().toString());
                me.getTertanggungModel().setTelp1_kantor_tt(et_telp1.getText().toString());
                me.getTertanggungModel().setKdtelp2_kantor_tt(et_kdtelp2.getText().toString());
                me.getTertanggungModel().setTelp2_kantor_tt(et_telp2.getText().toString());
//                me.getTertanggungModel().setKdfax_kantor_tt(et_kdfax.getText().toString());
                me.getTertanggungModel().setFax_kantor_tt(et_fax.getText().toString());
                break;
            case 6: //tempat tinggal
                me.getTertanggungModel().setAlamat_tinggal_tt(et_alamat.getText().toString());
                me.getTertanggungModel().setKota_tinggal_tt(et_kota.getText().toString());
                me.getTertanggungModel().setKdpos_tinggal_tt(et_kode_pos.getText().toString());
                me.getTertanggungModel().setKdtelp1_tinggal_tt(et_kdtelp1.getText().toString());
                me.getTertanggungModel().setTelp1_tinggal_tt(et_telp1.getText().toString());
                me.getTertanggungModel().setKdtelp2_tinggal_tt(et_kdtelp2.getText().toString());
                me.getTertanggungModel().setTelp2_tinggal_tt(et_telp2.getText().toString());
                me.getTertanggungModel().setKdfax_tinggal_tt(et_kdfax.getText().toString());
                me.getTertanggungModel().setFax_tinggal_tt(et_fax.getText().toString());
                break;
            case 7: //pp
                me.getPemegangPolisModel().setHp1_pp(et_hp1.getText().toString());
                me.getPemegangPolisModel().setHp2_pp(et_hp2.getText().toString());
                me.getPemegangPolisModel().setEmail_pp(et_email.getText().toString());
                break;
            case 8: //tt
                me.getTertanggungModel().setHp1_tt(et_hp1.getText().toString());
                me.getTertanggungModel().setHp2_tt(et_hp2.getText().toString());
                me.getTertanggungModel().setEmail_tt(et_email.getText().toString());
                break;
        }
    }

    private void restore() {
        switch (flag_input) {
            //PP
            case 1: //rumah
                et_alamat.setText(me.getPemegangPolisModel().getAlamat_pp());
                et_kota.setText(me.getPemegangPolisModel().getKota_pp());
                et_kode_pos.setText(me.getPemegangPolisModel().getKdpos_pp());
                et_kdtelp1.setText(me.getPemegangPolisModel().getKdtelp1_pp());
                et_telp1.setText(me.getPemegangPolisModel().getTelp1_pp());
                et_kdtelp2.setText(me.getPemegangPolisModel().getKdtelp2_pp());
                et_telp2.setText(me.getPemegangPolisModel().getTelp2_pp());
                cl_kdfax.setVisibility(View.GONE);
                cl_fax.setVisibility(View.GONE);
                if (!me.getPemegangPolisModel().getAlamat_pp().equals("")) {
                    btn_cancel.setText("HAPUS");
                } else {
                    btn_cancel.setText("BATAL");
                }
                break;
            case 2: //kantor
                et_alamat.setText(me.getPemegangPolisModel().getAlamat_kantor_pp());
                et_kota.setText(me.getPemegangPolisModel().getKota_kantor_pp());
                et_kode_pos.setText(me.getPemegangPolisModel().getKdpos_kantor_pp());
                et_kdtelp1.setText(me.getPemegangPolisModel().getKdtelp1_kantor_pp());
                et_telp1.setText(me.getPemegangPolisModel().getTelp1_kantor_pp());
                et_kdtelp2.setText(me.getPemegangPolisModel().getKdtelp2_kantor_pp());
                et_telp2.setText(me.getPemegangPolisModel().getTelp2_kantor_pp());
                et_kdfax.setText(me.getPemegangPolisModel().getKdfax_kantor_pp());
                et_fax.setText(me.getPemegangPolisModel().getFax_kantor_pp());
                if (!me.getPemegangPolisModel().getAlamat_kantor_pp().equals("")) {
                    btn_cancel.setText("HAPUS");
                } else {
                    btn_cancel.setText("BATAL");
                }
                break;
            case 3: //tempat tinggal
                et_alamat.setText(me.getPemegangPolisModel().getAlamat_tinggal_pp());
                et_kota.setText(me.getPemegangPolisModel().getKota_tinggal_pp());
                et_kode_pos.setText(me.getPemegangPolisModel().getKdpos_tinggal_pp());
                et_kdtelp1.setText(me.getPemegangPolisModel().getKdtelp1_tinggal_pp());
                et_telp1.setText(me.getPemegangPolisModel().getTelp1_tinggal_pp());
                et_kdtelp2.setText(me.getPemegangPolisModel().getKdtelp2_tinggal_pp());
                et_telp2.setText(me.getPemegangPolisModel().getTelp2_tinggal_pp());
                et_kdfax.setText(me.getPemegangPolisModel().getKdfax_tinggal_pp());
                et_fax.setText(me.getPemegangPolisModel().getFax_tinggal_pp());
                if (!me.getPemegangPolisModel().getAlamat_tinggal_pp().equals("")) {
                    btn_cancel.setText("HAPUS");
                } else {
                    btn_cancel.setText("BATAL");
                }
                break;
            //TT
            case 4: //rumah
                et_alamat.setText(me.getTertanggungModel().getAlamat_tt());
                et_kota.setText(me.getTertanggungModel().getKota_tt());
                et_kode_pos.setText(me.getTertanggungModel().getKdpos_tt());
                et_kdtelp1.setText(me.getTertanggungModel().getKdtelp1_tt());
                et_telp1.setText(me.getTertanggungModel().getTelp1_tt());
                et_kdtelp2.setText(me.getTertanggungModel().getKdtelp2_tt());
                et_telp2.setText(me.getTertanggungModel().getTelp2_tt());
                cl_kdfax.setVisibility(View.GONE);
                cl_fax.setVisibility(View.GONE);
                if (!me.getTertanggungModel().getAlamat_tt().equals("")) {
                    btn_cancel.setText("HAPUS");
                } else {
                    btn_cancel.setText("BATAL");
                }
                break;
            case 5: //kantor
                et_alamat.setText(me.getTertanggungModel().getAlamat_kantor_tt());
                et_kota.setText(me.getTertanggungModel().getKota_kantor_tt());
                et_kode_pos.setText(me.getTertanggungModel().getKdpos_kantor_tt());
                et_kdtelp1.setText(me.getTertanggungModel().getKdtelp1_kantor_tt());
                et_telp1.setText(me.getTertanggungModel().getTelp1_kantor_tt());
                et_kdtelp2.setText(me.getTertanggungModel().getKdtelp2_kantor_tt());
                et_telp2.setText(me.getTertanggungModel().getTelp1_kantor_tt());
//                et_kdfax.setText(me.getTertanggungModel().getko());
                et_fax.setText(me.getTertanggungModel().getFax_kantor_tt());
                if (!me.getTertanggungModel().getAlamat_kantor_tt().equals("")) {
                    btn_cancel.setText("HAPUS");
                } else {
                    btn_cancel.setText("BATAL");
                }
                break;
            case 6: //tempat tinggal
                et_alamat.setText(me.getTertanggungModel().getAlamat_tinggal_tt());
                et_kota.setText(me.getTertanggungModel().getKota_tinggal_tt());
                et_kode_pos.setText(me.getTertanggungModel().getKdpos_tinggal_tt());
                et_kdtelp1.setText(me.getTertanggungModel().getKdtelp1_tinggal_tt());
                et_telp1.setText(me.getTertanggungModel().getTelp1_tinggal_tt());
                et_kdtelp2.setText(me.getTertanggungModel().getKdtelp2_tinggal_tt());
                et_telp2.setText(me.getTertanggungModel().getTelp1_tinggal_tt());
                et_kdfax.setText(me.getTertanggungModel().getKdfax_tinggal_tt());
                et_fax.setText(me.getTertanggungModel().getFax_tinggal_tt());
                if (!me.getTertanggungModel().getAlamat_tinggal_tt().equals("")) {
                    btn_cancel.setText("HAPUS");
                } else {
                    btn_cancel.setText("BATAL");
                }
                break;
            case 7: //pp
                et_hp1.setText(me.getPemegangPolisModel().getHp1_pp());
                et_hp2.setText(me.getPemegangPolisModel().getHp2_pp());
                et_email.setText(me.getPemegangPolisModel().getEmail_pp());

                if (me.getPemegangPolisModel().getHp1_pp().equals("")) {
                    btn_cancel.setText("BATAL");
                } else {
                    btn_cancel.setText("HAPUS");
                }
                break;
            case 8: //tt
                et_hp1.setText(me.getTertanggungModel().getHp1_tt());
                et_hp2.setText(me.getTertanggungModel().getHp2_tt());
                et_email.setText(me.getTertanggungModel().getEmail_tt());
                if (me.getTertanggungModel().getHp1_tt().equals("")) {
                    btn_cancel.setText("BATAL");
                } else {
                    btn_cancel.setText("HAPUS");
                }
        }
    }

    private boolean Validation() {
        boolean val = true;
        int color = ContextCompat.getColor(this, R.color.red);
        if (cl_form_info_umum.getVisibility() == View.VISIBLE) {
            if (et_alamat.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_alamat, tv_error_alamat, iv_circle_alamat, color, this, getString(R.string.error_field_required));
//                tv_error_alamat.setText(R.string.field_requirement);
//                tv_error_alamat.setVisibility(View.VISIBLE);
//                iv_circle_alamat.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
            } else {
                if (!Method_Validator.ValEditRegex(et_alamat.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_alamat, tv_error_alamat, iv_circle_alamat, color, this, getString(R.string.error_regex));
                }
            }
            if (et_kota.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_kota, tv_error_kota, iv_circle_kota, color, this, getString(R.string.error_field_required));
//                tv_error_kota.setText(R.string.field_requirement);
//                tv_error_kota.setVisibility(View.VISIBLE);
//                iv_circle_kota.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
            } else {
                if (!Method_Validator.ValEditRegex(et_kota.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_kota, tv_error_kota, iv_circle_kota, color, this, getString(R.string.error_regex));
                }
            }
        } else if (cl_form_info_hp.getVisibility() == View.VISIBLE) {
            if (et_hp1.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_hp1, tv_error_hp1, iv_circle_hp1, color, this, getString(R.string.error_field_required));
//                tv_error_hp1.setText(R.string.field_requirement);
//                tv_error_hp1.setVisibility(View.VISIBLE);
//                iv_circle_hp1.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
            } else {
                if (!Method_Validator.ValEditRegex(et_hp1.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_hp1, tv_error_hp1, iv_circle_hp1, color, this, getString(R.string.error_regex));
                }
            }
            if (et_email.getText().toString().trim().length() != 0) {
                if (Method_Validator.emailValidator(et_email.getText().toString()) == false) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_email, tv_error_email, iv_circle_email, color, this, getString(R.string.error_field_required));
//                    tv_error_email.setText(R.string.false_format);
//                    tv_error_email.setVisibility(View.VISIBLE);
//                    iv_circle_email.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
                }
            }
        }

        return val;
    }

    private class MTextWatcher implements TextWatcher {
        private View view;

        MTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                case R.id.et_alamat:
                    if (StaticMethods.isTextWidgetEmpty(et_alamat)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_alamat.setColorFilter(color);
                    tv_error_alamat.setVisibility(View.GONE);
                    break;
                case R.id.et_kota:
                    if (StaticMethods.isTextWidgetEmpty(et_kota)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_kota.setColorFilter(color);
                    tv_error_kota.setVisibility(View.GONE);
                    break;
                case R.id.et_hp1:
                    if (StaticMethods.isTextWidgetEmpty(et_hp1)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_hp1.setColorFilter(color);
                    tv_error_hp1.setVisibility(View.GONE);
                    break;
                case R.id.et_email:
                    if (Method_Validator.emailValidator(et_email.getText().toString()) == false) {
                        et_email.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.red));
                    } else {
                        et_email.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.dua));
                        iv_circle_email.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.green));
                    }
                    break;
            }
        }
    }

    @BindView(R.id.et_alamat)
    EditText et_alamat;
    @BindView(R.id.et_kode_pos)
    EditText et_kode_pos;
    @BindView(R.id.et_kota)
    EditText et_kota;
    @BindView(R.id.et_kdtelp1)
    EditText et_kdtelp1;
    @BindView(R.id.et_telp1)
    EditText et_telp1;
    @BindView(R.id.et_kdtelp2)
    EditText et_kdtelp2;
    @BindView(R.id.et_telp2)
    EditText et_telp2;
    @BindView(R.id.et_kdfax)
    EditText et_kdfax;
    @BindView(R.id.et_fax)
    EditText et_fax;

    @BindView(R.id.btn_cancel)
    Button btn_cancel;
    @BindView(R.id.btn_lanjut)
    Button btn_lanjut;

    @BindView(R.id.iv_circle_alamat)
    ImageView iv_circle_alamat;
    @BindView(R.id.iv_circle_kota)
    ImageView iv_circle_kota;

    @BindView(R.id.tv_error_alamat)
    TextView tv_error_alamat;
    @BindView(R.id.tv_error_kota)
    TextView tv_error_kota;

    @BindView(R.id.cl_fax)
    RelativeLayout cl_fax;
    @BindView(R.id.cl_kdfax)
    RelativeLayout cl_kdfax;
    @BindView(R.id.cl_form_info_umum)
    RelativeLayout cl_form_info_umum;
    @BindView(R.id.cl_form_info_hp)
    RelativeLayout cl_form_info_hp;

    @BindView(R.id.et_hp1)
    EditText et_hp1;
    @BindView(R.id.et_hp2)
    EditText et_hp2;
    @BindView(R.id.et_email)
    EditText et_email;

    @BindView(R.id.iv_circle_hp1)
    ImageView iv_circle_hp1;
    @BindView(R.id.iv_circle_email)
    ImageView iv_circle_email;

    @BindView(R.id.tv_error_hp1)
    TextView tv_error_hp1;
    @BindView(R.id.tv_error_email)
    TextView tv_error_email;

    @BindView(R.id.scroll_alamat)
    ScrollView scroll_alamat;
    @BindView(R.id.cl_child_alamat)
    RelativeLayout cl_child_alamat;

    private EspajModel me;

    private int flag_input = 0;

    private Intent intent;
}
