package id.co.ajsmsig.nb.crm.model.form;
/*
 Created by faiz_f on 10/08/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

public class SimbakAuditModel implements Parcelable {
    private Long SA2_TAB_ID;
    private Long SA2_ID;
    private String SA2_USERID;
    private String SA2_INST_ID;
    private Integer SA2_INST_TYPE;
    private String SA2_DETAIL;
    private String SA2_DATA;
    private String SA2_ACTION;
    private Integer SA2_APP;

    public SimbakAuditModel() {
    }

    protected SimbakAuditModel(Parcel in) {
        SA2_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SA2_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SA2_USERID = in.readString();
        SA2_INST_ID = in.readString();
        SA2_INST_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SA2_DETAIL = in.readString();
        SA2_DATA = in.readString();
        SA2_ACTION = in.readString();
        SA2_APP = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<SimbakAuditModel> CREATOR = new Creator<SimbakAuditModel>() {
        @Override
        public SimbakAuditModel createFromParcel(Parcel in) {
            return new SimbakAuditModel(in);
        }

        @Override
        public SimbakAuditModel[] newArray(int size) {
            return new SimbakAuditModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(SA2_TAB_ID);
        dest.writeValue(SA2_ID);
        dest.writeString(SA2_USERID);
        dest.writeString(SA2_INST_ID);
        dest.writeValue(SA2_INST_TYPE);
        dest.writeString(SA2_DETAIL);
        dest.writeString(SA2_DATA);
        dest.writeString(SA2_ACTION);
        dest.writeValue(SA2_APP);
    }

    public Long getSA2_TAB_ID() {
        return SA2_TAB_ID;
    }

    public void setSA2_TAB_ID(Long SA2_TAB_ID) {
        this.SA2_TAB_ID = SA2_TAB_ID;
    }

    public Long getSA2_ID() {
        return SA2_ID;
    }

    public void setSA2_ID(Long SA2_ID) {
        this.SA2_ID = SA2_ID;
    }

    public String getSA2_USERID() {
        return SA2_USERID;
    }

    public void setSA2_USERID(String SA2_USERID) {
        this.SA2_USERID = SA2_USERID;
    }

    public String getSA2_INST_ID() {
        return SA2_INST_ID;
    }

    public void setSA2_INST_ID(String SA2_INST_ID) {
        this.SA2_INST_ID = SA2_INST_ID;
    }

    public Integer getSA2_INST_TYPE() {
        return SA2_INST_TYPE;
    }

    public void setSA2_INST_TYPE(Integer SA2_INST_TYPE) {
        this.SA2_INST_TYPE = SA2_INST_TYPE;
    }

    public String getSA2_DETAIL() {
        return SA2_DETAIL;
    }

    public void setSA2_DETAIL(String SA2_DETAIL) {
        this.SA2_DETAIL = SA2_DETAIL;
    }

    public String getSA2_DATA() {
        return SA2_DATA;
    }

    public void setSA2_DATA(String SA2_DATA) {
        this.SA2_DATA = SA2_DATA;
    }

    public String getSA2_ACTION() {
        return SA2_ACTION;
    }

    public void setSA2_ACTION(String SA2_ACTION) {
        this.SA2_ACTION = SA2_ACTION;
    }

    public Integer getSA2_APP() {
        return SA2_APP;
    }

    public void setSA2_APP(Integer SA2_APP) {
        this.SA2_APP = SA2_APP;
    }
}
