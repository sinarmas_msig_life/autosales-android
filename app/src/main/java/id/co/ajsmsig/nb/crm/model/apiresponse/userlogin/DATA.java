
package id.co.ajsmsig.nb.crm.model.apiresponse.userlogin;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DATA {

    @SerializedName("MENU")
    @Expose
    private List<MENU> mENU = null;
    @SerializedName("AGENT_INFO")
    @Expose
    private AGENTINFO aGENTINFO;
    @SerializedName("ROLE_ID")
    @Expose
    private Integer rOLEID;

    public List<MENU> getMENU() {
        return mENU;
    }

    public void setMENU(List<MENU> mENU) {
        this.mENU = mENU;
    }

    public AGENTINFO getAGENTINFO() {
        return aGENTINFO;
    }

    public void setAGENTINFO(AGENTINFO aGENTINFO) {
        this.aGENTINFO = aGENTINFO;
    }

    public Integer getROLEID() {
        return rOLEID;
    }

    public void setROLEID(Integer rOLEID) {
        this.rOLEID = rOLEID;
    }

}
