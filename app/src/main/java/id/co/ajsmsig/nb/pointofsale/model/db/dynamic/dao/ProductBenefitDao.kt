package id.co.ajsmsig.nb.pointofsale.model.db.dynamic.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductBenefit


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface ProductBenefitDao: BaseDao<ProductBenefit> {

    @Query("select * from ProductBenefit")
    fun loadAllProductBenefits(): LiveData<List<ProductBenefit>>

    @Query("select * from ProductBenefit where ProductLsbsId = :lsbsId")
    fun loadAllProductBenefitWithId(lsbsId: Int?): LiveData<List<ProductBenefit>>




}