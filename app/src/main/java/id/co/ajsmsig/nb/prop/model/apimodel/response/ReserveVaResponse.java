
package id.co.ajsmsig.nb.prop.model.apimodel.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReserveVaResponse {

    @SerializedName("virtual")
    @Expose
    private List<Virtual> virtual = null;
    @SerializedName("ERROR")
    @Expose
    private String eRROR;

    public List<Virtual> getVirtual() {
        return virtual;
    }

    public void setVirtual(List<Virtual> virtual) {
        this.virtual = virtual;
    }

    public String getERROR() {
        return eRROR;
    }

    public void setERROR(String eRROR) {
        this.eRROR = eRROR;
    }

}
