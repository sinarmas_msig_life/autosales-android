package id.co.ajsmsig.nb.crm.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.activity.C_TrackPolis;
import id.co.ajsmsig.nb.crm.adapter.C_SPAJListAdapter;
import id.co.ajsmsig.nb.crm.fragment.listspaj.ChildSPAJList;
import id.co.ajsmsig.nb.crm.fragment.listspaj.ParentSPAJList;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.database.E_Delete;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.DateUtils;

/**
 * Created by eriza on 06/09/2017.
 */

public class C_AgentListSpajFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, C_SPAJListAdapter.ClickListener, DialogInterface.OnClickListener {
    private static final String TAG = C_AgentListSpajFragment.class.getSimpleName();
    private String kodeAgen;
    private Cursor cursor = null;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.layoutNoSPAJFound)
    LinearLayout layoutNoSPAJFound;
    @BindView(R.id.btnOk)
    Button btnOk;

    private List<ParentSPAJList> parents;
    private List<ChildSPAJList> draftSpaj;
    private List<ChildSPAJList> submitSpaj;
    private C_SPAJListAdapter mAdapter;
    private final String SUBMIT = "1";
    private int selectedLeadSortMode;

    private final int LOADER_SORT_SPAJ_BY_DATE = 100;
    private final int LOADER_SORT_SPAJ_BY_NAME = 200;
    private final int LOADER_DISPLAY_SPAJ_CURRENT_MONTH_ONLY = 300;

    private final int SORT_BY_DATE = 0; //Default
    private final int SORT_BY_NAME = 1;
    private final int SORT_BY_DISPLAY_CURRENT_MONTH_ONLY = 2;

    public C_AgentListSpajFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), Context.MODE_PRIVATE);
        kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.e_fragment_agent_list_spaj, container, false);
        ButterKnife.bind(this, view);
        initRecyclerView();


        return view;
    }

    private void initRecyclerView() {
        parents = new ArrayList<>();
        draftSpaj = new ArrayList<>();
        submitSpaj = new ArrayList<>();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().restartLoader(LOADER_SORT_SPAJ_BY_DATE, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader cursorLoader;

        switch (id) {
            case LOADER_SORT_SPAJ_BY_DATE:
            case LOADER_SORT_SPAJ_BY_NAME:
            case LOADER_DISPLAY_SPAJ_CURRENT_MONTH_ONLY:
                cursor = new E_Select(getActivity()).getSPAJList(kodeAgen);
                break;
        }


        cursorLoader = new CursorLoader(getContext()) {
            @Override
            public Cursor loadInBackground() {
                return cursor;
            }
        };

        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        recyclerView.setVisibility(View.VISIBLE);
        layoutNoSPAJFound.setVisibility(View.GONE);

        clearPreviousSpajResult();

        int id = loader.getId();

        switch (id) {
            case LOADER_SORT_SPAJ_BY_DATE:
            case LOADER_SORT_SPAJ_BY_NAME:
            case LOADER_DISPLAY_SPAJ_CURRENT_MONTH_ONLY:
                if (data.getCount() > 0) {

                    List<ChildSPAJList> childs = new ArrayList<>();


                    while (data.moveToNext()) {

                        long _id = data.getLong(data.getColumnIndexOrThrow("_id"));
                        String spajIdTab = data.getString(data.getColumnIndexOrThrow("SPAJ_ID_TAB"));
                        String noProposalTab = data.getString(data.getColumnIndexOrThrow("NO_PROPOSAL_TAB"));
                        String namaPp = data.getString(data.getColumnIndexOrThrow("NAMA_PP"));
                        String namaTt = data.getString(data.getColumnIndexOrThrow("NAMA_TT"));
                        String premiPokokDi = data.getString(data.getColumnIndexOrThrow("PREMI_POKOK_DI"));
                        String kdProdukUa = data.getString(data.getColumnIndexOrThrow("KD_PRODUK_UA"));
                        String virtualAcc = data.getString(data.getColumnIndexOrThrow("VIRTUAL_ACC"));
                        String spajId = data.getString(data.getColumnIndexOrThrow("SPAJ_ID"));
                        String flagDokumen = data.getString(data.getColumnIndexOrThrow("FLAG_DOKUMEN"));
                        String subProdukUa = data.getString(data.getColumnIndexOrThrow("SUB_PRODUK_UA"));

                        String productName = new P_Select(getActivity()).selectProductName(
                                data.getInt(data.getColumnIndexOrThrow("KD_PRODUK_UA")),
                                data.getInt(data.getColumnIndexOrThrow("SUB_PRODUK_UA")));

                        int month = DateUtils.getMonthFromSpajIdTab(spajIdTab);
                        Date inputDate = DateUtils.getDateFromSpajIdTab(spajIdTab);

                        Log.v(getClass().getSimpleName(), "SPAJ "+spajIdTab+ " Bulan ke "+month+ " Bulan sekarang " + DateUtils.getCurrentMonth());

                        ChildSPAJList child = new ChildSPAJList(_id, spajIdTab, noProposalTab, namaPp, namaTt, premiPokokDi, productName, virtualAcc, spajId, flagDokumen, subProdukUa, inputDate, month);
                        childs.add(child);

                    }

                    filterSPAJData(selectedLeadSortMode, childs);


                    for (ChildSPAJList child : childs) {
                        if (child.getFlagDokumen().equalsIgnoreCase(SUBMIT)) {
                            submitSpaj.add(child);
                        } else {
                            draftSpaj.add(child);
                        }
                    }

                }
                break;
        }


        int draftSPAJCount = draftSpaj.size();
        int submitSPAJCount = submitSpaj.size();

        //Set group and its child
        parents.add(new ParentSPAJList("Draft - " + draftSPAJCount, draftSpaj));
        parents.add(new ParentSPAJList("Submit - " + submitSPAJCount, submitSpaj));


        displaySPAJData(parents);

    }

    private void filterSPAJData(int selectedLeadSortMode, List<ChildSPAJList> childs) {
        int currentMonth = DateUtils.getCurrentMonth();

        switch (selectedLeadSortMode) {
            case SORT_BY_NAME :
                Collections.sort(childs, (o1, o2) -> o1.getNamaPp().compareToIgnoreCase(o2.getNamaPp()));
                break;
            case SORT_BY_DISPLAY_CURRENT_MONTH_ONLY :
                Iterator<ChildSPAJList> iterator = childs.iterator();
                while (iterator.hasNext()) {
                    ChildSPAJList child = iterator.next();
                    int month = child.getMonth();
                    if (month != currentMonth) {
                        iterator.remove();
                    }
                }
                if (childs.size() == 0) {
                    recyclerView.setVisibility(View.GONE);
                    layoutNoSPAJFound.setVisibility(View.VISIBLE);
                }
                break;
            case SORT_BY_DATE :
                Collections.sort(childs, (o1, o2) -> {
                    if (o1.getInputDate().after(o2.getInputDate())) {
                        return 1;
                    } else if (o1.getInputDate().before(o2.getInputDate())) {
                        return -1;
                    } else {
                        return 0;
                    }

                });
                break;
        }

    }

    private void displaySPAJData(List<ParentSPAJList> parents) {
        mAdapter = new C_SPAJListAdapter(parents);
        mAdapter.setClickListener(this);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        //Expand draft group by default
        mAdapter.onGroupClick(0);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


    /**
     * Remove previous spaj result so it will displayed the latest one
     */
    private void clearPreviousSpajResult() {
        if (parents != null && parents.size() > 0) {

            parents.clear();

            if (draftSpaj != null) draftSpaj.clear();
            if (submitSpaj != null) submitSpaj.clear();
            if (mAdapter != null) mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_sort_spaj, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                //launchSearchLeadActivity();
                break;
            case R.id.action_sort:
                displaySortOptionsDialog();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Display sort options dialog. (Sort by date or sort by name)
     */
    private void displaySortOptionsDialog() {
        final String[] sortOptions = new String[]{"Data terbaru", "Nama", "Tampilkan SPAJ bulan ini saja"};
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_title_sort_by)
                .setSingleChoiceItems(sortOptions, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        selectedLeadSortMode = which;
                    }
                })
                .setPositiveButton("Pilih", this)
                .show();
    }

    /**
     * Dialog single choice click listener
     *
     * @param dialogInterface
     * @param i
     */
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (selectedLeadSortMode) {
            case SORT_BY_DATE:
                getLoaderManager().restartLoader(LOADER_SORT_SPAJ_BY_DATE, null, this);
                dialogInterface.dismiss();
                break;

            case SORT_BY_NAME:
                getLoaderManager().restartLoader(LOADER_SORT_SPAJ_BY_NAME, null, this);
                dialogInterface.dismiss();
                break;

            case SORT_BY_DISPLAY_CURRENT_MONTH_ONLY:
                getLoaderManager().restartLoader(LOADER_DISPLAY_SPAJ_CURRENT_MONTH_ONLY, null, this);
                dialogInterface.dismiss();
                break;

            default:
                throw new IllegalArgumentException("Undefined leads sort options");
        }

    }

    @OnClick(R.id.btnOk)
    void onButtonOkPressed() {
        getLoaderManager().restartLoader(LOADER_SORT_SPAJ_BY_DATE, null, this);
        selectedLeadSortMode = LOADER_SORT_SPAJ_BY_DATE;
    }
    @Override
    public void onSPAJPressed(ChildSPAJList spaj) {
        String spajIdTab = spaj.getSpajIdTab();
        String noProposalTab = spaj.getNoProposalTab();

        Intent intent = new Intent(getActivity(), E_MainActivity.class);
        intent.putExtra(getActivity().getString(R.string.SPAJ_ID_TAB), spajIdTab);
        intent.putExtra(getActivity().getString(R.string.idproposal_tab), noProposalTab);
        getActivity().startActivity(intent);
    }

    @Override
    public void onTrackPolisPressed(ChildSPAJList spaj) {
        String spajNumber = spaj.getSpajId();
        String ppName = spaj.getNamaPp();

        Intent intent = new Intent(getActivity(), C_TrackPolis.class);
        intent.putExtra(Const.INTENT_KEY_SPAJ_NO_TEMP, spajNumber);
        intent.putExtra(Const.INTENT_KEY_SPAJ_PP_NAME, ppName);
        getActivity().startActivity(intent);
    }

    @Override
    public void onDeletePressed(ChildSPAJList spaj, int position) {
        android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(getActivity());
        dialog.setTitle("Konfirmasi...");
        dialog.setMessage(R.string.delete_message_confirmation);
        dialog.setNegativeButton(getString(R.string.tidak), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.setPositiveButton(getString(R.string.iya), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String spajIdTab = spaj.getSpajIdTab();

                E_Delete delete = new E_Delete(getActivity() );
                delete.deleteListSpajDraft(spajIdTab);
                draftSpaj.remove(position-1);
                mAdapter.notifyItemRemoved(position-1);
                mAdapter.notifyItemRangeChanged(position-1, draftSpaj.size());
                refresh();
            }
        });
        dialog.show();
    }

    private void refresh() {
        getLoaderManager().restartLoader(LOADER_SORT_SPAJ_BY_DATE, null, this);
    }

}

