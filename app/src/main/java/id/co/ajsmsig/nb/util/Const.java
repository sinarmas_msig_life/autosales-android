package id.co.ajsmsig.nb.util;

/**
 * Created by Fajar.Azhar on 09/03/2018.
 */

public class Const {
    public static final String INTENT_KEY_AGENT_CODE = "AGENT_CODE";
    public static final String INTENT_KEY_CERTIFICATE_FILE = "certificate-file";
    public static final String INTENT_KEY_SPAJ_NO_TEMP = "spaj-number-temp";
    public static final String INTENT_KEY_SPAJ_PP_NAME = "spaj-pp-name";
    public static final String INTENT_KEY_ALLOWED_TO_DO_CLOSING_ACTIVITY = "closing-activity";
    public static final String BUNDLE_KEY_LOADER_SEARCH_LEAD = "query";
    public static final String INTENT_KEY_VA_EXIST = "va-exist";
    public static final String INTENT_KEY_VA_NUMBER = "va-number";
    public static final String INTENT_KEY_PP_NAME = "pp-name";
    public static final String PREF_NAME = "autosales";
    public static final String INTENT_KEY_LEAD_TAB_ID = "lead-tab-id";
    public static final String INTENT_KEY_PROPOSAL_ID = "proposal-id";
    public static final String INTENT_KEY_SL_TAB_ID = "sl-tab-id";
    public static final String INTENT_KEY_SL_NAME = "sl-name";
    public static final String INTENT_KEY_SLA_ID = "sla-id";
    public static final String INTENT_KEY_SLA_TAB_ID = "sla-tab-id";
    public static final String INTENT_KEY_EDIT_MODE= "edit-mode";
    public static final String INTENT_KEY_ADD_PROPOSAL_MODE= "add-proposal-mode";
    public static final String INTENT_KEY_ADD_ACTIVITY_MODE= "add-activity-mode";
    public static final String INTENT_KEY_NO_PROPOSAL_TAB= "no-proposal-tab";
    public static final String INTENT_KEY_ADD_PROPOSAL_MODE_SIAP2U= "add-proposal-mode-siap2u";
    public static final String INTENT_KEY_ADD_PROPOSAL_MODE_POS= "add-proposal-mode-pos";
    public static final String INTENT_KEY_SL_ID= "sl-id";
    public static final String INTENT_KEY_WEBVIEW_URL= "web-view-url";
    public static final String INTENT_KEY_SHOULD_HIDE_TARIK_VA_MENU = "shouldHideTarikVaMenu";
    public static final String INTENT_KEY_REPORT_MODE= "report-mode-id";

    //Credit Card Payment Siap2U
    public static final String INTENT_KEY_TRANSACTION_NUMBER= "transaction-number";
    public static final String INTENT_KEY_PAYMENT_STATUS= "payment-status";
    public static final String INTENT_KEY_PAYMENT_MESSAGE= "payment-message";

    public static final String AUTOSALES_BASE_URL = "https://crmuat.sinarmasmsiglife.co.id/";


    //Used by siap2U only
    public static final int TUNAITRANSFER_PAYMENT_CODE = 0;
    public static final int CREDIT_CARD_PAYMENT_CODE = 1;
    public static final int TABUNGAN_PAYMENT_CODE = 2;


    public static final int SIAP2U_LOGIN_TYPE = 2;
    public static final int BANCASSURANCE_LOGIN_TYPE = 9;
    public static final int SIAP2U_GROUP_ID = 40;

    //Bank code constant
    public static final int BANK_CODE_SINARMAS = 2;
    public static final int BANK_CODE_SINARMAS_SYARIAH = 16;
    public static final int BANK_CODE_BJB = 44;
    public static final int BANK_CODE_BUKOPIN= 50;
    public static final int BANK_CODE_JATIM = 51;
    public static final int BANK_CODE_BTN_SYARIAH = 56;

    //Page in SPAJ
    public static final int PAGE_PEMEGANG_POLIS = 1;
    public static final int PAGE_TERTANGGUNG = 2;
    public static final int PAGE_CALON_PEMBAYAR_PREMI = 3;
    public static final int PAGE_USULAN_ASURANSI = 4;
    public static final int PAGE_DETIL_INVESTASI = 5;
    public static final int PAGE_DETIL_AGEN = 6;
    public static final int PAGE_DOKUMEN_PENDUKUNG = 7;
    public static final int PAGE_QUESTIONAIRE = 8;
    public static final int PAGE_QUESTIONAIRE_SIO = 9;
    public static final int PAGE_PROFILE_RESIKO = 10;
    public static final int PAGE_KONFIRMASI_KIRIM = 11;

    public static final int GROUP_ID_AGENCY = 7;


    //PageReportDetail
    public static final int ALL_SUBMIT = 100;
    public static final int ON_PROCESS = 200;
    public static final int PENDING = 300;
    public static final int CANCELLED = 400;
    public static final int INFORCE = 500;

    //Product Code
    public static final int PRODUCT_MAGNA_LINK = 213;
    public static final int SUB_PRODUCT_MAGNA_LINK = 1;

    public static final int PRODUCT_MAGNA_LINK_SYARIAH = 216;
    public static final int SUB_PRODUCT_MAGNA_LINK_SYARIAH = 1;

    public static final int PRODUCT_PRIME_LINK = 134;
    public static final int SUB_PRODUCT_PRIME_LINK = 5;

    public static final int PRODUCT_PRIME_LINK_SYARIAH = 215;
    public static final int SUB_PRODUCT_PRIME_LINK_SYARIAH = 1;

    public static final String INTENT_KEY_AGENT_NAME = "AGENT_NAME";
    public static final String INTENT_KEY_CHANNEL ="KEY_CHANNEL";

    public static final String PREF_KEY_IS_CHANNEL_SELECTED = "is-channel-selected";
    public static final String PREF_KEY_SELECTED_CHANNEL_ID = "selected-channel-id";
    public static final String PREF_KEY_SELECTED_CHANNEL_NAME = "selected-channel-name";

}
