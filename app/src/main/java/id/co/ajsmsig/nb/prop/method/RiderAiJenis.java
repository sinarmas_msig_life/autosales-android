package id.co.ajsmsig.nb.prop.method;
/*
 *Created by faiz_f on 24/05/2017.
 */

public class RiderAiJenis {
    public static String getNamaRiderForIlustrasi(int lsbsId, int lsrJenis) {
        String namaRider = "";

        switch (lsbsId) {
            case 810://paFlag
                namaRider = "SMiLe Personal Accident";
                break;
            case 811://hcpFlag

                break;
            case 812:
                switch (lsrJenis) {
                    case 7://tpdFlag
                        namaRider = "SMiLe Total Permanent Disability (TPD)";
                        break;
                    default:
                        break;
                }
                break;
            case 813:
                switch (lsrJenis) {
                    case 8://ciFlag
                        namaRider = "SMiLe Critical Illness (CI)";
                        break;
                    default:
                        break;
                }
                break;
            case 814:
                switch (lsrJenis) {
                    case 4://WaiverTPDFlag
                        namaRider = "Waiver TPD";
                        break;
                    case 5://WaiverTPDFlag
                        namaRider = "Waiver TPD";
                        break;
                    default:
                        break;
                }
                break;
            case 815:
                switch (lsrJenis) {
                    case 4://PayorTpdDeathFlag
                        namaRider = "Payor TPD/Death";
                        break;
                    case 5://PayorTpdDeathFlag
                        namaRider = "Payor TPD/Death";
                        break;
                    case 6: //PayorSpouseTpdDeathFlag

                        break;
                    case 13: //PayorSpouseTpdDeathFlag NEW

                        break;
                    default:
                        break;

                }
                break;
            case 816:
                switch (lsrJenis) {
                    case 2://WaiverCiFlag
                        namaRider = "Waiver CI";
                        break;
                    case 3://WaiverCiFlag
                        namaRider = "Waiver CI";
                        break;
                    default:
                        break;
                }
                break;
            case 817:
                switch (lsrJenis) {
                    case 2://PayorCiDeathFlag5
                        namaRider = "Payor CI";
                        break;
                    case 3://PayorCiDeathFlag10
                        namaRider = "Payor CI";
                        break;
                    case 4://PayorCiFlag

                        break;
                }
                break;
            case 818:
                switch (lsrJenis) {
                    case 2://TermRiderFlag
                        namaRider = "SMiLe Term Insurance";
                        break;
                }
                break;
            case 819://HcpFamilyFlag
                namaRider = "SMiLe Hospital Protection (Family)";
                break;
            case 823://EkaSehatFlag
                namaRider = "SMiLe Medical (Benefit As Charge)";
                break;
            case 825://EkaSehatInnerLimitFlag

                break;
            case 826://HcpProviderFlag

                break;
            case 827:
                switch (lsrJenis) {
                    case 4://Waiver5Tpd10CiFlag
                        break;
                    case 5://Waiver5Tpd10CiFlag

                        break;
                    default://WaiverTpdCiFlag
//                            defaultnya 1, 2, & 3

                        break;
                }
                break;
            case 828:
                switch (lsrJenis) {
                    case 1://PayorTpdCiDeathFlag

                        break;
                    default://Payor5Tpd10CiDeathFlag
//                            defaultnya 2& 3

                        break;
                }
                break;
            case 830://LadiesInsFlag

                break;
            case 831://HcpLadiesFlag

                break;
            case 832://LadiesMedExpenseFlag

                break;
            case 833://LadiesMedExpenseInnerLimitFlag

                break;
            case 835://ScholarshipFlag

                break;
            case 836://BabyFlag

                break;
            case 837: //EarlyStageCi99Flag

                break;
            case 838: //MedicalPlusFlag

                break;
            default:
                break;
//        istr_prop.rider_baru[14] = 0;// swine
        }
        return namaRider;
    }
}
