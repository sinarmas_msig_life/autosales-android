package id.co.ajsmsig.nb.crm.fragment.listlead;

import android.os.Parcel;
import android.os.Parcelable;

public class AgencyLeadChildList implements Parcelable {
    private long id;
    private String leadName;
    private int leadAge;
    private int leadGender;
    private boolean isLeadFavorite;
    private String createdDate;
    private String updatedDate;
    private String firstNameInitial;
    private String lastNameInitial;


    public AgencyLeadChildList(long id, String leadName, int leadAge, int leadGender, boolean isLeadFavorite, String createdDate, String updatedDate, String firstNameInitial, String lastNameInitial) {
        this.id = id;
        this.leadName = leadName;
        this.leadAge = leadAge;
        this.leadGender = leadGender;
        this.isLeadFavorite = isLeadFavorite;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.firstNameInitial = firstNameInitial;
        this.lastNameInitial = lastNameInitial;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLeadName() {
        return leadName;
    }

    public void setLeadName(String leadName) {
        this.leadName = leadName;
    }

    public int getLeadAge() {
        return leadAge;
    }

    public void setLeadAge(int leadAge) {
        this.leadAge = leadAge;
    }

    public int getLeadGender() {
        return leadGender;
    }

    public void setLeadGender(int leadGender) {
        this.leadGender = leadGender;
    }

    public boolean isLeadFavorite() {
        return isLeadFavorite;
    }

    public void setLeadFavorite(boolean leadFavorite) {
        isLeadFavorite = leadFavorite;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }


    public String getFirstNameInitial() {
        return firstNameInitial;
    }

    public void setFirstNameInitial(String firstNameInitial) {
        this.firstNameInitial = firstNameInitial;
    }

    public String getLastNameInital() {
        return lastNameInitial;
    }

    public void setLastNameInital(String lastNameInital) {
        this.lastNameInitial = lastNameInital;
    }


    public static final Parcelable.Creator<AgencyLeadChildList> CREATOR = new Parcelable.Creator<AgencyLeadChildList>() {

        @Override
        public AgencyLeadChildList createFromParcel(Parcel source) {
            return null;
        }

        @Override
        public AgencyLeadChildList[] newArray(int size) {
            return new AgencyLeadChildList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(leadName);
    }



}
