package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Entity
class CalculatorResult(
        @PrimaryKey
        val id: Int,
        var pageOptionId: Int?,
        var totalEstimatedNeededText: String?,
        var totalEstimatedInvestmentText: String?,
        var totalEstimatedNeededImageName: String?,
        var totalEstimatedInvestmentImageName: String?,
        var totalFormula: String?,
        var totalPerMonthFormula: String?,
        var totalPerYearFormula: String?
): Serializable