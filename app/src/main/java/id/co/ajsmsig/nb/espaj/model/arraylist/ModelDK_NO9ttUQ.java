package id.co.ajsmsig.nb.espaj.model.arraylist;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 28/09/2017.
 */

public class ModelDK_NO9ttUQ implements Parcelable {
    private int counter = 0;
    private int  spin_ct = 0;
    private String str_ct = "";
    private String umur = "";
    private String keadaan = "";
    private String penyebab ="";
    private int id_keadaan = 0;

    public ModelDK_NO9ttUQ() {
    }

    public ModelDK_NO9ttUQ(int counter, int spin_ct, String str_ct, String umur, String keadaan, String penyebab, int id_keadaan) {
        this.counter = counter;
        this.spin_ct = spin_ct;
        this.str_ct = str_ct;
        this.umur = umur;
        this.keadaan = keadaan;
        this.penyebab = penyebab;
        this.id_keadaan = id_keadaan;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.counter);
        dest.writeInt(this.spin_ct);
        dest.writeString(this.str_ct);
        dest.writeString(this.umur);
        dest.writeString(this.keadaan);
        dest.writeString(this.penyebab);
        dest.writeInt(this.id_keadaan);
    }

    protected ModelDK_NO9ttUQ(Parcel in) {
        this.counter = in.readInt();
        this.spin_ct = in.readInt();
        this.str_ct = in.readString();
        this.umur = in.readString();
        this.keadaan = in.readString();
        this.penyebab = in.readString();
        this.id_keadaan = in.readInt();
    }

    public static final Parcelable.Creator<ModelDK_NO9ttUQ> CREATOR = new Parcelable.Creator<ModelDK_NO9ttUQ>() {
        @Override
        public ModelDK_NO9ttUQ createFromParcel(Parcel source) {
            return new ModelDK_NO9ttUQ(source);
        }

        @Override
        public ModelDK_NO9ttUQ[] newArray(int size) {
            return new ModelDK_NO9ttUQ[size];
        }
    };

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public int getSpin_ct() {
        return spin_ct;
    }

    public void setSpin_ct(int spin_ct) {
        this.spin_ct = spin_ct;
    }

    public String getStr_ct() {
        return str_ct;
    }

    public void setStr_ct(String str_ct) {
        this.str_ct = str_ct;
    }

    public String getUmur() {
        return umur;
    }

    public void setUmur(String umur) {
        this.umur = umur;
    }

    public String getKeadaan() {
        return keadaan;
    }

    public void setKeadaan(String keadaan) {
        this.keadaan = keadaan;
    }

    public String getPenyebab() {
        return penyebab;
    }

    public void setPenyebab(String penyebab) {
        this.penyebab = penyebab;
    }

    public int getId_keadaan() {
        return id_keadaan;
    }

    public void setId_keadaan(int id_keadaan) {
        this.id_keadaan = id_keadaan;
    }

    public static Creator<ModelDK_NO9ttUQ> getCREATOR() {
        return CREATOR;
    }
}
