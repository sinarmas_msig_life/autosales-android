package id.co.ajsmsig.nb.crm.adapter;

import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.fragment.listlead.ChildLeadList;
import id.co.ajsmsig.nb.crm.fragment.listspaj.ChildSPAJList;
import id.co.ajsmsig.nb.crm.fragment.listspaj.ParentSPAJList;

public class C_SPAJListAdapter extends ExpandableRecyclerViewAdapter<C_SPAJListAdapter.C_GroupSPAJViewHolder, C_SPAJListAdapter.C_ChildSPAJViewHolder> {
    private ClickListener clickListener;
    private final String SUBMIT = "1";
    public C_SPAJListAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    class C_ChildSPAJViewHolder extends ChildViewHolder {

        ConstraintLayout layoutSPAJ;
        TextView tvNoProposal;
        TextView tvPp;
        TextView tvTt;
        TextView tvPremi;
        TextView tvUp;
        Button btnTrackPolis;
        Button delete;
        ImageView imgSpaj;

        public C_ChildSPAJViewHolder(View view) {
            super(view);
            imgSpaj = view.findViewById(R.id.imgSpaj);
            layoutSPAJ = view.findViewById(R.id.layoutSPAJ);
            tvNoProposal = view.findViewById(R.id.tvNoProposal);
            tvPp = view.findViewById(R.id.tvPp);
            tvTt = view.findViewById(R.id.tvTt);
            tvPremi = view.findViewById(R.id.tvPremi);
            tvUp = view.findViewById(R.id.tvUp);
            btnTrackPolis = view.findViewById(R.id.btnTrackPolis);
            delete = view.findViewById(R.id.deleteButton);
        }

    }

    class C_GroupSPAJViewHolder extends GroupViewHolder {
        private TextView groupName;
        private ImageView imgExpandCollapse;

        public C_GroupSPAJViewHolder(View itemView) {
            super(itemView);
            groupName = itemView.findViewById(R.id.tvLeadGroupName);
            imgExpandCollapse = itemView.findViewById(R.id.imgExpandCollapse);
        }

        @Override
        public void expand() {
            imgExpandCollapse.setImageResource(R.drawable.ic_expand_less);
        }

        @Override
        public void collapse() {
            imgExpandCollapse.setImageResource(R.drawable.ic_expand_more);
        }

    }

    @Override
    public C_GroupSPAJViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.c_group_lead, parent, false);
        return new C_GroupSPAJViewHolder(view);
    }

    @Override
    public C_ChildSPAJViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.e_list_spaj, parent, false);
        return new C_ChildSPAJViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(final C_ChildSPAJViewHolder holder, int flatPosition, final ExpandableGroup group, int childIndex) {
        final ChildSPAJList model = ((ParentSPAJList) group).getItems().get(childIndex);

        boolean isSubmitted = model.getFlagDokumen().equalsIgnoreCase(SUBMIT);
        String ppName = "Nama Pemegang Polis : " + model.getNamaPp();
        String ttName = "Nama Tertanggung : " + model.getNamaTt();
        String proposalNumber = "No Proposal Tab : " + model.getNoProposalTab();
        String productName = "Produk : "+model.getKdProdukUa();

        if (isSubmitted) {
            holder.btnTrackPolis.setVisibility(View.VISIBLE);
            holder.tvNoProposal.setText(model.getSpajId());
            holder.imgSpaj.setImageResource(R.drawable.ic_complete);
            holder.delete.setVisibility(View.GONE);
        } else {
            holder.btnTrackPolis.setVisibility(View.GONE);
            holder.tvNoProposal.setText(model.getSpajIdTab());
            holder.imgSpaj.setImageResource(R.drawable.thumb_spaj);
            holder.delete.setVisibility(View.VISIBLE);
        }


        holder.tvPp.setText(ppName);
        holder.tvTt.setText(ttName);
        holder.tvPremi.setText(proposalNumber);
        holder.tvUp.setText(productName);

        holder.btnTrackPolis.setOnClickListener(v -> getClickListener().onTrackPolisPressed( model));
        holder.layoutSPAJ.setOnClickListener(v -> getClickListener().onSPAJPressed( model));
        holder.delete.setOnClickListener(v -> getClickListener().onDeletePressed( model, holder.getAdapterPosition()));

    }

    @Override
    public void onBindGroupViewHolder(final C_GroupSPAJViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.groupName.setText(group.getTitle());
    }

    public void removeItemAt(int position) {

    }
    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }
    public ClickListener getClickListener() {
        return clickListener;
    }

    public interface ClickListener {
        void onSPAJPressed(ChildSPAJList spaj);
        void onTrackPolisPressed(ChildSPAJList spaj);
        void onDeletePressed(ChildSPAJList spaj, int position);
    }
}
