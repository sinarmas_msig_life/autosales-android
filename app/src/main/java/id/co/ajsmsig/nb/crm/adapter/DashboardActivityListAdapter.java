package id.co.ajsmsig.nb.crm.adapter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.ActivityListModel;
import id.co.ajsmsig.nb.crm.model.DashboardActivityListModel;

public class DashboardActivityListAdapter extends RecyclerView.Adapter<DashboardActivityListAdapter.ActivityListVH> {
    private List<DashboardActivityListModel> activities;
    private ClickListener clickListener;

    public DashboardActivityListAdapter(List<DashboardActivityListModel> activities) {
        this.activities = activities;
    }

    public class ActivityListVH extends RecyclerView.ViewHolder {
        private TextView tvActivityDate;
        private TextView tvSubActivityName;
        private TextView tvLeadName;
        private ImageView imgActivity;
        private ConstraintLayout layoutActivity;
        private TextView tvActivityName;
        private ImageView ivSyncIndicator;

        public ActivityListVH(View view) {
            super(view);
            tvActivityDate = view.findViewById(R.id.tvActivityDate);
            tvSubActivityName = view.findViewById(R.id.tvSubActivity);
            tvLeadName = view.findViewById(R.id.tvLeadName);
            imgActivity = view.findViewById(R.id.imgActivity);
            layoutActivity = view.findViewById(R.id.layoutActivity);
            tvActivityName = view.findViewById(R.id.tvActivityName);
            ivSyncIndicator = view.findViewById(R.id.ivSyncIndicator);
        }

    }

    @Override
    public ActivityListVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_dashboard_activity, parent, false);
        return new ActivityListVH(itemView);
    }

    @Override
    public void onBindViewHolder(final ActivityListVH holder, int position) {
        DashboardActivityListModel model = activities.get(position);
        holder.tvLeadName.setText(model.getSlName());
        holder.tvSubActivityName.setText(model.getSubActivityName());
        holder.tvActivityName.setText(model.getActivityName());
        holder.tvActivityDate.setText(model.getActivityCreatedDate());
        holder.layoutActivity.setOnClickListener(v -> {
            clickListener.onActivitySelected(holder.getAdapterPosition(), model);
        });
        if (model.isSynced()){
            holder.ivSyncIndicator.setVisibility(View.INVISIBLE);
        } else {
            holder.ivSyncIndicator.setVisibility(View.VISIBLE);
        }

        String subActivityName = model.getSubActivityName();
        if (!TextUtils.isEmpty(subActivityName) && subActivityName.equalsIgnoreCase("CLOSING")) {
            holder.tvActivityName.setVisibility(View.GONE);
        } else {
            holder.tvActivityName.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onActivitySelected(int position, DashboardActivityListModel model);
    }

    public void addAll(List<DashboardActivityListModel> activityList) {
        for (DashboardActivityListModel activity : activityList) {
            activities.add(activity);
        }
        notifyDataSetChanged();
    }
}
