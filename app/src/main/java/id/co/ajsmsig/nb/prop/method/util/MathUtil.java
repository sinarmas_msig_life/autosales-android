package id.co.ajsmsig.nb.prop.method.util;

public class MathUtil
{
//    protected final Log logger = LogFactory.getLog( getClass() );

    public static double min( double value1, double value2 )
    {
        return value1 < value2? value1 : value2;
    }

    public static double max( double value1, double value2 )
    {
        return value1 > value2? value1 : value2;
    }

    public static int max( int[] arr )
    {
        return ArrUtil.max( arr );
    }

    public static int min( int[] arr )
    {
        return ArrUtil.min( arr );
    }

    public static double max( double[] arr )
    {
        return ArrUtil.max( arr );
    }

    public static double min( double[] arr )
    {
        return ArrUtil.min( arr );
    }
}
