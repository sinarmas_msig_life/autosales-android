package id.co.ajsmsig.nb.espaj.method;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAddRiderUA;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAskesUA;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_KetKesUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_NO9ppUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_NO9ttUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDataDitunjukDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelJnsDanaDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelMst_Answer;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTT_NO3UQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTT_NO5UQ;

/**
 * Created by Envy on 9/29/2017.
 */

public class JSONToString {
    public static String JSONSPAJ(Context context, EspajModel me) {
        String json = "";

        //Pemegang Polis
        try {
            JSONObject jsonObjectPP = new JSONObject();
            jsonObjectPP.put("nama_pp", me.getPemegangPolisModel().getNama_pp());
            jsonObjectPP.put("gelar_pp", me.getPemegangPolisModel().getGelar_pp());
            jsonObjectPP.put("nama_ibu_pp", me.getPemegangPolisModel().getNama_ibu_pp());
            jsonObjectPP.put("bukti_identitas_pp", me.getPemegangPolisModel().getBukti_identitas_pp());
            jsonObjectPP.put("bukti_identitas_lain_pp", me.getPemegangPolisModel().getBukti_identitas_lain_pp());
            jsonObjectPP.put("no_identitas_pp", me.getPemegangPolisModel().getNo_identitas_pp());
            jsonObjectPP.put("tgl_berlaku_pp", me.getPemegangPolisModel().getTgl_berlaku_pp());
            jsonObjectPP.put("warga_negara_pp", me.getPemegangPolisModel().getWarga_negara_pp());
            jsonObjectPP.put("tempat_pp", me.getPemegangPolisModel().getTempat_pp());
            jsonObjectPP.put("ttl_pp", me.getPemegangPolisModel().getTtl_pp());
            jsonObjectPP.put("usia_pp", me.getPemegangPolisModel().getUsia_pp().replace(" ", ""));
            jsonObjectPP.put("jekel_pp", me.getPemegangPolisModel().getJekel_pp());
            jsonObjectPP.put("status_pp", me.getPemegangPolisModel().getStatus_pp());
            jsonObjectPP.put("agama_pp", me.getPemegangPolisModel().getAgama_pp());
            jsonObjectPP.put("agama_lain_pp", me.getPemegangPolisModel().getAgama_lain_pp());
            jsonObjectPP.put("pendidikan_pp", me.getPemegangPolisModel().getPendidikan_pp());

            String Alamat = me.getPemegangPolisModel().getAlamat_pp();
//                    + " Kel - "+MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_pp()), me.getPemegangPolisModel().getKelurahan_pp())
//                    + " Kec - " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_pp()), me.getPemegangPolisModel().getKecamatan_pp())
//                    + " Kab - " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_pp()), me.getPemegangPolisModel().getKabupaten_pp())
//                    + " Prov - "+MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Propinsi_PP(), (me.getPemegangPolisModel().getPropinsi_pp()));

            jsonObjectPP.put("alamat_pp", Alamat);


            if (me.getPemegangPolisModel().getPropinsi_pp() == null || me.getPemegangPolisModel().getPropinsi_pp().equals("")) {

            }else {
                jsonObjectPP.put("lspr_id_pp", me.getPemegangPolisModel().getPropinsi_pp());
            }


            if (me.getPemegangPolisModel().getKabupaten_pp() == null || me.getPemegangPolisModel().getKabupaten_pp().equals("")){

            }else {
                jsonObjectPP.put("lska_id_pp", me.getPemegangPolisModel().getKabupaten_pp());
            }


            if (me.getPemegangPolisModel().getKecamatan_pp() == null || me.getPemegangPolisModel().getKecamatan_pp().equals("")){

            }else {
                jsonObjectPP.put("lskc_id_pp", me.getPemegangPolisModel().getKecamatan_pp());
            }


            if (me.getPemegangPolisModel().getKelurahan_pp() == null || me.getPemegangPolisModel().getKelurahan_pp().equals("")){

            }else {
                jsonObjectPP.put("lskl_id_pp", me.getPemegangPolisModel().getKelurahan_pp());
            }


            jsonObjectPP.put("kota_pp", me.getPemegangPolisModel().getKota_pp());
            jsonObjectPP.put("kdpos_pp", me.getPemegangPolisModel().getKdpos_pp());
            jsonObjectPP.put("telp1_pp", me.getPemegangPolisModel().getTelp1_pp());
            jsonObjectPP.put("kdtelp1_pp", me.getPemegangPolisModel().getKdtelp1_pp());
            jsonObjectPP.put("telp2_pp", me.getPemegangPolisModel().getTelp2_pp());
            jsonObjectPP.put("kdtelp2_pp", me.getPemegangPolisModel().getKdtelp2_pp());

            String Alamat_Kantor = me.getPemegangPolisModel().getAlamat_kantor_pp()
                    + " Kel - " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_kantor_pp()), me.getPemegangPolisModel().getKelurahan_kantor_pp())
                    + " Kec - " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_kantor_pp()), me.getPemegangPolisModel().getKecamatan_kantor_pp())
                    + " Kab - " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_kantor_pp()), me.getPemegangPolisModel().getKabupaten_kantor_pp())
                    + " Prov - " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Propinsi_PP(), me.getPemegangPolisModel().getPropinsi_kantor_pp());

            jsonObjectPP.put("alamat_kantor_pp", Alamat_Kantor);

            if (me.getPemegangPolisModel().getPropinsi_kantor_pp() == null || me.getPemegangPolisModel().getPropinsi_kantor_pp().equals("")) {

            }else {
                jsonObjectPP.put("lspr_id_kantor_pp", me.getPemegangPolisModel().getPropinsi_kantor_pp());
            }


            if (me.getPemegangPolisModel().getKabupaten_kantor_pp() == null || me.getPemegangPolisModel().getKabupaten_kantor_pp().equals("")) {

            }else {
                jsonObjectPP.put("lska_id_kantor_pp", me.getPemegangPolisModel().getKabupaten_kantor_pp());
            }


            if (me.getPemegangPolisModel().getKecamatan_kantor_pp() == null || me.getPemegangPolisModel().getKecamatan_kantor_pp().equals("")) {

            }else {
                jsonObjectPP.put("lskc_id_kantor_pp", me.getPemegangPolisModel().getKecamatan_kantor_pp());
            }


            if (me.getPemegangPolisModel().getKelurahan_kantor_pp() == null || me.getPemegangPolisModel().getKelurahan_kantor_pp().equals("")) {

            }else {
                jsonObjectPP.put("lskl_id_kantor_pp", me.getPemegangPolisModel().getKelurahan_kantor_pp());
            }


            jsonObjectPP.put("kota_kantor_pp", me.getPemegangPolisModel().getKota_kantor_pp());
            jsonObjectPP.put("kdpos_kantor_pp", me.getPemegangPolisModel().getKdpos_kantor_pp());
            jsonObjectPP.put("kdtelp1_kantor_pp", me.getPemegangPolisModel().getKdtelp1_kantor_pp());
            jsonObjectPP.put("telp1_kantor_pp", me.getPemegangPolisModel().getTelp1_kantor_pp());
            jsonObjectPP.put("kdtelp2_kantor_pp", me.getPemegangPolisModel().getKdtelp2_kantor_pp());
            jsonObjectPP.put("telp2_kantor_pp", me.getPemegangPolisModel().getTelp2_kantor_pp());
            jsonObjectPP.put("kdfax_kantor_pp", me.getPemegangPolisModel().getKdfax_kantor_pp());
            jsonObjectPP.put("fax_kantor_pp", me.getPemegangPolisModel().getFax_kantor_pp());

            String Alamat_Tinggal = me.getPemegangPolisModel().getAlamat_tinggal_pp()+
                    " Kel - " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_tinggal_pp()),me.getPemegangPolisModel().getKelurahan_tinggal_pp())
                    + " Kec -" + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_tinggal_pp()), me.getPemegangPolisModel().getKecamatan_tinggal_pp())
                    + " Kab - " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_tinggal_pp()), me.getPemegangPolisModel().getKabupaten_tinggal_pp())
                    + " Prov - " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Propinsi_PP(),me.getPemegangPolisModel().getPropinsi_tinggal_pp());

            jsonObjectPP.put("alamat_tinggal_pp", Alamat_Tinggal);

            if (me.getPemegangPolisModel().getPropinsi_tinggal_pp() == null || me.getPemegangPolisModel().getPropinsi_tinggal_pp().equals("")) {

            }else {
                jsonObjectPP.put("lspr_id_other_pp", me.getPemegangPolisModel().getPropinsi_tinggal_pp());
            }


            if (me.getPemegangPolisModel().getKabupaten_tinggal_pp() == null || me.getPemegangPolisModel().getKabupaten_tinggal_pp().equals("")) {

            }else {
                jsonObjectPP.put("lska_id_other_pp", me.getPemegangPolisModel().getKabupaten_tinggal_pp());
            }


            if (me.getPemegangPolisModel().getKecamatan_tinggal_pp() == null || me.getPemegangPolisModel().getKecamatan_tinggal_pp().equals("")) {

            }else {
                jsonObjectPP.put("lskc_id_other_pp", me.getPemegangPolisModel().getKecamatan_tinggal_pp());
            }


            if (me.getPemegangPolisModel().getKelurahan_tinggal_pp() == null || me.getPemegangPolisModel().getKelurahan_tinggal_pp().equals("")) {

            }else {
                jsonObjectPP.put("lskl_id_other_pp", me.getPemegangPolisModel().getKelurahan_tinggal_pp());
            }


            jsonObjectPP.put("kota_tinggal_pp", me.getPemegangPolisModel().getKota_tinggal_pp());
            jsonObjectPP.put("kdpos_tinggal_pp", me.getPemegangPolisModel().getKdpos_tinggal_pp());
            jsonObjectPP.put("kdtelp1_tinggal_pp", me.getPemegangPolisModel().getKdtelp1_tinggal_pp());
            jsonObjectPP.put("telp1_tinggal_pp", me.getPemegangPolisModel().getTelp1_tinggal_pp());
            jsonObjectPP.put("kdtelp2_tinggal_pp", me.getPemegangPolisModel().getKdtelp2_tinggal_pp());
            jsonObjectPP.put("telp2_tinggal_pp", me.getPemegangPolisModel().getTelp2_tinggal_pp());
            jsonObjectPP.put("kdfax_tinggal_pp", me.getPemegangPolisModel().getKdfax_tinggal_pp());
            jsonObjectPP.put("fax_tinggal_pp", me.getPemegangPolisModel().getFax_tinggal_pp());

            jsonObjectPP.put("tagihan_pp", me.getPemegangPolisModel().getTagihan_pp());

            jsonObjectPP.put("hp1_pp", me.getPemegangPolisModel().getHp1_pp());
            jsonObjectPP.put("hp2_pp", me.getPemegangPolisModel().getHp2_pp());
            jsonObjectPP.put("email_pp", me.getPemegangPolisModel().getEmail_pp());
            jsonObjectPP.put("klasifikasi_pekerjaan_pp", me.getPemegangPolisModel().getKlasifikasi_pekerjaan_pp());
            jsonObjectPP.put("uraian_pekerjaan_pp", me.getPemegangPolisModel().getUraian_pekerjaan_pp());
            jsonObjectPP.put("jabatan_klasifikasi_pp", me.getPemegangPolisModel().getJabatan_klasifikasi_pp());
            jsonObjectPP.put("hubungan_pp", me.getPemegangPolisModel().getHubungan_pp());
            jsonObjectPP.put("greencard_pp", me.getPemegangPolisModel().getGreencard_pp());
            jsonObjectPP.put("alias_pp", me.getPemegangPolisModel().getAlias_pp());
            jsonObjectPP.put("nm_perusahaan_pp", me.getPemegangPolisModel().getNm_perusahaan_pp());
            jsonObjectPP.put("npwp_pp", me.getPemegangPolisModel().getNpwp_pp());
            jsonObjectPP.put("tagihan_rutin_pp", me.getPemegangPolisModel().getTagihan_rutin_pp());
            jsonObjectPP.put("krm_polis_pp", me.getPemegangPolisModel().getKrm_polis_pp());
            jsonObjectPP.put("suamirt_pp", me.getPemegangPolisModel().getSuamirt_pp());
            jsonObjectPP.put("ttlsuami_rt_pp", me.getPemegangPolisModel().getTtlsuami_rt_pp());
            jsonObjectPP.put("usiasuami_rt_pp", me.getPemegangPolisModel().getUsiasuami_rt_pp());
            jsonObjectPP.put("pekerjaansuami_rt_pp", me.getPemegangPolisModel().getPekerjaansuami_rt_pp());
            jsonObjectPP.put("jabatansuami_rt_pp", me.getPemegangPolisModel().getJabatansuami_rt_pp());
            jsonObjectPP.put("perusahaansuami_rt_pp", me.getPemegangPolisModel().getPerusahaansuami_rt_pp());
            jsonObjectPP.put("bidusaha_suamirt_pp", me.getPemegangPolisModel().getBidusaha_suamirt_pp());
            jsonObjectPP.put("npwp_suamirt_pp", me.getPemegangPolisModel().getNpwp_suamirt_pp());
            jsonObjectPP.put("penghasilan_suamithn_pp", me.getPemegangPolisModel().getPenghasilan_suamithn_pp());
            jsonObjectPP.put("ayah_pp", me.getPemegangPolisModel().getAyah_pp());
            jsonObjectPP.put("ttl_ayah_pp", me.getPemegangPolisModel().getTtl_ayah_pp());
            jsonObjectPP.put("usia_ayah_pp", me.getPemegangPolisModel().getUsia_ayah_pp());
            jsonObjectPP.put("pekerjaan_ayah_pp", me.getPemegangPolisModel().getPekerjaan_ayah_pp());
            jsonObjectPP.put("jabatan_ayah_pp", me.getPemegangPolisModel().getJabatan_ayah_pp());
            jsonObjectPP.put("perusahaan_ayah_pp", me.getPemegangPolisModel().getPerusahaan_ayah_pp());
            jsonObjectPP.put("bidusaha_ayah_pp", me.getPemegangPolisModel().getBidusaha_ayah_pp());
            jsonObjectPP.put("npwp_ayah_pp", me.getPemegangPolisModel().getNpwp_ayah_pp());
            jsonObjectPP.put("penghasilan_ayah_pp", me.getPemegangPolisModel().getPenghasilan_ayah_pp());
            jsonObjectPP.put("nm_ibu_pp", me.getPemegangPolisModel().getNm_ibu_pp());
            jsonObjectPP.put("ttl_ibu_pp", me.getPemegangPolisModel().getTtl_ibu_pp());
            jsonObjectPP.put("usia_ibu_pp", me.getPemegangPolisModel().getUsia_ibu_pp());
            jsonObjectPP.put("pekerjaan_ibu_pp", me.getPemegangPolisModel().getPekerjaan_ibu_pp());
            jsonObjectPP.put("jabatan_ibu_pp", me.getPemegangPolisModel().getJabatan_ibu_pp());
            jsonObjectPP.put("perusahaan_ibu_pp", me.getPemegangPolisModel().getPerusahaan_ibu_pp());
            jsonObjectPP.put("bidusaha_ibu_pp", me.getPemegangPolisModel().getBidusaha_ibu_pp());
            jsonObjectPP.put("npwp_ibu_pp", me.getPemegangPolisModel().getNpwp_ibu_pp());
            jsonObjectPP.put("penghasilan_ibu_pp", me.getPemegangPolisModel().getPenghasilan_ibu_pp());
            jsonObjectPP.put("pendapatan_pp", me.getPemegangPolisModel().getPendapatan_pp());
            jsonObjectPP.put("hubungancp_pp", me.getPemegangPolisModel().getHubungancp_pp());
            jsonObjectPP.put("no_ciff_pp", me.getPemegangPolisModel().getNo_ciff_pp());

            JSONArray dana_pp = new JSONArray();
            if (!me.getPemegangPolisModel().getD_gaji_pp().equals("")) {
                JSONObject dana = new JSONObject();
                dana.put("dana", me.getPemegangPolisModel().getD_gaji_pp());
                dana_pp.put(dana);
            }
            if (!me.getPemegangPolisModel().getD_tabungan_pp().equals("")) {
                JSONObject dana = new JSONObject();
                dana.put("dana", me.getPemegangPolisModel().getD_tabungan_pp());
                dana_pp.put(dana);
            }
            if (!me.getPemegangPolisModel().getD_warisan_pp().equals("")) {
                JSONObject dana = new JSONObject();
                dana.put("dana", me.getPemegangPolisModel().getD_warisan_pp());
                dana_pp.put(dana);
            }
            if (!me.getPemegangPolisModel().getD_hibah_pp().equals("")) {
                JSONObject dana = new JSONObject();
                dana.put("dana", me.getPemegangPolisModel().getD_hibah_pp());
                dana_pp.put(dana);
            }
            if (!me.getPemegangPolisModel().getD_lainnya_pp().equals("")) {
                JSONObject dana = new JSONObject();
                dana.put("dana", me.getPemegangPolisModel().getD_lainnya_pp() + "~" +
                        me.getPemegangPolisModel().getEdit_d_lainnya_pp());
                dana_pp.put(dana);
            }

            jsonObjectPP.put("sumber_dana_pp", dana_pp);

            JSONArray tujuan_pp = new JSONArray();
            if (!me.getPemegangPolisModel().getT_proteksi_pp().equals("")) {
                JSONObject tujuan = new JSONObject();
                tujuan.put("tujuan", me.getPemegangPolisModel().getT_proteksi_pp());
                tujuan_pp.put(tujuan);
            }
            if (!me.getPemegangPolisModel().getT_inves_pp().equals("")) {
                JSONObject tujuan = new JSONObject();
                tujuan.put("tujuan", me.getPemegangPolisModel().getT_inves_pp());
                tujuan_pp.put(tujuan);
            }
            if (!me.getPemegangPolisModel().getT_lainnya_pp().equals("")) {
                JSONObject tujuan = new JSONObject();
                tujuan.put("tujuan", me.getPemegangPolisModel().getT_lainnya_pp() + "~" +
                        me.getPemegangPolisModel().getEdit_t_lainnya());
                tujuan_pp.put(tujuan);
            }

            jsonObjectPP.put("tujuan_pp", tujuan_pp);

////Tertanggung
            JSONObject jsonObjectTT = new JSONObject();

            jsonObjectTT.put("nama_tt", me.getTertanggungModel().getNama_tt());
            jsonObjectTT.put("gelar_tt", me.getTertanggungModel().getGelar_tt());
            jsonObjectTT.put("nama_ibu_tt", me.getTertanggungModel().getNama_ibu_tt());
            jsonObjectTT.put("bukti_identitas_tt", me.getTertanggungModel().getBukti_identitas_tt());
            jsonObjectTT.put("no_identitas_tt", me.getTertanggungModel().getNo_identitas_tt());
            jsonObjectTT.put("tgl_berlaku_tt", me.getTertanggungModel().getTgl_berlaku_tt());
            jsonObjectTT.put("warga_negara_tt", me.getTertanggungModel().getWarga_negara_tt());
            jsonObjectTT.put("tempat_tt", me.getTertanggungModel().getTempat_tt());
            jsonObjectTT.put("ttl_tt", me.getTertanggungModel().getTtl_tt());
            jsonObjectTT.put("usia_tt", me.getTertanggungModel().getUsia_tt().replace(" ", ""));
            jsonObjectTT.put("jekel_tt", me.getTertanggungModel().getJekel_tt());
            jsonObjectTT.put("status_tt", me.getTertanggungModel().getStatus_tt());
            jsonObjectTT.put("agama_tt", me.getTertanggungModel().getAgama_tt());
            jsonObjectTT.put("agama_lain_tt", me.getTertanggungModel().getAgama_lain_tt());
            jsonObjectTT.put("pendidikan_tt", me.getTertanggungModel().getPendidikan_tt());

            String Alamat_tt = me.getTertanggungModel().getAlamat_tt();
//                    + " Kel - "+MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_tt()), me.getTertanggungModel().getKelurahan_tt())
//                    + " Kec -" + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_tt()), me.getTertanggungModel().getKecamatan_tt())
//                    + " Kab - " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_tt()), me.getTertanggungModel().getKabupaten_tt())
//                    + " Prov - "+MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Propinsi_PP(), (me.getTertanggungModel().getPropinsi_tt()));

            jsonObjectTT.put("alamat_tt", Alamat_tt);

            if (me.getTertanggungModel().getPropinsi_tt() == null || me.getTertanggungModel().getPropinsi_tt().equals("")){

            }else {
                jsonObjectTT.put("lspr_id_tt", me.getTertanggungModel().getPropinsi_tt());
            }


            if (me.getTertanggungModel().getKabupaten_tt() == null || me.getTertanggungModel().getKabupaten_tt().equals("")){

            }else {
                jsonObjectTT.put("lska_id_tt", me.getTertanggungModel().getKabupaten_tt());
            }


            if (me.getTertanggungModel().getKecamatan_tt() == null || me.getTertanggungModel().getKecamatan_tt().equals("")){

            }else {
                jsonObjectTT.put("lskc_id_tt", me.getTertanggungModel().getKecamatan_tt());
            }


            if (me.getTertanggungModel().getKelurahan_tt() == null || me.getTertanggungModel().getKelurahan_tt().equals("")){

            }else {
                jsonObjectTT.put("lskl_id_tt", me.getTertanggungModel().getKelurahan_tt());
            }


            jsonObjectTT.put("kota_tt", me.getTertanggungModel().getKota_tt());
            jsonObjectTT.put("kdpos_tt", me.getTertanggungModel().getKdpos_tt());
            jsonObjectTT.put("kdtelp1_tt", me.getTertanggungModel().getKdtelp1_tt());
            jsonObjectTT.put("telp1_tt", me.getTertanggungModel().getTelp1_tt());
            jsonObjectTT.put("kdtelp2_tt", me.getTertanggungModel().getKdtelp2_tt());
            jsonObjectTT.put("telp2_tt", me.getTertanggungModel().getTelp2_tt());

            String Alamat_Kantor_tt = me.getTertanggungModel().getAlamat_kantor_tt()
                    + " Kel - "+MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_kantor_tt()), me.getTertanggungModel().getKelurahan_kantor_tt())
                    + " Kec -" + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_kantor_tt()), me.getTertanggungModel().getKecamatan_kantor_tt())
                    + " Kab - " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_kantor_tt()), me.getTertanggungModel().getKabupaten_kantor_tt())
                    + " Prov - "+MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Propinsi_PP(), (me.getTertanggungModel().getPropinsi_kantor_tt()));

            jsonObjectTT.put("alamat_kantor_tt", Alamat_Kantor_tt);

            if (me.getTertanggungModel().getPropinsi_kantor_tt() == null || me.getTertanggungModel().getPropinsi_kantor_tt().equals("")){

            }else {
                jsonObjectTT.put("lspr_id_kantor_tt", me.getTertanggungModel().getPropinsi_kantor_tt());
            }


            if (me.getTertanggungModel().getKabupaten_kantor_tt() == null || me.getTertanggungModel().getKabupaten_kantor_tt().equals("")){

            }else {
                jsonObjectTT.put("lska_id_kantor_tt", me.getTertanggungModel().getKabupaten_kantor_tt());
            }


            if (me.getTertanggungModel().getKecamatan_kantor_tt() == null || me.getTertanggungModel().getKecamatan_kantor_tt().equals("")){

            }else {
                jsonObjectTT.put("lskc_id_kantor_tt", me.getTertanggungModel().getKecamatan_kantor_tt());
            }


            if (me.getTertanggungModel().getKelurahan_kantor_tt() == null || me.getTertanggungModel().getKelurahan_kantor_tt().equals("")){

            }else {
                jsonObjectTT.put("lskl_id_kantor_tt", me.getTertanggungModel().getKelurahan_kantor_tt());
            }

            jsonObjectTT.put("kota_kantor_tt", me.getTertanggungModel().getKota_kantor_tt());
            jsonObjectTT.put("kdpos_kantor_tt", me.getTertanggungModel().getKdpos_kantor_tt());
            jsonObjectTT.put("kdtelp1_kantor_tt", me.getTertanggungModel().getKdtelp1_kantor_tt());
            jsonObjectTT.put("telp1_kantor_tt", me.getTertanggungModel().getTelp1_kantor_tt());
            jsonObjectTT.put("kdtelp2_kantor_tt", me.getTertanggungModel().getKdtelp2_kantor_tt());
            jsonObjectTT.put("telp2_kantor_tt", me.getTertanggungModel().getTelp2_kantor_tt());
            jsonObjectTT.put("fax_kantor_tt", me.getTertanggungModel().getFax_kantor_tt());

            String Alamat_Tinggal_tt = me.getTertanggungModel().getAlamat_tinggal_tt()
                    + " Kel - "+MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kelurahan_PP(me.getTertanggungModel().getAlamat_tinggal_tt()), me.getTertanggungModel().getKelurahan_tinggal_tt())
                    + " Kec -" + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_tinggal_tt()), me.getTertanggungModel().getKecamatan_tinggal_tt())
                    + " Kab - " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_tinggal_tt()), me.getTertanggungModel().getKabupaten_tinggal_tt())
                    + " Prov - "+MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Propinsi_PP(), (me.getTertanggungModel().getPropinsi_tinggal_tt()));

            jsonObjectTT.put("alamat_tinggal_tt", Alamat_Tinggal_tt);

            if (me.getTertanggungModel().getPropinsi_tinggal_tt() == null || me.getTertanggungModel().getPropinsi_tinggal_tt().equals("")){

            }else {
                jsonObjectTT.put("lspr_id_other_tt", me.getTertanggungModel().getPropinsi_tinggal_tt());
            }


            if (me.getTertanggungModel().getKabupaten_tinggal_tt() == null || me.getTertanggungModel().getKabupaten_tinggal_tt().equals("")){

            }else {
                jsonObjectTT.put("lska_id_other_tt", me.getTertanggungModel().getKabupaten_tinggal_tt());
            }


            if (me.getTertanggungModel().getKecamatan_tinggal_tt() == null || me.getTertanggungModel().getKecamatan_tinggal_tt().equals("")){

            }else {
                jsonObjectTT.put("lskc_id_other_tt", me.getTertanggungModel().getKecamatan_tinggal_tt());
            }


            if (me.getTertanggungModel().getKelurahan_tinggal_tt() == null || me.getTertanggungModel().getKelurahan_tinggal_tt().equals("")){

            }else {
                jsonObjectTT.put("lskl_id_other_tt", me.getTertanggungModel().getKelurahan_tinggal_tt());
            }

            jsonObjectTT.put("kota_tinggal_tt", me.getTertanggungModel().getKota_tinggal_tt());
            jsonObjectTT.put("kdpos_tinggal_tt", me.getTertanggungModel().getKdpos_tinggal_tt());
            jsonObjectTT.put("kdtelp1_tinggal_tt", me.getTertanggungModel().getKdtelp1_tinggal_tt());
            jsonObjectTT.put("telp1_tinggal_tt", me.getTertanggungModel().getTelp1_tinggal_tt());
            jsonObjectTT.put("kdtelp2_tinggal_tt", me.getTertanggungModel().getKdtelp2_tinggal_tt());
            jsonObjectTT.put("telp2_tinggal_tt", me.getTertanggungModel().getTelp2_tinggal_tt());
            jsonObjectTT.put("kdfax_tinggal_tt", me.getTertanggungModel().getKdfax_tinggal_tt());
            jsonObjectTT.put("fax_tinggal_tt", me.getTertanggungModel().getFax_tinggal_tt());
            jsonObjectTT.put("hp1_tt", me.getTertanggungModel().getHp1_tt());
            jsonObjectTT.put("hp2_tt", me.getTertanggungModel().getHp2_tt());
            jsonObjectTT.put("email_tt", me.getTertanggungModel().getEmail_tt());
            jsonObjectTT.put("penghasilan_thn_tt", me.getTertanggungModel().getPenghasilan_thn_tt());
            jsonObjectTT.put("klasifikasi_pekerjaan_tt", me.getTertanggungModel().getKlasifikasi_pekerjaan_tt());
            jsonObjectTT.put("uraian_pekerjaan_tt", me.getTertanggungModel().getUraian_pekerjaan_tt());
            jsonObjectTT.put("jabatan_klasifikasi_tt", me.getTertanggungModel().getJabatan_klasifikasi_tt());
            jsonObjectTT.put("greencard_tt", me.getTertanggungModel().getGreencard_tt());
            jsonObjectTT.put("alias_tt", me.getTertanggungModel().getAlias_tt());
            jsonObjectTT.put("nm_perusahaan_tt", me.getTertanggungModel().getNm_perusahaan_tt());
            jsonObjectTT.put("npwp_tt", me.getTertanggungModel().getNpwp_tt());
            JSONArray dana_tt = new JSONArray();
            if (!me.getTertanggungModel().getDana_gaji_tt().equals("")) {
                JSONObject dana = new JSONObject();
                dana.put("dana", me.getTertanggungModel().getDana_gaji_tt());
                dana_tt.put(dana);
            }
            if (!me.getTertanggungModel().getDana_tabungan_tt().equals("")) {
                JSONObject dana = new JSONObject();
                dana.put("dana", me.getTertanggungModel().getDana_tabungan_tt());
                dana_tt.put(dana);
            }
            if (!me.getTertanggungModel().getDana_warisan_tt().equals("")) {
                JSONObject dana = new JSONObject();
                dana.put("dana", me.getTertanggungModel().getDana_warisan_tt());
                dana_tt.put(dana);
            }
            if (!me.getTertanggungModel().getDana_hibah_tt().equals("")) {
                JSONObject dana = new JSONObject();
                dana.put("dana", me.getTertanggungModel().getDana_hibah_tt());
                dana_tt.put(dana);
            }
            if (!me.getTertanggungModel().getDana_lainnya_tt().equals("")) {
                JSONObject dana = new JSONObject();
                dana.put("dana", me.getTertanggungModel().getDana_lainnya_tt() + "~" +
                        me.getTertanggungModel().getDana_lainnya_tt());
                dana_tt.put(dana);
            }

            jsonObjectTT.put("sumber_dana_tt", dana_tt);

            JSONArray tujuan_tt = new JSONArray();
            if (!me.getTertanggungModel().getTujuan_proteksi_tt().equals("")) {
                JSONObject tujuan = new JSONObject();
                tujuan.put("tujuan", me.getTertanggungModel().getTujuan_proteksi_tt());
                tujuan_tt.put(tujuan);
            }
            if (!me.getTertanggungModel().getTujuan_inves_tt().equals("")) {
                JSONObject tujuan = new JSONObject();
                tujuan.put("tujuan", me.getTertanggungModel().getTujuan_inves_tt());
                tujuan_tt.put(tujuan);
            }
            if (!me.getTertanggungModel().getTujuan_lainnya_tt().equals("")) {
                JSONObject tujuan = new JSONObject();
                tujuan.put("tujuan", me.getTertanggungModel().getTujuan_lainnya_tt() + "~" +
                        me.getTertanggungModel().getTujuan_lainnya_tt());
                tujuan_tt.put(tujuan);
            }
            jsonObjectTT.put("tujuan_tt", tujuan_tt);

            //Usulan Asuransi
            JSONObject jsonObjectUA = new JSONObject();
            jsonObjectUA.put("jenis_produk_ua", me.getUsulanAsuransiModel().getJenis_produk_ua());
            jsonObjectUA.put("kd_produk_ua", me.getUsulanAsuransiModel().getKd_produk_ua());
            jsonObjectUA.put("sub_produk_ua", me.getUsulanAsuransiModel().getSub_produk_ua());
            //	jsonObjectUA.put("produk_ua", String.valueOf(me.getModelUsulanAsuransi().getProduk_ua()+"~X"+me.getModelUsulanAsuransi().getNm_produk_ua()));
            jsonObjectUA.put("klas_ua", me.getUsulanAsuransiModel().getKlas_ua());
            jsonObjectUA.put("paket_ua", me.getUsulanAsuransiModel().getPaket_ua());
            //	jsonObjectUA.put("stable_khusus_ua", me.getModelUsulanAsuransi().getKhusus_ua());
            //	jsonObjectUA.put("stable_special_ua", me.getModelUsulanAsuransi().getCheck_spesial_ua());
            //	jsonObjectUA.put("stable_karyawan_ua", me.getModelUsulanAsuransi().getCheck_krywan_ua());
            //	jsonObjectUA.put("masa_pembayaran_ua", me.getModelUsulanAsuransi().getEdit_masa_bayar_ua());
            jsonObjectUA.put("ekasehat_plan_ua", me.getUsulanAsuransiModel().getEkasehat_plan_ua());
            //	jsonObjectUA.put("ekasehat_gmit_ua", me.getModelUsulanAsuransi().getCheck_gmit_ua());
            jsonObjectUA.put("masa_pertanggungan_ua", me.getUsulanAsuransiModel().getMasa_pertanggungan_ua());
            jsonObjectUA.put("cuti_premi_ua", me.getUsulanAsuransiModel().getCuti_premi_ua());
            jsonObjectUA.put("kurs_up_ua", me.getUsulanAsuransiModel().getKurs_up_ua());
            jsonObjectUA.put("up_ua", me.getUsulanAsuransiModel().getUp_ua());
            jsonObjectUA.put("unitlink_opsipremi_ua", me.getUsulanAsuransiModel().getUnitlink_opsipremi_ua());
            jsonObjectUA.put("unitlink_kurs_ua", me.getUsulanAsuransiModel().getUnitlink_kurs_ua());
            jsonObjectUA.put("unitlink_premistandard_ua", me.getUsulanAsuransiModel().getUnitlink_premistandard_ua());
            jsonObjectUA.put("unitlink_kombinasi_ua", me.getUsulanAsuransiModel().getUnitlink_kombinasi_ua());
            jsonObjectUA.put("unitlink_kurs_total_ua", me.getUsulanAsuransiModel().getUnitlink_kurs_total_ua());
            jsonObjectUA.put("unitlink_total_ua", me.getUsulanAsuransiModel().getUnitlink_total_ua());
            jsonObjectUA.put("carabayar_ua", me.getUsulanAsuransiModel().getCarabayar_ua());
            jsonObjectUA.put("bentukbayar_ua", me.getUsulanAsuransiModel().getBentukbayar_ua());
            jsonObjectUA.put("awalpertanggungan_ua", me.getUsulanAsuransiModel().getAwalpertanggungan_ua());
            jsonObjectUA.put("akhirpertanggungan_ua", me.getUsulanAsuransiModel().getAkhirpertanggungan_ua());
            jsonObjectUA.put("masa_pembayaran_ua", me.getUsulanAsuransiModel().getMasa_pembayaran_ua());

            JSONArray ArrayRiderUA = new JSONArray();
            List<ModelAddRiderUA> ListRiderUA = me.getListRiderUA();
            for (int i = 0; i < ListRiderUA.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("kode_produk_rider", ListRiderUA.get(i).getKode_produk_rider());
                jsonObject.put("kode_subproduk_rider", ListRiderUA.get(i).getKode_subproduk_rider());
                jsonObject.put("tertanggung_rider", ListRiderUA.get(i).getTertanggung_rider());
                jsonObject.put("rate_rider", ListRiderUA.get(i).getRate_rider());
                jsonObject.put("unit_rider", ListRiderUA.get(i).getUnit_rider());
                jsonObject.put("klas_rider", ListRiderUA.get(i).getKlas_rider());
                jsonObject.put("persentase_rider", ListRiderUA.get(i).getPersentase_rider());
                jsonObject.put("up_rider", ListRiderUA.get(i).getUp_rider());
                jsonObject.put("premi_rider", ListRiderUA.get(i).getPremi_rider());
                jsonObject.put("tglmulai_rider", ListRiderUA.get(i).getTglmulai_rider());
                jsonObject.put("tglakhir_rider", ListRiderUA.get(i).getTglakhir_rider());
                jsonObject.put("masa_rider", ListRiderUA.get(i).getMasa_rider());
                jsonObject.put("akhirbayar_rider", ListRiderUA.get(i).getAkhirbayar_rider());
                ArrayRiderUA.put(jsonObject);
            }
            jsonObjectUA.put("rider_ua", ArrayRiderUA);

            JSONArray ArrayAskesUA = new JSONArray();
            List<ModelAskesUA> ListAskesUA = me.getListASkesUA();
            for (int i = 0; i < ListAskesUA.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("peserta_askes", ListAskesUA.get(i).getPeserta_askes());
                jsonObject.put("jekel_askes", ListAskesUA.get(i).getJekel_askes());
                jsonObject.put("ttl_askes", ListAskesUA.get(i).getTtl_askes());
                jsonObject.put("usia_askes", ListAskesUA.get(i).getUsia_askes());
                jsonObject.put("hubungan_askes", ListAskesUA.get(i).getHubungan_askes());
                jsonObject.put("produk_askes", ListAskesUA.get(i).getProduk_askes());
                jsonObject.put("rider_askes", ListAskesUA.get(i).getRider_askes());
                jsonObject.put("kode_produk_rider", ListAskesUA.get(i).getKode_produk_rider());
                jsonObject.put("kode_subproduk_rider", ListAskesUA.get(i).getKode_subproduk_rider());
                jsonObject.put("tinggi_askes", ListAskesUA.get(i).getTinggi_askes());
                jsonObject.put("berat_askes", ListAskesUA.get(i).getBerat_askes());
                jsonObject.put("warganegara_askes", ListAskesUA.get(i).getWarganegara_askes());
                jsonObject.put("pekerjaan_askes", ListAskesUA.get(i).getPekerjaan_askes());
                ArrayAskesUA.put(jsonObject);
            }
            jsonObjectUA.put("askes_ua", ArrayAskesUA);

            // Detail Investasi
            JSONObject jsonObjectDI = new JSONObject();
            jsonObjectDI.put("premi_pokok_di", me.getDetilInvestasiModel().getPremi_pokok_di());
            jsonObjectDI.put("premi_berkala_di", me.getDetilInvestasiModel().getPremi_berkala_di());
            jsonObjectDI.put("premitopup_berkala_di", me.getDetilInvestasiModel().getPremitopup_berkala_di());
            jsonObjectDI.put("premi_tunggal_di", me.getDetilInvestasiModel().getPremi_tunggal_di());
            jsonObjectDI.put("premitopup_tunggal_di", me.getDetilInvestasiModel().getPremitopup_tunggal_di());
            jsonObjectDI.put("premi_tambahan_di", me.getDetilInvestasiModel().getPremi_tambahan_di());
            jsonObjectDI.put("jumlah_di", me.getDetilInvestasiModel().getJumlah_di());
            jsonObjectDI.put("invest_bayar_premi_di", me.getDetilInvestasiModel().getInvest_bayar_premi_di());
            jsonObjectDI.put("invest_bank_di", me.getDetilInvestasiModel().getInvest_bank_di());
            jsonObjectDI.put("invest_cabang_di", me.getDetilInvestasiModel().getInvest_cabang_di());
            jsonObjectDI.put("invest_kota_di", me.getDetilInvestasiModel().getInvest_kota_di());
            jsonObjectDI.put("invest_jnstab_di", me.getDetilInvestasiModel().getInvest_jnstab_di());
            jsonObjectDI.put("invest_jnsnasabah_di", me.getDetilInvestasiModel().getInvest_jnsnasabah_di());
            jsonObjectDI.put("invest_norek_di", me.getDetilInvestasiModel().getInvest_norek_di());
            jsonObjectDI.put("invest_nama_di", me.getDetilInvestasiModel().getInvest_nama_di());
            jsonObjectDI.put("invest_kurs_di", me.getDetilInvestasiModel().getInvest_kurs_di());
            jsonObjectDI.put("invest_berikuasa_di", me.getDetilInvestasiModel().getInvest_berikuasa_di());
            jsonObjectDI.put("invest_tglkuasa_di", me.getDetilInvestasiModel().getInvest_tglkuasa_di());
            jsonObjectDI.put("invest_keterangan_di", me.getDetilInvestasiModel().getInvest_keterangan_di());
            jsonObjectDI.put("autodbt_bank_di", me.getDetilInvestasiModel().getAutodbt_bank_di());
            jsonObjectDI.put("autodbt_jnstab_di", me.getDetilInvestasiModel().getAutodbt_jnstab_di());
            jsonObjectDI.put("autodbt_kurs_di", me.getDetilInvestasiModel().getAutodbt_kurs_di());
            jsonObjectDI.put("autodbt_norek_di", me.getDetilInvestasiModel().getAutodbt_norek_di());
            jsonObjectDI.put("autodbt_nama_di", me.getDetilInvestasiModel().getAutodbt_nama_di());
            jsonObjectDI.put("autodbt_tgldebet_di", me.getDetilInvestasiModel().getAutodbt_tgldebet_di());
            jsonObjectDI.put("autodbt_tglvalid_di", me.getDetilInvestasiModel().getAutodbt_tglvalid_di());
            jsonObjectDI.put("autodbt_aktif_di", me.getDetilInvestasiModel().getAutodbt_aktif_di());
            jsonObjectDI.put("autodbt_premipertama_di", me.getDetilInvestasiModel().getAutodbt_premipertama_di());
            jsonObjectDI.put("autodbt_no_simascard_di", me.getDetilInvestasiModel().getAutodbt_no_simascard_di());
            jsonObjectDI.put("danainvest_alokasi_di", me.getDetilInvestasiModel().getDanainvest_alokasi_di());
            //   	jsonObjectDI.put("alokasi_jns_dana_di",  me.getModelDetailInvestasi().getEdit_jenis_dana_di());

            //  jsonObjectDI.put("dana_alokasi", me.getModelDetailInvestasi().getEdit_jenis_dana_di());
            JSONArray ArrayBiaya = new JSONArray();
            {
                JSONObject jsonbiaya = new JSONObject();
                jsonbiaya.put("id", 1);
                jsonbiaya.put("jumlah", me.getDetilInvestasiModel().getJumlah1_di());
                jsonbiaya.put("persen", me.getDetilInvestasiModel().getPersen1_di());
                ArrayBiaya.put(jsonbiaya);
            }
            {
                JSONObject jsonbiaya = new JSONObject();
                jsonbiaya.put("id", 2);
                jsonbiaya.put("jumlah", me.getDetilInvestasiModel().getJumlah2_di());
                jsonbiaya.put("persen", me.getDetilInvestasiModel().getPersen2_di());
                ArrayBiaya.put(jsonbiaya);
            }
            {
                JSONObject jsonbiaya = new JSONObject();
                jsonbiaya.put("id", 3);
                jsonbiaya.put("jumlah", me.getDetilInvestasiModel().getJumlah3_di());
                jsonbiaya.put("persen", me.getDetilInvestasiModel().getPersen3_di());
                ArrayBiaya.put(jsonbiaya);
            }
            jsonObjectDI.put("biaya_di", ArrayBiaya);
            //   	jsonObjectDI.put("pa_jumlah4_di", me.getModelDetailInvestasi().getEtjumlah4_di());
            //   	jsonObjectDI.put("pa_persen4_di", me.getModelDetailInvestasi().getEtpersen4_di());
            //   	jsonObjectDI.put("term_jumlah5_di", me.getModelDetailInvestasi().getEtjumlah5_di());
            //   	jsonObjectDI.put("term_persen5_di", me.getModelDetailInvestasi().getEtpersen5_di());
            //// powersave
            //   	jsonObjectDI.put("power_waktu_di", me.getModelDetailInvestasi().getJangka_waktu_di());
            //   	jsonObjectDI.put("power_jatuh_tempo_di", me.getModelDetailInvestasi().getJatuh_tempo_di());
            //   	jsonObjectDI.put("power_jns_bunga_di", me.getModelDetailInvestasi().getJenis_bunga_di());
            //   	jsonObjectDI.put("power_bunga_di", me.getModelDetailInvestasi().getEdit_bunga_detil_di());
            //   	jsonObjectDI.put("power_jml_invest_di", me.getModelDetailInvestasi().getJml_invest_di());
            //   	jsonObjectDI.put("power_jml_bunga_di",  me.getModelDetailInvestasi().getEdit_jml_bunga_di());
            //   	jsonObjectDI.put("power_jns_rol_di", me.getModelDetailInvestasi().getJns_rollover_di());
            //   	jsonObjectDI.put("power_memo_di", me.getModelDetailInvestasi().getEdit_memo_di());
            //   	jsonObjectDI.put("power_sts_karyawan", me.getModelDetailInvestasi().getSts_kryawan_di());
            //   	jsonObjectDI.put("power_fee_income_di", me.getModelDetailInvestasi().getEdit_fee_income_di());
            //   	jsonObjectDI.put("power_tglnab_di", me.getModelDetailInvestasi().getTtl_nab_di());
            //   	jsonObjectDI.put("power_jml_bunga2_di", me.getModelDetailInvestasi().getEdit_jml_bunga2_di());
            //   	jsonObjectDI.put("power_begdate_di", me.getModelDetailInvestasi().getBegdate_di());
            //   	jsonObjectDI.put("power_persenrate_bp_di", me.getModelDetailInvestasi().getEdit_percent_di());
            //   	jsonObjectDI.put("power_penarikan_bunga_di", me.getModelDetailInvestasi().getPnrikan_bunga_di());
            // 	jsonObjectDI.put("power_break_di", me.getModelDetailInvestasi().getCheck_break_di());
            // 	jsonObjectDI.put("power_telemarketing_di", me.getModelDetailInvestasi().getCheck_tele_di());
            //   	jsonObjectDI.put("power_cr_bayarrider_di", me.getModelDetailInvestasi().getCr_bayar_di());
            //   	jsonObjectDI.put("power_cr_bayarrider2_di", me.getModelDetailInvestasi().getCara_bayar_di2());
            //   	jsonObjectDI.put("simas_bunga_di", me.getModelDetailInvestasi().getBunga_powersave_mnfaatblnan1_di());
            //   	jsonObjectDI.put("simas_bunga2_di", me.getModelDetailInvestasi().getBunga_powersave_mnfaatblnan2_di());
            //   	jsonObjectDI.put("detil_bonus_tahapan_di", me.getModelDetailInvestasi().getDetil_bonus_tahapan_di());
            //   	jsonObjectDI.put("jumlah_manfaat_ditunjuk_di",  me.getModelDetailInvestasi().getEdit_jmlh_mnfaat_di());


            JSONArray ArrayDanaInvest = new JSONArray();
            List<ModelJnsDanaDI> ListJnsDanaDI = me.getListDanaDI();
            if (me.getUsulanAsuransiModel().getKd_produk_ua() != 208 && me.getUsulanAsuransiModel().getKd_produk_ua() != 219
                    && me.getUsulanAsuransiModel().getKd_produk_ua() != 212) {
                for (int i = 0; i < ListJnsDanaDI.size(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    if (ListJnsDanaDI.get(i).getJenis_dana() == null || ListJnsDanaDI.get(i).getJenis_dana().equals(null)) {
                        jsonObject.put("jenis_dana", "00");
                    } else {
                        jsonObject.put("jenis_dana", ListJnsDanaDI.get(i).getJenis_dana());
                    }
                    jsonObject.put("jumlah_alokasi", ListJnsDanaDI.get(i).getJumlah_alokasi());
                    jsonObject.put("persen_alokasi", ListJnsDanaDI.get(i).getPersen_alokasi());
                    ArrayDanaInvest.put(jsonObject);
                }
            }

            jsonObjectDI.put("jns_dana_invest_di", ArrayDanaInvest);
            JSONArray ArrayDataDitunjuk = new JSONArray();
            List<ModelDataDitunjukDI> ListDataDItunjukDI = me.getListManfaatDI();
            for (int i = 0; i < ListDataDItunjukDI.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("nama", ListDataDItunjukDI.get(i).getNama());
                jsonObject.put("manfaat", ListDataDItunjukDI.get(i).getManfaat());
                jsonObject.put("jekel", ListDataDItunjukDI.get(i).getJekel());
                jsonObject.put("ttl", ListDataDItunjukDI.get(i).getTtl());
                jsonObject.put("hub_dgcalon_tt", ListDataDItunjukDI.get(i).getHub_dgcalon_tt());
                jsonObject.put("warganegara", ListDataDItunjukDI.get(i).getWarganegara());
                ArrayDataDitunjuk.put(jsonObject);
            }
            jsonObjectDI.put("penerima_manfaat_di", ArrayDataDitunjuk);
            System.out.println(jsonObjectDI);

            //Detail Agen
            JSONObject jsonObjectDA = new JSONObject();
            jsonObjectDA.put("lca_id", me.getDetilAgenModel().getLca_id());
            jsonObjectDA.put("lwk_id", me.getDetilAgenModel().getLwk_id());
            jsonObjectDA.put("lsrg_id", me.getDetilAgenModel().getLsrg_id());
            jsonObjectDA.put("tgl_spaj_da", me.getDetilAgenModel().getTgl_spaj_da());
            jsonObjectDA.put("nm_regional_da", me.getDetilAgenModel().getNm_regional_da());
            jsonObjectDA.put("kd_regional_da", me.getDetilAgenModel().getKd_regional_da());
            jsonObjectDA.put("kd_leader_da", me.getDetilAgenModel().getKd_leader_da());
            jsonObjectDA.put("nmlead_pnutup_da", me.getDetilAgenModel().getNmlead_pnutup_da());
            jsonObjectDA.put("kd_penutup_da", me.getDetilAgenModel().getKd_penutup_da());
            jsonObjectDA.put("nm_pnutup_da", me.getDetilAgenModel().getNm_pnutup_da());
            jsonObjectDA.put("check_kd_ao_da", me.getDetilAgenModel().getCheck_kd_ao_da());
            jsonObjectDA.put("kd_ao_da", me.getDetilAgenModel().getKd_ao_da());
            jsonObjectDA.put("check_kd_pribadi_da", me.getDetilAgenModel().getCheck_kd_pribadi_da());
            jsonObjectDA.put("kd_pnagihan_da", me.getDetilAgenModel().getKd_pnagihan_da());
            jsonObjectDA.put("nm_Regional_Penagihan_da", me.getDetilAgenModel().getNm_Regional_Penagihan_da());
            jsonObjectDA.put("check_broker_da", me.getDetilAgenModel().getCheck_broker_da());
            jsonObjectDA.put("broker_da", me.getDetilAgenModel().getBroker_da());
            jsonObjectDA.put("nama_broker_da", me.getDetilAgenModel().getNama_broker_da());
            jsonObjectDA.put("bank_da", me.getDetilAgenModel().getBank_da());
            jsonObjectDA.put("norek_da", me.getDetilAgenModel().getNorek_da());
            if (me.getDetilAgenModel().getJENIS_LOGIN_BC()==2 && me.getDetilInvestasiModel().getInvest_bank_di()==224){
                jsonObjectDA.put("jn_bank_da", 16);
            }else {
                jsonObjectDA.put("jn_bank_da", me.getDetilAgenModel().getJENIS_LOGIN_BC());
            }
            //   	jsonObjectDA.put("ajs_nik_da", me.getModelDetailAgen().getNik_da());
            //   	jsonObjectDA.put("ajs_nm_karyawan_da", me.getModelDetailAgen().getNm_karyawan_da());
            //   	jsonObjectDA.put("ajs_cabang_da", me.getModelDetailAgen().getCabang_da());
            //   	jsonObjectDA.put("ajs_dept_da", me.getModelDetailAgen().getDept_da());
            //   	if(!(me.getModelDetailAgen().getPremi_ke_da().equals(""))){
            //   	jsonObjectDA.put("ajs_premi_ke_da", me.getModelDetailAgen().getPremi_ke_da());
            //   	}
            //   	jsonObjectDA.put("ajs_potongan_da", me.getModelDetailAgen().getPotongan_da());
            //   	jsonObjectDA.put("ajs_tgl_proses_da", me.getModelDetailAgen().getTgl_proses_da());
            //   	jsonObjectDA.put("worksite_status_da", me.getModelDetailAgen().getStatus_da());
            //   	jsonObjectDA.put("worksite_nik_kryawan_da", me.getModelDetailAgen().getNik_kryawan3_da());
            //   	jsonObjectDA.put("khusus_mnc_da", me.getModelDetailAgen().getKhusus_mnc_da());
            jsonObjectDA.put("pnytaan_setuju_da", me.getDetilAgenModel().getPnytaan_setuju_da());
//            if (KODE_REGIONAL.equals("37M105")){
//                jsonObjectDA.put("id_refferal_da", "");
//                jsonObjectDA.put("nm_refferal_da", "");
//                jsonObjectDA.put("id_sponsor_da", me.getDetilAgenModel().getId_sponsor_da());
//                jsonObjectDA.put("id_penempatan_da", me.getDetilAgenModel().getId_penempatan_da()); // untuk sponsor agen
//            }else {
            jsonObjectDA.put("id_refferal_da", me.getDetilAgenModel().getId_refferal_da());
            jsonObjectDA.put("nm_refferal_da", me.getDetilAgenModel().getNm_refferal_da());
            jsonObjectDA.put("cab_transaksi_da", me.getDetilAgenModel().getId_cabang_da());
            jsonObjectDA.put("id_sponsor_da", me.getDetilAgenModel().getId_sponsor_da()); // untuk sponsor agen
            jsonObjectDA.put("id_penempatan_da", me.getDetilAgenModel().getId_penempatan_da());
            jsonObjectDA.put("id_member_da", me.getDetilAgenModel().getId_member_da());
//            }
            jsonObjectDA.put("email_da", me.getDetilAgenModel().getEmail_da());

            //quisioner
            JSONObject jsonObjectKU = new JSONObject();
            JSONArray ArrayKuis = new JSONArray();
            //	List<ModelAddQuisioner>listQuisioner=me.getModelQuisioner().getListQuisioner();
            //	for (int i=0; i<listQuisioner.size();i++) {
            //   		JSONObject jsonObject= new JSONObject();
            //   		jsonObject.put("peserta", listQuisioner.get(i).getCode_initial());
            //   		jsonObject.put("Ano1a_bb_qu", listQuisioner.get(i).getBb_ku());
            //   		jsonObject.put("Ano1a_tinggi_qu", listQuisioner.get(i).getTinggi_ku());
            //   		jsonObject.put("Ano1b_checkbbturun_qu", listQuisioner.get(i).getSpinbb_ku());
            //   		jsonObject.put("Ano1b_editbbturun_qu", listQuisioner.get(i).getEditbb_ku1());
            //   		jsonObject.put("Ano2_checksehat_qu", listQuisioner.get(i).getSpinsehat_ku());
            //   		jsonObject.put("Ano2_editsehat_qu", listQuisioner.get(i).getEditsehat_ku());
            //   		jsonObject.put("Ano3_checkpenyakit_qu", listQuisioner.get(i).getSpinpenyakit_ku());
            //   		jsonObject.put("Ano3_editpenyakit_qu", listQuisioner.get(i).getEditpenyakit_ku());
            //   		jsonObject.put("Ano4a_checkrawat_qu", listQuisioner.get(i).getSpinoperasi_ku());
            //   		jsonObject.put("Ano4a_editrawat_qu", listQuisioner.get(i).getEditoperasi_ku());
            //   		jsonObject.put("Ano4b_checkahlijiwa_qu", listQuisioner.get(i).getSpin_ahlijiwa_ku());
            //   		jsonObject.put("Ano4b_editahlijiwa_qu", listQuisioner.get(i).getEdit_ahlijiwa_ku());
            //   		jsonObject.put("Ano5_checkketkeluarga_qu", listQuisioner.get(i).getSpinketkeluarga_ku());
            //   		jsonObject.put("Ano5_editketkeluarga_qu", listQuisioner.get(i).getEditketkeluarga_ku());
            //   		jsonObject.put("Ano6a_checkhamil_qu", listQuisioner.get(i).getSpinwnthamil_ku());
            //   		jsonObject.put("Ano6a_edithamil_qu", listQuisioner.get(i).getEditwnthamil_ku());
            //   		jsonObject.put("Ano6b_checkcaesar_qu", listQuisioner.get(i).getSpincaesar_ku());
            //   		jsonObject.put("Ano6b_editcaesar_qu", listQuisioner.get(i).getEditcaesar_ku());
            //   		jsonObject.put("Ano6c_checktest_pap_qu", listQuisioner.get(i).getSpintest_pap_ku());
            //   		jsonObject.put("Ano6c_edittest_pap_qu", listQuisioner.get(i).getEdittest_pap_ku());
            //   		jsonObject.put("Bno1_checkolahraga_qu", listQuisioner.get(i).getSpinolahraga_ku());
            //   		jsonObject.put("Bno1_editolahraga_qu", listQuisioner.get(i).getEditolahraga_ku());
            //   		jsonObject.put("Bno2_checkperjalanan_qu", listQuisioner.get(i).getSpinperjalanan_ku());
            //   		jsonObject.put("Bno2_editperjalanan_qu", listQuisioner.get(i).getEditperjalanan_ku());
            //   		jsonObject.put("Bno3a_checkrokok_qu", listQuisioner.get(i).getSpinrokok_ku());
            //   		jsonObject.put("Bno3a_batangrokok_qu", listQuisioner.get(i).getEditrokok_ku());
            //   		jsonObject.put("Bno3a_tahunrokok_qu", listQuisioner.get(i).getEdittahunrokok_ku());
            //   		jsonObject.put("Bno3b_checkalkohol_qu", listQuisioner.get(i).getSpinalkohol_ku());
            //   		jsonObject.put("Bno3b_editalkohol_qu", listQuisioner.get(i).getEditalkohol_ku());
            //   		jsonObject.put("Bno3c_checknarkotika_qu", listQuisioner.get(i).getSpinnarkotika_ku());
            //   		jsonObject.put("Bno3c_editnarkotika_qu", listQuisioner.get(i).getEditnarkotika_ku());
            //   		jsonObject.put("Bno4_checkpolis_qu", listQuisioner.get(i).getSpinpunya_polis_ku());
            //   		jsonObject.put("Bno4_editpolis_qu", listQuisioner.get(i).getEditpunya_polis_ku());
            //   		ArrayKuis.put(jsonObject);
            //	}
            List<ModelMst_Answer> listQuisioner = me.getModelUpdateQuisioner().getListQuisioner();
            for (int i = 0; i < listQuisioner.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("question_valid_date", listQuisioner.get(i).getQuestion_valid_date());
                jsonObject.put("question_type_id", listQuisioner.get(i).getQuestion_type_id());
                jsonObject.put("question_id", listQuisioner.get(i).getQuestion_id());
                jsonObject.put("option_type", listQuisioner.get(i).getOption_type());
                jsonObject.put("option_group", listQuisioner.get(i).getOption_group());
                jsonObject.put("option_order", listQuisioner.get(i).getOption_order());
                jsonObject.put("answer_order", 1);
                jsonObject.put("answer", listQuisioner.get(i).getAnswer());
                ArrayKuis.put(jsonObject);
            }
            if (me.getModelID().getJns_spaj() == 3) {
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("question_valid_date", "01/09/2015");
                jsonObject1.put("question_type_id", 3);
                jsonObject1.put("question_id", 138);
                jsonObject1.put("option_type", 0);
                jsonObject1.put("option_group", 0);
                jsonObject1.put("option_order", 0);
                jsonObject1.put("answer_order", 0);
                jsonObject1.put("answer", "");
                ArrayKuis.put(jsonObject1);

                jsonObject1 = new JSONObject();
                jsonObject1.put("question_valid_date", "01/09/2015");
                jsonObject1.put("question_type_id", 3);
                jsonObject1.put("question_id", 151);
                jsonObject1.put("option_type", 0);
                jsonObject1.put("option_group", 0);
                jsonObject1.put("option_order", 0);
                jsonObject1.put("answer_order", 0);
                jsonObject1.put("answer", "");
                ArrayKuis.put(jsonObject1);

                List<ModelTT_NO3UQ> listModelTT_NO3UQ = me.getModelUpdateQuisioner().getList_ttno3();
                if (listModelTT_NO3UQ.isEmpty()) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("question_valid_date", "01/08/2014");
                    jsonObject.put("question_type_id", 2);
                    jsonObject.put("question_id", 3);
                    jsonObject.put("option_type", 3);
                    jsonObject.put("option_group", 0);
                    jsonObject.put("option_order", 1);
                    jsonObject.put("answer_order", 0);
                    jsonObject.put("answer", "");
                    ArrayKuis.put(jsonObject);

                } else {
                    for (int i = 0; i < listModelTT_NO3UQ.size(); i++) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("question_valid_date", "01/08/2014");
                        jsonObject.put("question_type_id", 2);
                        jsonObject.put("question_id", 3);
                        jsonObject.put("option_type", 3);
                        jsonObject.put("option_group", 0);
                        jsonObject.put("option_order", 1);
                        jsonObject.put("answer_order", listModelTT_NO3UQ.get(i).getCounter());
                        jsonObject.put("answer", listModelTT_NO3UQ.get(i).getCounter() + "~" + listModelTT_NO3UQ.get(i).getNama_perushn() +
                                "~" + listModelTT_NO3UQ.get(i).getStr_produk() + "~" + listModelTT_NO3UQ.get(i).getUp() + "~" +
                                listModelTT_NO3UQ.get(i).getTgl_polis());
                        ArrayKuis.put(jsonObject);
                    }
                }

                List<ModelTT_NO5UQ> listModelTT_NO5UQ = me.getModelUpdateQuisioner().getList_ttno5();
                if (listModelTT_NO5UQ.isEmpty()) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("question_valid_date", "01/08/2014");
                    jsonObject.put("question_type_id", 2);
                    jsonObject.put("question_id", 5);
                    jsonObject.put("option_type", 3);
                    jsonObject.put("option_group", 0);
                    jsonObject.put("option_order", 1);
                    jsonObject.put("answer_order", 0);
                    jsonObject.put("answer", "");
                    ArrayKuis.put(jsonObject);

                } else {
                    for (int i = 0; i < listModelTT_NO5UQ.size(); i++) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("question_valid_date", "01/08/2014");
                        jsonObject.put("question_type_id", 2);
                        jsonObject.put("question_id", 5);
                        jsonObject.put("option_type", 3);
                        jsonObject.put("option_group", 0);
                        jsonObject.put("option_order", 1);
                        jsonObject.put("answer_order", listModelTT_NO5UQ.get(i).getCounter());
                        jsonObject.put("answer", listModelTT_NO5UQ.get(i).getCounter() + "~" + listModelTT_NO5UQ.get(i).getNama_perushn() +
                                "~" + listModelTT_NO5UQ.get(i).getStr_produk() + "~" + listModelTT_NO5UQ.get(i).getUp() + "~" +
                                listModelTT_NO5UQ.get(i).getTgl_polis() + "~" + listModelTT_NO5UQ.get(i).getStr_klaim());
                        ArrayKuis.put(jsonObject);
                    }
                }
            }

            List<ModelDK_NO9ttUQ> listNo9ttUQ = me.getModelUpdateQuisioner().getList_dkno9tt();
            if (listNo9ttUQ.isEmpty()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("question_valid_date", "01/09/2015");
                jsonObject.put("question_type_id", 3);
                jsonObject.put("question_id", 155);
                jsonObject.put("option_type", 3);
                jsonObject.put("option_group", 0);
                jsonObject.put("option_order", 1);
                jsonObject.put("answer_order", 0);
                jsonObject.put("answer", "");
                ArrayKuis.put(jsonObject);

            } else {
                for (int i = 0; i < listNo9ttUQ.size(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("question_valid_date", "01/09/2015");
                    jsonObject.put("question_type_id", 3);
                    jsonObject.put("question_id", 155);
                    jsonObject.put("option_type", 3);
                    jsonObject.put("option_group", 0);
                    jsonObject.put("option_order", 1);
                    jsonObject.put("answer_order", listNo9ttUQ.get(i).getCounter());
                    jsonObject.put("answer", listNo9ttUQ.get(i).getStr_ct() + "~" + listNo9ttUQ.get(i).getUmur() + "~" +
                            listNo9ttUQ.get(i).getKeadaan() + "~" + listNo9ttUQ.get(i).getPenyebab());
                    ArrayKuis.put(jsonObject);
                }
            }

            List<ModelDK_NO9ppUQ> listNo9ppUQ = me.getModelUpdateQuisioner().getList_dkno9pp();
            if (listNo9ppUQ.isEmpty()) {
                    JSONObject jsonObject = new JSONObject();
                jsonObject.put("question_valid_date", "01/09/2015");
                jsonObject.put("question_type_id", 3);
                jsonObject.put("question_id", 155);
                jsonObject.put("option_type", 3);
                jsonObject.put("option_group", 0);
                jsonObject.put("option_order", 2);
                jsonObject.put("answer_order", 0);
                jsonObject.put("answer", "");
                ArrayKuis.put(jsonObject);

            } else {
                for (int i = 0; i < listNo9ppUQ.size(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("question_valid_date", "01/09/2015");
                    jsonObject.put("question_type_id", 3);
                    jsonObject.put("question_id", 155);
                    jsonObject.put("option_type", 3);
                    jsonObject.put("option_group", 0);
                    jsonObject.put("option_order", 2);
                    jsonObject.put("answer_order", listNo9ppUQ.get(i).getCounter());
                    jsonObject.put("answer", listNo9ppUQ.get(i).getStr_cp() + "~" + listNo9ppUQ.get(i).getUmur() + "~" +
                            listNo9ppUQ.get(i).getKeadaan() + "~" + listNo9ppUQ.get(i).getPenyebab());
                    ArrayKuis.put(jsonObject);
                }
            }

            List<ModelDK_KetKesUQ> listKetKesUQ = me.getModelUpdateQuisioner().getList_dkketkes();
            if (listKetKesUQ.isEmpty()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("question_valid_date", "01/09/2015");
                jsonObject.put("question_type_id", 3);
                jsonObject.put("question_id", 156);
                jsonObject.put("option_type", 3);
                jsonObject.put("option_group", 0);
                jsonObject.put("option_order", 1);
                jsonObject.put("answer_order", 0);
                jsonObject.put("answer", "");
                ArrayKuis.put(jsonObject);

            } else {
                for (int i = 0; i < listKetKesUQ.size(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("question_valid_date", "01/09/2015");
                    jsonObject.put("question_type_id", 3);
                    jsonObject.put("question_id", 156);
                    jsonObject.put("option_type", 3);
                    jsonObject.put("option_group", 0);
                    jsonObject.put("option_order", 1);
                    jsonObject.put("answer_order", listKetKesUQ.get(i).getCounter());
                    jsonObject.put("answer", listKetKesUQ.get(i).getCounter() + "~" + listKetKesUQ.get(i).getKet());
                    ArrayKuis.put(jsonObject);
                }
            }
            jsonObjectKU.put("MST_ANSWER", ArrayKuis);
            Log.i("jsonObjectKU ", jsonObjectKU.toString());

            JSONObject jsonObjectCP = new JSONObject();
            jsonObjectCP.put("nama_perush_cp", me.getCalonPembayarPremiModel().getNama_perush_cp());
            jsonObjectCP.put("almt_perush_cp", me.getCalonPembayarPremiModel().getAlmt_perush_cp());
            jsonObjectCP.put("kota_perush_cp", me.getCalonPembayarPremiModel().getKota_perush_cp());
            jsonObjectCP.put("kdpos_perush_cp", me.getCalonPembayarPremiModel().getKdpos_perush_cp());
            jsonObjectCP.put("telp_perush_cp", me.getCalonPembayarPremiModel().getTelp_perush_cp());
            jsonObjectCP.put("nofax_perush_cp", me.getCalonPembayarPremiModel().getNofax_perush_cp());
            jsonObjectCP.put("bid_usaha_cp", me.getCalonPembayarPremiModel().getBid_usaha_cp());
            jsonObjectCP.put("editbisnis_cp", me.getCalonPembayarPremiModel().getEditbisnis_cp());
            jsonObjectCP.put("editpihak3_cp", me.getCalonPembayarPremiModel().getEditpihak3_cp());
            jsonObjectCP.put("edit_nmpihak3_cp", me.getCalonPembayarPremiModel().getEdit_nmpihak3_cp());
            jsonObjectCP.put("almt_phk3_cp", me.getCalonPembayarPremiModel().getAlmt_phk3_cp());
            jsonObjectCP.put("notlp_phk3_cp", me.getCalonPembayarPremiModel().getNotlp_phk3_cp());
            jsonObjectCP.put("notlp_kntrphk3_cp", me.getCalonPembayarPremiModel().getNotlp_kntrphk3_cp());
            jsonObjectCP.put("edit_emailphk3_cp", me.getCalonPembayarPremiModel().getEdit_emailphk3_cp());
            jsonObjectCP.put("tmpt_kedudukan_cp", me.getCalonPembayarPremiModel().getTmpt_kedudukan_cp());
            jsonObjectCP.put("bidusaha_phk3_cp", me.getCalonPembayarPremiModel().getBidusaha_phk3_cp());
            jsonObjectCP.put("lain_jns_pkrjaan_cp", me.getCalonPembayarPremiModel().getLain_jns_pkrjaan_cp());
            jsonObjectCP.put("instansi_phk3_cp", me.getCalonPembayarPremiModel().getInstansi_phk3_cp());
            jsonObjectCP.put("npwp_phk3_cp", me.getCalonPembayarPremiModel().getNpwp_phk3_cp());
            jsonObjectCP.put("sumber_dana_phk3_cp", me.getCalonPembayarPremiModel().getSumber_dana_phk3_cp());
            jsonObjectCP.put("tujuan_phk3_cp", me.getCalonPembayarPremiModel().getTujuan_phk3_cp());
            jsonObjectCP.put("alasan_phk3_cp", me.getCalonPembayarPremiModel().getAlasan_phk3_cp());
            jsonObjectCP.put("tgl_pendirian_cp", me.getCalonPembayarPremiModel().getTgl_pendirian_cp());
            jsonObjectCP.put("tgl_lhrphk3_cp", me.getCalonPembayarPremiModel().getTgl_lhrphk3_cp());
            jsonObjectCP.put("tmpt_lhrphk3_cp", me.getCalonPembayarPremiModel().getTmpt_lhrphk3_cp());
            jsonObjectCP.put("int_propinsi_cp", me.getCalonPembayarPremiModel().getInt_propinsi_cp());
            jsonObjectCP.put("int_spinbisnis_cp", me.getCalonPembayarPremiModel().getInt_spinbisnis_cp());
            jsonObjectCP.put("str_spintotal_bln_cp", me.getCalonPembayarPremiModel().getStr_spintotal_bln_cp());
            jsonObjectCP.put("str_spintotal_thn_cp", me.getCalonPembayarPremiModel().getStr_spintotal_thn_cp());
            jsonObjectCP.put("int_spinpihak3_cp", me.getCalonPembayarPremiModel().getInt_spinpihak3_cp());
            if (me.getCalonPembayarPremiModel().getInt_spinpihak3_cp() == 0) {
                jsonObjectCP.put("int_spin_hub_dgppphk3_cp", 40);
                jsonObjectCP.put("int_ket_cp", 40);
            } else {
                jsonObjectCP.put("int_spin_hub_dgppphk3_cp", me.getCalonPembayarPremiModel().getInt_spin_hub_dgppphk3_cp());
                jsonObjectCP.put("int_ket_cp", me.getCalonPembayarPremiModel().getInt_ket_cp());
            }
            jsonObjectCP.put("int_spin_kewrgn_cp", me.getCalonPembayarPremiModel().getInt_spin_kewrgn_cp());
            jsonObjectCP.put("int_spin_pekerjaan_cp", me.getCalonPembayarPremiModel().getSpin_pekerjaan_cp());
            jsonObjectCP.put("str_spin_jns_pkrjaan_cp", me.getCalonPembayarPremiModel().getStr_spin_jns_pkrjaan_cp());
            jsonObjectCP.put("str_spin_jabatanphk3_cp", me.getCalonPembayarPremiModel().getStr_spin_jabatanphk3_cp());


            JSONArray blnrutin_cp = new JSONArray();

            JSONArray thnrutin_cp = new JSONArray();

            JSONArray tujuan_cp = new JSONArray();
//	if(me.getModelPemegangPolis().getCp_pp() == 40){
            if (!me.getCalonPembayarPremiModel().getStr_gaji_cp().equals("")) {
                JSONObject sumber = new JSONObject();
                sumber.put("sumber", me.getCalonPembayarPremiModel().getStr_gaji_cp());
                blnrutin_cp.put(sumber);
            }

            if (!me.getCalonPembayarPremiModel().getStr_penghsl_cp().equals("")) {
                JSONObject sumber = new JSONObject();
                sumber.put("sumber", me.getCalonPembayarPremiModel().getStr_penghsl_cp());
                blnrutin_cp.put(sumber);
            }

            if (!me.getCalonPembayarPremiModel().getStr_ortu_cp().equals("")) {
                JSONObject sumber = new JSONObject();
                sumber.put("sumber", me.getCalonPembayarPremiModel().getStr_ortu_cp());
                blnrutin_cp.put(sumber);
            }

            if (!me.getCalonPembayarPremiModel().getStr_laba_cp().equals("")) {
                JSONObject sumber = new JSONObject();
                sumber.put("sumber", me.getCalonPembayarPremiModel().getStr_laba_cp());
                blnrutin_cp.put(sumber);
            }

            if (!me.getCalonPembayarPremiModel().getStr_hslusaha_cp().equals("")) {
                JSONObject sumber = new JSONObject();
                sumber.put("sumber", me.getCalonPembayarPremiModel().getStr_hslusaha_cp() + "~" +
                        me.getCalonPembayarPremiModel().getEdit_hslusaha_cp());
                blnrutin_cp.put(sumber);
            }

            if (!me.getCalonPembayarPremiModel().getStr_hslinves_cp().equals("")) {
                JSONObject sumber = new JSONObject();
                sumber.put("sumber", me.getCalonPembayarPremiModel().getStr_hslinves_cp() + "~" +
                        me.getCalonPembayarPremiModel().getEdit_hslinves_cp());
                blnrutin_cp.put(sumber);
            }

            if (!me.getCalonPembayarPremiModel().getStr_lainnya_cp().equals("")) {
                JSONObject sumber = new JSONObject();
                sumber.put("sumber", me.getCalonPembayarPremiModel().getStr_lainnya_cp() + "~" +
                        me.getCalonPembayarPremiModel().getEdit_lainnya_cp());
                blnrutin_cp.put(sumber);
            }
            if (!me.getCalonPembayarPremiModel().getStr_bonus_cp().equals("")) {
                JSONObject tahun = new JSONObject();
                tahun.put("sumber_tahun", me.getCalonPembayarPremiModel().getStr_bonus_cp());
                thnrutin_cp.put(tahun);
            }

            if (!me.getCalonPembayarPremiModel().getStr_komisi_cp().equals("")) {
                JSONObject tahun = new JSONObject();
                tahun.put("sumber_tahun", me.getCalonPembayarPremiModel().getStr_komisi_cp());
                thnrutin_cp.put(tahun);
            }

            if (!me.getCalonPembayarPremiModel().getStr_aset_cp().equals("")) {
                JSONObject tahun = new JSONObject();
                tahun.put("sumber_tahun", me.getCalonPembayarPremiModel().getStr_aset_cp());
                thnrutin_cp.put(tahun);
            }

            if (!me.getCalonPembayarPremiModel().getStr_hadiah_cp().equals("")) {
                JSONObject tahun = new JSONObject();
                tahun.put("sumber_tahun", me.getCalonPembayarPremiModel().getStr_hadiah_cp());
                thnrutin_cp.put(tahun);
            }

            if (!me.getCalonPembayarPremiModel().getStr_hslinves_thn_cp().equals("")) {
                JSONObject tahun = new JSONObject();
                tahun.put("sumber_tahun", me.getCalonPembayarPremiModel().getStr_hslinves_thn_cp() + "~" +
                        me.getCalonPembayarPremiModel().getEdit_hslinves_thn_cp());
                thnrutin_cp.put(tahun);
            }

            if (!me.getCalonPembayarPremiModel().getStr_lainnya_thn_cp().equals("")) {
                JSONObject tahun = new JSONObject();
                tahun.put("sumber_tahun", me.getCalonPembayarPremiModel().getStr_lainnya_thn_cp() + "~" +
                        me.getCalonPembayarPremiModel().getEdit_lainnya_thn_cp());
                thnrutin_cp.put(tahun);
            }

            if (!me.getCalonPembayarPremiModel().getStr_proteksi_cp().equals("")) {
                JSONObject tujuan = new JSONObject();
                tujuan.put("tujuan", me.getCalonPembayarPremiModel().getStr_proteksi_cp());
                tujuan_cp.put(tujuan);
            }

            if (!me.getCalonPembayarPremiModel().getStr_pend_cp().equals("")) {
                JSONObject tujuan = new JSONObject();
                tujuan.put("tujuan", me.getCalonPembayarPremiModel().getStr_pend_cp());
                tujuan_cp.put(tujuan);
            }

            if (!me.getCalonPembayarPremiModel().getStr_inves_cp().equals("")) {
                JSONObject tujuan = new JSONObject();
                tujuan.put("tujuan", me.getCalonPembayarPremiModel().getStr_inves_cp());
                tujuan_cp.put(tujuan);
            }

            if (!me.getCalonPembayarPremiModel().getStr_tabungan_cp().equals("")) {
                JSONObject tujuan = new JSONObject();
                tujuan.put("tujuan", me.getCalonPembayarPremiModel().getStr_tabungan_cp());
                tujuan_cp.put(tujuan);
            }

            if (!me.getCalonPembayarPremiModel().getStr_pensiun_cp().equals("")) {
                JSONObject tujuan = new JSONObject();
                tujuan.put("tujuan", me.getCalonPembayarPremiModel().getStr_pensiun_cp());
                tujuan_cp.put(tujuan);
            }

            if (!me.getCalonPembayarPremiModel().getStr_lainnya_tujuan_cp().equals("")) {
                JSONObject tujuan = new JSONObject();
                tujuan.put("sumber_tahun", me.getCalonPembayarPremiModel().getStr_lainnya_tujuan_cp() + "~" +
                        me.getCalonPembayarPremiModel().getEdit_lainnya_tujuan_cp());
                tujuan_cp.put(tujuan);
            }

//		}

            jsonObjectCP.put("sumberpendapatan_blnrutin_cp", blnrutin_cp);
            jsonObjectCP.put("sumberpendapatan_thn_nonrutin_cp", thnrutin_cp);
            jsonObjectCP.put("tujuan_cp", tujuan_cp);
            //profile Resiko bernard
            JSONObject jsonObjectPR = new JSONObject();

            JSONArray ArrayProfile = new JSONArray();
            { // no 1
                JSONObject jsonpr = new JSONObject();
                jsonpr.put("question_id", 1); // nomor urut pertanyaanya bisa
                // bernard lihat di RadioGroup
                // profile resiko.java
                if (me.getProfileResikoModel().getNilai_no1_pr() == 2) {
                    jsonpr.put("answer_id", 2);
                } else if (me.getProfileResikoModel().getNilai_no1_pr() == 4) {
                    jsonpr.put("answer_id", 3);
                } else if (me.getProfileResikoModel().getNilai_no1_pr() == 6) {
                    jsonpr.put("answer_id", 4);
                } else if (me.getProfileResikoModel().getNilai_no1_pr() == 8) {
                    jsonpr.put("answer_id", 5);
                } else if (me.getProfileResikoModel().getNilai_no1_pr() == 10) {
                    jsonpr.put("answer_id", 6);
                } else {
                    jsonpr.put("answer_id", 0);
                }
                jsonpr.put("result", me.getProfileResikoModel().getNilai_no1_pr());
                ArrayProfile.put(jsonpr);
            }

            { // no 2
                JSONObject jsonpr = new JSONObject();
                jsonpr.put("question_id", 7); // nomor urut pertanyaanya bisa
                // bernard lihat di LinearLayout
                // profile resiko.java
                if (me.getProfileResikoModel().getNilai_no7_pr() == 2) {
                    jsonpr.put("answer_id", 8);
                } else if (me.getProfileResikoModel().getNilai_no7_pr() == 4) {
                    jsonpr.put("answer_id", 9);
                } else if (me.getProfileResikoModel().getNilai_no7_pr() == 6) {
                    jsonpr.put("answer_id", 10);
                } else if (me.getProfileResikoModel().getNilai_no7_pr() == 8) {
                    jsonpr.put("answer_id", 11);
                } else {
                    jsonpr.put("answer_id", 0);
                }
                jsonpr.put("result", me.getProfileResikoModel().getNilai_no7_pr());
                ArrayProfile.put(jsonpr);
            }

            { // no 3
                JSONObject jsonpr = new JSONObject();
                jsonpr.put("question_id", 12); // nomor urut pertanyaanya bisa
                // bernard lihat di LinearLayout
                // profile resiko.java
                if (me.getProfileResikoModel().getNilai_no12_pr() == 2) {
                    jsonpr.put("answer_id", 13);
                } else if (me.getProfileResikoModel().getNilai_no12_pr() == 4) {
                    jsonpr.put("answer_id", 14);
                } else if (me.getProfileResikoModel().getNilai_no12_pr() == 6) {
                    jsonpr.put("answer_id", 15);
                } else if (me.getProfileResikoModel().getNilai_no12_pr() == 8) {
                    jsonpr.put("answer_id", 16);
                } else if (me.getProfileResikoModel().getNilai_no12_pr() == 10) {
                    jsonpr.put("answer_id", 17);
                } else {
                    jsonpr.put("answer_id", 0);
                }
                jsonpr.put("result", me.getProfileResikoModel().getNilai_no12_pr());
                ArrayProfile.put(jsonpr);
            }

            { // no 4
                JSONObject jsonpr = new JSONObject();
                jsonpr.put("question_id", 18); // nomor urut pertanyaanya bisa
                // bernard lihat di LinearLayout
                // profile resiko.java
                if (me.getProfileResikoModel().getNilai_no18_pr() == 2) {
                    jsonpr.put("answer_id", 19);
                } else if (me.getProfileResikoModel().getNilai_no18_pr() == 4) {
                    jsonpr.put("answer_id", 20);
                } else if (me.getProfileResikoModel().getNilai_no18_pr() == 6) {
                    jsonpr.put("answer_id", 21);
                } else if (me.getProfileResikoModel().getNilai_no18_pr() == 8) {
                    jsonpr.put("answer_id", 22);
                } else {
                    jsonpr.put("answer_id", 0);
                }
                jsonpr.put("result", me.getProfileResikoModel().getNilai_no18_pr());
                ArrayProfile.put(jsonpr);
            }

            { // no 5
                JSONObject jsonpr = new JSONObject();
                jsonpr.put("question_id", 23); // nomor urut pertanyaanya bisa
                // bernard lihat di LinearLayout
                // profile resiko.java
                if (me.getProfileResikoModel().getNilai_no23_pr() == 2) {
                    jsonpr.put("answer_id", 24);
                } else if (me.getProfileResikoModel().getNilai_no23_pr() == 4) {
                    jsonpr.put("answer_id", 25);
                } else if (me.getProfileResikoModel().getNilai_no23_pr() == 6) {
                    jsonpr.put("answer_id", 26);
                } else if (me.getProfileResikoModel().getNilai_no23_pr() == 8) {
                    jsonpr.put("answer_id", 27);
                } else {
                    jsonpr.put("answer_id", 0);
                }
                jsonpr.put("result", me.getProfileResikoModel().getNilai_no23_pr());
                ArrayProfile.put(jsonpr);
            }

            { // no 6
                JSONObject jsonpr = new JSONObject();
                jsonpr.put("question_id", 28); // nomor urut pertanyaanya bisa
                // bernard lihat di LinearLayout
                // profile resiko.java
                if (me.getProfileResikoModel().getNilai_no28_pr() == 2) {
                    jsonpr.put("answer_id", 29);
                } else if (me.getProfileResikoModel().getNilai_no28_pr() == 4) {
                    jsonpr.put("answer_id", 30);
                } else if (me.getProfileResikoModel().getNilai_no28_pr() == 6) {
                    jsonpr.put("answer_id", 31);
                } else if (me.getProfileResikoModel().getNilai_no28_pr() == 8) {
                    jsonpr.put("answer_id", 32);
                } else if (me.getProfileResikoModel().getNilai_no28_pr() == 10) {
                    jsonpr.put("answer_id", 33);
                } else {
                    jsonpr.put("answer_id", 0);
                }
                jsonpr.put("result", me.getProfileResikoModel().getNilai_no28_pr());
                ArrayProfile.put(jsonpr);
            }

            { // no 7
                JSONObject jsonpr = new JSONObject();
                jsonpr.put("question_id", 34); // nomor urut pertanyaanya bisa
                // bernard lihat di LinearLayout
                // profile resiko.java
                if (me.getProfileResikoModel().getNilai_no34_pr() == 2) {
                    jsonpr.put("answer_id", 35);
                } else if (me.getProfileResikoModel().getNilai_no34_pr() == 4) {
                    jsonpr.put("answer_id", 36);
                } else if (me.getProfileResikoModel().getNilai_no34_pr() == 6) {
                    jsonpr.put("answer_id", 37);
                } else if (me.getProfileResikoModel().getNilai_no34_pr() == 8) {
                    jsonpr.put("answer_id", 38);
                } else if (me.getProfileResikoModel().getNilai_no34_pr() == 10) {
                    jsonpr.put("answer_id", 39);
                } else {
                    jsonpr.put("answer_id", 0);
                }
                jsonpr.put("result", me.getProfileResikoModel().getNilai_no34_pr());
                ArrayProfile.put(jsonpr);
            }

            { // no 8
                JSONObject jsonpr = new JSONObject();
                jsonpr.put("question_id", 40); // nomor urut pertanyaanya bisa
                // bernard lihat di LinearLayout
                // profile resiko.java
                if (me.getProfileResikoModel().getNilai_no40_pr() == 2) {
                    jsonpr.put("answer_id", 41);
                } else if (me.getProfileResikoModel().getNilai_no40_pr() == 4) {
                    jsonpr.put("answer_id", 42);
                } else if (me.getProfileResikoModel().getNilai_no40_pr() == 6) {
                    jsonpr.put("answer_id", 43);
                } else if (me.getProfileResikoModel().getNilai_no40_pr() == 8) {
                    jsonpr.put("answer_id", 44);
                } else {
                    jsonpr.put("answer_id", 0);
                }
                jsonpr.put("result", me.getProfileResikoModel().getNilai_no40_pr());
                ArrayProfile.put(jsonpr);
            }

            { // no 9
                JSONObject jsonpr = new JSONObject();
                jsonpr.put("question_id", 45); // nomor urut pertanyaanya bisa
                // bernard lihat di LinearLayout
                // profile resiko.java
                if (me.getProfileResikoModel().getNilai_no45_pr() == 2) {
                    jsonpr.put("answer_id", 46);
                } else if (me.getProfileResikoModel().getNilai_no45_pr() == 4) {
                    jsonpr.put("answer_id", 47);
                } else if (me.getProfileResikoModel().getNilai_no45_pr() == 6) {
                    jsonpr.put("answer_id", 48);
                } else if (me.getProfileResikoModel().getNilai_no45_pr() == 8) {
                    jsonpr.put("answer_id", 49);
                } else if (me.getProfileResikoModel().getNilai_no45_pr() == 10) {
                    jsonpr.put("answer_id", 50);
                } else {
                    jsonpr.put("answer_id", 0);
                }
                jsonpr.put("result", me.getProfileResikoModel().getNilai_no45_pr());
                ArrayProfile.put(jsonpr);
            }

            { // no 10
                JSONObject jsonpr = new JSONObject();
                jsonpr.put("question_id", 51); // nomor urut pertanyaanya bisa
                // bernard lihat di LinearLayout
                // profile resiko.java
                if (me.getProfileResikoModel().getNilai_no51_pr() == 2) {
                    jsonpr.put("answer_id", 52);
                } else if (me.getProfileResikoModel().getNilai_no51_pr() == 4) {
                    jsonpr.put("answer_id", 53);
                } else if (me.getProfileResikoModel().getNilai_no51_pr() == 6) {
                    jsonpr.put("answer_id", 54);
                } else if (me.getProfileResikoModel().getNilai_no51_pr() == 8) {
                    jsonpr.put("answer_id", 55);
                } else {
                    jsonpr.put("answer_id", 0);
                }
                jsonpr.put("result", me.getProfileResikoModel().getNilai_no51_pr());
                ArrayProfile.put(jsonpr);
            }

            // lanjutin kebawah
            jsonObjectPR.put("answer_pr", ArrayProfile);

            //final
            JSONObject JSONESPAJ = null;
            JSONESPAJ = new JSONObject();
            JSONObject JSONFINAL = new JSONObject();

            String noProposalTab = me.getModelID().getProposal_tab();
            E_Select e_select = new E_Select(context);
            String noProposal = e_select.getProposalNo(noProposalTab);

            if (me.getDetilAgenModel().getJENIS_LOGIN() == 2) {
                if (me.getDetilAgenModel().getGroup_id_da() == 40) {
                    JSONESPAJ.put("NO_PROPOSAL", "");
                } else {
                    if (TextUtils.isEmpty(noProposal)){
                        JSONESPAJ.put("NO_PROPOSAL", "");
                    }else {
                        JSONESPAJ.put("NO_PROPOSAL", noProposal);
                    }
                }
            } else {
                //Bancass
                if (TextUtils.isEmpty(noProposal)){
                    JSONESPAJ.put("NO_PROPOSAL", "");
                }else {
                    JSONESPAJ.put("NO_PROPOSAL", noProposal);
                }
            }
            JSONESPAJ.put("GADGET_SPAJ_KEY", me.getModelID().getSPAJ_ID_TAB());
            JSONESPAJ.put("SERTIFIKAT", me.getModelID().getSertifikat());
            JSONESPAJ.put("no_policy", "");
            JSONESPAJ.put("NO_VA", me.getModelID().getVa_number());
            JSONESPAJ.put("IMEI", me.getModelID().getImei());
            JSONESPAJ.put("JENIS_SPAJ", me.getModelID().getJns_spaj());
            JSONESPAJ.put("Pemegang_Polis", jsonObjectPP);
            JSONESPAJ.put("Tertanggung", jsonObjectTT);
            JSONESPAJ.put("Usulan_Asuransi", jsonObjectUA);
            JSONESPAJ.put("Detail_investasi", jsonObjectDI);
            JSONESPAJ.put("Detail_Agen", jsonObjectDA);
//            JSONESPAJ.put("Dokumen_Pendukung",jsonObjectDP);
            JSONESPAJ.put("Kuisioner", jsonObjectKU);
            JSONESPAJ.put("Calon_Pembayar", jsonObjectCP);
            JSONESPAJ.put("profile_resiko", jsonObjectPR);
            JSONESPAJ.put("FLAG_APP", 1);
            JSONESPAJ.put("FLAG_POS", me.getModelID().getFlag_pos());
            JSONESPAJ.put("VERSION_APP", context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
            JSONESPAJ.put("CLIENT_KEY", "5DryOzDAxz");
            JSONFINAL.put("ESPAJ", JSONESPAJ);
            JSONFINAL.put("CLIENT", "ANDROID");
            JSONFINAL.put("APP", "AUTOSALES");

            json = JSONFINAL.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static String JSONSertifikat(Context context, EspajModel me) {
        String json = "";
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("lca_id", me.getDetilAgenModel().getLca_id());
            jsonObject.put("lwk_id", me.getDetilAgenModel().getLwk_id());
            jsonObject.put("nama_pp", me.getPemegangPolisModel().getNama_pp());
            jsonObject.put("nama_tt", me.getTertanggungModel().getNama_tt());
            jsonObject.put("address", val_tagihan(context, me, me.getPemegangPolisModel().getTagihan_pp()));
            jsonObject.put("lsbs_id", String.valueOf(me.getUsulanAsuransiModel().getKd_produk_ua()));
            jsonObject.put("lsdbs_number", String.valueOf(me.getUsulanAsuransiModel().getSub_produk_ua()));
            jsonObject.put("msag_id", me.getDetilAgenModel().getKd_penutup_da());
            jsonObject.put("no_spaj_gadget", me.getModelID().getSPAJ_ID_TAB());
            jsonObject.put("age_pp", me.getPemegangPolisModel().getUsia_pp());
            jsonObject.put("age_tt", me.getTertanggungModel().getUsia_tt());
            jsonObject.put("up", String.valueOf(me.getUsulanAsuransiModel().getUp_ua()));// edit bernard
            jsonObject.put("premi", String.valueOf(me.getUsulanAsuransiModel().getUnitlink_premistandard_ua()));// edit bernard
            jsonObject.put("bod_pp", me.getPemegangPolisModel().getTtl_pp());
            jsonObject.put("bod_tt", me.getTertanggungModel().getTtl_tt());
            jsonObject.put("insperiod", String.valueOf(me.getUsulanAsuransiModel().getMasa_pertanggungan_ua()));
            jsonObject.put("begdate", me.getUsulanAsuransiModel().getAwalpertanggungan_ua());
//            jsonObject.put("begdate", me.getUsulanAsuransiModel().getAwalpertanggungan_ua());
            jsonObject.put("sertifikat", me.getModelID().getSertifikat());
            jsonObject.put("cara_bayar", String.valueOf(me.getUsulanAsuransiModel().getCarabayar_ua()));
            JSONObject jsonfinal = new JSONObject();
            jsonfinal.put("ESPAJ", jsonObject);
            json = jsonfinal.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    private static String val_tagihan(Context context,EspajModel me, int tagihan) {
        String alamat = "";
        switch (tagihan) {
            case 0:
                alamat = me.getPemegangPolisModel().getAlamat_pp()
                        + ", Kel "+MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_pp()), me.getPemegangPolisModel().getKelurahan_pp())
                        + ", Kec " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_pp()), me.getPemegangPolisModel().getKecamatan_pp())
                        + ", Kab " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_pp()), me.getPemegangPolisModel().getKabupaten_pp())
                        + ", Prov "+MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Propinsi_PP(), (me.getPemegangPolisModel().getPropinsi_pp()))
                        + ", " + me.getPemegangPolisModel().getKdpos_pp() ;
                break;

            case 1:
              alamat = me.getPemegangPolisModel().getAlamat_kantor_pp()
                      + ", Kel " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_kantor_pp()), me.getPemegangPolisModel().getKelurahan_kantor_pp())
                      + ", Kec " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_kantor_pp()), me.getPemegangPolisModel().getKecamatan_kantor_pp())
                      + ", Kab " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_kantor_pp()), me.getPemegangPolisModel().getKabupaten_kantor_pp())
                      + ", Prov " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Propinsi_PP(), me.getPemegangPolisModel().getPropinsi_kantor_pp())
                      + ", "+me.getPemegangPolisModel().getKdpos_kantor_pp();
                break;

            case 2:
                alamat = me.getPemegangPolisModel().getAlamat_tinggal_pp()
                        + ", Kel " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_tinggal_pp()),me.getPemegangPolisModel().getKelurahan_tinggal_pp())
                        + ", Kec " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_tinggal_pp()), me.getPemegangPolisModel().getKecamatan_tinggal_pp())
                        + ", Kab " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_tinggal_pp()), me.getPemegangPolisModel().getKabupaten_tinggal_pp())
                        + ", Prov " + MethodSupport.getAdapterPositionString(new E_Select(context).getLst_Propinsi_PP(),me.getPemegangPolisModel().getPropinsi_tinggal_pp())
                        + ", " +me.getPemegangPolisModel().getKdpos_tinggal_pp();

                break;
        }
        return alamat;
    }
}
