package id.co.ajsmsig.nb.crm.model;

/**
 * Created by Fajar.Azhar on 09/03/2018.
 */

public class C_SearchLeadModel {
    private long slTabId;
    private String costumerName;
    private int costumerAge;
    private long slId;

    public C_SearchLeadModel( String costumerName, int costumerAge, long slTabId, long slId) {
        this.costumerName = costumerName;
        this.costumerAge = costumerAge;
        this.slTabId = slTabId;
        this.slId = slId;
    }


    public long getSlTabId() {
        return slTabId;
    }

    public void setSlTabId(long slTabId) {
        this.slTabId = slTabId;
    }
    public String getCostumerName() {
        return costumerName;
    }

    public void setCostumerName(String costumerName) {
        this.costumerName = costumerName;
    }

    public int getCostumerAge() {
        return costumerAge;
    }

    public void setCostumerAge(int costumerAge) {
        this.costumerAge = costumerAge;
    }

    public long getSlId() {
        return slId;
    }

    public void setSlId(long slId) {
        this.slId = slId;
    }
}
