package id.co.ajsmsig.nb.crm.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.apiresponse.ListProvider;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class C_ListProviderActivity extends AppCompatActivity implements OnMapReadyCallback {

    @BindView(R.id.mapView)
    MapView mapView;
    @BindView(R.id.layoutLoading)
    RelativeLayout layoutLoading;

    private GoogleMap map;
    private LocationManager mLocationManager;

    private double JAKARTA_LAT = -6.175110;
    private double JAKARTA_LNG = 106.865036;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c_list_provider);
        ButterKnife.bind(this);

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        refreshLocation();


    }

    @SuppressLint("MissingPermission")
    private void refreshLocation() {
        if (isLocationPermissionAllowed()) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0.5f, mLocationListener);
        }
    }

    private boolean isLocationPermissionAllowed() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;

    }

    private void checkForPermission() {
        Dexter.withActivity(this)
                .withPermissions(android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(permissionsListener)
                .withErrorListener(error -> Toast.makeText(C_ListProviderActivity.this, "Terjadi kesalahan " + error.toString(), Toast.LENGTH_LONG).show())
                .onSameThread()
                .check();
    }

    private void downloadListProvider() {
        showProgressBar();
        String url = "http://ews.sinarmasmsiglife.co.id/api/esehat/view_provider";
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<List<ListProvider>> call = api.downloadListProvider(url);
        call.enqueue(new Callback<List<ListProvider>>() {
            @SuppressLint("MissingPermission")
            @Override
            public void onResponse(Call<List<ListProvider>> call, Response<List<ListProvider>> response) {
                if (response.isSuccessful() && response.body() != null) {

                    List<ListProvider> body = response.body();
                    displayProvider(body);
                    refreshLocation();
                } else {
                    hideProgressBar();
                }
            }

            @Override
            public void onFailure(Call<List<ListProvider>> call, Throwable t) {
                hideProgressBar();
                Toast.makeText(C_ListProviderActivity.this, "Terjadi kesalahan dalam mengunduh provider " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void displayProvider(List<ListProvider> providers) {
        if (!providers.isEmpty()) {

            for (ListProvider provider : providers) {

                //Filter to only add to list if provider contains lat long
                if (provider.getMAPPOS() != null) {

                    String providerName = provider.getRSNAMA();
                    String providerAddress = provider.getRSALAMAT();

                    String currentLatLong = provider.getMAPPOS();
                    String[] separated = currentLatLong.split(", ");

                    if (separated != null && separated.length == 2) {

                        if (!TextUtils.isEmpty(separated[0]) && !TextUtils.isEmpty(separated[1])) {
                            String lat = separated[0];
                            String lng = separated[1];

                            double latitude = Double.parseDouble(lat);
                            double longitude = Double.parseDouble(lng);

                            //Load icon from drawable
                            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_provider);

                            //Create marker
                            MarkerOptions marker = new MarkerOptions()
                                    .position(new LatLng(latitude, longitude))
                                    .title(providerName)
                                    .icon(icon)
                                    .snippet(providerAddress);

                            map.addMarker(marker)
                                    .showInfoWindow();
                        }


                    }

                }


            }

        }
        hideProgressBar();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        map.getUiSettings().setMyLocationButtonEnabled(true);

        //Enable zoom button
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        checkForPermission();
        animateCameraTo(JAKARTA_LAT, JAKARTA_LNG);
    }

    private void animateCameraTo(double latitude, double longitude) {
        //Animate camera to the marker position
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))
                .zoom(12)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void showProgressBar() {
        layoutLoading.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        layoutLoading.setVisibility(View.GONE);
    }

    private void showPermissionRequired() {
        AlertDialog.Builder builder = new AlertDialog.Builder(C_ListProviderActivity.this);
        builder.setTitle("Izin diperlukan");
        builder.setMessage("Fitur ini membutuhkan izin untuk mengakses lokasi Anda");
        builder.setPositiveButton("Ok", (dialog, which) -> dialog.cancel());
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();

    }

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.v("TAG", "Location is " + location.getLatitude() + ", " + location.getLongitude());
            animateCameraTo(location.getLatitude(), location.getLongitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private MultiplePermissionsListener permissionsListener = new MultiplePermissionsListener() {
        @Override
        public void onPermissionsChecked(MultiplePermissionsReport report) {
            if (report.areAllPermissionsGranted()) {
                downloadListProvider();

            }

            if (report.isAnyPermissionPermanentlyDenied()) {
                showPermissionRequired();
            }
        }

        @Override
        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
            token.continuePermissionRequest();
        }
    };


}
