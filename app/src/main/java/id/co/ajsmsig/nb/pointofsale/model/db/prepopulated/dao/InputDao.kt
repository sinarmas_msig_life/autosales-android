package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Input
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.InputAllChoices


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface InputDao {
    @Query("SELECT * from Input")
    fun loadAllInputs(): LiveData<List<Input>>


    @Query("SELECT * from Input where pageId = :pageId")
    fun loadAllInputsWith(pageId: Int?): LiveData<List<InputAllChoices>>


    @Query("SELECT * from Input where pageId = :pageId AND showOnlyIfSelectPageOptionIds LIKE :query")
    fun loadAllInputsWith(pageId: Int?, query: String?): LiveData<List<InputAllChoices>>
}