package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import id.co.ajsmsig.nb.espaj.model.DetilAgenModel;
import id.co.ajsmsig.nb.espaj.model.EspajModel;

/**
 * Created by Bernard on 05/09/2017.
 */

public class TableDetilAgen {
    private Context context;

    public TableDetilAgen(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(EspajModel me){
        ContentValues cv = new ContentValues();
        cv.put("TGL_SPAJ_DA", me.getDetilAgenModel().getTgl_spaj_da());
        cv.put("NM_REGIONAL_DA", me.getDetilAgenModel().getNm_regional_da());
        cv.put("KD_REGIONAL_DA", me.getDetilAgenModel().getKd_regional_da());
        cv.put("KD_LEADER_DA", me.getDetilAgenModel().getKd_leader_da());
        cv.put("NMLEAD_PNUTUP_DA", me.getDetilAgenModel().getNmlead_pnutup_da());
        cv.put("KD_PENUTUP_DA", me.getDetilAgenModel().getKd_penutup_da());
        cv.put("NM_PNUTUP_DA", me.getDetilAgenModel().getNm_pnutup_da());
        cv.put("CHECK_KD_AO_DA", me.getDetilAgenModel().getCheck_kd_ao_da());
        cv.put("KD_AO_DA", me.getDetilAgenModel().getKd_ao_da());
        cv.put("CHECK_KD_PRIBADI_DA", me.getDetilAgenModel().getCheck_kd_pribadi_da());
        cv.put("KD_PNAGIHAN_DA", me.getDetilAgenModel().getKd_pnagihan_da());
        cv.put("NM_REGIONAL_PENAGIHAN_DA", me.getDetilAgenModel().getNm_Regional_Penagihan_da());
        cv.put("CHECK_BROKER_DA", me.getDetilAgenModel().getCheck_broker_da());
        cv.put("BROKER_DA", me.getDetilAgenModel().getBroker_da());
        cv.put("NAMA_BROKER_DA", me.getDetilAgenModel().getNama_broker_da());
        cv.put("BANK_DA", me.getDetilAgenModel().getBank_da());
        cv.put("NOREK_DA", me.getDetilAgenModel().getNorek_da());
        cv.put("PNYTAAN_SETUJU_DA", me.getDetilAgenModel().getPnytaan_setuju_da());
        cv.put("ID_SPONSOR_DA", me.getDetilAgenModel().getId_sponsor_da());
        cv.put("ID_PENEMPATAN_DA", me.getDetilAgenModel().getId_penempatan_da());
        cv.put("ID_MEMBER_DA", me.getDetilAgenModel().getId_member_da());
        cv.put("ID_REFFERAL_DA", me.getDetilAgenModel().getId_refferal_da());
        cv.put("NM_REFFERAL_DA", me.getDetilAgenModel().getNm_refferal_da());
        cv.put("EMAIL_DA", me.getDetilAgenModel().getEmail_da());
        cv.put("KODE_REG1", me.getDetilAgenModel().getLca_id());
        cv.put("KODE_REG2", me.getDetilAgenModel().getLwk_id());
        cv.put("KODE_REG3", me.getDetilAgenModel().getLsrg_id());
        cv.put("JENIS_LOGIN", me.getDetilAgenModel().getJENIS_LOGIN());
        cv.put("JENIS_LOGIN_BC", me.getDetilAgenModel().getJENIS_LOGIN_BC());
        cv.put("ID_CABANG_DA", me.getDetilAgenModel().getId_cabang_da());
        cv.put("NM_CABANG_DA", me.getDetilAgenModel().getNm_cabang_da());
//        cv.put("GROUP_ID_DA", me.getDetilAgenModel().getGroup_id_da());
        cv.put("VALIDATION", me.getDetilAgenModel().getValidation());

        return cv;
    }

    public DetilAgenModel getObject (Cursor cursor){
        DetilAgenModel detilAgenModel= new DetilAgenModel();
        if (cursor != null && cursor.getCount()>0){
            cursor.moveToFirst();
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("TGL_SPAJ_DA"))){
                detilAgenModel.setTgl_spaj_da(cursor.getString(cursor.getColumnIndexOrThrow("TGL_SPAJ_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NM_REGIONAL_DA"))){
                detilAgenModel.setNm_regional_da(cursor.getString(cursor.getColumnIndexOrThrow("NM_REGIONAL_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KD_REGIONAL_DA"))){
                detilAgenModel.setKd_regional_da(cursor.getString(cursor.getColumnIndexOrThrow("KD_REGIONAL_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KD_LEADER_DA"))){
                detilAgenModel.setKd_leader_da(cursor.getString(cursor.getColumnIndexOrThrow("KD_LEADER_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NMLEAD_PNUTUP_DA"))){
                detilAgenModel.setNmlead_pnutup_da(cursor.getString(cursor.getColumnIndexOrThrow("NMLEAD_PNUTUP_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KD_PENUTUP_DA"))){
                detilAgenModel.setKd_penutup_da(cursor.getString(cursor.getColumnIndexOrThrow("KD_PENUTUP_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NM_PNUTUP_DA"))){
                detilAgenModel.setNm_pnutup_da(cursor.getString(cursor.getColumnIndexOrThrow("NM_PNUTUP_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("CHECK_KD_AO_DA"))){
                detilAgenModel.setCheck_kd_ao_da(cursor.getInt(cursor.getColumnIndexOrThrow("CHECK_KD_AO_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KD_AO_DA"))){
                detilAgenModel.setKd_ao_da(cursor.getString(cursor.getColumnIndexOrThrow("KD_AO_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("CHECK_KD_PRIBADI_DA"))){
                detilAgenModel.setCheck_kd_pribadi_da(cursor.getInt(cursor.getColumnIndexOrThrow("CHECK_KD_PRIBADI_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KD_PNAGIHAN_DA"))){
                detilAgenModel.setKd_pnagihan_da(cursor.getString(cursor.getColumnIndexOrThrow("KD_PNAGIHAN_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NM_REGIONAL_PENAGIHAN_DA"))){
                detilAgenModel.setNm_Regional_Penagihan_da(cursor.getString(cursor.getColumnIndexOrThrow("NM_REGIONAL_PENAGIHAN_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("CHECK_BROKER_DA"))){
                detilAgenModel.setCheck_broker_da(cursor.getInt(cursor.getColumnIndexOrThrow("CHECK_BROKER_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("BROKER_DA"))){
                detilAgenModel.setBroker_da(cursor.getString(cursor.getColumnIndexOrThrow("BROKER_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NAMA_BROKER_DA"))){
                detilAgenModel.setNama_broker_da(cursor.getString(cursor.getColumnIndexOrThrow("NAMA_BROKER_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("BANK_DA"))){
                detilAgenModel.setBank_da(cursor.getInt(cursor.getColumnIndexOrThrow("BANK_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NOREK_DA"))){
                detilAgenModel.setNorek_da(cursor.getString(cursor.getColumnIndexOrThrow("NOREK_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PNYTAAN_SETUJU_DA"))){
                detilAgenModel.setPnytaan_setuju_da(cursor.getInt(cursor.getColumnIndexOrThrow("PNYTAAN_SETUJU_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ID_SPONSOR_DA"))){
                detilAgenModel.setId_sponsor_da(cursor.getString(cursor.getColumnIndexOrThrow("ID_SPONSOR_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ID_PENEMPATAN_DA"))){
                detilAgenModel.setId_penempatan_da(cursor.getString(cursor.getColumnIndexOrThrow("ID_PENEMPATAN_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ID_MEMBER_DA"))){
                detilAgenModel.setId_member_da(cursor.getString(cursor.getColumnIndexOrThrow("ID_MEMBER_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ID_REFFERAL_DA"))){
                detilAgenModel.setId_refferal_da(cursor.getString(cursor.getColumnIndexOrThrow("ID_REFFERAL_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NM_REFFERAL_DA"))){
                detilAgenModel.setNm_refferal_da(cursor.getString(cursor.getColumnIndexOrThrow("NM_REFFERAL_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("EMAIL_DA"))){
                detilAgenModel.setEmail_da(cursor.getString(cursor.getColumnIndexOrThrow("EMAIL_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KODE_REG1"))){
                detilAgenModel.setLca_id(cursor.getString(cursor.getColumnIndexOrThrow("KODE_REG1")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KODE_REG2"))){
                detilAgenModel.setLwk_id(cursor.getString(cursor.getColumnIndexOrThrow("KODE_REG2")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KODE_REG3"))){
                detilAgenModel.setLsrg_id(cursor.getString(cursor.getColumnIndexOrThrow("KODE_REG3")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("JENIS_LOGIN"))){
                detilAgenModel.setJENIS_LOGIN(cursor.getInt(cursor.getColumnIndexOrThrow("JENIS_LOGIN")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("JENIS_LOGIN_BC"))){
                detilAgenModel.setJENIS_LOGIN_BC(cursor.getInt(cursor.getColumnIndexOrThrow("JENIS_LOGIN_BC")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ID_CABANG_DA"))){
                detilAgenModel.setId_cabang_da(cursor.getString(cursor.getColumnIndexOrThrow("ID_CABANG_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NM_CABANG_DA"))){
                detilAgenModel.setNm_cabang_da(cursor.getString(cursor.getColumnIndexOrThrow("NM_CABANG_DA")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("VALIDATION"))){
                int valid = cursor.getInt(cursor.getColumnIndexOrThrow("VALIDATION"));
//                boolean isValid = valid == 1 ? true : false;
                boolean val;
                val = valid == 1;
                detilAgenModel.setValidation(val);
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("GROUP_ID_DA"))){
//                detilAgenModel.setGroup_id_da(cursor.getInt(cursor.getColumnIndexOrThrow("GROUP_ID_DA")));
//            } //dikomen dulu untuk versi 1.3.8
            cursor.close();
        }
        return detilAgenModel;
    }
}