package id.co.ajsmsig.nb.pointofsale.ui.mrecommendationsummary

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Analytics
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.RecommendationSummaryField
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import id.co.ajsmsig.nb.pointofsale.utils.Constants
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 08/03/18.
 */
class RecommendationSummaryVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository, var sharedViewModel: SharedViewModel, var mainRepository: MainRepository): AndroidViewModel(application){

    val observableFields = MediatorLiveData<List<RecommendationSummaryField>>()

    var page: Page? = null
    set(value) {
        if (field == null) {
            field = value

            val fields = repository.getAllRecommendationSummaryFieldWithRiskProfileId(sharedViewModel.selectedNeedAnalysis?.id)
            observableFields.addSource(fields, observableFields::setValue)
        }
    }

    fun sendFinishCycleEvent(fromStart: Boolean) {

        mainRepository.sendAnalytics(Analytics(
                agentCode = sharedViewModel.agentCode,
                agentName = sharedViewModel.agentName,
                group = sharedViewModel.groupId,
                event = if (fromStart) Constants.FINISH_CYCLE else Constants.UN_FINISH_CYCLE,
                timestamp = System.currentTimeMillis()
        ))
    }
}