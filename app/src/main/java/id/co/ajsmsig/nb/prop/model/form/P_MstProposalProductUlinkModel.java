package id.co.ajsmsig.nb.prop.model.form;

import android.os.Parcel;
import android.os.Parcelable;

public class P_MstProposalProductUlinkModel implements Parcelable{
    private String no_proposal_tab;
    private String no_proposal;
    private String lji_id;
    private Integer mdu_persen;

    public P_MstProposalProductUlinkModel() {
    }


    protected P_MstProposalProductUlinkModel(Parcel in) {
        no_proposal_tab = in.readString();
        no_proposal = in.readString();
        lji_id = in.readString();
        mdu_persen = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(no_proposal_tab);
        dest.writeString(no_proposal);
        dest.writeString(lji_id);
        dest.writeValue(mdu_persen);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<P_MstProposalProductUlinkModel> CREATOR = new Creator<P_MstProposalProductUlinkModel>() {
        @Override
        public P_MstProposalProductUlinkModel createFromParcel(Parcel in) {
            return new P_MstProposalProductUlinkModel(in);
        }

        @Override
        public P_MstProposalProductUlinkModel[] newArray(int size) {
            return new P_MstProposalProductUlinkModel[size];
        }
    };

    public String getNo_proposal_tab() {
        return no_proposal_tab;
    }

    public void setNo_proposal_tab(String no_proposal_tab) {
        this.no_proposal_tab = no_proposal_tab;
    }

    public String getNo_proposal() {
        return no_proposal;
    }

    public void setNo_proposal(String no_proposal) {
        this.no_proposal = no_proposal;
    }

    public String getLji_id() {
        return lji_id;
    }

    public void setLji_id(String lji_id) {
        this.lji_id = lji_id;
    }

    public Integer getMdu_persen() {
        return mdu_persen;
    }

    public void setMdu_persen(Integer mdu_persen) {
        this.mdu_persen = mdu_persen;
    }


}
