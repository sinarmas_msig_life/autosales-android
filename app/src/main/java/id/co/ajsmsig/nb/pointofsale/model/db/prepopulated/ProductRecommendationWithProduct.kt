package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.Embedded
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Product

/**
 * Created by andreyyoshuamanik on 02/03/18.
 */
class ProductRecommendationWithProduct: Option() {

        @Embedded
        var productRecommendation: ProductRecommendation? = null

        @Embedded
        var product: Product? = null
}