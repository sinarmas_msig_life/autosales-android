package id.co.ajsmsig.nb.pointofsale.binding

import android.databinding.BindingAdapter
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.widget.RadioButton
import android.widget.RadioGroup
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosRowInputRadiobuttonBinding
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.InputAllChoices
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.InputChoice

/**
 * Created by andreyyoshuamanik on 16/03/18.
 */

@BindingAdapter("app:entries")
fun setRadioGroupFromInputs(radioGroup: RadioGroup, inputAllChoices: InputAllChoices?) {

    inputAllChoices?.let {
        it.choices?.let {
            val inflater = LayoutInflater.from(radioGroup.context)
            for (inputChoice in it) {
                val dataBinding = DataBindingUtil.inflate<PosRowInputRadiobuttonBinding>(inflater, R.layout.pos_row_input_radiobutton, radioGroup, true)
                dataBinding.vm = inputChoice
                (dataBinding.root as? RadioButton)?.let {
                    it.id = inputChoice.id
                }
            }
        }

    }

}

@BindingAdapter("app:selectedItem")
fun setSpinnerSelectedItemFromInput(radioGroup: RadioGroup, choice: InputChoice?) {
    choice?.let { radioGroup.check(it.id) }
}
