package id.co.ajsmsig.nb.prop.model.hardCode;

public class Proposal {
	
//	public static final String MST_DATA_PROPOSAL = "MST_DATA_PROPOSAL";
//	public static final String MST_PROPOSAL_PRODUCT = "MST_PROPOSAL_PRODUCT";
//	public static final String MST_PROPOSAL_PRODUCT_PACKET = "MST_PROPOSAL_PRODUCT_PACKET";
//	public static final String MST_PROPOSAL_PRODUCT_RIDER = "MST_PROPOSAL_PRODUCT_RIDER";
//	public static final String MST_PROPOSAL_PRODUCT_PESERTA = "MST_PROPOSAL_PRODUCT_PESERTA";
//	public static final String MST_PROPOSAL_PRODUCT_TOPUP = "MST_PROPOSAL_PRODUCT_TOPUP";
//	public static final String MST_PROPOSAL_PRODUCT_ULINK = "MST_PROPOSAL_PRODUCT_ULINK";
	
//	public static final String PROPOSAL = "PROPOSAL";
//	public static final String NO_PROPOSAL = "NO_PROPOSAL";

//    public static final String SUPER_USER_MSAG_ID = "999999";
//    public static final String ACTUARIST_MSAG_ID = "999998";
//    public static final String TRAINER_MSAG_ID = "999997";
//    public static final String DMTM_MSAG_ID = "999996";
//    public static final String WORKSITE = "999995";
//    public static final String USER_ASM = "999994";
//    public static final String CLICKFORLIFE = "999993";
//    public static final String MALLASSURANCE = "999988";
//    public static final String MNC_TEST = "999990";
//    public static final String MNC = "999992";
//    public static final String FCD = "999991";
//    public static final String BANCASS = "999989";
//    public static final String ACTUARIST_VITO = "999987";
//    public static final String ACTUARIST_GUSTI= "999979";
//    public static final String ACTUARIST_MAYDA= "999985";
//    public static final String ACTUARIST_DWI_KUSUMA= "999984";
//    public static final String TRAINER_CO= "999983";
//    public static final String AGENCY_TEST= "999981";
//    public static final String ACTUARIST_FITRIA= "999980";
//    public static final String DEMO_AGENCY= "999982";
//    public static final String TIM_HAFRI= "999978";
//    public static final String SEKURITAS= "999977";
//    public static final String BANK_SINARMAS= "999976";
//    public static final String ACTUARIST_JOKO = "999975";
//    public static final String ALIVENET1  = "999974";
//    public static final String ALIVENET2  = "999973";
//    public static final String BISDEV_SYARIAH_EKO = "999972";
//    public static final String BISDEV_SYARIAH_HAFRI = "999971";
//    public static final String BISDEV_SYARIAH_JAZAI = "999970";
//    public static final String BISDEV_SYARIAH_SADEWO = "999969";
//    public static final String JAMIE_MSAG_ID = "999968";
//    public static final String UNDERWRITING_MSAG_ID = "999967";
//    public static final String CORPORATE_EB = "999966";
//    public static final String IT_SUPPORT = "999986";

//    public static final String GROUP_WORKSITE = "10";
//    public static final String GROUP_MNC = "15";
//    public static final String GROUP_MNC_TEST = "17";
//    public static final String GROUP_AGENCY = "7";
//    public static final String GROUP_FCD = "16";
//    public static final String GROUP_BANCASS = "18";
//    public static final String GROUP_BANCASS_SUPPORT = "29";
//    public static final String GROUP_BANK_SINARMAS = "23";
//    public static final String GROUP_SEKURITAS = "25";
//    public static final String GROUP_TIM_HAFRI = "26";
    public static final String GROUP_DMTM = "11";
//    public static final String GROUP_BISDEV_SYARIAH = "27";
//    public static final String GROUP_AKTUARIA = "3";
//    public static final String GROUP_UNDERWRITING = "30";
//    public static final String GROUP_CORPORATE_EB = "31";
//    public static final String GROUP_BRIDGE = "32";
    
    public static final Integer DUMMY_ZERO = 0;
    public static final boolean DUMMY_FALSE = false;

    public static final String CUR_IDR_CD = "01";
    public static final String CUR_USD_CD = "02";

    public static final int PAY_MODE_CD_SEKALIGUS = 0;
    public static final int PAY_MODE_CD_TRIWULANAN = 1;
    public static final int PAY_MODE_CD_SEMESTERAN = 2;
    public static final int PAY_MODE_CD_TAHUNAN = 3;
    public static final int PAY_MODE_CD_5_TAHUNAN = 4;
    public static final int PAY_MODE_CD_6_TAHUNAN = 5;
    public static final int PAY_MODE_CD_BULANAN = 6;
    public static final int PAY_MODE_CD_CICILAN = 7;
    public static final int PAY_MODE_CD_KWARTALAN = 8;
	
}
