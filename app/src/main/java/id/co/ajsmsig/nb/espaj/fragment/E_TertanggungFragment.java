package id.co.ajsmsig.nb.espaj.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.pdf.PdfRenderer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.TabSettings;
import com.itextpdf.text.pdf.PdfWriter;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.AutoCompleteTextViewClickListener;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.hardcode.RequestCode;
import id.co.ajsmsig.nb.espaj.activity.E_FormAlamatActivity;
import id.co.ajsmsig.nb.espaj.activity.E_FormAlamatAutoZipActivity;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.database.TableAddRiderUA;
import id.co.ajsmsig.nb.espaj.method.ExtendedViewPager;
import id.co.ajsmsig.nb.espaj.method.F_Hit_Umur;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.method.ProgressDialogClass;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;
import id.co.ajsmsig.nb.prop.method.ValueView;
import id.co.ajsmsig.nb.util.Const;

import static android.app.Activity.RESULT_OK;
import static id.co.ajsmsig.nb.crm.method.StaticMethods.toCalendar;

/**
 * Created by Bernard on 22/05/2017.
 */

public class E_TertanggungFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, AdapterView.OnItemClickListener, RadioGroup.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener, TextView.OnEditorActionListener {
    private boolean isJobDescriptionValid;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem menu_validasi = menu.findItem(R.id.menu_validasi);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.e_fragment_tertanggung_layout, container,
                false);
        me = E_MainActivity.e_bundle.getParcelable(getString(R.string.modelespaj));
        ButterKnife.bind(this, view);
        ((E_MainActivity) getActivity()).setSecondToolbar("Tertanggung (2/", 2);
        //AutoCompleteTextView

        ac_agama_tt.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_agama_tt, this));
        ac_agama_tt.setOnClickListener(this);
        ac_agama_tt.setOnFocusChangeListener(this);
        ac_agama_tt.addTextChangedListener(new generalTextWatcher(ac_agama_tt));

        ac_pendidikan_tt.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_pendidikan_tt, this));
        ac_pendidikan_tt.setOnClickListener(this);
        ac_pendidikan_tt.setOnFocusChangeListener(this);
        ac_pendidikan_tt.addTextChangedListener(new generalTextWatcher(ac_pendidikan_tt));

        ac_bukti_identitas_tt.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_bukti_identitas_tt, this));
        ac_bukti_identitas_tt.setOnClickListener(this);
        ac_bukti_identitas_tt.setOnFocusChangeListener(this);
        ac_bukti_identitas_tt.addTextChangedListener(new generalTextWatcher(ac_bukti_identitas_tt));

        ac_status_tt.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_status_tt, this));
        ac_status_tt.setOnClickListener(this);
        ac_status_tt.setOnFocusChangeListener(this);
        ac_status_tt.addTextChangedListener(new generalTextWatcher(ac_status_tt));

        ac_pendapatan_tt.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_pendapatan_tt, this));
        ac_pendapatan_tt.setOnClickListener(this);
        ac_pendapatan_tt.setOnFocusChangeListener(this);
        ac_pendapatan_tt.addTextChangedListener(new generalTextWatcher(ac_pendapatan_tt));

        ac_klasifikasi_pekerjaan_tt.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_klasifikasi_pekerjaan_tt, this));
        ac_klasifikasi_pekerjaan_tt.addTextChangedListener(new generalTextWatcher(ac_klasifikasi_pekerjaan_tt));

        ac_jabatan_tt.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jabatan_tt, this));
        ac_jabatan_tt.setOnClickListener(this);
        ac_jabatan_tt.setOnFocusChangeListener(this);
        ac_jabatan_tt.addTextChangedListener(new generalTextWatcher(ac_jabatan_tt));

        ac_warganegara_tt.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_warganegara_tt, this));
        ac_warganegara_tt.setOnClickListener(this);
        ac_warganegara_tt.setOnFocusChangeListener(this);
        ac_warganegara_tt.addTextChangedListener(new generalTextWatcher(ac_warganegara_tt));

        //button
//        btn_next_tt.setOnClickListener(this);
//        btn_prev_tt.setOnClickListener(this);

        //editText
        et_nama_tt.addTextChangedListener(new generalTextWatcher(et_nama_tt));
        et_tempat_tt.addTextChangedListener(new generalTextWatcher(et_tempat_tt));
        et_ttl_tt.addTextChangedListener(new generalTextWatcher(et_ttl_tt));
        et_usia_tt.addTextChangedListener(new generalTextWatcher(et_usia_tt));
        et_teks_agama_lain_tt.addTextChangedListener(new generalTextWatcher(et_teks_agama_lain_tt));
        et_gelar_tt.addTextChangedListener(new generalTextWatcher(et_gelar_tt));
        et_ibu_tt.addTextChangedListener(new generalTextWatcher(et_ibu_tt));
        et_teks_bukti_lain_tt.addTextChangedListener(new generalTextWatcher(et_teks_bukti_lain_tt));
        et_nomor_bukti_tt.addTextChangedListener(new generalTextWatcher(et_nomor_bukti_tt));
        et_tgl_berlaku_tt.addTextChangedListener(new generalTextWatcher(et_tgl_berlaku_tt));
        et_npwp_tt.addTextChangedListener(new generalTextWatcher(et_npwp_tt));
        et_editd_lainnya_tt.addTextChangedListener(new generalTextWatcher(et_editd_lainnya_tt));
        et_editt_lainnya_tt.addTextChangedListener(new generalTextWatcher(et_editt_lainnya_tt));
        et_nama_perusahaan_tt.addTextChangedListener(new generalTextWatcher(et_nama_perusahaan_tt));
        et_uraikan_tugas_tt.addTextChangedListener(new generalTextWatcher(et_uraikan_tugas_tt));

        et_ttl_tt.setOnClickListener(this);
        et_ttl_tt.setOnFocusChangeListener(this);

        et_tgl_berlaku_tt.setOnClickListener(this);
        et_tgl_berlaku_tt.setOnFocusChangeListener(this);


        //RadioGroup
        rg_green_card_tt.setOnCheckedChangeListener(this);
        rg_jekel_tt.setOnCheckedChangeListener(this);

        //CheckButton
        check_dana_tt = new ArrayList<CheckBox>();
        check_dana_tt.add(cb_checkd_gaji_tt);
        check_dana_tt.add(cb_checkd_tabungan_tt);
        check_dana_tt.add(cb_checkd_warisan_tt);
        check_dana_tt.add(cb_checkd_hibah_tt);
        check_dana_tt.add(cb_checkd_lainnya_tt);

        cb_checkd_gaji_tt.setOnCheckedChangeListener(this);
        cb_checkd_tabungan_tt.setOnCheckedChangeListener(this);
        cb_checkd_warisan_tt.setOnCheckedChangeListener(this);
        cb_checkd_hibah_tt.setOnCheckedChangeListener(this);
        cb_checkd_lainnya_tt.setOnCheckedChangeListener(this);

        check_tujuan_tt = new ArrayList<CheckBox>();
        check_tujuan_tt.add(cb_checkt_proteksi_tt);
        check_tujuan_tt.add(cb_checkt_inves_tt);
        check_tujuan_tt.add(cb_checkt_lainnya_tt);

        cb_checkt_proteksi_tt.setOnCheckedChangeListener(this);
        cb_checkt_inves_tt.setOnCheckedChangeListener(this);
        cb_checkt_lainnya_tt.setOnCheckedChangeListener(this);
        //TextView
        tv_isi_alamat_kantor_tt.setOnClickListener(this);
        tv_isi_alamat_rumah_tt.setOnClickListener(this);
        tv_isi_alamat_tempat_tinggal_tt.setOnClickListener(this);
        tv_ubah_alamat_kantor_tt.setOnClickListener(this);
        tv_ubah_alamat_rumah_tt.setOnClickListener(this);
        tv_ubah_tempat_tinggal_tt.setOnClickListener(this);
        tv_isi_hp_email_tt.setOnClickListener(this);
        tv_ubah_hp_email_tt.setOnClickListener(this);
        //ImageView
        img_alamat_kantor_tt.setOnClickListener(this);
        img_alamat_rumah_tt.setOnClickListener(this);
        img_alamat_tempat_tinggal_tt.setOnClickListener(this);
        img_hp_email_tt.setOnClickListener(this);
        //button di Activity
        btn_lanjut = getActivity().findViewById(R.id.btn_lanjut);
        btn_lanjut.setOnClickListener(this);
        btn_kembali = getActivity().findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(this);
        second_toolbar = ((E_MainActivity) getActivity()).getSecond_toolbar();
        iv_save = second_toolbar.findViewById(R.id.iv_save);
        iv_save.setOnClickListener(this);
        iv_next = second_toolbar.findViewById(R.id.iv_next);
        iv_next.setOnClickListener(this);
        iv_prev = second_toolbar.findViewById(R.id.iv_prev);
        iv_prev.setOnClickListener(this);

        restore();
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        ProgressDialogClass.removeSimpleProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        ProgressDialogClass.removeSimpleProgressDialog();
        MethodSupport.active_view(1, btn_lanjut);
        MethodSupport.active_view(1, iv_next);
        MethodSupport.active_view(1, btn_kembali);
        MethodSupport.active_view(1, iv_prev);

        try {
            MethodSupport.load_setDropXml(R.xml.e_agama, ac_agama_tt, getContext(), 0);
            MethodSupport.load_setDropXml(R.xml.e_identity, ac_bukti_identitas_tt, getContext(), 0);
            MethodSupport.load_setDropXml(R.xml.e_marital, ac_status_tt, getContext(), 0);
            MethodSupport.load_setDropXml(R.xml.e_pendidikan, ac_pendidikan_tt, getContext(), 0);
            MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_warganegara_tt, getContext(), 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        if(wn_tt==0) {
            ac_warganegara_tt.setText("INDONESIA");
            wn_tt=1;
        }else {
            try {
                MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_warganegara_tt, getContext(), 0);
            }catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
        }

        setAdapterPekerjaan("", ac_klasifikasi_pekerjaan_tt);
        setAdapterJabatan();
        setAdapterPenghasilan();

        checkForSelectedJobDescriptionIsOnTheList(ac_klasifikasi_pekerjaan_tt.getText().toString().trim(), ac_klasifikasi_pekerjaan_tt);
        //Make the job description field dots orange as default when user haven't select any job desc.
        if (TextUtils.isEmpty(ac_klasifikasi_pekerjaan_tt.getText().toString().trim())) {
            iv_circle_klasifikasi_pekerjaan_tt.setColorFilter(ContextCompat.getColor(getActivity(), R.color.orange));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_validasi:
                me.getTertanggungModel().setValidation(Validation());
                loadview();
                MyTaskValidasi taskValidasi = new MyTaskValidasi();
                taskValidasi.execute();
                Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTaskValidasi extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_TERTANGGUNG);
            return null;
        }
    }

    private class MyTaskSavePage extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_TERTANGGUNG);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //View di activity
            case R.id.btn_lanjut:
                onClickLanjut();
                break;
            case R.id.iv_next:
                onClickLanjut();
                break;
            case R.id.iv_save:
                String jobDescription = ac_klasifikasi_pekerjaan_tt.getText().toString().trim();
                if (TextUtils.isEmpty(jobDescription)) {
                    ac_klasifikasi_pekerjaan_tt.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
                    ac_klasifikasi_pekerjaan_tt.requestFocus();
                    return;
                }

                if (!isJobDescriptionValid) {

                    ac_klasifikasi_pekerjaan_tt.setError(getResources().getString(R.string.err_msg_jobdesc_not_found_on_the_list));

                    ac_klasifikasi_pekerjaan_tt.requestFocus();
                    return;
                }

                loadview();
                MyTaskSavePage taskSavePage = new MyTaskSavePage();
                taskSavePage.execute();

                break;
            case R.id.iv_prev:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.btn_kembali:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            //drop down
            case R.id.ac_agama_tt:
                ac_agama_tt.showDropDown();
                break;
            case R.id.ac_pendidikan_tt:
                ac_pendidikan_tt.showDropDown();
                break;
            case R.id.ac_bukti_identitas_tt:
                ac_bukti_identitas_tt.showDropDown();
                break;
            case R.id.ac_status_tt:
                ac_status_tt.showDropDown();
                break;
            case R.id.ac_pendapatan_tt:
                ac_pendapatan_tt.showDropDown();
                break;
            case R.id.ac_jabatan_tt:
                ac_jabatan_tt.showDropDown();
                break;
            case R.id.ac_warganegara_tt:
                try {
                    MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_warganegara_tt, getContext(), 0);
                }catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
                ac_warganegara_tt.showDropDown();
                break;
            //TextView
            case R.id.tv_isi_alamat_rumah_tt:
                goToForm(4);
                break;
            case R.id.tv_isi_alamat_kantor_tt:
                goToForm(5);
                break;
            case R.id.tv_isi_alamat_tempat_tinggal_tt:
                goToForm(6);
                break;
            case R.id.tv_ubah_alamat_rumah_tt:
                goToForm(4);
                break;
            case R.id.tv_ubah_alamat_kantor_tt:
                goToForm(5);
                break;
            case R.id.tv_ubah_tempat_tinggal_tt:
                goToForm(6);
                break;
            case R.id.tv_isi_hp_email_tt:
                goToForm(8);
                break;
            case R.id.tv_ubah_hp_email_tt:
                goToForm(8);
                break;
            //ImageView
            case R.id.img_alamat_rumah_tt:
                goToForm(4);
                break;
            case R.id.img_alamat_kantor_tt:
                goToForm(5);
                break;
            case R.id.img_alamat_tempat_tinggal_tt:
                goToForm(6);
                break;
            case R.id.img_hp_email_tt:
                goToForm(8);
                break;
            //EditText
            case R.id.et_ttl_tt:
                showDateDialog(1, et_ttl_tt.getText().toString(), et_ttl_tt);
                break;
            case R.id.et_tgl_berlaku_tt:
                showDateDialog(2, et_tgl_berlaku_tt.getText().toString(), et_tgl_berlaku_tt);
                break;
        }

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.ac_agama_tt:
                if (hasFocus) ac_agama_tt.showDropDown();
                break;
            case R.id.ac_pendidikan_tt:
                if (hasFocus) ac_pendidikan_tt.showDropDown();
                break;
            case R.id.ac_bukti_identitas_tt:
                if (hasFocus) ac_bukti_identitas_tt.showDropDown();
                break;
            case R.id.ac_status_tt:
                if (hasFocus) ac_status_tt.showDropDown();
                break;
            case R.id.ac_pendapatan_tt:
                if (hasFocus) ac_pendapatan_tt.showDropDown();
                break;
            case R.id.ac_jabatan_tt:
                if (hasFocus) ac_jabatan_tt.showDropDown();
                break;
            case R.id.et_ttl_tt:
                if (hasFocus) showDateDialog(1, et_ttl_tt.getText().toString(), et_ttl_tt);
                break;
            case R.id.et_tgl_berlaku_tt:
                if (hasFocus)
                    showDateDialog(1, et_tgl_berlaku_tt.getText().toString(), et_tgl_berlaku_tt);
                break;
            case R.id.ac_warganegara_tt:
                try {
                    MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_warganegara_tt, getContext(), 0);
                }catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                }
                if (hasFocus) ac_warganegara_tt.showDropDown();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.ac_agama_tt:
                ModelDropdownInt modelAgama_tt = (ModelDropdownInt) parent.getAdapter().getItem(position);
                agama_tt = modelAgama_tt.getId();
                val_agama(agama_tt);
                break;
            case R.id.ac_klasifikasi_pekerjaan_tt:
                //To make sure job description is only allowed picked from the dropdown
                isJobDescriptionValid = true;
                iv_circle_klasifikasi_pekerjaan_tt.setColorFilter(ContextCompat.getColor(getActivity(), R.color.green));
                break;
            case R.id.ac_pendidikan_tt:
                ModelDropdownInt modelPendidikan_tt = (ModelDropdownInt) parent.getAdapter().getItem(position);
                pend_tt = modelPendidikan_tt.getId();
                break;
            case R.id.ac_bukti_identitas_tt:
                ModelDropdownInt modelBuktiIdentitas_tt = (ModelDropdownInt) parent.getAdapter().getItem(position);
                bukti_tt = modelBuktiIdentitas_tt.getId();
                val_bukti(bukti_tt);
                break;
            case R.id.ac_status_tt:
                ModelDropdownInt modelStatus_tt = (ModelDropdownInt) parent.getAdapter().getItem(position);
                stat_tt = modelStatus_tt.getId();
                break;
            case R.id.ac_pendapatan_tt:
                str_penghasilan_thn_tt = ac_pendapatan_tt.getText().toString();
                break;
            case R.id.ac_jabatan_tt:
                ModelDropDownString modelJabatan_tt = (ModelDropDownString) parent.getAdapter().getItem(position);
                str_jabatan_klasifikasi_tt = modelJabatan_tt.getId();
                break;
            case R.id.ac_warganegara_tt:
                ModelDropdownInt ModelWarga = (ModelDropdownInt) parent.getAdapter().getItem(position);
                wn_tt = ModelWarga.getId();
//                val_bukti(bukti_tt);
                break;
        }
    }

    //khusus RadioGroup
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
        switch (radioGroup.getId()) {
            case R.id.rg_green_card_tt:
                greencard_tt = MethodSupport.OnCheckRadio(rg_green_card_tt, checkedId);
                break;
            case R.id.rg_jekel_tt:
                jekel_tt = MethodSupport.OnCheckRadio(rg_jekel_tt, checkedId);
                iv_circle_jekel_tt.setColorFilter(ContextCompat.getColor(getContext(), R.color.green));
                break;
        }
    }

    //khusus Checkbox
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_checkd_gaji_tt:
                dana_gaji_tt = (isChecked) ? cb_checkd_gaji_tt.getText().toString() : "";
                bool_dana_tt = Method_Validator.ValidCheckDana(check_dana_tt, iv_circle_checkd_gaji_tt, getContext());
                break;
            case R.id.cb_checkd_tabungan_tt:
                dana_tabungan_tt = (isChecked) ? cb_checkd_tabungan_tt.getText().toString() : "";
                bool_dana_tt = Method_Validator.ValidCheckDana(check_dana_tt, iv_circle_checkd_gaji_tt, getContext());
                break;
            case R.id.cb_checkd_warisan_tt:
                dana_warisan_tt = (isChecked) ? cb_checkd_warisan_tt.getText().toString() : "";
                bool_dana_tt = Method_Validator.ValidCheckDana(check_dana_tt, iv_circle_checkd_gaji_tt, getContext());
                break;
            case R.id.cb_checkd_hibah_tt:
                dana_hibah_tt = (isChecked) ? cb_checkd_hibah_tt.getText().toString() : "";
                bool_dana_tt = Method_Validator.ValidCheckDana(check_dana_tt, iv_circle_checkd_gaji_tt, getContext());
                break;
            case R.id.cb_checkd_lainnya_tt:
                dana_lainnya_tt = (isChecked) ? cb_checkd_lainnya_tt.getText().toString() : "";
                bool_dana_tt = Method_Validator.ValidCheckDana(check_dana_tt, iv_circle_checkd_gaji_tt, getContext());
                break;
            case R.id.cb_checkt_proteksi_tt:
                tujuan_proteksi_tt = (isChecked) ? cb_checkt_proteksi_tt.getText().toString() : "";
                bool_tujuan_tt = Method_Validator.ValidCheckDana(check_tujuan_tt, iv_circle_checkt_proteksi_tt, getContext());
                break;
            case R.id.cb_checkt_inves_tt:
                tujuan_inves_tt = (isChecked) ? cb_checkt_inves_tt.getText().toString() : "";
                bool_tujuan_tt = Method_Validator.ValidCheckDana(check_tujuan_tt, iv_circle_checkt_proteksi_tt, getContext());
                break;
            case R.id.cb_checkt_lainnya_tt:
                tujuan_lainnya_tt = (isChecked) ? cb_checkt_lainnya_tt.getText().toString() : "";
                bool_tujuan_tt = Method_Validator.ValidCheckDana(check_tujuan_tt, iv_circle_checkt_proteksi_tt, getContext());
                break;
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return false;
    }

    private class generalTextWatcher implements TextWatcher {
        private View view;

        generalTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                //autocompletetextview
                case R.id.ac_agama_tt:
                    if (StaticMethods.isTextWidgetEmpty(ac_agama_tt)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_agama_tt.setColorFilter(color);
                    tv_error_agama_tt.setVisibility(View.GONE);
                    break;
                case R.id.ac_pendidikan_tt:
                    if (StaticMethods.isTextWidgetEmpty(ac_pendidikan_tt)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_pendidikan_tt.setColorFilter(color);
                    tv_error_pendidikan_tt.setVisibility(View.GONE);
                    break;
                case R.id.ac_bukti_identitas_tt:
                    if (StaticMethods.isTextWidgetEmpty(ac_bukti_identitas_tt)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_bukti_identitas_tt.setColorFilter(color);
                    tv_error_bukti_identitas_tt.setVisibility(View.GONE);
                    break;
                case R.id.ac_status_tt:
                    if (StaticMethods.isTextWidgetEmpty(ac_status_tt)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_status_tt.setColorFilter(color);
                    tv_error_status_tt.setVisibility(View.GONE);
                    break;
                case R.id.ac_pendapatan_tt:
                    if (StaticMethods.isTextWidgetEmpty(ac_pendapatan_tt)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_pendapatan_tt.setColorFilter(color);
                    tv_error_pendapatan_tt.setVisibility(View.GONE);
                    break;
                case R.id.ac_klasifikasi_pekerjaan_tt:
                    setAdapterPekerjaan(ac_klasifikasi_pekerjaan_tt.getText().toString().trim(), ac_klasifikasi_pekerjaan_tt);
                    validateJobDescription(ac_klasifikasi_pekerjaan_tt.getText().toString().trim(), ac_klasifikasi_pekerjaan_tt);
                    break;
                case R.id.ac_jabatan_tt:
                    if (StaticMethods.isTextWidgetEmpty(ac_jabatan_tt)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jabatan_tt.setColorFilter(color);
                    tv_error_jabatan_tt.setVisibility(View.GONE);
                    break;
                case R.id.ac_warganegara_tt:
                    if (StaticMethods.isTextWidgetEmpty(ac_warganegara_tt)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_warganegara_tt.setColorFilter(color);
                    tv_error_warganegara_tt.setVisibility(View.GONE);
                    break;

                //EditText
                case R.id.et_nama_tt:
                    Method_Validator.ValEditWatcher(et_nama_tt, iv_circle_nama_tt,
                            tv_error_nama_tt, getContext());
                    break;
                case R.id.et_tempat_tt:
                    Method_Validator.ValEditWatcher(et_tempat_tt, iv_circle_tempat_tt,
                            tv_error_tempat_tt, getContext());
                    break;
                case R.id.et_ttl_tt:
                    Method_Validator.ValEditWatcher(et_ttl_tt, iv_circle_ttl_tt,
                            tv_error_ttl_tt, getContext());
                    break;
                case R.id.et_usia_tt:
                    Method_Validator.ValEditWatcher(et_usia_tt, iv_circle_usia_tt,
                            tv_error_usia_tt, getContext());
                    break;
                case R.id.et_teks_agama_lain_tt:
                    Method_Validator.ValEditWatcher(et_teks_agama_lain_tt, iv_circle_teks_agama_lain_tt,
                            tv_error_teks_agama_lain_tt, getContext());
                    break;
                case R.id.et_ibu_tt:
                    Method_Validator.ValEditWatcher(et_ibu_tt, iv_circle_ibu_tt,
                            tv_error_ibu_tt, getContext());
                    break;
                case R.id.et_teks_bukti_lain_tt:
                    Method_Validator.ValEditWatcher(et_teks_bukti_lain_tt, iv_circle_teks_bukti_lain_tt,
                            tv_error_teks_bukti_lain_tt, getContext());
                    break;
                case R.id.et_nomor_bukti_tt:
                    Method_Validator.ValEditWatcher(et_nomor_bukti_tt, iv_circle_nomor_bukti_tt,
                            tv_error_nomor_bukti_tt, getContext());
                    break;
                case R.id.et_tgl_berlaku_tt:
                    Method_Validator.ValEditWatcher(et_tgl_berlaku_tt, iv_circle_tgl_berlaku_tt,
                            tv_error_tgl_berlaku_tt, getContext());
                    break;
                case R.id.et_nama_perusahaan_tt:
                    Method_Validator.ValEditWatcher(et_nama_perusahaan_tt, iv_circle_nama_perusahaan_tt,
                            tv_error_nama_perusahaan_tt, getContext());
                    break;
                case R.id.et_uraikan_tugas_tt:
                    Method_Validator.ValEditWatcher(et_uraikan_tugas_tt, iv_circle_uraikan_tugas_tt,
                            tv_error_uraikan_tugas_tt, getContext());
                    break;

            }
        }
    }

    private void loadview() {
        me.getTertanggungModel().setNama_tt(et_nama_tt.getText().toString());
        me.getTertanggungModel().setGelar_tt(et_gelar_tt.getText().toString());
        me.getTertanggungModel().setNama_ibu_tt(et_ibu_tt.getText().toString());
        me.getTertanggungModel().setBukti_identitas_tt(bukti_tt);
        me.getTertanggungModel().setNo_identitas_tt(et_nomor_bukti_tt.getText().toString());
        me.getTertanggungModel().setTgl_berlaku_tt(et_tgl_berlaku_tt.getText().toString());
        me.getTertanggungModel().setWarga_negara_tt(wn_tt);
        me.getTertanggungModel().setTempat_tt(et_tempat_tt.getText().toString());
        me.getTertanggungModel().setTtl_tt(et_ttl_tt.getText().toString());
        me.getTertanggungModel().setUsia_tt(et_usia_tt.getText().toString());
        me.getTertanggungModel().setJekel_tt(jekel_tt);
        me.getTertanggungModel().setStatus_tt(stat_tt);
        me.getTertanggungModel().setAgama_tt(agama_tt);
        me.getTertanggungModel().setAgama_lain_tt(et_teks_agama_lain_tt.getText().toString());
        me.getTertanggungModel().setPendidikan_tt(pend_tt);

//        me.getTertanggungModel().setAlamat_tt();
//        me.getTertanggungModel().setKota_tt();
//        me.getTertanggungModel().setKdpos_tt();
//        me.getTertanggungModel().setKdtelp1_tt();
//        me.getTertanggungModel().setTelp1_tt();
//        me.getTertanggungModel().setKdtelp2_tt();
//        me.getTertanggungModel().setTelp2_tt();
//        me.getTertanggungModel().setAlamat_kantor_tt();
//        me.getTertanggungModel().setKota_kantor_tt();
//        me.getTertanggungModel().setKdpos_kantor_tt();
//        me.getTertanggungModel().setKdtelp1_kantor_tt();
//        me.getTertanggungModel().setTelp1_kantor_tt();
//        me.getTertanggungModel().setKdtelp2_kantor_tt();
//        me.getTertanggungModel().setTelp2_kantor_tt();
//        me.getTertanggungModel().setFax_kantor_tt();
//        me.getTertanggungModel().setAlamat_tinggal_tt();
//        me.getTertanggungModel().setKota_tinggal_tt();
//        me.getTertanggungModel().setKdpos_tinggal_tt();
//        me.getTertanggungModel().setKdtelp1_tinggal_tt();
//        me.getTertanggungModel().setTelp1_kantor_tt();
//        me.getTertanggungModel().setKota_tinggal_tt();
//        me.getTertanggungModel().setTelp2_tinggal_tt();
//        me.getTertanggungModel().setKota_tinggal_tt();
//        me.getTertanggungModel().setFax_tinggal_tt();
//        me.getTertanggungModel().setHp1_tt();
//        me.getTertanggungModel().setHp2_tt();
//        me.getTertanggungModel().setEmail_tt();

        me.getTertanggungModel().setPenghasilan_thn_tt(str_penghasilan_thn_tt);
        me.getTertanggungModel().setKlasifikasi_pekerjaan_tt(ac_klasifikasi_pekerjaan_tt.getText().toString());
        me.getTertanggungModel().setUraian_pekerjaan_tt(et_uraikan_tugas_tt.getText().toString());
        me.getTertanggungModel().setJabatan_klasifikasi_tt(str_jabatan_klasifikasi_tt);
        me.getTertanggungModel().setGreencard_tt(greencard_tt);
        me.getTertanggungModel().setAlias_tt(et_alias_tt.getText().toString());
        me.getTertanggungModel().setNm_perusahaan_tt(et_nama_perusahaan_tt.getText().toString());
        me.getTertanggungModel().setNpwp_tt(et_npwp_tt.getText().toString());
        me.getTertanggungModel().setDana_gaji_tt(dana_gaji_tt);
        me.getTertanggungModel().setDana_tabungan_tt(dana_tabungan_tt);
        me.getTertanggungModel().setDana_warisan_tt(dana_warisan_tt);
        me.getTertanggungModel().setDana_hibah_tt(dana_hibah_tt);
        me.getTertanggungModel().setDana_lainnya_tt(dana_lainnya_tt);
        me.getTertanggungModel().setEdit_d_lainnya_tt(et_editd_lainnya_tt.getText().toString());
        me.getTertanggungModel().setTujuan_proteksi_tt(tujuan_proteksi_tt);
        me.getTertanggungModel().setTujuan_inves_tt(tujuan_inves_tt);
        me.getTertanggungModel().setTujuan_lainnya_tt(tujuan_lainnya_tt);
        me.getTertanggungModel().setEdit_t_lainnya_tt(et_editt_lainnya_tt.getText().toString());
        E_MainActivity.e_bundle.putParcelable(getString(R.string.modelespaj), me);
    }

    private void restore() {
        try {
            //edittext
            et_nama_tt.setText(me.getTertanggungModel().getNama_tt());
            et_gelar_tt.setText(me.getTertanggungModel().getGelar_tt());
            et_ibu_tt.setText(me.getTertanggungModel().getNama_ibu_tt());
            et_nomor_bukti_tt.setText(me.getTertanggungModel().getNo_identitas_tt());
            et_tgl_berlaku_tt.setText(me.getTertanggungModel().getTgl_berlaku_tt());
            et_tempat_tt.setText(me.getTertanggungModel().getTempat_tt());
            et_ttl_tt.setText(me.getTertanggungModel().getTtl_tt());
            et_usia_tt.setText(me.getTertanggungModel().getUsia_tt());
            et_teks_agama_lain_tt.setText(me.getTertanggungModel().getAgama_lain_tt());
            et_uraikan_tugas_tt.setText(me.getTertanggungModel().getUraian_pekerjaan_tt());
            et_nama_perusahaan_tt.setText(me.getTertanggungModel().getNm_perusahaan_tt());
            et_npwp_tt.setText(me.getTertanggungModel().getNpwp_tt());

            //dropdown

            bukti_tt = me.getTertanggungModel().getBukti_identitas_tt();
            ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_identity, 0);
            ac_bukti_identitas_tt.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, bukti_tt));
            val_bukti(bukti_tt);

            wn_tt = me.getTertanggungModel().getWarga_negara_tt();
            ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_warganegara, 0);
            ac_warganegara_tt.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, wn_tt));

            stat_tt = me.getTertanggungModel().getStatus_tt();
            ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_marital, 0);
            ac_status_tt.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, stat_tt));

            agama_tt = me.getTertanggungModel().getAgama_tt();
            ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_agama, 0);
            ac_agama_tt.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, agama_tt));
            val_agama(agama_tt);

            pend_tt = me.getTertanggungModel().getPendidikan_tt();
            ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_pendidikan, 0);
            ac_pendidikan_tt.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, pend_tt));

            str_penghasilan_thn_tt = me.getTertanggungModel().getPenghasilan_thn_tt();
            Adapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.penghasilan));
            ac_pendapatan_tt.setText(str_penghasilan_thn_tt);

            ac_klasifikasi_pekerjaan_tt.setText(me.getTertanggungModel().getKlasifikasi_pekerjaan_tt());

            str_jabatan_klasifikasi_tt = me.getTertanggungModel().getJabatan_klasifikasi_tt();
            ListDropDownString = new E_Select(getContext()).getLstJabatan();
            ac_jabatan_tt.setText(str_jabatan_klasifikasi_tt);
            //RadioGroup
            jekel_tt = me.getTertanggungModel().getJekel_tt();
            MethodSupport.OnsetTwoRadio(rg_jekel_tt, jekel_tt);
            iv_circle_jekel_tt.setColorFilter(ContextCompat.getColor(getContext(), R.color.green));
            greencard_tt = me.getTertanggungModel().getGreencard_tt();
            MethodSupport.OnsetTwoRadio(rg_green_card_tt, greencard_tt);
            //Checkbox
            dana_gaji_tt = me.getTertanggungModel().getDana_gaji_tt();
            if (!dana_gaji_tt.equals("")) {
                cb_checkd_gaji_tt.setChecked(true);
            }
            dana_tabungan_tt = me.getTertanggungModel().getDana_tabungan_tt();
            if (!dana_tabungan_tt.equals("")) {
                cb_checkd_tabungan_tt.setChecked(true);
            }
            dana_warisan_tt = me.getTertanggungModel().getDana_warisan_tt();
            if (!dana_warisan_tt.equals("")) {
                cb_checkd_warisan_tt.setChecked(true);
            }
            dana_hibah_tt = me.getTertanggungModel().getDana_hibah_tt();
            if (!dana_hibah_tt.equals("")) {
                cb_checkd_hibah_tt.setChecked(true);
            }
            dana_lainnya_tt = me.getTertanggungModel().getDana_lainnya_tt();
            if (!dana_lainnya_tt.equals("")) {
                cb_checkd_lainnya_tt.setChecked(true);
            }
            et_editd_lainnya_tt.setText(me.getTertanggungModel().getEdit_d_lainnya_tt());
            tujuan_proteksi_tt = me.getTertanggungModel().getTujuan_proteksi_tt();
            if (!tujuan_proteksi_tt.equals("")) {
                cb_checkt_proteksi_tt.setChecked(true);
            }
            tujuan_inves_tt = me.getTertanggungModel().getTujuan_inves_tt();
            if (!tujuan_inves_tt.equals("")) {
                cb_checkt_inves_tt.setChecked(true);
            }
            tujuan_lainnya_tt = me.getTertanggungModel().getTujuan_lainnya_tt();
            if (!tujuan_lainnya_tt.equals("")) {
                cb_checkt_lainnya_tt.setChecked(true);
            }
            et_editt_lainnya_tt.setText(me.getTertanggungModel().getEdit_t_lainnya_tt());
            //radioGroup
            jekel_tt = me.getTertanggungModel().getJekel_tt();
            MethodSupport.OnsetTwoRadio(rg_jekel_tt, jekel_tt);
            greencard_tt = me.getTertanggungModel().getGreencard_tt();
            MethodSupport.OnsetTwoRadio(rg_green_card_tt, greencard_tt);
            if (!me.getModelID().getProposal_tab().equals("")) {
                onDeactiveView();
            }
            updateAlamatRmh();
            updateAlamatKantor();
            updateAlamatTinggal();
            UpdateHP();
            if (me.getPemegangPolisModel().getHubungan_pp() == 1) {
                MethodSupport.disable(false, cl_child_tt);
            }
            if (me.getModelID().getJns_spaj() == 2) {
                cl_alias_tt.setVisibility(View.VISIBLE);
            } else {
                cl_alias_tt.setVisibility(View.GONE);
            }
            if (!me.getValidation()) {
                Validation();
            }

            if (me.getModelID().getFlag_aktif() == 1) {
                MethodSupport.disable(false, cl_child_tt);
            }
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    private void onDeactiveView() {
        MethodSupport.active_view(0, et_nama_tt);
        MethodSupport.active_view(0, et_ttl_tt);
        MethodSupport.active_view(0, et_usia_tt);
        MethodSupport.active_radio(0, rg_jekel_tt);
    }


    private void setAdapterJabatan() {
        ArrayList<ModelDropDownString> Dropdown = new E_Select(getContext()).getLstJabatan();
        ArrayAdapter<ModelDropDownString> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_jabatan_tt.setAdapter(Adapter);
    }

    private void setAdapterPekerjaan(String jobDesc, AutoCompleteTextView ac_kerja) {
        ArrayList<ModelDropdownInt> result = new E_Select(getContext()).getLst_Pekerjaan(jobDesc);
        ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, result);
        ac_kerja.setAdapter(Adapter);
    }


    /**
     * To determine is user selected job description is from dropdown or not
     * If user type the job description, the input will not be valid
     * User will have to select from the dropdown
     *
     * @param searchQuery
     * @param ac_kerja
     * @return
     */
    private void validateJobDescription(String searchQuery, AutoCompleteTextView ac_kerja) {
        //Check if job desc search query empty
        if (!TextUtils.isEmpty(searchQuery)) {
            ac_kerja.setError(null);
            iv_circle_klasifikasi_pekerjaan_tt.setColorFilter(ContextCompat.getColor(getActivity(), R.color.orange));

            //Search from DB
            ArrayList<ModelDropdownInt> result = new E_Select(getActivity()).getLst_Pekerjaan(searchQuery);
            if (result.isEmpty()) {
                ac_kerja.setError(getString(R.string.err_msg_jobdesc_not_found_on_the_list));
            } else {
                ac_kerja.setError(null);
                iv_circle_klasifikasi_pekerjaan_tt.setColorFilter(ContextCompat.getColor(getContext(), R.color.green));
                ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, result);
                ac_kerja.setAdapter(Adapter);
            }
            // To make sure user only pick job desc from dropdown so we make it false
            isJobDescriptionValid = false;

        } else {
            ac_kerja.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
            iv_circle_klasifikasi_pekerjaan_tt.setColorFilter(ContextCompat.getColor(getActivity(), R.color.orange));

            // To make sure user only pick job desc from dropdown so we make it false
            isJobDescriptionValid = false;
        }
    }

    /**
     * Check whether user previously selected job description is on the suggestion list
     * Invoked when user go to this process from the previous step
     *
     * @param selectedJobDesc
     * @param ac_kerja
     * @return
     */
    private void checkForSelectedJobDescriptionIsOnTheList(String selectedJobDesc, AutoCompleteTextView ac_kerja) {
        //Search from DB
        ArrayList<ModelDropdownInt> result = new E_Select(getActivity()).getLst_Pekerjaan(selectedJobDesc);
        if (result.isEmpty()) {
            ac_kerja.setError(getString(R.string.err_msg_jobdesc_not_found_on_the_list));
            isJobDescriptionValid = false;
        } else {
            ac_kerja.setError(null);
            iv_circle_klasifikasi_pekerjaan_tt.setColorFilter(ContextCompat.getColor(getActivity(), R.color.green));
            ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, result);
            ac_kerja.setAdapter(Adapter);
            isJobDescriptionValid = true;
        }
    }

    private void setAdapterPenghasilan() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.penghasilan));
        ac_pendapatan_tt.setAdapter(Adapter);
    }

    private boolean Validation() {
        boolean val = true;
        int color = ContextCompat.getColor(getContext(), R.color.red);

        //RadioGroup

        if (!Method_Validator.onEmptyRadio(rg_green_card_tt, iv_circle_green_card_tt, tv_error_green_card_tt, cl_green_card_tt, getContext())) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, rg_green_card_tt, tv_error_green_card_tt, iv_circle_green_card_tt, color, getContext(), getString(R.string.error_field_required));
        }

        if (!Method_Validator.onEmptyRadio(rg_jekel_tt, iv_circle_jekel_tt, tv_error_jekel_tt, cl_jekel_tt, getContext())) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, rg_jekel_tt, tv_error_jekel_tt, iv_circle_jekel_tt, color, getContext(), getString(R.string.error_field_required));
        }

        //EditText
        if (et_nama_tt.isShown() && et_nama_tt.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, et_nama_tt, tv_error_nama_tt, iv_circle_nama_tt, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nama_tt.setVisibility(View.VISIBLE);
//            tv_error_nama_tt.setText(getString(R.string.error_field_required));
//            iv_circle_nama_tt.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_nama_tt.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_tt, cl_child_tt, et_nama_tt, tv_error_nama_tt, iv_circle_nama_tt, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_tempat_tt.isShown() && et_tempat_tt.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, et_tempat_tt, tv_error_tempat_tt, iv_circle_tempat_tt, color, getContext(), getString(R.string.error_field_required));
//            tv_error_tempat_tt.setVisibility(View.VISIBLE);
//            tv_error_tempat_tt.setText(getString(R.string.error_field_required));
//            iv_circle_tempat_tt.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_tempat_tt.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_tt, cl_child_tt, et_tempat_tt, tv_error_tempat_tt, iv_circle_tempat_tt, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_ttl_tt.isShown() && et_ttl_tt.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, et_ttl_tt, tv_error_ttl_tt, iv_circle_ttl_tt, color, getContext(), getString(R.string.error_field_required));
//            tv_error_ttl_tt.setVisibility(View.VISIBLE);
//            tv_error_ttl_tt.setText(getString(R.string.error_field_required));
//            iv_circle_ttl_tt.setColorFilter(color);
        }

        if (et_ibu_tt.isShown() && et_ibu_tt.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, et_ibu_tt, tv_error_ibu_tt, iv_circle_ibu_tt, color, getContext(), getString(R.string.error_field_required));
//            tv_error_ibu_tt.setVisibility(View.VISIBLE);
//            tv_error_ibu_tt.setText(getString(R.string.error_field_required));
//            iv_circle_ibu_tt.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_ibu_tt.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_tt, cl_child_tt, et_ibu_tt, tv_error_ibu_tt, iv_circle_ibu_tt, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_nomor_bukti_tt.isShown() && et_nomor_bukti_tt.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, et_nomor_bukti_tt, tv_error_nomor_bukti_tt, iv_circle_nomor_bukti_tt, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nomor_bukti_tt.setVisibility(View.VISIBLE);
//            tv_error_nomor_bukti_tt.setText(getString(R.string.error_field_required));
//            iv_circle_nomor_bukti_tt.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_nomor_bukti_tt.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_tt, cl_child_tt, et_nomor_bukti_tt, tv_error_nomor_bukti_tt, iv_circle_nomor_bukti_tt, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (ac_bukti_identitas_tt.isShown() && ac_bukti_identitas_tt.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, ac_bukti_identitas_tt, tv_error_bukti_identitas_tt, iv_circle_bukti_identitas_tt, color, getContext(), getString(R.string.error_field_required));
//            tv_error_bukti_identitas_tt.setVisibility(View.VISIBLE);
//            tv_error_bukti_identitas_tt.setText(getString(R.string.error_field_required));
//            iv_circle_bukti_identitas_tt.setColorFilter(color);
        }
        if (ac_warganegara_tt.isShown() && ac_warganegara_tt.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, ac_warganegara_tt, tv_error_warganegara_tt, iv_circle_warganegara_tt, color, getContext(), getString(R.string.error_field_required));
//            tv_error_warganegara_tt.setVisibility(View.VISIBLE);
//            tv_error_warganegara_tt.setText(getString(R.string.error_field_required));
//            iv_circle_warganegara_tt.setColorFilter(color);
        }
        if (ac_status_tt.isShown() && ac_status_tt.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, ac_status_tt, tv_error_status_tt, iv_circle_status_tt, color, getContext(), getString(R.string.error_field_required));
//            tv_error_status_tt.setVisibility(View.VISIBLE);
//            tv_error_status_tt.setText(getString(R.string.error_field_required));
//            iv_circle_status_tt.setColorFilter(color);
        }
        if (ac_agama_tt.isShown() && ac_agama_tt.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, ac_agama_tt, tv_error_agama_tt, iv_circle_agama_tt, color, getContext(), getString(R.string.error_field_required));
//            tv_error_agama_tt.setVisibility(View.VISIBLE);
//            tv_error_agama_tt.setText(getString(R.string.error_field_required));
//            iv_circle_agama_tt.setColorFilter(color);
        }
        if (ac_pendidikan_tt.isShown() && ac_pendidikan_tt.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, ac_pendidikan_tt, tv_error_pendidikan_tt, iv_circle_pendidikan_tt, color, getContext(), getString(R.string.error_field_required));
//            tv_error_pendidikan_tt.setVisibility(View.VISIBLE);
//            tv_error_pendidikan_tt.setText(getString(R.string.error_field_required));
//            iv_circle_pendidikan_tt.setColorFilter(color);
        }
        if (ac_pendapatan_tt.isShown() && ac_pendapatan_tt.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, ac_pendapatan_tt, tv_error_pendapatan_tt, iv_circle_pendapatan_tt, color, getContext(), getString(R.string.error_field_required));
//            tv_error_pendapatan_tt.setVisibility(View.VISIBLE);
//            tv_error_pendapatan_tt.setText(getString(R.string.error_field_required));
//            iv_circle_pendapatan_tt.setColorFilter(color);
        }
        if (ac_klasifikasi_pekerjaan_tt.isShown() && ac_klasifikasi_pekerjaan_tt.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_tt, cl_child_tt, ac_klasifikasi_pekerjaan_tt, tv_error_klasifikasi_pekerjaan_tt, iv_circle_klasifikasi_pekerjaan_tt, color, getContext(), getString(R.string.error_field_required));
//            tv_error_klasifikasi_pekerjaan_tt.setVisibility(View.VISIBLE);
//            tv_error_klasifikasi_pekerjaan_tt.setText(getString(R.string.error_field_required));
//            iv_circle_klasifikasi_pekerjaan_tt.setColorFilter(color);
        }

        if (me.getTertanggungModel().getAlamat_tt().equals("")) {
            tv_error_alamat_rumah_tt.setVisibility(View.VISIBLE);
            val = false;
        } else {
            tv_error_alamat_rumah_tt.setVisibility(View.GONE);
        }

        if (jekel_tt == -1) {
            val = false;
            tv_error_jekel_tt.setVisibility(View.VISIBLE);
            tv_error_jekel_tt.setText(getString(R.string.error_field_required));
            iv_circle_jekel_tt.setColorFilter(color);
        }

        if (!bool_dana_tt) {
            val = false;
        }
        if (!bool_tujuan_tt) {
            val = false;
        }
        return val;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCode.FILL_ALAMAT_SPAJ) {
            if (resultCode == RESULT_OK) {
                EspajModel modelUpdate = data.getParcelableExtra(getString(R.string.modelespaj));
                switch (data.getIntExtra(getString(R.string.flag), 0)) {
                    case 4:
                        me.getTertanggungModel().setAlamat_tt(modelUpdate.getTertanggungModel().getAlamat_tt());
//                        me.getTertanggungModel().setKota_tt(modelUpdate.getTertanggungModel().getKota_tt());
                        me.getTertanggungModel().setPropinsi_tt(modelUpdate.getTertanggungModel().getPropinsi_tt());
                        me.getTertanggungModel().setKabupaten_tt(modelUpdate.getTertanggungModel().getKabupaten_tt());
                        me.getTertanggungModel().setKecamatan_tt(modelUpdate.getTertanggungModel().getKecamatan_tt());
                        me.getTertanggungModel().setKelurahan_tt(modelUpdate.getTertanggungModel().getKelurahan_tt());
                        me.getTertanggungModel().setKdpos_tt(modelUpdate.getTertanggungModel().getKdpos_tt());
                        me.getTertanggungModel().setKdtelp1_tt(modelUpdate.getTertanggungModel().getKdtelp1_tt());
                        me.getTertanggungModel().setTelp1_tt(modelUpdate.getTertanggungModel().getTelp1_tt());
                        me.getTertanggungModel().setKdtelp2_tt(modelUpdate.getTertanggungModel().getKdtelp2_tt());
                        me.getTertanggungModel().setTelp2_tt(modelUpdate.getTertanggungModel().getTelp2_tt());
                        updateAlamatRmh();
                        break;
                    case 5:
                        me.getTertanggungModel().setAlamat_kantor_tt(modelUpdate.getTertanggungModel().getAlamat_kantor_tt());
//                        me.getTertanggungModel().setKota_kantor_tt(modelUpdate.getTertanggungModel().getKota_kantor_tt());
                        me.getTertanggungModel().setPropinsi_kantor_tt(modelUpdate.getTertanggungModel().getPropinsi_kantor_tt());
                        me.getTertanggungModel().setKabupaten_kantor_tt(modelUpdate.getTertanggungModel().getKabupaten_kantor_tt());
                        me.getTertanggungModel().setKecamatan_kantor_tt(modelUpdate.getTertanggungModel().getKecamatan_kantor_tt());
                        me.getTertanggungModel().setKelurahan_kantor_tt(modelUpdate.getTertanggungModel().getKelurahan_kantor_tt());
                        me.getTertanggungModel().setKdpos_kantor_tt(modelUpdate.getTertanggungModel().getKdpos_kantor_tt());
                        me.getTertanggungModel().setKdtelp1_kantor_tt(modelUpdate.getTertanggungModel().getKdtelp1_kantor_tt());
                        me.getTertanggungModel().setTelp1_kantor_tt(modelUpdate.getTertanggungModel().getTelp1_kantor_tt());
                        me.getTertanggungModel().setKdtelp2_kantor_tt(modelUpdate.getTertanggungModel().getKdtelp2_kantor_tt());
                        me.getTertanggungModel().setTelp2_kantor_tt(modelUpdate.getTertanggungModel().getTelp2_kantor_tt());
//                        me.getTertanggungModel().setKdfax_kantor_tt(modelUpdate.getTertanggungModel().getKdfax_kantor_tt());
                        me.getTertanggungModel().setFax_kantor_tt(modelUpdate.getTertanggungModel().getFax_kantor_tt());
                        updateAlamatKantor();
                        break;
                    case 6:
                        me.getTertanggungModel().setAlamat_tinggal_tt(modelUpdate.getTertanggungModel().getAlamat_tinggal_tt());
//                        me.getTertanggungModel().setKota_tinggal_tt(modelUpdate.getTertanggungModel().getKota_tinggal_tt());
                        me.getTertanggungModel().setPropinsi_tinggal_tt(modelUpdate.getTertanggungModel().getPropinsi_tinggal_tt());
                        me.getTertanggungModel().setKabupaten_tinggal_tt(modelUpdate.getTertanggungModel().getKabupaten_tinggal_tt());
                        me.getTertanggungModel().setKecamatan_tinggal_tt(modelUpdate.getTertanggungModel().getKecamatan_tinggal_tt());
                        me.getTertanggungModel().setKelurahan_tinggal_tt(modelUpdate.getTertanggungModel().getKelurahan_tinggal_tt());
                        me.getTertanggungModel().setKdpos_tinggal_tt(modelUpdate.getTertanggungModel().getKdpos_tinggal_tt());
                        me.getTertanggungModel().setKdtelp1_tinggal_tt(modelUpdate.getTertanggungModel().getKdtelp1_tinggal_tt());
                        me.getTertanggungModel().setTelp1_tinggal_tt(modelUpdate.getTertanggungModel().getTelp1_tinggal_tt());
                        me.getTertanggungModel().setKdtelp2_tinggal_tt(modelUpdate.getTertanggungModel().getKdtelp2_tinggal_tt());
                        me.getTertanggungModel().setTelp2_tinggal_tt(modelUpdate.getTertanggungModel().getTelp2_tinggal_tt());
                        me.getTertanggungModel().setKdfax_tinggal_tt(modelUpdate.getTertanggungModel().getKdfax_tinggal_tt());
                        me.getTertanggungModel().setFax_tinggal_tt(modelUpdate.getTertanggungModel().getFax_tinggal_tt());
                        updateAlamatTinggal();
                        break;
                    case 8:
                        me.getTertanggungModel().setHp1_tt(modelUpdate.getTertanggungModel().getHp1_tt());
                        me.getTertanggungModel().setHp2_tt(modelUpdate.getTertanggungModel().getHp2_tt());
                        me.getTertanggungModel().setEmail_tt(modelUpdate.getTertanggungModel().getEmail_tt());
                        UpdateHP();
                        break;
                    default:
                        throw new IllegalArgumentException("Field Type belum diset");
                }
            }
        }
    }

    public void goToForm(int flag) {
        Intent intent = new Intent(getContext(), E_FormAlamatAutoZipActivity.class);
        intent.putExtra(getString(R.string.flag), flag);
        intent.putExtra(getString(R.string.modelespaj), me);
        startActivityForResult(intent, RequestCode.FILL_ALAMAT_SPAJ);
    }

    private void updateAlamatRmh() {
        if (!me.getTertanggungModel().getAlamat_tt().equals("")) {
            tv_isi_alamat_rumah_tt.setVisibility(View.GONE);
            img_alamat_rumah_tt.setVisibility(View.GONE);
            tv_ubah_alamat_rumah_tt.setVisibility(View.VISIBLE);
            ll_alamat_rumah_tt.setVisibility(View.VISIBLE);

            String AlamatpostalCode = me.getTertanggungModel().getKdpos_tt();
            ListDropDownKodepos = new E_Select(getContext()).getLst_Kodepos_PP(me.getTertanggungModel().getKdpos_tt());
            ListDropDownKelurahan = new E_Select(getContext()).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_tt());
            ListDropDownKecamatan = new E_Select(getContext()).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_tt());
            ListDropDownKabupaten = new E_Select(getContext()).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_tt());
            ListDropDownProvinsi = new E_Select(getContext()).getLst_Propinsi_PP();
            String[][] values = new String[][]{
                    new String[]{getString(R.string.alamat), me.getTertanggungModel().getAlamat_tt()},
//                    new String[]{getString(R.string.kota), me.getTertanggungModel().getKota_tt()},
                    new String[]{getString(R.string.provinsi), MethodSupport.getAdapterPositionString(ListDropDownProvinsi, me.getTertanggungModel().getPropinsi_tt())},
                    new String[]{getString(R.string.kabupaten), MethodSupport.getAdapterPositionString(ListDropDownKabupaten, me.getTertanggungModel().getKabupaten_tt())},
                    new String[]{getString(R.string.kecamatan), MethodSupport.getAdapterPositionString(ListDropDownKecamatan, me.getTertanggungModel().getKecamatan_tt())},
                    new String[]{getString(R.string.kelurahan), MethodSupport.getAdapterPositionString(ListDropDownKelurahan, me.getTertanggungModel().getKelurahan_tt())},
                    new String[]{getString(R.string.kode_pos), AlamatpostalCode},
                    new String[]{getString(R.string.telp_1), me.getTertanggungModel().getKdtelp1_tt() + " - " + me.getTertanggungModel().getTelp1_tt()},
                    new String[]{getString(R.string.telp_2), me.getTertanggungModel().getKdtelp1_tt() + " - " + me.getTertanggungModel().getTelp2_tt()}
            };
            new ValueView(getContext()).addToLinearLayout(ll_alamat_rumah_tt, values);
        } else {
            tv_isi_alamat_rumah_tt.setVisibility(View.VISIBLE);
            img_alamat_rumah_tt.setVisibility(View.VISIBLE);
            tv_ubah_alamat_rumah_tt.setVisibility(View.GONE);
            ll_alamat_rumah_tt.setVisibility(View.GONE);
        }
    }

    private void updateAlamatKantor() {
        if (!me.getTertanggungModel().getAlamat_kantor_tt().equals("")) {
            tv_isi_alamat_kantor_tt.setVisibility(View.GONE);
            img_alamat_kantor_tt.setVisibility(View.GONE);
            tv_ubah_alamat_kantor_tt.setVisibility(View.VISIBLE);
            ll_alamat_kantor_tt.setVisibility(View.VISIBLE);
            String AlamatpostalCode = me.getTertanggungModel().getKdpos_kantor_tt();
            ListDropDownKodepos = new E_Select(getContext()).getLst_Kodepos_PP(me.getTertanggungModel().getKdpos_kantor_tt());
            ListDropDownKelurahan = new E_Select(getContext()).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_kantor_tt());
            ListDropDownKecamatan = new E_Select(getContext()).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_kantor_tt());
            ListDropDownKabupaten = new E_Select(getContext()).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_kantor_tt());
            ListDropDownProvinsi = new E_Select(getContext()).getLst_Propinsi_PP();

            String[][] values = new String[][]{
                    new String[]{getString(R.string.alamat), me.getTertanggungModel().getAlamat_kantor_tt()},
//                    new String[]{getString(R.string.kota), me.getTertanggungModel().getKota_kantor_tt()},
                    new String[]{getString(R.string.provinsi), MethodSupport.getAdapterPositionString(ListDropDownProvinsi, me.getTertanggungModel().getPropinsi_kantor_tt())},
                    new String[]{getString(R.string.kabupaten), MethodSupport.getAdapterPositionString(ListDropDownKabupaten, me.getTertanggungModel().getKabupaten_kantor_tt())},
                    new String[]{getString(R.string.kecamatan), MethodSupport.getAdapterPositionString(ListDropDownKecamatan, me.getTertanggungModel().getKecamatan_kantor_tt())},
                    new String[]{getString(R.string.kelurahan), MethodSupport.getAdapterPositionString(ListDropDownKelurahan, me.getTertanggungModel().getKelurahan_kantor_tt())},
                    new String[]{getString(R.string.kode_pos), AlamatpostalCode},
                    new String[]{getString(R.string.telp_1), me.getTertanggungModel().getKdtelp1_kantor_tt() + " - " + me.getTertanggungModel().getTelp1_kantor_tt()},
                    new String[]{getString(R.string.telp_2), me.getTertanggungModel().getKdtelp1_kantor_tt() + " - " + me.getTertanggungModel().getTelp2_kantor_tt()},
//                    new String[]{getString(R.string.no_fax), me.getTertanggungModel().getKdfax_kantor_tt() + " - " + me.getTertanggungModel().getFax_kantor_tt()}
            };
            new ValueView(getContext()).addToLinearLayout(ll_alamat_kantor_tt, values);
        } else {
            tv_isi_alamat_kantor_tt.setVisibility(View.VISIBLE);
            img_alamat_kantor_tt.setVisibility(View.VISIBLE);
            tv_ubah_alamat_kantor_tt.setVisibility(View.GONE);
            ll_alamat_kantor_tt.setVisibility(View.GONE);
        }
    }

    private void updateAlamatTinggal() {
        if (!me.getTertanggungModel().getAlamat_tinggal_tt().equals("")) {
            tv_isi_alamat_tempat_tinggal_tt.setVisibility(View.GONE);
            img_alamat_tempat_tinggal_tt.setVisibility(View.GONE);
            tv_ubah_tempat_tinggal_tt.setVisibility(View.VISIBLE);
            ll_alamat_tempat_tinggal_tt.setVisibility(View.VISIBLE);
            String AlamatpostalCode = me.getTertanggungModel().getKdpos_tinggal_tt();
            ListDropDownKodepos = new E_Select(getContext()).getLst_Kodepos_PP(me.getTertanggungModel().getKdpos_tinggal_tt());
            ListDropDownKabupaten = new E_Select(getContext()).getLst_Kabupaten_PP(me.getTertanggungModel().getKdpos_tinggal_tt());
            ListDropDownKelurahan = new E_Select(getContext()).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_tinggal_tt());
            ListDropDownKecamatan = new E_Select(getContext()).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_tinggal_tt());
            ListDropDownKabupaten = new E_Select(getContext()).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_tinggal_tt());
            ListDropDownProvinsi = new E_Select(getContext()).getLst_Propinsi_PP();

            String[][] values = new String[][]{
                    new String[]{getString(R.string.alamat), me.getTertanggungModel().getAlamat_tinggal_tt()},
//                    new String[]{getString(R.string.kota), me.getTertanggungModel().getKota_tinggal_tt()},
                    new String[]{getString(R.string.provinsi), MethodSupport.getAdapterPositionString(ListDropDownProvinsi, me.getTertanggungModel().getPropinsi_tinggal_tt())},
                    new String[]{getString(R.string.kabupaten), MethodSupport.getAdapterPositionString(ListDropDownKabupaten, me.getTertanggungModel().getKabupaten_tinggal_tt())},
                    new String[]{getString(R.string.kecamatan), MethodSupport.getAdapterPositionString(ListDropDownKecamatan, me.getTertanggungModel().getKecamatan_tinggal_tt())},
                    new String[]{getString(R.string.kelurahan), MethodSupport.getAdapterPositionString(ListDropDownKelurahan, me.getTertanggungModel().getKelurahan_tinggal_tt())},
                    new String[]{getString(R.string.kode_pos), AlamatpostalCode},
                    new String[]{getString(R.string.telp_1), me.getTertanggungModel().getKdtelp1_tinggal_tt() + " - " + me.getTertanggungModel().getTelp1_tinggal_tt()},
                    new String[]{getString(R.string.telp_2), me.getTertanggungModel().getKdtelp1_tinggal_tt() + " - " + me.getTertanggungModel().getTelp2_tinggal_tt()},
                    new String[]{getString(R.string.no_fax), me.getTertanggungModel().getKdfax_tinggal_tt() + " - " + me.getTertanggungModel().getFax_tinggal_tt()}
            };
            new ValueView(getContext()).addToLinearLayout(ll_alamat_tempat_tinggal_tt, values);
        } else {
            tv_isi_alamat_tempat_tinggal_tt.setVisibility(View.VISIBLE);
            img_alamat_tempat_tinggal_tt.setVisibility(View.VISIBLE);
            tv_ubah_tempat_tinggal_tt.setVisibility(View.GONE);
            ll_alamat_tempat_tinggal_tt.setVisibility(View.GONE);
        }
    }

    private void UpdateHP() {
        if (!me.getTertanggungModel().getHp1_tt().equals("")) {
            tv_isi_hp_email_tt.setVisibility(View.GONE);
            img_hp_email_tt.setVisibility(View.GONE);
            tv_ubah_hp_email_tt.setVisibility(View.VISIBLE);
            ll_hp_email_tt.setVisibility(View.VISIBLE);

            String[][] values = new String[][]{
                    new String[]{getString(R.string.hp1), me.getTertanggungModel().getHp1_tt()},
                    new String[]{getString(R.string.hp2), me.getTertanggungModel().getHp2_tt()},
                    new String[]{getString(R.string.email), me.getTertanggungModel().getEmail_tt()}
            };
            new ValueView(getContext()).addToLinearLayout(ll_hp_email_tt, values);
        } else {
            tv_isi_hp_email_tt.setVisibility(View.VISIBLE);
            img_hp_email_tt.setVisibility(View.VISIBLE);
            tv_ubah_hp_email_tt.setVisibility(View.GONE);
            ll_hp_email_tt.setVisibility(View.GONE);
        }
    }

    private void showDateDialog(int flag_ttl, String tanggal, EditText edit) {
        switch (flag_ttl) {
            case 1:
                if (tanggal.length() != 0) {
                    cal_tt = toCalendar(tanggal);
                } else {
                    cal_tt = Calendar.getInstance();
                }
                DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                        datePickerListener, cal_tt.get(Calendar.YEAR), cal_tt.get(Calendar.MONTH), cal_tt.get(Calendar.DATE));
                dialog.getDatePicker().setMaxDate(cal_tt.getTimeInMillis());
                dialog.show();
                break;
            case 2:
                if (tanggal.length() != 0) {
                    cal_berlaku_tt = toCalendar(tanggal);
                } else {
                    cal_berlaku_tt = Calendar.getInstance();
                }
                DatePickerDialog dialog1 = new DatePickerDialog(getActivity(),
                        datePickerListener_berlaku, cal_berlaku_tt.get(Calendar.YEAR), cal_berlaku_tt.get(Calendar.MONTH), cal_berlaku_tt.get(Calendar.DATE));
                dialog1.getDatePicker().setMinDate(cal_berlaku_tt.getTimeInMillis());
                dialog1.show();
                break;
        }
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            cal_tt.set(Calendar.YEAR, selectedYear);
            cal_tt.set(Calendar.MONTH, selectedMonth);
            cal_tt.set(Calendar.DAY_OF_MONTH, selectedDay);
            et_ttl_tt.setText(StaticMethods.toStringDate(cal_tt, 5));
            et_usia_tt.setText(F_Hit_Umur.OnCountUsia(cal_tt.get(Calendar.YEAR), cal_tt.get(Calendar.MONTH), cal_tt.get(Calendar.DATE)));
        }
    };

    private DatePickerDialog.OnDateSetListener datePickerListener_berlaku = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            cal_berlaku_tt.set(Calendar.YEAR, selectedYear);
            cal_berlaku_tt.set(Calendar.MONTH, selectedMonth);
            cal_berlaku_tt.set(Calendar.DAY_OF_MONTH, selectedDay);
            et_tgl_berlaku_tt.setText(StaticMethods.toStringDate(cal_berlaku_tt, 5));
        }
    };

    private void val_agama(int agama) {
        if (agama == 6) {
            et_teks_agama_lain_tt.setVisibility(View.VISIBLE);
        } else {
            et_teks_agama_lain_tt.setVisibility(View.GONE);
        }
    }

    private void val_bukti(int bukti) {
        if (bukti == 7) {
            et_teks_bukti_lain_tt.setVisibility(View.VISIBLE);
        } else {
            et_teks_bukti_lain_tt.setVisibility(View.GONE);
        }
        if (bukti == 9 || bukti == 5) {
            et_tgl_berlaku_tt.setVisibility(View.GONE);
            iv_circle_tgl_berlaku_tt.setVisibility(View.GONE);
        } else {
            et_tgl_berlaku_tt.setVisibility(View.VISIBLE);
            iv_circle_tgl_berlaku_tt.setVisibility(View.VISIBLE);
        }
    }

    private void onClickLanjut() {
        String jobDescription = ac_klasifikasi_pekerjaan_tt.getText().toString().trim();
        if (TextUtils.isEmpty(jobDescription)) {
            ac_klasifikasi_pekerjaan_tt.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
            ac_klasifikasi_pekerjaan_tt.requestFocus();
            return;
        }

        if (!isJobDescriptionValid) {

            ac_klasifikasi_pekerjaan_tt.setError(getResources().getString(R.string.err_msg_jobdesc_not_found_on_the_list));

            ac_klasifikasi_pekerjaan_tt.requestFocus();
            return;
        }

        me.getTertanggungModel().setValidation(Validation());
        loadview();
        MyTask task = new MyTask();
        task.execute();
//        me.getTertanggungModel().setValidation(Validation());
//
//        loadview();
//        ((E_MainActivity) getActivity()).onSave();
//        View cl_child_tt = getActivity().findViewById(R.id.cl_child_tt);
//        File file_bitmap = new File(
//                getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//        if (!file_bitmap.exists()) {
//            file_bitmap.mkdirs();
//        }

        //create JPG
//        String fileName = "TT_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
//        Context context = getActivity();
//        MethodSupport.onCreateBitmap(cl_child_tt, scroll_tt, file_bitmap, fileName, context);
//        ((E_MainActivity) getActivity()).setFragment(new E_CalonPembayarPremiFragment(), getResources().getString(R.string.calon_pembayar_premi));


//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setTitle("Melakukan perpindahan halaman");
//        progressDialog.setMessage("Mohon Menunggu...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
//        Runnable progressRunnable = new Runnable() {
//            @Override
//            public void run() {
//        MethodSupport.active_view(0, btn_lanjut);
//        MethodSupport.active_view(0, iv_next);
//
//        me.getTertanggungModel().setValidation(Validation());
//
//        loadview();
//        ((E_MainActivity) getActivity()).onSave();
//        View cl_child_tt = getActivity().findViewById(R.id.cl_child_tt);
//        File file_bitmap = new File(
//                getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//        if (!file_bitmap.exists()) {
//            file_bitmap.mkdirs();
//        }
//        String fileName = "TT_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
//        Context context = getActivity();
//        MethodSupport.onCreateBitmap(cl_child_tt, scroll_tt, file_bitmap, fileName, context);
//
//        ((E_MainActivity) getActivity()).setFragment(new E_CalonPembayarPremiFragment(), getResources().getString(R.string.calon_pembayar_premi));
//
//                progressDialog.cancel();
//            }
//        };
//
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 5000);
        try {
            createPdf();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    private class MyTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_TERTANGGUNG);
            return null;
        }

        @Override
        protected void onPostExecute(Void params) {
            ((E_MainActivity) getActivity()).setFragment(new E_CalonPembayarPremiFragment(), getResources().getString(R.string.calon_pembayar_premi));
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    private class MyTaskBack extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_TERTANGGUNG);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ((E_MainActivity) getActivity()).setFragment(new E_PemegangPolisFragment(), getResources().getString(R.string.pemegang_polis));
        }
    }

    private void onClickBack() {

        loadview();
//        ((E_MainActivity) getActivity()).onSave();
//        ((E_MainActivity) getActivity()).setFragment(new E_PemegangPolisFragment(), getResources().getString(R.string.pemegang_polis));

        MyTaskBack taskBack = new MyTaskBack();
        taskBack.execute();

//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setTitle("Melakukan perpindahan halaman");
//        progressDialog.setMessage("Mohon Menunggu...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
//        Runnable progressRunnable = new Runnable() {
//
//            @Override
//            public void run() {
//        MethodSupport.active_view(0, btn_kembali);
//        MethodSupport.active_view(0, iv_prev);
//        ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
//
//        loadview();
//        ((E_MainActivity) getActivity()).onSave();
//        ((E_MainActivity) getActivity()).setFragment(new E_PemegangPolisFragment(), getResources().getString(R.string.pemegang_polis));
//                progressDialog.cancel();
//            }
//        };
//
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 1000);
    }

    private void createPdf() throws FileNotFoundException, DocumentException {
        try {

            File docsFolder = new File(Environment.getExternalStorageDirectory() + "/Documents");
            if (!docsFolder.exists()) {
                docsFolder.mkdir();
//            Log.i(TAG, "Created a new directory for PDF");
            }

//            pdfFile = new File(docsFolder.getAbsolutePath(), "PDF STRING "+ me.getModelID().getSPAJ_ID_TAB()+".pdf");

            Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 14);
            Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 14,
                    Font.BOLD);

            pdfFile = new File(docsFolder.getAbsolutePath(), "HelloWorld.pdf");

            OutputStream output = new FileOutputStream(pdfFile);

            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, output);
            writer.setFullCompression();
            document.open();
            document.add(new Paragraph(getString(R.string.tertanggung) + "\n\n", normalFont));

            Phrase alamat_rmh = new Phrase(getString(R.string.alamat) + " " + "Rumah" , normalFont);
            alamat_rmh.setTabSettings(new TabSettings(250f));
            alamat_rmh.add(Chunk.TABBING);
            alamat_rmh.add(new Chunk("     : " + me.getTertanggungModel().getAlamat_tt() + "\n", boldFont));
            document.add(alamat_rmh);

            ListDropDownString = new E_Select(getContext()).getLst_Propinsi_PP();
            Phrase provinsi = new Phrase(getString(R.string.provinsi), normalFont);
            provinsi.setTabSettings(new TabSettings(250f));
            provinsi.add(Chunk.TABBING);
            provinsi.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getPropinsi_tt())) + "\n", boldFont));
            document.add(provinsi);

            ListDropDownString = new E_Select(getContext()).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_tt());
            Phrase kabupaten = new Phrase(getString(R.string.kabupaten), normalFont);
            kabupaten.setTabSettings(new TabSettings(250f));
            kabupaten.add(Chunk.TABBING);
            kabupaten.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKabupaten_tt())) + "\n", boldFont));
            document.add(kabupaten);

            ListDropDownString = new E_Select(getContext()).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_tt());
            Phrase kecamatan = new Phrase(getString(R.string.kecamatan), normalFont);
            kecamatan.setTabSettings(new TabSettings(250f));
            kecamatan.add(Chunk.TABBING);
            kecamatan.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKecamatan_tt())) + "\n", boldFont));
            document.add(kecamatan);

            ListDropDownString = new E_Select(getContext()).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_tt());
            Phrase kelurahan = new Phrase(getString(R.string.kelurahan), normalFont);
            kelurahan.setTabSettings(new TabSettings(250f));
            kelurahan.add(Chunk.TABBING);
            kelurahan.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKelurahan_tt())) + "\n", boldFont));
            document.add(kelurahan);

            ListDropDownString = new E_Select(getContext()).getLst_Kodepos_PP(me.getTertanggungModel().getKelurahan_tt());
            Phrase kdpos_rmh = new Phrase(getString(R.string.kode_pos), normalFont);
            kdpos_rmh.setTabSettings(new TabSettings(250f));
            kdpos_rmh.add(Chunk.TABBING);
            kdpos_rmh.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKdpos_tt())) + "\n", boldFont));
            document.add(kdpos_rmh);

            Phrase telp1_rmh = new Phrase(getString(R.string.telp_1), normalFont);
            telp1_rmh.setTabSettings(new TabSettings(250f));
            telp1_rmh.add(Chunk.TABBING);
            telp1_rmh.add(new Chunk("     : " + me.getTertanggungModel().getKdtelp1_tt() + " - " + me.getTertanggungModel().getTelp1_tt() + "\n", boldFont));
            document.add(telp1_rmh);

            Phrase telp2_rmh = new Phrase(getString(R.string.telp_2), normalFont);
            telp2_rmh.setTabSettings(new TabSettings(250f));
            telp2_rmh.add(Chunk.TABBING);
            telp2_rmh.add(new Chunk("     : " + me.getTertanggungModel().getKdtelp2_tt() + " - " + me.getTertanggungModel().getTelp2_tt() + "\n", boldFont));
            document.add(telp2_rmh);

            Phrase alamat_kntr = new Phrase(getString(R.string.alamat) + " " + "Kantor", normalFont);
            alamat_kntr.setTabSettings(new TabSettings(250f));
            alamat_kntr.add(Chunk.TABBING);
            alamat_kntr.add(new Chunk("     : " + me.getTertanggungModel().getAlamat_kantor_tt() + "\n", boldFont));
            document.add(alamat_kntr);

            ListDropDownString = new E_Select(getContext()).getLst_Propinsi_PP();
            Phrase provinsi_kntr = new Phrase(getString(R.string.provinsi), normalFont);
            provinsi_kntr.setTabSettings(new TabSettings(250f));
            provinsi_kntr.add(Chunk.TABBING);
            provinsi_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getPropinsi_kantor_tt())) + "\n", boldFont));
            document.add(provinsi_kntr);

            ListDropDownString = new E_Select(getContext()).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_kantor_tt());
            Phrase kabupaten_kntr = new Phrase(getString(R.string.kabupaten), normalFont);
            kabupaten_kntr.setTabSettings(new TabSettings(250f));
            kabupaten_kntr.add(Chunk.TABBING);
            kabupaten_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKabupaten_kantor_tt())) + "\n", boldFont));
            document.add(kabupaten_kntr);

            ListDropDownString = new E_Select(getContext()).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_kantor_tt());
            Phrase kecamatan_kntr = new Phrase(getString(R.string.kecamatan), normalFont);
            kecamatan_kntr.setTabSettings(new TabSettings(250f));
            kecamatan_kntr.add(Chunk.TABBING);
            kecamatan_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKecamatan_kantor_tt())) + "\n", boldFont));
            document.add(kecamatan_kntr);

            ListDropDownString = new E_Select(getContext()).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_kantor_tt());
            Phrase kelurahan_kntr = new Phrase(getString(R.string.kelurahan), normalFont);
            kelurahan_kntr.setTabSettings(new TabSettings(250f));
            kelurahan_kntr.add(Chunk.TABBING);
            kelurahan_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKelurahan_kantor_tt())) + "\n", boldFont));
            document.add(kelurahan_kntr);

            ListDropDownString = new E_Select(getContext()).getLst_Kodepos_PP(me.getTertanggungModel().getKelurahan_kantor_tt());
            Phrase kdpos_kntr = new Phrase(getString(R.string.kode_pos), normalFont);
            kdpos_kntr.setTabSettings(new TabSettings(250f));
            kdpos_kntr.add(Chunk.TABBING);
            kdpos_kntr.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKdpos_kantor_tt())) + "\n", boldFont));
            document.add(kdpos_kntr);

            Phrase telp1_kntr = new Phrase(getString(R.string.telp_1), normalFont);
            telp1_kntr.setTabSettings(new TabSettings(250f));
            telp1_kntr.add(Chunk.TABBING);
            telp1_kntr.add(new Chunk("     : " + me.getTertanggungModel().getKdtelp1_kantor_tt() + " - " + me.getTertanggungModel().getTelp1_kantor_tt() + "\n", boldFont));
            document.add(telp1_kntr);

            Phrase telp2_kntr = new Phrase(getString(R.string.telp_2), normalFont);
            telp2_kntr.setTabSettings(new TabSettings(250f));
            telp2_kntr.add(Chunk.TABBING);
            telp2_kntr.add(new Chunk("     : " + me.getTertanggungModel().getKdtelp2_kantor_tt() + " - " + me.getTertanggungModel().getTelp2_kantor_tt() + "\n", boldFont));
            document.add(telp2_kntr);

            Phrase alamat_tgl = new Phrase(getString(R.string.alamat) + " " + "Tinggal", normalFont);
            alamat_tgl.setTabSettings(new TabSettings(250f));
            alamat_tgl.add(Chunk.TABBING);
            alamat_tgl.add(new Chunk("     : " + me.getTertanggungModel().getAlamat_tinggal_tt() + "\n", boldFont));
            document.add(alamat_tgl);

            ListDropDownString = new E_Select(getContext()).getLst_Propinsi_PP();
            Phrase provinsi_tgl = new Phrase(getString(R.string.provinsi), normalFont);
            provinsi_tgl.setTabSettings(new TabSettings(250f));
            provinsi_tgl.add(Chunk.TABBING);
            provinsi_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getPropinsi_tinggal_tt())) + "\n", boldFont));
            document.add(provinsi_tgl);

            ListDropDownString = new E_Select(getContext()).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_tinggal_tt());
            Phrase kabupaten_tgl = new Phrase(getString(R.string.kabupaten), normalFont);
            kabupaten_tgl.setTabSettings(new TabSettings(250f));
            kabupaten_tgl.add(Chunk.TABBING);
            kabupaten_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKabupaten_tinggal_tt())) + "\n", boldFont));
            document.add(kabupaten_tgl);

            ListDropDownString = new E_Select(getContext()).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_tinggal_tt());
            Phrase kecamatan_tgl = new Phrase(getString(R.string.kecamatan), normalFont);
            kecamatan_tgl.setTabSettings(new TabSettings(250f));
            kecamatan_tgl.add(Chunk.TABBING);
            kecamatan_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKecamatan_tinggal_tt())) + "\n", boldFont));
            document.add(kecamatan_tgl);

            ListDropDownString = new E_Select(getContext()).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_tinggal_tt());
            Phrase kelurahan_tgl = new Phrase(getString(R.string.kelurahan), normalFont);
            kelurahan_tgl.setTabSettings(new TabSettings(250f));
            kelurahan_tgl.add(Chunk.TABBING);
            kelurahan_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKelurahan_tinggal_tt())) + "\n", boldFont));
            document.add(kelurahan_tgl);

            ListDropDownString = new E_Select(getContext()).getLst_Kodepos_PP(me.getTertanggungModel().getKelurahan_tinggal_tt());
            Phrase kdpos_tgl = new Phrase(getString(R.string.kode_pos), normalFont);
            kdpos_tgl.setTabSettings(new TabSettings(250f));
            kdpos_tgl.add(Chunk.TABBING);
            kdpos_tgl.add(new Chunk("     : " + MethodSupport.getAdapterPositionString(ListDropDownString, (me.getTertanggungModel().getKdpos_tinggal_tt())) + "\n", boldFont));
            document.add(kdpos_tgl);

            Phrase telp1_tgl = new Phrase(getString(R.string.telp_1), normalFont);
            telp1_tgl.setTabSettings(new TabSettings(250f));
            telp1_tgl.add(Chunk.TABBING);
            telp1_tgl.add(new Chunk("     : " + me.getTertanggungModel().getKdtelp1_tinggal_tt() + " - " + me.getTertanggungModel().getTelp1_tinggal_tt() + "\n", boldFont));
            document.add(telp1_tgl);

            Phrase telp2_tgl = new Phrase(getString(R.string.telp_2), normalFont);
            telp2_tgl.setTabSettings(new TabSettings(250f));
            telp2_tgl.add(Chunk.TABBING);
            telp2_tgl.add(new Chunk("     : " + me.getTertanggungModel().getKdtelp2_tinggal_tt() + " - " + me.getTertanggungModel().getTelp2_tinggal_tt() + "\n", boldFont));
            document.add(telp2_tgl);


            document.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }


    private ProgressDialog dialogmessage;

    public void startProgress() {
        dialogmessage = new ProgressDialog(getActivity());
        dialogmessage.setMessage("Mohon Menunggu ");
        dialogmessage.setTitle("Sedang Melakukan Perpindahan Halaman ...");
        dialogmessage.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialogmessage.show();
        dialogmessage.setCancelable(false);
        btn_lanjut.setEnabled(false);
        iv_next.setEnabled(false);
        iv_prev.setEnabled(false);
        btn_kembali.setEnabled(false);
        dialogmessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                btn_lanjut.setEnabled(true);
                iv_next.setEnabled(true);
                iv_prev.setEnabled(true);
                btn_kembali.setEnabled(true);
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (dialogmessage.getProgress() <= dialogmessage
                            .getMax()) {
                        Thread.sleep(10);
                        handle.sendMessage(handle.obtainMessage());
                        if (dialogmessage.getProgress() == dialogmessage
                                .getMax()) {
                            dialogmessage.dismiss();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    Handler handle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            dialogmessage.incrementProgressBy(1);
        }
    };

    @BindView(R.id.tv_data_diri_tt)
    TextView tv_data_diri_tt;
    @BindView(R.id.tv_error_nama_tt)
    TextView tv_error_nama_tt;
    @BindView(R.id.tv_error_tempat_tt)
    TextView tv_error_tempat_tt;
    @BindView(R.id.tv_error_ttl_tt)
    TextView tv_error_ttl_tt;
    @BindView(R.id.tv_error_usia_tt)
    TextView tv_error_usia_tt;
    @BindView(R.id.tv_error_jekel_tt)
    TextView tv_error_jekel_tt;
    @BindView(R.id.tv_error_agama_tt)
    TextView tv_error_agama_tt;
    @BindView(R.id.tv_error_teks_agama_lain_tt)
    TextView tv_error_teks_agama_lain_tt;
    @BindView(R.id.tv_error_pendidikan_tt)
    TextView tv_error_pendidikan_tt;
    @BindView(R.id.tv_error_gelar_tt)
    TextView tv_error_gelar_tt;
    @BindView(R.id.tv_error_ibu_tt)
    TextView tv_error_ibu_tt;
    @BindView(R.id.tv_error_green_card_tt)
    TextView tv_error_green_card_tt;
    @BindView(R.id.tv_error_warganegara_tt)
    TextView tv_error_warganegara_tt;
    @BindView(R.id.tv_bukti_identitas_tt)
    TextView tv_bukti_identitas_tt;
    @BindView(R.id.tv_error_bukti_identitas_tt)
    TextView tv_error_bukti_identitas_tt;
    @BindView(R.id.tv_error_teks_bukti_lain_tt)
    TextView tv_error_teks_bukti_lain_tt;
    @BindView(R.id.tv_error_nomor_bukti_tt)
    TextView tv_error_nomor_bukti_tt;
    @BindView(R.id.tv_error_tgl_berlaku_tt)
    TextView tv_error_tgl_berlaku_tt;
    @BindView(R.id.tv_error_status_tt)
    TextView tv_error_status_tt;
    @BindView(R.id.tv_alamat_kantor_tt)
    TextView tv_alamat_kantor_tt;
    @BindView(R.id.tv_isi_alamat_kantor_tt)
    TextView tv_isi_alamat_kantor_tt;
    @BindView(R.id.tv_alamat_rumah_tt)
    TextView tv_alamat_rumah_tt;
    @BindView(R.id.tv_ubah_alamat_rumah_tt)
    TextView tv_ubah_alamat_rumah_tt;
    @BindView(R.id.tv_alamat_tempat_tinggal_tt)
    TextView tv_alamat_tempat_tinggal_tt;
    @BindView(R.id.tv_isi_alamat_tempat_tinggal_tt)
    TextView tv_isi_alamat_tempat_tinggal_tt;
    @BindView(R.id.tv_hp_email_tt)
    TextView tv_hp_email_tt;
    @BindView(R.id.tv_ubah_hp_email_tt)
    TextView tv_ubah_hp_email_tt;
    @BindView(R.id.tv_penghasilan_tt)
    TextView tv_penghasilan_tt;
    @BindView(R.id.tv_error_npwp_tt)
    TextView tv_error_npwp_tt;
    @BindView(R.id.tv_error_pendapatan_tt)
    TextView tv_error_pendapatan_tt;
    @BindView(R.id.tv_sumber_dana_tt)
    TextView tv_sumber_dana_tt;
    @BindView(R.id.tv_lain_lain_tt)
    TextView tv_lain_lain_tt;
    @BindView(R.id.tv_tujuan_pengajuan_asuransi_tt)
    TextView tv_tujuan_pengajuan_asuransi_tt;
    @BindView(R.id.tv_ubah_alamat_kantor_tt)
    TextView tv_ubah_alamat_kantor_tt;
    @BindView(R.id.tv_isi_alamat_rumah_tt)
    TextView tv_isi_alamat_rumah_tt;
    @BindView(R.id.tv_ubah_tempat_tinggal_tt)
    TextView tv_ubah_tempat_tinggal_tt;
    @BindView(R.id.tv_error_nama_perusahaan_tt)
    TextView tv_error_nama_perusahaan_tt;
    @BindView(R.id.tv_error_pekerjaan_tt)
    TextView tv_error_pekerjaan_tt;
    @BindView(R.id.tv_error_klasifikasi_pekerjaan_tt)
    TextView tv_error_klasifikasi_pekerjaan_tt;
    @BindView(R.id.tv_error_jabatan_tt)
    TextView tv_error_jabatan_tt;
    @BindView(R.id.tv_error_uraikan_tugas_tt)
    TextView tv_error_uraikan_tugas_tt;
    @BindView(R.id.tv_isi_hp_email_tt)
    TextView tv_isi_hp_email_tt;
    @BindView(R.id.tv_error_alias_tt)
    TextView tv_error_alias_tt;
    @BindView(R.id.tv_error_alamat_rumah_tt)
    TextView tv_error_alamat_rumah_tt;

    @BindView(R.id.cl_child_tt)
    ConstraintLayout cl_child_tt;
    @BindView(R.id.cl_data_diri_tt)
    ConstraintLayout cl_data_diri_tt;
    @BindView(R.id.cl_form_data_diri_tt)
    ConstraintLayout cl_form_data_diri_tt;
    @BindView(R.id.cl_nama_tt)
    ConstraintLayout cl_nama_tt;
    @BindView(R.id.cl_tempat_tt)
    ConstraintLayout cl_tempat_tt;
    @BindView(R.id.cl_ttl_tt)
    ConstraintLayout cl_ttl_tt;
    @BindView(R.id.cl_usia_tt)
    ConstraintLayout cl_usia_tt;
    @BindView(R.id.cl_jekel_tt)
    ConstraintLayout cl_jekel_tt;
    @BindView(R.id.cl_agama_tt)
    ConstraintLayout cl_agama_tt;
    @BindView(R.id.cl_teks_agama_lain_tt)
    ConstraintLayout cl_teks_agama_lain_tt;
    @BindView(R.id.cl_pendidikan_tt)
    ConstraintLayout cl_pendidikan_tt;
    @BindView(R.id.cl_gelar_tt)
    ConstraintLayout cl_gelar_tt;
    @BindView(R.id.cl_ibu_tt)
    ConstraintLayout cl_ibu_tt;
    @BindView(R.id.cl_green_card_tt)
    ConstraintLayout cl_green_card_tt;
    @BindView(R.id.cl_warganegara_tt)
    ConstraintLayout cl_warganegara_tt;
    @BindView(R.id.cl_bukti_identitas_head_tt)
    ConstraintLayout cl_bukti_identitas_head_tt;
    @BindView(R.id.cl_form_bukti_identitas_tt)
    ConstraintLayout cl_form_bukti_identitas_tt;
    @BindView(R.id.cl_bukti_identitas_tt)
    ConstraintLayout cl_bukti_identitas_tt;
    @BindView(R.id.cl_teks_bukti_lain_tt)
    ConstraintLayout cl_teks_bukti_lain_tt;
    @BindView(R.id.cl_nomor_bukti_tt)
    ConstraintLayout cl_nomor_bukti_tt;
    @BindView(R.id.cl_tgl_berlaku_tt)
    ConstraintLayout cl_tgl_berlaku_tt;
    @BindView(R.id.cl_status_tt)
    ConstraintLayout cl_status_tt;
    @BindView(R.id.cl_alamat_kantor_tt)
    ConstraintLayout cl_alamat_kantor_tt;
    @BindView(R.id.cl_alamat_rumah_tt)
    ConstraintLayout cl_alamat_rumah_tt;
    @BindView(R.id.cl_alamat_tempat_tinggal_tt)
    ConstraintLayout cl_alamat_tempat_tinggal_tt;
    @BindView(R.id.cl_hp_email_tt)
    ConstraintLayout cl_hp_email_tt;
    @BindView(R.id.cl_penghasilan_tt)
    ConstraintLayout cl_penghasilan_tt;
    @BindView(R.id.cl_form_penghasilan_tt)
    ConstraintLayout cl_form_penghasilan_tt;
    @BindView(R.id.cl_npwp_tt)
    ConstraintLayout cl_npwp_tt;
    @BindView(R.id.cl_pendapatan_tt)
    ConstraintLayout cl_pendapatan_tt;
    @BindView(R.id.cl_sumber_dana_tt)
    ConstraintLayout cl_sumber_dana_tt;
    @BindView(R.id.cl_checkd_gaji_tt)
    ConstraintLayout cl_checkd_gaji_tt;
    @BindView(R.id.cl_sumber_dana3_tt)
    ConstraintLayout cl_sumber_dana3_tt;
    @BindView(R.id.cl_sumber_dana4_tt)
    ConstraintLayout cl_sumber_dana4_tt;
    @BindView(R.id.cl_sumber_dana5_tt)
    ConstraintLayout cl_sumber_dana5_tt;
    @BindView(R.id.cl_sumber_dana6_tt)
    ConstraintLayout cl_sumber_dana6_tt;
    @BindView(R.id.cl_sumber_dana7_tt)
    ConstraintLayout cl_sumber_dana7_tt;
    @BindView(R.id.cl_lain_lain_tt)
    ConstraintLayout cl_lain_lain_tt;
    @BindView(R.id.cl_form_lain_lain_tt)
    ConstraintLayout cl_form_lain_lain_tt;
    @BindView(R.id.cl_checkt_proteksi_tt)
    ConstraintLayout cl_checkt_proteksi_tt;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi2_tt)
    ConstraintLayout cl_tujuan_pengajuan_asuransi2_tt;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi3_tt)
    ConstraintLayout cl_tujuan_pengajuan_asuransi3_tt;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi4_tt)
    ConstraintLayout cl_tujuan_pengajuan_asuransi4_tt;
    //    @BindView(R.id.cl_btn_prev_tt)
//    ConstraintLayout cl_btn_prev_tt;
//    @BindView(R.id.cl_btn_next_tt)
//    ConstraintLayout cl_btn_next_tt;
    @BindView(R.id.cl_nama_perusahaan_tt)
    ConstraintLayout cl_nama_perusahaan_tt;

    @BindView(R.id.cl_klasifikasi_pekerjaan_tt)
    ConstraintLayout cl_klasifikasi_pekerjaan_tt;
    @BindView(R.id.cl_jabatan_tt)
    ConstraintLayout cl_jabatan_tt;
    @BindView(R.id.cl_uraikan_tugas_tt)
    ConstraintLayout cl_uraikan_tugas_tt;
    @BindView(R.id.cl_alias_tt)
    ConstraintLayout cl_alias_tt;

    @BindView(R.id.ll_alamat_rumah_tt)
    LinearLayout ll_alamat_rumah_tt;
    @BindView(R.id.ll_alamat_kantor_tt)
    LinearLayout ll_alamat_kantor_tt;
    @BindView(R.id.ll_alamat_tempat_tinggal_tt)
    LinearLayout ll_alamat_tempat_tinggal_tt;
    @BindView(R.id.ll_hp_email_tt)
    LinearLayout ll_hp_email_tt;

    @BindView(R.id.iv_circle_nama_tt)
    ImageView iv_circle_nama_tt;
    @BindView(R.id.iv_circle_tempat_tt)
    ImageView iv_circle_tempat_tt;
    @BindView(R.id.iv_circle_ttl_tt)
    ImageView iv_circle_ttl_tt;
    @BindView(R.id.iv_circle_usia_tt)
    ImageView iv_circle_usia_tt;
    @BindView(R.id.iv_circle_jekel_tt)
    ImageView iv_circle_jekel_tt;
    @BindView(R.id.iv_circle_agama_tt)
    ImageView iv_circle_agama_tt;
    @BindView(R.id.iv_circle_teks_agama_lain_tt)
    ImageView iv_circle_teks_agama_lain_tt;
    @BindView(R.id.iv_circle_pendidikan_tt)
    ImageView iv_circle_pendidikan_tt;
    @BindView(R.id.iv_circle_jabatan_tt)
    ImageView iv_circle_jabatan_tt;
    @BindView(R.id.iv_circle_uraikan_tugas_tt)
    ImageView iv_circle_uraikan_tugas_tt;
    @BindView(R.id.iv_circle_nama_perusahaan_tt)
    ImageView iv_circle_nama_perusahaan_tt;

    @BindView(R.id.iv_circle_ibu_tt)
    ImageView iv_circle_ibu_tt;
    @BindView(R.id.iv_circle_green_card_tt)
    ImageView iv_circle_green_card_tt;
    @BindView(R.id.iv_circle_warganegara_tt)
    ImageView iv_circle_warganegara_tt;
    @BindView(R.id.iv_circle_bukti_identitas_tt)
    ImageView iv_circle_bukti_identitas_tt;
    @BindView(R.id.iv_circle_teks_bukti_lain_tt)
    ImageView iv_circle_teks_bukti_lain_tt;
    @BindView(R.id.iv_circle_nomor_bukti_tt)
    ImageView iv_circle_nomor_bukti_tt;
    @BindView(R.id.iv_circle_tgl_berlaku_tt)
    ImageView iv_circle_tgl_berlaku_tt;
    @BindView(R.id.iv_circle_status_tt)
    ImageView iv_circle_status_tt;
    @BindView(R.id.iv_circle_pendapatan_tt)
    ImageView iv_circle_pendapatan_tt;
    @BindView(R.id.iv_circle_sumber_dana_tt)
    ImageView iv_circle_sumber_dana_tt;
    @BindView(R.id.iv_circle_checkd_gaji_tt)
    ImageView iv_circle_checkd_gaji_tt;
    @BindView(R.id.iv_circle_sumber_dana3_tt)
    ImageView iv_circle_sumber_dana3_tt;
    @BindView(R.id.iv_circle_sumber_dana4_tt)
    ImageView iv_circle_sumber_dana4_tt;
    @BindView(R.id.iv_circle_sumber_dana5_tt)
    ImageView iv_circle_sumber_dana5_tt;
    @BindView(R.id.iv_circle_sumber_dana6_tt)
    ImageView iv_circle_sumber_dana6_tt;
    @BindView(R.id.iv_circle_sumber_dana7_tt)
    ImageView iv_circle_sumber_dana7_tt;
    @BindView(R.id.iv_circle_checkt_proteksi_tt)
    ImageView iv_circle_checkt_proteksi_tt;
    @BindView(R.id.iv_circle_tujuan_pengajuan_asuransi2_tt)
    ImageView iv_circle_tujuan_pengajuan_asuransi2_tt;
    @BindView(R.id.iv_circle_tujuan_pengajuan_asuransi3_tt)
    ImageView iv_circle_tujuan_pengajuan_asuransi3_tt;
    @BindView(R.id.iv_circle_tujuan_pengajuan_asuransi4_tt)
    ImageView iv_circle_tujuan_pengajuan_asuransi4_tt;
    @BindView(R.id.iv_circle_klasifikasi_pekerjaan_tt)
    ImageView iv_circle_klasifikasi_pekerjaan_tt;
    @BindView(R.id.iv_circle_alias_tt)
    ImageView iv_circle_alias_tt;

    @BindView(R.id.img_alamat_kantor_tt)
    ImageButton img_alamat_kantor_tt;
    @BindView(R.id.img_alamat_tempat_tinggal_tt)
    ImageButton img_alamat_tempat_tinggal_tt;
    @BindView(R.id.img_alamat_rumah_tt)
    ImageButton img_alamat_rumah_tt;
    @BindView(R.id.img_hp_email_tt)
    ImageButton img_hp_email_tt;

    @BindView(R.id.ac_agama_tt)
    AutoCompleteTextView ac_agama_tt;
    @BindView(R.id.ac_pendidikan_tt)
    AutoCompleteTextView ac_pendidikan_tt;
    @BindView(R.id.ac_bukti_identitas_tt)
    AutoCompleteTextView ac_bukti_identitas_tt;
    @BindView(R.id.ac_status_tt)
    AutoCompleteTextView ac_status_tt;
    @BindView(R.id.ac_pendapatan_tt)
    AutoCompleteTextView ac_pendapatan_tt;
    @BindView(R.id.ac_klasifikasi_pekerjaan_tt)
    AutoCompleteTextView ac_klasifikasi_pekerjaan_tt;
    @BindView(R.id.ac_jabatan_tt)
    AutoCompleteTextView ac_jabatan_tt;
    @BindView(R.id.ac_warganegara_tt)
    AutoCompleteTextView ac_warganegara_tt;

    @BindView(R.id.et_nama_tt)
    EditText et_nama_tt;
    @BindView(R.id.et_tempat_tt)
    EditText et_tempat_tt;
    @BindView(R.id.et_ttl_tt)
    EditText et_ttl_tt;
    @BindView(R.id.et_usia_tt)
    EditText et_usia_tt;
    @BindView(R.id.et_teks_agama_lain_tt)
    EditText et_teks_agama_lain_tt;
    @BindView(R.id.et_gelar_tt)
    EditText et_gelar_tt;
    @BindView(R.id.et_ibu_tt)
    EditText et_ibu_tt;
    @BindView(R.id.et_teks_bukti_lain_tt)
    EditText et_teks_bukti_lain_tt;
    @BindView(R.id.et_nomor_bukti_tt)
    EditText et_nomor_bukti_tt;
    @BindView(R.id.et_tgl_berlaku_tt)
    EditText et_tgl_berlaku_tt;
    @BindView(R.id.et_npwp_tt)
    EditText et_npwp_tt;
    @BindView(R.id.et_editd_lainnya_tt)
    EditText et_editd_lainnya_tt;
    @BindView(R.id.et_editt_lainnya_tt)
    EditText et_editt_lainnya_tt;
    @BindView(R.id.et_nama_perusahaan_tt)
    EditText et_nama_perusahaan_tt;
    @BindView(R.id.et_uraikan_tugas_tt)
    EditText et_uraikan_tugas_tt;
    @BindView(R.id.et_alias_tt)
    EditText et_alias_tt;

    @BindView(R.id.rg_jekel_tt)
    RadioGroup rg_jekel_tt;
    @BindView(R.id.rb_pria_tt)
    RadioButton rb_pria_tt;
    @BindView(R.id.rb_wanita_tt)
    RadioButton rb_wanita_tt;

    @BindView(R.id.rg_green_card_tt)
    RadioGroup rg_green_card_tt;
    @BindView(R.id.rb_gc_y_tt)
    RadioButton rb_gc_y_tt;
    @BindView(R.id.rb_gc_n_tt)
    RadioButton rb_gc_n_tt;

    @BindView(R.id.cb_checkd_gaji_tt)
    CheckBox cb_checkd_gaji_tt;
    @BindView(R.id.cb_checkd_tabungan_tt)
    CheckBox cb_checkd_tabungan_tt;
    @BindView(R.id.cb_checkd_warisan_tt)
    CheckBox cb_checkd_warisan_tt;
    @BindView(R.id.cb_checkd_hibah_tt)
    CheckBox cb_checkd_hibah_tt;
    @BindView(R.id.cb_checkd_lainnya_tt)
    CheckBox cb_checkd_lainnya_tt;
    @BindView(R.id.cb_checkt_proteksi_tt)
    CheckBox cb_checkt_proteksi_tt;
    @BindView(R.id.cb_checkt_inves_tt)
    CheckBox cb_checkt_inves_tt;
    @BindView(R.id.cb_checkt_lainnya_tt)
    CheckBox cb_checkt_lainnya_tt;

    @BindView(R.id.scroll_tt)
    ScrollView scroll_tt;

    private Button btn_lanjut, btn_kembali;
    private ImageView iv_save, iv_next, iv_prev;
    private Toolbar second_toolbar;

    private ArrayList<CheckBox> check_dana_tt;
    private ArrayList<CheckBox> check_tujuan_tt;

    private EspajModel me;
    View view;

    private int greencard_tt = 0;
    private int jekel_tt = -1;
    private int bukti_tt = 0;
    private int wn_tt = 0;
    private int stat_tt = 0;
    private int agama_tt = 0;
    private int pend_tt = 0;

    private String str_jabatan_klasifikasi_tt = "";
    private String str_penghasilan_thn_tt = "";

    private String dana_gaji_tt = "";
    private String dana_tabungan_tt = "";
    private String dana_warisan_tt = "";
    private String dana_hibah_tt = "";
    private String dana_lainnya_tt = "";
    private String edit_d_lainnya_tt = "";

    private String tujuan_proteksi_tt = "";
    private String tujuan_inves_tt = "";
    private String tujuan_lainnya_tt = "";
    private String edit_t_lainnya_tt = "";

    private boolean bool_dana_tt = false;
    private boolean bool_tujuan_tt = false;

    private ArrayList<ModelDropdownInt> ListDropDownSPAJ;
    private ArrayAdapter<String> Adapter;
    private ArrayList<ModelDropDownString> ListDropDownString;
    private ArrayList<ModelDropDownString> ListDropDownProvinsi;
    private ArrayList<ModelDropDownString> ListDropDownKabupaten;
    private ArrayList<ModelDropDownString> ListDropDownKecamatan;
    private ArrayList<ModelDropDownString> ListDropDownKelurahan;
    private ArrayList<ModelDropDownString> ListDropDownKodepos;

    private Calendar cal_tt, cal_berlaku_tt;

    private ProgressDialog progressDialog;

    private File pdfFile;
    private PdfRenderer renderer;
    private ExtendedViewPager pager;
}
