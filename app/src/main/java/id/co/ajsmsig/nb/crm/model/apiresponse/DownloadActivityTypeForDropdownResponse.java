
package id.co.ajsmsig.nb.crm.model.apiresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DownloadActivityTypeForDropdownResponse {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("saved_id")
    @Expose
    private Integer savedId;
    @SerializedName("indonesia")
    @Expose
    private String indonesia;
    @SerializedName("english")
    @Expose
    private String english;
    @SerializedName("level1")
    @Expose
    private Integer level1;
    @SerializedName("level2")
    @Expose
    private Integer level2;
    @SerializedName("level3")
    @Expose
    private Integer level3;
    @SerializedName("level4")
    @Expose
    private Integer level4;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSavedId() {
        return savedId;
    }

    public void setSavedId(Integer savedId) {
        this.savedId = savedId;
    }

    public String getIndonesia() {
        return indonesia;
    }

    public void setIndonesia(String indonesia) {
        this.indonesia = indonesia;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public Integer getLevel1() {
        return level1;
    }

    public void setLevel1(Integer level1) {
        this.level1 = level1;
    }

    public Integer getLevel2() {
        return level2;
    }

    public void setLevel2(Integer level2) {
        this.level2 = level2;
    }

    public Integer getLevel3() {
        return level3;
    }

    public void setLevel3(Integer level3) {
        this.level3 = level3;
    }

    public Integer getLevel4() {
        return level4;
    }

    public void setLevel4(Integer level4) {
        this.level4 = level4;
    }

}
