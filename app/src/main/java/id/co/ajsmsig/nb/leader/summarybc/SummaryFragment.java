package id.co.ajsmsig.nb.leader.summarybc;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.leader.summarybc.adapter.SummaryListAdapter;
import id.co.ajsmsig.nb.leader.summarybc.utils.NetworkState;
import id.co.ajsmsig.nb.leader.summarybc.viewmodel.SummaryViewModel;
import id.co.ajsmsig.nb.util.Const;


public class SummaryFragment extends Fragment {

    String TAG = SummaryFragment.class.getSimpleName();

    private SummaryListAdapter adapter;
    private SummaryViewModel viewModel;
    private String agentCode;
    private String channelBank;

    @BindView(R.id.recyclerViewSummary)
    RecyclerView recyclerViewSummary;

    @BindView(R.id.progressBarSummary)
    ProgressBar progressBarSummary;

    @BindView(R.id.constraintLayoutSummary)
    ConstraintLayout constraintLayoutSummary;

    @BindView(R.id.constraintLayoutNoConnection)
    ConstraintLayout constraintLayoutNoConnection;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_summary, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        Intent intent = getActivity().getIntent();

        agentCode = intent.getStringExtra(Const.INTENT_KEY_AGENT_CODE);
        channelBank = intent.getStringExtra(Const.INTENT_KEY_CHANNEL);

        viewModel = new SummaryViewModel(agentCode, channelBank);

        initRecylerView();
        getPageSize();
        conditionState();
    }

    public void initRecylerView() {
        adapter = new SummaryListAdapter(getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewSummary.setLayoutManager(mLayoutManager);
        recyclerViewSummary.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getActivity()), LinearLayoutManager.VERTICAL));
        recyclerViewSummary.setItemAnimator(new DefaultItemAnimator());
        recyclerViewSummary.setNestedScrollingEnabled(false);
        recyclerViewSummary.setAdapter(adapter);
    }

    public void getPageSize() {
        viewModel.getSummaryLiveData().observe(this, pagedList -> {
            adapter.submitList(pagedList);
            Log.v("Count", " Size : " + pagedList.size());
        });
    }

    public void conditionState() {
        viewModel.getNetworkState().observe(this, networkState -> {
            if (networkState == NetworkState.LOADING) {
                constraintLayoutNoConnection.setVisibility(View.INVISIBLE);
                constraintLayoutSummary.setVisibility(View.INVISIBLE);
                progressBarSummary.setVisibility(View.VISIBLE);
            }
            if (networkState == NetworkState.LOADED) {
                constraintLayoutNoConnection.setVisibility(View.INVISIBLE);
                progressBarSummary.setVisibility(View.INVISIBLE);
                constraintLayoutSummary.setVisibility(View.INVISIBLE);
            }
            if (networkState == NetworkState.NODATA) {
                constraintLayoutNoConnection.setVisibility(View.INVISIBLE);
                constraintLayoutSummary.setVisibility(View.VISIBLE);
                progressBarSummary.setVisibility(View.INVISIBLE);
            }
            if (networkState == NetworkState.FAILED) {
                constraintLayoutNoConnection.setVisibility(View.VISIBLE);
                constraintLayoutSummary.setVisibility(View.INVISIBLE);
                progressBarSummary.setVisibility(View.INVISIBLE);
                onBtnRetryPressed();
            }
            adapter.setNetworkState(networkState);
        });
    }

    @OnClick(R.id.btnRetry)
    void onBtnRetryPressed() {
        if (StaticMethods.isNetworkAvailable(Objects.requireNonNull(getActivity()))) {
            hideNoInternetConnection();
            viewModel = new SummaryViewModel(agentCode, channelBank);
            initRecylerView();
            getPageSize();
            conditionState();
        } else {
            showNoInternetConnection();
            Toast.makeText(getActivity(), "Anda tidak terkoneksi ke internet. Mohon pastikan Anda terkoneksi dengan internet", Toast.LENGTH_SHORT).show();
        }
    }

    private void showNoInternetConnection() {
        constraintLayoutNoConnection.setVisibility(View.VISIBLE);
        progressBarSummary.setVisibility(View.INVISIBLE);
        constraintLayoutSummary.setVisibility(View.INVISIBLE);
    }

    private void hideNoInternetConnection() {
        constraintLayoutNoConnection.setVisibility(View.INVISIBLE);
        progressBarSummary.setVisibility(View.VISIBLE);
        constraintLayoutSummary.setVisibility(View.INVISIBLE);
    }
}

