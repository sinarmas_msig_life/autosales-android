package id.co.ajsmsig.nb.pointofsale.binding

import android.databinding.BindingAdapter
import android.text.Html
import android.widget.TextView
import me.grantland.widget.AutofitTextView

/**
 * Created by andreyyoshuamanik on 19/03/18.
 */


@BindingAdapter("textFromHtml")
fun setTextFromHtmlToTextView(textView: TextView, text: String?) {
    text?.let { textView.text = Html.fromHtml(text) }
}


@BindingAdapter("textFromHtml")
fun setTextFromHtmlToAutoFitTextView(textView: AutofitTextView, text: String?) {
    text?.let { textView.text = Html.fromHtml(text) }

}

