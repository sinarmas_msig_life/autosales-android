
package id.co.ajsmsig.nb.leader.subordinate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfo {

    @SerializedName("MSAG_ID")
    @Expose
    private String mSAGID;
    @SerializedName("AGENT_NAME")
    @Expose
    private String aGENTNAME;
    @SerializedName("JENIS")
    @Expose
    private String jENIS;
    @SerializedName("AGENT_EMAIL")
    @Expose
    private String aGENTEMAIL;

    /**
     * No args constructor for use in serialization
     * 
     */
    public UserInfo() {
    }

    /**
     * 
     * @param aGENTEMAIL
     * @param mSAGID
     * @param jENIS
     * @param aGENTNAME
     */
    public UserInfo(String mSAGID, String aGENTNAME, String jENIS, String aGENTEMAIL) {
        super();
        this.mSAGID = mSAGID;
        this.aGENTNAME = aGENTNAME;
        this.jENIS = jENIS;
        this.aGENTEMAIL = aGENTEMAIL;
    }

    public String getMSAGID() {
        return mSAGID;
    }

    public void setMSAGID(String mSAGID) {
        this.mSAGID = mSAGID;
    }

    public String getAGENTNAME() {
        return aGENTNAME;
    }

    public void setAGENTNAME(String aGENTNAME) {
        this.aGENTNAME = aGENTNAME;
    }

    public String getJENIS() {
        return jENIS;
    }

    public void setJENIS(String jENIS) {
        this.jENIS = jENIS;
    }

    public String getAGENTEMAIL() {
        return aGENTEMAIL;
    }

    public void setAGENTEMAIL(String aGENTEMAIL) {
        this.aGENTEMAIL = aGENTEMAIL;
    }

}
