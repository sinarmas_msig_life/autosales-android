package id.co.ajsmsig.nb.crm.database;
/*
 *Created by faiz_f on 08/05/2017.
 */

import android.content.Context;

import java.util.HashMap;

import id.co.ajsmsig.nb.R;

public class SimbakLinkUsrToLeadTable {
    private Context context;

    public SimbakLinkUsrToLeadTable(Context context) {
        this.context = context;
    }

    public HashMap<String, String> getProjectionMap(){
        HashMap<String, String> projectionMap = new HashMap<>();
        projectionMap.put(context.getString(R.string.UTL_ID),  context.getString(R.string.UTL_ID) +" AS " +  context.getString(R.string._ID));
        projectionMap.put(context.getString(R.string.UTL_LEAD_ID), context.getString(R.string.UTL_LEAD_ID));
        projectionMap.put(context.getString(R.string.UTL_USER_ID), context.getString(R.string.UTL_USER_ID));

        return projectionMap;
    }


}
