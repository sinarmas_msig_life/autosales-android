package id.co.ajsmsig.nb.pointofsale.ui.blifestage

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData

import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 21/02/18.
 */
class LifeStageVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository, val sharedViewModel: SharedViewModel): AndroidViewModel(application) {


    var observablePageOptions: MediatorLiveData<List<PageOption>> = MediatorLiveData<List<PageOption>>()
    var row: Int? = 3

    var page: Page? = null
    set(value) {

        sharedViewModel.startFromLifeStage = true

        val pageOptions = repository.getPageOptionsFromPage(value)
        observablePageOptions.addSource(pageOptions, observablePageOptions::setValue)
    }

}