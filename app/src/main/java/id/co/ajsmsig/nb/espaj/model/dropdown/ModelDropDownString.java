package id.co.ajsmsig.nb.espaj.model.dropdown;

/**
 * Created by eriza on 04/09/2017.
 */

public class ModelDropDownString {
    private String value = "";
    private String id = "";

    public ModelDropDownString() {
    }

    public ModelDropDownString(String value, String id) {
        super();
        this.value = value;
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String toString() {
        return value;
    }

}