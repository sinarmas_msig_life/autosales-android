package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.HandlingObjection
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.HandlingObjectionAllAnswers


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface HandlingObjectionDao {

    @Query("SELECT * from HandlingObjection")
    fun loadAllHandlingObjection(): LiveData<List<HandlingObjectionAllAnswers>>

    @Query("SELECT * from HandlingObjection where parentId is null order by id")
    fun loadAllHandlingObjectionCategory(): LiveData<List<HandlingObjection>>

    @Query("SELECT * from HandlingObjection where parentId = :parentId")
    fun loadAllHandlingObjectionWithQuestionId(parentId: Int?): LiveData<List<HandlingObjectionAllAnswers>>


}