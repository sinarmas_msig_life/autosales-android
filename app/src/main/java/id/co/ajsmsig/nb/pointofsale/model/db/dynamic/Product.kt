package id.co.ajsmsig.nb.pointofsale.model.db.dynamic

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Product(
        @PrimaryKey
        var ProductId: Int = 0,
        val LsbsId: Int?,
        var FundFactSheet: String?,
        var Group_ID: Int?,
        var LsbsName: String?,
        var Brosur: String?,
        var Presentation: String?,
        var Sheet: String?,
        var Icon: String?,
        var RiderSheet: String?) {

        override fun toString(): String {
                return LsbsName ?: ""
        }
}

