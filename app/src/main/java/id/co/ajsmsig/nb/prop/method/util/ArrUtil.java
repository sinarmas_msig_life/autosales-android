package id.co.ajsmsig.nb.prop.method.util;

import java.util.ArrayList;
import java.util.List;

import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.Prop_Model_LimitVO;

public class ArrUtil
{
    public static List<String> toListFromArray(String[] params )
    {
        List<String> list = new ArrayList<String>();
        if( params != null )
        {
            for( String content : params )
            {
                list.add( content );
            }
        }
        return list;
    }

    public static List<Integer> toListFromArray(Integer[] params )
    {
        List<Integer> list = new ArrayList<Integer>();
        if( params != null )
        {
            for( Integer content : params )
            {
                list.add( content );
            }
        }
        return list;
    }

    public static List<String> toListFromArrayDisregardEmpty(String[] params )
    {
        List<String> list = new ArrayList<String>();
        if( params != null )
        {
            for( String content : params )
            {
                if( !CommonUtil.isEmpty( content ) )
                {
                    list.add( content );
                }
            }
        }
        return list;
    }
    
    public static int max( int[] arr )
    {
        int max = Integer.MIN_VALUE;
        int length = arr.length;
        for( int i = 0; i < length; i++ )
        {
            if( max < arr[ i ] ) max = arr[ i ];
        }
        return max;
    }

    public static int min( int[] arr )
    {
        int min = Integer.MAX_VALUE;
        int length = arr.length;
        for( int i = 0; i < length; i++ )
        {
            if( min > arr[ i ] ) min = arr[ i ];
        }
        return min;
    }

    public static double max( double[] arr )
    {
        double max = Double.MIN_VALUE;
        int length = arr.length;
        for( int i = 0; i < length; i++ )
        {
            if( max < arr[ i ] ) max = arr[ i ];
        }
        return max;
    }

    public static double min( double[] arr )
    {
        double min = Double.MAX_VALUE;
        int length = arr.length;
        for( int i = 0; i < length; i++ )
        {
            if( min > arr[ i ] ) min = arr[ i ];
        }
        return min;
    }

    public static int upperBound( int[] arr )
    {
        return arr.length - 1;
    }

    public static int upperBound( double[] arr )
    {
        return arr.length - 1;
    }

    public static Integer[] genPartitionsIntArray(Prop_Model_LimitVO[] propModelLimitVOArr)
    {
    	int length = 0;
    	for( Prop_Model_LimitVO vo : propModelLimitVOArr)
    	{
    		length = length + ( vo.getUp() - vo.getDown() + 1 );
    	}
    	Integer[] result = new Integer[length];
    	int idx = 0;
    	for( Prop_Model_LimitVO propModelLimitVO : propModelLimitVOArr)
    	{
    		for(int i = propModelLimitVO.getDown(); i <= propModelLimitVO.getUp(); i++ )
    		{
    			result[idx] = i;
    			idx++;
    		}
    	}
    	return result;
    }
    
    public static List<Integer> genPartitionsIntList(Prop_Model_LimitVO[] propModelLimitVOArr)
    {
    	return toListFromArray( genPartitionsIntArray(propModelLimitVOArr) );
    }
    
    

    public static void main( String[] args )
    {
        double[] testArr = { 4, 3, 5, 2, 7 };
        System.out.println( "*-*-*-* max( testArr ) = " + max( testArr ) );
        System.out.println( "*-*-*-* min( testArr ) = " + min( testArr ) );

        List<Integer> yearNoList;
        int age = 22;
        Prop_Model_LimitVO[] propModelLimitVOArr = new Prop_Model_LimitVO[]{ new Prop_Model_LimitVO( 1 + age, 20 + age ), new Prop_Model_LimitVO( 150, 153  )};
        yearNoList = ArrUtil.toListFromArray( ArrUtil.genPartitionsIntArray(propModelLimitVOArr) );
        System.out.println( "*-*-*-* yearNoList = " + yearNoList );

    }
}
