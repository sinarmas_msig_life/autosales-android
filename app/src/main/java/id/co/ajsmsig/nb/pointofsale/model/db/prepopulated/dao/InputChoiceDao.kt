package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.InputChoice


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface InputChoiceDao {
    @Query("SELECT * from InputChoice")
    fun loadAllInputChoices(): LiveData<List<InputChoice>>


    @Query("SELECT * from InputChoice where inputId = :inputId")
    fun loadAllInputChoicesWith(inputId: Int?): LiveData<List<InputChoice>>

}