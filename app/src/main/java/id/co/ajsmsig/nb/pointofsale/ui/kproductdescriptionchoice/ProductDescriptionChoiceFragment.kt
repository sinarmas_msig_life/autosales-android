package id.co.ajsmsig.nb.pointofsale.ui.kproductdescriptionchoice


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.pointofsale.adapter.OptionAdapter
import id.co.ajsmsig.nb.pointofsale.ui.fragment.BaseFragment
import id.co.ajsmsig.nb.pointofsale.custom.flexbox.*
import id.co.ajsmsig.nb.databinding.PosFragmentProductDescriptionChoiceBinding
import id.co.ajsmsig.nb.pointofsale.ui.fragment.SingleOptionFragment
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import id.co.ajsmsig.nb.pointofsale.utils.Constants.Companion.PAGE

/**
 * A simple [BaseFragment] subclass.
 */
class ProductDescriptionChoiceFragment : SingleOptionFragment() {

    private lateinit var dataBinding: PosFragmentProductDescriptionChoiceBinding
    private lateinit var optionAdapter: OptionAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        arguments?.getSerializable(PAGE)?.let {
            page = it as Page
            page?.title = sharedViewModel.selectedProduct?.LsbsName
        }
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductDescriptionChoiceVM::class.java)
        subscribeUi(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_product_description_choice, container, false)

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupList(dataBinding.recyclerView)
    }

    fun setupList(recyclerView: RecyclerView) {
        optionAdapter = OptionAdapter()
        recyclerView.adapter = optionAdapter
        val layoutManager = FlexboxLayoutManager(activity, FlexDirection.ROW, JustifyContent.CENTER, true, 2, 2)
        recyclerView.layoutManager = layoutManager
        recyclerView.setHasFixedSize(true)
    }

    fun subscribeUi(viewModel: ProductDescriptionChoiceVM) {
        dataBinding.vm = viewModel

        viewModel.page = page
        viewModel.observablePageOptions.observe(this, Observer {
            it?.let {
                optionAdapter.options = it.toTypedArray()
            }
        })

    }

    override fun optionSelected(): PageOption? {
        val viewModel = ViewModelProviders.of(this).get(ProductDescriptionChoiceVM::class.java)
        return viewModel.observablePageOptions.value?.firstOrNull { it.selected.get() }
    }
}// Required empty public constructor
