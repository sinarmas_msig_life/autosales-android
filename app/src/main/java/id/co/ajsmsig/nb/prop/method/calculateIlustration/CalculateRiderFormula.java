package id.co.ajsmsig.nb.prop.method.calculateIlustration;//package sample.eproposal.sinarma.eproposalbeta2.Method;

import android.content.Context;
import android.util.Log;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

//import id.co.ajsmsig.nb.constant.Prop_Model_Istr_prop;
//import id.co.ajsmsig.nb.util.FormatNumber;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.prop.method.util.CommonUtil;
import id.co.ajsmsig.nb.prop.method.util.FormatNumber;
import id.co.ajsmsig.nb.prop.method.util.MathUtil;
import id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_Istr_prop;

/**
 * Created by faiz_f on 29/08/2016.
 */
public class CalculateRiderFormula extends IllustrationFormula{
    //    private final EproposalManager eproposalManager;
    Prop_Model_Istr_prop istr_prop;

    CalculateRiderFormula(Context context) {
        super(context);
    }

//    public CalculateRiderFormula(EproposalManager eproposalManager, Object command) {
//        this.eproposalManager = eproposalManager;
//        istr_prop = PbConverter.get_istr_prop( command, 0 );
//    }

    public double of_get_coi_134( int ai_jenis, int ai_th)
    {

        double ldec_total = 0, ldec_temp, ldec_persen_ci = 0;
        double ldec_pct = 1;
        double ldec_rate, ldec_max = 100000;
        int li_usia, li_tahun_ke, li_kali = 1, li_jenis, li_class, li_temp, li_risk, li_usia_tt, li_usia_pp;
        li_usia = istr_prop.umur_tt + ai_th - 1;
        li_usia_pp = istr_prop.umur_pp + ai_th - 1;
        String no_proposal = null;
        Integer ins_per = 0;
//
////        if( istr_prop.rider_baru[ 5 ] > 0 || istr_prop.rider_baru[ 6 ] > 0 || istr_prop.rider_baru[ 7 ] > 0 || istr_prop.rider_baru[ 8 ] > 0 || istr_prop.rider_baru[ 9 ] > 0 || istr_prop.rider_baru[ 10 ] > 0 )
////        {
        HashMap<String, Object> dataProposal = getSelect().selectDataProposal(no_proposal);
        Log.d(TAG, "getIllustrationResult: " + dataProposal);
        if(!CommonUtil.isEmpty(dataProposal)) {
            ins_per =  Integer.valueOf(dataProposal.get("THN_MASA_KONTRAK").toString());
        }

        if( istr_prop.cb_id == 1 )
        {
            li_kali = 4;
            ldec_pct = 0.27;
        }
        else if( istr_prop.cb_id == 2 )
        {
            li_kali = 2;
            ldec_pct = 0.525;
        }
        else if( istr_prop.cb_id == 6 )
        {
            li_kali = 12;
            ldec_pct = 0.1;
        }
////        }
//        wf_set_120();
        switch( ai_jenis )
        {
            case 1:
                //pa

                if( ( istr_prop.rider_baru[ 1 ] > 0 ) && ( li_usia < 60 ) )
                {
                    li_class = istr_prop.pa_class;
                    li_risk = 1;
                    li_temp = li_usia;
                    if(istr_prop.bisnis_id == 120  || istr_prop.bisnis_id == 202 || istr_prop.bisnis_id == 141){
                        //khusus 120-cerdas usia tdk naik
                        li_risk = istr_prop.pa_risk;
                        li_temp = istr_prop.umur_tt;
                    }
                    if(istr_prop.bisnis_id == 127 || istr_prop.bisnis_id == 141 || istr_prop.bisnis_id == 202 ){
                        if( li_temp <= 20 ) li_class = 2;
                    }
                    if(istr_prop.bisnis_id == 120){
                        if( li_temp <= 16 ) li_class = 2;
                    }
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "paRisk", li_risk );
                    params.put( "liClass", li_class );
                    double maks = 2000000000.0;
                    if( istr_prop.kurs_id.equals("02") ){
                        maks = 200000.0;
                    }
                    for( int i = 1; i <= ins_per; i++ ) {
                        ldec_rate = getLdec_coi(dataProposal, i);
                        double maxAmount = MathUtil.min(istr_prop.rider_baru[1] * istr_prop.up, maks);
                        ldec_temp = FormatNumber.round(((maxAmount / 1000) * ldec_rate) * 0.1, 2);
                        ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("810"), new BigDecimal(li_risk),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                    }
                }
                break;
            case 2:
                //hcp
                if( ( istr_prop.rider_baru[ 2 ] > 0 ) && ( li_usia < 65 ) )
                {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "riderBaru", istr_prop.rider_baru[ 2 ] );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liUsia", li_usia );

//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateHcp( params ) );
//                    ldec_temp = FormatNumber.round( ldec_rate * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("811"), new BigDecimal(istr_prop.rider_baru[ 2 ]),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
//
            case 3:
                //tpd
                if( istr_prop.rider_baru[ 3 ] > 0 )
                {
                    li_jenis = istr_prop.bisnis_id == 120 || istr_prop.bisnis_id == 202 ? 7 : istr_prop.rider_baru[3];
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liUsia", li_usia );
                    params.put( "li_jenis", li_jenis );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateTpd_120( params ) );
//                    // maks 2 milyar
//                    if( "01".equals( istr_prop.kurs_id ) ){
//                        ldec_max = 2000000000;
//                    }else if( "02".equals( istr_prop.kurs_id ) ){
//                        ldec_max = 200000;
//                    }
//                    double maxAmount = MathUtil.min( istr_prop.up, ldec_max );
//                    ldec_temp = FormatNumber.round( ( ( maxAmount / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("812"), new BigDecimal(li_jenis),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
            case 4:
                //ci
                if( istr_prop.rider_baru[ 4 ] > 0 )
                {
                    li_tahun_ke = li_usia;
                    if(istr_prop.bisnis_id == 127 || istr_prop.bisnis_id == 141){
                        li_tahun_ke = istr_prop.umur_pp + ai_th - 1;
                    }
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put( "kursId", istr_prop.kurs_id );
//                    params.put( "liUsia", li_tahun_ke );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateCi( params ) );
//
//                    ldec_temp = ( ( istr_prop.premi * istr_prop.pct_premi / 100 ) * li_kali ) * 2;
//                    ldec_max = 500000000;
//                    if(istr_prop.kurs_id.equals("02")){
//                        ldec_max = 50000;
//                    }
//                    double minAmount = MathUtil.min( ldec_temp, ldec_max );
//                    ldec_temp = FormatNumber.round( ( ( minAmount / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    if(istr_prop.bisnis_id == 120 || istr_prop.bisnis_id == 127 || istr_prop.bisnis_id == 128 || istr_prop.bisnis_id == 129 || istr_prop.bisnis_id == 202){
//                        if( istr_prop.bisnis_id == 127 || istr_prop.bisnis_id == 128 || istr_prop.bisnis_id == 129  ){
//                            ldec_persen_ci = 1;
//                            if( istr_prop.bisnis_id == 129 && cepr01030102Form.getSmilePensionPackageCd() != null && cepr01030102Form.getSmilePensionPackageCd() > 1 ){
//                                ldec_persen_ci = 0.5;
//                            }
//                        }else{
//                            ldec_persen_ci = MethodSupport.ConverttoDouble( new BigDecimal(cepr01030103Form.getCiChooseCd())) / 100.0;
//                        }
//                        ldec_max = 2000000000;
//                        if(istr_prop.kurs_id.equals("02")){
//                            ldec_max = 200000;
//                        }
//                        minAmount = MathUtil.min(istr_prop.up * ldec_persen_ci, ldec_max);
//                        ldec_temp = FormatNumber.round(((minAmount / 1000) * ldec_rate) * 0.1, 2);
//                    }
//
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("813"), new BigDecimal("8"),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
//            //
            case 5:
                //wp 60tpd
                if( istr_prop.rider_baru[ 5 ] > 0 )
                {
                    li_jenis = istr_prop.rider_baru[ 5 ];
                    if( ( istr_prop.bisnis_id == 120 && istr_prop.bisnis_no != 10 && istr_prop.bisnis_no != 11 && istr_prop.bisnis_no != 12 && istr_prop.bisnis_no != 22 && istr_prop.bisnis_no != 23 && istr_prop.bisnis_no != 24 )|| istr_prop.bisnis_id == 127 || istr_prop.bisnis_id == 128 || istr_prop.bisnis_id == 129 ){
//                        if( cepr01030102Form.getTermOfPayment().intValue() == 5 ){//5
//                            li_jenis = 14;
//                        }else if( cepr01030102Form.getTermOfPayment().intValue() == 10 ){//10
//                            li_jenis = 15;
//                        }
                    }
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "li_jenis", li_jenis );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "ai_th", ai_th );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateWp60Tpd_120( params ) );
//                    double factor = istr_prop.pct_premi / 100;
//                    if( istr_prop.bisnis_id == 129 && cepr01030102Form.getSmilePensionPackageCd() != null && cepr01030102Form.getSmilePensionPackageCd() > 1 ){
//                        factor = 1;
//                    }
                    // *** Rider Payor dan Waiver Simas Power Link(120) mengcover Premi Pokok + Top Up : Desvinna@9/10/2013 ***
                    //ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi * factor ) * li_kali / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi ) * li_kali / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("814"), new BigDecimal(li_jenis),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
            case 6:
                //pb 25 tpd
                if( istr_prop.rider_baru[ 6 ] > 0 )
                {
                    li_tahun_ke = istr_prop.umur_pp + ai_th - 1;
                    li_jenis = istr_prop.rider_baru[ 6 ];
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "liJenis", li_jenis );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liTahunKe", li_tahun_ke );
                    params.put( "liUsia", ai_th );
//                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRatePb25Tpd( params ) );
                    //ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi * istr_prop.pct_premi / 100 ) * li_kali / 1000 ) * ldec_rate ) * 0.1, 2 );
                    // *** Rider Payor dan Waiver Simas Power Link(120) mengcover Premi Pokok + Top Up : Desvinna@9/10/2013 ***
//                    ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi ) * li_kali / 1000 ) * ldec_rate ) * 0.1, 2 );
//
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("815"), new BigDecimal(li_jenis),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
//
            case 7:
                //wp 60ci
                if( istr_prop.rider_baru[ 7 ] > 0 )
                {
//                	li_jenis = istr_prop.bisnis_id == 127 || istr_prop.bisnis_id == 129 || istr_prop.bisnis_id == 128 ? 4 : istr_prop.rider_baru[ 7 ];
                    li_jenis = istr_prop.rider_baru[ 7 ];
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "riderBaru", li_jenis );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "ai_th", ai_th );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateWp60Ci_120( params ) );
                    double factor = istr_prop.pct_premi / 100;
//                    if( istr_prop.bisnis_id == 129 && cepr01030102Form.getSmilePensionPackageCd() != null && cepr01030102Form.getSmilePensionPackageCd() > 1 ){
//                        factor = 1;
//                    }
                    // *** Rider Payor dan Waiver Simas Power Link(120) mengcover Premi Pokok + Top Up : Desvinna@9/10/2013 ***
                    //ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi * factor ) * li_kali / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi ) * li_kali / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("816"), new BigDecimal(li_jenis),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
            case 8:
                //pb 25 ci/death
                if( istr_prop.rider_baru[ 8 ] > 0 )
                {
                    li_tahun_ke = istr_prop.umur_pp + ai_th - 1;
                    li_jenis = istr_prop.rider_baru[ 8 ];
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "liJenis", li_jenis );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liTahunKe", li_tahun_ke );
                    params.put( "liUsia", ai_th );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRatePb25CiDeath( params ) );
//
//                    ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi * istr_prop.pct_premi / 100 ) * li_kali / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("817"), new BigDecimal(li_jenis),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
            //pb 25 ci
//            //--
            case 9:
                if( istr_prop.rider_baru[ 9 ] > 0 )
                {
                    li_tahun_ke = istr_prop.umur_pp + ai_th - 1;
                    li_jenis = istr_prop.rider_baru[ 9 ];
                    if( istr_prop.bisnis_id == 120 ){
                        if( istr_prop.bisnis_no == 10 || istr_prop.bisnis_no == 22){// simpol
                            li_jenis = 2;
                        }else if( istr_prop.bisnis_no == 11 || istr_prop.bisnis_no == 23 ){
                            li_jenis = 3;
                        }
                    }
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "liJenis", li_jenis );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liTahunKe", li_tahun_ke );
                    params.put( "liUsia", ai_th );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRatePb25Ci( params ) );
                    // *** Rider Payor dan Waiver Simas Power Link(120) mengcover Premi Pokok + Top Up : Desvinna@9/10/2013 ***
                    //ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi * istr_prop.pct_premi / 100 ) * li_kali / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi ) * li_kali / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("817"), new BigDecimal(li_jenis),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
            //payor spouse
            case 10:
                if( istr_prop.rider_baru[ 10 ] > 0 )
                {
                    li_tahun_ke = istr_prop.umur_pp + ai_th - 1;
                    li_jenis = 13;

                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "liJenis", li_jenis );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liTahunKe", li_tahun_ke );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRatePayorSpouse( params ) );

//                    ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi * istr_prop.pct_premi / 100 ) * li_kali / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("815"), new BigDecimal(li_jenis),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
//
            case 11:
                //hcp family
                if( istr_prop.rider_baru[ 11 ] > 0 && li_usia < 65 )
                {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "riderBaru", istr_prop.rider_baru[ 11 ] + 140 );   // li_jenis in PB = riderBaru, + 140 bcoz start from 141
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateHcpFamily( params ) );
//
//                    ldec_temp = FormatNumber.round( ldec_rate * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("819"), new BigDecimal( istr_prop.rider_baru[ 11 ] + 140 ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                //hcp family peserta tambahan
                if( istr_prop.rider_baru[ 11 ] > 0 && istr_prop.hcpf.peserta > 0  )
                {
                    for( int i = 1; i <= istr_prop.hcpf.peserta; i++ )
                    {
                        li_usia_tt = istr_prop.hcpf.usia[ i ] + ai_th - 1;
                        Integer lsdbs_hcp_fam = null;

                        Map<String, Object> params = new HashMap<String, Object>();
                        params.put( "riderBaru", istr_prop.rider_baru[ 11 ] + 140 );    // li_jenis in PB = riderBaru, + 140 bcoz start from 141
                        params.put( "kursId", istr_prop.kurs_id );
                        params.put( "liUsia", li_usia_tt );
//                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateHcpFamilyPesertaTambahan( params ) );
//                        ldec_temp = 0;
//                        if (!( i > 1 && li_usia_tt > 24 ) ){
//                            ldec_temp = FormatNumber.round( ldec_rate * 0.09, 2 );
//                        }
//                        ldec_total += ldec_temp;
                        if( i == 1 ) {
                            lsdbs_hcp_fam = istr_prop.rider_baru[ 11 ] + 140 + 20;
                        }else if( i == 2 ) {
                            lsdbs_hcp_fam = istr_prop.rider_baru[ 11 ] + 140 + ( 20 * 2 );
                        }else if( i == 3 ) {
                            lsdbs_hcp_fam = istr_prop.rider_baru[ 11 ] + 140 + ( 20 * 3 );
                        }else if( i == 4 ) {
                            lsdbs_hcp_fam = istr_prop.rider_baru[ 11 ] + 140 + ( 20 * 4 );
                        }else if( i == 5 ) {
                            lsdbs_hcp_fam = istr_prop.rider_baru[ 11 ] + 140 + ( 20 * 5 );
                        }
//                        if( ai_th == 1 ){
//                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("819"), new BigDecimal( lsdbs_hcp_fam ),
//                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                    new BigDecimal(istr_prop.cb_id),  istr_prop.hcpf.tgl[i], istr_prop.hcpf.usia[i] ) );
//                        }
                    }
                }
                break;
            case 12:
                //term rider
                li_temp = 10;
                if(istr_prop.bisnis_id == 120 || istr_prop.bisnis_id == 202)	li_temp = 70;
                if( istr_prop.rider_baru[ 12 ] > 0 && ai_th <= li_temp )
                {
                    double up_term = istr_prop.up_term;
                    if( istr_prop.bisnis_id == 128 ){
                        up_term = istr_prop.up;
                    }
                    li_jenis = istr_prop.rider_baru[ 12 ];
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "riderBaru", li_jenis );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateTermRider_120( params ) );
//                    ldec_temp = FormatNumber.round( ( ( up_term / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("818"), new BigDecimal( li_jenis ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
            case 13:
                //eka sehat
                if( istr_prop.rider_baru[ 13 ] > 0 && li_usia < 75 )
                {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "riderBaru", istr_prop.rider_baru[ 13 ] );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectEkaSehatRider( params ) );
                    if( istr_prop.cb_id == 1 ){  //triwulan
                        ldec_pct = 0.35;
                    }else if( istr_prop.cb_id == 2 ){ //semesteran
                        ldec_pct = 0.65;
                    }else if( istr_prop.cb_id == 6 ){ //bulanan
                        ldec_pct = 0.12;
                    }
//                    ldec_temp = FormatNumber.round( (ldec_rate * ldec_pct) * 0.1, 2 );
//                    ldec_temp = FormatNumber.round( (ldec_rate) * 0.12, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("823"), new BigDecimal( istr_prop.rider_baru[ 13 ] ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }

                //eka sehat peserta tambahan
                if( istr_prop.rider_baru[ 13 ] > 0 && istr_prop.eka_sehat.peserta > 0   && li_usia < 75 )
                {
                    for( int i = 1; i <= istr_prop.eka_sehat.peserta; i++ )
                    {
                        li_usia = istr_prop.eka_sehat.usia[ i ] + ai_th - 1;

                        Map<String, Object> params = new HashMap<String, Object>();
                        params.put( "riderBaru", istr_prop.rider_baru[ 13 ]  );
                        params.put( "kursId", istr_prop.kurs_id );
                        params.put( "liUsia", li_usia );
//                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectEkaSehatRider( params ) );
////                        ldec_temp = FormatNumber.round( (ldec_rate * 0.975 * ldec_pct) * 0.1, 2 );
//                        ldec_temp = FormatNumber.round( (ldec_rate * 0.117) , 2 );
//                        ldec_total += ldec_temp;

                        Integer lsdbs_e_sehat = null;
                        if( i == 1 ) {
                            lsdbs_e_sehat = istr_prop.rider_baru[ 13 ] + 15;
                        }else if( i == 2 ) {
                            lsdbs_e_sehat = istr_prop.rider_baru[ 13 ] + ( 15 * 2 );
                        }else if( i == 3 ) {
                            lsdbs_e_sehat = istr_prop.rider_baru[ 13 ] + ( 15 * 3 );
                        }else if( i == 4 ) {
                            lsdbs_e_sehat = istr_prop.rider_baru[ 13 ] + ( 15 * 4 );
                        }else if( i == 5 ) {
                            lsdbs_e_sehat = istr_prop.rider_baru[ 13 ] + ( 15 * 5 );
                        }
//                        if( ai_th == 1 ){
//                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("823"), new BigDecimal( lsdbs_e_sehat ),
//                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                    new BigDecimal(istr_prop.cb_id), istr_prop.eka_sehat.tgl[i], istr_prop.eka_sehat.usia[i] ) );
//                        }
                    }
                }
                break;
//
            case 15:
                //eka sehat inner limit
                if( istr_prop.rider_baru[ 15 ] > 0 && li_usia < 75 )
                {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "riderBaru", istr_prop.rider_baru[ 15 ] );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectEkaSehatInnerLimitRider( params ) );
//                    if( istr_prop.cb_id == 1 ){  //triwulan
//                        ldec_pct = 0.35;
//                    }else if( istr_prop.cb_id == 2 ){ //semesteran
//                        ldec_pct = 0.65;
//                    }else if( istr_prop.cb_id == 6 ){ //bulanan
//                        ldec_pct = 0.12;
//                    }
//                    ldec_temp = FormatNumber.round( (ldec_rate) * 0.12, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("825"), new BigDecimal( istr_prop.rider_baru[ 15 ] ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                if( istr_prop.rider_baru[ 15 ] > 0 && istr_prop.ekaSehatInnerLimit.peserta > 0 && li_usia < 75 )
                {
                    for ( int i = 1; i <= istr_prop.ekaSehatInnerLimit.peserta; i++)
                    {
                        li_usia = istr_prop.ekaSehatInnerLimit.usia[ i ] + ai_th - 1;

                        Map<String, Object> params = new HashMap<String, Object>();
                        params.put( "riderBaru", istr_prop.rider_baru[ 15 ] );
                        params.put( "kursId", istr_prop.kurs_id );
                        params.put( "liUsia", li_usia );
//                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectEkaSehatInnerLimitRider( params ));
//                        ldec_temp = FormatNumber.round((ldec_rate * 0.117), 2);
//                        ldec_total += ldec_temp;

                        Integer lsdbs_e_sehat = null;
                        if( i == 1 ) {
                            lsdbs_e_sehat = istr_prop.rider_baru[ 15 ] + 15;
                        }else if( i == 2 ) {
                            lsdbs_e_sehat = istr_prop.rider_baru[ 15 ] + ( 15 * 2 );
                        }else if( i == 3 ) {
                            lsdbs_e_sehat = istr_prop.rider_baru[ 15 ] + ( 15 * 3 );
                        }else if( i == 4 ) {
                            lsdbs_e_sehat = istr_prop.rider_baru[ 15 ] + ( 15 * 4 );
                        }else if( i == 5 ) {
                            lsdbs_e_sehat = istr_prop.rider_baru[ 15 ] + ( 15 * 5 );
                        }
//                        if( ai_th == 1 ){
//                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("825"), new BigDecimal( lsdbs_e_sehat ),
//                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                    new BigDecimal(istr_prop.cb_id), istr_prop.ekaSehatInnerLimit.tgl[i], istr_prop.ekaSehatInnerLimit.usia[i] ) );
//                        }
                    }
                }
                break;
//
            case 16:
                //hcp Provider
                if( istr_prop.rider_baru[ 16 ] > 0 && li_usia < 65 )
                {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "riderBaru", istr_prop.rider_baru[ 16 ]  );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateHcpProvider( params ) );
//
//                    ldec_temp = FormatNumber.round( ldec_rate * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("826"), new BigDecimal( istr_prop.rider_baru[ 16 ] ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }

                //hcp provider peserta tambahan
                if( istr_prop.rider_baru[ 16 ] > 0 && istr_prop.hcpPro.peserta > 0 && li_usia < 65 )
                {
                    for( int i = 1; i <= istr_prop.hcpPro.peserta; i++ )
                    {
                        li_usia = istr_prop.hcpPro.usia[ i ] + ai_th - 1;

                        Map<String, Object> params = new HashMap<String, Object>();
                        params.put( "riderBaru", istr_prop.rider_baru[ 16 ]  );    // li_jenis in PB = riderBaru, + 140 bcoz start from 141
                        params.put( "kursId", istr_prop.kurs_id );
                        params.put( "liUsia", li_usia );
//                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateHcpProviderPesertaTambahan( params ) );
//                        ldec_temp = 0;
//                        if (!( i > 1 && li_usia > 24 ) ){
//                            ldec_temp = FormatNumber.round( ldec_rate * 0.09, 2 );
//                        }
//                        ldec_total += ldec_temp;

                        Integer lsdbs_hcp_pro= null;
                        if( i == 1 ) {
                            lsdbs_hcp_pro = istr_prop.rider_baru[ 16 ] + 12;
                        }else if( i == 2 ) {
                            lsdbs_hcp_pro = istr_prop.rider_baru[ 16 ] + ( 12 * 2 );
                        }else if( i == 3 ) {
                            lsdbs_hcp_pro = istr_prop.rider_baru[ 16 ] + ( 12 * 3 );
                        }else if( i == 4 ) {
                            lsdbs_hcp_pro = istr_prop.rider_baru[ 16 ] + ( 12 * 4 );
                        }else if( i == 5 ) {
                            lsdbs_hcp_pro = istr_prop.rider_baru[ 16 ] + ( 12 * 5 );
                        }

//                        if( ai_th == 1 ){
//                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("826"), new BigDecimal( lsdbs_hcp_pro ),
//                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                    new BigDecimal(istr_prop.cb_id), istr_prop.hcpPro.tgl[i], istr_prop.hcpPro.usia[i] ) );
//                        }
                    }
                }
                break;
//
            case 17:
                //Waiver TPD/CI/Death
                if( istr_prop.rider_baru[ 17 ] > 0 )
                {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "liJenis", istr_prop.rider_baru[ 17 ] );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liTahunKe", 0 );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateWaiverTpdCi(params) );
//
//                    ldec_max = 200000;
//                    ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi * 1 ) * li_kali / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("827"), new BigDecimal( istr_prop.rider_baru[ 17 ] ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
//
            case 18:
                //Payor TPD/CI/Death
                if( istr_prop.rider_baru[ 18 ] > 0 )
                {
                    li_tahun_ke = istr_prop.umur_pp + ai_th - 1;
                    li_jenis = istr_prop.rider_baru[ 18 ];

                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "liJenis", li_jenis );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liTahunKe", li_tahun_ke );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRatePayorTpdCiDeath(params) );
//
//                    ldec_max = 200000;
////                    ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi * istr_prop.pct_premi / 100 ) * li_kali / 1000 ) * ldec_rate ), 2 );
//                    ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi * 1 ) * li_kali / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("828"), new BigDecimal( li_jenis ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
//
            case 19:
                //ladies insurance
                if( istr_prop.rider_baru[ 19 ] > 0 )
                {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "liJenis", istr_prop.rider_baru[ 19 ] );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liTahunKe", 0 );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateLadiesIns(params) );
//
//                    ldec_max = 200000;
//                    if( istr_prop.kurs_id.equals("01") ){ ldec_max = 2000000000;}
//                    double persentase = MethodSupport.ConverttoDouble( new BigDecimal(cepr01030103Form.getLadiesInsChooseCd())) / 100.0;
//                    double maxAmount = MathUtil.min( istr_prop.up * persentase, ldec_max );
//                    ldec_temp = FormatNumber.round( ( ( maxAmount / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("830"), new BigDecimal( istr_prop.rider_baru[ 19 ] ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
//
            case 20:
                //hcp ladies
                if( istr_prop.rider_baru[ 20 ] > 0 && istr_prop.hcpLad.peserta == 0)
                {
                    if( li_usia < 66 && istr_prop.hcpLad.peserta == 0 ){
                        Map<String, Object> params = new HashMap<String, Object>();
                        params.put( "riderBaru", istr_prop.rider_baru[ 20 ] );   // li_jenis in PB = riderBaru, + 140 bcoz start from 141
                        params.put( "kursId", istr_prop.kurs_id );
                        params.put( "liUsia", li_usia );
//                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateHcpLadies( params ) );
//
//                        ldec_temp = FormatNumber.round( ldec_rate * 0.1, 2 );
//                        ldec_total += ldec_temp;
//                        if( ai_th == 1 ){
//                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("831"), new BigDecimal( istr_prop.rider_baru[ 20 ] ),
//                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                    new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                        }
                    }
                }

                //hcp ladies peserta tambahan
                if( istr_prop.rider_baru[ 20 ] > 0 && istr_prop.hcpLad.peserta > 0 )
                {
                    li_usia = istr_prop.hcpLad.usia[ 1 ] + ai_th - 1;
                    if( li_usia < 66 && istr_prop.hcpLad.peserta > 0 )
                    {
                        for( int i = 1; i <= istr_prop.hcpLad.peserta; i++ )
                        {
                            li_usia = istr_prop.hcpLad.usia[ i ] + ai_th - 1;

                            Map<String, Object> params = new HashMap<String, Object>();
                            params.put( "riderBaru", istr_prop.rider_baru[ 20 ] );    // li_jenis in PB = riderBaru, + 140 bcoz start from 141
                            params.put( "kursId", istr_prop.kurs_id );
                            params.put( "liUsia", li_usia );
//                            ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateHcpLadiesPesertaTambahan( params ) );
//                            ldec_temp = FormatNumber.round( ldec_rate * 0.1, 2);
//                            if(i >= 2){
//                                ldec_temp = FormatNumber.round( ldec_rate * 0.09, 2 );
//                                if(i>=3 && li_usia>25) ldec_temp = 0;
//                            }
//                            ldec_total += ldec_temp;

                            Integer lsdbs_hcp_lad = null;
                            if( i == 1 ) {
                                lsdbs_hcp_lad = istr_prop.rider_baru[ 20 ];
                            }else if( i == 2 ) {
                                lsdbs_hcp_lad = istr_prop.rider_baru[ 20 ] + ( 24 );
                            }else if( i == 3 ) {
                                lsdbs_hcp_lad = istr_prop.rider_baru[ 20 ] + ( 24 * 2 );
                            }else if( i == 4 ) {
                                lsdbs_hcp_lad = istr_prop.rider_baru[ 20 ] + ( 24 * 3 );
                            }else if( i == 5 ) {
                                lsdbs_hcp_lad = istr_prop.rider_baru[ 20 ] + ( 24 * 4 );
                            }
//                            if( ai_th == 1 ){
//                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("831"), new BigDecimal( lsdbs_hcp_lad ),
//                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                        new BigDecimal(istr_prop.cb_id), istr_prop.hcpLad.tgl[i], istr_prop.hcpLad.usia[i] ) );
//                            }
                        }
                    }
                }
                break;
            case 21:
                //ladies medical expense
                if( istr_prop.rider_baru[ 21 ] > 0  && istr_prop.ladiesMedExpense.peserta == 0 && li_usia < 75 )
                {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "riderBaru", istr_prop.rider_baru[ 21 ] );   // li_jenis in PB = riderBaru, + 140 bcoz start from 141
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLadiesMedExpenseRider(params));
//
//                    ldec_temp = FormatNumber.round( ldec_rate * 0.12, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("832"), new BigDecimal( istr_prop.rider_baru[ 21 ] ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }

                //ladies medical expense peserta tambahan


                if( istr_prop.rider_baru[ 21 ] > 0 && istr_prop.ladiesMedExpense.peserta > 0  )
                {
                    li_usia = istr_prop.ladiesMedExpense.usia[1] + ai_th - 1;
                    if( li_usia < 75 ){
                        for( int i = 1; i <= istr_prop.ladiesMedExpense.peserta; i++ )
                        {
                            li_usia = istr_prop.ladiesMedExpense.usia[ i ] + ai_th - 1;

                            Map<String, Object> params = new HashMap<String, Object>();
                            params.put( "riderBaru", istr_prop.rider_baru[ 21 ] );    // li_jenis in PB = riderBaru, + 140 bcoz start from 141
                            params.put( "kursId", istr_prop.kurs_id );
                            params.put( "liUsia", li_usia );
//                            ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLadiesMedExpenseRider( params ) );
//                            ldec_temp = FormatNumber.round( ldec_rate * 0.12, 2 );
//
//                            if(i > 1){
//                                ldec_temp = FormatNumber.round( ldec_rate * 0.117, 2 );
//                            }
//                            ldec_total += ldec_temp;

                            Integer lsdbs_ladies_med = null;
                            if( i == 1 ) {
                                lsdbs_ladies_med = istr_prop.rider_baru[ 21 ];
                            }else if( i == 2 ) {
                                lsdbs_ladies_med = istr_prop.rider_baru[ 21 ] + ( 7 );
                            }else if( i == 3 ) {
                                lsdbs_ladies_med = istr_prop.rider_baru[ 21 ] + ( 7 * 2 );
                            }else if( i == 4 ) {
                                lsdbs_ladies_med = istr_prop.rider_baru[ 21 ] + ( 7 * 3 );
                            }else if( i == 5 ) {
                                lsdbs_ladies_med = istr_prop.rider_baru[ 21 ] + ( 7 * 4 );
                            }

//                            if( ai_th == 1 ){
//                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("832"), new BigDecimal( lsdbs_ladies_med ),
//                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                        new BigDecimal(istr_prop.cb_id), istr_prop.ladiesMedExpense.tgl[i], istr_prop.ladiesMedExpense.usia[i] ) );
//                            }
                        }
                    }
                }
                break;
            case 22:
                //ladies medical expense inner limit
                if( istr_prop.rider_baru[ 22 ] > 0 && li_usia < 75 && istr_prop.ladiesMedExpenseInnerLimit.peserta == 0)
                {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "riderBaru", istr_prop.rider_baru[ 22 ] );   // li_jenis in PB = riderBaru, + 140 bcoz start from 141
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLadiesMedExpenseInnerLimitRider(params));
//
//                    ldec_temp = FormatNumber.round( ldec_rate * 0.12, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("833"), new BigDecimal( istr_prop.rider_baru[ 22 ] ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }

                //ladies medical expense inner limit peserta tambahan
                if( istr_prop.rider_baru[ 22 ] > 0 && istr_prop.ladiesMedExpenseInnerLimit.peserta > 0 )
                {
                    li_usia = istr_prop.ladiesMedExpenseInnerLimit.usia[1] + ai_th - 1;
                    if( li_usia < 75 ){
                        for( int i = 1; i <= istr_prop.ladiesMedExpenseInnerLimit.peserta; i++ )
                        {
                            li_usia = istr_prop.ladiesMedExpenseInnerLimit.usia[ i ] + ai_th - 1;

                            Map<String, Object> params = new HashMap<String, Object>();
                            params.put( "riderBaru", istr_prop.rider_baru[ 22 ] );    // li_jenis in PB = riderBaru, + 140 bcoz start from 141
                            params.put( "kursId", istr_prop.kurs_id );
                            params.put( "liUsia", li_usia );
//                            ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLadiesMedExpenseInnerLimitRider( params ) );
//                            ldec_temp = FormatNumber.round( ldec_rate * 0.12, 2 );
//                            if(i > 1){
//                                ldec_temp = FormatNumber.round( ldec_rate * 0.117, 2 );
//                            }
//                            ldec_total += ldec_temp;

                            Integer lsdbs_ladies_med_il = null;
                            if( i == 1 ) {
                                lsdbs_ladies_med_il = istr_prop.rider_baru[ 22 ];
                            }else if( i == 2 ) {
                                lsdbs_ladies_med_il = istr_prop.rider_baru[ 22 ] + ( 7 );
                            }else if( i == 3 ) {
                                lsdbs_ladies_med_il = istr_prop.rider_baru[ 22 ] + ( 7 * 2 );
                            }else if( i == 4 ) {
                                lsdbs_ladies_med_il = istr_prop.rider_baru[ 22 ] + ( 7 * 3 );
                            }else if( i == 5 ) {
                                lsdbs_ladies_med_il = istr_prop.rider_baru[ 22 ] + ( 7 * 4 );
                            }

//                            if( ai_th == 1 ){
//                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("833"), new BigDecimal( lsdbs_ladies_med_il ),
//                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                        new BigDecimal(istr_prop.cb_id), istr_prop.ladiesMedExpenseInnerLimit.tgl[i], istr_prop.ladiesMedExpenseInnerLimit.usia[i] ) );
//                            }
                        }
                    }
                }
                break;
            case 23:
                //Payor 5/10 TPD/CI/Death
                if( istr_prop.rider_baru[ 23 ] > 0 )
                {
                    li_tahun_ke = istr_prop.umur_pp + ai_th - 1;
                    li_jenis = istr_prop.rider_baru[ 23 ];

                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "liJenis", li_jenis );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liTahunKe", li_tahun_ke );
                    params.put( "liUsia", ai_th );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRatePayorTpdCiDeath(params) );
//
//                    ldec_max = 200000;
//                    if( istr_prop.kurs_id == "01" ) ldec_max = 2000000000;
//                    ldec_temp = FormatNumber.round( ( ( ( ( istr_prop.premi * istr_prop.pct_premi / 100 ) * li_kali / 1000 ) * ldec_rate)  * 0.1 ), 2 );
//                    if( istr_prop.bisnis_id == 120 ||  istr_prop.bisnis_id == 202){
//                        ldec_temp = FormatNumber.round( ( ( ( ldec_rate / 1000 ) * ( istr_prop.premi * li_kali)) * 0.1 ), 2 );
//                    }
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("828"), new BigDecimal( li_jenis ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
            case 24:
                //Waiver 5/10 TPD/CI
                if( istr_prop.rider_baru[ 24 ] > 0 )
                {
                    li_usia = istr_prop.umur_tt + ai_th - 1;
                    li_jenis = istr_prop.rider_baru[24]	;
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "liJenis", li_jenis );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liTahunKe", ai_th );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateWaiverTpdCi(params) );
//
//                    ldec_max = 200000;
////            		ldec_temp = Round(((ldec_rate/1000)*(istr_prop.premi *li_kali))*0.1, 2)
//                    ldec_temp = FormatNumber.round( ( ( ldec_rate / 1000 )*( istr_prop.premi * li_kali ) ) * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("827"), new BigDecimal( li_jenis ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
            case 25:
                if( istr_prop.rider_baru[25] > 0 && li_usia < istr_prop.usia_schol ){
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liUsia", li_usia_pp );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateScholarship( params ) );

                    Map<String, Object> paramsRate = new HashMap<String, Object>();
                    Integer lijenis = 0;
                    if( istr_prop.usia_schol == 22 ){
                        lijenis = 1;
                    }else if( istr_prop.usia_schol == 25 ){
                        lijenis = 2;
                    }
                    paramsRate = new HashMap<String, Object>();
                    paramsRate.put( "lsbsId", "835" );
                    paramsRate.put( "liJenis", lijenis );
                    paramsRate.put( "liUsia", li_usia );
                    paramsRate.put( "kursId", istr_prop.kurs_id );
//                    BigDecimal rate_factor = eproposalManager.selectTableFactor( paramsRate );
//                    if( rate_factor == null ) { rate_factor = BigDecimal.ZERO;}
//                    double rate_factor_double = MethodSupport.ConverttoDouble( rate_factor );
////                  153.240.000 * 1,79 /1000 *10% = 27.430 (yg ini yg bener ya)
////                  200.000.000 * 1.79 / 1000 * 10% = 35.800
//
//                    ldec_temp = FormatNumber.round( ( ( ldec_rate / 1000 )* istr_prop.up_schol * rate_factor_double ) * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("835"), new BigDecimal( lijenis ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
//
            case 27:
                if( istr_prop.rider_baru[27] > 0  ){
//                    String genderInsured = cepr01030101Form.getInsuredSexCd().trim();
//                    li_jenis=0;
//                    if(genderInsured.equals("L")){
//                        li_jenis = 1;
//                    }
//                    else if(genderInsured.equals("P")){
//                        li_jenis = 2;
//                    }

//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put( "lku_id", istr_prop.kurs_id );
//                    params.put( "lsr_jenis", li_jenis );
//                    params.put( "li_usia", li_usia);
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateEarlyStageCi99( params ) );
//
//                    double persentase = MethodSupport.ConverttoDouble( new BigDecimal(cepr01030103Form.getEsci99ChooseCd())) / 100.0;
//                    double upEsci99 = ( istr_prop.up * persentase );
//                    double maxUp;
//
//                    double temp = MathUtil.min( upEsci99, maxUp );
//                    ldec_temp = FormatNumber.round(((ldec_rate / 1000) * temp) * 0.1, 2);
//                    ldec_total += ldec_temp;

//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("837"), new BigDecimal( li_jenis ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
//
            case 28:
                //SMiLe Medical (+)
                if( istr_prop.rider_baru[28] > 0 ){
                    Map<String, Object> params = new HashMap<String, Object>();
                    int riderBaru = istr_prop.rider_baru[ 28 ];

                    params.put( "riderBaru", riderBaru );
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRider( params ) );
//
//                    if( istr_prop.cb_id == 1 ){  //triwulan
//                        ldec_pct = 0.35;
//                    }else if( istr_prop.cb_id == 2 ){ //semesteran
//                        ldec_pct = 0.65;
//                    }else if( istr_prop.cb_id == 6 ){ //bulanan
//                        ldec_pct = 0.12;
//                    }
////                        ldec_temp = FormatNumber.round( (ldec_rate * ldec_pct) * 0.1, 2 );
//                    ldec_temp = FormatNumber.round( (ldec_rate) * 0.12, 2 );
//                    ldec_total += ldec_temp;

//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("838"), new BigDecimal( riderBaru ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }

//                    if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag())){
//                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
//                        ldec_temp = FormatNumber.round( (ldec_rate) * 0.12, 2 );
//                        ldec_total += ldec_temp;
//
////                        if( ai_th == 1 ){
////                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
////                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                    new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                        }
//                    }
//                    if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag()) && CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRgFlag())){
//                        riderBaru = istr_prop.rider_baru[ 28 ]+20;
//
//                        params.put( "riderBaru", riderBaru );
//                        params.put( "kursId", istr_prop.kurs_id );
//                        params.put( "liUsia", li_usia );
//                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
//                        ldec_temp = FormatNumber.round( (ldec_rate) * 0.12, 2 );
//                        ldec_total += ldec_temp;

//                        if( ai_th == 1 ){
//                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
//                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                    new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                        }
//                    }
//                    if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag()) && CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRbFlag())){
//                        riderBaru = istr_prop.rider_baru[ 28 ]+40;
//
//                        params.put( "riderBaru", riderBaru );
//                        params.put( "kursId", istr_prop.kurs_id );
//                        params.put( "liUsia", li_usia );
//                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
//                        ldec_temp = FormatNumber.round( (ldec_rate) * 0.12, 2 );
//                        ldec_total += ldec_temp;

//                        if( ai_th == 1 ){
//                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
//                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                    new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                        }
                }
//                    if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag()) && CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusPkFlag())){
//                        riderBaru = istr_prop.rider_baru[ 28 ]+60;
//
//                        params.put( "riderBaru", riderBaru );
//                        params.put( "kursId", istr_prop.kurs_id );
//                        params.put( "liUsia", li_usia );
//                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
//                        ldec_temp = FormatNumber.round( (ldec_rate) * 0.12, 2 );
//                        ldec_total += ldec_temp;

//                        if( ai_th == 1 ){
//                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
//                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                    new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                        }
//                    }
//                }
                //SMiLe Medical(+) Peserta Tambahan
                if( istr_prop.rider_baru[ 28 ] > 0 && istr_prop.medicalPlus.peserta > 0   && li_usia < 75 )
                {
                    for( int i = 1; i <= istr_prop.medicalPlus.peserta; i++ )
                    {
                        li_usia = istr_prop.medicalPlus.usia[ i ] + ai_th - 1;
                        int riderBaru = istr_prop.rider_baru[ 28 ];

                        if(i==1){
                            riderBaru = riderBaru + 4;
                        }
                        else if(i==2){
                            riderBaru = riderBaru + 8;
                        }
                        else if(i==3){
                            riderBaru = riderBaru + 12;
                        }
                        else if(i==4){
                            riderBaru = riderBaru + 16;
                        }
                        Map<String, Object> params = new HashMap<String, Object>();
                        params.put( "riderBaru", riderBaru );
                        params.put( "kursId", istr_prop.kurs_id );
                        params.put( "liUsia", li_usia );
//                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRider( params ) );
//                        ldec_temp = FormatNumber.round( (ldec_rate * 0.117) , 2 );
//                        ldec_total += ldec_temp;

//                        if( ai_th == 1 ){
//                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("838"), new BigDecimal( riderBaru ),
//                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                    new BigDecimal(istr_prop.cb_id), istr_prop.medicalPlus.tgl[i], istr_prop.medicalPlus.usia[i] ) );
//                        }

//                        if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag())){
//                            ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
//                            ldec_temp = FormatNumber.round( (ldec_rate * 0.117) , 2 );
//                            ldec_total += ldec_temp;

//                            if( ai_th == 1 ){
//                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
//                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                        new BigDecimal(istr_prop.cb_id),  istr_prop.medicalPlus.tgl[i], istr_prop.medicalPlus.usia[i] ) );
//                            }
//                        }
//                        if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag()) && CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRgFlag())){
//                            riderBaru = istr_prop.rider_baru[ 28 ]+20;

                        if(i==1){
                            riderBaru = riderBaru + 4;
                        }
                        else if(i==2){
                            riderBaru = riderBaru + 8;
                        }
                        else if(i==3){
                            riderBaru = riderBaru + 12;
                        }
                        else if(i==4){
                            riderBaru = riderBaru + 16;
                        }

                        params.put( "riderBaru", riderBaru );
                        params.put( "kursId", istr_prop.kurs_id );
                        params.put( "liUsia", li_usia );
//                            ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
//                            ldec_temp = FormatNumber.round( (ldec_rate * 0.117) , 2 );
//                            ldec_total += ldec_temp;

//                            if( ai_th == 1 ){
//                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
//                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                        new BigDecimal(istr_prop.cb_id),  istr_prop.medicalPlus.tgl[i], istr_prop.medicalPlus.usia[i] ) );
//                            }
//                        }
//                        if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag()) && CommonConst.CHECKED_TRUE.equals(istr_prop.medicalPlus.medicalPlusRbFlag[i])){
//                            riderBaru = istr_prop.rider_baru[ 28 ]+40;

                        if(i==1){
                            riderBaru = riderBaru + 4;
                        }
                        else if(i==2){
                            riderBaru = riderBaru + 8;
                        }
                        else if(i==3){
                            riderBaru = riderBaru + 12;
                        }
                        else if(i==4){
                            riderBaru = riderBaru + 16;
                        }
                        params.put( "riderBaru", riderBaru );
                        params.put( "kursId", istr_prop.kurs_id );
                        params.put( "liUsia", li_usia );
//                            ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
//                            ldec_temp = FormatNumber.round( (ldec_rate * 0.117) , 2 );
//                            ldec_total += ldec_temp;

//                            if( ai_th == 1 ){
//                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
//                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                        new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                            }
//                        }
//                        if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag()) && CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusPkFlag())){
//                            riderBaru = istr_prop.rider_baru[ 28 ]+60;

                        if(i==1){
                            riderBaru = riderBaru + 4;
                        }
                        else if(i==2){
                            riderBaru = riderBaru + 8;
                        }
                        else if(i==3){
                            riderBaru = riderBaru + 12;
                        }
                        else if(i==4){
                            riderBaru = riderBaru + 16;
                        }
                        params.put( "riderBaru", riderBaru );
                        params.put( "kursId", istr_prop.kurs_id );
                        params.put( "liUsia", li_usia );
//                            ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
//                            ldec_temp = FormatNumber.round( (ldec_rate * 0.117) , 2 );
//                            ldec_total += ldec_temp;

//                            if( ai_th == 1 ){
//                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
//                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                        new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
                    }
                }
//                    }
//                }
                break;
            case 112:
                //term rider header
                if( istr_prop.rider_baru[ 12 ] > 0 )
                {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put( "kursId", istr_prop.kurs_id );
                    params.put( "liUsia", li_usia );
//                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateTermRider( params ) );
//                    ldec_temp = FormatNumber.round( ( ( istr_prop.up_term / 1000 ) * ldec_rate ) * 0.1, 2 );
//                    ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("818"), new BigDecimal( "2" ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                }
                break;
            default:
                break;
        }
////        cepr01031001Form.setMstProposal( mstProposal );
        return ldec_total;
    }
}
