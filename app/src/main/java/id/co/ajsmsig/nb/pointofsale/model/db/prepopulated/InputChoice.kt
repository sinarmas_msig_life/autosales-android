package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */
@Entity
class InputChoice(
        @PrimaryKey
        val id: Int,
        var text: String?,
        var inputId: Int?,
        var value: Long?,
        var minAge: Int?,
        var maxAge: Int?
): Option() {
        override fun toString(): String {
                return text ?: ""
        }
}