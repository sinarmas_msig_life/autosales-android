package id.co.ajsmsig.nb.espaj.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.AutoCompleteTextViewClickListener;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;

/**
 * Created by Kur 2018-06-21.
 */

public class E_FormAlamatAutoZipActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, RadioGroup.OnCheckedChangeListener, View.OnFocusChangeListener {
    private static final String TAG = E_FormAlamatAutoZipActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.e_activity_form_alamat_autozip);
        ActionBar toolbar = getSupportActionBar();
        toolbar.setDisplayHomeAsUpEnabled(true);
        toolbar.setDisplayShowHomeEnabled(true);
        ButterKnife.bind(this);

        intent = getIntent();
        flag_input = intent.getIntExtra(getString(R.string.flag), 0);
        me = intent.getParcelableExtra(getString(R.string.modelespaj));
        if (flag_input == 7 || flag_input == 8) {
            toolbar.setTitle("Form isi Hp");
            cl_form_info_hp.setVisibility(View.VISIBLE);
            cl_form_info_umum.setVisibility(View.GONE);
            tv_notice_alamat.setVisibility(View.GONE);
        } else {
            toolbar.setTitle("Form Alamat");
            cl_form_info_hp.setVisibility(View.GONE);
            cl_form_info_umum.setVisibility(View.VISIBLE);
        }
        boolean isShow = tv_notice_alamat.getVisibility() == View.VISIBLE;

        et_alamat.addTextChangedListener(new MTextWatcher(et_alamat));
//        ac_provinsi.addTextChangedListener(new MTextWatcher(ac_provinsi));
        ac_provinsi.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_provinsi, (AdapterView.OnItemClickListener) this));
        ac_provinsi.setOnClickListener(this);
        ac_provinsi.setOnFocusChangeListener(this);
        ac_provinsi.addTextChangedListener(new MTextWatcher(ac_provinsi));

        ac_kabupaten.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_kabupaten, (AdapterView.OnItemClickListener) this));
        ac_kabupaten.setOnClickListener(this);
        ac_kabupaten.setOnFocusChangeListener(this);
        ac_kabupaten.addTextChangedListener(new MTextWatcher(ac_kabupaten));

        ac_kecamatan.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_kecamatan, (AdapterView.OnItemClickListener) this));
        ac_kecamatan.setOnClickListener(this);
        ac_kecamatan.setOnFocusChangeListener(this);
        ac_kecamatan.addTextChangedListener(new MTextWatcher(ac_kecamatan));

        ac_kelurahan.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_kelurahan, (AdapterView.OnItemClickListener) this));
        ac_kelurahan.setOnClickListener(this);
        ac_kelurahan.setOnFocusChangeListener(this);
        ac_kelurahan.addTextChangedListener(new MTextWatcher(ac_kelurahan));


        et_hp1.addTextChangedListener(new MTextWatcher(et_hp1));
        et_email.addTextChangedListener(new MTextWatcher(et_email));

        btn_lanjut.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);

        SetAdapterPropinsi();

        restore();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_lanjut:
                load();
                if (Validation()) {
                    intent = new Intent();
                    intent.putExtra(getString(R.string.modelespaj), me);
                    intent.putExtra(getString(R.string.flag), flag_input);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
            case R.id.btn_cancel:
                et_alamat.setText("");
                ac_provinsi.setText("");
                ac_kabupaten.setText("");
                ac_kecamatan.setText("");
                ac_kelurahan.setText("");
                etPostalCode.setText("");
                et_kdtelp1.setText("");
                et_telp1.setText("");
                et_kdtelp2.setText("");
                et_telp2.setText("");
                et_kdfax.setText("");
                et_fax.setText("");
                load();
                intent = new Intent();
                intent.putExtra(getString(R.string.modelespaj), me);
                intent.putExtra(getString(R.string.flag), flag_input);
                setResult(RESULT_OK, intent);
                finish();
                break;

            case R.id.ac_provinsi:
                ac_provinsi.showDropDown();
                break;
            case R.id.ac_kabupaten:
                ac_kabupaten.showDropDown();
                break;
            case R.id.ac_kecamatan:
                ac_kecamatan.showDropDown();
                break;
            case R.id.ac_kelurahan:
                ac_kelurahan.showDropDown();
                break;

        }
    }

    private void load() {
        switch (flag_input) {
            //PP
            case 1: //rumah
                me.getPemegangPolisModel().setAlamat_pp(et_alamat.getText().toString());
                me.getPemegangPolisModel().setPropinsi_pp(propinsi);
                me.getPemegangPolisModel().setKabupaten_pp(kabupaten);
                me.getPemegangPolisModel().setKecamatan_pp(kecamatan);
                me.getPemegangPolisModel().setKelurahan_pp(kelurahan);
                me.getPemegangPolisModel().setKdpos_pp(kode_pos);
                me.getPemegangPolisModel().setKdtelp1_pp(et_kdtelp1.getText().toString());
                me.getPemegangPolisModel().setTelp1_pp(et_telp1.getText().toString());
                me.getPemegangPolisModel().setKdtelp2_pp(et_kdtelp2.getText().toString());
                me.getPemegangPolisModel().setTelp2_pp(et_telp2.getText().toString());
                break;
            case 2: //kantor
                me.getPemegangPolisModel().setAlamat_kantor_pp(et_alamat.getText().toString());
                me.getPemegangPolisModel().setPropinsi_kantor_pp(propinsi);
                me.getPemegangPolisModel().setKabupaten_kantor_pp(kabupaten);
                me.getPemegangPolisModel().setKecamatan_kantor_pp(kecamatan);
                me.getPemegangPolisModel().setKelurahan_kantor_pp(kelurahan);
                me.getPemegangPolisModel().setKdpos_kantor_pp(kode_pos);
                me.getPemegangPolisModel().setKdtelp1_kantor_pp(et_kdtelp1.getText().toString());
                me.getPemegangPolisModel().setTelp1_kantor_pp(et_telp1.getText().toString());
                me.getPemegangPolisModel().setKdtelp2_kantor_pp(et_kdtelp2.getText().toString());
                me.getPemegangPolisModel().setTelp2_kantor_pp(et_telp2.getText().toString());
                me.getPemegangPolisModel().setKdfax_kantor_pp(et_kdfax.getText().toString());
                me.getPemegangPolisModel().setFax_kantor_pp(et_fax.getText().toString());
                break;
            case 3: //tempat tinggal
                me.getPemegangPolisModel().setAlamat_tinggal_pp(et_alamat.getText().toString());
                me.getPemegangPolisModel().setPropinsi_tinggal_pp(propinsi);
                me.getPemegangPolisModel().setKabupaten_tinggal_pp(kabupaten);
                me.getPemegangPolisModel().setKecamatan_tinggal_pp(kecamatan);
                me.getPemegangPolisModel().setKelurahan_tinggal_pp(kelurahan);
                me.getPemegangPolisModel().setKdpos_tinggal_pp(kode_pos);
                me.getPemegangPolisModel().setKdtelp1_tinggal_pp(et_kdtelp1.getText().toString());
                me.getPemegangPolisModel().setTelp1_tinggal_pp(et_telp1.getText().toString());
                me.getPemegangPolisModel().setKdtelp2_tinggal_pp(et_kdtelp2.getText().toString());
                me.getPemegangPolisModel().setTelp2_tinggal_pp(et_telp2.getText().toString());
                me.getPemegangPolisModel().setKdfax_tinggal_pp(et_kdfax.getText().toString());
                me.getPemegangPolisModel().setFax_tinggal_pp(et_fax.getText().toString());
                break;
            //TT
            case 4: //rumah
                me.getTertanggungModel().setAlamat_tt(et_alamat.getText().toString());
                me.getTertanggungModel().setPropinsi_tt(propinsi);
                me.getTertanggungModel().setKabupaten_tt(kabupaten);
                me.getTertanggungModel().setKecamatan_tt(kecamatan);
                me.getTertanggungModel().setKelurahan_tt(kelurahan);
                me.getTertanggungModel().setKdpos_tt(kode_pos);
                me.getTertanggungModel().setKdtelp1_tt(et_kdtelp1.getText().toString());
                me.getTertanggungModel().setTelp1_tt(et_telp1.getText().toString());
                me.getTertanggungModel().setKdtelp2_tt(et_kdtelp2.getText().toString());
                me.getTertanggungModel().setTelp2_tt(et_telp2.getText().toString());
                break;
            case 5: //kantor
                me.getTertanggungModel().setAlamat_kantor_tt(et_alamat.getText().toString());
                me.getTertanggungModel().setPropinsi_kantor_tt(propinsi);
                me.getTertanggungModel().setKabupaten_kantor_tt(kabupaten);
                me.getTertanggungModel().setKecamatan_kantor_tt(kecamatan);
                me.getTertanggungModel().setKelurahan_kantor_tt(kelurahan);
                me.getTertanggungModel().setKdpos_kantor_tt(kode_pos);
                me.getTertanggungModel().setKdtelp1_kantor_tt(et_kdtelp1.getText().toString());
                me.getTertanggungModel().setTelp1_kantor_tt(et_telp1.getText().toString());
                me.getTertanggungModel().setKdtelp2_kantor_tt(et_kdtelp2.getText().toString());
                me.getTertanggungModel().setTelp2_kantor_tt(et_telp2.getText().toString());
//                me.getTertanggungModel().setKdfax_kantor_tt(et_kdfax.getText().toString());
                me.getTertanggungModel().setFax_kantor_tt(et_fax.getText().toString());
                break;
            case 6: //tempat tinggal
                me.getTertanggungModel().setAlamat_tinggal_tt(et_alamat.getText().toString());
                me.getTertanggungModel().setPropinsi_tinggal_tt(propinsi);
                me.getTertanggungModel().setKabupaten_tinggal_tt(kabupaten);
                me.getTertanggungModel().setKecamatan_tinggal_tt(kecamatan);
                me.getTertanggungModel().setKelurahan_tinggal_tt(kelurahan);
                me.getTertanggungModel().setKdpos_tinggal_tt(kode_pos);
                me.getTertanggungModel().setKdtelp1_tinggal_tt(et_kdtelp1.getText().toString());
                me.getTertanggungModel().setTelp1_tinggal_tt(et_telp1.getText().toString());
                me.getTertanggungModel().setKdtelp2_tinggal_tt(et_kdtelp2.getText().toString());
                me.getTertanggungModel().setTelp2_tinggal_tt(et_telp2.getText().toString());
                me.getTertanggungModel().setKdfax_tinggal_tt(et_kdfax.getText().toString());
                me.getTertanggungModel().setFax_tinggal_tt(et_fax.getText().toString());
                break;
            case 7: //pp
                me.getPemegangPolisModel().setHp1_pp(et_hp1.getText().toString());
                me.getPemegangPolisModel().setHp2_pp(et_hp2.getText().toString());
                me.getPemegangPolisModel().setEmail_pp(et_email.getText().toString());
                break;
            case 8: //tt
                me.getTertanggungModel().setHp1_tt(et_hp1.getText().toString());
                me.getTertanggungModel().setHp2_tt(et_hp2.getText().toString());
                me.getTertanggungModel().setEmail_tt(et_email.getText().toString());
                break;
        }
    }

    private void restore() {
        switch (flag_input) {
            //PP
            case 1: //rumah
                et_alamat.setText(me.getPemegangPolisModel().getAlamat_pp());
//                ac_provinsi.setText(me.getPemegangPolisModel().getProvinsi_pp());

                propinsi = me.getPemegangPolisModel().getPropinsi_pp();
                ListDropDownProvinsi = new E_Select(this).getLst_Propinsi_PP();
                ac_provinsi.setText(MethodSupport.getAdapterPositionString(ListDropDownProvinsi, propinsi));
//
                kabupaten = me.getPemegangPolisModel().getKabupaten_pp();
                ListDropDownKabupaten = new E_Select(this).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_pp());
                ac_kabupaten.setText(MethodSupport.getAdapterPositionString(ListDropDownKabupaten, kabupaten));

                kecamatan = me.getPemegangPolisModel().getKecamatan_pp();
                ListDropDownKecamatan = new E_Select(this).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_pp());
                ac_kecamatan.setText(MethodSupport.getAdapterPositionString(ListDropDownKecamatan, kecamatan));
//
                kelurahan = me.getPemegangPolisModel().getKelurahan_pp();
                ListDropDownKelurahan = new E_Select(this).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_pp());
                ac_kelurahan.setText(MethodSupport.getAdapterPositionString(ListDropDownKelurahan, kelurahan));

                kode_pos = me.getPemegangPolisModel().getKdpos_pp();
                /*ListDropDownKodepos = new E_Select(this).getLst_Kodepos_PP(me.getPemegangPolisModel().getKelurahan_pp());*/
                etPostalCode.setText(kode_pos);

//                et_kode_pos.setText(me.getPemegangPolisModel().getKdpos_pp());
                et_kdtelp1.setText(me.getPemegangPolisModel().getKdtelp1_pp());
                et_telp1.setText(me.getPemegangPolisModel().getTelp1_pp());
                et_kdtelp2.setText(me.getPemegangPolisModel().getKdtelp2_pp());
                et_telp2.setText(me.getPemegangPolisModel().getTelp2_pp());
                cl_kdfax.setVisibility(View.GONE);
                cl_fax.setVisibility(View.GONE);
                if (!me.getPemegangPolisModel().getAlamat_pp().equals("")) {
                    btn_cancel.setText("HAPUS");
                } else {
                    btn_cancel.setText("BATAL");
                }
                break;
            case 2: //kantor
                et_alamat.setText(me.getPemegangPolisModel().getAlamat_kantor_pp());

                propinsi = me.getPemegangPolisModel().getPropinsi_kantor_pp();
                ListDropDownProvinsi = new E_Select(this).getLst_Propinsi_PP();
                ac_provinsi.setText(MethodSupport.getAdapterPositionString(ListDropDownProvinsi, propinsi));
//
                kabupaten = me.getPemegangPolisModel().getKabupaten_kantor_pp();
                ListDropDownKabupaten = new E_Select(this).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_kantor_pp());
                ac_kabupaten.setText(MethodSupport.getAdapterPositionString(ListDropDownKabupaten, kabupaten));

                kecamatan = me.getPemegangPolisModel().getKecamatan_kantor_pp();
                ListDropDownKecamatan = new E_Select(this).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_kantor_pp());
                ac_kecamatan.setText(MethodSupport.getAdapterPositionString(ListDropDownKecamatan, kecamatan));
//
                kelurahan = me.getPemegangPolisModel().getKelurahan_kantor_pp();
                ListDropDownKelurahan = new E_Select(this).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_kantor_pp());
                ac_kelurahan.setText(MethodSupport.getAdapterPositionString(ListDropDownKelurahan, kelurahan));

                kode_pos = me.getPemegangPolisModel().getKdpos_kantor_pp();
                /*ListDropDownKodepos = new E_Select(this).getLst_Kodepos_PP(me.getPemegangPolisModel().getKelurahan_kantor_pp());*/
                etPostalCode.setText(kode_pos);

//                et_kode_pos.setText(me.getPemegangPolisModel().getKdpos_kantor_pp());
                et_kdtelp1.setText(me.getPemegangPolisModel().getKdtelp1_kantor_pp());
                et_telp1.setText(me.getPemegangPolisModel().getTelp1_kantor_pp());
                et_kdtelp2.setText(me.getPemegangPolisModel().getKdtelp2_kantor_pp());
                et_telp2.setText(me.getPemegangPolisModel().getTelp2_kantor_pp());
                et_kdfax.setText(me.getPemegangPolisModel().getKdfax_kantor_pp());
                et_fax.setText(me.getPemegangPolisModel().getFax_kantor_pp());
                if (!me.getPemegangPolisModel().getAlamat_kantor_pp().equals("")) {
                    btn_cancel.setText("HAPUS");
                } else {
                    btn_cancel.setText("BATAL");
                }
                break;
            case 3: //tempat tinggal
                et_alamat.setText(me.getPemegangPolisModel().getAlamat_tinggal_pp());

                propinsi = me.getPemegangPolisModel().getPropinsi_tinggal_pp();
                ListDropDownProvinsi = new E_Select(this).getLst_Propinsi_PP();
                ac_provinsi.setText(MethodSupport.getAdapterPositionString(ListDropDownProvinsi, propinsi));
//
                kabupaten = me.getPemegangPolisModel().getKabupaten_tinggal_pp();
                ListDropDownKabupaten = new E_Select(this).getLst_Kabupaten_PP(me.getPemegangPolisModel().getPropinsi_tinggal_pp());
                ac_kabupaten.setText(MethodSupport.getAdapterPositionString(ListDropDownKabupaten, kabupaten));

                kecamatan = me.getPemegangPolisModel().getKecamatan_tinggal_pp();
                ListDropDownKecamatan = new E_Select(this).getLst_Kecamatan_PP(me.getPemegangPolisModel().getKabupaten_tinggal_pp());
                ac_kecamatan.setText(MethodSupport.getAdapterPositionString(ListDropDownKecamatan, kecamatan));
//
                kelurahan = me.getPemegangPolisModel().getKelurahan_tinggal_pp();
                ListDropDownKecamatan = new E_Select(this).getLst_Kelurahan_PP(me.getPemegangPolisModel().getKecamatan_tinggal_pp());
                ac_kelurahan.setText(MethodSupport.getAdapterPositionString(ListDropDownKecamatan, kelurahan));

                kode_pos = me.getPemegangPolisModel().getKdpos_tinggal_pp();
                /* ListDropDownKodepos = new E_Select(this).getLst_Kodepos_PP(me.getPemegangPolisModel().getKelurahan_tinggal_pp());*/
                etPostalCode.setText(kode_pos);

//                et_kode_pos.setText(me.getPemegangPolisModel().getKdpos_tinggal_pp());
                et_kdtelp1.setText(me.getPemegangPolisModel().getKdtelp1_tinggal_pp());
                et_telp1.setText(me.getPemegangPolisModel().getTelp1_tinggal_pp());
                et_kdtelp2.setText(me.getPemegangPolisModel().getKdtelp2_tinggal_pp());
                et_telp2.setText(me.getPemegangPolisModel().getTelp2_tinggal_pp());
                et_kdfax.setText(me.getPemegangPolisModel().getKdfax_tinggal_pp());
                et_fax.setText(me.getPemegangPolisModel().getFax_tinggal_pp());
                if (!me.getPemegangPolisModel().getAlamat_tinggal_pp().equals("")) {
                    btn_cancel.setText("HAPUS");
                } else {
                    btn_cancel.setText("BATAL");
                }
                break;
            //TT
            case 4: //rumah
                et_alamat.setText(me.getTertanggungModel().getAlamat_tt());

                propinsi = me.getTertanggungModel().getPropinsi_tt();
                ListDropDownProvinsi = new E_Select(this).getLst_Propinsi_PP();
                ac_provinsi.setText(MethodSupport.getAdapterPositionString(ListDropDownProvinsi, propinsi));
//
                kabupaten = me.getTertanggungModel().getKabupaten_tt();
                ListDropDownKabupaten = new E_Select(this).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_tt());
                ac_kabupaten.setText(MethodSupport.getAdapterPositionString(ListDropDownKabupaten, kabupaten));

                kecamatan = me.getTertanggungModel().getKecamatan_tt();
                ListDropDownKecamatan = new E_Select(this).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_tt());
                ac_kecamatan.setText(MethodSupport.getAdapterPositionString(ListDropDownKecamatan, kecamatan));
//
                kelurahan = me.getTertanggungModel().getKelurahan_tt();
                ListDropDownKelurahan = new E_Select(this).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_tt());
                ac_kelurahan.setText(MethodSupport.getAdapterPositionString(ListDropDownKelurahan, kelurahan));

                kode_pos = me.getTertanggungModel().getKdpos_tt();
                /*ListDropDownKodepos = new E_Select(this).getLst_Kodepos_PP(me.getTertanggungModel().getKelurahan_tt());*/
                etPostalCode.setText(kode_pos);

//                et_kode_pos.setText(me.getTertanggungModel().getKdpos_tt());
                et_kdtelp1.setText(me.getTertanggungModel().getKdtelp1_tt());
                et_telp1.setText(me.getTertanggungModel().getTelp1_tt());
                et_kdtelp2.setText(me.getTertanggungModel().getKdtelp2_tt());
                et_telp2.setText(me.getTertanggungModel().getTelp2_tt());
                cl_kdfax.setVisibility(View.GONE);
                cl_fax.setVisibility(View.GONE);
                if (!me.getTertanggungModel().getAlamat_tt().equals("")) {
                    btn_cancel.setText("HAPUS");
                } else {
                    btn_cancel.setText("BATAL");
                }
                break;
            case 5: //kantor
                et_alamat.setText(me.getTertanggungModel().getAlamat_kantor_tt());

                propinsi = me.getTertanggungModel().getPropinsi_kantor_tt();
                ListDropDownProvinsi = new E_Select(this).getLst_Propinsi_PP();
                ac_provinsi.setText(MethodSupport.getAdapterPositionString(ListDropDownProvinsi, propinsi));
//
                kabupaten = me.getTertanggungModel().getKabupaten_kantor_tt();
                ListDropDownKabupaten = new E_Select(this).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_tt());
                ac_kabupaten.setText(MethodSupport.getAdapterPositionString(ListDropDownKabupaten, kabupaten));

                kecamatan = me.getTertanggungModel().getKecamatan_kantor_tt();
                ListDropDownKecamatan = new E_Select(this).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_kantor_tt());
                ac_kecamatan.setText(MethodSupport.getAdapterPositionString(ListDropDownKecamatan, kecamatan));
//
                kelurahan = me.getTertanggungModel().getKelurahan_kantor_tt();
                ListDropDownKelurahan = new E_Select(this).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_kantor_tt());
                ac_kelurahan.setText(MethodSupport.getAdapterPositionString(ListDropDownKelurahan, kelurahan));

                kode_pos = me.getTertanggungModel().getKdpos_kantor_tt();
                /*ListDropDownKodepos = new E_Select(this).getLst_Kodepos_PP(me.getTertanggungModel().getKelurahan_kantor_tt());*/
                etPostalCode.setText(kode_pos);

//                et_kode_pos.setText(me.getTertanggungModel().getKdpos_kantor_tt());
                et_kdtelp1.setText(me.getTertanggungModel().getKdtelp1_kantor_tt());
                et_telp1.setText(me.getTertanggungModel().getTelp1_kantor_tt());
                et_kdtelp2.setText(me.getTertanggungModel().getKdtelp2_kantor_tt());
                et_telp2.setText(me.getTertanggungModel().getTelp1_kantor_tt());
//                et_kdfax.setText(me.getTertanggungModel().getko());
                et_fax.setText(me.getTertanggungModel().getFax_kantor_tt());
                if (!me.getTertanggungModel().getAlamat_kantor_tt().equals("")) {
                    btn_cancel.setText("HAPUS");
                } else {
                    btn_cancel.setText("BATAL");
                }
                break;
            case 6: //tempat tinggal
                et_alamat.setText(me.getTertanggungModel().getAlamat_tinggal_tt());

                propinsi = me.getTertanggungModel().getPropinsi_tinggal_tt();
                ListDropDownProvinsi = new E_Select(this).getLst_Propinsi_PP();
                ac_provinsi.setText(MethodSupport.getAdapterPositionString(ListDropDownProvinsi, propinsi));
//
                kabupaten = me.getTertanggungModel().getKabupaten_tinggal_tt();
                ListDropDownKabupaten = new E_Select(this).getLst_Kabupaten_PP(me.getTertanggungModel().getPropinsi_tinggal_tt());
                ac_kabupaten.setText(MethodSupport.getAdapterPositionString(ListDropDownKabupaten, kabupaten));

                kecamatan = me.getPemegangPolisModel().getKecamatan_tinggal_pp();
                ListDropDownKecamatan = new E_Select(this).getLst_Kecamatan_PP(me.getTertanggungModel().getKabupaten_tinggal_tt());
                ac_kecamatan.setText(MethodSupport.getAdapterPositionString(ListDropDownKecamatan, kecamatan));
//
                kelurahan = me.getPemegangPolisModel().getKelurahan_tinggal_pp();
                ListDropDownKecamatan = new E_Select(this).getLst_Kelurahan_PP(me.getTertanggungModel().getKecamatan_kantor_tt());
                ac_kelurahan.setText(MethodSupport.getAdapterPositionString(ListDropDownKecamatan, kelurahan));

                kode_pos = me.getTertanggungModel().getKdpos_tinggal_tt();
                /*ListDropDownKodepos = new E_Select(this).getLst_Kodepos_PP(me.getTertanggungModel().getKelurahan_tinggal_tt());*/
                etPostalCode.setText(kode_pos);

//                et_kode_pos.setText(me.getTertanggungModel().getKdpos_tinggal_tt());
                et_kdtelp1.setText(me.getTertanggungModel().getKdtelp1_tinggal_tt());
                et_telp1.setText(me.getTertanggungModel().getTelp1_tinggal_tt());
                et_kdtelp2.setText(me.getTertanggungModel().getKdtelp2_tinggal_tt());
                et_telp2.setText(me.getTertanggungModel().getTelp1_tinggal_tt());
                et_kdfax.setText(me.getTertanggungModel().getKdfax_tinggal_tt());
                et_fax.setText(me.getTertanggungModel().getFax_tinggal_tt());
                if (!me.getTertanggungModel().getAlamat_tinggal_tt().equals("")) {
                    btn_cancel.setText("HAPUS");
                } else {
                    btn_cancel.setText("BATAL");
                }
                break;
            case 7: //pp
                et_hp1.setText(me.getPemegangPolisModel().getHp1_pp());
                et_hp2.setText(me.getPemegangPolisModel().getHp2_pp());
                et_email.setText(me.getPemegangPolisModel().getEmail_pp());

                if (me.getPemegangPolisModel().getHp1_pp().equals("")) {
                    btn_cancel.setText("BATAL");
                } else {
                    btn_cancel.setText("HAPUS");
                }
                break;
            case 8: //tt
                et_hp1.setText(me.getTertanggungModel().getHp1_tt());
                et_hp2.setText(me.getTertanggungModel().getHp2_tt());
                et_email.setText(me.getTertanggungModel().getEmail_tt());
                if (me.getTertanggungModel().getHp1_tt().equals("")) {
                    btn_cancel.setText("BATAL");
                } else {
                    btn_cancel.setText("HAPUS");
                }
        }
    }

    private void SetAdapterPropinsi() {
        ArrayList<ModelDropDownString> Dropdown = new E_Select(this).getLst_Propinsi_PP();
        ArrayAdapter<ModelDropDownString> Adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_provinsi.setAdapter(Adapter);

    }

    private void SetAdapterKabupaten(String provinceCode) {
        ArrayList<ModelDropDownString> Dropdown = new E_Select(this).getLst_Kabupaten_PP(provinceCode);
        ArrayAdapter<ModelDropDownString> Adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_kabupaten.setAdapter(Adapter);
    }

    private void setAdapterKecamatan(String kecamatanCode) {
        ArrayList<ModelDropDownString> kecamatanList = new E_Select(this).getLst_Kecamatan_PP(kecamatanCode);
        ArrayAdapter<ModelDropDownString> Adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, kecamatanList);
        ac_kecamatan.setAdapter(Adapter);
    }

    private void setAdapterKelurahan(String kelurahanCode) {
        ArrayList<ModelDropDownString> kelurahanList = new E_Select(this).getLst_Kelurahan_PP(kelurahanCode);
        ArrayAdapter<ModelDropDownString> Adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, kelurahanList);
        ac_kelurahan.setAdapter(Adapter);
    }

    private void setAdapterKodepos(String getPostalCodeFromDistrict) {
        ArrayList<ModelDropDownString> kodePostList = new E_Select(this).getLst_Kelurahan_PP(getPostalCodeFromDistrict);
        ArrayAdapter<ModelDropDownString> Adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, kodePostList);
        ac_kelurahan.setAdapter(Adapter);
    }

    private boolean Validation() {
        boolean val = true;
        int color = ContextCompat.getColor(this, R.color.red);

        int alamatLengkap = et_alamat.getText().toString().length();
        int provinsiLength = ac_provinsi.getText().toString().length();
        int kabupatenLength = ac_kabupaten.getText().toString().length();
        int kecamatanLength = ac_kecamatan.getText().toString().length();
        int kelurahanLength = ac_kelurahan.getText().toString().length();
        int totalLength = alamatLengkap + provinsiLength + kabupatenLength + kecamatanLength + kelurahanLength;
        if (totalLength > 200) {
            Toast.makeText(this, "Kombinasi alamat lengkap (alamat, provinsi, kabupaten, kecamatan, kelurahan) tidak boleh melebihi 200 karakter", Toast.LENGTH_LONG).show();
            val = false;
        }

        if (cl_form_info_umum.getVisibility() == View.VISIBLE) {
            if (et_alamat.getText().toString().trim().length() <= 3) {
                val = false;
                MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_alamat, tv_error_alamat, iv_circle_alamat, color, this, getString(R.string.error_field_required));

            } else {
                if (!Method_Validator.ValEditRegex(et_alamat.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_alamat, tv_error_alamat, iv_circle_alamat, color, this, getString(R.string.error_regex));
                }
            }

            if (ac_provinsi.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, ac_provinsi, tv_error_provinsi, iv_circle_provinsi, color, this, getString(R.string.error_field_required));

            } else {
                if (!Method_Validator.ValEditRegex(ac_provinsi.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, ac_provinsi, tv_error_provinsi, iv_circle_provinsi, color, this, getString(R.string.error_regex));
                }
            }

/*                if (ac_kabupaten.getText().toString().trim().length() == 0) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, ac_kabupaten, tv_error_kabupaten, iv_circle_kabupaten, color, this, getString(R.string.error_field_required));
//                tv_error_alamat.setText(R.string.field_requirement);
//                tv_error_alamat.setVisibility(View.VISIBLE);
//                iv_circle_alamat.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
                } else {
                    if (!Method_Validator.ValEditRegex(ac_kabupaten.getText().toString())) {
                        val = false;
                        MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, ac_kabupaten, tv_error_kabupaten, iv_circle_kabupaten, color, this, getString(R.string.error_regex));
                    }
                }*/
/*                if (ac_kecamatan.getText().toString().trim().length() == 0) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, ac_kecamatan, tv_error_kecamatan, iv_circle_kecamatan, color, this, getString(R.string.error_field_required));
//                tv_error_alamat.setText(R.string.field_requirement);
//                tv_error_alamat.setVisibility(View.VISIBLE);
//                iv_circle_alamat.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
                } else {
                    if (!Method_Validator.ValEditRegex(et_alamat.getText().toString())) {
                        val = false;
                        MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, ac_kecamatan, tv_error_kecamatan, iv_circle_kecamatan, color, this, getString(R.string.error_regex));
                    }
                }
                if (ac_kelurahan.getText().toString().trim().length() == 0) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, ac_kelurahan, tv_error_kelurahan, iv_circle_kelurahan, color, this, getString(R.string.error_field_required));
//                tv_error_alamat.setText(R.string.field_requirement);
//                tv_error_alamat.setVisibility(View.VISIBLE);
//                iv_circle_alamat.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
                } else {
                    if (!Method_Validator.ValEditRegex(et_alamat.getText().toString())) {
                        val = false;
                        MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, ac_kelurahan, tv_error_kelurahan, iv_circle_kelurahan, color, this, getString(R.string.error_regex));
                    }
                }*/
        } else if (cl_form_info_hp.getVisibility() == View.VISIBLE) {
            if (et_hp1.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_hp1, tv_error_hp1, iv_circle_hp1, color, this, getString(R.string.error_field_required));
//                tv_error_hp1.setText(R.string.field_requirement);
//                tv_error_hp1.setVisibility(View.VISIBLE);
//                iv_circle_hp1.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
            } else {
                if (!Method_Validator.ValEditRegex(et_hp1.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_hp1, tv_error_hp1, iv_circle_hp1, color, this, getString(R.string.error_regex));
                }
            }
            if (et_email.getText().toString().trim().length() != 0) {
                if (Method_Validator.emailValidator(et_email.getText().toString()) == false) {
                    val = false;
                    MethodSupport.onFocusview(scroll_alamat, cl_child_alamat, et_email, tv_error_email, iv_circle_email, color, this, getString(R.string.error_field_required));
//                    tv_error_email.setText(R.string.false_format);
//                    tv_error_email.setVisibility(View.VISIBLE);
//                    iv_circle_email.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
                }
            }
        }

        return val;
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId()) {
            case R.id.ac_provinsi:
                if (hasWindowFocus()) ac_provinsi.showDropDown();
                break;
            case R.id.ac_kabupaten:
                if (hasWindowFocus()) ac_kabupaten.showDropDown();
                break;
            case R.id.ac_kecamatan:
                if (hasWindowFocus()) ac_kecamatan.showDropDown();
                break;
            case R.id.ac_kelurahan:
                if (hasWindowFocus()) ac_kelurahan.showDropDown();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        switch (view.getId()) {
            case R.id.ac_provinsi:
                ModelDropDownString modelProvinsi = (ModelDropDownString) adapterView.getAdapter().getItem(i);
                propinsi = modelProvinsi.getId();
                SetAdapterKabupaten(propinsi);
                ac_kabupaten.setText(null);
                ac_kecamatan.setText(null);
                ac_kelurahan.setText(null);
                etPostalCode.setError(null);
                etPostalCode.setText(null);
                break;

            case R.id.ac_kabupaten:
                ModelDropDownString modelKabupaten = (ModelDropDownString) adapterView.getAdapter().getItem(i);
                kabupaten = modelKabupaten.getId();
                setAdapterKecamatan(kabupaten);
                ac_kecamatan.setText(null);
                ac_kelurahan.setText(null);
                etPostalCode.setError(null);
                etPostalCode.setText(null);
                break;

            case R.id.ac_kecamatan:
                ModelDropDownString modelKecamatan = (ModelDropDownString) adapterView.getAdapter().getItem(i);
                kecamatan = modelKecamatan.getId();
                setAdapterKelurahan(kecamatan);
                ac_kelurahan.setText(null);
                etPostalCode.setError(null);
                etPostalCode.setText(null);
                break;

            case R.id.ac_kelurahan:
                ModelDropDownString modelKelurahan = (ModelDropDownString) adapterView.getAdapter().getItem(i);
                kelurahan = modelKelurahan.getId();
                kode_pos = new E_Select(this).getPostalCodeFromDistrict(kelurahan);
                if (!TextUtils.isEmpty(kode_pos)) {
                    etPostalCode.setText(kode_pos);
                    etPostalCode.setEnabled(false);
                    etPostalCode.setFocusable(false);
                    etPostalCode.setFocusableInTouchMode(false);
                } else {
                    //Empty or null
                    etPostalCode.setError(getResources().getString(R.string.error_msg_postal_code));
                    etPostalCode.setEnabled(true);
                    etPostalCode.setFocusable(true);
                    etPostalCode.setFocusableInTouchMode(true);
                }
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {

    }

    public class MTextWatcher implements TextWatcher {
        private View view;

        MTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }


        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                case R.id.et_alamat:
                    if (et_alamat.getText().toString().trim().length() <= 3) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_alamat.setColorFilter(color);
                    tv_error_alamat.setVisibility(View.GONE);
                case R.id.ac_provinsi:
                    if (StaticMethods.isTextWidgetEmpty(ac_provinsi)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_provinsi.setColorFilter(color);
                    tv_error_provinsi.setVisibility(View.GONE);
                    break;
                case R.id.ac_kabupaten:
                    if (StaticMethods.isTextWidgetEmpty(ac_kabupaten)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_kabupaten.setColorFilter(color);
                    tv_error_kabupaten.setVisibility(View.GONE);
                    break;
                case R.id.ac_kecamatan:
                    if (StaticMethods.isTextWidgetEmpty(ac_kecamatan)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_kecamatan.setColorFilter(color);
                    tv_error_kecamatan.setVisibility(View.GONE);
                    break;
                case R.id.ac_kelurahan:
                    if (StaticMethods.isTextWidgetEmpty(ac_kelurahan)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_kelurahan.setColorFilter(color);
                    tv_error_kelurahan.setVisibility(View.GONE);
                    break;
                case R.id.et_hp1:
                    if (StaticMethods.isTextWidgetEmpty(et_hp1)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_hp1.setColorFilter(color);
                    tv_error_hp1.setVisibility(View.GONE);
                    break;
                case R.id.et_email:
                    if (Method_Validator.emailValidator(et_email.getText().toString()) == false) {
                        et_email.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.red));
                    } else {
                        et_email.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.dua));
                        iv_circle_email.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.green));
                    }
                    break;
            }
        }
    }


    @BindView(R.id.et_alamat)
    EditText et_alamat;
    @BindView(R.id.ac_provinsi)
    AutoCompleteTextView ac_provinsi;
    @BindView(R.id.ac_kabupaten)
    AutoCompleteTextView ac_kabupaten;
    @BindView(R.id.ac_kecamatan)
    AutoCompleteTextView ac_kecamatan;
    @BindView(R.id.ac_kelurahan)
    AutoCompleteTextView ac_kelurahan;
    @BindView(R.id.etPostalCode)
    TextInputEditText etPostalCode;
    @BindView(R.id.et_kdtelp1)
    EditText et_kdtelp1;
    @BindView(R.id.et_telp1)
    EditText et_telp1;
    @BindView(R.id.et_kdtelp2)
    EditText et_kdtelp2;
    @BindView(R.id.et_telp2)
    EditText et_telp2;
    @BindView(R.id.et_kdfax)
    EditText et_kdfax;
    @BindView(R.id.et_fax)
    EditText et_fax;

    @BindView(R.id.btn_cancel)
    Button btn_cancel;
    @BindView(R.id.btn_lanjut)
    Button btn_lanjut;

    @BindView(R.id.iv_circle_alamat)
    ImageView iv_circle_alamat;
    @BindView(R.id.iv_circle_provinsi)
    ImageView iv_circle_provinsi;
    @BindView(R.id.iv_circle_kabupaten)
    ImageView iv_circle_kabupaten;
    @BindView(R.id.iv_circle_kecamatan)
    ImageView iv_circle_kecamatan;
    @BindView(R.id.iv_circle_kelurahan)
    ImageView iv_circle_kelurahan;
    @BindView(R.id.tv_error_alamat)
    TextView tv_error_alamat;
    @BindView(R.id.tv_error_provinsi)
    TextView tv_error_provinsi;
    @BindView(R.id.tv_error_kabupaten)
    TextView tv_error_kabupaten;
    @BindView(R.id.tv_error_kecamatan)
    TextView tv_error_kecamatan;
    @BindView(R.id.tv_error_kelurahan)
    TextView tv_error_kelurahan;
    @BindView(R.id.tv_error_kode_pos)
    TextView tv_error_kode_pos;

    @BindView(R.id.tvNotice)
    TextView tv_notice_alamat;

    @BindView(R.id.cl_fax)
    RelativeLayout cl_fax;
    @BindView(R.id.cl_kdfax)
    RelativeLayout cl_kdfax;
    @BindView(R.id.cl_form_info_umum)
    RelativeLayout cl_form_info_umum;
    @BindView(R.id.cl_form_info_hp)
    RelativeLayout cl_form_info_hp;

    @BindView(R.id.et_hp1)
    EditText et_hp1;
    @BindView(R.id.et_hp2)
    EditText et_hp2;
    @BindView(R.id.et_email)
    EditText et_email;

    @BindView(R.id.iv_circle_hp1)
    ImageView iv_circle_hp1;
    @BindView(R.id.iv_circle_email)
    ImageView iv_circle_email;

    @BindView(R.id.tv_error_hp1)
    TextView tv_error_hp1;
    @BindView(R.id.tv_error_email)
    TextView tv_error_email;

    @BindView(R.id.scroll_alamat)
    ScrollView scroll_alamat;
    @BindView(R.id.cl_child_alamat)
    RelativeLayout cl_child_alamat;

    private EspajModel me;

    private ArrayList<ModelDropDownString> ListDropDownProvinsi;
    private ArrayList<ModelDropDownString> ListDropDownKabupaten;
    private ArrayList<ModelDropDownString> ListDropDownKecamatan;
    private ArrayList<ModelDropDownString> ListDropDownKelurahan;
    private ArrayList<ModelDropDownString> ListDropDownKodepos;

    private int flag_input = 0;


    private String kabupaten = "";
    private String kecamatan = "";
    private String kelurahan = "";
    private String propinsi = "";
    private String kode_pos = "";
    private int lspr_id = 0;
    private int lska_id;
    private int lskc_id;
    private int lskl_id;
    private Intent intent;

}
