package id.co.ajsmsig.nb.crm.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.activity.AgencySearchLeadActivity;
import id.co.ajsmsig.nb.crm.activity.C_MainActivity;
import id.co.ajsmsig.nb.crm.adapter.AgencyLeadAdapter;
import id.co.ajsmsig.nb.crm.database.C_Delete;
import id.co.ajsmsig.nb.crm.database.C_Insert;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.database.C_Update;
import id.co.ajsmsig.nb.crm.fragment.listlead.AgencyLeadChildList;
import id.co.ajsmsig.nb.crm.fragment.listlead.AgencyLeadModel;
import id.co.ajsmsig.nb.crm.fragment.listlead.AgencyLeadParentList;
import id.co.ajsmsig.nb.crm.model.hardcode.RequestCode;
import id.co.ajsmsig.nb.pointofsale.POSActivity;
import id.co.ajsmsig.nb.pointofsale.utils.Constants;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.DateUtils;


/**
 * A simple {@link Fragment} subclass.
 */
public class AgencyLeadFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,
        AgencyLeadAdapter.ClickListener,
        DialogInterface.OnClickListener {

    @BindView(R.id.rv_agency_lead)
    RecyclerView recyclerView;


    @BindView(R.id.rootLayoutLead)
    ConstraintLayout rootLayoutLead;

    @BindView(R.id.layoutLoading)
    RelativeLayout layoutLoading;

    @BindView(R.id.layoutNoLeadFound)
    LinearLayout layoutNoLeadFound;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.tvLoadingMessage)
    TextView tvLoadingMessage;

    private AlertDialog dialogAddLead;
    private AlertDialog dialogUpdateLead;
    private TextInputLayout inputLayoutLeadName;
    private TextInputLayout inputLayoutLeadAge;
    private TextInputEditText etLeadName;
    private TextInputEditText etLeadAge;
    private RadioGroup radioGroup;

    private Cursor cursor;
    private C_Insert insert;
    private C_Update update;
    private C_Select select;
    private C_Delete delete;

    private String agentCode;

    private final int FEMALE = 0;
    private final int MALE = 1;
    private int selectedLeadSortMode;

    private AgencyLeadAdapter mAdapter;

    private List<AgencyLeadParentList> parents;
    private List<AgencyLeadChildList> leads;
    private List<AgencyLeadChildList> favoriteLeads;

    private final int LOADER_SORT_LEAD_BY_DATE = 100;
    private final int LOADER_SORT_LEAD_BY_NAME = 200;
    private final int SORT_BY_DATE = 0; //Default
    private final int SORT_BY_NAME = 1;
    private boolean shouldUnlockPOSMenu;

    public AgencyLeadFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_agency_lead, container, false);
        ButterKnife.bind(this, view);


        insert = new C_Insert(getActivity());
        select = new C_Select(getActivity());
        update = new C_Update(getActivity());
        delete = new C_Delete(getActivity());

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), Context.MODE_PRIVATE);
        agentCode = sharedPreferences.getString("KODE_AGEN", "");

        initRecyclerView();
        getLoaderManager().initLoader(LOADER_SORT_LEAD_BY_DATE, null, this);

        if (!TextUtils.isEmpty(agentCode)) {
            List<String> menuList = new C_Select(getActivity()).getUserMenuForUser(agentCode);
            shouldUnlockPOSMenu = isContainsPOSAccess(menuList);
        }


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        restartLoader();
    }

    @OnClick(R.id.fab)
    void onButtonAddNasabahPressed() {
        displayCreateLeadDialog();
    }

    private void displayCreateLeadDialog() {
        View view = getLayoutInflater().inflate(R.layout.dialog_add_lead, null);
        inputLayoutLeadName = view.findViewById(R.id.inputLayoutLeadName);
        inputLayoutLeadAge = view.findViewById(R.id.inputLayoutLeadAge);
        etLeadName = view.findViewById(R.id.etLeadName);
        etLeadAge = view.findViewById(R.id.etLeadAge);
        radioGroup = view.findViewById(R.id.radioGroup);


        etLeadName.addTextChangedListener(new TextWatcherListener(etLeadName));
        etLeadAge.addTextChangedListener(new TextWatcherListener(etLeadAge));

        Button btnAddLead = view.findViewById(R.id.btnAddLead);
        Button btnCancel = view.findViewById(R.id.btnCancel);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Tambah Nasabah");
        builder.setView(view);

        btnAddLead.setOnClickListener(v -> {
            if (validateLeadName() && validateLeadAge()) {

                long id = DateUtils.generateTimeStamp();
                String leadName = etLeadName.getText().toString().trim();
                int leadAge = Integer.parseInt(etLeadAge.getText().toString());

                int selectedRadioGroupId = radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = view.findViewById(selectedRadioGroupId);
                String selectedGender = radioButton.getText().toString();
                int genderFlag = selectedGender.equalsIgnoreCase("Pria") ? MALE : FEMALE;

                String createdDate = DateUtils.getCurrentDateInString();

                if (insert != null) {
                    insert.insertToLstAgencyLead(id, leadName, leadAge, genderFlag, 0, agentCode, createdDate, createdDate);
                }

                if (dialogAddLead != null) {
                    dialogAddLead.dismiss();
                }

                restartLoader();

                hideLayoutLeadNotFound();
                Toast.makeText(getActivity(), "Data nasabah berhasil ditambahkan", Toast.LENGTH_LONG).show();
            }
        });
        btnCancel.setOnClickListener(v -> {
            if (dialogAddLead != null) {
                dialogAddLead.dismiss();
            }
        });

        dialogAddLead = builder.create();
        dialogAddLead.show();
    }

    private void displayUpdateLeadDialog(String leadId, String agentCode) {
        View view = getLayoutInflater().inflate(R.layout.dialog_add_lead, null);
        inputLayoutLeadName = view.findViewById(R.id.inputLayoutLeadName);
        inputLayoutLeadAge = view.findViewById(R.id.inputLayoutLeadAge);
        etLeadName = view.findViewById(R.id.etLeadName);
        etLeadAge = view.findViewById(R.id.etLeadAge);
        radioGroup = view.findViewById(R.id.radioGroup);


        AgencyLeadModel model = select.getAgencyLeadInfoOf(leadId, agentCode);
        if (model != null) {
            String savedLeadName = model.getLeadName();
            int savedLeadAge = model.getLeadAge();
            int savedLeadGender = model.getLeadGender();

            etLeadName.setText(savedLeadName);
            etLeadName.setSelection(savedLeadName.length());
            etLeadAge.setText(String.valueOf(savedLeadAge));

            if (savedLeadGender == MALE) {
                ((RadioButton)radioGroup.getChildAt(0)).setChecked(true);
            } else {
                ((RadioButton)radioGroup.getChildAt(1)).setChecked(true);
            }

        }


        etLeadName.addTextChangedListener(new TextWatcherListener(etLeadName));
        etLeadAge.addTextChangedListener(new TextWatcherListener(etLeadAge));

        Button btnAddLead = view.findViewById(R.id.btnAddLead);
        btnAddLead.setText(R.string.text_update);
        Button btnCancel = view.findViewById(R.id.btnCancel);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Edit Nasabah");
        builder.setView(view);

        btnAddLead.setOnClickListener(v -> {
            if (validateLeadName() && validateLeadAge()) {

                String leadName = etLeadName.getText().toString().trim();
                int leadAge = Integer.parseInt(etLeadAge.getText().toString());

                int selectedRadioGroupId = radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = view.findViewById(selectedRadioGroupId);
                String selectedGender = radioButton.getText().toString();
                int genderFlag = selectedGender.equalsIgnoreCase("Pria") ? MALE : FEMALE;

                String updateDate = DateUtils.getCurrentDateInString();

                if (update != null && model != null) {
                    update.updateAgencyLead(String.valueOf(leadId), leadName, leadAge, genderFlag, updateDate);

                    if (dialogUpdateLead != null) {
                        dialogUpdateLead.dismiss();
                    }

                    restartLoader();
                    Toast.makeText(getActivity(), "Data nasabah berhasil diperbaharui", Toast.LENGTH_LONG).show();
                }

            }
        });
        btnCancel.setOnClickListener(v -> {
            if (dialogUpdateLead != null) {
                dialogUpdateLead.dismiss();
            }
        });

        dialogUpdateLead = builder.create();
        dialogUpdateLead.show();
    }

    private void initRecyclerView() {
        parents = new ArrayList<>();
        leads = new ArrayList<>();
        favoriteLeads = new ArrayList<>();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        recyclerView.setVisibility(View.GONE);
        showLoadingIndicator("Mengurutkan data lead.\nMohon tunggu");
        fab.setVisibility(View.GONE);

        CursorLoader cursorLoader;

        switch (id) {
            case LOADER_SORT_LEAD_BY_DATE:
                cursor = select.getAllSavedAgencyLead(agentCode, "LEAD_FAVORITE DESC, CREATED_DATE DESC"); //Default sort is by date
                break;
            case LOADER_SORT_LEAD_BY_NAME:
                cursor = select.getAllSavedAgencyLead(agentCode, "LEAD_FAVORITE DESC, LEAD_NAME COLLATE NOCASE ASC");
                break;
        }


        cursorLoader = new CursorLoader(getContext()) {
            @Override
            public Cursor loadInBackground() {
                return cursor;
            }
        };

        return cursorLoader;

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        recyclerView.setVisibility(View.VISIBLE);
        hideLoadingIndicator();
        fab.setVisibility(View.VISIBLE);


        int id = loader.getId();

        switch (id) {
            case LOADER_SORT_LEAD_BY_DATE:
            case LOADER_SORT_LEAD_BY_NAME:
                clearPreviousLeadResult();

                if (cursor != null && cursor.getCount() > 0) {

                    String firstNameInitial = null;
                    String lastNameInitial = null;

                    while (cursor.moveToNext()) {
                        long leadId = cursor.getLong(cursor.getColumnIndexOrThrow("ID"));
                        String leadName = cursor.getString(cursor.getColumnIndexOrThrow("LEAD_NAME"));
                        int leadAge = cursor.getInt(cursor.getColumnIndexOrThrow("LEAD_AGE"));
                        int leadGender = cursor.getInt(cursor.getColumnIndexOrThrow("LEAD_GENDER"));
                        int leadFavorite = cursor.getInt(cursor.getColumnIndexOrThrow("LEAD_FAVORITE"));
                        String createdDate = cursor.getString(cursor.getColumnIndexOrThrow("CREATED_DATE"));
                        String updatedDate = cursor.getString(cursor.getColumnIndexOrThrow("UPDATE_DATE"));

                        if (!TextUtils.isEmpty(leadName)) {
                            String[] splitByWhiteSpace = leadName.split("\\s+");

                            if (splitByWhiteSpace != null && splitByWhiteSpace.length > 0) {

                                if (splitByWhiteSpace.length == 3) {
                                    //Name contains first, middle, last name
                                    if (!TextUtils.isEmpty(splitByWhiteSpace[0]) && !TextUtils.isEmpty(splitByWhiteSpace[2])) {
                                        firstNameInitial = splitByWhiteSpace[0];
                                        firstNameInitial = String.valueOf(firstNameInitial.charAt(0)).toUpperCase();

                                        lastNameInitial = splitByWhiteSpace[2];
                                        lastNameInitial = String.valueOf(lastNameInitial.charAt(0)).toUpperCase();
                                    }


                                } else if (splitByWhiteSpace.length == 2) {

                                    if (!TextUtils.isEmpty(splitByWhiteSpace[0]) && !TextUtils.isEmpty(splitByWhiteSpace[1])) {
                                        //Name contains first, middle name only
                                        firstNameInitial = splitByWhiteSpace[0];
                                        firstNameInitial = String.valueOf(firstNameInitial.charAt(0)).toUpperCase();

                                        lastNameInitial = splitByWhiteSpace[1];
                                        lastNameInitial = String.valueOf(lastNameInitial.charAt(0)).toUpperCase();
                                    }


                                } else if (splitByWhiteSpace.length == 1) {

                                    if (!TextUtils.isEmpty(splitByWhiteSpace[0])) {
                                        //Name contains first name only
                                        firstNameInitial = splitByWhiteSpace[0];
                                        firstNameInitial = String.valueOf(firstNameInitial.charAt(0)).toUpperCase();
                                        lastNameInitial = "";
                                    }

                                }
                            }

                        }


                        boolean isFavoriteLead = isOnFavoriteLead(leadFavorite);

                        if (isFavoriteLead) {
                            favoriteLeads.add(new AgencyLeadChildList(leadId, leadName, leadAge, leadGender, true, createdDate, updatedDate, firstNameInitial, lastNameInitial));
                        } else {
                            leads.add(new AgencyLeadChildList(leadId, leadName, leadAge, leadGender, false, createdDate, updatedDate, firstNameInitial, lastNameInitial));
                        }

                    }

                    int favoriteLeadCount = favoriteLeads.size();
                    int leadCount = leads.size();

                    //Set group and its child
                    parents.add(new AgencyLeadParentList("Lead Favorit - " + favoriteLeadCount, favoriteLeads));
                    parents.add(new AgencyLeadParentList("Lead - " + leadCount, leads));

                    displayLeadsData(parents);

                    hideLayoutLeadNotFound();
                } else if (cursor != null && cursor.getCount() == 0) {
                    showLayoutLeadNotFound();
                }
                break;

        }

    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }

    @Override
    public void onLeadSelected(int position, View view, AgencyLeadChildList model) {

        long id = model.getId();
        String leadName = model.getLeadName();
        int leadGender = model.getLeadGender();
        boolean isUserAFavoriteLead = model.isLeadFavorite();

        switch (view.getId()) {
            case R.id.imgFavoriteLead:

                //Make someone add or removed from a favorite lead
                if (!isUserAFavoriteLead) {
                    update.markAsFavoriteLead(String.valueOf(id), false);
                    Snackbar.make(rootLayoutLead, leadName.concat(" dihapus dari daftar favorit"), Snackbar.LENGTH_SHORT).show();
                } else {
                    update.markAsFavoriteLead(String.valueOf(id), true);
                    Snackbar.make(rootLayoutLead, leadName.concat(" ditambahkan ke daftar favorit"), Snackbar.LENGTH_SHORT).show();
                }


                //Make a delete animation, so newly selected leads moved upward to favorite lead
                if (leads != null && mAdapter != null) {
                    mAdapter.notifyItemRemoved(position);
                }

                restartLoader();

                break;
            case R.id.tvPresentToClient:
                if (shouldUnlockPOSMenu) {
                    launchPointOfSales(id, leadName, leadGender, Const.GROUP_ID_AGENCY);
                } else {
                    Toast.makeText(getActivity(), "Fitur Point of Sales belum tersedia untuk saat ini", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tvEditClient:
                displayUpdateLeadDialog(String.valueOf(id), agentCode);
                break;
            case R.id.tvRemoveClient:
                new AlertDialog.Builder(getActivity())
                        .setTitle("Hapus nasabah "+leadName+"?")
                        .setMessage("Apakah Anda yakin untuk menghapus " +leadName+ " dari list nasabah?")
                        .setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss())
                        .setPositiveButton("Hapus", (dialog, which) -> {

                            deleteLead(position, id);
                            restartLoader();

                            if (!isLeadExist()) {
                                //There are no lead left
                                showLayoutLeadNotFound();
                            }

                            String message = "Nasabah "+leadName+" berhasil dihapus";
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

                            dialog.dismiss();
                        })
                        .show();

                break;


        }

    }

    private void launchPointOfSales(long id, String leadName, int leadGender, int groupId) {
        Intent intent = new Intent(getActivity(), POSActivity.class);
        intent.putExtra(Constants.Companion.getSL_TAB_ID(), id);
        intent.putExtra(Constants.Companion.getSL_NAME(), leadName);
        intent.putExtra(Constants.Companion.getSL_GENDER(), leadGender);
        intent.putExtra(Constants.Companion.getGROUP_ID(), groupId);
        startActivityForResult(intent, RequestCode.REQ_POS);
    }

    private void displayLeadsData(final List<AgencyLeadParentList> parents) {
        mAdapter = new AgencyLeadAdapter(parents);
        mAdapter.setClickListener(this);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        //Expand lead group by default
        mAdapter.onGroupClick(1);

    }

    /**
     * Remove previous lead result when user changed sort mode
     */
    private void clearPreviousLeadResult() {
        if (parents != null && parents.size() > 0) {

            parents.clear();

            if (leads != null) leads.clear();
            if (favoriteLeads != null) favoriteLeads.clear();
            if (mAdapter != null) mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Determine if a costumer is on a favorite lead or not
     * 0 = NOT a favorite lead
     * 1 = YES a favorite lead
     *
     * @param costumerFavoriteLeadFlag
     * @return
     */
    private boolean isOnFavoriteLead(int costumerFavoriteLeadFlag) {
        return costumerFavoriteLeadFlag == 1;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_agency_lead, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                launchSearchLeadActivity();
                break;
            case R.id.action_sort:
                displaySortOptionsDialog();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Display sort options dialog. (Sort by date or sort by name)
     */
    private void displaySortOptionsDialog() {
        final String[] sortOptions = new String[]{"Data terbaru", "Nama"};
        new android.app.AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_title_sort_by)
                .setSingleChoiceItems(sortOptions, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        selectedLeadSortMode = which;
                    }
                })
                .setPositiveButton("Pilih", this)
                .show();
    }

    /**
     * Dialog single choice click listener
     *
     * @param dialogInterface
     * @param i
     */
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (selectedLeadSortMode) {
            case SORT_BY_DATE:
                getLoaderManager().restartLoader(LOADER_SORT_LEAD_BY_DATE, null, this);
                dialogInterface.dismiss();
                break;

            case SORT_BY_NAME:
                getLoaderManager().restartLoader(LOADER_SORT_LEAD_BY_NAME, null, this);
                dialogInterface.dismiss();

                //Reset to default sort mode, so when the user reselect the sort options it points to the sort by date
                selectedLeadSortMode = SORT_BY_DATE;

                break;

            default:
                throw new IllegalArgumentException("Undefined leads sort options");
        }

    }

    private class TextWatcherListener implements TextWatcher {

        private View view;

        private TextWatcherListener(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etLeadName:
                    validateLeadName();
                    break;
                case R.id.etLeadAge:
                    validateLeadAge();
                    break;
            }
        }
    }

    private boolean validateLeadName() {
        if (etLeadName.getText().toString().trim().isEmpty()) {
            inputLayoutLeadName.setError(getString(R.string.err_msg_field_cannot_be_empty));
            requestFocus(etLeadName);
            return false;
        } else {
            inputLayoutLeadName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateLeadAge() {
        if (etLeadAge.getText().toString().trim().isEmpty()) {
            inputLayoutLeadAge.setError(getString(R.string.err_msg_field_cannot_be_empty));
            requestFocus(etLeadAge);
            return false;
        } else {
            inputLayoutLeadAge.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            if (getActivity().getWindow() != null) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        }
    }


    private void showLoadingIndicator(String loadingMessage) {
        layoutLoading.setVisibility(View.VISIBLE);
        tvLoadingMessage.setText(loadingMessage);
    }

    private void hideLoadingIndicator() {
        layoutLoading.setVisibility(View.GONE);
        tvLoadingMessage.setText(null);
    }

    private void showLayoutLeadNotFound() {
        layoutNoLeadFound.setVisibility(View.VISIBLE);
    }

    private void hideLayoutLeadNotFound() {
        layoutNoLeadFound.setVisibility(View.GONE);
    }

    private boolean isLeadExist() {
        if (select != null) {
            int leadCount = select.getAgencyLeadCount(agentCode);

            return leadCount > 0;
        }

        return false;
    }

    private void deleteLead(int position, long id) {
        //Make a delete animation, so newly selected leads moved upward to favorite lead
        if (leads != null && mAdapter != null && delete != null) {
            mAdapter.notifyItemRemoved(position);
            delete.deleteAgencyLead(String.valueOf(id));
        }

    }

    private void restartLoader() {
        getLoaderManager().restartLoader(LOADER_SORT_LEAD_BY_DATE, null, this);
    }

    /**
     * Launch search lead activity to search lead by name
     */
    private void launchSearchLeadActivity() {
        Intent intent = new Intent(getActivity(), AgencySearchLeadActivity.class);
        intent.putExtra(Const.INTENT_KEY_AGENT_CODE, agentCode);
        startActivity(intent);
    }

    private boolean isContainsPOSAccess(List<String> menuList) {
        if (menuList != null && !menuList.isEmpty()) {

            return menuList.contains("2148");
        }
        return false;
    }

}
