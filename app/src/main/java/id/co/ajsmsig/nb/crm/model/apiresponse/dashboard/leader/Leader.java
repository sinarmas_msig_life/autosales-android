
package id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.leader;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.leader.LeaderClosing;
import id.co.ajsmsig.nb.crm.model.apiresponse.dashboard.leader.LeaderMeetandpre;

public class Leader {

    @SerializedName("meetandpre")
    @Expose
    private LeaderMeetandpre meetandpre;
    @SerializedName("closing")
    @Expose
    private LeaderClosing closing;

    public LeaderMeetandpre getMeetandpre() {
        return meetandpre;
    }

    public void setMeetandpre(LeaderMeetandpre meetandpre) {
        this.meetandpre = meetandpre;
    }

    public LeaderClosing getClosing() {
        return closing;
    }

    public void setClosing(LeaderClosing closing) {
        this.closing = closing;
    }
}
