package id.co.ajsmsig.nb.util;

public class PaymentEvent {
    private boolean shouldRefreshSelectedPaymentMethod;
    private String paymentMethodName;
    private int paymentMethodId;
    private boolean shouldOnlyExecutedByPemegangPolisFragment;

    public PaymentEvent(int paymentMethodId, boolean shouldRefreshSelectedPaymentMethod, String paymentMethodName, boolean shouldOnlyExecutedByPemegangPolisFragment) {
        this.shouldRefreshSelectedPaymentMethod = shouldRefreshSelectedPaymentMethod;
        this.paymentMethodName = paymentMethodName;
        this.paymentMethodId = paymentMethodId;
        this.shouldOnlyExecutedByPemegangPolisFragment = shouldOnlyExecutedByPemegangPolisFragment;
    }

    public boolean isShouldRefreshSelectedPaymentMethod() {
        return shouldRefreshSelectedPaymentMethod;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public int getPaymentMethodId() {
        return paymentMethodId;
    }

    public boolean isShouldOnlyExecutedByPemegangPolisFragment() {
        return shouldOnlyExecutedByPemegangPolisFragment;
    }


}
