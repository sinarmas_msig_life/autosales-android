
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BcBranch {

    @SerializedName("lrb_id")
    @Expose
    private String lrbId;
    @SerializedName("msag_id")
    @Expose
    private String msagId;
    @SerializedName("agent_name")
    @Expose
    private String agentName;
    @SerializedName("ajs_cbng")
    @Expose
    private String ajsCbng;
    @SerializedName("simas_cbng")
    @Expose
    private String simasCbng;
    @SerializedName("cabang")
    @Expose
    private String cabang;
    @SerializedName("wilayah")
    @Expose
    private String wilayah;

    public String getLrbId() {
        return lrbId;
    }

    public void setLrbId(String lrbId) {
        this.lrbId = lrbId;
    }

    public String getMsagId() {
        return msagId;
    }

    public void setMsagId(String msagId) {
        this.msagId = msagId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAjsCbng() {
        return ajsCbng;
    }

    public void setAjsCbng(String ajsCbng) {
        this.ajsCbng = ajsCbng;
    }

    public String getSimasCbng() {
        return simasCbng;
    }

    public void setSimasCbng(String simasCbng) {
        this.simasCbng = simasCbng;
    }

    public String getCabang() {
        return cabang;
    }

    public void setCabang(String cabang) {
        this.cabang = cabang;
    }

    public String getWilayah() {
        return wilayah;
    }

    public void setWilayah(String wilayah) {
        this.wilayah = wilayah;
    }

}
