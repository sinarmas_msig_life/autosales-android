package id.co.ajsmsig.nb.pointofsale.ui.griskprofiletools


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosFragmentRiskProfileToolsBinding
import id.co.ajsmsig.nb.pointofsale.ui.fragment.BaseFragment

/**
 * A simple [Fragment] subclass.
 */
class RiskProfileToolsFragment : BaseFragment() {

    private lateinit var dataBinding: PosFragmentRiskProfileToolsBinding

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(RiskProfileToolsVM::class.java)
        subscribeUi(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_risk_profile_tools, container, false)

        return dataBinding.root
    }

    fun subscribeUi(viewModel: RiskProfileToolsVM) {
        viewModel.page = page

        dataBinding.vm = viewModel

        mainVM.hideSubtitle()
    }
}// Required empty public constructor
