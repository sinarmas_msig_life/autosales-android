
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataVersionResponse {

    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("list_version")
    @Expose
    private List<ListVersion> listVersion = null;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<ListVersion> getListVersion() {
        return listVersion;
    }

    public void setListVersion(List<ListVersion> listVersion) {
        this.listVersion = listVersion;
    }

}
