package id.co.ajsmsig.nb.espaj.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.AutoCompleteTextViewClickListener;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.method.F_Hit_Umur;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAddRiderUA;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;
import id.co.ajsmsig.nb.prop.model.DropboxModel;

import static id.co.ajsmsig.nb.crm.method.StaticMethods.toCalendar;

/**
 * Created by Bernard on 12/07/2017.
 */

public class E_FormAsuransiTambahanActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, RadioGroup.OnCheckedChangeListener, View.OnFocusChangeListener {
    private boolean isJobDescriptionValid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.e_activity_form_asuransi_tambahan_layout);
        ActionBar toolbar = getSupportActionBar();
        toolbar.setDisplayHomeAsUpEnabled(true);
        toolbar.setDisplayShowHomeEnabled(true);
        toolbar.setTitle("Form Asuransi Tambahan");
        ButterKnife.bind(this);
        Intent intent = getIntent();
        pos = intent.getIntExtra(getString(R.string.pos), 0);
        modelRider = intent.getParcelableExtra(getString(R.string.modelrider));
        me = intent.getParcelableExtra(getString(R.string.modelespaj));

        et_nama.addTextChangedListener(new MTextWatcher(et_nama));
        et_bb.addTextChangedListener(new MTextWatcher(et_bb));
        et_tgl_lahir.addTextChangedListener(new MTextWatcher(et_tgl_lahir));
        et_tgl_lahir.setOnClickListener(this);
        et_tgl_lahir.setOnFocusChangeListener(this);
        et_usia.addTextChangedListener(new MTextWatcher(et_usia));
        et_tinggi.addTextChangedListener(new MTextWatcher(et_tinggi));
        et_unit.addTextChangedListener(new MTextWatcher(et_unit));
        et_klas.addTextChangedListener(new MTextWatcher(et_klas));
        et_persentase.addTextChangedListener(new MTextWatcher(et_persentase));

        ac_pekerjaan.addTextChangedListener(new MTextWatcher(ac_pekerjaan));
        ac_pekerjaan.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_pekerjaan, this));
        ac_pekerjaan.setOnClickListener(this);
        ac_pekerjaan.setOnFocusChangeListener(this);

        ac_hubungan_dengan_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_hubungan_dengan_pp, this));
        ac_hubungan_dengan_pp.setOnClickListener(this);
        ac_hubungan_dengan_pp.setOnFocusChangeListener(this);
        ac_hubungan_dengan_pp.addTextChangedListener(new MTextWatcher(ac_hubungan_dengan_pp));

        ac_jns_produk.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jns_produk, this));
        ac_jns_produk.setOnClickListener(this);
        ac_jns_produk.setOnFocusChangeListener(this);
        ac_jns_produk.addTextChangedListener(new MTextWatcher(ac_jns_produk));

        ac_jns_rider.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jns_rider, this));
        ac_jns_rider.setOnClickListener(this);
        ac_jns_rider.setOnFocusChangeListener(this);
        ac_jns_rider.addTextChangedListener(new MTextWatcher(ac_jns_rider));

        ac_warganegara.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_warganegara, this));
        ac_warganegara.setOnClickListener(this);
        ac_warganegara.setOnFocusChangeListener(this);
        ac_warganegara.addTextChangedListener(new MTextWatcher(ac_warganegara));

        btn_cancel.setOnClickListener(this);
        btn_lanjut.setOnClickListener(this);

        rg_jeniskel.setOnCheckedChangeListener(this);

//        try {
//            MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_warganegara, this, 0);
//            MethodSupport.load_setDropXml(R.xml.e_relasi, ac_hubungan_dengan_pp, this, 1);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (XmlPullParserException e) {
//            e.printStackTrace();
//        }
//        setAdapterProduk();
//        setAdapterSubProduk();
        restore();
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_warganegara, this, 0);
            MethodSupport.load_setDropXml(R.xml.e_relasi, ac_hubungan_dengan_pp, this, 1);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
//        setAdapterProduk();
//        setAdapterSubProduk();
        setAdapterPekerjaan("", ac_pekerjaan);

        checkForSelectedJobDescriptionIsOnTheList(ac_pekerjaan.getText().toString().trim(), ac_pekerjaan);
        //Make the job description field dots orange as default when user haven't select any job desc.
        if (TextUtils.isEmpty(ac_pekerjaan.getText().toString().trim())) {
            iv_circle_pekerjaan.setColorFilter(ContextCompat.getColor(this, R.color.orange));
        }
    }

    private void setAdapterPekerjaan(String cari_kerja, AutoCompleteTextView ac_kerja) {
        ArrayList<ModelDropdownInt> Dropdown = new E_Select(this).getLst_Pekerjaan(cari_kerja);
        ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_kerja.setAdapter(Adapter);
    }

    private void checkForSelectedJobDescriptionIsOnTheList(String selectedJobDesc, AutoCompleteTextView ac_kerja) {
        //Search from DB
        ArrayList<ModelDropdownInt> result = new E_Select(this).getLst_Pekerjaan(selectedJobDesc);
        if (result.isEmpty()) {
            ac_kerja.setError(getString(R.string.err_msg_jobdesc_not_found_on_the_list));
            isJobDescriptionValid = false;
        } else {
            ac_kerja.setError(null);
            iv_circle_pekerjaan.setColorFilter(ContextCompat.getColor(this, R.color.green));
            ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, result);
            ac_kerja.setAdapter(Adapter);
            isJobDescriptionValid = true;
        }
    }

    private void loadview() {
        //lanjutin loadnya
        modelRider.setVal_rider(Validation());
        modelRider.setKode_produk_rider(produk);
        modelRider.setKode_subproduk_rider(sub_produk);
        modelRider.setWarganegara_askes(warganegara);
        modelRider.setHubungan_askes(hubungan);
        modelRider.setJekel_askes(jekel);
        modelRider.setPeserta_askes(et_nama.getText().toString());
        modelRider.setPekerjaan_askes(ac_pekerjaan.getText().toString());
        modelRider.setPeserta_askes(et_nama.getText().toString());
        modelRider.setPekerjaan_askes(ac_pekerjaan.getText().toString());
        modelRider.setPersentase_rider(MethodSupport.ConverttoInt(et_persentase));
        modelRider.setUnit_rider(MethodSupport.ConverttoInt(et_unit));
        modelRider.setKlas_rider(MethodSupport.ConverttoInt(et_klas));
        modelRider.setBerat_askes(MethodSupport.ConverttoInt(et_bb));
        modelRider.setTinggi_askes(MethodSupport.ConverttoInt(et_tinggi));
        modelRider.setUsia_askes(MethodSupport.ConverttoInt(et_usia));
        modelRider.setTtl_askes(et_tgl_lahir.getText().toString());
    }

    private void restore() {
        produk = modelRider.getKode_produk_rider();
        ac_jns_produk.setText(new E_Select(this).getNamaProduk(produk));
        MethodSupport.active_view(0, ac_jns_produk);
        sub_produk = modelRider.getKode_subproduk_rider();
        ac_jns_rider.setText(new E_Select(this).getNamaSubProduk(produk, sub_produk));
        MethodSupport.active_view(0, ac_jns_rider);

        warganegara = modelRider.getWarganegara_askes();
        ListDropDownSPAJ = MethodSupport.getXML(this, R.xml.e_warganegara, 0);
        ac_warganegara.setText(MethodSupport.getAdapterPositionSPAJ( ListDropDownSPAJ, warganegara));
        String tertanggung = new E_Select(this).getSubPesertaI(produk, sub_produk);
        if (tertanggung.contains("(TERTANGGUNG I)")) {
            hubungan = me.getPemegangPolisModel().getHubungan_pp();
        } else {
            hubungan = modelRider.getHubungan_askes();
        }
        ListDropDownSPAJ = MethodSupport.getXML(this, R.xml.e_relasi, 1);
        ac_hubungan_dengan_pp.setText(MethodSupport.getAdapterPositionSPAJ( ListDropDownSPAJ, hubungan));
//        MethodSupport.active_view(0, ac_hubungan_dengan_pp);
        jekel = modelRider.getJekel_askes();
        MethodSupport.OnsetTwoRadio(rg_jeniskel, jekel);

//        MethodSupport.active_radio(0, rg_jeniskel);
        if (!modelRider.getPeserta_askes().equals("")) {
            cl_peserta.setVisibility(View.VISIBLE);
            et_nama.setText(modelRider.getPeserta_askes());
            MethodSupport.active_view(0, et_nama);
            pekerjaan = modelRider.getPekerjaan_askes();
            ac_pekerjaan.setText(pekerjaan);
            if (modelRider.getPersentase_rider() == 0){
                et_persentase.setText("");
            }else {
                et_persentase.setText(String.valueOf(modelRider.getPersentase_rider()));
            }
            et_unit.setText(String.valueOf(modelRider.getUnit_rider()));
            et_klas.setText(String.valueOf(modelRider.getKlas_rider()));
            et_tinggi.setText(String.valueOf(modelRider.getTinggi_askes()));
            et_bb.setText(String.valueOf(modelRider.getBerat_askes()));
            et_usia.setText(String.valueOf(modelRider.getUsia_askes()));
            MethodSupport.active_view(0, et_usia);
            et_tgl_lahir.setText(String.valueOf(modelRider.getTtl_askes()));
            MethodSupport.active_view(0, et_tgl_lahir);
        } else {
            cl_peserta.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ac_hubungan_dengan_pp:
                ac_hubungan_dengan_pp.showDropDown();
                break;
            case R.id.ac_jns_rider:
                ac_jns_rider.showDropDown();
                break;
            case R.id.ac_jns_produk:
                ac_jns_produk.showDropDown();
                break;
            case R.id.ac_warganegara:
                ac_warganegara.showDropDown();
                break;
            case R.id.ac_pekerjaan:
                ac_pekerjaan.showDropDown();
                break;
            case R.id.btn_cancel:
                finish();
                break;
            case R.id.btn_lanjut:
                loadview();
                if ((Validation())){
                Intent intent = new Intent();
                intent.putExtra(getString(R.string.modelrider), modelRider);
                intent.putExtra(getString(R.string.pos), pos);
                setResult(RESULT_OK, intent);
                finish();
                }
                break;
            case R.id.et_tgl_lahir:
                showDateDialog(1, et_tgl_lahir.getText().toString(), et_tgl_lahir);
                break;
        }
    }

    private void showDateDialog(int flag_ttl, String tanggal, EditText edit) {
        switch (flag_ttl) {
            case 1:
                if (tanggal.length() != 0) {
                    calendar = toCalendar(tanggal);
                } else {
                    calendar = Calendar.getInstance();
                }
                DatePickerDialog dialog = new DatePickerDialog(this,
                        datePickerListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                dialog.show();
                break;
        }
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            calendar.set(Calendar.YEAR, selectedYear);
            calendar.set(Calendar.MONTH, selectedMonth);
            calendar.set(Calendar.DAY_OF_MONTH, selectedDay);
            et_tgl_lahir.setText(StaticMethods.toStringDate(calendar, 5));
            et_usia.setText(F_Hit_Umur.OnCountUsia(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE)));
        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.ac_hubungan_dengan_pp:
                ModelDropdownInt modelNm_produk_ua = (ModelDropdownInt) parent.getAdapter().getItem(position);
                hubungan = modelNm_produk_ua.getId();
                break;
            case R.id.ac_jns_rider:
                DropboxModel rider = (DropboxModel) parent.getAdapter().getItem(position);
                produk = rider.getId();
                break;
            case R.id.ac_jns_produk:
                DropboxModel modelSubRider = (DropboxModel) parent.getAdapter().getItem(position);
                sub_produk = modelSubRider.getId();
                break;
            case R.id.ac_warganegara:
                ModelDropdownInt modelWn = (ModelDropdownInt) parent.getAdapter().getItem(position);
                warganegara = modelWn.getId();
                break;
            case R.id.ac_pekerjaan:
                //To make sure job description is only allowed picked from the dropdown
                isJobDescriptionValid = true;
                iv_circle_pekerjaan.setColorFilter(ContextCompat.getColor(this, R.color.green));
//                ModelDropdownInt model_pekerjaan = (ModelDropdownInt) parent.getAdapter().getItem(position);
                pekerjaan = ac_pekerjaan.getText().toString();
                break;
        }
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.ac_hubungan_dengan_pp:
                if (hasFocus) ac_hubungan_dengan_pp.showDropDown();
                break;
            case R.id.ac_jns_rider:
                if (hasFocus) ac_jns_rider.showDropDown();
                break;
            case R.id.ac_jns_produk:
                if (hasFocus) ac_jns_produk.showDropDown();
                break;
            case R.id.ac_warganegara:
                if (hasFocus) ac_warganegara.showDropDown();
                break;
            case R.id.ac_pekerjaan:
                if (hasFocus) ac_pekerjaan.showDropDown();
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
        jekel = MethodSupport.OnCheckRadio(rg_jeniskel, i);
        iv_circle_jenis_kelamin.setColorFilter(ContextCompat.getColor(this, R.color.green));

    }

    public class MTextWatcher implements TextWatcher {
        private View view;

        MTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                case R.id.et_nama:
                    Method_Validator.ValEditWatcher(et_nama, iv_circle_nama, tv_error_nama, getApplicationContext());
                    break;
                case R.id.et_bb:
                    Method_Validator.ValEditWatcherKes(et_bb, iv_circle_bb, tv_error_bb, getApplicationContext());
                    break;
                case R.id.et_tgl_lahir:
                    Method_Validator.ValEditWatcher(et_tgl_lahir, iv_circle_tgl_lahir, tv_error_tgl_lahir, getApplicationContext());
                    break;
                case R.id.et_usia:
                    Method_Validator.ValEditWatcher(et_usia, iv_circle_usia, tv_error_usia, getApplicationContext());
                    break;
                case R.id.et_tinggi:
                    Method_Validator.ValEditWatcherKes(et_tinggi, iv_circle_tinggi, tv_error_tinggi, getApplicationContext());
                    break;
                case R.id.et_unit:
                    Method_Validator.ValEditWatcher(et_unit, iv_circle_unit, tv_error_unit, getApplicationContext());
                    break;
                case R.id.et_persentase:
                    Method_Validator.ValEditWatcher(et_persentase, iv_circle_persentase, tv_error_persentase, getApplicationContext());
                    break;
                case R.id.et_klas:
                    Method_Validator.ValEditWatcher(et_klas, iv_circle_unit, tv_error_klas, getApplicationContext());
                    break;
                //auto
                case R.id.ac_hubungan_dengan_pp:
                    Method_Validator.ValEditWatcher(ac_hubungan_dengan_pp, iv_circle_hubungan_dengan_pp, tv_error_hubungan_dengan_pp, getApplicationContext());
                    break;
//                case R.id.ac_jns_rider:
//                    Method_Validator.ValEditWatcher(ac_jns_rider, iv_circle_jns_rider, tv_error_jns_rider, getApplicationContext());
//                    break;
//                case R.id.ac_jns_produk:
//                    Method_Validator.ValEditWatcher(ac_jns_produk, iv_circle_jns_produk, tv_error_jns_produk, getApplicationContext());
//                    break; dinon aktifkan dulu untuk autosales, untuk espaj nanti dibuka lagi
                case R.id.ac_warganegara:
                    Method_Validator.ValEditWatcher(ac_warganegara, iv_circle_warganegara, tv_error_warganegara, getApplicationContext());
                    break;
                case R.id.ac_pekerjaan:
                    setAdapterPekerjaan(ac_pekerjaan.getText().toString().trim(), ac_pekerjaan);
                    validateJobDescription(ac_pekerjaan.getText().toString().trim(), ac_pekerjaan);
                    break;
            }
        }
    }

    private void validateJobDescription(String searchQuery, AutoCompleteTextView ac_kerja) {
        //Check if job desc search query empty
        if (!TextUtils.isEmpty(searchQuery)) {
            ac_kerja.setError(null);
            iv_circle_pekerjaan.setColorFilter(ContextCompat.getColor(this, R.color.orange));

            //Search from DB
            ArrayList<ModelDropdownInt> result = new E_Select(this).getLst_Pekerjaan(searchQuery);
            if (result.isEmpty()) {
                ac_kerja.setError(getString(R.string.err_msg_jobdesc_not_found_on_the_list));
            } else {
                ac_kerja.setError(null);
                ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, result);
                ac_kerja.setAdapter(Adapter);
            }
            // To make sure user only pick job desc from dropdown so we make it false
            isJobDescriptionValid = false;

        } else {
            ac_kerja.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
            iv_circle_pekerjaan.setColorFilter(ContextCompat.getColor(this, R.color.red));

            // To make sure user only pick job desc from dropdown so we make it false
            isJobDescriptionValid = false;
        }
    }

    private Boolean Validation() {
        Boolean val = true;
        int color = ContextCompat.getColor(this, R.color.red);
        if (ac_jns_produk.getVisibility() == View.VISIBLE && ac_jns_produk.getText().toString().trim().length() == 0) {
            val = false;
//            MethodSupport.onFocusview(scroll_asuransi, cl_child_asuransi, ac_jns_produk, tv_error_jns_produk, iv_circle_jns_produk, color, this, getString(R.string.error_field_required));
            tv_error_jns_produk.setVisibility(View.VISIBLE);
            tv_error_jns_produk.setText(getString(R.string.error_field_required));
            iv_circle_jns_produk.setColorFilter(color);
        }

        if (ac_jns_rider.getVisibility() == View.VISIBLE && ac_jns_rider.getText().toString().trim().length() == 0) {
            val = false;
//            MethodSupport.onFocusview(scroll_asuransi, cl_child_asuransi, ac_jns_rider, tv_error_jns_rider, iv_circle_jns_rider, color, this, getString(R.string.error_field_required));
            tv_error_jns_rider.setVisibility(View.VISIBLE);
            tv_error_jns_rider.setText(getString(R.string.error_field_required));
            iv_circle_jns_rider.setColorFilter(color);
        }

        if (et_bb.getVisibility() == View.VISIBLE) {
            if ((et_bb.getText().toString().trim().length() == 0 || et_bb.getText().toString().trim().equals("0")) ||
                    (et_tinggi.getText().toString().trim().length() == 0 || et_tinggi.getText().toString().trim().equals("0"))) {
                val = false;
                MethodSupport.onFocusview(scroll_asuransi, cl_child_asuransi, et_bb, tv_error_bb, iv_circle_bb, color, this, getString(R.string.error_field_required));
//                tv_error_bb.setVisibility(View.VISIBLE);
//                tv_error_bb.setText(getString(R.string.error_field_required));
//                iv_circle_bb.setColorFilter(color);
            }
        }

        if (et_nama.getVisibility() == View.VISIBLE && et_nama.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_asuransi, cl_child_asuransi, et_nama, tv_error_nama, iv_circle_nama, color, this, getString(R.string.error_field_required));
//            tv_error_nama.setVisibility(View.VISIBLE);
//            tv_error_nama.setText(getString(R.string.error_field_required));
//            iv_circle_nama.setColorFilter(color);
        }else {
            if (!Method_Validator.ValEditRegex(et_nama.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_asuransi, cl_child_asuransi, et_nama, tv_error_nama, iv_circle_nama, color, this, getString(R.string.error_regex));
            }
        }

        if (et_tgl_lahir.getVisibility() == View.VISIBLE && et_tgl_lahir.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_asuransi, cl_child_asuransi, et_tgl_lahir, tv_error_tgl_lahir, iv_circle_tgl_lahir, color, this, getString(R.string.error_field_required));
//            tv_error_tgl_lahir.setVisibility(View.VISIBLE);
//            tv_error_tgl_lahir.setText(getString(R.string.error_field_required));
//            iv_circle_tgl_lahir.setColorFilter(color);
        }

        if (ac_hubungan_dengan_pp.getVisibility() == View.VISIBLE && ac_hubungan_dengan_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_asuransi, cl_child_asuransi, ac_hubungan_dengan_pp, tv_error_hubungan_dengan_pp, iv_circle_hubungan_dengan_pp, color, this, getString(R.string.error_field_required));
//            tv_error_hubungan_dengan_pp.setVisibility(View.VISIBLE);
//            tv_error_hubungan_dengan_pp.setText(getString(R.string.error_field_required));
//            iv_circle_hubungan_dengan_pp.setColorFilter(color);
        }

        if (ac_pekerjaan.getVisibility() == View.VISIBLE && ac_pekerjaan.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_asuransi, cl_child_asuransi, ac_pekerjaan, tv_error_pekerjaan, iv_circle_pekerjaan, color, this, getString(R.string.error_field_required));
        }

        //radiogroup
        if (jekel == -1) {
            val = false;
            tv_error_jenis_kelamin.setVisibility(View.VISIBLE);
            tv_error_jenis_kelamin.setText(getString(R.string.error_field_required));
            iv_circle_jenis_kelamin.setColorFilter(color);
        }
        return val;
    }

    private void setAdapterProduk() {
//        ArrayList<DropboxModel> dRencanas = new P_Select(this).selectListRencana(groupId);
//        ArrayAdapter<DropboxModel> mRencanaAdapter = new ArrayAdapter<>(this,
//                android.R.layout.simple_dropdown_item_1line, dRencanas);
//        ac_produk_ua.setAdapter(mRencanaAdapter);
    }

    private void setAdapterSubProduk() {
//        ArrayList<DropboxModel> dRencanaSubs = new P_Select(this).selectListRencanaSub(groupId, produk);
//        ArrayAdapter<DropboxModel> mRencanaSubAdapter = new ArrayAdapter<DropboxModel>(this,
//                android.R.layout.simple_dropdown_item_1line, dRencanaSubs);
//        ac_nm_produk_ua.setAdapter(mRencanaSubAdapter);
    }

    @BindView(R.id.cl_nama)
    RelativeLayout cl_nama;
    @BindView(R.id.et_nama)
    EditText et_nama;
    @BindView(R.id.iv_circle_nama)
    ImageView iv_circle_nama;
    @BindView(R.id.tv_error_nama)
    TextView tv_error_nama;

    @BindView(R.id.cl_jenis_kelamin)
    RelativeLayout cl_jenis_kelamin;
    @BindView(R.id.iv_circle_jenis_kelamin)
    ImageView iv_circle_jenis_kelamin;
    @BindView(R.id.rg_jeniskel)
    RadioGroup rg_jeniskel;
    @BindView(R.id.rb_pria)
    RadioButton rb_pria;
    @BindView(R.id.rb_wanita)
    RadioButton rb_wanita;
    @BindView(R.id.tv_error_jenis_kelamin)
    TextView tv_error_jenis_kelamin;

    @BindView(R.id.cl_tgl_lahir)
    RelativeLayout cl_tgl_lahir;
    @BindView(R.id.iv_circle_tgl_lahir)
    ImageView iv_circle_tgl_lahir;
    @BindView(R.id.et_tgl_lahir)
    EditText et_tgl_lahir;
    @BindView(R.id.tv_error_tgl_lahir)
    TextView tv_error_tgl_lahir;


    @BindView(R.id.cl_usia)
    RelativeLayout cl_usia;
    @BindView(R.id.iv_circle_usia)
    ImageView iv_circle_usia;
    @BindView(R.id.iv_circle_tinggi)
    ImageView iv_circle_tinggi;
    @BindView(R.id.et_usia)
    EditText et_usia;
    @BindView(R.id.tv_error_usia)
    TextView tv_error_usia;

    @BindView(R.id.cl_hubungan_dengan_pp)
    RelativeLayout cl_hubungan_dengan_pp;
    @BindView(R.id.iv_circle_hubungan_dengan_pp)
    ImageView iv_circle_hubungan_dengan_pp;
    @BindView(R.id.ac_hubungan_dengan_pp)
    AutoCompleteTextView ac_hubungan_dengan_pp;
    @BindView(R.id.tv_error_hubungan_dengan_pp)
    TextView tv_error_hubungan_dengan_pp;

    @BindView(R.id.cl_jns_produk)
    RelativeLayout cl_jns_produk;
    @BindView(R.id.iv_circle_jns_produk)
    ImageView iv_circle_jns_produk;
    @BindView(R.id.ac_jns_produk)
    AutoCompleteTextView ac_jns_produk;
    @BindView(R.id.tv_error_jns_produk)
    TextView tv_error_jns_produk;

    @BindView(R.id.cl_jns_rider)
    RelativeLayout cl_jns_rider;
    @BindView(R.id.iv_circle_jns_rider)
    ImageView iv_circle_jns_rider;
    @BindView(R.id.ac_jns_rider)
    AutoCompleteTextView ac_jns_rider;
    @BindView(R.id.tv_error_jns_rider)
    TextView tv_error_jns_rider;

    @BindView(R.id.cl_unit)
    RelativeLayout cl_unit;
    @BindView(R.id.iv_circle_unit)
    ImageView iv_circle_unit;
    @BindView(R.id.et_unit)
    EditText et_unit;
    @BindView(R.id.tv_error_unit)
    TextView tv_error_unit;

    @BindView(R.id.cl_klas)
    RelativeLayout cl_klas;
    @BindView(R.id.et_klas)
    EditText et_klas;
    @BindView(R.id.tv_error_klas)
    TextView tv_error_klas;

    @BindView(R.id.cl_persentase)
    RelativeLayout cl_persentase;
    @BindView(R.id.et_persentase)
    EditText et_persentase;
    @BindView(R.id.iv_circle_persentase)
    ImageView iv_circle_persentase;
    @BindView(R.id.tv_error_persentase)
    TextView tv_error_persentase;

    @BindView(R.id.cl_bb)
    RelativeLayout cl_bb;
    @BindView(R.id.et_bb)
    EditText et_bb;
    @BindView(R.id.iv_circle_bb)
    ImageView iv_circle_bb;
    @BindView(R.id.tv_error_bb)
    TextView tv_error_bb;

    @BindView(R.id.cl_tinggi)
    RelativeLayout cl_tinggi;
    @BindView(R.id.et_tinggi)
    EditText et_tinggi;
    @BindView(R.id.tv_error_tinggi)
    TextView tv_error_tinggi;

    @BindView(R.id.cl_pekerjaan)
    RelativeLayout cl_pekerjaan;
    @BindView(R.id.iv_circle_pekerjaan)
    ImageView iv_circle_pekerjaan;
    @BindView(R.id.tv_error_pekerjaan)
    TextView tv_error_pekerjaan;

    @BindView(R.id.cl_warganegara)
    RelativeLayout cl_warganegara;
    @BindView(R.id.iv_circle_warganegara)
    ImageView iv_circle_warganegara;
    @BindView(R.id.ac_warganegara)
    AutoCompleteTextView ac_warganegara;
    @BindView(R.id.tv_error_warganegara)
    TextView tv_error_warganegara;
    @BindView(R.id.ac_pekerjaan)
    AutoCompleteTextView ac_pekerjaan;

    @BindView(R.id.cl_form_info_asuransi_tambahan)
    RelativeLayout cl_form_info_asuransi_tambahan;
    @BindView(R.id.cl_peserta)
    RelativeLayout cl_peserta;

    @BindView(R.id.btn_cancel)
    Button btn_cancel;
    @BindView(R.id.btn_lanjut)
    Button btn_lanjut;

    @BindView(R.id.scroll_asuransi)
    ScrollView scroll_asuransi;

    @BindView(R.id.cl_child_asuransi)
    RelativeLayout cl_child_asuransi;

    private ModelAddRiderUA modelRider;
    private EspajModel me;

    private int produk = 0;
    private int sub_produk = 0;
    private int jekel = -1;
    private int warganegara = 0;
    private int hubungan = 0;
    private int pos = 0;

    private String pekerjaan = "";

    private ArrayList<ModelDropdownInt> ListDropDownSPAJ;

    private Intent intent;

    private Calendar calendar;
}
