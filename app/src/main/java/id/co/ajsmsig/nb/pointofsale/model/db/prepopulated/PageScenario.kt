package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Entity
class PageScenario(
        @PrimaryKey
        val id: Int,
        var pageId: Int?,
        var optionId: Int?,
        var nextPageId: Int?
)