package id.co.ajsmsig.nb.espaj.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by eriza on 5/08/2017.
 */

public class PemegangPolisModel implements Parcelable {
    private String nama_pp = "";
    private String gelar_pp = "";
    private String nama_ibu_pp = "";
    private int bukti_identitas_pp = 0;
    private String bukti_identitas_lain_pp = "";
    private String no_identitas_pp = "";
    private String tgl_berlaku_pp = "";
    private int warga_negara_pp = 0;
    private String tempat_pp = "";
    private String ttl_pp = "";
    private String usia_pp = "";
    private int jekel_pp = 0;
    private int status_pp = 0;
    private int agama_pp = 0;
    private String agama_lain_pp = "";
    private int pendidikan_pp = 0;
    private String alamat_pp = "";
    private String kota_pp = "";
    private String kdpos_pp = "";
    private String telp1_pp = "";
    private String kdtelp1_pp = "";
    private String telp2_pp = "";
    private String kdtelp2_pp = "";
    private String alamat_kantor_pp = "";
    private String kota_kantor_pp = "";
    private String kdpos_kantor_pp = "";
    private String kdtelp1_kantor_pp = "";
    private String telp1_kantor_pp = "";
    private String kdtelp2_kantor_pp = "";
    private String telp2_kantor_pp = "";
    private String kdfax_kantor_pp = "";
    private String fax_kantor_pp = "";
    private String alamat_tinggal_pp = "";
    private String kota_tinggal_pp = "";
    private String kdpos_tinggal_pp = "";
    private String kdtelp1_tinggal_pp = "";
    private String telp1_tinggal_pp = "";
    private String kdtelp2_tinggal_pp = "";
    private String telp2_tinggal_pp = "";
    private String kdfax_tinggal_pp = "";
    private String fax_tinggal_pp = "";
    private int tagihan_pp = 0;
    private String hp1_pp = "";
    private String hp2_pp = "";
    private String email_pp = "";
    private String klasifikasi_pekerjaan_pp = "";
    private String uraian_pekerjaan_pp = "";
    private String jabatan_klasifikasi_pp = "";
    private int hubungan_pp = 0;
    private int greencard_pp = 0;
    private String alias_pp = "";
    private String nm_perusahaan_pp = "";
    private String npwp_pp = "";
    private String tagihan_rutin_pp = "";
    private String krm_polis_pp = "";
    private String suamirt_pp = "";
    private String ttlsuami_rt_pp = "";
    private String usiasuami_rt_pp = "";
    private String pekerjaansuami_rt_pp = "";
    private String jabatansuami_rt_pp = "";
    private String perusahaansuami_rt_pp = "";
    private String bidusaha_suamirt_pp = "";
    private String npwp_suamirt_pp = "";
    private String penghasilan_suamithn_pp = "";
    private String ayah_pp = "";
    private String ttl_ayah_pp = "";
    private String usia_ayah_pp = "";
    private String pekerjaan_ayah_pp = "";
    private String jabatan_ayah_pp = "";
    private String perusahaan_ayah_pp = "";
    private String bidusaha_ayah_pp = "";
    private String npwp_ayah_pp = "";
    private String penghasilan_ayah_pp = "";
    private String nm_ibu_pp = "";
    private String ttl_ibu_pp = "";
    private String usia_ibu_pp = "";
    private String pekerjaan_ibu_pp = "";
    private String jabatan_ibu_pp = "";
    private String perusahaan_ibu_pp = "";
    private String bidusaha_ibu_pp = "";
    private String npwp_ibu_pp = "";
    private String penghasilan_ibu_pp = "";
    private String pendapatan_pp = "";
    private int hubungancp_pp = 0;
    private String no_ciff_pp = "";
    private String d_gaji_pp = "";
    private String d_tabungan_pp = "";
    private String d_warisan_pp = "";
    private String d_hibah_pp = "";
    private String d_lainnya_pp = "";
    private String edit_d_lainnya_pp = "";
    private String t_proteksi_pp = "";
    private String t_inves_pp = "";
    private String t_lainnya_pp = "";
    private String edit_t_lainnya = "";
    private Boolean validation = false;
    String propinsi_pp = "";
    String propinsi_kantor_pp = "";
    String propinsi_tinggal_pp = "";
    String kabupaten_pp = "";
    String kabupaten_kantor_pp = "";
    String kabupaten_tinggal_pp = "";
    String kecamatan_pp = "";
    String kecamatan_kantor_pp = "";
    String kecamatan_tinggal_pp = "";
    String kelurahan_pp = "";
    String kelurahan_kantor_pp = "";
    String kelurahan_tinggal_pp = "";

    public PemegangPolisModel() {
    }


    protected PemegangPolisModel(Parcel in) {
        nama_pp = in.readString();
        gelar_pp = in.readString();
        nama_ibu_pp = in.readString();
        bukti_identitas_pp = in.readInt();
        bukti_identitas_lain_pp = in.readString();
        no_identitas_pp = in.readString();
        tgl_berlaku_pp = in.readString();
        warga_negara_pp = in.readInt();
        tempat_pp = in.readString();
        ttl_pp = in.readString();
        usia_pp = in.readString();
        jekel_pp = in.readInt();
        status_pp = in.readInt();
        agama_pp = in.readInt();
        agama_lain_pp = in.readString();
        pendidikan_pp = in.readInt();
        alamat_pp = in.readString();
        kota_pp = in.readString();
        kdpos_pp = in.readString();
        telp1_pp = in.readString();
        kdtelp1_pp = in.readString();
        telp2_pp = in.readString();
        kdtelp2_pp = in.readString();
        alamat_kantor_pp = in.readString();
        kota_kantor_pp = in.readString();
        kdpos_kantor_pp = in.readString();
        kdtelp1_kantor_pp = in.readString();
        telp1_kantor_pp = in.readString();
        kdtelp2_kantor_pp = in.readString();
        telp2_kantor_pp = in.readString();
        kdfax_kantor_pp = in.readString();
        fax_kantor_pp = in.readString();
        alamat_tinggal_pp = in.readString();
        kota_tinggal_pp = in.readString();
        kdpos_tinggal_pp = in.readString();
        kdtelp1_tinggal_pp = in.readString();
        telp1_tinggal_pp = in.readString();
        kdtelp2_tinggal_pp = in.readString();
        telp2_tinggal_pp = in.readString();
        kdfax_tinggal_pp = in.readString();
        fax_tinggal_pp = in.readString();
        tagihan_pp = in.readInt();
        hp1_pp = in.readString();
        hp2_pp = in.readString();
        email_pp = in.readString();
        klasifikasi_pekerjaan_pp = in.readString();
        uraian_pekerjaan_pp = in.readString();
        jabatan_klasifikasi_pp = in.readString();
        hubungan_pp = in.readInt();
        greencard_pp = in.readInt();
        alias_pp = in.readString();
        nm_perusahaan_pp = in.readString();
        npwp_pp = in.readString();
        tagihan_rutin_pp = in.readString();
        krm_polis_pp = in.readString();
        suamirt_pp = in.readString();
        ttlsuami_rt_pp = in.readString();
        usiasuami_rt_pp = in.readString();
        pekerjaansuami_rt_pp = in.readString();
        jabatansuami_rt_pp = in.readString();
        perusahaansuami_rt_pp = in.readString();
        bidusaha_suamirt_pp = in.readString();
        npwp_suamirt_pp = in.readString();
        penghasilan_suamithn_pp = in.readString();
        ayah_pp = in.readString();
        ttl_ayah_pp = in.readString();
        usia_ayah_pp = in.readString();
        pekerjaan_ayah_pp = in.readString();
        jabatan_ayah_pp = in.readString();
        perusahaan_ayah_pp = in.readString();
        bidusaha_ayah_pp = in.readString();
        npwp_ayah_pp = in.readString();
        penghasilan_ayah_pp = in.readString();
        nm_ibu_pp = in.readString();
        ttl_ibu_pp = in.readString();
        usia_ibu_pp = in.readString();
        pekerjaan_ibu_pp = in.readString();
        jabatan_ibu_pp = in.readString();
        perusahaan_ibu_pp = in.readString();
        bidusaha_ibu_pp = in.readString();
        npwp_ibu_pp = in.readString();
        penghasilan_ibu_pp = in.readString();
        pendapatan_pp = in.readString();
        hubungancp_pp = in.readInt();
        no_ciff_pp = in.readString();
        d_gaji_pp = in.readString();
        d_tabungan_pp = in.readString();
        d_warisan_pp = in.readString();
        d_hibah_pp = in.readString();
        d_lainnya_pp = in.readString();
        edit_d_lainnya_pp = in.readString();
        t_proteksi_pp = in.readString();
        t_inves_pp = in.readString();
        t_lainnya_pp = in.readString();
        edit_t_lainnya = in.readString();
        byte tmpValidation = in.readByte();
        validation = tmpValidation == 0 ? null : tmpValidation == 1;
        propinsi_pp = in.readString();
        propinsi_kantor_pp = in.readString();
        propinsi_tinggal_pp = in.readString();
        kabupaten_pp = in.readString();
        kabupaten_kantor_pp = in.readString();
        kabupaten_tinggal_pp = in.readString();
        kecamatan_pp = in.readString();
        kecamatan_kantor_pp = in.readString();
        kecamatan_tinggal_pp = in.readString();
        kelurahan_pp = in.readString();
        kelurahan_kantor_pp = in.readString();
        kelurahan_tinggal_pp = in.readString();
    }

    public String getNama_pp() {
        return nama_pp;
    }

    public void setNama_pp(String nama_pp) {
        this.nama_pp = nama_pp;
    }

    public String getGelar_pp() {
        return gelar_pp;
    }

    public void setGelar_pp(String gelar_pp) {
        this.gelar_pp = gelar_pp;
    }

    public String getNama_ibu_pp() {
        return nama_ibu_pp;
    }

    public void setNama_ibu_pp(String nama_ibu_pp) {
        this.nama_ibu_pp = nama_ibu_pp;
    }

    public int getBukti_identitas_pp() {
        return bukti_identitas_pp;
    }

    public void setBukti_identitas_pp(int bukti_identitas_pp) {
        this.bukti_identitas_pp = bukti_identitas_pp;
    }

    public String getBukti_identitas_lain_pp() {
        return bukti_identitas_lain_pp;
    }

    public void setBukti_identitas_lain_pp(String bukti_identitas_lain_pp) {
        this.bukti_identitas_lain_pp = bukti_identitas_lain_pp;
    }

    public String getNo_identitas_pp() {
        return no_identitas_pp;
    }

    public void setNo_identitas_pp(String no_identitas_pp) {
        this.no_identitas_pp = no_identitas_pp;
    }

    public String getTgl_berlaku_pp() {
        return tgl_berlaku_pp;
    }

    public void setTgl_berlaku_pp(String tgl_berlaku_pp) {
        this.tgl_berlaku_pp = tgl_berlaku_pp;
    }

    public int getWarga_negara_pp() {
        return warga_negara_pp;
    }

    public void setWarga_negara_pp(int warga_negara_pp) {
        this.warga_negara_pp = warga_negara_pp;
    }

    public String getTempat_pp() {
        return tempat_pp;
    }

    public void setTempat_pp(String tempat_pp) {
        this.tempat_pp = tempat_pp;
    }

    public String getTtl_pp() {
        return ttl_pp;
    }

    public void setTtl_pp(String ttl_pp) {
        this.ttl_pp = ttl_pp;
    }

    public String getUsia_pp() {
        return usia_pp;
    }

    public void setUsia_pp(String usia_pp) {
        this.usia_pp = usia_pp;
    }

    public int getJekel_pp() {
        return jekel_pp;
    }

    public void setJekel_pp(int jekel_pp) {
        this.jekel_pp = jekel_pp;
    }

    public int getStatus_pp() {
        return status_pp;
    }

    public void setStatus_pp(int status_pp) {
        this.status_pp = status_pp;
    }

    public int getAgama_pp() {
        return agama_pp;
    }

    public void setAgama_pp(int agama_pp) {
        this.agama_pp = agama_pp;
    }

    public String getAgama_lain_pp() {
        return agama_lain_pp;
    }

    public void setAgama_lain_pp(String agama_lain_pp) {
        this.agama_lain_pp = agama_lain_pp;
    }

    public int getPendidikan_pp() {
        return pendidikan_pp;
    }

    public void setPendidikan_pp(int pendidikan_pp) {
        this.pendidikan_pp = pendidikan_pp;
    }

    public String getAlamat_pp() {
        return alamat_pp;
    }

    public void setAlamat_pp(String alamat_pp) {
        this.alamat_pp = alamat_pp;
    }

    public String getKota_pp() {
        return kota_pp;
    }

    public void setKota_pp(String kota_pp) {
        this.kota_pp = kota_pp;
    }

    public String getKdpos_pp() {
        return kdpos_pp;
    }

    public void setKdpos_pp(String kdpos_pp) {
        this.kdpos_pp = kdpos_pp;
    }

    public String getTelp1_pp() {
        return telp1_pp;
    }

    public void setTelp1_pp(String telp1_pp) {
        this.telp1_pp = telp1_pp;
    }

    public String getKdtelp1_pp() {
        return kdtelp1_pp;
    }

    public void setKdtelp1_pp(String kdtelp1_pp) {
        this.kdtelp1_pp = kdtelp1_pp;
    }

    public String getTelp2_pp() {
        return telp2_pp;
    }

    public void setTelp2_pp(String telp2_pp) {
        this.telp2_pp = telp2_pp;
    }

    public String getKdtelp2_pp() {
        return kdtelp2_pp;
    }

    public void setKdtelp2_pp(String kdtelp2_pp) {
        this.kdtelp2_pp = kdtelp2_pp;
    }

    public String getAlamat_kantor_pp() {
        return alamat_kantor_pp;
    }

    public void setAlamat_kantor_pp(String alamat_kantor_pp) {
        this.alamat_kantor_pp = alamat_kantor_pp;
    }

    public String getKota_kantor_pp() {
        return kota_kantor_pp;
    }

    public void setKota_kantor_pp(String kota_kantor_pp) {
        this.kota_kantor_pp = kota_kantor_pp;
    }

    public String getKdpos_kantor_pp() {
        return kdpos_kantor_pp;
    }

    public void setKdpos_kantor_pp(String kdpos_kantor_pp) {
        this.kdpos_kantor_pp = kdpos_kantor_pp;
    }

    public String getKdtelp1_kantor_pp() {
        return kdtelp1_kantor_pp;
    }

    public void setKdtelp1_kantor_pp(String kdtelp1_kantor_pp) {
        this.kdtelp1_kantor_pp = kdtelp1_kantor_pp;
    }

    public String getTelp1_kantor_pp() {
        return telp1_kantor_pp;
    }

    public void setTelp1_kantor_pp(String telp1_kantor_pp) {
        this.telp1_kantor_pp = telp1_kantor_pp;
    }

    public String getKdtelp2_kantor_pp() {
        return kdtelp2_kantor_pp;
    }

    public void setKdtelp2_kantor_pp(String kdtelp2_kantor_pp) {
        this.kdtelp2_kantor_pp = kdtelp2_kantor_pp;
    }

    public String getTelp2_kantor_pp() {
        return telp2_kantor_pp;
    }

    public void setTelp2_kantor_pp(String telp2_kantor_pp) {
        this.telp2_kantor_pp = telp2_kantor_pp;
    }

    public String getKdfax_kantor_pp() {
        return kdfax_kantor_pp;
    }

    public void setKdfax_kantor_pp(String kdfax_kantor_pp) {
        this.kdfax_kantor_pp = kdfax_kantor_pp;
    }

    public String getFax_kantor_pp() {
        return fax_kantor_pp;
    }

    public void setFax_kantor_pp(String fax_kantor_pp) {
        this.fax_kantor_pp = fax_kantor_pp;
    }

    public String getAlamat_tinggal_pp() {
        return alamat_tinggal_pp;
    }

    public void setAlamat_tinggal_pp(String alamat_tinggal_pp) {
        this.alamat_tinggal_pp = alamat_tinggal_pp;
    }

    public String getKota_tinggal_pp() {
        return kota_tinggal_pp;
    }

    public void setKota_tinggal_pp(String kota_tinggal_pp) {
        this.kota_tinggal_pp = kota_tinggal_pp;
    }

    public String getKdpos_tinggal_pp() {
        return kdpos_tinggal_pp;
    }

    public void setKdpos_tinggal_pp(String kdpos_tinggal_pp) {
        this.kdpos_tinggal_pp = kdpos_tinggal_pp;
    }

    public String getKdtelp1_tinggal_pp() {
        return kdtelp1_tinggal_pp;
    }

    public void setKdtelp1_tinggal_pp(String kdtelp1_tinggal_pp) {
        this.kdtelp1_tinggal_pp = kdtelp1_tinggal_pp;
    }

    public String getTelp1_tinggal_pp() {
        return telp1_tinggal_pp;
    }

    public void setTelp1_tinggal_pp(String telp1_tinggal_pp) {
        this.telp1_tinggal_pp = telp1_tinggal_pp;
    }

    public String getKdtelp2_tinggal_pp() {
        return kdtelp2_tinggal_pp;
    }

    public void setKdtelp2_tinggal_pp(String kdtelp2_tinggal_pp) {
        this.kdtelp2_tinggal_pp = kdtelp2_tinggal_pp;
    }

    public String getTelp2_tinggal_pp() {
        return telp2_tinggal_pp;
    }

    public void setTelp2_tinggal_pp(String telp2_tinggal_pp) {
        this.telp2_tinggal_pp = telp2_tinggal_pp;
    }

    public String getKdfax_tinggal_pp() {
        return kdfax_tinggal_pp;
    }

    public void setKdfax_tinggal_pp(String kdfax_tinggal_pp) {
        this.kdfax_tinggal_pp = kdfax_tinggal_pp;
    }

    public String getFax_tinggal_pp() {
        return fax_tinggal_pp;
    }

    public void setFax_tinggal_pp(String fax_tinggal_pp) {
        this.fax_tinggal_pp = fax_tinggal_pp;
    }

    public int getTagihan_pp() {
        return tagihan_pp;
    }

    public void setTagihan_pp(int tagihan_pp) {
        this.tagihan_pp = tagihan_pp;
    }

    public String getHp1_pp() {
        return hp1_pp;
    }

    public void setHp1_pp(String hp1_pp) {
        this.hp1_pp = hp1_pp;
    }

    public String getHp2_pp() {
        return hp2_pp;
    }

    public void setHp2_pp(String hp2_pp) {
        this.hp2_pp = hp2_pp;
    }

    public String getEmail_pp() {
        return email_pp;
    }

    public void setEmail_pp(String email_pp) {
        this.email_pp = email_pp;
    }

    public String getKlasifikasi_pekerjaan_pp() {
        return klasifikasi_pekerjaan_pp;
    }

    public void setKlasifikasi_pekerjaan_pp(String klasifikasi_pekerjaan_pp) {
        this.klasifikasi_pekerjaan_pp = klasifikasi_pekerjaan_pp;
    }

    public String getUraian_pekerjaan_pp() {
        return uraian_pekerjaan_pp;
    }

    public void setUraian_pekerjaan_pp(String uraian_pekerjaan_pp) {
        this.uraian_pekerjaan_pp = uraian_pekerjaan_pp;
    }

    public String getJabatan_klasifikasi_pp() {
        return jabatan_klasifikasi_pp;
    }

    public void setJabatan_klasifikasi_pp(String jabatan_klasifikasi_pp) {
        this.jabatan_klasifikasi_pp = jabatan_klasifikasi_pp;
    }

    public int getHubungan_pp() {
        return hubungan_pp;
    }

    public void setHubungan_pp(int hubungan_pp) {
        this.hubungan_pp = hubungan_pp;
    }

    public int getGreencard_pp() {
        return greencard_pp;
    }

    public void setGreencard_pp(int greencard_pp) {
        this.greencard_pp = greencard_pp;
    }

    public String getAlias_pp() {
        return alias_pp;
    }

    public void setAlias_pp(String alias_pp) {
        this.alias_pp = alias_pp;
    }

    public String getNm_perusahaan_pp() {
        return nm_perusahaan_pp;
    }

    public void setNm_perusahaan_pp(String nm_perusahaan_pp) {
        this.nm_perusahaan_pp = nm_perusahaan_pp;
    }

    public String getNpwp_pp() {
        return npwp_pp;
    }

    public void setNpwp_pp(String npwp_pp) {
        this.npwp_pp = npwp_pp;
    }

    public String getTagihan_rutin_pp() {
        return tagihan_rutin_pp;
    }

    public void setTagihan_rutin_pp(String tagihan_rutin_pp) {
        this.tagihan_rutin_pp = tagihan_rutin_pp;
    }

    public String getKrm_polis_pp() {
        return krm_polis_pp;
    }

    public void setKrm_polis_pp(String krm_polis_pp) {
        this.krm_polis_pp = krm_polis_pp;
    }

    public String getSuamirt_pp() {
        return suamirt_pp;
    }

    public void setSuamirt_pp(String suamirt_pp) {
        this.suamirt_pp = suamirt_pp;
    }

    public String getTtlsuami_rt_pp() {
        return ttlsuami_rt_pp;
    }

    public void setTtlsuami_rt_pp(String ttlsuami_rt_pp) {
        this.ttlsuami_rt_pp = ttlsuami_rt_pp;
    }

    public String getUsiasuami_rt_pp() {
        return usiasuami_rt_pp;
    }

    public void setUsiasuami_rt_pp(String usiasuami_rt_pp) {
        this.usiasuami_rt_pp = usiasuami_rt_pp;
    }

    public String getPekerjaansuami_rt_pp() {
        return pekerjaansuami_rt_pp;
    }

    public void setPekerjaansuami_rt_pp(String pekerjaansuami_rt_pp) {
        this.pekerjaansuami_rt_pp = pekerjaansuami_rt_pp;
    }

    public String getJabatansuami_rt_pp() {
        return jabatansuami_rt_pp;
    }

    public void setJabatansuami_rt_pp(String jabatansuami_rt_pp) {
        this.jabatansuami_rt_pp = jabatansuami_rt_pp;
    }

    public String getPerusahaansuami_rt_pp() {
        return perusahaansuami_rt_pp;
    }

    public void setPerusahaansuami_rt_pp(String perusahaansuami_rt_pp) {
        this.perusahaansuami_rt_pp = perusahaansuami_rt_pp;
    }

    public String getBidusaha_suamirt_pp() {
        return bidusaha_suamirt_pp;
    }

    public void setBidusaha_suamirt_pp(String bidusaha_suamirt_pp) {
        this.bidusaha_suamirt_pp = bidusaha_suamirt_pp;
    }

    public String getNpwp_suamirt_pp() {
        return npwp_suamirt_pp;
    }

    public void setNpwp_suamirt_pp(String npwp_suamirt_pp) {
        this.npwp_suamirt_pp = npwp_suamirt_pp;
    }

    public String getPenghasilan_suamithn_pp() {
        return penghasilan_suamithn_pp;
    }

    public void setPenghasilan_suamithn_pp(String penghasilan_suamithn_pp) {
        this.penghasilan_suamithn_pp = penghasilan_suamithn_pp;
    }

    public String getAyah_pp() {
        return ayah_pp;
    }

    public void setAyah_pp(String ayah_pp) {
        this.ayah_pp = ayah_pp;
    }

    public String getTtl_ayah_pp() {
        return ttl_ayah_pp;
    }

    public void setTtl_ayah_pp(String ttl_ayah_pp) {
        this.ttl_ayah_pp = ttl_ayah_pp;
    }

    public String getUsia_ayah_pp() {
        return usia_ayah_pp;
    }

    public void setUsia_ayah_pp(String usia_ayah_pp) {
        this.usia_ayah_pp = usia_ayah_pp;
    }

    public String getPekerjaan_ayah_pp() {
        return pekerjaan_ayah_pp;
    }

    public void setPekerjaan_ayah_pp(String pekerjaan_ayah_pp) {
        this.pekerjaan_ayah_pp = pekerjaan_ayah_pp;
    }

    public String getJabatan_ayah_pp() {
        return jabatan_ayah_pp;
    }

    public void setJabatan_ayah_pp(String jabatan_ayah_pp) {
        this.jabatan_ayah_pp = jabatan_ayah_pp;
    }

    public String getPerusahaan_ayah_pp() {
        return perusahaan_ayah_pp;
    }

    public void setPerusahaan_ayah_pp(String perusahaan_ayah_pp) {
        this.perusahaan_ayah_pp = perusahaan_ayah_pp;
    }

    public String getBidusaha_ayah_pp() {
        return bidusaha_ayah_pp;
    }

    public void setBidusaha_ayah_pp(String bidusaha_ayah_pp) {
        this.bidusaha_ayah_pp = bidusaha_ayah_pp;
    }

    public String getNpwp_ayah_pp() {
        return npwp_ayah_pp;
    }

    public void setNpwp_ayah_pp(String npwp_ayah_pp) {
        this.npwp_ayah_pp = npwp_ayah_pp;
    }

    public String getPenghasilan_ayah_pp() {
        return penghasilan_ayah_pp;
    }

    public void setPenghasilan_ayah_pp(String penghasilan_ayah_pp) {
        this.penghasilan_ayah_pp = penghasilan_ayah_pp;
    }

    public String getNm_ibu_pp() {
        return nm_ibu_pp;
    }

    public void setNm_ibu_pp(String nm_ibu_pp) {
        this.nm_ibu_pp = nm_ibu_pp;
    }

    public String getTtl_ibu_pp() {
        return ttl_ibu_pp;
    }

    public void setTtl_ibu_pp(String ttl_ibu_pp) {
        this.ttl_ibu_pp = ttl_ibu_pp;
    }

    public String getUsia_ibu_pp() {
        return usia_ibu_pp;
    }

    public void setUsia_ibu_pp(String usia_ibu_pp) {
        this.usia_ibu_pp = usia_ibu_pp;
    }

    public String getPekerjaan_ibu_pp() {
        return pekerjaan_ibu_pp;
    }

    public void setPekerjaan_ibu_pp(String pekerjaan_ibu_pp) {
        this.pekerjaan_ibu_pp = pekerjaan_ibu_pp;
    }

    public String getJabatan_ibu_pp() {
        return jabatan_ibu_pp;
    }

    public void setJabatan_ibu_pp(String jabatan_ibu_pp) {
        this.jabatan_ibu_pp = jabatan_ibu_pp;
    }

    public String getPerusahaan_ibu_pp() {
        return perusahaan_ibu_pp;
    }

    public void setPerusahaan_ibu_pp(String perusahaan_ibu_pp) {
        this.perusahaan_ibu_pp = perusahaan_ibu_pp;
    }

    public String getBidusaha_ibu_pp() {
        return bidusaha_ibu_pp;
    }

    public void setBidusaha_ibu_pp(String bidusaha_ibu_pp) {
        this.bidusaha_ibu_pp = bidusaha_ibu_pp;
    }

    public String getNpwp_ibu_pp() {
        return npwp_ibu_pp;
    }

    public void setNpwp_ibu_pp(String npwp_ibu_pp) {
        this.npwp_ibu_pp = npwp_ibu_pp;
    }

    public String getPenghasilan_ibu_pp() {
        return penghasilan_ibu_pp;
    }

    public void setPenghasilan_ibu_pp(String penghasilan_ibu_pp) {
        this.penghasilan_ibu_pp = penghasilan_ibu_pp;
    }

    public String getPendapatan_pp() {
        return pendapatan_pp;
    }

    public void setPendapatan_pp(String pendapatan_pp) {
        this.pendapatan_pp = pendapatan_pp;
    }

    public int getHubungancp_pp() {
        return hubungancp_pp;
    }

    public void setHubungancp_pp(int hubungancp_pp) {
        this.hubungancp_pp = hubungancp_pp;
    }

    public String getNo_ciff_pp() {
        return no_ciff_pp;
    }

    public void setNo_ciff_pp(String no_ciff_pp) {
        this.no_ciff_pp = no_ciff_pp;
    }

    public String getD_gaji_pp() {
        return d_gaji_pp;
    }

    public void setD_gaji_pp(String d_gaji_pp) {
        this.d_gaji_pp = d_gaji_pp;
    }

    public String getD_tabungan_pp() {
        return d_tabungan_pp;
    }

    public void setD_tabungan_pp(String d_tabungan_pp) {
        this.d_tabungan_pp = d_tabungan_pp;
    }

    public String getD_warisan_pp() {
        return d_warisan_pp;
    }

    public void setD_warisan_pp(String d_warisan_pp) {
        this.d_warisan_pp = d_warisan_pp;
    }

    public String getD_hibah_pp() {
        return d_hibah_pp;
    }

    public void setD_hibah_pp(String d_hibah_pp) {
        this.d_hibah_pp = d_hibah_pp;
    }

    public String getD_lainnya_pp() {
        return d_lainnya_pp;
    }

    public void setD_lainnya_pp(String d_lainnya_pp) {
        this.d_lainnya_pp = d_lainnya_pp;
    }

    public String getEdit_d_lainnya_pp() {
        return edit_d_lainnya_pp;
    }

    public void setEdit_d_lainnya_pp(String edit_d_lainnya_pp) {
        this.edit_d_lainnya_pp = edit_d_lainnya_pp;
    }

    public String getT_proteksi_pp() {
        return t_proteksi_pp;
    }

    public void setT_proteksi_pp(String t_proteksi_pp) {
        this.t_proteksi_pp = t_proteksi_pp;
    }

    public String getT_inves_pp() {
        return t_inves_pp;
    }

    public void setT_inves_pp(String t_inves_pp) {
        this.t_inves_pp = t_inves_pp;
    }

    public String getT_lainnya_pp() {
        return t_lainnya_pp;
    }

    public void setT_lainnya_pp(String t_lainnya_pp) {
        this.t_lainnya_pp = t_lainnya_pp;
    }

    public String getEdit_t_lainnya() {
        return edit_t_lainnya;
    }

    public void setEdit_t_lainnya(String edit_t_lainnya) {
        this.edit_t_lainnya = edit_t_lainnya;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public String getPropinsi_pp() {
        return propinsi_pp;
    }

    public void setPropinsi_pp(String propinsi_pp) {
        this.propinsi_pp = propinsi_pp;
    }

    public String getPropinsi_kantor_pp() {
        return propinsi_kantor_pp;
    }

    public void setPropinsi_kantor_pp(String propinsi_kantor_pp) {
        this.propinsi_kantor_pp = propinsi_kantor_pp;
    }

    public String getPropinsi_tinggal_pp() {
        return propinsi_tinggal_pp;
    }

    public void setPropinsi_tinggal_pp(String propinsi_tinggal_pp) {
        this.propinsi_tinggal_pp = propinsi_tinggal_pp;
    }

    public String getKabupaten_pp() {
        return kabupaten_pp;
    }

    public void setKabupaten_pp(String kabupaten_pp) {
        this.kabupaten_pp = kabupaten_pp;
    }

    public String getKabupaten_kantor_pp() {
        return kabupaten_kantor_pp;
    }

    public void setKabupaten_kantor_pp(String kabupaten_kantor_pp) {
        this.kabupaten_kantor_pp = kabupaten_kantor_pp;
    }

    public String getKabupaten_tinggal_pp() {
        return kabupaten_tinggal_pp;
    }

    public void setKabupaten_tinggal_pp(String kabupaten_tinggal_pp) {
        this.kabupaten_tinggal_pp = kabupaten_tinggal_pp;
    }

    public String getKecamatan_pp() {
        return kecamatan_pp;
    }

    public void setKecamatan_pp(String kecamatan_pp) {
        this.kecamatan_pp = kecamatan_pp;
    }

    public String getKecamatan_kantor_pp() {
        return kecamatan_kantor_pp;
    }

    public void setKecamatan_kantor_pp(String kecamatan_kantor_pp) {
        this.kecamatan_kantor_pp = kecamatan_kantor_pp;
    }

    public String getKecamatan_tinggal_pp() {
        return kecamatan_tinggal_pp;
    }

    public void setKecamatan_tinggal_pp(String kecamatan_tinggal_pp) {
        this.kecamatan_tinggal_pp = kecamatan_tinggal_pp;
    }

    public String getKelurahan_pp() {
        return kelurahan_pp;
    }

    public void setKelurahan_pp(String kelurahan_pp) {
        this.kelurahan_pp = kelurahan_pp;
    }

    public String getKelurahan_kantor_pp() {
        return kelurahan_kantor_pp;
    }

    public void setKelurahan_kantor_pp(String kelurahan_kantor_pp) {
        this.kelurahan_kantor_pp = kelurahan_kantor_pp;
    }

    public String getKelurahan_tinggal_pp() {
        return kelurahan_tinggal_pp;
    }

    public void setKelurahan_tinggal_pp(String kelurahan_tinggal_pp) {
        this.kelurahan_tinggal_pp = kelurahan_tinggal_pp;
    }

    public static Creator<PemegangPolisModel> getCREATOR() {
        return CREATOR;
    }

    public PemegangPolisModel(String nama_pp, String gelar_pp, String nama_ibu_pp, int bukti_identitas_pp, String bukti_identitas_lain_pp, String no_identitas_pp, String tgl_berlaku_pp, int warga_negara_pp, String tempat_pp, String ttl_pp, String usia_pp, int jekel_pp, int status_pp, int agama_pp, String agama_lain_pp, int pendidikan_pp, String alamat_pp, String kota_pp, String kdpos_pp, String telp1_pp, String kdtelp1_pp, String telp2_pp, String kdtelp2_pp, String alamat_kantor_pp, String kota_kantor_pp, String kdpos_kantor_pp, String kdtelp1_kantor_pp, String telp1_kantor_pp, String kdtelp2_kantor_pp, String telp2_kantor_pp, String kdfax_kantor_pp, String fax_kantor_pp, String alamat_tinggal_pp, String kota_tinggal_pp, String kdpos_tinggal_pp, String kdtelp1_tinggal_pp, String telp1_tinggal_pp, String kdtelp2_tinggal_pp, String telp2_tinggal_pp, String kdfax_tinggal_pp, String fax_tinggal_pp, int tagihan_pp, String hp1_pp, String hp2_pp, String email_pp, String klasifikasi_pekerjaan_pp, String uraian_pekerjaan_pp, String jabatan_klasifikasi_pp, int hubungan_pp, int greencard_pp, String alias_pp, String nm_perusahaan_pp, String npwp_pp, String tagihan_rutin_pp, String krm_polis_pp, String suamirt_pp, String ttlsuami_rt_pp, String usiasuami_rt_pp, String pekerjaansuami_rt_pp, String jabatansuami_rt_pp, String perusahaansuami_rt_pp, String bidusaha_suamirt_pp, String npwp_suamirt_pp, String penghasilan_suamithn_pp, String ayah_pp, String ttl_ayah_pp, String usia_ayah_pp, String pekerjaan_ayah_pp, String jabatan_ayah_pp, String perusahaan_ayah_pp, String bidusaha_ayah_pp, String npwp_ayah_pp, String penghasilan_ayah_pp, String nm_ibu_pp, String ttl_ibu_pp, String usia_ibu_pp, String pekerjaan_ibu_pp, String jabatan_ibu_pp, String perusahaan_ibu_pp, String bidusaha_ibu_pp, String npwp_ibu_pp, String penghasilan_ibu_pp, String pendapatan_pp, int hubungancp_pp, String no_ciff_pp, String d_gaji_pp, String d_tabungan_pp, String d_warisan_pp, String d_hibah_pp, String d_lainnya_pp, String edit_d_lainnya_pp, String t_proteksi_pp, String t_inves_pp, String t_lainnya_pp, String edit_t_lainnya, Boolean validation, String propinsi_pp, String propinsi_kantor_pp, String propinsi_tinggal_pp, String kabupaten_pp, String kabupaten_kantor_pp, String kabupaten_tinggal_pp, String kecamatan_pp, String kecamatan_kantor_pp, String kecamatan_tinggal_pp, String kelurahan_pp, String kelurahan_kantor_pp, String kelurahan_tinggal_pp) {

        this.nama_pp = nama_pp;
        this.gelar_pp = gelar_pp;
        this.nama_ibu_pp = nama_ibu_pp;
        this.bukti_identitas_pp = bukti_identitas_pp;
        this.bukti_identitas_lain_pp = bukti_identitas_lain_pp;
        this.no_identitas_pp = no_identitas_pp;
        this.tgl_berlaku_pp = tgl_berlaku_pp;
        this.warga_negara_pp = warga_negara_pp;
        this.tempat_pp = tempat_pp;
        this.ttl_pp = ttl_pp;
        this.usia_pp = usia_pp;
        this.jekel_pp = jekel_pp;
        this.status_pp = status_pp;
        this.agama_pp = agama_pp;
        this.agama_lain_pp = agama_lain_pp;
        this.pendidikan_pp = pendidikan_pp;
        this.alamat_pp = alamat_pp;
        this.kota_pp = kota_pp;
        this.kdpos_pp = kdpos_pp;
        this.telp1_pp = telp1_pp;
        this.kdtelp1_pp = kdtelp1_pp;
        this.telp2_pp = telp2_pp;
        this.kdtelp2_pp = kdtelp2_pp;
        this.alamat_kantor_pp = alamat_kantor_pp;
        this.kota_kantor_pp = kota_kantor_pp;
        this.kdpos_kantor_pp = kdpos_kantor_pp;
        this.kdtelp1_kantor_pp = kdtelp1_kantor_pp;
        this.telp1_kantor_pp = telp1_kantor_pp;
        this.kdtelp2_kantor_pp = kdtelp2_kantor_pp;
        this.telp2_kantor_pp = telp2_kantor_pp;
        this.kdfax_kantor_pp = kdfax_kantor_pp;
        this.fax_kantor_pp = fax_kantor_pp;
        this.alamat_tinggal_pp = alamat_tinggal_pp;
        this.kota_tinggal_pp = kota_tinggal_pp;
        this.kdpos_tinggal_pp = kdpos_tinggal_pp;
        this.kdtelp1_tinggal_pp = kdtelp1_tinggal_pp;
        this.telp1_tinggal_pp = telp1_tinggal_pp;
        this.kdtelp2_tinggal_pp = kdtelp2_tinggal_pp;
        this.telp2_tinggal_pp = telp2_tinggal_pp;
        this.kdfax_tinggal_pp = kdfax_tinggal_pp;
        this.fax_tinggal_pp = fax_tinggal_pp;
        this.tagihan_pp = tagihan_pp;
        this.hp1_pp = hp1_pp;
        this.hp2_pp = hp2_pp;
        this.email_pp = email_pp;
        this.klasifikasi_pekerjaan_pp = klasifikasi_pekerjaan_pp;
        this.uraian_pekerjaan_pp = uraian_pekerjaan_pp;
        this.jabatan_klasifikasi_pp = jabatan_klasifikasi_pp;
        this.hubungan_pp = hubungan_pp;
        this.greencard_pp = greencard_pp;
        this.alias_pp = alias_pp;
        this.nm_perusahaan_pp = nm_perusahaan_pp;
        this.npwp_pp = npwp_pp;
        this.tagihan_rutin_pp = tagihan_rutin_pp;
        this.krm_polis_pp = krm_polis_pp;
        this.suamirt_pp = suamirt_pp;
        this.ttlsuami_rt_pp = ttlsuami_rt_pp;
        this.usiasuami_rt_pp = usiasuami_rt_pp;
        this.pekerjaansuami_rt_pp = pekerjaansuami_rt_pp;
        this.jabatansuami_rt_pp = jabatansuami_rt_pp;
        this.perusahaansuami_rt_pp = perusahaansuami_rt_pp;
        this.bidusaha_suamirt_pp = bidusaha_suamirt_pp;
        this.npwp_suamirt_pp = npwp_suamirt_pp;
        this.penghasilan_suamithn_pp = penghasilan_suamithn_pp;
        this.ayah_pp = ayah_pp;
        this.ttl_ayah_pp = ttl_ayah_pp;
        this.usia_ayah_pp = usia_ayah_pp;
        this.pekerjaan_ayah_pp = pekerjaan_ayah_pp;
        this.jabatan_ayah_pp = jabatan_ayah_pp;
        this.perusahaan_ayah_pp = perusahaan_ayah_pp;
        this.bidusaha_ayah_pp = bidusaha_ayah_pp;
        this.npwp_ayah_pp = npwp_ayah_pp;
        this.penghasilan_ayah_pp = penghasilan_ayah_pp;
        this.nm_ibu_pp = nm_ibu_pp;
        this.ttl_ibu_pp = ttl_ibu_pp;
        this.usia_ibu_pp = usia_ibu_pp;
        this.pekerjaan_ibu_pp = pekerjaan_ibu_pp;
        this.jabatan_ibu_pp = jabatan_ibu_pp;
        this.perusahaan_ibu_pp = perusahaan_ibu_pp;
        this.bidusaha_ibu_pp = bidusaha_ibu_pp;
        this.npwp_ibu_pp = npwp_ibu_pp;
        this.penghasilan_ibu_pp = penghasilan_ibu_pp;
        this.pendapatan_pp = pendapatan_pp;
        this.hubungancp_pp = hubungancp_pp;
        this.no_ciff_pp = no_ciff_pp;
        this.d_gaji_pp = d_gaji_pp;
        this.d_tabungan_pp = d_tabungan_pp;
        this.d_warisan_pp = d_warisan_pp;
        this.d_hibah_pp = d_hibah_pp;
        this.d_lainnya_pp = d_lainnya_pp;
        this.edit_d_lainnya_pp = edit_d_lainnya_pp;
        this.t_proteksi_pp = t_proteksi_pp;
        this.t_inves_pp = t_inves_pp;
        this.t_lainnya_pp = t_lainnya_pp;
        this.edit_t_lainnya = edit_t_lainnya;
        this.validation = validation;
        this.propinsi_pp = propinsi_pp;
        this.propinsi_kantor_pp = propinsi_kantor_pp;
        this.propinsi_tinggal_pp = propinsi_tinggal_pp;
        this.kabupaten_pp = kabupaten_pp;
        this.kabupaten_kantor_pp = kabupaten_kantor_pp;
        this.kabupaten_tinggal_pp = kabupaten_tinggal_pp;
        this.kecamatan_pp = kecamatan_pp;
        this.kecamatan_kantor_pp = kecamatan_kantor_pp;
        this.kecamatan_tinggal_pp = kecamatan_tinggal_pp;
        this.kelurahan_pp = kelurahan_pp;
        this.kelurahan_kantor_pp = kelurahan_kantor_pp;
        this.kelurahan_tinggal_pp = kelurahan_tinggal_pp;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nama_pp);
        dest.writeString(gelar_pp);
        dest.writeString(nama_ibu_pp);
        dest.writeInt(bukti_identitas_pp);
        dest.writeString(bukti_identitas_lain_pp);
        dest.writeString(no_identitas_pp);
        dest.writeString(tgl_berlaku_pp);
        dest.writeInt(warga_negara_pp);
        dest.writeString(tempat_pp);
        dest.writeString(ttl_pp);
        dest.writeString(usia_pp);
        dest.writeInt(jekel_pp);
        dest.writeInt(status_pp);
        dest.writeInt(agama_pp);
        dest.writeString(agama_lain_pp);
        dest.writeInt(pendidikan_pp);
        dest.writeString(alamat_pp);
        dest.writeString(kota_pp);
        dest.writeString(kdpos_pp);
        dest.writeString(telp1_pp);
        dest.writeString(kdtelp1_pp);
        dest.writeString(telp2_pp);
        dest.writeString(kdtelp2_pp);
        dest.writeString(alamat_kantor_pp);
        dest.writeString(kota_kantor_pp);
        dest.writeString(kdpos_kantor_pp);
        dest.writeString(kdtelp1_kantor_pp);
        dest.writeString(telp1_kantor_pp);
        dest.writeString(kdtelp2_kantor_pp);
        dest.writeString(telp2_kantor_pp);
        dest.writeString(kdfax_kantor_pp);
        dest.writeString(fax_kantor_pp);
        dest.writeString(alamat_tinggal_pp);
        dest.writeString(kota_tinggal_pp);
        dest.writeString(kdpos_tinggal_pp);
        dest.writeString(kdtelp1_tinggal_pp);
        dest.writeString(telp1_tinggal_pp);
        dest.writeString(kdtelp2_tinggal_pp);
        dest.writeString(telp2_tinggal_pp);
        dest.writeString(kdfax_tinggal_pp);
        dest.writeString(fax_tinggal_pp);
        dest.writeInt(tagihan_pp);
        dest.writeString(hp1_pp);
        dest.writeString(hp2_pp);
        dest.writeString(email_pp);
        dest.writeString(klasifikasi_pekerjaan_pp);
        dest.writeString(uraian_pekerjaan_pp);
        dest.writeString(jabatan_klasifikasi_pp);
        dest.writeInt(hubungan_pp);
        dest.writeInt(greencard_pp);
        dest.writeString(alias_pp);
        dest.writeString(nm_perusahaan_pp);
        dest.writeString(npwp_pp);
        dest.writeString(tagihan_rutin_pp);
        dest.writeString(krm_polis_pp);
        dest.writeString(suamirt_pp);
        dest.writeString(ttlsuami_rt_pp);
        dest.writeString(usiasuami_rt_pp);
        dest.writeString(pekerjaansuami_rt_pp);
        dest.writeString(jabatansuami_rt_pp);
        dest.writeString(perusahaansuami_rt_pp);
        dest.writeString(bidusaha_suamirt_pp);
        dest.writeString(npwp_suamirt_pp);
        dest.writeString(penghasilan_suamithn_pp);
        dest.writeString(ayah_pp);
        dest.writeString(ttl_ayah_pp);
        dest.writeString(usia_ayah_pp);
        dest.writeString(pekerjaan_ayah_pp);
        dest.writeString(jabatan_ayah_pp);
        dest.writeString(perusahaan_ayah_pp);
        dest.writeString(bidusaha_ayah_pp);
        dest.writeString(npwp_ayah_pp);
        dest.writeString(penghasilan_ayah_pp);
        dest.writeString(nm_ibu_pp);
        dest.writeString(ttl_ibu_pp);
        dest.writeString(usia_ibu_pp);
        dest.writeString(pekerjaan_ibu_pp);
        dest.writeString(jabatan_ibu_pp);
        dest.writeString(perusahaan_ibu_pp);
        dest.writeString(bidusaha_ibu_pp);
        dest.writeString(npwp_ibu_pp);
        dest.writeString(penghasilan_ibu_pp);
        dest.writeString(pendapatan_pp);
        dest.writeInt(hubungancp_pp);
        dest.writeString(no_ciff_pp);
        dest.writeString(d_gaji_pp);
        dest.writeString(d_tabungan_pp);
        dest.writeString(d_warisan_pp);
        dest.writeString(d_hibah_pp);
        dest.writeString(d_lainnya_pp);
        dest.writeString(edit_d_lainnya_pp);
        dest.writeString(t_proteksi_pp);
        dest.writeString(t_inves_pp);
        dest.writeString(t_lainnya_pp);
        dest.writeString(edit_t_lainnya);
        dest.writeByte((byte) (validation == null ? 0 : validation ? 1 : 2));
        dest.writeString(propinsi_pp);
        dest.writeString(propinsi_kantor_pp);
        dest.writeString(propinsi_tinggal_pp);
        dest.writeString(kabupaten_pp);
        dest.writeString(kabupaten_kantor_pp);
        dest.writeString(kabupaten_tinggal_pp);
        dest.writeString(kecamatan_pp);
        dest.writeString(kecamatan_kantor_pp);
        dest.writeString(kecamatan_tinggal_pp);
        dest.writeString(kelurahan_pp);
        dest.writeString(kelurahan_kantor_pp);
        dest.writeString(kelurahan_tinggal_pp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PemegangPolisModel> CREATOR = new Creator<PemegangPolisModel>() {
        @Override
        public PemegangPolisModel createFromParcel(Parcel in) {
            return new PemegangPolisModel(in);
        }

        @Override
        public PemegangPolisModel[] newArray(int size) {
            return new PemegangPolisModel[size];
        }
    };
}