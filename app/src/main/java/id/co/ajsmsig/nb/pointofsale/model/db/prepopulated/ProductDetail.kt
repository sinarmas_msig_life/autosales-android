package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

/**
 * Created by andreyyoshuamanik on 02/03/18.
 */

class ProductDetail(
        var text: String?,
        var url: String?,
        var imageName: String = "product_detail_doc.png",
        var viewButtonImageName: String = "product_detail_view.png",
        var downloadButtonImageName: String = "product_detail_download.png",
        var eventName: String = ""
)