package id.co.ajsmsig.nb.pointofsale.ui.fragment


import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosFragmentAlertDialogBinding


/**
 * A simple [Fragment] subclass.
 */
class AlertDialogFragment : DialogFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val dataBinding = DataBindingUtil.inflate<PosFragmentAlertDialogBinding>(inflater, R.layout.pos_fragment_alert_dialog, container, false)
        dataBinding.vm = this


        return dataBinding.root
    }

    fun accept() {
        listener(true)
        dismiss()
    }

    fun reject() {
        listener(false)
        dismiss()
    }

    lateinit var listener: (Boolean) -> Unit

    override fun onResume() {
        super.onResume()

        val window = dialog.window
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }
    companion object {
        fun create(listener: (Boolean) -> Unit): AlertDialogFragment {
            val fragment = AlertDialogFragment()
            fragment.listener = listener
            return fragment
        }
    }

}// Required empty public constructor
