package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.RecommendationSummaryField


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface RecommendationSummaryFieldDao {

    @Query("select RecommendationSummaryField.id, RecommendationSummaryField.text, RecommendationSummaryField.inputSourceName from RecommendationSummaryField " +
            "inner join RecommendationSummaryFieldToPageOption on RecommendationSummaryField.id = RecommendationSummaryFieldToPageOption.recommendationSummaryFieldId " +
            "where RecommendationSummaryFieldToPageOption.pageOptionId = :riskProfileId")
    fun loadAllRecommendationSummaryFieldsWith(riskProfileId: Int?): LiveData<List<RecommendationSummaryField>>

    @Query("select RecommendationSummaryField.id, RecommendationSummaryField.text, RecommendationSummaryField.inputSourceName from RecommendationSummaryField " +
            "inner join RecommendationSummaryFieldToPageOption on RecommendationSummaryField.id = RecommendationSummaryFieldToPageOption.recommendationSummaryFieldId " +
            "where RecommendationSummaryFieldToPageOption.pageOptionId isnull")
    fun loadAllDefaultRecommendationSummaryFields(): LiveData<List<RecommendationSummaryField>>

}