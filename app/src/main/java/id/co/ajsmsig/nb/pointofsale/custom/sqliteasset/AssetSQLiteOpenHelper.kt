package id.co.ajsmsig.nb.pointofsale.custom.sqliteasset

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.db.SupportSQLiteOpenHelper
import android.content.Context
import android.database.DatabaseErrorHandler
import android.database.sqlite.SQLiteDatabase
import android.os.Build
import android.support.annotation.RequiresApi

internal class AssetSQLiteOpenHelper(context: Context, name: String,
                                     factory: SQLiteDatabase.CursorFactory?, version: Int?,
                                     errorHandler: DatabaseErrorHandler?,
                                     callback: SupportSQLiteOpenHelper.Callback) : SupportSQLiteOpenHelper {
    private val mDelegate: OpenHelper

    init {
        mDelegate = createDelegate(context, name, factory, version, errorHandler, callback)
    }

    private fun createDelegate(context: Context, name: String,
                               factory: SQLiteDatabase.CursorFactory?, version: Int?, errorHandler: DatabaseErrorHandler?,
                               callback: SupportSQLiteOpenHelper.Callback): OpenHelper {
        return object : OpenHelper(context, name, factory, version, errorHandler) {
            override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
                mWrappedDb = FrameworkSQLiteDatabase(sqLiteDatabase)
                callback.onCreate(mWrappedDb)
            }

            override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
                callback.onUpgrade(getWrappedDb(sqLiteDatabase), oldVersion, newVersion)
            }

            override fun onConfigure(db: SQLiteDatabase) {
                callback.onConfigure(getWrappedDb(db))
            }

            override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
                callback.onDowngrade(getWrappedDb(db), oldVersion, newVersion)
            }

            override fun onOpen(db: SQLiteDatabase) {
                callback.onOpen(getWrappedDb(db))
            }
        }
    }

    override fun getDatabaseName(): String {
        return mDelegate.databaseName
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    override fun setWriteAheadLoggingEnabled(enabled: Boolean) {
        mDelegate.setWriteAheadLoggingEnabled(enabled)
    }

    override fun getWritableDatabase(): SupportSQLiteDatabase {
        return mDelegate.writableSupportDatabase
    }

    override fun getReadableDatabase(): SupportSQLiteDatabase {
        return mDelegate.readableSupportDatabase
    }

    override fun close() {
        mDelegate.close()
    }

    internal abstract class OpenHelper(context: Context, name: String,
                                       factory: SQLiteDatabase.CursorFactory?, version: Int?,
                                       errorHandler: DatabaseErrorHandler?) : SQLiteAssetHelper(context, name, null, factory, version ?: 1, errorHandler) {

        var mWrappedDb: FrameworkSQLiteDatabase? = null

        val writableSupportDatabase: SupportSQLiteDatabase
            get() {
                val db = super.getWritableDatabase()
                return getWrappedDb(db)
            }

        val readableSupportDatabase: SupportSQLiteDatabase
            get() {
                val db = super.getReadableDatabase()
                return getWrappedDb(db)
            }

        fun getWrappedDb(sqLiteDatabase: SQLiteDatabase): FrameworkSQLiteDatabase {
            if (mWrappedDb == null) {
                mWrappedDb = FrameworkSQLiteDatabase(sqLiteDatabase)
            }
            return mWrappedDb as FrameworkSQLiteDatabase
        }

        @Synchronized
        override fun close() {
            super.close()
            mWrappedDb = null
        }
    }
}
