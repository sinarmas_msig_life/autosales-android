package id.co.ajsmsig.nb.pointofsale.utils

import android.app.IntentService
import android.app.Service
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.Context

import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasServiceInjector
import id.co.ajsmsig.nb.pointofsale.api.APIService
import id.co.ajsmsig.nb.pointofsale.api.ApiResponse
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Analytics
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository
import javax.inject.Inject


class AnalyticsService : IntentService("AnalyticsService") {

    @Inject
    lateinit var mainRepository: MainRepository

    @Inject
    lateinit var apiService: APIService

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }

    override fun onHandleIntent(intent: Intent?) {
        val unsentAnalytics = mainRepository.getAllUnsentAnalytics()
        unsentAnalytics.observeForever(object : Observer<List<Analytics>> {
            override fun onChanged(list: List<Analytics>?) {
                list ?: return

                val sendAnalytics = apiService.sendAnalytics(list)
                sendAnalytics.observeForever(object : Observer<ApiResponse<String>> {
                    override fun onChanged(response: ApiResponse<String>?) {

                        if (response?.isSuccessful == true) {
                            list.forEach { it.isSent = true }
                            mainRepository.update(list)
                            sendAnalytics.removeObserver(this)
                        }
                    }
                })

                unsentAnalytics.removeObserver(this)
            }

        })
    }


    companion object {

        @JvmStatic
        fun startUploading(context: Context) {
            val intent = Intent(context, AnalyticsService::class.java)
            context.startService(intent)
        }
    }
}
