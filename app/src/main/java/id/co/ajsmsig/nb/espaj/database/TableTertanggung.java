package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.TertanggungModel;

/**
 * Created by Bernard on 05/09/2017.
 */

public class TableTertanggung {
    private Context context;

    public TableTertanggung(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(EspajModel me){
        ContentValues cv = new ContentValues();
        cv.put("NAMA_TT", me.getTertanggungModel().getNama_tt());
        cv.put("GELAR_TT", me.getTertanggungModel().getGelar_tt());
        cv.put("NAMA_IBU_TT", me.getTertanggungModel().getNama_ibu_tt());
        cv.put("BUKTI_IDENTITAS_TT", me.getTertanggungModel().getBukti_identitas_tt());
        cv.put("NO_IDENTITAS_TT", me.getTertanggungModel().getNo_identitas_tt());
        cv.put("TGL_BERLAKU_TT", me.getTertanggungModel().getTgl_berlaku_tt());
        cv.put("WARGA_NEGARA_TT", me.getTertanggungModel().getWarga_negara_tt());
        cv.put("TEMPAT_TT", me.getTertanggungModel().getTempat_tt());
        cv.put("TTL_TT", me.getTertanggungModel().getTtl_tt());
        cv.put("USIA_TT", me.getTertanggungModel().getUsia_tt());
        cv.put("JEKEL_TT", me.getTertanggungModel().getJekel_tt());
        cv.put("STATUS_TT", me.getTertanggungModel().getStatus_tt());
        cv.put("AGAMA_TT", me.getTertanggungModel().getAgama_tt());
        cv.put("AGAMA_LAIN_TT", me.getTertanggungModel().getAgama_lain_tt());
        cv.put("PENDIDIKAN_TT", me.getTertanggungModel().getPendidikan_tt());
        cv.put("ALAMAT_TT", me.getTertanggungModel().getAlamat_tt());
        cv.put("KOTA_TT", me.getTertanggungModel().getKota_tt());
        cv.put("KDPOS_TT", me.getTertanggungModel().getKdpos_tt());
        cv.put("KDTELP1_TT", me.getTertanggungModel().getKdtelp1_tt());
        cv.put("TELP1_TT", me.getTertanggungModel().getTelp1_tt());
        cv.put("KDTELP2_TT", me.getTertanggungModel().getKdtelp2_tt());
        cv.put("TELP2_TT", me.getTertanggungModel().getTelp2_tt());
        cv.put("ALAMAT_KANTOR_TT", me.getTertanggungModel().getAlamat_kantor_tt());
        cv.put("KOTA_KANTOR_TT", me.getTertanggungModel().getKota_kantor_tt());
        cv.put("KDPOS_KANTOR_TT", me.getTertanggungModel().getKdpos_kantor_tt());
        cv.put("KDTELP1_KANTOR_TT", me.getTertanggungModel().getKdtelp1_kantor_tt());
        cv.put("TELP1_KANTOR_TT", me.getTertanggungModel().getTelp1_kantor_tt());
        cv.put("KDTELP2_KANTOR_TT", me.getTertanggungModel().getKdtelp2_kantor_tt());
        cv.put("TELP2_KANTOR_TT", me.getTertanggungModel().getTelp2_kantor_tt());
        cv.put("FAX_KANTOR_TT", me.getTertanggungModel().getFax_kantor_tt());
        cv.put("ALAMAT_TINGGAL_TT", me.getTertanggungModel().getAlamat_tinggal_tt());
        cv.put("KOTA_TINGGAL_TT", me.getTertanggungModel().getKota_tinggal_tt());
        cv.put("KDPOS_TINGGAL_TT", me.getTertanggungModel().getKdpos_tinggal_tt());
        cv.put("KDTELP1_TINGGAL_TT", me.getTertanggungModel().getKdtelp1_tinggal_tt());
        cv.put("TELP1_TINGGAL_TT", me.getTertanggungModel().getTelp1_tinggal_tt());
        cv.put("KDTELP2_TINGGAL_TT", me.getTertanggungModel().getKdtelp2_tinggal_tt());
        cv.put("TELP2_TINGGAL_TT", me.getTertanggungModel().getTelp2_tinggal_tt());
        cv.put("KDFAX_TINGGAL_TT", me.getTertanggungModel().getKdfax_tinggal_tt());
        cv.put("FAX_TINGGAL_TT", me.getTertanggungModel().getFax_tinggal_tt());
        cv.put("HP1_TT", me.getTertanggungModel().getHp1_tt());
        cv.put("HP2_TT", me.getTertanggungModel().getHp2_tt());
        cv.put("EMAIL_TT", me.getTertanggungModel().getEmail_tt());
        cv.put("PENGHASILAN_THN_TT", me.getTertanggungModel().getPenghasilan_thn_tt());
        cv.put("KLASIFIKASI_PEKERJAAN_TT", me.getTertanggungModel().getKlasifikasi_pekerjaan_tt());
        cv.put("URAIAN_PEKERJAAN_TT", me.getTertanggungModel().getUraian_pekerjaan_tt());
        cv.put("JABATAN_KLASIFIKASI_TT", me.getTertanggungModel().getJabatan_klasifikasi_tt());
        cv.put("GREENCARD_TT", me.getTertanggungModel().getGreencard_tt());
        cv.put("ALIAS_TT", me.getTertanggungModel().getAlias_tt());
        cv.put("NM_PERUSAHAAN_TT", me.getTertanggungModel().getNm_perusahaan_tt());
        cv.put("NPWP_TT", me.getTertanggungModel().getNpwp_tt());
        cv.put("DANA_GAJI_TT", me.getTertanggungModel().getDana_gaji_tt());
        cv.put("DANA_TABUNGAN_TT", me.getTertanggungModel().getDana_tabungan_tt());
        cv.put("DANA_WARISAN_TT", me.getTertanggungModel().getDana_warisan_tt());
        cv.put("DANA_HIBAH_TT", me.getTertanggungModel().getDana_hibah_tt());
        cv.put("DANA_LAINNYA_TT", me.getTertanggungModel().getDana_lainnya_tt());
        cv.put("TUJUAN_PROTEKSI_TT", me.getTertanggungModel().getTujuan_proteksi_tt());
        cv.put("TUJUAN_INVES_TT", me.getTertanggungModel().getTujuan_inves_tt());
        cv.put("TUJUAN_LAINNYA_TT", me.getTertanggungModel().getTujuan_lainnya_tt());
        cv.put("PROPINSI_TT", me.getTertanggungModel().getPropinsi_tt());
        cv.put("PROPINSI_KANTOR_TT", me.getTertanggungModel().getPropinsi_kantor_tt());
        cv.put("PROPINSI_TINGGAL_TT", me.getTertanggungModel().getPropinsi_tinggal_tt());
        cv.put("KABUPATEN_TT", me.getTertanggungModel().getKabupaten_tt());
        cv.put("KABUPATEN_KANTOR_TT", me.getTertanggungModel().getKabupaten_kantor_tt());
        cv.put("KABUPATEN_TINGGAL_TT", me.getTertanggungModel().getKabupaten_tinggal_tt());
        cv.put("KECAMATAN_TT", me.getTertanggungModel().getKecamatan_tt());
        cv.put("KECAMATAN_KANTOR_TT", me.getTertanggungModel().getKecamatan_kantor_tt());
        cv.put("KECAMATAN_TINGGAL_TT", me.getTertanggungModel().getKecamatan_tinggal_tt());
        cv.put("KELURAHAN_TT", me.getTertanggungModel().getKelurahan_tt());
        cv.put("KELURAHAN_KANTOR_TT", me.getTertanggungModel().getKelurahan_kantor_tt());
        cv.put("KELURAHAN_TINGGAL_TT", me.getTertanggungModel().getKelurahan_tinggal_tt());
        cv.put("VALIDATION", me.getTertanggungModel().getValidation());

        return cv;
    }

    public TertanggungModel getObject(Cursor cursor){
        TertanggungModel tertanggungModel = new TertanggungModel();
        if (cursor != null && cursor.getCount()>0){
            cursor.moveToFirst();
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NAMA_TT"))){
                tertanggungModel.setNama_tt(cursor.getString(cursor.getColumnIndexOrThrow("NAMA_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("GELAR_TT"))){
                tertanggungModel.setGelar_tt(cursor.getString(cursor.getColumnIndexOrThrow("GELAR_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NAMA_IBU_TT"))){
                tertanggungModel.setNama_ibu_tt(cursor.getString(cursor.getColumnIndexOrThrow("NAMA_IBU_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("BUKTI_IDENTITAS_TT"))){
                tertanggungModel.setBukti_identitas_tt(cursor.getInt(cursor.getColumnIndexOrThrow("BUKTI_IDENTITAS_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NO_IDENTITAS_TT"))){
                tertanggungModel.setNo_identitas_tt(cursor.getString(cursor.getColumnIndexOrThrow("NO_IDENTITAS_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("TGL_BERLAKU_TT"))){
                tertanggungModel.setTgl_berlaku_tt(cursor.getString(cursor.getColumnIndexOrThrow("TGL_BERLAKU_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("WARGA_NEGARA_TT"))){
                tertanggungModel.setWarga_negara_tt(cursor.getInt(cursor.getColumnIndexOrThrow("WARGA_NEGARA_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("TEMPAT_TT"))){
                tertanggungModel.setTempat_tt(cursor.getString(cursor.getColumnIndexOrThrow("TEMPAT_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("TTL_TT"))){
                tertanggungModel.setTtl_tt( cursor.getString(cursor.getColumnIndexOrThrow("TTL_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("USIA_TT"))){
                tertanggungModel.setUsia_tt(cursor.getString(cursor.getColumnIndexOrThrow("USIA_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("JEKEL_TT"))){
                tertanggungModel.setJekel_tt(cursor.getInt(cursor.getColumnIndexOrThrow("JEKEL_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("STATUS_TT"))){
                tertanggungModel.setStatus_tt(cursor.getInt(cursor.getColumnIndexOrThrow("STATUS_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AGAMA_TT"))){
                tertanggungModel.setAgama_tt(cursor.getInt(cursor.getColumnIndexOrThrow("AGAMA_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AGAMA_LAIN_TT"))){
                tertanggungModel.setAgama_lain_tt(cursor.getString(cursor.getColumnIndexOrThrow("AGAMA_LAIN_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PENDIDIKAN_TT"))){
                tertanggungModel.setPendidikan_tt(cursor.getInt(cursor.getColumnIndexOrThrow("PENDIDIKAN_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ALAMAT_TT"))){
                tertanggungModel.setAlamat_tt(cursor.getString(cursor.getColumnIndexOrThrow("ALAMAT_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KOTA_TT"))){
                tertanggungModel.setKota_tt(cursor.getString(cursor.getColumnIndexOrThrow("KOTA_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KDPOS_TT"))){
                tertanggungModel.setKdpos_tt(cursor.getString(cursor.getColumnIndexOrThrow("KDPOS_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KDTELP1_TT"))){
                tertanggungModel.setKdtelp1_tt(cursor.getString(cursor.getColumnIndexOrThrow("KDTELP1_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("TELP1_TT"))){
                tertanggungModel.setTelp1_tt(cursor.getString(cursor.getColumnIndexOrThrow("TELP1_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KDTELP2_TT"))){
                tertanggungModel.setKdtelp2_tt(cursor.getString(cursor.getColumnIndexOrThrow("KDTELP2_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("TELP2_TT"))){
                tertanggungModel.setTelp2_tt(cursor.getString(cursor.getColumnIndexOrThrow("TELP2_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ALAMAT_KANTOR_TT"))){
                tertanggungModel.setAlamat_kantor_tt(cursor.getString(cursor.getColumnIndexOrThrow("ALAMAT_KANTOR_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KOTA_KANTOR_TT"))){
                tertanggungModel.setKota_kantor_tt(cursor.getString(cursor.getColumnIndexOrThrow("KOTA_KANTOR_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KDPOS_KANTOR_TT"))){
                tertanggungModel.setKdpos_kantor_tt(cursor.getString(cursor.getColumnIndexOrThrow("KDPOS_KANTOR_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KDTELP1_KANTOR_TT"))){
                tertanggungModel.setKdtelp1_kantor_tt(cursor.getString(cursor.getColumnIndexOrThrow("KDTELP1_KANTOR_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("TELP1_KANTOR_TT"))){
                tertanggungModel.setTelp1_kantor_tt(cursor.getString(cursor.getColumnIndexOrThrow("TELP1_KANTOR_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KDTELP2_KANTOR_TT"))){
                tertanggungModel.setKdtelp2_kantor_tt(cursor.getString(cursor.getColumnIndexOrThrow("KDTELP2_KANTOR_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("TELP2_KANTOR_TT"))){
                tertanggungModel.setTelp2_kantor_tt(cursor.getString(cursor.getColumnIndexOrThrow("TELP2_KANTOR_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("FAX_KANTOR_TT"))){
                tertanggungModel.setFax_kantor_tt(cursor.getString(cursor.getColumnIndexOrThrow("FAX_KANTOR_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ALAMAT_TINGGAL_TT"))){
                tertanggungModel.setAlamat_tinggal_tt(cursor.getString(cursor.getColumnIndexOrThrow("ALAMAT_TINGGAL_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KOTA_TINGGAL_TT"))){
                tertanggungModel.setKota_tinggal_tt(cursor.getString(cursor.getColumnIndexOrThrow("KOTA_TINGGAL_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KDPOS_TINGGAL_TT"))){
                tertanggungModel.setKdpos_tinggal_tt(cursor.getString(cursor.getColumnIndexOrThrow("KDPOS_TINGGAL_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KDTELP1_TINGGAL_TT"))){
                tertanggungModel.setKdtelp1_tinggal_tt(cursor.getString(cursor.getColumnIndexOrThrow("KDTELP1_TINGGAL_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("TELP1_TINGGAL_TT"))){
                tertanggungModel.setTelp1_tinggal_tt(cursor.getString(cursor.getColumnIndexOrThrow("TELP1_TINGGAL_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KDTELP2_TINGGAL_TT"))){
                tertanggungModel.setKdtelp2_tinggal_tt(cursor.getString(cursor.getColumnIndexOrThrow("KDTELP2_TINGGAL_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("TELP2_TINGGAL_TT"))){
                tertanggungModel.setTelp2_tinggal_tt(cursor.getString(cursor.getColumnIndexOrThrow("TELP2_TINGGAL_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KDFAX_TINGGAL_TT"))){
                tertanggungModel.setKdfax_tinggal_tt(cursor.getString(cursor.getColumnIndexOrThrow("KDFAX_TINGGAL_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("FAX_TINGGAL_TT"))){
                tertanggungModel.setFax_tinggal_tt(cursor.getString(cursor.getColumnIndexOrThrow("FAX_TINGGAL_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("HP1_TT"))){
                tertanggungModel.setHp1_tt(cursor.getString(cursor.getColumnIndexOrThrow("HP1_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("HP2_TT"))){
                tertanggungModel.setHp2_tt(cursor.getString(cursor.getColumnIndexOrThrow("HP2_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("EMAIL_TT"))){
                tertanggungModel.setEmail_tt(cursor.getString(cursor.getColumnIndexOrThrow("EMAIL_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PENGHASILAN_THN_TT"))){
                tertanggungModel.setPenghasilan_thn_tt(cursor.getString(cursor.getColumnIndexOrThrow("PENGHASILAN_THN_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KLASIFIKASI_PEKERJAAN_TT"))){
                tertanggungModel.setKlasifikasi_pekerjaan_tt(cursor.getString(cursor.getColumnIndexOrThrow("KLASIFIKASI_PEKERJAAN_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("URAIAN_PEKERJAAN_TT"))){
                tertanggungModel.setUraian_pekerjaan_tt(cursor.getString(cursor.getColumnIndexOrThrow("URAIAN_PEKERJAAN_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("JABATAN_KLASIFIKASI_TT"))){
                tertanggungModel.setJabatan_klasifikasi_tt(cursor.getString(cursor.getColumnIndexOrThrow("JABATAN_KLASIFIKASI_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("GREENCARD_TT"))){
                tertanggungModel.setGreencard_tt(cursor.getInt(cursor.getColumnIndexOrThrow("GREENCARD_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("ALIAS_TT"))){
                tertanggungModel.setAlias_tt(cursor.getString(cursor.getColumnIndexOrThrow("ALIAS_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NM_PERUSAHAAN_TT"))){
                tertanggungModel.setNm_perusahaan_tt(cursor.getString(cursor.getColumnIndexOrThrow("NM_PERUSAHAAN_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NPWP_TT"))){
                tertanggungModel.setNpwp_tt(cursor.getString(cursor.getColumnIndexOrThrow("NPWP_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("DANA_GAJI_TT"))){
                tertanggungModel.setDana_gaji_tt(cursor.getString(cursor.getColumnIndexOrThrow("DANA_GAJI_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("DANA_TABUNGAN_TT"))){
                tertanggungModel.setDana_tabungan_tt(cursor.getString(cursor.getColumnIndexOrThrow("DANA_TABUNGAN_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("DANA_WARISAN_TT"))){
                tertanggungModel.setDana_warisan_tt(cursor.getString(cursor.getColumnIndexOrThrow("DANA_WARISAN_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("DANA_HIBAH_TT"))){
                tertanggungModel.setDana_hibah_tt(cursor.getString(cursor.getColumnIndexOrThrow("DANA_HIBAH_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("DANA_LAINNYA_TT"))){
                tertanggungModel.setDana_lainnya_tt(cursor.getString(cursor.getColumnIndexOrThrow("DANA_LAINNYA_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("TUJUAN_PROTEKSI_TT"))){
                tertanggungModel.setTujuan_proteksi_tt(cursor.getString(cursor.getColumnIndexOrThrow("TUJUAN_PROTEKSI_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("TUJUAN_INVES_TT"))){
                tertanggungModel.setTujuan_inves_tt(cursor.getString(cursor.getColumnIndexOrThrow("TUJUAN_INVES_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("TUJUAN_LAINNYA_TT"))){
                tertanggungModel.setTujuan_lainnya_tt(cursor.getString(cursor.getColumnIndexOrThrow("TUJUAN_LAINNYA_TT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PROPINSI_TT"))){
                tertanggungModel.setPropinsi_tt(cursor.getString(cursor.getColumnIndexOrThrow("PROPINSI_TT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PROPINSI_KANTOR_TT"))){
                tertanggungModel.setPropinsi_kantor_tt(cursor.getString(cursor.getColumnIndexOrThrow("PROPINSI_KANTOR_TT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PROPINSI_TINGGAL_TT"))){
                tertanggungModel.setPropinsi_tinggal_tt(cursor.getString(cursor.getColumnIndexOrThrow("PROPINSI_TINGGAL_TT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KABUPATEN_TT"))){
                tertanggungModel.setKabupaten_tt(cursor.getString(cursor.getColumnIndexOrThrow("KABUPATEN_TT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KABUPATEN_KANTOR_TT"))){
                tertanggungModel.setKabupaten_kantor_tt(cursor.getString(cursor.getColumnIndexOrThrow("KABUPATEN_KANTOR_TT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KABUPATEN_TINGGAL_TT"))){
                tertanggungModel.setKabupaten_tinggal_tt(cursor.getString(cursor.getColumnIndexOrThrow("KABUPATEN_TINGGAL_TT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KECAMATAN_TT"))){
                tertanggungModel.setKecamatan_tt(cursor.getString(cursor.getColumnIndexOrThrow("KECAMATAN_TT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KECAMATAN_KANTOR_TT"))){
                tertanggungModel.setKecamatan_kantor_tt(cursor.getString(cursor.getColumnIndexOrThrow("KECAMATAN_KANTOR_TT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KECAMATAN_TINGGAL_TT"))){
                tertanggungModel.setKecamatan_tinggal_tt(cursor.getString(cursor.getColumnIndexOrThrow("KECAMATAN_TINGGAL_TT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KELURAHAN_TT"))){
                tertanggungModel.setKelurahan_tt(cursor.getString(cursor.getColumnIndexOrThrow("KELURAHAN_TT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KELURAHAN_KANTOR_TT"))){
                tertanggungModel.setKelurahan_kantor_tt(cursor.getString(cursor.getColumnIndexOrThrow("KELURAHAN_KANTOR_TT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KELURAHAN_TINGGAL_TT"))){
                tertanggungModel.setKelurahan_tinggal_tt(cursor.getString(cursor.getColumnIndexOrThrow("KELURAHAN_TINGGAL_TT")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("VALIDATION"))){
                int valid = cursor.getInt(cursor.getColumnIndexOrThrow("VALIDATION"));
//                boolean isValid = valid == 1 ? true : false;
                boolean val;
                val = valid == 1;
                tertanggungModel.setValidation(val);
            }
            cursor.close();
        }
        return tertanggungModel;
    }
}
