package id.co.ajsmsig.nb.crm.model.form;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by faiz_f on 12/04/2017.
 */

public class SimbakLinkAccToLeadModel implements Parcelable {
    private Long ATL_ID;
    private Long ATL_ACC_ID;
    private Long ATL_LEAD_ID;

    public SimbakLinkAccToLeadModel() {
    }


    protected SimbakLinkAccToLeadModel(Parcel in) {
        ATL_ID = (Long) in.readValue(Integer.class.getClassLoader());
        ATL_ACC_ID = (Long) in.readValue(Integer.class.getClassLoader());
        ATL_LEAD_ID = (Long) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<SimbakLinkAccToLeadModel> CREATOR = new Creator<SimbakLinkAccToLeadModel>() {
        @Override
        public SimbakLinkAccToLeadModel createFromParcel(Parcel in) {
            return new SimbakLinkAccToLeadModel(in);
        }

        @Override
        public SimbakLinkAccToLeadModel[] newArray(int size) {
            return new SimbakLinkAccToLeadModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ATL_ID);
        dest.writeValue(ATL_ACC_ID);
        dest.writeValue(ATL_LEAD_ID);
    }

    public Long getATL_ID() {
        return ATL_ID;
    }

    public void setATL_ID(Long ATL_ID) {
        this.ATL_ID = ATL_ID;
    }

    public Long getATL_ACC_ID() {
        return ATL_ACC_ID;
    }

    public void setATL_ACC_ID(Long ATL_ACC_ID) {
        this.ATL_ACC_ID = ATL_ACC_ID;
    }

    public Long getATL_LEAD_ID() {
        return ATL_LEAD_ID;
    }

    public void setATL_LEAD_ID(Long ATL_LEAD_ID) {
        this.ATL_LEAD_ID = ATL_LEAD_ID;
    }

}

