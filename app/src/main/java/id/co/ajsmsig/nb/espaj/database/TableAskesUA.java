package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAskesUA;

/**
 * Created by rizky_c on 30/09/2017.
 */

public class TableAskesUA {

    private Context context;

    public TableAskesUA(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(ModelAskesUA me, int counter,  String SPAJ_ID_TAB) {
        ContentValues cv = new ContentValues();
        cv.put("SPAJ_ID_TAB", SPAJ_ID_TAB);
        cv.put("COUNTER", counter);
        cv.put("PESERTA_ASKES", me.getPeserta_askes());
        cv.put("JEKEL_ASKES", me.getJekel_askes());
        cv.put("TTL_ASKES", me.getTtl_askes());
        cv.put("USIA_ASKES", me.getUsia_askes());
        cv.put("HUBUNGAN_ASKES", me.getHubungan_askes());
        cv.put("RIDER_ASKES", me.getRider_askes());
        cv.put("KODE_PRODUK_RIDER", me.getKode_produk_rider());
        cv.put("KODE_SUBPRODUK_RIDER", me.getKode_subproduk_rider());
        cv.put("TINGGI_ASKES", me.getTinggi_askes());
        cv.put("BERAT_ASKES", me.getBerat_askes());
        cv.put("WARGANEGARA_ASKES", me.getWarganegara_askes());
        cv.put("PEKERJAAN_ASKES", me.getPekerjaan_askes());

        return cv;
    }

    public ArrayList<ModelAskesUA> getObjectArray(Cursor cursor) {
        ArrayList<ModelAskesUA> mes = new ArrayList<ModelAskesUA>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ModelAskesUA me = new ModelAskesUA();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("COUNTER"))) {
                    me.setCounter(cursor.getInt(cursor.getColumnIndexOrThrow("COUNTER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("PESERTA_ASKES"))) {
                    me.setPeserta_askes(cursor.getString(cursor.getColumnIndexOrThrow("PESERTA_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("JEKEL_ASKES"))) {
                    me.setJekel_askes(cursor.getInt(cursor.getColumnIndexOrThrow("JEKEL_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("TTL_ASKES"))) {
                    me.setTtl_askes(cursor.getString(cursor.getColumnIndexOrThrow("TTL_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("USIA_ASKES"))) {
                    me.setUsia_askes(cursor.getInt(cursor.getColumnIndexOrThrow("USIA_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("HUBUNGAN_ASKES"))) {
                    me.setHubungan_askes(cursor.getInt(cursor.getColumnIndexOrThrow("HUBUNGAN_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("KODE_PRODUK_RIDER"))) {
                    me.setProduk_askes(cursor.getInt(cursor.getColumnIndexOrThrow("KODE_PRODUK_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("RIDER_ASKES"))) {
                    me.setRider_askes(cursor.getInt(cursor.getColumnIndexOrThrow("RIDER_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("KODE_PRODUK_RIDER"))) {
                    me.setKode_produk_rider(cursor.getInt(cursor.getColumnIndexOrThrow("KODE_PRODUK_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("KODE_SUBPRODUK_RIDER"))) {
                    me.setKode_subproduk_rider(cursor.getInt(cursor.getColumnIndexOrThrow("KODE_SUBPRODUK_RIDER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("TINGGI_ASKES"))) {
                    me.setTinggi_askes(cursor.getInt(cursor.getColumnIndexOrThrow("TINGGI_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("BERAT_ASKES"))) {
                    me.setBerat_askes(cursor.getInt(cursor.getColumnIndexOrThrow("BERAT_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("WARGANEGARA_ASKES"))) {
                    me.setWarganegara_askes(cursor.getInt(cursor.getColumnIndexOrThrow("WARGANEGARA_ASKES")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("PEKERJAAN_ASKES"))) {
                    me.setPekerjaan_askes(cursor.getString(cursor.getColumnIndexOrThrow("PEKERJAAN_ASKES")));
                }
                mes.add(me);
                cursor.moveToNext();
            }

            // always close the cursor
            cursor.close();
        }

        return mes;
    }
}
