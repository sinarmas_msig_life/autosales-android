package id.co.ajsmsig.nb.prop.method.calculateIlustration;

import android.content.Context;
import android.util.Log;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.method.util.ArrUtil;
import id.co.ajsmsig.nb.prop.method.util.CommonUtil;
import id.co.ajsmsig.nb.prop.method.util.FormatNumber;
import id.co.ajsmsig.nb.prop.method.util.MathUtil;
import id.co.ajsmsig.nb.prop.method.util.ProposalStringFormatter;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductRiderModel;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;
import id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_Istr_prop;
import id.co.ajsmsig.nb.prop.model.hardCode.Proposal;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.IllustrationResultVO;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.S_biaya;


public class IllustrationFormula134 extends IllustrationFormula {

    private P_ProposalModel proposalModel;
    private P_MstProposalProductModel productModel;
    private P_Select select;
    private Context context;
    private Prop_Model_Istr_prop istr_prop;
    private DropboxModel dCaraPembayaran = new DropboxModel();

    IllustrationFormula134(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public HashMap<String, Object> getIllustrationResult(String no_proposal) {
        IllustrationResultVO result = new IllustrationResultVO();
        ArrayList<LinkedHashMap<String, String>> mapList = new ArrayList<>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        ArrayList<HashMap<String, Object>> topupDrawList = getSelect().selectTopupAndWithdrawList(no_proposal);
        Integer defaultTopupDrawListSize = 50;
        String premiumTotal;
        String topup;
        String draw;

        int li_ke = 0, li_bagi = 1000, li_hal = 3;
        double[] ldec_bak ;
        double[] ldec_hasil_ppokok = new double[3 + 1];
        double[] ldec_hasil_ptu = new double[3 + 1];
        double[] ldec_hasil_wdraw = new double[3 + 1];
        double[] ldec_fph = {Proposal.DUMMY_ZERO, 0, 0, 0.9, 0.85, 0.8};
        double ldec_bawal = 100000;
        double[] ldec_man_non = new double[2 + 1];
        double ldec_bak_tu = 0.05;
        double ldec_bak_tut = 0.05;
        double ldec_akuisisi;
        double ldec_premi_invest;
        double[][] ldec_hasil_invest = new double[5 + 1][3 + 1];
        double[] ldec_tarik = { Proposal.DUMMY_ZERO, 0.04, 0.04, 0.03, 0.02, 0.01 };
        double ldec_wdraw;
        double[] ldec_premi_bulan = new double[12 + 1];
        double ldec_topup;
        double ldec_bass;
        double[][] ldec_bunga = new double[5 + 1][3 + 1];
        double[] ldec_bunga_avg = new double[3 + 1]; //= {0.06, 0.1, 0.08, 0.18, 0.12, 0.25} //{0.09, 0.06, 0.11, 0.15, 0.165, 0.25}  //fixed:0.09, 0.11, 0.15 (0.165); dynamic:0.08, 0.11, 0.18 (0.195)
        double ldec_fee = 0.020075;
        double ldec_coi = 0, ldec_mfc, ldec_sc, ldec_cost, ldec_ppokok, ldec_ptu, ldec_aph, ldec_ff, ldec_man_celaka, ldec_temp = 0; //, ldec_man[10+1] = {1, 1, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7}
        boolean[] lb_minus = { Proposal.DUMMY_FALSE, false, false, false };
        boolean lb_rider = false;
        String ls_sy = "", ls_temp; //ls_dpp = ' dari Premi Pokok', ls_sy = ''
        S_biaya lstr;
        double ldec_manfaat, ldec_premi_setahun = 0, ldec_coi24 = 0;
        ldec_mfc = 27500;
//        Prop_Model_Istr_prop istr_prop = null;


        Integer umur_pp = 0, umur_tt = 0, bisnis_id = 0, bisnis_no = 0, ins_per = 0, lscb_kali=0, lscb_id = 0, lamaBayar = 0, cuti_premi = 0;
        double premi = 0, premi_komb = 0, premi_pokok = 0, up = 0;
        String kondisi_layak_jual = null, lscb_pay_mode = null, lku_id = "00", userMsagId = "";
        HashMap<String, Object> dataProposal = getSelect().selectDataProposal(no_proposal);
        Log.d(TAG, "getIllustrationResult: " + dataProposal);
        if(!CommonUtil.isEmpty(dataProposal)) {
            bisnis_id = ((BigDecimal) dataProposal.get("LSBS_ID")).intValue();
            bisnis_no = ((BigDecimal) dataProposal.get("LSDBS_NUMBER")).intValue();
            umur_tt =  Integer.valueOf(dataProposal.get("UMUR_TT").toString());
            umur_pp =  Integer.valueOf(dataProposal.get("UMUR_PP").toString());
            premi = Double.valueOf(dataProposal.get("PREMI").toString());
            premi_komb = Double.valueOf(dataProposal.get("PREMI_KOMB").toString());
            premi_pokok = Double.valueOf(dataProposal.get("PREMI_POKOK").toString());
            up =  Double.valueOf(dataProposal.get("UP").toString());
            ins_per =  Integer.valueOf(dataProposal.get("THN_MASA_KONTRAK").toString());
            kondisi_layak_jual = (String) dataProposal.get("NAME_ELIGIBLE");
            lscb_kali = Integer.valueOf(dataProposal.get("LSCB_KALI").toString());
            lscb_pay_mode = dataProposal.get("LSCB_PAY_MODE").toString();
            lscb_id = Integer.valueOf(dataProposal.get("LSCB_ID").toString());
            lku_id = dataProposal.get("LKU_ID").toString();
            cuti_premi = Integer.valueOf(dataProposal.get("THN_CUTI_PREMI").toString());
            lamaBayar = Integer.valueOf(dataProposal.get("THN_LAMA_BAYAR").toString());
            userMsagId = dataProposal.get("MSAG_ID").toString();
        }

        if(Proposal.CUR_USD_CD.equals(lku_id)){
            ldec_mfc = 2.75;
            li_bagi = 1;
        }
        for( int i = 1; i <= 12; i++ )
        {
            ldec_premi_bulan[ i ] = 0;
            if( i == 1 ) ldec_premi_bulan[ i ] = premi;
            if( Proposal.PAY_MODE_CD_TRIWULANAN == lscb_id )
            {
                if( i == 4 || i == 7 || i == 10 ) ldec_premi_bulan[ i ] = premi;
            }
            else if( Proposal.PAY_MODE_CD_SEMESTERAN == lscb_id )
            {
                if( i == 7 ) ldec_premi_bulan[ i ] = premi;
            }
            else if( Proposal.PAY_MODE_CD_BULANAN == lscb_id )
            {
                ldec_premi_bulan[ i ] = premi;
            }
            ldec_premi_setahun += ldec_premi_bulan[ i ];
        }
        //
        for(int i = 1 ; i <= 3 ; i++){
            ldec_hasil_invest[1][i] = 0;
            ldec_hasil_ppokok[i] = 0;
            ldec_hasil_ptu[i] = 0;
            ldec_hasil_wdraw[i] = 0;
        }
        //
        ldec_manfaat = up;
        //    Biaya Akuisisi & asumsi penarikan (1 tarik, 1-nya tdk tarik)
        lstr = getBiaya(bisnis_id, bisnis_no, no_proposal);
        ldec_bak = lstr.bak;
        ldec_bunga = lstr.bunga;

        ldec_bunga_avg = getSelect().selectBungaAvg( no_proposal );

        double[] np = new double[4];
        double[] celaka = new double[4];

        int j;
        ldec_fee = 0;
        for( int i = 1; i <= ins_per; i++ )
        {
//          surrender charge
            ldec_sc = 0;
            ldec_wdraw = 0;
            ldec_topup = 0;

            ldec_coi = getLdec_coi(dataProposal, i);



//            TODO: nanti coba cek disini klo beda
//            lscb_id = Integer.valueOf(dataProposal.get("LSCB_ID").toString());
//            if (lscb_id == 0) {
//                int count = getSelect().getRiderCount(no_proposal);
//                Log.v("ridercount", String.valueOf(count));
//                int lsbsId = getSelect().RiderId(no_proposal);
//                for (int k = 1; k <= count; k++) {
//                    if (lsbsId != -1){
//                        ldec_temp = of_get_coi_134(k, i,no_proposal);
//                        Log.v("ridercountproses","triplex"+ i);
//                        ldec_coi += ldec_temp;
//                    }
//                }
//            }

            if(i <= ArrUtil.upperBound(lstr.topup)) ldec_topup = lstr.topup[i];
            if(i <= 5) ldec_sc = ldec_tarik[i];
            //
            ldec_cost = (ldec_coi + ldec_mfc);

            for(int k = 1 ; k <= 3; k++){
                if(i <= ArrUtil.upperBound(lstr.tarik)) ldec_wdraw = lstr.tarik[i];
                for(int li_bulan = 1 ; li_bulan <= 12 ; li_bulan++){
                    ldec_cost = (ldec_coi + ldec_mfc);
                    ldec_premi_invest = 0;
                    ldec_ppokok = 0;
                    ldec_ptu = 0;
                    ldec_ff = 0;
                    ldec_aph = 0;
                    if(i <= cuti_premi){
                        ldec_premi_invest = ldec_premi_bulan[li_bulan] * (premi_komb / 100);
                        ldec_premi_invest += ((ldec_premi_bulan[li_bulan] * (100 - premi_komb) / 100));
                        ldec_ppokok = ldec_premi_bulan[li_bulan] * (premi_komb / 100);
                        ldec_ptu = (ldec_premi_bulan[li_bulan] * (100 - premi_komb) / 100);
                    }

                    if(li_bulan == 1) ldec_ptu += ldec_topup;
                    // Em@il Vito@18/05/2016=> Rumusan SIMAS PRIMELINK & SMiLe AssetPro(Agency)(134-X5/X6)
                    if(i == 1 && li_bulan == 1){
                        ldec_hasil_ppokok[k] = FormatNumber.round(( ldec_ppokok - ldec_cost ) * (Math.pow((1 + ldec_bunga_avg[k]),( ( double ) 1/12))), 2) ;
                    }else{
                        ldec_hasil_ppokok[k] = FormatNumber.round(( ldec_hasil_ppokok[k] - ldec_cost ) * (Math.pow((1 + ldec_bunga_avg[k]),( ( double ) 1/12))), 2) ;
                    }
                    ldec_hasil_ptu[k] = (ldec_ptu + ldec_hasil_ptu[k]) * (Math.pow((1 + ldec_bunga_avg[k]),( ( double ) 1/12)));
                    if(k == 1)
                    {
                        ldec_hasil_invest[1][k] = ldec_hasil_ppokok[k] + ldec_hasil_ptu[k];
                    }
                }

                ldec_hasil_wdraw[k] += ldec_wdraw;
                ldec_hasil_invest[1][k] = ldec_hasil_ppokok[k] + ldec_hasil_ptu[k];
                Log.d(TAG, "getIllustrationResult: hasil_invest " + ldec_hasil_invest[1][k]);
                Log.d(TAG, "getIllustrationResult: hasil pokok " + ldec_hasil_ppokok[k]);
                Log.d(TAG, "getIllustrationResult: hasil ptu " + ldec_hasil_ptu[k]);
                ldec_hasil_invest[ 1 ][ k ] -= ( ldec_hasil_wdraw[k] * ( 1 + ldec_sc ) );


                if( ldec_hasil_invest[ 1 ][ k ] <= 0 &&  ( i <= 10 ) ){
                    lb_minus[k] = true;
                }
            }

            //j = cepr01030101Form.getInsuredAge() + i;
            j = umur_tt + i;

            if( i <= 23 || j == 55 || j == 65 || j == 75 || j == 80 || j == 100 )
            {
                for( int k = 1; k <= 3; k++ )
                {
                    np[ k ] = FormatNumber.round( ldec_hasil_invest[ 1 ][ k ] / li_bagi, 0 );
                    celaka[ k ] = FormatNumber.round( ( ldec_hasil_invest[ 1 ][ k ] + ldec_manfaat ) / li_bagi, 0 );
                }

                if( i <= cuti_premi )
                {
                    premiumTotal = ProposalStringFormatter.convertToStringWithoutCent( ldec_premi_setahun / li_bagi );
                }
                else
                {
                    premiumTotal = "";
                }


                if( i < defaultTopupDrawListSize )
                {
                    topup = "0";
                    draw = "0";

                    for(HashMap<String, Object> topupDraw : topupDrawList) {
                        Integer thn_ke = ((BigDecimal) topupDraw.get("THN_KE")).intValue();
                        if(i == thn_ke) {
                            BigDecimal topupAmount = (BigDecimal) topupDraw.get("TOPUP");
                            BigDecimal drawAmount = (BigDecimal) topupDraw.get("TARIK");

                            topup = ProposalStringFormatter.convertToString(topupAmount.divide(new BigDecimal("1000")));
                            draw = ProposalStringFormatter.convertToString(drawAmount.divide(new BigDecimal("1000")));
                        }
                    }

                    if( "0".equals( topup ) ) topup = "0.00";
                    if( "0".equals( draw ) ) draw = "0.00";
                }
                else
                {
                    topup = "0.00";
                    draw = "0.00";
                }

                String valueLow = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 1 ] ) );
                String valueMid = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 2 ] ) );
                String valueHi = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 3 ] ) );

                String benefitLow = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 1 ], np[ 1 ] );
                String benefitMid = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 2 ], np[ 2 ] );
                String benefitHi = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 3 ], np[ 3 ] );

                LinkedHashMap<String, String> map1 = new LinkedHashMap<>();
                map1.put( "yearNo", ProposalStringFormatter.convertToString( i ) );
                map1.put( "insuredAge", ProposalStringFormatter.convertToString( umur_tt + i ) );
                map1.put( "premiumTotal", premiumTotal );
                map1.put( "topupAssumption", topup );
                map1.put( "drawAssumption", draw );
                map1.put( "valueLow", valueLow );
                map1.put( "valueMid", valueMid );
                map1.put( "valueHi", valueHi );
                map1.put( "benefitLow", benefitLow );
                map1.put( "benefitMid", benefitMid );
                map1.put( "benefitHi", benefitHi );
                mapList.add( map1 );

            }
        }

        LinkedHashMap<String, String> map1 = new LinkedHashMap<>();
        map1.put( "yearNo", getContext().getString(R.string.Thn_Polis_Ke));
        map1.put( "insuredAge", getContext().getString(R.string.Usia_Ttg) );
        map1.put( "premiumTotal", getContext().getString(R.string.Total_Premi));
        map1.put( "topupAssumption", getContext().getString(R.string.Asumsi_Top_up) );
        map1.put( "drawAssumption",  getContext().getString(R.string.Asumsi_Penarikan) );
        map1.put( "valueLow",getContext().getString(R.string.ENP) + " " + getContext().getString(R.string.Rendah) );
        map1.put( "valueMid",getContext().getString(R.string.ENP) + " " + getContext().getString(R.string.Sedang) );
        map1.put( "valueHi", getContext().getString(R.string.ENP) + " " +getContext().getString(R.string.Tinggi) );
        map1.put( "benefitLow", getContext().getString(R.string.EMM) + " " +getContext().getString(R.string.Rendah) );
        map1.put( "benefitMid",getContext().getString(R.string.EMM) + " " + getContext().getString(R.string.Sedang) );
        map1.put( "benefitHi",getContext().getString(R.string.EMM) + " " + getContext().getString(R.string.Tinggi) );
        mapList.add(0, map1 );



        result.setValidityMsg( lb_minus[ 1 ] ? "Maaf, Proposal yang Anda buat Tidak Layak Jual." : "" );
        result.setIllustrationList( mapList );
        map.put("Illustration1", result);

        return map;
    }

    public double of_get_coi_134( int ai_jenis, int ai_th,String no_proposal) {

        double ldec_total = 0, ldec_temp, ldec_persen_ci = 0, up = 0, premi = 0, premi_komb = 0, premi_pokok = 0;
        double ldec_pct = 1;
        double ldec_rate, persentase;
        double ldec_max = 100000;
        int pa_class = 0;

        int pa_risk = 0, li_usia, li_tahun_ke, li_kali = 1, li_jenis, li_class, li_temp, li_risk, li_usia_tt = 0, li_usia_pp = 0;
        li_usia = li_usia_tt + ai_th - 1;
        li_usia_pp = li_usia_pp + ai_th - 1;
        String lku_id = "00";
        Integer ins_per = 0, lsdbsNumber = 0, lsbsId = 0, cara_bayar = 0;
//
////        if( istr_prop.rider_baru[ 5 ] > 0 || istr_prop.rider_baru[ 6 ] > 0 || istr_prop.rider_baru[ 7 ] > 0 || istr_prop.rider_baru[ 8 ] > 0 || istr_prop.rider_baru[ 9 ] > 0 || istr_prop.rider_baru[ 10 ] > 0 )
////        {

        HashMap<String, Object> dataProposal = getSelect().selectDataProposal(no_proposal);
        Log.d(TAG, "getIllustrationResult: " + dataProposal);
        if (!CommonUtil.isEmpty(dataProposal)) {
            ins_per = Integer.valueOf(dataProposal.get("THN_MASA_KONTRAK").toString());
            lku_id = dataProposal.get("LKU_ID").toString();
            li_usia_tt =  Integer.valueOf(dataProposal.get("UMUR_TT").toString());
            li_usia_pp =  Integer.valueOf(dataProposal.get("UMUR_PP").toString());
            up =  Double.valueOf(dataProposal.get("UP").toString());
            lsbsId = ((BigDecimal) dataProposal.get("LSBS_ID")).intValue();
            lsdbsNumber = ((BigDecimal) dataProposal.get("LSDBS_NUMBER")).intValue();
//                umur_pp =  Integer.valueOf(dataProposal.get("UMUR_PP").toString());
            premi = Double.valueOf(dataProposal.get("PREMI").toString());
            premi_komb = Double.valueOf(dataProposal.get("PREMI_KOMB").toString());
//                premi_pokok = Double.valueOf(dataProposal.get("PREMI_POKOK").toString());
//                kondisi_layak_jual = (String) dataProposal.get("NAME_ELIGIBLE");
//                lscb_kali = Integer.valueOf(dataProposal.get("LSCB_KALI").toString());
//                lscb_pay_mode = dataProposal.get("LSCB_PAY_MODE").toString();
            cara_bayar = Integer.valueOf(dataProposal.get("LSCB_ID").toString());
//                cuti_premi = Integer.valueOf(dataProposal.get("THN_CUTI_PREMI").toString());
//                lamaBayar = Integer.valueOf(dataProposal.get("THN_LAMA_BAYAR").toString());
//                userMsagId = dataProposal.get("MSAG_ID").toString();
        }

        if (cara_bayar == 1) {
            li_kali = 4;
            ldec_pct = 0.27;
        } else if (cara_bayar == 2) {
            li_kali = 2;
            ldec_pct = 0.525;
        } else if (cara_bayar == 6) {
            li_kali = 12;
            ldec_pct = 0.1;
        }
////        }
//        wf_set_120();
        switch (ai_jenis) {
//            case 810:
//                //pa
//
//                if ((ai_jenis > 0) && (li_usia < 60)) {
//                    li_class = pa_class;
//                    if (li_usia_tt <= 16) li_class = 2;
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("paRisk", pa_risk);
//                    params.put("liClass", li_class);
//                    params.put("liUsia", li_usia_pp);
//                    for (int i = 1; i <= ins_per; i++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        ldec_rate = select.selectLdecRatePa(lsr_jenis, li_usia_pp);
//                        double maks = 2000000000.0;
//                        if ("02".equals(lku_id)) {
//                            maks = 200000.0;
//                        }
//
//                        double unit = Double.parseDouble((getSelect().getUnit(no_proposal)));
//                        double maxAmount = MathUtil.min(unit * up, maks);
//                        ldec_temp = FormatNumber.round(((maxAmount / 1000) * ldec_rate * ldec_pct) * 0.1, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("810"), new BigDecimal( istr_prop.pa_risk ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                break;
//            case 811:
//                //hcp
//                if ((ai_jenis > 0) && (li_usia < 65)) {
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("riderBaru", ai_jenis);
//                    params.put("kursId", lku_id);
//                    params.put("liUsia", li_usia);
//
//                    for (int i = 1; i <= ins_per; i++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = select.selectLdecRateHcp(lsr_jenis, li_usia_tt, lku_id);
//                        ldec_temp = FormatNumber.round(ldec_rate * 0.1 * ldec_pct, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("811"), new BigDecimal(istr_prop.rider_baru[ 2 ]),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                break;
////
            case 812:
                //tpd
                if (ai_jenis > 0) {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put("li_jenis", 1);
                    params.put("kursId", lku_id);
                    params.put("liUsia", li_usia);

                    for (int i = 1; i <= ins_per; i++) {
                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
                        lku_id = getSelect().getLkuId(no_proposal);
                        ldec_rate = getSelect().selectLdecRateTpd(lsr_jenis, li_usia_tt, lku_id);
                        // maks 2 milyar
                        if ("01".equals(lku_id)) {
                            ldec_max = 1000000000;
                        } else if ("02".equals(lku_id)) {
                            ldec_max = 100000;
                        }

                        double ldec_up = MathUtil.min(up, ldec_max);
                        ldec_temp = FormatNumber.round(((ldec_up / 1000) * ldec_rate * ldec_pct) * 0.1, 2);
                        ldec_total += ldec_temp;
//                    if( ai_th == 1 ){
//                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("812"), new BigDecimal( "1" ),
//                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
                    }
                }
                break;
            case 813:
                //ci
                if (ai_jenis > 0) {
                    int li_total_usia = li_usia_tt + li_usia;
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put("kursId", lku_id);
                    params.put("liUsia", li_total_usia);
                    params.put("up", up);
//                    for (int i = 1; i <= ins_per; i++) {
//                        lku_id = getSelect().getLkuId(no_proposal);
                    ldec_rate = getSelect().selectLdecRateCi(li_total_usia, lku_id);
//                        double up = Double.parseDouble(getSelect().getUp(no_proposal));
                    persentase = Double.parseDouble(getSelect().getPersentase(no_proposal));
                    double persen = persentase / 100.0;
                    double upCi = (up * persen);
                    double maxUp;
                    if ("02".equals(lku_id)) {
                        maxUp = 200000.0;
                    } else {
                        maxUp = 2000000000.0;
                    }
                    double temp = MathUtil.min(upCi, maxUp);
                    ldec_temp = FormatNumber.round(((temp / 1000) * (ldec_rate * ldec_pct)) * 0.1, 2);
                    ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("813"), new BigDecimal("8"),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
//                     }
                }
                break;
//            //
//            case 814:
//                //wp 10tpd
//                if (ai_jenis > 0) {
//                    li_jenis = 5;//istr_prop.rider_baru[ 5 ];
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("riderBaru", li_jenis);
//                    params.put("kursId", lku_id);
//                    params.put("liTahunKe", ai_th);
//                    params.put("liUsia", li_usia);
//                    for (int i = 1; i <= ins_per; i++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLdecRateWp10Tpd(lsr_jenis, li_usia_pp, li_usia_tt, lku_id);
//
//                        ldec_temp = FormatNumber.round((((premi * premi_komb / 100) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
////                        if (ai_th == 1) {
////                            mstProposal.add(new Mst_proposal(cepr01030102Form.getCurrencyCd(), new BigDecimal("814"), new BigDecimal(li_jenis),
////                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                    new BigDecimal(istr_prop.cb_id), cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge()));
////                        }
//                    }
//                }
//                break;
//            case 815:
//                //pb 10 tpd
//                if (ai_jenis > 0) {
//                    li_tahun_ke = li_usia_pp + ai_th - 1;
//                    li_jenis = 5;//istr_prop.rider_baru[ 6 ];
//
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("liJenis", li_jenis);
//                    params.put("kursId", lku_id);
//                    params.put("liTahunKe", li_tahun_ke);
//                    params.put("liUsia", ai_th);
//
//                    for (int i = 1; i <= ins_per; i++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLdecRatePb25Tpd(lsr_jenis, li_usia_pp, li_usia_tt, lku_id);
//
//                        ldec_temp = FormatNumber.round((((premi * premi_komb / 100) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("815"), new BigDecimal( li_jenis ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                break;
////
//            case 816:
//                //wp 10 ci
//                if (ai_jenis > 0) {
//                    li_jenis = 3;//istr_prop.rider_baru[ 7 ];
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("riderBaru", li_jenis);
//                    params.put("kursId", lku_id);
//                    params.put("liTahunKe", ai_th);
//                    params.put("liUsia", li_usia);
//
//                    for (int i = 1; i <= ins_per; i++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLdecRateWp10Ci(lsr_jenis, li_usia_pp, li_usia_tt, lku_id);
//
//                        ldec_temp = FormatNumber.round((((premi * premi_komb / 100) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("816"), new BigDecimal( li_jenis ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                break;
//            case 817:
//                //pb 25 ci/death
//                if (ai_jenis > 0) {
//                    li_tahun_ke = li_usia_pp + ai_th - 1;
//                    li_jenis = 3;//istr_prop.rider_baru[ 8 ];
//
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("liJenis", li_jenis);
//                    params.put("kursId", lku_id);
//                    params.put("liTahunKe", li_tahun_ke);
//                    params.put("liUsia", li_usia);
//                    for (int i = 1; i <= ins_per; i++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLdecRatePb25CiDeath(lsr_jenis, li_usia_pp, li_usia_tt, lku_id);
//
//                        ldec_temp = FormatNumber.round((((premi * premi_komb / 100) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("817"), new BigDecimal(li_jenis),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                break;
//            //pb 10 ci
////            //--
//            case 9:
//                if (ai_jenis > 0) {
//                    li_tahun_ke = li_usia_pp + ai_th - 1;
//                    li_jenis = 3;
//
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("liJenis", li_jenis);
//                    params.put("kursId", lku_id);
//                    params.put("liTahunKe", li_tahun_ke);
//                    params.put("liUsia", ai_th);
//                    for (int i = 1; i <= ins_per; i++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLdecRatePb25Ci(lsr_jenis, li_usia_pp, li_usia_tt, lku_id);
//
//                        ldec_temp = FormatNumber.round((((premi * premi_komb / 100) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("817"), new BigDecimal(li_jenis),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                break;
//            //payor spouse
//            case 10:
//                if (ai_jenis > 0) {
//                    li_tahun_ke = li_usia_pp + ai_th - 1;
//                    li_jenis = 6;
//
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("liJenis", li_jenis);
//                    params.put("kursId", lku_id);
//                    params.put("liTahunKe", li_tahun_ke);
//                    params.put("liUsia", li_usia);
//
//                    for (int i = 1; i <= ins_per; i++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLdecRatePayorSpouse(lsr_jenis, li_usia_pp, li_usia_tt, lku_id);
//
//                        ldec_temp = FormatNumber.round((((premi * premi_komb / 100) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("815"), new BigDecimal(li_jenis),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                break;
////
//            case 819:
//                //hcp family
//                if (ai_jenis > 0 && li_usia < 65) {
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("riderBaru", istr_prop.rider_baru[11] + 140);   // li_jenis in PB = riderBaru, + 140 bcoz start from 141
//                    params.put("kursId", lku_id);
//                    params.put("liUsia", li_usia);
//
//                    for (int i = 1; i <= ins_per; i++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLdecRateHcpFamily(lsr_jenis, li_usia_tt, lku_id);
//
//                        ldec_temp = FormatNumber.round(ldec_rate * 0.1, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("819"), new BigDecimal( istr_prop.rider_baru[ 11 ] + 140 ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                //hcp family peserta tambahan
//                if (istr_prop.rider_baru[11] > 0 && istr_prop.hcpf.peserta > 0) {
//                    for (int i = 1; i <= istr_prop.hcpf.peserta; i++) {
//                        li_usia_tt = istr_prop.hcpf.usia[i] + ai_th - 1;
//
//                        Map<String, Object> params = new HashMap<String, Object>();
//                        params.put("riderBaru", istr_prop.rider_baru[11] + 140);    // li_jenis in PB = riderBaru, + 140 bcoz start from 141
//                        params.put("kursId", lku_id);
//                        params.put("liUsia", li_usia_tt);
//                        for (int l = 1; l <= ins_per; l++) {
//                            double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                            lku_id = getSelect().getLkuId(no_proposal);
//                            ldec_rate = getSelect().selectLdecRateHcpFamilyPesertaTambahan(lsr_jenis, li_usia_tt, lku_id);
////                        ldec_temp = FormatNumber.round( ldec_rate * 0.09, 2 );
//                            ldec_temp = 0;
//                            if (!(i > 1 && li_usia_tt > 24)) {
//                                ldec_temp = FormatNumber.round(ldec_rate * 0.09, 2);
//                            }
//                            ldec_total += ldec_temp;
//                            Integer lsdbs_hcp_fam = null;
//                            if (i == 1) {
//                                lsdbs_hcp_fam = istr_prop.rider_baru[11] + 140 + 20;
//                            } else if (i == 2) {
//                                lsdbs_hcp_fam = istr_prop.rider_baru[11] + 140 + (20 * 2);
//                            } else if (i == 3) {
//                                lsdbs_hcp_fam = istr_prop.rider_baru[11] + 140 + (20 * 3);
//                            } else if (i == 4) {
//                                lsdbs_hcp_fam = istr_prop.rider_baru[11] + 140 + (20 * 4);
//                            } else if (i == 5) {
//                                lsdbs_hcp_fam = istr_prop.rider_baru[11] + 140 + (20 * 5);
//                            }
////                        if( ai_th == 1 ){
////                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("819"), new BigDecimal( lsdbs_hcp_fam ),
////                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                    new BigDecimal(istr_prop.cb_id),  istr_prop.hcpf.tgl[i], istr_prop.hcpf.usia[i] ) );
////                        }
//                        }
//                    }
//                }
//                break;
//            case 818:
//                //term rider
//                if (ai_jenis > 0) {
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("kursId", lku_id);
//                    params.put("liUsia", li_usia);
//                    for (int l = 1; l <= ins_per; l++) {
//                        lku_id = getSelect().getLkuId(no_proposal);
//
//                        ldec_rate = getSelect().selectLdecRateTermRider(li_usia_tt, lku_id);
//                        double termup = Double.parseDouble((getSelect().getTermUp(no_proposal)));
//                        ldec_temp = FormatNumber.round(((termup / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("818"), new BigDecimal( "2" ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                break;
//            case 823:
//                //eka sehat
//                if (ai_jenis > 0 && li_usia < 65) {
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("riderBaru", istr_prop.rider_baru[13]);
//                    params.put("kursId", lku_id);
//                    params.put("liUsia", li_usia);
//                    for (int l = 1; l <= ins_per; l++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectEkaSehatRider(lsr_jenis, li_usia_tt, lku_id);
//                        if (cara_bayar == 1) {  //triwulan
//                            ldec_pct = 0.35;
//                        } else if (cara_bayar == 2) { //semesteran
//                            ldec_pct = 0.65;
//                        } else if (cara_bayar == 6) { //bulanan
//                            ldec_pct = 0.12;
//                        }
//                        ldec_temp = FormatNumber.round((ldec_rate * 0.12) * 0.1, 2);
////        			ldec_temp = FormatNumber.round( (ldec_rate * ldec_pct) * 0.1, 2 );
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("823"), new BigDecimal( istr_prop.rider_baru[ 13 ] ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                //eka sehat peserta tambahan
//                if (ai_jenis > 0 && istr_prop.eka_sehat.peserta > 0) {
//                    for (int i = 1; i <= istr_prop.eka_sehat.peserta; i++) {
//                        li_usia = istr_prop.eka_sehat.usia[i] + ai_th - 1;
//
//                        Map<String, Object> params = new HashMap<String, Object>();
//                        params.put("riderBaru", istr_prop.rider_baru[13]);
//                        params.put("kursId", lku_id);
//                        params.put("liUsia", li_usia);
//                        for (int l = 1; l <= ins_per; l++) {
//                            double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                            lku_id = getSelect().getLkuId(no_proposal);
//                            ldec_rate = getSelect().selectEkaSehatRider(lsr_jenis, li_usia_tt, lku_id);
////                        ldec_temp = FormatNumber.round( (ldec_rate * 0.975 * ldec_pct) * 0.1, 2 );
//                            ldec_temp = FormatNumber.round((ldec_rate * 0.117), 2);
//                            ldec_total += ldec_temp;
//
//                            Integer lsdbs_e_sehat = null;
//                            if (i == 1) {
//                                lsdbs_e_sehat = istr_prop.rider_baru[13] + 15;
//                            } else if (i == 2) {
//                                lsdbs_e_sehat = istr_prop.rider_baru[13] + (15 * 2);
//                            } else if (i == 3) {
//                                lsdbs_e_sehat = istr_prop.rider_baru[13] + (15 * 3);
//                            } else if (i == 4) {
//                                lsdbs_e_sehat = istr_prop.rider_baru[13] + (15 * 4);
//                            } else if (i == 5) {
//                                lsdbs_e_sehat = istr_prop.rider_baru[13] + (15 * 5);
//                            }
////                        if( ai_th == 1 ){
////                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("823"), new BigDecimal( lsdbs_e_sehat ),
////                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                    new BigDecimal(istr_prop.cb_id), istr_prop.eka_sehat.tgl[i], istr_prop.eka_sehat.usia[i] ) );
////                        }
//                        }
//                    }
//                }
//                break;
////
//            case 825:
//                //eka sehat inner limit
//                if (ai_jenis > 0 && li_usia < 75) {
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("riderBaru", istr_prop.rider_baru[15]);
//                    params.put("kursId", lku_id);
//                    params.put("liUsia", li_usia);
//                    for (int l = 1; l <= ins_per; l++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectEkaSehatInnerLimitRider(lsr_jenis, li_usia_tt, lku_id);
//                        if (cara_bayar == 1) {  //triwulan
//                            ldec_pct = 0.35;
//                        } else if (cara_bayar == 2) { //semesteran
//                            ldec_pct = 0.65;
//                        } else if (cara_bayar == 6) { //bulanan
//                            ldec_pct = 0.12;
//                        }
//                        ldec_temp = FormatNumber.round((ldec_rate * 0.12) * 0.1, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("825"), new BigDecimal( istr_prop.rider_baru[ 15 ] ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                if (ai_jenis > 0 && istr_prop.ekaSehatInnerLimit.peserta > 0 && li_usia < 75) {
//                    for (int i = 1; i <= istr_prop.ekaSehatInnerLimit.peserta; i++) {
//                        li_usia = istr_prop.ekaSehatInnerLimit.usia[i] + ai_th - 1;
//
//                        Map<String, Object> params = new HashMap<String, Object>();
//                        params.put("riderBaru", istr_prop.rider_baru[15]);
//                        params.put("kursId", lku_id);
//                        params.put("liUsia", li_usia);
//                        for (int l = 1; l <= ins_per; l++) {
//                            double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                            lku_id = getSelect().getLkuId(no_proposal);
//                            ldec_rate = getSelect().selectEkaSehatInnerLimitRider(lsr_jenis, li_usia_tt, lku_id);
//                            ldec_temp = FormatNumber.round((ldec_rate * 0.117), 2);
//                            ldec_total += ldec_temp;
//
//                            Integer lsdbs_e_sehat = null;
//                            if (i == 1) {
//                                lsdbs_e_sehat = istr_prop.rider_baru[15] + 15;
//                            } else if (i == 2) {
//                                lsdbs_e_sehat = istr_prop.rider_baru[15] + (15 * 2);
//                            } else if (i == 3) {
//                                lsdbs_e_sehat = istr_prop.rider_baru[15] + (15 * 3);
//                            } else if (i == 4) {
//                                lsdbs_e_sehat = istr_prop.rider_baru[15] + (15 * 4);
//                            } else if (i == 5) {
//                                lsdbs_e_sehat = istr_prop.rider_baru[15] + (15 * 5);
//                            }
////                        if( ai_th == 1 ){
////                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("825"), new BigDecimal( lsdbs_e_sehat ),
////                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                    new BigDecimal(istr_prop.cb_id), istr_prop.ekaSehatInnerLimit.tgl[i], istr_prop.ekaSehatInnerLimit.usia[i] ) );
////                        }
//                        }
//                    }
//                }
//                break;
////
//            case 826:
//                //hcp Provider
//                if (ai_jenis > 0 && li_usia < 65) {
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("riderBaru", istr_prop.rider_baru[16]);
//                    params.put("kursId", lku_id);
//                    params.put("liUsia", li_usia);
//
//                    for (int l = 1; l <= ins_per; l++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLdecRateHcpProvider(lsr_jenis, li_usia_tt, lku_id);
//
//                        ldec_temp = FormatNumber.round(ldec_rate * 0.1, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("826"), new BigDecimal( istr_prop.rider_baru[ 16 ] ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                //hcp provider peserta tambahan
//                if (istr_prop.rider_baru[16] > 0 && istr_prop.hcpPro.peserta > 0 && li_usia < 65) {
//                    for (int i = 1; i <= istr_prop.hcpPro.peserta; i++) {
//                        li_usia = istr_prop.hcpPro.usia[i] + ai_th - 1;
//
//                        Map<String, Object> params = new HashMap<String, Object>();
//                        params.put("riderBaru", istr_prop.rider_baru[16]);    // li_jenis in PB = riderBaru, + 140 bcoz start from 141
//                        params.put("kursId", lku_id);
//                        params.put("liUsia", li_usia);
//
//                        for (int l = 1; l <= ins_per; l++) {
//                            double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                            lku_id = getSelect().getLkuId(no_proposal);
//                            ldec_rate = getSelect().selectLdecRateHcpProviderPesertaTambahan(lsr_jenis, li_usia_tt, lku_id);
//                            ldec_temp = 0;
//                            if (!(i > 1 && li_usia > 24)) {
//                                ldec_temp = FormatNumber.round(ldec_rate * 0.09, 2);
//                            }
//                            ldec_total += ldec_temp;
//
//                            Integer lsdbs_hcp_pro = null;
//                            if (i == 1) {
//                                lsdbs_hcp_pro = istr_prop.rider_baru[16] + 12;
//                            } else if (i == 2) {
//                                lsdbs_hcp_pro = istr_prop.rider_baru[16] + (12 * 2);
//                            } else if (i == 3) {
//                                lsdbs_hcp_pro = istr_prop.rider_baru[16] + (12 * 3);
//                            } else if (i == 4) {
//                                lsdbs_hcp_pro = istr_prop.rider_baru[16] + (12 * 4);
//                            } else if (i == 5) {
//                                lsdbs_hcp_pro = istr_prop.rider_baru[16] + (12 * 5);
//                            }
//
////                        if( ai_th == 1 ){
////                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("826"), new BigDecimal( lsdbs_hcp_pro ),
////                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                    new BigDecimal(istr_prop.cb_id), istr_prop.hcpPro.tgl[i], istr_prop.hcpPro.usia[i] ) );
////                        }
//                        }
//                    }
//                }
//                break;
////
//            case 827:
//                //Waiver TPD/CI/Death
//                if (ai_jenis > 0) {
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("liJenis", istr_prop.rider_baru[17]);
//                    params.put("kursId", lku_id);
//                    params.put("liTahunKe", 0);
//                    params.put("liUsia", li_usia);
//                    for (int l = 1; l <= ins_per; l++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLdecRateWaiverTpdCi(lsr_jenis, li_usia_tt, li_usia_pp, lku_id);
//
//                        ldec_max = 200000;
////                    ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi * istr_prop.pct_premi / 10 ) * li_kali / 1000 ) * ldec_rate ) * 0.1, 2 );
//                        ldec_temp = FormatNumber.round((((premi * 1) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("827"), new BigDecimal( istr_prop.rider_baru[ 17 ] ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                break;
//            case 828:
//                //Payor TPD/CI/Death
//                if (ai_jenis > 0) {
//                    li_tahun_ke = li_usia_pp + ai_th - 1;
//                    li_jenis = istr_prop.rider_baru[18];
//
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("liJenis", li_jenis);
//                    params.put("kursId", lku_id);
//                    params.put("liTahunKe", li_tahun_ke);
//                    params.put("liUsia", li_usia);
//                    for (int l = 1; l <= ins_per; l++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLdecRatePayorTpdCiDeath(lsr_jenis, li_usia_tt, li_usia_pp, lku_id);
//
//                        ldec_max = 200000;
////                    ldec_temp = FormatNumber.round( ( ( ( istr_prop.premi * istr_prop.pct_premi / 100 ) * ldec_pct ) * ldec_rate ) * 0.1, 2 );
//                        ldec_temp = FormatNumber.round((((premi * 1) * li_kali / 1000) * ldec_rate) * 0.1, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("828"), new BigDecimal( li_jenis ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                break;
////
//            case 830:
//                //ladies insurance
//                if (ai_jenis > 0) {
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("liJenis", istr_prop.rider_baru[19]);
//                    params.put("kursId", lku_id);
//                    params.put("liTahunKe", 0);
//                    params.put("liUsia", li_usia);
//                    for (int l = 1; l <= ins_per; l++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLdecRateLadiesIns(lsr_jenis, li_usia_tt, li_usia_pp, lku_id);
//
//                        ldec_max = 200000;
//                        if ("01".equals(lku_id)) {
//                            ldec_max = 2000000000;
//                        }
//                        persentase = Double.parseDouble(getSelect().getPersentase(no_proposal)) / 100.0;
//                        double maxAmount = MathUtil.min( up * persentase, ldec_max );
//                        ldec_temp = FormatNumber.round( ( ( maxAmount / 1000 ) * ldec_rate ) * 0.1, 2 );
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("830"), new BigDecimal( istr_prop.rider_baru[ 19 ] ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                break;
////
//            case 831:
//                //hcp ladies
//                if (ai_jenis > 0 && istr_prop.hcpLad.peserta == 0) {
//                    if (li_usia < 66 && istr_prop.hcpLad.peserta == 0) {
//                        Map<String, Object> params = new HashMap<String, Object>();
//                        params.put("riderBaru", istr_prop.rider_baru[20]);   // li_jenis in PB = riderBaru, + 140 bcoz start from 141
//                        params.put("kursId", lku_id);
//                        params.put("liUsia", li_usia);
//                        for (int l = 1; l <= ins_per; l++) {
//                            double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                            lku_id = getSelect().getLkuId(no_proposal);
//                            ldec_rate = getSelect().selectLdecRateHcpLadies(lsr_jenis, li_usia_tt, lku_id);
//
//                            ldec_temp = FormatNumber.round(ldec_rate * 0.1, 2);
//                            ldec_total += ldec_temp;
////                        if( ai_th == 1 ){
////                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("831"), new BigDecimal( istr_prop.rider_baru[ 20 ] ),
////                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                    new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                        }
//                        }
//                    }
//                }
//                //hcp ladies peserta tambahan
//                if (ai_jenis > 0 && istr_prop.hcpLad.peserta > 0) {
//                    li_usia = istr_prop.hcpLad.usia[1] + ai_th - 1;
//                    if (li_usia < 66 && istr_prop.hcpLad.peserta > 0) {
//                        for (int i = 1; i <= istr_prop.hcpLad.peserta; i++) {
//                            li_usia = istr_prop.hcpLad.usia[i] + ai_th - 1;
//
//                            Map<String, Object> params = new HashMap<String, Object>();
//                            params.put("riderBaru", istr_prop.rider_baru[20]);    // li_jenis in PB = riderBaru, + 140 bcoz start from 141
//                            params.put("kursId", lku_id);
//                            params.put("liUsia", li_usia);
//                            for (int l = 1; l <= ins_per; l++) {
//                                double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                                lku_id = getSelect().getLkuId(no_proposal);
//                                ldec_rate = getSelect().selectLdecRateHcpLadiesPesertaTambahan(lsr_jenis, li_usia_tt, lku_id);
//                                ldec_temp = FormatNumber.round(ldec_rate * 0.1, 2);
//                                if (i >= 2) {
//                                    ldec_temp = FormatNumber.round(ldec_rate * 0.09, 2);
//                                    if (i >= 3 && li_usia > 25) ldec_temp = 0;
//                                }
//                                ldec_total += ldec_temp;
//
//                                Integer lsdbs_hcp_lad = null;
//                                if (i == 1) {
//                                    lsdbs_hcp_lad = istr_prop.rider_baru[20];
//                                } else if (i == 2) {
//                                    lsdbs_hcp_lad = istr_prop.rider_baru[20] + (24);
//                                } else if (i == 3) {
//                                    lsdbs_hcp_lad = istr_prop.rider_baru[20] + (24 * 2);
//                                } else if (i == 4) {
//                                    lsdbs_hcp_lad = istr_prop.rider_baru[20] + (24 * 3);
//                                } else if (i == 5) {
//                                    lsdbs_hcp_lad = istr_prop.rider_baru[20] + (24 * 4);
//                                }
////                            if( ai_th == 1 ){
////                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("831"), new BigDecimal( lsdbs_hcp_lad ),
////                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                        new BigDecimal(istr_prop.cb_id), istr_prop.hcpLad.tgl[i], istr_prop.hcpLad.usia[i] ) );
////                            }
//                            }
//                        }
//                    }
//                }
//                break;
//            case 832:
//                //ladies medical expense
//                if (ai_jenis > 0 && li_usia < 75 && istr_prop.ladiesMedExpense.peserta == 0) {
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("riderBaru", istr_prop.rider_baru[21]);   // li_jenis in PB = riderBaru, + 140 bcoz start from 141
//                    params.put("kursId", lku_id);
//                    params.put("liUsia", li_usia);
//                    for (int l = 1; l <= ins_per; l++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLadiesMedExpenseRider(lsr_jenis, li_usia_tt, lku_id);
//
//                        ldec_temp = FormatNumber.round(ldec_rate * 0.12, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("832"), new BigDecimal( istr_prop.rider_baru[ 21 ] ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                //ladies medical expense peserta tambahan
//                if (ai_jenis > 0 && istr_prop.ladiesMedExpense.peserta > 0) {
//                    li_usia = istr_prop.ladiesMedExpense.usia[1] + ai_th - 1;
//                    if (li_usia < 75) {
//                        for (int i = 1; i <= istr_prop.ladiesMedExpense.peserta; i++) {
//                            li_usia = istr_prop.ladiesMedExpense.usia[i] + ai_th - 1;
//
//                            Map<String, Object> params = new HashMap<String, Object>();
//                            params.put("riderBaru", istr_prop.rider_baru[21]);    // li_jenis in PB = riderBaru, + 140 bcoz start from 141
//                            params.put("kursId", lku_id);
//                            params.put("liUsia", li_usia);
//                            for (int l = 1; l <= ins_per; l++) {
//                                double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                                lku_id = getSelect().getLkuId(no_proposal);
//                                ldec_rate = getSelect().selectLadiesMedExpenseRider(lsr_jenis, li_usia_tt, lku_id);
//                                ldec_temp = FormatNumber.round(ldec_rate * 0.12, 2);
//                                if (i > 1) {
//                                    ldec_temp = FormatNumber.round(ldec_rate * 0.117, 2);
//                                }
//                                ldec_total += ldec_temp;
//
//                                Integer lsdbs_ladies_med = null;
//                                if (i == 1) {
//                                    lsdbs_ladies_med = istr_prop.rider_baru[21];
//                                } else if (i == 2) {
//                                    lsdbs_ladies_med = istr_prop.rider_baru[21] + (7);
//                                } else if (i == 3) {
//                                    lsdbs_ladies_med = istr_prop.rider_baru[21] + (7 * 2);
//                                } else if (i == 4) {
//                                    lsdbs_ladies_med = istr_prop.rider_baru[21] + (7 * 3);
//                                } else if (i == 5) {
//                                    lsdbs_ladies_med = istr_prop.rider_baru[21] + (7 * 4);
//                                }
//
////                            if( ai_th == 1 ){
////                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("832"), new BigDecimal( lsdbs_ladies_med ),
////                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                        new BigDecimal(istr_prop.cb_id), istr_prop.ladiesMedExpense.tgl[i], istr_prop.ladiesMedExpense.usia[i] ) );
////                            }
//                            }
//                        }
//                    }
//                }
//                break;
//            case 833:
//                //ladies medical expense inner limit
//                if (ai_jenis > 0 && li_usia < 75 && istr_prop.ladiesMedExpenseInnerLimit.peserta == 0) {
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("riderBaru", istr_prop.rider_baru[22]);   // li_jenis in PB = riderBaru, + 140 bcoz start from 141
//                    params.put("kursId", lku_id);
//                    params.put("liUsia", li_usia);
//                    for (int l = 1; l <= ins_per; l++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLadiesMedExpenseInnerLimitRider(lsr_jenis, li_usia_tt, lku_id);
//
//                        ldec_temp = FormatNumber.round(ldec_rate * 0.12, 2);
//                        ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("833"), new BigDecimal( istr_prop.rider_baru[ 22 ] ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                //ladies medical expense inner limit peserta tambahan
//                if (ai_jenis > 0 && istr_prop.ladiesMedExpenseInnerLimit.peserta > 0) {
//                    li_usia = istr_prop.ladiesMedExpenseInnerLimit.usia[1] + ai_th - 1;
//                    if (li_usia < 75) {
//                        for (int i = 1; i <= istr_prop.ladiesMedExpenseInnerLimit.peserta; i++) {
//                            li_usia = istr_prop.ladiesMedExpenseInnerLimit.usia[i] + ai_th - 1;
//
//                            Map<String, Object> params = new HashMap<String, Object>();
//                            params.put("riderBaru", istr_prop.rider_baru[22]);    // li_jenis in PB = riderBaru, + 140 bcoz start from 141
//                            params.put("kursId", lku_id);
//                            params.put("liUsia", li_usia);
//                            for (int l = 1; l <= ins_per; l++) {
//                                double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                                lku_id = getSelect().getLkuId(no_proposal);
//                                ldec_rate = getSelect().selectLadiesMedExpenseInnerLimitRider(lsr_jenis, li_usia_tt, lku_id);
//                                ldec_temp = FormatNumber.round(ldec_rate * 0.12, 2);
//                                if (i > 1) {
//                                    ldec_temp = FormatNumber.round(ldec_rate * 0.117, 2);
//                                }
//                                ldec_total += ldec_temp;
//
//                                Integer lsdbs_ladies_med_il = null;
//                                if (i == 1) {
//                                    lsdbs_ladies_med_il = istr_prop.rider_baru[22];
//                                } else if (i == 2) {
//                                    lsdbs_ladies_med_il = istr_prop.rider_baru[22] + (7);
//                                } else if (i == 3) {
//                                    lsdbs_ladies_med_il = istr_prop.rider_baru[22] + (7 * 2);
//                                } else if (i == 4) {
//                                    lsdbs_ladies_med_il = istr_prop.rider_baru[22] + (7 * 3);
//                                } else if (i == 5) {
//                                    lsdbs_ladies_med_il = istr_prop.rider_baru[22] + (7 * 4);
//                                }
//
////                            if( ai_th == 1 ){
////                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("833"), new BigDecimal( lsdbs_ladies_med_il ),
////                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                        new BigDecimal(istr_prop.cb_id), istr_prop.ladiesMedExpenseInnerLimit.tgl[i], istr_prop.ladiesMedExpenseInnerLimit.usia[i] ) );
////                            }
//                            }
//                        }
//                    }
//                }
//                break;
//            case 23:
//                //Payor 5/10 TPD/CI/Death
//                if (istr_prop.rider_baru[23] > 0) {
//                    li_tahun_ke = li_usia_pp + ai_th - 1;
////                    if (cepr01030102Form.getPremiumFurloughYear() == 5) {
////                        istr_prop.rider_baru[23] = 2;
////                    } else if (cepr01030102Form.getPremiumFurloughYear() == 10) {
////                        istr_prop.rider_baru[23] = 3;
////                    }
//                    li_jenis = istr_prop.rider_baru[23];
//
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("liJenis", li_jenis);
//                    params.put("kursId", lku_id);
//                    params.put("liTahunKe", li_tahun_ke);
//                    params.put("liUsia", ai_th);
//                    for (int l = 1; l <= ins_per; l++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLdecRatePayorTpdCiDeath(lsr_jenis, li_usia_tt, li_usia_pp, lku_id);
//
//
//                        ldec_max = 200000;
//                        if (lku_id == "01") ldec_max = 2000000000;
//                        ldec_temp = FormatNumber.round(((((premi * premi_komb / 100) * li_kali / 1000) * ldec_rate) * 0.1), 2);
//                        if (lsbsId == 120 || lsbsId == 202) {
//                            ldec_temp = FormatNumber.round((((ldec_rate / 1000) * (premi * li_kali)) * 0.1), 2);
//                        }
//                        ldec_total += ldec_temp;
//
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("828"), new BigDecimal( li_jenis ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                break;
//            case 24:
//                //Waiver 5/10 TPD/CI
//                if (istr_prop.rider_baru[24] > 0) {
//                    li_usia = li_usia_tt + ai_th - 1;
////                    if(  cepr01030102Form.getPremiumFurloughYear() == 5 ){
////                        istr_prop.rider_baru[24] = 4;
////                    }else if(  cepr01030102Form.getPremiumFurloughYear() == 10 ){
////                        istr_prop.rider_baru[24] = 5;
////                    }
//                    li_jenis = istr_prop.rider_baru[24];
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put("liJenis", li_jenis);
//                    params.put("kursId", lku_id);
//                    params.put("liTahunKe", ai_th);
//                    params.put("liUsia", li_usia);
//                    for (int l = 1; l <= ins_per; l++) {
//                        double lsr_jenis = Double.parseDouble(getSelect().getLsrJenis(no_proposal));
//                        lku_id = getSelect().getLkuId(no_proposal);
//                        ldec_rate = getSelect().selectLdecRateWaiverTpdCi(lsr_jenis, li_usia_tt, li_usia_pp, lku_id);
//
//                        ldec_max = 200000;
////            		ldec_temp = Round(((ldec_rate/1000)*(istr_prop.premi *li_kali))*0.1, 2)
//                        ldec_temp = FormatNumber.round(((ldec_rate / 1000) * (premi * li_kali)) * 0.1, 2);
//                        ldec_total += ldec_temp;
//
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("827"), new BigDecimal( li_jenis ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                    }
//                }
//                break;
//            case 25:
//                if( istr_prop.rider_baru[25] > 0 && li_usia < istr_prop.usia_schol ){
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put( "kursId", istr_prop.kurs_id );
//                    params.put( "liUsia", li_usia_pp );
////                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateScholarship( params ) );
//
//                    Map<String, Object> paramsRate = new HashMap<String, Object>();
//                    Integer lijenis = 0;
//                    if( istr_prop.usia_schol == 22 ){
//                        lijenis = 1;
//                    }else if( istr_prop.usia_schol == 25 ){
//                        lijenis = 2;
//                    }
//                    paramsRate = new HashMap<String, Object>();
//                    paramsRate.put( "lsbsId", "835" );
//                    paramsRate.put( "liJenis", lijenis );
//                    paramsRate.put( "liUsia", li_usia );
//                    paramsRate.put( "kursId", istr_prop.kurs_id );
////                    BigDecimal rate_factor = eproposalManager.selectTableFactor( paramsRate );
////                    if( rate_factor == null ) { rate_factor = BigDecimal.ZERO;}
////                    double rate_factor_double = MethodSupport.ConverttoDouble( rate_factor );
//////                  153.240.000 * 1,79 /1000 *10% = 27.430 (yg ini yg bener ya)
//////                  200.000.000 * 1.79 / 1000 * 10% = 35.800
////
////                    ldec_temp = FormatNumber.round( ( ( ldec_rate / 1000 )* istr_prop.up_schol * rate_factor_double ) * 0.1, 2 );
////                    ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("835"), new BigDecimal( lijenis ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                }
//                break;
////
//            case 27:
//                if( istr_prop.rider_baru[27] > 0  ){
////                    String genderInsured = cepr01030101Form.getInsuredSexCd().trim();
////                    li_jenis=0;
////                    if(genderInsured.equals("L")){
////                        li_jenis = 1;
////                    }
////                    else if(genderInsured.equals("P")){
////                        li_jenis = 2;
////                    }
//
////                    Map<String, Object> params = new HashMap<String, Object>();
////                    params.put( "lku_id", istr_prop.kurs_id );
////                    params.put( "lsr_jenis", li_jenis );
////                    params.put( "li_usia", li_usia);
////                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateEarlyStageCi99( params ) );
////
////                    double persentase = MethodSupport.ConverttoDouble( new BigDecimal(cepr01030103Form.getEsci99ChooseCd())) / 100.0;
////                    double upEsci99 = ( istr_prop.up * persentase );
////                    double maxUp;
////
////                    double temp = MathUtil.min( upEsci99, maxUp );
////                    ldec_temp = FormatNumber.round(((ldec_rate / 1000) * temp) * 0.1, 2);
////                    ldec_total += ldec_temp;
//
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("837"), new BigDecimal( li_jenis ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                }
//                break;
////
//            case 28:
//                //SMiLe Medical (+)
//                if( istr_prop.rider_baru[28] > 0 ){
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    int riderBaru = istr_prop.rider_baru[ 28 ];
//
//                    params.put( "riderBaru", riderBaru );
//                    params.put( "kursId", istr_prop.kurs_id );
//                    params.put( "liUsia", li_usia );
////                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRider( params ) );
////
////                    if( istr_prop.cb_id == 1 ){  //triwulan
////                        ldec_pct = 0.35;
////                    }else if( istr_prop.cb_id == 2 ){ //semesteran
////                        ldec_pct = 0.65;
////                    }else if( istr_prop.cb_id == 6 ){ //bulanan
////                        ldec_pct = 0.12;
////                    }
//////                        ldec_temp = FormatNumber.round( (ldec_rate * ldec_pct) * 0.1, 2 );
////                    ldec_temp = FormatNumber.round( (ldec_rate) * 0.12, 2 );
////                    ldec_total += ldec_temp;
//
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("838"), new BigDecimal( riderBaru ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//
////                    if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag())){
////                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
////                        ldec_temp = FormatNumber.round( (ldec_rate) * 0.12, 2 );
////                        ldec_total += ldec_temp;
////
//////                        if( ai_th == 1 ){
//////                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
//////                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
//////                                    new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//////                        }
////                    }
////                    if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag()) && CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRgFlag())){
////                        riderBaru = istr_prop.rider_baru[ 28 ]+20;
////
////                        params.put( "riderBaru", riderBaru );
////                        params.put( "kursId", istr_prop.kurs_id );
////                        params.put( "liUsia", li_usia );
////                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
////                        ldec_temp = FormatNumber.round( (ldec_rate) * 0.12, 2 );
////                        ldec_total += ldec_temp;
//
////                        if( ai_th == 1 ){
////                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
////                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                    new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                        }
////                    }
////                    if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag()) && CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRbFlag())){
////                        riderBaru = istr_prop.rider_baru[ 28 ]+40;
////
////                        params.put( "riderBaru", riderBaru );
////                        params.put( "kursId", istr_prop.kurs_id );
////                        params.put( "liUsia", li_usia );
////                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
////                        ldec_temp = FormatNumber.round( (ldec_rate) * 0.12, 2 );
////                        ldec_total += ldec_temp;
//
////                        if( ai_th == 1 ){
////                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
////                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                    new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                        }
//                }
////                    if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag()) && CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusPkFlag())){
////                        riderBaru = istr_prop.rider_baru[ 28 ]+60;
////
////                        params.put( "riderBaru", riderBaru );
////                        params.put( "kursId", istr_prop.kurs_id );
////                        params.put( "liUsia", li_usia );
////                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
////                        ldec_temp = FormatNumber.round( (ldec_rate) * 0.12, 2 );
////                        ldec_total += ldec_temp;
//
////                        if( ai_th == 1 ){
////                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
////                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                    new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                        }
////                    }
////                }
//                //SMiLe Medical(+) Peserta Tambahan
//                if( istr_prop.rider_baru[ 28 ] > 0 && istr_prop.medicalPlus.peserta > 0   && li_usia < 75 )
//                {
//                    for( int i = 1; i <= istr_prop.medicalPlus.peserta; i++ )
//                    {
//                        li_usia = istr_prop.medicalPlus.usia[ i ] + ai_th - 1;
//                        int riderBaru = istr_prop.rider_baru[ 28 ];
//
//                        if(i==1){
//                            riderBaru = riderBaru + 4;
//                        }
//                        else if(i==2){
//                            riderBaru = riderBaru + 8;
//                        }
//                        else if(i==3){
//                            riderBaru = riderBaru + 12;
//                        }
//                        else if(i==4){
//                            riderBaru = riderBaru + 16;
//                        }
//                        Map<String, Object> params = new HashMap<String, Object>();
//                        params.put( "riderBaru", riderBaru );
//                        params.put( "kursId", istr_prop.kurs_id );
//                        params.put( "liUsia", li_usia );
////                        ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRider( params ) );
////                        ldec_temp = FormatNumber.round( (ldec_rate * 0.117) , 2 );
////                        ldec_total += ldec_temp;
//
////                        if( ai_th == 1 ){
////                            mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("838"), new BigDecimal( riderBaru ),
////                                    BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                    new BigDecimal(istr_prop.cb_id), istr_prop.medicalPlus.tgl[i], istr_prop.medicalPlus.usia[i] ) );
////                        }
//
////                        if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag())){
////                            ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
////                            ldec_temp = FormatNumber.round( (ldec_rate * 0.117) , 2 );
////                            ldec_total += ldec_temp;
//
////                            if( ai_th == 1 ){
////                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
////                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                        new BigDecimal(istr_prop.cb_id),  istr_prop.medicalPlus.tgl[i], istr_prop.medicalPlus.usia[i] ) );
////                            }
////                        }
////                        if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag()) && CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRgFlag())){
////                            riderBaru = istr_prop.rider_baru[ 28 ]+20;
//
//                        if(i==1){
//                            riderBaru = riderBaru + 4;
//                        }
//                        else if(i==2){
//                            riderBaru = riderBaru + 8;
//                        }
//                        else if(i==3){
//                            riderBaru = riderBaru + 12;
//                        }
//                        else if(i==4){
//                            riderBaru = riderBaru + 16;
//                        }
//
//                        params.put( "riderBaru", riderBaru );
//                        params.put( "kursId", istr_prop.kurs_id );
//                        params.put( "liUsia", li_usia );
////                            ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
////                            ldec_temp = FormatNumber.round( (ldec_rate * 0.117) , 2 );
////                            ldec_total += ldec_temp;
//
////                            if( ai_th == 1 ){
////                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
////                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                        new BigDecimal(istr_prop.cb_id),  istr_prop.medicalPlus.tgl[i], istr_prop.medicalPlus.usia[i] ) );
////                            }
////                        }
////                        if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag()) && CommonConst.CHECKED_TRUE.equals(istr_prop.medicalPlus.medicalPlusRbFlag[i])){
////                            riderBaru = istr_prop.rider_baru[ 28 ]+40;
//
//                        if(i==1){
//                            riderBaru = riderBaru + 4;
//                        }
//                        else if(i==2){
//                            riderBaru = riderBaru + 8;
//                        }
//                        else if(i==3){
//                            riderBaru = riderBaru + 12;
//                        }
//                        else if(i==4){
//                            riderBaru = riderBaru + 16;
//                        }
//                        params.put( "riderBaru", riderBaru );
//                        params.put( "kursId", istr_prop.kurs_id );
//                        params.put( "liUsia", li_usia );
////                            ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
////                            ldec_temp = FormatNumber.round( (ldec_rate * 0.117) , 2 );
////                            ldec_total += ldec_temp;
//
////                            if( ai_th == 1 ){
////                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
////                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                        new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                            }
////                        }
////                        if(CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusRjFlag()) && CommonConst.CHECKED_TRUE.equals(cepr01030103Form.getMedicalPlusPkFlag())){
////                            riderBaru = istr_prop.rider_baru[ 28 ]+60;
//
//                        if(i==1){
//                            riderBaru = riderBaru + 4;
//                        }
//                        else if(i==2){
//                            riderBaru = riderBaru + 8;
//                        }
//                        else if(i==3){
//                            riderBaru = riderBaru + 12;
//                        }
//                        else if(i==4){
//                            riderBaru = riderBaru + 16;
//                        }
//                        params.put( "riderBaru", riderBaru );
//                        params.put( "kursId", istr_prop.kurs_id );
//                        params.put( "liUsia", li_usia );
////                            ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectMedicalPlusRjRider( params ) );
////                            ldec_temp = FormatNumber.round( (ldec_rate * 0.117) , 2 );
////                            ldec_total += ldec_temp;
//
////                            if( ai_th == 1 ){
////                                mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("839"), new BigDecimal( riderBaru ),
////                                        BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                        new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
//                    }
//                }
////                    }
////                }
//                break;
//            case 112:
//                //term rider header
//                if( istr_prop.rider_baru[ 12 ] > 0 )
//                {
//                    Map<String, Object> params = new HashMap<String, Object>();
//                    params.put( "kursId", istr_prop.kurs_id );
//                    params.put( "liUsia", li_usia );
////                    ldec_rate = MethodSupport.ConverttoDouble( eproposalManager.selectLdecRateTermRider( params ) );
////                    ldec_temp = FormatNumber.round( ( ( istr_prop.up_term / 1000 ) * ldec_rate ) * 0.1, 2 );
////                    ldec_total += ldec_temp;
////                    if( ai_th == 1 ){
////                        mstProposal.add( new Mst_proposal( cepr01030102Form.getCurrencyCd(), new BigDecimal("818"), new BigDecimal( "2" ),
////                                BigDecimal.ZERO, new BigDecimal(ldec_temp * 10), new BigDecimal(ldec_rate), new BigDecimal(cepr01030102Form.getTermOfContract()),
////                                new BigDecimal(istr_prop.cb_id),  cepr01030101Form.getInsuredBirthDay(), cepr01030101Form.getInsuredAge() ) );
////                    }
//                }
//                break;
            default:
                break;
        }
////        cepr01031001Form.setMstProposal( mstProposal );
        return ldec_total;

    }
}

