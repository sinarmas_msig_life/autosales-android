package id.co.ajsmsig.nb.leader.summarybc;

import android.app.Application;
import android.content.Context;

import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.leader.summarybc.rest.RestApiFactory;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class AppControllerSummary extends Application {

    private ApiEndpoints apiEndpoints;
    private Scheduler scheduler;

    @Override
    public void onCreate() {
        super.onCreate();
    }


    private static AppControllerSummary get(Context context) {
        return (AppControllerSummary) context.getApplicationContext();
    }

    public static AppControllerSummary create(Context context) {
        return AppControllerSummary.get(context);
    }

    public ApiEndpoints getApiEndpoints() {
        if (apiEndpoints == null) {
            apiEndpoints = RestApiFactory.create();
        }
        return apiEndpoints;
    }

    public void setRestApi(ApiEndpoints apiEndpoints) {
        this.apiEndpoints = apiEndpoints;
    }

    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            scheduler = Schedulers.io();
        }

        return scheduler;
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }
}
