package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.*
import android.content.Context
import android.support.annotation.VisibleForTesting
import id.co.ajsmsig.nb.pointofsale.custom.sqliteasset.AssetSQLiteOpenHelperFactory
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.LiveData
import android.arch.persistence.room.migration.Migration
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao.*


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Database(entities = [
    Page::class,
    PageOption::class,
    PageScenario::class,
    Input::class,
    InputType::class,
    InputChoice::class,
    ProductRecommendation::class,
    RiskProfileQuestionnaire::class,
    RiskProfileScoring::class,
    CalculatorResult::class,
    HandlingObjection::class,
    PageToPageOption::class,
    RecommendationSummaryField::class,
    RecommendationSummaryFieldToPageOption::class
],
        version = 1)
abstract class PrePopulatedDB: RoomDatabase() {

    private val mIsDatabaseCreated = MutableLiveData<Boolean>()

    abstract fun pageDao(): PageDao
    abstract fun pageOptionDao(): PageOptionDao
    abstract fun pageToPageOptionDao(): PageToPageOptionDao
    abstract fun pageScenarioDao(): PageScenarioDao
    abstract fun inputTypeDao(): InputTypeDao
    abstract fun inputDao(): InputDao
    abstract fun inputChoiceDao(): InputChoiceDao
    abstract fun riskProfileQuestionnaireDao(): RiskProfileQuestionnaireDao
    abstract fun riskProfileScoringDao(): RiskProfileScoringDao
    abstract fun calculatorResultDao(): CalculatorResultDao
    abstract fun handlingObjectionDao(): HandlingObjectionDao
    abstract fun productRecommendationDao(): ProductRecommendationDao
    abstract fun recommendationSummaryFieldDao(): RecommendationSummaryFieldDao

    /**
     * Check whether the database already exists and expose it via [.getDatabaseCreated]
     */
    fun updateDatabaseCreated(context: Context) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            setDatabaseCreated()
        }
    }


    private fun setDatabaseCreated() {
        mIsDatabaseCreated.postValue(true)
    }

    fun getDatabaseCreated(): LiveData<Boolean> {
        return mIsDatabaseCreated
    }


    companion object {

        private var instance: PrePopulatedDB? = null

        fun getInstance(context: Context): PrePopulatedDB {
            synchronized(PrePopulatedDB::class.java) {

                if (instance == null) {
                    instance = buildDatabase(context.applicationContext)
                    instance?.updateDatabaseCreated(context.applicationContext)
                }
            }

            return instance!!
        }

        @VisibleForTesting
        val DATABASE_NAME = "PosDB.db"

        private fun buildDatabase(context: Context): PrePopulatedDB {
            return  Room.databaseBuilder(context, PrePopulatedDB::class.java, DATABASE_NAME)
                    .openHelperFactory(AssetSQLiteOpenHelperFactory())
                    .build()
        }


        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // Since we didn't alter the table, there's nothing else to do here.

            }
        }
    }

}