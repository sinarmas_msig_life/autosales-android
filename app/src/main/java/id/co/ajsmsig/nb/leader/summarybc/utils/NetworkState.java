package id.co.ajsmsig.nb.leader.summarybc.utils;

public class NetworkState {
    public enum Status {
        RUNNING,
        SUCCESS,
        FAILED,
        NODATASUMMARY
    }


    private final Status status;
    private final String msg;

    public static final NetworkState LOADED;
    public static final NetworkState LOADING;
    public static final NetworkState NODATA;
    public static final NetworkState FAILED ;

    public NetworkState(Status status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    static {
        FAILED = new NetworkState(Status.FAILED,"Failed");
        NODATA = new NetworkState(Status.NODATASUMMARY, "Nodata");
        LOADED = new NetworkState(Status.SUCCESS, "Success");
        LOADING = new NetworkState(Status.RUNNING, "Running");
    }

    public Status getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }
}
