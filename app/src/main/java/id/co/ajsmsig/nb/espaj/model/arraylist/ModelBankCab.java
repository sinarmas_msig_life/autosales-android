/**
 * @author Eriza Siti Mulyani
 *
 */
package id.co.ajsmsig.nb.espaj.model.arraylist;

public class ModelBankCab{
	private int LSBP_ID;
	private int LBN_ID;
	private String LBN_NAMA;
	private int LSBP_PANJANG_REKENING;

	public ModelBankCab() {
	}

	public ModelBankCab(int LSBP_ID, int lBN_ID, String lBN_NAMA, int LSBP_PANJANG_REKENING) {
		super();
		this.LSBP_ID = LSBP_ID;
		this.LBN_ID = lBN_ID;
		this.LBN_NAMA = lBN_NAMA;
		this.LSBP_PANJANG_REKENING = LSBP_PANJANG_REKENING;
	}

	public int getLSBP_ID() {
		return LSBP_ID;
	}

	public void setLSBP_ID(int LSBP_ID) {
		this.LSBP_ID = LSBP_ID;
	}

	public int getLBN_ID() {
		return LBN_ID;
	}

	public void setLBN_ID(int LBN_ID) {
		this.LBN_ID = LBN_ID;
	}

	public String getLBN_NAMA() {
		return LBN_NAMA;
	}

	public void setLBN_NAMA(String LBN_NAMA) {
		this.LBN_NAMA = LBN_NAMA;
	}

	public int getLSBP_PANJANG_REKENING() {
		return LSBP_PANJANG_REKENING;
	}

	public void setLSBP_PANJANG_REKENING(int LSBP_PANJANG_REKENING) {
		this.LSBP_PANJANG_REKENING = LSBP_PANJANG_REKENING;
	}

	public String toString() {
		  return LBN_NAMA;
	  }
}