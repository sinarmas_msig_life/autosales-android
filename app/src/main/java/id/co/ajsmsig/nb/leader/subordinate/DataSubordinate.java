
package id.co.ajsmsig.nb.leader.subordinate;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataSubordinate {

    @SerializedName("user_info")
    @Expose
    private List<UserInfo> userInfo = null;
    @SerializedName("subordinate")
    @Expose
    private List<Subordinate> subordinate = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DataSubordinate() {
    }

    /**
     * 
     * @param subordinate
     * @param userInfo
     */
    public DataSubordinate(List<UserInfo> userInfo, List<Subordinate> subordinate) {
        super();
        this.userInfo = userInfo;
        this.subordinate = subordinate;
    }

    public List<UserInfo> getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(List<UserInfo> userInfo) {
        this.userInfo = userInfo;
    }

    public List<Subordinate> getSubordinate() {
        return subordinate;
    }

    public void setSubordinate(List<Subordinate> subordinate) {
        this.subordinate = subordinate;
    }

}
