package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.RiskProfileQuestionnaireAllAnswers


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface RiskProfileQuestionnaireDao {

    @Query("SELECT * from RiskProfileQuestionnaire")
    fun loadAllRiskProfileQuestionnaire(): LiveData<List<RiskProfileQuestionnaireAllAnswers>>

    @Query("SELECT * from RiskProfileQuestionnaire where parentId is null order by id")
    fun loadAllQuestion(): LiveData<List<RiskProfileQuestionnaireAllAnswers>>

    @Query("SELECT * from RiskProfileQuestionnaire where parentId = :parentId")
    fun loadAllAnswerWithQuestionId(parentId: Int): LiveData<List<RiskProfileQuestionnaireAllAnswers>>


}