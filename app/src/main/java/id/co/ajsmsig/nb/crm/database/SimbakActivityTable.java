package id.co.ajsmsig.nb.crm.database;
/*
 *Created by faiz_f on 08/05/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.HashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.form.SimbakActivityModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakSpajModel;

public class SimbakActivityTable {
    private Context context;

    public SimbakActivityTable(Context context) {
        this.context = context;
    }

    public HashMap<String, String> getProjectionMap() {
        HashMap<String, String> projectionMap = new HashMap<>();
        projectionMap.put(context.getString(R.string.SLA_TAB_ID), context.getString(R.string.SLA_TAB_ID) + " AS " + context.getString(R.string._ID));
        projectionMap.put(context.getString(R.string.SLA_ID), context.getString(R.string.SLA_ID));
        projectionMap.put(context.getString(R.string.SLA_INST_ID), context.getString(R.string.SLA_INST_ID));
        projectionMap.put(context.getString(R.string.SLA_INST_TAB_ID), context.getString(R.string.SLA_INST_TAB_ID));
        projectionMap.put(context.getString(R.string.SLA_NAME), context.getString(R.string.SLA_NAME));
        projectionMap.put(context.getString(R.string.SLA_DETAIL), context.getString(R.string.SLA_DETAIL));
        projectionMap.put(context.getString(R.string.SLA_PREMI), context.getString(R.string.SLA_PREMI));
        projectionMap.put(context.getString(R.string.SLA_IDATE), context.getString(R.string.SLA_IDATE));
        projectionMap.put(context.getString(R.string.SLA_SDATE), context.getString(R.string.SLA_SDATE));
        projectionMap.put(context.getString(R.string.SLA_EDATE), context.getString(R.string.SLA_EDATE));
        projectionMap.put(context.getString(R.string.SLA_REMINDER_DATE), context.getString(R.string.SLA_REMINDER_DATE));
        projectionMap.put(context.getString(R.string.SLA_CRTD_DATE), context.getString(R.string.SLA_CRTD_DATE));
        projectionMap.put(context.getString(R.string.SLA_CRTD_ID), context.getString(R.string.SLA_CRTD_ID));
        projectionMap.put(context.getString(R.string.SLA_OWNER_ID), context.getString(R.string.SLA_OWNER_ID));
        projectionMap.put(context.getString(R.string.SLA_UPDTD_DATE), context.getString(R.string.SLA_UPDTD_DATE));
        projectionMap.put(context.getString(R.string.SLA_UPDTD_ID), context.getString(R.string.SLA_UPDTD_ID));
        projectionMap.put(context.getString(R.string.SLA_LON), context.getString(R.string.SLA_LON));
        projectionMap.put(context.getString(R.string.SLA_LAT), context.getString(R.string.SLA_LAT));
        projectionMap.put(context.getString(R.string.SLA_NEXT_ID), context.getString(R.string.SLA_NEXT_ID));
        projectionMap.put(context.getString(R.string.SLA_REFF_ID), context.getString(R.string.SLA_REFF_ID));
        projectionMap.put(context.getString(R.string.SLA_OLD_ID), context.getString(R.string.SLA_OLD_ID));
        projectionMap.put(context.getString(R.string.SLA_INST_TYPE), context.getString(R.string.SLA_INST_TYPE));
        projectionMap.put(context.getString(R.string.SLA_ACTIVE), context.getString(R.string.SLA_ACTIVE));
        projectionMap.put(context.getString(R.string.SLA_TYPE), context.getString(R.string.SLA_TYPE));
        projectionMap.put(context.getString(R.string.SLA_SUBTYPE), context.getString(R.string.SLA_SUBTYPE));
        projectionMap.put(context.getString(R.string.SLA_CORP_STAGE), context.getString(R.string.SLA_CORP_STAGE));
        projectionMap.put(context.getString(R.string.SLA_STATUS), context.getString(R.string.SLA_STATUS));
        projectionMap.put(context.getString(R.string.SLA_REMINDER_TYPE), context.getString(R.string.SLA_REMINDER_TYPE));
        projectionMap.put(context.getString(R.string.SLA_OWNER_TYPE), context.getString(R.string.SLA_OWNER_TYPE));
        projectionMap.put(context.getString(R.string.SLA_OLD_TYPE), context.getString(R.string.SLA_OLD_TYPE));
        projectionMap.put(context.getString(R.string.SLA_OLD_SUBTYPE), context.getString(R.string.SLA_OLD_SUBTYPE));
        projectionMap.put(context.getString(R.string.SLA_USER_ID), context.getString(R.string.SLA_USER_ID));
        projectionMap.put(context.getString(R.string.SLA_RPT_CAT), context.getString(R.string.SLA_RPT_CAT));
        projectionMap.put(context.getString(R.string.SLA_NXT_ACTION), context.getString(R.string.SLA_NXT_ACTION));
        projectionMap.put(context.getString(R.string.SLA_CURR_LVL), context.getString(R.string.SLA_CURR_LVL));
        projectionMap.put(context.getString(R.string.SLA_CORP_COMMEN), context.getString(R.string.SLA_CORP_COMMEN));
        projectionMap.put(context.getString(R.string.SLA_PREMI2), context.getString(R.string.SLA_PREMI2));
        projectionMap.put(context.getString(R.string.SLA_PREMI3), context.getString(R.string.SLA_PREMI3));
        projectionMap.put(context.getString(R.string.SLA_SRC), context.getString(R.string.SLA_SRC));
        projectionMap.put(context.getString(R.string.SLA_LAST_POS), context.getString(R.string.SLA_LAST_POS));
        projectionMap.put(context.getString(R.string.SL_ID_), context.getString(R.string.SL_ID_));
        projectionMap.put(context.getString(R.string.SL_NAME), context.getString(R.string.SL_NAME));

        return projectionMap;
    }

    public HashMap<String, String> getProjectionMapJoinTable() {
        HashMap<String, String> projectionMap = new HashMap<>();
        projectionMap.put(context.getString(R.string.SLA_TAB_ID), context.getString(R.string.SLA_TAB_ID) + " AS " + context.getString(R.string._ID));
        projectionMap.put(context.getString(R.string.SL_NAME), context.getString(R.string.SL_NAME));
        projectionMap.put(context.getString(R.string.LABEL), "(CASE WHEN JSON_SUB_ACTIVITY.LABEL IS NOT NULL THEN JSON_ACTIVITY.LABEL || ' ' || JSON_SUB_ACTIVITY.LABEL ELSE JSON_ACTIVITY.LABEL END)  LABEL");
        projectionMap.put(context.getString(R.string.SLA_SDATE), context.getString(R.string.SLA_SDATE));
        projectionMap.put(context.getString(R.string.SLA_LAST_POS), context.getString(R.string.SLA_LAST_POS));
        projectionMap.put(context.getString(R.string.SLA_UPDTD_DATE), context.getString(R.string.QUERY_CASE_DATE_ACTIVITY_NAME));

        return projectionMap;
    }

    public HashMap<String, String> getProjectionMapLeadActivity() {
        HashMap<String, String> projectionMap = new HashMap<>();
        projectionMap.put(context.getString(R.string.SLA_TAB_ID), context.getString(R.string.SLA_TAB_ID) + " AS " + context.getString(R.string._ID));
        projectionMap.put(context.getString(R.string.LABEL), "(CASE WHEN JSON_SUB_ACTIVITY.LABEL IS NOT NULL THEN JSON_ACTIVITY.LABEL || ' ' || JSON_SUB_ACTIVITY.LABEL ELSE JSON_ACTIVITY.LABEL END)  LABEL");
        projectionMap.put(context.getString(R.string.SLA_SDATE), context.getString(R.string.SLA_SDATE));
        projectionMap.put(context.getString(R.string.SLA_LAST_POS), context.getString(R.string.SLA_LAST_POS));
        projectionMap.put(context.getString(R.string.SLA_UPDTD_DATE), context.getString(R.string.QUERY_CASE_DATE_ACTIVITY_NAME));

        return projectionMap;
    }

    public HashMap<String, String> getProjectionMapForListActivity() {
        HashMap<String, String> projectionMap = new HashMap<>();
        projectionMap.put(context.getString(R.string.SLA_TAB_ID), context.getString(R.string.SLA_TAB_ID) + " AS " + context.getString(R.string._ID));
        projectionMap.put(context.getString(R.string.LABEL), "(CASE WHEN JSON_SUB_ACTIVITY.LABEL IS NOT NULL THEN JSON_ACTIVITY.LABEL || ' ' || JSON_SUB_ACTIVITY.LABEL ELSE JSON_ACTIVITY.LABEL END)  LABEL");
        projectionMap.put(context.getString(R.string.SLA_SDATE), context.getString(R.string.SLA_SDATE));
        projectionMap.put(context.getString(R.string.SLA_LAST_POS), context.getString(R.string.SLA_LAST_POS));
        projectionMap.put(context.getString(R.string.SLA_UPDTD_DATE), context.getString(R.string.QUERY_CASE_DATE_ACTIVITY_NAME));

        return projectionMap;
    }

    public HashMap<String, String> getProjectionMapProfile() {
        HashMap<String, String> projectionMap = new HashMap<>();
        projectionMap.put(context.getString(R.string.SL_TAB_ID), context.getString(R.string.SL_TAB_ID) + " AS " + context.getString(R.string._ID));
        projectionMap.put(context.getString(R.string.SL_NAME), context.getString(R.string.SL_NAME));
        projectionMap.put(context.getString(R.string.SL_BDATE), context.getString(R.string.SL_BDATE));

        return projectionMap;
    }


    ArrayList<SimbakActivityModel> getArrayObject(Cursor cursor) {
        ArrayList<SimbakActivityModel> simbakActivityModels = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            SimbakActivityModel simbakActivityModel = new SimbakActivityModel();
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_TAB_ID))))
                simbakActivityModel.setSLA_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_ID))))
                simbakActivityModel.setSLA_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_INST_ID))))
                simbakActivityModel.setSLA_INST_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_INST_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_INST_TAB_ID))))
                simbakActivityModel.setSLA_INST_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_INST_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_NAME))))
                simbakActivityModel.setSLA_NAME(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_NAME))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_DETAIL))))
                simbakActivityModel.setSLA_DETAIL(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_DETAIL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI))))
                simbakActivityModel.setSLA_PREMI(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_IDATE))))
                simbakActivityModel.setSLA_IDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_IDATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_SDATE))))
                simbakActivityModel.setSLA_SDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_SDATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_EDATE))))
                simbakActivityModel.setSLA_EDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_EDATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_REMINDER_DATE))))
                simbakActivityModel.setSLA_REMINDER_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_REMINDER_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CRTD_DATE))))
                simbakActivityModel.setSLA_CRTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_CRTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CRTD_ID))))
                simbakActivityModel.setSLA_CRTD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_CRTD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OWNER_ID))))
                simbakActivityModel.setSLA_OWNER_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_OWNER_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_DATE))))
                simbakActivityModel.setSLA_UPDTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_ID))))
                simbakActivityModel.setSLA_UPDTD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_LON))))
                simbakActivityModel.setSLA_LON(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_LON))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_LAT))))
                simbakActivityModel.setSLA_LAT(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_LAT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_NEXT_ID))))
                simbakActivityModel.setSLA_NEXT_ID(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_NEXT_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_REFF_ID))))
                simbakActivityModel.setSLA_REFF_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_REFF_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_ID))))
                simbakActivityModel.setSLA_OLD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_INST_TYPE))))
                simbakActivityModel.setSLA_INST_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_INST_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_ACTIVE))))
                simbakActivityModel.setSLA_ACTIVE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_ACTIVE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_TYPE))))
                simbakActivityModel.setSLA_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_SUBTYPE))))
                simbakActivityModel.setSLA_SUBTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_SUBTYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CORP_STAGE))))
                simbakActivityModel.setSLA_CORP_STAGE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_CORP_STAGE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_STATUS))))
                simbakActivityModel.setSLA_STATUS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_STATUS))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_REMINDER_TYPE))))
                simbakActivityModel.setSLA_REMINDER_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_REMINDER_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OWNER_TYPE))))
                simbakActivityModel.setSLA_OWNER_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_OWNER_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_TYPE))))
                simbakActivityModel.setSLA_OLD_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_SUBTYPE))))
                simbakActivityModel.setSLA_OLD_SUBTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_SUBTYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_USER_ID))))
                simbakActivityModel.setSLA_USER_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_USER_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_RPT_CAT))))
                simbakActivityModel.setSLA_RPT_CAT(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_RPT_CAT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_NXT_ACTION))))
                simbakActivityModel.setSLA_NXT_ACTION(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_NXT_ACTION))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CURR_LVL))))
                simbakActivityModel.setSLA_CURR_LVL(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_CURR_LVL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CORP_COMMEN))))
                simbakActivityModel.setSLA_CORP_COMMEN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_CORP_COMMEN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI2))))
                simbakActivityModel.setSLA_PREMI2(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI2))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI3))))
                simbakActivityModel.setSLA_PREMI3(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI3))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_SRC))))
                simbakActivityModel.setSLA_SRC(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_SRC))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_LAST_POS))))
                simbakActivityModel.setSLA_LAST_POS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_LAST_POS))));

            SimbakSpajModel simbakSpajModel =  new C_Select(context).selectSimbakSpajModelByActivityId(simbakActivityModel.getSLA_TAB_ID());
            if(simbakSpajModel!=null)
                simbakActivityModel.setSPAJ(simbakSpajModel);

            simbakActivityModels.add(simbakActivityModel);
            cursor.moveToNext();
        }
        cursor.close();
        return simbakActivityModels;
    }

    public SimbakActivityModel getObject(Cursor cursor) {
        SimbakActivityModel simbakActivityModel = new SimbakActivityModel();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_TAB_ID))))
                simbakActivityModel.setSLA_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_ID))))
                simbakActivityModel.setSLA_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_INST_ID))))
                simbakActivityModel.setSLA_INST_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_INST_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_INST_TAB_ID))))
                simbakActivityModel.setSLA_INST_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SLA_INST_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_NAME))))
                simbakActivityModel.setSLA_NAME(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_NAME))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_DETAIL))))
                simbakActivityModel.setSLA_DETAIL(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_DETAIL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI))))
                simbakActivityModel.setSLA_PREMI(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_IDATE))))
                simbakActivityModel.setSLA_IDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_IDATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_SDATE))))
                simbakActivityModel.setSLA_SDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_SDATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_EDATE))))
                simbakActivityModel.setSLA_EDATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_EDATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_REMINDER_DATE))))
                simbakActivityModel.setSLA_REMINDER_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_REMINDER_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CRTD_DATE))))
                simbakActivityModel.setSLA_CRTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_CRTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CRTD_ID))))
                simbakActivityModel.setSLA_CRTD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_CRTD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OWNER_ID))))
                simbakActivityModel.setSLA_OWNER_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_OWNER_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_DATE))))
                simbakActivityModel.setSLA_UPDTD_DATE(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_DATE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_ID))))
                simbakActivityModel.setSLA_UPDTD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_UPDTD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_LON))))
                simbakActivityModel.setSLA_LON(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_LON))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_LAT))))
                simbakActivityModel.setSLA_LAT(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_LAT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_NEXT_ID))))
                simbakActivityModel.setSLA_NEXT_ID(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_NEXT_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_REFF_ID))))
                simbakActivityModel.setSLA_REFF_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_REFF_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_ID))))
                simbakActivityModel.setSLA_OLD_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_INST_TYPE))))
                simbakActivityModel.setSLA_INST_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_INST_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_ACTIVE))))
                simbakActivityModel.setSLA_ACTIVE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_ACTIVE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_TYPE))))
                simbakActivityModel.setSLA_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_SUBTYPE))))
                simbakActivityModel.setSLA_SUBTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_SUBTYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CORP_STAGE))))
                simbakActivityModel.setSLA_CORP_STAGE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_CORP_STAGE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_STATUS))))
                simbakActivityModel.setSLA_STATUS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_STATUS))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_REMINDER_TYPE))))
                simbakActivityModel.setSLA_REMINDER_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_REMINDER_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OWNER_TYPE))))
                simbakActivityModel.setSLA_OWNER_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_OWNER_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_TYPE))))
                simbakActivityModel.setSLA_OLD_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_SUBTYPE))))
                simbakActivityModel.setSLA_OLD_SUBTYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_OLD_SUBTYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_USER_ID))))
                simbakActivityModel.setSLA_USER_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_USER_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_RPT_CAT))))
                simbakActivityModel.setSLA_RPT_CAT(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_RPT_CAT))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_NXT_ACTION))))
                simbakActivityModel.setSLA_NXT_ACTION(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_NXT_ACTION))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CURR_LVL))))
                simbakActivityModel.setSLA_CURR_LVL(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_CURR_LVL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_CORP_COMMEN))))
                simbakActivityModel.setSLA_CORP_COMMEN(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SLA_CORP_COMMEN))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI2))))
                simbakActivityModel.setSLA_PREMI2(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI2))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI3))))
                simbakActivityModel.setSLA_PREMI3(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_PREMI3))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_SRC))))
                simbakActivityModel.setSLA_SRC(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_SRC))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SLA_LAST_POS))))
                simbakActivityModel.setSLA_LAST_POS(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SLA_LAST_POS))));

            cursor.close();
        }
        return simbakActivityModel;
    }

    public ContentValues toContentValues(SimbakActivityModel simbakActivityModel) {
        ContentValues contentValues = new ContentValues();

//        if (simbakActivityModel.getSLA_TAB_ID() != null) {
        contentValues.put(context.getString(R.string.SLA_TAB_ID), simbakActivityModel.getSLA_TAB_ID());
//        }
//        if (simbakActivityModel.getSLA_ID() != null) {
        contentValues.put(context.getString(R.string.SLA_ID), simbakActivityModel.getSLA_ID());
//        }
//        if (simbakActivityModel.getSLA_INST_ID() != null) {
        contentValues.put(context.getString(R.string.SLA_INST_ID), simbakActivityModel.getSLA_INST_ID());
//        }
//        if (simbakActivityModel.getSLA_INST_TAB_ID() != null) {
        contentValues.put(context.getString(R.string.SLA_INST_TAB_ID), simbakActivityModel.getSLA_INST_TAB_ID());
//        }
//        if (simbakActivityModel.getSLA_NAME() != null) {
        contentValues.put(context.getString(R.string.SLA_NAME), simbakActivityModel.getSLA_NAME());
//        }
//        if (simbakActivityModel.getSLA_DETAIL() != null) {
        contentValues.put(context.getString(R.string.SLA_DETAIL), simbakActivityModel.getSLA_DETAIL());
//        }
//        if (simbakActivityModel.getSLA_PREMI() != null) {
        contentValues.put(context.getString(R.string.SLA_PREMI), simbakActivityModel.getSLA_PREMI());
//        }
//        if (simbakActivityModel.getSLA_IDATE() != null) {
        contentValues.put(context.getString(R.string.SLA_IDATE), simbakActivityModel.getSLA_IDATE());
//        }
//        if (simbakActivityModel.getSLA_SDATE() != null) {
        contentValues.put(context.getString(R.string.SLA_SDATE), simbakActivityModel.getSLA_SDATE());
//        }
//        if (simbakActivityModel.getSLA_EDATE() != null) {
        contentValues.put(context.getString(R.string.SLA_EDATE), simbakActivityModel.getSLA_EDATE());
//        }
//        if (simbakActivityModel.getSLA_REMINDER_DATE() != null) {
        contentValues.put(context.getString(R.string.SLA_REMINDER_DATE), simbakActivityModel.getSLA_REMINDER_DATE());
//        }
//        if (simbakActivityModel.getSLA_CRTD_DATE() != null) {
        contentValues.put(context.getString(R.string.SLA_CRTD_DATE), simbakActivityModel.getSLA_CRTD_DATE());
//        }
//        if (simbakActivityModel.getSLA_CRTD_ID() != null) {
        contentValues.put(context.getString(R.string.SLA_CRTD_ID), simbakActivityModel.getSLA_CRTD_ID());
//        }
//        if (simbakActivityModel.getSLA_OWNER_ID() != null) {
        contentValues.put(context.getString(R.string.SLA_OWNER_ID), simbakActivityModel.getSLA_OWNER_ID());
//        }
//        if (simbakActivityModel.getSLA_UPDTD_DATE() != null) {
        contentValues.put(context.getString(R.string.SLA_UPDTD_DATE), simbakActivityModel.getSLA_UPDTD_DATE());
//        }
//        if (simbakActivityModel.getSLA_UPDTD_ID() != null) {
        contentValues.put(context.getString(R.string.SLA_UPDTD_ID), simbakActivityModel.getSLA_UPDTD_ID());
//        }
//        if (simbakActivityModel.getSLA_LON() != null) {
        contentValues.put(context.getString(R.string.SLA_LON), simbakActivityModel.getSLA_LON());
//        }
//        if (simbakActivityModel.getSLA_LAT() != null) {
        contentValues.put(context.getString(R.string.SLA_LAT), simbakActivityModel.getSLA_LAT());
//        }
//        if (simbakActivityModel.getSLA_NEXT_ID() != null) {
        contentValues.put(context.getString(R.string.SLA_NEXT_ID), simbakActivityModel.getSLA_NEXT_ID());
//        }
//        if (simbakActivityModel.getSLA_REFF_ID() != null) {
        contentValues.put(context.getString(R.string.SLA_REFF_ID), simbakActivityModel.getSLA_REFF_ID());
//        }
//        if (simbakActivityModel.getSLA_OLD_ID() != null) {
        contentValues.put(context.getString(R.string.SLA_OLD_ID), simbakActivityModel.getSLA_OLD_ID());
//        }
//        if (simbakActivityModel.getSLA_INST_TYPE() != null) {
        contentValues.put(context.getString(R.string.SLA_INST_TYPE), simbakActivityModel.getSLA_INST_TYPE());
//        }
//        if (simbakActivityModel.getSLA_ACTIVE() != null) {
        contentValues.put(context.getString(R.string.SLA_ACTIVE), simbakActivityModel.getSLA_ACTIVE());
//        }
//        if (simbakActivityModel.getSLA_TYPE() != null) {
        contentValues.put(context.getString(R.string.SLA_TYPE), simbakActivityModel.getSLA_TYPE());
//        }
//        if (simbakActivityModel.getSLA_SUBTYPE() != null) {
        contentValues.put(context.getString(R.string.SLA_SUBTYPE), simbakActivityModel.getSLA_SUBTYPE());
//        }
//        if (simbakActivityModel.getSLA_CORP_STAGE() != null) {
        contentValues.put(context.getString(R.string.SLA_CORP_STAGE), simbakActivityModel.getSLA_CORP_STAGE());
//        }
//        if (simbakActivityModel.getSLA_STATUS() != null) {
        contentValues.put(context.getString(R.string.SLA_STATUS), simbakActivityModel.getSLA_STATUS());
//        }
//        if (simbakActivityModel.getSLA_REMINDER_TYPE() != null) {
        contentValues.put(context.getString(R.string.SLA_REMINDER_TYPE), simbakActivityModel.getSLA_REMINDER_TYPE());
//        }
//        if (simbakActivityModel.getSLA_OWNER_TYPE() != null) {
        contentValues.put(context.getString(R.string.SLA_OWNER_TYPE), simbakActivityModel.getSLA_OWNER_TYPE());
//        }
//        if (simbakActivityModel.getSLA_OLD_TYPE() != null) {
        contentValues.put(context.getString(R.string.SLA_OLD_TYPE), simbakActivityModel.getSLA_OLD_TYPE());
//        }
//        if (simbakActivityModel.getSLA_OLD_SUBTYPE() != null) {
        contentValues.put(context.getString(R.string.SLA_OLD_SUBTYPE), simbakActivityModel.getSLA_OLD_SUBTYPE());
//        }
//        if (simbakActivityModel.getSLA_USER_ID() != null) {
        contentValues.put(context.getString(R.string.SLA_USER_ID), simbakActivityModel.getSLA_USER_ID());
//        }
//        if (simbakActivityModel.getSLA_RPT_CAT() != null) {
        contentValues.put(context.getString(R.string.SLA_RPT_CAT), simbakActivityModel.getSLA_RPT_CAT());
//        }
        //        if (simbakActivityModel.getSLA_NXT_ACTION() != null) {
        contentValues.put(context.getString(R.string.SLA_NXT_ACTION), simbakActivityModel.getSLA_NXT_ACTION());
//        }
//        if (simbakActivityModel.getSLA_CURR_LVL() != null) {
        contentValues.put(context.getString(R.string.SLA_CURR_LVL), simbakActivityModel.getSLA_CURR_LVL());
//        }
        //        if (simbakActivityModel.getSLA_CORP_COMMEN() != null) {
        contentValues.put(context.getString(R.string.SLA_CORP_COMMEN), simbakActivityModel.getSLA_CORP_COMMEN());
//        }
        //        if (simbakActivityModel.getSLA_PREMI2() != null) {
        contentValues.put(context.getString(R.string.SLA_PREMI2), simbakActivityModel.getSLA_PREMI2());
//        }
        //        if (simbakActivityModel.getSLA_PREMI3() != null) {
        contentValues.put(context.getString(R.string.SLA_PREMI3), simbakActivityModel.getSLA_PREMI3());
//        }
        //        if (simbakActivityModel.getSLA_LAST_POS() != null) {
        contentValues.put(context.getString(R.string.SLA_LAST_POS), simbakActivityModel.getSLA_LAST_POS());

        contentValues.put(context.getString(R.string.SLA_SRC), simbakActivityModel.getSLA_SRC());
        contentValues.put(context.getString(R.string.SL_ID_), simbakActivityModel.getSL_ID_());
        contentValues.put(context.getString(R.string.SL_NAME), simbakActivityModel.getSL_NAME());
        return contentValues;
    }
}
