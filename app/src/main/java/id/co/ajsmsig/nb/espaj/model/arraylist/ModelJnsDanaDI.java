package id.co.ajsmsig.nb.espaj.model.arraylist;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 30/08/2017.
 */

public class ModelJnsDanaDI implements Parcelable{

    private int counter = 0;
    private String jenis_dana="";
    private Double jumlah_alokasi=(double)0;
    private Double persen_alokasi=(double)0;
    private Boolean validation = true;

    public ModelJnsDanaDI() {
    }

    protected ModelJnsDanaDI(Parcel in) {
        counter = in.readInt();
        jenis_dana = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(counter);
        dest.writeString(jenis_dana);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelJnsDanaDI> CREATOR = new Creator<ModelJnsDanaDI>() {
        @Override
        public ModelJnsDanaDI createFromParcel(Parcel in) {
            return new ModelJnsDanaDI(in);
        }

        @Override
        public ModelJnsDanaDI[] newArray(int size) {
            return new ModelJnsDanaDI[size];
        }
    };

    public ModelJnsDanaDI(int counter, String jenis_dana, Double jumlah_alokasi, Double persen_alokasi, Boolean validation) {
        this.counter = counter;
        this.jenis_dana = jenis_dana;
        this.jumlah_alokasi = jumlah_alokasi;
        this.persen_alokasi = persen_alokasi;
        this.validation = validation;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getJenis_dana() {
        return jenis_dana;
    }

    public void setJenis_dana(String jenis_dana) {
        this.jenis_dana = jenis_dana;
    }

    public Double getJumlah_alokasi() {
        return jumlah_alokasi;
    }

    public void setJumlah_alokasi(Double jumlah_alokasi) {
        this.jumlah_alokasi = jumlah_alokasi;
    }

    public Double getPersen_alokasi() {
        return persen_alokasi;
    }

    public void setPersen_alokasi(Double persen_alokasi) {
        this.persen_alokasi = persen_alokasi;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public static Creator<ModelJnsDanaDI> getCREATOR() {
        return CREATOR;
    }
}
