/*
package id.co.ajsmsig.nb.pointofsale.binding

import android.arch.lifecycle.MediatorLiveData
import android.databinding.BindingAdapter
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import id.co.ajsmsig.nb.pointofsale.adapter.*
import id.co.ajsmsig.nb.pointofsale.custom.flexbox.FlexDirection
import id.co.ajsmsig.nb.pointofsale.custom.flexbox.FlexboxLayoutManager
import id.co.ajsmsig.nb.pointofsale.custom.flexbox.JustifyContent
import id.co.ajsmsig.nb.pointofsale.handler.SelectionHandler
import id.co.ajsmsig.nb.pointofsale.model.Resource
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductBenefit
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductRider
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.ProductDetail
import id.co.ajsmsig.nb.pointofsale.utils.splitIfTwoWords

*/
/**
 * Created by andreyyoshuamanik on 11/03/18.
 *//*


@BindingAdapter("options", "optionRow", "optionColumn", "optionSplitTwoLines", requireAll = false)
fun setOptionToRecyclerView(recyclerView: android.support.v7.widget.RecyclerView, pageOptions: MediatorLiveData<List<PageOption>>, row: Int?, column: Int?, optionSplitTwoLines: Boolean = false) {

    pageOptions.observeForever {
        it?.let {

            val optionAdapter = OptionAdapter(optionSplitTwoLines)
            optionAdapter.options = it.toTypedArray()
            recyclerView.adapter = optionAdapter
            if (row != null) {
                var layoutManager = FlexboxLayoutManager(recyclerView.context, FlexDirection.ROW, JustifyContent.CENTER, true, row)
                if (column != null) {
                    layoutManager = FlexboxLayoutManager(recyclerView.context, FlexDirection.ROW, JustifyContent.CENTER, true, row, column)
                }
                recyclerView.layoutManager = layoutManager

            }
            recyclerView.setHasFixedSize(true)
        }
    }
}


@BindingAdapter("selections", "selectionHandler", "selectionRow", "selectionColumn", requireAll = false)
fun setSelectionToRecyclerView(recyclerView: android.support.v7.widget.RecyclerView, pageOptions: MediatorLiveData<List<PageOption>>, handler: SelectionHandler, row: Int?, column: Int?) {
    pageOptions.observeForever {
        it?.let {

            val optionAdapter = SelectionAdapter(handler)
            optionAdapter.menus = it.toTypedArray()
            recyclerView.adapter = optionAdapter
            if (row != null) {
                var layoutManager = FlexboxLayoutManager(recyclerView.context, FlexDirection.ROW, JustifyContent.CENTER, true, row)
                if (column != null) {
                    layoutManager = FlexboxLayoutManager(recyclerView.context, FlexDirection.ROW, JustifyContent.CENTER, true, row, column)
                }
                recyclerView.layoutManager = layoutManager

            }
            recyclerView.setHasFixedSize(true)
        }
    }
}



@BindingAdapter("riders", "riderRow", "riderColumn", requireAll = false)
fun setRidersToRecyclerView(recyclerView: android.support.v7.widget.RecyclerView, pageOptions: MediatorLiveData<Resource<List<ProductRider>>>, row: Int?, column: Int?) {

    pageOptions.observeForever {
        it?.data?.let {

            if (recyclerView.adapter == null) {

                val optionAdapter = id.co.ajsmsig.nb.pointofsale.adapter.ProductRiderAdapter()
                optionAdapter.options = it.toTypedArray()

                recyclerView.adapter = optionAdapter
                recyclerView.layoutManager = GridLayoutManager(recyclerView.context, row ?: 3)
                recyclerView.setHasFixedSize(true)
                recyclerView.isNestedScrollingEnabled = true
            } else {
                (recyclerView.adapter as? ProductRiderAdapter)?.options = it.toTypedArray()
            }
        }
    }
}


@BindingAdapter("fabs", "fabRow", "fabColumn", requireAll = false)
fun setFABstoRecyclerView(recyclerView: android.support.v7.widget.RecyclerView, pageOptions: MediatorLiveData<Resource<List<ProductBenefit>>>, row: Int?, column: Int?) {
    pageOptions.observeForever {
        Log.i("INFO", "${it}")
        it?.data?.let {
            Log.i("INFO", "${it}")
            if (recyclerView.adapter == null) {

                val optionAdapter = ProductFABAdapter()
                optionAdapter.options = it.toTypedArray()

                recyclerView.adapter = optionAdapter
                recyclerView.layoutManager = GridLayoutManager(recyclerView.context, row ?: 2)
                recyclerView.setHasFixedSize(true)
                recyclerView.isNestedScrollingEnabled = true
            } else {
                (recyclerView.adapter as? ProductFABAdapter)?.options = it.toTypedArray()
            }
        }
    }
}

@BindingAdapter("productDetails", "loadingButtonListener")
fun setProductDetailstoRecyclerView(recyclerView: android.support.v7.widget.RecyclerView, productDetails: MediatorLiveData<List<ProductDetail>>?, loadingButtonListener: LoadingButtonListener?) {

    productDetails?.observeForever {
        it?.let {

            if (recyclerView.adapter == null) {

                val optionAdapter = ProductDetailAdapter()
                optionAdapter.options = it.toTypedArray()
                optionAdapter.listener = loadingButtonListener

                recyclerView.adapter = optionAdapter
                recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
                recyclerView.setHasFixedSize(true)
                recyclerView.isNestedScrollingEnabled = true

            } else {
                (recyclerView.adapter as? ProductDetailAdapter)?.options = it.toTypedArray()
                (recyclerView.adapter as? ProductDetailAdapter)?.listener = loadingButtonListener
            }
        }
    }
}
*/
