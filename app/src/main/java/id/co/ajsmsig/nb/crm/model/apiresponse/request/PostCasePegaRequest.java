
package id.co.ajsmsig.nb.crm.model.apiresponse.request;

public class PostCasePegaRequest {
    private String caseTypeID;
    private String processID;
    private String parentCaseID;
    private Content content;

    public PostCasePegaRequest(String caseTypeID, String processID, String parentCaseID, Content content) {
        this.caseTypeID = caseTypeID;
        this.processID = processID;
        this.parentCaseID = parentCaseID;
        this.content = content;
    }

}
