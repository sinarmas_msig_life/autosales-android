package id.co.ajsmsig.nb.pointofsale.ui.lproductpresentation


import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.google.android.exoplayer2.*

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.pointofsale.ui.fragment.BaseFragment
import com.google.android.exoplayer2.ui.PlaybackControlView
import id.co.ajsmsig.nb.pointofsale.adapter.ProductDetailAdapter
import id.co.ajsmsig.nb.pointofsale.custom.LoadingButton
import id.co.ajsmsig.nb.databinding.PosFragmentProductPresentationVideoBinding
import id.co.ajsmsig.nb.pointofsale.handler.SelectionHandler
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.ProductDetail
import id.co.ajsmsig.nb.pointofsale.utils.fileName
import java.io.File
import id.co.ajsmsig.nb.pointofsale.utils.*

/**
 * A simple [BaseFragment] subclass.
 */
class ProductPresentationVideoFragment : BaseFragment(), SelectionHandler {
    private lateinit var dataBinding: PosFragmentProductPresentationVideoBinding
    private lateinit var viewModel: ProductPresentationVideoVM

    private var mExoPlayerFullscreen = false
    private var mFullScreenButton: FrameLayout? = null
    private var mFullScreenIcon: ImageView? = null
    private var mFullScreenDialog: Dialog? = null

    private var mResumeWindow: Int = 0
    private var mResumePosition: Long = 0
    private var isVideoEnded = false

    private val STATE_RESUME_WINDOW = "resumeWindow"
    private val STATE_RESUME_POSITION = "resumePosition"
    private val STATE_PLAYER_FULLSCREEN = "playerFullscreen"

    /**
     * Permissions required to read and write contacts. Used by the [ContactsFragment].
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private val PERMISSIONS_STORAGE = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
    private val REQUEST_STORAGE: Int = 1

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductPresentationVideoVM::class.java)

        subscribeUi(viewModel)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)


        if (savedInstanceState != null) {
            mResumeWindow = savedInstanceState.getInt(STATE_RESUME_WINDOW)
            mResumePosition = savedInstanceState.getLong(STATE_RESUME_POSITION)
            mExoPlayerFullscreen = savedInstanceState.getBoolean(STATE_PLAYER_FULLSCREEN)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(STATE_RESUME_WINDOW, mResumeWindow)
        outState.putLong(STATE_RESUME_POSITION, mResumePosition)
        outState.putBoolean(STATE_PLAYER_FULLSCREEN, mExoPlayerFullscreen)


    }


    private fun initFullscreenDialog() {

        mFullScreenDialog = object : Dialog(activity, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            override fun onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog()
                super.onBackPressed()
            }
        }
    }

    private fun openFullscreenDialog() {

        (dataBinding.playerView.parent as ViewGroup).removeView(dataBinding.playerView)
        mFullScreenDialog?.addContentView(dataBinding.playerView, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        mFullScreenIcon?.setImageDrawable(ContextCompat.getDrawable(activity as Context, R.drawable.pos_ic_fullscreen_skrink))
        mExoPlayerFullscreen = true
        mFullScreenDialog?.show()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
    }


    private fun closeFullscreenDialog() {

        (dataBinding.playerView.parent as ViewGroup).removeView(dataBinding.playerView)
        dataBinding.playerFrame.addView(dataBinding.playerView)
        mExoPlayerFullscreen = false
        mFullScreenDialog?.dismiss()
        mFullScreenIcon?.setImageDrawable(ContextCompat.getDrawable(activity as Context, R.drawable.pos_ic_fullscreen_expand))
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_product_presentation_video, container, false)

        return dataBinding.root
    }


    @SuppressLint("ResourceType")
    fun subscribeUi(viewModel: ProductPresentationVideoVM) {
        dataBinding.vm = viewModel
        dataBinding.handler = this

        viewModel.page = page

        this.viewModel = viewModel
        mainVM.subtitle.set(sharedViewModel.selectedProduct?.LsbsName)


        getPermissionAndStartPlayer()
        if (mFullScreenDialog == null) initFullscreenDialog()


        val controlView = dataBinding.playerView.findViewById<PlaybackControlView>(R.id.exo_controller)
        mFullScreenIcon = controlView.findViewById(R.id.exo_fullscreen_icon)
        mFullScreenButton = controlView.findViewById(R.id.exo_fullscreen_button)
        mFullScreenButton?.setOnClickListener({
            if (!dataBinding.playerView.player.isLoading && (dataBinding.playerView.player as ExoPlayer).playbackState == Player.STATE_READY) {

                if (!mExoPlayerFullscreen)
                    openFullscreenDialog()
                else
                    closeFullscreenDialog()
            }
        })

        viewModel.observableIsThereRider.observe(this, Observer {

            dataBinding.root.findViewById<View?>(4000)?.visibility = if (it == true) View.VISIBLE  else View.GONE
        })



    }

    private fun getPermissionAndStartPlayer() {

        // Verify that all required contact permissions have been granted.
        if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Contacts permissions have not been granted.
            Log.i("INFO", "Storage permissions has NOT been granted. Requesting permissions.")
            requestStoragePermissions()

        } else {
            startVideoPlayer()
        }
    }

    private fun requestStoragePermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example, if the request has been denied previously.
            Log.i("INFO",
                    "Displaying contacts permission rationale to provide additional context.")

            // Display a SnackBar with an explanation and a button to trigger the request.

            Snackbar.make(dataBinding.root, R.string.permission_external_storage,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, {
                        requestPermissions(PERMISSIONS_STORAGE, REQUEST_STORAGE)
                    })
                    .show()
        } else {
            requestPermissions(PERMISSIONS_STORAGE, REQUEST_STORAGE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_STORAGE) {

            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startVideoPlayer()
            }

        }
    }

    private fun startVideoPlayer() {
        viewModel.startDownload.set(false) // For first time dont start player immediately
    }

    override fun onResume() {
        super.onResume()

        val haveResumePosition = mResumeWindow != C.INDEX_UNSET

        if (haveResumePosition) {
            dataBinding.playerView.player?.seekTo(mResumeWindow, mResumePosition)
        }

        if (mExoPlayerFullscreen) {
            (dataBinding.playerView.parent as ViewGroup).removeView(dataBinding.playerView)
            mFullScreenDialog?.addContentView(dataBinding.playerView, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
            mFullScreenIcon?.setImageDrawable(ContextCompat.getDrawable(activity as Context, R.drawable.pos_ic_fullscreen_skrink))
            mFullScreenDialog?.show()
        }
    }

    override fun onPause() {
        super.onPause()

        mResumeWindow = dataBinding.playerView.player?.currentWindowIndex ?: 0
        mResumePosition = Math.max(0, dataBinding.playerView.player?.contentPosition ?: 0)

        dataBinding.playerView.player?.release()

        if (mFullScreenDialog != null) mFullScreenDialog?.dismiss()
//        PRDownloader.cancelAll()
        viewModel.startDownload.set(false)
    }

    override fun onStop() {
        super.onStop()

        dataBinding.playerView.player?.stop(true)
    }

    override fun onClickSelection(selection: PageOption) {
        navigationController.optionSelected(selection)
    }

}// Required empty public constructor
