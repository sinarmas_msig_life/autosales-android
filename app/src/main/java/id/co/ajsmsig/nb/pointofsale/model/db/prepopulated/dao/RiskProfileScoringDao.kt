package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.RiskProfileScoring


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface RiskProfileScoringDao {

    @Query("SELECT * from RiskProfileScoring")
    fun loadAllRiskProfileScoring(): LiveData<List<RiskProfileScoring>>

    @Query("select * from RiskProfileScoring where RiskProfileScoring.startRange <= :score and RiskProfileScoring.endRange >= :score limit 1")
    fun loadRiskProfileScoringFromScore(score: Int?): LiveData<RiskProfileScoring>


}