package id.co.ajsmsig.nb.crm.method.async;
/*
 Created by Eriza on 03/03/2018.
 */

import android.content.Context;
import android.os.Looper;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import cz.msebera.android.httpclient.HttpEntity;
import id.co.ajsmsig.nb.util.AppConfig;

public class ProposalApiRestClient {

    private static AsyncHttpClient client = new AsyncHttpClient();
    private static AsyncHttpClient syncHttpClient = new SyncHttpClient();

    public static void get(String url, RequestParams params, int timeOut, AsyncHttpResponseHandler responseHandler) {
        getAsyncHttpClient().setTimeout(timeOut);
        getAsyncHttpClient().get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, int timeOut, AsyncHttpResponseHandler responseHandler) {
        getAsyncHttpClient().setTimeout(timeOut);
        getAsyncHttpClient().post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(Context context, String url, HttpEntity entity, String contentType, int timeOut, AsyncHttpResponseHandler responseHandler) {
        getAsyncHttpClient().setTimeout(timeOut);
        getAsyncHttpClient().post(context, getAbsoluteUrl(url), entity, contentType, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return AppConfig.getBaseUrlProposal() + relativeUrl;
    }


    /**
     * @return an async asyncHttpClient when calling from the main thread, otherwise a sync asyncHttpClient.
     */
    private static AsyncHttpClient getAsyncHttpClient() {
        // Return the synchronous HTTP asyncHttpClient when the thread is not prepared
        if (Looper.myLooper() == null)
            return syncHttpClient;
        syncHttpClient.setEnableRedirects(true);
        return client;
    }
}
