package id.co.ajsmsig.nb.pointofsale.ui.fragment


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosFragmentHeaderBinding
import id.co.ajsmsig.nb.pointofsale.di.Injectable
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.*
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class HeaderFragment : Fragment(), Injectable {

    private lateinit var dataBinding: PosFragmentHeaderBinding

    @Inject lateinit var viewModelFactory: PageViewModelFactory

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(HeaderVM::class.java)
        dataBinding.vm = viewModel

        val mainVM = ViewModelProviders.of(activity!!, viewModelFactory).get(MainVM::class.java)
        mainVM.currentPage.observe(this, Observer {
            viewModel.title.set(it?.title)
            viewModel.titleImage.set(it?.titleImage)
        })
        viewModel.subtitle = mainVM.subtitle
        viewModel.isThereSubtitle = mainVM.isThereSubtitle
        viewModel.isSubtitleTop = mainVM.isSubtitleTop

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_header, container, false)

        return dataBinding.root
    }

}// Required empty public constructor
