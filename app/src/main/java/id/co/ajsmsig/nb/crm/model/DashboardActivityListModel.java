package id.co.ajsmsig.nb.crm.model;

public class DashboardActivityListModel {
    private long slId;
    private long slaInstTabId;
    private String slName;
    private String activityName;
    private String subActivityName;
    private String activityCreatedDate;
    private int itemViewType;
    private String activityHeader;
    private boolean isSynced;


    public DashboardActivityListModel(long slId, long slaInstTabId, String slName, String activityName, String subActivityName, String activityCreatedDate, int itemViewType, String activityHeader, boolean isSynced) {
        this.slId = slId;
        this.slaInstTabId = slaInstTabId;
        this.slName = slName;
        this.activityName = activityName;
        this.subActivityName = subActivityName;
        this.activityCreatedDate = activityCreatedDate;
        this.itemViewType = itemViewType;
        this.activityHeader = activityHeader;
        this.isSynced = isSynced;
    }

    public long getSlId() {
        return slId;
    }

    public void setSlId(long slId) {
        this.slId = slId;
    }

    public String getSlName() {
        return slName;
    }

    public void setSlName(String slName) {
        this.slName = slName;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getSubActivityName() {
        return subActivityName;
    }

    public void setSubActivityName(String subActivityName) {
        this.subActivityName = subActivityName;
    }

    public String getActivityCreatedDate() {
        return activityCreatedDate;
    }

    public void setActivityCreatedDate(String activityCreatedDate) {
        this.activityCreatedDate = activityCreatedDate;
    }
    public long getSlaInstTabId() {
        return slaInstTabId;
    }
    public int getItemViewType() {
        return itemViewType;
    }
    public String getActivityHeader() {
        return activityHeader;
    }

    public boolean isSynced() {
        return isSynced;
    }

    public void setSynced(boolean synced) {
        isSynced = synced;
    }

}

