package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.*
import javax.inject.Singleton

/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Singleton
class PrePopulatedRepository private constructor(var database: PrePopulatedDB) {

    var observablePages: MediatorLiveData<List<Page>> = MediatorLiveData()

    var observablePageScenarios: MediatorLiveData<List<PageScenario>> = MediatorLiveData()


    companion object {
        private var instance: PrePopulatedRepository? = null

        fun getInstance(database: PrePopulatedDB): PrePopulatedRepository {
            synchronized(PrePopulatedRepository::class.java) {
                if (instance == null) {
                    instance = PrePopulatedRepository(database)
                }
            }
            return instance!!
        }
    }

    init {
        observablePages.addSource(database.pageDao().loadAllPages(), observablePages::setValue)
        observablePageScenarios.addSource(database.pageScenarioDao().loadAllPageScenarios(), observablePageScenarios::setValue)
    }

    fun getPageOptionsFromPage(page: Page?, selectedId: Int? = null): LiveData<List<PageOption>> {
        if (selectedId != null) {
            val query = "%,$selectedId,%"
            return database.pageOptionDao().loadAllPageOptionsWithPreviouslySelectedIds(page?.id, query)
        }

        return database.pageOptionDao().loadAllPageOptions(page?.id)
    }

    fun getPageOptionFrom(id: Int?): LiveData<PageOption> {
        return  database.pageOptionDao().loadPageOption(id)
    }

    fun getAllPages(): LiveData<List<Page>> {
        return database.pageDao().loadAllPages()
    }

    fun getAllPageScenarios(): LiveData<List<PageScenario>> {
        return database.pageScenarioDao().loadAllPageScenarios()
    }

    fun getAllInputTypes(): LiveData<List<InputType>> {
        return database.inputTypeDao().loadAllInputTypes()
    }

    fun getInputTypeWith(type: String?): LiveData<InputType> {
        return database.inputTypeDao().getInputTypeWith(type)
    }

    fun getInputForPage(page: Page?, selectedOptionId: Int? = null): LiveData<List<InputAllChoices>> {
        var inputs: LiveData<List<InputAllChoices>>? = null
        if (selectedOptionId != null) {
            val query = "%$selectedOptionId%"
            inputs = database.inputDao().loadAllInputsWith(page?.id, query)
        } else {
            inputs = database.inputDao().loadAllInputsWith(page?.id)
        }

        return inputs
    }

    fun getAllRiskProfileQuestionnaire(): LiveData<List<RiskProfileQuestionnaireAllAnswers>> {
        return database.riskProfileQuestionnaireDao().loadAllQuestion()
    }


    fun getAllRiskProfileQuestions(): LiveData<List<RiskProfileQuestionnaireAllAnswers>> {
        return database.riskProfileQuestionnaireDao().loadAllQuestion()
    }


    fun getAllAnswersForRiskProfileQuestionId(questionId: Int): LiveData<List<RiskProfileQuestionnaireAllAnswers>> {
        return database.riskProfileQuestionnaireDao().loadAllAnswerWithQuestionId(questionId)
    }

    fun getRiskProfileScoringFromScore(score: Int?): LiveData<RiskProfileScoring> {
        return database.riskProfileScoringDao().loadRiskProfileScoringFromScore(score)
    }

    fun getAllRiskProfileScoring(): LiveData<List<RiskProfileScoring>> {
        return database.riskProfileScoringDao().loadAllRiskProfileScoring()
    }

    fun getCalculatorResultFromNeedAnalysisId(needAnalysisId: Int?): LiveData<CalculatorResult> {
        return database.calculatorResultDao().loadPageWithSelectedNeedAnalysis(needAnalysisId)
    }

    fun getPageFromType(type: String?): LiveData<Page> {
        return database.pageDao().loadPage(type)
    }

    fun getHandlingObjectionCategories(): LiveData<List<HandlingObjection>> {
        return database.handlingObjectionDao().loadAllHandlingObjectionCategory()
    }

    fun getHandlingObjectionQuestionsWithCategoryId(handlingObjectionCategoryId: Int?): LiveData<List<HandlingObjectionAllAnswers>> {
        return database.handlingObjectionDao().loadAllHandlingObjectionWithQuestionId(handlingObjectionCategoryId)
    }

    fun getAllProductRecommendationWithGroupId(groupId: Int?): LiveData<List<ProductRecommendation>> {

        return database.productRecommendationDao().loadAllProductRecommendationsWithGroupId(groupId)
    }

    fun getProductRecommendationWithGroupId(groupId: Int?, riskProfileId: Int?): LiveData<List<ProductRecommendation>> {

        return database.productRecommendationDao().loadProductRecommendationsWithGroupId(groupId, riskProfileId)
    }

    fun getProductRecommendationWithGroupId(groupId: Int?, riskProfileId: Int?, selectedNeedId: Int?): LiveData<List<ProductRecommendation>> {

        return database.productRecommendationDao().loadProductRecommendationsWithGroupId(groupId, riskProfileId, selectedNeedId)
    }


    fun getProductRecommendationWithGroupId(groupId: Int?, riskProfileId: Int?, selectedNeedId: Int?, age: Int?): LiveData<List<ProductRecommendation>> {

        return database.productRecommendationDao().loadProductRecommendationsWithGroupId(groupId, riskProfileId, selectedNeedId, age)
    }

    fun getProductRecommendationWithGroupIdAndSelectedNeedAnalysisId(groupId: Int?, selectedNeedId: Int?): LiveData<List<ProductRecommendation>> {

        return database.productRecommendationDao().loadProductRecommendationsWithGroupIdAndSelectedNeedId(groupId, selectedNeedId)
    }

    fun getProductRecommendationWithGroupIdAndSelectedPriorityNeedId(groupId: Int?, selectedPriorityNeedId: Int?): LiveData<List<ProductRecommendation>> {

        return database.productRecommendationDao().loadProductRecommendationsWithGroupIdAndSelectedPriorityNeedId(groupId, selectedPriorityNeedId)
    }

    fun getAllRecommendationSummaryFieldWithRiskProfileId(riskProfileId: Int?): LiveData<List<RecommendationSummaryField>> {
        if (riskProfileId != null) {
            return database.recommendationSummaryFieldDao().loadAllRecommendationSummaryFieldsWith(riskProfileId)
        }
        return database.recommendationSummaryFieldDao().loadAllDefaultRecommendationSummaryFields()
    }

    fun getNextPageFrom(page: Page?, pageOption: PageOption): LiveData<Page> {
        return database.pageScenarioDao().loadNextPage(pageId = page?.id, optionId =  pageOption.id)
    }

}