package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.*
import android.databinding.ObservableField

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */

class InputAllChoices {

        @Embedded
        var input: Input? = null

        @Relation(entity = InputChoice::class, parentColumn = "id", entityColumn = "inputId")
        var choices: List<InputChoice>? = null

        @Ignore
        var selectedValue: ObservableField<Long> = ObservableField()

        @Ignore
        var selectedChoice: ObservableField<InputChoice?> = ObservableField()
}