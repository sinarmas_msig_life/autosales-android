package id.co.ajsmsig.nb.espaj.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.util.Const;

public class E_CreditCardPaymentActivity extends AppCompatActivity {
    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_credit_card_payment);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        String paymentUrl = intent.getStringExtra(Const.INTENT_KEY_WEBVIEW_URL);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.activity_title_payment);
            getSupportActionBar().setSubtitle(R.string.payment_method_credit_card);
            toolbar.setNavigationOnClickListener(view -> onBackPressed());
        }

        if (!TextUtils.isEmpty(paymentUrl)) {
            initWebView(paymentUrl);
        } else {
            displayPaymentUrlError();
        }

    }

    private void initWebView(String url) {
        WebView webView = findViewById(R.id.webView);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);

        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setWebViewClient(new AutosalesWebView());
        webView.loadUrl(url);
    }

    private void displayPaymentUrlError() {
        new AlertDialog.Builder(E_CreditCardPaymentActivity.this)
                .setTitle("Terjadi kesalahan")
                .setMessage("Mohon maaf. Terjadi kesalahan. Tidak dapat memuat URL web pembayaran")
                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                .show();
    }

    private class AutosalesWebView extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.v(TAG, "Page started "+url);
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.v(TAG, "Page finished "+url);
            //http://paymentcc.sinarmasmsiglife.co.id:8082/api/sprint/rcb?trxn=838702171710087&payStat=00&payMsg=Approved
            if (url != null && url.contains("trxn=")) {

                new Handler().postDelayed(() -> {

                    String[] splits = url.split("=");
                    if (splits.length > 0) {
                        String no = splits[1];
                        String[] splitsByAmp = no.split(Pattern.quote("&"));
                        String transactionNo = splitsByAmp[0];
                        String payStatus = getPayStatus(url);
                        String payMessage = getPayMessage(url);
                        finishAndSendTransactionNumber(transactionNo, payStatus, payMessage);
                    }

                }, 1000);


            }
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        //when you click on any interlink on webview that time you got url :
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.v(TAG,"when you click on any interlink on webview that time you got url :-" + url );
            return super.shouldOverrideUrlLoading(view,url);
        }
    }
    private String getPayStatus(String message) {
        //Extract http://paymentcc.sinarmasmsiglife.co.id:8082/api/sprint/rcb?trxn=8387021717100817&payStat=00&payMsg=Approved to get "00" only
        if (!TextUtils.isEmpty(message)) {
            String[] splits = message.split(Pattern.quote("&"));
            if (splits.length > 0) {
                String rawPayStat = splits[1]; //This would produce "payStat=00"

                String[] splitsByEqualSign = rawPayStat.split(Pattern.quote("="));
                if (splitsByEqualSign.length > 0) {
                    String payStat = splitsByEqualSign[1];
                    return payStat;
                }

            }
        }

        return null;

    }
    private String getPayMessage(String message) {
        //Extract http://paymentcc.sinarmasmsiglife.co.id:8082/api/sprint/rcb?trxn=8387021717100817&payStat=00&payMsg=Approved to get "Approved" only
        if (!TextUtils.isEmpty(message)) {
            String[] splits = message.split(Pattern.quote("&"));
            if (splits.length > 0) {
                String rawPayMsg = splits[2]; //This would produce "payMsg=Approved"

                String[] splitsByEqualSign = rawPayMsg.split(Pattern.quote("="));
                if (splitsByEqualSign.length > 0) {
                    String payMessage = splitsByEqualSign[1];
                    return payMessage;
                }

            }
        }

        return null;
    }
    private void finishAndSendTransactionNumber(String transactionNumber, String paymentStatus, String paymentMessage) {
        Intent intent = new Intent(E_CreditCardPaymentActivity.this, E_MainActivity.class);
        intent.putExtra(Const.INTENT_KEY_TRANSACTION_NUMBER, transactionNumber);
        intent.putExtra(Const.INTENT_KEY_PAYMENT_STATUS, paymentStatus);
        intent.putExtra(Const.INTENT_KEY_PAYMENT_MESSAGE, paymentMessage);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
