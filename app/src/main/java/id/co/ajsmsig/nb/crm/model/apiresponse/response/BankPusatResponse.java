
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BankPusatResponse {

    @SerializedName("BANK PUSAT")
    @Expose
    private List<BANKPUSAT> bANKPUSAT = null;

    public List<BANKPUSAT> getBANKPUSAT() {
        return bANKPUSAT;
    }

    public void setBANKPUSAT(List<BANKPUSAT> bANKPUSAT) {
        this.bANKPUSAT = bANKPUSAT;
    }

}
