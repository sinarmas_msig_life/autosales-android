package id.co.ajsmsig.nb.crm.fragment;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.activity.C_ContactUsActivity;
import id.co.ajsmsig.nb.crm.activity.C_FaqActivity;
import id.co.ajsmsig.nb.crm.activity.C_ListProviderActivity;
import id.co.ajsmsig.nb.crm.activity.C_LoginActivity;
import id.co.ajsmsig.nb.crm.activity.C_ProfileAgentActivity;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.apiresponse.request.UpdatePasswordRequest;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.UpdatePasswordResponse;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.database.P_Update;
import id.co.ajsmsig.nb.prop.model.apimodel.request.ValidateVaRequest;
import id.co.ajsmsig.nb.prop.model.apimodel.response.ValidateVaResponse;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.MessageEvent;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by faiz_f on 03/02/2017.
 */

public class C_MoreFragment extends Fragment implements View.OnClickListener {


    public C_MoreFragment() {
    }

    private String kodeAgen;
    private int jenisLogin;
    private AlertDialog dialogChangePassword;
    private CompositeDisposable disposable = new CompositeDisposable();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.c_fragment_more, container, false);
        ButterKnife.bind(this, view);

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
        jenisLogin = sharedPreferences.getInt("JENIS_LOGIN", 0);

        view.findViewById(R.id.ll_profile).setOnClickListener(this);
        TextView tv_profile_name = view.findViewById(R.id.tv_profile_name);
        TextView tv_kode_agen = view.findViewById(R.id.tv_kode_agen);
        tv_profile_name.setText(sharedPreferences.getString("NAMA_AGEN", ""));
        tv_kode_agen.setText(sharedPreferences.getString("KODE_AGEN", ""));

//        view.findViewById(R.id.ll_perbaharui).setOnClickListener(this);
        view.findViewById(R.id.ll_faq).setOnClickListener(this);
        view.findViewById(R.id.ll_logout).setOnClickListener(this);
        view.findViewById(R.id.layoutContactUs).setOnClickListener(this);
        view.findViewById(R.id.layoutChangePassword).setOnClickListener(this);
        view.findViewById(R.id.layoutProvider).setOnClickListener(this);
        view.findViewById(R.id.llClearDiskSpace).setOnClickListener(this);

        return view;
    }


    public void logout() {
        AlertDialog.Builder msgBoxDelete = new AlertDialog.Builder(getContext());
        msgBoxDelete.setTitle("Logout");
        msgBoxDelete.setMessage("Apakah Anda yakin ingin melakukan logout?");
        msgBoxDelete.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //no code
            }
        });
        msgBoxDelete.setPositiveButton("Log Out", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_preferences), MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.apply();
                Intent intent = new Intent(getContext(), C_LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        msgBoxDelete.show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_profile:
                startActivity(new Intent(getContext(), C_ProfileAgentActivity.class));
                break;
//            case R.id.ll_perbaharui:
//                break;
            case R.id.ll_faq:
                startActivity(new Intent(getActivity(), C_FaqActivity.class));
                break;
            case R.id.ll_logout:
                logout();
                break;
            case R.id.layoutContactUs:
                startActivity(new Intent(getActivity(), C_ContactUsActivity.class));
                break;
            case R.id.layoutChangePassword:
                displayChangePasswordDialog();
                break;
            case R.id.layoutProvider:
                if (StaticMethods.isNetworkAvailable(getActivity())) {
                    startActivity(new Intent(getActivity(), C_ListProviderActivity.class));
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Tidak ada koneksi internet")
                            .setMessage("Fitur ini membutuhkan koneksi internet. Silahkan periksa kembali koneksi internet Anda kemudian coba kembali.")
                            .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                            .show();
                }

                break;
            case R.id.llClearDiskSpace:
                ProgressDialog dialog = new ProgressDialog(requireActivity());
                dialog.setTitle("Mohon tunggu");
                dialog.setMessage("Sedang melakukan penghapusan beberapa file yang sudah tidak digunakan. Proses ini bisa memakan waktu beberapa menit");
                dialog.setCancelable(false);
                dialog.show();

                disposable.add(
                        Completable.fromAction(this::removeDiskSpace)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableCompletableObserver() {
                                                   @Override
                                                   public void onComplete() {
                                                       dialog.dismiss();
                                                       Toast.makeText(requireActivity(), "Penghapusan berhasil", Toast.LENGTH_SHORT).show();
                                                   }

                                                   @Override
                                                   public void onError(Throwable e) {
                                                       dialog.dismiss();
                                                       Toast.makeText(requireActivity(), "Penghapusan gagal" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                                   }
                                               }

                                )
                );
                break;
            default:
                throw new IllegalArgumentException("No idnya");
        }
    }


    private boolean shouldRemoveFile(File file) {
        if (file != null) {
            long dateModifiedMillis = file.lastModified();
            long difference = System.currentTimeMillis() - dateModifiedMillis;

            //Remove file if bigger than 7 days
            if (difference > 604800000) {
                //Remove file
                return true;
            }

        }
        return false;
    }

    private void removeDiskSpace() {
        File fileDir = requireActivity().getFilesDir();
        File espajDir = new File(fileDir.getAbsolutePath(), "ESPAJ");

        File[] filesOnEspajDirectory = espajDir.listFiles();
        ///data/user/0/id.co.ajsmsig.nb.dev/files/ESPAJ/spaj_foto ,  /data/user/0/id.co.ajsmsig.nb.dev/files/ESPAJ/spaj_ttd, /data/user/0/id.co.ajsmsig.nb.dev/files/ESPAJ/spaj_file

        for (File file : filesOnEspajDirectory) {
            Log.v("File", "Folder name " + file.getName());

            //1 . spaj_foto -> is directory, if dipenuhi
            //2. spaj_ttd -> is directory, if dipenuhi
            if (file.isDirectory()) {
                File[] fileNamesPath = file.listFiles();

                if (file.getName().contains("spaj_file")) {
                    removeSpajFile(fileNamesPath);
                } else {
                    removeSpajFotoAndTtd(fileNamesPath);
                }

            }
        }


    }

    private void removeSpajFotoAndTtd(File[] fileNamesPath) {
        if (fileNamesPath.length == 0) {
            Log.v("File", "No files to delete");
            return;
        }

        for (File f : fileNamesPath) {
            Log.v("File", "Trying to remove file " + f.getName());

            if (shouldRemoveFile(f)) {
                boolean success = f.delete();
                Log.v("File", "Remove result : " + success);
            } else {
                Log.v("File", "Remove result : Failed because the file date modified less than 7 days");
            }


        }
    }

    private void removeSpajFile(File[] files) {

        for (File file : files) {
            Log.v("File", "Folder name " + file.getName());

            if (file.isDirectory()) {
                File[] filesOnAndroidSpajDirectory = file.listFiles();

                for (File f : filesOnAndroidSpajDirectory) {
                    Log.v("File", "Trying to remove file " + f.getName());

                    if (shouldRemoveFile(f)) {
                        boolean success = f.delete();
                        Log.v("File", "Remove result : " + success);
                    } else {
                        Log.v("File", "Remove result : Failed because the file date modified less than 7 days");
                    }

                }

            }
        }

    }

    private void displayChangePasswordDialog() {
        //Init custom alert dialog
        View view = getLayoutInflater().inflate(R.layout.dialog_change_password, null);
        TextInputLayout inputLayoutPassword = view.findViewById(R.id.inputLayoutChangePassword);
        EditText etNewPassword = view.findViewById(R.id.etNewPassword);
        Button btnChangePassword = view.findViewById(R.id.btnChangePassword);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Ubah password");
        builder.setView(view);

        btnChangePassword.setOnClickListener(v -> {
                    String newPassword = etNewPassword.getText().toString().trim();

                    if (!TextUtils.isEmpty(newPassword)) {
                        inputLayoutPassword.setErrorEnabled(false);
                        dialogChangePassword.dismiss();
                        updatePasswordWith(newPassword);
                    } else {
                        inputLayoutPassword.setError("Tidak boleh kosong");
                    }


                }
        );

        dialogChangePassword = builder.create();
        dialogChangePassword.show();

    }

    private void updatePasswordWith(String newPassword) {
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        String url = AppConfig.getBaseUrlWs().concat("member/api/json/updatePassword");

        //Make a POST request body
        UpdatePasswordRequest request = new UpdatePasswordRequest(kodeAgen, newPassword, String.valueOf(jenisLogin));


        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Mengubah password");
        progressDialog.setMessage("Mohon tunggu");
        progressDialog.show();

        Call<List<UpdatePasswordResponse>> call = api.updatePassword(url, request);
        call.enqueue(new Callback<List<UpdatePasswordResponse>>() {
            @Override
            public void onResponse(Call<List<UpdatePasswordResponse>> call, Response<List<UpdatePasswordResponse>> serverResponse) {
                List<UpdatePasswordResponse> response = serverResponse.body();

                if (response != null && !response.isEmpty()) {
                    UpdatePasswordResponse responseBody = response.get(0);
                    boolean error = responseBody.getError();
                    String message = responseBody.getMessage();

                    if (!error) {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }

                }

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<UpdatePasswordResponse>> call, Throwable t) {
                Toast.makeText(getActivity(), "Pembaharuan password gagal " + t.getMessage(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }

        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
