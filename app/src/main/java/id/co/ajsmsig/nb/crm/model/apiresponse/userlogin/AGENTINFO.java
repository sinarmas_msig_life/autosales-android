
package id.co.ajsmsig.nb.crm.model.apiresponse.userlogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AGENTINFO {

    @SerializedName("REGIONAL_CODE1")
    @Expose
    private String rEGIONALCODE1;
    @SerializedName("REGIONAL_CODE2")
    @Expose
    private String rEGIONALCODE2;
    @SerializedName("REGIONAL_CODE3")
    @Expose
    private String rEGIONALCODE3;
    @SerializedName("GROUP_ID")
    @Expose
    private Integer gROUPID;
    @SerializedName("REGIONAL_NAME")
    @Expose
    private String rEGIONALNAME;
    @SerializedName("FLAG_ADMIN")
    @Expose
    private Integer fLAGADMIN;
    @SerializedName("AGEN_NAME")
    @Expose
    private String aGENNAME;
    @SerializedName("JENIS_LOGIN_BC")
    @Expose
    private Integer jENISLOGINBC;
    @SerializedName("BANK_NAME")
    @Expose
    private String bANKNAME;
    @SerializedName("REGIONAL_CODE")
    @Expose
    private String rEGIONALCODE;
    @SerializedName("JENIS_LOGIN")
    @Expose
    private Integer jENISLOGIN;
    @SerializedName("AGENT_EMAIL")
    @Expose
    private String agentEmail;
    @SerializedName("LEADER_CODE")
    @Expose
    private String lEADERCODE;
    @SerializedName("AGEN_CODE")
    @Expose
    private String aGENCODE;

    public String getREGIONALCODE1() {
        return rEGIONALCODE1;
    }

    public void setREGIONALCODE1(String rEGIONALCODE1) {
        this.rEGIONALCODE1 = rEGIONALCODE1;
    }

    public String getREGIONALCODE2() {
        return rEGIONALCODE2;
    }

    public void setREGIONALCODE2(String rEGIONALCODE2) {
        this.rEGIONALCODE2 = rEGIONALCODE2;
    }

    public String getREGIONALCODE3() {
        return rEGIONALCODE3;
    }

    public void setREGIONALCODE3(String rEGIONALCODE3) {
        this.rEGIONALCODE3 = rEGIONALCODE3;
    }

    public Integer getGROUPID() {
        return gROUPID;
    }

    public void setGROUPID(Integer gROUPID) {
        this.gROUPID = gROUPID;
    }

    public String getREGIONALNAME() {
        return rEGIONALNAME;
    }

    public void setREGIONALNAME(String rEGIONALNAME) {
        this.rEGIONALNAME = rEGIONALNAME;
    }

    public Integer getFLAGADMIN() {
        return fLAGADMIN;
    }

    public void setFLAGADMIN(Integer fLAGADMIN) {
        this.fLAGADMIN = fLAGADMIN;
    }

    public String getAGENNAME() {
        return aGENNAME;
    }

    public void setAGENNAME(String aGENNAME) {
        this.aGENNAME = aGENNAME;
    }

    public Integer getJENISLOGINBC() {
        return jENISLOGINBC;
    }

    public void setJENISLOGINBC(Integer jENISLOGINBC) {
        this.jENISLOGINBC = jENISLOGINBC;
    }

    public String getBANKNAME() {
        return bANKNAME;
    }

    public void setBANKNAME(String bANKNAME) {
        this.bANKNAME = bANKNAME;
    }

    public String getREGIONALCODE() {
        return rEGIONALCODE;
    }

    public void setREGIONALCODE(String rEGIONALCODE) {
        this.rEGIONALCODE = rEGIONALCODE;
    }

    public Integer getJENISLOGIN() {
        return jENISLOGIN;
    }

    public void setJENISLOGIN(Integer jENISLOGIN) {
        this.jENISLOGIN = jENISLOGIN;
    }

    public String getLEADERCODE() {
        return lEADERCODE;
    }

    public void setLEADERCODE(String lEADERCODE) {
        this.lEADERCODE = lEADERCODE;
    }

    public String getAGENCODE() {
        return aGENCODE;
    }

    public void setAGENCODE(String aGENCODE) {
        this.aGENCODE = aGENCODE;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }


}
