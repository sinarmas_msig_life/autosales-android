package id.co.ajsmsig.nb.util;

import android.text.TextUtils;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    /**
     * Convert dd/MM/yyyy HH:mm:ss date format to a more readable one
     *
     * @param specifiedDate
     * @return
     */
    public static String getReadableDateFrom(String specifiedDate) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            Date date = format.parse(specifiedDate);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);


            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            int mo = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            int second = calendar.get(Calendar.SECOND);

            String[] days = new String[]{"", "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"};
            String day = days[dayOfWeek];

            String[] months = new String[]{"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
            String month = months[mo];

            return day
                    .concat(", ")
                    .concat(String.valueOf(dayOfMonth))
                    .concat(" ")
                    .concat(month)
                    .concat(" ")
                    .concat(String.valueOf(year))
                    .concat(" ")
                    .concat(String.valueOf(hour))
                    .concat(":")
                    .concat(String.valueOf(minute))
                    .concat(":")
                    .concat(String.valueOf(second));


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Convert dd-MM-yyyy HH:mm:ss date format to a more readable one
     *
     * @param specifiedDate
     * @return
     */
    public static String getDateFrom(String specifiedDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(specifiedDate);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);


            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            int mo = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            int second = calendar.get(Calendar.SECOND);

            String[] days = new String[]{"", "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"};
            String day = days[dayOfWeek];

            String[] months = new String[]{"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
            String month = months[mo];

            return day
                    .concat(", ")
                    .concat(String.valueOf(dayOfMonth))
                    .concat(" ")
                    .concat(month)
                    .concat(" ")
                    .concat(String.valueOf(year))
                    .concat(" ")
                    .concat(String.valueOf(hour))
                    .concat(":")
                    .concat(String.valueOf(minute))
                    .concat(":")
                    .concat(String.valueOf(second));


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Convert date format to a more readable one
     *
     * @param specifiedDate
     * @return
     */
    public static String getDateFrom(Date specifiedDate) {

        if (specifiedDate != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(specifiedDate);

            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            int mo = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);

            String[] days = new String[]{"", "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"};
            String day = days[dayOfWeek];

            String[] months = new String[]{"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
            String month = months[mo];

            return day
                    .concat(", ")
                    .concat(String.valueOf(dayOfMonth))
                    .concat(" ")
                    .concat(month)
                    .concat(" ")
                    .concat(String.valueOf(year));

        }

        return null;
    }

    /**
     * Extract month from spaj id tab
     *
     * @param spajIdTab
     * @return
     */
    public static int getMonthFromSpajIdTab(String spajIdTab) {
        if (!TextUtils.isEmpty(spajIdTab)) {
            DateFormat dateFormat = new SimpleDateFormat("'AndroidESPAJ'yyyy.MM.dd.HH.mm.ss.SSS");
            try {
                Date date = dateFormat.parse(spajIdTab);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                return calendar.get(Calendar.MONTH) + 1;

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return 0;
    }
    /**
     * Extract day from spaj id tab
     *
     * @param spajIdTab
     * @return
     */
    public static int getDayFromSpajIdTab(String spajIdTab) {
        if (!TextUtils.isEmpty(spajIdTab)) {
            DateFormat dateFormat = new SimpleDateFormat("'AndroidESPAJ'yyyy.MM.dd.HH.mm.ss.SSS");
            try {
                Date date = dateFormat.parse(spajIdTab);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                return calendar.get(Calendar.DAY_OF_MONTH);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return 0;
    }
    public static String getMonthAndYearFromDate(String inputDate) {
        if (!TextUtils.isEmpty(inputDate)) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date date = dateFormat.parse(inputDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                //Get month in string format
                int mo = calendar.get(Calendar.MONTH);
                String[] months = new String[]{"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
                String month = months[mo];

                //Get year
                int year = calendar.get(Calendar.YEAR);

                String monthAndYear = month.concat(" ").concat(String.valueOf(year));
                return monthAndYear;

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    /**
     * Get current month
     *
     * @return
     */
    public static int getCurrentMonth() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.MONTH) + 1;
    }

    public static int getCurrentDayOfMonth() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DAY_OF_MONTH);
    }
    /**
     * Extract date from spaj id tab
     *
     * @param spajIdTab
     * @return
     */
    public static Date getDateFromSpajIdTab(String spajIdTab) {
        if (!TextUtils.isEmpty(spajIdTab)) {
            DateFormat dateFormat = new SimpleDateFormat("'AndroidESPAJ'yyyy.MM.dd.HH.mm.ss.SSS");
            try {
                return dateFormat.parse(spajIdTab);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String addASecondFrom(String currentDate) {
        if (!TextUtils.isEmpty(currentDate)) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date date = format.parse(currentDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.SECOND, 1);
                return format.format(calendar.getTime());

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;


    }

    public static String getMonthStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date date = calendar.getTime();
        String startDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        return startDate;
    }
    public static String getMonthEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date date = calendar.getTime();
        String endDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        return endDate;
    }

    public static String getLastMonthStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        int lastMonth = calendar.get(Calendar.MONTH) - 1 ;
        calendar.set(Calendar.MONTH, lastMonth);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        Date date = calendar.getTime();
        String startDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        return startDate;
    }
    public static String getLastMonthEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        int lastMonth = calendar.get(Calendar.MONTH) -1;
        calendar.set(Calendar.MONTH, lastMonth);

        int daysCount = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        calendar.set(Calendar.DAY_OF_MONTH, daysCount);


        Date date = calendar.getTime();
        String endDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        return endDate;
    }

    public static String getCurrentMonthStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        Date date = calendar.getTime();
        String startDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        return startDate;
    }
    public static String getCurrentMonthEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int daysCount = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, daysCount);
        Date date = calendar.getTime();
        String endDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        return endDate;
    }

    public static int getCurrentMonthNumber() {
        Calendar calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH) + 1;
        return currentMonth;
    }
    /**
     * Return last month number
     *
     * @return
     */
    public static int getLastMonthNumber() {
        Calendar calendar = Calendar.getInstance();
        int lastMonth = calendar.get(Calendar.MONTH);
        return lastMonth;
    }

    public static long generateTimeStamp() {
        String timestamp =  new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date());
        long convertedTimestamp = Long.parseLong(timestamp);
        return convertedTimestamp;
    }

    public static long getMillisFrom(String activityCreatedDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = dateFormat.parse(activityCreatedDate);
            return date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getCurrentDateInString() {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        String currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        return currentDate;
    }

    public static String getSPAJProcessDate(String stringDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss'.0'");
        try {
            Date date = format.parse(stringDate);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);


            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            int mo = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            int second = calendar.get(Calendar.SECOND);

            String[] days = new String[]{"", "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"};
            String day = days[dayOfWeek];

            String[] months = new String[]{"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
            String month = months[mo];

            return day
                    .concat(", ")
                    .concat(String.valueOf(dayOfMonth))
                    .concat(" ")
                    .concat(month)
                    .concat(" ")
                    .concat(String.valueOf(year))
                    .concat(" ")
                    .concat(String.valueOf(hour))
                    .concat(":")
                    .concat(String.valueOf(minute))
                    .concat(":")
                    .concat(String.valueOf(second));


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

}
