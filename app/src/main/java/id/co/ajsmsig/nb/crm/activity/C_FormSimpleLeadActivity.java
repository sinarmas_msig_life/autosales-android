package id.co.ajsmsig.nb.crm.activity;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.C_Delete;
import id.co.ajsmsig.nb.crm.database.C_Insert;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.database.C_Update;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.method.async.CrmRestClientUsage;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.BcBranch;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.Referral;
import id.co.ajsmsig.nb.crm.model.form.SimbakActivityModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakAddressModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakLeadModel;
import id.co.ajsmsig.nb.crm.model.hardcode.CrmTableCode;
import id.co.ajsmsig.nb.crm.model.hardcode.Sex;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.espaj.database.E_Delete;
import id.co.ajsmsig.nb.espaj.database.E_Insert;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.prop.method.MoneyTextWatcher;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.DateUtils;
import id.co.ajsmsig.nb.util.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class C_FormSimpleLeadActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener, AdapterView.OnItemSelectedListener {
    private final static String TAG = C_FormSimpleLeadActivity.class.getSimpleName();

    @BindView(R.id.cl_nama_referral)
    ConstraintLayout cl_nama_referral;
    @BindView(R.id.clSelectReferral)
    ConstraintLayout clSelectReferral;
    @BindView(R.id.et_nama)
    EditText et_nama;
    @BindView(R.id.et_umur)
    EditText et_umur;
    @BindView(R.id.et_handphone)
    EditText et_handphone;
    @BindView(R.id.et_email)
    EditText et_email;

    @BindView(R.id.ac_referral_lead)
    AutoCompleteTextView ac_referral_lead;
    @BindView(R.id.ac_nama_referral)
    AutoCompleteTextView ac_nama_referral;

    @BindView(R.id.tv_error_nama)
    TextView tv_error_nama;
    @BindView(R.id.tv_error_umur)
    TextView tv_error_umur;
    @BindView(R.id.tv_error_handphone)
    TextView tv_error_handphone;
    @BindView(R.id.tv_error_email)
    TextView tv_error_email;
    @BindView(R.id.tv_error_referal_lead)
    TextView tv_error_referal_lead;
    @BindView(R.id.tv_error_nama_referral)
    TextView tv_error_nama_referral;

    @BindView(R.id.iv_circle_nama)
    ImageView iv_circle_nama;
    @BindView(R.id.iv_circle_umur)
    ImageView iv_circle_umur;


    @BindView(R.id.rb_pria)
    RadioButton rb_pria;
    @BindView(R.id.rb_wanita)
    RadioButton rb_wanita;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_ok)
    Button btn_ok;
    @BindView(R.id.spinner)
    Spinner spinnerBcBranch;
    @BindView(R.id.tvSelectBranch)
    TextView tvSelectBranch;
    @BindView(R.id.spinnerReferral)
    Spinner spinnerReferral;
    @BindView(R.id.spinnerReff)
    Spinner spinnerReff;
    @BindView(R.id.tvSelectReferral)
    TextView tvSelectReferral;
    @BindView(R.id.layoutLoading)
    LinearLayout layoutLoading;
    @BindView(R.id.tvLoadingMessage)
    TextView tvLoadingMessage;

    DropboxModel dSumberReferral = new DropboxModel();

    private SimbakLeadModel simbakLeadModel = new SimbakLeadModel();
    private SimbakAddressModel simbakAddressHouse = new SimbakAddressModel();

    private boolean isClickAble = true;

    private boolean isSave = false;
    private Integer sex = null;
    private BcBranchSpinnerAdapter bcBranchSpinnerAdapter;
    private ReferralSpinnerAdapter referallSpinnerAdapter;
    private String msagId;
    private int bcLoginType;
    private final String SIMBAK_AGENT_BRANCH_TABLE = "SIMBAK_AGENT_BRANCH";
    private String selectedBranchCode;
    private String selectedBranchName;
    private String selectedReferallId;
    private String selectedReferallName;
    private List<BcBranch> savedBranches;
    private boolean isEditMode;
    private long slTabId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_activity_form_simple_lead);
        ButterKnife.bind(this);

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        msagId = sharedPreferences.getString("KODE_AGEN", "");
        bcLoginType = sharedPreferences.getInt("JENIS_LOGIN_BC", 0);

        initViews();
    }

    private void initViews() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) bundle = new Bundle();

        initToolbar("Tambah lead");
        isEditMode = bundle.getBoolean(Const.INTENT_KEY_EDIT_MODE);

        //Edit lead
        if (isEditMode) {
            slTabId = bundle.getLong(Const.INTENT_KEY_SL_TAB_ID);
            initToolbar("Edit lead");
            fillForm(String.valueOf(slTabId));
        } else {
            dSumberReferral = getSumberReferensi(2);
            ac_referral_lead.setText(dSumberReferral.getLabel());
        }


        et_nama.addTextChangedListener(new MTextWatcher(et_nama));
        et_umur.addTextChangedListener(new MTextWatcher(et_umur));
        et_email.addTextChangedListener(new MoneyTextWatcher(et_email));
        ac_referral_lead.addTextChangedListener(new MoneyTextWatcher(ac_referral_lead));
        ac_nama_referral.addTextChangedListener(new MoneyTextWatcher(ac_nama_referral));
        ac_referral_lead.setOnClickListener(this);
        ac_referral_lead.setOnFocusChangeListener(this);
        et_umur.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        et_handphone.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

        btn_ok.setOnClickListener(this);
        rb_pria.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                sex = Sex.MALE;
            }
        });

        rb_wanita.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                sex = Sex.FEMALE;
            }
        });


        //Hide branch select for non-BSIM bank
        if (bcLoginType == Const.BANK_CODE_SINARMAS) {
            shouldDisplayBranchSelect(true);
            initSpinner();
        } else {
            shouldDisplayBranchSelect(false);
        }


    }

    private void initToolbar(String toolbarTitle) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(view -> onBackPressed());
            //Display back navigation on toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            //Set text on toolbar
            if (!TextUtils.isEmpty(toolbarTitle)) {
                getSupportActionBar().setTitle(toolbarTitle);
            }

        }
    }

    /**
     * Only show spinner select BC branch if only the user is BSIM
     *
     * @param shouldShow
     */
    private void shouldDisplayBranchSelect(boolean shouldShow) {

        if (shouldShow) {
            spinnerBcBranch.setVisibility(View.VISIBLE);
            tvSelectBranch.setVisibility(View.VISIBLE);
            return;
        }

        spinnerBcBranch.setVisibility(View.GONE);
        tvSelectBranch.setVisibility(View.GONE);
    }

    /**
     * Setup spinner and determine the data source
     */
    private void initSpinner() {

        List<BcBranch> branches = new ArrayList<>();
        bcBranchSpinnerAdapter = new BcBranchSpinnerAdapter(C_FormSimpleLeadActivity.this, android.R.layout.simple_spinner_item, branches);
        bcBranchSpinnerAdapter.setDropDownViewResource(R.layout.spinner_bc_branch);
        spinnerBcBranch.setAdapter(bcBranchSpinnerAdapter);
        spinnerBcBranch.setOnItemSelectedListener(this);

        //Check for saved bc branch
        savedBranches = new C_Select(C_FormSimpleLeadActivity.this).getBcBranchFromDatabase(msagId);

        int savedBranchCount = savedBranches.size();
        if (StaticMethods.isNetworkAvailable(this)) {

            if (savedBranchCount > 0) {
                //Remove previous data
                C_Delete delete = new C_Delete(C_FormSimpleLeadActivity.this);
                delete.deleteAll(SIMBAK_AGENT_BRANCH_TABLE);
            }

            //Add the latest data
            fetchBCBranchFromServer();

        } else if (!StaticMethods.isNetworkAvailable(this) && savedBranchCount == 0) {
            //No branch list is saved, ask user to download the branch list first
            new AlertDialog.Builder(C_FormSimpleLeadActivity.this)
                    .setTitle(R.string.dialog_no_internet_connection)
                    .setMessage(R.string.dialog_branch_data_not_found)
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                    .show();

        } else if (!StaticMethods.isNetworkAvailable(this) && savedBranchCount > 0) {
            bcBranchSpinnerAdapter.addAll(savedBranches);
        }


    }

    /**
     * Setup spinner and determine the data source
     */
    private void initReferralSpinner(String selectedBranchCode, String selectedBranchName) {

        List<Referral> referrals = new ArrayList<>();
        referallSpinnerAdapter = new ReferralSpinnerAdapter(C_FormSimpleLeadActivity.this, android.R.layout.simple_spinner_item, referrals);
        referallSpinnerAdapter.setDropDownViewResource(R.layout.spinner_bc_branch);
        spinnerReferral.setAdapter(referallSpinnerAdapter);
        spinnerReferral.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Referral model = (Referral) parent.getItemAtPosition(position);
                selectedReferallId = model.getReffId();
                selectedReferallName = model.getReffName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<Referral> savedReferrals = new E_Select(C_FormSimpleLeadActivity.this).getReferralListFromBranchName(selectedBranchName);

        int savedReferal = savedReferrals.size();
        if (StaticMethods.isNetworkAvailable(this)) {

            if (savedReferal > 0) {
                //Remove previous data
                E_Delete delete = new E_Delete(C_FormSimpleLeadActivity.this);
                delete.deleteAllBranchWithBranchName(selectedBranchName);
            }

            //Add the latest data
            fetchReferralFromServer(selectedBranchCode, true, msagId);

        } else if (!StaticMethods.isNetworkAvailable(this) && savedReferal == 0) {
            //No branch list is saved, ask user to download the branch list first
            new AlertDialog.Builder(C_FormSimpleLeadActivity.this)
                    .setTitle(R.string.dialog_no_internet_connection)
                    .setMessage(R.string.dialog_referall_data_not_found)
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                    .show();

        } else if (!StaticMethods.isNetworkAvailable(this) && savedReferal > 0) {
            referallSpinnerAdapter.addAll(savedReferrals);
        }


    }

    private void fillForm(String slTabId) {
        C_Select select = new C_Select(C_FormSimpleLeadActivity.this);
        simbakLeadModel = select.getLeadInfo(slTabId);

        if (simbakLeadModel.getSL_TAB_ID() != null)
            simbakAddressHouse = new C_Select(this).selectSimbakAddress(simbakLeadModel.getSL_TAB_ID(), 1);

        if (!TextUtils.isEmpty(simbakLeadModel.getSL_NAME())) {
            int color = ContextCompat.getColor(getBaseContext(), R.color.green);
            iv_circle_nama.setColorFilter(color);
            et_nama.setText(simbakLeadModel.getSL_NAME());
        }


        if (simbakLeadModel.getSL_UMUR() != null)
            et_umur.setText(String.valueOf(simbakLeadModel.getSL_UMUR()));

        if (simbakLeadModel.getSL_GENDER() != null) {
            if (simbakLeadModel.getSL_GENDER() == 1) {
                rb_pria.setChecked(true);
            } else {
                rb_wanita.setChecked(true);
            }
        }

        if (simbakLeadModel.getSL_TYPE() != null) {
            Log.d(TAG, "fillForm: sltype not null");
            dSumberReferral = getSumberReferensi(simbakLeadModel.getSL_TYPE());
            ac_referral_lead.setText(dSumberReferral.getLabel());
            if (simbakLeadModel.getSL_TYPE() == 1)
                cl_nama_referral.setVisibility(View.VISIBLE);
        } else {
            defaultSumberReferral();
        }

        if (simbakLeadModel.getSL_BAC_REFF_NAME() != null) {
            ac_nama_referral.setText(simbakLeadModel.getSL_BAC_REFF_NAME());
        }

        if (simbakAddressHouse.getSLA2_HP1() != null)
            et_handphone.setText(simbakAddressHouse.getSLA2_HP1());

        if (simbakAddressHouse.getSLA2_EMAIL() != null)
            et_email.setText(simbakAddressHouse.getSLA2_EMAIL());


        String currentBranchName = simbakLeadModel.getSL_NAMA_CABANG();
        String currentBacReferralName = simbakLeadModel.getSL_BAC_REFF_NAME();

        if (savedBranches != null && !savedBranches.isEmpty() && !TextUtils.isEmpty(currentBranchName)) {

            for (int i = 0; i < savedBranches.size(); i++) {
                String cabang = savedBranches.get(i).getCabang();

                if (cabang.contains(currentBranchName)) {

                    BcBranchSpinnerAdapter bcBranchSpinnerAdapter = new BcBranchSpinnerAdapter(C_FormSimpleLeadActivity.this, android.R.layout.simple_spinner_item, savedBranches);
                    bcBranchSpinnerAdapter.setDropDownViewResource(R.layout.spinner_bc_branch);
                    spinnerBcBranch.setAdapter(bcBranchSpinnerAdapter);
                    spinnerBcBranch.setOnItemSelectedListener(this);
                    spinnerBcBranch.setSelection(i);

                }
            }

        }

        List<Referral> referrals = new E_Select(C_FormSimpleLeadActivity.this).getReferralListFromBranchName(currentBranchName);
        if (referrals != null && !referrals.isEmpty() && !TextUtils.isEmpty(currentBacReferralName)) {
            clSelectReferral.setVisibility(View.VISIBLE);
            spinnerReferral.setVisibility(View.GONE);
            cl_nama_referral.setVisibility(View.GONE);

            for (int i = 0; i < referrals.size(); i++) {
                String referralName = referrals.get(i).getReffName();
                if (referralName.contains(currentBacReferralName)) {
                    ReferralSpinnerAdapter adapter = new ReferralSpinnerAdapter(C_FormSimpleLeadActivity.this, android.R.layout.simple_spinner_item, referrals);
                    adapter.setDropDownViewResource(R.layout.spinner_bc_branch);
                    spinnerReff.setAdapter(adapter);
                    spinnerReff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Referral model = (Referral) parent.getItemAtPosition(position);
                            selectedReferallId = model.getReffId();
                            selectedReferallName = model.getReffName();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    spinnerReff.setSelection(i);
                    Log.v(TAG, "Found at index " + i);
                }
            }

        } else {
            clSelectReferral.setVisibility(View.GONE);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        initiateAdapter();
    }

    private void defaultSumberReferral() {
        dSumberReferral = getSumberReferensi(2);
        ac_referral_lead.setText(dSumberReferral.getLabel());
    }

    private void initiateAdapter() {
        ArrayList<DropboxModel> dropboxModels = getListSumberReferensi();
        ArrayAdapter<DropboxModel> acBranchOfiice = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, dropboxModels);

        ac_referral_lead.setAdapter(acBranchOfiice);
        ac_referral_lead.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dSumberReferral = (DropboxModel) parent.getAdapter().getItem(position);

                if (dSumberReferral.getId() == 1) {

                    if (bcLoginType == Const.BANK_CODE_SINARMAS) {
                        //BSIM user has to select the referral from the dropdown
                        clSelectReferral.setVisibility(View.VISIBLE);

                        //Hide referal input text field for BSIM.
                        cl_nama_referral.setVisibility(View.GONE);

                    } else {
                        //Non BSIM bank have to type the referral name
                        clSelectReferral.setVisibility(View.GONE);
                        cl_nama_referral.setVisibility(View.VISIBLE);
                    }

                } else {
                    cl_nama_referral.setVisibility(View.GONE);
                    clSelectReferral.setVisibility(View.GONE);
                }
            }
        });
    }

    private ArrayList<DropboxModel> getListSumberReferensi() {
        ArrayList<DropboxModel> dropboxModels = new ArrayList<>();
        DropboxModel dbModWalkin = getSumberReferensi(2, "Walk In");
        DropboxModel dbModOwnCust = getSumberReferensi(3, "New to Bank");
        DropboxModel dbModRef = getSumberReferensi(1, "Referral");

        dropboxModels.add(dbModWalkin);
        dropboxModels.add(dbModOwnCust);
        dropboxModels.add(dbModRef);
        return dropboxModels;
    }

    private DropboxModel getSumberReferensi(int id) {
        switch (id) {
            case 1:
                return getSumberReferensi(id, "Referral");
            case 2:
                return getSumberReferensi(id, "Walk In");
            case 3:
                return getSumberReferensi(id, "Own Customer");
            default:
                throw new IllegalArgumentException("Id nya belum di set");
        }
    }

    private DropboxModel getSumberReferensi(int id, String label) {
        DropboxModel dropboxModel = new DropboxModel();
        dropboxModel.setId(id);
        dropboxModel.setLabel(label);

        return dropboxModel;
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick: " + isClickAble);
        if (isClickAble) {
            switch (v.getId()) {
                case R.id.btn_ok:
                    clickSelesai();
                    break;
                case R.id.ac_referral_lead:
                    ac_referral_lead.showDropDown();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.ac_referral_lead:
                if (hasFocus)
                    ac_referral_lead.showDropDown();
                break;
            default:
                throw new IllegalArgumentException("Id belum di set");
        }
    }


    private void clickSelesai() {
        boolean isOkay = true;

        String leadName = et_nama.getText().toString().trim();
        if (leadName.contains("'")) {
            new AlertDialog.Builder(C_FormSimpleLeadActivity.this)
                    .setTitle("Tidak dapat menambahkan lead")
                    .setMessage("Nama lead tidak boleh mengandung karakter tanda petik")
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                    .show();
            return;
        }

        if (et_nama.getText().toString().trim().length() == 0) {
            isOkay = false;
            tv_error_nama.setVisibility(View.VISIBLE);
            tv_error_nama.setText(getString(R.string.error_field_required));
        }

        if (et_umur.getText().toString().trim().length() == 0) {
            isOkay = false;
            tv_error_umur.setVisibility(View.VISIBLE);
            tv_error_umur.setText(getString(R.string.error_field_required));
        }
        String email = et_email.getText().toString();
        if (!email.trim().isEmpty()) {
            if (!email.contains("@") || !email.contains(".")) {
                isOkay = false;
                tv_error_email.setVisibility(View.VISIBLE);
                tv_error_email.setText(getString(R.string.error_invalid_email));

            }
        }

        if (dSumberReferral.getId() == 1 && !Utils.isBankSinarmasBC(bcLoginType)) {
            if (ac_nama_referral.getText().toString().trim().length() == 0) {
                isOkay = false;
                tv_error_nama_referral.setVisibility(View.VISIBLE);
                tv_error_nama_referral.setText(getString(R.string.error_field_required));
            }
        }

        Log.d(TAG, "clickSelesai: " + isOkay);
        if (isOkay) {
            Intent intent = new Intent();
            intent.putExtra(getString(R.string.title), et_nama.getText().toString());
            setResult(RESULT_OK, intent);
            isSave = true;
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isSave)
            saveState();
    }

    private void saveState() {
        C_Insert insert = new C_Insert(C_FormSimpleLeadActivity.this);
        C_Update update = new C_Update(C_FormSimpleLeadActivity.this);


        SharedPreferences sharedPreferences = getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
        String kodeAgen = sharedPreferences.getString(getString(R.string.KODE_AGEN), "");
        Integer SL_APP = sharedPreferences.getInt(getString(R.string.SL_APP), 0);

        simbakLeadModel.setSL_NAME(et_nama.getText().toString());
        simbakLeadModel.setSL_UMUR(Integer.valueOf(et_umur.getText().toString()));
        if (sex != null) simbakLeadModel.setSL_GENDER(sex);
        simbakLeadModel.setSL_APP(SL_APP);
        simbakLeadModel.setSL_UPDATED(kodeAgen);
        simbakLeadModel.setSL_UPTD_DATE(StaticMethods.getTodayDateTime());
        simbakLeadModel.setSL_ACTIVE(1);
        simbakLeadModel.setSL_LAST_POS(1);
        simbakLeadModel.setSL_FAV(0);
        simbakLeadModel.setSL_BAC_CURR_BRANCH(selectedBranchCode);
        simbakLeadModel.setSL_BAC_FIRST_BRANCH(selectedBranchCode);
        simbakLeadModel.setSL_NAMA_CABANG(selectedBranchName);

        if (dSumberReferral.getId() != null) {
            simbakLeadModel.setSL_TYPE(dSumberReferral.getId());
            if (dSumberReferral.getId() == 1) {

                if (bcLoginType == Const.BANK_CODE_SINARMAS) {
                    simbakLeadModel.setSL_BAC_REFF(selectedReferallId);
                    simbakLeadModel.setSL_BAC_REFF_NAME(selectedReferallName);
                } else {
                    simbakLeadModel.setSL_BAC_REFF("");
                    simbakLeadModel.setSL_BAC_REFF_NAME(ac_nama_referral.getText().toString());
                }

            }
        }

        //Add lead mode
        if (!isEditMode) {
            long timestamp = DateUtils.generateTimeStamp();
            //insert
            simbakLeadModel.setSL_TAB_ID(timestamp);
            simbakLeadModel.setSL_CREATED(kodeAgen);
            simbakLeadModel.setSL_CRTD_DATE(StaticMethods.getTodayDateTime());
            long newlyInsertedSlId = insert.insertToSimbakLead(simbakLeadModel);

            createTerimaNasabahActivity(kodeAgen, newlyInsertedSlId, insert);

            //Insert lead record to UsrToLead table
            insert.insertToTableLinkUsrToLead(String.valueOf(newlyInsertedSlId), msagId);

            String handphoneNo = et_handphone.getText().toString().trim();
            String email = et_email.getText().toString().trim();

            simbakAddressHouse.setSLA2_HP1(handphoneNo);
            simbakAddressHouse.setSLA2_EMAIL(email);
            simbakAddressHouse.setSLA2_INST_TAB_ID(newlyInsertedSlId);
            simbakAddressHouse.setSLA2_INST_TYPE(2);
            simbakAddressHouse.setSLA2_ADDR_TYPE(1);
            long SLA2_TAB_ID = insert.insertToSimbakListAddress(simbakAddressHouse);
            simbakAddressHouse.setSLA2_TAB_ID(SLA2_TAB_ID);

            simbakLeadModel.setHome(simbakAddressHouse);

            List<Long> leadIds = new ArrayList<>();
            leadIds.add(newlyInsertedSlId);

            CrmRestClientUsage crmRestClientUsage = new CrmRestClientUsage(this);
            crmRestClientUsage.uploadLead(leadIds);

        } else {
            //Edit lead mode

            String handphoneNo = et_handphone.getText().toString().trim();
            String email = et_email.getText().toString().trim();
            simbakAddressHouse.setSLA2_HP1(handphoneNo);
            simbakAddressHouse.setSLA2_EMAIL(email);
            simbakAddressHouse.setSLA2_INST_TAB_ID(slTabId);
            simbakAddressHouse.setSLA2_INST_TYPE(2);
            simbakAddressHouse.setSLA2_ADDR_TYPE(1);

            //Update lead
            update.updateSimbakLead(simbakLeadModel);
            update.updateSimbakListAddress(simbakAddressHouse);

            simbakLeadModel.setHome(simbakAddressHouse);

            List<Long> leadIds = new ArrayList<>();
            leadIds.add(slTabId);

            CrmRestClientUsage crmRestClientUsage = new CrmRestClientUsage(this);
            crmRestClientUsage.uploadLead(leadIds);
        }


    }


    private void createTerimaNasabahActivity(String msagId, long slTabId, C_Insert insert) {
        SimbakActivityModel simbakActivityModel = new SimbakActivityModel();
        simbakActivityModel.setSLA_CRTD_ID(msagId);
        simbakActivityModel.setSLA_CRTD_DATE(StaticMethods.getTodayDateTime());
        simbakActivityModel.setSLA_UPDTD_ID(msagId);
        simbakActivityModel.setSLA_UPDTD_DATE(StaticMethods.getTodayDateTime());
        simbakActivityModel.setSLA_ACTIVE(1);
        simbakActivityModel.setSLA_LAST_POS(1);
        simbakActivityModel.setSLA_SDATE(StaticMethods.getTodayDateTime());
        simbakActivityModel.setSLA_TYPE(50);
        simbakActivityModel.setSLA_SUBTYPE(48);
        simbakActivityModel.setSLA_DETAIL("Terima Nasabah");
        simbakActivityModel.setSLA_SRC(dSumberReferral.getId());
        simbakActivityModel.setSLA_IDATE(StaticMethods.getTodayDateTime());
        simbakActivityModel.setSLA_INST_TAB_ID(slTabId);
        simbakActivityModel.setSLA_INST_TYPE(CrmTableCode.LEAD);
        simbakActivityModel.setSL_NAME(et_nama.getText().toString().trim());
        simbakActivityModel.setSL_ID_(null);
        long timestamp = DateUtils.generateTimeStamp();
        simbakActivityModel.setSLA_TAB_ID(timestamp);

        insert.insertToSimbakListActivity(simbakActivityModel);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        BcBranch model = (BcBranch) parent.getItemAtPosition(position);
        selectedBranchCode = model.getAjsCbng();
        selectedBranchName = model.getCabang();

        initReferralSpinner(selectedBranchCode, selectedBranchName);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    /**
     * Fetch BC branch from the server and insert it to database
     */
    private void fetchBCBranchFromServer() {
        tvSelectBranch.setVisibility(View.GONE);
        showLoadingProgressBar(true, "Mengunduh data cabang terbaru");

        String url = AppConfig.getBaseUrlCRM().concat("v2/toll/get_reff?msag_id=").concat(msagId);
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<List<BcBranch>> call = api.fetchBcBranch(url);
        call.enqueue(new Callback<List<BcBranch>>() {
            @Override
            public void onResponse(Call<List<BcBranch>> call, Response<List<BcBranch>> response) {

                if (response.body() != null) {
                    List<BcBranch> branches = response.body();

                    if (branches != null && branches.size() > 0) {

                        for (BcBranch branch : branches) {
                            String cabang = branch.getCabang();

                            //Remove single quote (if exist) from branch name
                            if (cabang.contains("'")) {
                                cabang = cabang.replaceAll("\'","");
                                branch.setCabang(cabang);
                            }

                        }
                        
                        bcBranchSpinnerAdapter.addAll(branches);

                        //Insert agent branch to database
                        C_Insert insert = new C_Insert(C_FormSimpleLeadActivity.this);
                        insert.insertAgentBranchToDatabase(branches);
                    }



                }

                showLoadingProgressBar(false, null);

            }

            @Override
            public void onFailure(Call<List<BcBranch>> call, Throwable t) {
                Toast.makeText(C_FormSimpleLeadActivity.this, "Gagal mengunduh data cabang " + t.getMessage(), Toast.LENGTH_LONG).show();
                showLoadingProgressBar(false, null);
            }
        });
    }

    /**
     * Fetch referall from the server and insert it to database (For BSIM only)
     */
    private void fetchReferralFromServer(String selectedBranchCode, boolean shouldReturnActiveReferrallOnly, String msagId) {
        tvSelectReferral.setVisibility(View.GONE);
        showLoadingProgressBar(true, "Mengunduh data terbaru");

        String url = AppConfig.getBaseUrlCRM().concat("main/module/external/get_reff");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<List<Referral>> call = api.fetchReferral(url, selectedBranchCode, shouldReturnActiveReferrallOnly, msagId);
        call.enqueue(new Callback<List<Referral>>() {
            @Override
            public void onResponse(Call<List<Referral>> call, Response<List<Referral>> response) {

                if (response.body() != null) {
                    List<Referral> referrals = response.body();

                    //Refresh the spinnerBcBranch with the latest data
                    if (referrals != null && referrals.size() > 0)
                        referallSpinnerAdapter.addAll(referrals);

                    //Insert referral to database
                    E_Insert insert = new E_Insert(C_FormSimpleLeadActivity.this);
                    insert.insertReferallListToDatabase(referrals);
                }

                showLoadingProgressBar(false, null);

            }

            @Override
            public void onFailure(Call<List<Referral>> call, Throwable t) {
                Toast.makeText(C_FormSimpleLeadActivity.this, "Gagal mengunduh data referral " + t.getMessage(), Toast.LENGTH_LONG).show();
                showLoadingProgressBar(false, null);
            }
        });
    }


    /**
     * Toggle loading progressbar indicator
     */
    private void showLoadingProgressBar(boolean shouldShowProgress, String loadingMessage) {
        if (shouldShowProgress) {
            layoutLoading.setVisibility(View.VISIBLE);
            tvLoadingMessage.setText(loadingMessage);
            return;
        }

        new Handler().postDelayed(() -> layoutLoading.setVisibility(View.GONE), 500L);
        tvSelectBranch.setVisibility(View.VISIBLE);
        tvSelectReferral.setVisibility(View.VISIBLE);
    }

    private class MTextWatcher implements TextWatcher {
        private View view;

        MTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                case R.id.et_nama:
                    if (StaticMethods.isTextWidgetEmpty(et_nama)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_nama.setColorFilter(color);
                    tv_error_nama.setVisibility(View.GONE);
                    break;
                case R.id.et_umur:
                    if (StaticMethods.isTextWidgetEmpty(et_umur)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_umur.setColorFilter(color);
                    tv_error_umur.setVisibility(View.GONE);
                    break;
                case R.id.et_email:
                    tv_error_umur.setVisibility(View.GONE);
                    break;
                case R.id.ac_referral_lead:
                    tv_error_referal_lead.setVisibility(View.GONE);
                    tv_error_nama_referral.setVisibility(View.GONE);
                    break;
                case R.id.ac_nama_referral:
                    tv_error_nama_referral.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }
    }

    class BcBranchSpinnerAdapter extends ArrayAdapter<BcBranch> {
        private List<BcBranch> branches;
        private LayoutInflater inflater;

        public BcBranchSpinnerAdapter(@NonNull Context context, int resource, List<BcBranch> branches) {
            super(context, resource);
            this.branches = branches;
            inflater = LayoutInflater.from(context);
        }

        public void addAll(List<BcBranch> bcBranches) {
            for (BcBranch branch : bcBranches) {

                //Trim the whitespace
                String ajsCabang = branch.getAjsCbng().trim();
                branch.setAjsCbng(ajsCabang);

                branches.add(branch);
            }
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return branches.size();
        }

        @Override
        public BcBranch getItem(int position) {
            return branches.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflater.inflate(R.layout.spinner_bc_branch, null);
            TextView tvBranchName = convertView.findViewById(R.id.tvBranchName);
            tvBranchName.setText(branches.get(position).getCabang());
            return convertView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            convertView = inflater.inflate(R.layout.spinner_bc_branch, null);
            TextView tvBranchName = convertView.findViewById(R.id.tvBranchName);
            tvBranchName.setText(branches.get(position).getCabang());
            return convertView;
        }
    }

    class ReferralSpinnerAdapter extends ArrayAdapter<Referral> {
        private List<Referral> referrals;
        private LayoutInflater inflater;

        public ReferralSpinnerAdapter(@NonNull Context context, int resource, List<Referral> referrals) {
            super(context, resource);
            this.referrals = referrals;
            inflater = LayoutInflater.from(context);
        }

        public void addAll(List<Referral> referralList) {
            for (Referral referral : referralList) {
                referrals.add(referral);
            }
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return referrals.size();
        }

        @Override
        public Referral getItem(int position) {
            return referrals.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflater.inflate(R.layout.spinner_referral, null);
            TextView tvReferralName = convertView.findViewById(R.id.tvReferralName);
            tvReferralName.setText(referrals.get(position).getReffName());
            return convertView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            convertView = inflater.inflate(R.layout.spinner_referral, null);
            TextView tvReferralName = convertView.findViewById(R.id.tvReferralName);
            tvReferralName.setText(referrals.get(position).getReffName());
            return convertView;
        }
    }
}
