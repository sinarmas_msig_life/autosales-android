package id.co.ajsmsig.nb.prop.model.modelCalcIllustration;

import java.io.Serializable;

public class Prop_Model_OptionVO implements Serializable {

    private String value;
    private String label;

    public Prop_Model_OptionVO() {
    }

    public Prop_Model_OptionVO(String value, String label) {
        this.value = value;
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
