package id.co.ajsmsig.nb.pointofsale.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.pointofsale.binding.LoadingButtonListener
import id.co.ajsmsig.nb.pointofsale.custom.LoadingButton
import id.co.ajsmsig.nb.databinding.PosRowProductDetailBinding
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.ProductDetail

/**
 * Created by andreyyoshuamanik on 21/02/18.
 */
class ProductDetailAdapter: RecyclerView.Adapter<ProductDetailAdapter.OptionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionViewHolder {

        val dataBinding = DataBindingUtil.inflate<PosRowProductDetailBinding>(LayoutInflater.from(parent.context), R.layout.pos_row_product_detail, parent, false)
        return OptionViewHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        options?.let {
            holder.bind(it[position])
        }
    }

    var options: Array<ProductDetail>? = null
    set(value) {
        field = value

        notifyDataSetChanged()
    }

    var listener: LoadingButtonListener? = null


    override fun getItemCount(): Int {
        return options?.size ?: 0
    }

    inner class OptionViewHolder(private val dataBinding: PosRowProductDetailBinding): RecyclerView.ViewHolder(dataBinding.root) {

        fun bind(option: ProductDetail) {
            dataBinding.vm = option
            dataBinding.loadingButtonListener = listener
        }
    }
}