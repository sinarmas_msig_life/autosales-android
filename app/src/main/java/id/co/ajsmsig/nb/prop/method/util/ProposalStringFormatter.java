package id.co.ajsmsig.nb.prop.method.util;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * String Formatter untuk display proposal
 * 
 * @author Daru
 * @since Jun 3, 2016
 *
 */
public class ProposalStringFormatter {
    
    public static String convertToString(Object param) {
        String result;
        if( param instanceof Date)
        {
            result = new SimpleDateFormat("dd/MM/yyyy").format(param);
        }
        else if( param instanceof Integer)
        {
            result = String.valueOf(param);
        }
        else if( param instanceof BigDecimal)
        {
            DecimalFormat df = new DecimalFormat("###,###,###,###,##0.00");
            result = df.format(((BigDecimal) param).doubleValue());
        }
        else if( param instanceof Double)
        {
            DecimalFormat df = new DecimalFormat("###,###,###,###,##0.00");
            result = df.format(param);
        }
        else if( param instanceof String)
        {
            result = (String) param;
        }
        else
        {
            if( param == null )
            {
                result = "";
            }
            else
            {
                result = param.toString();
            }
        }

        return result;
    }
    
    public static String convertToStringWithoutCentAndNillIfNegative(Object param )
    {

        String result = param.toString();
        if ( result != null && !result.equals("")){
            BigDecimal value = new BigDecimal( result );
            if( value.compareTo( new BigDecimal( "0" ) ) < 0 )
            {
                result = "nil";
            }
            else
            {
                result = convertToStringWithoutCent( value );
            }
        }

        return result;
    }
    
    public static String convertToStringWithoutCent(Object param )
    {
        String result = convertToString( param );
        if( !"".equals( result ) )
        {
            result = result.substring( 0, result.length() - 3 );
        }
        return result;
    }
    
    public static String convertToStringWithoutCentAndSetNill(double celaka, double np )
    {
        String result;

        if( celaka < 0 )
        {
            result = "nil";
        }
        else if( np > 0 )
        {
            result = convertToStringWithoutCent( celaka );
        }
        else
        {
            result = "nil";
        }

        return result;
    }
    
    public static String convertToRoundedNoDigit(BigDecimal amount )
    {
        String result;
        amount = amount.setScale( 0, RoundingMode.HALF_UP );
        DecimalFormat decimalFormat = new DecimalFormat( "###,###,###,###,##0" );
        result = decimalFormat.format( amount );

        return result;
    }

}
