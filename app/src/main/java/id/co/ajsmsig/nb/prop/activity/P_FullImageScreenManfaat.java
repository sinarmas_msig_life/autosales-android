package id.co.ajsmsig.nb.prop.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.co.ajsmsig.nb.BuildConfig;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.adapter.P_ExtendedViewPager;
import id.co.ajsmsig.nb.prop.adapter.P_ImagePagerAdapter;

/**
 * Created by rizky_c on 18/09/2017.
 */

public class P_FullImageScreenManfaat extends AppCompatActivity {

    private static final String TAG = P_FullImageScreenManfaat.class.getSimpleName();
    private ArrayList<HashMap<String, String>> imagePaths;
    private ArrayList <Integer> rsc_image;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_full_image_screen_manfaat);

        // get intent id.co.ajsmsig.nb.data
        Bundle bundle = getIntent().getExtras();


        imagePaths = (ArrayList<HashMap<String, String>>) bundle.getSerializable("array");
        Log.d(TAG, "onCreate: jumlah array is " + imagePaths.size());

        // Selected image id
        int position = bundle.getInt("id");
        Log.d(TAG, "onCreate: id position is " + position);
        List<ImageView> images = new ArrayList<>();
        rsc_image = new ArrayList<>();

        for (HashMap<String, String> image: imagePaths) {
            ImageView imageView = new ImageView(this);

            int resID = getResources().getIdentifier(image.get("path"), "drawable", BuildConfig.APPLICATION_ID);
//            Picasso.with(this).load(resID).into(imageView);
            images.add(imageView);
            rsc_image.add(resID);
        }
        P_ExtendedViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(new P_ImagePagerAdapter(rsc_image));
        viewPager.setCurrentItem(position);
    }

}