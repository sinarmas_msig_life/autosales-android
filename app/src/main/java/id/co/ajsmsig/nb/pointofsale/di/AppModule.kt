package id.co.ajsmsig.nb.pointofsale.di

import android.app.Application
import dagger.Module
import dagger.Provides
import id.co.ajsmsig.nb.pointofsale.AppExecutors
import id.co.ajsmsig.nb.pointofsale.api.APIService
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PrePopulatedDB
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.MainDB
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository
import id.co.ajsmsig.nb.pointofsale.utils.Constants
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import id.co.ajsmsig.nb.pointofsale.utils.LiveDataCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor



/**
 * Created by andreyyoshuamanik on 26/02/18.
 */
@Module(includes = arrayOf(ViewModelModule::class))
class AppModule {

    @Provides
    @Singleton
    fun provideAPIService() : APIService {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        return Retrofit.Builder()
                .addConverterFactory(
                        GsonConverterFactory.create())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .baseUrl("${Constants.API_URL}/api/")
                .client(client)
                .build()
                .create(APIService::class.java)
    }



    @Provides
    @Singleton
    fun provideMainDB(app: Application): MainDB = MainDB.getInstance(app)

    @Provides
    @Singleton
    fun provideMainRepository(mainDB: MainDB, appExecutors: AppExecutors, apiService: APIService): MainRepository = MainRepository.getInstance(mainDB, appExecutors, apiService)

    @Provides
    @Singleton
    fun provideInitialDb(app: Application): PrePopulatedDB = PrePopulatedDB.getInstance(app)

    @Provides
    @Singleton
    fun provideInitialRepository(db: PrePopulatedDB): PrePopulatedRepository = PrePopulatedRepository.getInstance(db)

    @Provides
    @Singleton
    fun provideSharedViewModel(app:Application, repository: MainRepository): SharedViewModel = SharedViewModel(app, repository)
}