package id.co.ajsmsig.nb.prop.model.modelCalcIllustration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class IllustrationResultVO implements Serializable
{
	private static final long serialVersionUID = 7758479117856050487L;
	private ArrayList<LinkedHashMap<String, String>> illustrationList;
    private String validityMsg;

    public ArrayList<LinkedHashMap<String, String>> getIllustrationList()
    {
        return illustrationList;
    }

    public void setIllustrationList( ArrayList<LinkedHashMap<String, String>> illustrationList )
    {
        this.illustrationList = illustrationList;
    }

    public String getValidityMsg()
    {
        return validityMsg;
    }

    public void setValidityMsg( String validityMsg )
    {
        this.validityMsg = validityMsg;
    }
}
