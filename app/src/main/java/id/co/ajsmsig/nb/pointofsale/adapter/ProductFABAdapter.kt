package id.co.ajsmsig.nb.pointofsale.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosRowProductFabBinding
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductBenefit

/**
 * Created by andreyyoshuamanik on 21/02/18.
 */
class ProductFABAdapter: RecyclerView.Adapter<ProductFABAdapter.OptionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionViewHolder {

        val dataBinding = DataBindingUtil.inflate<PosRowProductFabBinding>(LayoutInflater.from(parent.context), R.layout.pos_row_product_fab, parent, false)
        return OptionViewHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        options?.let {
            holder.bind(it[position])
        }
    }

    var options: Array<ProductBenefit>? = null
    set(value) {
        field = value

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return options?.size ?: 0
    }

    inner class OptionViewHolder(private val dataBinding: PosRowProductFabBinding): RecyclerView.ViewHolder(dataBinding.root) {

        fun bind(option: ProductBenefit) {
            dataBinding.vm = option
        }

    }
}