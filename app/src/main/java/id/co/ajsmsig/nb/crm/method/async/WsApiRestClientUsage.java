package id.co.ajsmsig.nb.crm.method.async;
/*
  Created by Eriza on 03/03/2018.
 */

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;
import id.co.ajsmsig.nb.crm.method.StaticMethods;

public class WsApiRestClientUsage {
    private final static String TAG = WsApiRestClient.class.getSimpleName();
    private Context context;
    private ListenerInfo mListenerInfo;

    private static class ListenerInfo {
        ReferralListener referralListener;
        MemberIDListener memberIDListener;
        CheckAppListener mcheckAppListener;
    }

    public WsApiRestClientUsage(Context context) {
        this.context = context;
    }

    //set All Listener Here
   private ListenerInfo getListenerInfo() {
        if (mListenerInfo != null) {
            return mListenerInfo;
        }
        mListenerInfo = new ListenerInfo();
        return mListenerInfo;
    }
    public void setonMemberIDListener(MemberIDListener l) {
        getListenerInfo().memberIDListener = l;
    }

    public void setonReferralListener(ReferralListener l) {
        getListenerInfo().referralListener = l;
    }

    public void setonCheckAppListener(CheckAppListener l) {
        getListenerInfo().mcheckAppListener = l;
    }
    //set Interface
    public interface MemberIDListener {
        void onMemberIDSucceess(int statusCode, Header[] headers, JSONObject response);

        void onMemberIDFailure(String title, String message, JSONObject response);
    }

    public interface ReferralListener {
        void onReferralSuccess(int statusCode, Header[] headers, JSONArray response);

        void onReferralFailure(String title, String message);
    }

    public interface CheckAppListener {
        void onCheckAppSuccess(int statusCode, Header[] headers, JSONObject response);

        void onCheckAppFailure(String title, String message);
    }

    //set All Method Here
    public void getMemberID(String member){
        final boolean result;
        final WsApiRestClientUsage.ListenerInfo li = mListenerInfo;
        String path = "";
        path = "member/api/smws/fetch_data/"+member;
        if (li != null && li.memberIDListener != null) {

            WsApiRestClient.get(path, null, 60000, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    li.memberIDListener.onMemberIDSucceess(statusCode, headers, response);

                }
                @Override
                public void onFailure(final int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    li.memberIDListener.onMemberIDFailure("Gagal", throwable.getMessage() , errorResponse);
                }


            });

        }
    }

    public void getRefferal(int jenis_bank) {
        final boolean result;
        final ListenerInfo li = mListenerInfo;
        String path = "";
        path = "member/api/json/data_bc/" + jenis_bank;
//        if(jenis_bank == 2){
//            path = "member/api/json/data_bc/2~16";    //masih belum dipastikan untuk referal bank sinarmas
//        }
        if (li != null && li.referralListener != null) {

            WsApiRestClient.get(path, null, 60000, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    li.referralListener.onReferralSuccess(statusCode, headers, response);

                }

                @Override
                public void onFailure(final int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    li.referralListener.onReferralFailure("Gagal", throwable.getMessage() + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    li.referralListener.onReferralFailure("Gagal", throwable.getMessage() + errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    li.referralListener.onReferralFailure("Gagal", throwable.getMessage() + responseString);
                }

            });

        }
    }
    public void onCheckAppUpdate() {
        final ListenerInfo li = mListenerInfo;
        if (li != null && li.mcheckAppListener != null) {
            try {
                String spajJson = "";
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("flag_app", 1);
                jsonObject.put("flag_platform", 1);

//                JSONObject JSONFINAL = new JSONObject();
//                JSONFINAL.put("data", jsonObject);
                spajJson = jsonObject.toString();
                StaticMethods.backUpToJsonFile(spajJson, "SpajJson", "SpajJson");

                StringEntity entity = new StringEntity(spajJson);
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                WsApiRestClient.post(context, "member/api/json/code_version/", entity, "application/json", 60000, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        li.mcheckAppListener.onCheckAppSuccess(statusCode, headers, response);

                    }

                    @Override
                    public void onFailure(final int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        li.mcheckAppListener.onCheckAppFailure("Gagal", throwable.getMessage() + errorResponse);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        li.mcheckAppListener.onCheckAppFailure("Gagal", throwable.getMessage() + errorResponse);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        li.mcheckAppListener.onCheckAppFailure("Gagal", throwable.getMessage() + responseString);
                    }

                });
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "login: error is " + e.getMessage());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
