package id.co.ajsmsig.nb.crm.fragment.listlead;

import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

import id.co.ajsmsig.nb.R;

/**
 * Created by Fajar.Azhar on 21/03/2018.
 */

public class C_LeadAdapter extends ExpandableRecyclerViewAdapter<C_LeadAdapter.C_GroupLeadViewHolder, C_LeadAdapter.C_ChildLeadViewHolder> {
    private C_LeadAdapter.ClickListener clickListener;
    public C_LeadAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    class C_ChildLeadViewHolder extends ChildViewHolder {
        private TextView tvLeadNameInitial;
        private TextView tvName;
        private TextView tvAge;
        private TextView tvCurrentBranch;
        private ConstraintLayout layoutLeads;
        private ImageView imgFavoriteLead;

        public C_ChildLeadViewHolder(View view) {
            super(view);
            layoutLeads = view.findViewById(R.id.layoutLeads);
            tvLeadNameInitial = view.findViewById(R.id.tvLeadNameInitial);
            tvName = view.findViewById(R.id.tvName);
            tvAge = view.findViewById(R.id.tvAge);
            tvCurrentBranch = view.findViewById(R.id.tvCurrentBranch);
            imgFavoriteLead = view.findViewById(R.id.imgFavoriteLead);
        }

    }

    class C_GroupLeadViewHolder extends GroupViewHolder {
        private TextView groupName;
        private ImageView imgExpandCollapse;

        public C_GroupLeadViewHolder(View itemView) {
            super(itemView);
            groupName = itemView.findViewById(R.id.tvLeadGroupName);
            imgExpandCollapse = itemView.findViewById(R.id.imgExpandCollapse);
        }

        @Override
        public void expand() {
            imgExpandCollapse.setImageResource(R.drawable.ic_expand_less);
        }

        @Override
        public void collapse() {
            imgExpandCollapse.setImageResource(R.drawable.ic_expand_more);
        }

    }

    @Override
    public C_GroupLeadViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.c_group_lead, parent, false);
        return new C_GroupLeadViewHolder(view);
    }

    @Override
    public C_ChildLeadViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.c_child_lead, parent, false);
        return new C_ChildLeadViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(final C_ChildLeadViewHolder holder, int flatPosition, final ExpandableGroup group, int childIndex) {
        final ChildLeadList model = ((ParentLeadList) group).getItems().get(childIndex);

        String age = String.valueOf(model.getslUmur()).concat(" tahun");

        if (!TextUtils.isEmpty(model.getslCurrentBranch())) {
            String currentBranch = "Cabang " + model.getslCurrentBranch();
            holder.tvCurrentBranch.setText(currentBranch);
            holder.tvCurrentBranch.setVisibility(View.VISIBLE);
        } else {
            holder.tvCurrentBranch.setVisibility(View.INVISIBLE);
        }

        if (model.isFavoriteLead()) {
            holder.imgFavoriteLead.setImageResource(R.drawable.ic_star_gold);
        } else {
            holder.imgFavoriteLead.setImageResource(R.drawable.ic_star_gray);
        }

        if (model.getslUmur() > 0) {
            holder.tvAge.setVisibility(View.VISIBLE);
            holder.tvAge.setText(age);
        } else {
            holder.tvAge.setVisibility(View.INVISIBLE);
        }

        String firstNameInitial = model.getSlNameFirstNameInitial();
        String lastNameInitial = model.getSlNameLastNameInitial();

        if (!TextUtils.isEmpty(firstNameInitial)) {
            String initial = firstNameInitial + lastNameInitial;
            holder.tvLeadNameInitial.setText(initial);
        }

        holder.tvName.setText(model.getSlName());

        holder.layoutLeads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onLeadSelected(holder.getAdapterPosition(), view, model);
            }
        });
        holder.imgFavoriteLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!model.isFavoriteLead()) {
                    model.setFavoriteLead(true);
                    holder.imgFavoriteLead.setImageResource(R.drawable.ic_star_gold);

                } else {
                    model.setFavoriteLead(false);
                    holder.imgFavoriteLead.setImageResource(R.drawable.ic_star_gray);
                }

                clickListener.onLeadSelected(holder.getAdapterPosition(), view, model);
            }
        });


    }

    @Override
    public void onBindGroupViewHolder(final C_GroupLeadViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.groupName.setText(group.getTitle());

    }

    public void setClickListener (C_LeadAdapter.ClickListener clickListener) {
        this.clickListener = clickListener;
    }
    public interface ClickListener{
        void onLeadSelected(int position, View view, ChildLeadList leads);
    }


}
