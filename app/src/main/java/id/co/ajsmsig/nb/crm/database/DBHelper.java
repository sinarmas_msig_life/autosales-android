package id.co.ajsmsig.nb.crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.DataUpdateModel;

/**
 * Created by faiz_f on 17/03/2017.
 *
 * Don't edit or modify this class
 */

public class DBHelper extends SQLiteOpenHelper {
    private static final String TAG = "DBHelper";
    private Context context;
    private ArrayList<DataUpdateModel> dataUpdateModels = null;
    private ContentValues contentLatestVersion;

    public DBHelper(Context context, int version) {
        super(context, context.getString(R.string.DATABASE_NAME), null, version);
        this.context = context;
    }

    public DBHelper(Context context, int version, ArrayList<DataUpdateModel> dataUpdateModels, ContentValues ctntLatestVersion) {
        super(context, context.getString(R.string.DATABASE_NAME), null, version);
        this.dataUpdateModels = dataUpdateModels;
        this.context = context;
        this.contentLatestVersion = ctntLatestVersion;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(context.getString(R.string.CREATE_TABLE_SIMBAK_LEAD));
        db.execSQL(context.getString(R.string.CREATE_TABLE_SIMBAK_LIST_ADDRESS));
        db.execSQL(context.getString(R.string.CREATE_TABLE_SIMBAK_ACCOUNT));
        db.execSQL(context.getString(R.string.CREATE_TABLE_SIMBAK_LINK_USR_TO_LEAD));
        db.execSQL(context.getString(R.string.CREATE_TABLE_SIMBAK_LINK_ACC_TO_LEAD));
        db.execSQL(context.getString(R.string.CREATE_TABLE_SIMBAK_LIST_PRODUCT));
        db.execSQL(context.getString(R.string.CREATE_TABLE_SIMBAK_LIST_ACTIVITY));
        db.execSQL(context.getString(R.string.CREATE_TABLE_SIMBAK_LIST_SPAJ));
        db.execSQL(context.getString(R.string.CREATE_TABLE_SIMBAK_AUDIT));
        db.execSQL(context.getString(R.string.CREATE_TABLE_JSON_INIT));
        db.execSQL(context.getString(R.string.CREATE_TABLE_JSON_ADDRESS_TYPE));
        db.execSQL(context.getString(R.string.CREATE_TABLE_JSON_COUNTRY));
        db.execSQL(context.getString(R.string.CREATE_TABLE_JSON_ID_TYPE));
        db.execSQL(context.getString(R.string.CREATE_TABLE_JSON_LAST_EDUCATION));
        db.execSQL(context.getString(R.string.CREATE_TABLE_JSON_MARRIAGE));
        db.execSQL(context.getString(R.string.CREATE_TABLE_JSON_MONTH_SALARY));
        db.execSQL(context.getString(R.string.CREATE_TABLE_JSON_RELIGION));
        db.execSQL(context.getString(R.string.CREATE_TABLE_JSON_JOB_OCCUPATION));
        db.execSQL(context.getString(R.string.CREATE_TABLE_JSON_ACTIVITY));
        db.execSQL(context.getString(R.string.CREATE_TABLE_JSON_SUB_ACTIVITY));
        db.execSQL(context.getString(R.string.CREATE_TABLE_JSON_BRANCH_OFFICE));
        db.execSQL(context.getString(R.string.CREATE_TABLE_JSON_REFFERAL));
        db.execSQL(context.getString(R.string.CREATE_TABLE_MST_DATA_PROPOSAL));
        db.execSQL(context.getString(R.string.CREATE_TABLE_MST_PROPOSAL_PRODUCT));
        db.execSQL(context.getString(R.string.CREATE_TABLE_MST_PROPOSAL_PRODUCT_RIDER));
        db.execSQL(context.getString(R.string.CREATE_TABLE_MST_PROPOSAL_PRODUCT_TOPUP));
        db.execSQL(context.getString(R.string.CREATE_TABLE_MST_PROPOSAL_PRODUCT_PESERTA));
        db.execSQL(context.getString(R.string.CREATE_TABLE_MST_PROPOSAL_PRODUCT_ULINK));
        db.execSQL(context.getString(R.string.CREATE_TABLE_MST_ILUSTRATION_DEVELOPMENT_FUND));
        db.execSQL(context.getString(R.string.CREATE_TABLE_LST_APP_VERSION));
        db.execSQL(context.getString(R.string.CREATE_TABLE_LST_APP_VERSION_LOG));
        //ESPAJ
        db.execSQL(context.getString(R.string.CREATE_MST_SPAJ));
          db.execSQL(context.getString(R.string.CREATE_PEMEGANG_POLIS));
        db.execSQL(context.getString(R.string.CREATE_TERTANGGUNG));
        db.execSQL(context.getString(R.string.CREATE_CALON_PEMBAYAR_PREMI));
        db.execSQL(context.getString(R.string.CREATE_USULAN_ASURANSI));
        db.execSQL(context.getString(R.string.CREATE_DETIL_INVESTASI));
        db.execSQL(context.getString(R.string.CREATE_DETIL_AGEN));
        db.execSQL(context.getString(R.string.CREATE_PROFILE_RESIKO));
        db.execSQL(context.getString(R.string.CREATE_ADD_RIDER_UA));
        db.execSQL(context.getString(R.string.CREATE_ASKES_UA));
        db.execSQL(context.getString(R.string.CREATE_JNS_DANA_DI));
        db.execSQL(context.getString(R.string.CREATE_DATA_DITUNJUK_DI));
        db.execSQL(context.getString(R.string.CREATE_TTD));
        db.execSQL(context.getString(R.string.CREATE_DP));
        db.execSQL(context.getString(R.string.CREATE_QUESTION_ANSWER));
        db.execSQL(context.getString(R.string.CREATE_TABEL_TTNO3UQ));
        db.execSQL(context.getString(R.string.CREATE_TABEL_TTNO5UQ));
        db.execSQL(context.getString(R.string.CREATE_TABEL_NO9TTUQ));
        db.execSQL(context.getString(R.string.CREATE_TABEL_NO9TTUQ));
        db.execSQL(context.getString(R.string.CREATE_TABEL_TABEL_KETKESUQ));

        if (dataUpdateModels != null) {
            for (DataUpdateModel dataUpdateModel : dataUpdateModels) {
                Log.e(TAG, "onCreate: " + dataUpdateModel.getLOG_ID());
                if (dataUpdateModel.getLOG_ID().equals("632853")) { //mengecek apakah tabelk sudah punya column SLA_LAST_POST atau belum
                    // coding kondisi dan angka log_id disini jangan diubah atau diedit,
                    try {
                        db.execSQL(dataUpdateModel.getQUERY_VALUE());
                    } catch (SQLiteException ex) { //
                        Log.w(TAG, "Altering SIMBAK_LIST_ACTIVITY : " + ex.getMessage());
                    }
                }else {
                    db.execSQL(dataUpdateModel.getQUERY_VALUE());
                }
            }
            Log.e(TAG, "onCreate: Finish");
            boolean updateSuccess = false;
            if (contentLatestVersion != null) {
                updateSuccess = insertToTable(db, context.getString(R.string.LST_APP_VERSION), contentLatestVersion);

                if(updateSuccess) {
                    SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.update_data_preferences), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(context.getString(R.string.data_version), contentLatestVersion.getAsString(context.getString(R.string.data_version)));
                    editor.putInt(context.getString(R.string.lav_data_id), contentLatestVersion.getAsInteger(context.getString(R.string.lav_data_id)));
                    editor.putString(context.getString(R.string.last_update), contentLatestVersion.getAsString(context.getString(R.string.last_update)));
                    editor.apply();

                    Log.d(TAG, "updateSuccess");
                }
            }

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < newVersion) {
            onCreate(db);
        }
    }

    private boolean insertToTable(SQLiteDatabase db, String tableName, ContentValues content) {
        long rowInserted = db.insert(tableName, null, content);
        return rowInserted != -1;
    }

    public int selectLavDataIdAppVersion() {
        int lavDataId = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = context.getString(R.string.selectLavDataIdAppVersion);
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            lavDataId = cursor.getInt(cursor.getColumnIndex(context.getString(R.string.LAV_DATA_ID)));

            cursor.moveToNext();
        }
        cursor.close();

        return lavDataId;
    }
}
