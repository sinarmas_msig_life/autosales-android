package id.co.ajsmsig.nb.pointofsale.model.db.dynamic.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.UserRecord


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
abstract class UserRecordDao: BaseDao<UserRecord> {

    @Query("select * from UserRecord")
    abstract fun loadAllUserRecord(): LiveData<List<UserRecord>>

    @Query("select * from UserRecord where userId = :userId and complete = 0 limit 1")
    abstract fun loadUserAndRecordWithUserId(userId: Long?): LiveData<UserRecord>

    @Query("select * from UserRecord where userId = :userId and groupId = :groupId and complete = 0 limit 1")
    abstract fun loadUserAndRecordWithUserId(userId: Long?, groupId: Int?): LiveData<UserRecord>




}