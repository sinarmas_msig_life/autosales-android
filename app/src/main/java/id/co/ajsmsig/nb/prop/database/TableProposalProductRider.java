package id.co.ajsmsig.nb.prop.database;
/*
 *Created by faiz_f on 10/05/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductPesertaModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductRiderModel;

public class TableProposalProductRider {
    private Context context;

    public TableProposalProductRider(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(P_MstProposalProductRiderModel p_mstProposalProductRiderModel) {
        ContentValues cv = new ContentValues();
        cv.put(context.getString(R.string.NO_PROPOSAL_TAB), p_mstProposalProductRiderModel.getNo_proposal_tab());
        cv.put(context.getString(R.string.NO_PROPOSAL), p_mstProposalProductRiderModel.getNo_proposal());
        cv.put(context.getString(R.string.LSBS_ID), p_mstProposalProductRiderModel.getLsbs_id());
        cv.put(context.getString(R.string.LSDBS_NUMBER), p_mstProposalProductRiderModel.getLsdbs_number());
        cv.put(context.getString(R.string.LKU_ID), p_mstProposalProductRiderModel.getLku_id());
        cv.put(context.getString(R.string.PREMI), p_mstProposalProductRiderModel.getPremi());
        cv.put(context.getString(R.string.UP), p_mstProposalProductRiderModel.getUp());
        cv.put(context.getString(R.string.PERSEN_UP), p_mstProposalProductRiderModel.getPersen_up());
        cv.put(context.getString(R.string.KELAS), p_mstProposalProductRiderModel.getKelas());
        cv.put(context.getString(R.string.JML_UNIT), p_mstProposalProductRiderModel.getJml_unit());
        cv.put(context.getString(R.string.BULAN_KE), p_mstProposalProductRiderModel.getBulan_ke());

        return cv;
    }

    public ArrayList<P_MstProposalProductRiderModel> getObjectArray(Cursor cursor, boolean withPeserta) {
        ArrayList<P_MstProposalProductRiderModel> p_mstProposalProductRiderModels = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                P_MstProposalProductRiderModel p_mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB)))) {
                    p_mstProposalProductRiderModel.setNo_proposal_tab(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL)))) {
                    p_mstProposalProductRiderModel.setNo_proposal(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LSBS_ID)))) {
                    p_mstProposalProductRiderModel.setLsbs_id(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LSBS_ID))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LSDBS_NUMBER)))) {
                    p_mstProposalProductRiderModel.setLsdbs_number(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LSDBS_NUMBER))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LKU_ID)))) {
                    p_mstProposalProductRiderModel.setLku_id(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.LKU_ID))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.PREMI)))) {
                    p_mstProposalProductRiderModel.setPremi(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.PREMI))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.UP)))) {
                    p_mstProposalProductRiderModel.setUp(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.UP))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.PERSEN_UP)))) {
                    p_mstProposalProductRiderModel.setPersen_up(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.PERSEN_UP))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.KELAS)))) {
                    p_mstProposalProductRiderModel.setKelas(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.KELAS))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.JML_UNIT)))) {
                    p_mstProposalProductRiderModel.setJml_unit(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.JML_UNIT))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.BULAN_KE)))) {
                    p_mstProposalProductRiderModel.setBulan_ke(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.BULAN_KE))));
                }

                if(withPeserta) {
                    String selection = context.getString(R.string.NO_PROPOSAL_TAB) + " = ? AND " +
                            context.getString(R.string.LSBS_ID) + " = ?";
                    String[] selectionArgs = new String[]{
                            p_mstProposalProductRiderModel.getNo_proposal_tab(),
                            String.valueOf(p_mstProposalProductRiderModel.getLsbs_id())
                    };

                    Cursor cursorPeserta = new P_Select(context).query(
                            context.getString(R.string.MST_PROPOSAL_PRODUCT_PESERTA),
                            null,
                            selection,
                            selectionArgs,
                            null
                    );
                    ArrayList<P_MstProposalProductPesertaModel> p_mstProposalProductPesertaModels = new TableProposalProductPeserta(context).getObjectArray(cursorPeserta, true);
                    Log.d("TAG", "getObjectArray: peserta jumlah adalah " + p_mstProposalProductPesertaModels.size());
                    p_mstProposalProductRiderModel.setP_mstProposalProductPesertaModels(p_mstProposalProductPesertaModels);
                }

                p_mstProposalProductRiderModels.add(p_mstProposalProductRiderModel);
                cursor.moveToNext();
            }

            // always close the cursor
            cursor.close();
        }

        return p_mstProposalProductRiderModels;
    }

    public P_MstProposalProductRiderModel getObject(Cursor cursor) {
        P_MstProposalProductRiderModel riderModel = new P_MstProposalProductRiderModel();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB)))) {
                riderModel.setNo_proposal_tab(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL)))) {
                riderModel.setNo_proposal(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LSBS_ID)))) {
                riderModel.setLsbs_id(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LSBS_ID))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LSDBS_NUMBER)))) {
                riderModel.setLsdbs_number(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LSDBS_NUMBER))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LKU_ID)))) {
                riderModel.setLku_id(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.LKU_ID))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.PREMI)))) {
                riderModel.setPremi(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.PREMI))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.UP)))) {
                riderModel.setUp(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.UP))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.PERSEN_UP)))) {
                riderModel.setPersen_up(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.PERSEN_UP))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.KELAS)))) {
                riderModel.setKelas(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.KELAS))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.JML_UNIT)))) {
                riderModel.setJml_unit(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.JML_UNIT))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.BULAN_KE)))) {
                riderModel.setBulan_ke(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.BULAN_KE))));
            }

            String selection = context.getString(R.string.NO_PROPOSAL_TAB) + " = ? AND " +
                    context.getString(R.string.LSBS_ID) + " = ? AND " +
                    context.getString(R.string.LSDBS_NUMBER) + " = ?";
            String[] selectionArgs = new String[]{
                    riderModel.getNo_proposal_tab(),
                    String.valueOf(riderModel.getLsbs_id()),
                    String.valueOf(riderModel.getLsdbs_number())
            };
            Cursor cursorPeserta = new P_Select(context).query(
                    context.getString(R.string.MST_PROPOSAL_PRODUCT_PESERTA),
                    null,
                    selection,
                    selectionArgs,
                    null
            );
            ArrayList<P_MstProposalProductPesertaModel> p_mstProposalProductPesertaModels = new TableProposalProductPeserta(context).getObjectArray(cursorPeserta, true);
            riderModel.setP_mstProposalProductPesertaModels(p_mstProposalProductPesertaModels);

            // always close the cursor
            cursor.close();
        }

        return riderModel;
    }
}
