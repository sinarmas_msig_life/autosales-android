package id.co.ajsmsig.nb.pointofsale.handler

import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */
interface OnOptionSelected {
    fun optionSelected(option: PageOption)
}