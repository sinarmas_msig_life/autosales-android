package id.co.ajsmsig.nb.prop.method.calculateIlustration;

import android.content.Context;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.prop.method.util.CommonUtil;
import id.co.ajsmsig.nb.prop.method.util.ProposalStringFormatter;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.IllustrationResultVO;


public class IllustrationFormula212 extends IllustrationFormula {
    ;

    public IllustrationFormula212(Context context) {
        super(context);
    }

    @Override
    public HashMap<String, Object> getIllustrationResult(String no_proposal) {
        ArrayList<LinkedHashMap<String, String>> mapList = new ArrayList<>();
        LinkedHashMap<String, String> map;
        double[] listTahapan = new double[23];
        double premi = 0.0;
        String lku_id = "";
        int bisnisId = 0, usiaTT = 0, lscb_id = 0, lamaBayar = 0, lsdbs_number = 0;

        
        HashMap<String, Object> dataProposal = getSelect().selectDataProposal(no_proposal);
        if(!CommonUtil.isEmpty(dataProposal)) {
            bisnisId = ((BigDecimal) dataProposal.get("LSBS_ID")).intValue();
            usiaTT =  Integer.valueOf(dataProposal.get("UMUR_TT").toString());
            premi =  Double.valueOf(dataProposal.get("PREMI").toString());
            lscb_id = Integer.valueOf(dataProposal.get("LSCB_ID").toString());
            lku_id = dataProposal.get("LKU_ID").toString();
            lamaBayar = Integer.valueOf(dataProposal.get("THN_LAMA_BAYAR").toString());
            lsdbs_number = ((BigDecimal) dataProposal.get("LSDBS_NUMBER")).intValue();
        }
        
        Integer pset_id = getSelect().selectPsetIdProposal(no_proposal);
        HashMap<String, Object> productCalc = getSelect().selectProductCalc(pset_id, lku_id, lscb_id);
        
        for( int i = 1; i <= lamaBayar; i++ )
        {

            listTahapan[i] = 0;
            map = new LinkedHashMap<>();
            map.put( "yearNo", ProposalStringFormatter.convertToString(i) );
//            map.put( "age", ProposalStringFormatter.convertToString(usiaTT+i) );
                         
            map.put( "premi", ProposalStringFormatter.convertToRoundedNoDigit(new BigDecimal(premi)));
            
            String queryNT = getSelect().selectProductFormula((Integer) productCalc.get("LPF_POLICY_VALUE"));
            queryNT = queryNT.replaceAll("\\$P\\{LSBS_ID\\}", String.valueOf(bisnisId));
            queryNT = queryNT.replaceAll("\\$P\\{PREMIUM\\}", String.valueOf(premi));
            queryNT = queryNT.replaceAll("\\$P\\{LI_USIA_TT\\}", String.valueOf(usiaTT));
            queryNT = queryNT.replaceAll("\\$P\\{TAHUN_KE\\}", String.valueOf(i));
            queryNT = queryNT.replaceAll("\\$P\\{THN_LAMA_BAYAR\\}", String.valueOf(lamaBayar));
            queryNT = queryNT.replaceAll("\\$P\\{LSDBS_NUMBER\\}", String.valueOf(lsdbs_number));
            queryNT = queryNT.replaceAll("\\$P\\{LSCB_ID\\}", String.valueOf(lscb_id));

            BigDecimal nt =  new BigDecimal(getSelect().selectFormulaResult(queryNT));
            map.put( "nt", ProposalStringFormatter.convertToRoundedNoDigit(nt) );              
          
            if(i == lamaBayar){
                if(lscb_id == 1) { //triwulanan
                    map.put("lt", ProposalStringFormatter.convertToRoundedNoDigit(new BigDecimal(premi * 4 * i)));
                }
                else if (lscb_id == 2) { //semesteran
                    map.put("lt", ProposalStringFormatter.convertToRoundedNoDigit(new BigDecimal(premi * 2 * i)));
                }
                else if (lscb_id == 3) { //tahunan
                    map.put("lt", ProposalStringFormatter.convertToRoundedNoDigit(new BigDecimal(premi * 1 * i)));
                }
                else if (lscb_id == 6) { //bulanan
                    map.put("lt", ProposalStringFormatter.convertToRoundedNoDigit(new BigDecimal(premi * 12 * i)));
                }
            } else {
                map.put( "lt", ProposalStringFormatter.convertToRoundedNoDigit(new BigDecimal(0)));      
            }
          
            mapList.add( map ); 
         
        }

        map = new LinkedHashMap<>();
        map.put( "yearNo", getContext().getString(R.string.Thn_Polis_Ke));
//        map.put( "age", getContext().getString(R.string.P) );
        map.put( "premi", getContext().getString(R.string.Premi_Bulanan));
        map.put( "nt", getContext().getString(R.string.Nilai_Tunai) );
        map.put( "lt", getContext().getString(R.string.Manfaat_Akhir_Polis));
        mapList.add(0, map);

        IllustrationResultVO resultVO = new IllustrationResultVO();
        resultVO.setIllustrationList(mapList);
        
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("Illustration1", resultVO);
        
        return result;
    }

}
