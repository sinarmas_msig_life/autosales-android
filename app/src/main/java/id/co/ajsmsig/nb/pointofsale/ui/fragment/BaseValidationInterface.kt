package id.co.ajsmsig.nb.pointofsale.ui.fragment

import android.support.annotation.StringRes

/**
 * Created by andreyyoshuamanik on 02/03/18.
 */
interface BaseValidationInterface {

    fun canNext(): Boolean = true
    @StringRes fun validationMessageIDRes(): Int? = null
}