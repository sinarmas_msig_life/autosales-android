package id.co.ajsmsig.nb.pointofsale.utils

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Environment
import android.support.v4.app.Fragment
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.NumberFormat
import java.util.*
import android.support.v4.content.FileProvider
import id.co.ajsmsig.nb.BuildConfig
import id.co.ajsmsig.nb.pointofsale.*



/**
 * Created by andreyyoshuamanik on 02/03/18.
 */

fun Double.format(digits: Int) = java.lang.String.format("%.${digits}f", this)

fun Double.moneyFormat(): String {
    val numberFormat = NumberFormat.getNumberInstance(Locale.getDefault())
    numberFormat.maximumFractionDigits = 0
    return numberFormat.format(this).replace(",", ".") + ",-"
}

fun Float.format(digits: Int): String {
    var formattedString = String.format("%.${digits}f", this)

    while (formattedString.endsWith("0") && formattedString.contains(".")) {
        formattedString = formattedString.removeSuffix("0")
    }
    return formattedString.replace(".?0*$", "")
}

fun Bitmap.toByteArray(): ByteArray {

    val stream = ByteArrayOutputStream()
    this.compress(Bitmap.CompressFormat.PNG, 100, stream)
    return stream.toByteArray()
}

fun Fragment.share(file: File?) {

    file ?: return

    val intent = Intent(Intent.ACTION_VIEW)
//    intent.data = Uri.fromFile(file)

    val photoURI = FileProvider.getUriForFile(activity as Context,
            BuildConfig.APPLICATION_ID + ".provider",
            file)
    intent.data = photoURI
    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    val chooserIntent = Intent.createChooser(intent, "Buka Dengan")
    startActivity(intent)

}

fun Context.share(file: File) {

    val intent = Intent(Intent.ACTION_VIEW)
//    intent.data = Uri.fromFile(file)

    val photoURI = FileProvider.getUriForFile(this,
            BuildConfig.APPLICATION_ID + ".provider",
            file)
    intent.data = photoURI
    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    val chooserIntent = Intent.createChooser(intent, "Buka Dengan")
    startActivity(intent)

}

fun isExternalStorageWritable(): Boolean {
    return android.os.Environment.getExternalStorageState() == android.os.Environment.MEDIA_MOUNTED
}

/* Checks if external storage is available for read and write */
fun Fragment.isExternalStorageWritable(): Boolean {
    return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
}

fun String.extension(): String {
    return this.substring(this.lastIndexOf("."))
}

fun String.splitIfTwoWords(): String {
    return if (this.split(" ").size == 2) this.replace(" ", "\n") else this
}

fun String?.fileName(): String? {
    if (this?.indexOf("/") ?: 0 >= 0) {
        return this?.substring(this.lastIndexOf("/") + 1)
    }
    return this
}
