package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.CalculatorResult


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface CalculatorResultDao {
    @Query("SELECT * from CalculatorResult")
    fun loadAllPages(): LiveData<List<CalculatorResult>>


    @Query("SELECT * from CalculatorResult where pageOptionId = :pageOptionId limit 1")
    fun loadPageWithSelectedNeedAnalysis(pageOptionId: Int?): LiveData<CalculatorResult>
}