package id.co.ajsmsig.nb.espaj.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 30/08/2017.
 */

public class TertanggungModel implements Parcelable {

    private String nama_tt ="";
    private String gelar_tt= "";
    private String nama_ibu_tt= "";
    private int bukti_identitas_tt= 0;
    private String no_identitas_tt= "";
    private String tgl_berlaku_tt= "";
    private int warga_negara_tt= 0;
    private String tempat_tt= "";
    private String ttl_tt= "";
    private String usia_tt= "";
    private int jekel_tt= 0;
    private int status_tt= 0;
    private int agama_tt= 0;
    private String agama_lain_tt= "";
    private int pendidikan_tt= 0;
    private String alamat_tt= "";
    private String kota_tt= "";
    private String kdpos_tt= "";
    private String kdtelp1_tt= "";
    private String telp1_tt= "";
    private String kdtelp2_tt= "";
    private String telp2_tt= "";
    private String alamat_kantor_tt= "";
    private String kota_kantor_tt= "";
    private String kdpos_kantor_tt= "";
    private String kdtelp1_kantor_tt= "";
    private String telp1_kantor_tt= "";
    private String kdtelp2_kantor_tt= "";
    private String telp2_kantor_tt= "";
    private String fax_kantor_tt= "";
    private String alamat_tinggal_tt= "";
    private String kota_tinggal_tt= "";
    private String kdpos_tinggal_tt= "";
    private String kdtelp1_tinggal_tt= "";
    private String telp1_tinggal_tt= "";
    private String kdtelp2_tinggal_tt= "";
    private String telp2_tinggal_tt= "";
    private String kdfax_tinggal_tt= "";
    private String fax_tinggal_tt= "";
    private String hp1_tt= "";
    private String hp2_tt= "";
    private String email_tt= "";
    private String penghasilan_thn_tt= "";
    private String klasifikasi_pekerjaan_tt= "";
    private String uraian_pekerjaan_tt= "";
    private String jabatan_klasifikasi_tt= "";
    private int greencard_tt= 0;
    private String alias_tt= "";
    private String nm_perusahaan_tt= "";
    private String npwp_tt= "";
    private String dana_gaji_tt="";
    private String dana_tabungan_tt="";
    private String dana_warisan_tt="";
    private String dana_hibah_tt="";
    private String dana_lainnya_tt="";
    private String edit_d_lainnya_tt = "";
    private String tujuan_proteksi_tt="";
    private String tujuan_inves_tt="";
    private String tujuan_lainnya_tt="";
    private String edit_t_lainnya_tt = "";
    String propinsi_tt = "";
    String propinsi_kantor_tt = "";
    String propinsi_tinggal_tt = "";
    String kabupaten_tt = "";
    String kabupaten_kantor_tt = "";
    String kabupaten_tinggal_tt = "";
    String kecamatan_tt = "";
    String kecamatan_kantor_tt = "";
    String kecamatan_tinggal_tt = "";
    String kelurahan_tt = "";
    String kelurahan_kantor_tt = "";
    String kelurahan_tinggal_tt = "";
    private Boolean validation = false;

    public TertanggungModel(){

    }


    protected TertanggungModel(Parcel in) {
        nama_tt = in.readString();
        gelar_tt = in.readString();
        nama_ibu_tt = in.readString();
        bukti_identitas_tt = in.readInt();
        no_identitas_tt = in.readString();
        tgl_berlaku_tt = in.readString();
        warga_negara_tt = in.readInt();
        tempat_tt = in.readString();
        ttl_tt = in.readString();
        usia_tt = in.readString();
        jekel_tt = in.readInt();
        status_tt = in.readInt();
        agama_tt = in.readInt();
        agama_lain_tt = in.readString();
        pendidikan_tt = in.readInt();
        alamat_tt = in.readString();
        kota_tt = in.readString();
        kdpos_tt = in.readString();
        kdtelp1_tt = in.readString();
        telp1_tt = in.readString();
        kdtelp2_tt = in.readString();
        telp2_tt = in.readString();
        alamat_kantor_tt = in.readString();
        kota_kantor_tt = in.readString();
        kdpos_kantor_tt = in.readString();
        kdtelp1_kantor_tt = in.readString();
        telp1_kantor_tt = in.readString();
        kdtelp2_kantor_tt = in.readString();
        telp2_kantor_tt = in.readString();
        fax_kantor_tt = in.readString();
        alamat_tinggal_tt = in.readString();
        kota_tinggal_tt = in.readString();
        kdpos_tinggal_tt = in.readString();
        kdtelp1_tinggal_tt = in.readString();
        telp1_tinggal_tt = in.readString();
        kdtelp2_tinggal_tt = in.readString();
        telp2_tinggal_tt = in.readString();
        kdfax_tinggal_tt = in.readString();
        fax_tinggal_tt = in.readString();
        hp1_tt = in.readString();
        hp2_tt = in.readString();
        email_tt = in.readString();
        penghasilan_thn_tt = in.readString();
        klasifikasi_pekerjaan_tt = in.readString();
        uraian_pekerjaan_tt = in.readString();
        jabatan_klasifikasi_tt = in.readString();
        greencard_tt = in.readInt();
        alias_tt = in.readString();
        nm_perusahaan_tt = in.readString();
        npwp_tt = in.readString();
        dana_gaji_tt = in.readString();
        dana_tabungan_tt = in.readString();
        dana_warisan_tt = in.readString();
        dana_hibah_tt = in.readString();
        dana_lainnya_tt = in.readString();
        edit_d_lainnya_tt = in.readString();
        tujuan_proteksi_tt = in.readString();
        tujuan_inves_tt = in.readString();
        tujuan_lainnya_tt = in.readString();
        edit_t_lainnya_tt = in.readString();
        propinsi_tt = in.readString();
        propinsi_kantor_tt = in.readString();
        propinsi_tinggal_tt = in.readString();
        kabupaten_tt = in.readString();
        kabupaten_kantor_tt = in.readString();
        kabupaten_tinggal_tt = in.readString();
        kecamatan_tt = in.readString();
        kecamatan_kantor_tt = in.readString();
        kecamatan_tinggal_tt = in.readString();
        kelurahan_tt = in.readString();
        kelurahan_kantor_tt = in.readString();
        kelurahan_tinggal_tt = in.readString();
        byte tmpValidation = in.readByte();
        validation = tmpValidation == 0 ? null : tmpValidation == 1;
    }

    public TertanggungModel(String nama_tt, String gelar_tt, String nama_ibu_tt, int bukti_identitas_tt, String no_identitas_tt, String tgl_berlaku_tt, int warga_negara_tt, String tempat_tt, String ttl_tt, String usia_tt, int jekel_tt, int status_tt, int agama_tt, String agama_lain_tt, int pendidikan_tt, String alamat_tt, String kota_tt, String kdpos_tt, String kdtelp1_tt, String telp1_tt, String kdtelp2_tt, String telp2_tt, String alamat_kantor_tt, String kota_kantor_tt, String kdpos_kantor_tt, String kdtelp1_kantor_tt, String telp1_kantor_tt, String kdtelp2_kantor_tt, String telp2_kantor_tt, String fax_kantor_tt, String alamat_tinggal_tt, String kota_tinggal_tt, String kdpos_tinggal_tt, String kdtelp1_tinggal_tt, String telp1_tinggal_tt, String kdtelp2_tinggal_tt, String telp2_tinggal_tt, String kdfax_tinggal_tt, String fax_tinggal_tt, String hp1_tt, String hp2_tt, String email_tt, String penghasilan_thn_tt, String klasifikasi_pekerjaan_tt, String uraian_pekerjaan_tt, String jabatan_klasifikasi_tt, int greencard_tt, String alias_tt, String nm_perusahaan_tt, String npwp_tt, String dana_gaji_tt, String dana_tabungan_tt, String dana_warisan_tt, String dana_hibah_tt, String dana_lainnya_tt, String edit_d_lainnya_tt, String tujuan_proteksi_tt, String tujuan_inves_tt, String tujuan_lainnya_tt, String edit_t_lainnya_tt, String propinsi_tt, String propinsi_kantor_tt, String propinsi_tinggal_tt, String kabupaten_tt, String kabupaten_kantor_tt, String kabupaten_tinggal_tt, String kecamatan_tt, String kecamatan_kantor_tt, String kecamatan_tinggal_tt, String kelurahan_tt, String kelurahan_kantor_tt, String kelurahan_tinggal_tt, Boolean validation) {
        this.nama_tt = nama_tt;
        this.gelar_tt = gelar_tt;
        this.nama_ibu_tt = nama_ibu_tt;
        this.bukti_identitas_tt = bukti_identitas_tt;
        this.no_identitas_tt = no_identitas_tt;
        this.tgl_berlaku_tt = tgl_berlaku_tt;
        this.warga_negara_tt = warga_negara_tt;
        this.tempat_tt = tempat_tt;
        this.ttl_tt = ttl_tt;
        this.usia_tt = usia_tt;
        this.jekel_tt = jekel_tt;
        this.status_tt = status_tt;
        this.agama_tt = agama_tt;
        this.agama_lain_tt = agama_lain_tt;
        this.pendidikan_tt = pendidikan_tt;
        this.alamat_tt = alamat_tt;
        this.kota_tt = kota_tt;
        this.kdpos_tt = kdpos_tt;
        this.kdtelp1_tt = kdtelp1_tt;
        this.telp1_tt = telp1_tt;
        this.kdtelp2_tt = kdtelp2_tt;
        this.telp2_tt = telp2_tt;
        this.alamat_kantor_tt = alamat_kantor_tt;
        this.kota_kantor_tt = kota_kantor_tt;
        this.kdpos_kantor_tt = kdpos_kantor_tt;
        this.kdtelp1_kantor_tt = kdtelp1_kantor_tt;
        this.telp1_kantor_tt = telp1_kantor_tt;
        this.kdtelp2_kantor_tt = kdtelp2_kantor_tt;
        this.telp2_kantor_tt = telp2_kantor_tt;
        this.fax_kantor_tt = fax_kantor_tt;
        this.alamat_tinggal_tt = alamat_tinggal_tt;
        this.kota_tinggal_tt = kota_tinggal_tt;
        this.kdpos_tinggal_tt = kdpos_tinggal_tt;
        this.kdtelp1_tinggal_tt = kdtelp1_tinggal_tt;
        this.telp1_tinggal_tt = telp1_tinggal_tt;
        this.kdtelp2_tinggal_tt = kdtelp2_tinggal_tt;
        this.telp2_tinggal_tt = telp2_tinggal_tt;
        this.kdfax_tinggal_tt = kdfax_tinggal_tt;
        this.fax_tinggal_tt = fax_tinggal_tt;
        this.hp1_tt = hp1_tt;
        this.hp2_tt = hp2_tt;
        this.email_tt = email_tt;
        this.penghasilan_thn_tt = penghasilan_thn_tt;
        this.klasifikasi_pekerjaan_tt = klasifikasi_pekerjaan_tt;
        this.uraian_pekerjaan_tt = uraian_pekerjaan_tt;
        this.jabatan_klasifikasi_tt = jabatan_klasifikasi_tt;
        this.greencard_tt = greencard_tt;
        this.alias_tt = alias_tt;
        this.nm_perusahaan_tt = nm_perusahaan_tt;
        this.npwp_tt = npwp_tt;
        this.dana_gaji_tt = dana_gaji_tt;
        this.dana_tabungan_tt = dana_tabungan_tt;
        this.dana_warisan_tt = dana_warisan_tt;
        this.dana_hibah_tt = dana_hibah_tt;
        this.dana_lainnya_tt = dana_lainnya_tt;
        this.edit_d_lainnya_tt = edit_d_lainnya_tt;
        this.tujuan_proteksi_tt = tujuan_proteksi_tt;
        this.tujuan_inves_tt = tujuan_inves_tt;
        this.tujuan_lainnya_tt = tujuan_lainnya_tt;
        this.edit_t_lainnya_tt = edit_t_lainnya_tt;
        this.propinsi_tt = propinsi_tt;
        this.propinsi_kantor_tt = propinsi_kantor_tt;
        this.propinsi_tinggal_tt = propinsi_tinggal_tt;
        this.kabupaten_tt = kabupaten_tt;
        this.kabupaten_kantor_tt = kabupaten_kantor_tt;
        this.kabupaten_tinggal_tt = kabupaten_tinggal_tt;
        this.kecamatan_tt = kecamatan_tt;
        this.kecamatan_kantor_tt = kecamatan_kantor_tt;
        this.kecamatan_tinggal_tt = kecamatan_tinggal_tt;
        this.kelurahan_tt = kelurahan_tt;
        this.kelurahan_kantor_tt = kelurahan_kantor_tt;
        this.kelurahan_tinggal_tt = kelurahan_tinggal_tt;
        this.validation = validation;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nama_tt);
        dest.writeString(gelar_tt);
        dest.writeString(nama_ibu_tt);
        dest.writeInt(bukti_identitas_tt);
        dest.writeString(no_identitas_tt);
        dest.writeString(tgl_berlaku_tt);
        dest.writeInt(warga_negara_tt);
        dest.writeString(tempat_tt);
        dest.writeString(ttl_tt);
        dest.writeString(usia_tt);
        dest.writeInt(jekel_tt);
        dest.writeInt(status_tt);
        dest.writeInt(agama_tt);
        dest.writeString(agama_lain_tt);
        dest.writeInt(pendidikan_tt);
        dest.writeString(alamat_tt);
        dest.writeString(kota_tt);
        dest.writeString(kdpos_tt);
        dest.writeString(kdtelp1_tt);
        dest.writeString(telp1_tt);
        dest.writeString(kdtelp2_tt);
        dest.writeString(telp2_tt);
        dest.writeString(alamat_kantor_tt);
        dest.writeString(kota_kantor_tt);
        dest.writeString(kdpos_kantor_tt);
        dest.writeString(kdtelp1_kantor_tt);
        dest.writeString(telp1_kantor_tt);
        dest.writeString(kdtelp2_kantor_tt);
        dest.writeString(telp2_kantor_tt);
        dest.writeString(fax_kantor_tt);
        dest.writeString(alamat_tinggal_tt);
        dest.writeString(kota_tinggal_tt);
        dest.writeString(kdpos_tinggal_tt);
        dest.writeString(kdtelp1_tinggal_tt);
        dest.writeString(telp1_tinggal_tt);
        dest.writeString(kdtelp2_tinggal_tt);
        dest.writeString(telp2_tinggal_tt);
        dest.writeString(kdfax_tinggal_tt);
        dest.writeString(fax_tinggal_tt);
        dest.writeString(hp1_tt);
        dest.writeString(hp2_tt);
        dest.writeString(email_tt);
        dest.writeString(penghasilan_thn_tt);
        dest.writeString(klasifikasi_pekerjaan_tt);
        dest.writeString(uraian_pekerjaan_tt);
        dest.writeString(jabatan_klasifikasi_tt);
        dest.writeInt(greencard_tt);
        dest.writeString(alias_tt);
        dest.writeString(nm_perusahaan_tt);
        dest.writeString(npwp_tt);
        dest.writeString(dana_gaji_tt);
        dest.writeString(dana_tabungan_tt);
        dest.writeString(dana_warisan_tt);
        dest.writeString(dana_hibah_tt);
        dest.writeString(dana_lainnya_tt);
        dest.writeString(edit_d_lainnya_tt);
        dest.writeString(tujuan_proteksi_tt);
        dest.writeString(tujuan_inves_tt);
        dest.writeString(tujuan_lainnya_tt);
        dest.writeString(edit_t_lainnya_tt);
        dest.writeString(propinsi_tt);
        dest.writeString(propinsi_kantor_tt);
        dest.writeString(propinsi_tinggal_tt);
        dest.writeString(kabupaten_tt);
        dest.writeString(kabupaten_kantor_tt);
        dest.writeString(kabupaten_tinggal_tt);
        dest.writeString(kecamatan_tt);
        dest.writeString(kecamatan_kantor_tt);
        dest.writeString(kecamatan_tinggal_tt);
        dest.writeString(kelurahan_tt);
        dest.writeString(kelurahan_kantor_tt);
        dest.writeString(kelurahan_tinggal_tt);
        dest.writeByte((byte) (validation == null ? 0 : validation ? 1 : 2));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TertanggungModel> CREATOR = new Creator<TertanggungModel>() {
        @Override
        public TertanggungModel createFromParcel(Parcel in) {
            return new TertanggungModel(in);
        }

        @Override
        public TertanggungModel[] newArray(int size) {
            return new TertanggungModel[size];
        }
    };

    public String getNama_tt() {
        return nama_tt;
    }

    public void setNama_tt(String nama_tt) {
        this.nama_tt = nama_tt;
    }

    public String getGelar_tt() {
        return gelar_tt;
    }

    public void setGelar_tt(String gelar_tt) {
        this.gelar_tt = gelar_tt;
    }

    public String getNama_ibu_tt() {
        return nama_ibu_tt;
    }

    public void setNama_ibu_tt(String nama_ibu_tt) {
        this.nama_ibu_tt = nama_ibu_tt;
    }

    public int getBukti_identitas_tt() {
        return bukti_identitas_tt;
    }

    public void setBukti_identitas_tt(int bukti_identitas_tt) {
        this.bukti_identitas_tt = bukti_identitas_tt;
    }

    public String getNo_identitas_tt() {
        return no_identitas_tt;
    }

    public void setNo_identitas_tt(String no_identitas_tt) {
        this.no_identitas_tt = no_identitas_tt;
    }

    public String getTgl_berlaku_tt() {
        return tgl_berlaku_tt;
    }

    public void setTgl_berlaku_tt(String tgl_berlaku_tt) {
        this.tgl_berlaku_tt = tgl_berlaku_tt;
    }

    public int getWarga_negara_tt() {
        return warga_negara_tt;
    }

    public void setWarga_negara_tt(int warga_negara_tt) {
        this.warga_negara_tt = warga_negara_tt;
    }

    public String getTempat_tt() {
        return tempat_tt;
    }

    public void setTempat_tt(String tempat_tt) {
        this.tempat_tt = tempat_tt;
    }

    public String getTtl_tt() {
        return ttl_tt;
    }

    public void setTtl_tt(String ttl_tt) {
        this.ttl_tt = ttl_tt;
    }

    public String getUsia_tt() {
        return usia_tt;
    }

    public void setUsia_tt(String usia_tt) {
        this.usia_tt = usia_tt;
    }

    public int getJekel_tt() {
        return jekel_tt;
    }

    public void setJekel_tt(int jekel_tt) {
        this.jekel_tt = jekel_tt;
    }

    public int getStatus_tt() {
        return status_tt;
    }

    public void setStatus_tt(int status_tt) {
        this.status_tt = status_tt;
    }

    public int getAgama_tt() {
        return agama_tt;
    }

    public void setAgama_tt(int agama_tt) {
        this.agama_tt = agama_tt;
    }

    public String getAgama_lain_tt() {
        return agama_lain_tt;
    }

    public void setAgama_lain_tt(String agama_lain_tt) {
        this.agama_lain_tt = agama_lain_tt;
    }

    public int getPendidikan_tt() {
        return pendidikan_tt;
    }

    public void setPendidikan_tt(int pendidikan_tt) {
        this.pendidikan_tt = pendidikan_tt;
    }

    public String getAlamat_tt() {
        return alamat_tt;
    }

    public void setAlamat_tt(String alamat_tt) {
        this.alamat_tt = alamat_tt;
    }

    public String getKota_tt() {
        return kota_tt;
    }

    public void setKota_tt(String kota_tt) {
        this.kota_tt = kota_tt;
    }

    public String getKdpos_tt() {
        return kdpos_tt;
    }

    public void setKdpos_tt(String kdpos_tt) {
        this.kdpos_tt = kdpos_tt;
    }

    public String getKdtelp1_tt() {
        return kdtelp1_tt;
    }

    public void setKdtelp1_tt(String kdtelp1_tt) {
        this.kdtelp1_tt = kdtelp1_tt;
    }

    public String getTelp1_tt() {
        return telp1_tt;
    }

    public void setTelp1_tt(String telp1_tt) {
        this.telp1_tt = telp1_tt;
    }

    public String getKdtelp2_tt() {
        return kdtelp2_tt;
    }

    public void setKdtelp2_tt(String kdtelp2_tt) {
        this.kdtelp2_tt = kdtelp2_tt;
    }

    public String getTelp2_tt() {
        return telp2_tt;
    }

    public void setTelp2_tt(String telp2_tt) {
        this.telp2_tt = telp2_tt;
    }

    public String getAlamat_kantor_tt() {
        return alamat_kantor_tt;
    }

    public void setAlamat_kantor_tt(String alamat_kantor_tt) {
        this.alamat_kantor_tt = alamat_kantor_tt;
    }

    public String getKota_kantor_tt() {
        return kota_kantor_tt;
    }

    public void setKota_kantor_tt(String kota_kantor_tt) {
        this.kota_kantor_tt = kota_kantor_tt;
    }

    public String getKdpos_kantor_tt() {
        return kdpos_kantor_tt;
    }

    public void setKdpos_kantor_tt(String kdpos_kantor_tt) {
        this.kdpos_kantor_tt = kdpos_kantor_tt;
    }

    public String getKdtelp1_kantor_tt() {
        return kdtelp1_kantor_tt;
    }

    public void setKdtelp1_kantor_tt(String kdtelp1_kantor_tt) {
        this.kdtelp1_kantor_tt = kdtelp1_kantor_tt;
    }

    public String getTelp1_kantor_tt() {
        return telp1_kantor_tt;
    }

    public void setTelp1_kantor_tt(String telp1_kantor_tt) {
        this.telp1_kantor_tt = telp1_kantor_tt;
    }

    public String getKdtelp2_kantor_tt() {
        return kdtelp2_kantor_tt;
    }

    public void setKdtelp2_kantor_tt(String kdtelp2_kantor_tt) {
        this.kdtelp2_kantor_tt = kdtelp2_kantor_tt;
    }

    public String getTelp2_kantor_tt() {
        return telp2_kantor_tt;
    }

    public void setTelp2_kantor_tt(String telp2_kantor_tt) {
        this.telp2_kantor_tt = telp2_kantor_tt;
    }

    public String getFax_kantor_tt() {
        return fax_kantor_tt;
    }

    public void setFax_kantor_tt(String fax_kantor_tt) {
        this.fax_kantor_tt = fax_kantor_tt;
    }

    public String getAlamat_tinggal_tt() {
        return alamat_tinggal_tt;
    }

    public void setAlamat_tinggal_tt(String alamat_tinggal_tt) {
        this.alamat_tinggal_tt = alamat_tinggal_tt;
    }

    public String getKota_tinggal_tt() {
        return kota_tinggal_tt;
    }

    public void setKota_tinggal_tt(String kota_tinggal_tt) {
        this.kota_tinggal_tt = kota_tinggal_tt;
    }

    public String getKdpos_tinggal_tt() {
        return kdpos_tinggal_tt;
    }

    public void setKdpos_tinggal_tt(String kdpos_tinggal_tt) {
        this.kdpos_tinggal_tt = kdpos_tinggal_tt;
    }

    public String getKdtelp1_tinggal_tt() {
        return kdtelp1_tinggal_tt;
    }

    public void setKdtelp1_tinggal_tt(String kdtelp1_tinggal_tt) {
        this.kdtelp1_tinggal_tt = kdtelp1_tinggal_tt;
    }

    public String getTelp1_tinggal_tt() {
        return telp1_tinggal_tt;
    }

    public void setTelp1_tinggal_tt(String telp1_tinggal_tt) {
        this.telp1_tinggal_tt = telp1_tinggal_tt;
    }

    public String getKdtelp2_tinggal_tt() {
        return kdtelp2_tinggal_tt;
    }

    public void setKdtelp2_tinggal_tt(String kdtelp2_tinggal_tt) {
        this.kdtelp2_tinggal_tt = kdtelp2_tinggal_tt;
    }

    public String getTelp2_tinggal_tt() {
        return telp2_tinggal_tt;
    }

    public void setTelp2_tinggal_tt(String telp2_tinggal_tt) {
        this.telp2_tinggal_tt = telp2_tinggal_tt;
    }

    public String getKdfax_tinggal_tt() {
        return kdfax_tinggal_tt;
    }

    public void setKdfax_tinggal_tt(String kdfax_tinggal_tt) {
        this.kdfax_tinggal_tt = kdfax_tinggal_tt;
    }

    public String getFax_tinggal_tt() {
        return fax_tinggal_tt;
    }

    public void setFax_tinggal_tt(String fax_tinggal_tt) {
        this.fax_tinggal_tt = fax_tinggal_tt;
    }

    public String getHp1_tt() {
        return hp1_tt;
    }

    public void setHp1_tt(String hp1_tt) {
        this.hp1_tt = hp1_tt;
    }

    public String getHp2_tt() {
        return hp2_tt;
    }

    public void setHp2_tt(String hp2_tt) {
        this.hp2_tt = hp2_tt;
    }

    public String getEmail_tt() {
        return email_tt;
    }

    public void setEmail_tt(String email_tt) {
        this.email_tt = email_tt;
    }

    public String getPenghasilan_thn_tt() {
        return penghasilan_thn_tt;
    }

    public void setPenghasilan_thn_tt(String penghasilan_thn_tt) {
        this.penghasilan_thn_tt = penghasilan_thn_tt;
    }

    public String getKlasifikasi_pekerjaan_tt() {
        return klasifikasi_pekerjaan_tt;
    }

    public void setKlasifikasi_pekerjaan_tt(String klasifikasi_pekerjaan_tt) {
        this.klasifikasi_pekerjaan_tt = klasifikasi_pekerjaan_tt;
    }

    public String getUraian_pekerjaan_tt() {
        return uraian_pekerjaan_tt;
    }

    public void setUraian_pekerjaan_tt(String uraian_pekerjaan_tt) {
        this.uraian_pekerjaan_tt = uraian_pekerjaan_tt;
    }

    public String getJabatan_klasifikasi_tt() {
        return jabatan_klasifikasi_tt;
    }

    public void setJabatan_klasifikasi_tt(String jabatan_klasifikasi_tt) {
        this.jabatan_klasifikasi_tt = jabatan_klasifikasi_tt;
    }

    public int getGreencard_tt() {
        return greencard_tt;
    }

    public void setGreencard_tt(int greencard_tt) {
        this.greencard_tt = greencard_tt;
    }

    public String getAlias_tt() {
        return alias_tt;
    }

    public void setAlias_tt(String alias_tt) {
        this.alias_tt = alias_tt;
    }

    public String getNm_perusahaan_tt() {
        return nm_perusahaan_tt;
    }

    public void setNm_perusahaan_tt(String nm_perusahaan_tt) {
        this.nm_perusahaan_tt = nm_perusahaan_tt;
    }

    public String getNpwp_tt() {
        return npwp_tt;
    }

    public void setNpwp_tt(String npwp_tt) {
        this.npwp_tt = npwp_tt;
    }

    public String getDana_gaji_tt() {
        return dana_gaji_tt;
    }

    public void setDana_gaji_tt(String dana_gaji_tt) {
        this.dana_gaji_tt = dana_gaji_tt;
    }

    public String getDana_tabungan_tt() {
        return dana_tabungan_tt;
    }

    public void setDana_tabungan_tt(String dana_tabungan_tt) {
        this.dana_tabungan_tt = dana_tabungan_tt;
    }

    public String getDana_warisan_tt() {
        return dana_warisan_tt;
    }

    public void setDana_warisan_tt(String dana_warisan_tt) {
        this.dana_warisan_tt = dana_warisan_tt;
    }

    public String getDana_hibah_tt() {
        return dana_hibah_tt;
    }

    public void setDana_hibah_tt(String dana_hibah_tt) {
        this.dana_hibah_tt = dana_hibah_tt;
    }

    public String getDana_lainnya_tt() {
        return dana_lainnya_tt;
    }

    public void setDana_lainnya_tt(String dana_lainnya_tt) {
        this.dana_lainnya_tt = dana_lainnya_tt;
    }

    public String getEdit_d_lainnya_tt() {
        return edit_d_lainnya_tt;
    }

    public void setEdit_d_lainnya_tt(String edit_d_lainnya_tt) {
        this.edit_d_lainnya_tt = edit_d_lainnya_tt;
    }

    public String getTujuan_proteksi_tt() {
        return tujuan_proteksi_tt;
    }

    public void setTujuan_proteksi_tt(String tujuan_proteksi_tt) {
        this.tujuan_proteksi_tt = tujuan_proteksi_tt;
    }

    public String getTujuan_inves_tt() {
        return tujuan_inves_tt;
    }

    public void setTujuan_inves_tt(String tujuan_inves_tt) {
        this.tujuan_inves_tt = tujuan_inves_tt;
    }

    public String getTujuan_lainnya_tt() {
        return tujuan_lainnya_tt;
    }

    public void setTujuan_lainnya_tt(String tujuan_lainnya_tt) {
        this.tujuan_lainnya_tt = tujuan_lainnya_tt;
    }

    public String getEdit_t_lainnya_tt() {
        return edit_t_lainnya_tt;
    }

    public void setEdit_t_lainnya_tt(String edit_t_lainnya_tt) {
        this.edit_t_lainnya_tt = edit_t_lainnya_tt;
    }

    public String getPropinsi_tt() {
        return propinsi_tt;
    }

    public void setPropinsi_tt(String propinsi_tt) {
        this.propinsi_tt = propinsi_tt;
    }

    public String getPropinsi_kantor_tt() {
        return propinsi_kantor_tt;
    }

    public void setPropinsi_kantor_tt(String propinsi_kantor_tt) {
        this.propinsi_kantor_tt = propinsi_kantor_tt;
    }

    public String getPropinsi_tinggal_tt() {
        return propinsi_tinggal_tt;
    }

    public void setPropinsi_tinggal_tt(String propinsi_tinggal_tt) {
        this.propinsi_tinggal_tt = propinsi_tinggal_tt;
    }

    public String getKabupaten_tt() {
        return kabupaten_tt;
    }

    public void setKabupaten_tt(String kabupaten_tt) {
        this.kabupaten_tt = kabupaten_tt;
    }

    public String getKabupaten_kantor_tt() {
        return kabupaten_kantor_tt;
    }

    public void setKabupaten_kantor_tt(String kabupaten_kantor_tt) {
        this.kabupaten_kantor_tt = kabupaten_kantor_tt;
    }

    public String getKabupaten_tinggal_tt() {
        return kabupaten_tinggal_tt;
    }

    public void setKabupaten_tinggal_tt(String kabupaten_tinggal_tt) {
        this.kabupaten_tinggal_tt = kabupaten_tinggal_tt;
    }

    public String getKecamatan_tt() {
        return kecamatan_tt;
    }

    public void setKecamatan_tt(String kecamatan_tt) {
        this.kecamatan_tt = kecamatan_tt;
    }

    public String getKecamatan_kantor_tt() {
        return kecamatan_kantor_tt;
    }

    public void setKecamatan_kantor_tt(String kecamatan_kantor_tt) {
        this.kecamatan_kantor_tt = kecamatan_kantor_tt;
    }

    public String getKecamatan_tinggal_tt() {
        return kecamatan_tinggal_tt;
    }

    public void setKecamatan_tinggal_tt(String kecamatan_tinggal_tt) {
        this.kecamatan_tinggal_tt = kecamatan_tinggal_tt;
    }

    public String getKelurahan_tt() {
        return kelurahan_tt;
    }

    public void setKelurahan_tt(String kelurahan_tt) {
        this.kelurahan_tt = kelurahan_tt;
    }

    public String getKelurahan_kantor_tt() {
        return kelurahan_kantor_tt;
    }

    public void setKelurahan_kantor_tt(String kelurahan_kantor_tt) {
        this.kelurahan_kantor_tt = kelurahan_kantor_tt;
    }

    public String getKelurahan_tinggal_tt() {
        return kelurahan_tinggal_tt;
    }

    public void setKelurahan_tinggal_tt(String kelurahan_tinggal_tt) {
        this.kelurahan_tinggal_tt = kelurahan_tinggal_tt;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public static Creator<TertanggungModel> getCREATOR() {
        return CREATOR;
    }
}
