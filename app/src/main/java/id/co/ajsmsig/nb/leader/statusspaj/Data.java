
package id.co.ajsmsig.nb.leader.statusspaj;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {
    public Data(List<SpajData> inforce, List<SpajData> cancelled, List<SpajData> allSubmission, List<SpajData> furtherRequirement, List<SpajData> onProcess) {
        this.inforce = inforce;
        this.cancelled = cancelled;
        this.allSubmission = allSubmission;
        this.furtherRequirement = furtherRequirement;
        this.onProcess = onProcess;
    }

    @SerializedName("inforce")
    @Expose
    private List<SpajData> inforce;

    @SerializedName("cancelled")
    @Expose
    private List<SpajData> cancelled;

    @SerializedName("all_submission")
    @Expose
    private List<SpajData> allSubmission;

    @SerializedName("further_requirement")
    @Expose
    private List<SpajData> furtherRequirement;

    @SerializedName("onprocess")
    @Expose
    private List<SpajData> onProcess;

    public List<SpajData> getAllSubmission() {
        return allSubmission;
    }

    public void setAllSubmission(List<SpajData> allSubmission) {
        this.allSubmission = allSubmission;
    }

    public List<SpajData> getFurtherRequirement() {
        return furtherRequirement;
    }

    public void setFurtherRequirement(List<SpajData> furtherRequirement) {
        this.furtherRequirement = furtherRequirement;
    }

    public List<SpajData> getOnProcess() {
        return onProcess;
    }

    public void setOnProcess(List<SpajData> onProcess) {
        this.onProcess = onProcess;
    }

    public List<SpajData> getInforce() {
        return inforce;
    }

    public void setInforce(List<SpajData> inforce) {
        this.inforce = inforce;
    }

    public List<SpajData> getCancelled() {
        return cancelled;
    }

    public void setCancelled(List<SpajData> cancelled) {
        this.cancelled = cancelled;
    }



}
