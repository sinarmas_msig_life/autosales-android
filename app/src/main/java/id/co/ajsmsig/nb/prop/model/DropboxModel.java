package id.co.ajsmsig.nb.prop.model;

/**
 * Created by faiz_f on 03/04/2017.
 */

public class DropboxModel {
    private String label;
    private Integer id;
    private Integer idSecond;
    private String idString;
    private String labelSecond;
    private String detail;
    private Integer flagPacket; // baru ditambahkan siap2u

    public DropboxModel() {
    }

    public DropboxModel(String label, Integer id, Integer idSecond) {
        this.label = label;
        this.id = id;
        this.idSecond = idSecond;
    }

    public DropboxModel(String label, Integer id) {
        this.label = label;
        this.id = id;
    }

    public DropboxModel(String label, String idString) {
        this.label = label;
        this.idString = idString;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdSecond() {
        return idSecond;
    }

    public void setIdSecond(Integer idSecond) {
        this.idSecond = idSecond;
    }

    public String getIdString() {
        return idString;
    }

    public void setIdString(String idString) {
        this.idString = idString;
    }

    public String getLabelSecond() {
        return labelSecond;
    }

    public void setLabelSecond(String labelSecond) {
        this.labelSecond = labelSecond;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Integer getFlagPacket() {
        return flagPacket;
    }

    public void setFlagPacket(Integer flagPacket) {
        this.flagPacket = flagPacket;
    }

    @Override
    public String toString() {
        return label;
    }



}
