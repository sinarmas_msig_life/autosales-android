package id.co.ajsmsig.nb.pointofsale.ui.handlingobjection


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.pointofsale.adapter.HandlingObjectionListAdapter
import id.co.ajsmsig.nb.databinding.PosFragmentHandlingObjectionListBinding
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.HandlingObjection
import id.co.ajsmsig.nb.pointofsale.ui.fragment.BaseFragment


/**
 * A simple [Fragment] subclass.
 */
class HandlingObjectionListFragment : BaseFragment() {


    private lateinit var dataBinding: PosFragmentHandlingObjectionListBinding
    private lateinit var optionAdapter: HandlingObjectionListAdapter


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(HandlingObjectionListVM::class.java)
        subscribeUi(viewModel)
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_handling_objection_list, container, false)

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupList(dataBinding.recyclerView)
    }

    fun setupList(recyclerView: RecyclerView) {
        optionAdapter = HandlingObjectionListAdapter(onClickHandlingObjection)
        recyclerView.adapter = optionAdapter
        recyclerView.layoutManager = GridLayoutManager(activity, 2)
        recyclerView.setHasFixedSize(true)
    }

    fun subscribeUi(viewModel: HandlingObjectionListVM) {
        dataBinding.vm = viewModel
        viewModel.page = page

        viewModel.observableHandlingObjectionCategories.observe(this, Observer {
            it?.let { optionAdapter.options = it.toTypedArray() }
        })

    }

    val onClickHandlingObjection: (HandlingObjection) -> Unit =  {
        navigationController.goToHandlingObjectionDetail()
        sharedViewModel.selectedHandlingObjection = it
    }

}// Required empty public constructor
