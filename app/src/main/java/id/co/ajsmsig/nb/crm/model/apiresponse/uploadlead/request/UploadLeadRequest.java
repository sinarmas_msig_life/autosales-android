package id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.request;

import java.util.List;

public class UploadLeadRequest {
    private List<ListActivity> LIST_ACTIVITY;
    private int SL_ACTIVE;
    private int SL_APP;
    private String SL_CREATED;
    private String SL_CRTD_DATE;
    private int SL_FAV;
    private int SL_GENDER;
    private int SL_LAST_POS;
    private String SL_NAME;
    private int SL_TAB_ID;
    private int SL_TYPE;
    private int SL_UMUR;
    private String SL_UPDATED;
    private String SL_UPTD_DATE;
    private id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.request.Home home;

    public UploadLeadRequest(List<ListActivity> LIST_ACTIVITY, int SL_ACTIVE, int SL_APP, String SL_CREATED, String SL_CRTD_DATE, int SL_FAV, int SL_GENDER, int SL_LAST_POS, String SL_NAME, int SL_TAB_ID, int SL_TYPE, int SL_UMUR, String SL_UPDATED, String SL_UPTD_DATE, id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.request.Home home) {
        this.LIST_ACTIVITY = LIST_ACTIVITY;
        this.SL_ACTIVE = SL_ACTIVE;
        this.SL_APP = SL_APP;
        this.SL_CREATED = SL_CREATED;
        this.SL_CRTD_DATE = SL_CRTD_DATE;
        this.SL_FAV = SL_FAV;
        this.SL_GENDER = SL_GENDER;
        this.SL_LAST_POS = SL_LAST_POS;
        this.SL_NAME = SL_NAME;
        this.SL_TAB_ID = SL_TAB_ID;
        this.SL_TYPE = SL_TYPE;
        this.SL_UMUR = SL_UMUR;
        this.SL_UPDATED = SL_UPDATED;
        this.SL_UPTD_DATE = SL_UPTD_DATE;
        this.home = home;
    }

    public void setLIST_ACTIVITY(List<ListActivity> LIST_ACTIVITY) {
        this.LIST_ACTIVITY = LIST_ACTIVITY;
    }

    public void setSL_ACTIVE(int SL_ACTIVE) {
        this.SL_ACTIVE = SL_ACTIVE;
    }

    public void setSL_APP(int SL_APP) {
        this.SL_APP = SL_APP;
    }

    public void setSL_CREATED(String SL_CREATED) {
        this.SL_CREATED = SL_CREATED;
    }

    public void setSL_CRTD_DATE(String SL_CRTD_DATE) {
        this.SL_CRTD_DATE = SL_CRTD_DATE;
    }

    public void setSL_FAV(int SL_FAV) {
        this.SL_FAV = SL_FAV;
    }

    public void setSL_GENDER(int SL_GENDER) {
        this.SL_GENDER = SL_GENDER;
    }

    public void setSL_LAST_POS(int SL_LAST_POS) {
        this.SL_LAST_POS = SL_LAST_POS;
    }

    public void setSL_NAME(String SL_NAME) {
        this.SL_NAME = SL_NAME;
    }

    public void setSL_TAB_ID(int SL_TAB_ID) {
        this.SL_TAB_ID = SL_TAB_ID;
    }

    public void setSL_TYPE(int SL_TYPE) {
        this.SL_TYPE = SL_TYPE;
    }

    public void setSL_UMUR(int SL_UMUR) {
        this.SL_UMUR = SL_UMUR;
    }

    public void setSL_UPDATED(String SL_UPDATED) {
        this.SL_UPDATED = SL_UPDATED;
    }

    public void setSL_UPTD_DATE(String SL_UPTD_DATE) {
        this.SL_UPTD_DATE = SL_UPTD_DATE;
    }

    public void setHome(id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.request.Home home) {
        this.home = home;
    }

}
