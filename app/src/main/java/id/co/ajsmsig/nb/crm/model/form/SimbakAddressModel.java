package id.co.ajsmsig.nb.crm.model.form;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by faiz_f on 11/04/2017.
 */

public class SimbakAddressModel implements Parcelable {
    private Long SLA2_TAB_ID;
    private Long SLA2_ID;
    private Long SLA2_INST_ID;
    private Long SLA2_INST_TAB_ID;
    private Integer SLA2_INST_TYPE;
    private Integer SLA2_ADDR_TYPE;
    private String SLA2_DETAIL;
    private String SLA2_POSTCODE;
    private String SLA2_CITY;
    private String SLA2_STATE;
    private String SLA2_HP1;
    private String SLA2_HP2;
    private String SLA2_PHONECODE;
    private String SLA2_PHONE1;
    private String SLA2_PHONE2;
    private String SLA2_EMAIL;
    private String SLA2_FAXCODE;
    private String SLA2_FAX;
    private String SLA2_LAT;
    private String SLA2_LON;
    private String SLA2_OLD_ID;
    private Integer SLA2_LSPR_ID;
    private Integer SLA2_LSKA_ID;
    private Integer SLA2_LSKC_ID;
    private Integer SLA2_LSKL_ID;

    public SimbakAddressModel() {
    }

    private SimbakAddressModel(Parcel in) {
        SLA2_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLA2_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLA2_INST_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLA2_INST_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLA2_INST_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA2_ADDR_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA2_DETAIL = in.readString();
        SLA2_POSTCODE = in.readString();
        SLA2_CITY = in.readString();
        SLA2_STATE = in.readString();
        SLA2_HP1 = in.readString();
        SLA2_HP2 = in.readString();
        SLA2_PHONECODE = in.readString();
        SLA2_PHONE1 = in.readString();
        SLA2_PHONE2 = in.readString();
        SLA2_EMAIL = in.readString();
        SLA2_FAXCODE = in.readString();
        SLA2_FAX = in.readString();
        SLA2_LAT = in.readString();
        SLA2_LON = in.readString();
        SLA2_OLD_ID = in.readString();
        SLA2_LSPR_ID = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA2_LSKA_ID = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA2_LSKC_ID = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA2_LSKL_ID = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<SimbakAddressModel> CREATOR = new Creator<SimbakAddressModel>() {
        @Override
        public SimbakAddressModel createFromParcel(Parcel in) {
            return new SimbakAddressModel(in);
        }

        @Override
        public SimbakAddressModel[] newArray(int size) {
            return new SimbakAddressModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(SLA2_TAB_ID);
        dest.writeValue(SLA2_ID);
        dest.writeValue(SLA2_INST_ID);
        dest.writeValue(SLA2_INST_TAB_ID);
        dest.writeValue(SLA2_INST_TYPE);
        dest.writeValue(SLA2_ADDR_TYPE);
        dest.writeString(SLA2_DETAIL);
        dest.writeString(SLA2_POSTCODE);
        dest.writeString(SLA2_CITY);
        dest.writeString(SLA2_STATE);
        dest.writeString(SLA2_HP1);
        dest.writeString(SLA2_HP2);
        dest.writeString(SLA2_PHONECODE);
        dest.writeString(SLA2_PHONE1);
        dest.writeString(SLA2_PHONE2);
        dest.writeString(SLA2_EMAIL);
        dest.writeString(SLA2_FAXCODE);
        dest.writeString(SLA2_FAX);
        dest.writeString(SLA2_LAT);
        dest.writeString(SLA2_LON);
        dest.writeString(SLA2_OLD_ID);
        dest.writeValue(SLA2_LSPR_ID);
        dest.writeValue(SLA2_LSKA_ID);
        dest.writeValue(SLA2_LSKC_ID);
        dest.writeValue(SLA2_LSKL_ID);
    }

    public Long getSLA2_TAB_ID() {
        return SLA2_TAB_ID;
    }

    public void setSLA2_TAB_ID(Long SLA2_TAB_ID) {
        this.SLA2_TAB_ID = SLA2_TAB_ID;
    }

    public Long getSLA2_ID() {
        return SLA2_ID;
    }

    public void setSLA2_ID(Long SLA2_ID) {
        this.SLA2_ID = SLA2_ID;
    }

    public Long getSLA2_INST_ID() {
        return SLA2_INST_ID;
    }

    public void setSLA2_INST_ID(Long SLA2_INST_ID) {
        this.SLA2_INST_ID = SLA2_INST_ID;
    }

    public Long getSLA2_INST_TAB_ID() {
        return SLA2_INST_TAB_ID;
    }

    public void setSLA2_INST_TAB_ID(Long SLA2_INST_TAB_ID) {
        this.SLA2_INST_TAB_ID = SLA2_INST_TAB_ID;
    }

    public Integer getSLA2_INST_TYPE() {
        return SLA2_INST_TYPE;
    }

    public void setSLA2_INST_TYPE(Integer SLA2_INST_TYPE) {
        this.SLA2_INST_TYPE = SLA2_INST_TYPE;
    }

    public Integer getSLA2_ADDR_TYPE() {
        return SLA2_ADDR_TYPE;
    }

    public void setSLA2_ADDR_TYPE(Integer SLA2_ADDR_TYPE) {
        this.SLA2_ADDR_TYPE = SLA2_ADDR_TYPE;
    }

    public String getSLA2_DETAIL() {
        return SLA2_DETAIL;
    }

    public void setSLA2_DETAIL(String SLA2_DETAIL) {
        this.SLA2_DETAIL = SLA2_DETAIL;
    }

    public String getSLA2_POSTCODE() {
        return SLA2_POSTCODE;
    }

    public void setSLA2_POSTCODE(String SLA2_POSTCODE) {
        this.SLA2_POSTCODE = SLA2_POSTCODE;
    }

    public String getSLA2_CITY() {
        return SLA2_CITY;
    }

    public void setSLA2_CITY(String SLA2_CITY) {
        this.SLA2_CITY = SLA2_CITY;
    }

    public String getSLA2_STATE() {
        return SLA2_STATE;
    }

    public void setSLA2_STATE(String SLA2_STATE) {
        this.SLA2_STATE = SLA2_STATE;
    }

    public String getSLA2_HP1() {
        return SLA2_HP1;
    }

    public void setSLA2_HP1(String SLA2_HP1) {
        this.SLA2_HP1 = SLA2_HP1;
    }

    public String getSLA2_HP2() {
        return SLA2_HP2;
    }

    public void setSLA2_HP2(String SLA2_HP2) {
        this.SLA2_HP2 = SLA2_HP2;
    }

    public String getSLA2_PHONECODE() {
        return SLA2_PHONECODE;
    }

    public void setSLA2_PHONECODE(String SLA2_PHONECODE) {
        this.SLA2_PHONECODE = SLA2_PHONECODE;
    }

    public String getSLA2_PHONE1() {
        return SLA2_PHONE1;
    }

    public void setSLA2_PHONE1(String SLA2_PHONE1) {
        this.SLA2_PHONE1 = SLA2_PHONE1;
    }

    public String getSLA2_PHONE2() {
        return SLA2_PHONE2;
    }

    public void setSLA2_PHONE2(String SLA2_PHONE2) {
        this.SLA2_PHONE2 = SLA2_PHONE2;
    }

    public String getSLA2_EMAIL() {
        return SLA2_EMAIL;
    }

    public void setSLA2_EMAIL(String SLA2_EMAIL) {
        this.SLA2_EMAIL = SLA2_EMAIL;
    }

    public String getSLA2_FAXCODE() {
        return SLA2_FAXCODE;
    }

    public void setSLA2_FAXCODE(String SLA2_FAXCODE) {
        this.SLA2_FAXCODE = SLA2_FAXCODE;
    }

    public String getSLA2_FAX() {
        return SLA2_FAX;
    }

    public void setSLA2_FAX(String SLA2_FAX) {
        this.SLA2_FAX = SLA2_FAX;
    }

    public String getSLA2_LAT() {
        return SLA2_LAT;
    }

    public void setSLA2_LAT(String SLA2_LAT) {
        this.SLA2_LAT = SLA2_LAT;
    }

    public String getSLA2_LON() {
        return SLA2_LON;
    }

    public void setSLA2_LON(String SLA2_LON) {
        this.SLA2_LON = SLA2_LON;
    }

    public String getSLA2_OLD_ID() {
        return SLA2_OLD_ID;
    }

    public void setSLA2_OLD_ID(String SLA2_OLD_ID) {
        this.SLA2_OLD_ID = SLA2_OLD_ID;
    }

    public Integer getSLA2_LSPR_ID() {
        return SLA2_LSPR_ID;
    }

    public void setSLA2_LSPR_ID(Integer SLA2_LSPR_ID) {
        this.SLA2_LSPR_ID = SLA2_LSPR_ID;
    }

    public Integer getSLA2_LSKA_ID() {
        return SLA2_LSKA_ID;
    }

    public void setSLA2_LSKA_ID(Integer SLA2_LSKA_ID) {
        this.SLA2_LSKA_ID = SLA2_LSKA_ID;
    }

    public Integer getSLA2_LSKC_ID() {
        return SLA2_LSKC_ID;
    }

    public void setSLA2_LSKC_ID(Integer SLA2_LSKC_ID) {
        this.SLA2_LSKC_ID = SLA2_LSKC_ID;
    }

    public Integer getSLA2_LSKL_ID() {
        return SLA2_LSKL_ID;
    }

    public void setSLA2_LSKL_ID(Integer SLA2_LSKL_ID) {
        this.SLA2_LSKL_ID = SLA2_LSKL_ID;
    }
}

