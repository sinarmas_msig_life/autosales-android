package id.co.ajsmsig.nb.espaj.model.arraylist;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 30/08/2017.
 */

public class ModelAskesUA implements Parcelable {

    private int counter = 0;
    private String peserta_askes = "";
    private int jekel_askes = 0;
    private String ttl_askes = "";
    private int usia_askes = 0;
    private int hubungan_askes = 0;
    private int produk_askes = 0;
    private int rider_askes = 0;
    private int kode_produk_rider = 0;
    private int kode_subproduk_rider = 0;
    private int tinggi_askes = 0;
    private int berat_askes = 0;
    private int warganegara_askes = 0;
    private String pekerjaan_askes = "";
    private Boolean validation = true;

    public ModelAskesUA() {
    }

    protected ModelAskesUA(Parcel in) {
        counter = in.readInt();
        peserta_askes = in.readString();
        jekel_askes = in.readInt();
        ttl_askes = in.readString();
        usia_askes = in.readInt();
        hubungan_askes = in.readInt();
        produk_askes = in.readInt();
        rider_askes = in.readInt();
        kode_produk_rider = in.readInt();
        kode_subproduk_rider = in.readInt();
        tinggi_askes = in.readInt();
        berat_askes = in.readInt();
        warganegara_askes = in.readInt();
        pekerjaan_askes = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(counter);
        dest.writeString(peserta_askes);
        dest.writeInt(jekel_askes);
        dest.writeString(ttl_askes);
        dest.writeInt(usia_askes);
        dest.writeInt(hubungan_askes);
        dest.writeInt(produk_askes);
        dest.writeInt(rider_askes);
        dest.writeInt(kode_produk_rider);
        dest.writeInt(kode_subproduk_rider);
        dest.writeInt(tinggi_askes);
        dest.writeInt(berat_askes);
        dest.writeInt(warganegara_askes);
        dest.writeString(pekerjaan_askes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelAskesUA> CREATOR = new Creator<ModelAskesUA>() {
        @Override
        public ModelAskesUA createFromParcel(Parcel in) {
            return new ModelAskesUA(in);
        }

        @Override
        public ModelAskesUA[] newArray(int size) {
            return new ModelAskesUA[size];
        }
    };

    public ModelAskesUA(int counter, String peserta_askes, int jekel_askes, String ttl_askes, int usia_askes, int hubungan_askes, int produk_askes, int rider_askes, int kode_produk_rider, int kode_subproduk_rider, int tinggi_askes, int berat_askes, int warganegara_askes, String pekerjaan_askes, Boolean validation) {
        this.counter = counter;
        this.peserta_askes = peserta_askes;
        this.jekel_askes = jekel_askes;
        this.ttl_askes = ttl_askes;
        this.usia_askes = usia_askes;
        this.hubungan_askes = hubungan_askes;
        this.produk_askes = produk_askes;
        this.rider_askes = rider_askes;
        this.kode_produk_rider = kode_produk_rider;
        this.kode_subproduk_rider = kode_subproduk_rider;
        this.tinggi_askes = tinggi_askes;
        this.berat_askes = berat_askes;
        this.warganegara_askes = warganegara_askes;
        this.pekerjaan_askes = pekerjaan_askes;
        this.validation = validation;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getPeserta_askes() {
        return peserta_askes;
    }

    public void setPeserta_askes(String peserta_askes) {
        this.peserta_askes = peserta_askes;
    }

    public int getJekel_askes() {
        return jekel_askes;
    }

    public void setJekel_askes(int jekel_askes) {
        this.jekel_askes = jekel_askes;
    }

    public String getTtl_askes() {
        return ttl_askes;
    }

    public void setTtl_askes(String ttl_askes) {
        this.ttl_askes = ttl_askes;
    }

    public int getUsia_askes() {
        return usia_askes;
    }

    public void setUsia_askes(int usia_askes) {
        this.usia_askes = usia_askes;
    }

    public int getHubungan_askes() {
        return hubungan_askes;
    }

    public void setHubungan_askes(int hubungan_askes) {
        this.hubungan_askes = hubungan_askes;
    }

    public int getProduk_askes() {
        return produk_askes;
    }

    public void setProduk_askes(int produk_askes) {
        this.produk_askes = produk_askes;
    }

    public int getRider_askes() {
        return rider_askes;
    }

    public void setRider_askes(int rider_askes) {
        this.rider_askes = rider_askes;
    }

    public int getKode_produk_rider() {
        return kode_produk_rider;
    }

    public void setKode_produk_rider(int kode_produk_rider) {
        this.kode_produk_rider = kode_produk_rider;
    }

    public int getKode_subproduk_rider() {
        return kode_subproduk_rider;
    }

    public void setKode_subproduk_rider(int kode_subproduk_rider) {
        this.kode_subproduk_rider = kode_subproduk_rider;
    }

    public int getTinggi_askes() {
        return tinggi_askes;
    }

    public void setTinggi_askes(int tinggi_askes) {
        this.tinggi_askes = tinggi_askes;
    }

    public int getBerat_askes() {
        return berat_askes;
    }

    public void setBerat_askes(int berat_askes) {
        this.berat_askes = berat_askes;
    }

    public int getWarganegara_askes() {
        return warganegara_askes;
    }

    public void setWarganegara_askes(int warganegara_askes) {
        this.warganegara_askes = warganegara_askes;
    }

    public String getPekerjaan_askes() {
        return pekerjaan_askes;
    }

    public void setPekerjaan_askes(String pekerjaan_askes) {
        this.pekerjaan_askes = pekerjaan_askes;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public static Creator<ModelAskesUA> getCREATOR() {
        return CREATOR;
    }
}
