
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostCasePegaResponse {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("pxObjClass")
    @Expose
    private String pxObjClass;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getPxObjClass() {
        return pxObjClass;
    }

    public void setPxObjClass(String pxObjClass) {
        this.pxObjClass = pxObjClass;
    }

}
