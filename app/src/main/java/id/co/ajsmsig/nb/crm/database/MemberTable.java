package id.co.ajsmsig.nb.crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import id.co.ajsmsig.nb.crm.model.MemberModel;

/**
 * Created by eriza on 05/01/2018.
 */

public class MemberTable {
    private Context context;

    public MemberTable(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(MemberModel me) {
        ContentValues cv = new ContentValues();
        cv.put("NOID", me.getNOID());
        cv.put("FULLNAME", me.getFULLNAME());
        cv.put("BTEXT", me.getBTEXT());
        cv.put("PREMI", me.getPACKET());
        cv.put("SOCIALID", me.getSOCIALID());
        cv.put("MPHONE", me.getMPHONE());
        cv.put("EMAIL", me.getEMAIL());
        cv.put("SPONSOR", me.getSPONSOR());
        cv.put("UPLINE", me.getUPLINE());
        cv.put("SEXID", me.getSEXID());
        cv.put("NO_PROPOSAL_TAB", me.getNO_PROPOSAL_TAB());
        cv.put("NO_PROPOSAL", me.getNO_PROPOSAL());
        cv.put("SPAJ_ID_TAB", me.getSPAJ_ID_TAB());
        cv.put("SPAJ_ID", me.getSPAJ_ID());
        cv.put("VIRTUAL_ACC", me.getVIRTUAL_ACC());
        return cv;
    }

    public MemberModel getObject(Cursor cursor) {
        MemberModel me = new MemberModel();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NOID"))) {
                me.setNOID(cursor.getString(cursor.getColumnIndexOrThrow("NOID")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("FULLNAME"))) {
                me.setFULLNAME(cursor.getString(cursor.getColumnIndexOrThrow("FULLNAME")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("BTEXT"))) {
                me.setBTEXT(cursor.getString(cursor.getColumnIndexOrThrow("BTEXT")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PREMI"))) {
                me.setPACKET(cursor.getString(cursor.getColumnIndexOrThrow("PREMI")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SOCIALID"))) {
                me.setSOCIALID(cursor.getString(cursor.getColumnIndexOrThrow("SOCIALID")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("MPHONE"))) {
                me.setMPHONE(cursor.getString(cursor.getColumnIndexOrThrow("MPHONE")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("EMAIL"))) {
                me.setEMAIL(cursor.getString(cursor.getColumnIndexOrThrow("EMAIL")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SPONSOR"))) {
                me.setSPONSOR(cursor.getString(cursor.getColumnIndexOrThrow("SPONSOR")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("UPLINE"))) {
                me.setUPLINE(cursor.getString(cursor.getColumnIndexOrThrow("UPLINE")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SEXID"))) {
                me.setSEXID(cursor.getString(cursor.getColumnIndexOrThrow("SEXID")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NO_PROPOSAL_TAB"))) {
                me.setNO_PROPOSAL_TAB(cursor.getString(cursor.getColumnIndexOrThrow("NO_PROPOSAL_TAB")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NO_PROPOSAL"))) {
                me.setNO_PROPOSAL(cursor.getString(cursor.getColumnIndexOrThrow("NO_PROPOSAL")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SPAJ_ID_TAB"))) {
                me.setSPAJ_ID_TAB(cursor.getString(cursor.getColumnIndexOrThrow("SPAJ_ID_TAB")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SPAJ_ID"))) {
                me.setSPAJ_ID(cursor.getString(cursor.getColumnIndexOrThrow("SPAJ_ID")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("VIRTUAL_ACC"))) {
                me.setVIRTUAL_ACC(cursor.getString(cursor.getColumnIndexOrThrow("VIRTUAL_ACC")));
            }
            cursor.close();
        }
        return me;
    }
}
