package id.co.ajsmsig.nb.leader.summarybc.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import id.co.ajsmsig.nb.leader.summarybc.datasource.factory.SummaryDataSourceFactory;
import id.co.ajsmsig.nb.leader.summarybc.model.SummaryData;
import id.co.ajsmsig.nb.leader.summarybc.utils.NetworkState;

public class SummaryViewModel extends ViewModel {
    private String agentCode;
    private String channelBank;
    private Executor executor;
    private LiveData<NetworkState> networkState;
    private LiveData<PagedList<SummaryData>> summaryLiveData;

    public SummaryViewModel(String agentCode, String channelBank) {
        this.agentCode = agentCode;
        this.channelBank = channelBank;
        init();
    }

    private void init() {
        executor = Executors.newFixedThreadPool(5);
        SummaryDataSourceFactory factory = new SummaryDataSourceFactory(agentCode, channelBank);
        networkState = Transformations.switchMap(factory.getMutableLiveData(),
                dataSource -> dataSource.getNetworkState());

        PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false).setInitialLoadSizeHint(10).setPageSize(10).build();
        summaryLiveData = (new LivePagedListBuilder(factory, pagedListConfig)).setFetchExecutor(executor).build();
    }

    public LiveData<NetworkState> getNetworkState() {
        return networkState;
    }

    public LiveData<PagedList<SummaryData>> getSummaryLiveData() {
        return summaryLiveData;
    }

}
