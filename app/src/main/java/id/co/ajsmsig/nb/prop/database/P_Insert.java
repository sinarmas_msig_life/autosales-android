package id.co.ajsmsig.nb.prop.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.DBHelper;
import id.co.ajsmsig.nb.prop.model.apimodel.response.Virtual;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;

public class P_Insert {
    private final String TAG = P_Insert.class.getSimpleName();
    private DBHelper dbHelper;
    private Context context;

    //Table E_LST_VA
    private final String TABLE_NAME_E_LST_VA = "E_LST_VA";
    private final String TABLE_NAME_MST_DATA_PROPOSAL = "MST_DATA_PROPOSAL";
    private final String COLUMN_NO_VA = "NO_VA";
    private final String COLUMN_LSBS_ID = "LSBS_ID";
    private final String COLUMN_LSBP_ID= "LSBP_ID";
    private final String COLUMN_MSAG_ID = "MSAG_ID";
    private final String COLUMN_FLAG_AKTIF = "FLAG_AKTIF";

    public P_Insert(Context context) {
        this.context = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.update_data_preferences), Context.MODE_PRIVATE);
        int version = sharedPreferences.getInt(context.getString(R.string.lav_data_id), 0);
        this.dbHelper = new DBHelper(context, version);
    }

    public void insertVirtualAccountList(int lsbsId, int lsbpId, int msagId, int activeFlag, List<Virtual> virtualAccountList) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            for (Virtual model : virtualAccountList) {
                Log.v(TAG, "Inserting VA number " + model.getNOVA() + " to database");
                String vaNumber = model.getNOVA();
                contentValues.put(COLUMN_NO_VA, vaNumber);
                contentValues.put(COLUMN_LSBS_ID, lsbsId);
                contentValues.put(COLUMN_LSBP_ID, lsbpId);
                contentValues.put(COLUMN_MSAG_ID, msagId);
                contentValues.put(COLUMN_FLAG_AKTIF, activeFlag);
                db.insert(TABLE_NAME_E_LST_VA, null, contentValues);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public long saveNewProposal(P_MstDataProposalModel p_mstDataProposalModel) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(context.getString(R.string.NO_PROPOSAL_TAB), p_mstDataProposalModel.getNo_proposal_tab());
        cv.put(context.getString(R.string.NO_PROPOSAL), p_mstDataProposalModel.getNo_proposal());
        cv.put(context.getString(R.string.NAMA_PP), p_mstDataProposalModel.getNama_pp());
        cv.put(context.getString(R.string.TGL_LAHIR_PP), p_mstDataProposalModel.getTgl_lahir_pp());
        cv.put(context.getString(R.string.UMUR_PP), p_mstDataProposalModel.getUmur_pp());
        cv.put(context.getString(R.string.SEX_PP), p_mstDataProposalModel.getSex_pp());
        cv.put(context.getString(R.string.NAMA_TT), p_mstDataProposalModel.getNama_tt());
        cv.put(context.getString(R.string.TGL_LAHIR_TT), p_mstDataProposalModel.getTgl_lahir_tt());
        cv.put(context.getString(R.string.UMUR_TT), p_mstDataProposalModel.getUmur_tt());
        cv.put(context.getString(R.string.SEX_TT), p_mstDataProposalModel.getSex_tt());
        cv.put(context.getString(R.string.NAMA_AGEN), p_mstDataProposalModel.getNama_agen());
        cv.put(context.getString(R.string.MSAG_ID), p_mstDataProposalModel.getMsag_id());
        cv.put(context.getString(R.string.TGL_INPUT), p_mstDataProposalModel.getTgl_input());
        cv.put(context.getString(R.string.FLAG_AKTIF), p_mstDataProposalModel.getFlag_aktif());
        cv.put(context.getString(R.string.FLAG_TEST), p_mstDataProposalModel.getFlag_test());
        cv.put(context.getString(R.string.NAMA_USER), p_mstDataProposalModel.getNama_user());
        cv.put(context.getString(R.string.KODE_AGEN), p_mstDataProposalModel.getKode_agen());
        cv.put(context.getString(R.string.LSTB_ID), p_mstDataProposalModel.getLstb_id());
        cv.put(context.getString(R.string.ID_DIST), p_mstDataProposalModel.getId_dist());
        cv.put(context.getString(R.string.JENIS_ID), p_mstDataProposalModel.getJenis_id());
        cv.put(context.getString(R.string.LEAD_ID), p_mstDataProposalModel.getLead_id());
        cv.put(context.getString(R.string.LEAD_TAB_ID), p_mstDataProposalModel.getLead_tab_id());
        cv.put(context.getString(R.string.PROSPECT_ID), p_mstDataProposalModel.getProspect_id());
        cv.put(context.getString(R.string.FLAG_TT_CALON_BAYI), p_mstDataProposalModel.getFlag_tt_calon_bayi());
        cv.put(context.getString(R.string.FLAG_FINISH), p_mstDataProposalModel.getFlag_finish());
        cv.put(context.getString(R.string.FLAG_STAR), p_mstDataProposalModel.getFlag_star());
        cv.put(context.getString(R.string.LAST_PAGE_POSITION), p_mstDataProposalModel.getLast_page_position());
        cv.put(context.getString(R.string.FLAG_PROPER), p_mstDataProposalModel.getFlag_proper());
        cv.put(context.getString(R.string.FLAG_PACKET), p_mstDataProposalModel.getFlag_packet());
        cv.put(context.getString(R.string.va), p_mstDataProposalModel.getVirtual_acc());
        cv.put(context.getString(R.string.noid_member), p_mstDataProposalModel.getNoid());
        cv.put(context.getString(R.string.flag_pos), p_mstDataProposalModel.getFlag_pos());


        long insertedId = db.insert(TABLE_NAME_MST_DATA_PROPOSAL, null, cv);
        db.close();
        return insertedId;
    }
}
