/*
package id.co.ajsmsig.nb.pointofsale.binding

import android.databinding.BindingAdapter
import android.view.View
import android.widget.Toast
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.Progress
import id.co.ajsmsig.nb.pointofsale.custom.LoadingButton
import id.co.ajsmsig.nb.pointofsale.utils.Constants
import id.co.ajsmsig.nb.pointofsale.utils.DownloaderUtil
import id.co.ajsmsig.nb.pointofsale.utils.fileName
import id.co.ajsmsig.nb.pointofsale.utils.share
import org.wlf.filedownloader.DownloadFileInfo
import org.wlf.filedownloader.FileDownloader
import org.wlf.filedownloader.listener.OnDetectBigUrlFileListener
import org.wlf.filedownloader.listener.OnFileDownloadStatusListener
import java.io.File


@BindingAdapter("url", "eventName", "listener")
fun setURLForProductDetail(loadingButton: LoadingButton, url: String?, eventName: String? = null, listener: LoadingButtonListener? = null) {

    loadingButton.imageView.setOnClickListener(View.OnClickListener {
        val rootPath = DownloaderUtil.getRootDirPath(loadingButton.context)
        val savedFile = File(DownloaderUtil.getRootDirPath(loadingButton.context), url?.fileName())
        if (savedFile.exists()) {
            loadingButton.context.share(savedFile)
            listener?.onClick(eventName)
        } else {
            loadingButton.showProgress()

            FileDownloader.registerDownloadStatusListener(object : OnFileDownloadStatusListener {
                override fun onFileDownloadStatusCompleted(downloadFileInfo: DownloadFileInfo?) {

                    if (downloadFileInfo?.fileName == url?.fileName()) {
                        Toast.makeText(loadingButton.context, "Finish", Toast.LENGTH_SHORT).show()
                        loadingButton.dismissProgress()
                        try {
                            loadingButton.context.share(savedFile)
                            listener?.onClick(eventName)
                        } catch (e: Exception) {
                            Toast.makeText(loadingButton.context, "There is something error occured", Toast.LENGTH_SHORT).show()
                        }
                    }
                }

                override fun onFileDownloadStatusWaiting(downloadFileInfo: DownloadFileInfo?) {

                }

                override fun onFileDownloadStatusPreparing(downloadFileInfo: DownloadFileInfo?) {

                }

                override fun onFileDownloadStatusDownloading(downloadFileInfo: DownloadFileInfo?, downloadSpeed: Float, remainingTime: Long) {
                    if (downloadFileInfo == null) return
                }

                override fun onFileDownloadStatusFailed(url: String?, downloadFileInfo: DownloadFileInfo?, failReason: OnFileDownloadStatusListener.FileDownloadStatusFailReason?) {
                    Toast.makeText(loadingButton.context, failReason?.message, Toast.LENGTH_SHORT).show()
                }

                override fun onFileDownloadStatusPrepared(downloadFileInfo: DownloadFileInfo?) {
                    Toast.makeText(loadingButton.context, "Start", Toast.LENGTH_SHORT).show()
                }

                override fun onFileDownloadStatusPaused(downloadFileInfo: DownloadFileInfo?) {
                    Toast.makeText(loadingButton.context, "Pause", Toast.LENGTH_SHORT).show()
                }

            })

            FileDownloader.detect(url?.replace("~", Constants.API_URL), object : OnDetectBigUrlFileListener {
                override fun onDetectUrlFileFailed(url: String?, failReason: OnDetectBigUrlFileListener.DetectBigUrlFileFailReason?) {
                    Toast.makeText(loadingButton.context, failReason?.message, Toast.LENGTH_SHORT).show()
                }

                override fun onDetectNewDownloadFile(url: String?, fileName: String?, saveDir: String?, fileSize: Long) {
                    // here to change to custom fileName, saveDir if needed
                    FileDownloader.createAndStart(url, rootPath, url.fileName());
                }

                override fun onDetectUrlFileExist(url: String?) {
                    // continue to download
                    FileDownloader.start(url);
                }

            })


        }
    })
}

interface LoadingButtonListener {
    fun onClick(eventName: String?)
}*/
