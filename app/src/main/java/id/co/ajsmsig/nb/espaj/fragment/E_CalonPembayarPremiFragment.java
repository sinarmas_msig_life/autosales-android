package id.co.ajsmsig.nb.espaj.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.AutoCompleteTextViewClickListener;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.method.ProgressDialogClass;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;
import id.co.ajsmsig.nb.util.Const;

import static id.co.ajsmsig.nb.crm.method.StaticMethods.toCalendar;

/**
 * Created by Bernard on 23/05/2017.
 */

public class E_CalonPembayarPremiFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, AdapterView.OnItemClickListener, CompoundButton.OnCheckedChangeListener, TextView.OnEditorActionListener {
    private boolean isJobDescriptionValid;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem menu_validasi = menu.findItem(R.id.menu_validasi);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.e_fragment_calon_pembayar_premi_layout, container,
                false);
        me = E_MainActivity.e_bundle.getParcelable(getString(R.string.modelespaj));
        ButterKnife.bind(this, view);
        ((E_MainActivity) getActivity()).setSecondToolbar("Calon Pembayar Premi (3/", 3);

        //AutoCompleteTextView
        ac_calon_pemegang_premi_cp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_calon_pemegang_premi_cp, this));
        ac_calon_pemegang_premi_cp.setOnClickListener(this);
        ac_calon_pemegang_premi_cp.setOnFocusChangeListener(this);
        ac_calon_pemegang_premi_cp.addTextChangedListener(new generalTextWatcher(ac_calon_pemegang_premi_cp));

        ac_provinsi_cp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_provinsi_cp, this));
        ac_provinsi_cp.setOnClickListener(this);
        ac_provinsi_cp.setOnFocusChangeListener(this);
        ac_provinsi_cp.addTextChangedListener(new generalTextWatcher(ac_provinsi_cp));

        ac_pekerjaanlain_diluar_pekerjaanutama_cp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_pekerjaanlain_diluar_pekerjaanutama_cp, this));
        ac_pekerjaanlain_diluar_pekerjaanutama_cp.setOnClickListener(this);
        ac_pekerjaanlain_diluar_pekerjaanutama_cp.setOnFocusChangeListener(this);
        ac_pekerjaanlain_diluar_pekerjaanutama_cp.addTextChangedListener(new generalTextWatcher(ac_pekerjaanlain_diluar_pekerjaanutama_cp));

        ac_jumlah_total_pendapatan_rutin_bulan_cp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jumlah_total_pendapatan_rutin_bulan_cp, this));
        ac_jumlah_total_pendapatan_rutin_bulan_cp.setOnClickListener(this);
        ac_jumlah_total_pendapatan_rutin_bulan_cp.setOnFocusChangeListener(this);
        ac_jumlah_total_pendapatan_rutin_bulan_cp.addTextChangedListener(new generalTextWatcher(ac_jumlah_total_pendapatan_rutin_bulan_cp));

        ac_jumlah_total_pendapatan_nonrutin_tahun_cp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jumlah_total_pendapatan_nonrutin_tahun_cp, this));
        ac_jumlah_total_pendapatan_nonrutin_tahun_cp.setOnClickListener(this);
        ac_jumlah_total_pendapatan_nonrutin_tahun_cp.setOnFocusChangeListener(this);
        ac_jumlah_total_pendapatan_nonrutin_tahun_cp.addTextChangedListener(new generalTextWatcher(ac_jumlah_total_pendapatan_nonrutin_tahun_cp));

        ac_pilih_status_pihak_ketiga_cp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_pilih_status_pihak_ketiga_cp, this));
        ac_pilih_status_pihak_ketiga_cp.setOnClickListener(this);
        ac_pilih_status_pihak_ketiga_cp.setOnFocusChangeListener(this);
        ac_pilih_status_pihak_ketiga_cp.addTextChangedListener(new generalTextWatcher(ac_pilih_status_pihak_ketiga_cp));

        ac_kewarganegaraan_pihak_ketiga_cp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_kewarganegaraan_pihak_ketiga_cp, this));
        ac_kewarganegaraan_pihak_ketiga_cp.setOnClickListener(this);
        ac_kewarganegaraan_pihak_ketiga_cp.setOnFocusChangeListener(this);
        ac_kewarganegaraan_pihak_ketiga_cp.addTextChangedListener(new generalTextWatcher(ac_kewarganegaraan_pihak_ketiga_cp));

        ac_jenis_pekerjaan_cp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jenis_pekerjaan_cp, this));
        ac_jenis_pekerjaan_cp.setOnClickListener(this);
        ac_jenis_pekerjaan_cp.setOnFocusChangeListener(this);
        ac_jenis_pekerjaan_cp.addTextChangedListener(new generalTextWatcher(ac_jenis_pekerjaan_cp));

        ac_jabatan_pangkat_golongan_cp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jabatan_pangkat_golongan_cp, this));
        ac_jabatan_pangkat_golongan_cp.setOnClickListener(this);
        ac_jabatan_pangkat_golongan_cp.setOnFocusChangeListener(this);
        ac_jabatan_pangkat_golongan_cp.addTextChangedListener(new generalTextWatcher(ac_jabatan_pangkat_golongan_cp));

        ac_hubungan_dengan_pemegang_polis_cp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_hubungan_dengan_pemegang_polis_cp, this));
        ac_hubungan_dengan_pemegang_polis_cp.setOnClickListener(this);
        ac_hubungan_dengan_pemegang_polis_cp.setOnFocusChangeListener(this);
        ac_hubungan_dengan_pemegang_polis_cp.addTextChangedListener(new generalTextWatcher(ac_hubungan_dengan_pemegang_polis_cp));

        ac_klasifikasi_pekerjaan_phk3_cp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_klasifikasi_pekerjaan_phk3_cp, this));
        ac_klasifikasi_pekerjaan_phk3_cp.addTextChangedListener(new generalTextWatcher(ac_klasifikasi_pekerjaan_phk3_cp));

        //button
//        bt_lanjut_cp.setOnClickListener(this);
//        bt_kembali_cp.setOnClickListener(this);

        //editText
        et_nama_perusahaan_cp.addTextChangedListener(new generalTextWatcher(et_nama_perusahaan_cp));
        et_alamat_perusahaan_cp.addTextChangedListener(new generalTextWatcher(et_alamat_perusahaan_cp));
        et_kode_pos_cp.addTextChangedListener(new generalTextWatcher(et_kode_pos_cp));
        et_nomor_telp_perusahaan_cp.addTextChangedListener(new generalTextWatcher(et_nomor_telp_perusahaan_cp));
        et_nomor_fax_cp.addTextChangedListener(new generalTextWatcher(et_nomor_fax_cp));
        et_bidang_usaha_cp.addTextChangedListener(new generalTextWatcher(et_bidang_usaha_cp));
        et_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp.addTextChangedListener(new generalTextWatcher(et_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp));
        et_sebutkan1_cp.addTextChangedListener(new generalTextWatcher(et_sebutkan1_cp));
        et_sebutkan2_cp.addTextChangedListener(new generalTextWatcher(et_sebutkan2_cp));
        et_sebutkan3_cp.addTextChangedListener(new generalTextWatcher(et_sebutkan3_cp));
        et_sebutkan4_cp.addTextChangedListener(new generalTextWatcher(et_sebutkan4_cp));
        et_sebutkan5_cp.addTextChangedListener(new generalTextWatcher(et_sebutkan5_cp));
        et_sebutkan6_cp.addTextChangedListener(new generalTextWatcher(et_sebutkan6_cp));
        et_keterangan_status_pihak_ketiga_cp.addTextChangedListener(new generalTextWatcher(et_keterangan_status_pihak_ketiga_cp));
        et_nama_pihak_ketiga_cp.addTextChangedListener(new generalTextWatcher(et_nama_pihak_ketiga_cp));
        et_alamat_pihak_ketiga_cp.addTextChangedListener(new generalTextWatcher(et_alamat_pihak_ketiga_cp));
        et_no_telp_rumah_pihak_ketiga_cp.addTextChangedListener(new generalTextWatcher(et_no_telp_rumah_pihak_ketiga_cp));
        et_no_telp_kantor_pihak_ketiga_cp.addTextChangedListener(new generalTextWatcher(et_no_telp_kantor_pihak_ketiga_cp));
        et_alamat_email_pihak_ketiga_cp.addTextChangedListener(new generalTextWatcher(et_alamat_email_pihak_ketiga_cp));
        et_tanggal_pendirian_cp.addTextChangedListener(new generalTextWatcher(et_tanggal_pendirian_cp));
        et_tanggal_pendirian_cp.setOnClickListener(this);
        et_tanggal_pendirian_cp.setOnFocusChangeListener(this);
        et_kota_perusahaan_cp.addTextChangedListener(new generalTextWatcher(et_kota_perusahaan_cp));

        et_tempat_kedudukan_pihak_ketiga_cp.addTextChangedListener(new generalTextWatcher(et_tempat_kedudukan_pihak_ketiga_cp));
        et_tempat_lahir_pihak_ketiga_cp.addTextChangedListener(new generalTextWatcher(et_tempat_lahir_pihak_ketiga_cp));
        et_tanggal_lahir_cp.addTextChangedListener(new generalTextWatcher(et_tanggal_lahir_cp));
        et_tanggal_lahir_cp.setOnClickListener(this);
        et_tanggal_lahir_cp.setOnFocusChangeListener(this);
        et_bidang_usaha_pihak_ketiga_cp.addTextChangedListener(new generalTextWatcher(et_bidang_usaha_pihak_ketiga_cp));
        et_keterangan_jenis_pekerjaan_cp.addTextChangedListener(new generalTextWatcher(et_keterangan_jenis_pekerjaan_cp));
        et_instansi_departemen_cp.addTextChangedListener(new generalTextWatcher(et_instansi_departemen_cp));
        et_npwppihakketiga_pp.addTextChangedListener(new generalTextWatcher(et_npwppihakketiga_pp));
        et_sumber_dana_cp.addTextChangedListener(new generalTextWatcher(et_sumber_dana_cp));
        et_tujuan_penggunaan_dana_cp.addTextChangedListener(new generalTextWatcher(et_tujuan_penggunaan_dana_cp));
        et_tidak_dapat_info_dari_pihak_ketiga_cp.addTextChangedListener(new generalTextWatcher(et_tidak_dapat_info_dari_pihak_ketiga_cp));


        //checkButton
        check_dana_bulan_cp = new ArrayList<CheckBox>();
        check_dana_bulan_cp.add(cb_gaji_cp);
        check_dana_bulan_cp.add(cb_laba_perusahaan_cp);
        check_dana_bulan_cp.add(cb_hasil_investasi_cp);
        check_dana_bulan_cp.add(cb_lainnya_cp);
        check_dana_bulan_cp.add(cb_penghasil_suami_istri_cp);
        check_dana_bulan_cp.add(cb_hasil_usaha_cp);
        check_dana_bulan_cp.add(cb_orangtua_cp);

        cb_gaji_cp.setOnCheckedChangeListener(this);
        cb_laba_perusahaan_cp.setOnCheckedChangeListener(this);
        cb_hasil_investasi_cp.setOnCheckedChangeListener(this);
        cb_lainnya_cp.setOnCheckedChangeListener(this);
        cb_penghasil_suami_istri_cp.setOnCheckedChangeListener(this);
        cb_hasil_usaha_cp.setOnCheckedChangeListener(this);
        cb_orangtua_cp.setOnCheckedChangeListener(this);

        check_dana_tahun_cp = new ArrayList<CheckBox>();
        check_dana_tahun_cp.add(cb_bonus);
        check_dana_tahun_cp.add(cb_hadiah_warisan);
        check_dana_tahun_cp.add(cb_lainnya2_cp);
        check_dana_tahun_cp.add(cb_komisi);
        check_dana_tahun_cp.add(cb_hasil_investasi2_cp);
        check_dana_tahun_cp.add(cb_penjualan_aset);

        cb_bonus.setOnCheckedChangeListener(this);
        cb_hadiah_warisan.setOnCheckedChangeListener(this);
        cb_lainnya2_cp.setOnCheckedChangeListener(this);
        cb_komisi.setOnCheckedChangeListener(this);
        cb_hasil_investasi2_cp.setOnCheckedChangeListener(this);
        cb_penjualan_aset.setOnCheckedChangeListener(this);

        check_tujuan_cp = new ArrayList<CheckBox>();
        check_tujuan_cp.add(cb_proteksi);
        check_tujuan_cp.add(cb_tabungan_deposito);
        check_tujuan_cp.add(cb_lainnya3_cp);
        check_tujuan_cp.add(cb_pendidikan_cp);
        check_tujuan_cp.add(cb_dana_pensiun);
        check_tujuan_cp.add(cb_investasi);

        cb_proteksi.setOnCheckedChangeListener(this);
        cb_tabungan_deposito.setOnCheckedChangeListener(this);
        cb_lainnya3_cp.setOnCheckedChangeListener(this);
        cb_pendidikan_cp.setOnCheckedChangeListener(this);
        cb_dana_pensiun.setOnCheckedChangeListener(this);
        cb_investasi.setOnCheckedChangeListener(this);

        //button di Activity
        btn_lanjut = getActivity().findViewById(R.id.btn_lanjut);
        btn_lanjut.setOnClickListener(this);
        btn_kembali = getActivity().findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(this);
        second_toolbar = ((E_MainActivity) getActivity()).getSecond_toolbar();
        iv_save = second_toolbar.findViewById(R.id.iv_save);
        iv_save.setOnClickListener(this);
        iv_next = second_toolbar.findViewById(R.id.iv_next);
        iv_next.setOnClickListener(this);
        iv_prev = second_toolbar.findViewById(R.id.iv_prev);
        iv_prev.setOnClickListener(this);

        try {
            MethodSupport.load_setDropXml(R.xml.e_relasi, ac_calon_pemegang_premi_cp, getContext(), 2);
            MethodSupport.load_setDropXml(R.xml.e_relasi, ac_hubungan_dengan_pemegang_polis_cp, getContext(), 2);
            MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_kewarganegaraan_pihak_ketiga_cp, getContext(), 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        SetAdapterPropinsi();
        setAdapterJabatan();
        setAdapterYaTidak();
        setAdapterPenghasilan();
        if (!TextUtils.isEmpty(str_spintotal_thn_cp)){
            setAdapterPenghasilanNONada();
        }else {
            setAdapterPenghasilanNON();
        }
        setAdapterJnsPekerjaan();
        setAdapterPekerjaan("", ac_klasifikasi_pekerjaan_phk3_cp);
        restore();
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        ProgressDialogClass.removeSimpleProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        MethodSupport.active_view(1, btn_lanjut);
        MethodSupport.active_view(1, iv_next);
        MethodSupport.active_view(1, btn_kembali);
        MethodSupport.active_view(1, iv_prev);
        ProgressDialogClass.removeSimpleProgressDialog();
        try {
            MethodSupport.load_setDropXml(R.xml.e_relasi, ac_calon_pemegang_premi_cp, getContext(), 2);
            MethodSupport.load_setDropXml(R.xml.e_relasi, ac_hubungan_dengan_pemegang_polis_cp, getContext(), 2);
            MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_kewarganegaraan_pihak_ketiga_cp, getContext(), 0);
            MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_kewarganegaraan_pihak_ketiga_cp, getContext(), 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        SetAdapterPropinsi();
        setAdapterJabatan();
        setAdapterYaTidak();
        setAdapterPenghasilan();
        if (!TextUtils.isEmpty(str_spintotal_thn_cp)){
            setAdapterPenghasilanNONada();
        }else {
            setAdapterPenghasilanNON();
        }
        setAdapterJnsPekerjaan();
        setAdapterPekerjaan("", ac_klasifikasi_pekerjaan_phk3_cp);
        checkForSelectedJobDescriptionIsOnTheList(ac_klasifikasi_pekerjaan_phk3_cp.getText().toString().trim(), ac_klasifikasi_pekerjaan_phk3_cp);
        //Make the job description field dots orange as default when user haven't select any job desc.
        if (TextUtils.isEmpty(ac_klasifikasi_pekerjaan_phk3_cp.getText().toString().trim())) {
            iv_circle_klasifikasi_pekerjaan_phk3_cp.setColorFilter(ContextCompat.getColor(getActivity(), R.color.orange));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_validasi:
                me.getCalonPembayarPremiModel().setValidation(Validation());
                loadview();
                MyTaskValidasi taskValidasi = new MyTaskValidasi();
                taskValidasi.execute();
                Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTaskValidasi extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_CALON_PEMBAYAR_PREMI);
            return null;
        }
    }

    private class MyTaskSavePage extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_CALON_PEMBAYAR_PREMI);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_lanjut:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_next:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_save:
                String jobDescription = ac_klasifikasi_pekerjaan_phk3_cp.getText().toString().trim();
                boolean isKlasifikasiPekerjaanVisible = cl_data_pihak_ketiga_cp.isShown();

                //Only do klasifikasi pekerjaan re-validation if only the field is visible
                if (isKlasifikasiPekerjaanVisible && TextUtils.isEmpty(jobDescription)) {
                    ac_klasifikasi_pekerjaan_phk3_cp.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
                    ac_klasifikasi_pekerjaan_phk3_cp.requestFocus();
                    return;
                }

                if (isKlasifikasiPekerjaanVisible && !isJobDescriptionValid) {
                    ac_klasifikasi_pekerjaan_phk3_cp.setError(getResources().getString(R.string.err_msg_jobdesc_not_found_on_the_list));
                    ac_klasifikasi_pekerjaan_phk3_cp.requestFocus();
                    return;
                }

                loadview();
                MyTaskSavePage taskSavePage = new MyTaskSavePage();
                taskSavePage.execute();

                break;
            case R.id.iv_prev:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.btn_kembali:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.ac_provinsi_cp:
                ac_provinsi_cp.showDropDown();
                break;
            case R.id.ac_calon_pemegang_premi_cp:
                ac_calon_pemegang_premi_cp.showDropDown();
                break;
            case R.id.ac_pekerjaanlain_diluar_pekerjaanutama_cp:
                ac_pekerjaanlain_diluar_pekerjaanutama_cp.showDropDown();
                break;
            case R.id.ac_jumlah_total_pendapatan_rutin_bulan_cp:
                ac_jumlah_total_pendapatan_rutin_bulan_cp.showDropDown();
                break;
            case R.id.ac_jumlah_total_pendapatan_nonrutin_tahun_cp:
                setAdapterPenghasilanNONada();
                ac_jumlah_total_pendapatan_nonrutin_tahun_cp.showDropDown();
                break;
            case R.id.ac_pilih_status_pihak_ketiga_cp:
                ac_pilih_status_pihak_ketiga_cp.showDropDown();
                break;
            case R.id.ac_kewarganegaraan_pihak_ketiga_cp:
                ac_kewarganegaraan_pihak_ketiga_cp.showDropDown();
                break;
            case R.id.ac_jenis_pekerjaan_cp:
                ac_jenis_pekerjaan_cp.showDropDown();
                break;
            case R.id.ac_jabatan_pangkat_golongan_cp:
                ac_jabatan_pangkat_golongan_cp.showDropDown();
                break;
            case R.id.ac_hubungan_dengan_pemegang_polis_cp:
                ac_hubungan_dengan_pemegang_polis_cp.showDropDown();
                break;
            case R.id.et_tanggal_pendirian_cp:
                showDateDialog(1, et_tanggal_pendirian_cp.getText().toString(), et_tanggal_pendirian_cp);
                break;
            case R.id.et_tanggal_lahir_cp:
                showDateDialog(2, et_tanggal_lahir_cp.getText().toString(), et_tanggal_lahir_cp);
                break;

        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        return false;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.ac_calon_pemegang_premi_cp:
                if (hasFocus) ac_calon_pemegang_premi_cp.showDropDown();
                break;
            case R.id.ac_provinsi_cp:
                if (hasFocus) ac_provinsi_cp.showDropDown();
                break;
            case R.id.ac_pekerjaanlain_diluar_pekerjaanutama_cp:
                if (hasFocus) ac_pekerjaanlain_diluar_pekerjaanutama_cp.showDropDown();
                break;
            case R.id.ac_jumlah_total_pendapatan_rutin_bulan_cp:
                if (hasFocus) ac_jumlah_total_pendapatan_rutin_bulan_cp.showDropDown();
                break;
            case R.id.ac_jumlah_total_pendapatan_nonrutin_tahun_cp:
                setAdapterPenghasilanNONada();
                if (hasFocus) ac_jumlah_total_pendapatan_nonrutin_tahun_cp.showDropDown();
                break;
            case R.id.ac_pilih_status_pihak_ketiga_cp:
                if (hasFocus) ac_pilih_status_pihak_ketiga_cp.showDropDown();
                break;
            case R.id.ac_kewarganegaraan_pihak_ketiga_cp:
                if (hasFocus) ac_kewarganegaraan_pihak_ketiga_cp.showDropDown();
                break;
            case R.id.ac_jenis_pekerjaan_cp:
                if (hasFocus) ac_jenis_pekerjaan_cp.showDropDown();
                break;
            case R.id.ac_jabatan_pangkat_golongan_cp:
                if (hasFocus) ac_jabatan_pangkat_golongan_cp.showDropDown();
                break;
            case R.id.ac_hubungan_dengan_pemegang_polis_cp:
                if (hasFocus) ac_hubungan_dengan_pemegang_polis_cp.showDropDown();
                break;
            case R.id.et_tanggal_pendirian_cp:
                if (hasFocus)
                    showDateDialog(1, et_tanggal_pendirian_cp.getText().toString(), et_tanggal_pendirian_cp);
                break;
            case R.id.et_tanggal_lahir_cp:
                if (hasFocus)
                    showDateDialog(2, et_tanggal_lahir_cp.getText().toString(), et_tanggal_lahir_cp);
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.ac_calon_pemegang_premi_cp:
                ModelDropdownInt ModelHub = (ModelDropdownInt) parent.getAdapter().getItem(position);
                int_ket_cp = ModelHub.getId();
                val_hubcp();
                break;
            case R.id.ac_provinsi_cp:
                ModelDropdownInt modelProvinsi = (ModelDropdownInt) parent.getAdapter().getItem(position);
                int_propinsi_cp = modelProvinsi.getId();
                break;
            case R.id.ac_pekerjaanlain_diluar_pekerjaanutama_cp:
//                str_spin_pekerjaan_cp = ac_pekerjaanlain_diluar_pekerjaanutama_cp.getText().toString();
                int_spinbisnis_cp = ac_pekerjaanlain_diluar_pekerjaanutama_cp.getListSelection();
                break;
            case R.id.ac_jumlah_total_pendapatan_rutin_bulan_cp:
                str_spintotal_bln_cp = ac_jumlah_total_pendapatan_rutin_bulan_cp.getText().toString();
                break;
            case R.id.ac_jumlah_total_pendapatan_nonrutin_tahun_cp:
                str_spintotal_thn_cp = ac_jumlah_total_pendapatan_nonrutin_tahun_cp.getText().toString();
                break;
            case R.id.ac_pilih_status_pihak_ketiga_cp:
                String selection = (String) parent.getItemAtPosition(position);
                String[] yatidak = getResources().getStringArray(R.array.yatidak);
                for (int i = 0; i < yatidak.length; i++) {
                    if (yatidak[i].equals(selection)) {
                        int_spinpihak3_cp = i;
                        break;
                    }
                }
                val_pihak3();
                break;
            case R.id.ac_kewarganegaraan_pihak_ketiga_cp:
                ModelDropdownInt modelKewrgn_cp = (ModelDropdownInt) parent.getAdapter().getItem(position);
                int_spin_kewrgn_cp = modelKewrgn_cp.getId();
                break;
            case R.id.ac_jenis_pekerjaan_cp:
                str_spin_jns_pkrjaan_cp = ac_jenis_pekerjaan_cp.getText().toString();
                break;
            case R.id.ac_jabatan_pangkat_golongan_cp:
                ModelDropDownString modelJabatanPhk3 = (ModelDropDownString) parent.getAdapter().getItem(position);
                str_spin_jabatanphk3_cp = modelJabatanPhk3.getId();
                break;
            case R.id.ac_hubungan_dengan_pemegang_polis_cp:
                ModelDropdownInt modelHub_dgppphk3_cp = (ModelDropdownInt) parent.getAdapter().getItem(position);
                int_spin_hub_dgppphk3_cp = modelHub_dgppphk3_cp.getId();
                break;
            case R.id.ac_klasifikasi_pekerjaan_phk3_cp:
                //To make sure job description is only allowed picked from the dropdown
                isJobDescriptionValid = true;
                iv_circle_klasifikasi_pekerjaan_phk3_cp.setColorFilter(ContextCompat.getColor(getContext(), R.color.green));

                ModelDropdownInt model_pekerjaan = (ModelDropdownInt) parent.getAdapter().getItem(position);
                str_spin_pekerjaan_cp = model_pekerjaan.getValue();
                int_spin_pekerjaan_cp = model_pekerjaan.getId();
                break;
        }
    }

    //khusus checkbox
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_gaji_cp:
                str_gaji_cp = (isChecked) ? cb_gaji_cp.getText().toString() : "";

                bool_dana_bulan_cp = Method_Validator.ValidCheckDana(check_dana_bulan_cp, iv_circle_gaji_cp, getContext());
                break;
            case R.id.cb_laba_perusahaan_cp:
                str_laba_cp = (isChecked) ? cb_laba_perusahaan_cp.getText().toString() : "";
                bool_dana_bulan_cp = Method_Validator.ValidCheckDana(check_dana_bulan_cp, iv_circle_gaji_cp, getContext());
                break;
            case R.id.cb_hasil_investasi_cp:
                str_hslinves_cp = (isChecked) ? cb_hasil_investasi_cp.getText().toString() : "";
                bool_dana_bulan_cp = Method_Validator.ValidCheckDana(check_dana_bulan_cp, iv_circle_gaji_cp, getContext());
                break;
            case R.id.cb_lainnya_cp:
                str_lainnya_cp = (isChecked) ? cb_lainnya_cp.getText().toString() : "";
                bool_dana_bulan_cp = Method_Validator.ValidCheckDana(check_dana_bulan_cp, iv_circle_gaji_cp, getContext());
                break;
            case R.id.cb_penghasil_suami_istri_cp:
                str_penghsl_cp = (isChecked) ? cb_penghasil_suami_istri_cp.getText().toString() : "";
                bool_dana_bulan_cp = Method_Validator.ValidCheckDana(check_dana_bulan_cp, iv_circle_gaji_cp, getContext());
                break;
            case R.id.cb_hasil_usaha_cp:
                str_hslusaha_cp = (isChecked) ? cb_hasil_usaha_cp.getText().toString() : "";
                bool_dana_bulan_cp = Method_Validator.ValidCheckDana(check_dana_bulan_cp, iv_circle_gaji_cp, getContext());
                break;
            case R.id.cb_orangtua_cp:
                str_ortu_cp = (isChecked) ? cb_orangtua_cp.getText().toString() : "";
                bool_dana_bulan_cp = Method_Validator.ValidCheckDana(check_dana_bulan_cp, iv_circle_gaji_cp, getContext());
                break;

            case R.id.cb_bonus:
                str_bonus_cp = (isChecked) ? cb_bonus.getText().toString() : "";
//                bool_dana_tahun_cp = Method_Validator.ValidCheckDana(check_dana_tahun_cp, iv_circle_sumber_pendapatan_nonrutin_cp, getContext());
                break;
            case R.id.cb_hadiah_warisan:
                str_hadiah_cp = (isChecked) ? cb_hadiah_warisan.getText().toString() : "";
//                bool_dana_tahun_cp = Method_Validator.ValidCheckDana(check_dana_tahun_cp, iv_circle_sumber_pendapatan_nonrutin_cp, getContext());
                break;
            case R.id.cb_lainnya2_cp:
                str_lainnya_thn_cp = (isChecked) ? cb_lainnya2_cp.getText().toString() : "";
//                bool_dana_tahun_cp = Method_Validator.ValidCheckDana(check_dana_tahun_cp, iv_circle_sumber_pendapatan_nonrutin_cp, getContext());
                break;
            case R.id.cb_komisi:
                str_komisi_cp = (isChecked) ? cb_komisi.getText().toString() : "";
//                bool_dana_tahun_cp = Method_Validator.ValidCheckDana(check_dana_tahun_cp, iv_circle_sumber_pendapatan_nonrutin_cp, getContext());
                break;
            case R.id.cb_hasil_investasi2_cp:
                str_hslinves_thn_cp = (isChecked) ? cb_hasil_investasi2_cp.getText().toString() : "";
//                bool_dana_tahun_cp = Method_Validator.ValidCheckDana(check_dana_tahun_cp, iv_circle_sumber_pendapatan_nonrutin_cp, getContext());
                break;
            case R.id.cb_penjualan_aset:
                str_aset_cp = (isChecked) ? cb_penjualan_aset.getText().toString() : "";
//                bool_dana_tahun_cp = Method_Validator.ValidCheckDana(check_dana_tahun_cp, iv_circle_sumber_pendapatan_nonrutin_cp, getContext());
                break;

            case R.id.cb_proteksi:
                str_proteksi_cp = (isChecked) ? cb_proteksi.getText().toString() : "";
                bool_tujuan_cp = Method_Validator.ValidCheckDana(check_tujuan_cp, iv_circle_tujuan_pengajuan_asuransi_cp, getContext());
                break;
            case R.id.cb_tabungan_deposito:
                str_tabungan_cp = (isChecked) ? cb_tabungan_deposito.getText().toString() : "";
                bool_tujuan_cp = Method_Validator.ValidCheckDana(check_tujuan_cp, iv_circle_tujuan_pengajuan_asuransi_cp, getContext());
                break;
            case R.id.cb_lainnya3_cp:
                str_lainnya_tujuan_cp = (isChecked) ? cb_lainnya3_cp.getText().toString() : "";
                bool_tujuan_cp = Method_Validator.ValidCheckDana(check_tujuan_cp, iv_circle_tujuan_pengajuan_asuransi_cp, getContext());
                break;
            case R.id.cb_pendidikan_cp:
                str_pend_cp = (isChecked) ? cb_pendidikan_cp.getText().toString() : "";
                bool_tujuan_cp = Method_Validator.ValidCheckDana(check_tujuan_cp, iv_circle_tujuan_pengajuan_asuransi_cp, getContext());
                break;
            case R.id.cb_dana_pensiun:
                str_pensiun_cp = (isChecked) ? cb_dana_pensiun.getText().toString() : "";
                bool_tujuan_cp = Method_Validator.ValidCheckDana(check_tujuan_cp, iv_circle_tujuan_pengajuan_asuransi_cp, getContext());
                break;
            case R.id.cb_investasi:
                str_inves_cp = (isChecked) ? cb_investasi.getText().toString() : "";
                bool_tujuan_cp = Method_Validator.ValidCheckDana(check_tujuan_cp, iv_circle_tujuan_pengajuan_asuransi_cp, getContext());
                break;
        }
    }

    private class generalTextWatcher implements TextWatcher {
        private View view;

        generalTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                //autocompleteTextview
                case R.id.ac_calon_pemegang_premi_cp:
                    if (StaticMethods.isTextWidgetEmpty(ac_calon_pemegang_premi_cp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_calon_pemegang_premi_cp.setColorFilter(color);
                    tv_error_calon_pemegang_premi_cp.setVisibility(View.GONE);
                    break;
                case R.id.ac_provinsi_cp:
                    if (StaticMethods.isTextWidgetEmpty(ac_provinsi_cp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_provinsi_cp.setColorFilter(color);
                    tv_error_provinsi_cp.setVisibility(View.GONE);
                    break;
                case R.id.ac_pekerjaanlain_diluar_pekerjaanutama_cp:
                    if (StaticMethods.isTextWidgetEmpty(ac_pekerjaanlain_diluar_pekerjaanutama_cp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_pekerjaanlain_diluar_pekerjaanutama_cp.setColorFilter(color);
                    tv_error_pekerjaanlain_diluar_pekerjaanutama_cp.setVisibility(View.GONE);
                    break;
                case R.id.ac_jumlah_total_pendapatan_rutin_bulan_cp:
                    if (StaticMethods.isTextWidgetEmpty(ac_jumlah_total_pendapatan_rutin_bulan_cp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jumlah_total_pendapatan_rutin_bulan_cp.setColorFilter(color);
                    tv_error_jumlah_total_pendapatan_rutin_bulan_cp.setVisibility(View.GONE);
                    break;
                case R.id.ac_jumlah_total_pendapatan_nonrutin_tahun_cp:
                    if (StaticMethods.isTextWidgetEmpty(ac_jumlah_total_pendapatan_nonrutin_tahun_cp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jumlah_total_pendapatan_nonrutin_tahun_cp.setColorFilter(color);
                    tv_error_jumlah_total_pendapatan_nonrutin_tahun_cp.setVisibility(View.GONE);
                    if (ac_jumlah_total_pendapatan_nonrutin_tahun_cp.getText().toString().equals("< RP. 5 JUTA")){
                        tv_info_jumlah_total_pendapatan_nonrutin_tahun_cp.setVisibility(View.VISIBLE);
                    }else {
                        tv_info_jumlah_total_pendapatan_nonrutin_tahun_cp.setVisibility(View.GONE);
                    }
                    break;
                case R.id.ac_pilih_status_pihak_ketiga_cp:
                    if (StaticMethods.isTextWidgetEmpty(ac_pilih_status_pihak_ketiga_cp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_pilih_status_pihak_ketiga_cp.setColorFilter(color);
                    tv_error_pilih_status_pihak_ketiga_cp.setVisibility(View.GONE);
                    break;
                case R.id.ac_kewarganegaraan_pihak_ketiga_cp:
                    if (StaticMethods.isTextWidgetEmpty(ac_kewarganegaraan_pihak_ketiga_cp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_kewarganegaraan_pihak_ketiga_cp.setColorFilter(color);
                    tv_error_kewarganegaraan_pihak_ketiga_cp.setVisibility(View.GONE);
                    break;
                case R.id.ac_jenis_pekerjaan_cp:
                    if (StaticMethods.isTextWidgetEmpty(ac_jenis_pekerjaan_cp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jenis_pekerjaan_cp.setColorFilter(color);
                    tv_error_jenis_pekerjaan_cp.setVisibility(View.GONE);
                    break;
                case R.id.ac_jabatan_pangkat_golongan_cp:
                    if (StaticMethods.isTextWidgetEmpty(ac_jabatan_pangkat_golongan_cp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jabatan_pangkat_golongan_cp.setColorFilter(color);
                    tv_error_jabatan_pangkat_golongan_cp.setVisibility(View.GONE);
                    break;
                case R.id.ac_hubungan_dengan_pemegang_polis_cp:
                    if (StaticMethods.isTextWidgetEmpty(ac_hubungan_dengan_pemegang_polis_cp)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_hubungan_dengan_pemegang_polis_cp.setColorFilter(color);
                    tv_error_hubungan_dengan_pemegang_polis_cp.setVisibility(View.GONE);
                    break;
                case R.id.ac_klasifikasi_pekerjaan_phk3_cp:
                    validateJobDescription(ac_klasifikasi_pekerjaan_phk3_cp.getText().toString().trim(), ac_klasifikasi_pekerjaan_phk3_cp);
                    break;

                //EditText
                case R.id.et_nama_perusahaan_cp:
                    Method_Validator.ValEditWatcher(et_nama_perusahaan_cp, iv_circle_nama_perusahaan_cp,
                            tv_error_nama_perusahaan_cp, getContext());
                    break;
                case R.id.et_alamat_perusahaan_cp:
                    Method_Validator.ValEditWatcher(et_alamat_perusahaan_cp, iv_circle_alamat_perusahaan_cp,
                            tv_error_alamat_perusahaan_cp, getContext());
                    break;
                case R.id.et_kode_pos_cp:
                    Method_Validator.ValEditWatcher(et_kode_pos_cp, iv_circle_kode_pos_cp,
                            tv_error_kode_pos_cp, getContext());
                    break;
                case R.id.et_nomor_telp_perusahaan_cp:
                    Method_Validator.ValEditWatcher(et_nomor_telp_perusahaan_cp, iv_circle_nomor_telp_perusahaan_cp,
                            tv_error_nomor_telp_perusahaan_cp, getContext());
                    break;
                case R.id.et_bidang_usaha_cp:
                    Method_Validator.ValEditWatcher(et_bidang_usaha_cp, iv_circle_bidang_usaha_cp,
                            tv_error_bidang_usaha_cp, getContext());
                    break;
                case R.id.et_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp:
                    Method_Validator.ValEditWatcher(et_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp, iv_circle_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp,
                            tv_error_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp, getContext());
                    break;
                case R.id.et_keterangan_status_pihak_ketiga_cp:
                    Method_Validator.ValEditWatcher(et_keterangan_status_pihak_ketiga_cp, iv_circle_keterangan_status_pihak_ketiga_cp,
                            tv_error_keterangan_status_pihak_ketiga_cp, getContext());
                    break;
                case R.id.et_nama_pihak_ketiga_cp:
                    Method_Validator.ValEditWatcher(et_nama_pihak_ketiga_cp, iv_circle_nama_pihak_ketiga_cp,
                            tv_error_nama_pihak_ketiga_cp, getContext());
                    break;
                case R.id.et_alamat_pihak_ketiga_cp:
                    Method_Validator.ValEditWatcher(et_alamat_pihak_ketiga_cp, iv_circle_alamat_pihak_ketiga_cp,
                            tv_error_alamat_pihak_ketiga_cp, getContext());
                    break;
                case R.id.et_no_telp_rumah_pihak_ketiga_cp:
                    Method_Validator.ValEditWatcher(et_no_telp_rumah_pihak_ketiga_cp, iv_circle_no_telp_rumah_pihak_ketiga_cp,
                            tv_error_no_telp_kantor_pihak_ketiga_cp, getContext());
                    break;
                case R.id.et_no_telp_kantor_pihak_ketiga_cp:
                    Method_Validator.ValEditWatcher(et_no_telp_kantor_pihak_ketiga_cp, iv_circle_no_telp_kantor_pihak_ketiga_cp,
                            tv_error_no_telp_kantor_pihak_ketiga_cp, getContext());
                    break;
                case R.id.et_alamat_email_pihak_ketiga_cp:
                    Method_Validator.ValEditWatcherEmail(et_alamat_email_pihak_ketiga_cp, iv_circle_alamat_email_pihak_ketiga_cp,
                            tv_error_alamat_email_pihak_ketiga_cp, getContext());
                    break;
                case R.id.et_tanggal_pendirian_cp:
                    Method_Validator.ValEditWatcher(et_tanggal_pendirian_cp, iv_circle_tanggal_pendirian_cp,
                            tv_error_tanggal_pendirian_cp, getContext());
                    break;
                case R.id.et_tempat_kedudukan_pihak_ketiga_cp:
                    Method_Validator.ValEditWatcher(et_tempat_kedudukan_pihak_ketiga_cp, iv_circle_tempat_kedudukan_pihak_ketiga_cp,
                            tv_error_tempat_kedudukan_pihak_ketiga_cp, getContext());
                    break;
                case R.id.et_tempat_lahir_pihak_ketiga_cp:
                    Method_Validator.ValEditWatcher(et_tempat_lahir_pihak_ketiga_cp, iv_circle_tempat_lahir_pihak_ketiga_cp,
                            tv_error_tempat_lahir_pihak_ketiga_cp, getContext());
                    break;
                case R.id.et_tanggal_lahir_cp:
                    Method_Validator.ValEditWatcher(et_tanggal_lahir_cp, iv_circle_tanggal_lahir_cp,
                            tv_error_tanggal_lahir_cp, getContext());
                    break;
                case R.id.et_bidang_usaha_pihak_ketiga_cp:
                    Method_Validator.ValEditWatcher(et_bidang_usaha_pihak_ketiga_cp, iv_circle_bidang_usaha_pihak_ketiga_cp,
                            tv_error_bidang_usaha_pihak_ketiga_cp, getContext());
                    break;
                case R.id.et_keterangan_jenis_pekerjaan_cp:
                    Method_Validator.ValEditWatcher(et_keterangan_jenis_pekerjaan_cp, iv_circle_keterangan_jenis_pekerjaan_cp,
                            tv_error_keterangan_jenis_pekerjaan_cp, getContext());
                    break;
                case R.id.et_instansi_departemen_cp:
                    Method_Validator.ValEditWatcher(et_instansi_departemen_cp, iv_circle_instansi_departemen_cp,
                            tv_error_instansi_departemen_cp, getContext());
                    break;
                case R.id.et_npwppihakketiga_pp:
                    Method_Validator.ValEditWatcher(et_npwppihakketiga_pp, iv_circle_npwppihakketiga_pp,
                            tv_error_npwppihakketiga_pp, getContext());
                    break;
                case R.id.et_sumber_dana_cp:
                    Method_Validator.ValEditWatcher(et_sumber_dana_cp, iv_circle_sumber_dana_cp,
                            tv_error_sumber_dana_cp, getContext());
                    break;
                case R.id.et_tujuan_penggunaan_dana_cp:
                    Method_Validator.ValEditWatcher(et_tujuan_penggunaan_dana_cp, iv_circle_tujuan_penggunaan_dana_cp,
                            tv_error_tujuan_penggunaan_dana_cp, getContext());
                    break;
                case R.id.et_kota_perusahaan_cp:
                    Method_Validator.ValEditWatcher(et_kota_perusahaan_cp, iv_circle_kota_perusahaan_cp,
                            tv_error_kota_perusahaan_cp, getContext());
                    break;
            }
        }
    }

    private void loadview() {
        me.getCalonPembayarPremiModel().setStr_spin_pekerjaan_cp(ac_klasifikasi_pekerjaan_phk3_cp.getText().toString());
        me.getCalonPembayarPremiModel().setNama_perush_cp(et_nama_perusahaan_cp.getText().toString());
        me.getCalonPembayarPremiModel().setAlmt_perush_cp(et_alamat_perusahaan_cp.getText().toString());
        me.getCalonPembayarPremiModel().setKota_perush_cp(et_kota_perusahaan_cp.getText().toString());
        me.getCalonPembayarPremiModel().setKdpos_perush_cp(et_kode_pos_cp.getText().toString());
        me.getCalonPembayarPremiModel().setNofax_perush_cp(et_nomor_fax_cp.getText().toString());
        me.getCalonPembayarPremiModel().setBid_usaha_cp(et_bidang_usaha_cp.getText().toString());
        me.getCalonPembayarPremiModel().setEditbisnis_cp(et_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp.getText().toString());
        me.getCalonPembayarPremiModel().setEditpihak3_cp(et_keterangan_status_pihak_ketiga_cp.getText().toString());
        me.getCalonPembayarPremiModel().setEdit_nmpihak3_cp(et_nama_pihak_ketiga_cp.getText().toString());
        me.getCalonPembayarPremiModel().setAlmt_phk3_cp(et_alamat_pihak_ketiga_cp.getText().toString());
        me.getCalonPembayarPremiModel().setNotlp_phk3_cp(et_no_telp_rumah_pihak_ketiga_cp.getText().toString());
        me.getCalonPembayarPremiModel().setNotlp_kntrphk3_cp(et_no_telp_kantor_pihak_ketiga_cp.getText().toString());
        me.getCalonPembayarPremiModel().setEdit_emailphk3_cp(et_alamat_email_pihak_ketiga_cp.getText().toString());
        me.getCalonPembayarPremiModel().setTmpt_kedudukan_cp(et_tempat_kedudukan_pihak_ketiga_cp.getText().toString());
        me.getCalonPembayarPremiModel().setBidusaha_phk3_cp(et_bidang_usaha_pihak_ketiga_cp.getText().toString());
        me.getCalonPembayarPremiModel().setLain_jns_pkrjaan_cp(et_keterangan_jenis_pekerjaan_cp.getText().toString());
        me.getCalonPembayarPremiModel().setInstansi_phk3_cp(et_instansi_departemen_cp.getText().toString());
        me.getCalonPembayarPremiModel().setNpwp_phk3_cp(et_npwppihakketiga_pp.getText().toString());
        me.getCalonPembayarPremiModel().setSumber_dana_phk3_cp(et_sumber_dana_cp.getText().toString());
        me.getCalonPembayarPremiModel().setTujuan_phk3_cp(et_tujuan_penggunaan_dana_cp.getText().toString());
        me.getCalonPembayarPremiModel().setAlasan_phk3_cp(et_tidak_dapat_info_dari_pihak_ketiga_cp.getText().toString());
        me.getCalonPembayarPremiModel().setTgl_pendirian_cp(et_tanggal_pendirian_cp.getText().toString());
        me.getCalonPembayarPremiModel().setTelp_perush_cp(et_nomor_telp_perusahaan_cp.getText().toString());
//        me.getCalonPembayarPremiModel().setDittd_phk3_cp();
//        me.getCalonPembayarPremiModel().setNama_ttdphk3_cp();
//        me.getCalonPembayarPremiModel().setTgl_pendirian_cp();
//        me.getCalonPembayarPremiModel().setTgl_lhrphk3_cp();
//        me.getCalonPembayarPremiModel().setPdtgl_phk3_cp();
        me.getCalonPembayarPremiModel().setTmpt_lhrphk3_cp(et_tempat_lahir_pihak_ketiga_cp.getText().toString());
        me.getCalonPembayarPremiModel().setTgl_lhrphk3_cp(et_tanggal_lahir_cp.getText().toString());
        me.getCalonPembayarPremiModel().setSpin_kewrgn_cp(int_spin_kewrgn_cp);
        me.getCalonPembayarPremiModel().setInt_gaji_cp(int_gaji_cp);
        me.getCalonPembayarPremiModel().setStr_gaji_cp(str_gaji_cp);
        me.getCalonPembayarPremiModel().setInt_penghsl_cp(int_penghsl_cp);
        me.getCalonPembayarPremiModel().setStr_penghsl_cp(str_penghsl_cp);
        me.getCalonPembayarPremiModel().setInt_ortu_cp(int_ortu_cp);
        me.getCalonPembayarPremiModel().setStr_ortu_cp(str_ortu_cp);
        me.getCalonPembayarPremiModel().setInt_laba_cp(int_laba_cp);
        me.getCalonPembayarPremiModel().setStr_laba_cp(str_laba_cp);
        me.getCalonPembayarPremiModel().setInt_hslusaha_cp(int_hslusaha_cp);
        me.getCalonPembayarPremiModel().setStr_hslusaha_cp(str_hslusaha_cp);

        me.getCalonPembayarPremiModel().setEdit_hslusaha_cp(et_sebutkan3_cp.getText().toString());

        me.getCalonPembayarPremiModel().setInt_hslinves_cp(int_hslinves_cp);
        me.getCalonPembayarPremiModel().setStr_hslinves_cp(str_hslinves_cp);

        me.getCalonPembayarPremiModel().setEdit_hslinves_cp(et_sebutkan1_cp.getText().toString());

        me.getCalonPembayarPremiModel().setInt_lainnya_cp(int_lainnya_cp);
        me.getCalonPembayarPremiModel().setStr_lainnya_cp(str_lainnya_cp);

        me.getCalonPembayarPremiModel().setEdit_lainnya_cp(et_sebutkan2_cp.getText().toString());

        me.getCalonPembayarPremiModel().setInt_bonus_cp(int_bonus_cp);
        me.getCalonPembayarPremiModel().setStr_bonus_cp(str_bonus_cp);
        me.getCalonPembayarPremiModel().setInt_komisi_cp(int_komisi_cp);
        me.getCalonPembayarPremiModel().setStr_komisi_cp(str_komisi_cp);
        me.getCalonPembayarPremiModel().setInt_aset_cp(int_aset_cp);
        me.getCalonPembayarPremiModel().setStr_aset_cp(str_aset_cp);
        me.getCalonPembayarPremiModel().setInt_hadiah_cp(int_hadiah_cp);
        me.getCalonPembayarPremiModel().setStr_hadiah_cp(str_hadiah_cp);
        me.getCalonPembayarPremiModel().setInt_hslinves_thn_cp(int_hslinves_thn_cp);
        me.getCalonPembayarPremiModel().setStr_hslinves_thn_cp(str_hslinves_thn_cp);

        me.getCalonPembayarPremiModel().setEdit_hslinves_thn_cp(et_sebutkan5_cp.getText().toString());

        me.getCalonPembayarPremiModel().setInt_lainnya_thn_cp(int_lainnya_thn_cp);
        me.getCalonPembayarPremiModel().setStr_lainnya_thn_cp(str_lainnya_thn_cp);

        me.getCalonPembayarPremiModel().setEdit_lainnya_thn_cp(et_sebutkan4_cp.getText().toString());

        me.getCalonPembayarPremiModel().setInt_proteksi_cp(int_proteksi_cp);
        me.getCalonPembayarPremiModel().setStr_proteksi_cp(str_proteksi_cp);
        me.getCalonPembayarPremiModel().setInt_gaji_cp(int_pend_cp);
        me.getCalonPembayarPremiModel().setStr_pend_cp(str_pend_cp);
        me.getCalonPembayarPremiModel().setInt_inves_cp(int_inves_cp);
        me.getCalonPembayarPremiModel().setStr_inves_cp(str_inves_cp);
        me.getCalonPembayarPremiModel().setInt_tabungan_cp(int_tabungan_cp);
        me.getCalonPembayarPremiModel().setStr_tabungan_cp(str_tabungan_cp);
        me.getCalonPembayarPremiModel().setInt_pensiun_cp(int_pensiun_cp);
        me.getCalonPembayarPremiModel().setStr_pensiun_cp(str_pensiun_cp);
        me.getCalonPembayarPremiModel().setInt_lainnya_tujuan_cp(int_lainnya_tujuan_cp);
        me.getCalonPembayarPremiModel().setStr_lainnya_tujuan_cp(str_lainnya_tujuan_cp);

        me.getCalonPembayarPremiModel().setEdit_lainnya_tujuan_cp(et_sebutkan6_cp.getText().toString());

        me.getCalonPembayarPremiModel().setInt_ket_cp(int_ket_cp);
        me.getCalonPembayarPremiModel().setInt_propinsi_cp(int_propinsi_cp);
        me.getCalonPembayarPremiModel().setInt_spinbisnis_cp(int_spinbisnis_cp);
        me.getCalonPembayarPremiModel().setStr_spintotal_bln_cp(str_spintotal_bln_cp);
        me.getCalonPembayarPremiModel().setStr_spintotal_thn_cp(str_spintotal_thn_cp);
        me.getCalonPembayarPremiModel().setInt_spinpihak3_cp(int_spinpihak3_cp);
        me.getCalonPembayarPremiModel().setInt_spin_kewrgn_cp(int_spin_kewrgn_cp);
        me.getCalonPembayarPremiModel().setSpin_pekerjaan_cp(int_spin_pekerjaan_cp);
        me.getCalonPembayarPremiModel().setStr_spin_jns_pkrjaan_cp(str_spin_jns_pkrjaan_cp);
        me.getCalonPembayarPremiModel().setInt_spin_hub_dgppphk3_cp(int_spin_hub_dgppphk3_cp);
        me.getCalonPembayarPremiModel().setStr_spin_jabatanphk3_cp(str_spin_jabatanphk3_cp);
        me.getCalonPembayarPremiModel().setFile_ttd_cp(file);
        E_MainActivity.e_bundle.putParcelable(getString(R.string.modelespaj), me);
    }

    private void restore() {
        try {
            //edittext
            et_nama_perusahaan_cp.setText(me.getCalonPembayarPremiModel().getNama_perush_cp());
            et_alamat_perusahaan_cp.setText(me.getCalonPembayarPremiModel().getAlmt_perush_cp());
            et_kota_perusahaan_cp.setText(me.getCalonPembayarPremiModel().getKota_perush_cp());
            et_tanggal_pendirian_cp.setText(me.getCalonPembayarPremiModel().getTgl_pendirian_cp());
            et_nomor_telp_perusahaan_cp.setText(me.getCalonPembayarPremiModel().getTelp_perush_cp());

            et_kode_pos_cp.setText(me.getCalonPembayarPremiModel().getKdpos_perush_cp());
            et_nomor_fax_cp.setText(me.getCalonPembayarPremiModel().getNofax_perush_cp());
            et_bidang_usaha_cp.setText(me.getCalonPembayarPremiModel().getBid_usaha_cp());
            et_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp.setText(me.getCalonPembayarPremiModel().getEditbisnis_cp());
            et_keterangan_status_pihak_ketiga_cp.setText(me.getCalonPembayarPremiModel().getEditpihak3_cp());
            et_nama_pihak_ketiga_cp.setText(me.getCalonPembayarPremiModel().getEdit_nmpihak3_cp());
            et_alamat_pihak_ketiga_cp.setText(me.getCalonPembayarPremiModel().getAlmt_phk3_cp());
            et_no_telp_rumah_pihak_ketiga_cp.setText(me.getCalonPembayarPremiModel().getNotlp_phk3_cp());
            et_no_telp_kantor_pihak_ketiga_cp.setText(me.getCalonPembayarPremiModel().getNotlp_kntrphk3_cp());
            et_alamat_email_pihak_ketiga_cp.setText(me.getCalonPembayarPremiModel().getEdit_emailphk3_cp());
            et_tempat_kedudukan_pihak_ketiga_cp.setText(me.getCalonPembayarPremiModel().getTmpt_kedudukan_cp());
            et_bidang_usaha_pihak_ketiga_cp.setText(me.getCalonPembayarPremiModel().getBidusaha_phk3_cp());
            et_keterangan_jenis_pekerjaan_cp.setText(me.getCalonPembayarPremiModel().getLain_jns_pkrjaan_cp());
            et_instansi_departemen_cp.setText(me.getCalonPembayarPremiModel().getInstansi_phk3_cp());
            et_npwppihakketiga_pp.setText(me.getCalonPembayarPremiModel().getNpwp_phk3_cp());
            et_sumber_dana_cp.setText(me.getCalonPembayarPremiModel().getSumber_dana_phk3_cp());
            et_tujuan_penggunaan_dana_cp.setText(me.getCalonPembayarPremiModel().getTujuan_phk3_cp());
            et_tidak_dapat_info_dari_pihak_ketiga_cp.setText(me.getCalonPembayarPremiModel().getAlasan_phk3_cp());
            et_tanggal_lahir_cp.setText(me.getCalonPembayarPremiModel().getTgl_lhrphk3_cp());
            et_tempat_lahir_pihak_ketiga_cp.setText(me.getCalonPembayarPremiModel().getTmpt_lhrphk3_cp());
            et_sebutkan3_cp.setText(me.getCalonPembayarPremiModel().getEdit_hslusaha_cp());
            et_sebutkan1_cp.setText(me.getCalonPembayarPremiModel().getEdit_hslinves_cp());
            et_sebutkan2_cp.setText(me.getCalonPembayarPremiModel().getEdit_lainnya_cp());
            et_sebutkan5_cp.setText(me.getCalonPembayarPremiModel().getEdit_hslinves_thn_cp());
            et_sebutkan4_cp.setText(me.getCalonPembayarPremiModel().getEdit_lainnya_thn_cp());
            et_sebutkan6_cp.setText(me.getCalonPembayarPremiModel().getEdit_lainnya_tujuan_cp());

            int_spin_kewrgn_cp = me.getCalonPembayarPremiModel().getInt_spin_kewrgn_cp();
            ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_warganegara, 0);
            ac_kewarganegaraan_pihak_ketiga_cp.setText((MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, int_spin_kewrgn_cp)));

            int_ket_cp = me.getCalonPembayarPremiModel().getInt_ket_cp();
            ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_relasi, 2);
            ac_calon_pemegang_premi_cp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, int_ket_cp));
            MethodSupport.active_view(0, ac_calon_pemegang_premi_cp);
            val_hubcp();
            int_ket_cp = me.getCalonPembayarPremiModel().getInt_ket_cp();
            ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_relasi, 2);
            ac_calon_pemegang_premi_cp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, int_ket_cp));

            int_propinsi_cp = me.getCalonPembayarPremiModel().getInt_propinsi_cp();
            ListDropDownSPAJ = new E_Select(getContext()).getLst_Propinsi();
            ac_provinsi_cp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, int_propinsi_cp));

            str_spintotal_bln_cp = me.getCalonPembayarPremiModel().getStr_spintotal_bln_cp();
            ac_jumlah_total_pendapatan_rutin_bulan_cp.setText(str_spintotal_bln_cp);

            str_spintotal_thn_cp = me.getCalonPembayarPremiModel().getStr_spintotal_thn_cp();
            ac_jumlah_total_pendapatan_nonrutin_tahun_cp.setText(str_spintotal_thn_cp);
            if (int_ket_cp == 40) {
                int_spinpihak3_cp = 0;
            } else {
                int_spinpihak3_cp = 1;
            }
            ac_pilih_status_pihak_ketiga_cp.setText(MethodSupport.getAdapterPositionArray(getResources().getStringArray(R.array.yatidak), int_spinpihak3_cp));

            int_spinbisnis_cp = me.getCalonPembayarPremiModel().getInt_spinbisnis_cp();
            ac_pekerjaanlain_diluar_pekerjaanutama_cp.setText(MethodSupport.getAdapterPositionArray(getResources().getStringArray(R.array.yatidak), int_spinbisnis_cp));

            str_spin_jns_pkrjaan_cp = me.getCalonPembayarPremiModel().getStr_spin_jns_pkrjaan_cp();
            Adapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.jns_pekerjaan));
            ac_jenis_pekerjaan_cp.setText(str_spin_jns_pkrjaan_cp);

            int_spin_hub_dgppphk3_cp = int_ket_cp;
            ListDropDownSPAJ = MethodSupport.getXML(getContext(), R.xml.e_relasi, 2);
            ac_hubungan_dengan_pemegang_polis_cp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, int_spin_hub_dgppphk3_cp));
            MethodSupport.active_view(0, ac_hubungan_dengan_pemegang_polis_cp);
            str_spin_pekerjaan_cp = me.getCalonPembayarPremiModel().getStr_spin_pekerjaan_cp();
            int_spin_pekerjaan_cp = me.getCalonPembayarPremiModel().getSpin_pekerjaan_cp();
            ListDropDownSPAJ = new E_Select(getContext()).getLst_Pekerjaan("");
            ac_klasifikasi_pekerjaan_phk3_cp.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownSPAJ, int_spin_pekerjaan_cp));
//            ac_klasifikasi_pekerjaan_phk3_cp.setText(str_spin_pekerjaan_cp);
            str_spin_jabatanphk3_cp = me.getCalonPembayarPremiModel().getStr_spin_jabatanphk3_cp();
            ListDropDownString = new E_Select(getContext()).getLstJabatan();
            ac_jabatan_pangkat_golongan_cp.setText(str_spin_jabatanphk3_cp);

            //CHECKBOX
            int_gaji_cp = me.getCalonPembayarPremiModel().getInt_gaji_cp();
            str_gaji_cp = me.getCalonPembayarPremiModel().getStr_gaji_cp();
            if (!str_gaji_cp.equals("")) {
                cb_gaji_cp.setChecked(true);
            }
            int_penghsl_cp = me.getCalonPembayarPremiModel().getInt_penghsl_cp();
            str_penghsl_cp = me.getCalonPembayarPremiModel().getStr_penghsl_cp();
            if (!str_penghsl_cp.equals("")) {
                cb_penghasil_suami_istri_cp.setChecked(true);
            }
            int_ortu_cp = me.getCalonPembayarPremiModel().getInt_ortu_cp();
            str_ortu_cp = me.getCalonPembayarPremiModel().getStr_ortu_cp();
            if (!str_ortu_cp.equals("")) {
                cb_orangtua_cp.setChecked(true);
            }
            int_laba_cp = me.getCalonPembayarPremiModel().getInt_laba_cp();
            str_laba_cp = me.getCalonPembayarPremiModel().getStr_laba_cp();
            if (!str_laba_cp.equals("")) {
                cb_laba_perusahaan_cp.setChecked(true);
            }
            int_hslusaha_cp = me.getCalonPembayarPremiModel().getInt_hslusaha_cp();
            str_hslusaha_cp = me.getCalonPembayarPremiModel().getStr_hslusaha_cp();
            if (!str_hslusaha_cp.equals("")) {
                cb_hasil_usaha_cp.setChecked(true);
            }
            int_hslinves_cp = me.getCalonPembayarPremiModel().getInt_hslinves_cp();
            str_hslinves_cp = me.getCalonPembayarPremiModel().getStr_hslinves_cp();
            if (!str_hslinves_cp.equals("")) {
                cb_hasil_investasi_cp.setChecked(true);
            }
            int_lainnya_cp = me.getCalonPembayarPremiModel().getInt_lainnya_cp();
            str_lainnya_cp = me.getCalonPembayarPremiModel().getStr_lainnya_cp();
            if (!str_lainnya_cp.equals("")) {
                cb_lainnya_cp.setChecked(true);
            }
            int_bonus_cp = me.getCalonPembayarPremiModel().getInt_bonus_cp();
            str_bonus_cp = me.getCalonPembayarPremiModel().getStr_bonus_cp();
            if (!str_bonus_cp.equals("")) {
                cb_bonus.setChecked(true);
            }
            int_komisi_cp = me.getCalonPembayarPremiModel().getInt_komisi_cp();
            str_komisi_cp = me.getCalonPembayarPremiModel().getStr_komisi_cp();
            if (!str_komisi_cp.equals("")) {
                cb_komisi.setChecked(true);
            }
            int_aset_cp = me.getCalonPembayarPremiModel().getInt_aset_cp();
            str_aset_cp = me.getCalonPembayarPremiModel().getStr_aset_cp();
            if (!str_aset_cp.equals("")) {
                cb_penjualan_aset.setChecked(true);
            }
            int_hadiah_cp = me.getCalonPembayarPremiModel().getInt_hadiah_cp();
            str_hadiah_cp = me.getCalonPembayarPremiModel().getStr_hadiah_cp();
            if (!str_hadiah_cp.equals("")) {
                cb_hadiah_warisan.setChecked(true);
            }
            int_hslinves_thn_cp = me.getCalonPembayarPremiModel().getInt_hslinves_thn_cp();
            str_hslinves_thn_cp = me.getCalonPembayarPremiModel().getStr_hslinves_thn_cp();
            if (!str_hslinves_thn_cp.equals("")) {
                cb_hasil_investasi2_cp.setChecked(true);
            }
            int_lainnya_thn_cp = me.getCalonPembayarPremiModel().getInt_lainnya_thn_cp();
            str_lainnya_thn_cp = me.getCalonPembayarPremiModel().getStr_lainnya_thn_cp();
            if (!str_lainnya_thn_cp.equals("")) {
                cb_lainnya2_cp.setChecked(true);
            }
            int_proteksi_cp = me.getCalonPembayarPremiModel().getInt_proteksi_cp();
            str_proteksi_cp = me.getCalonPembayarPremiModel().getStr_proteksi_cp();
            if (!str_proteksi_cp.equals("")) {
                cb_proteksi.setChecked(true);
            }
            int_pend_cp = me.getCalonPembayarPremiModel().getInt_pend_cp();
            str_pend_cp = me.getCalonPembayarPremiModel().getStr_pend_cp();
            if (!str_pend_cp.equals("")) {
                cb_pendidikan_cp.setChecked(true);
            }
            int_inves_cp = me.getCalonPembayarPremiModel().getInt_inves_cp();
            str_inves_cp = me.getCalonPembayarPremiModel().getStr_inves_cp();
            if (!str_inves_cp.equals("")) {
                cb_investasi.setChecked(true);
            }
            int_tabungan_cp = me.getCalonPembayarPremiModel().getInt_tabungan_cp();
            str_tabungan_cp = me.getCalonPembayarPremiModel().getStr_tabungan_cp();
            if (!str_tabungan_cp.equals("")) {
                cb_tabungan_deposito.setChecked(true);
            }
            int_pensiun_cp = me.getCalonPembayarPremiModel().getInt_pensiun_cp();
            str_pensiun_cp = me.getCalonPembayarPremiModel().getStr_pensiun_cp();
            if (!str_pensiun_cp.equals("")) {
                cb_dana_pensiun.setChecked(true);
            }
            int_lainnya_tujuan_cp = me.getCalonPembayarPremiModel().getInt_lainnya_tujuan_cp();
            str_lainnya_tujuan_cp = me.getCalonPembayarPremiModel().getStr_lainnya_tujuan_cp();
            if (!str_lainnya_tujuan_cp.equals("")) {
                cb_lainnya3_cp.setChecked(true);
            }

            file = me.getCalonPembayarPremiModel().getFile_ttd_cp();
            if (!me.getValidation()) {
                Validation();
            }
            if (me.getModelID().getFlag_aktif() == 1) {
                MethodSupport.disable(false, cl_child_cp);
            }
            MethodSupport.active_view(1, ac_klasifikasi_pekerjaan_phk3_cp); //sementara
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    private boolean Validation() {
        boolean val = true;
        int color = ContextCompat.getColor(getContext(), R.color.red);
        if (cl_form_data_perusahaan_cp.isShown()) {
            if (et_nama_perusahaan_cp.isShown() && et_nama_perusahaan_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_nama_perusahaan_cp, tv_error_nama_perusahaan_cp, iv_circle_nama_perusahaan_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_nama_perusahaan_cp.setVisibility(View.VISIBLE);
//                tv_error_nama_perusahaan_cp.setText(getString(R.string.error_field_required));
//                iv_circle_nama_perusahaan_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_nama_perusahaan_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_nama_perusahaan_cp, tv_error_nama_perusahaan_cp, iv_circle_nama_perusahaan_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (et_alamat_perusahaan_cp.isShown() && et_alamat_perusahaan_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_alamat_perusahaan_cp, tv_error_alamat_perusahaan_cp, iv_circle_alamat_perusahaan_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_alamat_perusahaan_cp.setVisibility(View.VISIBLE);
//                tv_error_alamat_perusahaan_cp.setText(getString(R.string.error_field_required));
//                iv_circle_alamat_perusahaan_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_alamat_perusahaan_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_alamat_perusahaan_cp, tv_error_alamat_perusahaan_cp, iv_circle_alamat_perusahaan_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (et_kota_perusahaan_cp.isShown() && et_kota_perusahaan_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_kota_perusahaan_cp, tv_error_kota_perusahaan_cp, iv_circle_kota_perusahaan_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_kota_perusahaan_cp.setVisibility(View.VISIBLE);
//                tv_error_kota_perusahaan_cp.setText(getString(R.string.error_field_required));
//                iv_circle_kota_perusahaan_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_kota_perusahaan_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_kota_perusahaan_cp, tv_error_kota_perusahaan_cp, iv_circle_kota_perusahaan_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (ac_provinsi_cp.isShown() && ac_provinsi_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, ac_provinsi_cp, tv_error_provinsi_cp, iv_circle_provinsi_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_provinsi_cp.setVisibility(View.VISIBLE);
//                tv_error_provinsi_cp.setText(getString(R.string.error_field_required));
//                iv_circle_provinsi_cp.setColorFilter(color);
            }
            if (et_nomor_telp_perusahaan_cp.isShown() && et_nomor_telp_perusahaan_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_nomor_telp_perusahaan_cp, tv_error_nomor_telp_perusahaan_cp, iv_circle_nomor_telp_perusahaan_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_nomor_telp_perusahaan_cp.setVisibility(View.VISIBLE);
//                tv_error_nomor_telp_perusahaan_cp.setText(getString(R.string.error_field_required));
//                iv_circle_nomor_telp_perusahaan_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_nomor_telp_perusahaan_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_nomor_telp_perusahaan_cp, tv_error_nomor_telp_perusahaan_cp, iv_circle_nomor_telp_perusahaan_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (et_bidang_usaha_cp.isShown() && et_bidang_usaha_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_bidang_usaha_cp, tv_error_bidang_usaha_cp, iv_circle_bidang_usaha_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_bidang_usaha_cp.setVisibility(View.VISIBLE);
//                tv_error_bidang_usaha_cp.setText(getString(R.string.error_field_required));
//                iv_circle_bidang_usaha_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_bidang_usaha_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_bidang_usaha_cp, tv_error_bidang_usaha_cp, iv_circle_bidang_usaha_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (ac_pekerjaanlain_diluar_pekerjaanutama_cp.isShown() && ac_pekerjaanlain_diluar_pekerjaanutama_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, ac_pekerjaanlain_diluar_pekerjaanutama_cp, tv_error_pekerjaanlain_diluar_pekerjaanutama_cp, iv_circle_pekerjaanlain_diluar_pekerjaanutama_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_pekerjaanlain_diluar_pekerjaanutama_cp.setVisibility(View.VISIBLE);
//                tv_error_pekerjaanlain_diluar_pekerjaanutama_cp.setText(getString(R.string.error_field_required));
//                iv_circle_pekerjaanlain_diluar_pekerjaanutama_cp.setColorFilter(color);
            }
        }
        if (cl_form_data_pihak_ketiga2_cp.isShown()) {
            if (et_nama_pihak_ketiga_cp.isShown() && et_nama_pihak_ketiga_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_nama_pihak_ketiga_cp, tv_error_nama_pihak_ketiga_cp, iv_circle_nama_pihak_ketiga_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_nama_pihak_ketiga_cp.setVisibility(View.VISIBLE);
//                tv_error_nama_pihak_ketiga_cp.setText(getString(R.string.error_field_required));
//                iv_circle_nama_pihak_ketiga_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_nama_pihak_ketiga_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_nama_pihak_ketiga_cp, tv_error_nama_pihak_ketiga_cp, iv_circle_nama_pihak_ketiga_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (ac_kewarganegaraan_pihak_ketiga_cp.isShown() && ac_kewarganegaraan_pihak_ketiga_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, ac_kewarganegaraan_pihak_ketiga_cp, tv_error_kewarganegaraan_pihak_ketiga_cp, iv_circle_kewarganegaraan_pihak_ketiga_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_kewarganegaraan_pihak_ketiga_cp.setVisibility(View.VISIBLE);
//                tv_error_kewarganegaraan_pihak_ketiga_cp.setText(getString(R.string.error_field_required));
//                iv_circle_kewarganegaraan_pihak_ketiga_cp.setColorFilter(color);
            }
            if (et_alamat_pihak_ketiga_cp.isShown() && et_alamat_pihak_ketiga_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_alamat_pihak_ketiga_cp, tv_error_alamat_pihak_ketiga_cp, iv_circle_alamat_pihak_ketiga_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_alamat_pihak_ketiga_cp.setVisibility(View.VISIBLE);
//                tv_error_alamat_pihak_ketiga_cp.setText(getString(R.string.error_field_required));
//                iv_circle_alamat_pihak_ketiga_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_alamat_pihak_ketiga_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_alamat_pihak_ketiga_cp, tv_error_alamat_pihak_ketiga_cp, iv_circle_alamat_pihak_ketiga_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (et_no_telp_rumah_pihak_ketiga_cp.isShown() && et_no_telp_rumah_pihak_ketiga_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_no_telp_rumah_pihak_ketiga_cp, tv_error_no_telp_rumah_pihak_ketiga_cp, iv_circle_no_telp_rumah_pihak_ketiga_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_no_telp_rumah_pihak_ketiga_cp.setVisibility(View.VISIBLE);
//                tv_error_no_telp_rumah_pihak_ketiga_cp.setText(getString(R.string.error_field_required));
//                iv_circle_no_telp_rumah_pihak_ketiga_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_no_telp_rumah_pihak_ketiga_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_no_telp_rumah_pihak_ketiga_cp, tv_error_no_telp_rumah_pihak_ketiga_cp, iv_circle_no_telp_rumah_pihak_ketiga_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (et_no_telp_kantor_pihak_ketiga_cp.isShown() && et_no_telp_kantor_pihak_ketiga_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_no_telp_kantor_pihak_ketiga_cp, tv_error_no_telp_kantor_pihak_ketiga_cp, iv_circle_no_telp_kantor_pihak_ketiga_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_no_telp_kantor_pihak_ketiga_cp.setVisibility(View.VISIBLE);
//                tv_error_no_telp_kantor_pihak_ketiga_cp.setText(getString(R.string.error_field_required));
//                iv_circle_no_telp_kantor_pihak_ketiga_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_no_telp_kantor_pihak_ketiga_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_no_telp_kantor_pihak_ketiga_cp, tv_error_no_telp_kantor_pihak_ketiga_cp, iv_circle_no_telp_kantor_pihak_ketiga_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (et_alamat_email_pihak_ketiga_cp.isShown() && et_alamat_email_pihak_ketiga_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_alamat_email_pihak_ketiga_cp, tv_error_alamat_email_pihak_ketiga_cp, iv_circle_alamat_email_pihak_ketiga_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_alamat_email_pihak_ketiga_cp.setVisibility(View.VISIBLE);
//                tv_error_alamat_email_pihak_ketiga_cp.setText(getString(R.string.error_field_required));
//                iv_circle_alamat_email_pihak_ketiga_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.emailValidator(et_alamat_email_pihak_ketiga_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_alamat_email_pihak_ketiga_cp, tv_error_alamat_email_pihak_ketiga_cp, iv_circle_alamat_email_pihak_ketiga_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (et_tanggal_pendirian_cp.isShown() && et_tanggal_pendirian_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_tanggal_pendirian_cp, tv_error_tanggal_pendirian_cp, iv_circle_tanggal_pendirian_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_tanggal_pendirian_cp.setVisibility(View.VISIBLE);
//                tv_error_tanggal_pendirian_cp.setText(getString(R.string.error_field_required));
//                iv_circle_tanggal_pendirian_cp.setColorFilter(color);
            }
            if (et_tempat_kedudukan_pihak_ketiga_cp.isShown() && et_tempat_kedudukan_pihak_ketiga_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_tempat_kedudukan_pihak_ketiga_cp, tv_error_tanggal_pendirian_cp, iv_circle_tanggal_pendirian_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_tanggal_pendirian_cp.setVisibility(View.VISIBLE);
//                tv_error_tanggal_pendirian_cp.setText(getString(R.string.error_field_required));
//                iv_circle_tanggal_pendirian_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_tempat_kedudukan_pihak_ketiga_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_tempat_kedudukan_pihak_ketiga_cp, tv_error_tanggal_pendirian_cp, iv_circle_tanggal_pendirian_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (et_tempat_lahir_pihak_ketiga_cp.isShown() && et_tempat_lahir_pihak_ketiga_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_tempat_lahir_pihak_ketiga_cp, tv_error_tempat_lahir_pihak_ketiga_cp, iv_circle_tempat_lahir_pihak_ketiga_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_tempat_lahir_pihak_ketiga_cp.setVisibility(View.VISIBLE);
//                tv_error_tempat_lahir_pihak_ketiga_cp.setText(getString(R.string.error_field_required));
//                iv_circle_tempat_lahir_pihak_ketiga_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_tempat_lahir_pihak_ketiga_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_tempat_lahir_pihak_ketiga_cp, tv_error_tempat_lahir_pihak_ketiga_cp, iv_circle_tempat_lahir_pihak_ketiga_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (et_tanggal_lahir_cp.isShown() && et_tanggal_lahir_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_tanggal_lahir_cp, tv_error_tempat_lahir_pihak_ketiga_cp, iv_circle_tempat_lahir_pihak_ketiga_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_tempat_lahir_pihak_ketiga_cp.setVisibility(View.VISIBLE);
//                tv_error_tempat_lahir_pihak_ketiga_cp.setText(getString(R.string.error_field_required));
//                iv_circle_tempat_lahir_pihak_ketiga_cp.setColorFilter(color);
            }
            if (et_bidang_usaha_pihak_ketiga_cp.isShown() && et_bidang_usaha_pihak_ketiga_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_bidang_usaha_pihak_ketiga_cp, tv_error_bidang_usaha_pihak_ketiga_cp, iv_circle_bidang_usaha_pihak_ketiga_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_bidang_usaha_pihak_ketiga_cp.setVisibility(View.VISIBLE);
//                tv_error_bidang_usaha_pihak_ketiga_cp.setText(getString(R.string.error_field_required));
//                iv_circle_bidang_usaha_pihak_ketiga_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_bidang_usaha_pihak_ketiga_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_bidang_usaha_pihak_ketiga_cp, tv_error_bidang_usaha_pihak_ketiga_cp, iv_circle_bidang_usaha_pihak_ketiga_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (ac_jenis_pekerjaan_cp.isShown() && ac_jenis_pekerjaan_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, ac_jenis_pekerjaan_cp, tv_error_jenis_pekerjaan_cp, iv_circle_jenis_pekerjaan_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_jenis_pekerjaan_cp.setVisibility(View.VISIBLE);
//                tv_error_jenis_pekerjaan_cp.setText(getString(R.string.error_field_required));
//                iv_circle_jenis_pekerjaan_cp.setColorFilter(color);
            }
            if (ac_jabatan_pangkat_golongan_cp.isShown() && ac_jabatan_pangkat_golongan_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, ac_jabatan_pangkat_golongan_cp, tv_error_jabatan_pangkat_golongan_cp, iv_circle_jabatan_pangkat_golongan_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_jabatan_pangkat_golongan_cp.setVisibility(View.VISIBLE);
//                tv_error_jabatan_pangkat_golongan_cp.setText(getString(R.string.error_field_required));
//                iv_circle_jabatan_pangkat_golongan_cp.setColorFilter(color);
            }
            if (et_instansi_departemen_cp.isShown() && et_instansi_departemen_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_instansi_departemen_cp, tv_error_instansi_departemen_cp, iv_circle_instansi_departemen_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_instansi_departemen_cp.setVisibility(View.VISIBLE);
//                tv_error_instansi_departemen_cp.setText(getString(R.string.error_field_required));
//                iv_circle_instansi_departemen_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_instansi_departemen_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_instansi_departemen_cp, tv_error_instansi_departemen_cp, iv_circle_instansi_departemen_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (ac_hubungan_dengan_pemegang_polis_cp.isShown() && ac_hubungan_dengan_pemegang_polis_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, ac_hubungan_dengan_pemegang_polis_cp, tv_error_hubungan_dengan_pemegang_polis_cp, iv_circle_hubungan_dengan_pemegang_polis_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_hubungan_dengan_pemegang_polis_cp.setVisibility(View.VISIBLE);
//                tv_error_hubungan_dengan_pemegang_polis_cp.setText(getString(R.string.error_field_required));
//                iv_circle_hubungan_dengan_pemegang_polis_cp.setColorFilter(color);
            }
            if (ac_klasifikasi_pekerjaan_phk3_cp.isShown() && ac_klasifikasi_pekerjaan_phk3_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, ac_klasifikasi_pekerjaan_phk3_cp, tv_error_klasifikasi_pekerjaan_phk3_cp, iv_circle_klasifikasi_pekerjaan_phk3_cp, color, getContext(), getString(R.string.error_field_required));
// tv_error_klasifikasi_pekerjaan_phk3_cp.setVisibility(View.VISIBLE);
//                tv_error_klasifikasi_pekerjaan_phk3_cp.setText(getString(R.string.error_field_required));
//                iv_circle_klasifikasi_pekerjaan_phk3_cp.setColorFilter(color);            Log
            }
            if (et_sumber_dana_cp.isShown() && et_sumber_dana_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_sumber_dana_cp, tv_error_sumber_dana_cp, iv_circle_sumber_dana_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_sumber_dana_cp.setVisibility(View.VISIBLE);
//                tv_error_sumber_dana_cp.setText(getString(R.string.error_field_required));
//                iv_circle_sumber_dana_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_sumber_dana_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_sumber_dana_cp, tv_error_sumber_dana_cp, iv_circle_sumber_dana_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
            if (et_tujuan_penggunaan_dana_cp.isShown() && et_tujuan_penggunaan_dana_cp.getText().toString().trim().length() == 0) {
                val = false;
                MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_tujuan_penggunaan_dana_cp, tv_error_tujuan_penggunaan_dana_cp, iv_circle_tujuan_penggunaan_dana_cp, color, getContext(), getString(R.string.error_field_required));
//                tv_error_tujuan_penggunaan_dana_cp.setVisibility(View.VISIBLE);
//                tv_error_tujuan_penggunaan_dana_cp.setText(getString(R.string.error_field_required));
//                iv_circle_tujuan_penggunaan_dana_cp.setColorFilter(color);
            } else {
                if (!Method_Validator.ValEditRegex(et_tujuan_penggunaan_dana_cp.getText().toString())) {
                    val = false;
                    MethodSupport.onFocusview(scroll_cp, cl_child_cp, et_tujuan_penggunaan_dana_cp, tv_error_tujuan_penggunaan_dana_cp, iv_circle_tujuan_penggunaan_dana_cp, color, getContext(), getString(R.string.error_regex));
                }
            }
        }
        if (cl_sumber_penghasilan_cp.isShown()) {
            if (!bool_dana_bulan_cp) {
                val = false;
                iv_circle_gaji_cp.setColorFilter(color);
            }
//            if (!bool_dana_tahun_cp) {
//                val = false;
//                iv_circle_sumber_pendapatan_nonrutin_cp.setColorFilter(color);
//            }
            if (!bool_tujuan_cp) {
                val = false;
                iv_circle_tujuan_pengajuan_asuransi_cp.setColorFilter(color);
            }

            if (ac_jumlah_total_pendapatan_nonrutin_tahun_cp.isShown() && ac_jumlah_total_pendapatan_nonrutin_tahun_cp.getText().toString().trim().length() == 0) {
                val = false;
                tv_error_jumlah_total_pendapatan_nonrutin_tahun_cp.setVisibility(View.VISIBLE);
                tv_error_jumlah_total_pendapatan_nonrutin_tahun_cp.setText(getString(R.string.error_field_required));
                iv_circle_jumlah_total_pendapatan_nonrutin_tahun_cp.setColorFilter(color);
            }

            if (ac_jumlah_total_pendapatan_rutin_bulan_cp.isShown() && ac_jumlah_total_pendapatan_rutin_bulan_cp.getText().toString().trim().length() == 0) {
                val = false;
                tv_error_jumlah_total_pendapatan_rutin_bulan_cp.setVisibility(View.VISIBLE);
                tv_error_jumlah_total_pendapatan_rutin_bulan_cp.setText(getString(R.string.error_field_required));
                iv_circle_jumlah_total_pendapatan_rutin_bulan_cp.setColorFilter(color);
            }
        }
        return val;
    }

    private void SetAdapterPropinsi() {
        ArrayList<ModelDropdownInt> Dropdown = new E_Select(getContext()).getLst_Propinsi();
        ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_provinsi_cp.setAdapter(Adapter);
    }

    private void setAdapterJabatan() {
        ArrayList<ModelDropDownString> Dropdown = new E_Select(getContext()).getLstJabatan();
        ArrayAdapter<ModelDropDownString> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_jabatan_pangkat_golongan_cp.setAdapter(Adapter);
    }

    private void setAdapterYaTidak() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.yatidak));
        ac_pekerjaanlain_diluar_pekerjaanutama_cp.setAdapter(Adapter);
        ac_pilih_status_pihak_ketiga_cp.setAdapter(Adapter);
    }

    private void setAdapterPenghasilan() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.penghasilan_cp));
        ac_jumlah_total_pendapatan_rutin_bulan_cp.setAdapter(Adapter);
//        ac_jumlah_total_pendapatan_nonrutin_tahun_cp.setAdapter(Adapter);
    }

    private void setAdapterPenghasilanNON() {
//        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
//                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.penghasilan_cp));
//        ac_jumlah_total_pendapatan_rutin_bulan_cp.setAdapter(Adapter);
        ac_jumlah_total_pendapatan_nonrutin_tahun_cp.setText("< RP. 5 JUTA");
        str_spintotal_thn_cp = "< RP. 5 JUTA";
    }

    private void setAdapterPenghasilanNONada() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.penghasilan_cp));
//        ac_jumlah_total_pendapatan_rutin_bulan_cp.setAdapter(Adapter);
        ac_jumlah_total_pendapatan_nonrutin_tahun_cp.setAdapter(Adapter);
    }

    private void setAdapterJnsPekerjaan() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.jns_pekerjaan));
        ac_jenis_pekerjaan_cp.setAdapter(Adapter);
    }

    private void setAdapterPekerjaan(String cari_kerja, AutoCompleteTextView ac_kerja) {
        ArrayList<ModelDropdownInt> Dropdown = new E_Select(getContext()).getLst_Pekerjaan(cari_kerja);
        ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_kerja.setAdapter(Adapter);
    }

    /**
     * To determine is user selected job description is from dropdown or not
     * If user type the job description, the input will not be valid
     * User will have to select from the dropdown
     *
     * @param searchQuery
     * @param ac_kerja
     * @return
     */
    private void validateJobDescription(String searchQuery, AutoCompleteTextView ac_kerja) {
        //Check if job desc search query empty
        if (!TextUtils.isEmpty(searchQuery)) {
            ac_kerja.setError(null);
            iv_circle_klasifikasi_pekerjaan_phk3_cp.setColorFilter(ContextCompat.getColor(getActivity(), R.color.orange));

            //Search from DB
            ArrayList<ModelDropdownInt> result = new E_Select(getActivity()).getLst_Pekerjaan(searchQuery);
            if (result.isEmpty()) {
                ac_kerja.setError(getString(R.string.err_msg_jobdesc_not_found_on_the_list));
            } else {
                ac_kerja.setError(null);
                ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, result);
                ac_kerja.setAdapter(Adapter);
            }
            // To make sure user only pick job desc from dropdown so we make it false
            isJobDescriptionValid = false;

        } else {
            ac_kerja.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
            iv_circle_klasifikasi_pekerjaan_phk3_cp.setColorFilter(ContextCompat.getColor(getActivity(), R.color.orange));

            // To make sure user only pick job desc from dropdown so we make it false
            isJobDescriptionValid = false;
        }
    }

    /**
     * Check whether user previously selected job description is on the suggestion list
     * Invoked when user go to this process from the previous step
     *
     * @param selectedJobDesc
     * @param ac_kerja
     * @return
     */
    private void checkForSelectedJobDescriptionIsOnTheList(String selectedJobDesc, AutoCompleteTextView ac_kerja) {
        //Search from DB
        ArrayList<ModelDropdownInt> result = new E_Select(getActivity()).getLst_Pekerjaan(selectedJobDesc);
        if (result.isEmpty()) {
            ac_kerja.setError(getString(R.string.err_msg_jobdesc_not_found_on_the_list));
            isJobDescriptionValid = false;
        } else {
            ac_kerja.setError(null);
            iv_circle_klasifikasi_pekerjaan_phk3_cp.setColorFilter(ContextCompat.getColor(getActivity(), R.color.green));
            ArrayAdapter<ModelDropdownInt> Adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, result);
            ac_kerja.setAdapter(Adapter);
            isJobDescriptionValid = true;
        }
    }

    private void onClickLanjut() {
        String jobDescription = ac_klasifikasi_pekerjaan_phk3_cp.getText().toString().trim();
        boolean isKlasifikasiPekerjaanVisible = cl_data_pihak_ketiga_cp.isShown();

        //Only do klasifikasi pekerjaan re-validation if only the field is visible
        if (isKlasifikasiPekerjaanVisible && TextUtils.isEmpty(jobDescription)) {
            ac_klasifikasi_pekerjaan_phk3_cp.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
            ac_klasifikasi_pekerjaan_phk3_cp.requestFocus();
            return;
        }

        if (isKlasifikasiPekerjaanVisible && !isJobDescriptionValid) {
            ac_klasifikasi_pekerjaan_phk3_cp.setError(getResources().getString(R.string.err_msg_jobdesc_not_found_on_the_list));
            ac_klasifikasi_pekerjaan_phk3_cp.requestFocus();
            return;
        }

//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setTitle("Melakukan perpindahan halaman");
//        progressDialog.setMessage("Mohon Menunggu...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);

//        Runnable progressRunnable = new Runnable() {
//            @Override
//            public void run() {
        //MethodSupport.active_view(0, btn_lanjut);
        // MethodSupport.active_view(0, iv_next);

        me.getCalonPembayarPremiModel().setValidation(Validation());
        loadview();
        MyTask task = new MyTask();
        task.execute();

//        me.getCalonPembayarPremiModel().setValidation(Validation());
//        loadview();
//        ((E_MainActivity) getActivity()).onSave();
//                View cl_child_cp = getActivity().findViewById(R.id.cl_child_cp);
//                File file_bitmap = new File(
//                        getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//                if (!file_bitmap.exists()) {
//                    file_bitmap.mkdirs();
//                }

        //create JPG
//        String fileName = "CP_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
//        Context context = getActivity();
//        MethodSupport.onCreateBitmap(cl_child_cp, scroll_cp, file_bitmap, fileName, context);

//        ((E_MainActivity) getActivity()).setFragment(new E_UsulanAsuransiFragment(), getResources().getString(R.string.usulan_asuransi));
//            }
//        };

    }

    private class MyTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_CALON_PEMBAYAR_PREMI);
            return null;
        }

        @Override
        protected void onPostExecute(Void params) {
            ((E_MainActivity) getActivity()).setFragment(new E_UsulanAsuransiFragment(), getResources().getString(R.string.usulan_asuransi));
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    private class MyTaskBack extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_CALON_PEMBAYAR_PREMI);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ((E_MainActivity) getActivity()).setFragment(new E_TertanggungFragment(), getResources().getString(R.string.tertanggung));
        }
    }

    private ProgressDialog dialogmessage;

    public void startProgress() {
        dialogmessage = new ProgressDialog(getActivity());
        dialogmessage.setMessage("Mohon Menunggu ");
        dialogmessage.setTitle("Sedang Melakukan Perpindahan Halaman ...");
        dialogmessage.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialogmessage.show();
        dialogmessage.setCancelable(false);
        btn_lanjut.setEnabled(false);
        iv_next.setEnabled(false);
        iv_prev.setEnabled(false);
        btn_kembali.setEnabled(false);
        dialogmessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                btn_lanjut.setEnabled(true);
                iv_next.setEnabled(true);
                iv_prev.setEnabled(true);
                btn_kembali.setEnabled(true);
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (dialogmessage.getProgress() <= dialogmessage
                            .getMax()) {
                        Thread.sleep(10);
                        handle.sendMessage(handle.obtainMessage());
                        if (dialogmessage.getProgress() == dialogmessage
                                .getMax()) {
                            dialogmessage.dismiss();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    Handler handle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            dialogmessage.incrementProgressBy(1);
        }
    };

    private void onClickBack() {

        loadview();
        MyTaskBack taskBack = new MyTaskBack();
        taskBack.execute();
//        ((E_MainActivity) getActivity()).setFragment(new E_TertanggungFragment(), getResources().getString(R.string.tertanggung));
//        ((E_MainActivity) getActivity()).onSave();

//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setTitle("Melakukan perpindahan halaman");
//        progressDialog.setMessage("Mohon Menunggu...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
//        Runnable progressRunnable = new Runnable() {
//
//            @Override
//            public void run() {
//        MethodSupport.active_view(0, btn_kembali);
//        MethodSupport.active_view(0, iv_prev);
//        ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
//        loadview();
//        ((E_MainActivity) getActivity()).setFragment(new E_TertanggungFragment(), getResources().getString(R.string.tertanggung));
//                progressDialog.cancel();
//            }
//        };
//
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 1000);
    }


    private void val_hubcp() {
        if (int_ket_cp == 40) {
            tv_data_perusahaan_cp.setVisibility(View.GONE);
            cl_form_data_perusahaan_cp.setVisibility(View.GONE);
            cl_data_pihak_ketiga_cp.setVisibility(View.GONE);
            cl_sumber_penghasilan_cp.setVisibility(View.VISIBLE);
        } else if (int_ket_cp == 41) {
            tv_data_perusahaan_cp.setVisibility(View.VISIBLE);
            cl_form_data_perusahaan_cp.setVisibility(View.VISIBLE);
            cl_data_pihak_ketiga_cp.setVisibility(View.GONE);
            cl_sumber_penghasilan_cp.setVisibility(View.GONE);
        } else {
            tv_data_perusahaan_cp.setVisibility(View.GONE);
            cl_form_data_perusahaan_cp.setVisibility(View.GONE);
            cl_data_pihak_ketiga_cp.setVisibility(View.VISIBLE);
            cl_sumber_penghasilan_cp.setVisibility(View.VISIBLE);
        }
    }

    private void val_pihak3() {
        if (int_spinpihak3_cp == 0) {
            MethodSupport.active_view(1, et_keterangan_status_pihak_ketiga_cp);
            cl_form_data_pihak_ketiga2_cp.setVisibility(View.GONE);
            cl_form_data_pihak_ketiga3_cp.setVisibility(View.GONE);
        } else {
            MethodSupport.active_view(0, et_keterangan_status_pihak_ketiga_cp);
            cl_form_data_pihak_ketiga2_cp.setVisibility(View.VISIBLE);
            cl_form_data_pihak_ketiga3_cp.setVisibility(View.VISIBLE);
        }
    }

    private void showDateDialog(int flag_ttl, String tanggal, EditText edit) {
        switch (flag_ttl) {
            case 1:
                if (tanggal.length() != 0) {
                    cal_cp = toCalendar(tanggal);
                } else {
                    cal_cp = Calendar.getInstance();
                }
                DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                        datePickerListener, cal_cp.get(Calendar.YEAR), cal_cp.get(Calendar.MONTH), cal_cp.get(Calendar.DATE));
                dialog.getDatePicker().setMaxDate(cal_cp.getTimeInMillis());
                dialog.show();
                break;
            case 2:
                if (tanggal.length() != 0) {
                    cal_ttl_cp = toCalendar(tanggal);
                } else {
                    cal_ttl_cp = Calendar.getInstance();
                }
                DatePickerDialog dialog1 = new DatePickerDialog(getActivity(),
                        datePickerListener_lhr, cal_ttl_cp.get(Calendar.YEAR), cal_ttl_cp.get(Calendar.MONTH), cal_ttl_cp.get(Calendar.DATE));
                dialog1.getDatePicker().setMaxDate(cal_ttl_cp.getTimeInMillis());
                dialog1.show();
                break;
        }
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            cal_cp.set(Calendar.YEAR, selectedYear);
            cal_cp.set(Calendar.MONTH, selectedMonth);
            cal_cp.set(Calendar.DAY_OF_MONTH, selectedDay);
            et_tanggal_pendirian_cp.setText(StaticMethods.toStringDate(cal_cp, 5));
        }
    };

    private DatePickerDialog.OnDateSetListener datePickerListener_lhr = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            cal_ttl_cp.set(Calendar.YEAR, selectedYear);
            cal_ttl_cp.set(Calendar.MONTH, selectedMonth);
            cal_ttl_cp.set(Calendar.DAY_OF_MONTH, selectedDay);
            et_tanggal_lahir_cp.setText(StaticMethods.toStringDate(cal_ttl_cp, 5));
        }
    };


    @BindView(R.id.cl_data_perusahaan_cp)
    ConstraintLayout cl_data_perusahaan_cp;
    @BindView(R.id.cl_form_data_perusahaan_cp)
    ConstraintLayout cl_form_data_perusahaan_cp;
    @BindView(R.id.cl_nama_perusahaan_cp)
    ConstraintLayout cl_nama_perusahaan_cp;
    @BindView(R.id.cl_alamat_perusahaan_cp)
    ConstraintLayout cl_alamat_perusahaan_cp;
    @BindView(R.id.cl_provinsi_cp)
    ConstraintLayout cl_provinsi_cp;
    @BindView(R.id.cl_kode_pos_cp)
    ConstraintLayout cl_kode_pos_cp;
    @BindView(R.id.cl_nomor_telp_perusahaan_cp)
    ConstraintLayout cl_nomor_telp_perusahaan_cp;
    @BindView(R.id.cl_nomor_fax_cp)
    ConstraintLayout cl_nomor_fax_cp;
    @BindView(R.id.cl_bidang_usaha_cp)
    ConstraintLayout cl_bidang_usaha_cp;
    @BindView(R.id.cl_pekerjaanlain_diluar_pekerjaanutama_cp)
    ConstraintLayout cl_pekerjaanlain_diluar_pekerjaanutama_cp;
    @BindView(R.id.cl_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp)
    ConstraintLayout cl_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp;
    @BindView(R.id.cl_sumber_penghasilan_cp)
    ConstraintLayout cl_sumber_penghasilan_cp;
    @BindView(R.id.cl_form_sumber_penghasilan_cp)
    ConstraintLayout cl_form_sumber_penghasilan_cp;
    @BindView(R.id.cl_sumber_pendapatan_rutin_bulan_cp)
    ConstraintLayout cl_sumber_pendapatan_rutin_bulan_cp;
    @BindView(R.id.cl_sumber_pendapatan_rutin_bulan2_cp)
    ConstraintLayout cl_sumber_pendapatan_rutin_bulan2_cp;
    @BindView(R.id.cl_sumber_pendapatan_rutin_bulan3_cp)
    ConstraintLayout cl_sumber_pendapatan_rutin_bulan3_cp;
    @BindView(R.id.cl_sumber_pendapatan_rutin_bulan4_cp)
    ConstraintLayout cl_sumber_pendapatan_rutin_bulan4_cp;
    @BindView(R.id.cl_sumber_pendapatan_rutin_bulan5_cp)
    ConstraintLayout cl_sumber_pendapatan_rutin_bulan5_cp;
    @BindView(R.id.cl_sumber_pendapatan_rutin_bulan6_cp)
    ConstraintLayout cl_sumber_pendapatan_rutin_bulan6_cp;
    @BindView(R.id.cl_sumber_pendapatan_rutin_bulan7_cp)
    ConstraintLayout cl_sumber_pendapatan_rutin_bulan7_cp;
    @BindView(R.id.cl_sumber_pendapatan_rutin_bulan8_cp)
    ConstraintLayout cl_sumber_pendapatan_rutin_bulan8_cp;
    @BindView(R.id.cl_sumber_pendapatan_rutin_bulan9_cp)
    ConstraintLayout cl_sumber_pendapatan_rutin_bulan9_cp;
    @BindView(R.id.cl_sumber_pendapatan_rutin_bulan10_cp)
    ConstraintLayout cl_sumber_pendapatan_rutin_bulan10_cp;
    @BindView(R.id.cl_form_sumber_penghasilan_nonrutin_cp)
    ConstraintLayout cl_form_sumber_penghasilan_nonrutin_cp;
    @BindView(R.id.cl_sumber_pendapatan_nonrutin_cp)
    ConstraintLayout cl_sumber_pendapatan_nonrutin_cp;
    @BindView(R.id.cl_sumber_pendapatan_nonrutin2_cp)
    ConstraintLayout cl_sumber_pendapatan_nonrutin2_cp;
    @BindView(R.id.cl_sumber_pendapatan_nonrutin3_cp)
    ConstraintLayout cl_sumber_pendapatan_nonrutin3_cp;
    @BindView(R.id.cl_sumber_pendapatan_nonrutin4_cp)
    ConstraintLayout cl_sumber_pendapatan_nonrutin4_cp;
    @BindView(R.id.cl_sumber_pendapatan_nonrutin5_cp)
    ConstraintLayout cl_sumber_pendapatan_nonrutin5_cp;
    @BindView(R.id.cl_sumber_pendapatan_nonrutin6_cp)
    ConstraintLayout cl_sumber_pendapatan_nonrutin6_cp;
    @BindView(R.id.cl_sumber_pendapatan_nonrutin7_cp)
    ConstraintLayout cl_sumber_pendapatan_nonrutin7_cp;
    @BindView(R.id.cl_sumber_pendapatan_nonrutin8_cp)
    ConstraintLayout cl_sumber_pendapatan_nonrutin8_cp;
    @BindView(R.id.cl_form_total_pendapatan_cp)
    ConstraintLayout cl_form_total_pendapatan_cp;
    @BindView(R.id.cl_jumlah_total_pendapatan_rutin_bulan_cp)
    ConstraintLayout cl_jumlah_total_pendapatan_rutin_bulan_cp;
    @BindView(R.id.cl_jumlah_total_pendapatan_nonrutin_tahun_cp)
    ConstraintLayout cl_jumlah_total_pendapatan_nonrutin_tahun_cp;
    @BindView(R.id.cl_form_tujuan_pengajuan_asuransi_cp)
    ConstraintLayout cl_form_tujuan_pengajuan_asuransi_cp;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi_cp)
    ConstraintLayout cl_tujuan_pengajuan_asuransi_cp;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi2_cp)
    ConstraintLayout cl_tujuan_pengajuan_asuransi2_cp;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi3_cp)
    ConstraintLayout cl_tujuan_pengajuan_asuransi3_cp;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi4_cp)
    ConstraintLayout cl_tujuan_pengajuan_asuransi4_cp;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi5_cp)
    ConstraintLayout cl_tujuan_pengajuan_asuransi5_cp;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi6_cp)
    ConstraintLayout cl_tujuan_pengajuan_asuransi6_cp;
    @BindView(R.id.cl_tujuan_pengajuan_asuransi7_cp)
    ConstraintLayout cl_tujuan_pengajuan_asuransi7_cp;
    @BindView(R.id.cl_data_pihak_ketiga_cp)
    ConstraintLayout cl_data_pihak_ketiga_cp;
    @BindView(R.id.cl_form_data_pihak_ketiga_cp)
    ConstraintLayout cl_form_data_pihak_ketiga_cp;
    @BindView(R.id.cl_pilih_status_pihak_ketiga_cp)
    ConstraintLayout cl_pilih_status_pihak_ketiga_cp;
    @BindView(R.id.cl_keterangan_status_pihak_ketiga_cp)
    ConstraintLayout cl_keterangan_status_pihak_ketiga_cp;
    @BindView(R.id.cl_form_data_pihak_ketiga2_cp)
    ConstraintLayout cl_form_data_pihak_ketiga2_cp;
    @BindView(R.id.cl_nama_pihak_ketiga_cp)
    ConstraintLayout cl_nama_pihak_ketiga_cp;
    @BindView(R.id.cl_kewarganegaraan_pihak_ketiga_cp)
    ConstraintLayout cl_kewarganegaraan_pihak_ketiga_cp;
    @BindView(R.id.cl_alamat_pihak_ketiga_cp)
    ConstraintLayout cl_alamat_pihak_ketiga_cp;
    @BindView(R.id.cl_no_telp_rumah_pihak_ketiga_cp)
    ConstraintLayout cl_no_telp_rumah_pihak_ketiga_cp;
    @BindView(R.id.cl_no_telp_kantor_pihak_ketiga_cp)
    ConstraintLayout cl_no_telp_kantor_pihak_ketiga_cp;
    @BindView(R.id.cl_alamat_email_pihak_ketiga_cp)
    ConstraintLayout cl_alamat_email_pihak_ketiga_cp;
    @BindView(R.id.cl_tanggal_pendirian_cp)
    ConstraintLayout cl_tanggal_pendirian_cp;
    @BindView(R.id.cl_tempat_kedudukan_pihak_ketiga_cp)
    ConstraintLayout cl_tempat_kedudukan_pihak_ketiga_cp;
    @BindView(R.id.cl_tempat_lahir_pihak_ketiga_cp)
    ConstraintLayout cl_tempat_lahir_pihak_ketiga_cp;
    @BindView(R.id.cl_tanggal_lahir_cp)
    ConstraintLayout cl_tanggal_lahir_cp;
    @BindView(R.id.cl_bidang_usaha_pihak_ketiga_cp)
    ConstraintLayout cl_bidang_usaha_pihak_ketiga_cp;
    @BindView(R.id.cl_form_data_pihak_ketiga3_cp)
    ConstraintLayout cl_form_data_pihak_ketiga3_cp;
    @BindView(R.id.cl_jenis_pekerjaan_cp)
    ConstraintLayout cl_jenis_pekerjaan_cp;
    @BindView(R.id.cl_keterangan_jenis_pekerjaan_cp)
    ConstraintLayout cl_keterangan_jenis_pekerjaan_cp;
    @BindView(R.id.cl_jabatan_pangkat_golongan_cp)
    ConstraintLayout cl_jabatan_pangkat_golongan_cp;
    @BindView(R.id.cl_instansi_departemen_cp)
    ConstraintLayout cl_instansi_departemen_cp;
    @BindView(R.id.cl_npwppihakketiga_pp)
    ConstraintLayout cl_npwppihakketiga_pp;
    @BindView(R.id.cl_hubungan_dengan_pemegang_polis_cp)
    ConstraintLayout cl_hubungan_dengan_pemegang_polis_cp;
    @BindView(R.id.cl_sumber_dana_cp)
    ConstraintLayout cl_sumber_dana_cp;
    @BindView(R.id.cl_tujuan_penggunaan_dana_cp)
    ConstraintLayout cl_tujuan_penggunaan_dana_cp;
    @BindView(R.id.cl_tidak_dapat_info_dari_pihak_ketiga_cp)
    ConstraintLayout cl_tidak_dapat_info_dari_pihak_ketiga_cp;
    @BindView(R.id.cl_form_data_pihak_ketiga4_cp)
    ConstraintLayout cl_form_data_pihak_ketiga4_cp;
    @BindView(R.id.cl_kota_perusahaan_cp)
    ConstraintLayout cl_kota_perusahaan_cp;
    @BindView(R.id.cl_child_cp)
    ConstraintLayout cl_child_cp;

    @BindView(R.id.tv_data_perusahaan_cp)
    TextView tv_data_perusahaan_cp;
    @BindView(R.id.tv_error_nama_perusahaan_cp)
    TextView tv_error_nama_perusahaan_cp;
    @BindView(R.id.tv_error_alamat_perusahaan_cp)
    TextView tv_error_alamat_perusahaan_cp;
    @BindView(R.id.tv_error_provinsi_cp)
    TextView tv_error_provinsi_cp;
    @BindView(R.id.tv_error_kode_pos_cp)
    TextView tv_error_kode_pos_cp;
    @BindView(R.id.tv_error_nomor_telp_perusahaan_cp)
    TextView tv_error_nomor_telp_perusahaan_cp;
    @BindView(R.id.tv_error_nomor_fax_cp)
    TextView tv_error_nomor_fax_cp;
    @BindView(R.id.tv_error_bidang_usaha_cp)
    TextView tv_error_bidang_usaha_cp;
    @BindView(R.id.tv_error_pekerjaanlain_diluar_pekerjaanutama_cp)
    TextView tv_error_pekerjaanlain_diluar_pekerjaanutama_cp;
    @BindView(R.id.tv_error_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp)
    TextView tv_error_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp;
    @BindView(R.id.tv_sumber_penghasilan_cp)
    TextView tv_sumber_penghasilan_cp;
    @BindView(R.id.tv_sumber_pendapatan_rutin_bulan_cp)
    TextView tv_sumber_pendapatan_rutin_bulan_cp;
    @BindView(R.id.tv_sumber_pendapatan_nonrutin_cp)
    TextView tv_sumber_pendapatan_nonrutin_cp;
    @BindView(R.id.tv_error_jumlah_total_pendapatan_rutin_bulan_cp)
    TextView tv_error_jumlah_total_pendapatan_rutin_bulan_cp;
    @BindView(R.id.tv_error_jumlah_total_pendapatan_nonrutin_tahun_cp)
    TextView tv_error_jumlah_total_pendapatan_nonrutin_tahun_cp;
    @BindView(R.id.tv_info_jumlah_total_pendapatan_nonrutin_tahun_cp)
    TextView tv_info_jumlah_total_pendapatan_nonrutin_tahun_cp;
    @BindView(R.id.tv_tujuan_pengajuan_asuransi_cp)
    TextView tv_tujuan_pengajuan_asuransi_cp;
    @BindView(R.id.tv_data_pihak_ketiga_cp)
    TextView tv_data_pihak_ketiga_cp;
    @BindView(R.id.tv_status_pihak_ketiga_cp)
    TextView tv_status_pihak_ketiga_cp;
    @BindView(R.id.tv_error_pilih_status_pihak_ketiga_cp)
    TextView tv_error_pilih_status_pihak_ketiga_cp;
    @BindView(R.id.tv_error_keterangan_status_pihak_ketiga_cp)
    TextView tv_error_keterangan_status_pihak_ketiga_cp;
    @BindView(R.id.tv_error_nama_pihak_ketiga_cp)
    TextView tv_error_nama_pihak_ketiga_cp;
    @BindView(R.id.tv_error_kewarganegaraan_pihak_ketiga_cp)
    TextView tv_error_kewarganegaraan_pihak_ketiga_cp;
    @BindView(R.id.tv_error_alamat_pihak_ketiga_cp)
    TextView tv_error_alamat_pihak_ketiga_cp;
    @BindView(R.id.tv_error_no_telp_rumah_pihak_ketiga_cp)
    TextView tv_error_no_telp_rumah_pihak_ketiga_cp;
    @BindView(R.id.tv_error_no_telp_kantor_pihak_ketiga_cp)
    TextView tv_error_no_telp_kantor_pihak_ketiga_cp;
    @BindView(R.id.tv_error_alamat_email_pihak_ketiga_cp)
    TextView tv_error_alamat_email_pihak_ketiga_cp;
    @BindView(R.id.tv_error_tanggal_pendirian_cp)
    TextView tv_error_tanggal_pendirian_cp;
    @BindView(R.id.tv_error_tempat_kedudukan_pihak_ketiga_cp)
    TextView tv_error_tempat_kedudukan_pihak_ketiga_cp;
    @BindView(R.id.tv_error_tempat_lahir_pihak_ketiga_cp)
    TextView tv_error_tempat_lahir_pihak_ketiga_cp;
    @BindView(R.id.tv_error_tanggal_lahir_cp)
    TextView tv_error_tanggal_lahir_cp;
    @BindView(R.id.tv_error_pekerjaan_pihak_ketiga_cp)
    TextView tv_error_pekerjaan_pihak_ketiga_cp;
    @BindView(R.id.tv_error_bidang_usaha_pihak_ketiga_cp)
    TextView tv_error_bidang_usaha_pihak_ketiga_cp;
    @BindView(R.id.tv_error_jenis_pekerjaan_cp)
    TextView tv_error_jenis_pekerjaan_cp;
    @BindView(R.id.tv_error_keterangan_jenis_pekerjaan_cp)
    TextView tv_error_keterangan_jenis_pekerjaan_cp;
    @BindView(R.id.tv_error_jabatan_pangkat_golongan_cp)
    TextView tv_error_jabatan_pangkat_golongan_cp;
    @BindView(R.id.tv_error_instansi_departemen_cp)
    TextView tv_error_instansi_departemen_cp;
    @BindView(R.id.tv_error_npwppihakketiga_pp)
    TextView tv_error_npwppihakketiga_pp;
    @BindView(R.id.tv_error_hubungan_dengan_pemegang_polis_cp)
    TextView tv_error_hubungan_dengan_pemegang_polis_cp;
    @BindView(R.id.tv_error_sumber_dana_cp)
    TextView tv_error_sumber_dana_cp;
    @BindView(R.id.tv_error_tujuan_penggunaan_dana_cp)
    TextView tv_error_tujuan_penggunaan_dana_cp;
    @BindView(R.id.tv_error_tidak_dapat_info_dari_pihak_ketiga_cp)
    TextView tv_error_tidak_dapat_info_dari_pihak_ketiga_cp;
    @BindView(R.id.tv_error_kota_perusahaan_cp)
    TextView tv_error_kota_perusahaan_cp;
    @BindView(R.id.tv_error_calon_pemegang_premi_cp)
    TextView tv_error_calon_pemegang_premi_cp;
    @BindView(R.id.tv_error_klasifikasi_pekerjaan_phk3_cp)
    TextView tv_error_klasifikasi_pekerjaan_phk3_cp;

    @BindView(R.id.iv_circle_calon_pemegang_premi_cp)
    ImageView iv_circle_calon_pemegang_premi_cp;
    @BindView(R.id.iv_circle_nama_perusahaan_cp)
    ImageView iv_circle_nama_perusahaan_cp;
    @BindView(R.id.iv_circle_alamat_perusahaan_cp)
    ImageView iv_circle_alamat_perusahaan_cp;
    @BindView(R.id.iv_circle_provinsi_cp)
    ImageView iv_circle_provinsi_cp;
    @BindView(R.id.iv_circle_kode_pos_cp)
    ImageView iv_circle_kode_pos_cp;
    @BindView(R.id.iv_circle_nomor_telp_perusahaan_cp)
    ImageView iv_circle_nomor_telp_perusahaan_cp;
    @BindView(R.id.iv_circle_bidang_usaha_cp)
    ImageView iv_circle_bidang_usaha_cp;
    @BindView(R.id.iv_circle_pekerjaanlain_diluar_pekerjaanutama_cp)
    ImageView iv_circle_pekerjaanlain_diluar_pekerjaanutama_cp;
    @BindView(R.id.iv_circle_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp)
    ImageView iv_circle_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp;
    @BindView(R.id.iv_circle_gaji_cp)
    ImageView iv_circle_gaji_cp;
    @BindView(R.id.iv_circle_laba_perusahaan_cp)
    ImageView iv_circle_laba_perusahaan_cp;
    @BindView(R.id.iv_circle_hasil_investasi_cp)
    ImageView iv_circle_hasil_investasi_cp;
    @BindView(R.id.iv_circle_lainnya_cp)
    ImageView iv_circle_lainnya_cp;
    @BindView(R.id.iv_circle_penghasil_suami_istri_cp)
    ImageView iv_circle_penghasil_suami_istri_cp;
    @BindView(R.id.iv_circle_hasil_usaha_cp)
    ImageView iv_circle_hasil_usaha_cp;
    @BindView(R.id.iv_circle_sebutkan1_cp)
    ImageView iv_circle_sebutkan1_cp;
    @BindView(R.id.iv_circle_sebutkan2_cp)
    ImageView iv_circle_sebutkan2_cp;
    @BindView(R.id.iv_circle_orangtua_cp)
    ImageView iv_circle_orangtua_cp;
    @BindView(R.id.iv_circle_sebutkan3_cp)
    ImageView iv_circle_sebutkan3_cp;
    @BindView(R.id.iv_circle_sumber_pendapatan_nonrutin_cp)
    ImageView iv_circle_sumber_pendapatan_nonrutin_cp;
    @BindView(R.id.iv_circle_sumber_pendapatan_nonrutin2_cp)
    ImageView iv_circle_sumber_pendapatan_nonrutin2_cp;
    @BindView(R.id.iv_circle_sumber_pendapatan_nonrutin3_cp)
    ImageView iv_circle_sumber_pendapatan_nonrutin3_cp;
    @BindView(R.id.iv_circle_sumber_pendapatan_nonrutin4_cp)
    ImageView iv_circle_sumber_pendapatan_nonrutin4_cp;
    @BindView(R.id.iv_circle_sumber_pendapatan_nonrutin5_cp)
    ImageView iv_circle_sumber_pendapatan_nonrutin5_cp;
    @BindView(R.id.iv_circle_sumber_pendapatan_nonrutin6_cp)
    ImageView iv_circle_sumber_pendapatan_nonrutin6_cp;
    @BindView(R.id.iv_circle_sumber_pendapatan_nonrutin7_cp)
    ImageView iv_circle_sumber_pendapatan_nonrutin7_cp;
    @BindView(R.id.iv_circle_sumber_pendapatan_nonrutin8_cp)
    ImageView iv_circle_sumber_pendapatan_nonrutin8_cp;
    @BindView(R.id.iv_circle_jumlah_total_pendapatan_rutin_bulan_cp)
    ImageView iv_circle_jumlah_total_pendapatan_rutin_bulan_cp;
    @BindView(R.id.iv_circle_jumlah_total_pendapatan_nonrutin_tahun_cp)
    ImageView iv_circle_jumlah_total_pendapatan_nonrutin_tahun_cp;
    @BindView(R.id.iv_circle_tujuan_pengajuan_asuransi_cp)
    ImageView iv_circle_tujuan_pengajuan_asuransi_cp;
    @BindView(R.id.iv_circle_tujuan_pengajuan_asuransi2_cp)
    ImageView iv_circle_tujuan_pengajuan_asuransi2_cp;
    @BindView(R.id.iv_circle_tujuan_pengajuan_asuransi3_cp)
    ImageView iv_circle_tujuan_pengajuan_asuransi3_cp;
    @BindView(R.id.iv_circle_tujuan_pengajuan_asuransi4_cp)
    ImageView iv_circle_tujuan_pengajuan_asuransi4_cp;
    @BindView(R.id.iv_circle_tujuan_pengajuan_asuransi5_cp)
    ImageView iv_circle_tujuan_pengajuan_asuransi5_cp;
    @BindView(R.id.iv_circle_tujuan_pengajuan_asuransi6_cp)
    ImageView iv_circle_tujuan_pengajuan_asuransi6_cp;
    @BindView(R.id.iv_circle_tujuan_pengajuan_asuransi7_cp)
    ImageView iv_circle_tujuan_pengajuan_asuransi7_cp;
    @BindView(R.id.iv_circle_pilih_status_pihak_ketiga_cp)
    ImageView iv_circle_pilih_status_pihak_ketiga_cp;
    @BindView(R.id.iv_circle_keterangan_status_pihak_ketiga_cp)
    ImageView iv_circle_keterangan_status_pihak_ketiga_cp;
    @BindView(R.id.iv_circle_nama_pihak_ketiga_cp)
    ImageView iv_circle_nama_pihak_ketiga_cp;
    @BindView(R.id.iv_circle_kewarganegaraan_pihak_ketiga_cp)
    ImageView iv_circle_kewarganegaraan_pihak_ketiga_cp;
    @BindView(R.id.iv_circle_alamat_pihak_ketiga_cp)
    ImageView iv_circle_alamat_pihak_ketiga_cp;
    @BindView(R.id.iv_circle_no_telp_rumah_pihak_ketiga_cp)
    ImageView iv_circle_no_telp_rumah_pihak_ketiga_cp;
    @BindView(R.id.iv_circle_no_telp_kantor_pihak_ketiga_cp)
    ImageView iv_circle_no_telp_kantor_pihak_ketiga_cp;
    @BindView(R.id.iv_circle_alamat_email_pihak_ketiga_cp)
    ImageView iv_circle_alamat_email_pihak_ketiga_cp;
    @BindView(R.id.iv_circle_tanggal_pendirian_cp)
    ImageView iv_circle_tanggal_pendirian_cp;
    @BindView(R.id.iv_circle_tempat_kedudukan_pihak_ketiga_cp)
    ImageView iv_circle_tempat_kedudukan_pihak_ketiga_cp;
    @BindView(R.id.iv_circle_tempat_lahir_pihak_ketiga_cp)
    ImageView iv_circle_tempat_lahir_pihak_ketiga_cp;
    @BindView(R.id.iv_circle_tanggal_lahir_cp)
    ImageView iv_circle_tanggal_lahir_cp;
    @BindView(R.id.iv_circle_bidang_usaha_pihak_ketiga_cp)
    ImageView iv_circle_bidang_usaha_pihak_ketiga_cp;
    @BindView(R.id.iv_circle_jenis_pekerjaan_cp)
    ImageView iv_circle_jenis_pekerjaan_cp;
    @BindView(R.id.iv_circle_keterangan_jenis_pekerjaan_cp)
    ImageView iv_circle_keterangan_jenis_pekerjaan_cp;
    @BindView(R.id.iv_circle_jabatan_pangkat_golongan_cp)
    ImageView iv_circle_jabatan_pangkat_golongan_cp;
    @BindView(R.id.iv_circle_instansi_departemen_cp)
    ImageView iv_circle_instansi_departemen_cp;
    @BindView(R.id.iv_circle_npwppihakketiga_pp)
    ImageView iv_circle_npwppihakketiga_pp;
    @BindView(R.id.iv_circle_hubungan_dengan_pemegang_polis_cp)
    ImageView iv_circle_hubungan_dengan_pemegang_polis_cp;
    @BindView(R.id.iv_circle_sumber_dana_cp)
    ImageView iv_circle_sumber_dana_cp;
    @BindView(R.id.iv_circle_tujuan_penggunaan_dana_cp)
    ImageView iv_circle_tujuan_penggunaan_dana_cp;
    @BindView(R.id.iv_circle_kota_perusahaan_cp)
    ImageView iv_circle_kota_perusahaan_cp;
    @BindView(R.id.iv_circle_klasifikasi_pekerjaan_phk3_cp)
    ImageView iv_circle_klasifikasi_pekerjaan_phk3_cp;

    @BindView(R.id.et_nama_perusahaan_cp)
    EditText et_nama_perusahaan_cp;
    @BindView(R.id.et_alamat_perusahaan_cp)
    EditText et_alamat_perusahaan_cp;
    @BindView(R.id.et_kode_pos_cp)
    EditText et_kode_pos_cp;
    @BindView(R.id.et_nomor_telp_perusahaan_cp)
    EditText et_nomor_telp_perusahaan_cp;
    @BindView(R.id.et_nomor_fax_cp)
    EditText et_nomor_fax_cp;
    @BindView(R.id.et_bidang_usaha_cp)
    EditText et_bidang_usaha_cp;
    @BindView(R.id.et_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp)
    EditText et_pekerjaanlain_diluar_pekerjaanutama_keterangan_cp;
    @BindView(R.id.et_sebutkan1_cp)
    EditText et_sebutkan1_cp;
    @BindView(R.id.et_sebutkan2_cp)
    EditText et_sebutkan2_cp;
    @BindView(R.id.et_sebutkan3_cp)
    EditText et_sebutkan3_cp;
    @BindView(R.id.et_sebutkan4_cp)
    EditText et_sebutkan4_cp;
    @BindView(R.id.et_sebutkan5_cp)
    EditText et_sebutkan5_cp;
    @BindView(R.id.et_sebutkan6_cp)
    EditText et_sebutkan6_cp;
    @BindView(R.id.et_keterangan_status_pihak_ketiga_cp)
    EditText et_keterangan_status_pihak_ketiga_cp;
    @BindView(R.id.et_nama_pihak_ketiga_cp)
    EditText et_nama_pihak_ketiga_cp;
    @BindView(R.id.et_alamat_pihak_ketiga_cp)
    EditText et_alamat_pihak_ketiga_cp;
    @BindView(R.id.et_no_telp_rumah_pihak_ketiga_cp)
    EditText et_no_telp_rumah_pihak_ketiga_cp;
    @BindView(R.id.et_no_telp_kantor_pihak_ketiga_cp)
    EditText et_no_telp_kantor_pihak_ketiga_cp;
    @BindView(R.id.et_alamat_email_pihak_ketiga_cp)
    EditText et_alamat_email_pihak_ketiga_cp;
    @BindView(R.id.et_tanggal_pendirian_cp)
    EditText et_tanggal_pendirian_cp;
    @BindView(R.id.et_tempat_kedudukan_pihak_ketiga_cp)
    EditText et_tempat_kedudukan_pihak_ketiga_cp;
    @BindView(R.id.et_tempat_lahir_pihak_ketiga_cp)
    EditText et_tempat_lahir_pihak_ketiga_cp;
    @BindView(R.id.et_tanggal_lahir_cp)
    EditText et_tanggal_lahir_cp;
    @BindView(R.id.et_bidang_usaha_pihak_ketiga_cp)
    EditText et_bidang_usaha_pihak_ketiga_cp;
    @BindView(R.id.et_keterangan_jenis_pekerjaan_cp)
    EditText et_keterangan_jenis_pekerjaan_cp;
    @BindView(R.id.et_instansi_departemen_cp)
    EditText et_instansi_departemen_cp;
    @BindView(R.id.et_npwppihakketiga_pp)
    EditText et_npwppihakketiga_pp;
    @BindView(R.id.et_sumber_dana_cp)
    EditText et_sumber_dana_cp;
    @BindView(R.id.et_tujuan_penggunaan_dana_cp)
    EditText et_tujuan_penggunaan_dana_cp;
    @BindView(R.id.et_tidak_dapat_info_dari_pihak_ketiga_cp)
    EditText et_tidak_dapat_info_dari_pihak_ketiga_cp;
    @BindView(R.id.et_kota_perusahaan_cp)
    EditText et_kota_perusahaan_cp;

    @BindView(R.id.ac_calon_pemegang_premi_cp)
    AutoCompleteTextView ac_calon_pemegang_premi_cp;
    @BindView(R.id.ac_provinsi_cp)
    AutoCompleteTextView ac_provinsi_cp;
    @BindView(R.id.ac_pekerjaanlain_diluar_pekerjaanutama_cp)
    AutoCompleteTextView ac_pekerjaanlain_diluar_pekerjaanutama_cp;
    @BindView(R.id.ac_jumlah_total_pendapatan_rutin_bulan_cp)
    AutoCompleteTextView ac_jumlah_total_pendapatan_rutin_bulan_cp;
    @BindView(R.id.ac_jumlah_total_pendapatan_nonrutin_tahun_cp)
    AutoCompleteTextView ac_jumlah_total_pendapatan_nonrutin_tahun_cp;
    @BindView(R.id.ac_pilih_status_pihak_ketiga_cp)
    AutoCompleteTextView ac_pilih_status_pihak_ketiga_cp;
    @BindView(R.id.ac_kewarganegaraan_pihak_ketiga_cp)
    AutoCompleteTextView ac_kewarganegaraan_pihak_ketiga_cp;
    @BindView(R.id.ac_jenis_pekerjaan_cp)
    AutoCompleteTextView ac_jenis_pekerjaan_cp;
    @BindView(R.id.ac_jabatan_pangkat_golongan_cp)
    AutoCompleteTextView ac_jabatan_pangkat_golongan_cp;
    @BindView(R.id.ac_hubungan_dengan_pemegang_polis_cp)
    AutoCompleteTextView ac_hubungan_dengan_pemegang_polis_cp;
    @BindView(R.id.ac_klasifikasi_pekerjaan_phk3_cp)
    AutoCompleteTextView ac_klasifikasi_pekerjaan_phk3_cp;

    @BindView(R.id.cb_gaji_cp)
    CheckBox cb_gaji_cp;
    @BindView(R.id.cb_laba_perusahaan_cp)
    CheckBox cb_laba_perusahaan_cp;
    @BindView(R.id.cb_hasil_investasi_cp)
    CheckBox cb_hasil_investasi_cp;
    @BindView(R.id.cb_lainnya_cp)
    CheckBox cb_lainnya_cp;
    @BindView(R.id.cb_penghasil_suami_istri_cp)
    CheckBox cb_penghasil_suami_istri_cp;
    @BindView(R.id.cb_hasil_usaha_cp)
    CheckBox cb_hasil_usaha_cp;
    @BindView(R.id.cb_orangtua_cp)
    CheckBox cb_orangtua_cp;
    @BindView(R.id.cb_bonus)
    CheckBox cb_bonus;
    @BindView(R.id.cb_hadiah_warisan)
    CheckBox cb_hadiah_warisan;
    @BindView(R.id.cb_lainnya2_cp)
    CheckBox cb_lainnya2_cp;
    @BindView(R.id.cb_komisi)
    CheckBox cb_komisi;
    @BindView(R.id.cb_hasil_investasi2_cp)
    CheckBox cb_hasil_investasi2_cp;
    @BindView(R.id.cb_penjualan_aset)
    CheckBox cb_penjualan_aset;
    @BindView(R.id.cb_proteksi)
    CheckBox cb_proteksi;
    @BindView(R.id.cb_tabungan_deposito)
    CheckBox cb_tabungan_deposito;
    @BindView(R.id.cb_lainnya3_cp)
    CheckBox cb_lainnya3_cp;
    @BindView(R.id.cb_pendidikan_cp)
    CheckBox cb_pendidikan_cp;
    @BindView(R.id.cb_dana_pensiun)
    CheckBox cb_dana_pensiun;
    @BindView(R.id.cb_investasi)
    CheckBox cb_investasi;
    @BindView(R.id.cl_klasifikasi_pekerjaan_phk3_cp)
    ConstraintLayout cl_klasifikasi_pekerjaan_phk3_cp;

    @BindView(R.id.scroll_cp)
    ScrollView scroll_cp;

    private Button btn_lanjut, btn_kembali;
    private ImageView iv_save, iv_next, iv_prev;
    private android.support.v7.widget.Toolbar second_toolbar;

    View view;
    private EspajModel me;

    private ArrayList<CheckBox> check_dana_bulan_cp;
    private ArrayList<CheckBox> check_dana_tahun_cp;
    private ArrayList<CheckBox> check_tujuan_cp;
    private ArrayList<ModelDropdownInt> ListDropDownSPAJ;
    private ArrayList<ModelDropDownString> ListDropDownString;
    private ArrayAdapter<String> Adapter;

    private boolean bool_dana_bulan_cp = false;
    private boolean bool_dana_tahun_cp = false;
    private boolean bool_tujuan_cp = false;

    int int_gaji_cp = 0;
    String str_gaji_cp = "";
    int int_penghsl_cp = 0;
    String str_penghsl_cp = "";
    int int_ortu_cp = 0;
    String str_ortu_cp = "";
    int int_laba_cp = 0;
    String str_laba_cp = "";
    int int_hslusaha_cp = 0;
    String str_hslusaha_cp = "";
    int int_hslinves_cp = 0;
    String str_hslinves_cp = "";
    int int_lainnya_cp = 0;
    String str_lainnya_cp = "";
    //bernard
    int int_bonus_cp = 0;
    String str_bonus_cp = "";
    int int_komisi_cp = 0;
    String str_komisi_cp = "";
    int int_aset_cp = 0;
    String str_aset_cp = "";
    int int_hadiah_cp = 0;
    String str_hadiah_cp = "";
    int int_hslinves_thn_cp = 0;
    String str_hslinves_thn_cp = "";
    int int_lainnya_thn_cp = 0;
    String str_lainnya_thn_cp = "";
    int int_proteksi_cp = 0;
    String str_proteksi_cp = "";
    int int_pend_cp = 0;
    String str_pend_cp = "";
    int int_inves_cp = 0;
    String str_inves_cp = "";
    int int_tabungan_cp = 0;
    String str_tabungan_cp = "";
    int int_pensiun_cp = 0;
    String str_pensiun_cp = "";
    int int_lainnya_tujuan_cp = 0;
    String str_lainnya_tujuan_cp = "";
    //Spinner int
    int int_ket_cp = 0;
    int int_propinsi_cp = 0;
    int int_spinbisnis_cp = 0;
    String str_spintotal_bln_cp = "";
    String str_spintotal_thn_cp = "";
    int int_spinpihak3_cp = 0;
    int int_spin_kewrgn_cp = 0;
    String str_spin_pekerjaan_cp = "";
    String str_spin_jns_pkrjaan_cp = "";
    int int_spin_hub_dgppphk3_cp = 0;
    String str_spin_jabatanphk3_cp = "";
    int int_spin_pekerjaan_cp = 0;
    String file = "";

    private Calendar cal_cp, cal_ttl_cp;
    private File pdfFile;

    private ProgressDialog progressDialog;
}
