package id.co.ajsmsig.nb.leader.summarybc.datasource.factory;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import id.co.ajsmsig.nb.leader.summarybc.datasource.SummaryDataSource;

public class SummaryDataSourceFactory extends DataSource.Factory {

    private MutableLiveData<SummaryDataSource> mutableLiveData;
    private SummaryDataSource summaryDataSource;
    private String agentCode;
    private String channelBank;

    public SummaryDataSourceFactory(String agentCode, String channelBank) {
        this.channelBank = channelBank;
        this.agentCode = agentCode;
        this.mutableLiveData = new MutableLiveData<>();
    }

    @Override
    public DataSource create() {
        summaryDataSource = new SummaryDataSource(agentCode, channelBank);
        mutableLiveData.postValue(summaryDataSource);
        return summaryDataSource;
    }

    public MutableLiveData<SummaryDataSource> getMutableLiveData() {
        return mutableLiveData;
    }

}

