
package id.co.ajsmsig.nb.leader.subordinatedetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostCount {

    @SerializedName("request_code")
    @Expose
    private int requestCode;
    @SerializedName("msag_id")
    @Expose
    private String msagId;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PostCount() {
    }

    /**
     * 
     * @param requestCode
     * @param msagId
     */
    public PostCount(int requestCode, String msagId) {
        super();
        this.requestCode = requestCode;
        this.msagId = msagId;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(int requestCode) {
        this.requestCode = requestCode;
    }

    public String getMsagId() {
        return msagId;
    }

    public void setMsagId(String msagId) {
        this.msagId = msagId;
    }

}
