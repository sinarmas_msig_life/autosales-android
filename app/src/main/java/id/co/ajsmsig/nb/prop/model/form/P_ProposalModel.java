package id.co.ajsmsig.nb.prop.model.form;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by faiz_f on 30/03/2017.
 */

public class P_ProposalModel implements Parcelable{
    private P_MstDataProposalModel mst_data_proposal;
    private P_MstProposalProductModel mst_proposal_product;
    private ArrayList<P_MstProposalProductUlinkModel> mst_proposal_product_ulink;
    private ArrayList<P_MstProposalProductRiderModel> mst_proposal_product_rider;
    private ArrayList<P_MstProposalProductPesertaModel> mst_proposal_product_peserta;
    private ArrayList<P_MstProposalProductTopUpModel> mst_proposal_product_topup;

    public P_ProposalModel() {
    }

    public P_ProposalModel(Parcel in) {
        mst_data_proposal = in.readParcelable(P_MstDataProposalModel.class.getClassLoader());
        mst_proposal_product = in.readParcelable(P_MstProposalProductModel.class.getClassLoader());
        mst_proposal_product_ulink = in.createTypedArrayList(P_MstProposalProductUlinkModel.CREATOR);
        mst_proposal_product_rider = in.createTypedArrayList(P_MstProposalProductRiderModel.CREATOR);
        mst_proposal_product_peserta = in.createTypedArrayList(P_MstProposalProductPesertaModel.CREATOR);
        mst_proposal_product_topup = in.createTypedArrayList(P_MstProposalProductTopUpModel.CREATOR);
    }

    public static final Creator<P_ProposalModel> CREATOR = new Creator<P_ProposalModel>() {
        @Override
        public P_ProposalModel createFromParcel(Parcel in) {
            return new P_ProposalModel(in);
        }

        @Override
        public P_ProposalModel[] newArray(int size) {
            return new P_ProposalModel[size];
        }
    };

    public P_MstDataProposalModel getMst_data_proposal() {
        return mst_data_proposal;
    }

    public void setMst_data_proposal(P_MstDataProposalModel mst_data_proposal) {
        this.mst_data_proposal = mst_data_proposal;
    }



    public void setMst_proposal_product(P_MstProposalProductModel mst_proposal_product) {
        this.mst_proposal_product = mst_proposal_product;
    }

    public P_MstProposalProductModel getMst_proposal_product() {
        return mst_proposal_product;
    }

    public ArrayList<P_MstProposalProductUlinkModel> getMst_proposal_product_ulink() {
        return mst_proposal_product_ulink;
    }

    public void setMst_proposal_product_ulink(ArrayList<P_MstProposalProductUlinkModel> mst_proposal_product_ulink) {
        this.mst_proposal_product_ulink = mst_proposal_product_ulink;
    }

    public ArrayList<P_MstProposalProductRiderModel> getMst_proposal_product_rider() {
        return mst_proposal_product_rider;
    }

    public void setMst_proposal_product_rider(ArrayList<P_MstProposalProductRiderModel> mst_proposal_product_rider) {
        this.mst_proposal_product_rider = mst_proposal_product_rider;
    }

    public ArrayList<P_MstProposalProductPesertaModel> getMst_proposal_product_peserta() {
        return mst_proposal_product_peserta;
    }

    public void setMst_proposal_product_peserta(ArrayList<P_MstProposalProductPesertaModel> mst_proposal_product_peserta) {
        this.mst_proposal_product_peserta = mst_proposal_product_peserta;
    }

    public ArrayList<P_MstProposalProductTopUpModel> getMst_proposal_product_topup() {
        return mst_proposal_product_topup;
    }

    public void setMst_proposal_product_topup(ArrayList<P_MstProposalProductTopUpModel> mst_proposal_product_topup) {
        this.mst_proposal_product_topup = mst_proposal_product_topup;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mst_data_proposal, flags);
        dest.writeParcelable(mst_proposal_product, flags);
        dest.writeTypedList(mst_proposal_product_ulink);
        dest.writeTypedList(mst_proposal_product_rider);
        dest.writeTypedList(mst_proposal_product_peserta);
        dest.writeTypedList(mst_proposal_product_topup);
    }
}
