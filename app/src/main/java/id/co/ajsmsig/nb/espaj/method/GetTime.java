/**
 * @author Eriza Siti Mulyani
 */
package id.co.ajsmsig.nb.espaj.method;

import android.app.Activity;
import android.provider.Settings;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class GetTime {

    public static String getCurrentDate(String strDateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd.kk.mm.ss.SSS");
//		formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        formatter.setLenient(false);
        Date now = new Date();
        String currentDate = formatter.format(now);
        return currentDate;
    }

    public static String getSetCurentime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd.kk.mm.ss.SSS");
//		formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        formatter.setLenient(false);
        Date now = new Date();
        String currentDate = formatter.format(now);
        return currentDate;
    }

    public static String getTimeUpdate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        formatter.setLenient(false);
        Date now = new Date();
        String currentDate = formatter.format(now);
        return currentDate;
    }

    public static String getHTTPProxy(Activity actPoint) {
        return Settings.Secure.getString(actPoint.getApplicationContext().getContentResolver(), Settings.Secure.HTTP_PROXY);
    }

    public static String getCurrentDate() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        Calendar cal = Calendar.getInstance();

        return sdf.format(cal.getTime());
    }

    public static String getEndDate(String tgl_awal, int tahun) {
        Calendar idt_beg_date = Calendar.getInstance();
        Calendar idt_end_date = Calendar.getInstance();
        Calendar adt_bdate = Calendar.getInstance();
        Calendar ldt_end = Calendar.getInstance();
        int bulan = 0;

        String[] separated = tgl_awal.split("/");
        int tgl = Integer.parseInt(separated[0]);
        int bln = Integer.parseInt(separated[1]);
        int thn = Integer.parseInt(separated[2]);

        bulan = tahun * 12;

        adt_bdate.set(thn, bln - 1, tgl);
        idt_beg_date.set(thn, bln - 1, tgl);
        ldt_end.set(thn, bln - 1, tgl);
        ldt_end.add(Calendar.MONTH, bulan);

        idt_end_date.set(thn, bln - 1, tgl);
        idt_end_date.add(Calendar.MONTH, bulan);
        idt_end_date.add(Calendar.DAY_OF_MONTH, -1);

        int tanggalEd = Integer.valueOf(idt_end_date.getTime().getDate());
        int bulanEd = Integer.valueOf(idt_end_date.getTime().getMonth() + 1);
        int tahunEd = Integer.valueOf(idt_end_date.getTime().getYear() + 1900);

        String tgl_end = tanggalEd + "/" + bulanEd + "/" +
                tahunEd;

        return tgl_end;
    }
}
