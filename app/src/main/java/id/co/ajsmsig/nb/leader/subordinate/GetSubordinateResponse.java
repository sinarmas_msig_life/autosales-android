
package id.co.ajsmsig.nb.leader.subordinate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetSubordinateResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("data")
    @Expose
    private DataSubordinate dataSubordinate;

    /**
     * No args constructor for use in serialization
     * 
     */
    public GetSubordinateResponse() {
    }

    /**
     * 
     * @param message
     * @param error
     * @param dataSubordinate
     */
    public GetSubordinateResponse(String message, boolean error, DataSubordinate dataSubordinate) {
        super();
        this.message = message;
        this.error = error;
        this.dataSubordinate = dataSubordinate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public DataSubordinate getDataSubordinate() {
        return dataSubordinate;
    }

    public void setDataSubordinate(DataSubordinate dataSubordinate) {
        this.dataSubordinate = dataSubordinate;
    }

}
