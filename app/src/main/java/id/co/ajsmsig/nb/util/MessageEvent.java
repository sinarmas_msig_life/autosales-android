package id.co.ajsmsig.nb.util;

public class MessageEvent {
    private String productId;
    private String productName;
    private boolean shouldDisableProductReselect;

    public MessageEvent(String productId, String productName) {
        this.productId = productId;
        this.productName = productName;
    }
    public MessageEvent(boolean shouldDisableProductReselect) {
        this.shouldDisableProductReselect = shouldDisableProductReselect;
    }

    public String getProductId() {
        return productId;
    }
    public String getProductName() {
        return productName;
    }
    public boolean isShouldDisableProductReselect() {
        return shouldDisableProductReselect;
    }
}
