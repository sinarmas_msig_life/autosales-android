package id.co.ajsmsig.nb.espaj.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAddRiderUA;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAskesUA;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDataDitunjukDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelFoto;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelJnsDanaDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelMst_Answer;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTTD;

/**
 * Created by eriza on 25/08/2017.
 */

public class EspajModel implements Parcelable {
    private ModelID modelID;
    private PemegangPolisModel pemegangPolisModel;
    private TertanggungModel tertanggungModel;
    private CalonPembayarPremiModel calonPembayarPremiModel;
    private DetilInvestasiModel detilInvestasiModel;
    private DetilAgenModel detilAgenModel;
    private UsulanAsuransiModel usulanAsuransiModel;
    private ProfileResikoModel profileResikoModel;
    private DokumenPendukungModel dokumenPendukungModel;
    private ModelUpdateQuisioner modelUpdateQuisioner;
    private ArrayList<ModelAddRiderUA> ListRiderUA;
    private ArrayList<ModelAskesUA> ListASkesUA;
    private ArrayList<ModelDataDitunjukDI> ListManfaatDI;
    private ArrayList<ModelJnsDanaDI> ListDanaDI;
    private ArrayList<ModelMst_Answer> ListAnswer;
    private Boolean Validation;

    public EspajModel() {
    }

    protected EspajModel(Parcel in) {
        modelID = in.readParcelable(ModelID.class.getClassLoader());
        pemegangPolisModel = in.readParcelable(PemegangPolisModel.class.getClassLoader());
        tertanggungModel = in.readParcelable(TertanggungModel.class.getClassLoader());
        calonPembayarPremiModel = in.readParcelable(CalonPembayarPremiModel.class.getClassLoader());
        detilInvestasiModel = in.readParcelable(DetilInvestasiModel.class.getClassLoader());
        detilAgenModel = in.readParcelable(DetilAgenModel.class.getClassLoader());
        usulanAsuransiModel = in.readParcelable(UsulanAsuransiModel.class.getClassLoader());
        profileResikoModel = in.readParcelable(ProfileResikoModel.class.getClassLoader());
        dokumenPendukungModel = in.readParcelable(DokumenPendukungModel.class.getClassLoader());
        modelUpdateQuisioner = in.readParcelable(ModelUpdateQuisioner.class.getClassLoader());
        ListRiderUA = in.createTypedArrayList(ModelAddRiderUA.CREATOR);
        ListASkesUA = in.createTypedArrayList(ModelAskesUA.CREATOR);
        ListManfaatDI = in.createTypedArrayList(ModelDataDitunjukDI.CREATOR);
        ListDanaDI = in.createTypedArrayList(ModelJnsDanaDI.CREATOR);
        ListAnswer = in.createTypedArrayList(ModelMst_Answer.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(modelID, flags);
        dest.writeParcelable(pemegangPolisModel, flags);
        dest.writeParcelable(tertanggungModel, flags);
        dest.writeParcelable(calonPembayarPremiModel, flags);
        dest.writeParcelable(detilInvestasiModel, flags);
        dest.writeParcelable(detilAgenModel, flags);
        dest.writeParcelable(usulanAsuransiModel, flags);
        dest.writeParcelable(profileResikoModel, flags);
        dest.writeParcelable(dokumenPendukungModel, flags);
        dest.writeParcelable(modelUpdateQuisioner, flags);
        dest.writeTypedList(ListRiderUA);
        dest.writeTypedList(ListASkesUA);
        dest.writeTypedList(ListManfaatDI);
        dest.writeTypedList(ListDanaDI);
        dest.writeTypedList(ListAnswer);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EspajModel> CREATOR = new Creator<EspajModel>() {
        @Override
        public EspajModel createFromParcel(Parcel in) {
            return new EspajModel(in);
        }

        @Override
        public EspajModel[] newArray(int size) {
            return new EspajModel[size];
        }
    };

    public ModelID getModelID() {
        return modelID;
    }

    public void setModelID(ModelID modelID) {
        this.modelID = modelID;
    }

    public PemegangPolisModel getPemegangPolisModel() {
        return pemegangPolisModel;
    }

    public void setPemegangPolisModel(PemegangPolisModel pemegangPolisModel) {
        this.pemegangPolisModel = pemegangPolisModel;
    }

    public TertanggungModel getTertanggungModel() {
        return tertanggungModel;
    }

    public void setTertanggungModel(TertanggungModel tertanggungModel) {
        this.tertanggungModel = tertanggungModel;
    }

    public CalonPembayarPremiModel getCalonPembayarPremiModel() {
        return calonPembayarPremiModel;
    }

    public void setCalonPembayarPremiModel(CalonPembayarPremiModel calonPembayarPremiModel) {
        this.calonPembayarPremiModel = calonPembayarPremiModel;
    }

    public DetilInvestasiModel getDetilInvestasiModel() {
        return detilInvestasiModel;
    }

    public void setDetilInvestasiModel(DetilInvestasiModel detilInvestasiModel) {
        this.detilInvestasiModel = detilInvestasiModel;
    }

    public DetilAgenModel getDetilAgenModel() {
        return detilAgenModel;
    }

    public void setDetilAgenModel(DetilAgenModel detilAgenModel) {
        this.detilAgenModel = detilAgenModel;
    }

    public UsulanAsuransiModel getUsulanAsuransiModel() {
        return usulanAsuransiModel;
    }

    public void setUsulanAsuransiModel(UsulanAsuransiModel usulanAsuransiModel) {
        this.usulanAsuransiModel = usulanAsuransiModel;
    }

    public ProfileResikoModel getProfileResikoModel() {
        return profileResikoModel;
    }

    public void setProfileResikoModel(ProfileResikoModel profileResikoModel) {
        this.profileResikoModel = profileResikoModel;
    }

    public DokumenPendukungModel getDokumenPendukungModel() {
        return dokumenPendukungModel;
    }

    public void setDokumenPendukungModel(DokumenPendukungModel dokumenPendukungModel) {
        this.dokumenPendukungModel = dokumenPendukungModel;
    }

    public ModelUpdateQuisioner getModelUpdateQuisioner() {
        return modelUpdateQuisioner;
    }

    public void setModelUpdateQuisioner(ModelUpdateQuisioner modelUpdateQuisioner) {
        this.modelUpdateQuisioner = modelUpdateQuisioner;
    }

    public ArrayList<ModelAddRiderUA> getListRiderUA() {
        return ListRiderUA;
    }

    public void setListRiderUA(ArrayList<ModelAddRiderUA> listRiderUA) {
        ListRiderUA = listRiderUA;
    }

    public ArrayList<ModelAskesUA> getListASkesUA() {
        return ListASkesUA;
    }

    public void setListASkesUA(ArrayList<ModelAskesUA> listASkesUA) {
        ListASkesUA = listASkesUA;
    }

    public ArrayList<ModelDataDitunjukDI> getListManfaatDI() {
        return ListManfaatDI;
    }

    public void setListManfaatDI(ArrayList<ModelDataDitunjukDI> listManfaatDI) {
        ListManfaatDI = listManfaatDI;
    }

    public ArrayList<ModelJnsDanaDI> getListDanaDI() {
        return ListDanaDI;
    }

    public void setListDanaDI(ArrayList<ModelJnsDanaDI> listDanaDI) {
        ListDanaDI = listDanaDI;
    }

    public ArrayList<ModelMst_Answer> getListAnswer() {
        return ListAnswer;
    }

    public void setListAnswer(ArrayList<ModelMst_Answer> listAnswer) {
        ListAnswer = listAnswer;
    }

    public Boolean getValidation() {
        return Validation;
    }

    public void setValidation(Boolean validation) {
        Validation = validation;
    }

    public static Creator<EspajModel> getCREATOR() {
        return CREATOR;
    }
}