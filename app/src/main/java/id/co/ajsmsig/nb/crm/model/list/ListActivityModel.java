package id.co.ajsmsig.nb.crm.model.list;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by faiz_f on 01/05/2017.
 */

public class ListActivityModel implements Parcelable {
    private Long SL_TAB_ID;
    private Long SLP_TAB_ID;
    private Long SLA_TAB_ID;
    private Integer SLA_SUBTYPE;
    private String SLA_SDATE;

    public ListActivityModel(Long SL_TAB_ID, Long SLP_TAB_ID, Long SLA_TAB_ID, Integer SLA_SUBTYPE, String SLA_SDATE) {
        this.SL_TAB_ID = SL_TAB_ID;
        this.SLP_TAB_ID = SLP_TAB_ID;
        this.SLA_TAB_ID = SLA_TAB_ID;
        this.SLA_SUBTYPE = SLA_SUBTYPE;
        this.SLA_SDATE = SLA_SDATE;
    }


    protected ListActivityModel(Parcel in) {
        SL_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLP_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLA_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLA_SUBTYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_SDATE = in.readString();
    }

    public static final Creator<ListActivityModel> CREATOR = new Creator<ListActivityModel>() {
        @Override
        public ListActivityModel createFromParcel(Parcel in) {
            return new ListActivityModel(in);
        }

        @Override
        public ListActivityModel[] newArray(int size) {
            return new ListActivityModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(SL_TAB_ID);
        dest.writeValue(SLP_TAB_ID);
        dest.writeValue(SLA_TAB_ID);
        dest.writeValue(SLA_SUBTYPE);
        dest.writeString(SLA_SDATE);
    }

    public Long getSL_TAB_ID() {
        return SL_TAB_ID;
    }

    public void setSL_TAB_ID(Long SL_TAB_ID) {
        this.SL_TAB_ID = SL_TAB_ID;
    }

    public Long getSLP_TAB_ID() {
        return SLP_TAB_ID;
    }

    public void setSLP_TAB_ID(Long SLP_TAB_ID) {
        this.SLP_TAB_ID = SLP_TAB_ID;
    }

    public Long getSLA_TAB_ID() {
        return SLA_TAB_ID;
    }

    public void setSLA_TAB_ID(Long SLA_TAB_ID) {
        this.SLA_TAB_ID = SLA_TAB_ID;
    }

    public Integer getSLA_SUBTYPE() {
        return SLA_SUBTYPE;
    }

    public void setSLA_SUBTYPE(Integer SLA_SUBTYPE) {
        this.SLA_SUBTYPE = SLA_SUBTYPE;
    }

    public String getSLA_SDATE() {
        return SLA_SDATE;
    }

    public void setSLA_SDATE(String SLA_SDATE) {
        this.SLA_SDATE = SLA_SDATE;
    }
}
