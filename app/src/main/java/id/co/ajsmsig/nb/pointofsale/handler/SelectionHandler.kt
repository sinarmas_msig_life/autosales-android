package id.co.ajsmsig.nb.pointofsale.handler

import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption

/**
 * Created by andreyyoshuamanik on 21/02/18.
 */
interface SelectionHandler {

    fun onClickSelection(selection: PageOption)
}