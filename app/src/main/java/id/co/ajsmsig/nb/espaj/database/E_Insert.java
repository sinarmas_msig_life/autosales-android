package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.DBHelper;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.Referral;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.prop.method.QueryUtil;

/**
 * Created by eriza on 05/09/2017.
 */

public class E_Insert {
    private DBHelper dbHelper;
    private Context context;
    private final String TAG = E_Insert.this.getClass().getSimpleName();

    private final String TABLE_NAME_E_LST_REFF= "E_LST_REFF";
    private final String NAMA_REFF_COLUMN = "NAMA_REFF";
    private final String AGENT_CODE_COLUMN = "AGENT_CODE";
    private final String NAMA_CABANG_COLUMN = "NAMA_CABANG";
    private final String LCB_NO_COLUMN = "LCB_NO";

    public E_Insert(Context context) {
        this.context = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.update_data_preferences), Context.MODE_PRIVATE);
        int version = sharedPreferences.getInt(context.getString(R.string.lav_data_id), 0);
        this.dbHelper = new DBHelper(context, version);
    }

    public void InsertESPAJ(EspajModel me) {
        ContentValues cv = new ContentValues();
        QueryUtil queryUtil = new QueryUtil(context);
        cv.put("SPAJ_ID_TAB", me.getModelID().getSPAJ_ID_TAB());
        cv.put("IDPROPOSAL", me.getModelID().getProposal());
        cv.put(context.getString(R.string.NO_PROPOSAL_TAB), me.getModelID().getProposal_tab());
        cv.put("ID_CRM", me.getModelID().getId_crm());
        cv.put("ID_CRM_TAB", me.getModelID().getId_crm_tab());
        cv.put("IMEI", me.getModelID().getImei());
        cv.put("KODE_AGEN", me.getDetilAgenModel().getKd_penutup_da());
        cv.put("FLAG_AKTIF", 0);
        cv.put("JNS_SPAJ", 0);
        dbHelper.getWritableDatabase().insert(context.getString(R.string.TABLE_E_MST_SPAJ), "", cv);

        cv = new ContentValues();
        cv.put("SPAJ_ID_TAB", me.getModelID().getSPAJ_ID_TAB());
        dbHelper.getWritableDatabase().insert(context.getString(R.string.TABLE_E_PP), "", cv);

        cv = new ContentValues();
        cv.put("SPAJ_ID_TAB", me.getModelID().getSPAJ_ID_TAB());
        dbHelper.getWritableDatabase().insert(context.getString(R.string.TABLE_E_TT), "", cv);

        cv = new ContentValues();
        cv.put("SPAJ_ID_TAB", me.getModelID().getSPAJ_ID_TAB());
        dbHelper.getWritableDatabase().insert(context.getString(R.string.TABLE_E_CP), "", cv);

        cv = new ContentValues();
        cv.put("SPAJ_ID_TAB", me.getModelID().getSPAJ_ID_TAB());
        dbHelper.getWritableDatabase().insert(context.getString(R.string.TABLE_E_UA), "", cv);

        cv = new ContentValues();
        cv.put("SPAJ_ID_TAB", me.getModelID().getSPAJ_ID_TAB());
        dbHelper.getWritableDatabase().insert(context.getString(R.string.TABLE_E_DI), "", cv);

        cv = new ContentValues();
        cv.put("SPAJ_ID_TAB", me.getModelID().getSPAJ_ID_TAB());
        dbHelper.getWritableDatabase().insert(context.getString(R.string.TABLE_E_DA), "", cv);


        Cursor cursorPropPOS = queryUtil.query(
                context.getString(R.string.TABLE_E_PR),
                null,
                context.getString(R.string.SPAJ_ID_TAB) + " = ?",
                new String[]{String.valueOf(me.getModelID().getProposal_tab())},
                null
        );
        int fundCursorSize = cursorPropPOS.getCount();
        cursorPropPOS.close();

        if (fundCursorSize > 0) {
            if (new E_Select(context).selectLinkNonLink(me.getUsulanAsuransiModel().getKd_produk_ua()) == 17) {
                cv = new ContentValues();
                cv.put("SPAJ_ID_TAB", me.getModelID().getSPAJ_ID_TAB());
                queryUtil.update(context.getString(R.string.TABLE_E_PR),
                        cv,
                        context.getString(R.string.SPAJ_ID_TAB) + " = ?",
                        new String[]{String.valueOf(me.getModelID().getProposal_tab())});

            } else {
                queryUtil.delete(context.getString(R.string.TABLE_E_PR),
                        context.getString(R.string.SPAJ_ID_TAB) + " = ?",
                        new String[]{String.valueOf(me.getModelID().getProposal_tab())});

                cv = new ContentValues();
                cv.put("SPAJ_ID_TAB", me.getModelID().getSPAJ_ID_TAB());
                dbHelper.getWritableDatabase().insert(context.getString(R.string.TABLE_E_PR), "", cv);
            }
        } else {
            cv = new ContentValues();
            cv.put("SPAJ_ID_TAB", me.getModelID().getSPAJ_ID_TAB());
            dbHelper.getWritableDatabase().insert(context.getString(R.string.TABLE_E_PR), "", cv);
        }
    }

    public void InsertProfilResiko(String no_proposal_tab) {
        ContentValues cv = new ContentValues();
        cv.put("SPAJ_ID_TAB", no_proposal_tab);
        dbHelper.getWritableDatabase().insert(context.getString(R.string.TABLE_E_PR), "", cv);
    }

    public void insertReferallListToDatabase(List<Referral> referrals) {
        if (referrals != null && referrals.size() > 0) {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.beginTransaction();
            try {
                ContentValues contentValues = new ContentValues();
                for (Referral model : referrals) {
                    Log.v(TAG, "Inserting referral to database");

                    contentValues.put(NAMA_REFF_COLUMN, model.getReffName());
                    contentValues.put(AGENT_CODE_COLUMN, model.getReffId());
                    contentValues.put(NAMA_CABANG_COLUMN, model.getCabang());
                    contentValues.put(LCB_NO_COLUMN, model.getAjsCbng());

                    db.insert(TABLE_NAME_E_LST_REFF, null, contentValues);
                }
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }

        }
    }
}
