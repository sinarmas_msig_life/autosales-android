package id.co.ajsmsig.nb.espaj.fragment;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.FileRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.C_Insert;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.method.async.CrmRestClientUsage;
import id.co.ajsmsig.nb.crm.method.async.ProposalApiRestClientUsage;
import id.co.ajsmsig.nb.crm.method.async.SpajApiRestClientUsage;
import id.co.ajsmsig.nb.crm.model.apiresponse.ProgressRequestBody.ProgressRequestBody;
import id.co.ajsmsig.nb.crm.model.apiresponse.request.Content;
import id.co.ajsmsig.nb.crm.model.apiresponse.request.PostCasePegaRequest;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.PostCasePegaResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.SubmitResponse;
import id.co.ajsmsig.nb.crm.model.form.SimbakActivityModel;
import id.co.ajsmsig.nb.crm.model.hardcode.CrmTableCode;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.database.E_Update;
import id.co.ajsmsig.nb.espaj.method.GetTime;
import id.co.ajsmsig.nb.espaj.method.JSONToPDF;
import id.co.ajsmsig.nb.espaj.method.JSONToString;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.ProgressDialogClass;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelFoto;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTTD;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.database.P_Update;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.DateUtils;
import id.co.ajsmsig.nb.util.ProductUtils;
import okhttp3.Credentials;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Bernard on 15/06/2017.
 */

/*Change from AppCompatActivity into fragment*/
public class E_KonfirmasiDanKirimFragment extends Fragment implements View.OnClickListener, ProposalApiRestClientUsage.UploadProposalListener, ProposalApiRestClientUsage.DownloadPdfProposalListener, CrmRestClientUsage.UploadCrmListener, ProposalApiRestClientUsage.StempelListener, CrmRestClientUsage.UploadLeadListener {
    private final String TAG = E_KonfirmasiDanKirimFragment.class.getSimpleName();
    private boolean isSendOk = false;
    private ProgressDialog dialogOpenPdfProposal;
    private SpajApiRestClientUsage ewsRestClientUsage;
    private ProposalApiRestClientUsage eproposalRestClientUsage;
    private CrmRestClientUsage crmRestClientUsage;
    private P_MstDataProposalModel dataProposalModel;
    private String noProposal;
    private String kodeAgen;
    private int GROUP_ID;
    private final int KUNJUNGAN_DAN_PRESENTASI_ID = 51;
    private final int CLOSING = 45;
    private C_Select select;
    private int JENIS_LOGIN_BC;
    ProgressBar progressBar;
    File file;
    String result = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
        GROUP_ID = sharedPreferences.getInt("GROUP_ID", 0);
        JENIS_LOGIN_BC = sharedPreferences.getInt("JENIS_LOGIN_BC", 2);

        ewsRestClientUsage = new SpajApiRestClientUsage(getActivity());
        eproposalRestClientUsage = new ProposalApiRestClientUsage(getActivity());
        crmRestClientUsage = new CrmRestClientUsage(getContext());

        eproposalRestClientUsage.setOnUploadProposalListener(this);
        eproposalRestClientUsage.setonStempelProposalListener(this);
        eproposalRestClientUsage.setOnDownloadPDFProposalListener(this);
        crmRestClientUsage.setOnUploadCrmListener(this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.e_fragment_konfirmasi_dan_kirim_layout, container,
                false);

        select = new C_Select(getActivity());
        me = E_MainActivity.e_bundle.getParcelable(getString(R.string.modelespaj));
        dataProposalModel = new P_Select(getContext()).selectDataProposalByNoProposal(me.getModelID().getProposal_tab());
        ((E_MainActivity) getActivity()).setSecondToolbar("Konfirmasi Dan Kirim (10/", 10);
        ButterKnife.bind(this, view);

        btn_lanjut = getActivity().findViewById(R.id.btn_lanjut);
        btn_lanjut.setText(getString(R.string.kirim));
        btn_lanjut.setOnClickListener(this);
        btn_kembali = getActivity().findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(this);

        second_toolbar = ((E_MainActivity) getActivity()).getSecond_toolbar();
        iv_save = second_toolbar.findViewById(R.id.iv_save);
        iv_save.setOnClickListener(this);
        iv_next = second_toolbar.findViewById(R.id.iv_next);
        iv_next.setOnClickListener(this);
        iv_prev = second_toolbar.findViewById(R.id.iv_prev);
        iv_prev.setOnClickListener(this);

        iv_cb_send.setOnClickListener(this);

//        prev_sub.setOnClickListener(this);
//        send_sub.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MethodSupport.active_view(1, btn_lanjut);
        ProgressDialogClass.removeSimpleProgressDialog();
        if (dialogOpenPdfProposal != null)
            dialogOpenPdfProposal.cancel();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_cb_send:
                isSendOk = !isSendOk;
                Log.d(TAG, "onClick: " + isSendOk);
                if (isSendOk) {
                    iv_cb_send.setImageResource(R.drawable.spaj_send_s);
                } else {
                    iv_cb_send.setImageResource(R.drawable.spaj_send);
                }
                break;
            case R.id.iv_save:
                /*nothing to save*/
                break;
            case R.id.btn_lanjut:

                if (isSendOk) {
                    if (StaticMethods.isNetworkAvailable(getContext())) {
                        onClickSend();
                    } else {
                        showDialogNoInternetConnection();
                    }
                } else {
                    iv_save.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));
                    Toast.makeText(getContext(), "Tekan gambar ditengah untuk setuju", Toast.LENGTH_SHORT).show();
                }


                break;
            case R.id.iv_next:
                onClickSend();
                break;
            case R.id.btn_kembali:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_prev:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
        }
    }

    private String uploadDokumenPendukung(String Url, String fileName, File file) {
        View view = getLayoutInflater().inflate(R.layout.dialog_upload_progress, null);
        ProgressBar progressBar = view.findViewById(R.id.progressBar);
        TextView tvProgress = view.findViewById(R.id.tvProgress);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Mengunggah dokumen");
        builder.setView(view);
        AlertDialog dialog = builder.create();
        dialog.show();

        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);

        ProgressRequestBody requestBody = new ProgressRequestBody(file, "multipart/form-data", new ProgressRequestBody.UploadCallbacks() {
            @Override
            public void onProgressUpdate(int percentage) {
                //progressBar.setProgress(percentage);
                progressBar.setProgress(percentage);
                tvProgress.setText(String.valueOf(percentage));
                Log.v("Uploading", "Current "+percentage);
            }

            @Override
            public void onError() {
                showToast("Error saat Loading");
            }

            @Override
            public void onFinish() {
                dialog.dismiss();
                //progressBar.setProgress(100);
            }
        });
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", fileName, requestBody);

        Call<JsonObject> request = api.uploadImage(Url, filePart);

        request.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                result = response.toString();
                Log.v("Upload file success ","Test masuk ke response");
                result = String.valueOf(response.code());
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.v("Upload file failed",t.getMessage());
            }
        });
        return result;
    }
    private class MyTaskBack extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_KONFIRMASI_KIRIM);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            MethodSupport.active_view(1, btn_lanjut);
            if (new E_Select(getContext()).selectLinkNonLink(me.getUsulanAsuransiModel().getKd_produk_ua()) == 17) {
                ((E_MainActivity) getActivity()).setFragment(new E_ProfileResikoFragment(), getResources().getString(R.string.profil_nasabah));
            } else {
                if (me.getModelID().getJns_spaj() == 3) {
                    ((E_MainActivity) getActivity()).setFragment(new E_QuestionnaireFragment(), getResources().getString(R.string.questionnaire));
                } else if (me.getModelID().getJns_spaj() == 4) {
                    ((E_MainActivity) getActivity()).setFragment(new E_UpdateQuisionerSIOFragment(), getResources().getString(R.string.questionnaire));
                } else {
                    ((E_MainActivity) getActivity()).setFragment(new E_DokumenPendukungFragment(), getResources().getString(R.string.dokumen_pendukung));
                }
            }
        }
    }

    private void onClickBack() {
        MyTaskBack taskBack = new MyTaskBack();
        taskBack.execute();

//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setTitle("Melakukan perpindahan halaman");
//        progressDialog.setMessage("Mohon Menunggu...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
//        Runnable progressRunnable = new Runnable() {
//
//            @Override
//            public void run() {
//                MethodSupport.active_view(0, btn_kembali);
//                MethodSupport.active_view(0, iv_prev);
//                ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
//
//                ((E_MainActivity) getActivity()).onSave();
//                if (new E_Select(getContext()).selectLinkNonLink(me.getUsulanAsuransiModel().getKd_produk_ua()) == 17) {
//                    ((E_MainActivity) getActivity()).setFragment(new E_ProfileResikoFragment(), getResources().getString(R.string.profil_nasabah));
//                } else {
//                    if (me.getModelID().getJns_spaj() == 3) {
//                        ((E_MainActivity) getActivity()).setFragment(new E_QuestionnaireFragment(), getResources().getString(R.string.questionnaire));
//                    } else if (me.getModelID().getJns_spaj() == 4) {
//                        ((E_MainActivity) getActivity()).setFragment(new E_UpdateQuisionerSIOFragment(), getResources().getString(R.string.questionnaire));
//                    } else {
//                        ((E_MainActivity) getActivity()).setFragment(new E_DokumenPendukungFragment(), getResources().getString(R.string.dokumen_pendukung));
//                    }
//                }
//                progressDialog.cancel();
//            }
//        };
//
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 1000);
    }

    private ProgressDialog progressDialog;

    private void showDialogNoInternetConnection() {
        MethodSupport.active_view(1, btn_lanjut);
        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(getContext());

        alertDialogBuilder.setTitle("Tidak Terhubung Ke Internet ");
//                    alertDialogBuilder.setMessage(getString(R.string.solusi_masalah_koneksi));
        alertDialogBuilder.setMessage("Mohon nyalakan wifi atau mobile data anda terlebih dahulu");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    /**
     * if SL_ID = null it means that the leads hasn't been synchronized/ uploaded to server
     * @return
     */
    private boolean isLeadSlIdNull() {
        if (dataProposalModel.getLead_tab_id() != null) {
            C_Select select = new C_Select(getActivity());
            long leadTabId = dataProposalModel.getLead_tab_id();
            boolean isLeadSlIdNull = select.isLeadSlIdNull(leadTabId);
            return isLeadSlIdNull;
        }
        return false;
    }


    private void uploadLeadToServer() {
        if (dataProposalModel.getLead_tab_id() != null) {
            long leadTabId = dataProposalModel.getLead_tab_id();

            List<Long> leadIds = new ArrayList<>();
            leadIds.add(leadTabId);
            CrmRestClientUsage crmRestClientUsage = new CrmRestClientUsage(getActivity());
            crmRestClientUsage.setUploadLeadListener(this);
            crmRestClientUsage.uploadLead(leadIds);
            showUploadLeadProgressDialog();
        }

    }

    private void showUploadLeadProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Mengunggah lead");
        progressDialog.setMessage("Mohon tunggu");
        progressDialog.show();
    }

    private void hideUploadLeadProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onUploadLeadSuccess(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        hideUploadLeadProgressDialog();
        proceedSubmit();
    }



    @Override
    public void onUploadLeadFailed(String errorMessage) {
        hideUploadLeadProgressDialog();
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    private void onClickSend() {
        if (isLeadSlIdNull() && me.getDetilAgenModel().getJENIS_LOGIN() == 9) {
            if (StaticMethods.isNetworkAvailable(getActivity())) {
                uploadLeadToServer();
            } else {
                Toast.makeText(getActivity(), "Tidak terkoneksi dengan internet. Mohon cek kembali koneksi internet Anda", Toast.LENGTH_SHORT).show();
            }
        } else {

            proceedSubmit();

        }

    }

    private void proceedSubmit() {
        MethodSupport.active_view(0, btn_lanjut);

        dialogOpenPdfProposal = new ProgressDialog(getActivity());
        dialogOpenPdfProposal.setTitle("Mengirim Data");
        dialogOpenPdfProposal.setMessage("Mohon Menunggu...");
        dialogOpenPdfProposal.show();
        dialogOpenPdfProposal.setCancelable(false);

        JSONToPDF.JSONPDF(getContext(), me);

//        new UploadFileMulti("me.getModelID().getSPAJ_ID_TAB()").execute();
//        onUploadFile(me.getModelID().getSPAJ_ID_TAB(), 0, 0, 200, 0);

        if (me.getDetilAgenModel().getJENIS_LOGIN() == 2) {
            if (GROUP_ID == 40) {
                if (me.getModelID().getFlag_aktif() == 0) {
                    dialogOpenPdfProposal.setTitle("Mengirim SPAJ");
//                    ewsRestClientUsage.performUploadSPAJ(me);
                    submit(me);
                } else {
                    dialogOpenPdfProposal.setTitle("Mengirim File Dokumen Pendukung");

                    new UploadFileMulti(me.getModelID().getSPAJ_ID()).execute();
                }
            } else if (GROUP_ID == 7){
                if (dataProposalModel.getFlag_finish() == null) dataProposalModel.setFlag_finish(0);
                if (dataProposalModel.getFlag_finish() == 0) {
                    /*belum di update keserver*/
                    eproposalRestClientUsage.uploadProposal(dataProposalModel.getNo_proposal_tab());
                } else {
                    noProposal = me.getModelID().getProposal();
                    onGeneratePDF();
                }
            }
        } else if (me.getDetilAgenModel().getJENIS_LOGIN() == 9) {

            String localProposalId = me.getModelID().getProposal_tab();
            String proposalId = new P_Select(getActivity()).getProposalNoInSpaj(localProposalId);

            //Proposal belum di upload. Harus upload proposal dulu baru bisa submit.
            if (TextUtils.isEmpty(proposalId)) {
                if (StaticMethods.isNetworkAvailable(getActivity())) {
                    eproposalRestClientUsage.uploadProposalBuatSPAJ(dataProposalModel.getNo_proposal_tab());
                    eproposalRestClientUsage.setOnUploadProposalListener(this);
                } else {
                    Toast.makeText(getActivity(), "Tidak terkoneksi dengan internet. Mohon cek kembali koneksi internet Anda", Toast.LENGTH_SHORT).show();
                }
            } else {
                //Proposal sudah di upload sebelumnya. Lanjut submit guys

                if (dataProposalModel.getFlag_finish() == null) dataProposalModel.setFlag_finish(0);
                if (dataProposalModel.getFlag_finish() == 0) {
                    /*belum di update keserver*/
                    eproposalRestClientUsage.uploadProposal(dataProposalModel.getNo_proposal_tab());
                } else {
                    noProposal = me.getModelID().getProposal();
                    onGeneratePDF();
                }
            }


        }

    }

    private void submit(EspajModel me) {
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        String url_path = "";
        final String spajJson = JSONToString.JSONSPAJ(getContext(), me);
        E_Select select = new E_Select(getActivity());
        String paymentType = String.valueOf(select.getPaymentType(me.getModelID().getSPAJ_ID_TAB()));
        String transactionNumber = select.getPaymentTransactionNumber(me.getModelID().getSPAJ_ID_TAB());
        if (me.getDetilAgenModel().getGroup_id_da() == 40) {
            if (paymentType.equals(String.valueOf(Const.CREDIT_CARD_PAYMENT_CODE)) && !transactionNumber.isEmpty()) {
                url_path = "API-SPAJ-CC/api/json1/submitcc"; // for credit card
            } else {
            url_path = "spaj/api/json/submit";
//            url_path = "API-SPAJ/api/json/submit";
            }
        } else {
            url_path = "spaj/api/json/jsonTemp";
//            url_path = "API-SPAJ/api/json/jsonTemp";
        }
        String url = AppConfig.getBaseUrlSPAJ().concat(url_path);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), spajJson);
        Call<SubmitResponse> call = api.submit(url, requestBody);

        call.enqueue(new Callback<SubmitResponse>() {
            @Override
            public void onResponse(Call<SubmitResponse> call, Response<SubmitResponse> response) {
                try {
                    SubmitResponse body = response.body();
                    String newkey = body.getSpaj();
                    String virtual = body.getVirtual();
                    String error = body.getERROR();
                    String spaj_real = body.getSpaj_real();
//            Double hitung = response.getDouble("HITUNG");
                    Log.i("newkey", newkey);
                    Log.i("error", error);
                    if (!error.equals("") || (newkey.equals("") || newkey.equals("NO_SPAJ_TEMP"))) {
                        MethodSupport.Alert(getContext(), getString(R.string.title_upload_spaj), error, 0);
                        dialogOpenPdfProposal.dismiss();
                        MethodSupport.active_view(1, btn_lanjut);
                    }
//            else if(GROUP_ID == 40){
//                if(hitung > 10800 && hitung < 86401){
//                    MethodSupport.Alert(getContext(), getString(R.string.title_upload_spaj), "id member sudah ", 0);
//                    dialogOpenPdfProposal.dismiss();
//                }
//            }
                    else {
                        if (body.getSpaj_real() != null) {
                            me.getModelID().setProposal(body.getSpaj_real());
//                    me.getModelID().setFlag_proposal(0);
                        }
                        me.getModelID().setSPAJ_ID(newkey);
                        me.getModelID().setFlag_aktif(1);
                        me.getModelID().setVa_number(virtual);
                        new E_Update(getContext()).UpdateSUBMIT(me);
                        /*untuk update SPAJ_ID_TABnya belum*/
                        if (me.getDetilAgenModel().getJENIS_LOGIN() == 2) {
                            if (GROUP_ID == 40) {
                                dialogOpenPdfProposal.setTitle("Mengirim File Dokumen Pendukung");
                                new UploadFileMulti(me.getModelID().getSPAJ_ID()).execute();
                            } else if (GROUP_ID == 7) {
                                dialogOpenPdfProposal.setTitle("Mengirim File Dokumen Pendukung");
                                new UploadFileMulti(me.getModelID().getSPAJ_ID()).execute();
                            }
                        } else if (me.getDetilAgenModel().getJENIS_LOGIN() == 9) {

//                            int lsbsId = me.getUsulanAsuransiModel().getKd_produk_ua();
//                            int lsdbsNumber = me.getUsulanAsuransiModel().getSub_produk_ua();

//                            if (ProductUtils.isMagnaLink(lsbsId, lsdbsNumber)) {
//                                pegaCase(me.getModelID().getSPAJ_ID());
//                            }

                            if (updateAktivitasSuccesCreateSpaj(noProposal, newkey)) {
                                List<Long> idLeads = new ArrayList<>();
                                idLeads.add(Long.valueOf(me.getModelID().getId_crm_tab()));
                                dialogOpenPdfProposal.setTitle("Mengupdate Data Lead");
                                crmRestClientUsage.uploadLead(idLeads);
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SubmitResponse> call, Throwable t) {
                MethodSupport.active_view(1, btn_lanjut);
                showToast("Kirim SPAJ gagal");
                dialogOpenPdfProposal.dismiss();
            }
        });
    }

    private void onUploadFile(String spaj_id, int pos, int flag, int status_code, int flag_awal) {
        String[] url = null;
        String result = "";
        String filepath = "";
        int i = pos;
        Log.i("ID SPAJ NEW KET", spaj_id);
        switch (flag) {
            case 0:
                File file = new File(getActivity().getFilesDir() + "/ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
                File[] file1 = file.listFiles();
                List<String> List_File = new ArrayList<String>();
                for (int j = 0; j < file1.length; j++) {
                    File foto = new File(getActivity().getFilesDir(),
                            "/ESPAJ/spaj_file/" + spaj_id + "/" + file1[j].getName());
                    List_File.add(foto.toString());
                }
                if (status_code == 200) {
                    if (flag_awal != 0) {
                        i++;
                    }
                    // kirim screenshot SPAJ
                    if (i < List_File.size()) {
//                        filepath = "http://test.sinarmasmsiglife.co.id:8800/EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                        filepath = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                                + spaj_id + "/Spaj/" + List_File.get(i).substring(List_File.get(i).lastIndexOf("/") + 1,
                                List_File.get(i).lastIndexOf("_"))
                                + "/jpg/pdf";
                        POSTUpload(filepath, List_File.get(i), i, flag, 1);

                        //kirim pdf string
//                        filepath = "http://test.sinarmasmsiglife.co.id:8800/EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                        filepath = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                                + spaj_id + "/Spaj/" + "PDF" + "_" + me.getModelID().getSPAJ_ID_TAB()
                                + "/pdf/pdf";
                        String file_pdf = new File(getActivity().getFilesDir() + "/ESPAJ/PDF/"
                                + "PDF" + me.getModelID().getSPAJ_ID_TAB() + ".PDF").toString();
                        POSTUpload(filepath, file_pdf, pos, flag, 1);
//            Log.i("result ttd", result);


                    } else {
                        onUploadFile(me.getModelID().getSPAJ_ID_TAB(), 0, 1, 200, 0);
                    }
                } else {
                    showToast("Dokumen Gagal Diupload");
                    MethodSupport.active_view(1, btn_lanjut);
                }
                break;
            case 1:
                // kirim foto
                if (status_code == 200) {
                    if (flag_awal != 0) {
                        i++;
                    }
                    List<ModelFoto> ListFoto = me.getDokumenPendukungModel().getListFoto();
                    if (i < ListFoto.size()) {
                        if (!(ListFoto.get(i).getFile_foto().equals(""))
                                || !(ListFoto.get(i).getFile_foto().equals(null))) {
//                            filepath = "http://test.sinarmasmsiglife.co.id:8800/API-EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                            filepath = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                                    + spaj_id + "/Spaj/" + ListFoto.get(i).getJudul_foto() + "/jpg/pdf";
                            POSTUpload(filepath, ListFoto.get(i).getFile_foto(), i, flag, 1);
                            Log.i("result foto", result);
                        }
                    } else {
                        onUploadFile(me.getModelID().getSPAJ_ID_TAB(), 0, 2, 200, 0);
                    }
                } else {
                    showToast("Dokumen Gagal Diupload");
                    MethodSupport.active_view(1, btn_lanjut);
                }
                break;
            case 2:
                if (status_code == 200) {
                    if (flag_awal != 0) {
                        i++;
                    }
                    // kirim sertifikat
//                    if (me.getUsulanAsuransiModel().getJenis_produk_ua() == 23
//                            && me.getUsulanAsuransiModel().getKd_produk_ua() == 212) {
//                        if (!me.getModelID().getSertifikat().equals("") || !me.getModelID().getSertifikat().equals(null)) {
//                            filepath = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
//                                    + spaj_id + "/Spaj/" + "SERTIIFIKAT" + "_" + me.getModelID().getSertifikat()
//                                    + "/pdf/pdf";
//                            String file_sertifikat = new File(getActivity().getFilesDir() + "/ESPAJ/SERTIFIKAT/"
//                                    + "SERTIFIKAT" + me.getModelID().getSertifikat() + ".PDF").toString();
//                            POSTUpload(filepath, file_sertifikat, pos, flag);
//                            Log.i("result ttd", result);
//                        }
//                    }
                    List<ModelTTD> ListTtd = me.getDokumenPendukungModel().getListTTD();
                    if (i < ListTtd.size()) {
                        if (!(ListTtd.get(i).getFile_ttd().equals("")) || !(ListTtd.get(i).getFile_ttd().equals(null))) {
//                            filepath = "http://test.sinarmasmsiglife.co.id:8800/API-EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                            filepath = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                                    + spaj_id + "/Spaj/" + ListTtd.get(i).getJudul_ttd() + "_" + "/jpg/pdf";
                            POSTUpload(filepath, ListTtd.get(i).getFile_ttd(), i, flag, 1);
                        }
                    } else {
                        eproposalRestClientUsage.UploadStempelProposal(me);
                    }
                } else {
                    showToast("Dokumen Gagal Diupload");
                    MethodSupport.active_view(1, btn_lanjut);
                }
                break;
        }
    }

    public void onGeneratePDF() {
//        File pdfFile = P_StaticMethods.getFileProposalPdf(getContext(), dataProposalModel.getNo_proposal());
//
//        if (!pdfFile.exists()) {
//            Log.d(TAG, "prosesBukaPdf: file not exist");
//            eproposalRestClientUsage.performDownloadPdfProposal(dataProposalModel.getNo_proposal(), false);
//        } else {
        if (me.getModelID().getFlag_aktif() == 0) {
            dialogOpenPdfProposal.setTitle("Mengirim SPAJ");
//            ewsRestClientUsage.performUploadSPAJ(me);
            submit(me);
        } else {
            if (me.getDetilAgenModel().getJENIS_LOGIN() == 2) {
                if (me.getDetilAgenModel().getGroup_id_da() == 7) {
                    dialogOpenPdfProposal.setTitle("Mengirim File Dokumen Pendukung");
                    new UploadFileMulti(me.getModelID().getSPAJ_ID()).execute();
                }
            }else {
                int lastPost = new C_Select(getContext()).selectCurrentActivityYesLastPos(me.getModelID().getSPAJ_ID(), me.getModelID().getProposal());
                if (lastPost != -1) {
                    if (lastPost == 1) {// belum ke update
                        List<Long> idLeads = new ArrayList<>();
                        idLeads.add(Long.valueOf(me.getModelID().getId_crm_tab()));
                        crmRestClientUsage.uploadLead(idLeads);
                    } else {//udah terupdate
                        dialogOpenPdfProposal.setTitle("Mengirim File Dokumen Pendukung");
                        //tambah jsontopdf nya disini
                        new UploadFileMulti(me.getModelID().getSPAJ_ID()).execute();
                    }
                } else {
                    Log.d(TAG, "onGeneratePDF: harusnya gak boleh -1");
                    dialogOpenPdfProposal.setTitle("Mengirim File Dokumen Pendukung");
                    new UploadFileMulti(me.getModelID().getSPAJ_ID()).execute();
//                masih di test
//                uploadDokumenPendukungMethodAll(me.getModelID().getSPAJ_ID());
                }
            }
        }
//        }
    }

    public void uploadDokumenPendukungMethodAll(String spaj_id){
        String filepath = "";
        String filepathJPG = "";
        String filepathTTD = "";

        filepath = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                + spaj_id + "/Spaj/" + "SPAJ"
                + "/pdf/pdf";
        String fileName = new File(getActivity().getFilesDir() + "/ESPAJ/PDF/"
                + "PDF" + me.getModelID().getSPAJ_ID_TAB() + ".PDF").toString();
        File pdfFile = new File(fileName);
        uploadDokumenPendukung(filepath, fileName, pdfFile);
        Log.i("result PDF", result);

        List<ModelFoto> ListFoto = me.getDokumenPendukungModel().getListFoto();
        for (int i = 0; i < ListFoto.size(); i++) {
            if (!(ListFoto.get(i).getFile_foto().equals("")) || !(ListFoto.get(i).getFile_foto().equals(null))) {
                filepathJPG = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"//data asli dan data test
                        + spaj_id + "/Spaj/" + ListFoto.get(i).getId_foto() + "/jpg/pdf";

                String imgFile = new File (ListFoto.get(i).getFile_foto()).toString();

                File jpgfile = new File(imgFile);

                uploadDokumenPendukung(filepathJPG, imgFile, jpgfile);
                Log.i("result foto", result);
            }
        }

        if (me.getUsulanAsuransiModel().getJenis_produk_ua() == 23
                && me.getUsulanAsuransiModel().getKd_produk_ua() == 212) {
            if (!me.getModelID().getSertifikat().equals("") || !me.getModelID().getSertifikat().equals(null)) {
//                        filepath = "http://test.sinarmasmsiglife.co.id:8800/EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                filepath = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                        + spaj_id + "/Spaj/" + "SERTIFIKAT" + "_" + me.getModelID().getSertifikat()
                        + "/pdf/pdf";
                String file_sertifikat = new File(getActivity().getFilesDir() + "/ESPAJ/SERTIFIKAT/"
                        + "SERTIFIKAT" + me.getModelID().getSertifikat() + ".PDF").toString();
                File sertifikat = new File(file_sertifikat);
                uploadDokumenPendukung(filepath, file_sertifikat,sertifikat);
                Log.i("result sertifikat", result);
            }
        }

        List<ModelTTD> ListTtd = me.getDokumenPendukungModel().getListTTD();
        for (int i = 0; i < ListTtd.size(); i++) {
            if (!(ListTtd.get(i).getFile_ttd().equals("")) || !(ListTtd.get(i).getFile_ttd().equals(null))) {
//                        filepath = "http://test.sinarmasmsiglife.co.id:8800/API-EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                filepathTTD = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
//                        filepath = "http://192.168.13.209:8080/EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"//link pak adrian
                        + spaj_id + "/Spaj/" + ListTtd.get(i).getId_ttd() + "_" + "/jpg/pdf";
                String ttdImg = new File(ListTtd.get(i).getId_ttd()).toString();

                File ttdfile = new File(ttdImg);
                uploadDokumenPendukung(filepathTTD, ttdImg, ttdfile);
                Log.i("result ttd", result);
            }
        }
    }

    @Override
    public void onUploadProposalSuccess(String noProposal, String message) {
        Log.d(TAG, "onUploadProposalSuccess: success");
        this.noProposal = noProposal;
        me.getModelID().setProposal(noProposal);
        new E_Update(getContext()).UpdateSUBMIT(me);
        P_Update p_update = new P_Update(getContext());
        p_update.updateNoProposal(me.getModelID().getProposal_tab(), noProposal);
        p_update.updateFlagFinish(me.getModelID().getProposal_tab(), 1);
        dataProposalModel.setFlag_finish(1);

        onGeneratePDF();
    }


    @Override
    public void onUploadProposalFailure(String message) {
        MethodSupport.active_view(1, btn_lanjut);
        Log.d(TAG, "onUploadProposalFailure: " + message);
        showToast("Kirim proposal gagal");
        dialogOpenPdfProposal.dismiss();
    }

    @Override
    public void onUploadProposalSuccessBuatSPAJ(String noProposal, String message) {
        Log.d(TAG, "onUploadProposalSuccess: success");
        this.noProposal = noProposal;
        me.getModelID().setProposal(noProposal);
        new E_Update(getContext()).UpdateSUBMIT(me);
        P_Update p_update = new P_Update(getContext());
        p_update.updateNoProposal(me.getModelID().getProposal_tab(), noProposal);
        p_update.updateFlagFinish(me.getModelID().getProposal_tab(), 1);
        dataProposalModel.setFlag_finish(1);

        onGeneratePDF();
    }

    @Override
    public void onDownloadPDFProposalSuccess() {
//        ewsRestClientUsage.performUploadSPAJ(me);
//        onGeneratePDF();
        if (me.getModelID().getFlag_aktif() == 0) {
            /*belum kekirim*/
//            ewsRestClientUsage.performUploadSPAJ(me);
            submit(me);
        } else {
            int lastPost = new C_Select(getContext()).selectCurrentActivityYesLastPos(me.getModelID().getSPAJ_ID(), me.getModelID().getProposal());
            if (lastPost != -1) {
                if (lastPost == 1) {// belum ke update
                    List<Long> idLeads = new ArrayList<>();
                    idLeads.add(Long.valueOf(me.getModelID().getId_crm_tab()));
                    crmRestClientUsage.uploadLead(idLeads);
                } else {//udah terupdate
                    dialogOpenPdfProposal.setTitle("Mengirim File Dokumen Pendukung");
                    new UploadFileMulti(me.getModelID().getSPAJ_ID()).execute();
                }
            } else {
                dialogOpenPdfProposal.setTitle("Mengirim File Dokumen Pendukung");
                new UploadFileMulti(me.getModelID().getSPAJ_ID()).execute();
                Log.d(TAG, "onGeneratePDF: harusnya gak boleh -1");
            }
        }
    }


    @Override
    public void onDownloadPDFProposalFailure(String title, String message) {
        MethodSupport.active_view(1, btn_lanjut);
        showToast("Gagal membuat PDF proposal, pesan : " + message);
        dialogOpenPdfProposal.dismiss();
    }

    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        dialogOpenPdfProposal.dismiss();
    }

    @Override
    public void onUploadCrmSuccess(Long leadId, String message, Boolean isUpdateLead) {
//        Habis ini harusnya ditambahin stempel TODO buat mbak epang
//        new UploadFile(me.getModelID().getSPAJ_ID()).execute();
//        onUploadFile(me.getModelID().getSPAJ_ID_TAB(), 0, 0, 200, 0);
        dialogOpenPdfProposal.setTitle("Mengirim File Dokumen Pendukung");
        new UploadFileMulti(me.getModelID().getSPAJ_ID()).execute();
//        showToast("SPAJ Berhasil Di submit");
//        getActivity().finish();

    }

    @Override
    public void onUploadCrmFail(String message) {
        showToast("Kirim data lead gagal");
        dialogOpenPdfProposal.dismiss();
        MethodSupport.active_view(1, btn_lanjut);
    }

    public boolean updateAktivitasSuccesCreateSpaj(String noProposal, String SPAJ_ID_TAB) {
        C_Select c_select = new C_Select(getContext());
        HashMap<String, String> lastActivity = c_select.selectLastActivityForUpdatedTOYes(me.getModelID().getId_crm_tab());
        String slaTabId = lastActivity.get(getString(R.string.SLA_TAB_ID));
        if (lastActivity.get(getString(R.string.SLA_SUBTYPE)) != null) {
            if (!lastActivity.get(getString(R.string.SLA_SUBTYPE)).equals("3")) {
//            Insert
                /*Set Activity yes*/
                slaTabId = onInsertActivity(kodeAgen);
            } else {
//            update
                onUpdateActivity(kodeAgen, slaTabId);
            }
        } else {
            //insert
            slaTabId = onInsertActivity(kodeAgen);
        }

        /*Insert SIMBAK SPAJ*/
        ContentValues cvSimbakSPAJ = new ContentValues();
        cvSimbakSPAJ.put("SLS_SPAJ_TMP", SPAJ_ID_TAB);
        cvSimbakSPAJ.put(getString(R.string.SLS_PROPOSAL), noProposal);
        cvSimbakSPAJ.put(getString(R.string.SLS_CRTD_ID), kodeAgen);
        cvSimbakSPAJ.put(getString(R.string.SLS_CREATED), StaticMethods.getTodayDateTime());
        cvSimbakSPAJ.put(getString(R.string.SLS_UPDTD_ID), kodeAgen);
        cvSimbakSPAJ.put(getString(R.string.SLS_UPDATED), StaticMethods.getTodayDateTime());
        cvSimbakSPAJ.put(getString(R.string.SLS_INST_TAB_ID), slaTabId);
        cvSimbakSPAJ.put(getString(R.string.SLS_INST_TYPE), CrmTableCode.ACTIVITY);

        new QueryUtil(getContext()).insert(getString(R.string.TABLE_SIMBAK_LIST_SPAJ), cvSimbakSPAJ);
        return true;
    }

    private String onInsertActivity(String kodeAgen) {
        SimbakActivityModel simbakActivityModel = new SimbakActivityModel();
        simbakActivityModel.setSLA_CRTD_ID(kodeAgen);
        long timestamp = DateUtils.generateTimeStamp();
        simbakActivityModel.setSLA_TAB_ID(timestamp);
        simbakActivityModel.setSLA_CRTD_DATE(StaticMethods.getTodayDateTime());
        simbakActivityModel.setSLA_UPDTD_ID(kodeAgen);
        simbakActivityModel.setSLA_UPDTD_DATE(StaticMethods.getTodayDateTime());
        simbakActivityModel.setSLA_ACTIVE(1);
        simbakActivityModel.setSLA_LAST_POS(1);
        simbakActivityModel.setSLA_SDATE(StaticMethods.getTodayDateTime());
        simbakActivityModel.setSLA_TYPE(KUNJUNGAN_DAN_PRESENTASI_ID);
        simbakActivityModel.setSLA_SUBTYPE(CLOSING);
        simbakActivityModel.setSLA_DETAIL(me.getModelID().getSPAJ_ID());
        int slType = new C_Select(getContext()).selectSLType(Long.parseLong(me.getModelID().getId_crm_tab()));
        if (slType != -1)
            simbakActivityModel.setSLA_SRC(slType);
        simbakActivityModel.setSLA_IDATE(StaticMethods.getTodayDateTime());
        simbakActivityModel.setSLA_INST_TAB_ID(Long.valueOf(me.getModelID().getId_crm_tab()));
        if (JENIS_LOGIN_BC == Const.BANK_CODE_SINARMAS) {
            simbakActivityModel.setSLA_INST_TYPE(CrmTableCode.PRODUCT);
        } else {
            simbakActivityModel.setSLA_INST_TYPE(CrmTableCode.LEAD);
        }
        if (select != null) {
            String slId = me.getModelID().getId_crm_tab();
            simbakActivityModel.setSL_NAME(select.getLeadNameFromSLID(slId));
            simbakActivityModel.setSL_ID_(null); //SL_ID must be ID from the from server
        }

        C_Insert insert = new C_Insert(getActivity());
        insert.insertToSimbakListActivity(simbakActivityModel);

        return String.valueOf(timestamp);
    }

    private void onUpdateActivity(String kodeAgen, String slaTabId) {
        ContentValues cv = new ContentValues();
        cv.put(getString(R.string.SLA_SUBTYPE), 3);
        cv.put(getString(R.string.SLA_UPDTD_DATE), StaticMethods.getTodayDateTime());
        cv.put(getString(R.string.SLA_UPDTD_ID), kodeAgen);
        cv.put(getString(R.string.SLA_DETAIL), me.getModelID().getSPAJ_ID());


        String selection = getString(R.string.SLA_TAB_ID) + "= ?";
        String[] selectionArgs = new String[]{slaTabId};

        new QueryUtil(getContext()).update(
                getString(R.string.TABLE_SIMBAK_LIST_ACTIVITY),
                cv,
                selection,
                selectionArgs
        );
    }

    @Override
    public void onStempelSuccess(int statusCode, Header[] headers, JSONObject response) {
        showToast("SPAJ Berhasil Di submit");
        getActivity().finish();
        MethodSupport.active_view(1, btn_lanjut);
    }

    @Override
    public void onStempelFailure(String title, String message) {
        showToast("Gagal Stempel Proposal");
        dialogOpenPdfProposal.dismiss();
        MethodSupport.active_view(1, btn_lanjut);
    }

    private class UploadFileMulti extends AsyncTask<String, Void, String> {
        //        ProgressDialog dialog;
        String spaj_id = "";

        public UploadFileMulti(String spaj_id) {
            this.spaj_id = spaj_id;
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
//            dialog = new ProgressDialog(getActivity());
//            dialog = ProgressDialog.show(getActivity(), "Mohon Menunggu", "Melakukan Penguploadan gambar...");
        }

        @Override
        protected String doInBackground(String... urls) {
            String[] url = null;
            String result = "";
            String filepath = "";
            Log.i("ID SPAJ NEW KET", spaj_id);
//            File file = new File(getActivity().getFilesDir() + "/ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//            File file1[] = file.listFiles();
//            List<String> List_File = new ArrayList<String>();
//            for (int i = 0; i < file1.length; i++) {
//                File foto = new File(getActivity().getFilesDir(),
//                        "/ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB() + "/" + file1[i].getName());
//                List_File.add(foto.toString());
//            }
            // kirim screenshot SPAJ
//            for (int i = 0; i < List_File.size(); i++) {
////                filepath = "http://test.sinarmasmsiglife.co.id:8800/EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
//                filepath = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
//                        + spaj_id + "/Spaj/" + List_File.get(i).substring(List_File.get(i).lastIndexOf("/") + 1,
//                        List_File.get(i).lastIndexOf("_")) + "/jpg/pdf";
//                result = POSTUploadFile(filepath, List_File.get(i));
//                Log.i("result ss", result);
//                if (!result.equals("200")) {
//                    break;
//                }
//            }

            //kirim pdf string
//            filepath = "http://test.sinarmasmsiglife.co.id:8800/EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
            filepath = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                    + spaj_id + "/Spaj/" + "SPAJ"
                    + "/pdf/pdf";
            String file_pdf = new File(getActivity().getFilesDir() + "/ESPAJ/PDF/"
                    + "PDF" + me.getModelID().getSPAJ_ID_TAB() + ".PDF").toString();

            File filePdfSpaj = new File(getActivity().getFilesDir() + "/ESPAJ/PDF/"
                    + "PDF" + me.getModelID().getSPAJ_ID_TAB() + ".PDF");

            long fileSizeInKb = filePdfSpaj.length()/1024;
            if (fileSizeInKb == 0){
                result = "404";
            } else {
                result = POSTUploadFile(filepath, file_pdf);
            }
            Log.i("result PDF", result);

            if (result.equals("200")) {
                // kirim foto
                List<ModelFoto> ListFoto = me.getDokumenPendukungModel().getListFoto();
                for (int i = 0; i < ListFoto.size(); i++) {
                    if (!(ListFoto.get(i).getFile_foto().equals(""))
                            || !(ListFoto.get(i).getFile_foto().equals(null))) {
//                        filepath = "http://test.sinarmasmsiglife.co.id:8800/API-EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                        filepath = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"//data asli dan data test
//                        filepath = "http://192.168.13.97:8080/EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"//link pak adrian
                                + spaj_id + "/Spaj/" + ListFoto.get(i).getId_foto() + "/jpg/pdf";
                        result = POSTUploadFile(filepath, ListFoto.get(i).getFile_foto());
                        Log.i("result foto", result);
                    }
                    if (!result.equals("200")) {
                        break;
                    }
                }
            }
            if (result.equals("200")) {
                // kirim sertifikat
                if (me.getUsulanAsuransiModel().getJenis_produk_ua() == 23
                        && me.getUsulanAsuransiModel().getKd_produk_ua() == 212) {
                    if (!me.getModelID().getSertifikat().equals("") || !me.getModelID().getSertifikat().equals(null)) {
//                        filepath = "http://test.sinarmasmsiglife.co.id:8800/EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                        filepath = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                                + spaj_id + "/Spaj/" + "SERTIFIKAT" + "_" + me.getModelID().getSertifikat()
                                + "/pdf/pdf";
                        String file_sertifikat = new File(getActivity().getFilesDir() + "/ESPAJ/SERTIFIKAT/"
                                + "SERTIFIKAT" + me.getModelID().getSertifikat() + ".PDF").toString();
                        result = POSTUploadFile(filepath, file_sertifikat);
                        Log.i("result sertifikat", result);
                    }
                }

                List<ModelTTD> ListTtd = me.getDokumenPendukungModel().getListTTD();
                for (int i = 0; i < ListTtd.size(); i++) {
                    if (!(ListTtd.get(i).getFile_ttd().equals("")) || !(ListTtd.get(i).getFile_ttd().equals(null))) {
//                        filepath = "http://test.sinarmasmsiglife.co.id:8800/API-EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
                        filepath = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
//                        filepath = "http://192.168.13.209:8080/EWS/api/json/uploadFile/~~storage~pdfind~spaj_temp/"//link pak adrian
                                + spaj_id + "/Spaj/" + ListTtd.get(i).getId_ttd() + "_" + "/jpg/pdf";
                        result = POSTUploadFile(filepath, ListTtd.get(i).getFile_ttd());
                        Log.i("result ttd", result);
                    }
                    if (!result.equals("200")) {
                        break;
                    }
                }
            }
//            String  filepath = "http://fws.sinarmasmsiglife.co.id:8082/api/json/uploadFile/~~storage~pdfind~spaj_temp/"
//                                + "TEST_EPANK" + "/Spaj/"+ "tes"+ "_"+ "/zip/pdf";
//            String result = POSTUploadFile(filepath, "tes");
            return result;
        }

        protected void onProgressUpdate(Integer... progress) {
//            dialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            MethodSupport.active_view(1, btn_lanjut);
            if (!result.equals("200")) {
                dialogOpenPdfProposal.dismiss();
                AlertDialog.Builder dialog_error = new AlertDialog.Builder(getActivity());
                dialog_error.setTitle("Gagal Mengupload Dokumen Pendukung");
                dialog_error.setMessage(
                        "Mohon diperiksa kembali koneksi internet anda, kemudian silakan coba kirim kembali.\n\n\n");
                dialog_error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog_error.show();
            } else {
                //                me.getModelID().setFlag_dokumen(1);
//                new E_Update(getContext()).UpdateSUBMIT(me);
//
//                ewsRestClientUsage.UploadStempelProposal(me);
                if (me.getDetilAgenModel().getJENIS_LOGIN() == 2) {
                    if (GROUP_ID == 40) {
                        new DownloadProposal(me.getModelID().getSPAJ_ID(), me.getModelID().getProposal())
//                                .execute("http://192.168.10.170:8080/EWS/api/json/generate_proposal_ultimate/"); // link nya json untuk post proposal SIAP2U
//                                .execute("http://test.sinarmasmsiglife.co.id:8800/proposal/api/eproposal/generate_proposal_ultimate/");
                                .execute("https://proposal-api.sinarmasmsiglife.co.id/proposal/api/eproposal/generate_proposal_ultimate/"); // link asli
//                                .execute("http://192.168.13.97:8080/API-PROPOSAL/api/eproposal/generate_proposal_ultimate");//link pak adrian

                    } else {
                        dialogOpenPdfProposal.dismiss();
                        me.getModelID().setFlag_proposal(1);
                        me.getModelID().setFlag_dokumen(1);
                        new E_Update(getContext()).UpdateSUBMIT(me);
                        Log.i("download", "dowmload_stempel");
                        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                        dialog.setTitle("Data Berhasil Terkirim");
                        dialog.setMessage(
                                "Data Anda Telah Kami Periksa dan Kami Akan Proses Dalam Waktu Maksimal 2 x 24 Jam.\n Terima Kasih.");
                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                getActivity().finish();
                            }
                        });
                        dialog.show();
                    }
                } else {
//                    new StempelProposal(me.getModelID().getSPAJ_ID(), me.getModelID().getProposal())
//                            .execute("https://proposal-api.sinarmasmsiglife.co.id/proposal/api/eproposal/stempel_pdf_proposal/");
////                            .execute("http://test.sinarmasmsiglife.co.id:8800/EWS/api/eproposal/stempel_pdf_proposal/");
                    dialogOpenPdfProposal.dismiss();
                    me.getModelID().setFlag_proposal(1);
                    me.getModelID().setFlag_dokumen(1);
                    new E_Update(getContext()).UpdateSUBMIT(me);
                    Log.i("download", "dowmload_stempel");
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setTitle("Data Berhasil Terkirim");
                    dialog.setMessage(
                            "Data Anda Telah Kami Periksa dan Kami Akan Proses Dalam Waktu Maksimal 2 x 24 Jam.\n Terima Kasih.");
                    dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            getActivity().finish();
                        }
                    });
                    dialog.show();

                }
            }
        }
    }

    private String POSTUploadFile(String url, String foto) {
        int status = 0;
        String result = "";

        PostMethod method = new PostMethod(url);
        try {
            HttpClient httpClient = new HttpClient();
            httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(180000);
            httpClient.getHttpConnectionManager().getParams().setSoTimeout(180000);
//            onZipFile();
            method.setRequestEntity(new FileRequestEntity(new File(foto), "multipart/form-data"));
//            File outputFile = new File(Environment.getExternalStorageDirectory()+ "/ESPAJ/test_epank.zip");
//            outputFile.toString();
//            method.setRequestEntity(new FileRequestEntity(outputFile, "multipart/form-data"));

            status = httpClient.executeMethod(method);
            result = String.valueOf(status);
            System.out.println("HTTP status " + method.getStatusCode() + " creating con\n\n");
        } catch (ConnectTimeoutException e) {
            Log.e("Timeout Exception: ", e.toString());
        } catch (SocketTimeoutException ste) {
            Log.e("Timeout Exception: ", ste.toString());
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
        return result;
    }

    private void POSTUpload(String url_upload, String foto, final int pos, final int flag, int flag_awal) {
        RequestParams params = new RequestParams();
        try {
            params.put("uploaded_file", new File(foto));
            params.setForceMultipartEntityContentType(true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.setMaxConnections(100);
        client.post(url_upload, params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                System.out.println("statusCode " + statusCode);
                onUploadFile(me.getModelID().getSPAJ_ID_TAB(), pos, flag, statusCode, 1);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                onUploadFile(me.getModelID().getSPAJ_ID_TAB(), pos, flag, -1, 1);
                MethodSupport.active_view(1, btn_lanjut);
            }
        });
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            // hasil=line;
            result += line;

        inputStream.close();
        return result;

    }

    public class DownloadProposal extends AsyncTask<String, Void, String> {
        String no_temp = "";
        String spaj = "";

        public DownloadProposal(String no_temp, String spaj) {
            this.no_temp = no_temp;
            this.spaj = spaj;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            // me=((ModelESPAJ)getApplication());
            return POSTDOWNLOAD(urls[0], me.getModelID().getSPAJ_ID(), me.getModelID().getProposal());
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            MethodSupport.active_view(1, btn_lanjut);
            dialogOpenPdfProposal.dismiss();
            if (result.equals("Gagal") || result.equals(null)) {
                AlertDialog.Builder dialog_error = new AlertDialog.Builder(getActivity());
                dialog_error.setTitle("Gagal Melakukan Proses Proposal...");
                dialog_error.setMessage(
                        "Mohon diperiksa kembali koneksi internet anda, kemudian silakan coba kirim kembali.\n\n\n");
                dialog_error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog_error.show();
            } else {
                if (result.contains("error")) {
                    AlertDialog.Builder dialog_error = new AlertDialog.Builder(getActivity());
                    dialog_error.setTitle("Gagal generate dokumen proposal...");
                    dialog_error.setMessage("Mohon Hubungi Pihak IT (Team Mobile) untuk menangani hal ini.\n\n\n");
                    dialog_error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog_error.show();

                } else {
                    me.getModelID().setFlag_proposal(1);
                    me.getModelID().setFlag_dokumen(1);
                    new E_Update(getContext()).UpdateSUBMIT(me);
                    // File outputFile = new
                    // File(Environment.getExternalStorageDirectory()+
                    // "/ESPAJ/PROPOSALPDF/"+no_temp+"/"+no_temp+"_PROPOSAL.PDF");
                    // getFragmentManager().beginTransaction()
                    // .replace(R.id.frame_container, new
                    // PdfReaderProposal(outputFile)).addToBackStack(null).commit();
                    File outputFile = new File(Environment.getExternalStorageDirectory() + "/ESPAJ/PROPOSALPDF/"
                            + no_temp + "/" + no_temp + "_PROPOSAL.PDF");
                    StaticMethods.openPDF(getContext(), outputFile);
//
//                    Intent intent = new Intent(Intent.ACTION_VIEW);
//                    intent.setDataAndType(Uri.fromFile(outputFile), "application/pdf");
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                    startActivity(intent);
                }
            }
        }
    }

    private String POSTDOWNLOAD(String url, String no_temp, String spaj) {
        InputStream inputStream = null;
        FileOutputStream outStream = null;
        String result = "";
        try {
            PostMethod httpPost = new PostMethod(url);
            String json = "";
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("no_temp", no_temp);
            jsonObject.put("spaj", spaj);

            final String CODEPAGE = "UTF-8";

            json = jsonObject.toString();

            HttpClient httpClient = new HttpClient();
            httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(90000);
            httpClient.getHttpConnectionManager().getParams().setSoTimeout(90000);

            // set JSON ke hhtp post entity
            StringRequestEntity se = new StringRequestEntity(json, "application/json", null);
            httpPost.setRequestEntity(se);

            httpClient.executeMethod(httpPost);

            File myFilesDir = new File(
                    Environment.getExternalStorageDirectory() + "/ESPAJ/PROPOSALPDF/" + no_temp + "/");
            if (!myFilesDir.exists()) {
                myFilesDir.mkdirs();
            }
            inputStream = httpPost.getResponseBodyAsStream();

            if (inputStream != null) {
                File outputFile = new File(myFilesDir, no_temp + "_PROPOSAL.PDF");
                outStream = new FileOutputStream(outputFile);
                byte[] buffer = new byte[1024];
                int len1 = 0;
                int size = 1;
                while ((len1 = inputStream.read(buffer)) != -1) {
                    // Log.v(LOG_TAG, strDownloadString+size);
                    outStream.write(buffer, 0, len1);
                    size++;

                }
                result = convertInputStreamToString(inputStream);
            } else {
                result = "Did not work!";
            }

            outStream.close();
            inputStream.close();
        } catch (OutOfMemoryError e) {
            Log.e("MEMORY EXCEPTION: ", e.toString());
            result = "Gagal";
        } catch (ConnectTimeoutException e) {
            Log.e("Timeout Exception: ", e.toString());
            result = "Gagal";
        } catch (SocketTimeoutException ste) {
            Log.e("Timeout Exception: ", ste.toString());
            result = "Gagal";
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
            result = "Gagal";
        }

        // 11. return result
        return result;

    }


    private Button btn_lanjut, btn_kembali;
    private ImageView iv_next, iv_prev, iv_save;
    private Toolbar second_toolbar;

    @BindView(R.id.iv_cb_send)
    ImageView iv_cb_send;

    private String response_image = "";

    EspajModel me;

}
