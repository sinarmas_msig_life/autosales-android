
package id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("SL_ID")
    @Expose
    private String sLID;
    @SerializedName("SL_TAB_ID")
    @Expose
    private String sLTABID;
    @SerializedName("SL_NAME")
    @Expose
    private String sLNAME;
    @SerializedName("SL_REMARK")
    @Expose
    private Object sLREMARK;

    public String getSLID() {
        return sLID;
    }

    public void setSLID(String sLID) {
        this.sLID = sLID;
    }

    public String getSLTABID() {
        return sLTABID;
    }

    public void setSLTABID(String sLTABID) {
        this.sLTABID = sLTABID;
    }

    public String getSLNAME() {
        return sLNAME;
    }

    public void setSLNAME(String sLNAME) {
        this.sLNAME = sLNAME;
    }

    public Object getSLREMARK() {
        return sLREMARK;
    }

    public void setSLREMARK(Object sLREMARK) {
        this.sLREMARK = sLREMARK;
    }

}
