package id.co.ajsmsig.nb.pointofsale.ui.lproductfabdescription

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Transformations
import id.co.ajsmsig.nb.pointofsale.binding.LoadingButtonListener
import id.co.ajsmsig.nb.pointofsale.model.Resource
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Analytics
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductBenefit
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductRider
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.ProductDetail
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 08/03/18.
 */
class ProductFABDescriptionVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository, var sharedViewModel: SharedViewModel, var mainRepository: MainRepository): AndroidViewModel(application){

    val observablePageOptions = MediatorLiveData<List<PageOption>>()
    val observableProductDetails = MediatorLiveData<List<ProductDetail>>()
    val observableProductBenefits = MediatorLiveData<Resource<List<ProductBenefit>>>()
    val observableProductRider = MediatorLiveData<Resource<List<ProductRider>>>()
    val observableIsThereRider = Transformations.map(observableProductRider, {
        (it.data?.size ?: 0) > 0
    })

    var page: Page? = null
    set(value) {
        if (field == null) {
            field = value

            val pageOptions = repository.getPageOptionsFromPage(value)
            observablePageOptions.addSource(pageOptions, {

                observablePageOptions.value = it
            })

            sharedViewModel.selectedProduct?.let {

                val product = it

                val productDetails = ArrayList<ProductDetail>()
                product.Sheet?.let { productDetails.add(ProductDetail("Product Sheet", it, eventName = "Product Sheet - ${product.LsbsName}")) }
                product.FundFactSheet?.let { productDetails.add(ProductDetail("Fund Fact Sheet", it, eventName = "Fund Fact Sheet - ${product.LsbsName}")) }
                product.Brosur?.let { productDetails.add(ProductDetail("Brosur", it, eventName = "Brosur - ${product.LsbsName}")) }


                observableProductDetails.postValue(productDetails)

                observableProductBenefits.addSource(mainRepository.getProductBenefitsWith(product.LsbsId, sharedViewModel.groupId), observableProductBenefits::setValue)
                observableProductRider.addSource(mainRepository.getProductRidersWith(it.LsbsId, sharedViewModel.groupId), observableProductRider::setValue)
            }




        }
    }

    val loadingButtonListener = object : LoadingButtonListener {
        override fun onClick(eventName: String?) {
            mainRepository.sendAnalytics(
                    Analytics(
                            agentCode = sharedViewModel.agentCode,
                            agentName = sharedViewModel.agentName,
                            group = sharedViewModel.groupId,
                            event = eventName,
                            timestamp = System.currentTimeMillis()
                    ))
        }

    }
}