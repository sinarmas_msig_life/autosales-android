package id.co.ajsmsig.nb.crm.model.form;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by faiz_f on 11/04/2017.
 */

public class SimbakLinkUsrToLeadModel implements Parcelable {
    private Long UTL_ID       ;
    private Long UTL_LEAD_ID  ;
    private String  UTL_USER_ID  ;

    public SimbakLinkUsrToLeadModel() {
    }

    protected SimbakLinkUsrToLeadModel(Parcel in) {
        UTL_ID = (Long) in.readValue(Integer.class.getClassLoader());
        UTL_LEAD_ID = (Long) in.readValue(Integer.class.getClassLoader());
        UTL_USER_ID = in.readString();
    }

    public static final Creator<SimbakLinkUsrToLeadModel> CREATOR = new Creator<SimbakLinkUsrToLeadModel>() {
        @Override
        public SimbakLinkUsrToLeadModel createFromParcel(Parcel in) {
            return new SimbakLinkUsrToLeadModel(in);
        }

        @Override
        public SimbakLinkUsrToLeadModel[] newArray(int size) {
            return new SimbakLinkUsrToLeadModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(UTL_ID);
        dest.writeValue(UTL_LEAD_ID);
        dest.writeString(UTL_USER_ID);
    }

    public Long getUTL_ID() {
        return UTL_ID;
    }

    public void setUTL_ID(Long UTL_ID) {
        this.UTL_ID = UTL_ID;
    }

    public Long getUTL_LEAD_ID() {
        return UTL_LEAD_ID;
    }

    public void setUTL_LEAD_ID(Long UTL_LEAD_ID) {
        this.UTL_LEAD_ID = UTL_LEAD_ID;
    }

    public String getUTL_USER_ID() {
        return UTL_USER_ID;
    }

    public void setUTL_USER_ID(String UTL_USER_ID) {
        this.UTL_USER_ID = UTL_USER_ID;
    }

}
