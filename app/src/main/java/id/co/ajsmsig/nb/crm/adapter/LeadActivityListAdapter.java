package id.co.ajsmsig.nb.crm.adapter;

import android.media.Image;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.ActivityListModel;
import id.co.ajsmsig.nb.crm.model.TrackPolisModel;

public class LeadActivityListAdapter extends RecyclerView.Adapter<LeadActivityListAdapter.ActivityListVH> {
    private List<ActivityListModel> activities;
    private ClickListener clickListener;

    public LeadActivityListAdapter(List<ActivityListModel> activities) {
        this.activities = activities;
    }

    public class ActivityListVH extends RecyclerView.ViewHolder {
        private TextView tvActivityDate;
        private TextView tvSubActivity;
        private TextView tvActivity;
        private ImageView imgEdit;
        private TextView tvEdit;
        private ImageView ivSyncIndicator;

        public ActivityListVH(View view) {
            super(view);
            tvActivityDate = view.findViewById(R.id.tvActivityDate);
            tvSubActivity = view.findViewById(R.id.tvSubActivity);
            tvActivity = view.findViewById(R.id.tvActivity);
            imgEdit = view.findViewById(R.id.imgEdit);
            tvEdit = view.findViewById(R.id.tvEdit);
            ivSyncIndicator = view.findViewById(R.id.ivSyncIndicator);
        }

    }

    @Override
    public ActivityListVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.c_lead_list_activity, parent, false);
        return new ActivityListVH(itemView);
    }

    @Override
    public void onBindViewHolder(final ActivityListVH holder, int position) {
        ActivityListModel model = activities.get(position);
        String activityDetail = model.getActivityDetail();

        if (!TextUtils.isEmpty(activityDetail)) {
            holder.tvActivity.setText("Catatan : "+model.getActivityDetail());
        } else {
            holder.tvActivity.setText(model.getActivityName());
        }

        if (model.isSynced()){
            holder.ivSyncIndicator.setVisibility(View.INVISIBLE);
        } else {
            holder.ivSyncIndicator.setVisibility(View.VISIBLE);
        }

        holder.tvSubActivity.setText(model.getSubActivityName());
        holder.tvActivityDate.setText(model.getActivityCreatedDate());
        holder.tvEdit.setOnClickListener(v -> clickListener.onActivitySelected(holder.getAdapterPosition(), model));
        holder.imgEdit.setOnClickListener(v -> clickListener.onActivitySelected(holder.getAdapterPosition(), model));
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onActivitySelected(int position, ActivityListModel model);
    }

    public void addAll(List<ActivityListModel> activityList) {
        for (ActivityListModel activity : activityList) {
            activities.add(activity);
        }
        notifyDataSetChanged();
    }
}
