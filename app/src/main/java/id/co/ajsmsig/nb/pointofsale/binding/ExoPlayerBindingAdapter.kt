package id.co.ajsmsig.nb.pointofsale.binding

import android.arch.lifecycle.MediatorLiveData
import android.content.Context
import android.databinding.*
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.downloader.*
import com.downloader.utils.Utils
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosRowQuestionBinding
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.RiskProfileQuestionnaireAllAnswers
import id.co.ajsmsig.nb.pointofsale.utils.Constants
import id.co.ajsmsig.nb.pointofsale.utils.DownloaderUtil
import id.co.ajsmsig.nb.pointofsale.utils.fileName
import java.io.File
import java.util.*
import org.wlf.filedownloader.FileDownloader
import org.wlf.filedownloader.FileDownloadConfiguration
import android.os.Environment.getExternalStorageDirectory
import org.wlf.filedownloader.DownloadFileInfo
import org.wlf.filedownloader.listener.OnDetectBigUrlFileListener
import org.wlf.filedownloader.listener.OnFileDownloadStatusListener


@BindingAdapter("url", "onDownloadVideoListener", "start", "playerListener")
fun setURLForVideo(playerView: SimpleExoPlayerView, url: ObservableField<String>, onDownloadListener: DownloadListener, start: ObservableBoolean, playerListener: Player.EventListener) {

    val link = url.get()?.replace("~", Constants.API_URL)
    val rootPath = DownloaderUtil.getRootDirPath(playerView.context)

    if (url.get() == null) {
        onDownloadListener.onError(Error())
        return
    }

    val player = playerView.player as? SimpleExoPlayer ?: ExoPlayerFactory.newSimpleInstance(DefaultRenderersFactory(playerView.context), DefaultTrackSelector(), DefaultLoadControl())
    if (playerView.player == null)
        playerView.player = player

    player?.playWhenReady = start.get()
    player?.seekTo(0, 0)

    val savedFile = File(rootPath, url.get()?.fileName())

    if (savedFile.exists()) {

        val uri = Uri.fromFile(savedFile)

        val mediaSource = ExtractorMediaSource.Factory(
                DefaultDataSourceFactory(playerView.context,"exoplayer-codelab")).createMediaSource(uri)

        player?.removeListener(playerListener)
        player?.addListener(playerListener)

        player?.prepare(mediaSource, true, false)
    } else {

        FileDownloader.registerDownloadStatusListener(object : OnFileDownloadStatusListener {
            override fun onFileDownloadStatusCompleted(downloadFileInfo: DownloadFileInfo?) {
                onDownloadListener.onDownloadComplete()
                setURLForVideo(playerView, url, onDownloadListener, start, playerListener)
            }

            override fun onFileDownloadStatusWaiting(downloadFileInfo: DownloadFileInfo?) {

            }

            override fun onFileDownloadStatusPreparing(downloadFileInfo: DownloadFileInfo?) {

            }

            override fun onFileDownloadStatusDownloading(downloadFileInfo: DownloadFileInfo?, downloadSpeed: Float, remainingTime: Long) {
                if (downloadFileInfo == null) return
                onDownloadListener.onProgress(Progress(downloadFileInfo.downloadedSizeLong, downloadFileInfo.fileSizeLong))
            }

            override fun onFileDownloadStatusFailed(url: String?, downloadFileInfo: DownloadFileInfo?, failReason: OnFileDownloadStatusListener.FileDownloadStatusFailReason?) {
                onDownloadListener.onError(Error())
            }

            override fun onFileDownloadStatusPrepared(downloadFileInfo: DownloadFileInfo?) {
                onDownloadListener.onStartOrResume()
            }

            override fun onFileDownloadStatusPaused(downloadFileInfo: DownloadFileInfo?) {
                onDownloadListener.onPause()
            }

        })

        FileDownloader.detect(link, object : OnDetectBigUrlFileListener {
            override fun onDetectUrlFileFailed(url: String?, failReason: OnDetectBigUrlFileListener.DetectBigUrlFileFailReason?) {
                Toast.makeText(playerView.context, failReason?.message, Toast.LENGTH_SHORT).show()
            }

            override fun onDetectNewDownloadFile(url: String?, fileName: String?, saveDir: String?, fileSize: Long) {
                // here to change to custom fileName, saveDir if needed
                FileDownloader.createAndStart(url, rootPath, url.fileName())
            }

            override fun onDetectUrlFileExist(url: String?) {
                // continue to download
                FileDownloader.start(url)
            }

        })

    }

}

interface DownloadListener {
    fun onStartOrResume()
    fun onPause()
    fun onCancel()
    fun onDownloadComplete()
    fun onError(error: Error?)
    fun onProgress(progress: Progress?)
}
