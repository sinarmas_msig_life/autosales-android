package id.co.ajsmsig.nb.pointofsale.ui.dneedanalysis

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Observer
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.ProductRecommendation
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */

class NeedAnalysisVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository, var sharedViewModel: SharedViewModel): AndroidViewModel(application) {

    var observablePageOptions: MediatorLiveData<List<PageOption>> = MediatorLiveData()
    var row = 3

    var listOrOneCard: ObservableBoolean = ObservableBoolean(false)
    var additionalText = ObservableField<String>()
    var needAnalysisText = ObservableField<String>()
    var needAnalysisImageName = ObservableField<String>()

    var page: Page? = null
    set(value) {


        val pageOptions = repository.getPageOptionsFromPage(value, sharedViewModel.selectedPriorityNeed?.id)
        observablePageOptions.addSource(pageOptions, Observer {
            it?.let {
                observablePageOptions.value = it


                listOrOneCard.set(sharedViewModel.selectedPriorityNeed == null)
                it.firstOrNull { it.id == sharedViewModel.selectedNeedAnalysis?.id }?.selected?.set(true)
                if (sharedViewModel.selectedPriorityNeed != null) {
                    additionalText.set(value?.additionalText)
                    needAnalysisText.set(it.firstOrNull()?.title)
                    needAnalysisImageName.set(it.firstOrNull()?.imageName)
                    it.firstOrNull()?.selected?.set(true)
                }



                // Disable some page option when no recommendation product found
                it.forEach {
                    val productRecommendations = repository.getProductRecommendationWithGroupIdAndSelectedNeedAnalysisId(sharedViewModel.groupId, it.id)
                    productRecommendations.observeForever(object : Observer<List<ProductRecommendation>> {
                        override fun onChanged(t: List<ProductRecommendation>?) {

                            it.disabled.set(t?.isEmpty() == true)
                            productRecommendations.removeObserver(this)
                        }

                    })
                }
            }
        })
    }

}