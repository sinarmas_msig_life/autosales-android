package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.*

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */
class HandlingObjectionAllQuestions {

        @Embedded
        var handlingObjection: HandlingObjection? = null

        @Relation(entity = HandlingObjection::class, parentColumn = "id", entityColumn = "parentId")
        var questions: List<HandlingObjection>? = null
}