package id.co.ajsmsig.nb.pointofsale.ui.handlingobjection

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.HandlingObjection
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 08/03/18.
 */
class HandlingObjectionListVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository): AndroidViewModel(application){

    var observableHandlingObjectionCategories = MediatorLiveData<List<HandlingObjection>>()

    var page: Page? = null
    set(value) {
        field = value

        val handlingObjectionCategories = repository.getHandlingObjectionCategories()
        observableHandlingObjectionCategories.addSource(handlingObjectionCategories, observableHandlingObjectionCategories::setValue)

    }
}