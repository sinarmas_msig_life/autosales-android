package id.co.ajsmsig.nb.crm.model;

public class TrackPolisModel {

    private int lssaId;
    private String mspsDesc;
    private String mspsDate;
    private String statusPolis;
    private String statusAccept;
    private String regSpaj;
    private int lsspId;


    public TrackPolisModel(int lssaId, String mspsDesc, String mspsDate, String statusPolis, String statusAccept, String regSpaj, int lsspId) {
        this.lssaId = lssaId;
        this.mspsDesc = mspsDesc;
        this.mspsDate = mspsDate;
        this.statusPolis = statusPolis;
        this.statusAccept = statusAccept;
        this.regSpaj = regSpaj;
        this.lsspId = lsspId;
    }

    public int getLssaId() {
        return lssaId;
    }

    public void setLssaId(int lssaId) {
        this.lssaId = lssaId;
    }

    public String getMspsDesc() {
        return mspsDesc;
    }

    public void setMspsDesc(String mspsDesc) {
        this.mspsDesc = mspsDesc;
    }

    public String getMspsDate() {
        return mspsDate;
    }

    public void setMspsDate(String mspsDate) {
        this.mspsDate = mspsDate;
    }

    public String getStatusPolis() {
        return statusPolis;
    }

    public void setStatusPolis(String statusPolis) {
        this.statusPolis = statusPolis;
    }

    public String getStatusAccept() {
        return statusAccept;
    }

    public void setStatusAccept(String statusAccept) {
        this.statusAccept = statusAccept;
    }

    public String getRegSpaj() {
        return regSpaj;
    }

    public void setRegSpaj(String regSpaj) {
        this.regSpaj = regSpaj;
    }

    public int getLsspId() {
        return lsspId;
    }

    public void setLsspId(int lsspId) {
        this.lsspId = lsspId;
    }
}
