
package id.co.ajsmsig.nb.prop.model.apimodel.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Virtual {

    @SerializedName("NO_VA")
    @Expose
    private String nOVA;

    public String getNOVA() {
        return nOVA;
    }

    public void setNOVA(String nOVA) {
        this.nOVA = nOVA;
    }

}
