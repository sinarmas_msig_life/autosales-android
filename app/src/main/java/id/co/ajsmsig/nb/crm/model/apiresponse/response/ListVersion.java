
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListVersion {

    @SerializedName("DATA_VERSION")
    @Expose
    private String dATAVERSION;
    @SerializedName("LAV_DATA_ID")
    @Expose
    private int lAVDATAID;

    public String getDATAVERSION() {
        return dATAVERSION;
    }

    public void setDATAVERSION(String dATAVERSION) {
        this.dATAVERSION = dATAVERSION;
    }

    public int getLAVDATAID() {
        return lAVDATAID;
    }

    public void setLAVDATAID(int lAVDATAID) {
        this.lAVDATAID = lAVDATAID;
    }

}
