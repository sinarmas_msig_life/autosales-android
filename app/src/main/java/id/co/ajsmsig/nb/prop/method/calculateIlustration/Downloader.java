package id.co.ajsmsig.nb.prop.method.calculateIlustration;

import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
  Created by faiz_f on 26/10/2016.
 */

public class Downloader {

    private static final String TAG = "Downloader";

    public static int DownloadFile(String fileURL, File file) {
        int status = 0;
        long startTime = System.nanoTime();
        InputStream in = null;
        HttpURLConnection c = null;
//        fileURL = "https://www.tarsnap.com/scrypt/scrypt-slides.pdf";
        try {
            URL u = new URL(fileURL);

            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(false);
//            c.setRequestProperty("User-agent", "");
            c.setReadTimeout(20000);
            c.setConnectTimeout(20000);
            c.connect();

            int responseCode = c.getResponseCode();
            Log.i(TAG, "DownloadFile ResponseCode:" + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                status = 1;
                FileOutputStream f = new FileOutputStream(file);

                in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1;
                while ((len1 = in.read(buffer)) > 0) {
                    f.write(buffer, 0, len1);
                }
                f.close();

                Log.i(TAG, "Download Folder Path: " + file.getAbsolutePath());
                Log.i(TAG, "DownloadFile: File " + file.exists());
                if (!file.exists()) status = 2;
            } else if (responseCode >= 400 && responseCode < 500) {
//                status 2, it means clien is the problem
                status = 2;
            } else if (responseCode >= 500 && responseCode < 600) {
                // status 3, it mean the server is the problem.
                status = 3;
            }

        } catch (Exception e) {
            Log.i(TAG, "Exception :" + e.getMessage());
            status = 5;
        } finally {
            if (in != null) {
                try {
                    in.close();
                    long endTime = System.nanoTime();
                    Log.i(TAG, "close inputstream took " + (endTime - startTime) / 1000000);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (c != null) {
                c.disconnect();
            }

        }
        return status;
    }

}
