package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageScenario


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface PageScenarioDao {
    @Query("select * from PageScenario")
    fun loadAllPageScenarios(): LiveData<List<PageScenario>>


    @Query("select * from PageScenario where pageId = :pageId and optionId = :optionId limit 1")
    fun loadPageScenario(pageId: Int, optionId: Int): LiveData<PageScenario>

    @Query("select * from Page where Page.id = (select nextPageId from PageScenario where pageId = :pageId and optionId = :optionId limit 1)")
    fun loadNextPage(pageId: Int?, optionId: Int?): LiveData<Page>
}