package id.co.ajsmsig.nb.crm.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import id.co.ajsmsig.nb.crm.fragment.C_ListActivityFragment;
import id.co.ajsmsig.nb.crm.fragment.C_ListProposalFragment;

/**
 * Created by faiz_f on 15/03/2017.
 */

public class C_ProspectPagerAdapter extends FragmentPagerAdapter {
    private static CharSequence[] Titles = new CharSequence[]{
            "Aktivitas",
            "Proposal"
    };

    public C_ProspectPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new C_ListActivityFragment();
                break;
            case 1:
                fragment = new C_ListProposalFragment();
                break;
            default:
                fragment = new C_ListProposalFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return Titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

}
