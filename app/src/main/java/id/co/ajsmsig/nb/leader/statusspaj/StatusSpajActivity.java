package id.co.ajsmsig.nb.leader.statusspaj;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.leader.statusspajdetail.StatusSpajDetailActivity;
import id.co.ajsmsig.nb.leader.subordinatedetail.PostCount;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.DateUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StatusSpajActivity extends AppCompatActivity implements StatusSpajAdapter.OnItemPressed {

    private ArrayList<SpajData> statusList = new ArrayList<>();

    private RecyclerView recyclerView;
    private ConstraintLayout layoutSpajNotFound;
    private TextView tvErrorMessage;
    private ProgressBar progressBar;
    private Button btnRetry;
    private StatusSpajAdapter mAdapter;
    private ConstraintLayout layoutNoInternetConnection;
    private int selectedMode;
    private String msagId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_spaj);

        Intent intent = getIntent();
        msagId = intent.getStringExtra(Const.INTENT_KEY_AGENT_CODE);
        selectedMode = intent.getIntExtra(Const.INTENT_KEY_REPORT_MODE, 0);

        initToolbar();
        initView();
        initRecyclerView();

        if (StaticMethods.isNetworkAvailable(this)) {
            hideNoConnection();
            fetchSpajListFromServer(selectedMode, msagId);
        } else {
            showNoConnection();
        }
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String statusName = getSelectedStatusName(selectedMode);

        if (getSupportActionBar() != null && !TextUtils.isEmpty(statusName)) {
            getSupportActionBar().setTitle(statusName);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                onBackPressed();
            });
        }
    }

    private void initView() {
        progressBar = findViewById(R.id.progressBar);
        layoutSpajNotFound = findViewById(R.id.layoutSpajNotFound);
        tvErrorMessage = findViewById(R.id.tvErrorMessage);
        layoutNoInternetConnection = findViewById(R.id.layoutNoInternetConnection);
        btnRetry = findViewById(R.id.btnRetry);
        btnRetry.setOnClickListener(v -> {
            if (StaticMethods.isNetworkAvailable(StatusSpajActivity.this)) {
                fetchSpajListFromServer(selectedMode, msagId);
                hideNoConnection();
            } else {
                showNoConnection();
                Toast.makeText(StatusSpajActivity.this, "Anda tidak terkoneksi ke internet. Mohon pastikan Anda terkoneksi dengan internet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recyclerViewStatus);
        mAdapter = new StatusSpajAdapter(statusList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    private void fetchSpajListFromServer(int selectedMode, String msagId) {
        showLoading();
        String url = AppConfig.getBaseUrlSPAJ().concat("spaj/api/json/getallspajlist");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        PostCount requestPostMenu = new PostCount(selectedMode, msagId);
        Call<GetAllSpajResponse> call = api.postSpajList(url, requestPostMenu);
        call.enqueue(new Callback<GetAllSpajResponse>() {
            @Override
            public void onResponse(Call<GetAllSpajResponse> call, Response<GetAllSpajResponse> response) {
                GetAllSpajResponse responseBody = response.body();

                if (responseBody != null) {
                    boolean error = responseBody.isError();
                    String message = responseBody.getMessage();

                    Data data = responseBody.getData();

                    if (!error) {
                        insertAllSpajStatusIntoNewBucket(data);
                    } else {
                        Toast.makeText(StatusSpajActivity.this, "Tidak dapat menampilkan list SPAJ " + message, Toast.LENGTH_SHORT).show();
                    }

                }

                hideLoading();

            }

            @Override
            public void onFailure(Call<GetAllSpajResponse> call, Throwable t) {
                Toast.makeText(StatusSpajActivity.this, "Tidak dapat menampilkan list SPAJ " + t.getMessage(), Toast.LENGTH_SHORT).show();
                hideLoading();
            }
        });
    }


    private void insertAllSpajStatusIntoNewBucket(Data data) {
        List<SpajData> spajStatusList = new ArrayList<>();
        String statusName = getSelectedStatusName(selectedMode);

        //All submission
        for (SpajData model : data.getAllSubmission()) {

            String lusFullName = model.getLusFullName();
            String ldeDept = model.getLdeDept();
            String regSpaj = model.getRegSpaj();
            String ppName = model.getPpName();
            String lsdbsName = model.getLsdbsName();
            String mspsDesc = model.getMspsDesc();
            String mspsDate = model.getMspsDate();
            String noTemp = model.getNoTemp();

            String processDate = DateUtils.getSPAJProcessDate(mspsDate);

            spajStatusList.add(new SpajData(lusFullName, ldeDept, regSpaj, ppName, lsdbsName, mspsDesc, processDate, noTemp));
        }

        //Cancelled
        for (SpajData model : data.getCancelled()) {

            String lusFullName = model.getLusFullName();
            String ldeDept = model.getLdeDept();
            String regSpaj = model.getRegSpaj();
            String ppName = model.getPpName();
            String lsdbsName = model.getLsdbsName();
            String mspsDesc = model.getMspsDesc();
            String mspsDate = model.getMspsDate();
            String noTemp = model.getNoTemp();

            String processDate = DateUtils.getSPAJProcessDate(mspsDate);

            spajStatusList.add(new SpajData(lusFullName, ldeDept, regSpaj, ppName, lsdbsName, mspsDesc, processDate, noTemp));
        }

        //Further requirement
        for (SpajData model : data.getFurtherRequirement()) {

            String lusFullName = model.getLusFullName();
            String ldeDept = model.getLdeDept();
            String regSpaj = model.getRegSpaj();
            String ppName = model.getPpName();
            String lsdbsName = model.getLsdbsName();
            String mspsDesc = model.getMspsDesc();
            String mspsDate = model.getMspsDate();
            String noTemp = model.getNoTemp();

            String processDate = DateUtils.getSPAJProcessDate(mspsDate);

            spajStatusList.add(new SpajData(lusFullName, ldeDept, regSpaj, ppName, lsdbsName, mspsDesc, processDate, noTemp));
        }

        //onProcess
        for (SpajData model : data.getOnProcess()) {

            String lusFullName = model.getLusFullName();
            String ldeDept = model.getLdeDept();
            String regSpaj = model.getRegSpaj();
            String ppName = model.getPpName();
            String lsdbsName = model.getLsdbsName();
            String mspsDesc = model.getMspsDesc();
            String mspsDate = model.getMspsDate();
            String noTemp = model.getNoTemp();

            String processDate = DateUtils.getSPAJProcessDate(mspsDate);

            spajStatusList.add(new SpajData(lusFullName, ldeDept, regSpaj, ppName, lsdbsName, mspsDesc, processDate, noTemp));
        }

        //Inforce
        for (SpajData model : data.getInforce()) {

            String lusFullName = model.getLusFullName();
            String ldeDept = model.getLdeDept();
            String regSpaj = model.getRegSpaj();
            String ppName = model.getPpName();
            String lsdbsName = model.getLsdbsName();
            String mspsDesc = model.getMspsDesc();
            String mspsDate = model.getMspsDate();
            String noTemp = model.getNoTemp();

            String processDate = DateUtils.getSPAJProcessDate(mspsDate);

            spajStatusList.add(new SpajData(lusFullName, ldeDept, regSpaj, ppName, lsdbsName, mspsDesc, processDate, noTemp));
        }

        StatusWithListSpajCount spajData = new StatusWithListSpajCount(statusName, spajStatusList);


        displaySpajlist(spajData);
    }

    private String getSelectedStatusName(int selectedMode) {
        if (selectedMode == Const.ALL_SUBMIT) {
            return "All Submit";
        }

        if (selectedMode == Const.ON_PROCESS) {
            return "On process";
        }

        if (selectedMode == Const.PENDING) {
            return "Pending";
        }

        if (selectedMode == Const.CANCELLED) {
            return "Cancelled";
        }

        if (selectedMode == Const.INFORCE) {
            return "Inforce";
        }

        return "";

    }

    private void displaySpajlist(StatusWithListSpajCount spajData) {
        String statusName = spajData.getStatusName();
        int spajCount = spajData.getStatusSpajData().size();

        //Tidak ada spaj yg ditampilkan dengan status yg dipilih
        if (isSpajEmpty(spajCount)) {
            //Tampilkan layout tidak ada spaj
            showNoSpajFound(statusName);
        } else {
            hideNoSpajFound();
            //Hide layout tidak ada spaj
            mAdapter.refreshSpajList(spajData.getStatusSpajData());
        }

    }

    private boolean isSpajEmpty(int spajCount) {
        return spajCount == 0;
    }

    private void showNoSpajFound(String statusName) {
        layoutSpajNotFound.setVisibility(View.VISIBLE);
        String lowerCaseStatusName = statusName.toLowerCase();
        String message = "Tidak ada SPAJ ditemukan untuk status " + lowerCaseStatusName;
        tvErrorMessage.setText(message);
    }


    private void hideNoSpajFound() {
        layoutSpajNotFound.setVisibility(View.GONE);
        tvErrorMessage.setText(null);
    }

    private void showNoConnection() {
        layoutNoInternetConnection.setVisibility(View.VISIBLE);
    }

    private void hideNoConnection() {
        layoutNoInternetConnection.setVisibility(View.GONE);
    }

    private void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onStatusSpajPressed(int position, SpajData spajData) {
        Intent intent = new Intent(StatusSpajActivity.this, StatusSpajDetailActivity.class);
        String spajNo = spajData.getNoTemp();
        intent.putExtra(Const.INTENT_KEY_SPAJ_NO_TEMP, spajNo);
        startActivity(intent);
    }
}
