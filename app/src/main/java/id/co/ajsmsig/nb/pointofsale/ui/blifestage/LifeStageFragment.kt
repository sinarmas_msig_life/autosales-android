package id.co.ajsmsig.nb.pointofsale.ui.blifestage


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosFragmentLifeStageBinding
import id.co.ajsmsig.nb.pointofsale.ui.fragment.SingleOptionFragment
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption


/**
 * A simple [Fragment] subclass.
 */
class LifeStageFragment : SingleOptionFragment() {

    private lateinit var dataBinding: PosFragmentLifeStageBinding

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(LifeStageVM::class.java)
        subscribeUi(viewModel)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_life_stage, container, false)

        return dataBinding.root
    }

    private fun subscribeUi(viewModel: LifeStageVM) {
        dataBinding.vm = viewModel

        viewModel.page = page
        viewModel.observablePageOptions.observe(this, Observer {
            it?.let { it.firstOrNull { it.id == sharedViewModel.selectedLifeStage?.id }?.selected?.set(true) }
        })

        // No Subtitle For this page
        mainVM.hideSubtitle()

    }

    override fun onDestroy() {
        super.onDestroy()

        sharedViewModel.selectedLifeStage = null
    }


    override fun optionSelected(): PageOption? {
        val viewModel = ViewModelProviders.of(this).get(LifeStageVM::class.java)
        val selectedOption = viewModel.observablePageOptions.value?.firstOrNull { it.selected.get() }
        sharedViewModel.selectedLifeStage = selectedOption

        return selectedOption
    }

}// Required empty public constructor
