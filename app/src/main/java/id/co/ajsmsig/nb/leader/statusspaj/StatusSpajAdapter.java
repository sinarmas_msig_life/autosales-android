package id.co.ajsmsig.nb.leader.statusspaj;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.co.ajsmsig.nb.R;

public class StatusSpajAdapter extends RecyclerView.Adapter<StatusSpajAdapter.MyViewHolder> {
    private List<SpajData> statuslistSpaj;
    private OnItemPressed listener;

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNoSpaj;
        private TextView tvDateProcess;
        private TextView tvPolicyHolderName;
        private TextView tvProductName;
        private TextView tvDescriptionProcess;
        private TextView tvDivisionProcess;
        private ConstraintLayout constraintLayoutStatus;

        public MyViewHolder(View view) {
            super(view);
            tvNoSpaj = view.findViewById(R.id.tvNoSpaj);
            tvDateProcess = view.findViewById(R.id.tvDateProcess);
            tvPolicyHolderName = view.findViewById(R.id.tvPolicyHolderName);
            tvProductName = view.findViewById(R.id.tvProductName);
            tvDescriptionProcess = view.findViewById(R.id.tvDescriptionProcess);
            tvDivisionProcess = view.findViewById(R.id.tvDivisionProcess);
            constraintLayoutStatus = view.findViewById(R.id.constraintLayoutStatus);
        }

    }

    public StatusSpajAdapter(ArrayList<SpajData> statuslistSpaj, OnItemPressed listener) {
        this.statuslistSpaj = statuslistSpaj;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_status_spaj, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        SpajData model = statuslistSpaj.get(position);
        holder.tvNoSpaj.setText(model.getNoTemp());
        holder.tvDateProcess.setText(model.getMspsDate());
        holder.tvPolicyHolderName.setText("Pemegang polis : \n"+model.getPpName());
        holder.tvProductName.setText(model.getLsdbsName());
        holder.tvDescriptionProcess.setText(model.getMspsDesc());
        holder.tvDivisionProcess.setText(model.getLusFullName() + " - "+model.getLdeDept());
        holder.constraintLayoutStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onStatusSpajPressed(holder.getAdapterPosition(), model);
            }
        });
    }

    @Override
    public int getItemCount() {

        return statuslistSpaj.size();
    }

    interface OnItemPressed {
        void onStatusSpajPressed(int position, SpajData spajData);
    }


    public void refreshSpajList(List<SpajData> newStatus) {
        //Hapus list yg lama
        statuslistSpaj.clear();
//            //Update dengan data yg baru
        statuslistSpaj = newStatus;
//            //Refresh recyclerview
        notifyDataSetChanged();
    }

}
