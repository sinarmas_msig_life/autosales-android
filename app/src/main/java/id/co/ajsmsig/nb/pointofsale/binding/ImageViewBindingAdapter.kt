package id.co.ajsmsig.nb.pointofsale.binding

import android.databinding.BindingAdapter
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import id.co.ajsmsig.nb.pointofsale.custom.LoadingButton
import id.co.ajsmsig.nb.pointofsale.custom.LoadingImageView
import id.co.ajsmsig.nb.pointofsale.utils.Constants

/**
 * Created by andreyyoshuamanik on 11/03/18.
 */

@BindingAdapter("android:src")
fun setImageViewResource(view: ImageView, resId : Int) {
    view.setImageResource(resId)
}


@BindingAdapter("android:srcBitmap")
fun setImageViewBitmap(view: ImageView, bitmap: Bitmap?) {
    view.setImageBitmap(bitmap)
}

@BindingAdapter("android:srcAssets", "forceDownload", requireAll = false)
fun setImageViewAssets(view: ImageView, imageName: String?, forceDownload: Boolean? = true) {
    imageName?.let {
        if (it != "") {
            var request = Glide.with(view.context)
                    .load(Uri.parse("file:///android_asset/$it"))

            if (it.contains("~")) {
                 request = Glide.with(view.context)
                         .load(Uri.parse(it.replace("~", Constants.API_URL)))
            }
            if (forceDownload == true) {
                val requestOptions = RequestOptions()

                request.apply(requestOptions.skipMemoryCache(true))
//                        .apply(requestOptions.pl)
                        .apply(requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE))
                        .into(view)
            } else {
                request.into(view)
            }
        }
    }
}

@BindingAdapter("android:srcAssets", "forceDownload", requireAll = false)
fun setLoadingImageViewAssets(view: LoadingImageView, imageName: String?, forceDownload: Boolean? = true) {
    imageName?.let {
        if (it != "") {
            var request = Glide.with(view.context)
                    .load(Uri.parse("file:///android_asset/$it"))

            if (it.contains("~")) {
                 request = Glide.with(view.context)
                         .load(Uri.parse(it.replace("~", Constants.API_URL)))
            }
            if (forceDownload == true) {
                val requestOptions = RequestOptions()

                request.apply(requestOptions.skipMemoryCache(true))
//                        .apply(requestOptions.pl)
                        .apply(requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE))
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                view.dismissProgress()
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                view.dismissProgress()
                                return false
                            }

                        })
                        .into(view.imageView)
            } else {
                request
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                view.dismissProgress()
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                view.dismissProgress()
                                return false
                            }

                        })
                        .into(view.imageView)
            }
        }
    }
}



@BindingAdapter("android:srcAssets", "forceDownload", requireAll = false)
fun setImageViewAssets(view: LoadingButton, imageName: String?, forceDownload: Boolean? = true) {
    imageName?.let {
        if (it != "") {
            var request = Glide.with(view.context)
                    .load(Uri.parse("file:///android_asset/$it"))

            if (it.contains("~")) {
                 request = Glide.with(view.context)
                         .load(Uri.parse(it.replace("~", Constants.API_URL)))
            }
            if (forceDownload == true) {
                val requestOptions = RequestOptions()

                request.apply(requestOptions.skipMemoryCache(true))
                        .apply(requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE))
                        .into(view.imageView)
            } else {
                request.into(view.imageView)
            }
        }
    }
}

