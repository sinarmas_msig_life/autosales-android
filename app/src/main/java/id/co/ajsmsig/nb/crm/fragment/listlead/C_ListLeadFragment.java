package id.co.ajsmsig.nb.crm.fragment.listlead;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.activity.C_FormSimpleLeadActivity;
import id.co.ajsmsig.nb.crm.activity.C_LeadActivity;
import id.co.ajsmsig.nb.crm.activity.C_MainActivity;

import id.co.ajsmsig.nb.crm.activity.C_SearchLeadActivity;

import id.co.ajsmsig.nb.crm.database.C_Delete;
import id.co.ajsmsig.nb.crm.database.C_Insert;
import id.co.ajsmsig.nb.crm.database.C_Select;

import id.co.ajsmsig.nb.crm.database.C_Update;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.method.async.CrmRestClientUsage;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.DownloadLeadResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.LeadActivityList;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.DateUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by faiz_f on 03/02/2017.
 */

public class C_ListLeadFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<Cursor>,
        DialogInterface.OnClickListener,
        C_LeadAdapter.ClickListener, CrmRestClientUsage.UploadCrmListener {

    private static final String TAG = C_ListLeadFragment.class.getSimpleName();
    public C_MainActivity activity;
    private String kodeAgen;
    private int jenisLoginBC;
    private Cursor cursor;
    @BindView(R.id.expRecyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.rootLayoutLead)
    ConstraintLayout rootLayoutLead;

    @BindView(R.id.layoutLoading)
    RelativeLayout layoutLoading;

    @BindView(R.id.layoutNoLeadFound)
    LinearLayout layoutNoLeadFound;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.tvLoadingMessage)
    TextView tvLoadingMessage;

    private final int LOADER_SORT_LEAD_BY_DATE = 100;
    private final int LOADER_SORT_LEAD_BY_NAME = 200;
    private final int LOADER_SORT_LEAD_BY_CURRENT_BRANCH = 300;
    private final int SORT_BY_DATE = 0; //Default
    private final int SORT_BY_NAME = 1;
    private final int SORT_BY_CURRENT_BRANCH = 2;
    private C_LeadAdapter mAdapter;
    private int selectedLeadSortMode;
    private List<ParentLeadList> parents;
    private List<ChildLeadList> leads;
    private List<ChildLeadList> favoriteLeads;
    private C_Insert insert;
    private C_Update update;
    private C_Select select;
    private C_Delete delete;
    private ProgressDialog progressDialog;

    public C_ListLeadFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        activity = (C_MainActivity) getContext();
        SharedPreferences sharedPreferences = activity.getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
        kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
        jenisLoginBC = sharedPreferences.getInt("JENIS_LOGIN_BC", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.c_fragment_list_lead, container, false);
        ButterKnife.bind(this, view);
        initRecyclerView();

        insert = new C_Insert(getActivity());
        select = new C_Select(getActivity());
        update = new C_Update(getActivity());
        delete = new C_Delete(getActivity());

        if (StaticMethods.isNetworkAvailable(getActivity())) {
            fetchLeadFromServer();
            getLoaderManager().initLoader(LOADER_SORT_LEAD_BY_DATE, null, this);
        } else {
            getLoaderManager().initLoader(LOADER_SORT_LEAD_BY_DATE, null, this);
        }


        return view;
    }

    private void initRecyclerView() {
        parents = new ArrayList<>();
        leads = new ArrayList<>();
        favoriteLeads = new ArrayList<>();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
    }


    @OnClick(R.id.fab)
    void onFabAddLeadPressed() {
        Intent intent = new Intent(getContext(), C_FormSimpleLeadActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnAddLead)
    void onButtonAddLeadPressed() {
        Intent intent = new Intent(getContext(), C_FormSimpleLeadActivity.class);
        startActivity(intent);
    }

    /**
     * Download new lead after a specific date
     */
    private void fetchLeadFromServer() {

        String afterDate = new C_Select(getActivity()).getLatestLeadInputDate(kodeAgen);
        afterDate = DateUtils.addASecondFrom(afterDate);
        Log.v(TAG, "Downloading lead after " + afterDate);
        //afterDate = "2018-06-12 09:57:31";

        if (TextUtils.isEmpty(afterDate)) {
            //There are no lead saved on the database. Fetch all lead from server
            downloadAllLead(kodeAgen, false, "multiple", "SL_CRTD_DATE", "desc", true, null);
        } else {
            //There are already some lead on the database, fetch only new lead after the specified date
            downloadLead(kodeAgen, 75, "multiple", "SL_CRTD_DATE", "desc", true, afterDate);
        }

    }

    private void downloadAllLead(String msagId, boolean useLimit, String returnType, String sortBy, String sortDirection, boolean includeAddress, String afterDate) {
        showProgressDialogDownloadAllLead("Mengunduh lead", "Mengunduh semua data lead Anda. Proses ini dapat berlangsung selama beberapa menit. Mohon tunggu");
        String url = AppConfig.getBaseUrlCRM().concat("v2/toll/module_lead2?");
        afterDate = DateUtils.addASecondFrom(afterDate);
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<List<DownloadLeadResponse>> call = api.downloadAllLead(url, msagId, useLimit, returnType, sortBy, sortDirection, includeAddress, afterDate);
        call.enqueue(new Callback<List<DownloadLeadResponse>>() {
            @Override
            public void onResponse(Call<List<DownloadLeadResponse>> call, Response<List<DownloadLeadResponse>> response) {
                dismissProgressDialogDownloadAllLead();
                hideLoadingIndicator();
                layoutNoLeadFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                if (response.body() != null) {
                    List<DownloadLeadResponse> leadList = response.body();

                    int leadCount = leadList.size();

                    if (!leadList.isEmpty()) {
                        Log.v(TAG, "Downloading lead. New lead found. Count : " + leadCount);
                        insertToDatabase(leadList);

                        //Refresh the data
                        getLoaderManager().restartLoader(LOADER_SORT_LEAD_BY_DATE, null, C_ListLeadFragment.this);
                    }

                    downloadAllActivity(kodeAgen, false, "SLA_CRTD_DATE", "desc", null);
                }

            }

            @Override
            public void onFailure(Call<List<DownloadLeadResponse>> call, Throwable t) {
                dismissProgressDialogDownloadAllLead();
                hideLoadingIndicator();
                Toast.makeText(getActivity(), getString(R.string.unable_to_download_new_lead) + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.v(TAG, getString(R.string.unable_to_download_new_lead) + t.getLocalizedMessage());
            }
        });
    }
    private void downloadLead(String msagId, int limit, String returnType, String sortBy, String sortDirection, boolean includeAddress, String afterDate) {
        showLoadingIndicator("Mengunduh data lead terbaru.\nMohon tunggu");
        String url = AppConfig.getBaseUrlCRM().concat("v2/toll/module_lead2?");
        afterDate = DateUtils.addASecondFrom(afterDate);
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<List<DownloadLeadResponse>> call = api.downloadLatestLead(url, msagId, limit, returnType, sortBy, sortDirection, includeAddress, afterDate);
        call.enqueue(new Callback<List<DownloadLeadResponse>>() {
            @Override
            public void onResponse(Call<List<DownloadLeadResponse>> call, Response<List<DownloadLeadResponse>> response) {
                dismissProgressDialogDownloadAllLead();
                hideLoadingIndicator();
                layoutNoLeadFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                if (response.body() != null) {
                    List<DownloadLeadResponse> leadList = response.body();

                    int leadCount = leadList.size();

                    if (!leadList.isEmpty()) {
                        Log.v(TAG, "Downloading lead. New lead found. Count : " + leadCount);
                        insertToDatabase(leadList);

                        //Refresh the data
                        getLoaderManager().restartLoader(LOADER_SORT_LEAD_BY_DATE, null, C_ListLeadFragment.this);
                    } else if (response.body() == null) {
                        Log.v(TAG, "Downloading lead. No new lead found");
                    }

                }
            }

            @Override
            public void onFailure(Call<List<DownloadLeadResponse>> call, Throwable t) {
                dismissProgressDialogDownloadAllLead();
                hideLoadingIndicator();
                Toast.makeText(getActivity(), getString(R.string.unable_to_download_new_lead) + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.v(TAG, getString(R.string.unable_to_download_new_lead) + t.getLocalizedMessage());
            }
        });
    }



    private void downloadAllActivity(String msagId, boolean useLimit, String sortBy, String sortDirection, String afterDate) {
        showProgressDialogDownloadAllLead("Mengunduh aktivitas", "Mengunduh semua data aktivitas lead Anda. Proses ini dapat berlangsung selama beberapa menit. Mohon tunggu");
        String url = AppConfig.getBaseUrlCRM().concat("v2/toll/module_activity2");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<List<LeadActivityList>> call = api.fetchActivityForDashboard(url, msagId, useLimit, sortBy, sortDirection, afterDate);
        call.enqueue(new Callback<List<LeadActivityList>>() {
            @Override
            public void onResponse(Call<List<LeadActivityList>> call, Response<List<LeadActivityList>> response) {

                if (response.body() != null) {
                    List<LeadActivityList> activities = response.body();

                    //Refresh  with the latest data
                    if (activities != null && activities.size() > 0) {

                        for (LeadActivityList activity : activities) {
                            insert.insertActivityToDatabase(activity);
                        }

                    }

                }

                dismissProgressDialogDownloadAllLead();
            }

            @Override
            public void onFailure(Call<List<LeadActivityList>> call, Throwable t) {
                dismissProgressDialogDownloadAllLead();
            }
        });
    }

    /**
     * Inserting new lead to database
     *
     * @param leadList
     */
    private void insertToDatabase(List<DownloadLeadResponse> leadList) {
        C_Insert insert = new C_Insert(getActivity());

        List<String> insertedLeadIdList = insert.insertSimbakLeadToDatabase(leadList);
        insert.insertAddressToDatabase(leadList, insertedLeadIdList);
        insert.insertSimbakLinkUserLeadToDatabase(leadList, kodeAgen, insertedLeadIdList);
    }

    @Override
    public void onResume() {
        super.onResume();

        getLoaderManager().restartLoader(LOADER_SORT_LEAD_BY_DATE, null, this);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        recyclerView.setVisibility(View.GONE);
        showLoadingIndicator("Mengurutkan data lead.\nMohon tunggu");
        fab.setVisibility(View.GONE);

        CursorLoader cursorLoader;

        switch (id) {
            case LOADER_SORT_LEAD_BY_DATE:
                cursor = select.getAllSavedLead(kodeAgen, "SL_FAV DESC, SL_CRTD_DATE DESC"); //Default sort is by date
                break;
            case LOADER_SORT_LEAD_BY_NAME:
                cursor = select.getAllSavedLead(kodeAgen, "SL_FAV DESC, SL_NAME COLLATE NOCASE ASC");
                break;
            case LOADER_SORT_LEAD_BY_CURRENT_BRANCH:
                cursor = select.getAllSavedLead(kodeAgen, "SL_FAV DESC, SL_BAC_CURR_BRANCH COLLATE NOCASE ASC");
                break;
        }


        cursorLoader = new CursorLoader(getContext()) {
            @Override
            public Cursor loadInBackground() {
                return cursor;
            }
        };

        return cursorLoader;

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        recyclerView.setVisibility(View.VISIBLE);
        hideLoadingIndicator();
        fab.setVisibility(View.VISIBLE);


        int id = loader.getId();

        switch (id) {
            case LOADER_SORT_LEAD_BY_CURRENT_BRANCH:
            case LOADER_SORT_LEAD_BY_DATE:
            case LOADER_SORT_LEAD_BY_NAME:

                clearPreviousLeadResult();

                if (cursor != null && cursor.getCount() > 0) {

                    String slNameFirstNameInitial = null;
                    String slNameLastNameInital = null;

                    while (cursor.moveToNext()) {
                        long slTabId = cursor.getLong(cursor.getColumnIndexOrThrow("SL_TAB_ID"));
                        long slId = cursor.getLong(cursor.getColumnIndexOrThrow("SL_ID"));
                        String slName = cursor.getString(cursor.getColumnIndexOrThrow("SL_NAME"));

                        if (!TextUtils.isEmpty(slName)) {
                            String[] splitByWhiteSpace  = slName.split("\\s+");

                            if (splitByWhiteSpace != null && splitByWhiteSpace.length > 0) {

                                if (splitByWhiteSpace.length == 3) {
                                    //Name contains first, middle, last name
                                    if ( !TextUtils.isEmpty(splitByWhiteSpace[0]) && !TextUtils.isEmpty(splitByWhiteSpace[2]) ) {
                                        slNameFirstNameInitial = splitByWhiteSpace[0];
                                        slNameFirstNameInitial = String.valueOf(slNameFirstNameInitial.charAt(0)).toUpperCase();

                                        slNameLastNameInital = splitByWhiteSpace[2];
                                        slNameLastNameInital = String.valueOf(slNameLastNameInital.charAt(0)).toUpperCase();
                                    }


                                } else if(splitByWhiteSpace.length == 2) {

                                    if ( !TextUtils.isEmpty(splitByWhiteSpace[0]) && !TextUtils.isEmpty(splitByWhiteSpace[1]) ) {
                                        //Name contains first, middle name only
                                        slNameFirstNameInitial = splitByWhiteSpace[0];
                                        slNameFirstNameInitial = String.valueOf(slNameFirstNameInitial.charAt(0)).toUpperCase();

                                        slNameLastNameInital = splitByWhiteSpace[1];
                                        slNameLastNameInital = String.valueOf(slNameLastNameInital.charAt(0)).toUpperCase();
                                    }


                                } else if (splitByWhiteSpace.length == 1) {

                                    if (!TextUtils.isEmpty(splitByWhiteSpace[0])) {
                                        //Name contains first name only
                                        slNameFirstNameInitial = splitByWhiteSpace[0];
                                        slNameFirstNameInitial = String.valueOf(slNameFirstNameInitial.charAt(0)).toUpperCase();
                                        slNameLastNameInital = "";
                                    }

                                }
                            }

                        }

                        String slCreatedDate = cursor.getString(cursor.getColumnIndexOrThrow("SL_CRTD_DATE"));
                        int slUmur = cursor.getInt(cursor.getColumnIndexOrThrow("SL_UMUR"));
                        int slLastPos = cursor.getInt(cursor.getColumnIndexOrThrow("SL_LAST_POS"));
                        int slFav = cursor.getInt(cursor.getColumnIndexOrThrow("SL_FAV"));
                        String slCurrentBranch = cursor.getString(cursor.getColumnIndexOrThrow("NAMA_CABANG"));

                        boolean isFavoriteLead = isOnFavoriteLead(slFav);

                        if (isFavoriteLead) {
                            favoriteLeads.add(new ChildLeadList(slTabId, slId, slCreatedDate, slName,slNameFirstNameInitial,slNameLastNameInital, slUmur, slLastPos, true, slCurrentBranch));
                        } else {
                            leads.add(new ChildLeadList(slTabId, slId, slCreatedDate, slName, slNameFirstNameInitial,slNameLastNameInital, slUmur, slLastPos, false, slCurrentBranch));
                        }

                    }

                    int favoriteLeadCount = favoriteLeads.size();
                    int leadCount = leads.size();

                    //Set group and its child
                    parents.add(new ParentLeadList("Lead Favorit - " + favoriteLeadCount, favoriteLeads));
                    parents.add(new ParentLeadList("Lead - " + leadCount, leads));

                    displayLeadsData(parents);

                    layoutNoLeadFound.setVisibility(View.GONE);
                } else if (cursor != null && cursor.getCount() == 0) {
                    layoutNoLeadFound.setVisibility(View.VISIBLE);
                }
                break;

        }

    }

    private void displayLeadsData(final List<ParentLeadList> parents) {
        mAdapter = new C_LeadAdapter(parents);
        mAdapter.setClickListener(this);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        //Expand lead group by default
        mAdapter.onGroupClick(1);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_tabs_lead, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                launchSearchLeadActivity();
                break;
            case R.id.action_sort:
                displaySortOptionsDialog();
                break;
            case R.id.action_refresh_lead:
                if (StaticMethods.isNetworkAvailable(getActivity())) {
                    fetchLeadFromServer();
                } else {
                    Toast.makeText(getActivity(), "Tidak dapat memperbaharui data lead. Mohon periksa kembali koneksi internet Anda", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.action_upload_unsynchronized_lead:
                uploadUnsynchronizedLead();
                break;
            case R.id.action_download_all_lead :
                if (StaticMethods.isNetworkAvailable(getActivity())) {
                    showReDownloadAllLeadDialogConfirmation();
                } else {
                    Toast.makeText(getActivity(), "Mohon periksa kembali koneksi internet Anda untuk menggunakan fitur ini", Toast.LENGTH_LONG).show();
                }

                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showReDownloadAllLeadDialogConfirmation() {
        new AlertDialog.Builder(getActivity())
                .setTitle("Download ulang semua lead?")
                .setMessage("Proses ini akan menghapus semua data nasabah Anda pada tablet ini (termasuk yang belum di sinkron dengan server) kemudian akan digantikan dengan semua data nasabah terbaru. Apakah Anda ingin melanjutkan?")
                .setPositiveButton("Ya", (dialog, which) -> {
                    clearPreviousLeadResult();
                    delete.removeAllLead();
                    delete.removeAllLeadActivity();
                    downloadAllLead(kodeAgen, false, "multiple", "SL_CRTD_DATE", "desc", true, null);
                })
                .setNegativeButton("Tidak", (dialog, which) -> dialog.dismiss())
                .show();
    }

    /**
     * Launch search lead activity to search lead by name
     */
    private void launchSearchLeadActivity() {
        Intent intent = new Intent(getActivity(), C_SearchLeadActivity.class);
        intent.putExtra(Const.INTENT_KEY_AGENT_CODE, kodeAgen);
        startActivity(intent);
    }

    /**
     * Display sort options dialog. (Sort by date or sort by name)
     */
    private void displaySortOptionsDialog() {
        final String[] sortOptions = new String[]{"Data terbaru", "Nama", "Cabang"};
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_title_sort_by)
                .setSingleChoiceItems(sortOptions, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        selectedLeadSortMode = which;
                    }
                })
                .setPositiveButton("Pilih", this)
                .show();
    }

    /**
     * Dialog single choice click listener
     *
     * @param dialogInterface
     * @param i
     */
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (selectedLeadSortMode) {
            case SORT_BY_DATE:
                getLoaderManager().restartLoader(LOADER_SORT_LEAD_BY_DATE, null, this);
                dialogInterface.dismiss();
                break;

            case SORT_BY_NAME:
                getLoaderManager().restartLoader(LOADER_SORT_LEAD_BY_NAME, null, this);
                dialogInterface.dismiss();

                //Reset to default sort mode, so when the user reselect the sort options it points to the sort by date
                selectedLeadSortMode = SORT_BY_DATE;

                break;

            case SORT_BY_CURRENT_BRANCH:
                getLoaderManager().restartLoader(LOADER_SORT_LEAD_BY_CURRENT_BRANCH, null, this);
                dialogInterface.dismiss();

                //Reset to default sort mode, so when the user reselect the sort options it points to the sort by date
                selectedLeadSortMode = SORT_BY_DATE;
                break;

            default:
                throw new IllegalArgumentException("Undefined leads sort options");
        }

    }

    @Override
    public void onLeadSelected(int position, View view, ChildLeadList model) {

        String leadName = model.getSlName();
        long slTabId = model.getSlTabId();
        long slId = model.getSlId();
        boolean isUserAFavoriteLead = model.isFavoriteLead();

        switch (view.getId()) {
            case R.id.layoutLeads:

                Intent intent = new Intent(getActivity(), C_LeadActivity.class);
                Bundle bundle = new Bundle();
                bundle.putLong(getString(R.string.SL_TAB_ID), slTabId);
                bundle.putLong(Const.INTENT_KEY_SL_ID, slId);
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.imgFavoriteLead:

                //Make someone add or removed from a favorite lead
                if (!isUserAFavoriteLead) {
                    update.markAsFavoriteLead(String.valueOf(slTabId),String.valueOf(slId), false);
                    Snackbar.make(rootLayoutLead, leadName.concat(" dihapus dari daftar favorit"), Snackbar.LENGTH_SHORT).show();
                } else {
                    update.markAsFavoriteLead(String.valueOf(slTabId),String.valueOf(slId), true);
                    Snackbar.make(rootLayoutLead, leadName.concat(" ditambahkan ke daftar favorit"), Snackbar.LENGTH_SHORT).show();
                }


                //Make a delete animation, so newly selected leads moved upward to favorite lead
                if (leads != null && mAdapter != null) {
                    mAdapter.notifyItemRemoved(position);
                }

                getLoaderManager().restartLoader(LOADER_SORT_LEAD_BY_DATE, null, this);
                break;
        }

    }

    /**
     * Remove previous lead result when user changed sort mode
     */
    private void clearPreviousLeadResult() {
        if (parents != null && parents.size() > 0) {

            parents.clear();

            if (leads != null) leads.clear();
            if (favoriteLeads != null) favoriteLeads.clear();
            if (mAdapter != null) mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Determine if a costumer is on a favorite lead or not
     * 0 = NOT a favorite lead
     * 1 = YES a favorite lead
     *
     * @param costumerFavoriteLeadFlag
     * @return
     */
    private boolean isOnFavoriteLead(int costumerFavoriteLeadFlag) {
        return costumerFavoriteLeadFlag == 1;
    }


    /**
     * upload un-synchronized activity data to server
     */
    private void uploadUnsynchronizedLead() {
        List<Long> listIdActs = new C_Select(getActivity()).selectListActivityIdWhichNeedToBeUpdate();

        if (!listIdActs.isEmpty()) {

            if (StaticMethods.isNetworkAvailable(getActivity())) {

                CrmRestClientUsage crm = new CrmRestClientUsage(getActivity());
                crm.setOnUploadCrmListener(this);
                crm.createUploadActivityRequestBodyFrom(listIdActs);
                crm.getActivityTypeForDropdown(kodeAgen, jenisLoginBC);

                showLoadingIndicator("Mengunggah data lead ke server.\nMohon tunggu");
            } else {
                Toast.makeText(getActivity(), "Tidak dapat mengunggah data lead. Mohon periksa kembali koneksi internet Anda", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(getActivity(), "Semua data lead Anda sudah diperbaharui ke server", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onUploadCrmSuccess(Long leadId, String message, Boolean isUpdateLead) {
        hideLoadingIndicator();
        Toast.makeText(getActivity(), "Data lead berhasil diunggah ke server", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onUploadCrmFail(String message) {
        hideLoadingIndicator();
        Toast.makeText(getActivity(), "Tidak dapat mengunggah data lead ke server. " + message, Toast.LENGTH_LONG).show();
    }

    private void showLoadingIndicator(String loadingMessage) {
        layoutLoading.setVisibility(View.VISIBLE);
        tvLoadingMessage.setText(loadingMessage);
    }

    private void hideLoadingIndicator() {
        layoutLoading.setVisibility(View.GONE);
        tvLoadingMessage.setText(null);
    }

    private void showProgressDialogDownloadAllLead(String title, String message) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    private void dismissProgressDialogDownloadAllLead() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
