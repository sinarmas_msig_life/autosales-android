
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Referral {

    @SerializedName("lrb_id")
    @Expose
    private String lrbId;
    @SerializedName("msag_id")
    @Expose
    private String msagId;
    @SerializedName("agent_name")
    @Expose
    private String agentName;
    @SerializedName("ajs_cbng")
    @Expose
    private String ajsCbng;
    @SerializedName("simas_cbng")
    @Expose
    private String simasCbng;
    @SerializedName("cabang")
    @Expose
    private String cabang;
    @SerializedName("reff_id")
    @Expose
    private String reffId;
    @SerializedName("reff_name")
    @Expose
    private String reffName;
    @SerializedName("wilayah")
    @Expose
    private String wilayah;

    public String getLrbId() {
        return lrbId;
    }

    public void setLrbId(String lrbId) {
        this.lrbId = lrbId;
    }

    public String getMsagId() {
        return msagId;
    }

    public void setMsagId(String msagId) {
        this.msagId = msagId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAjsCbng() {
        return ajsCbng;
    }

    public void setAjsCbng(String ajsCbng) {
        this.ajsCbng = ajsCbng;
    }

    public String getSimasCbng() {
        return simasCbng;
    }

    public void setSimasCbng(String simasCbng) {
        this.simasCbng = simasCbng;
    }

    public String getCabang() {
        return cabang;
    }

    public void setCabang(String cabang) {
        this.cabang = cabang;
    }

    public String getReffId() {
        return reffId;
    }

    public void setReffId(String reffId) {
        this.reffId = reffId;
    }

    public String getReffName() {
        return reffName;
    }

    public void setReffName(String reffName) {
        this.reffName = reffName;
    }

    public String getWilayah() {
        return wilayah;
    }

    public void setWilayah(String wilayah) {
        this.wilayah = wilayah;
    }

}
