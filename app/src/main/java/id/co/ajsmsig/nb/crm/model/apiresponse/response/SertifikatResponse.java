
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SertifikatResponse {

    @SerializedName("ERROR")
    @Expose
    private String eRROR;
    @SerializedName("no_polis")
    @Expose
    private String noPolis;

    public String getERROR() {
        return eRROR;
    }

    public void setERROR(String eRROR) {
        this.eRROR = eRROR;
    }

    public String getNoPolis() {
        return noPolis;
    }

    public void setNoPolis(String noPolis) {
        this.noPolis = noPolis;
    }

}
