package id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.request;

public class ListActivity {
    private int SLA_ACTIVE;
    private String SLA_CRTD_DATE;
    private String SLA_CRTD_ID;
    private String SLA_DETAIL;
    private String SLA_IDATE;
    private int SLA_INST_TAB_ID;
    private int SLA_INST_TYPE;
    private int SLA_LAST_POS;
    private String SLA_SDATE;
    private int SLA_SRC;
    private int SLA_TAB_ID;
    private int SLA_TYPE;
    private String SLA_UPDTD_DATE;
    private String SLA_UPDTD_ID;

    public ListActivity(int SLA_ACTIVE, String SLA_CRTD_DATE, String SLA_CRTD_ID, String SLA_DETAIL, String SLA_IDATE, int SLA_INST_TAB_ID, int SLA_INST_TYPE, int SLA_LAST_POS, String SLA_SDATE, int SLA_SRC, int SLA_TAB_ID, int SLA_TYPE, String SLA_UPDTD_DATE, String SLA_UPDTD_ID) {
        this.SLA_ACTIVE = SLA_ACTIVE;
        this.SLA_CRTD_DATE = SLA_CRTD_DATE;
        this.SLA_CRTD_ID = SLA_CRTD_ID;
        this.SLA_DETAIL = SLA_DETAIL;
        this.SLA_IDATE = SLA_IDATE;
        this.SLA_INST_TAB_ID = SLA_INST_TAB_ID;
        this.SLA_INST_TYPE = SLA_INST_TYPE;
        this.SLA_LAST_POS = SLA_LAST_POS;
        this.SLA_SDATE = SLA_SDATE;
        this.SLA_SRC = SLA_SRC;
        this.SLA_TAB_ID = SLA_TAB_ID;
        this.SLA_TYPE = SLA_TYPE;
        this.SLA_UPDTD_DATE = SLA_UPDTD_DATE;
        this.SLA_UPDTD_ID = SLA_UPDTD_ID;
    }



}
