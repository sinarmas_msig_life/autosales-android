package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import id.co.ajsmsig.nb.espaj.model.DetilInvestasiModel;
import id.co.ajsmsig.nb.espaj.model.EspajModel;

/**
 * Created by Bernard on 05/09/2017.
 */

public class TableDetilInvestasi {
    private Context context;

    public TableDetilInvestasi(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(EspajModel me){
        ContentValues cv = new ContentValues();
        cv.put("PREMI_POKOK_DI", me.getDetilInvestasiModel().getPremi_pokok_di());
        cv.put("PREMI_BERKALA_DI", me.getDetilInvestasiModel().getPremi_berkala_di());
        cv.put("PREMITOPUP_BERKALA_DI", me.getDetilInvestasiModel().getPremitopup_berkala_di());
        cv.put("PREMI_TUNGGAL_DI", me.getDetilInvestasiModel().getPremi_tunggal_di());
        cv.put("PREMITOPUP_TUNGGAL_DI", me.getDetilInvestasiModel().getPremitopup_tunggal_di());
        cv.put("PREMI_TAMBAHAN_DI", me.getDetilInvestasiModel().getPremi_tambahan_di());
        cv.put("JUMLAH_DI", me.getDetilInvestasiModel().getJumlah_di());
        cv.put("INVEST_BAYAR_PREMI_DI", me.getDetilInvestasiModel().getInvest_bayar_premi_di());
        cv.put("INVEST_BANK_DI", me.getDetilInvestasiModel().getInvest_bank_di());
        cv.put("INVEST_CABANG_DI", me.getDetilInvestasiModel().getInvest_cabang_di());
        cv.put("INVEST_KOTA_DI", me.getDetilInvestasiModel().getInvest_kota_di());
        cv.put("INVEST_JNSTAB_DI", me.getDetilInvestasiModel().getInvest_jnstab_di());
        cv.put("INVEST_JNSNASABAH_DI", me.getDetilInvestasiModel().getInvest_jnsnasabah_di());
        cv.put("INVEST_NOREK_DI", me.getDetilInvestasiModel().getInvest_norek_di());
        cv.put("INVEST_NAMA_DI", me.getDetilInvestasiModel().getInvest_nama_di());
        cv.put("INVEST_KURS_DI", me.getDetilInvestasiModel().getInvest_kurs_di());
        cv.put("INVEST_BERIKUASA_DI", me.getDetilInvestasiModel().getInvest_berikuasa_di());
        cv.put("INVEST_TGLKUASA_DI", me.getDetilInvestasiModel().getInvest_tglkuasa_di());
        cv.put("INVEST_KETERANGAN_DI", me.getDetilInvestasiModel().getInvest_keterangan_di());
        cv.put("AUTODBT_BANK_DI", me.getDetilInvestasiModel().getAutodbt_bank_di());
        cv.put("AUTODBT_JNSTAB_DI", me.getDetilInvestasiModel().getAutodbt_jnstab_di());
        cv.put("AUTODBT_KURS_DI", me.getDetilInvestasiModel().getAutodbt_kurs_di());
        cv.put("AUTODBT_NOREK_DI", me.getDetilInvestasiModel().getAutodbt_norek_di());
        cv.put("AUTODBT_NAMA_DI", me.getDetilInvestasiModel().getAutodbt_nama_di());
        cv.put("AUTODBT_TGLDEBET_DI", me.getDetilInvestasiModel().getAutodbt_tgldebet_di());
        cv.put("AUTODBT_TGLVALID_DI", me.getDetilInvestasiModel().getAutodbt_tglvalid_di());
        cv.put("AUTODBT_AKTIF_DI", me.getDetilInvestasiModel().getAutodbt_aktif_di());
        cv.put("AUTODBT_PREMIPERTAMA_DI", me.getDetilInvestasiModel().getAutodbt_premipertama_di());
        cv.put("AUTODBT_NO_SIMASCARD_DI", me.getDetilInvestasiModel().getAutodbt_no_simascard_di());
        cv.put("DANAINVEST_ALOKASI_DI", me.getDetilInvestasiModel().getDanainvest_alokasi_di());
        cv.put("JUMLAH1_DI", me.getDetilInvestasiModel().getJumlah1_di());
        cv.put("PERSEN1_DI", me.getDetilInvestasiModel().getPersen1_di());
        cv.put("JUMLAH2_DI", me.getDetilInvestasiModel().getJumlah2_di());
        cv.put("PERSEN2_DI", me.getDetilInvestasiModel().getPersen2_di());
        cv.put("JUMLAH3_DI", me.getDetilInvestasiModel().getJumlah3_di());
        cv.put("PERSEN3_DI", me.getDetilInvestasiModel().getPersen3_di());
        cv.put("VALIDATION", me.getDetilInvestasiModel().getValidation());

        return cv;
    }

    public DetilInvestasiModel getObject (Cursor cursor){
        DetilInvestasiModel detilInvestasiModel= new DetilInvestasiModel();
        if (cursor != null && cursor.getCount()>0){
            cursor.moveToFirst();
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PREMI_POKOK_DI"))){
                detilInvestasiModel.setPremi_pokok_di(cursor.getDouble(cursor.getColumnIndexOrThrow("PREMI_POKOK_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PREMI_BERKALA_DI"))){
                detilInvestasiModel.setPremi_berkala_di(cursor.getDouble(cursor.getColumnIndexOrThrow("PREMI_BERKALA_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PREMITOPUP_BERKALA_DI"))){
                detilInvestasiModel.setPremitopup_berkala_di(cursor.getInt(cursor.getColumnIndexOrThrow("PREMITOPUP_BERKALA_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PREMI_TUNGGAL_DI"))){
                detilInvestasiModel.setPremi_tunggal_di(cursor.getDouble(cursor.getColumnIndexOrThrow("PREMI_TUNGGAL_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PREMITOPUP_TUNGGAL_DI"))){
                detilInvestasiModel.setPremitopup_tunggal_di(cursor.getInt(cursor.getColumnIndexOrThrow("PREMITOPUP_TUNGGAL_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PREMI_TAMBAHAN_DI"))){
                detilInvestasiModel.setPremi_tambahan_di(cursor.getDouble(cursor.getColumnIndexOrThrow("PREMI_TAMBAHAN_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("JUMLAH_DI"))){
                detilInvestasiModel.setJumlah_di(cursor.getDouble(cursor.getColumnIndexOrThrow("JUMLAH_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INVEST_BAYAR_PREMI_DI"))){
                detilInvestasiModel.setInvest_bayar_premi_di(cursor.getInt(cursor.getColumnIndexOrThrow("INVEST_BAYAR_PREMI_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INVEST_BANK_DI"))){
                detilInvestasiModel.setInvest_bank_di(cursor.getInt(cursor.getColumnIndexOrThrow("INVEST_BANK_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INVEST_CABANG_DI"))){
                detilInvestasiModel.setInvest_cabang_di(cursor.getString(cursor.getColumnIndexOrThrow("INVEST_CABANG_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INVEST_KOTA_DI"))){
                detilInvestasiModel.setInvest_kota_di(cursor.getString(cursor.getColumnIndexOrThrow("INVEST_KOTA_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INVEST_JNSTAB_DI"))){
                detilInvestasiModel.setInvest_jnstab_di(cursor.getInt(cursor.getColumnIndexOrThrow("INVEST_JNSTAB_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INVEST_JNSNASABAH_DI"))){
                detilInvestasiModel.setInvest_jnsnasabah_di(cursor.getInt(cursor.getColumnIndexOrThrow("INVEST_JNSNASABAH_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INVEST_NOREK_DI"))){
                detilInvestasiModel.setInvest_norek_di(cursor.getString(cursor.getColumnIndexOrThrow("INVEST_NOREK_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INVEST_NAMA_DI"))){
                detilInvestasiModel.setInvest_nama_di(cursor.getString(cursor.getColumnIndexOrThrow("INVEST_NAMA_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INVEST_KURS_DI"))){
                detilInvestasiModel.setInvest_kurs_di(cursor.getString(cursor.getColumnIndexOrThrow("INVEST_KURS_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INVEST_BERIKUASA_DI"))){
                detilInvestasiModel.setInvest_berikuasa_di(cursor.getInt(cursor.getColumnIndexOrThrow("INVEST_BERIKUASA_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INVEST_TGLKUASA_DI"))){
                detilInvestasiModel.setInvest_tglkuasa_di(cursor.getString(cursor.getColumnIndexOrThrow("INVEST_TGLKUASA_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INVEST_KETERANGAN_DI"))){
                detilInvestasiModel.setInvest_keterangan_di(cursor.getString(cursor.getColumnIndexOrThrow("INVEST_KETERANGAN_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AUTODBT_BANK_DI"))){
                detilInvestasiModel.setAutodbt_bank_di(cursor.getInt(cursor.getColumnIndexOrThrow("AUTODBT_BANK_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AUTODBT_JNSTAB_DI"))){
                detilInvestasiModel.setAutodbt_jnstab_di(cursor.getInt(cursor.getColumnIndexOrThrow("AUTODBT_JNSTAB_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AUTODBT_KURS_DI"))){
                detilInvestasiModel.setAutodbt_kurs_di(cursor.getString(cursor.getColumnIndexOrThrow("AUTODBT_KURS_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AUTODBT_NOREK_DI"))){
                detilInvestasiModel.setAutodbt_norek_di(cursor.getString(cursor.getColumnIndexOrThrow("AUTODBT_NOREK_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AUTODBT_NAMA_DI"))){
                detilInvestasiModel.setAutodbt_nama_di(cursor.getString(cursor.getColumnIndexOrThrow("AUTODBT_NAMA_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AUTODBT_TGLDEBET_DI"))){
                detilInvestasiModel.setAutodbt_tgldebet_di(cursor.getString(cursor.getColumnIndexOrThrow("AUTODBT_TGLDEBET_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AUTODBT_TGLVALID_DI"))){
                detilInvestasiModel.setAutodbt_tglvalid_di(cursor.getString(cursor.getColumnIndexOrThrow("AUTODBT_TGLVALID_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AUTODBT_AKTIF_DI"))){
                detilInvestasiModel.setAutodbt_aktif_di(cursor.getInt(cursor.getColumnIndexOrThrow("AUTODBT_AKTIF_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AUTODBT_PREMIPERTAMA_DI"))){
                detilInvestasiModel.setAutodbt_premipertama_di(cursor.getInt(cursor.getColumnIndexOrThrow("AUTODBT_PREMIPERTAMA_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("AUTODBT_NO_SIMASCARD_DI"))){
                detilInvestasiModel.setAutodbt_no_simascard_di(cursor.getString(cursor.getColumnIndexOrThrow("AUTODBT_NO_SIMASCARD_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("DANAINVEST_ALOKASI_DI"))){
                detilInvestasiModel.setDanainvest_alokasi_di(cursor.getInt(cursor.getColumnIndexOrThrow("DANAINVEST_ALOKASI_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("JUMLAH1_DI"))){
                detilInvestasiModel.setJumlah1_di(cursor.getDouble(cursor.getColumnIndexOrThrow("JUMLAH1_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PERSEN1_DI"))){
                detilInvestasiModel.setPersen1_di(cursor.getDouble(cursor.getColumnIndexOrThrow("PERSEN1_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("JUMLAH2_DI"))){
                detilInvestasiModel.setJumlah2_di(cursor.getDouble(cursor.getColumnIndexOrThrow("JUMLAH2_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PERSEN2_DI"))){
                detilInvestasiModel.setPersen2_di(cursor.getDouble(cursor.getColumnIndexOrThrow("PERSEN2_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("JUMLAH3_DI"))){
                detilInvestasiModel.setJumlah3_di(cursor.getDouble(cursor.getColumnIndexOrThrow("JUMLAH3_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PERSEN3_DI"))){
                detilInvestasiModel.setPersen3_di(cursor.getDouble(cursor.getColumnIndexOrThrow("PERSEN3_DI")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("VALIDATION"))){
                int valid = cursor.getInt(cursor.getColumnIndexOrThrow("VALIDATION"));
//                boolean isValid = valid == 1 ? true : false;
                boolean val;
                val = valid == 1;
                detilInvestasiModel.setValidation(val);
            }
            cursor.close();
        }
        return detilInvestasiModel;

    }
}
