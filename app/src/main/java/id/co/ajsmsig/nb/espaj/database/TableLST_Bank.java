package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBank;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBankCab;

/**
 * Created by eriza on 03/03/2018.
 */

public class TableLST_Bank {
    private Context context;

    public TableLST_Bank(Context context) {
        this.context = context;
    }

    public ContentValues getContentBank(ModelBank me) {
        ContentValues cv = new ContentValues();
        cv.put("LSBP_ID", me.getLSBP_ID());
        cv.put("LSBP_NAMA", me.getLSBP_NAMA());
        cv.put("LSBP_PANJANG_REKENING", me.getLSBP_PANJANG_REKENING());
        cv.put("LSBP_MIN_PANJANG_REKENING", me.getLSBP_MIN_PANJANG_REKENING());
        return cv;
    }

    public ContentValues getContentBankCab(ModelBankCab me) {
        ContentValues cv = new ContentValues();
        cv.put("LBN_ID", me.getLBN_ID());
        cv.put("LSBP_ID", me.getLSBP_ID());
        cv.put("LBN_NAMA", me.getLBN_NAMA());
        return cv;
    }

    public ArrayList<ModelBank> getObjectArrayBank(Cursor cursor) {
        ArrayList<ModelBank> List = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ModelBank msa = new ModelBank();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("LSBP_ID"))) {
                    msa.setLSBP_ID(cursor.getInt(cursor.getColumnIndexOrThrow("LSBP_ID")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("LSBP_NAMA"))) {
                    msa.setLSBP_NAMA(cursor.getString(cursor.getColumnIndexOrThrow("LSBP_NAMA")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("LSBP_PANJANG_REKENING"))) {
                    msa.setLSBP_PANJANG_REKENING(cursor.getInt(cursor.getColumnIndexOrThrow("LSBP_PANJANG_REKENING")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("LSBP_MIN_PANJANG_REKENING"))) {
                    msa.setLSBP_MIN_PANJANG_REKENING(cursor.getInt(cursor.getColumnIndexOrThrow("LSBP_MIN_PANJANG_REKENING")));
                }
                List.add(msa);
                cursor.moveToNext();
            }
            // always close the cursor
            cursor.close();
        }
        return List;
    }

    public ArrayList<ModelBankCab> getObjectArrayBankCab(Cursor cursor) {
        ArrayList<ModelBankCab> List = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ModelBankCab msa = new ModelBankCab();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("LBN_ID"))) {
                    msa.setLBN_ID(cursor.getInt(cursor.getColumnIndexOrThrow("LBN_ID")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("LSBP_ID"))) {
                    msa.setLSBP_ID(cursor.getInt(cursor.getColumnIndexOrThrow("LSBP_ID")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("LBN_NAMA"))) {
                    msa.setLBN_NAMA(cursor.getString(cursor.getColumnIndexOrThrow("LBN_NAMA")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("LSBP_PANJANG_REKENING"))) {
                    msa.setLSBP_PANJANG_REKENING(cursor.getInt(cursor.getColumnIndexOrThrow("LSBP_PANJANG_REKENING")));
                }
                List.add(msa);
                cursor.moveToNext();
            }
            // always close the cursor
            cursor.close();
        }
        return List;
    }
}
