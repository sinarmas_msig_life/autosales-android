
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("tgl_input")
    @Expose
    private String tglInput;
    @SerializedName("no_proposal")
    @Expose
    private String noProposal;

    public String getTglInput() {
        return tglInput;
    }

    public void setTglInput(String tglInput) {
        this.tglInput = tglInput;
    }

    public String getNoProposal() {
        return noProposal;
    }

    public void setNoProposal(String noProposal) {
        this.noProposal = noProposal;
    }

}
