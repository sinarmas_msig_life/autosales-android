/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package id.co.ajsmsig.nb.pointofsale.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.co.ajsmsig.nb.pointofsale.ui.fragment.HeaderFragment
import id.co.ajsmsig.nb.pointofsale.ui.jproductrecomendation.ProductRecomendationFragment
import id.co.ajsmsig.nb.pointofsale.ui.kproductdescriptionchoice.ProductDescriptionChoiceFragment
import id.co.ajsmsig.nb.pointofsale.ui.lproductfabdescription.ProductFABDescriptionFragment
import id.co.ajsmsig.nb.pointofsale.ui.lproductpresentation.ProductPresentationVideoFragment
import id.co.ajsmsig.nb.pointofsale.ui.lproductrider.ProductRiderFragment
import id.co.ajsmsig.nb.pointofsale.ui.mrecommendationsummary.RecommendationSummaryFragment
import id.co.ajsmsig.nb.pointofsale.ui.ahome.HomeFragment
import id.co.ajsmsig.nb.pointofsale.ui.blifestage.LifeStageFragment
import id.co.ajsmsig.nb.pointofsale.ui.cpriorityneed.PriorityNeedFragment
import id.co.ajsmsig.nb.pointofsale.ui.dneedanalysis.NeedAnalysisFragment
import id.co.ajsmsig.nb.pointofsale.ui.eneedcalculator.NeedCalculatorFragment
import id.co.ajsmsig.nb.pointofsale.ui.ffinancialcalculator.FinancialCalculatorFragment
import id.co.ajsmsig.nb.pointofsale.ui.griskprofiletools.RiskProfileToolsFragment
import id.co.ajsmsig.nb.pointofsale.ui.hriskprofiling.RiskProfilingFragment
import id.co.ajsmsig.nb.pointofsale.ui.iriskprofileresult.RiskProfileResultFragment
import id.co.ajsmsig.nb.pointofsale.ui.handlingobjection.HandlingObjectionDetailFragment
import id.co.ajsmsig.nb.pointofsale.ui.handlingobjection.HandlingObjectionListFragment

@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    internal abstract fun contributeHeaderFragment(): HeaderFragment

    @ContributesAndroidInjector
    internal abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    internal abstract fun contributeLifeStageFragment(): LifeStageFragment

    @ContributesAndroidInjector
    internal abstract fun contributePriorityNeedFragment(): PriorityNeedFragment

    @ContributesAndroidInjector
    internal abstract fun contributeNeedAnalysisFragment(): NeedAnalysisFragment

    @ContributesAndroidInjector
    internal abstract fun contributeNeedCalculatorFragment(): NeedCalculatorFragment

    @ContributesAndroidInjector
    internal abstract fun contributeFinancFragment(): FinancialCalculatorFragment

    @ContributesAndroidInjector
    internal abstract fun contributeRiskProfileToolsFragment(): RiskProfileToolsFragment

    @ContributesAndroidInjector
    internal abstract fun contributeRiskProfilingFragment(): RiskProfilingFragment

    @ContributesAndroidInjector
    internal abstract fun contributeRiskProfileResultFragment(): RiskProfileResultFragment

    @ContributesAndroidInjector
    internal abstract fun contributeProductRecommendationFragment(): ProductRecomendationFragment

    @ContributesAndroidInjector
    internal abstract fun contributeProductDescriptionChoiceFragment(): ProductDescriptionChoiceFragment

    @ContributesAndroidInjector
    internal abstract fun contributeProductFABDescriptionFragment(): ProductFABDescriptionFragment

    @ContributesAndroidInjector
    internal abstract fun contributeProductPresentationVideoFragment(): ProductPresentationVideoFragment

    @ContributesAndroidInjector
    internal abstract fun contributeProductRiderFragment(): ProductRiderFragment

    @ContributesAndroidInjector
    internal abstract fun contributeRecommendationSummaryFragment(): RecommendationSummaryFragment

    @ContributesAndroidInjector
    internal abstract fun contributeHandlingObjectionListFragment(): HandlingObjectionListFragment

    @ContributesAndroidInjector
    internal abstract fun contributeHandlingObjectionDetailFragment(): HandlingObjectionDetailFragment

}
