package id.co.ajsmsig.nb.pointofsale.model.db.dynamic

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation

/**
 * Created by andreyyoshuamanik on 13/03/18.
 */
class ProductWithBenefitAndRider {

    @Embedded
    var product: Product? = null

    @Relation(entity = ProductBenefit::class, parentColumn = "LsbsId", entityColumn = "ProductLsbsId")
    var productBenefits: List<ProductBenefit>? = null

    @Relation(entity = ProductRider::class, parentColumn = "LsbsId", entityColumn = "ProductLsbsId")
    var productRiders: List<ProductRider>? = null
}