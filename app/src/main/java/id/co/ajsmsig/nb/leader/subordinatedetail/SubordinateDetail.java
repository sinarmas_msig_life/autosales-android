package id.co.ajsmsig.nb.leader.subordinatedetail;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.leader.summarybc.SummaryFragment;
import id.co.ajsmsig.nb.util.Const;

public class SubordinateDetail extends AppCompatActivity {


    private String agentCode;
    private String agentName;
    private String keyChannel;

    @BindView(R.id.tvSubordinateName)
    TextView tvSubordinateName;

    @BindView(R.id.layoutNoInternetConnection)
    ConstraintLayout layoutNoInternetConnection;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvAgentInitialNameDetail)
    TextView tvAgentInitialNameDetail;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_spaj_detail);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        agentCode = intent.getStringExtra(Const.INTENT_KEY_AGENT_CODE);
        keyChannel = intent.getStringExtra(Const.INTENT_KEY_CHANNEL);
        agentName = intent.getStringExtra(Const.INTENT_KEY_AGENT_NAME);
        String agentInitialName = getAgentInitialName(agentName);

        tvSubordinateName.setText(agentName);
        tvAgentInitialNameDetail.setText(agentInitialName);
        initToolbar();
        initViewPager();

        if (StaticMethods.isNetworkAvailable(SubordinateDetail.this)) {
            hideNoInternetConnection();
        } else {
            showNoInternetConnection();
        }
    }

    private void initViewPager() {
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ContentSubordinateDetailFragment(), "Bancassurance");
        adapter.addFragment(new SummaryFragment(), "Summary Bancassurance");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void initToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Lihat subordinate");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    @OnClick(R.id.btnRetry)
    void onBtnRetryPressed() {
        if (StaticMethods.isNetworkAvailable(SubordinateDetail.this)) {
            hideNoInternetConnection();
        } else {
            showNoInternetConnection();
            Toast.makeText(SubordinateDetail.this, "Anda tidak terkoneksi ke internet. Mohon pastikan Anda terkoneksi dengan internet", Toast.LENGTH_SHORT).show();
        }
    }
    private void showNoInternetConnection() {
        layoutNoInternetConnection.setVisibility(View.GONE);
    }

    private void hideNoInternetConnection() {
        layoutNoInternetConnection.setVisibility(View.GONE);
    }

    private String getAgentInitialName(String agentName) {

        if (!TextUtils.isEmpty(agentName)) {
            String[] splitByWhiteSpace = agentName.split("\\s+");

            if (splitByWhiteSpace != null && splitByWhiteSpace.length > 0) {

                if (splitByWhiteSpace.length == 3) {

                    //Name contains first, middle, last name
                    if (!TextUtils.isEmpty(splitByWhiteSpace[0]) && !TextUtils.isEmpty(splitByWhiteSpace[2])) {
                        String agentNameFirstNameInitial = splitByWhiteSpace[0];
                        agentNameFirstNameInitial = String.valueOf(agentNameFirstNameInitial.charAt(0)).toUpperCase();

                        String agentNameLastNameInital = splitByWhiteSpace[2];
                        agentNameLastNameInital = String.valueOf(agentNameLastNameInital.charAt(0)).toUpperCase();

                        String firstLastNameInitial = agentNameFirstNameInitial + agentNameLastNameInital;

                        return firstLastNameInitial;
                    }
                } else if (splitByWhiteSpace.length == 2) {

                    if (!TextUtils.isEmpty(splitByWhiteSpace[0]) && !TextUtils.isEmpty(splitByWhiteSpace[1])) {
                        //Name contains first, middle name only
                        String agentNameFirstNameInitial = splitByWhiteSpace[0];
                        agentNameFirstNameInitial = String.valueOf(agentNameFirstNameInitial.charAt(0)).toUpperCase();

                        String agentNameLastNameInital = splitByWhiteSpace[1];
                        agentNameLastNameInital = String.valueOf(agentNameLastNameInital.charAt(0)).toUpperCase();

                        String firstLastNameInitial = agentNameFirstNameInitial + agentNameLastNameInital;

                        return firstLastNameInitial;

                    }
                } else if (splitByWhiteSpace.length == 1) {

                    if (!TextUtils.isEmpty(splitByWhiteSpace[0])) {
                        //Name contains first name only
                        String agentNameFirstNameInitial = splitByWhiteSpace[0];
                        agentNameFirstNameInitial = String.valueOf(agentNameFirstNameInitial.charAt(0)).toUpperCase();

                        String agentNameLastNameInital = "";

                        String firstLastNameInitial = agentNameFirstNameInitial + agentNameLastNameInital;

                        return firstLastNameInitial;
                    }

                }
            }
        }
        return "";
    }
}





