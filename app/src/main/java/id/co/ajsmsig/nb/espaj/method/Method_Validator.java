/**
 * @author Eriza Siti Mulyani
 * <p>
 * Method ini digunakan untuk validasi
 * <p>
 * method public static boolean yang digunakan didalam class ini
 */
package id.co.ajsmsig.nb.espaj.method;

import android.app.Activity;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.fragment.E_CalonPembayarPremiFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_DetilAgenFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_DetilInvestasiFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_DokumenPendukungFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_KonfirmasiDanKirimFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_PemegangPolisFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_ProfileResikoFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_QuestionnaireFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_TertanggungFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_UpdateQuisionerSIOFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_UsulanAsuransiFragment;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTT_NO3UQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTT_NO5UQ;
import id.co.ajsmsig.nb.util.Const;

public class Method_Validator {

    //method check email
    public static boolean emailValidator(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (email.length() != 0 && matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean ValEditRegex(String text) {
        boolean val = true;
        String expression = "[a-z/()A-Z,0-9-.\\s]+$";
        if (text.length() != 0 && !text.matches(expression)) {
            val = false;
        }
        return val;
    }

    public static void ValEditWatcher(EditText text, ImageView imageView, TextView textView, Context context) {
        int color;
        if (StaticMethods.isTextWidgetEmpty(text)) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            if (!ValEditRegex(text.getText().toString())) {
                color = ContextCompat.getColor(context, R.color.red);
                textView.setVisibility(View.VISIBLE);
                textView.setText(context.getString(R.string.error_regex));
            } else {
                color = ContextCompat.getColor(context, R.color.green);
                textView.setVisibility(View.GONE);
            }
        }
        imageView.setColorFilter(color);
    }

    public static void ValEditWatcher2text(EditText text, EditText text2, ImageView imageView, TextView textView, Context context) {
        int color;
        if (StaticMethods.isTextWidgetEmpty(text)||StaticMethods.isTextWidgetEmpty(text2)) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            if (!ValEditRegex(text.getText().toString())||!ValEditRegex(text2.getText().toString())) {
                color = ContextCompat.getColor(context, R.color.red);
                textView.setVisibility(View.VISIBLE);
                textView.setText(context.getString(R.string.error_regex));
            } else {
                color = ContextCompat.getColor(context, R.color.green);
                textView.setVisibility(View.GONE);
            }
        }
        imageView.setColorFilter(color);
    }

    public static void ValEditWatcherNonRegex(EditText text, ImageView imageView, TextView textView, Context context) {
        int color;
        if (StaticMethods.isTextWidgetEmpty(text)) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            color = ContextCompat.getColor(context, R.color.green);
            textView.setVisibility(View.GONE);
        }
        imageView.setColorFilter(color);
    }
//
//    public static void ValEditWatcherkhususno7(EditText text, ImageView imageView, Context context) {
//        int color;
//        if (StaticMethods.isTextWidgetEmpty(text)) {
//            color = ContextCompat.getColor(context, R.color.orange);
//        }else {
//            if (!ValEditRegex(text.getText().toString())){
//                color = ContextCompat.getColor(context, R.color.red);
////                textView.setVisibility(View.VISIBLE);
////                textView.setText(context.getString(R.string.error_regex));
//            }else {
//                color = ContextCompat.getColor(context, R.color.green);
////                textView.setVisibility(View.GONE);
//            }
//        }
//        imageView.setColorFilter(color);
//    }

    public static void ValEditWatcherEmail(EditText text, ImageView imageView, TextView textView, Context context) {
        int color;
        if (StaticMethods.isTextWidgetEmpty(text)) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            if (!emailValidator(text.getText().toString())) {
                color = ContextCompat.getColor(context, R.color.red);
                textView.setVisibility(View.VISIBLE);
                textView.setText(context.getString(R.string.error_regex));
            } else {
                color = ContextCompat.getColor(context, R.color.green);
                textView.setVisibility(View.GONE);
            }
        }
        imageView.setColorFilter(color);
    }

    //    updatequesionerSIO
    public static void ValEditWatcherSIO(EditText text, EditText text2, EditText text3, EditText text4, ImageView imageView, TextView textView, Context context) {
        int color;
        if (text.getText().toString().trim().length() > 0 && text2.getText().toString().trim().length() > 0 && text3.getText().toString().trim().length() > 0 && text4.getText().toString().trim().length() > 0) {
            color = ContextCompat.getColor(context, R.color.green);
        } else {
            color = ContextCompat.getColor(context, R.color.orange);
        }
        imageView.setColorFilter(color);
        textView.setVisibility(View.GONE);
    }

    public static void ValEditWatcherKes(EditText text, ImageView imageView, TextView textView, Context context) {
        int color;
        if (text.getText().toString().trim().length() == 0 || text.getText().toString().trim().equals("0")) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            color = ContextCompat.getColor(context, R.color.green);
        }
        imageView.setColorFilter(color);
        textView.setVisibility(View.GONE);
    }

    public static void ValEditWatcherRek(EditText text, ImageView imageView, TextView textView, Context context, int Length) {
        int color;
        if (text.getText().toString().length() < Length || text.getText().toString().length() == 0) {
            color = ContextCompat.getColor(context, R.color.orange);
            textView.setVisibility(View.VISIBLE);
            textView.setText(context.getString(R.string.error_norek));
        } else {
            color = ContextCompat.getColor(context, R.color.green);
            textView.setVisibility(View.GONE);
        }
        imageView.setColorFilter(color);
    }

    //validasi
    public static void ValTextWatcher(AutoCompleteTextView text, ImageView imageView, TextView textView, Context context) {
        int color;
        if (StaticMethods.isTextWidgetEmpty(text)) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            color = ContextCompat.getColor(context, R.color.green);
        }
        imageView.setColorFilter(color);
        textView.setVisibility(View.GONE);
    }

    public static boolean ValidCheckDana(ArrayList<CheckBox> list_check, ImageView imageView, Context context) {
        boolean check = false;
        int color;
        for (int i = 0; i < list_check.size(); i++) {
            if (list_check.get(i).isChecked()) {
                check = true;
            }
        }
        if (check == true) {
            color = ContextCompat.getColor(context, R.color.green);
        } else {
            color = ContextCompat.getColor(context, R.color.orange);
        }
        imageView.setColorFilter(color);
        return check;
    }

    //NewUpdateQuisioner Validator
    public static boolean onEmptyRadio(RadioGroup radioGroup, ImageView img_val, TextView text_val, ConstraintLayout layout, Context context) {
        boolean next = true;
        int color;
        if (radioGroup.getVisibility() == View.VISIBLE) {
            if (radioGroup.getCheckedRadioButtonId() == -1) {
                next = false;
            }
            if (!next) {
                text_val.setVisibility(View.VISIBLE);
                color = ContextCompat.getColor(context, R.color.red);
                layout.requestChildFocus(text_val, text_val);
                text_val.startAnimation(AnimationUtils.loadAnimation(context, R.anim.wobble));
                img_val.setColorFilter(color);
//                img_val.setImageResource(R.drawable.espaj_red_checklist);
//                layout.setBackgroundResource(R.drawable.border_corner_baris1_klik);
            } else {
                text_val.setVisibility(View.GONE);
                color = ContextCompat.getColor(context, R.color.green);
                img_val.setColorFilter(color);
//                img_val.setImageResource(R.drawable.espaj_gray_checklist);
//                layout.setBackgroundResource(R.drawable.border_corner_baris1);
            }
        }
        return next;
    }

    //NewUpdateQuisioner Validator untuk yang punya table
    public static boolean onEmptyRadioTable(String val, RadioGroup radioGroup, List<ModelTT_NO3UQ> list_ttno3,
                                            List<ModelTT_NO5UQ> list_ttno5, ImageButton button, ImageView img_val, TextView text_val, ConstraintLayout layout, Context context) {
        boolean next = true;
        int color;
        if (radioGroup.getCheckedRadioButtonId() == -1) {
            next = false;
        } else {
            if (val.equals("no3tt")) {
                if (button.isClickable() == true) {
                    if (list_ttno3.isEmpty()) {
                        next = true;
                    }
                }
            } else if (val.equals("no5tt")) {
                if (button.isClickable() == true) {
                    if (list_ttno5.isEmpty()) {
                        next = true;
                    }
                }
            }
        }
        if (!next) {
            text_val.setVisibility(View.VISIBLE);
            color = ContextCompat.getColor(context, R.color.red);
            img_val.setColorFilter(color);
//            img_val.setImageResource(R.drawable.espaj_red_checklist);
//            layout.setBackgroundResource(R.drawable.border_corner_baris1_klik);
        } else {
            text_val.setVisibility(View.GONE);
            color = ContextCompat.getColor(context, R.color.green);
            img_val.setColorFilter(color);
//            img_val.setImageResource(R.drawable.espaj_gray_checklist);
//            layout.setBackgroundResource(R.drawable.border_corner_baris1);
        }
        return next;
    }

    //NewUpdateQuisioner Validator khusus no 8 TT
    public static boolean onEmptyRadioText2(RadioGroup radioGroup, EditText edit, EditText edit2, ImageView img_val, TextView text_val, ConstraintLayout layout, Context context) {
        boolean next = true;
        int color;
        if (radioGroup.getCheckedRadioButtonId() == -1) {
            next = false;
        } else {
            if ((edit.isEnabled() == true && edit.getText().toString().length() == 0) || (edit2.isEnabled() == true && edit2.getText().toString().length() == 0)) {
                next = false;
            }
        }
        if (!next) {
            text_val.setVisibility(View.VISIBLE);
            color = ContextCompat.getColor(context, R.color.red);
            img_val.setColorFilter(color);
//            img_val.setImageResource(R.drawable.espaj_red_checklist);
//            layout.setBackgroundResource(R.drawable.border_corner_baris1_klik);
        } else {
            text_val.setVisibility(View.GONE);
            color = ContextCompat.getColor(context, R.color.green);
            img_val.setColorFilter(color);
//            img_val.setImageResource(R.drawable.espaj_gray_checklist);
//            layout.setBackgroundResource(R.drawable.border_corner_baris1);
        }
        return next;
    }

    //NewUpdateQuisioner Validator Text
    public static boolean onEmptyRadioText(RadioGroup radioGroup, EditText edit, ImageView img_val, TextView text_val, ConstraintLayout layout, Context context) {
        boolean next = true;
        int color;
        if (radioGroup.getCheckedRadioButtonId() == -1) {
            next = false;
        } else {
            if (edit.isEnabled() == true && edit.getText().toString().length() == 0) {
                next = false;
            }
        }
        if (!next) {
            text_val.setVisibility(View.VISIBLE);
            color = ContextCompat.getColor(context, R.color.red);
            img_val.setColorFilter(color);
//            img_val.setImageResource(R.drawable.espaj_red_checklist);
//            layout.setBackgroundResource(R.drawable.border_corner_baris1_klik);
        } else {
            text_val.setVisibility(View.GONE);
            color = ContextCompat.getColor(context, R.color.green);
            img_val.setColorFilter(color);
//            img_val.setImageResource(R.drawable.espaj_gray_checklist);
//            layout.setBackgroundResource(R.drawable.border_corner_baris1);
        }
        return next;
    }

    //NewUpdateQuisioner Validator khusus Pertanyaan tinggi dan berat
    public static boolean onEmptyText(EditText edit, EditText edit2, ImageView img_val, TextView text_val, ConstraintLayout layout, Context context) {
        boolean next = true;
        int color;
        if ((edit.getText().toString().length() == 0 || edit.getText().toString().equals("0")) ||
                (edit2.getText().toString().length() == 0 || edit2.getText().toString().equals("0"))) {
            next = false;
        }
        if (!next) {
            text_val.setVisibility(View.VISIBLE);
            color = ContextCompat.getColor(context, R.color.red);
            img_val.setColorFilter(color);
            layout.requestChildFocus(text_val, text_val);
            text_val.startAnimation(AnimationUtils.loadAnimation(context, R.anim.wobble));
//            img_val.setImageResource(R.drawable.espaj_red_checklist);
//            layout.setBackgroundResource(R.drawable.border_corner_baris1_klik);
        } else {
            text_val.setVisibility(View.GONE);
            color = ContextCompat.getColor(context, R.color.green);
            img_val.setColorFilter(color);
//            img_val.setImageResource(R.drawable.espaj_gray_checklist);
//            layout.setBackgroundResource(R.drawable.border_corner_baris1);
        }
        return next;
    }

    //Untuk yang radio groupnya banyak, parameter dibuat berdasarkan jumlah radio group.
    public static boolean onEmptyRadioKesehatan(RadioGroup radioGroup1, RadioGroup radioGroup2, RadioGroup radioGroup3, RadioGroup radioGroup4,
                                                RadioGroup radioGroup5, ImageView img_val, TextView text_val, Context context) {
        boolean val = true;
        int color;
        if (radioGroup1.getVisibility() == View.VISIBLE && (radioGroup1.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (radioGroup2.getVisibility() == View.VISIBLE && (radioGroup2.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (radioGroup3.getVisibility() == View.VISIBLE && (radioGroup3.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (radioGroup4.getVisibility() == View.VISIBLE && (radioGroup4.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (radioGroup5.getVisibility() == View.VISIBLE && (radioGroup5.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (!val) {
            text_val.setVisibility(View.VISIBLE);
            color = ContextCompat.getColor(context, R.color.red);
        } else {
            text_val.setVisibility(View.GONE);
            color = ContextCompat.getColor(context, R.color.green);
        }
        img_val.setColorFilter(color);
        return val;
    }

    public static boolean validateFirstPageValidation(EspajModel me, Context context, E_MainActivity activity) {
        if (!me.getPemegangPolisModel().getValidation()) {
            me.setValidation(false);
            activity.e_bundle.putParcelable(activity.getString(R.string.modelespaj), me);
            MethodSupport.Alert(context, context.getString(R.string.title_error_val), "Mohon lengkapi semua data pemegang polis terlebih dahulu", 0);
            activity.setFragment(new E_PemegangPolisFragment(), context.getResources().getString(R.string.pemegang_polis));
            return false;
        }
        return true;
    }

    public static void onGetAllValidation(EspajModel me, Context context, E_MainActivity activity) {
        int flag_va = new E_Select(context).getFlagVa(me.getDetilAgenModel().getJENIS_LOGIN(),
                me.getDetilAgenModel().getJENIS_LOGIN_BC(), me.getDetilAgenModel().getLca_id(), me.getDetilAgenModel().getLwk_id());

        if (!me.getPemegangPolisModel().getValidation()) {
            me.setValidation(false);
            activity.e_bundle.putParcelable(activity.getString(R.string.modelespaj), me);
            MethodSupport.Alert(context, context.getString(R.string.title_error_val), context.getString(R.string.msg_val_all), 0);
            activity.setFragment(new E_PemegangPolisFragment(), context.getResources().getString(R.string.pemegang_polis));
        } else if (!me.getTertanggungModel().getValidation()) {
            me.setValidation(false);
            activity.e_bundle.putParcelable(activity.getString(R.string.modelespaj), me);
            MethodSupport.Alert(context, context.getString(R.string.title_error_val), context.getString(R.string.msg_val_all), 0);
            activity.setFragment(new E_TertanggungFragment(), context.getResources().getString(R.string.tertanggung));
        } else if (!me.getCalonPembayarPremiModel().getValidation()) {
            me.setValidation(false);
            activity.e_bundle.putParcelable(activity.getString(R.string.modelespaj), me);
            MethodSupport.Alert(context, context.getString(R.string.title_error_val), context.getString(R.string.msg_val_all), 0);
            activity.setFragment(new E_CalonPembayarPremiFragment(), context.getResources().getString(R.string.calon_pembayar_premi));
        } else if (!me.getUsulanAsuransiModel().getValidation()) {
            me.setValidation(false);
            activity.e_bundle.putParcelable(activity.getString(R.string.modelespaj), me);
            MethodSupport.Alert(context, context.getString(R.string.title_error_val), context.getString(R.string.msg_val_all), 0);
            activity.setFragment(new E_UsulanAsuransiFragment(), context.getResources().getString(R.string.usulan_asuransi));
        } else if (!me.getDetilInvestasiModel().getValidation()) {
            me.setValidation(false);
            activity.e_bundle.putParcelable(activity.getString(R.string.modelespaj), me);
            MethodSupport.Alert(context, context.getString(R.string.title_error_val), context.getString(R.string.msg_val_all), 0);
            activity.setFragment(new E_DetilInvestasiFragment(), context.getResources().getString(R.string.detil_investasi));
        } else if (!me.getDetilAgenModel().getValidation()) {
            me.setValidation(false);
            activity.e_bundle.putParcelable(activity.getString(R.string.modelespaj), me);
            MethodSupport.Alert(context, context.getString(R.string.title_error_val), context.getString(R.string.msg_val_all), 0);
            activity.setFragment(new E_DetilAgenFragment(), context.getResources().getString(R.string.detil_agen));
        } else if (!me.getDokumenPendukungModel().getValidation() && !me.getModelID().getVal_dp()) {
            me.setValidation(false);
            activity.e_bundle.putParcelable(activity.getString(R.string.modelespaj), me);
            MethodSupport.Alert(context, context.getString(R.string.title_error_val), context.getString(R.string.msg_val_all), 0);
            activity.setFragment(new E_DokumenPendukungFragment(), context.getResources().getString(R.string.dokumen_pendukung));
        } else if (!me.getModelUpdateQuisioner().getValidation() && !me.getModelID().getVal_qu()) {
            if (me.getModelID().getJns_spaj() == 3) {
                me.setValidation(false);
                activity.e_bundle.putParcelable(activity.getString(R.string.modelespaj), me);
                MethodSupport.Alert(context, context.getString(R.string.title_error_val), context.getString(R.string.msg_val_all), 0);
                activity.setFragment(new E_QuestionnaireFragment(), context.getResources().getString(R.string.questionnaire));
            } else if (me.getModelID().getJns_spaj() == 4) {
                me.setValidation(false);
                activity.e_bundle.putParcelable(activity.getString(R.string.modelespaj), me);
                MethodSupport.Alert(context, context.getString(R.string.title_error_val), context.getString(R.string.msg_val_all), 0);
                activity.setFragment(new E_UpdateQuisionerSIOFragment(), context.getResources().getString(R.string.questionnaire));
            }
        } else {
            if (new E_Select(context).selectLinkNonLink(me.getUsulanAsuransiModel().getKd_produk_ua()) == 17) {
                if (!me.getProfileResikoModel().getValidation()) {
                    me.setValidation(false);
                    activity.e_bundle.putParcelable(activity.getString(R.string.modelespaj), me);
                    MethodSupport.Alert(context, context.getString(R.string.title_error_val), context.getString(R.string.msg_val_all), 0);
                    activity.setFragment(new E_ProfileResikoFragment(), context.getResources().getString(R.string.profil_nasabah));
                } else {
                    if (flag_va == 1) {
                        if (me.getModelID().getVa_number().equals("") || me.getModelID().getVa_number() == null) {

                            //Add an exception : Allow user to submit for Magna/Prime Link using USD currency without user has to get VA first
                            String lkuId = me.getUsulanAsuransiModel().getKurs_up_ua();
                            int lsbsId = me.getUsulanAsuransiModel().getKd_produk_ua();
                            int lsdbsNumber = me.getUsulanAsuransiModel().getSub_produk_ua();

                            if ( isUsingUSDCurrency(lkuId) && ( isMagnaLink(lsbsId,lsdbsNumber) || isPrimeLink(lsbsId,lsdbsNumber)) ) {
                                activity.setFragment(new E_KonfirmasiDanKirimFragment(), context.getResources().getString(R.string.konfirmasi_dan_kirim));
                            } else {
                                MethodSupport.Alert(context, context.getString(R.string.title_error), context.getString(R.string.msg_va2), 0);
                                me.setValidation(false);
                                activity.e_bundle.putParcelable(activity.getString(R.string.modelespaj), me);
                                activity.setFragment(new E_ProfileResikoFragment(), context.getResources().getString(R.string.profil_nasabah));
                            }

                        } else {
                            activity.setFragment(new E_KonfirmasiDanKirimFragment(), context.getResources().getString(R.string.konfirmasi_dan_kirim));
                        }
                    } else {
                        activity.setFragment(new E_KonfirmasiDanKirimFragment(), context.getResources().getString(R.string.konfirmasi_dan_kirim));
                    }
                }
            } else {
                if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 50
                        && me.getUsulanAsuransiModel().getKd_produk_ua() == 212) {
                    if (me.getModelID().getSertifikat().equals("") || me.getModelID().getSertifikat() == null) {
                        MethodSupport.Alert(context, context.getString(R.string.title_error), context.getString(R.string.msg_sertifikat_2), 0);
                        activity.setFragment(new E_UpdateQuisionerSIOFragment(), context.getResources().getString(R.string.questionnaire));
                    } else {
                        activity.setFragment(new E_KonfirmasiDanKirimFragment(), context.getResources().getString(R.string.konfirmasi_dan_kirim));
                    }
                } else {
                    if (flag_va == 1) {
                        if (me.getModelID().getVa_number().equals("") || me.getModelID().getVa_number() == null) {

                            //Add an exception : Allow user to submit for Magna/Prime Link using USD currency without user has to get VA first
                            String lkuId = me.getUsulanAsuransiModel().getKurs_up_ua();
                            int lsbsId = me.getUsulanAsuransiModel().getKd_produk_ua();
                            int lsdbsNumber = me.getUsulanAsuransiModel().getSub_produk_ua();

                            if ( isUsingUSDCurrency(lkuId) && ( isMagnaLink(lsbsId,lsdbsNumber) || isPrimeLink(lsbsId,lsdbsNumber)) ) {
                                activity.setFragment(new E_KonfirmasiDanKirimFragment(), context.getResources().getString(R.string.konfirmasi_dan_kirim));
                            } else {
                                MethodSupport.Alert(context, context.getString(R.string.title_error), context.getString(R.string.msg_va2), 0);
                                activity.e_bundle.putParcelable(activity.getString(R.string.modelespaj), me);
                                if (me.getModelID().getJns_spaj() == 3) {
                                    activity.setFragment(new E_QuestionnaireFragment(), context.getResources().getString(R.string.questionnaire));
                                } else if (me.getModelID().getJns_spaj() == 4) {
                                    activity.setFragment(new E_UpdateQuisionerSIOFragment(), context.getResources().getString(R.string.questionnaire));
                                }
                            }

                        } else {
                            activity.setFragment(new E_KonfirmasiDanKirimFragment(), context.getResources().getString(R.string.konfirmasi_dan_kirim));
                        }
                    } else {
                        activity.setFragment(new E_KonfirmasiDanKirimFragment(), context.getResources().getString(R.string.konfirmasi_dan_kirim));
                    }
                }
            }
        }
    }

    /**
     *
     * @param lkuId = kurs id, dollar id is 02
     * @return
     */
    private static boolean isUsingUSDCurrency(String lkuId) {
        return !TextUtils.isEmpty(lkuId) && lkuId.equalsIgnoreCase("02");
    }

    private static boolean isMagnaLink(int lsbsId, int lsdbsNumber) {
        return lsbsId == 213 && lsdbsNumber == 1;
    }

    private static boolean isPrimeLink(int lsbsId, int lsdbsNumber) {
        return lsbsId == 134 && lsdbsNumber == 5;
    }
}