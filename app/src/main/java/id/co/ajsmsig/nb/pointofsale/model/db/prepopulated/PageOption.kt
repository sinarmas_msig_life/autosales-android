package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by andreyyoshuamanik on 22/02/18.
 */
@Entity
class PageOption(
        @PrimaryKey
        val id: Int,
        var title: String?,
        var imageName: String? = null
): Option()