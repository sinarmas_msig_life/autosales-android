package id.co.ajsmsig.nb.crm.model.form;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by faiz_f on 17/04/2017.
 */

public class SimbakSpajModel implements Parcelable{
    private Long SLS_TAB_ID;
    private Long SLS_ID;
    private Long SLS_INST_ID;
    private Long SLS_INST_TAB_ID;
    private String SLS_PROPOSAL;
    private String SLS_SPAJ;
    private String SLS_POLIS;
    private String SLS_OLD_ID;
    private Integer SLS_INST_TYPE;
    private Integer SLS_USED;
    private String SLS_PROP_IN;
    private String SLS_SPAJ_IN;
    private String SLS_SPAJ_TMP;
    private String SLS_SPAJ_TMP_IN;
    private String SLS_CREATED;
    private String SLS_CRTD_ID;
    private String SLS_UPDATED;
    private String SLS_UPDTD_ID;

    public SimbakSpajModel() {
    }


    protected SimbakSpajModel(Parcel in) {
        SLS_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLS_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLS_INST_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLS_INST_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLS_PROPOSAL = in.readString();
        SLS_SPAJ = in.readString();
        SLS_POLIS = in.readString();
        SLS_OLD_ID = in.readString();
        SLS_INST_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLS_USED = (Integer) in.readValue(Integer.class.getClassLoader());
        SLS_PROP_IN = in.readString();
        SLS_SPAJ_IN = in.readString();
        SLS_SPAJ_TMP = in.readString();
        SLS_SPAJ_TMP_IN = in.readString();
        SLS_CREATED = in.readString();
        SLS_CRTD_ID = in.readString();
        SLS_UPDATED = in.readString();
        SLS_UPDTD_ID = in.readString();
    }

    public static final Creator<SimbakSpajModel> CREATOR = new Creator<SimbakSpajModel>() {
        @Override
        public SimbakSpajModel createFromParcel(Parcel in) {
            return new SimbakSpajModel(in);
        }

        @Override
        public SimbakSpajModel[] newArray(int size) {
            return new SimbakSpajModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(SLS_TAB_ID);
        dest.writeValue(SLS_ID);
        dest.writeValue(SLS_INST_ID);
        dest.writeValue(SLS_INST_TAB_ID);
        dest.writeString(SLS_PROPOSAL);
        dest.writeString(SLS_SPAJ);
        dest.writeString(SLS_POLIS);
        dest.writeString(SLS_OLD_ID);
        dest.writeValue(SLS_INST_TYPE);
        dest.writeValue(SLS_USED);
        dest.writeString(SLS_PROP_IN);
        dest.writeString(SLS_SPAJ_IN);
        dest.writeString(SLS_SPAJ_TMP);
        dest.writeString(SLS_SPAJ_TMP_IN);
        dest.writeString(SLS_CREATED);
        dest.writeString(SLS_CRTD_ID);
        dest.writeString(SLS_UPDATED);
        dest.writeString(SLS_UPDTD_ID);
    }

    public Long getSLS_TAB_ID() {
        return SLS_TAB_ID;
    }

    public void setSLS_TAB_ID(Long SLS_TAB_ID) {
        this.SLS_TAB_ID = SLS_TAB_ID;
    }

    public Long getSLS_ID() {
        return SLS_ID;
    }

    public void setSLS_ID(Long SLS_ID) {
        this.SLS_ID = SLS_ID;
    }

    public Long getSLS_INST_ID() {
        return SLS_INST_ID;
    }

    public void setSLS_INST_ID(Long SLS_INST_ID) {
        this.SLS_INST_ID = SLS_INST_ID;
    }

    public Long getSLS_INST_TAB_ID() {
        return SLS_INST_TAB_ID;
    }

    public void setSLS_INST_TAB_ID(Long SLS_INST_TAB_ID) {
        this.SLS_INST_TAB_ID = SLS_INST_TAB_ID;
    }

    public String getSLS_PROPOSAL() {
        return SLS_PROPOSAL;
    }

    public void setSLS_PROPOSAL(String SLS_PROPOSAL) {
        this.SLS_PROPOSAL = SLS_PROPOSAL;
    }

    public String getSLS_SPAJ() {
        return SLS_SPAJ;
    }

    public void setSLS_SPAJ(String SLS_SPAJ) {
        this.SLS_SPAJ = SLS_SPAJ;
    }

    public String getSLS_POLIS() {
        return SLS_POLIS;
    }

    public void setSLS_POLIS(String SLS_POLIS) {
        this.SLS_POLIS = SLS_POLIS;
    }

    public String getSLS_OLD_ID() {
        return SLS_OLD_ID;
    }

    public void setSLS_OLD_ID(String SLS_OLD_ID) {
        this.SLS_OLD_ID = SLS_OLD_ID;
    }

    public Integer getSLS_INST_TYPE() {
        return SLS_INST_TYPE;
    }

    public void setSLS_INST_TYPE(Integer SLS_INST_TYPE) {
        this.SLS_INST_TYPE = SLS_INST_TYPE;
    }

    public Integer getSLS_USED() {
        return SLS_USED;
    }

    public void setSLS_USED(Integer SLS_USED) {
        this.SLS_USED = SLS_USED;
    }

    public String getSLS_PROP_IN() {
        return SLS_PROP_IN;
    }

    public void setSLS_PROP_IN(String SLS_PROP_IN) {
        this.SLS_PROP_IN = SLS_PROP_IN;
    }

    public String getSLS_SPAJ_IN() {
        return SLS_SPAJ_IN;
    }

    public void setSLS_SPAJ_IN(String SLS_SPAJ_IN) {
        this.SLS_SPAJ_IN = SLS_SPAJ_IN;
    }

    public String getSLS_SPAJ_TMP() {
        return SLS_SPAJ_TMP;
    }

    public void setSLS_SPAJ_TMP(String SLS_SPAJ_TMP) {
        this.SLS_SPAJ_TMP = SLS_SPAJ_TMP;
    }

    public String getSLS_SPAJ_TMP_IN() {
        return SLS_SPAJ_TMP_IN;
    }

    public void setSLS_SPAJ_TMP_IN(String SLS_SPAJ_TMP_IN) {
        this.SLS_SPAJ_TMP_IN = SLS_SPAJ_TMP_IN;
    }

    public String getSLS_CREATED() {
        return SLS_CREATED;
    }

    public void setSLS_CREATED(String SLS_CREATED) {
        this.SLS_CREATED = SLS_CREATED;
    }

    public String getSLS_CRTD_ID() {
        return SLS_CRTD_ID;
    }

    public void setSLS_CRTD_ID(String SLS_CRTD_ID) {
        this.SLS_CRTD_ID = SLS_CRTD_ID;
    }

    public String getSLS_UPDATED() {
        return SLS_UPDATED;
    }

    public void setSLS_UPDATED(String SLS_UPDATED) {
        this.SLS_UPDATED = SLS_UPDATED;
    }

    public String getSLS_UPDTD_ID() {
        return SLS_UPDTD_ID;
    }

    public void setSLS_UPDTD_ID(String SLS_UPDTD_ID) {
        this.SLS_UPDTD_ID = SLS_UPDTD_ID;
    }
}
