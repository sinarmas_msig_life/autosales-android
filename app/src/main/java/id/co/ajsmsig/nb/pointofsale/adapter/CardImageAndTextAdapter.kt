package id.co.ajsmsig.nb.pointofsale.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosRowOptionBinding
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption

/**
 * Created by andreyyoshuamanik on 21/02/18.
 */
class CardImageAndTextAdapter: RecyclerView.Adapter<CardImageAndTextAdapter.OptionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionViewHolder {

        val dataBinding = DataBindingUtil.inflate<PosRowOptionBinding>(LayoutInflater.from(parent.context), R.layout.pos_row_option, parent, false)
        return OptionViewHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        options?.let {
            holder.bind(it[position])
        }
    }

    var options: Array<PageOption>? = null
        set(value) {
            field = value

            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return options?.size ?: 0
    }

    inner class OptionViewHolder(private val dataBinding: PosRowOptionBinding): RecyclerView.ViewHolder(dataBinding.root) {

        fun bind(option: PageOption) {
            dataBinding.vm = option
        }

        fun onClickOption(option: PageOption) {

            options?.forEach {
                it.selected.set(option == it)
            }

        }
    }
}