package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import id.co.ajsmsig.nb.espaj.model.CalonPembayarPremiModel;
import id.co.ajsmsig.nb.espaj.model.EspajModel;

/**
 * Created by Bernard on 05/09/2017.
 */

public class TableCalonPembayarPremi {
    private Context context;

    public TableCalonPembayarPremi(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(EspajModel me) {
        ContentValues cv = new ContentValues();
        cv.put("NAMA_PERUSH_CP", me.getCalonPembayarPremiModel().getNama_perush_cp());
        cv.put("ALMT_PERUSH_CP", me.getCalonPembayarPremiModel().getAlmt_perush_cp());
        cv.put("KOTA_PERUSH_CP", me.getCalonPembayarPremiModel().getKota_perush_cp());
        cv.put("KDPOS_PERUSH_CP", me.getCalonPembayarPremiModel().getKdpos_perush_cp());
        cv.put("TELP_PERUSH_CP", me.getCalonPembayarPremiModel().getTelp_perush_cp());
        cv.put("NOFAX_PERUSH_CP", me.getCalonPembayarPremiModel().getNofax_perush_cp());
        cv.put("BID_USAHA_CP", me.getCalonPembayarPremiModel().getBid_usaha_cp());
        cv.put("EDITBISNIS_CP", me.getCalonPembayarPremiModel().getEditbisnis_cp());
        cv.put("EDITPIHAK3_CP", me.getCalonPembayarPremiModel().getEditpihak3_cp());
        cv.put("EDIT_NMPIHAK3_CP", me.getCalonPembayarPremiModel().getEdit_nmpihak3_cp());
        cv.put("ALMT_PHK3_CP", me.getCalonPembayarPremiModel().getAlmt_phk3_cp());
        cv.put("NOTLP_PHK3_CP", me.getCalonPembayarPremiModel().getNotlp_phk3_cp());
        cv.put("NOTLP_KNTRPHK3_CP", me.getCalonPembayarPremiModel().getNotlp_kntrphk3_cp());
        cv.put("EDIT_EMAILPHK3_CP", me.getCalonPembayarPremiModel().getEdit_emailphk3_cp());
        cv.put("TMPT_KEDUDUKAN_CP", me.getCalonPembayarPremiModel().getTmpt_kedudukan_cp());
        cv.put("BIDUSAHA_PHK3_CP", me.getCalonPembayarPremiModel().getBidusaha_phk3_cp());
        cv.put("LAIN_JNS_PKRJAAN_CP", me.getCalonPembayarPremiModel().getLain_jns_pkrjaan_cp());
        cv.put("INSTANSI_PHK3_CP", me.getCalonPembayarPremiModel().getInstansi_phk3_cp());
        cv.put("NPWP_PHK3_CP", me.getCalonPembayarPremiModel().getNpwp_phk3_cp());
        cv.put("SUMBER_DANA_PHK3_CP", me.getCalonPembayarPremiModel().getSumber_dana_phk3_cp());
        cv.put("TUJUAN_PHK3_CP", me.getCalonPembayarPremiModel().getTujuan_phk3_cp());
        cv.put("ALASAN_PHK3_CP", me.getCalonPembayarPremiModel().getAlasan_phk3_cp());
//        cv.put("DITTD_PHK3_CP", me.getCalonPembayarPremiModel().getDittd_phk3_cp());
//        cv.put("NAMA_TTDPHK3_CP", me.getCalonPembayarPremiModel().getNama_ttdphk3_cp());
        cv.put("TGL_PENDIRIAN_CP", me.getCalonPembayarPremiModel().getTgl_pendirian_cp());
        cv.put("TGL_LHRPHK3_CP", me.getCalonPembayarPremiModel().getTgl_lhrphk3_cp());
        cv.put("TMPT_LHRPHK3_CP", me.getCalonPembayarPremiModel().getTmpt_lhrphk3_cp());
//        cv.put("PDTGL_PHK3_CP", me.getCalonPembayarPremiModel().getPdtgl_phk3_cp());
//        cv.put("KET_CP", me.getCalonPembayarPremiModel().getKet_cp());
        cv.put("INT_KET_CP", me.getCalonPembayarPremiModel().getInt_ket_cp());
//        cv.put("PROV_PERUSH_CP", me.getCalonPembayarPremiModel().getProv_perush_cp());
        cv.put("INT_PROPINSI_CP", me.getCalonPembayarPremiModel().getInt_propinsi_cp());
//        cv.put("SPINBISNIS_CP", me.getCalonPembayarPremiModel().getSpinbisnis_cp());
        cv.put("INT_SPINBISNIS_CP", me.getCalonPembayarPremiModel().getInt_spinbisnis_cp());
//        cv.put("SPINTOTAL_BLN_CP", me.getCalonPembayarPremiModel().getSpintotal_bln_cp());
        cv.put("STR_SPINTOTAL_BLN_CP", me.getCalonPembayarPremiModel().getStr_spintotal_bln_cp());
//        cv.put("SPINTOTAL_THN_CP", me.getCalonPembayarPremiModel().getSpintotal_thn_cp());
        cv.put("STR_SPINTOTAL_THN_CP", me.getCalonPembayarPremiModel().getStr_spintotal_thn_cp());
//        cv.put("SPINPIHAK3_CP", me.getCalonPembayarPremiModel().getSpinpihak3_cp());
        cv.put("INT_SPINPIHAK3_CP", me.getCalonPembayarPremiModel().getInt_spinpihak3_cp());
//        cv.put("SPIN_KEWRGN_CP", me.getCalonPembayarPremiModel().getSpin_kewrgn_cp());
        cv.put("INT_SPIN_KEWRGN_CP", me.getCalonPembayarPremiModel().getInt_spin_kewrgn_cp());
        cv.put("INT_SPIN_PEKERJAAN_CP", me.getCalonPembayarPremiModel().getSpin_pekerjaan_cp());
//        cv.put("STR_SPIN_PEKERJAAN_CP", me.getCalonPembayarPremiModel().getStr_spin_pekerjaan_cp());
//        cv.put("SPIN_JNS_PKRJAAN_CP", me.getCalonPembayarPremiModel().getSpin_jns_pkrjaan_cp());
        cv.put("STR_SPIN_JNS_PKRJAAN_CP", me.getCalonPembayarPremiModel().getStr_spin_jns_pkrjaan_cp());
//        cv.put("SPIN_JABATANPHK3_CP", me.getCalonPembayarPremiModel().getSpin_jabatanphk3_cp());
        cv.put("STR_SPIN_JABATANPHK3_CP", me.getCalonPembayarPremiModel().getStr_spin_jabatanphk3_cp());
//        cv.put("SPIN_HUB_DGPPPHK3_CP", me.getCalonPembayarPremiModel().getSpin_hub_dgppphk3_cp());
        cv.put("INT_SPIN_HUB_DGPPPHK3_CP", me.getCalonPembayarPremiModel().getInt_spin_hub_dgppphk3_cp());
//        cv.put("SUMBER_GAJI_CP", me.getCalonPembayarPremiModel().get());
        cv.put("SUMBER_GAJI_CP", me.getCalonPembayarPremiModel().getStr_gaji_cp());
//        cv.put("INT_PENGHSL_CP", me.getCalonPembayarPremiModel().getInt_penghsl_cp());
        cv.put("SUMBER_PENGHSL_CP", me.getCalonPembayarPremiModel().getStr_penghsl_cp());
//        cv.put("INT_ORTU_CP", me.getCalonPembayarPremiModel().getInt_ortu_cp());
        cv.put("SUMBER_ORTU_CP", me.getCalonPembayarPremiModel().getStr_ortu_cp());
//        cv.put("INT_LABA_CP", me.getCalonPembayarPremiModel().getInt_laba_cp());
        cv.put("SUMBER_LABA_CP", me.getCalonPembayarPremiModel().getStr_laba_cp());
//        cv.put("INT_HSLUSAHA_CP", me.getCalonPembayarPremiModel().getInt_hslusaha_cp());
        cv.put("SUMBER_HSLUSAHA_CP", me.getCalonPembayarPremiModel().getStr_hslusaha_cp());
        cv.put("SUMBER_HSLUSAHA_LAINNYA_CP", me.getCalonPembayarPremiModel().getEdit_hslusaha_cp());
//        cv.put("INT_HSLINVES_CP", me.getCalonPembayarPremiModel().getInt_hslinves_cp());
        cv.put("SUMBER_HSLINVES_CP", me.getCalonPembayarPremiModel().getStr_hslinves_cp());
        cv.put("SUMBER_HSLINVES_LAINNYA_CP", me.getCalonPembayarPremiModel().getEdit_hslinves_cp());
//        cv.put("INT_LAINNYA_CP", me.getCalonPembayarPremiModel().getInt_lainnya_cp());
        cv.put("SUMBER_LAINNYA_CP", me.getCalonPembayarPremiModel().getStr_lainnya_cp());
        cv.put("SUMBER_LAINNYA_EDIT_CP", me.getCalonPembayarPremiModel().getEdit_lainnya_cp());
//        cv.put("INT_BONUS_CP", me.getCalonPembayarPremiModel().getInt_bonus_cp());
        cv.put("SUMBER_TAHUN_BONUS_CP", me.getCalonPembayarPremiModel().getStr_bonus_cp());
//        cv.put("INT_KOMISI_CP", me.getCalonPembayarPremiModel().getInt_komisi_cp());
        cv.put("SUMBER_TAHUN_KOMISI_CP", me.getCalonPembayarPremiModel().getStr_komisi_cp());
//        cv.put("INT_ASET_CP", me.getCalonPembayarPremiModel().getInt_aset_cp());
        cv.put("SUMBER_TAHUN_ASET_CP", me.getCalonPembayarPremiModel().getStr_aset_cp());
//        cv.put("INT_HADIAH_CP", me.getCalonPembayarPremiModel().getInt_hadiah_cp());
        cv.put("SUMBER_TAHUN_HADIAH_CP", me.getCalonPembayarPremiModel().getStr_hadiah_cp());
//        cv.put("INT_HSLINVES_THN_CP", me.getCalonPembayarPremiModel().getInt_hslinves_thn_cp());
        cv.put("SUMBER_TAHUN_HSLINVES_THN_CP", me.getCalonPembayarPremiModel().getStr_hslinves_thn_cp());
        cv.put("SUMBER_TAHUN_HSLINVES_LAINNYA_THN_CP", me.getCalonPembayarPremiModel().getEdit_hslinves_thn_cp());
//        cv.put("INT_LAINNYA_THN_CP", me.getCalonPembayarPremiModel().getInt_lainnya_thn_cp());
        cv.put("SUMBER_TAHUN_LAINNYA_THN_CP", me.getCalonPembayarPremiModel().getStr_lainnya_thn_cp());
        cv.put("SUMBER_TAHUN_LAINNYA_THN_EDIT_CP", me.getCalonPembayarPremiModel().getEdit_lainnya_thn_cp());
//        cv.put("INT_PROTEKSI_CP", me.getCalonPembayarPremiModel().getInt_proteksi_cp());
        cv.put("TUJUAN_PROTEKSI_CP", me.getCalonPembayarPremiModel().getStr_proteksi_cp());
//        cv.put("INT_PEND_CP", me.getCalonPembayarPremiModel().getInt_pend_cp());
        cv.put("TUJUAN_PEND_CP", me.getCalonPembayarPremiModel().getStr_pend_cp());
//        cv.put("INT_INVES_CP", me.getCalonPembayarPremiModel().getInt_inves_cp());
        cv.put("TUJUAN_INVES_CP", me.getCalonPembayarPremiModel().getStr_inves_cp());
//        cv.put("INT_TABUNGAN_CP", me.getCalonPembayarPremiModel().getInt_tabungan_cp());
        cv.put("TUJUAN_TABUNGAN_CP", me.getCalonPembayarPremiModel().getStr_tabungan_cp());
//        cv.put("INT_PENSIUN_CP", me.getCalonPembayarPremiModel().getInt_pensiun_cp());
        cv.put("TUJUAN_PENSIUN_CP", me.getCalonPembayarPremiModel().getStr_pensiun_cp());
//        cv.put("INT_LAINNYA_TUJUAN_CP", me.getCalonPembayarPremiModel().getInt_lainnya_tujuan_cp());
        cv.put("TUJUAN_LAINNYA_TUJUAN_CP", me.getCalonPembayarPremiModel().getStr_lainnya_tujuan_cp());
        cv.put("TUJUAN_LAINNYA_TUJUAN_EDIT_CP", me.getCalonPembayarPremiModel().getEdit_lainnya_tujuan_cp());
//        cv.put("FILE_FOTO", me.getCalonPembayarPremiModel().getFile_ttd_cp());
        cv.put("VALIDATION", me.getCalonPembayarPremiModel().getValidation());

        return cv;
    }

    public CalonPembayarPremiModel getObject(Cursor cursor) {
        CalonPembayarPremiModel calonPembayarPremiModel = new CalonPembayarPremiModel();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NAMA_PERUSH_CP"))) {
                calonPembayarPremiModel.setNama_perush_cp(cursor.getString(cursor.getColumnIndexOrThrow("NAMA_PERUSH_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("ALMT_PERUSH_CP"))) {
                calonPembayarPremiModel.setAlmt_perush_cp(cursor.getString(cursor.getColumnIndexOrThrow("ALMT_PERUSH_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KOTA_PERUSH_CP"))) {
                calonPembayarPremiModel.setKota_perush_cp(cursor.getString(cursor.getColumnIndexOrThrow("KOTA_PERUSH_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KDPOS_PERUSH_CP"))) {
                calonPembayarPremiModel.setKdpos_perush_cp(cursor.getString(cursor.getColumnIndexOrThrow("KDPOS_PERUSH_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TELP_PERUSH_CP"))) {
                calonPembayarPremiModel.setTelp_perush_cp(cursor.getString(cursor.getColumnIndexOrThrow("TELP_PERUSH_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NOFAX_PERUSH_CP"))) {
                calonPembayarPremiModel.setNofax_perush_cp(cursor.getString(cursor.getColumnIndexOrThrow("NOFAX_PERUSH_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("BID_USAHA_CP"))) {
                calonPembayarPremiModel.setBid_usaha_cp(cursor.getString(cursor.getColumnIndexOrThrow("BID_USAHA_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("EDITBISNIS_CP"))) {
                calonPembayarPremiModel.setEditbisnis_cp(cursor.getString(cursor.getColumnIndexOrThrow("EDITBISNIS_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("EDITPIHAK3_CP"))) {
                calonPembayarPremiModel.setEditpihak3_cp(cursor.getString(cursor.getColumnIndexOrThrow("EDITPIHAK3_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("EDIT_NMPIHAK3_CP"))) {
                calonPembayarPremiModel.setEdit_nmpihak3_cp(cursor.getString(cursor.getColumnIndexOrThrow("EDIT_NMPIHAK3_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("ALMT_PHK3_CP"))) {
                calonPembayarPremiModel.setAlmt_phk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("ALMT_PHK3_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NOTLP_PHK3_CP"))) {
                calonPembayarPremiModel.setNotlp_phk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("NOTLP_PHK3_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NOTLP_KNTRPHK3_CP"))) {
                calonPembayarPremiModel.setNotlp_kntrphk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("NOTLP_KNTRPHK3_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("EDIT_EMAILPHK3_CP"))) {
                calonPembayarPremiModel.setEdit_emailphk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("EDIT_EMAILPHK3_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TMPT_KEDUDUKAN_CP"))) {
                calonPembayarPremiModel.setTmpt_kedudukan_cp(cursor.getString(cursor.getColumnIndexOrThrow("TMPT_KEDUDUKAN_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("BIDUSAHA_PHK3_CP"))) {
                calonPembayarPremiModel.setBidusaha_phk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("BIDUSAHA_PHK3_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("LAIN_JNS_PKRJAAN_CP"))) {
                calonPembayarPremiModel.setLain_jns_pkrjaan_cp(cursor.getString(cursor.getColumnIndexOrThrow("LAIN_JNS_PKRJAAN_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("INSTANSI_PHK3_CP"))) {
                calonPembayarPremiModel.setInstansi_phk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("INSTANSI_PHK3_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NPWP_PHK3_CP"))) {
                calonPembayarPremiModel.setNpwp_phk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("NPWP_PHK3_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_DANA_PHK3_CP"))) {
                calonPembayarPremiModel.setSumber_dana_phk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_DANA_PHK3_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TUJUAN_PHK3_CP"))) {
                calonPembayarPremiModel.setTujuan_phk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("TUJUAN_PHK3_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("ALASAN_PHK3_CP"))) {
                calonPembayarPremiModel.setAlasan_phk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("ALASAN_PHK3_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("DITTD_PHK3_CP"))){calonPembayarPremiModel.setDittd_phk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("DITTD_PHK3_CP")));}
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("NAMA_TTDPHK3_CP"))){calonPembayarPremiModel.setNama_ttdphk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("NAMA_TTDPHK3_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TGL_PENDIRIAN_CP"))) {
                calonPembayarPremiModel.setTgl_pendirian_cp(cursor.getString(cursor.getColumnIndexOrThrow("TGL_PENDIRIAN_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TGL_LHRPHK3_CP"))) {
                calonPembayarPremiModel.setTgl_lhrphk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("TGL_LHRPHK3_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PDTGL_PHK3_CP"))){calonPembayarPremiModel.setPdtgl_phk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("PDTGL_PHK3_CP")));}
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("KET_CP"))){calonPembayarPremiModel.setKet_cp(cursor.getInt(cursor.getColumnIndexOrThrow("KET_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("INT_KET_CP"))) {
                calonPembayarPremiModel.setInt_ket_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_KET_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("PROV_PERUSH_CP"))){calonPembayarPremiModel.setProv_perush_cp(cursor.getInt(cursor.getColumnIndexOrThrow("PROV_PERUSH_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("INT_PROPINSI_CP"))) {
                calonPembayarPremiModel.setInt_propinsi_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_PROPINSI_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("SPINBISNIS_CP"))){calonPembayarPremiModel.setSpinbisnis_cp(cursor.getInt(cursor.getColumnIndexOrThrow("SPINBISNIS_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("INT_SPINBISNIS_CP"))) {
                calonPembayarPremiModel.setInt_spinbisnis_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_SPINBISNIS_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("SPINTOTAL_BLN_CP"))){calonPembayarPremiModel.setSpintotal_bln_cp(cursor.getInt(cursor.getColumnIndexOrThrow("SPINTOTAL_BLN_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("STR_SPINTOTAL_BLN_CP"))) {
                calonPembayarPremiModel.setStr_spintotal_bln_cp(cursor.getString(cursor.getColumnIndexOrThrow("STR_SPINTOTAL_BLN_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("SPINTOTAL_THN_CP"))){calonPembayarPremiModel.setSpintotal_thn_cp(cursor.getInt(cursor.getColumnIndexOrThrow("SPINTOTAL_THN_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("STR_SPINTOTAL_THN_CP"))) {
                calonPembayarPremiModel.setStr_spintotal_thn_cp(cursor.getString(cursor.getColumnIndexOrThrow("STR_SPINTOTAL_THN_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("SPINPIHAK3_CP"))){calonPembayarPremiModel.setSpinpihak3_cp(cursor.getInt(cursor.getColumnIndexOrThrow("SPINPIHAK3_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("INT_SPINPIHAK3_CP"))) {
                calonPembayarPremiModel.setInt_spinpihak3_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_SPINPIHAK3_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("SPIN_KEWRGN_CP"))){calonPembayarPremiModel.setSpin_kewrgn_cp(cursor.getInt(cursor.getColumnIndexOrThrow("SPIN_KEWRGN_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("INT_SPIN_KEWRGN_CP"))) {
                calonPembayarPremiModel.setInt_spin_kewrgn_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_SPIN_KEWRGN_CP")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_SPIN_PEKERJAAN_CP"))){calonPembayarPremiModel.setSpin_pekerjaan_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_SPIN_PEKERJAAN_CP")));}
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("STR_SPIN_PEKERJAAN_CP"))){calonPembayarPremiModel.setStr_spin_pekerjaan_cp(cursor.getString(cursor.getColumnIndexOrThrow("STR_SPIN_PEKERJAAN_CP")));}
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("SPIN_JNS_PKRJAAN_CP"))){calonPembayarPremiModel.setSpin_jns_pkrjaan_cp(cursor.getInt(cursor.getColumnIndexOrThrow("SPIN_JNS_PKRJAAN_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("STR_SPIN_JNS_PKRJAAN_CP"))) {
                calonPembayarPremiModel.setStr_spin_jns_pkrjaan_cp(cursor.getString(cursor.getColumnIndexOrThrow("STR_SPIN_JNS_PKRJAAN_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("SPIN_JABATANPHK3_CP"))){calonPembayarPremiModel.setSpin_jabatanphk3_cp(cursor.getInt(cursor.getColumnIndexOrThrow("SPIN_JABATANPHK3_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("STR_SPIN_JABATANPHK3_CP"))) {
                calonPembayarPremiModel.setStr_spin_jabatanphk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("STR_SPIN_JABATANPHK3_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("SPIN_HUB_DGPPPHK3_CP"))){calonPembayarPremiModel.setSpin_hub_dgppphk3_cp(cursor.getInt(cursor.getColumnIndexOrThrow("SPIN_HUB_DGPPPHK3_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("INT_SPIN_HUB_DGPPPHK3_CP"))) {
                calonPembayarPremiModel.setInt_spin_hub_dgppphk3_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_SPIN_HUB_DGPPPHK3_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_GAJI_CP"))){calonPembayarPremiModel.setInt_gaji_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_GAJI_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_GAJI_CP"))) {
                calonPembayarPremiModel.setStr_gaji_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_GAJI_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_PENGHSL_CP"))){calonPembayarPremiModel.setInt_penghsl_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_PENGHSL_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_PENGHSL_CP"))) {
                calonPembayarPremiModel.setStr_penghsl_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_PENGHSL_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_ORTU_CP"))){calonPembayarPremiModel.setInt_ortu_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_ORTU_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_ORTU_CP"))) {
                calonPembayarPremiModel.setStr_ortu_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_ORTU_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_LABA_CP"))){calonPembayarPremiModel.setInt_laba_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_LABA_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_LABA_CP"))) {
                calonPembayarPremiModel.setStr_laba_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_LABA_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_HSLUSAHA_CP"))){calonPembayarPremiModel.setInt_hslusaha_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_HSLUSAHA_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_HSLUSAHA_CP"))) {
                calonPembayarPremiModel.setStr_hslusaha_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_HSLUSAHA_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_HSLUSAHA_LAINNYA_CP"))) {
                calonPembayarPremiModel.setEdit_hslusaha_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_HSLUSAHA_LAINNYA_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_HSLINVES_CP"))){calonPembayarPremiModel.setInt_hslinves_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_HSLINVES_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_HSLINVES_CP"))) {
                calonPembayarPremiModel.setStr_hslinves_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_HSLINVES_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_HSLINVES_LAINNYA_CP"))) {
                calonPembayarPremiModel.setEdit_hslinves_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_HSLINVES_LAINNYA_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_LAINNYA_CP"))){calonPembayarPremiModel.setInt_lainnya_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_LAINNYA_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_LAINNYA_CP"))) {
                calonPembayarPremiModel.setStr_lainnya_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_LAINNYA_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_LAINNYA_EDIT_CP"))) {
                calonPembayarPremiModel.setEdit_lainnya_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_LAINNYA_EDIT_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_BONUS_CP"))){calonPembayarPremiModel.setInt_bonus_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_BONUS_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_BONUS_CP"))) {
                calonPembayarPremiModel.setStr_bonus_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_BONUS_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_KOMISI_CP"))){calonPembayarPremiModel.setInt_komisi_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_KOMISI_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_KOMISI_CP"))) {
                calonPembayarPremiModel.setStr_komisi_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_KOMISI_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_ASET_CP"))){calonPembayarPremiModel.setInt_aset_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_ASET_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_ASET_CP"))) {
                calonPembayarPremiModel.setStr_aset_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_ASET_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_HADIAH_CP"))){calonPembayarPremiModel.setInt_hadiah_cp(cursor.getInt(cursor.getColumnIndexOrThrow("STR_ASET_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_HADIAH_CP"))) {
                calonPembayarPremiModel.setStr_hadiah_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_HADIAH_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_HSLINVES_THN_CP"))){calonPembayarPremiModel.setInt_hslinves_thn_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_HSLINVES_THN_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_HSLINVES_THN_CP"))) {
                calonPembayarPremiModel.setStr_hslinves_thn_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_HSLINVES_THN_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_HSLINVES_LAINNYA_THN_CP"))) {
                calonPembayarPremiModel.setEdit_hslinves_thn_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_HSLINVES_LAINNYA_THN_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_LAINNYA_THN_CP"))){calonPembayarPremiModel.setInt_lainnya_thn_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_LAINNYA_THN_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_LAINNYA_THN_CP"))) {
                calonPembayarPremiModel.setStr_lainnya_thn_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_LAINNYA_THN_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_LAINNYA_THN_EDIT_CP"))) {
                calonPembayarPremiModel.setEdit_lainnya_thn_cp(cursor.getString(cursor.getColumnIndexOrThrow("SUMBER_TAHUN_LAINNYA_THN_EDIT_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_PROTEKSI_CP"))){calonPembayarPremiModel.setInt_proteksi_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_PROTEKSI_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TUJUAN_PROTEKSI_CP"))) {
                calonPembayarPremiModel.setStr_proteksi_cp(cursor.getString(cursor.getColumnIndexOrThrow("TUJUAN_PROTEKSI_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_PEND_CP"))){calonPembayarPremiModel.setInt_pend_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_PEND_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TUJUAN_PEND_CP"))) {
                calonPembayarPremiModel.setStr_pend_cp(cursor.getString(cursor.getColumnIndexOrThrow("TUJUAN_PEND_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_INVES_CP"))){calonPembayarPremiModel.setInt_inves_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_INVES_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TUJUAN_INVES_CP"))) {
                calonPembayarPremiModel.setStr_inves_cp(cursor.getString(cursor.getColumnIndexOrThrow("TUJUAN_INVES_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_TABUNGAN_CP"))){calonPembayarPremiModel.setInt_tabungan_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_TABUNGAN_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TUJUAN_TABUNGAN_CP"))) {
                calonPembayarPremiModel.setStr_tabungan_cp(cursor.getString(cursor.getColumnIndexOrThrow("TUJUAN_TABUNGAN_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_PENSIUN_CP"))){calonPembayarPremiModel.setInt_pensiun_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_PENSIUN_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TUJUAN_PENSIUN_CP"))) {
                calonPembayarPremiModel.setStr_pensiun_cp(cursor.getString(cursor.getColumnIndexOrThrow("TUJUAN_PENSIUN_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("INT_LAINNYA_TUJUAN_CP"))){calonPembayarPremiModel.setInt_lainnya_tujuan_cp(cursor.getInt(cursor.getColumnIndexOrThrow("INT_LAINNYA_TUJUAN_CP")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TUJUAN_LAINNYA_TUJUAN_CP"))) {
                calonPembayarPremiModel.setStr_lainnya_tujuan_cp(cursor.getString(cursor.getColumnIndexOrThrow("TUJUAN_LAINNYA_TUJUAN_CP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TUJUAN_LAINNYA_TUJUAN_EDIT_CP"))) {
                calonPembayarPremiModel.setEdit_lainnya_tujuan_cp(cursor.getString(cursor.getColumnIndexOrThrow("TUJUAN_LAINNYA_TUJUAN_EDIT_CP")));
            }
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow("FILE_FOTO"))){calonPembayarPremiModel.setFile_ttd_cp(cursor.getString(cursor.getColumnIndexOrThrow("FILE_FOTO")));}
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TMPT_LHRPHK3_CP"))) {
                calonPembayarPremiModel.setTmpt_lhrphk3_cp(cursor.getString(cursor.getColumnIndexOrThrow("TMPT_LHRPHK3_CP")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("VALIDATION"))){
                int valid = cursor.getInt(cursor.getColumnIndexOrThrow("VALIDATION"));
//                boolean isValid = valid == 1 ? true : false;
                boolean val;
                val = valid == 1;
                calonPembayarPremiModel.setValidation(val);
            }
            cursor.close();
        }
        return calonPembayarPremiModel;
    }

}
