package id.co.ajsmsig.nb.crm.database;
/*
 Created by faiz_f on 10/08/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.form.SimbakAuditModel;

public class SimbakAuditTable {
    private Context context;

    public SimbakAuditTable(Context context) {
        this.context = context;
    }

    public ArrayList<SimbakAuditModel> getArrayObject(Cursor cursor) {
        ArrayList<SimbakAuditModel> simbakAuditModels = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            SimbakAuditModel auditModel = new SimbakAuditModel();
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_TAB_ID))))
                auditModel.setSA2_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SA2_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_ID))))
                auditModel.setSA2_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SA2_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_USERID))))
                auditModel.setSA2_USERID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA2_USERID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_INST_ID))))
                auditModel.setSA2_INST_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA2_INST_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_INST_TYPE))))
                auditModel.setSA2_INST_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SA2_INST_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_DETAIL))))
                auditModel.setSA2_DETAIL(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA2_DETAIL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_DATA))))
                auditModel.setSA2_DATA(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA2_DATA))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_ACTION))))
                auditModel.setSA2_ACTION(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA2_ACTION))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_APP))))
                auditModel.setSA2_APP(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SA2_APP))));


            simbakAuditModels.add(auditModel);
            cursor.moveToNext();
        }
        cursor.close();
        return simbakAuditModels;
    }

    public SimbakAuditModel getObject(Cursor cursor) {
        SimbakAuditModel auditModel = new SimbakAuditModel();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_TAB_ID))))
                auditModel.setSA2_TAB_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SA2_TAB_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_ID))))
                auditModel.setSA2_ID(cursor.getLong(cursor.getColumnIndex(context.getString(R.string.SA2_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_USERID))))
                auditModel.setSA2_USERID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA2_USERID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_INST_ID))))
                auditModel.setSA2_INST_ID(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA2_INST_ID))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_INST_TYPE))))
                auditModel.setSA2_INST_TYPE(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SA2_INST_TYPE))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_DETAIL))))
                auditModel.setSA2_DETAIL(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA2_DETAIL))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_DATA))))
                auditModel.setSA2_DATA(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA2_DATA))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_ACTION))))
                auditModel.setSA2_ACTION(cursor.getString(cursor.getColumnIndex(context.getString(R.string.SA2_ACTION))));
            if (!cursor.isNull(cursor.getColumnIndex(context.getString(R.string.SA2_APP))))
                auditModel.setSA2_APP(cursor.getInt(cursor.getColumnIndex(context.getString(R.string.SA2_APP))));
            cursor.close();
        }
        return auditModel;
    }

    public ContentValues toContentValues(SimbakAuditModel auditModel) {
        ContentValues contentValues = new ContentValues();

        if (auditModel.getSA2_TAB_ID() != null)
            contentValues.put(context.getString(R.string.SA2_TAB_ID), auditModel.getSA2_TAB_ID());
        if (auditModel.getSA2_ID() != null)
            contentValues.put(context.getString(R.string.SA2_ID), auditModel.getSA2_ID());
        if (auditModel.getSA2_USERID() != null)
            contentValues.put(context.getString(R.string.SA2_USERID), auditModel.getSA2_USERID());
        if (auditModel.getSA2_INST_ID() != null)
            contentValues.put(context.getString(R.string.SA2_INST_ID), auditModel.getSA2_INST_ID());
        if (auditModel.getSA2_INST_TYPE() != null)
            contentValues.put(context.getString(R.string.SA2_INST_TYPE), auditModel.getSA2_INST_TYPE());
        if (auditModel.getSA2_DETAIL() != null)
            contentValues.put(context.getString(R.string.SA2_DETAIL), auditModel.getSA2_DETAIL());
        if (auditModel.getSA2_DATA() != null)
            contentValues.put(context.getString(R.string.SA2_DATA), auditModel.getSA2_DATA());
        if (auditModel.getSA2_ACTION() != null)
            contentValues.put(context.getString(R.string.SA2_ACTION), auditModel.getSA2_ACTION());
        if (auditModel.getSA2_APP() != null)
            contentValues.put(context.getString(R.string.SA2_APP), auditModel.getSA2_APP());

        return contentValues;
    }

}
