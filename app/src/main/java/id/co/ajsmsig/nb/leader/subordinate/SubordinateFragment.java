package id.co.ajsmsig.nb.leader.subordinate;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.activity.C_LoginActivity;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.leader.subordinatedetail.SubordinateDetail;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;


public class SubordinateFragment extends Fragment implements SubordinateListAdapter.OnItemPressed {
    private ArrayList<Subordinate> dataSubordinate = new ArrayList<>();
    private SubordinateListAdapter mAdapter;
    private String agentCode;

    public SubordinateFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.tvUserLogin)
    TextView tvUserLogin;

    @BindView(R.id.tvSelectedBank)
    TextView tvSelectedBank;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.layoutNoInternetConnection)
    ConstraintLayout layoutNoInternetConnection;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.profileUser)
    ConstraintLayout profileUser;

    @BindView(R.id.btnChoiceBank)
    Button btnChoiceBank;

    @BindView(R.id.tvAgentInitialNameDetail)
    TextView tvAgentInitialNameDetail;


    @OnClick(R.id.btnChoiceBank)
    void changeChannel(){
        //getSubordinate(agentCode, getUserSelectedChannelId());
        refetchGetUserChannel(agentCode);
    }

    
    @OnClick(R.id.btnRetry)
    void onBtnRetryPressed() {

        if (StaticMethods.isNetworkAvailable(getActivity())) {
            getUserChannel(agentCode);
            hideNoInternetConnection();
        } else {
            showNoInternetConnection();
            Toast.makeText(getActivity(),"Anda tidak terkoneksi ke internet. Mohon pastikan Anda terkoneksi dengan internet",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for getActivity() fragment
        return inflater.inflate(R.layout.fragment_subordinate, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        agentCode = sharedPreferences.getString("KODE_AGEN", "");
        String agentName = sharedPreferences.getString("NAMA_AGEN", "");

        displayLoggedInUserInfo(agentName, getUserSelectedChannelName());

        initView();

        if (StaticMethods.isNetworkAvailable(getActivity())) {
            getUserChannel(agentCode);
            hideNoInternetConnection();
        } else {
            showNoInternetConnection();
        }

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_leader, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.action_logout :
                logout();
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        mAdapter = new SubordinateListAdapter(dataSubordinate,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(mAdapter);
    }


    private void getSubordinate(String agentCode, String bcLoginType) {
        showProgressBar();
        String url = AppConfig.getBaseUrlWs().concat("member/api/json/getsubordinate");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<GetSubordinateResponse> call = api.getSubordinate(url, agentCode, bcLoginType);
        call.enqueue(new Callback<GetSubordinateResponse>() {
            @Override
            public void onResponse(Call<GetSubordinateResponse> call, Response<GetSubordinateResponse> response) {
                GetSubordinateResponse responseBody = response.body();
                if (responseBody != null) {

                    boolean error = responseBody.isError();
                    String message = responseBody.getMessage();
                    DataSubordinate data = responseBody.getDataSubordinate();
                    ArrayList<Subordinate> subordinates = (ArrayList<Subordinate>) data.getSubordinate();

                    if (!error) {
                        displaySubordinateData(subordinates);
                    } else {
                        Toast.makeText(getActivity(), "Terjadi kesalahan " + message, Toast.LENGTH_SHORT).show();
                    }
                    hideProgressBar();
                    showRecyclerView();
                    profileShow();
                }
            }

            @Override
            public void onFailure(Call<GetSubordinateResponse> call, Throwable t) {

                if (StaticMethods.isNetworkAvailable(getActivity())) {
                    getSubordinate(agentCode,bcLoginType);
                    hideNoInternetConnection();
                } else {
                    showNoInternetConnection();
                }

                Toast.makeText(getActivity(), "Tidak dapat menampilkan subordinate "+t.getMessage(), Toast.LENGTH_SHORT).show();
                hideRecyclerView();
                hideProgressBar();
               profileHide();
            }
        });
    }

    private void getUserChannel(String agentCode) {
        String url = AppConfig.getBaseUrlWs().concat("member/api/json/getbankchannel");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<GetJenisChannelResponse> call = api.getJenisChannel(url, agentCode);
        call.enqueue(new Callback<GetJenisChannelResponse>() {
            @Override
            public void onResponse(Call<GetJenisChannelResponse> call, Response<GetJenisChannelResponse> response) {
                GetJenisChannelResponse responsebody = response.body();
                if (responsebody != null) {
                    String message = responsebody.getMessage();
                    boolean error = responsebody.isError();
                    List<Data> data = responsebody.getData();
                    if (!error) {
                        fetchSubordinateFromServer(data, agentCode);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetJenisChannelResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Tidak dapat menampilkan opsi pilih channel "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void refetchGetUserChannel(String agentCode) {
        String url = AppConfig.getBaseUrlWs().concat("member/api/json/getbankchannel");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<GetJenisChannelResponse> call = api.getJenisChannel(url, agentCode);
        call.enqueue(new Callback<GetJenisChannelResponse>() {
            @Override
            public void onResponse(Call<GetJenisChannelResponse> call, Response<GetJenisChannelResponse> response) {
                GetJenisChannelResponse responsebody = response.body();
                if (responsebody != null) {
                    String message = responsebody.getMessage();
                    boolean error = responsebody.isError();
                    List<Data> data = responsebody.getData();

                    if (!error && !data.isEmpty() && data != null) {
                        showSelectChannelDialog(data, agentCode);
                    }

                }
            }

            @Override
            public void onFailure(Call<GetJenisChannelResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Tidak dapat menampilkan opsi pilih channel "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void displaySubordinateData(ArrayList<Subordinate> subordinates) {

        //To get initial name from agent name, and then set it to the model
        for (int i=0; i<subordinates.size(); i++) {
            Subordinate subordinate = subordinates.get(i);
            String agentName = subordinate.getNAMAREFF();
            String initialName = getAgentInitialName(agentName);
            subordinate.setInitialName(initialName);
        }
        //Ada list bawahan
        if (subordinates.size() > 0) {
            mAdapter.refreshSubordinate(subordinates);
        } else {
            //Tidak punya bawahan
            mAdapter.refreshSubordinate(subordinates);
            showNoSubordinateDialog();
        }
    }


    private void fetchSubordinateFromServer(List<Data> data, String agentCode) {

        if (data != null) {

            String channelId = data.get(0).getJENIS();
            String userLogged = data.get(0).getNAMAREFF();
            String channelBankName;

            if (!TextUtils.isEmpty(getUserSelectedChannelName())) {
                channelBankName = getUserSelectedChannelName();
            } else {
                channelBankName = data.get(0).getNMBANK();
            }



            displayLoggedInUserInfo(userLogged, channelBankName);


            if (isUserOnlyHaveOneChannel(data)) {

                //Hanya punya satu channel, jangan tampilkan dialog.
                getSubordinate(agentCode, channelId);
                hideButtonChangeChannel();

            } else {
                //Punya lebih dari satu channel, tampilkan dialog untuk pilih channel yang ingin dimonitor
                showButtonChangeChannel();

                //Kalau sebelumnya sudah pilih channel, jgn tampilkan lagi dialognya, langsung hit api get subordinate
                if (isUserHasAlreadySelectBankChannel()) {
                    getSubordinate(agentCode, getUserSelectedChannelId());
                } else {
                    //tampilin dialog biar bisa pilih channel
                    showSelectChannelDialog(data, agentCode);
                }

            }

        }

    }


    @Override
    public void onSubordinatePressed(int position, Subordinate subordinate) {
        Intent intent = new Intent(getActivity(), SubordinateDetail.class);
        String msagId = subordinate.getMSAGID();
        String agentName = subordinate.getNAMAREFF();
        String keyChannel = subordinate.getJENIS();
        intent.putExtra(Const.INTENT_KEY_AGENT_CODE, msagId);
        intent.putExtra(Const.INTENT_KEY_AGENT_NAME, agentName);
        intent.putExtra(Const.INTENT_KEY_CHANNEL,keyChannel);
        startActivity(intent);
    }

    private boolean isUserOnlyHaveOneChannel(List<Data> data) {
        return data.size() == 1;
    }

    /**
     * When user has 2 bank channels or more
     * @param data
     * @param agentCode
     */
    private void showSelectChannelDialog(List<Data> data, String agentCode) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Pilih channel bank");

        List<String> channelNames = new ArrayList<>();
        List<String> channelIds = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {

            String channelName = data.get(i).getNMBANK();
            String channelId = data.get(i).getJENIS();

            channelNames.add(channelName);
            channelIds.add(channelId);
        }

        CharSequence[] channels = channelNames.toArray(new CharSequence[channelNames.size()]);


        alert.setSingleChoiceItems(channels, 0, null);

        alert.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();

            //Tandai kalau user sudah pernah pilih channel beserta channel id nya
            setUserSelectedChannelId(channelIds.get(selectedPosition));
            setUserHasSelectChannelPreviously(true);
            setUserSelectedChannelName(channelNames.get(selectedPosition));

            displayLoggedInUserInfo(data.get(selectedPosition).getNAMAREFF(), data.get(selectedPosition).getNMBANK());

            getSubordinate(agentCode, channelIds.get(selectedPosition));


            dialog.dismiss();
        });


        AlertDialog mDialog = alert.create();
        mDialog.show();
    }

    public void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Logout");
        builder.setMessage("Apakah Anda yakin ingin melakukan logout?");
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton("Log Out", (dialog, which) -> {
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_preferences), MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();
            Intent intent = new Intent(getContext(), C_LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
        });
        builder.show();
    }

    private void showRecyclerView() {
        recyclerView.setVisibility(View.VISIBLE);

    }
    private void hideRecyclerView() {
        recyclerView.setVisibility(View.GONE);

    }
    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);

    }
    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);

    }
    private void showNoInternetConnection() {
        layoutNoInternetConnection.setVisibility(View.VISIBLE);

    }
    private void hideNoInternetConnection() {
        layoutNoInternetConnection.setVisibility(View.GONE);

    }

    private void profileShow() {
    profileUser.setVisibility(View.VISIBLE);

    }

    private void profileHide() {
        profileUser.setVisibility(View.GONE);

    }

    private void showButtonChangeChannel() {
        btnChoiceBank.setVisibility(View.VISIBLE);
    }

    private void hideButtonChangeChannel() {
        btnChoiceBank.setVisibility(View.GONE);
    }





    private String getAgentInitialName(String agentName) {

        if (!TextUtils.isEmpty(agentName)) {
            String[] splitByWhiteSpace  = agentName.split("\\s+");

            if (splitByWhiteSpace != null && splitByWhiteSpace.length > 0) {

                if (splitByWhiteSpace.length == 3) {

                    //Name contains first, middle, last name
                    if ( !TextUtils.isEmpty(splitByWhiteSpace[0]) && !TextUtils.isEmpty(splitByWhiteSpace[2]) ) {
                        String agentNameFirstNameInitial = splitByWhiteSpace[0];
                        agentNameFirstNameInitial = String.valueOf(agentNameFirstNameInitial.charAt(0)).toUpperCase();

                        String agentNameLastNameInital = splitByWhiteSpace[2];
                        agentNameLastNameInital = String.valueOf(agentNameLastNameInital.charAt(0)).toUpperCase();

                        String firstLastNameInitial = agentNameFirstNameInitial + agentNameLastNameInital;

                        return firstLastNameInitial;
                    }


                } else if(splitByWhiteSpace.length == 2) {

                    if ( !TextUtils.isEmpty(splitByWhiteSpace[0]) && !TextUtils.isEmpty(splitByWhiteSpace[1]) ) {
                        //Name contains first, middle name only
                        String agentNameFirstNameInitial = splitByWhiteSpace[0];
                        agentNameFirstNameInitial = String.valueOf(agentNameFirstNameInitial.charAt(0)).toUpperCase();

                        String agentNameLastNameInital = splitByWhiteSpace[1];
                        agentNameLastNameInital = String.valueOf(agentNameLastNameInital.charAt(0)).toUpperCase();

                        String firstLastNameInitial = agentNameFirstNameInitial + agentNameLastNameInital;

                        return firstLastNameInitial;

                    }


                } else if (splitByWhiteSpace.length == 1) {

                    if (!TextUtils.isEmpty(splitByWhiteSpace[0])) {
                        //Name contains first name only
                        String agentNameFirstNameInitial = splitByWhiteSpace[0];
                        agentNameFirstNameInitial = String.valueOf(agentNameFirstNameInitial.charAt(0)).toUpperCase();

                        String agentNameLastNameInital = "";

                        String firstLastNameInitial = agentNameFirstNameInitial + agentNameLastNameInital;

                        return firstLastNameInitial;
                    }

                }
            }

        }

        return "";
    }

    private void showNoSubordinateDialog() {
        new AlertDialog.Builder(getActivity())
                .setTitle("Tidak ada subordinate")
                .setMessage("Mohon maaf, saat ini Anda belum memiliki subordinate yang bisa ditampilkan")
                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void displayLoggedInUserInfo(String userName, String channelName) {
        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(channelName)) {
            tvUserLogin.setText(userName);
            tvSelectedBank.setText(channelName);
            tvAgentInitialNameDetail.setText(getAgentInitialName(userName));
        }

    }

    private void setUserSelectedChannelId(String channelId) {
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Const.PREF_KEY_SELECTED_CHANNEL_ID, channelId);
        editor.apply();
    }

    private void setUserHasSelectChannelPreviously(boolean hasSelectChannel) {
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(Const.PREF_KEY_IS_CHANNEL_SELECTED, hasSelectChannel);
        editor.apply();
    }

    private void setUserSelectedChannelName(String channelName) {
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Const.PREF_KEY_SELECTED_CHANNEL_NAME, channelName);
        editor.apply();
    }

    private boolean isUserHasAlreadySelectBankChannel() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        return sharedPreferences.getBoolean(Const.PREF_KEY_IS_CHANNEL_SELECTED, false);
    }

    private String getUserSelectedChannelId() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        return sharedPreferences.getString(Const.PREF_KEY_SELECTED_CHANNEL_ID, null);
    }

    private String getUserSelectedChannelName() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        return sharedPreferences.getString(Const.PREF_KEY_SELECTED_CHANNEL_NAME, null);
    }

}
