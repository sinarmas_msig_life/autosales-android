package id.co.ajsmsig.nb.pointofsale.ui.lproductpresentation

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Transformations
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import com.downloader.Error
import com.downloader.Progress
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.PlaybackParameters
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.Timeline
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import id.co.ajsmsig.nb.pointofsale.binding.DownloadListener
import id.co.ajsmsig.nb.pointofsale.binding.LoadingButtonListener
import id.co.ajsmsig.nb.pointofsale.model.Resource
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Analytics
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Product
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductRider
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.ProductDetail
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import java.util.*
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 08/03/18.
 */
class ProductPresentationVideoVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository, var sharedViewModel: SharedViewModel, val mainRepository: MainRepository): AndroidViewModel(application){


    val observablePageOptions = MediatorLiveData<List<PageOption>>()
    val observableProductDetails = MediatorLiveData<List<ProductDetail>>()
    val loadingVideo = ObservableBoolean(true)
    val noVideo = ObservableBoolean(false)
    val noVideoImage = "no_video.png"
    var product: Product? = null
    var videoUrl: ObservableField<String> = ObservableField()
    var videoDownloadProgress = ObservableInt()
    var onDownloadVideoListener: DownloadListener = object: DownloadListener {
        override fun onStartOrResume() {
            loadingVideo.set(true)
        }

        override fun onPause() {

        }

        override fun onCancel() {

        }

        override fun onDownloadComplete() {
            loadingVideo.set(false)
        }

        override fun onError(error: Error?) {
            loadingVideo.set(false)
        }

        override fun onProgress(progress: Progress?) {
            val cur = progress?.currentBytes ?: 0
            val total = progress?.totalBytes ?: 0
            videoDownloadProgress.set((cur.toFloat() / total.toFloat() * 100F).toInt())
        }

    }
    val startDownload = ObservableBoolean(false)
    val isVideoEnded = ObservableBoolean(false)

    val playerListener = object : Player.EventListener {
        override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {

        }

        override fun onSeekProcessed() {

        }

        override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {

        }

        override fun onPlayerError(error: ExoPlaybackException?) {
            noVideo.set(true)
            loadingVideo.set(false)
        }

        override fun onLoadingChanged(isLoading: Boolean) {

        }

        override fun onPositionDiscontinuity(reason: Int) {

        }

        override fun onRepeatModeChanged(repeatMode: Int) {

        }

        override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {

        }

        override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {

        }

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            if (playbackState == Player.STATE_READY) {
                loadingVideo.set(false)
                isVideoEnded.set(false)
            } else if (playbackState == Player.STATE_ENDED) {
                isVideoEnded.set(true)
            }
        }

    }

    val observableProductRider = MediatorLiveData<Resource<List<ProductRider>>>()
    val observableIsThereRider = Transformations.map(observableProductRider, {
        (it.data?.size ?: 0) > 0
    })


    var page: Page? = null
        set(value) {
            if (field == null) {
                field = value

                val pageOptions = repository.getPageOptionsFromPage(value)
                observablePageOptions.addSource(pageOptions, observablePageOptions::setValue)


                val productDetails = ArrayList<ProductDetail>()
                sharedViewModel.selectedProduct?.let {

                    product = it

                    product?.Sheet?.let {
                        productDetails.add(ProductDetail("Product Sheet", it, eventName = "Product Sheet - ${product?.LsbsName}"))
                    }

                    product?.FundFactSheet?.let {
                        productDetails.add(ProductDetail("Fund Fact Sheet", it, eventName = "Fund Fact Sheet - ${product?.LsbsName}"))
                    }

                    product?.Brosur?.let {
                        productDetails.add(ProductDetail("Brosur", it, eventName = "Brosur - ${product?.LsbsName}"))
                    }

                    videoUrl.set(it.Presentation)

                    observableProductRider.addSource(mainRepository.getProductRidersWith(it.LsbsId, sharedViewModel.groupId), observableProductRider::setValue)
                }

                observableProductDetails.postValue(productDetails)

            }
        }

    val loadingButtonListener = object : LoadingButtonListener {
        override fun onClick(eventName: String?) {
            mainRepository.sendAnalytics(
                    Analytics(
                            agentCode = sharedViewModel.agentCode,
                            agentName = sharedViewModel.agentName,
                            group = sharedViewModel.groupId,
                            event = eventName,
                            timestamp = System.currentTimeMillis()
                    ))
        }

    }
}