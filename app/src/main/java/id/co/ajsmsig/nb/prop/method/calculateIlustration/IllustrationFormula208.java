package id.co.ajsmsig.nb.prop.method.calculateIlustration;


import android.content.Context;
import android.util.Log;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import id.co.ajsmsig.nb.prop.method.util.CommonUtil;
import id.co.ajsmsig.nb.prop.method.util.ProposalStringFormatter;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.IllustrationResultVO;


public class IllustrationFormula208 extends IllustrationFormula {

    IllustrationFormula208(Context context) {
        super(context);
    }

    @Override
    public HashMap<String, Object> getIllustrationResult(String no_proposal) {
        ArrayList<LinkedHashMap<String, String>> mapList = new ArrayList<>();
        LinkedHashMap<String, String> map;
        double[][] tahapan = new double[4][5];
        tahapan[0][0] = 0.05;
        tahapan[0][1] = 0.1;
        tahapan[0][2] = 0.2;
        tahapan[0][3] = 0.5;
        tahapan[0][4] = 0.15;

        tahapan[1][0] = 0;
        tahapan[1][1] = 0.1;
        tahapan[1][2] = 0.2;
        tahapan[1][3] = 0.5;
        tahapan[1][4] = 0.2;

        tahapan[2][0] = 0;
        tahapan[2][1] = 0;
        tahapan[2][2] = 0.2;
        tahapan[2][3] = 0.5;
        tahapan[2][4] = 0.3;

        tahapan[3][0] = 0;
        tahapan[3][1] = 0;
        tahapan[3][2] = 0;
        tahapan[3][3] = 0.5;
        tahapan[3][4] = 0.5;
        double[] listTahapan = new double[23];
        double tmpTahapan = 0;
        int usiaTT = 0;
        double up = 0.0;
        String lku_id = "";
        int bisnisId = 0, bisnisNo = 0, lscb_kali = 0, lscb_id = 0, lamaBayar = 0, flag_tt_calon_bayi = 0;

        HashMap<String, Object> dataProposal = getSelect().selectDataProposal(no_proposal);
        if (!CommonUtil.isEmpty(dataProposal)) {
            bisnisId = ((BigDecimal) dataProposal.get("LSBS_ID")).intValue();
            bisnisNo = ((BigDecimal) dataProposal.get("LSDBS_NUMBER")).intValue();
            up = Double.valueOf(dataProposal.get("UP").toString());
            lscb_kali = Integer.valueOf(dataProposal.get("LSCB_KALI").toString());
            lscb_id = Integer.valueOf(dataProposal.get("LSCB_ID").toString());
            lku_id = dataProposal.get("LKU_ID").toString();
            lamaBayar = Integer.valueOf(dataProposal.get("THN_LAMA_BAYAR").toString());
            flag_tt_calon_bayi = Integer.valueOf(dataProposal.get("FLAG_TT_CALON_BAYI").toString());
            usiaTT = (flag_tt_calon_bayi == 1) ? 0 : Integer.valueOf(dataProposal.get("UMUR_TT").toString());

        }

        Integer pset_id = getSelect().selectPsetIdProposal(no_proposal);
        HashMap<String, Object> productCalc = getSelect().selectProductCalc(pset_id, lku_id, lscb_id);
        double ldec_factor = getSelect().selectProductFactor(no_proposal);

        if (pset_id == 970 || pset_id == 971 || pset_id == 972 || pset_id == 973 || pset_id == 974 ||
                pset_id == 975 || pset_id == 976 || pset_id == 977 || pset_id == 978 || pset_id == 979
                || pset_id == 980 || pset_id == 981 || pset_id == 1005 || pset_id == 1006 || pset_id == 1007 || pset_id == 1008) {
            String queryPremium = getSelect().selectProductFormula((Integer) productCalc.get("LPF_PREMI_O"));
            Log.d(TAG, "getIllustrationResult:  lpf  id " + productCalc.get("LPF_PREMI_O"));
            queryPremium = queryPremium.replaceAll("\\$P\\{NO_PROPOSAL\\}", "'" + no_proposal + "'");
            queryPremium = queryPremium.replaceAll("\\$P\\{LSBS_ID\\}", String.valueOf(bisnisId));
            queryPremium = queryPremium.replaceAll("\\$P\\{LSDBS_NUMBER\\}", String.valueOf(bisnisNo));
            queryPremium = queryPremium.replaceAll("\\$P\\{UP\\}", String.valueOf(up));
            queryPremium = queryPremium.replaceAll("\\$P\\{LDEC_FACTOR\\}", String.valueOf(ldec_factor));
            queryPremium = queryPremium.replaceAll("\\$P\\{LSCB_KALI\\}", String.valueOf(lscb_kali));
            queryPremium = queryPremium.replaceAll("\\$P\\{LSCB_ID\\}", String.valueOf(lscb_id));
            queryPremium = queryPremium.replaceAll("\\$P\\{LI_USIA_TT\\}", String.valueOf(usiaTT));
            queryPremium = queryPremium.replaceAll("\\$P\\{FLAG_TT_CALON_BAYI\\}", String.valueOf(flag_tt_calon_bayi));


            double premium = getSelect().selectRiderFormula(queryPremium);

            for (int i = 1; i <= (22 - usiaTT); i++) {
                listTahapan[i] = 0;
                //hitung Tahapan
                if ((usiaTT + i) == 6) {
                    if (bisnisNo >= 9 && bisnisNo <= 12) {
                        listTahapan[i] = tahapan[bisnisNo - 9][0] * up;
                    } else if (bisnisNo >= 13 && bisnisNo <= 16) {
                        listTahapan[i] = tahapan[bisnisNo - 13][0] * up;
                    } else if (bisnisNo >= 17 && bisnisNo <= 20) {
                        listTahapan[i] = tahapan[bisnisNo - 17][0] * up;
                    } else if (bisnisNo >= 21 && bisnisNo <= 24) {
                        listTahapan[i] = tahapan[bisnisNo - 21][0] * up;
                    } else if (bisnisNo >= 25 && bisnisNo <= 28) {
                        listTahapan[i] = tahapan[bisnisNo - 25][0] * up;
                    } else if (bisnisNo >= 1 && bisnisNo <= 4) {
                        listTahapan[i] = tahapan[bisnisNo - 1][0] * up;
                    } else {
                        listTahapan[i] = tahapan[bisnisNo - 5][0] * up;
                    }

                } else if ((usiaTT + i) == 12) {
                    if (bisnisNo >= 9 && bisnisNo <= 12) {
                        listTahapan[i] = tahapan[bisnisNo - 9][1] * up;
                    } else if (bisnisNo >= 13 && bisnisNo <= 16) {
                        listTahapan[i] = tahapan[bisnisNo - 13][1] * up;
                    } else if (bisnisNo >= 17 && bisnisNo <= 20) {
                        listTahapan[i] = tahapan[bisnisNo - 17][1] * up;
                    } else if (bisnisNo >= 21 && bisnisNo <= 24) {
                        listTahapan[i] = tahapan[bisnisNo - 21][1] * up;
                    } else if (bisnisNo >= 25 && bisnisNo <= 28) {
                        listTahapan[i] = tahapan[bisnisNo - 25][1] * up;
                    } else if (bisnisNo >= 1 && bisnisNo <= 4) {
                        listTahapan[i] = tahapan[bisnisNo - 1][1] * up;
                    } else {
                        listTahapan[i] = tahapan[bisnisNo - 5][1] * up;
                    }
                } else if ((usiaTT + i) == 15) {
                    if (bisnisNo >= 9 && bisnisNo <= 12) {
                        listTahapan[i] = tahapan[bisnisNo - 9][2] * up;
                    } else if (bisnisNo >= 13 && bisnisNo <= 16) {
                        listTahapan[i] = tahapan[bisnisNo - 13][2] * up;
                    } else if (bisnisNo >= 17 && bisnisNo <= 20) {
                        listTahapan[i] = tahapan[bisnisNo - 17][2] * up;
                    } else if (bisnisNo >= 21 && bisnisNo <= 24) {
                        listTahapan[i] = tahapan[bisnisNo - 21][2] * up;
                    } else if (bisnisNo >= 25 && bisnisNo <= 28) {
                        listTahapan[i] = tahapan[bisnisNo - 25][2] * up;
                    } else if (bisnisNo >= 1 && bisnisNo <= 4) {
                        listTahapan[i] = tahapan[bisnisNo - 1][2] * up;
                    } else {
                        listTahapan[i] = tahapan[bisnisNo - 5][2] * up;
                    }
                } else if ((usiaTT + i) == 18) {
                    if (bisnisNo >= 9 && bisnisNo <= 12) {
                        listTahapan[i] = tahapan[bisnisNo - 9][3] * up;
                    } else if (bisnisNo >= 13 && bisnisNo <= 16) {
                        listTahapan[i] = tahapan[bisnisNo - 13][3] * up;
                    } else if (bisnisNo >= 17 && bisnisNo <= 20) {
                        listTahapan[i] = tahapan[bisnisNo - 17][3] * up;
                    } else if (bisnisNo >= 21 && bisnisNo <= 24) {
                        listTahapan[i] = tahapan[bisnisNo - 21][3] * up;
                    } else if (bisnisNo >= 25 && bisnisNo <= 28) {
                        listTahapan[i] = tahapan[bisnisNo - 25][3] * up;
                    } else if (bisnisNo >= 1 && bisnisNo <= 4) {
                        listTahapan[i] = tahapan[bisnisNo - 1][3] * up;
                    } else {
                        listTahapan[i] = tahapan[bisnisNo - 5][3] * up;
                    }
                } else if ((usiaTT + i) == 22) {
                    if (bisnisNo >= 9 && bisnisNo <= 12) {
                        listTahapan[i] = tahapan[bisnisNo - 9][4] * up;
                    } else if (bisnisNo >= 13 && bisnisNo <= 16) {
                        listTahapan[i] = tahapan[bisnisNo - 13][4] * up;
                    } else if (bisnisNo >= 17 && bisnisNo <= 20) {
                        listTahapan[i] = tahapan[bisnisNo - 17][4] * up;
                    } else if (bisnisNo >= 21 && bisnisNo <= 24) {
                        listTahapan[i] = tahapan[bisnisNo - 21][4] * up;
                    } else if (bisnisNo >= 25 && bisnisNo <= 28) {
                        listTahapan[i] = tahapan[bisnisNo - 25][4] * up;
                    } else if (bisnisNo >= 1 && bisnisNo <= 4) {
                        listTahapan[i] = tahapan[bisnisNo - 1][4] * up;
                    } else {
                        listTahapan[i] = tahapan[bisnisNo - 5][4] * up;
                    }
                }
                tmpTahapan += listTahapan[i];

                map = new LinkedHashMap<>();
                map.put("yearNo", ProposalStringFormatter.convertToString(i));
                map.put("age", ProposalStringFormatter.convertToString(usiaTT + i));

                if (i <= lamaBayar) {
                    map.put("premi", ProposalStringFormatter.convertToRoundedNoDigit(new BigDecimal(premium)));
                } else {
                    map.put("premi", null);
                }

                String queryNT = getSelect().selectProductFormula((Integer) productCalc.get("LPF_POLICY_VALUE"));
                queryNT = queryNT.replaceAll("\\$P\\{LSBS_ID\\}", String.valueOf(bisnisId));
                queryNT = queryNT.replaceAll("\\$P\\{LSDBS_NUMBER\\}", String.valueOf(bisnisNo));
                queryNT = queryNT.replaceAll("\\$P\\{UP\\}", String.valueOf(up));
                queryNT = queryNT.replaceAll("\\$P\\{FLAG_TT_CALON_BAYI\\}", String.valueOf(flag_tt_calon_bayi));
                queryNT = queryNT.replaceAll("\\$P\\{TAHUN_KE\\}", String.valueOf(i));
                queryNT = queryNT.replaceAll("\\$P\\{USIA_KE\\}", String.valueOf(usiaTT));

                /*Select formula result itu sama dengan selectRiderFormula*/
                BigDecimal nt = new BigDecimal(getSelect().selectRiderFormula(queryNT));
                map.put("lump_sum", ProposalStringFormatter.convertToRoundedNoDigit(new BigDecimal(up)));
                map.put("nt", ProposalStringFormatter.convertToRoundedNoDigit(nt));
                String tahapanTemp = ProposalStringFormatter.convertToRoundedNoDigit(new BigDecimal(listTahapan[i]));
                map.put("lt", (tahapanTemp.equals("0")) ? "-" : tahapanTemp);
//            map.put("tb", ProposalStringFormatter.convertToRoundedNoDigit(nt.add(new BigDecimal(tmpTahapan))));


                mapList.add(map);

            }
        } else {
            String queryPremium = getSelect().selectProductFormula((Integer) productCalc.get("LPF_PREMIUM"));
            Log.d(TAG, "getIllustrationResult:  lpf  id " + productCalc.get("LPF_PREMIUM"));
            queryPremium = queryPremium.replaceAll("\\$P\\{NO_PROPOSAL\\}", "'" + no_proposal + "'");
            queryPremium = queryPremium.replaceAll("\\$P\\{LSBS_ID\\}", String.valueOf(bisnisId));
            queryPremium = queryPremium.replaceAll("\\$P\\{LSDBS_NUMBER\\}", String.valueOf(bisnisNo));
            queryPremium = queryPremium.replaceAll("\\$P\\{UP\\}", String.valueOf(up));
            queryPremium = queryPremium.replaceAll("\\$P\\{LDEC_FACTOR\\}", String.valueOf(ldec_factor));
            queryPremium = queryPremium.replaceAll("\\$P\\{LSCB_KALI\\}", String.valueOf(lscb_kali));
            queryPremium = queryPremium.replaceAll("\\$P\\{LSCB_ID\\}", String.valueOf(lscb_id));
            queryPremium = queryPremium.replaceAll("\\$P\\{LI_USIA_TT\\}", String.valueOf(usiaTT));
            queryPremium = queryPremium.replaceAll("\\$P\\{FLAG_TT_CALON_BAYI\\}", String.valueOf(flag_tt_calon_bayi));


            double premium = getSelect().selectRiderFormula(queryPremium);

            for (int i = 1; i <= (22 - usiaTT); i++) {
                listTahapan[i] = 0;
                //hitung Tahapan
                if ((usiaTT + i) == 6) {
                    if (bisnisNo >= 9 && bisnisNo <= 12) {
                        listTahapan[i] = tahapan[bisnisNo - 9][0] * up;
                    } else if (bisnisNo >= 13 && bisnisNo <= 16) {
                        listTahapan[i] = tahapan[bisnisNo - 13][0] * up;
                    } else if (bisnisNo >= 17 && bisnisNo <= 20) {
                        listTahapan[i] = tahapan[bisnisNo - 17][0] * up;
                    } else if (bisnisNo >= 21 && bisnisNo <= 24) {
                        listTahapan[i] = tahapan[bisnisNo - 21][0] * up;
                    } else if (bisnisNo >= 25 && bisnisNo <= 28) {
                        listTahapan[i] = tahapan[bisnisNo - 25][0] * up;
                    } else if (bisnisNo >= 1 && bisnisNo <= 4) {
                        listTahapan[i] = tahapan[bisnisNo - 1][0] * up;
                    } else {
                        listTahapan[i] = tahapan[bisnisNo - 5][0] * up;
                    }

                } else if ((usiaTT + i) == 12) {
                    if (bisnisNo >= 9 && bisnisNo <= 12) {
                        listTahapan[i] = tahapan[bisnisNo - 9][1] * up;
                    } else if (bisnisNo >= 13 && bisnisNo <= 16) {
                        listTahapan[i] = tahapan[bisnisNo - 13][1] * up;
                    } else if (bisnisNo >= 17 && bisnisNo <= 20) {
                        listTahapan[i] = tahapan[bisnisNo - 17][1] * up;
                    } else if (bisnisNo >= 21 && bisnisNo <= 24) {
                        listTahapan[i] = tahapan[bisnisNo - 21][1] * up;
                    } else if (bisnisNo >= 25 && bisnisNo <= 28) {
                        listTahapan[i] = tahapan[bisnisNo - 25][1] * up;
                    } else if (bisnisNo >= 1 && bisnisNo <= 4) {
                        listTahapan[i] = tahapan[bisnisNo - 1][1] * up;
                    } else {
                        listTahapan[i] = tahapan[bisnisNo - 5][1] * up;
                    }
                } else if ((usiaTT + i) == 15) {
                    if (bisnisNo >= 9 && bisnisNo <= 12) {
                        listTahapan[i] = tahapan[bisnisNo - 9][2] * up;
                    } else if (bisnisNo >= 13 && bisnisNo <= 16) {
                        listTahapan[i] = tahapan[bisnisNo - 13][2] * up;
                    } else if (bisnisNo >= 17 && bisnisNo <= 20) {
                        listTahapan[i] = tahapan[bisnisNo - 17][2] * up;
                    } else if (bisnisNo >= 21 && bisnisNo <= 24) {
                        listTahapan[i] = tahapan[bisnisNo - 21][2] * up;
                    } else if (bisnisNo >= 25 && bisnisNo <= 28) {
                        listTahapan[i] = tahapan[bisnisNo - 25][2] * up;
                    } else if (bisnisNo >= 1 && bisnisNo <= 4) {
                        listTahapan[i] = tahapan[bisnisNo - 1][2] * up;
                    } else {
                        listTahapan[i] = tahapan[bisnisNo - 5][2] * up;
                    }
                } else if ((usiaTT + i) == 18) {
                    if (bisnisNo >= 9 && bisnisNo <= 12) {
                        listTahapan[i] = tahapan[bisnisNo - 9][3] * up;
                    } else if (bisnisNo >= 13 && bisnisNo <= 16) {
                        listTahapan[i] = tahapan[bisnisNo - 13][3] * up;
                    } else if (bisnisNo >= 17 && bisnisNo <= 20) {
                        listTahapan[i] = tahapan[bisnisNo - 17][3] * up;
                    } else if (bisnisNo >= 21 && bisnisNo <= 24) {
                        listTahapan[i] = tahapan[bisnisNo - 21][3] * up;
                    } else if (bisnisNo >= 25 && bisnisNo <= 28) {
                        listTahapan[i] = tahapan[bisnisNo - 25][3] * up;
                    } else if (bisnisNo >= 1 && bisnisNo <= 4) {
                        listTahapan[i] = tahapan[bisnisNo - 1][3] * up;
                    } else {
                        listTahapan[i] = tahapan[bisnisNo - 5][3] * up;
                    }
                } else if ((usiaTT + i) == 22) {
                    if (bisnisNo >= 9 && bisnisNo <= 12) {
                        listTahapan[i] = tahapan[bisnisNo - 9][4] * up;
                    } else if (bisnisNo >= 13 && bisnisNo <= 16) {
                        listTahapan[i] = tahapan[bisnisNo - 13][4] * up;
                    } else if (bisnisNo >= 17 && bisnisNo <= 20) {
                        listTahapan[i] = tahapan[bisnisNo - 17][4] * up;
                    } else if (bisnisNo >= 21 && bisnisNo <= 24) {
                        listTahapan[i] = tahapan[bisnisNo - 21][4] * up;
                    } else if (bisnisNo >= 25 && bisnisNo <= 28) {
                        listTahapan[i] = tahapan[bisnisNo - 25][4] * up;
                    } else if (bisnisNo >= 1 && bisnisNo <= 4) {
                        listTahapan[i] = tahapan[bisnisNo - 1][4] * up;
                    } else {
                        listTahapan[i] = tahapan[bisnisNo - 5][4] * up;
                    }
                }
                tmpTahapan += listTahapan[i];

                map = new LinkedHashMap<>();
                map.put("yearNo", ProposalStringFormatter.convertToString(i));
                map.put("age", ProposalStringFormatter.convertToString(usiaTT + i));

                if (i <= lamaBayar) {
                    map.put("premi", ProposalStringFormatter.convertToRoundedNoDigit(new BigDecimal(premium)));
                } else {
                    map.put("premi", null);
                }

                String queryNT = getSelect().selectProductFormula((Integer) productCalc.get("LPF_POLICY_VALUE"));
                queryNT = queryNT.replaceAll("\\$P\\{LSBS_ID\\}", String.valueOf(bisnisId));
                queryNT = queryNT.replaceAll("\\$P\\{LSDBS_NUMBER\\}", String.valueOf(bisnisNo));
                queryNT = queryNT.replaceAll("\\$P\\{UP\\}", String.valueOf(up));
                queryNT = queryNT.replaceAll("\\$P\\{FLAG_TT_CALON_BAYI\\}", String.valueOf(flag_tt_calon_bayi));
                queryNT = queryNT.replaceAll("\\$P\\{TAHUN_KE\\}", String.valueOf(i));
                queryNT = queryNT.replaceAll("\\$P\\{USIA_KE\\}", String.valueOf(usiaTT));

                /*Select formula result itu sama dengan selectRiderFormula*/
                BigDecimal nt = new BigDecimal(getSelect().selectRiderFormula(queryNT));
                map.put("lump_sum", ProposalStringFormatter.convertToRoundedNoDigit(new BigDecimal(up)));
                map.put("nt", ProposalStringFormatter.convertToRoundedNoDigit(nt));
                String tahapanTemp = ProposalStringFormatter.convertToRoundedNoDigit(new BigDecimal(listTahapan[i]));
                map.put("lt", (tahapanTemp.equals("0")) ? "-" : tahapanTemp);
//            map.put("tb", ProposalStringFormatter.convertToRoundedNoDigit(nt.add(new BigDecimal(tmpTahapan))));


                mapList.add(map);

            }
        }

        map = new LinkedHashMap<>();
        map.put("yearNo", "Akhir Tahun Polis");
        map.put("age", "Usia");
        map.put("premi", "Pembayaran Premi");
        map.put("lump_sum", "Manfaat Meninggal");
        map.put("nt", "Nilai Tunai");
        map.put("lt", "Tahapan");
        mapList.add(0, map);

        IllustrationResultVO resultVO = new IllustrationResultVO();
        resultVO.setIllustrationList(mapList);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("Illustration1", resultVO);

        return result;
    }

}
