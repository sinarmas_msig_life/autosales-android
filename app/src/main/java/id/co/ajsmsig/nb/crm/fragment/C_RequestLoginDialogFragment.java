package id.co.ajsmsig.nb.crm.fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.activity.C_FormAktifitasActivity;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.method.async.CrmRestClientUsage;
import id.co.ajsmsig.nb.crm.model.apiresponse.RequestLoginResponse;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.util.AppConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A custom DialogFragment that is positioned above given "source" component.
 *
 * @author Jonik, https://stackoverflow.com/a/20419231/56285
 */

public class C_RequestLoginDialogFragment extends DialogFragment implements AdapterView.OnItemClickListener, View.OnClickListener {
    @BindView(R.id.tv_error_kode_agen)
    TextView tv_error_kode_agen;

    @BindView(R.id.et_kode_agen)
    TextView et_kode_agen;

    @BindView(R.id.btn_request)
    Button btn_request;

    private ProgressDialog progressDialog;

    public C_RequestLoginDialogFragment() {
    }

    public static C_RequestLoginDialogFragment newInstance(Bundle args) {
        C_RequestLoginDialogFragment f = new C_RequestLoginDialogFragment();
        f.setArguments(args);


        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setStyle(STYLE_NO_FRAME, R.style.Dialog);
    }


    @Override
    public void onStart() {
        super.onStart();
        initiateDialogSize();
    }

    private void initiateDialogSize() {
        // safety check
        if (getDialog() == null)
            return;

        // retrieve display dimensions
        Rect displayRectangle = new Rect();
//        C_MainActivity activity = (C_MainActivity) getContext();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        int width = (int) (displayRectangle.width() * 0.6f);
//        int height = (int) (displayRectangle.height() * 0.3f);

        if (getDialog().getWindow() != null) {
            Window windowDialog = getDialog().getWindow();
            // Less dimmed background; see https://stackoverflow.com/q/13822842/56285
            WindowManager.LayoutParams params = windowDialog.getAttributes();
            params.dimAmount = 0.9f; // dim only a little bit
//
//            windowDialog.setAttributes(params);
            windowDialog.setLayout(width, params.height);

            // Transparent background; see https://stackoverflow.com/q/15007272/56285
            // (Needed to make dialog's alpha shadow look good)
//            windowDialog.setBackgroundDrawableResource(android.R.color.transparent);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Put your dialog layout in R.layout.view_confirm_box
        View view = inflater.inflate(R.layout.c_fragment_request_login_dialog, container, false);
        ButterKnife.bind(this, view);
        initiateView();

        btn_request.setOnClickListener(this);
        view.findViewById(R.id.btn_batal).setOnClickListener(this);

        return view;
    }

    private void initiateView() {
        et_kode_agen.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tv_error_kode_agen.setVisibility(View.GONE);

            }
        });
    }


    public int dpToPx(float valueInDp) {
        DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                goTo(C_FormAktifitasActivity.class);
                break;
            case 1:
                break;
            default:
                throw new IllegalArgumentException("id belum diset");
        }
    }

    private void goTo(Class c) {
        Bundle bundleActivity = getActivity().getIntent().getExtras();

        Intent intent = new Intent(getActivity(), c);
        Bundle args = getArguments();
        args.putLong(getString(R.string.SL_TAB_ID), bundleActivity.getLong(getString(R.string.SL_TAB_ID), -1));
        intent.putExtras(args);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_request:
                requestLogin();
                break;
            case R.id.btn_batal:
                dismiss();
                break;
            default:
                throw new IllegalArgumentException(C_RequestLoginDialogFragment.class.getSimpleName() + " : Id onclick belum diset");
        }
    }

    private void requestLogin() {
        boolean isOk = true;

        if (et_kode_agen.getText().toString().trim().length() == 0) {
            isOk = false;
            tv_error_kode_agen.setVisibility(View.VISIBLE);
        }

        if (isOk) {
            showProgressDialog();

            String agentCode = et_kode_agen.getText().toString().trim();
            if (!TextUtils.isEmpty(agentCode)) {

                if (StaticMethods.isNetworkAvailable(getActivity())) {
                    requestLogin(agentCode, "individu", "ajax");
                } else {
                    showAlertDialog(getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi));
                }

            }

        }
    }

    private void showAlertDialog(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        AlertDialog alertDialog;
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(getString(R.string.ok), (dialogInterface, arg1) -> dialogInterface.dismiss());
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void requestLogin(String agentCode, String jenis, String req) {
        String url = AppConfig.getBaseUrlCRM().concat("v2/main/req_password");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);


        Call<RequestLoginResponse> call = api.requestLogin(url, agentCode, jenis, req);
        call.enqueue(new Callback<RequestLoginResponse>() {
            @Override
            public void onResponse(Call<RequestLoginResponse> call, Response<RequestLoginResponse> response) {
                hideProgressDialog();

                if (response.body() != null) {
                    boolean state = response.body().isState();
                    String message = response.body().getMessage();
                    Spanned spanned = StaticMethods.fromHtml(message);

                    if (state) {
                        dismiss();
                        showAlertDialog("Request login berhasil", spanned.toString());
                    } else {
                        dismiss();
                        showAlertDialog("Request login gagal", spanned.toString());
                    }


                }
            }

            @Override
            public void onFailure(Call<RequestLoginResponse> call, Throwable t) {
                hideProgressDialog();
                showAlertDialog("Terjadi kesalahan", t.getMessage());
            }
        });
    }

    private void showProgressDialog() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Meminta request login");
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
