
package id.co.ajsmsig.nb.crm.model.apiresponse.request;

public class Content {
    public Content(String CreateNewCaseOption, String flowFromAPI, String eSPAJNumber) {
        this.CreateNewCaseOption = CreateNewCaseOption;
        this.flowFromAPI = flowFromAPI;
        this.eSPAJNumber = eSPAJNumber;
    }

    private String CreateNewCaseOption;
    private String flowFromAPI;
    private String eSPAJNumber;


}
