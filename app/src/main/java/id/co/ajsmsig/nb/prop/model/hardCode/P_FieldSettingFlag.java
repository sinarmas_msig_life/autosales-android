package id.co.ajsmsig.nb.prop.model.hardCode;
/*
 Created by faiz_f on 11/08/2017.
 */

public class P_FieldSettingFlag {
    public static final int PREMI = 1;
    public static final int KOMBINASI_PREMI = 2;
    public static final int DISPLAY_HELPER_UTK_PREMI = 3;
    public static final int PREMI_POKOK = 4;
    public static final int PREMI_TOPUP = 5;
    public static final int UP = 6;
    public static final int CARA_BAYAR = 7;
    public static final int MASA_KONTRAK = 8;
    public static final int LAMA_PEMBAYARAN = 9;
    public static final int TOMBOL_RIDER = 10;
    public static final int TOMBOL_TOPUP_PENARIKAN = 11;
    public static final int TOMBOL_HITUNG_UP = 12;
    public static final int THN_CUTI_PREMI_MASA_PEMBAYARAN_PREMI = 13;
    public static final int TOMBOL_PESERTA_HCP_PROVIDER = 14;
    public static final int PILIHAN_INVESTASI =15;



}

