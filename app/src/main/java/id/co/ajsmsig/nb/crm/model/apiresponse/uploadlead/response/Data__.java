
package id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data__ {

    @SerializedName("SLA_ID")
    @Expose
    private String sLAID;
    @SerializedName("SLA_DETAIL")
    @Expose
    private String sLADETAIL;
    @SerializedName("SLA_NAME")
    @Expose
    private Object sLANAME;

    public String getSLAID() {
        return sLAID;
    }

    public void setSLAID(String sLAID) {
        this.sLAID = sLAID;
    }

    public String getSLADETAIL() {
        return sLADETAIL;
    }

    public void setSLADETAIL(String sLADETAIL) {
        this.sLADETAIL = sLADETAIL;
    }

    public Object getSLANAME() {
        return sLANAME;
    }

    public void setSLANAME(Object sLANAME) {
        this.sLANAME = sLANAME;
    }

}
