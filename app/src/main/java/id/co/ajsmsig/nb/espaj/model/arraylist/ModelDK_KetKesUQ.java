package id.co.ajsmsig.nb.espaj.model.arraylist;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 28/09/2017.
 */

public class ModelDK_KetKesUQ implements Parcelable {
    private int counter = 0;
    private String ket = "";
    private Boolean validation = true;


    protected ModelDK_KetKesUQ(Parcel in) {
        counter = in.readInt();
        ket = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(counter);
        dest.writeString(ket);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelDK_KetKesUQ> CREATOR = new Creator<ModelDK_KetKesUQ>() {
        @Override
        public ModelDK_KetKesUQ createFromParcel(Parcel in) {
            return new ModelDK_KetKesUQ(in);
        }

        @Override
        public ModelDK_KetKesUQ[] newArray(int size) {
            return new ModelDK_KetKesUQ[size];
        }
    };

    public ModelDK_KetKesUQ(int counter, String ket, Boolean validation) {
        this.counter = counter;
        this.ket = ket;
        this.validation = validation;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public static Creator<ModelDK_KetKesUQ> getCREATOR() {
        return CREATOR;
    }
}
