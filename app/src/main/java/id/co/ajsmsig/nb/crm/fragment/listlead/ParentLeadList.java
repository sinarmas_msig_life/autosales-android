package id.co.ajsmsig.nb.crm.fragment.listlead;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by Fajar.Azhar on 21/03/2018.
 */

public class ParentLeadList extends ExpandableGroup<ChildLeadList> {
    public ParentLeadList(String groupTitle, List<ChildLeadList> items) {
        super(groupTitle, items);
    }
}
