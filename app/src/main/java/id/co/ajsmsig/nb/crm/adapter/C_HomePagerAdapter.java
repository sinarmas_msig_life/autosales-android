package id.co.ajsmsig.nb.crm.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
  Created by faiz_f on 15/03/2017.
 */

public class C_HomePagerAdapter extends FragmentPagerAdapter {

    private CharSequence[] titles;
    private Fragment[] fragments;


    public C_HomePagerAdapter(FragmentManager fm, Fragment[] fragments, CharSequence[]titles) {
        super(fm);
        this.fragments = fragments;
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
//        return titles[position];
        return "";
    }

}
