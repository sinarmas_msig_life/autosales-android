package id.co.ajsmsig.nb.util;

import id.co.ajsmsig.nb.BuildConfig;

public class AppConfig {

    public static String getBaseUrlCRM() {
        if (BuildConfig.FLAVOR.equalsIgnoreCase("production")) {
            return "https://crm.sinarmasmsiglife.co.id/";
        } else {
            return "https://crmtest.sinarmasmsiglife.co.id/";
        }
    }
    public static String getBaseUrlEproposal() {
        if (BuildConfig.FLAVOR.equalsIgnoreCase("production")) {
            return "http://eproposal.sinarmasmsiglife.co.id/";
        } else {
//            return "http://test.sinarmasmsiglife.co.id:8800/E-Proposal/";
            return "http://128.21.32.16/E-ProposalSLPSPP/";
        }
    }

    public static String getBaseUrlEWS() {
        if (BuildConfig.FLAVOR.equalsIgnoreCase("production")) {
            return "https://ws-api.sinarmasmsiglife.co.id/";
        } else {
            return "https://ws-api.sinarmasmsiglife.co.id/";
        }
    }

    public static String getBaseUrlProposal() {
        if (BuildConfig.FLAVOR.equalsIgnoreCase("production")) {
            return "https://proposal-api.sinarmasmsiglife.co.id/";
        } else {
            return "http://test.sinarmasmsiglife.co.id:8800/";
        }
    }

    public static String getBaseUrlSPAJBank() {
        if (BuildConfig.FLAVOR.equalsIgnoreCase("production")) {
            return "https://spaj-api.sinarmasmsiglife.co.id/";
        } else {
            return "https://spaj-api.sinarmasmsiglife.co.id/";
        }
    }

    public static String getBaseUrlSPAJ() {
        if (BuildConfig.FLAVOR.equalsIgnoreCase("production")) {
            return "https://spaj-api.sinarmasmsiglife.co.id/";
        } else {
            return "http://test.sinarmasmsiglife.co.id:8800/";
        }
    }

    public static String getBaseUrlWs() {
        if (BuildConfig.FLAVOR.equalsIgnoreCase("production")) {
            return "https://ws-api.sinarmasmsiglife.co.id/";
        } else {
            return "http://test.sinarmasmsiglife.co.id:8800/";
        }
    }
    public static String getBaseUrlCreditCardPayment() {
        if (BuildConfig.FLAVOR.equalsIgnoreCase("production")) {
            return "http://paymentcc.sinarmasmsiglife.co.id:8081/";
        } else {
            return "http://test.sinarmasmsiglife.co.id:8800/API-SPAJ-CC/";
//            return "http://paymentcc.sinarmasmsiglife.co.id:8084/";
        }
    }
    public static String getBaseUrlSummaryFragment() {
        if (BuildConfig.FLAVOR.equalsIgnoreCase("production")) {
            return "http://paymentcc.sinarmasmsiglife.co.id:8999";
        } else {
            return "http://paymentcc.sinarmasmsiglife.co.id:8999";
        }
    }
}
