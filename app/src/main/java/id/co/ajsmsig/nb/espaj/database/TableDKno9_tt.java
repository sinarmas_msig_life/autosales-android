package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_NO9ttUQ;

public class TableDKno9_tt {
    private Context context;

    public TableDKno9_tt(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(ModelDK_NO9ttUQ me, int counter, String SPAJ_ID_TAB) {
        ContentValues cv = new ContentValues();
        cv.put("SPAJ_ID_TAB", SPAJ_ID_TAB);
        cv.put("COUNTER", counter);
        cv.put("ID_CT", me.getSpin_ct());
        cv.put("STR_CT", me.getStr_ct());
        cv.put("UMUR", me.getUmur());
        cv.put("KEADAAN", me.getKeadaan());
        cv.put("PENYEBAB", me.getPenyebab());
        cv.put("ID_KEADAAN", me.getId_keadaan());

        return cv;
    }

    public ArrayList<ModelDK_NO9ttUQ> getObjectArray(Cursor cursor) {
        ArrayList<ModelDK_NO9ttUQ> modelDK_no9ttUQArrayList = new ArrayList<ModelDK_NO9ttUQ>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ModelDK_NO9ttUQ me = new ModelDK_NO9ttUQ();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("COUNTER"))) {
                    me.setCounter(cursor.getInt(cursor.getColumnIndexOrThrow("COUNTER")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("ID_CT"))) {
                    me.setSpin_ct(cursor.getInt(cursor.getColumnIndexOrThrow("ID_CT")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("STR_CT"))) {
                    me.setStr_ct(cursor.getString(cursor.getColumnIndexOrThrow("STR_CT")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("UMUR"))) {
                    me.setUmur(cursor.getString(cursor.getColumnIndexOrThrow("UMUR")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("KEADAAN"))) {
                    me.setKeadaan(cursor.getString(cursor.getColumnIndexOrThrow("KEADAAN")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("PENYEBAB"))) {
                    me.setPenyebab(cursor.getString(cursor.getColumnIndexOrThrow("PENYEBAB")));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow("ID_KEADAAN"))) {
                    me.setId_keadaan(cursor.getInt(cursor.getColumnIndexOrThrow("ID_KEADAAN")));
                }
                modelDK_no9ttUQArrayList.add(me);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return modelDK_no9ttUQArrayList;
    }
}
