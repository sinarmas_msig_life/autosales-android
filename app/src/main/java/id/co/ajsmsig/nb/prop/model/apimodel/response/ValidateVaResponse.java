
package id.co.ajsmsig.nb.prop.model.apimodel.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValidateVaResponse {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("no_va")
    @Expose
    private String noVa;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getNoVa() {
        return noVa;
    }

    public void setNoVa(String noVa) {
        this.noVa = noVa;
    }

}
