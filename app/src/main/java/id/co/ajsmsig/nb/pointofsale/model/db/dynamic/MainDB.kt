package id.co.ajsmsig.nb.pointofsale.model.db.dynamic

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.*
import android.content.Context
import android.support.annotation.VisibleForTesting
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.LiveData
import android.arch.persistence.room.migration.Migration
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.dao.*
import java.util.*


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Database(entities = [
    UserRecord::class,
    Product::class,
    ProductBenefit::class,
    ProductRider::class,
    Analytics::class
],
        version = 4)

@TypeConverters(
        DateTypeConverter::class
)
abstract class MainDB: RoomDatabase() {

    private val mIsDatabaseCreated = MutableLiveData<Boolean>()


    abstract fun userRecordDao(): UserRecordDao
    abstract fun productDao(): ProductDao
    abstract fun productBenefitDao(): ProductBenefitDao
    abstract fun productRiderDao(): ProductRiderDao
    abstract fun analyticsDao(): AnalyticsDao


    /**
     * Check whether the database already exists and expose it via [.getDatabaseCreated]
     */
    fun updateDatabaseCreated(context: Context) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            setDatabaseCreated()
        }
    }


    private fun setDatabaseCreated() {
        mIsDatabaseCreated.postValue(true)
    }

    fun getDatabaseCreated(): LiveData<Boolean> {
        return mIsDatabaseCreated
    }


    companion object {

        private var instance: MainDB? = null

        fun getInstance(context: Context): MainDB {
            synchronized(MainDB::class.java) {

                if (instance == null) {
                    instance = buildDatabase(context.applicationContext)
                    instance?.updateDatabaseCreated(context.applicationContext)
                }
            }

            return instance!!
        }

        @VisibleForTesting
        val DATABASE_NAME = "POSData.db"

        private fun buildDatabase(context: Context): MainDB {
            return  Room.databaseBuilder(context, MainDB::class.java, DATABASE_NAME)
                    .addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4)
                    .build()
        }


        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // Since we didn't alter the table, there's nothing else to do here.
                database.execSQL("ALTER TABLE Product "
                        + " ADD COLUMN RiderSheet TEXT")
            }
        }
        val MIGRATION_2_3: Migration = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE `Analytics` (`id` INTEGER NOT NULL, " +
                                        "`group` INTEGER, " +
                                        "`timestamp` INTEGER, " +
                                        "`isSent` INTEGER NOT NULL, " +
                                        "`event` TEXT, PRIMARY KEY(`id`))")
            }
        }

        val MIGRATION_3_4: Migration = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE Analytics " +
                        " ADD COLUMN agentName TEXT")
                database.execSQL("ALTER TABLE Analytics " +
                        " ADD COLUMN agentCode TEXT")
            }
        }


    }

}


class DateTypeConverter {

    @TypeConverter
    fun toDate(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun toLong(value: Date?): Long? {
        return value?.time
    }
}