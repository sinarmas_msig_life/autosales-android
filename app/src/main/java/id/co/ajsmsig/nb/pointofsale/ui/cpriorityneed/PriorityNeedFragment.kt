package id.co.ajsmsig.nb.pointofsale.ui.cpriorityneed


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosFragmentPriorityNeedBinding
import id.co.ajsmsig.nb.pointofsale.ui.fragment.SingleOptionFragment
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption


/**
 * A simple [Fragment] subclass.
 */
class PriorityNeedFragment : SingleOptionFragment() {

    private lateinit var dataBinding: PosFragmentPriorityNeedBinding

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(PriorityNeedVM::class.java)
        subscribeUi(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_priority_need, container, false)

        return dataBinding.root
    }
    private fun subscribeUi(viewModel: PriorityNeedVM) {

        dataBinding.vm = viewModel

        viewModel.page = page

        viewModel.observablePageOptions.observe(this, Observer {
            it?.let { it.firstOrNull { it.id == sharedViewModel.selectedPriorityNeed?.id }?.selected?.set(true) }
            mainVM.subtitle.set(sharedViewModel.selectedLifeStage?.title)
        })

        mainVM.subtitleTop()
    }

    override fun onDestroy() {
        super.onDestroy()

        sharedViewModel.selectedPriorityNeed = null
    }

    override fun optionSelected(): PageOption? {
        val viewModel = ViewModelProviders.of(this).get(PriorityNeedVM::class.java)
        val selectedOption = viewModel.observablePageOptions.value?.firstOrNull { it.selected.get() }

        sharedViewModel.selectedPriorityNeed = selectedOption

        return selectedOption
    }
}// Required empty public constructor
