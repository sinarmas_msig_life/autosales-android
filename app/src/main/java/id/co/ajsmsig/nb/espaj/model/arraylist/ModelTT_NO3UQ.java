package id.co.ajsmsig.nb.espaj.model.arraylist;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 28/09/2017.
 */

public class ModelTT_NO3UQ implements Parcelable {
    private int counter = 0;
    private String nama_perushn = "";
    private int spin_produk = 0;
    private String str_produk = "";
    private String up = "";
    private String tgl_polis = "";
    private Boolean validation = true ;

    protected ModelTT_NO3UQ(Parcel in) {
        counter = in.readInt();
        nama_perushn = in.readString();
        spin_produk = in.readInt();
        str_produk = in.readString();
        up = in.readString();
        tgl_polis = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(counter);
        dest.writeString(nama_perushn);
        dest.writeInt(spin_produk);
        dest.writeString(str_produk);
        dest.writeString(up);
        dest.writeString(tgl_polis);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelTT_NO3UQ> CREATOR = new Creator<ModelTT_NO3UQ>() {
        @Override
        public ModelTT_NO3UQ createFromParcel(Parcel in) {
            return new ModelTT_NO3UQ(in);
        }

        @Override
        public ModelTT_NO3UQ[] newArray(int size) {
            return new ModelTT_NO3UQ[size];
        }
    };

    public ModelTT_NO3UQ(int counter, String nama_perushn, int spin_produk, String str_produk, String up, String tgl_polis, Boolean validation) {
        this.counter = counter;
        this.nama_perushn = nama_perushn;
        this.spin_produk = spin_produk;
        this.str_produk = str_produk;
        this.up = up;
        this.tgl_polis = tgl_polis;
        this.validation = validation;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getNama_perushn() {
        return nama_perushn;
    }

    public void setNama_perushn(String nama_perushn) {
        this.nama_perushn = nama_perushn;
    }

    public int getSpin_produk() {
        return spin_produk;
    }

    public void setSpin_produk(int spin_produk) {
        this.spin_produk = spin_produk;
    }

    public String getStr_produk() {
        return str_produk;
    }

    public void setStr_produk(String str_produk) {
        this.str_produk = str_produk;
    }

    public String getUp() {
        return up;
    }

    public void setUp(String up) {
        this.up = up;
    }

    public String getTgl_polis() {
        return tgl_polis;
    }

    public void setTgl_polis(String tgl_polis) {
        this.tgl_polis = tgl_polis;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public static Creator<ModelTT_NO3UQ> getCREATOR() {
        return CREATOR;
    }
}
