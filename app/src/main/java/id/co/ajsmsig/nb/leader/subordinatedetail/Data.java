
package id.co.ajsmsig.nb.leader.subordinatedetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("DONE")
    @Expose
    private String dONE;
    @SerializedName("ON_PROSES")
    @Expose
    private String oNPROSES;
    @SerializedName("TOTAL_ENTRY")
    @Expose
    private String tOTALENTRY;
    @SerializedName("FURTHER")
    @Expose
    private String fURTHER;
    @SerializedName("CANCELED")
    @Expose
    private String cANCELED;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param tOTALENTRY
     * @param oNPROSES
     * @param cANCELED
     * @param fURTHER
     * @param dONE
     */
    public Data(String dONE, String oNPROSES, String tOTALENTRY, String fURTHER, String cANCELED) {
        super();
        this.dONE = dONE;
        this.oNPROSES = oNPROSES;
        this.tOTALENTRY = tOTALENTRY;
        this.fURTHER = fURTHER;
        this.cANCELED = cANCELED;
    }

    public String getDONE() {
        return dONE;
    }

    public void setDONE(String dONE) {
        this.dONE = dONE;
    }

    public String getONPROSES() {
        return oNPROSES;
    }

    public void setONPROSES(String oNPROSES) {
        this.oNPROSES = oNPROSES;
    }

    public String getTOTALENTRY() {
        return tOTALENTRY;
    }

    public void setTOTALENTRY(String tOTALENTRY) {
        this.tOTALENTRY = tOTALENTRY;
    }

    public String getFURTHER() {
        return fURTHER;
    }

    public void setFURTHER(String fURTHER) {
        this.fURTHER = fURTHER;
    }

    public String getCANCELED() {
        return cANCELED;
    }

    public void setCANCELED(String cANCELED) {
        this.cANCELED = cANCELED;
    }

}
