package id.co.ajsmsig.nb.crm.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/*
 Created by faiz_f on 18/07/2017.
 */

public class C_ProfileAgentPagerAdapter extends FragmentPagerAdapter {
    private Fragment[] fragments;
    private CharSequence[] titles;


    public C_ProfileAgentPagerAdapter(FragmentManager fm, Fragment[] fragments, CharSequence[] titles) {
        super(fm);
        this.titles = titles;
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
//        return "";
    }
}
