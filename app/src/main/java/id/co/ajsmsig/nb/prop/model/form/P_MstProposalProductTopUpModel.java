package id.co.ajsmsig.nb.prop.model.form;


import android.os.Parcel;
import android.os.Parcelable;

public class P_MstProposalProductTopUpModel implements Parcelable{
	private String no_proposal_tab;
	private String no_proposal;
	private Integer thn_ke;
	private String topup;
	private String tarik;
	private Boolean isSelected;


	public P_MstProposalProductTopUpModel() {

	}


	protected P_MstProposalProductTopUpModel(Parcel in) {
		no_proposal_tab = in.readString();
		no_proposal = in.readString();
		thn_ke = (Integer) in.readValue(Integer.class.getClassLoader());
		topup = in.readString();
		tarik = in.readString();
		isSelected = in.readByte() != 0;
	}

	public static final Creator<P_MstProposalProductTopUpModel> CREATOR = new Creator<P_MstProposalProductTopUpModel>() {
		@Override
		public P_MstProposalProductTopUpModel createFromParcel(Parcel in) {
			return new P_MstProposalProductTopUpModel(in);
		}

		@Override
		public P_MstProposalProductTopUpModel[] newArray(int size) {
			return new P_MstProposalProductTopUpModel[size];
		}
	};

	public String getNo_proposal_tab() {
		return no_proposal_tab;
	}

	public void setNo_proposal_tab(String no_proposal_tab) {
		this.no_proposal_tab = no_proposal_tab;
	}

	public String getNo_proposal() {
		return no_proposal;
	}

	public void setNo_proposal(String no_proposal) {
		this.no_proposal = no_proposal;
	}

	public Integer getThn_ke() {
		return thn_ke;
	}

	public void setThn_ke(Integer thn_ke) {
		this.thn_ke = thn_ke;
	}

	public String getTopup() {
		return topup;
	}

	public void setTopup(String topup) {
		this.topup = topup;
	}

	public String getTarik() {
		return tarik;
	}

	public void setTarik(String tarik) {
		this.tarik = tarik;
	}

	public Boolean getSelected() {
		return isSelected;
	}

	public void setSelected(Boolean selected) {
		isSelected = selected;
	}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(no_proposal_tab);
		dest.writeString(no_proposal);
		dest.writeValue(thn_ke);
		dest.writeString(topup);
		dest.writeString(tarik);
		dest.writeByte((byte) (isSelected ? 1 : 0));
	}
}
