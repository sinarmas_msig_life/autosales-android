package id.co.ajsmsig.nb.espaj.model.arraylist;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 28/09/2017.
 */

public class ModelDK_NO9ppUQ implements Parcelable {
    private int counter = 0;
    private int  spin_cp = 0;
    private String str_cp = "";
    private String umur = "";
    private String keadaan = "";
    private String penyebab ="";
    private int id_keadaan = 0;

    public ModelDK_NO9ppUQ() {
    }

    protected ModelDK_NO9ppUQ(Parcel in) {
        counter = in.readInt();
        spin_cp = in.readInt();
        str_cp = in.readString();
        umur = in.readString();
        keadaan = in.readString();
        penyebab = in.readString();
        id_keadaan = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(counter);
        dest.writeInt(spin_cp);
        dest.writeString(str_cp);
        dest.writeString(umur);
        dest.writeString(keadaan);
        dest.writeString(penyebab);
        dest.writeInt(id_keadaan);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelDK_NO9ppUQ> CREATOR = new Creator<ModelDK_NO9ppUQ>() {
        @Override
        public ModelDK_NO9ppUQ createFromParcel(Parcel in) {
            return new ModelDK_NO9ppUQ(in);
        }

        @Override
        public ModelDK_NO9ppUQ[] newArray(int size) {
            return new ModelDK_NO9ppUQ[size];
        }
    };

    public ModelDK_NO9ppUQ(int counter, int spin_cp, String str_cp, String umur, String keadaan, String penyebab, int id_keadaan) {
        this.counter = counter;
        this.spin_cp = spin_cp;
        this.str_cp = str_cp;
        this.umur = umur;
        this.keadaan = keadaan;
        this.penyebab = penyebab;
        this.id_keadaan = id_keadaan;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public int getSpin_cp() {
        return spin_cp;
    }

    public void setSpin_cp(int spin_cp) {
        this.spin_cp = spin_cp;
    }

    public String getStr_cp() {
        return str_cp;
    }

    public void setStr_cp(String str_cp) {
        this.str_cp = str_cp;
    }

    public String getUmur() {
        return umur;
    }

    public void setUmur(String umur) {
        this.umur = umur;
    }

    public String getKeadaan() {
        return keadaan;
    }

    public void setKeadaan(String keadaan) {
        this.keadaan = keadaan;
    }

    public String getPenyebab() {
        return penyebab;
    }

    public void setPenyebab(String penyebab) {
        this.penyebab = penyebab;
    }

    public int getId_keadaan() {
        return id_keadaan;
    }

    public void setId_keadaan(int id_keadaan) {
        this.id_keadaan = id_keadaan;
    }

    public static Creator<ModelDK_NO9ppUQ> getCREATOR() {
        return CREATOR;
    }
}
