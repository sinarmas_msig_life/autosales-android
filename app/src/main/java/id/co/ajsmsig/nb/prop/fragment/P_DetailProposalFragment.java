package id.co.ajsmsig.nb.prop.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.AutoCompleteTextViewClickListener;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.method.MoneyTextWatcher;
import id.co.ajsmsig.nb.prop.method.P_StaticMethods;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductRiderModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductTopUpModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductUlinkModel;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;

import static id.co.ajsmsig.nb.prop.method.P_StaticMethods.getDefaultTPValue;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FieldSettingFlag.CARA_BAYAR;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FieldSettingFlag.KOMBINASI_PREMI;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FieldSettingFlag.LAMA_PEMBAYARAN;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FieldSettingFlag.MASA_KONTRAK;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FieldSettingFlag.PREMI;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FieldSettingFlag.PREMI_POKOK;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FieldSettingFlag.PREMI_TOPUP;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FieldSettingFlag.THN_CUTI_PREMI_MASA_PEMBAYARAN_PREMI;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FieldSettingFlag.TOMBOL_TOPUP_PENARIKAN;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FieldSettingFlag.UP;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FragmentPagePosition.DATA_PROPOSAL_PAGE;

/**
 * Created by faiz_f on 05/04/2017.
 */

public class P_DetailProposalFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, AdapterView.OnItemClickListener {
    private final static String TAG = "DetailPropFragment";


    private P_Select select;
    private int psetId;
    private int premium;
    private int up;
    private P_ProposalModel proposalModel;
    private P_MstDataProposalModel mstDataProposalModel;
    private P_MstProposalProductModel mstProposalProductModel;
    private ArrayList<P_MstProposalProductRiderModel> mstProposalProductRiderModels;
    private ArrayList<P_MstProposalProductUlinkModel> mstProposalProductUlinkModels;
    private ArrayList<P_MstProposalProductTopUpModel> mstProposalProductTopUpModels;

    private DropboxModel dKurs = new DropboxModel();
    private DropboxModel dKombPremi = new DropboxModel();
    private DropboxModel dCaraPembayaran = new DropboxModel();

//    private CalculationTextWatcher ctwPremi;
//    private CalculationTextWatcher ctwPremiPokok;
//    private CalculationTextWatcher ctwUp;

    private MoneyTextWatcher mtwPremi;
    private MoneyTextWatcher mtwPremiPokok;
    private MoneyTextWatcher mtwPremiTopUp;
    private MoneyTextWatcher mtwUp;
    private CalculationTextWatcher ctwPremi;
    private CalculationTextWatcher ctwPremiPokok;
    private CalculationTextWatcher ctwUP;

    private HashMap<String, Integer> productSetup = new HashMap<>();
    private int CALCULATION_FLAG = 0;

    public P_DetailProposalFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.p_fragment_detail_proposal, container, false);
        ButterKnife.bind(this, view);
        ((P_MainActivity) getActivity()).setSecondToolbarTitle(getString(R.string.fragment_title_detail_proposal));

        initiateView(view);

        return view;
    }

    private void initiateView(View view) {
        ((P_MainActivity) getActivity()).getSecond_toolbar().setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_save:
                        saveToMyBundle();
                        P_MainActivity.saveState(getContext(), true);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        select = new P_Select(getContext());
        proposalModel = P_MainActivity.myBundle.getParcelable(getString(R.string.proposal_model));
        mstDataProposalModel = proposalModel != null ? proposalModel.getMst_data_proposal() : null;
        mstProposalProductModel = proposalModel != null ? proposalModel.getMst_proposal_product() : null;
        if (mstProposalProductModel != null) {
            /*variabel product setup baru di set sesuai kebutuhan, jika nambah kebutuhan set variable yang ditangkap*/

            if (mstDataProposalModel.getFlag_packet() == null) {
                productSetup = select.selectProductSetupInteger(mstProposalProductModel.getLsbs_id(), mstProposalProductModel.getLsdbs_number());
                psetId = productSetup.get(getString(R.string.PSET_ID));
            } else {
                productSetup = select.selectProductSetupPacket(mstProposalProductModel.getLsbs_id(), mstProposalProductModel.getLsdbs_number(), mstDataProposalModel.getFlag_packet());
                psetId = productSetup.get(getString(R.string.PSET_ID));
                premium = productSetup.get(getString(R.string.PREMIUM));
                up = productSetup.get(getString(R.string.UP));
            }
        }
        mstProposalProductUlinkModels = proposalModel != null ? proposalModel.getMst_proposal_product_ulink() : null;
        mstProposalProductRiderModels = proposalModel != null ? proposalModel.getMst_proposal_product_rider() : null;
        mstProposalProductTopUpModels = proposalModel != null ? proposalModel.getMst_proposal_product_topup() : null;

        et_rencana_sub.addTextChangedListener(new MTextWatcher(et_rencana_sub));
        ac_kurs.addTextChangedListener(new MTextWatcher(ac_kurs));
        mtwPremi = new MoneyTextWatcher(et_premi);
        et_premi.addTextChangedListener(mtwPremi);
        et_premi.addTextChangedListener(new MTextWatcher(et_premi));
        et_premi.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        ac_komb_premi.addTextChangedListener(new MTextWatcher(ac_komb_premi));
        mtwPremiPokok = new MoneyTextWatcher(et_premi_pokok);
        et_premi_pokok.addTextChangedListener(mtwPremiPokok);
        et_premi_pokok.addTextChangedListener(new MTextWatcher(et_premi_pokok));
        et_premi_pokok.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        mtwPremiTopUp = new MoneyTextWatcher(et_premi_top_up);
        et_premi_top_up.addTextChangedListener(mtwPremiTopUp);
        et_premi_top_up.addTextChangedListener(new MTextWatcher(et_premi_top_up));
        ac_cara_pembayaran.addTextChangedListener(new MTextWatcher(ac_cara_pembayaran));
        mtwUp = new MoneyTextWatcher(et_up);
        et_up.addTextChangedListener(mtwUp);
        et_up.addTextChangedListener(new MTextWatcher(et_up));
        et_up.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        et_masa_kontrak.addTextChangedListener(new MTextWatcher(et_masa_kontrak));
        ac_masa_pembayaran_premi.addTextChangedListener(new MTextWatcher(ac_masa_pembayaran_premi));
        et_lama_pembayaran.addTextChangedListener(new MTextWatcher(et_lama_pembayaran));

        ac_kurs.setOnClickListener(this);
        ac_kurs.setOnFocusChangeListener(this);
        ac_kurs.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_kurs, this));
        ac_masa_pembayaran_premi.setOnClickListener(this);
        ac_masa_pembayaran_premi.setOnFocusChangeListener(this);
        ac_masa_pembayaran_premi.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_masa_pembayaran_premi, this));
        ac_cara_pembayaran.setOnClickListener(this);
        ac_cara_pembayaran.setOnFocusChangeListener(this);
        ac_cara_pembayaran.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_cara_pembayaran, this));
        ac_komb_premi.setOnClickListener(this);
        ac_komb_premi.setOnFocusChangeListener(this);
        ac_komb_premi.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_komb_premi, this));
//        view.findViewById(R.id.btn_lanjut).setOnClickListener(this);
//        view.findViewById(R.id.btn_kembali).setOnClickListener(this);
        getActivity().findViewById(R.id.btn_lanjut).setOnClickListener(this);
        getActivity().findViewById(R.id.btn_kembali).setOnClickListener(this);


        updateView();


//        /*special money harus setelah updateview*/
//        ctwPremi = new CalculationTextWatcher(et_premi);
//        ctwPremiPokok = new CalculationTextWatcher(et_premi_pokok);
//
//        et_premi.addTextChangedListener(ctwPremi);
//        et_premi_pokok.addTextChangedListener(ctwPremiPokok);

    }

    private void setCalculation() {

        String[] projection = new String[]{getString(R.string.FLAG_PREMIUM)};
        String selection = getString(R.string.PSET_ID) + " = ?";
        String[] selectionArgs = new String[]{String.valueOf(psetId)};

        QueryUtil queryUtil = new QueryUtil(getContext());
        Cursor cursor = queryUtil.query(
                getString(R.string.TABLE_LST_PRODUCT_CALC),
                projection,
                selection,
                selectionArgs,
                null
        );

        if (cursor != null) {
            Log.d(TAG, "setCalculation: cursor is not null");
            cursor.moveToFirst();
            CALCULATION_FLAG = cursor.getInt(cursor.getColumnIndexOrThrow(getString(R.string.FLAG_PREMIUM)));
            cursor.close();
        }

        Log.d(TAG, "setCalculation: CALCULATION FLAG is " + CALCULATION_FLAG);
        switch (CALCULATION_FLAG) {
            case 0:
                /*fill premi to calculate up*/
                ctwPremi = new CalculationTextWatcher(et_premi);
                et_premi.addTextChangedListener(ctwPremi);
                ctwPremiPokok = new CalculationTextWatcher(et_premi_pokok);
                et_premi_pokok.addTextChangedListener(ctwPremiPokok);
                break;
            case 1:
                /*fill up to fill premi*/
                ctwUP = new CalculationTextWatcher(et_up);
                et_up.addTextChangedListener(ctwUP);
                break;
            default:
                /*do 0 and 1*/

                break;
        }

    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.menu_va_proposal, menu);
//    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
        setCalculation();
        updateViewAdapter();
    }

    private void
    updateView() {
        DropboxModel dGeneral;

        int color = ContextCompat.getColor(getContext(), R.color.green);

        if (mstProposalProductModel.getLsbs_id() != null && mstProposalProductModel.getLsdbs_number() != null) {
            dGeneral = select.selectRencanaSub(mstProposalProductModel.getLsbs_id(), mstProposalProductModel.getLsdbs_number());
            et_rencana_sub.setText(dGeneral.getLabel());
            iv_circle_rencana_sub.setColorFilter(color);
        }

        if (mstProposalProductModel.getLku_id() != null) {
            Log.d(TAG, "updateView: lkuId = " + mstProposalProductModel.getLku_id());
            dKurs = select.selectKurs(mstProposalProductModel.getLku_id());
            ac_kurs.setText(dKurs.getLabel());
            iv_circle_kurs.setColorFilter(color);
        }

        if (mstProposalProductModel.getPremi() != null) {
            et_premi.setText(StaticMethods.toMoney(mstProposalProductModel.getPremi()));
            iv_circle_premi.setColorFilter(color);
        }

        if (mstProposalProductModel.getPremi_komb() != null) {
            dKombPremi = select.selectKombinasiPremi(mstProposalProductModel.getPremi_komb());
            ac_komb_premi.setText(dKombPremi.getLabel());
            iv_circle_komb_premi.setColorFilter(color);
        }

        if (mstProposalProductModel.getPremi_pokok() != null) {
            et_premi_pokok.setText(StaticMethods.toMoney(mstProposalProductModel.getPremi_pokok()));
            iv_circle_premi_pokok.setColorFilter(color);
        }
//        else if (mstProposalProductModel.getLsbs_id() == 134 && mstProposalProductModel.getLsdbs_number() == 10){
//            et_premi_pokok.setText(getString(R.string.MIN_PREMI_POKOK));
//        }

        if (mstProposalProductModel.getPremi_topup() != null) {
            et_premi_top_up.setText(StaticMethods.toMoney(mstProposalProductModel.getPremi_topup()));
            iv_circle_premi_top_up.setColorFilter(color);
        }

        if (mstProposalProductModel.getCara_bayar() != null) {
            dCaraPembayaran = select.selectCaraBayar(mstProposalProductModel.getCara_bayar());
            ac_cara_pembayaran.setText(dCaraPembayaran.getLabel());
            iv_circle_cara_pembayaran.setColorFilter(color);
        }

        if (mstProposalProductModel.getUp() != null) {
            et_up.setText(StaticMethods.toMoney(mstProposalProductModel.getUp()));
            iv_circle_up.setColorFilter(color);
        }


        /*khusus masa pembayaran premi, masakontrak, dan lama bayar itu ada di updateViewAdapter()*/


    }

    private ArrayList<SparseIntArray> widgetView;

    public void updateViewAdapter() {
        widgetView = select.selectLstProposalField(psetId);
        SparseIntArray display = widgetView.get(0);
        SparseIntArray enable = widgetView.get(1);
        ArrayList<DropboxModel> dropboxModels;

        /*set adapter kurs*/
        dropboxModels = select.selectListKurs(psetId);
//        if (psetId == 1012) {
//            ac_kurs.setEnabled(false); //smile ultimate link
//        }
        if (dKurs.getIdString() == null) {
            if (dropboxModels.size() > 0) {
                dKurs = dropboxModels.get(0);
                ac_kurs.setText(dKurs.getLabel());
            }
        }

        ArrayAdapter<DropboxModel> kursAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, dropboxModels);
        ac_kurs.setAdapter(kursAdapter);

        //Integer premi = productSetup.get(getString(R.string.PREMIUM));
        // set premi
        if (display.get(PREMI) == 1) {
            cl_premi.setVisibility(View.VISIBLE);
            et_premi.setEnabled(enable.get(PREMI) == 1);
//            if (psetId == 1012) {
//                et_premi.setText(String.valueOf(premium));
//            }
        } else {
            cl_premi.setVisibility(View.GONE);
        }

        /*set premi pokok*/
        if (display.get(PREMI_POKOK) == 1) {
            cl_premi_pokok.setVisibility(View.VISIBLE);
            et_premi_pokok.setEnabled((enable.get(PREMI_POKOK) == 1));
        } else {
            cl_premi_pokok.setVisibility(View.GONE);
        }

        /*set premi topup*/
        if (display.get(PREMI_TOPUP) == 1) {
            cl_premi_top_up.setVisibility(View.VISIBLE);
            et_premi_top_up.setEnabled(enable.get(PREMI_TOPUP) == 1);
        } else {
            cl_premi_top_up.setVisibility(View.GONE);
        }


        /*set up*/
        if (display.get(UP) == 1) {
            cl_up.setVisibility(View.VISIBLE);
            et_up.setEnabled(enable.get(UP) == 1);
//            if (psetId == 1012) {
//                et_up.setText(String.valueOf(up));
//            }
        } else {
            cl_up.setVisibility(View.GONE);
        }


        /*set adapter komb premi*/
        if (display.get(KOMBINASI_PREMI) == 1) {
            cl_komb_premi.setVisibility(View.VISIBLE);
            if (enable.get(KOMBINASI_PREMI) == 1 && psetId != 1260) {//TODO siap2u syariah masih di hardcode
                ArrayList<DropboxModel> kombinasiPremiList = select.selectListKombinasiPremiBaseProduct(psetId);
                ArrayAdapter<DropboxModel> kombPremiAdapter = new ArrayAdapter<>(getContext(),
                        android.R.layout.simple_dropdown_item_1line, kombinasiPremiList);
                ac_komb_premi.setAdapter(kombPremiAdapter);

            } else if (psetId == 1260) { // khusus slain 100% dan langsung di set label nya (siap2u syariah)
                ac_komb_premi.setEnabled(false);
                int color = ContextCompat.getColor(getContext(), R.color.green);
                dKombPremi = select.selectKombinasiPremi(80);
                ac_komb_premi.setText(dKombPremi.getLabel());
                iv_circle_komb_premi.setColorFilter(color);
            } else {
                ac_komb_premi.setEnabled(false);
                int color = ContextCompat.getColor(getContext(), R.color.green);
                dKombPremi = select.selectKombinasiPremi(100);
                ac_komb_premi.setText(dKombPremi.getLabel());
                iv_circle_komb_premi.setColorFilter(color);
            }

        } else {
            cl_komb_premi.setVisibility(View.GONE);
        }

        /*set cara bayar*/
        if (display.get(CARA_BAYAR) == 1) {
            cl_cara_pembayaran.setVisibility(View.VISIBLE);
            dropboxModels = select.selectListCaraBayar(psetId);
            ArrayAdapter<DropboxModel> caraBayarAdapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_dropdown_item_1line, dropboxModels);
//            if (psetId == 1012) {
//                ac_cara_pembayaran.setEnabled(false); //untuk smile link ultimate di disable
//            }
            if (dCaraPembayaran.getId() == null) {
                if (dropboxModels.size() > 0) {
                    dCaraPembayaran = dropboxModels.get(0);
                    ac_cara_pembayaran.setText(dCaraPembayaran.getLabel());
                }
            }
            ac_cara_pembayaran.setAdapter(caraBayarAdapter);
//            showInfoPremi();
        } else {
            cl_cara_pembayaran.setVisibility(View.GONE);
        }


        Integer insuredPeriod = productSetup.get(getString(R.string.INSURED_PERIOD));
        Integer lamaBayar = productSetup.get(getString(R.string.PAY_PERIOD));
        Integer cutiPremi = productSetup.get(getString(R.string.PREMIUM_HOLIDAY));
//        Integer cutiPremiFlag = productSetup.get(getString(R.string.PREMIUM_HOLIDAY_FLAG));
        int masaKontrak = insuredPeriod - mstDataProposalModel.getUmur_tt();


        /*set masa kontrak*/
        if (display.get(MASA_KONTRAK) == 1) {
            cl_masa_kontrak.setVisibility(View.VISIBLE);

            if (mstDataProposalModel.getFlag_tt_calon_bayi() == 1) {
                masaKontrak = insuredPeriod;
            }
            et_masa_kontrak.setText((masaKontrak < 1) ? "0" : String.valueOf(masaKontrak));
            et_masa_kontrak.setEnabled(enable.get(MASA_KONTRAK) == 1);

        } else {
            cl_masa_kontrak.setVisibility(View.GONE);
        }


        /*set Lama Bayar*/
        if (display.get(LAMA_PEMBAYARAN) == 1) {
            cl_lama_pembayaran.setVisibility(View.VISIBLE);
            if (lamaBayar != null) {
                et_lama_pembayaran.setText(String.valueOf(lamaBayar));
            } else {
                et_lama_pembayaran.setText(String.valueOf(masaKontrak));
            }

            et_lama_pembayaran.setEnabled(enable.get(LAMA_PEMBAYARAN) == 1);
        } else {
            cl_lama_pembayaran.setVisibility(View.GONE);
        }

        /*set masa pembayaran premi*/
        int masaPembayaranPremi;
        if (display.get(THN_CUTI_PREMI_MASA_PEMBAYARAN_PREMI) == 1) {
            cl_masa_pembayaran_premi.setVisibility(View.VISIBLE);
            ac_masa_pembayaran_premi.setEnabled(enable.get(THN_CUTI_PREMI_MASA_PEMBAYARAN_PREMI) == 1);
            if (ac_masa_pembayaran_premi.isEnabled()) {
                dropboxModels = select.selectMasaPembayaranPremi(mstProposalProductModel.getLsbs_id(), productSetup, mstDataProposalModel.getUmur_tt());
                if (mstProposalProductModel.getThn_cuti_premi() != null) {
                    Log.d(TAG, "updateView: getThnCutiPremi = " + mstProposalProductModel.getThn_cuti_premi());
                    masaPembayaranPremi = mstProposalProductModel.getThn_cuti_premi();
                    setViewMasaPembayaranPremi(masaPembayaranPremi);
                }
//                dropboxModels = select.selectMasaPembayaranPremi(productSetup, mstDataProposalModel.getUmur_tt());
                ArrayAdapter<DropboxModel> masaPembayaranAdapter = new ArrayAdapter<>(getContext(),
                        android.R.layout.simple_dropdown_item_1line, dropboxModels);
                ac_masa_pembayaran_premi.setAdapter(masaPembayaranAdapter);
            }
            else if (!ac_masa_pembayaran_premi.isEnabled() && mstDataProposalModel.getFlag_packet() != null) { // khusus smile link ultimate
                dropboxModels = select.selectMasaPembayaranPremi(mstProposalProductModel.getLsbs_id(), productSetup, mstDataProposalModel.getUmur_tt());
                ac_masa_pembayaran_premi.setText(String.valueOf(lamaBayar));
//                    Log.d(TAG, "updateView: getThnCutiPremi = " + mstProposalProductModel.getThn_cuti_premi());
//                    masaPembayaranPremi = mstProposalProductModel.getThn_lama_bayar();
//                    setViewMasaPembayaranPremi(masaPembayaranPremi);
//                dropboxModels = select.selectMasaPembayaranPremi(productSetup, mstDataProposalModel.getUmur_tt());
//                ArrayAdapter<DropboxModel> masaPembayaranAdapterULTIMATE = new ArrayAdapter<>(getContext(),
//                        android.R.layout.simple_dropdown_item_1line, dropboxModels);
//                ac_masa_pembayaran_premi.setAdapter(masaPembayaranAdapterULTIMATE);
            }
            else {
                if (cutiPremi != null) {
                    Log.d(TAG, "updateView: cuti premi tidak null");
                    masaPembayaranPremi = cutiPremi;
                } else {
                    Log.d(TAG, "updateView: cuti premi null");
                    masaPembayaranPremi = Integer.parseInt(et_lama_pembayaran.getText().toString());
                }
                setViewMasaPembayaranPremi(masaPembayaranPremi);
            }

        } else {
            cl_masa_pembayaran_premi.setVisibility(View.GONE);
        }

        if (mstDataProposalModel.getFlag_packet() != null) {
            MethodSupport.disable(false, scroll_detailproposal); // untuk disable allview
            et_premi.setText(String.valueOf(premium));
            et_up.setText(String.valueOf(up));
        }

    }

    public void setViewMasaPembayaranPremi(int masaPembayaranPremi) {
        ac_masa_pembayaran_premi.setText(String.valueOf(masaPembayaranPremi));
        iv_circle_masa_pembayaran_premi.setColorFilter(ContextCompat.getColor(getContext(), R.color.green));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ac_kurs:
                ac_kurs.showDropDown();
                break;
            case R.id.ac_komb_premi:
                ac_komb_premi.showDropDown();
                break;
            case R.id.ac_cara_pembayaran:
                ac_cara_pembayaran.showDropDown();
                break;
            case R.id.ac_masa_pembayaran_premi:
                ac_masa_pembayaran_premi.showDropDown();
                break;
            case R.id.btn_lanjut:
                attemptContinue();
                break;
            case R.id.btn_kembali:
                saveToMyBundle();
                ((P_MainActivity) getActivity()).onKembaliPressed();
                break;
            default:
                throw new IllegalArgumentException("Illegal id onClick");
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.ac_kurs:
                if (hasFocus)
                    ac_kurs.showDropDown();
                break;
            case R.id.ac_komb_premi:
                if (hasFocus)
                    ac_komb_premi.showDropDown();
                break;
            case R.id.ac_cara_pembayaran:
                if (hasFocus)
                    ac_cara_pembayaran.showDropDown();
                break;
            case R.id.ac_masa_pembayaran_premi:
                if (hasFocus)
                    ac_masa_pembayaran_premi.showDropDown();
                break;
            default:
                throw new IllegalArgumentException("Illegal id onClick");
        }
    }


    private void calculatePremiPokok() {
        BigDecimal premi = (et_premi.getText().toString().trim().length() == 0) ? BigDecimal.ZERO : StaticMethods.toBigDecimal(et_premi.getText().toString());
        BigDecimal premiPokok;
        if (StaticMethods.isViewVisible(cl_komb_premi)) {
            BigDecimal ppPercent = BigDecimal.ZERO;
            if (dKombPremi.getId() != null) {
                ppPercent = new BigDecimal(dKombPremi.getId());
            }
            premiPokok = premi.multiply(ppPercent).divide(new BigDecimal(100), BigDecimal.ROUND_DOWN);
            et_premi_pokok.removeTextChangedListener(mtwPremiPokok);
            et_premi_pokok.setText(StaticMethods.toMoney(premiPokok));
            et_premi_pokok.addTextChangedListener(mtwPremiPokok);
        }
    }

    private void calculatePremiTopup() {

        BigDecimal premi = (et_premi.getText().toString().trim().length() == 0) ? BigDecimal.ZERO : StaticMethods.toBigDecimal(et_premi.getText().toString());
        BigDecimal premiPokok = (et_premi_pokok.getText().toString().trim().length() == 0) ? BigDecimal.ZERO : StaticMethods.toBigDecimal(et_premi_pokok.getText().toString());

        BigDecimal premiTopup = (premi.subtract(premiPokok).compareTo(BigDecimal.ZERO) == -1) ? BigDecimal.ZERO : premi.subtract(premiPokok);

        et_premi_top_up.removeTextChangedListener(mtwPremiTopUp);
        et_premi_top_up.setText(StaticMethods.toMoney(premiTopup));
        et_premi_top_up.addTextChangedListener(mtwPremiTopUp);

    }


    private void calculatePremi() {
        Log.d(TAG, "calculatePremi: called");
        BigDecimal up = (et_up.getText().toString().trim().length() == 0) ? BigDecimal.ZERO : StaticMethods.toBigDecimal(et_up.getText().toString());
        HashMap<String, Object> params = getParamsForPremi(up);
        BigDecimal premiValue = (dCaraPembayaran.getId() == null) ? BigDecimal.ZERO : P_StaticMethods.getPremi(getContext(), params);

        if (premiValue.compareTo(BigDecimal.ZERO) == -1) premiValue = BigDecimal.ZERO;

        et_premi.removeTextChangedListener(mtwPremi);
        et_premi.setText(StaticMethods.toMoney(premiValue));
        et_premi.addTextChangedListener(mtwPremi);
//        showInfoUp();
    }

    private void calculateUp() {
        Log.d(TAG, "calculateUp: called");
        if (dCaraPembayaran.getId() != null) {
            BigDecimal premi = (et_premi.getText().toString().trim().length() == 0) ? BigDecimal.ZERO : StaticMethods.toBigDecimal(et_premi.getText().toString());
            BigDecimal premiPokok = (et_premi_pokok.getText().toString().trim().length() == 0) ? BigDecimal.ZERO : StaticMethods.toBigDecimal(et_premi_pokok.getText().toString());
            HashMap<String, Object> params = getParamsForUp(premi, premiPokok);
            BigDecimal upValue = P_StaticMethods.getUP(getContext(), params);
            if (upValue == null) upValue = P_StaticMethods.getMinUP(getContext(), params);
            if (upValue == null) upValue = P_StaticMethods.getMaxUP(getContext(), params);
            if (upValue == null) upValue = BigDecimal.ZERO;
            if (upValue.compareTo(BigDecimal.ZERO) == -1) upValue = BigDecimal.ZERO;

            et_up.removeTextChangedListener(mtwUp);
            et_up.setText(StaticMethods.toMoney(upValue));
            et_up.addTextChangedListener(mtwUp);
        }
//        showInfoUp();
    }

    public void showInfoUp() {
        if (dCaraPembayaran.getId() != null) {
            BigDecimal premi = (et_premi.getText().toString().trim().length() == 0) ? BigDecimal.ZERO : StaticMethods.toBigDecimal(et_premi.getText().toString());
            BigDecimal premiPokok = (et_premi_pokok.getText().toString().trim().length() == 0) ? BigDecimal.ZERO : StaticMethods.toBigDecimal(et_premi_pokok.getText().toString());
            HashMap<String, Object> params = getParamsForUp(premi, premiPokok);
            BigDecimal upMin = P_StaticMethods.getMinUP(getContext(), params);
            if (upMin == null) upMin = BigDecimal.ZERO;
            BigDecimal upMAx = P_StaticMethods.getMaxUP(getContext(), params);
            if (upMAx == null) upMAx = BigDecimal.ZERO;


            if (upMin.compareTo(upMAx) != 0) {
                String infoUp = "";
                Log.d(TAG, "showInfoUp: up min = " + upMin.toString() + "up max = " + upMAx);
                if (upMin.compareTo(upMAx) == 1 && !upMAx.equals(BigDecimal.ZERO)) {
                    infoUp = "Nilai premi pokok terlalu kecil sehingga minimal premi lebih besar dari max premi";
                } else if (upMin.compareTo(upMAx) == -1) {
                    infoUp = getString(R.string.nilai_min) + " " + StaticMethods.toMoney(upMin) + " Dan " + getString(R.string.nilai_max) + " " + StaticMethods.toMoney(upMAx);
                }
                if (!infoUp.equals("")) {
                    tv_info_up.setText(infoUp);
                    tv_info_up.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void showInfoPremi() {
        HashMap<String, BigDecimal> hmMinPremi = select.selectPremiRule(psetId,
                dCaraPembayaran.getId(),
                dKurs.getIdString());

        BigDecimal premiMinimal = hmMinPremi.get(getString(R.string.MIN_PREMIUM));
        BigDecimal premiPokokMinimal = hmMinPremi.get(getString(R.string.MIN_PREMI_POKOK));
        String infoPremi;
        if (premiMinimal != null) {
            infoPremi = getString(R.string.nilai_min) + " " + StaticMethods.toMoney(premiMinimal);
            tv_info_premi.setText(infoPremi);
            tv_info_premi.setVisibility(View.VISIBLE);
        }

        if (premiPokokMinimal != null) {
            infoPremi = getString(R.string.nilai_min) + " " + StaticMethods.toMoney(premiPokokMinimal);
            tv_info_premi_pokok.setText(infoPremi);
            tv_info_premi_pokok.setVisibility(View.VISIBLE);
        }

    }

    public void attemptContinue() {
        boolean success = true;
        boolean widgetFilled = true;

        if (cl_premi.isShown()) {
            if (et_premi.getText().toString().trim().length() == 0) {
                success = false;
                widgetFilled = false;
                tv_error_premi.setText(getString(R.string.field_requirement));
                tv_error_premi.setVisibility(View.VISIBLE);
            }
        }
        if (cl_premi_pokok.isShown()) { //StaticMethods.isViewVisible(cl_premi_pokok)
            if (et_premi_pokok.getText().toString().trim().length() == 0) {
                success = false;
                widgetFilled = false;
                tv_error_premi_pokok.setText(getString(R.string.field_requirement));
                tv_error_premi_pokok.setVisibility(View.VISIBLE);
            }
        }

        if (et_up.getText().toString().trim().length() == 0) {
            success = false;
            widgetFilled = false;
            tv_error_up.setText(getString(R.string.field_requirement));
            tv_error_up.setVisibility(View.VISIBLE);
        }

        if (ac_cara_pembayaran.isShown()) { //StaticMethods.isViewVisible(ac_cara_pembayaran)
            if (dCaraPembayaran.getId() == null) {
                success = false;
                widgetFilled = false;
                tv_error_cara_pembayaran.setText(getString(R.string.field_requirement));
                tv_error_cara_pembayaran.setVisibility(View.VISIBLE);
            }
        }

        if (cl_komb_premi.isShown()) { //StaticMethods.isViewVisible(cl_komb_premi)
            if (dKombPremi.getId() == null) {
                success = false;
                widgetFilled = false;
                tv_error_komb_premi.setText(getString(R.string.field_requirement));
                tv_error_komb_premi.setVisibility(View.VISIBLE);
            }
        }

        if (cl_masa_pembayaran_premi.isShown()) { //StaticMethods.isViewVisible(cl_masa_pembayaran_premi)
            if (ac_masa_pembayaran_premi.getText().toString().trim().length() == 0) {
                success = false;
                widgetFilled = false;
                tv_error_masa_pembayaran_premi.setText(getString(R.string.field_requirement));
                tv_error_masa_pembayaran_premi.setVisibility(View.VISIBLE);
            }
        }

        if (widgetFilled) {
            String errorPremi;
            BigDecimal premi = (et_premi.getText().toString().trim().length() == 0) ? BigDecimal.ZERO : StaticMethods.toBigDecimal(et_premi.getText().toString());
            BigDecimal premiPokok = (et_premi_pokok.getText().toString().trim().length() == 0) ? BigDecimal.ZERO : StaticMethods.toBigDecimal(et_premi_pokok.getText().toString());
            BigDecimal up = (et_up.getText().toString().trim().length() == 0) ? BigDecimal.ZERO : StaticMethods.toBigDecimal(et_up.getText().toString());

            HashMap<String, BigDecimal> hmMinPremi = select.selectPremiRule(psetId,
                    dCaraPembayaran.getId(),
                    dKurs.getIdString());

//            Integer flagPremi = hmMinPremi.get(getString(R.string.FLAG_PREMIUM)).intValue();
            BigDecimal premiMin = hmMinPremi.get(getString(R.string.MIN_PREMIUM));
            BigDecimal premiMax = hmMinPremi.get(getString(R.string.MAX_PREMIUM));
            BigDecimal premiPokokMin = hmMinPremi.get(getString(R.string.MIN_PREMI_POKOK));

            if (mstProposalProductModel.getLsbs_id() == 134 && mstProposalProductModel.getLsdbs_number() == 10){
                if(premiPokok.compareTo(premiPokokMin) == -1){
                    success = false;
                    tv_error_premi_pokok.setText("Nilai min Premi Pokok 10.000.000");
                    tv_error_premi_pokok.setVisibility(View.VISIBLE);
                } else if (premiPokok.compareTo(premiPokokMin) == 1) {
                    success = false;
                    tv_error_premi_pokok.setText("Nilai max Premi Pokok 10.000.000");
                    tv_error_premi_pokok.setVisibility(View.VISIBLE);
                }
            }

            if (et_premi_pokok.isShown()) {
                if (premi.compareTo(premiPokok) == -1) {
                    success = false;
                    tv_error_premi_pokok.setText(R.string.error_nilai_premi_kecil_dari_premi_pokok);
                    tv_error_premi_pokok.setVisibility(View.VISIBLE);
                } else {
                    if (premiPokokMin != null) {
                        if (premiPokok.compareTo(premiPokokMin) == -1) {
                            success = false;
                            errorPremi = getString(R.string.nilai_min) + " " + StaticMethods.toMoney(premiPokokMin);
                            tv_error_premi_pokok.setText(errorPremi);
                            tv_error_premi_pokok.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
            if (premiMin != null) {
                if (premi.compareTo(premiMin) == -1) {
                    success = false;
                    errorPremi = getString(R.string.nilai_min) + " " + StaticMethods.toMoney(premiMin);
                    tv_error_premi.setText(errorPremi);
                    tv_error_premi.setVisibility(View.VISIBLE);
                } else {
                    if (premiMax != null) {
                        if (premi.compareTo(premiMax) == 1) {
                            success = false;
                            errorPremi = getString(R.string.nilai_max) + " " + StaticMethods.toMoney(premiMax);
                            tv_error_premi.setText(errorPremi);
                            tv_error_premi.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }


            DecimalFormat formatter = new DecimalFormat("#,###");
            HashMap<String, Object> params = getParamsForUp(premi, premiPokok);
            BigDecimal upMin = P_StaticMethods.getMinUP(getContext(), params);
            BigDecimal upMAx = P_StaticMethods.getMaxUP(getContext(), params);
            BigDecimal upMaxPrime = BigDecimal.valueOf(15000000);
            BigDecimal upmaxPrimeDolar = BigDecimal.valueOf(1500);


            //check psetid kalau bukan
            //check usia diatas 70 atau tidak
            if (psetId == 27 || psetId == 982){
                if (mstDataProposalModel.getUmur_tt() >= 70 && mstDataProposalModel.getUmur_tt() <= 80){
                    if (dKurs.getIdString().equals("01") && up.compareTo(upMaxPrime) == 1 ) {
                        success = false;
//                    String stringMoney = formatter.format(upMin).replace(",", ".");
//                    String nilaiMin = ("Nilai Uang Pertanggungan harus") + " " + stringMoney;
                        tv_error_up.setText("Nilai Uang Pertanggungan harus 15.000.000");
                        tv_error_up.setVisibility(View.VISIBLE);
                    } else if (dKurs.getIdString().equals("02") && up.compareTo(upmaxPrimeDolar) == 1) {
                        success = false;
//                    String stringMoney = formatter.format(upMin).replace(",", ".");
//                    String nilaiMin = ("Nilai Uang Pertanggungan harus") + " " + stringMoney;
                        tv_error_up.setText("Nilai Uang Pertanggungan harus 1500");
                        tv_error_up.setVisibility(View.VISIBLE);
                    }
                } else{
                    if (upMin != null) {
                        if ((up.compareTo(upMin)) == -1) {
                            success = false;
                            String stringMoney = formatter.format(upMin).replace(",", ".");
                            String nilaiMin = getString(R.string.nilai_min) + " " + stringMoney;
                            tv_error_up.setText(nilaiMin);
                            tv_error_up.setVisibility(View.VISIBLE);
                        } else {
                            if (upMAx != null) {
                                if (up.compareTo(upMAx) == 1) {
                                    success = false;
                                    String stringMoney = formatter.format(upMAx).replace(",", ".");
                                    String nilaiMax = getString(R.string.nilai_max) + " " + stringMoney;
                                    tv_error_up.setText(nilaiMax);
                                    tv_error_up.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    }
                }
            }
            else {
                if (upMin != null) {
                    if ((up.compareTo(upMin)) == -1) {
                        success = false;
                        String stringMoney = formatter.format(upMin).replace(",", ".");
                        String nilaiMin = getString(R.string.nilai_min) + " " + stringMoney;
                        tv_error_up.setText(nilaiMin);
                        tv_error_up.setVisibility(View.VISIBLE);
                    } else {
                        if (upMAx != null) {
                            if (up.compareTo(upMAx) == 1) {
                                success = false;
                                String stringMoney = formatter.format(upMAx).replace(",", ".");
                                String nilaiMax = getString(R.string.nilai_max) + " " + stringMoney;
                                tv_error_up.setText(nilaiMax);
                                tv_error_up.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }
            }
            //jalanin didalam pset id cek usia
            //filter kurs


            String sCutiPremi = (ac_masa_pembayaran_premi.getText().toString().trim().length() == 0) ? "0" : ac_masa_pembayaran_premi.getText().toString();
            int cutiPremi = Integer.parseInt(sCutiPremi);
            if (mstProposalProductModel.getLsbs_id() == 212) {
                if (cutiPremi + mstDataProposalModel.getUmur_tt() > 60) {
                    success = false;
                    String errorCutiMessage = "Max masa pembayaran premi adalah " + (60 - mstDataProposalModel.getUmur_tt()) + " tahun";
                    tv_error_masa_pembayaran_premi.setText(errorCutiMessage);
                    tv_error_masa_pembayaran_premi.setVisibility(View.VISIBLE);
                }
            }
        }

        if (success) {
            if (mstProposalProductModel.getLku_id() == null) {
                goToNextPage();
            } else {
                if (mstProposalProductModel.getLku_id().equals(dKurs.getIdString())) {
                    goToNextPage();
                } else {
                    showDialogValidation();
                }
            }
        } else {
            Toast.makeText(getContext(), "Mohon lengkapi data terlebih dahulu", Toast.LENGTH_SHORT).show(); //bernard ganti jgn not success
        }
    }

    private void goToNextPage() {
        /*first check if there are any possible fund*/
        QueryUtil queryUtil = new QueryUtil(getContext());

        Cursor fundCursor = queryUtil.query(
                getString(R.string.TABLE_LST_PRODUCT_INVEST),
                null,
                getString(R.string.PSET_ID) + " = ?",
                new String[]{String.valueOf(psetId)},
                null
        );
        int fundCursorSize = fundCursor.getCount();
        fundCursor.close();

        Fragment fragment;
        if (fundCursorSize > 0) {
            fragment = new P_PilihanInvestasiFragment();
        } else {
            SparseIntArray display = widgetView.get(0);
            if (display.get(TOMBOL_TOPUP_PENARIKAN) == 1) {
                fragment = new P_TopUpFragment();
            } else {
                Cursor riderCursor = queryUtil.query(
                        getString(R.string.TABLE_LST_BISNIS_RIDER),
                        new String[]{"RIDER_ID"},
                        getString(R.string.LSBS_ID) + " = ? AND " + getString(R.string.LSDBS_NUMBER) + " = ?",
                        new String[]{String.valueOf(mstProposalProductModel.getLsbs_id()), String.valueOf(mstProposalProductModel.getLsdbs_number())},
                        null
                );
                int riderCount = riderCursor.getCount();
                if (riderCount > 0) {
                    if (mstProposalProductModel.getLsbs_id() != 208) {
                        fragment = new P_RiderFragment();
                    } else {
                        if (mstDataProposalModel.getFlag_tt_calon_bayi() == 1) {
                            fragment = new P_RiderFragment();
                        } else {
                            mstProposalProductRiderModels = new ArrayList<>();
                            saveToMyBundle();
                            P_MainActivity.saveState(getContext(), true);
                            fragment = new P_ResultFragment();
                        }
                    }
                } else {
                    saveToMyBundle();
                    P_MainActivity.saveState(getContext(), true);
                    fragment = new P_ResultFragment();
                }
            }
        }

        ((P_MainActivity) getActivity()).setFragmentBackStack(fragment, "fragment");
    }

    private void showDialogValidation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Konfirmasi");
        builder.setMessage(getString(R.string.message_konfirmasi_beda_kurs));

        builder.setPositiveButton("IYA", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                mstProposalProductUlinkModels = new ArrayList<>();
                mstProposalProductRiderModels = new ArrayList<>();
                mstProposalProductTopUpModels = getDefaultTPValue();

                goToNextPage();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onPause() {
        super.onPause();
        saveToMyBundle();
        Log.d(TAG, "urutan onPause: ");
    }

    public void setFragment(Fragment fragment, String fragmentTag) {
        if (fragment != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
            fragmentTransaction.replace(R.id.fl_container, fragment);
            fragmentTransaction.addToBackStack(fragmentTag);
            fragmentTransaction.commit();
        }
    }

    public void saveToMyBundle() {
        SparseIntArray display = widgetView.get(0);
        mstDataProposalModel.setLast_page_position(DATA_PROPOSAL_PAGE);//semua kembali keawal

        mstProposalProductModel.setLku_id(dKurs.getIdString());

        mstProposalProductModel.setPremi((et_premi.getText().toString().trim().length() != 0) ? StaticMethods.toStringNumber(et_premi.getText().toString()) : null);

        if (display.get(PREMI_POKOK) == 1) {
            mstProposalProductModel.setPremi_pokok((et_premi_pokok.getText().toString().trim().length() != 0) ? StaticMethods.toStringNumber(et_premi_pokok.getText().toString()) : null);
        } else {
            mstProposalProductModel.setPremi_pokok((et_premi.getText().toString().trim().length() != 0) ? StaticMethods.toStringNumber(et_premi.getText().toString()) : null);
        }
        if (display.get(PREMI_TOPUP) == 1) {
            mstProposalProductModel.setPremi_topup((et_premi_top_up.getText().toString().trim().length() != 0) ? StaticMethods.toStringNumber(et_premi_top_up.getText().toString()) : null);
        } else {
            mstProposalProductModel.setPremi_topup(null);
        }

        mstProposalProductModel.setPremi_komb((dKombPremi.getId() != null) ? dKombPremi.getId() : null);

        mstProposalProductModel.setCara_bayar((dCaraPembayaran.getId() != null) ? dCaraPembayaran.getId() : null);

        mstProposalProductModel.setUp((et_up.getText().toString().trim().length() != 0) ? StaticMethods.toStringNumber(et_up.getText().toString()) : null);

        if (mstProposalProductModel.getLsbs_id() == 212 || mstProposalProductModel.getLsbs_id() == 223) {
            if (ac_masa_pembayaran_premi.getText().toString().trim().length() != 0)
                mstProposalProductModel.setThn_masa_kontrak(Integer.parseInt(ac_masa_pembayaran_premi.getText().toString()));
        } else {
            mstProposalProductModel.setThn_masa_kontrak((et_masa_kontrak.getText().toString().trim().length() != 0) ? Integer.parseInt(et_masa_kontrak.getText().toString()) : null);
        }

        if (StaticMethods.isViewVisible(cl_lama_pembayaran)) {
            mstProposalProductModel.setThn_lama_bayar((et_lama_pembayaran.getText().toString().trim().length() != 0) ? Integer.parseInt(et_lama_pembayaran.getText().toString()) : 0);
        } else {
            mstProposalProductModel.setThn_lama_bayar((ac_masa_pembayaran_premi.getText().toString().trim().length() != 0) ? Integer.parseInt(ac_masa_pembayaran_premi.getText().toString()) : 0);
        }

        if (StaticMethods.isViewVisible(cl_masa_pembayaran_premi)) {
            mstProposalProductModel.setThn_cuti_premi((ac_masa_pembayaran_premi.getText().toString().trim().length() != 0) ? Integer.parseInt(ac_masa_pembayaran_premi.getText().toString()) : null);
        } else {
            mstProposalProductModel.setThn_cuti_premi((et_lama_pembayaran.getText().toString().trim().length() != 0) ? Integer.parseInt(et_lama_pembayaran.getText().toString()) : null);
        }


        proposalModel.setMst_data_proposal(mstDataProposalModel);
        proposalModel.setMst_proposal_product(mstProposalProductModel);
        proposalModel.setMst_proposal_product_rider(mstProposalProductRiderModels);
        proposalModel.setMst_proposal_product_ulink(mstProposalProductUlinkModels);
        proposalModel.setMst_proposal_product_topup(mstProposalProductTopUpModels);

        P_MainActivity.myBundle.putParcelable(getString(R.string.proposal_model), proposalModel);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SparseIntArray enable = widgetView.get(1);
        switch (view.getId()) {
            case R.id.ac_kurs:
                dKurs = (DropboxModel) parent.getAdapter().getItem(position);
                switch (CALCULATION_FLAG) {
                    case 0:
                        if (enable.get(UP) != 1) {
                            calculateUp();
                        }
//                        calculateUp();
                        showInfoUp();
                        showInfoPremi();
                        break;
                    case 1:
                        calculatePremi();
                        showInfoUp();
                        break;
                    case 2:
                        break;
                }

                break;
            case R.id.ac_komb_premi:
                dKombPremi = (DropboxModel) parent.getAdapter().getItem(position);
                calculatePremiPokok();
                break;
            case R.id.ac_cara_pembayaran:
                dCaraPembayaran = (DropboxModel) parent.getAdapter().getItem(position);
                switch (CALCULATION_FLAG) {
                    case 0:
                        if (enable.get(UP) != 1) {
                            calculateUp();
                        }
//                        calculateUp();
                        showInfoUp();
                        showInfoPremi();
                        break;
                    case 1:
                        calculatePremi();
                        showInfoUp();
                        break;
                    case 2:
                        break;
                }
                break;
            case R.id.ac_masa_pembayaran_premi:
                break;
            default:
                throw new IllegalArgumentException("Salah id pada OnItemClick");
        }
    }


    public HashMap<String, Object> getParamsForUp(BigDecimal premi, BigDecimal premiPokok) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(getString(R.string.PREMI_POKOK), premiPokok.longValue());
        params.put(getString(R.string.UMUR_TT), mstDataProposalModel.getUmur_tt());
        params.put(getString(R.string.PSET_ID), psetId);
        params.put(getString(R.string.PRODUCT_CODE), mstProposalProductModel.getLsbs_id());
        params.put(getString(R.string.PLAN), mstProposalProductModel.getLsdbs_number());
        params.put(getString(R.string.JENIS_APP), 0);
        params.put(getString(R.string.LKU_ID), (dKurs.getIdString() != null) ? dKurs.getIdString() : -1);
        params.put(getString(R.string.LSCB_ID), (dCaraPembayaran.getId() != null) ? dCaraPembayaran.getId() : -1);
        params.put(getString(R.string.PREMIUM), premi.longValue());
        params.put(getString(R.string.PREMIUM_COMB), (dKombPremi.getId() != null) ? dKombPremi.getId() : -1);

        return params;
    }

    public HashMap<String, Object> getParamsForPremi(BigDecimal up) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(getString(R.string.PRODUCT_CODE), mstProposalProductModel.getLsbs_id());
        params.put(getString(R.string.PLAN), mstProposalProductModel.getLsdbs_number());
        params.put(getString(R.string.UP), up);
        params.put(getString(R.string.UMUR_TT), mstDataProposalModel.getUmur_tt());
        params.put(getString(R.string.FLAG_TT_CALON_BAYI), mstDataProposalModel.getFlag_tt_calon_bayi());
        params.put(getString(R.string.LSCB_ID), (dCaraPembayaran.getId() != null) ? dCaraPembayaran.getId() : -1);
        params.put(getString(R.string.PSET_ID), psetId);
        params.put(getString(R.string.LKU_ID), dKurs.getIdString());
//
        return params;
    }

    private class MTextWatcher implements TextWatcher {
        private View view;

        MTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                case R.id.et_rencana_sub:
                    if (StaticMethods.isTextWidgetEmpty(et_rencana_sub)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_rencana_sub.setColorFilter(color);
                    tv_error_rencana_sub.setVisibility(View.GONE);
                    break;
                case R.id.ac_kurs:
                    if (StaticMethods.isTextWidgetEmpty(ac_kurs)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_kurs.setColorFilter(color);
                    tv_error_kurs.setVisibility(View.GONE);

                    //remove error premi
                    if (StaticMethods.isTextWidgetEmpty(et_premi)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_premi.setColorFilter(color);
                    tv_error_premi.setVisibility(View.GONE);

                    //remove error up
                    if (StaticMethods.isTextWidgetEmpty(et_up)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_up.setColorFilter(color);
                    tv_error_up.setVisibility(View.GONE);

                    break;
                case R.id.et_premi:
                    if (StaticMethods.isTextWidgetEmpty(et_premi)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_premi.setColorFilter(color);
                    tv_error_premi.setVisibility(View.GONE);
                    break;
                case R.id.ac_komb_premi:
                    if (StaticMethods.isTextWidgetEmpty(ac_komb_premi)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_komb_premi.setColorFilter(color);
                    tv_error_komb_premi.setVisibility(View.GONE);
                    break;
                case R.id.et_premi_pokok:
                    if (StaticMethods.isTextWidgetEmpty(et_premi_pokok)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_premi_pokok.setColorFilter(color);
                    tv_error_premi_pokok.setVisibility(View.GONE);
                    break;
                case R.id.et_premi_top_up:
                    if (StaticMethods.isTextWidgetEmpty(et_premi_top_up)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_premi_top_up.setColorFilter(color);
                    tv_error_premi_top_up.setVisibility(View.GONE);
                    break;
                case R.id.ac_cara_pembayaran:
                    if (StaticMethods.isTextWidgetEmpty(ac_cara_pembayaran)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_cara_pembayaran.setColorFilter(color);
                    tv_error_cara_pembayaran.setVisibility(View.GONE);
                    break;
                case R.id.et_up:
                    if (StaticMethods.isTextWidgetEmpty(et_up)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_up.setColorFilter(color);
                    tv_error_up.setVisibility(View.GONE);
                    break;
                case R.id.et_masa_kontrak:
                    if (StaticMethods.isTextWidgetEmpty(et_masa_kontrak)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_masa_kontrak.setColorFilter(color);
                    tv_error_masa_kontrak.setVisibility(View.GONE);
                    break;
                case R.id.ac_masa_pembayaran_premi:
                    if (StaticMethods.isTextWidgetEmpty(ac_masa_pembayaran_premi)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_masa_pembayaran_premi.setColorFilter(color);
                    tv_error_masa_pembayaran_premi.setVisibility(View.GONE);
                    break;
                case R.id.et_lama_pembayaran:
                    if (StaticMethods.isTextWidgetEmpty(et_lama_pembayaran)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_lama_pembayaran.setColorFilter(color);
                    tv_error_lama_pembayaran.setVisibility(View.GONE);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown Id " + view.getId());
            }
        }
    }

    private class CalculationTextWatcher implements TextWatcher {
        private View view;

        public CalculationTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            SparseIntArray enable = widgetView.get(1);
            switch (view.getId()) {
                case R.id.et_premi:
                    Log.d(TAG, "afterTextChanged: et_premi");
                     /*Untuk premi dan kombinasi premi cukup menghitung premi pokok, selebihnya
      tugas premi pokok textwatcher untuk hitung premi topup dan up*/
                    calculatePremiPokok();
                    if (!StaticMethods.isViewVisible(cl_premi_pokok)) {
                        if (enable.get(UP) != 1) {
                            calculateUp();
                        }
                        showInfoUp();
                    }
                    if (!StaticMethods.isViewVisible(cl_komb_premi)) calculatePremiTopup();

                    break;
                case R.id.et_premi_pokok:
                    Log.d(TAG, "afterTextChanged: et_premi_pokok");
                    calculatePremiTopup();
                    if (enable.get(UP) != 1) {
                        calculateUp();
                    }
                    showInfoUp();
                    break;
                case R.id.et_up:
                    calculatePremi();
                    showInfoUp();
                    break;
                default:
                    break;
            }
        }
    }


    @BindView(R.id.cl_rencana_sub)
    ConstraintLayout cl_rencana_sub;
    @BindView(R.id.cl_kurs)
    ConstraintLayout cl_kurs;
    @BindView(R.id.cl_premi)
    ConstraintLayout cl_premi;
    @BindView(R.id.cl_komb_premi)
    ConstraintLayout cl_komb_premi;
    @BindView(R.id.cl_premi_pokok)
    ConstraintLayout cl_premi_pokok;
    @BindView(R.id.cl_premi_top_up)
    ConstraintLayout cl_premi_top_up;
    @BindView(R.id.cl_cara_pembayaran)
    ConstraintLayout cl_cara_pembayaran;
    @BindView(R.id.cl_up)
    ConstraintLayout cl_up;
    @BindView(R.id.cl_masa_kontrak)
    ConstraintLayout cl_masa_kontrak;
    @BindView(R.id.cl_masa_pembayaran_premi)
    ConstraintLayout cl_masa_pembayaran_premi;
    @BindView(R.id.cl_lama_pembayaran)
    ConstraintLayout cl_lama_pembayaran;

    @BindView(R.id.scroll_detailproposal)
    ScrollView scroll_detailproposal;

    @BindView(R.id.iv_circle_rencana_sub)
    ImageView iv_circle_rencana_sub;
    @BindView(R.id.iv_circle_kurs)
    ImageView iv_circle_kurs;
    @BindView(R.id.iv_circle_premi)
    ImageView iv_circle_premi;
    @BindView(R.id.iv_circle_komb_premi)
    ImageView iv_circle_komb_premi;
    @BindView(R.id.iv_circle_premi_pokok)
    ImageView iv_circle_premi_pokok;
    @BindView(R.id.iv_circle_premi_top_up)
    ImageView iv_circle_premi_top_up;
    @BindView(R.id.iv_circle_cara_pembayaran)
    ImageView iv_circle_cara_pembayaran;
    @BindView(R.id.iv_circle_up)
    ImageView iv_circle_up;
    @BindView(R.id.iv_circle_masa_kontrak)
    ImageView iv_circle_masa_kontrak;
    @BindView(R.id.iv_circle_masa_pembayaran_premi)
    ImageView iv_circle_masa_pembayaran_premi;
    @BindView(R.id.iv_circle_lama_pembayaran)
    ImageView iv_circle_lama_pembayaran;

    @BindView(R.id.et_rencana_sub)
    EditText et_rencana_sub;
    @BindView(R.id.ac_kurs)
    AutoCompleteTextView ac_kurs;
    @BindView(R.id.et_premi)
    EditText et_premi;
    @BindView(R.id.ac_komb_premi)
    AutoCompleteTextView ac_komb_premi;
    @BindView(R.id.et_premi_pokok)
    EditText et_premi_pokok;
    @BindView(R.id.et_premi_top_up)
    EditText et_premi_top_up;
    @BindView(R.id.ac_cara_pembayaran)
    AutoCompleteTextView ac_cara_pembayaran;
    @BindView(R.id.et_up)
    EditText et_up;
    @BindView(R.id.et_masa_kontrak)
    EditText et_masa_kontrak;
    @BindView(R.id.ac_masa_pembayaran_premi)
    AutoCompleteTextView ac_masa_pembayaran_premi;
    @BindView(R.id.et_lama_pembayaran)
    EditText et_lama_pembayaran;

    @BindView(R.id.tv_info_premi)
    TextView tv_info_premi;
    @BindView(R.id.tv_info_premi_pokok)
    TextView tv_info_premi_pokok;
    @BindView(R.id.tv_info_up)
    TextView tv_info_up;

    @BindView(R.id.tv_error_rencana_sub)
    TextView tv_error_rencana_sub;
    @BindView(R.id.tv_error_kurs)
    TextView tv_error_kurs;
    @BindView(R.id.tv_error_premi)
    TextView tv_error_premi;
    @BindView(R.id.tv_error_komb_premi)
    TextView tv_error_komb_premi;
    @BindView(R.id.tv_error_premi_pokok)
    TextView tv_error_premi_pokok;
    @BindView(R.id.tv_error_premi_top_up)
    TextView tv_error_premi_top_up;
    @BindView(R.id.tv_error_cara_pembayaran)
    TextView tv_error_cara_pembayaran;
    @BindView(R.id.tv_error_up)
    TextView tv_error_up;
    @BindView(R.id.tv_error_masa_kontrak)
    TextView tv_error_masa_kontrak;
    @BindView(R.id.tv_error_masa_pembayaran_premi)
    TextView tv_error_masa_pembayaran_premi;
    @BindView(R.id.tv_error_lama_pembayaran)
    TextView tv_error_lama_pembayaran;


}
