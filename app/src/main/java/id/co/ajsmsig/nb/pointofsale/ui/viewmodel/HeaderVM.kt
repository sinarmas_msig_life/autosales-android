package id.co.ajsmsig.nb.pointofsale.ui.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 21/02/18.
 */

class HeaderVM
@Inject
constructor(application: Application): AndroidViewModel(application) {

    var title: ObservableField<String?> = ObservableField()
    var subtitle: ObservableField<String> = ObservableField()
    var isThereSubtitle: ObservableBoolean = ObservableBoolean(false)
    var isSubtitleTop: ObservableBoolean = ObservableBoolean(false)
    val titleImage: ObservableField<String> = ObservableField()

}