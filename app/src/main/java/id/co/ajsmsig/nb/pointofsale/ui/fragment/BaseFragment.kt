package id.co.ajsmsig.nb.pointofsale.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import id.co.ajsmsig.nb.pointofsale.di.Injectable
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.ui.common.NavigationController
import id.co.ajsmsig.nb.pointofsale.utils.Constants.Companion.PAGE
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.*
import javax.inject.Inject
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.*
import java.util.*

/**
 * Created by andreyyoshuamanik on 10/02/18.
 */
abstract class BaseFragment: Fragment(), Injectable, BaseValidationInterface {

    @Inject lateinit var viewModelFactory: PageViewModelFactory
    @Inject lateinit var navigationController: NavigationController
    @Inject lateinit var mainRepository: MainRepository

    @Inject lateinit var sharedViewModel: SharedViewModel

    var page: Page? = null
    lateinit var mainVM: MainVM

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainVM = ViewModelProviders.of(activity!!, viewModelFactory).get(MainVM::class.java)

        mainVM.subtitleBottom()
        mainVM.subtitle.set(null)

        arguments?.getSerializable(PAGE)?.let {


            (it as? Page)?.let {
                var event = it.title ?: "Home"

                if (mainVM.currentPage.value?.title == null) {
                    when (event.toLowerCase()) {
                        "kebutuhan prioritas masa depan" -> {

                            event = "Kebutuhan Prioritas Masa Depan (Direct Access)"
                        }
                        "risk profiling tool" -> {
                            event = "Risk Profiling Tool (header only) - direct access"
                        }
                    }
                }

                page = it
                mainVM.currentPage.value = page
                mainVM.isThereTitle.set((it.title != null && it.title != "") || (it.titleImage != null && it.titleImage != ""))

                if (!(sharedViewModel.pages.contains(it))) sharedViewModel.pages.add(it)

                mainRepository.sendAnalytics(Analytics(
                        agentCode = sharedViewModel.agentCode,
                        agentName = sharedViewModel.agentName,
                        group = sharedViewModel.groupId,
                        event = event,
                        timestamp = System.currentTimeMillis()
                ))
            }

        }
    }


    override fun onDestroy() {
        super.onDestroy()

        sharedViewModel.pages.remove(page)
    }

}
