package id.co.ajsmsig.nb.prop.model.modelCalcIllustration;

import java.io.Serializable;
import java.util.Date;

public class Prop_Model_S_hcpf implements Serializable
{
	private static final long serialVersionUID = 5067446188904598686L;
	public int peserta;
    public String[] nama = new String[4 + 1]; 
    public Date[] tgl = new Date[4 + 1];
    public int[] usia = new int[4 + 1];
    public boolean changed; 
}
