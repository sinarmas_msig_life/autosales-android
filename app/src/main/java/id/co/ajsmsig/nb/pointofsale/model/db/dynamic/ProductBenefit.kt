package id.co.ajsmsig.nb.pointofsale.model.db.dynamic

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class ProductBenefit(
        @PrimaryKey
        var Id: Int = 0,
        var Text: String?,
        var ProductLsbsId: Int?,
        var Image: String?) {

        override fun toString(): String {
                return Text ?: ""
        }
}

