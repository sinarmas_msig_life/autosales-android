package id.co.ajsmsig.nb.pointofsale.ui.iriskprofileresult


import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosFragmentRiskProfileResultBinding
import id.co.ajsmsig.nb.pointofsale.ui.fragment.BaseFragment


/**
 * A simple [Fragment] subclass.
 */
class RiskProfileResultFragment : BaseFragment() {

    private lateinit var dataBinding: PosFragmentRiskProfileResultBinding

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(RiskProfileResultVM::class.java)
        subscribeUI(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_risk_profile_result, container, false)

        return dataBinding.root
    }

    fun subscribeUI(viewModel: RiskProfileResultVM) {
        dataBinding.vm = viewModel
    }

    override fun onDestroy() {
        super.onDestroy()

        sharedViewModel.selectedRiskProfile = null
    }
    override fun canNext(): Boolean {

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(RiskProfileResultVM::class.java)
        sharedViewModel.selectedRiskProfile = viewModel.observableRiskProfile.value

        return super.canNext()
    }
}// Required empty public constructor
