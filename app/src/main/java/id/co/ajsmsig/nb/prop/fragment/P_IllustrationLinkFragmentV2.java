package id.co.ajsmsig.nb.prop.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.adapter.P_AdapterListLinkIlustrasi;
import id.co.ajsmsig.nb.prop.adapter.P_RecyclerViewIllustrationAdapter;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.IllustrationResultVO;

/**
 * Created by faiz_f on 03/02/2017.
 */

public class P_IllustrationLinkFragmentV2 extends Fragment {
    //    private ArrayList<P_IllustrationDevelopmentFundModel> illustrationDevelopmentFundModels;
    public P_IllustrationLinkFragmentV2() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.p_fragment_investation_illustration_grid, container, false);

        Bundle bundle = getActivity().getIntent().getExtras();
        @SuppressWarnings("unchecked")
        HashMap<String, Object> iHash = (HashMap<String, Object>) bundle.getSerializable(getString(R.string.Illustrasi));
        if (iHash != null) {
            IllustrationResultVO illustrationResult = (IllustrationResultVO) iHash.get(getContext().getString(R.string.Illustration1));
            String validityMsq = illustrationResult.getValidityMsg();
            ArrayList<LinkedHashMap<String, String>> mapList = illustrationResult.getIllustrationList();

//
//            illustrationDevelopmentFundModels = P_IllustrationActivity.myBundle.getParcelableArrayList(getString(R.string.Illustrasi));
//            if (illustrationDevelopmentFundModels == null) {
//                Log.e("array ilustrasi", "check array ilustrasi: null");
//                illustrationDevelopmentFundModels = new ArrayList<>();
//            }
//
            if (mapList != null) {
                List<String> data = new ArrayList<>();
                for (HashMap<String, String> map : mapList) {
                    for (String value : map.values()) {
                        data.add(value);
                    }
                }

                int numberOfColumns = 0;
                if (mapList.size() > 0) {
                    numberOfColumns = mapList.get(0).size();
                }

                if (numberOfColumns > 0) {
                    RecyclerView rv_illustration = view.findViewById(R.id.rv_illustration);
                    rv_illustration.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));
                    P_RecyclerViewIllustrationAdapter adapter = new P_RecyclerViewIllustrationAdapter(getContext(), data);
//                adapter.setClickListener(this);
                    rv_illustration.setAdapter(adapter);
                }
            }
        }

        return view;
    }


}
