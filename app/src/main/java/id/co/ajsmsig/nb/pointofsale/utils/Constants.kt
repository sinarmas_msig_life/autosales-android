package id.co.ajsmsig.nb.pointofsale.utils

/**
 * Created by andreyyoshuamanik on 27/02/18.
 */

class Constants {
    companion object {
        val PAGE = "page"
        val RADIOBUTTON = "radiobutton"
        val DROPDOWN = "dropdown"
        val SL_TAB_ID = "SL_TAB_ID"     // ID Lead
        val SL_NAME = "SL_NAME"         // NAMA Lead
        val SL_GENDER = "SL_GENDER"     // Jenis Kelamin Lead
        val GROUP_ID = "GROUP_ID"       // Bank Agent
        val LSBS_ID = "GROUP_ID"       // Kode Produk
        val AGENT_CODE = "msag_id"       // AGENT Code
        val AGENT_NAME = "nama_agen"       // Agent Name
//        val API_URL = "http://173.212.215.75:2402"
        val API_URL = "http://poscms.sinarmasmsiglife.co.id"
        val FINISH_CYCLE = "Full Cycle"
        val UN_FINISH_CYCLE = "Un Full Cycle"
    }
}