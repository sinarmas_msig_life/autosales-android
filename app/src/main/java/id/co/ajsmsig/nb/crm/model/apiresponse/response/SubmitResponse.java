
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubmitResponse {

    @SerializedName("virtual")
    @Expose
    private String virtual;
    @SerializedName("spaj")
    @Expose
    private String spaj;
    @SerializedName("ERROR")
    @Expose
    private String eRROR;
    @SerializedName("spaj_real")
    @Expose
    private String spaj_real;

    public String getVirtual() {
        return virtual;
    }

    public void setVirtual(String virtual) {
        this.virtual = virtual;
    }

    public String getSpaj() {
        return spaj;
    }

    public void setSpaj(String spaj) {
        this.spaj = spaj;
    }

    public String getERROR() {
        return eRROR;
    }

    public void setERROR(String eRROR) {
        this.eRROR = eRROR;
    }

    public String getSpaj_real() {
        return spaj_real;
    }

    public void setSpaj_real(String spaj_real) {
        this.spaj_real = spaj_real;
    }
}
