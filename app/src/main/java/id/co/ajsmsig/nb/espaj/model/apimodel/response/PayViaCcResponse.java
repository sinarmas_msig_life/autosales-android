
package id.co.ajsmsig.nb.espaj.model.apimodel.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PayViaCcResponse {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("result")
    @Expose
    private String result;

    @SerializedName("redirectURL")
    @Expose
    private String redirectURL;

    @SerializedName("no_va")
    @Expose
    private String no_va;

    @SerializedName("payment_method")
    @Expose
    private String payment_method;

    public String getMessage() {
        return message;
    }

    public String getResult() {
        return result;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public String getNo_va() {
        return no_va;
    }

    public String getPayment_method() {
        return payment_method;
    }


}
