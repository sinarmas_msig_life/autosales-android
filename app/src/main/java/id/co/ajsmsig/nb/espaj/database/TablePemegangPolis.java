package id.co.ajsmsig.nb.espaj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.PemegangPolisModel;

/**
 * Created by eriza on 05/09/2017.
 */

public class TablePemegangPolis {
    private Context context;

    public TablePemegangPolis(Context context) {
        this.context = context;
    }
    public ContentValues getContentValues(EspajModel me) {
        ContentValues cv = new ContentValues();
        cv.put("NAMA_PP", me.getPemegangPolisModel().getNama_pp());
        cv.put("GELAR_PP", me.getPemegangPolisModel().getGelar_pp());
        cv.put("NAMA_IBU_PP", me.getPemegangPolisModel().getNama_ibu_pp());
        cv.put("BUKTI_IDENTITAS_PP", me.getPemegangPolisModel().getBukti_identitas_pp());
        cv.put("BUKTI_IDENTITAS_LAIN_PP", me.getPemegangPolisModel().getBukti_identitas_lain_pp());
        cv.put("NO_IDENTITAS_PP", me.getPemegangPolisModel().getNo_identitas_pp());
        cv.put("TGL_BERLAKU_PP", me.getPemegangPolisModel().getTgl_berlaku_pp());
        cv.put("WARGA_NEGARA_PP", me.getPemegangPolisModel().getWarga_negara_pp());
        cv.put("TEMPAT_PP", me.getPemegangPolisModel().getTempat_pp());
        cv.put("TTL_PP", me.getPemegangPolisModel().getTtl_pp());
        cv.put("USIA_PP", me.getPemegangPolisModel().getUsia_pp());
        cv.put("JEKEL_PP", me.getPemegangPolisModel().getJekel_pp());
        cv.put("STATUS_PP", me.getPemegangPolisModel().getStatus_pp());
        cv.put("AGAMA_PP", me.getPemegangPolisModel().getAgama_pp());
        cv.put("AGAMA_LAIN_PP", me.getPemegangPolisModel().getAgama_lain_pp());
        cv.put("PENDIDIKAN_PP", me.getPemegangPolisModel().getPendidikan_pp());
        cv.put("ALAMAT_PP", me.getPemegangPolisModel().getAlamat_pp());
        cv.put("KOTA_PP", me.getPemegangPolisModel().getKota_pp());
        cv.put("KDPOS_PP", me.getPemegangPolisModel().getKdpos_pp());
        cv.put("TELP1_PP", me.getPemegangPolisModel().getTelp1_pp());
        cv.put("KDTELP1_PP", me.getPemegangPolisModel().getKdtelp1_pp());
        cv.put("TELP2_PP", me.getPemegangPolisModel().getTelp2_pp());
        cv.put("KDTELP2_PP", me.getPemegangPolisModel().getKdtelp2_pp());
        cv.put("ALAMAT_KANTOR_PP", me.getPemegangPolisModel().getAlamat_kantor_pp());
        cv.put("KOTA_KANTOR_PP", me.getPemegangPolisModel().getKota_kantor_pp());
        cv.put("KDPOS_KANTOR_PP", me.getPemegangPolisModel().getKdpos_kantor_pp());
        cv.put("KDTELP1_KANTOR_PP", me.getPemegangPolisModel().getKdtelp1_kantor_pp());
        cv.put("TELP1_KANTOR_PP", me.getPemegangPolisModel().getTelp1_kantor_pp());
        cv.put("KDTELP2_KANTOR_PP", me.getPemegangPolisModel().getKdtelp2_kantor_pp());
        cv.put("TELP2_KANTOR_PP", me.getPemegangPolisModel().getTelp2_kantor_pp());
        cv.put("KDFAX_KANTOR_PP", me.getPemegangPolisModel().getKdfax_kantor_pp());
        cv.put("FAX_KANTOR_PP", me.getPemegangPolisModel().getFax_kantor_pp());
        cv.put("ALAMAT_TINGGAL_PP", me.getPemegangPolisModel().getAlamat_tinggal_pp());
        cv.put("KOTA_TINGGAL_PP", me.getPemegangPolisModel().getKota_tinggal_pp());
        cv.put("KDPOS_TINGGAL_PP", me.getPemegangPolisModel().getKdpos_tinggal_pp());
        cv.put("KDTELP1_TINGGAL_PP", me.getPemegangPolisModel().getKdtelp1_tinggal_pp());
        cv.put("TELP1_TINGGAL_PP", me.getPemegangPolisModel().getTelp1_tinggal_pp());
        cv.put("KDTELP2_TINGGAL_PP", me.getPemegangPolisModel().getKdtelp2_tinggal_pp());
        cv.put("TELP2_TINGGAL_PP", me.getPemegangPolisModel().getTelp2_tinggal_pp());
        cv.put("KDFAX_TINGGAL_PP", me.getPemegangPolisModel().getKdfax_tinggal_pp());
        cv.put("FAX_TINGGAL_PP", me.getPemegangPolisModel().getFax_tinggal_pp());
        cv.put("TAGIHAN_PP", me.getPemegangPolisModel().getTagihan_pp());
        cv.put("HP1_PP", me.getPemegangPolisModel().getHp1_pp());
        cv.put("HP2_PP", me.getPemegangPolisModel().getHp2_pp());
        cv.put("EMAIL_PP", me.getPemegangPolisModel().getEmail_pp());
        cv.put("KLASIFIKASI_PEKERJAAN_PP", me.getPemegangPolisModel().getKlasifikasi_pekerjaan_pp());
        cv.put("URAIAN_PEKERJAAN_PP", me.getPemegangPolisModel().getUraian_pekerjaan_pp());
        cv.put("JABATAN_KLASIFIKASI_PP", me.getPemegangPolisModel().getJabatan_klasifikasi_pp());
        cv.put("HUBUNGAN_PP", me.getPemegangPolisModel().getHubungan_pp());
        cv.put("GREENCARD_PP", me.getPemegangPolisModel().getGreencard_pp());
        cv.put("ALIAS_PP", me.getPemegangPolisModel().getAlias_pp());
        cv.put("NM_PERUSAHAAN_PP", me.getPemegangPolisModel().getNm_perusahaan_pp());
        cv.put("NPWP_PP", me.getPemegangPolisModel().getNpwp_pp());
        cv.put("TAGIHAN_RUTIN_PP", me.getPemegangPolisModel().getTagihan_rutin_pp());
        cv.put("KRM_POLIS_PP", me.getPemegangPolisModel().getKrm_polis_pp());
        cv.put("SUAMIRT_PP", me.getPemegangPolisModel().getSuamirt_pp());
        cv.put("TTLSUAMI_RT_PP", me.getPemegangPolisModel().getTtlsuami_rt_pp());
        cv.put("USIASUAMI_RT_PP", me.getPemegangPolisModel().getUsiasuami_rt_pp());
        cv.put("PEKERJAANSUAMI_RT_PP", me.getPemegangPolisModel().getPekerjaansuami_rt_pp());
        cv.put("JABATANSUAMI_RT_PP", me.getPemegangPolisModel().getJabatansuami_rt_pp());
        cv.put("PERUSAHAANSUAMI_RT_PP", me.getPemegangPolisModel().getPerusahaansuami_rt_pp());
        cv.put("BIDUSAHA_SUAMIRT_PP", me.getPemegangPolisModel().getBidusaha_suamirt_pp());
        cv.put("NPWP_SUAMIRT_PP", me.getPemegangPolisModel().getNpwp_suamirt_pp());
        cv.put("PENGHASILAN_SUAMITHN_PP", me.getPemegangPolisModel().getPenghasilan_suamithn_pp());
        cv.put("AYAH_PP", me.getPemegangPolisModel().getAyah_pp());
        cv.put("TTL_AYAH_PP", me.getPemegangPolisModel().getTtl_ayah_pp());
        cv.put("USIA_AYAH_PP", me.getPemegangPolisModel().getUsia_ayah_pp());
        cv.put("PEKERJAAN_AYAH_PP", me.getPemegangPolisModel().getPekerjaan_ayah_pp());
        cv.put("JABATAN_AYAH_PP", me.getPemegangPolisModel().getJabatan_ayah_pp());
        cv.put("PERUSAHAAN_AYAH_PP", me.getPemegangPolisModel().getPerusahaan_ayah_pp());
        cv.put("BIDUSAHA_AYAH_PP", me.getPemegangPolisModel().getBidusaha_ayah_pp());
        cv.put("NPWP_AYAH_PP", me.getPemegangPolisModel().getNpwp_ayah_pp());
        cv.put("PENGHASILAN_AYAH_PP", me.getPemegangPolisModel().getPenghasilan_ayah_pp());
        cv.put("NM_IBU_PP", me.getPemegangPolisModel().getNm_ibu_pp());
        cv.put("TTL_IBU_PP", me.getPemegangPolisModel().getTtl_ibu_pp());
        cv.put("USIA_IBU_PP", me.getPemegangPolisModel().getUsia_ibu_pp());
        cv.put("PEKERJAAN_IBU_PP", me.getPemegangPolisModel().getPekerjaan_ibu_pp());
        cv.put("JABATAN_IBU_PP", me.getPemegangPolisModel().getJabatan_ibu_pp());
        cv.put("PERUSAHAAN_IBU_PP", me.getPemegangPolisModel().getPerusahaan_ibu_pp());
        cv.put("BIDUSAHA_IBU_PP", me.getPemegangPolisModel().getBidusaha_ibu_pp());
        cv.put("NPWP_IBU_PP", me.getPemegangPolisModel().getNpwp_ibu_pp());
        cv.put("PENGHASILAN_IBU_PP", me.getPemegangPolisModel().getPenghasilan_ibu_pp());
        cv.put("PENDAPATAN_PP", me.getPemegangPolisModel().getPendapatan_pp());
        cv.put("HUBUNGANCP_PP", me.getPemegangPolisModel().getHubungancp_pp());
        cv.put("NO_CIFF_PP", me.getPemegangPolisModel().getNo_ciff_pp());
        cv.put("D_GAJI_PP", me.getPemegangPolisModel().getD_gaji_pp());
        cv.put("D_TABUNGAN_PP", me.getPemegangPolisModel().getD_tabungan_pp());
        cv.put("D_WARISAN_PP", me.getPemegangPolisModel().getD_warisan_pp());
        cv.put("D_HIBAH_PP", me.getPemegangPolisModel().getD_hibah_pp());
        cv.put("D_LAINNYA_PP", me.getPemegangPolisModel().getD_lainnya_pp());
        cv.put("EDIT_D_LAINNYA_PP", me.getPemegangPolisModel().getEdit_d_lainnya_pp());
        cv.put("T_PROTEKSI_PP", me.getPemegangPolisModel().getT_proteksi_pp());
        cv.put("T_INVES_PP", me.getPemegangPolisModel().getT_inves_pp());
        cv.put("T_LAINNYA_PP", me.getPemegangPolisModel().getT_lainnya_pp());
        cv.put("EDIT_T_LAINNYA", me.getPemegangPolisModel().getEdit_t_lainnya());
        cv.put("PROPINSI_PP", me.getPemegangPolisModel().getPropinsi_pp());
        cv.put("PROPINSI_KANTOR_PP", me.getPemegangPolisModel().getPropinsi_kantor_pp());
        cv.put("PROPINSI_TINGGAL_PP", me.getPemegangPolisModel().getPropinsi_tinggal_pp());
        cv.put("KABUPATEN_PP", me.getPemegangPolisModel().getKabupaten_pp());
        cv.put("KABUPATEN_KANTOR_PP", me.getPemegangPolisModel().getKabupaten_kantor_pp());
        cv.put("KABUPATEN_TINGGAL_PP", me.getPemegangPolisModel().getKabupaten_tinggal_pp());
        cv.put("KECAMATAN_PP", me.getPemegangPolisModel().getKecamatan_pp());
        cv.put("KECAMATAN_KANTOR_PP", me.getPemegangPolisModel().getKecamatan_kantor_pp());
        cv.put("KECAMATAN_TINGGAL_PP", me.getPemegangPolisModel().getKecamatan_tinggal_pp());
        cv.put("KELURAHAN_PP", me.getPemegangPolisModel().getKelurahan_pp());
        cv.put("KELURAHAN_KANTOR_PP", me.getPemegangPolisModel().getKelurahan_kantor_pp());
        cv.put("KELURAHAN_TINGGAL_PP", me.getPemegangPolisModel().getKelurahan_tinggal_pp());
        cv.put("VALIDATION", me.getPemegangPolisModel().getValidation());

        return cv;
    }

    public PemegangPolisModel getObject(Cursor cursor){
        PemegangPolisModel pemegangPolisModel = new PemegangPolisModel();
        if (cursor != null && cursor.getCount()>0){
            cursor.moveToFirst();
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NAMA_PP"))){
                pemegangPolisModel.setNama_pp(cursor.getString(cursor.getColumnIndexOrThrow("NAMA_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("GELAR_PP"))){
                pemegangPolisModel.setGelar_pp(cursor.getString(cursor.getColumnIndexOrThrow("GELAR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NAMA_IBU_PP"))){
                pemegangPolisModel.setNama_ibu_pp(cursor.getString(cursor.getColumnIndexOrThrow("NAMA_IBU_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("BUKTI_IDENTITAS_PP"))){
                pemegangPolisModel.setBukti_identitas_pp(cursor.getInt(cursor.getColumnIndexOrThrow("BUKTI_IDENTITAS_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("BUKTI_IDENTITAS_LAIN_PP"))){
                pemegangPolisModel.setBukti_identitas_lain_pp(cursor.getString(cursor.getColumnIndexOrThrow("BUKTI_IDENTITAS_LAIN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NO_IDENTITAS_PP"))){
                pemegangPolisModel.setNo_identitas_pp(cursor.getString(cursor.getColumnIndexOrThrow("NO_IDENTITAS_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TGL_BERLAKU_PP"))){
                pemegangPolisModel.setTgl_berlaku_pp(cursor.getString(cursor.getColumnIndexOrThrow("TGL_BERLAKU_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("WARGA_NEGARA_PP"))){
                pemegangPolisModel.setWarga_negara_pp(cursor.getInt(cursor.getColumnIndexOrThrow("WARGA_NEGARA_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TEMPAT_PP"))){
                pemegangPolisModel.setTempat_pp(cursor.getString(cursor.getColumnIndexOrThrow("TEMPAT_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TTL_PP"))){
                pemegangPolisModel.setTtl_pp(cursor.getString(cursor.getColumnIndexOrThrow("TTL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("USIA_PP"))){
                pemegangPolisModel.setUsia_pp(cursor.getString(cursor.getColumnIndexOrThrow("USIA_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("JEKEL_PP"))){
                pemegangPolisModel.setJekel_pp(cursor.getInt(cursor.getColumnIndexOrThrow("JEKEL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("STATUS_PP"))){
                pemegangPolisModel.setStatus_pp(cursor.getInt(cursor.getColumnIndexOrThrow("STATUS_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("AGAMA_PP"))){
                pemegangPolisModel.setAgama_pp(cursor.getInt(cursor.getColumnIndexOrThrow("AGAMA_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("AGAMA_LAIN_PP"))){
                pemegangPolisModel.setAgama_lain_pp(cursor.getString(cursor.getColumnIndexOrThrow("AGAMA_LAIN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PENDIDIKAN_PP"))){
                pemegangPolisModel.setPendidikan_pp(cursor.getInt(cursor.getColumnIndexOrThrow("PENDIDIKAN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("ALAMAT_PP"))){
                pemegangPolisModel.setAlamat_pp(cursor.getString(cursor.getColumnIndexOrThrow("ALAMAT_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KOTA_PP"))){
                pemegangPolisModel.setKota_pp(cursor.getString(cursor.getColumnIndexOrThrow("KOTA_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KDPOS_PP"))){
                pemegangPolisModel.setKdpos_pp(cursor.getString(cursor.getColumnIndexOrThrow("KDPOS_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TELP1_PP"))){
                pemegangPolisModel.setTelp1_pp(cursor.getString(cursor.getColumnIndexOrThrow("TELP1_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KDTELP1_PP"))){
                pemegangPolisModel.setKdtelp1_pp(cursor.getString(cursor.getColumnIndexOrThrow("KDTELP1_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TELP2_PP"))){
                pemegangPolisModel.setTelp2_pp(cursor.getString(cursor.getColumnIndexOrThrow("TELP2_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KDTELP2_PP"))){
                pemegangPolisModel.setKdtelp2_pp(cursor.getString(cursor.getColumnIndexOrThrow("KDTELP2_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("ALAMAT_KANTOR_PP"))){
                pemegangPolisModel.setAlamat_kantor_pp(cursor.getString(cursor.getColumnIndexOrThrow("ALAMAT_KANTOR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KOTA_KANTOR_PP"))){
                pemegangPolisModel.setKota_kantor_pp(cursor.getString(cursor.getColumnIndexOrThrow("KOTA_KANTOR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KDPOS_KANTOR_PP"))){
                pemegangPolisModel.setKdpos_kantor_pp(cursor.getString(cursor.getColumnIndexOrThrow("KDPOS_KANTOR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KDTELP1_KANTOR_PP"))){
                pemegangPolisModel.setKdtelp1_kantor_pp(cursor.getString(cursor.getColumnIndexOrThrow("KDTELP1_KANTOR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TELP1_KANTOR_PP"))){
                pemegangPolisModel.setTelp1_kantor_pp(cursor.getString(cursor.getColumnIndexOrThrow("TELP1_KANTOR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KDTELP2_KANTOR_PP"))){
                pemegangPolisModel.setKdtelp2_kantor_pp(cursor.getString(cursor.getColumnIndexOrThrow("KDTELP2_KANTOR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TELP2_KANTOR_PP"))){
                pemegangPolisModel.setTelp2_kantor_pp(cursor.getString(cursor.getColumnIndexOrThrow("TELP2_KANTOR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KDFAX_KANTOR_PP"))){
                pemegangPolisModel.setKdfax_kantor_pp(cursor.getString(cursor.getColumnIndexOrThrow("KDFAX_KANTOR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("FAX_KANTOR_PP"))){
                pemegangPolisModel.setFax_kantor_pp(cursor.getString(cursor.getColumnIndexOrThrow("FAX_KANTOR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("ALAMAT_TINGGAL_PP"))){
                pemegangPolisModel.setAlamat_tinggal_pp(cursor.getString(cursor.getColumnIndexOrThrow("ALAMAT_TINGGAL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KOTA_TINGGAL_PP"))){
                pemegangPolisModel.setKota_tinggal_pp(cursor.getString(cursor.getColumnIndexOrThrow("KOTA_TINGGAL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KDPOS_TINGGAL_PP"))){
                pemegangPolisModel.setKdpos_tinggal_pp(cursor.getString(cursor.getColumnIndexOrThrow("KDPOS_TINGGAL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KDTELP1_TINGGAL_PP"))){
                pemegangPolisModel.setKdtelp1_tinggal_pp(cursor.getString(cursor.getColumnIndexOrThrow("KDTELP1_TINGGAL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TELP1_TINGGAL_PP"))){
                pemegangPolisModel.setTelp1_tinggal_pp(cursor.getString(cursor.getColumnIndexOrThrow("TELP1_TINGGAL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KDTELP2_TINGGAL_PP"))){
                pemegangPolisModel.setKdtelp2_tinggal_pp(cursor.getString(cursor.getColumnIndexOrThrow("KDTELP2_TINGGAL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TELP2_TINGGAL_PP"))){
                pemegangPolisModel.setTelp2_tinggal_pp(cursor.getString(cursor.getColumnIndexOrThrow("TELP2_TINGGAL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KDFAX_TINGGAL_PP"))){
                pemegangPolisModel.setKdfax_tinggal_pp(cursor.getString(cursor.getColumnIndexOrThrow("KDFAX_TINGGAL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("FAX_TINGGAL_PP"))){
                pemegangPolisModel.setFax_tinggal_pp(cursor.getString(cursor.getColumnIndexOrThrow("FAX_TINGGAL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TAGIHAN_PP"))){
                pemegangPolisModel.setTagihan_pp(cursor.getInt(cursor.getColumnIndexOrThrow("TAGIHAN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("HP1_PP"))){
                pemegangPolisModel.setHp1_pp(cursor.getString(cursor.getColumnIndexOrThrow("HP1_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("HP2_PP"))){
                pemegangPolisModel.setHp2_pp(cursor.getString(cursor.getColumnIndexOrThrow("HP2_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("EMAIL_PP"))){
                pemegangPolisModel.setEmail_pp(cursor.getString(cursor.getColumnIndexOrThrow("EMAIL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KLASIFIKASI_PEKERJAAN_PP"))){
                pemegangPolisModel.setKlasifikasi_pekerjaan_pp(cursor.getString(cursor.getColumnIndexOrThrow("KLASIFIKASI_PEKERJAAN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("URAIAN_PEKERJAAN_PP"))){
                pemegangPolisModel.setUraian_pekerjaan_pp(cursor.getString(cursor.getColumnIndexOrThrow("URAIAN_PEKERJAAN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("JABATAN_KLASIFIKASI_PP"))){
                pemegangPolisModel.setJabatan_klasifikasi_pp(cursor.getString(cursor.getColumnIndexOrThrow("JABATAN_KLASIFIKASI_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("HUBUNGAN_PP"))){
                pemegangPolisModel.setHubungan_pp(cursor.getInt(cursor.getColumnIndexOrThrow("HUBUNGAN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("GREENCARD_PP"))){
                pemegangPolisModel.setGreencard_pp(cursor.getInt(cursor.getColumnIndexOrThrow("GREENCARD_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("ALIAS_PP"))){
                pemegangPolisModel.setAlias_pp(cursor.getString(cursor.getColumnIndexOrThrow("ALIAS_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NM_PERUSAHAAN_PP"))){
                pemegangPolisModel.setNm_perusahaan_pp(cursor.getString(cursor.getColumnIndexOrThrow("NM_PERUSAHAAN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NPWP_PP"))){
                pemegangPolisModel.setNpwp_pp(cursor.getString(cursor.getColumnIndexOrThrow("NPWP_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TAGIHAN_RUTIN_PP"))){
                pemegangPolisModel.setTagihan_rutin_pp(cursor.getString(cursor.getColumnIndexOrThrow("TAGIHAN_RUTIN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KRM_POLIS_PP"))){
                pemegangPolisModel.setKrm_polis_pp(cursor.getString(cursor.getColumnIndexOrThrow("KRM_POLIS_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("SUAMIRT_PP"))){
                pemegangPolisModel.setSuamirt_pp(cursor.getString(cursor.getColumnIndexOrThrow("SUAMIRT_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TTLSUAMI_RT_PP"))){
                pemegangPolisModel.setTtlsuami_rt_pp(cursor.getString(cursor.getColumnIndexOrThrow("TTLSUAMI_RT_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("USIASUAMI_RT_PP"))){
                pemegangPolisModel.setUsiasuami_rt_pp(cursor.getString(cursor.getColumnIndexOrThrow("USIASUAMI_RT_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PEKERJAANSUAMI_RT_PP"))){
                pemegangPolisModel.setPekerjaansuami_rt_pp(cursor.getString(cursor.getColumnIndexOrThrow("PEKERJAANSUAMI_RT_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("JABATANSUAMI_RT_PP"))){
                pemegangPolisModel.setJabatansuami_rt_pp(cursor.getString(cursor.getColumnIndexOrThrow("JABATANSUAMI_RT_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PERUSAHAANSUAMI_RT_PP"))){
                pemegangPolisModel.setPerusahaansuami_rt_pp(cursor.getString(cursor.getColumnIndexOrThrow("PERUSAHAANSUAMI_RT_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("BIDUSAHA_SUAMIRT_PP"))){
                pemegangPolisModel.setBidusaha_suamirt_pp(cursor.getString(cursor.getColumnIndexOrThrow("BIDUSAHA_SUAMIRT_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NPWP_SUAMIRT_PP"))){
                pemegangPolisModel.setNpwp_suamirt_pp(cursor.getString(cursor.getColumnIndexOrThrow("NPWP_SUAMIRT_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PENGHASILAN_SUAMITHN_PP"))){
                pemegangPolisModel.setPenghasilan_suamithn_pp(cursor.getString(cursor.getColumnIndexOrThrow("PENGHASILAN_SUAMITHN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("AYAH_PP"))){
                pemegangPolisModel.setAyah_pp(cursor.getString(cursor.getColumnIndexOrThrow("AYAH_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TTL_AYAH_PP"))){
                pemegangPolisModel.setTtl_ayah_pp(cursor.getString(cursor.getColumnIndexOrThrow("TTL_AYAH_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("USIA_AYAH_PP"))){
                pemegangPolisModel.setUsia_ayah_pp(cursor.getString(cursor.getColumnIndexOrThrow("USIA_AYAH_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PEKERJAAN_AYAH_PP"))){
                pemegangPolisModel.setPekerjaan_ayah_pp(cursor.getString(cursor.getColumnIndexOrThrow("PEKERJAAN_AYAH_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("JABATAN_AYAH_PP"))){
                pemegangPolisModel.setJabatan_ayah_pp(cursor.getString(cursor.getColumnIndexOrThrow("JABATAN_AYAH_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PERUSAHAAN_AYAH_PP"))){
                pemegangPolisModel.setPerusahaan_ayah_pp(cursor.getString(cursor.getColumnIndexOrThrow("PERUSAHAAN_AYAH_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("BIDUSAHA_AYAH_PP"))){
                pemegangPolisModel.setBidusaha_ayah_pp(cursor.getString(cursor.getColumnIndexOrThrow("BIDUSAHA_AYAH_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NPWP_AYAH_PP"))){
                pemegangPolisModel.setNpwp_ayah_pp(cursor.getString(cursor.getColumnIndexOrThrow("NPWP_AYAH_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PENGHASILAN_AYAH_PP"))){
                pemegangPolisModel.setPenghasilan_ayah_pp(cursor.getString(cursor.getColumnIndexOrThrow("PENGHASILAN_AYAH_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NM_IBU_PP"))){
                pemegangPolisModel.setNm_ibu_pp(cursor.getString(cursor.getColumnIndexOrThrow("NM_IBU_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("TTL_IBU_PP"))){
                pemegangPolisModel.setTtl_ibu_pp(cursor.getString(cursor.getColumnIndexOrThrow("TTL_IBU_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("USIA_IBU_PP"))){
                pemegangPolisModel.setUsia_ibu_pp(cursor.getString(cursor.getColumnIndexOrThrow("USIA_IBU_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PEKERJAAN_IBU_PP"))){
                pemegangPolisModel.setPekerjaan_ibu_pp(cursor.getString(cursor.getColumnIndexOrThrow("PEKERJAAN_IBU_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("JABATAN_IBU_PP"))){
                pemegangPolisModel.setJabatan_ibu_pp(cursor.getString(cursor.getColumnIndexOrThrow("JABATAN_IBU_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PERUSAHAAN_IBU_PP"))){
                pemegangPolisModel.setPerusahaan_ibu_pp(cursor.getString(cursor.getColumnIndexOrThrow("PERUSAHAAN_IBU_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("BIDUSAHA_IBU_PP"))){
                pemegangPolisModel.setBidusaha_ibu_pp(cursor.getString(cursor.getColumnIndexOrThrow("BIDUSAHA_IBU_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NPWP_IBU_PP"))){
                pemegangPolisModel.setNpwp_ibu_pp(cursor.getString(cursor.getColumnIndexOrThrow("NPWP_IBU_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PENGHASILAN_IBU_PP"))){
                pemegangPolisModel.setPenghasilan_ibu_pp(cursor.getString(cursor.getColumnIndexOrThrow("PENGHASILAN_IBU_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PENDAPATAN_PP"))){
                pemegangPolisModel.setPendapatan_pp(cursor.getString(cursor.getColumnIndexOrThrow("PENDAPATAN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("HUBUNGANCP_PP"))){
                pemegangPolisModel.setHubungancp_pp(cursor.getInt(cursor.getColumnIndexOrThrow("HUBUNGANCP_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("NO_CIFF_PP"))){
                pemegangPolisModel.setNo_ciff_pp(cursor.getString(cursor.getColumnIndexOrThrow("NO_CIFF_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("D_GAJI_PP"))){
                pemegangPolisModel.setD_gaji_pp(cursor.getString(cursor.getColumnIndexOrThrow("D_GAJI_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("D_TABUNGAN_PP"))){
                pemegangPolisModel.setD_tabungan_pp(cursor.getString(cursor.getColumnIndexOrThrow("D_TABUNGAN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("D_WARISAN_PP"))){
                pemegangPolisModel.setD_warisan_pp(cursor.getString(cursor.getColumnIndexOrThrow("D_WARISAN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("D_HIBAH_PP"))){
                pemegangPolisModel.setD_hibah_pp(cursor.getString(cursor.getColumnIndexOrThrow("D_HIBAH_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("D_LAINNYA_PP"))){
                pemegangPolisModel.setD_lainnya_pp(cursor.getString(cursor.getColumnIndexOrThrow("D_LAINNYA_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("EDIT_D_LAINNYA_PP"))){
                pemegangPolisModel.setEdit_d_lainnya_pp(cursor.getString(cursor.getColumnIndexOrThrow("EDIT_D_LAINNYA_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("T_PROTEKSI_PP"))){
                pemegangPolisModel.setT_proteksi_pp(cursor.getString(cursor.getColumnIndexOrThrow("T_PROTEKSI_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("T_INVES_PP"))){
                pemegangPolisModel.setT_inves_pp(cursor.getString(cursor.getColumnIndexOrThrow("T_INVES_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("T_LAINNYA_PP"))){
                pemegangPolisModel.setT_lainnya_pp(cursor.getString(cursor.getColumnIndexOrThrow("T_LAINNYA_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("EDIT_T_LAINNYA"))){
                pemegangPolisModel.setEdit_t_lainnya(cursor.getString(cursor.getColumnIndexOrThrow("EDIT_T_LAINNYA")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PROPINSI_PP"))){
                pemegangPolisModel.setPropinsi_pp(cursor.getString(cursor.getColumnIndexOrThrow("PROPINSI_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PROPINSI_KANTOR_PP"))){
                pemegangPolisModel.setPropinsi_kantor_pp(cursor.getString(cursor.getColumnIndexOrThrow("PROPINSI_KANTOR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("PROPINSI_TINGGAL_PP"))){
                pemegangPolisModel.setPropinsi_tinggal_pp(cursor.getString(cursor.getColumnIndexOrThrow("PROPINSI_TINGGAL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KABUPATEN_PP"))){
                pemegangPolisModel.setKabupaten_pp(cursor.getString(cursor.getColumnIndexOrThrow("KABUPATEN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KABUPATEN_KANTOR_PP"))){
                pemegangPolisModel.setKabupaten_kantor_pp(cursor.getString(cursor.getColumnIndexOrThrow("KABUPATEN_KANTOR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KABUPATEN_TINGGAL_PP"))){
                pemegangPolisModel.setKabupaten_tinggal_pp(cursor.getString(cursor.getColumnIndexOrThrow("KABUPATEN_TINGGAL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KECAMATAN_PP"))){
                pemegangPolisModel.setKecamatan_pp(cursor.getString(cursor.getColumnIndexOrThrow("KECAMATAN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KECAMATAN_KANTOR_PP"))){
                pemegangPolisModel.setKecamatan_kantor_pp(cursor.getString(cursor.getColumnIndexOrThrow("KECAMATAN_KANTOR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KECAMATAN_TINGGAL_PP"))){
                pemegangPolisModel.setKecamatan_tinggal_pp(cursor.getString(cursor.getColumnIndexOrThrow("KECAMATAN_TINGGAL_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KELURAHAN_PP"))){
                pemegangPolisModel.setKelurahan_pp(cursor.getString(cursor.getColumnIndexOrThrow("KELURAHAN_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KELURAHAN_KANTOR_PP"))){
                pemegangPolisModel.setKelurahan_kantor_pp(cursor.getString(cursor.getColumnIndexOrThrow("KELURAHAN_KANTOR_PP")));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow("KELURAHAN_TINGGAL_PP"))){
                pemegangPolisModel.setKelurahan_tinggal_pp(cursor.getString(cursor.getColumnIndexOrThrow("KELURAHAN_TINGGAL_PP")));
            }
            if(!cursor.isNull(cursor.getColumnIndexOrThrow("VALIDATION"))){
                int valid = cursor.getInt(cursor.getColumnIndexOrThrow("VALIDATION"));
//                boolean isValid = valid == 1 ? true : false;
                boolean val;
                val = valid == 1;
                pemegangPolisModel.setValidation(val);
            }
            cursor.close();
        }
        return pemegangPolisModel;
    }
}
