
package id.co.ajsmsig.nb.leader.statusspaj;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpajData {

    @SerializedName("lus_full_name")
    @Expose
    private String lusFullName;
    @SerializedName("lde_dept")
    @Expose
    private String ldeDept;
    @SerializedName("reg_spaj")
    @Expose
    private String regSpaj;
    @SerializedName("pp_name")
    @Expose
    private String ppName;
    @SerializedName("lsdbs_name")
    @Expose
    private String lsdbsName;
    @SerializedName("msps_desc")
    @Expose
    private String mspsDesc;
    @SerializedName("msps_date")
    @Expose
    private String mspsDate;
    @SerializedName("no_temp")
    @Expose
    private String noTemp;

    /**
     * No args constructor for use in serialization
     * 
     */
    public SpajData() {
    }

    /**
     * 
     * @param noTemp
     * @param lusFullName
     * @param lsdbsName
     * @param regSpaj
     * @param mspsDate
     * @param ppName
     * @param ldeDept
     * @param mspsDesc
     */
    public SpajData(String lusFullName, String ldeDept, String regSpaj, String ppName, String lsdbsName, String mspsDesc, String mspsDate, String noTemp) {
        super();
        this.lusFullName = lusFullName;
        this.ldeDept = ldeDept;
        this.regSpaj = regSpaj;
        this.ppName = ppName;
        this.lsdbsName = lsdbsName;
        this.mspsDesc = mspsDesc;
        this.mspsDate = mspsDate;
        this.noTemp = noTemp;
    }

    public String getLusFullName() {
        return lusFullName;
    }

    public void setLusFullName(String lusFullName) {
        this.lusFullName = lusFullName;
    }

    public String getLdeDept() {
        return ldeDept;
    }

    public void setLdeDept(String ldeDept) {
        this.ldeDept = ldeDept;
    }

    public String getRegSpaj() {
        return regSpaj;
    }

    public void setRegSpaj(String regSpaj) {
        this.regSpaj = regSpaj;
    }

    public String getPpName() {
        return ppName;
    }

    public void setPpName(String ppName) {
        this.ppName = ppName;
    }

    public String getLsdbsName() {
        return lsdbsName;
    }

    public void setLsdbsName(String lsdbsName) {
        this.lsdbsName = lsdbsName;
    }

    public String getMspsDesc() {
        return mspsDesc;
    }

    public void setMspsDesc(String mspsDesc) {
        this.mspsDesc = mspsDesc;
    }

    public String getMspsDate() {
        return mspsDate;
    }

    public void setMspsDate(String mspsDate) {
        this.mspsDate = mspsDate;
    }

    public String getNoTemp() {
        return noTemp;
    }

    public void setNoTemp(String noTemp) {
        this.noTemp = noTemp;
    }

}
