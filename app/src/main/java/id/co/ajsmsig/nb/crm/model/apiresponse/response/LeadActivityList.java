
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadActivityList {
    @SerializedName("SL_NAME")
    @Expose
    private String SL_NAME;
    @SerializedName("SL_ID")
    @Expose
    private String SL_ID;
    @SerializedName("SL_TAB_ID")
    @Expose
    private String SL_TAB_ID;
    @SerializedName("SLA_ACTIVE")
    @Expose
    private String sLAACTIVE;
    @SerializedName("SLA_CORP_COMMEN")
    @Expose
    private String sLACORPCOMMEN;
    @SerializedName("SLA_CORP_STAGE")
    @Expose
    private String sLACORPSTAGE;
    @SerializedName("SLA_CRTD_DATE")
    @Expose
    private String sLACRTDDATE;
    @SerializedName("SLA_CRTD_ID")
    @Expose
    private String sLACRTDID;
    @SerializedName("SLA_CURR_LVL")
    @Expose
    private String sLACURRLVL;
    @SerializedName("SLA_DETAIL")
    @Expose
    private String sLADETAIL;
    @SerializedName("SLA_EDATE")
    @Expose
    private String sLAEDATE;
    @SerializedName("SLA_ID")
    @Expose
    private String sLAID;
    @SerializedName("SLA_IDATE")
    @Expose
    private String sLAIDATE;
    @SerializedName("SLA_INST_ID")
    @Expose
    private String sLAINSTID;
    @SerializedName("SLA_INST_TYPE")
    @Expose
    private String sLAINSTTYPE;
    @SerializedName("SLA_LAST_POS")
    @Expose
    private String sLALASTPOS;
    @SerializedName("SLA_LAT")
    @Expose
    private String sLALAT;
    @SerializedName("SLA_LON")
    @Expose
    private String sLALON;
    @SerializedName("SLA_NAME")
    @Expose
    private String sLANAME;
    @SerializedName("SLA_NEXT_ID")
    @Expose
    private String sLANEXTID;
    @SerializedName("SLA_NXT_ACTION")
    @Expose
    private String sLANXTACTION;
    @SerializedName("SLA_OWNER_ID")
    @Expose
    private String sLAOWNERID;
    @SerializedName("SLA_OWNER_TYPE")
    @Expose
    private String sLAOWNERTYPE;
    @SerializedName("SLA_PREMI")
    @Expose
    private String sLAPREMI;
    @SerializedName("SLA_PREMI2")
    @Expose
    private String sLAPREMI2;
    @SerializedName("SLA_PREMI3")
    @Expose
    private String sLAPREMI3;
    @SerializedName("SLA_REFF_ID")
    @Expose
    private String sLAREFFID;
    @SerializedName("SLA_REMINDER_DATE")
    @Expose
    private String sLAREMINDERDATE;
    @SerializedName("SLA_REMINDER_TYPE")
    @Expose
    private String sLAREMINDERTYPE;
    @SerializedName("SLA_RPT_CAT")
    @Expose
    private String sLARPTCAT;
    @SerializedName("SLA_SDATE")
    @Expose
    private String sLASDATE;
    @SerializedName("SLA_STATUS")
    @Expose
    private String sLASTATUS;
    @SerializedName("SLA_SUBTYPE")
    @Expose
    private String sLASUBTYPE;
    @SerializedName("SLA_TYPE")
    @Expose
    private String sLATYPE;
    @SerializedName("SLA_UPDTD_DATE")
    @Expose
    private String sLAUPDTDDATE;
    @SerializedName("SLA_UPDTD_ID")
    @Expose
    private String sLAUPDTDID;
    @SerializedName("SLA_USER_ID")
    @Expose
    private String sLAUSERID;
    @SerializedName("SLA_CURR_BRANCH")
    @Expose
    private String sLACURRBRANCH;
    @SerializedName("NAMA_CABANG")
    @Expose
    private String nAMACABANG;

    public String getSLAACTIVE() {
        return sLAACTIVE;
    }

    public void setSLAACTIVE(String sLAACTIVE) {
        this.sLAACTIVE = sLAACTIVE;
    }

    public String getSLACORPCOMMEN() {
        return sLACORPCOMMEN;
    }

    public void setSLACORPCOMMEN(String sLACORPCOMMEN) {
        this.sLACORPCOMMEN = sLACORPCOMMEN;
    }

    public String getSLACORPSTAGE() {
        return sLACORPSTAGE;
    }

    public void setSLACORPSTAGE(String sLACORPSTAGE) {
        this.sLACORPSTAGE = sLACORPSTAGE;
    }

    public String getSLACRTDDATE() {
        return sLACRTDDATE;
    }

    public void setSLACRTDDATE(String sLACRTDDATE) {
        this.sLACRTDDATE = sLACRTDDATE;
    }

    public String getSLACRTDID() {
        return sLACRTDID;
    }

    public void setSLACRTDID(String sLACRTDID) {
        this.sLACRTDID = sLACRTDID;
    }

    public String getSLACURRLVL() {
        return sLACURRLVL;
    }

    public void setSLACURRLVL(String sLACURRLVL) {
        this.sLACURRLVL = sLACURRLVL;
    }

    public String getSLADETAIL() {
        return sLADETAIL;
    }

    public void setSLADETAIL(String sLADETAIL) {
        this.sLADETAIL = sLADETAIL;
    }

    public String getSLAEDATE() {
        return sLAEDATE;
    }

    public void setSLAEDATE(String sLAEDATE) {
        this.sLAEDATE = sLAEDATE;
    }

    public String getSLAID() {
        return sLAID;
    }

    public void setSLAID(String sLAID) {
        this.sLAID = sLAID;
    }

    public String getSLAIDATE() {
        return sLAIDATE;
    }

    public void setSLAIDATE(String sLAIDATE) {
        this.sLAIDATE = sLAIDATE;
    }

    public String getSLAINSTID() {
        return sLAINSTID;
    }

    public void setSLAINSTID(String sLAINSTID) {
        this.sLAINSTID = sLAINSTID;
    }

    public String getSLAINSTTYPE() {
        return sLAINSTTYPE;
    }

    public void setSLAINSTTYPE(String sLAINSTTYPE) {
        this.sLAINSTTYPE = sLAINSTTYPE;
    }

    public String getSLALASTPOS() {
        return sLALASTPOS;
    }

    public void setSLALASTPOS(String sLALASTPOS) {
        this.sLALASTPOS = sLALASTPOS;
    }

    public String getSLALAT() {
        return sLALAT;
    }

    public void setSLALAT(String sLALAT) {
        this.sLALAT = sLALAT;
    }

    public String getSLALON() {
        return sLALON;
    }

    public void setSLALON(String sLALON) {
        this.sLALON = sLALON;
    }

    public String getSLANAME() {
        return sLANAME;
    }

    public void setSLANAME(String sLANAME) {
        this.sLANAME = sLANAME;
    }

    public String getSLANEXTID() {
        return sLANEXTID;
    }

    public void setSLANEXTID(String sLANEXTID) {
        this.sLANEXTID = sLANEXTID;
    }

    public String getSLANXTACTION() {
        return sLANXTACTION;
    }

    public void setSLANXTACTION(String sLANXTACTION) {
        this.sLANXTACTION = sLANXTACTION;
    }

    public String getSLAOWNERID() {
        return sLAOWNERID;
    }

    public void setSLAOWNERID(String sLAOWNERID) {
        this.sLAOWNERID = sLAOWNERID;
    }

    public String getSLAOWNERTYPE() {
        return sLAOWNERTYPE;
    }

    public void setSLAOWNERTYPE(String sLAOWNERTYPE) {
        this.sLAOWNERTYPE = sLAOWNERTYPE;
    }

    public String getSLAPREMI() {
        return sLAPREMI;
    }

    public void setSLAPREMI(String sLAPREMI) {
        this.sLAPREMI = sLAPREMI;
    }

    public String getSLAPREMI2() {
        return sLAPREMI2;
    }

    public void setSLAPREMI2(String sLAPREMI2) {
        this.sLAPREMI2 = sLAPREMI2;
    }

    public String getSLAPREMI3() {
        return sLAPREMI3;
    }

    public void setSLAPREMI3(String sLAPREMI3) {
        this.sLAPREMI3 = sLAPREMI3;
    }

    public String getSLAREFFID() {
        return sLAREFFID;
    }

    public void setSLAREFFID(String sLAREFFID) {
        this.sLAREFFID = sLAREFFID;
    }

    public String getSLAREMINDERDATE() {
        return sLAREMINDERDATE;
    }

    public void setSLAREMINDERDATE(String sLAREMINDERDATE) {
        this.sLAREMINDERDATE = sLAREMINDERDATE;
    }

    public String getSLAREMINDERTYPE() {
        return sLAREMINDERTYPE;
    }

    public void setSLAREMINDERTYPE(String sLAREMINDERTYPE) {
        this.sLAREMINDERTYPE = sLAREMINDERTYPE;
    }

    public String getSLARPTCAT() {
        return sLARPTCAT;
    }

    public void setSLARPTCAT(String sLARPTCAT) {
        this.sLARPTCAT = sLARPTCAT;
    }

    public String getSLASDATE() {
        return sLASDATE;
    }

    public void setSLASDATE(String sLASDATE) {
        this.sLASDATE = sLASDATE;
    }

    public String getSLASTATUS() {
        return sLASTATUS;
    }

    public void setSLASTATUS(String sLASTATUS) {
        this.sLASTATUS = sLASTATUS;
    }

    public String getSLASUBTYPE() {
        return sLASUBTYPE;
    }

    public void setSLASUBTYPE(String sLASUBTYPE) {
        this.sLASUBTYPE = sLASUBTYPE;
    }

    public String getSLATYPE() {
        return sLATYPE;
    }

    public void setSLATYPE(String sLATYPE) {
        this.sLATYPE = sLATYPE;
    }

    public String getSLAUPDTDDATE() {
        return sLAUPDTDDATE;
    }

    public void setSLAUPDTDDATE(String sLAUPDTDDATE) {
        this.sLAUPDTDDATE = sLAUPDTDDATE;
    }

    public String getSLAUPDTDID() {
        return sLAUPDTDID;
    }

    public void setSLAUPDTDID(String sLAUPDTDID) {
        this.sLAUPDTDID = sLAUPDTDID;
    }

    public String getSLAUSERID() {
        return sLAUSERID;
    }

    public void setSLAUSERID(String sLAUSERID) {
        this.sLAUSERID = sLAUSERID;
    }

    public String getSLACURRBRANCH() {
        return sLACURRBRANCH;
    }

    public void setSLACURRBRANCH(String sLACURRBRANCH) {
        this.sLACURRBRANCH = sLACURRBRANCH;
    }

    public String getNAMACABANG() {
        return nAMACABANG;
    }

    public void setNAMACABANG(String nAMACABANG) {
        this.nAMACABANG = nAMACABANG;
    }
    public String getSL_NAME() {
        return SL_NAME;
    }

    public void setSL_NAME(String SL_NAME) {
        this.SL_NAME = SL_NAME;
    }

    public String getSL_ID() {
        return SL_ID;
    }

    public void setSL_ID(String SL_ID) {
        this.SL_ID = SL_ID;
    }

    public String getSL_TAB_ID() {
        return SL_TAB_ID;
    }

}
