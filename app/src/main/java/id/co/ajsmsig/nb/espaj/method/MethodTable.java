package id.co.ajsmsig.nb.espaj.method;

/**
 * Created by eriza on 04/09/2017.
 */

public class MethodTable {

    public static String selectQueryJenis(int jns_login, int jns_login_bc, String KODE_REG1, String KODE_REG2) {
        String selectQuery = "SELECT LSTB_ID, LSTP_PRODUK FROM E_LST_CHANNEL_DIST";
        switch (jns_login) {
            case 2://agency
                selectQuery = "SELECT LSTB_ID,LSTP_PRODUK FROM E_LST_CHANNEL_DIST WHERE LSTB_ID = 1";
                break;

            case 11://fcd
                selectQuery = "SELECT LSTB_ID, LSTP_PRODUK FROM E_LST_CHANNEL_DIST WHERE LSTB_ID = 16";
                break;

            case 9://bancass
                switch (jns_login_bc) {
                    case 2://bank sinarmas
                        selectQuery = "SELECT LSTB_ID, LSTP_PRODUK FROM E_LST_CHANNEL_DIST WHERE LSTB_ID = 14";
                        break;

                    case 44://bjb
                        selectQuery = "SELECT LSTB_ID, LSTP_PRODUK FROM E_LST_CHANNEL_DIST WHERE LSTB_ID = 19";
                        break;

                    case 50://bukopin
                        selectQuery = "SELECT LSTB_ID, LSTP_PRODUK FROM E_LST_CHANNEL_DIST WHERE LSTB_ID = 23";
                        break;

                    case 51://jatim
                        selectQuery = "SELECT LSTB_ID, LSTP_PRODUK FROM E_LST_CHANNEL_DIST WHERE LSTB_ID = 22";
                        break;
//
                    case 56://BTN SYARIAH
                        selectQuery = "SELECT LSTB_ID, LSTP_PRODUK FROM E_LST_CHANNEL_DIST WHERE LSTB_ID = 24";
                        break;
                    case 58 : //JATIM SYARIAH
                        selectQuery = "SELECT LSTB_ID, LSTP_PRODUK FROM E_LST_CHANNEL_DIST WHERE LSTB_ID = 27";
                        break;
                    case 60 : // Bank Bukopin Syariah
                        selectQuery = "SELECT LSTB_ID, LSTP_PRODUK FROM E_LST_CHANNEL_DIST WHERE LSTB_ID = 28";
                        break;

                }
                break;
        }
        return selectQuery;
    }
}
