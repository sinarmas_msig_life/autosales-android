package id.co.ajsmsig.nb.espaj.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.method.ProgressDialogClass;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelMst_Answer;
import id.co.ajsmsig.nb.util.Const;

/**
 * Created by Bernard on 25/08/2017.
 */

public class E_UpdateQuisionerSIOFragment extends Fragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, View.OnFocusChangeListener {


    View kuisioner;
    ArrayList<ModelMst_Answer> list;
    EspajModel me;
    ArrayList<View> ListOfView;

    int int_y_id81_1, int_n_id81_1, int_y_id81_2, int_n_id81_2, int_y_id82_1, int_n_id82_1, int_y_id82_2, int_n_id82_2, int_y_id84_1,
            int_n_id84_1, int_y_id84_2, int_n_id84_2, int_y_id85_1, int_n_id85_1, int_y_id85_2, int_n_id85_2, int_y_id86_1, int_n_id86_1,
            int_y_id86_2, int_n_id86_2, int_y_id87_1, int_n_id87_1, int_y_id87_2, int_n_id87_2, int_y_id88_1, int_n_id88_1, int_y_id88_2,
            int_n_id88_2, int_y_id89_1, int_n_id89_1, int_y_id89_2, int_n_id89_2, int_y_id90_1, int_n_id90_1, int_y_id90_2, int_n_id90_2,
            int_y_id91_1, int_n_id91_1, int_y_id91_2, int_n_id91_2, int_y_id92_1, int_n_id92_1, int_y_id92_2, int_n_id92_2, int_y_id93_1,
            int_n_id93_1, int_y_id93_2, int_n_id93_2, int_y_id94_1, int_n_id94_1, int_y_id94_2, int_n_id94_2, int_y_id95_1, int_n_id95_1,
            int_y_id95_2, int_n_id95_2, int_y_id96_1, int_n_id96_1, int_y_id96_2, int_n_id96_2, int_y_id97_1, int_n_id97_1, int_y_id97_2,
            int_n_id97_2, int_y_id98_1, int_n_id98_1, int_y_id98_2, int_n_id98_2, int_y_id99_1, int_n_id99_1, int_y_id99_2, int_n_id99_2,
            int_y_id101_1, int_n_id101_1, int_y_id101_2, int_n_id101_2, int_y_id102_1, int_n_id102_1, int_y_id102_2, int_n_id102_2,
            int_y_id103_1, int_n_id103_1, int_y_id103_2, int_n_id103_2;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem menu_validasi = menu.findItem(R.id.menu_validasi);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        kuisioner = inflater.inflate(R.layout.e_fragment_updatequisioner_sio, container,
                false);
        me = E_MainActivity.e_bundle.getParcelable(getString(R.string.modelespaj));
        ((E_MainActivity) getActivity()).setSecondToolbar("Pertanyaan Kesehatan (8/", 8);
        ButterKnife.bind(this, kuisioner);

        //editText
        essay_sio_no1.addTextChangedListener(new generalTextWatcher(essay_sio_no1));
        essay_sio_no1.setOnFocusChangeListener(this);
        essay_sio_no2.addTextChangedListener(new generalTextWatcher(essay_sio_no2));
        essay_sio_no2.setOnFocusChangeListener(this);
        essay_sio_no3ket.addTextChangedListener(new generalTextWatcher(essay_sio_no3ket));
        essay_sio_no3ket.setOnFocusChangeListener(this);
        essay_sio_no4.addTextChangedListener(new generalTextWatcher(essay_sio_no4));
        essay_sio_no4.setOnFocusChangeListener(this);
        essay_sio_no5.addTextChangedListener(new generalTextWatcher(essay_sio_no5));
        essay_sio_no5.setOnFocusChangeListener(this);
        essay_sio_no6.addTextChangedListener(new generalTextWatcher(essay_sio_no6));
        essay_sio_no6.setOnFocusChangeListener(this);
        essay_sio_cm_ppno7.addTextChangedListener(new generalTextWatcher(essay_sio_cm_ppno7));
        essay_sio_cm_ppno7.setOnFocusChangeListener(this);
        essay_sio_kg_ppno7.addTextChangedListener(new generalTextWatcher(essay_sio_kg_ppno7));
        essay_sio_kg_ppno7.setOnFocusChangeListener(this);
        essay_sio_cm_ttno7.addTextChangedListener(new generalTextWatcher(essay_sio_cm_ttno7));
        essay_sio_cm_ttno7.setOnFocusChangeListener(this);
        essay_sio_kg_ttno7.addTextChangedListener(new generalTextWatcher(essay_sio_kg_ttno7));
        essay_sio_kg_ttno7.setOnFocusChangeListener(this);

        //radiogroup
        option_grup_sio_ppno1.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno1, this);
        option_grup_sio_ppno2.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno2, this);
        option_grup_sio_ppno3a.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3a, this);
        option_grup_sio_ppno3b.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3b, this);
        option_grup_sio_ppno3c.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3c, this);
        option_grup_sio_ppno3d.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3d, this);
        option_grup_sio_ppno3e.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3e, this);
        option_grup_sio_ppno3f.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3f, this);
        option_grup_sio_ppno3g.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3g, this);
        option_grup_sio_ppno3h.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3h, this);
        option_grup_sio_ppno3i.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3i, this);
        option_grup_sio_ppno3j.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3j, this);
        option_grup_sio_ppno3k.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3k, this);
        option_grup_sio_ppno3l.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3l, this);
        option_grup_sio_ppno3m.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3m, this);
        option_grup_sio_ppno3n.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3n, this);
        option_grup_sio_ppno3o.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3o, this);
        option_grup_sio_ppno3p.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno3p, this);
        option_grup_sio_ppno4.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno4, this);
        option_grup_sio_ppno5.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno5, this);
        option_grup_sio_ppno6.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ppno6, this);

        option_grup_sio_ttno1.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno1, this);
        option_grup_sio_ttno2.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno2, this);
        option_grup_sio_ttno3a.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3a, this);
        option_grup_sio_ttno3b.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3b, this);
        option_grup_sio_ttno3c.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3c, this);
        option_grup_sio_ttno3d.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3d, this);
        option_grup_sio_ttno3e.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3e, this);
        option_grup_sio_ttno3f.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3f, this);
        option_grup_sio_ttno3g.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3g, this);
        option_grup_sio_ttno3h.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3h, this);
        option_grup_sio_ttno3i.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3i, this);
        option_grup_sio_ttno3j.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3j, this);
        option_grup_sio_ttno3k.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3k, this);
        option_grup_sio_ttno3l.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3l, this);
        option_grup_sio_ttno3m.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3m, this);
        option_grup_sio_ttno3n.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3n, this);
        option_grup_sio_ttno3o.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3o, this);
        option_grup_sio_ttno3p.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno3p, this);
        option_grup_sio_ttno4.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno4, this);
        option_grup_sio_ttno5.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno5, this);
        option_grup_sio_ttno6.setOnCheckedChangeListener(this);
        MethodSupport.OnRadioGroup(option_grup_sio_ttno6, this);

        //radiobutton
        MethodSupport.OnRadioButton(option_y_sio_ppno1, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno1, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno1, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno1, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno2, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno2, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno2, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno2, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3a, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3a, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3a, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3a, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3b, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3b, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3b, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3b, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3c, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3c, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3c, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3c, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3d, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3d, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3d, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3d, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3e, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3e, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3e, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3e, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3f, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3f, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3f, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3f, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3g, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3g, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3g, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3g, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3h, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3h, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3h, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3h, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3i, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3i, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3i, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3i, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3j, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3j, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3j, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3j, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3k, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3k, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3k, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3k, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3l, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3l, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3l, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3l, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3m, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3m, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3m, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3m, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3n, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3n, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3n, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3n, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3o, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3o, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3o, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3o, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno3p, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno3p, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno3p, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno3p, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno4, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno4, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno4, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno4, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno5, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno5, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno5, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno5, this);
        MethodSupport.OnRadioButton(option_y_sio_ppno6, this);
        MethodSupport.OnRadioButton(option_n_sio_ppno6, this);
        MethodSupport.OnRadioButton(option_y_sio_ttno6, this);
        MethodSupport.OnRadioButton(option_n_sio_ttno6, this);
//button di Activity
        btn_lanjut = getActivity().findViewById(R.id.btn_lanjut);
        btn_lanjut.setOnClickListener(this);
        btn_kembali = getActivity().findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(this);
        second_toolbar = ((E_MainActivity) getActivity()).getSecond_toolbar();
        iv_save = second_toolbar.findViewById(R.id.iv_save);
        iv_save.setOnClickListener(this);
        iv_next = second_toolbar.findViewById(R.id.iv_next);
        iv_next.setOnClickListener(this);
        iv_prev = second_toolbar.findViewById(R.id.iv_prev);
        iv_prev.setOnClickListener(this);

//        essay_sio_no1.addTextChangedListener(new generalTextWatcher(essay_sio_no1));
//        essay_sio_no2.addTextChangedListener(new generalTextWatcher(essay_sio_no1));
//        essay_sio_no4.addTextChangedListener(new generalTextWatcher(essay_sio_no1));
//        essay_sio_no5.addTextChangedListener(new generalTextWatcher(essay_sio_no1));
//        essay_sio_no6.addTextChangedListener(new generalTextWatcher(essay_sio_no1));

        ListOfView = new ArrayList<View>();
        //VIEWNYA MASIH KURANG DAN KUDU DIPERIKSA LAGI
        View[] view = {essay_sio_no1, option_y_sio_ppno1, option_n_sio_ppno1, option_y_sio_ttno1, option_n_sio_ttno1,//81
                essay_sio_no2, option_y_sio_ppno2, option_n_sio_ppno2, option_y_sio_ttno2, option_n_sio_ttno2,
                option_y_sio_ppno3a, option_n_sio_ppno3a, option_y_sio_ttno3a, option_n_sio_ttno3a, //84
                option_y_sio_ppno3b, option_n_sio_ppno3b, option_y_sio_ttno3b, option_n_sio_ttno3b,
                option_y_sio_ppno3c, option_n_sio_ppno3c, option_y_sio_ttno3c, option_n_sio_ttno3c,
                option_y_sio_ppno3d, option_n_sio_ppno3d, option_y_sio_ttno3d, option_n_sio_ttno3d,
                option_y_sio_ppno3e, option_n_sio_ppno3e, option_y_sio_ttno3e, option_n_sio_ttno3e,
                option_y_sio_ppno3f, option_n_sio_ppno3f, option_y_sio_ttno3f, option_n_sio_ttno3f,
                option_y_sio_ppno3g, option_n_sio_ppno3g, option_y_sio_ttno3g, option_n_sio_ttno3g,
                option_y_sio_ppno3h, option_n_sio_ppno3h, option_y_sio_ttno3h, option_n_sio_ttno3h,
                option_y_sio_ppno3i, option_n_sio_ppno3i, option_y_sio_ttno3i, option_n_sio_ttno3i,
                option_y_sio_ppno3j, option_n_sio_ppno3j, option_y_sio_ttno3j, option_n_sio_ttno3j,
                option_y_sio_ppno3k, option_n_sio_ppno3k, option_y_sio_ttno3k, option_n_sio_ttno3k,
                option_y_sio_ppno3l, option_n_sio_ppno3l, option_y_sio_ttno3l, option_n_sio_ttno3l,
                option_y_sio_ppno3m, option_n_sio_ppno3m, option_y_sio_ttno3m, option_n_sio_ttno3m,
                option_y_sio_ppno3n, option_n_sio_ppno3n, option_y_sio_ttno3n, option_n_sio_ttno3n,
                option_y_sio_ppno3o, option_n_sio_ppno3o, option_y_sio_ttno3o, option_n_sio_ttno3o,
                option_y_sio_ppno3p, option_n_sio_ppno3p, option_y_sio_ttno3p, option_n_sio_ttno3p,
                essay_sio_no3ket,//100
                essay_sio_no4, option_y_sio_ppno4, option_n_sio_ppno4, option_y_sio_ttno4, option_n_sio_ttno4,
                essay_sio_no5, option_y_sio_ppno5, option_n_sio_ppno5, option_y_sio_ttno5, option_n_sio_ttno5,
                essay_sio_no6, option_y_sio_ppno6, option_n_sio_ppno6, option_y_sio_ttno6, option_n_sio_ttno6,
                essay_sio_cm_ppno7, essay_sio_kg_ppno7, essay_sio_cm_ttno7, essay_sio_kg_ttno7};//104
        ListOfView.addAll(Arrays.asList(view));
        restore();
        return kuisioner;
    }

    @Override
    public void onPause() {
        super.onPause();
        ProgressDialogClass.removeSimpleProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();

        MethodSupport.onFocusviewtest(scroll_qu, cl_child_qu, cl_data_kesehatan_sio_no1_qu);

        MethodSupport.active_view(1, btn_lanjut);
        MethodSupport.active_view(1, iv_next);
        MethodSupport.active_view(1, btn_kembali);
        MethodSupport.active_view(1, iv_prev);
        ProgressDialogClass.removeSimpleProgressDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_validasi:
                me.getModelUpdateQuisioner().setValidation(validation());
                me.getModelID().setVal_qu(validation());
                load();
                MyTaskValidasi taskValidasi = new MyTaskValidasi();
                taskValidasi.execute();
                Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTaskValidasi extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_QUESTIONAIRE_SIO);
            return null;
        }
    }

    private class MyTaskSavePage extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_QUESTIONAIRE_SIO);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //View di activity
            case R.id.btn_lanjut:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_next:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_save:
                load();
                MyTaskSavePage taskSavePage = new MyTaskSavePage();
                taskSavePage.execute();

                break;
            case R.id.iv_prev:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.btn_kembali:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
        }
    }

    //radiogroup
    @Override
    public void onCheckedChanged(RadioGroup radiogroup, @IdRes int checkedId) {
        switch (radiogroup.getId()) {
            case R.id.option_grup_sio_ppno1:
                HashMap<String, Integer> map = MethodSupport.OnCheckQuisionerRadioKesValueEditSIO(option_grup_sio_ppno1, option_grup_sio_ttno1, int_y_id81_1, int_n_id81_1, checkedId, iv_circle_data_kesehatan_sio_no1_qu, getContext(), essay_sio_no1, int_y_id81_2, int_n_id81_2);//essay_sio_no1,
                int_y_id81_1 = map.get("int_y_id1");
                int_n_id81_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno1:
                map = MethodSupport.OnCheckQuisionerRadioKesValueEditSIO(option_grup_sio_ttno1, option_grup_sio_ppno1, int_y_id81_2, int_n_id81_2, checkedId, iv_circle_data_kesehatan_sio_no1_qu, getContext(), essay_sio_no1, int_y_id81_1, int_n_id81_1);//essay_sio_no1,
                int_y_id81_2 = map.get("int_y_id1");
                int_n_id81_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno2:
                map = MethodSupport.OnCheckQuisionerRadioKesValueEditSIO(option_grup_sio_ppno2, option_grup_sio_ttno2, int_y_id82_1, int_n_id82_1, checkedId, iv_circle_data_kesehatan_sio_no2_qu, getContext(), essay_sio_no2, int_y_id82_2, int_n_id82_2); //essay_sio_no2,
                int_y_id82_1 = map.get("int_y_id1");
                int_n_id82_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno2:
                map = MethodSupport.OnCheckQuisionerRadioKesValueEditSIO(option_grup_sio_ttno2, option_grup_sio_ppno2, int_y_id82_2, int_n_id82_2, checkedId, iv_circle_data_kesehatan_sio_no2_qu, getContext(), essay_sio_no2, int_y_id82_1, int_n_id82_1); //essay_sio_no2,
                int_y_id82_2 = map.get("int_y_id1");
                int_n_id82_2 = map.get("int_n_id1");
                break;


            case R.id.option_grup_sio_ppno4:
                map = MethodSupport.OnCheckQuisionerRadioKesValueEditSIO(option_grup_sio_ppno4, option_grup_sio_ttno4, int_y_id101_1, int_n_id101_1, checkedId, iv_circle_data_kesehatan_sio_no4_qu, getContext(), essay_sio_no4, int_y_id101_2, int_n_id101_2); //essay_sio_no4,
                int_y_id101_1 = map.get("int_y_id1");
                int_n_id101_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno4:
                map = MethodSupport.OnCheckQuisionerRadioKesValueEditSIO(option_grup_sio_ttno4, option_grup_sio_ppno4, int_y_id101_2, int_n_id101_2, checkedId, iv_circle_data_kesehatan_sio_no4_qu, getContext(), essay_sio_no4, int_y_id101_1, int_n_id101_1); //essay_sio_no4,
                int_y_id101_2 = map.get("int_y_id1");
                int_n_id101_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno5:
                map = MethodSupport.OnCheckQuisionerRadioKesValueEditSIO(option_grup_sio_ppno5, option_grup_sio_ttno5, int_y_id102_1, int_n_id102_1, checkedId, iv_circle_data_kesehatan_sio_no5_qu, getContext(), essay_sio_no5, int_y_id102_2, int_n_id102_2); //essay_sio_no5,
                int_y_id102_1 = map.get("int_y_id1");
                int_n_id102_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno5:
                map = MethodSupport.OnCheckQuisionerRadioKesValueEditSIO(option_grup_sio_ttno5, option_grup_sio_ppno5, int_y_id102_2, int_n_id102_2, checkedId, iv_circle_data_kesehatan_sio_no5_qu, getContext(), essay_sio_no5, int_y_id102_1, int_n_id102_1); //essay_sio_no5,
                int_y_id102_2 = map.get("int_y_id1");
                int_n_id102_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno6:
                map = MethodSupport.OnCheckQuisionerRadioKesValueEditSIO(option_grup_sio_ppno6, option_grup_sio_ttno6, int_y_id103_1, int_n_id103_1, checkedId, iv_circle_data_kesehatan_sio_no6_qu, getContext(), essay_sio_no6, int_y_id103_2, int_n_id103_2); //essay_sio_no6,
                int_y_id103_1 = map.get("int_y_id1");
                int_n_id103_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno6:
                map = MethodSupport.OnCheckQuisionerRadioKesValueEditSIO(option_grup_sio_ttno6, option_grup_sio_ppno6, int_y_id103_2, int_n_id103_2, checkedId, iv_circle_data_kesehatan_sio_no6_qu, getContext(), essay_sio_no6, int_y_id103_1, int_n_id103_1); //essay_sio_no6,
                int_y_id103_2 = map.get("int_y_id1");
                int_n_id103_2 = map.get("int_n_id1");
                break;


            case R.id.option_grup_sio_ppno3a:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3a, option_grup_sio_ttno3a, int_y_id84_1, int_n_id84_1, checkedId, iv_circle_data_kesehatan_sio_no3_qu, getContext());
                int_y_id84_1 = map.get("int_y_id1");
                int_n_id84_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3a:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3a, option_grup_sio_ppno3a, int_y_id84_2, int_n_id84_2, checkedId, iv_circle_data_kesehatan_sio_no3_qu, getContext());
                int_y_id84_2 = map.get("int_y_id1");
                int_n_id84_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno3b:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3b, option_grup_sio_ttno3b, int_y_id85_1, int_n_id85_1, checkedId, iv_circle_data_kesehatan_sio_no3b_qu, getContext());
                int_y_id85_1 = map.get("int_y_id1");
                int_n_id85_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3b:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3b, option_grup_sio_ppno3b, int_y_id85_2, int_n_id85_2, checkedId, iv_circle_data_kesehatan_sio_no3b_qu, getContext());
                int_y_id85_2 = map.get("int_y_id1");
                int_n_id85_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno3c:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3c, option_grup_sio_ttno3c, int_y_id86_1, int_n_id86_1, checkedId, iv_circle_data_kesehatan_sio_no3c_qu, getContext());
                int_y_id86_1 = map.get("int_y_id1");
                int_n_id86_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3c:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3c, option_grup_sio_ppno3c, int_y_id86_2, int_n_id86_2, checkedId, iv_circle_data_kesehatan_sio_no3c_qu, getContext());
                int_y_id86_2 = map.get("int_y_id1");
                int_n_id86_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno3d:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3d, option_grup_sio_ttno3d, int_y_id87_1, int_n_id87_1, checkedId, iv_circle_data_kesehatan_sio_no3d_qu, getContext());
                int_y_id87_1 = map.get("int_y_id1");
                int_n_id87_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3d:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3d, option_grup_sio_ppno3d, int_y_id87_2, int_n_id87_2, checkedId, iv_circle_data_kesehatan_sio_no3d_qu, getContext());
                int_y_id87_2 = map.get("int_y_id1");
                int_n_id87_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno3e:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3e, option_grup_sio_ttno3e, int_y_id88_1, int_n_id88_1, checkedId, iv_circle_data_kesehatan_sio_no3e_qu, getContext());
                int_y_id88_1 = map.get("int_y_id1");
                int_n_id88_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3e:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3e, option_grup_sio_ppno3e, int_y_id88_2, int_n_id88_2, checkedId, iv_circle_data_kesehatan_sio_no3e_qu, getContext());
                int_y_id88_2 = map.get("int_y_id1");
                int_n_id88_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno3f:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3f, option_grup_sio_ttno3f, int_y_id89_1, int_n_id89_1, checkedId, iv_circle_data_kesehatan_sio_no3f_qu, getContext());
                int_y_id89_1 = map.get("int_y_id1");
                int_n_id89_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3f:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3f, option_grup_sio_ppno3f, int_y_id89_2, int_n_id89_2, checkedId, iv_circle_data_kesehatan_sio_no3f_qu, getContext());
                int_y_id89_2 = map.get("int_y_id1");
                int_n_id89_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno3g:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3g, option_grup_sio_ttno3g, int_y_id90_1, int_n_id90_1, checkedId, iv_circle_data_kesehatan_sio_no3g_qu, getContext());
                int_y_id90_1 = map.get("int_y_id1");
                int_n_id90_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3g:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3g, option_grup_sio_ppno3g, int_y_id90_2, int_n_id90_2, checkedId, iv_circle_data_kesehatan_sio_no3g_qu, getContext());
                int_y_id90_2 = map.get("int_y_id1");
                int_n_id90_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno3h:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3h, option_grup_sio_ttno3h, int_y_id91_1, int_n_id91_1, checkedId, iv_circle_data_kesehatan_sio_no3h_qu, getContext());
                int_y_id91_1 = map.get("int_y_id1");
                int_n_id91_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3h:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3h, option_grup_sio_ppno3h, int_y_id91_2, int_n_id91_2, checkedId, iv_circle_data_kesehatan_sio_no3h_qu, getContext());
                int_y_id91_2 = map.get("int_y_id1");
                int_n_id91_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno3i:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3i, option_grup_sio_ttno3i, int_y_id92_1, int_n_id92_1, checkedId, iv_circle_data_kesehatan_sio_no3i_qu, getContext());
                int_y_id92_1 = map.get("int_y_id1");
                int_n_id92_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3i:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3i, option_grup_sio_ppno3i, int_y_id92_2, int_n_id92_2, checkedId, iv_circle_data_kesehatan_sio_no3i_qu, getContext());
                int_y_id92_2 = map.get("int_y_id1");
                int_n_id92_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno3j:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3j, option_grup_sio_ttno3j, int_y_id93_1, int_n_id93_1, checkedId, iv_circle_data_kesehatan_sio_no3j_qu, getContext());
                int_y_id93_1 = map.get("int_y_id1");
                int_n_id93_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3j:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3j, option_grup_sio_ppno3j, int_y_id93_2, int_n_id93_2, checkedId, iv_circle_data_kesehatan_sio_no3j_qu, getContext());
                int_y_id93_2 = map.get("int_y_id1");
                int_n_id93_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno3k:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3k, option_grup_sio_ttno3k, int_y_id94_1, int_n_id94_1, checkedId, iv_circle_data_kesehatan_sio_no3k_qu, getContext());
                int_y_id94_1 = map.get("int_y_id1");
                int_n_id94_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3k:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3k, option_grup_sio_ppno3k, int_y_id94_2, int_n_id94_2, checkedId, iv_circle_data_kesehatan_sio_no3k_qu, getContext());
                int_y_id94_2 = map.get("int_y_id1");
                int_n_id94_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno3l:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3l, option_grup_sio_ttno3l, int_y_id95_1, int_n_id95_1, checkedId, iv_circle_data_kesehatan_sio_no3l_qu, getContext());
                int_y_id95_1 = map.get("int_y_id1");
                int_n_id95_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3l:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3l, option_grup_sio_ppno3l, int_y_id95_2, int_n_id95_2, checkedId, iv_circle_data_kesehatan_sio_no3l_qu, getContext());
                int_y_id95_2 = map.get("int_y_id1");
                int_n_id95_2 = map.get("int_n_id1");
                break;

            case R.id.option_grup_sio_ppno3m:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3m, option_grup_sio_ttno3m, int_y_id96_1, int_n_id96_1, checkedId, iv_circle_data_kesehatan_sio_no3m_qu, getContext());
                int_y_id96_1 = map.get("int_y_id1");
                int_n_id96_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3m:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3m, option_grup_sio_ppno3m, int_y_id96_2, int_n_id96_2, checkedId, iv_circle_data_kesehatan_sio_no3m_qu, getContext());
                int_y_id96_2 = map.get("int_y_id1");
                int_n_id96_2 = map.get("int_n_id1");
                break;

            case R.id.option_grup_sio_ppno3n:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3n, option_grup_sio_ttno3n, int_y_id97_1, int_n_id97_1, checkedId, iv_circle_data_kesehatan_sio_no3n_qu, getContext());
                int_y_id97_1 = map.get("int_y_id1");
                int_n_id97_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3n:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3n, option_grup_sio_ppno3n, int_y_id97_2, int_n_id97_2, checkedId, iv_circle_data_kesehatan_sio_no3n_qu, getContext());
                int_y_id97_2 = map.get("int_y_id1");
                int_n_id97_2 = map.get("int_n_id1");
                break;

            case R.id.option_grup_sio_ppno3o:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3o, option_grup_sio_ttno3o, int_y_id98_1, int_n_id98_1, checkedId, iv_circle_data_kesehatan_sio_no3o_qu, getContext());
                int_y_id98_1 = map.get("int_y_id1");
                int_n_id98_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3o:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3o, option_grup_sio_ppno3o, int_y_id98_2, int_n_id98_2, checkedId, iv_circle_data_kesehatan_sio_no3o_qu, getContext());
                int_y_id98_2 = map.get("int_y_id1");
                int_n_id98_2 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ppno3p:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ppno3p, option_grup_sio_ttno3p, int_y_id99_1, int_n_id99_1, checkedId, iv_circle_data_kesehatan_sio_no3p_qu, getContext());
                int_y_id99_1 = map.get("int_y_id1");
                int_n_id99_1 = map.get("int_n_id1");
                break;
            case R.id.option_grup_sio_ttno3p:
                map = MethodSupport.OnCheckQuisionerRadioKesValueSIO(option_grup_sio_ttno3p, option_grup_sio_ppno3p, int_y_id99_2, int_n_id99_2, checkedId, iv_circle_data_kesehatan_sio_no3p_qu, getContext());
                int_y_id99_2 = map.get("int_y_id1");
                int_n_id99_2 = map.get("int_n_id1");
                break;

        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.option_grup_sio_ppno1:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no1_qu);
                break;
            case R.id.option_grup_sio_ppno2:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no2_qu);
                break;
            case R.id.option_grup_sio_ppno3a:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3_qu);
                break;
            case R.id.option_grup_sio_ppno3b:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3b_qu);
                break;
            case R.id.option_grup_sio_ppno3c:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3c_qu);
                break;
            case R.id.option_grup_sio_ppno3d:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3d_qu);
                break;
            case R.id.option_grup_sio_ppno3e:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3e_qu);
                break;
            case R.id.option_grup_sio_ppno3f:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3f_qu);
                break;
            case R.id.option_grup_sio_ppno3g:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3g_qu);
                break;
            case R.id.option_grup_sio_ppno3h:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3h_qu);
                break;
            case R.id.option_grup_sio_ppno3i:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3i_qu);
                break;
            case R.id.option_grup_sio_ppno3j:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3j_qu);
                break;
            case R.id.option_grup_sio_ppno3k:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3k_qu);
                break;
            case R.id.option_grup_sio_ppno3l:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3l_qu);
                break;
            case R.id.option_grup_sio_ppno3m:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3m_qu);
                break;
            case R.id.option_grup_sio_ppno3n:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3n_qu);
                break;
            case R.id.option_grup_sio_ppno3o:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3o_qu);
                break;
            case R.id.option_grup_sio_ppno3p:
                MethodSupport.focuslayouttext(hasFocus, cl_data_kesehatan_sio_no3p_qu);
                break;

            //radiobutton

            case R.id.option_y_sio_ppno1:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no1_qu, option_grup_sio_ppno1, R.id.option_y_sio_ppno1);
                break;
            case R.id.option_n_sio_ppno1:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no1_qu, option_grup_sio_ppno1, R.id.option_n_sio_ppno1);
                break;
            case R.id.option_y_sio_ttno1:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no1_qu, option_grup_sio_ttno1, R.id.option_y_sio_ttno1);
                break;
            case R.id.option_n_sio_ttno1:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no1_qu, option_grup_sio_ttno1, R.id.option_n_sio_ttno1);
                break;
            case R.id.option_y_sio_ppno2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no2_qu, option_grup_sio_ppno2, R.id.option_y_sio_ppno2);
                break;
            case R.id.option_n_sio_ppno2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no2_qu, option_grup_sio_ppno2, R.id.option_n_sio_ppno2);
                break;
            case R.id.option_y_sio_ttno2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no2_qu, option_grup_sio_ttno2, R.id.option_y_sio_ttno2);
                break;
            case R.id.option_n_sio_ttno2:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no2_qu, option_grup_sio_ttno2, R.id.option_n_sio_ttno2);
                break;
            case R.id.option_y_sio_ppno3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3_qu, option_grup_sio_ppno3a, R.id.option_y_sio_ppno3a);
                break;
            case R.id.option_n_sio_ppno3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3_qu, option_grup_sio_ppno3a, R.id.option_n_sio_ppno3a);
                break;
            case R.id.option_y_sio_ttno3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3_qu, option_grup_sio_ttno3a, R.id.option_y_sio_ttno3a);
                break;
            case R.id.option_n_sio_ttno3a:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3_qu, option_grup_sio_ttno3a, R.id.option_n_sio_ttno3a);
                break;
            case R.id.option_y_sio_ppno3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3b_qu, option_grup_sio_ppno3b, R.id.option_y_sio_ppno3b);
                break;
            case R.id.option_n_sio_ppno3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3b_qu, option_grup_sio_ppno3b, R.id.option_n_sio_ppno3b);
                break;
            case R.id.option_y_sio_ttno3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3b_qu, option_grup_sio_ttno3b, R.id.option_y_sio_ttno3b);
                break;
            case R.id.option_n_sio_ttno3b:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3b_qu, option_grup_sio_ttno3b, R.id.option_n_sio_ttno3b);
                break;
            case R.id.option_y_sio_ppno3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3c_qu, option_grup_sio_ppno3c, R.id.option_y_sio_ppno3c);
                break;
            case R.id.option_n_sio_ppno3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3c_qu, option_grup_sio_ppno3c, R.id.option_n_sio_ppno3c);
                break;
            case R.id.option_y_sio_ttno3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3c_qu, option_grup_sio_ttno3c, R.id.option_y_sio_ttno3c);
                break;
            case R.id.option_n_sio_ttno3c:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3c_qu, option_grup_sio_ttno3c, R.id.option_n_sio_ttno3c);
                break;
            case R.id.option_y_sio_ppno3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3d_qu, option_grup_sio_ppno3d, R.id.option_y_sio_ppno3d);
                break;
            case R.id.option_n_sio_ppno3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3d_qu, option_grup_sio_ppno3d, R.id.option_n_sio_ppno3d);
                break;
            case R.id.option_y_sio_ttno3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3d_qu, option_grup_sio_ttno3d, R.id.option_y_sio_ttno3d);
                break;
            case R.id.option_n_sio_ttno3d:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3d_qu, option_grup_sio_ttno3d, R.id.option_n_sio_ttno3d);
                break;
            case R.id.option_y_sio_ppno3e:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3e_qu, option_grup_sio_ppno3e, R.id.option_y_sio_ppno3e);
                break;
            case R.id.option_n_sio_ppno3e:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3e_qu, option_grup_sio_ppno3e, R.id.option_n_sio_ppno3e);
                break;
            case R.id.option_y_sio_ttno3e:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3e_qu, option_grup_sio_ttno3e, R.id.option_y_sio_ttno3e);
                break;
            case R.id.option_n_sio_ttno3e:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3e_qu, option_grup_sio_ttno3e, R.id.option_n_sio_ttno3e);
                break;
            case R.id.option_y_sio_ppno3f:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3f_qu, option_grup_sio_ppno3f, R.id.option_y_sio_ppno3f);
                break;
            case R.id.option_n_sio_ppno3f:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3f_qu, option_grup_sio_ppno3f, R.id.option_n_sio_ppno3f);
                break;
            case R.id.option_y_sio_ttno3f:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3f_qu, option_grup_sio_ttno3f, R.id.option_y_sio_ttno3f);
                break;
            case R.id.option_n_sio_ttno3f:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3f_qu, option_grup_sio_ttno3f, R.id.option_n_sio_ttno3f);
                break;
            case R.id.option_y_sio_ppno3g:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3g_qu, option_grup_sio_ppno3g, R.id.option_y_sio_ppno3g);
                break;
            case R.id.option_n_sio_ppno3g:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3g_qu, option_grup_sio_ppno3g, R.id.option_n_sio_ppno3g);
                break;
            case R.id.option_y_sio_ttno3g:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3g_qu, option_grup_sio_ttno3g, R.id.option_y_sio_ttno3g);
                break;
            case R.id.option_n_sio_ttno3g:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3g_qu, option_grup_sio_ttno3g, R.id.option_n_sio_ttno3g);
                break;
            case R.id.option_y_sio_ppno3h:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3h_qu, option_grup_sio_ppno3h, R.id.option_y_sio_ppno3h);
                break;
            case R.id.option_n_sio_ppno3h:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3h_qu, option_grup_sio_ppno3h, R.id.option_n_sio_ppno3h);
                break;
            case R.id.option_y_sio_ttno3h:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3h_qu, option_grup_sio_ttno3h, R.id.option_y_sio_ttno3h);
                break;
            case R.id.option_n_sio_ttno3h:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3h_qu, option_grup_sio_ttno3h, R.id.option_n_sio_ttno3h);
                break;
            case R.id.option_y_sio_ppno3i:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3i_qu, option_grup_sio_ppno3i, R.id.option_y_sio_ppno3i);
                break;
            case R.id.option_n_sio_ppno3i:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3i_qu, option_grup_sio_ppno3i, R.id.option_n_sio_ppno3i);
                break;
            case R.id.option_y_sio_ttno3i:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3i_qu, option_grup_sio_ttno3i, R.id.option_y_sio_ttno3i);
                break;
            case R.id.option_n_sio_ttno3i:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3i_qu, option_grup_sio_ttno3i, R.id.option_n_sio_ttno3i);
                break;
            case R.id.option_y_sio_ppno3j:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3j_qu, option_grup_sio_ppno3j, R.id.option_y_sio_ppno3j);
                break;
            case R.id.option_n_sio_ppno3j:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3j_qu, option_grup_sio_ppno3j, R.id.option_n_sio_ppno3j);
                break;
            case R.id.option_y_sio_ttno3j:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3j_qu, option_grup_sio_ttno3j, R.id.option_y_sio_ttno3j);
                break;
            case R.id.option_n_sio_ttno3j:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3j_qu, option_grup_sio_ttno3j, R.id.option_n_sio_ttno3j);
                break;
            case R.id.option_y_sio_ppno3k:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3k_qu, option_grup_sio_ppno3k, R.id.option_y_sio_ppno3k);
                break;
            case R.id.option_n_sio_ppno3k:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3k_qu, option_grup_sio_ppno3k, R.id.option_n_sio_ppno3k);
                break;
            case R.id.option_y_sio_ttno3k:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3k_qu, option_grup_sio_ttno3k, R.id.option_y_sio_ttno3k);
                break;
            case R.id.option_n_sio_ttno3k:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3k_qu, option_grup_sio_ttno3k, R.id.option_n_sio_ttno3k);
                break;
            case R.id.option_y_sio_ppno3l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3l_qu, option_grup_sio_ppno3l, R.id.option_y_sio_ppno3l);
                break;
            case R.id.option_n_sio_ppno3l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3l_qu, option_grup_sio_ppno3l, R.id.option_n_sio_ppno3l);
                break;
            case R.id.option_y_sio_ttno3l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3l_qu, option_grup_sio_ttno3l, R.id.option_y_sio_ttno3l);
                break;
            case R.id.option_n_sio_ttno3l:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3l_qu, option_grup_sio_ttno3l, R.id.option_n_sio_ttno3l);
                break;
            case R.id.option_y_sio_ppno3m:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3m_qu, option_grup_sio_ppno3m, R.id.option_y_sio_ppno3m);
                break;
            case R.id.option_n_sio_ppno3m:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3m_qu, option_grup_sio_ppno3m, R.id.option_n_sio_ppno3m);
                break;
            case R.id.option_y_sio_ttno3m:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3m_qu, option_grup_sio_ttno3m, R.id.option_y_sio_ttno3m);
                break;
            case R.id.option_n_sio_ttno3m:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3m_qu, option_grup_sio_ttno3m, R.id.option_n_sio_ttno3m);
                break;
            case R.id.option_y_sio_ppno3n:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3n_qu, option_grup_sio_ppno3n, R.id.option_y_sio_ppno3n);
                break;
            case R.id.option_n_sio_ppno3n:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3n_qu, option_grup_sio_ppno3n, R.id.option_n_sio_ppno3n);
                break;
            case R.id.option_y_sio_ttno3n:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3n_qu, option_grup_sio_ttno3n, R.id.option_y_sio_ttno3n);
                break;
            case R.id.option_n_sio_ttno3n:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3n_qu, option_grup_sio_ttno3n, R.id.option_n_sio_ttno3n);
                break;
            case R.id.option_y_sio_ppno3o:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3o_qu, option_grup_sio_ppno3o, R.id.option_y_sio_ppno3o);
                break;
            case R.id.option_n_sio_ppno3o:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3o_qu, option_grup_sio_ppno3o, R.id.option_n_sio_ppno3o);
                break;
            case R.id.option_y_sio_ttno3o:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3o_qu, option_grup_sio_ttno3o, R.id.option_y_sio_ttno3o);
                break;
            case R.id.option_n_sio_ttno3o:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3o_qu, option_grup_sio_ttno3o, R.id.option_n_sio_ttno3o);
                break;
            case R.id.option_y_sio_ppno3p:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3p_qu, option_grup_sio_ppno3p, R.id.option_y_sio_ppno3p);
                break;
            case R.id.option_n_sio_ppno3p:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3p_qu, option_grup_sio_ppno3p, R.id.option_n_sio_ppno3p);
                break;
            case R.id.option_y_sio_ttno3p:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3p_qu, option_grup_sio_ttno3p, R.id.option_y_sio_ttno3p);
                break;
            case R.id.option_n_sio_ttno3p:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no3p_qu, option_grup_sio_ttno3p, R.id.option_n_sio_ttno3p);
                break;
            case R.id.option_y_sio_ppno4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no4_qu, option_grup_sio_ppno4, R.id.option_y_sio_ppno4);
                break;
            case R.id.option_n_sio_ppno4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no4_qu, option_grup_sio_ppno4, R.id.option_n_sio_ppno4);
                break;
            case R.id.option_y_sio_ttno4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no4_qu, option_grup_sio_ttno4, R.id.option_y_sio_ttno4);
                break;
            case R.id.option_n_sio_ttno4:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no4_qu, option_grup_sio_ttno4, R.id.option_n_sio_ttno4);
                break;
            case R.id.option_y_sio_ppno5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no5_qu, option_grup_sio_ppno5, R.id.option_y_sio_ppno5);
                break;
            case R.id.option_n_sio_ppno5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no5_qu, option_grup_sio_ppno5, R.id.option_n_sio_ppno5);
                break;
            case R.id.option_y_sio_ttno5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no5_qu, option_grup_sio_ttno5, R.id.option_y_sio_ttno5);
                break;
            case R.id.option_n_sio_ttno5:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no5_qu, option_grup_sio_ttno5, R.id.option_n_sio_ttno5);
                break;
            case R.id.option_y_sio_ppno6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no6_qu, option_grup_sio_ppno6, R.id.option_y_sio_ppno6);
                break;
            case R.id.option_n_sio_ppno6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no6_qu, option_grup_sio_ppno6, R.id.option_n_sio_ppno6);
                break;
            case R.id.option_y_sio_ttno6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no6_qu, option_grup_sio_ttno6, R.id.option_y_sio_ttno6);
                break;
            case R.id.option_n_sio_ttno6:
                MethodSupport.focuslayoutRadio(hasFocus, cl_data_kesehatan_sio_no6_qu, option_grup_sio_ttno6, R.id.option_n_sio_ttno6);
                break;
        }
    }

    private boolean validation() {
        boolean ready = true;
//        if (!Method_Validator.onEmptyRadioText(option_grup_sio_ppno1, essay_sio_no1, iv_circle_data_kesehatan_sio_no1_qu, tv_error_data_kesehatan_sio_no1_qu, cl_data_kesehatan_sio_no1_qu, getContext())) {
//            ready = false;
//        }
//        if (!Method_Validator.onEmptyRadioText(option_grup_sio_ttno1, essay_sio_no1, iv_circle_data_kesehatan_sio_no1_qu, tv_error_data_kesehatan_sio_no1_qu, cl_data_kesehatan_sio_no1_qu, getContext())) {
//            ready = false;
//        }
//        if (!Method_Validator.onEmptyRadioText(option_grup_sio_ppno2, essay_sio_no2, iv_circle_data_kesehatan_sio_no2_qu, tv_error_data_kesehatan_sio_no2_qu, cl_data_kesehatan_sio_no2_qu, getContext())) {
//            ready = false;
//        }
//        if (!Method_Validator.onEmptyRadioText(option_grup_sio_ttno2, essay_sio_no2, iv_circle_data_kesehatan_sio_no2_qu, tv_error_data_kesehatan_sio_no2_qu, cl_data_kesehatan_sio_no2_qu, getContext())) {
//            ready = false;
//        }


        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno1, iv_circle_data_kesehatan_sio_no1_qu, tv_error_data_kesehatan_sio_no1_qu, cl_data_kesehatan_sio_no1_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno1, iv_circle_data_kesehatan_sio_no1_qu, tv_error_data_kesehatan_sio_no1_qu, cl_data_kesehatan_sio_no1_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno2, iv_circle_data_kesehatan_sio_no2_qu, tv_error_data_kesehatan_sio_no2_qu, cl_data_kesehatan_sio_no2_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno2, iv_circle_data_kesehatan_sio_no2_qu, tv_error_data_kesehatan_sio_no2_qu, cl_data_kesehatan_sio_no2_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3a, iv_circle_data_kesehatan_sio_no3_qu, tv_error_data_kesehatan_sio_no3a_qu, cl_data_kesehatan_sio_no3_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3a, iv_circle_data_kesehatan_sio_no3_qu, tv_error_data_kesehatan_sio_no3a_qu, cl_data_kesehatan_sio_no3_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3b, iv_circle_data_kesehatan_sio_no3b_qu, tv_error_data_kesehatan_sio_no3b_qu, cl_data_kesehatan_sio_no3b_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3b, iv_circle_data_kesehatan_sio_no3b_qu, tv_error_data_kesehatan_sio_no3b_qu, cl_data_kesehatan_sio_no3b_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3c, iv_circle_data_kesehatan_sio_no3c_qu, tv_error_data_kesehatan_sio_no3c_qu, cl_data_kesehatan_sio_no3c_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3c, iv_circle_data_kesehatan_sio_no3c_qu, tv_error_data_kesehatan_sio_no3c_qu, cl_data_kesehatan_sio_no3c_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3d, iv_circle_data_kesehatan_sio_no3d_qu, tv_error_data_kesehatan_sio_no3d_qu, cl_data_kesehatan_sio_no3d_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3d, iv_circle_data_kesehatan_sio_no3d_qu, tv_error_data_kesehatan_sio_no3d_qu, cl_data_kesehatan_sio_no3d_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3e, iv_circle_data_kesehatan_sio_no3e_qu, tv_error_data_kesehatan_sio_no3e_qu, cl_data_kesehatan_sio_no3e_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3e, iv_circle_data_kesehatan_sio_no3e_qu, tv_error_data_kesehatan_sio_no3e_qu, cl_data_kesehatan_sio_no3e_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3f, iv_circle_data_kesehatan_sio_no3f_qu, tv_error_data_kesehatan_sio_no3f_qu, cl_data_kesehatan_sio_no3f_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3f, iv_circle_data_kesehatan_sio_no3f_qu, tv_error_data_kesehatan_sio_no3f_qu, cl_data_kesehatan_sio_no3f_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3g, iv_circle_data_kesehatan_sio_no3g_qu, tv_error_data_kesehatan_sio_no3g_qu, cl_data_kesehatan_sio_no3g_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3g, iv_circle_data_kesehatan_sio_no3g_qu, tv_error_data_kesehatan_sio_no3g_qu, cl_data_kesehatan_sio_no3g_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3h, iv_circle_data_kesehatan_sio_no3h_qu, tv_error_data_kesehatan_sio_no3h_qu, cl_data_kesehatan_sio_no3h_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3h, iv_circle_data_kesehatan_sio_no3h_qu, tv_error_data_kesehatan_sio_no3h_qu, cl_data_kesehatan_sio_no3h_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3i, iv_circle_data_kesehatan_sio_no3i_qu, tv_error_data_kesehatan_sio_no3i_qu, cl_data_kesehatan_sio_no3i_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3i, iv_circle_data_kesehatan_sio_no3i_qu, tv_error_data_kesehatan_sio_no3i_qu, cl_data_kesehatan_sio_no3i_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3j, iv_circle_data_kesehatan_sio_no3j_qu, tv_error_data_kesehatan_sio_no3j_qu, cl_data_kesehatan_sio_no3j_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3j, iv_circle_data_kesehatan_sio_no3j_qu, tv_error_data_kesehatan_sio_no3j_qu, cl_data_kesehatan_sio_no3j_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3k, iv_circle_data_kesehatan_sio_no3k_qu, tv_error_data_kesehatan_sio_no3k_qu, cl_data_kesehatan_sio_no3k_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3k, iv_circle_data_kesehatan_sio_no3k_qu, tv_error_data_kesehatan_sio_no3k_qu, cl_data_kesehatan_sio_no3k_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3l, iv_circle_data_kesehatan_sio_no3l_qu, tv_error_data_kesehatan_sio_no3l_qu, cl_data_kesehatan_sio_no3l_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3l, iv_circle_data_kesehatan_sio_no3l_qu, tv_error_data_kesehatan_sio_no3l_qu, cl_data_kesehatan_sio_no3l_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3m, iv_circle_data_kesehatan_sio_no3m_qu, tv_error_data_kesehatan_sio_no3m_qu, cl_data_kesehatan_sio_no3m_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3m, iv_circle_data_kesehatan_sio_no3m_qu, tv_error_data_kesehatan_sio_no3m_qu, cl_data_kesehatan_sio_no3m_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3n, iv_circle_data_kesehatan_sio_no3n_qu, tv_error_data_kesehatan_sio_no3n_qu, cl_data_kesehatan_sio_no3n_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3n, iv_circle_data_kesehatan_sio_no3n_qu, tv_error_data_kesehatan_sio_no3n_qu, cl_data_kesehatan_sio_no3n_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3o, iv_circle_data_kesehatan_sio_no3o_qu, tv_error_data_kesehatan_sio_no3o_qu, cl_data_kesehatan_sio_no3o_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3o, iv_circle_data_kesehatan_sio_no3o_qu, tv_error_data_kesehatan_sio_no3o_qu, cl_data_kesehatan_sio_no3o_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno3p, iv_circle_data_kesehatan_sio_no3p_qu, tv_error_data_kesehatan_sio_no3p_qu, cl_data_kesehatan_sio_no3p_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno3p, iv_circle_data_kesehatan_sio_no3p_qu, tv_error_data_kesehatan_sio_no3p_qu, cl_data_kesehatan_sio_no3p_qu, getContext())) {
            ready = false;
        }
//        if (!Method_Validator.onEmptyRadioText(option_grup_sio_ppno4, essay_sio_no4, iv_circle_data_kesehatan_sio_no4_qu, tv_error_data_kesehatan_sio_no4_qu, cl_data_kesehatan_sio_no4_qu, getContext())) {
//            ready = false;
//        }
//        if (!Method_Validator.onEmptyRadioText(option_grup_sio_ttno4, essay_sio_no4, iv_circle_data_kesehatan_sio_no4_qu, tv_error_data_kesehatan_sio_no4_qu, cl_data_kesehatan_sio_no4_qu, getContext())) {
//            ready = false;
//        }
//        if (!Method_Validator.onEmptyRadioText(option_grup_sio_ppno5, essay_sio_no5, iv_circle_data_kesehatan_sio_no5_qu, tv_error_data_kesehatan_sio_no5_qu, cl_data_kesehatan_sio_no5_qu, getContext())) {
//            ready = false;
//        }
//        if (!Method_Validator.onEmptyRadioText(option_grup_sio_ttno5, essay_sio_no5, iv_circle_data_kesehatan_sio_no5_qu, tv_error_data_kesehatan_sio_no5_qu, cl_data_kesehatan_sio_no5_qu, getContext())) {
//            ready = false;
//        }
//        if (!Method_Validator.onEmptyRadioText(option_grup_sio_ppno6, essay_sio_no6, iv_circle_data_kesehatan_sio_no6_qu, tv_error_data_kesehatan_sio_no6_qu, cl_data_kesehatan_sio_no6_qu, getContext())) {
//            ready = false;
//        }
//        if (!Method_Validator.onEmptyRadioText(option_grup_sio_ttno6, essay_sio_no6, iv_circle_data_kesehatan_sio_no6_qu, tv_error_data_kesehatan_sio_no6_qu, cl_data_kesehatan_sio_no6_qu, getContext())) {
//            ready = false;
//        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno4, iv_circle_data_kesehatan_sio_no4_qu, tv_error_data_kesehatan_sio_no4_qu, cl_data_kesehatan_sio_no4_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno4, iv_circle_data_kesehatan_sio_no4_qu, tv_error_data_kesehatan_sio_no4_qu, cl_data_kesehatan_sio_no4_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno5, iv_circle_data_kesehatan_sio_no5_qu, tv_error_data_kesehatan_sio_no5_qu, cl_data_kesehatan_sio_no5_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno5, iv_circle_data_kesehatan_sio_no5_qu, tv_error_data_kesehatan_sio_no5_qu, cl_data_kesehatan_sio_no5_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ppno6, iv_circle_data_kesehatan_sio_no6_qu, tv_error_data_kesehatan_sio_no6_qu, cl_data_kesehatan_sio_no6_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(option_grup_sio_ttno6, iv_circle_data_kesehatan_sio_no6_qu, tv_error_data_kesehatan_sio_no6_qu, cl_data_kesehatan_sio_no6_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyText(essay_sio_kg_ppno7, essay_sio_cm_ppno7, iv_circle_data_kesehatan_sio_no7_qu, tv_error_data_kesehatan_sio_no7_qu, cl_data_kesehatan_sio_no7_qu, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyText(essay_sio_kg_ttno7, essay_sio_cm_ttno7, iv_circle_data_kesehatan_sio_no7_qu, tv_error_data_kesehatan_sio_no7_qu, cl_data_kesehatan_sio_no7_qu, getContext())) {
            ready = false;
        }

        return ready;
    }

    private class generalTextWatcher implements TextWatcher {
        private View view;

        generalTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                //edittext
                case R.id.essay_sio_no1:
                    if (essay_sio_no1.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_sio_no1, iv_circle_data_kesehatan_sio_no1_qu, tv_error_data_kesehatan_sio_no1_qu, getContext());
                    }
                    break;
                case R.id.essay_sio_no2:
                    if (essay_sio_no2.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_sio_no2, iv_circle_data_kesehatan_sio_no2_qu, tv_error_data_kesehatan_sio_no2_qu, getContext());
                    }
                    break;
                case R.id.essay_sio_no4:
                    if (essay_sio_no4.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_sio_no4, iv_circle_data_kesehatan_sio_no4_qu, tv_error_data_kesehatan_sio_no4_qu, getContext());
                    }
                    break;
                case R.id.essay_sio_no5:
                    if (essay_sio_no5.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_sio_no5, iv_circle_data_kesehatan_sio_no5_qu, tv_error_data_kesehatan_sio_no5_qu, getContext());
                    }
                    break;
                case R.id.essay_sio_no6:
                    if (essay_sio_no6.isEnabled()) {
                        Method_Validator.ValEditWatcher(essay_sio_no6, iv_circle_data_kesehatan_sio_no6_qu, tv_error_data_kesehatan_sio_no6_qu, getContext());
                    }
                    break;
                case R.id.essay_sio_cm_ppno7:
                    if (essay_sio_cm_ppno7.isEnabled()) {
                        Method_Validator.ValEditWatcherSIO(essay_sio_cm_ppno7, essay_sio_cm_ttno7, essay_sio_kg_ppno7, essay_sio_kg_ttno7, iv_circle_data_kesehatan_sio_no7_qu, tv_error_data_kesehatan_sio_no7_qu, getContext());
                    }
                    break;
                case R.id.essay_sio_cm_ttno7:
                    if (essay_sio_cm_ttno7.isEnabled()) {
                        Method_Validator.ValEditWatcherSIO(essay_sio_cm_ppno7, essay_sio_cm_ttno7, essay_sio_kg_ppno7, essay_sio_kg_ttno7, iv_circle_data_kesehatan_sio_no7_qu, tv_error_data_kesehatan_sio_no7_qu, getContext());
                    }
                    break;
                case R.id.essay_sio_kg_ppno7:
                    if (essay_sio_kg_ppno7.isEnabled()) {
                        Method_Validator.ValEditWatcherSIO(essay_sio_cm_ppno7, essay_sio_cm_ttno7, essay_sio_kg_ppno7, essay_sio_kg_ttno7, iv_circle_data_kesehatan_sio_no7_qu, tv_error_data_kesehatan_sio_no7_qu, getContext());
                    }
                    break;
                case R.id.essay_sio_kg_ttno7:
                    if (essay_sio_kg_ttno7.isEnabled()) {
                        Method_Validator.ValEditWatcherSIO(essay_sio_cm_ppno7, essay_sio_cm_ttno7, essay_sio_kg_ppno7, essay_sio_kg_ttno7, iv_circle_data_kesehatan_sio_no7_qu, tv_error_data_kesehatan_sio_no7_qu, getContext());
                    }
                    break;

            }

        }
    }

    private void onClickLanjut() {
//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setTitle("Melakukan perpindahan halaman");
//        progressDialog.setMessage("Mohon Menunggu...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
//        Runnable progressRunnable = new Runnable() {
//            @Override
//            public void run() {
//                MethodSupport.active_view(0, btn_lanjut);
//                MethodSupport.active_view(0, iv_next);
//                ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
                load();
                MyTask task = new MyTask();
                task.execute();

//
//                ((E_MainActivity) getActivity()).onSave();
//                View cl_child_qu = getActivity().findViewById(R.id.cl_child_qu);
//                File file_bitmap = new File(
//                        getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//                if (!file_bitmap.exists()) {
//                    file_bitmap.mkdirs();
//                }
//                String fileName = "QU_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
//                Context context = getActivity();
//                MethodSupport.onCreateBitmap(cl_child_qu, scroll_qu, file_bitmap, fileName, context);
//                if (new E_Select(getContext()).selectLinkNonLink(me.getUsulanAsuransiModel().getKd_produk_ua()) == 17) {
//                    ((E_MainActivity) getActivity()).setFragment(new E_ProfileResikoFragment(), getResources().getString(R.string.profil_nasabah));
//                } else {
//                    Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
//                }
//                progressDialog.cancel();

//            }
//        };
////
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 1000);


    }

    private class MyTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_QUESTIONAIRE_SIO);
            View cl_child_qu = getActivity().findViewById(R.id.cl_child_qu);
            File file_bitmap = new File(
                    getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
            if (!file_bitmap.exists()) {
                file_bitmap.mkdirs();
            }
            String fileName = "QU_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
            Context context = getActivity();
//            MethodSupport.onCreateBitmap(cl_child_qu, scroll_qu, file_bitmap, fileName, context);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (new E_Select(getContext()).selectLinkNonLink(me.getUsulanAsuransiModel().getKd_produk_ua()) == 17) {
                ((E_MainActivity) getActivity()).setFragment(new E_ProfileResikoFragment(), getResources().getString(R.string.profil_nasabah));
            } else {
                Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
            }
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickBack() {

        load();
        ((E_MainActivity) getActivity()).setFragment(new E_DokumenPendukungFragment(), getResources().getString(R.string.dokumen_pendukung));

//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setTitle("Melakukan perpindahan halaman");
//        progressDialog.setMessage("Mohon Menunggu...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
//        Runnable progressRunnable = new Runnable() {
//
//            @Override
//            public void run() {
//        MethodSupport.active_view(0, btn_kembali);
//        MethodSupport.active_view(0, iv_prev);
//        ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
//
//        load();
//        ((E_MainActivity) getActivity()).setFragment(new E_DokumenPendukungFragment(), getResources().getString(R.string.dokumen_pendukung));
//                progressDialog.cancel();
//            }
//        };
//
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 1000);
    }

    private ProgressDialog dialogmessage;

    public void startProgress() {
        dialogmessage = new ProgressDialog(getActivity());
        dialogmessage.setMessage("Mohon Menunggu ");
        dialogmessage.setTitle("Sedang Melakukan Perpindahan Halaman ...");
        dialogmessage.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialogmessage.show();
        dialogmessage.setCancelable(false);
        btn_lanjut.setEnabled(false);
        iv_next.setEnabled(false);
        iv_prev.setEnabled(false);
        btn_kembali.setEnabled(false);
        dialogmessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                btn_lanjut.setEnabled(true);
                iv_next.setEnabled(true);
                iv_prev.setEnabled(true);
                btn_kembali.setEnabled(true);
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (dialogmessage.getProgress() <= dialogmessage
                            .getMax()) {
                        Thread.sleep(10);
                        handle.sendMessage(handle.obtainMessage());
                        if (dialogmessage.getProgress() == dialogmessage
                                .getMax()) {
                            dialogmessage.dismiss();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    Handler handle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            dialogmessage.incrementProgressBy(1);
        }
    };

    //PIKIRKAN TENTANG LOAD
    private void load() {
        int QUESTION_ID = 80;
        int OPTION_ORDER = 0;
        int OPTION_GROUP = 0;
        String QUESTION_VALID_DATE = "01/08/2014";
        if (!list.isEmpty()) {
            me.getModelUpdateQuisioner().getListQuisioner().clear();
            list.clear();

        }
        for (int i = 0; i < ListOfView.size(); i++) {
            int QUESTION_TYPE_ID = 12;
            int OPTION_TYPE = 0;
            String ANSWER = "";
            String strres_1 = "";
            String substrres_1 = "";
            String strview = ListOfView.get(i).toString();// untuk dapatkan
            // jenis viewnya
            Resources res = getContext().getResources();// untuk dapatkan nama
            // resource
            int id = ListOfView.get(i).getId();
            String strres = res.getResourceName(id);
            String substrres = strres.substring(strres.lastIndexOf("no"));

            if (i != 0) {
                int id1 = ListOfView.get(i - 1).getId();
                strres_1 = res.getResourceName(id1);
                substrres_1 = strres_1.substring(strres_1.lastIndexOf("no"));
            }
            ModelMst_Answer msa = null;
            // jika ditemukan nomornya sudah berbeda maka question IDnya
            // bertambah
            if (!(substrres).equals(substrres_1)) {
                QUESTION_ID++;
            }
            if (QUESTION_ID == 83) {
                QUESTION_ID = 84;
            }

            if (strview.toUpperCase().contains("EDITTEXT")) {
                //benerin yang PP dan TT
                OPTION_GROUP = 0;
                OPTION_ORDER = 1;
                OPTION_TYPE = 0;
                ANSWER = ((EditText) ListOfView.get(i)).getText().toString();

                if (strres.contains("_cm_pp")) {
                    OPTION_ORDER = 1;
                    OPTION_TYPE = 4;
                    OPTION_GROUP = 1;
                } else if (strres.contains("_kg_pp")) {
                    OPTION_ORDER = 2;
                    OPTION_TYPE = 4;
                    OPTION_GROUP = 1;
                } else if (strres.contains("_cm_tt")) {
                    OPTION_ORDER = 1;
                    OPTION_TYPE = 4;
                    OPTION_GROUP = 2;
                } else if (strres.contains("_kg_tt")) {
                    OPTION_ORDER = 2;
                    OPTION_TYPE = 4;
                    OPTION_GROUP = 2;
                }

                msa = new ModelMst_Answer(i, QUESTION_VALID_DATE, QUESTION_TYPE_ID, QUESTION_ID,
                        OPTION_TYPE, OPTION_GROUP, OPTION_ORDER, ANSWER);
            }

            if (strview.toUpperCase().contains("RADIOBUTTON")) {
                OPTION_GROUP = 1;
                OPTION_TYPE = 1;
                if (strres.contains("_y_")) {
                    OPTION_ORDER = 1;
                } else {
                    OPTION_ORDER = 2;
                }

                if (strres.contains("_sio_pp")) {
                    OPTION_GROUP = 1;
                } else {
                    OPTION_GROUP = 2;
                }
                if (((RadioButton) ListOfView.get(i)).isChecked()) {
                    ANSWER = "1";
                } else {
                    ANSWER = "0";
                }
                msa = new ModelMst_Answer(i, QUESTION_VALID_DATE, QUESTION_TYPE_ID, QUESTION_ID,
                        OPTION_TYPE, OPTION_GROUP, OPTION_ORDER, ANSWER);
            }

            list.add(msa);
        }

        System.out.println(list.size() + "&" + ListOfView.size());
        me.getModelUpdateQuisioner().setListQuisioner(list);
        me.getModelUpdateQuisioner().setValidation(validation());
        me.getModelID().setVal_qu(validation());
        E_MainActivity.e_bundle.putParcelable(getString(R.string.modelespaj), me);

    }

    private void restore() {
        try {
            list = new ArrayList<ModelMst_Answer>();
            list = me.getModelUpdateQuisioner().getListQuisioner();
            if (!list.isEmpty()) {
                for (int i = 0; i < ListOfView.size(); i++) {
                    String strview = ListOfView.get(i).toString();
                    if (strview.toUpperCase().contains("EDITTEXT")) {
                        ((EditText) ListOfView.get(i)).setText(list.get(i).getAnswer());
                        System.out.println(((EditText) ListOfView.get(0)).getText().toString());
                    }
                    if (strview.toUpperCase().contains("RADIOBUTTON")) {
                        if (list.get(i).getAnswer().equals("1")) {
                            ((RadioButton) ListOfView.get(i)).setChecked(true);
                        } else {
                            ((RadioButton) ListOfView.get(i)).setChecked(false);
                        }
                    }
                }
            } else {
                for (int i = 0; i < ListOfView.size(); i++) {
                    String strview = ListOfView.get(i).toString();
                    if (strview.toUpperCase().contains("EDITTEXT")) {
                        ((EditText) ListOfView.get(i)).setText("");
                    }
                    if (strview.toUpperCase().contains("RADIOBUTTON")) {
                        ((RadioButton) ListOfView.get(i)).setChecked(false);
                    }
                }
            }

            if (!me.getValidation()) {
                validation();
            }
            if (me.getModelID().getFlag_aktif() == 1) {
                MethodSupport.disable(false, cl_child_qu);
            }
        } catch (IndexOutOfBoundsException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }

    }

    @BindView(R.id.tv_kuisioner_sio_spaj)
    TextView tv_kuisioner_sio_spaj;
    @BindView(R.id.tv_data_kesehatan_sio_qu)
    TextView tv_data_kesehatan_sio_qu;
    @BindView(R.id.tv_no1_sio_qu)
    TextView tv_no1_sio_qu;
    @BindView(R.id.tv_no1_sio_isi_qu)
    TextView tv_no1_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no1_qu)
    TextView tv_error_data_kesehatan_sio_no1_qu;
    @BindView(R.id.tv_no2_sio_qu)
    TextView tv_no2_sio_qu;
    @BindView(R.id.tv_no2_sio_isi_qu)
    TextView tv_no2_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no2_qu)
    TextView tv_error_data_kesehatan_sio_no2_qu;
    @BindView(R.id.tv_no3_sio_qu)
    TextView tv_no3_sio_qu;
    @BindView(R.id.tv_no3_sio_isi_qu)
    TextView tv_no3_sio_isi_qu;
    @BindView(R.id.tv_no3a_sio_qu)
    TextView tv_no3a_sio_qu;
    @BindView(R.id.tv_no3a_sio_isi_qu)
    TextView tv_no3a_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3a_qu)
    TextView tv_error_data_kesehatan_sio_no3a_qu;
    @BindView(R.id.tv_no3b_sio_qu)
    TextView tv_no3b_sio_qu;
    @BindView(R.id.tv_no3b_sio_isi_qu)
    TextView tv_no3b_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3b_qu)
    TextView tv_error_data_kesehatan_sio_no3b_qu;
    @BindView(R.id.tv_no3c_sio_qu)
    TextView tv_no3c_sio_qu;
    @BindView(R.id.tv_no3c_sio_isi_qu)
    TextView tv_no3c_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3c_qu)
    TextView tv_error_data_kesehatan_sio_no3c_qu;
    @BindView(R.id.tv_no3d_sio_qu)
    TextView tv_no3d_sio_qu;
    @BindView(R.id.tv_no3d_sio_isi_qu)
    TextView tv_no3d_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3d_qu)
    TextView tv_error_data_kesehatan_sio_no3d_qu;
    @BindView(R.id.tv_no3e_sio_qu)
    TextView tv_no3e_sio_qu;
    @BindView(R.id.tv_no3e_sio_isi_qu)
    TextView tv_no3e_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3e_qu)
    TextView tv_error_data_kesehatan_sio_no3e_qu;
    @BindView(R.id.tv_no3f_sio_qu)
    TextView tv_no3f_sio_qu;
    @BindView(R.id.tv_no3f_sio_isi_qu)
    TextView tv_no3f_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3f_qu)
    TextView tv_error_data_kesehatan_sio_no3f_qu;
    @BindView(R.id.tv_no3g_sio_qu)
    TextView tv_no3g_sio_qu;
    @BindView(R.id.tv_no3g_sio_isi_qu)
    TextView tv_no3g_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3g_qu)
    TextView tv_error_data_kesehatan_sio_no3g_qu;
    @BindView(R.id.tv_no3h_sio_qu)
    TextView tv_no3h_sio_qu;
    @BindView(R.id.tv_no3h_sio_isi_qu)
    TextView tv_no3h_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3h_qu)
    TextView tv_error_data_kesehatan_sio_no3h_qu;
    @BindView(R.id.tv_no3i_sio_qu)
    TextView tv_no3i_sio_qu;
    @BindView(R.id.tv_no3i_sio_isi_qu)
    TextView tv_no3i_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3i_qu)
    TextView tv_error_data_kesehatan_sio_no3i_qu;
    @BindView(R.id.tv_no3j_sio_qu)
    TextView tv_no3j_sio_qu;
    @BindView(R.id.tv_no3j_sio_isi_qu)
    TextView tv_no3j_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3j_qu)
    TextView tv_error_data_kesehatan_sio_no3j_qu;
    @BindView(R.id.tv_no3k_sio_qu)
    TextView tv_no3k_sio_qu;
    @BindView(R.id.tv_no3k_sio_isi_qu)
    TextView tv_no3k_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3k_qu)
    TextView tv_error_data_kesehatan_sio_no3k_qu;
    @BindView(R.id.tv_no3l_sio_qu)
    TextView tv_no3l_sio_qu;
    @BindView(R.id.tv_no3l_sio_isi_qu)
    TextView tv_no3l_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3l_qu)
    TextView tv_error_data_kesehatan_sio_no3l_qu;
    @BindView(R.id.tv_no3m_sio_qu)
    TextView tv_no3m_sio_qu;
    @BindView(R.id.tv_no3m_sio_isi_qu)
    TextView tv_no3m_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3m_qu)
    TextView tv_error_data_kesehatan_sio_no3m_qu;
    @BindView(R.id.tv_no3n_sio_qu)
    TextView tv_no3n_sio_qu;
    @BindView(R.id.tv_no3n_sio_isi_qu)
    TextView tv_no3n_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3n_qu)
    TextView tv_error_data_kesehatan_sio_no3n_qu;
    @BindView(R.id.tv_no3o_sio_qu)
    TextView tv_no3o_sio_qu;
    @BindView(R.id.tv_no3o_sio_isi_qu)
    TextView tv_no3o_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3o_qu)
    TextView tv_error_data_kesehatan_sio_no3o_qu;
    @BindView(R.id.tv_no3p_sio_qu)
    TextView tv_no3p_sio_qu;
    @BindView(R.id.tv_no3p_sio_isi_qu)
    TextView tv_no3p_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3p_qu)
    TextView tv_error_data_kesehatan_sio_no3p_qu;
    @BindView(R.id.tv_no3ket_sio_qu)
    TextView tv_no3ket_sio_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no3ket_qu)
    TextView tv_error_data_kesehatan_sio_no3ket_qu;
    @BindView(R.id.tv_no4_sio_qu)
    TextView tv_no4_sio_qu;
    @BindView(R.id.tv_no4_sio_isi_qu)
    TextView tv_no4_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no4_qu)
    TextView tv_error_data_kesehatan_sio_no4_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no5_qu)
    ImageView iv_circle_data_kesehatan_sio_no5_qu;
    @BindView(R.id.tv_no5_sio_qu)
    TextView tv_no5_sio_qu;
    @BindView(R.id.tv_no5_sio_isi_qu)
    TextView tv_no5_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no5_qu)
    TextView tv_error_data_kesehatan_sio_no5_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no6_qu)
    ImageView iv_circle_data_kesehatan_sio_no6_qu;
    @BindView(R.id.tv_no6_sio_qu)
    TextView tv_no6_sio_qu;
    @BindView(R.id.tv_no6_sio_isi_qu)
    TextView tv_no6_sio_isi_qu;
    @BindView(R.id.tv_no7_sio_qu)
    TextView tv_no7_sio_qu;
    @BindView(R.id.tv_no7_sio_isi_qu)
    TextView tv_no7_sio_isi_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no6_qu)
    TextView tv_error_data_kesehatan_sio_no6_qu;
    @BindView(R.id.tv_error_data_kesehatan_sio_no7_qu)
    TextView tv_error_data_kesehatan_sio_no7_qu;

    @BindView(R.id.cl_kuisioner_sio_qu)
    ConstraintLayout cl_kuisioner_sio_qu;
    @BindView(R.id.cl_form_data_kesehatan_sio_qu)
    ConstraintLayout cl_form_data_kesehatan_sio_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no1_qu)
    ConstraintLayout cl_data_kesehatan_sio_no1_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no2_qu)
    ConstraintLayout cl_data_kesehatan_sio_no2_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3b_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3b_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3c_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3c_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3d_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3d_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3e_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3e_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3f_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3f_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3g_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3g_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3h_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3h_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3i_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3i_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3j_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3j_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3k_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3k_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3l_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3l_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3m_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3m_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3n_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3n_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3o_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3o_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3p_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3p_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no3ket_qu)
    ConstraintLayout cl_data_kesehatan_sio_no3ket_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no4_qu)
    ConstraintLayout cl_data_kesehatan_sio_no4_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no5_qu)
    ConstraintLayout cl_data_kesehatan_sio_no5_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no6_qu)
    ConstraintLayout cl_data_kesehatan_sio_no6_qu;
    @BindView(R.id.cl_data_kesehatan_sio_no7_qu)
    ConstraintLayout cl_data_kesehatan_sio_no7_qu;
    @BindView(R.id.cl_child_qu)
    ConstraintLayout cl_child_qu;
//    @BindView(R.id.cl_btn_prev_qu)
//    ConstraintLayout cl_btn_prev_qu;
//    @BindView(R.id.cl_btn_next_qu)
//    ConstraintLayout cl_btn_next_qu;

    @BindView(R.id.iv_circle_data_kesehatan_sio_no1_qu)
    ImageView iv_circle_data_kesehatan_sio_no1_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no2_qu)
    ImageView iv_circle_data_kesehatan_sio_no2_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3_qu)
    ImageView iv_circle_data_kesehatan_sio_no3_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3b_qu)
    ImageView iv_circle_data_kesehatan_sio_no3b_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3c_qu)
    ImageView iv_circle_data_kesehatan_sio_no3c_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3d_qu)
    ImageView iv_circle_data_kesehatan_sio_no3d_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3e_qu)
    ImageView iv_circle_data_kesehatan_sio_no3e_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3f_qu)
    ImageView iv_circle_data_kesehatan_sio_no3f_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3g_qu)
    ImageView iv_circle_data_kesehatan_sio_no3g_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3h_qu)
    ImageView iv_circle_data_kesehatan_sio_no3h_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3i_qu)
    ImageView iv_circle_data_kesehatan_sio_no3i_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3j_qu)
    ImageView iv_circle_data_kesehatan_sio_no3j_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3k_qu)
    ImageView iv_circle_data_kesehatan_sio_no3k_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3l_qu)
    ImageView iv_circle_data_kesehatan_sio_no3l_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3m_qu)
    ImageView iv_circle_data_kesehatan_sio_no3m_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3n_qu)
    ImageView iv_circle_data_kesehatan_sio_no3n_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3o_qu)
    ImageView iv_circle_data_kesehatan_sio_no3o_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3p_qu)
    ImageView iv_circle_data_kesehatan_sio_no3p_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no3ket_qu)
    ImageView iv_circle_data_kesehatan_sio_no3ket_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no4_qu)
    ImageView iv_circle_data_kesehatan_sio_no4_qu;
    @BindView(R.id.iv_circle_data_kesehatan_sio_no7_qu)
    ImageView iv_circle_data_kesehatan_sio_no7_qu;

    @BindView(R.id.ll_cpp_no1_sio_qu)
    LinearLayout ll_cpp_no1_sio_qu;
    @BindView(R.id.ll_ctt_no1_sio_qu)
    LinearLayout ll_ctt_no1_sio_qu;
    @BindView(R.id.ll_cpp_no2_sio_qu)
    LinearLayout ll_cpp_no2_sio_qu;
    @BindView(R.id.ll_ctt_no2_sio_qu)
    LinearLayout ll_ctt_no2_sio_qu;
    @BindView(R.id.ll_cpp_no3a_sio_qu)
    LinearLayout ll_cpp_no3a_sio_qu;
    @BindView(R.id.ll_ctt_no3a_sio_qu)
    LinearLayout ll_ctt_no3a_sio_qu;
    @BindView(R.id.ll_cpp_no3b_sio_qu)
    LinearLayout ll_cpp_no3b_sio_qu;
    @BindView(R.id.ll_ctt_no3b_sio_qu)
    LinearLayout ll_ctt_no3b_sio_qu;
    @BindView(R.id.ll_cpp_no3c_sio_qu)
    LinearLayout ll_cpp_no3c_sio_qu;
    @BindView(R.id.ll_ctt_no3c_sio_qu)
    LinearLayout ll_ctt_no3c_sio_qu;
    @BindView(R.id.ll_cpp_no3d_sio_qu)
    LinearLayout ll_cpp_no3d_sio_qu;
    @BindView(R.id.ll_ctt_no3d_sio_qu)
    LinearLayout ll_ctt_no3d_sio_qu;
    @BindView(R.id.ll_cpp_no3e_sio_qu)
    LinearLayout ll_cpp_no3e_sio_qu;
    @BindView(R.id.ll_ctt_no3e_sio_qu)
    LinearLayout ll_ctt_no3e_sio_qu;
    @BindView(R.id.ll_cpp_no3f_sio_qu)
    LinearLayout ll_cpp_no3f_sio_qu;
    @BindView(R.id.ll_ctt_no3f_sio_qu)
    LinearLayout ll_ctt_no3f_sio_qu;
    @BindView(R.id.ll_cpp_no3g_sio_qu)
    LinearLayout ll_cpp_no3g_sio_qu;
    @BindView(R.id.ll_ctt_no3g_sio_qu)
    LinearLayout ll_ctt_no3g_sio_qu;
    @BindView(R.id.ll_cpp_no3h_sio_qu)
    LinearLayout ll_cpp_no3h_sio_qu;
    @BindView(R.id.ll_ctt_no3h_sio_qu)
    LinearLayout ll_ctt_no3h_sio_qu;
    @BindView(R.id.ll_cpp_no3i_sio_qu)
    LinearLayout ll_cpp_no3i_sio_qu;
    @BindView(R.id.ll_ctt_no3i_sio_qu)
    LinearLayout ll_ctt_no3i_sio_qu;
    @BindView(R.id.ll_cpp_no3j_sio_qu)
    LinearLayout ll_cpp_no3j_sio_qu;
    @BindView(R.id.ll_ctt_no3j_sio_qu)
    LinearLayout ll_ctt_no3j_sio_qu;
    @BindView(R.id.ll_cpp_no3k_sio_qu)
    LinearLayout ll_cpp_no3k_sio_qu;
    @BindView(R.id.ll_ctt_no3k_sio_qu)
    LinearLayout ll_ctt_no3k_sio_qu;
    @BindView(R.id.ll_cpp_no3l_sio_qu)
    LinearLayout ll_cpp_no3l_sio_qu;
    @BindView(R.id.ll_ctt_no3l_sio_qu)
    LinearLayout ll_ctt_no3l_sio_qu;
    @BindView(R.id.ll_cpp_no3m_sio_qu)
    LinearLayout ll_cpp_no3m_sio_qu;
    @BindView(R.id.ll_ctt_no3m_sio_qu)
    LinearLayout ll_ctt_no3m_sio_qu;
    @BindView(R.id.ll_cpp_no3n_sio_qu)
    LinearLayout ll_cpp_no3n_sio_qu;
    @BindView(R.id.ll_ctt_no3n_sio_qu)
    LinearLayout ll_ctt_no3n_sio_qu;
    @BindView(R.id.ll_cpp_no3o_sio_qu)
    LinearLayout ll_cpp_no3o_sio_qu;
    @BindView(R.id.ll_ctt_no3o_sio_qu)
    LinearLayout ll_ctt_no3o_sio_qu;
    @BindView(R.id.ll_cpp_no3p_sio_qu)
    LinearLayout ll_cpp_no3p_sio_qu;
    @BindView(R.id.ll_ctt_no3p_sio_qu)
    LinearLayout ll_ctt_no3p_sio_qu;
    @BindView(R.id.ll_cpp_no4_sio_qu)
    LinearLayout ll_cpp_no4_sio_qu;
    @BindView(R.id.ll_ctt_no4_sio_qu)
    LinearLayout ll_ctt_no4_sio_qu;
    @BindView(R.id.ll_cpp_no5_sio_qu)
    LinearLayout ll_cpp_no5_sio_qu;
    @BindView(R.id.ll_ctt_no5_sio_qu)
    LinearLayout ll_ctt_no5_sio_qu;
    @BindView(R.id.ll_cpp_no6_sio_qu)
    LinearLayout ll_cpp_no6_sio_qu;
    @BindView(R.id.ll_ctt_no6_sio_qu)
    LinearLayout ll_ctt_no6_sio_qu;
    @BindView(R.id.ll_cpp_no7_sio_qu)
    LinearLayout ll_cpp_no7_sio_qu;
    @BindView(R.id.ll_ctt_no7_sio_qu)
    LinearLayout ll_ctt_no7_sio_qu;

    @BindView(R.id.option_grup_sio_ppno1)
    RadioGroup option_grup_sio_ppno1;
    @BindView(R.id.option_grup_sio_ttno1)
    RadioGroup option_grup_sio_ttno1;
    @BindView(R.id.option_grup_sio_ppno2)
    RadioGroup option_grup_sio_ppno2;
    @BindView(R.id.option_grup_sio_ttno2)
    RadioGroup option_grup_sio_ttno2;
    @BindView(R.id.option_grup_sio_ppno3a)
    RadioGroup option_grup_sio_ppno3a;
    @BindView(R.id.option_grup_sio_ttno3a)
    RadioGroup option_grup_sio_ttno3a;
    @BindView(R.id.option_grup_sio_ppno3b)
    RadioGroup option_grup_sio_ppno3b;
    @BindView(R.id.option_grup_sio_ttno3b)
    RadioGroup option_grup_sio_ttno3b;
    @BindView(R.id.option_grup_sio_ppno3c)
    RadioGroup option_grup_sio_ppno3c;
    @BindView(R.id.option_grup_sio_ttno3c)
    RadioGroup option_grup_sio_ttno3c;
    @BindView(R.id.option_grup_sio_ppno3d)
    RadioGroup option_grup_sio_ppno3d;
    @BindView(R.id.option_grup_sio_ttno3d)
    RadioGroup option_grup_sio_ttno3d;
    @BindView(R.id.option_grup_sio_ppno3e)
    RadioGroup option_grup_sio_ppno3e;
    @BindView(R.id.option_grup_sio_ttno3e)
    RadioGroup option_grup_sio_ttno3e;
    @BindView(R.id.option_grup_sio_ppno3f)
    RadioGroup option_grup_sio_ppno3f;
    @BindView(R.id.option_grup_sio_ttno3f)
    RadioGroup option_grup_sio_ttno3f;
    @BindView(R.id.option_grup_sio_ppno3g)
    RadioGroup option_grup_sio_ppno3g;
    @BindView(R.id.option_grup_sio_ttno3g)
    RadioGroup option_grup_sio_ttno3g;
    @BindView(R.id.option_grup_sio_ppno3h)
    RadioGroup option_grup_sio_ppno3h;
    @BindView(R.id.option_grup_sio_ttno3h)
    RadioGroup option_grup_sio_ttno3h;
    @BindView(R.id.option_grup_sio_ppno3i)
    RadioGroup option_grup_sio_ppno3i;
    @BindView(R.id.option_grup_sio_ttno3i)
    RadioGroup option_grup_sio_ttno3i;
    @BindView(R.id.option_grup_sio_ppno3j)
    RadioGroup option_grup_sio_ppno3j;
    @BindView(R.id.option_grup_sio_ttno3j)
    RadioGroup option_grup_sio_ttno3j;
    @BindView(R.id.option_grup_sio_ppno3k)
    RadioGroup option_grup_sio_ppno3k;
    @BindView(R.id.option_grup_sio_ttno3k)
    RadioGroup option_grup_sio_ttno3k;
    @BindView(R.id.option_grup_sio_ppno3l)
    RadioGroup option_grup_sio_ppno3l;
    @BindView(R.id.option_grup_sio_ttno3l)
    RadioGroup option_grup_sio_ttno3l;
    @BindView(R.id.option_grup_sio_ppno3m)
    RadioGroup option_grup_sio_ppno3m;
    @BindView(R.id.option_grup_sio_ttno3m)
    RadioGroup option_grup_sio_ttno3m;
    @BindView(R.id.option_grup_sio_ppno3n)
    RadioGroup option_grup_sio_ppno3n;
    @BindView(R.id.option_grup_sio_ttno3n)
    RadioGroup option_grup_sio_ttno3n;
    @BindView(R.id.option_grup_sio_ppno3o)
    RadioGroup option_grup_sio_ppno3o;
    @BindView(R.id.option_grup_sio_ttno3o)
    RadioGroup option_grup_sio_ttno3o;
    @BindView(R.id.option_grup_sio_ppno3p)
    RadioGroup option_grup_sio_ppno3p;
    @BindView(R.id.option_grup_sio_ttno3p)
    RadioGroup option_grup_sio_ttno3p;
    @BindView(R.id.option_grup_sio_ppno4)
    RadioGroup option_grup_sio_ppno4;
    @BindView(R.id.option_grup_sio_ttno4)
    RadioGroup option_grup_sio_ttno4;
    @BindView(R.id.option_grup_sio_ppno5)
    RadioGroup option_grup_sio_ppno5;
    @BindView(R.id.option_grup_sio_ttno5)
    RadioGroup option_grup_sio_ttno5;
    @BindView(R.id.option_grup_sio_ppno6)
    RadioGroup option_grup_sio_ppno6;
    @BindView(R.id.option_grup_sio_ttno6)
    RadioGroup option_grup_sio_ttno6;

    @BindView(R.id.option_y_sio_ppno1)
    RadioButton option_y_sio_ppno1;
    @BindView(R.id.option_n_sio_ppno1)
    RadioButton option_n_sio_ppno1;
    @BindView(R.id.option_y_sio_ttno1)
    RadioButton option_y_sio_ttno1;
    @BindView(R.id.option_n_sio_ttno1)
    RadioButton option_n_sio_ttno1;
    @BindView(R.id.option_y_sio_ppno2)
    RadioButton option_y_sio_ppno2;
    @BindView(R.id.option_n_sio_ppno2)
    RadioButton option_n_sio_ppno2;
    @BindView(R.id.option_y_sio_ttno2)
    RadioButton option_y_sio_ttno2;
    @BindView(R.id.option_n_sio_ttno2)
    RadioButton option_n_sio_ttno2;
    @BindView(R.id.option_y_sio_ppno3a)
    RadioButton option_y_sio_ppno3a;
    @BindView(R.id.option_n_sio_ppno3a)
    RadioButton option_n_sio_ppno3a;
    @BindView(R.id.option_y_sio_ttno3a)
    RadioButton option_y_sio_ttno3a;
    @BindView(R.id.option_n_sio_ttno3a)
    RadioButton option_n_sio_ttno3a;
    @BindView(R.id.option_y_sio_ppno3b)
    RadioButton option_y_sio_ppno3b;
    @BindView(R.id.option_n_sio_ppno3b)
    RadioButton option_n_sio_ppno3b;
    @BindView(R.id.option_y_sio_ttno3b)
    RadioButton option_y_sio_ttno3b;
    @BindView(R.id.option_n_sio_ttno3b)
    RadioButton option_n_sio_ttno3b;
    @BindView(R.id.option_y_sio_ppno3c)
    RadioButton option_y_sio_ppno3c;
    @BindView(R.id.option_n_sio_ppno3c)
    RadioButton option_n_sio_ppno3c;
    @BindView(R.id.option_y_sio_ttno3c)
    RadioButton option_y_sio_ttno3c;
    @BindView(R.id.option_n_sio_ttno3c)
    RadioButton option_n_sio_ttno3c;
    @BindView(R.id.option_y_sio_ppno3d)
    RadioButton option_y_sio_ppno3d;
    @BindView(R.id.option_n_sio_ppno3d)
    RadioButton option_n_sio_ppno3d;
    @BindView(R.id.option_y_sio_ttno3d)
    RadioButton option_y_sio_ttno3d;
    @BindView(R.id.option_n_sio_ttno3d)
    RadioButton option_n_sio_ttno3d;
    @BindView(R.id.option_y_sio_ppno3e)
    RadioButton option_y_sio_ppno3e;
    @BindView(R.id.option_n_sio_ppno3e)
    RadioButton option_n_sio_ppno3e;
    @BindView(R.id.option_y_sio_ttno3e)
    RadioButton option_y_sio_ttno3e;
    @BindView(R.id.option_n_sio_ttno3e)
    RadioButton option_n_sio_ttno3e;
    @BindView(R.id.option_y_sio_ppno3f)
    RadioButton option_y_sio_ppno3f;
    @BindView(R.id.option_n_sio_ppno3f)
    RadioButton option_n_sio_ppno3f;
    @BindView(R.id.option_y_sio_ttno3f)
    RadioButton option_y_sio_ttno3f;
    @BindView(R.id.option_n_sio_ttno3f)
    RadioButton option_n_sio_ttno3f;
    @BindView(R.id.option_y_sio_ppno3g)
    RadioButton option_y_sio_ppno3g;
    @BindView(R.id.option_n_sio_ppno3g)
    RadioButton option_n_sio_ppno3g;
    @BindView(R.id.option_y_sio_ttno3g)
    RadioButton option_y_sio_ttno3g;
    @BindView(R.id.option_n_sio_ttno3g)
    RadioButton option_n_sio_ttno3g;
    @BindView(R.id.option_y_sio_ppno3h)
    RadioButton option_y_sio_ppno3h;
    @BindView(R.id.option_n_sio_ppno3h)
    RadioButton option_n_sio_ppno3h;
    @BindView(R.id.option_y_sio_ttno3h)
    RadioButton option_y_sio_ttno3h;
    @BindView(R.id.option_n_sio_ttno3h)
    RadioButton option_n_sio_ttno3h;
    @BindView(R.id.option_y_sio_ppno3i)
    RadioButton option_y_sio_ppno3i;
    @BindView(R.id.option_n_sio_ppno3i)
    RadioButton option_n_sio_ppno3i;
    @BindView(R.id.option_y_sio_ttno3i)
    RadioButton option_y_sio_ttno3i;
    @BindView(R.id.option_n_sio_ttno3i)
    RadioButton option_n_sio_ttno3i;
    @BindView(R.id.option_y_sio_ppno3j)
    RadioButton option_y_sio_ppno3j;
    @BindView(R.id.option_n_sio_ppno3j)
    RadioButton option_n_sio_ppno3j;
    @BindView(R.id.option_y_sio_ttno3j)
    RadioButton option_y_sio_ttno3j;
    @BindView(R.id.option_n_sio_ttno3j)
    RadioButton option_n_sio_ttno3j;
    @BindView(R.id.option_y_sio_ppno3k)
    RadioButton option_y_sio_ppno3k;
    @BindView(R.id.option_n_sio_ppno3k)
    RadioButton option_n_sio_ppno3k;
    @BindView(R.id.option_y_sio_ttno3k)
    RadioButton option_y_sio_ttno3k;
    @BindView(R.id.option_n_sio_ttno3k)
    RadioButton option_n_sio_ttno3k;
    @BindView(R.id.option_y_sio_ppno3l)
    RadioButton option_y_sio_ppno3l;
    @BindView(R.id.option_n_sio_ppno3l)
    RadioButton option_n_sio_ppno3l;
    @BindView(R.id.option_y_sio_ttno3l)
    RadioButton option_y_sio_ttno3l;
    @BindView(R.id.option_n_sio_ttno3l)
    RadioButton option_n_sio_ttno3l;
    @BindView(R.id.option_y_sio_ppno3m)
    RadioButton option_y_sio_ppno3m;
    @BindView(R.id.option_n_sio_ppno3m)
    RadioButton option_n_sio_ppno3m;
    @BindView(R.id.option_y_sio_ttno3m)
    RadioButton option_y_sio_ttno3m;
    @BindView(R.id.option_n_sio_ttno3m)
    RadioButton option_n_sio_ttno3m;
    @BindView(R.id.option_y_sio_ppno3n)
    RadioButton option_y_sio_ppno3n;
    @BindView(R.id.option_n_sio_ppno3n)
    RadioButton option_n_sio_ppno3n;
    @BindView(R.id.option_y_sio_ttno3n)
    RadioButton option_y_sio_ttno3n;
    @BindView(R.id.option_n_sio_ttno3n)
    RadioButton option_n_sio_ttno3n;
    @BindView(R.id.option_y_sio_ppno3o)
    RadioButton option_y_sio_ppno3o;
    @BindView(R.id.option_n_sio_ppno3o)
    RadioButton option_n_sio_ppno3o;
    @BindView(R.id.option_y_sio_ttno3o)
    RadioButton option_y_sio_ttno3o;
    @BindView(R.id.option_n_sio_ttno3o)
    RadioButton option_n_sio_ttno3o;
    @BindView(R.id.option_y_sio_ppno3p)
    RadioButton option_y_sio_ppno3p;
    @BindView(R.id.option_n_sio_ppno3p)
    RadioButton option_n_sio_ppno3p;
    @BindView(R.id.option_y_sio_ttno3p)
    RadioButton option_y_sio_ttno3p;
    @BindView(R.id.option_n_sio_ttno3p)
    RadioButton option_n_sio_ttno3p;

    @BindView(R.id.option_y_sio_ppno4)
    RadioButton option_y_sio_ppno4;
    @BindView(R.id.option_n_sio_ppno4)
    RadioButton option_n_sio_ppno4;
    @BindView(R.id.option_y_sio_ttno4)
    RadioButton option_y_sio_ttno4;
    @BindView(R.id.option_n_sio_ttno4)
    RadioButton option_n_sio_ttno4;
    @BindView(R.id.option_y_sio_ppno5)
    RadioButton option_y_sio_ppno5;
    @BindView(R.id.option_n_sio_ppno5)
    RadioButton option_n_sio_ppno5;
    @BindView(R.id.option_y_sio_ttno5)
    RadioButton option_y_sio_ttno5;
    @BindView(R.id.option_n_sio_ttno5)
    RadioButton option_n_sio_ttno5;
    @BindView(R.id.option_y_sio_ppno6)
    RadioButton option_y_sio_ppno6;
    @BindView(R.id.option_n_sio_ppno6)
    RadioButton option_n_sio_ppno6;
    @BindView(R.id.option_y_sio_ttno6)
    RadioButton option_y_sio_ttno6;
    @BindView(R.id.option_n_sio_ttno6)
    RadioButton option_n_sio_ttno6;

    @BindView(R.id.essay_sio_no1)
    EditText essay_sio_no1;
    @BindView(R.id.essay_sio_no2)
    EditText essay_sio_no2;
    @BindView(R.id.essay_sio_no3ket)
    EditText essay_sio_no3ket;
    @BindView(R.id.essay_sio_no4)
    EditText essay_sio_no4;
    @BindView(R.id.essay_sio_no5)
    EditText essay_sio_no5;
    @BindView(R.id.essay_sio_no6)
    EditText essay_sio_no6;
    @BindView(R.id.essay_sio_cm_ppno7)
    EditText essay_sio_cm_ppno7;
    @BindView(R.id.essay_sio_kg_ppno7)
    EditText essay_sio_kg_ppno7;
    @BindView(R.id.essay_sio_cm_ttno7)
    EditText essay_sio_cm_ttno7;
    @BindView(R.id.essay_sio_kg_ttno7)
    EditText essay_sio_kg_ttno7;

    @BindView(R.id.scroll_qu)
    ScrollView scroll_qu;

    private Button btn_lanjut, btn_kembali;
    private ImageView iv_save, iv_next, iv_prev;
    private Toolbar second_toolbar;

    private ProgressDialog progressDialog;
}
