package id.co.ajsmsig.nb.prop.method.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.Prop_Model_AssurancePlanList;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.Prop_Model_OptionVO;

public class MappingUtil {
//    protected final Log logger = LogFactory.getLog( getClass() );

    public MappingUtil() {
//        logger.info("*-*-*-* MappingUtil constructor is called ...");
    }

    // example:
    // MappingUtil.getLabel( cepr01030102Form.getCurrencyList(), cepr01030102Form.getCurrencyCd() ) ;
    public static String getLabel(List<Prop_Model_OptionVO> propModelOptionVOList, Object value) {
        String result = "";
        if (propModelOptionVOList != null && value != null) {
            int size = propModelOptionVOList.size();
            Prop_Model_OptionVO propModelOptionVO;
            boolean valueHasBeenFound = false;
            int idx = 0;
            while (!valueHasBeenFound && idx < size) {
                propModelOptionVO = propModelOptionVOList.get(idx);
                if (propModelOptionVO != null && propModelOptionVO.getValue().equals(value.toString())) {
                    valueHasBeenFound = true;
                    result = propModelOptionVO.getLabel();
                }
                idx++;
            }
        }
        return result;
    }


    public static String getLabelProduct(List<Prop_Model_AssurancePlanList> optionVOList, Object value) {
        String result = "";
        if (optionVOList != null && value != null) {
            int size = optionVOList.size();
            Prop_Model_AssurancePlanList optionVO;
            boolean valueHasBeenFound = false;
            int idx = 0;
            while (!valueHasBeenFound && idx < size) {
                optionVO = optionVOList.get(idx);
                if (optionVO != null && optionVO.getValue().equals(value.toString())) {
                    valueHasBeenFound = true;
                    result = optionVO.getLabel();
                }
                idx++;
            }
        }
        return result;
    }

    public static HashMap serializableMap(Map dataMap) {
        if (dataMap != null) {
            return new HashMap(dataMap);
        } else {
            return null;
        }
    }
}
