package id.co.ajsmsig.nb.espaj.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.hardcode.RequestCode;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.espaj.activity.E_CreditCardPaymentActivity;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.database.E_Update;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.method.ProgressDialogClass;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.ProfileResikoModel;
import id.co.ajsmsig.nb.espaj.model.apimodel.request.PayViaCcRequest;
import id.co.ajsmsig.nb.espaj.model.apimodel.response.PayViaCcResponse;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
//import id.co.ajsmsig.nb.espaj.Method.MethodSupport;
//import id.co.ajsmsig.nb.espaj.Model.ModelESPAJ;
//import id.co.ajsmsig.nb.espaj.Model.ModelProfilResiko;

/**
 * Created by Bernard on 18/08/2017.
 */

public class E_ProfileResikoFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, RadioGroup.OnCheckedChangeListener {


    private ConstraintLayout layout_pr_no1, layout_pr_no2, layout_pr_no3, layout_pr_no4,
            layout_pr_no5, layout_pr_no6, layout_pr_no7, layout_pr_no8, layout_pr_no9,
            layout_pr_no10;

    private RadioGroup optiongrup_no1_pr, optiongrup_no7_pr, optiongrup_no12_pr, optiongrup_no18_pr,
            optiongrup_no23_pr, optiongrup_no28_pr, optiongrup_no34_pr, optiongrup_no40_pr,
            optiongrup_no45_pr, optiongrup_no51_pr;

    private TextView nilai_no1_pr, nilai_no7_pr, nilai_no12_pr, nilai_no18_pr, nilai_no23_pr,
            nilai_no28_pr, nilai_no34_pr, nilai_no40_pr, nilai_no45_pr, nilai_no51_pr;

    private ImageView imgval_no1_pr, imgval_no7_pr, imgval_no12_pr, imgval_no18_pr, imgval_no23_pr,
            imgval_no28_pr, imgval_no34_pr, imgval_no40_pr, imgval_no45_pr, imgval_no51_pr;

    private final String SUCCESS_CODE = "00";
    private final String PAID = "1";
    private final String WAITING_FOR_PAYMENT = "0";
    private ProgressDialog dialog;
    private int groupId = 0;
    @BindView(R.id.layoutPaymentSuccess)
    LinearLayout layoutPaymentSuccess;

    public E_ProfileResikoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        groupId = sharedPreferences.getInt("GROUP_ID", 0);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem menu_validasi = menu.findItem(R.id.menu_validasi);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        profilrisk = inflater.inflate(R.layout.e_fragment_profile_resiko, container, false);
        me = E_MainActivity.e_bundle.getParcelable(getString(R.string.modelespaj));
        ((E_MainActivity) getActivity()).setSecondToolbar("Profile Resiko Nasabah (9/", 9);
        ButterKnife.bind(this, profilrisk);

        layout_pr_no1 = profilrisk.findViewById(R.id.layout_pr_no1);
        layout_pr_no2 = profilrisk.findViewById(R.id.layout_pr_no2);
        layout_pr_no3 = profilrisk.findViewById(R.id.layout_pr_no3);
        layout_pr_no4 = profilrisk.findViewById(R.id.layout_pr_no4);
        layout_pr_no5 = profilrisk.findViewById(R.id.layout_pr_no5);
        layout_pr_no6 = profilrisk.findViewById(R.id.layout_pr_no6);
        layout_pr_no7 = profilrisk.findViewById(R.id.layout_pr_no7);
        layout_pr_no8 = profilrisk.findViewById(R.id.layout_pr_no8);
        layout_pr_no9 = profilrisk.findViewById(R.id.layout_pr_no9);
        layout_pr_no10 = profilrisk.findViewById(R.id.layout_pr_no10);

        optiongrup_no1_pr = profilrisk.findViewById(R.id.optiongrup_no1_pr);
        optiongrup_no7_pr = profilrisk.findViewById(R.id.optiongrup_no7_pr);
        optiongrup_no12_pr = profilrisk.findViewById(R.id.optiongrup_no12_pr);
        optiongrup_no18_pr = profilrisk.findViewById(R.id.optiongrup_no18_pr);
        optiongrup_no23_pr = profilrisk.findViewById(R.id.optiongrup_no23_pr);
        optiongrup_no28_pr = profilrisk.findViewById(R.id.optiongrup_no28_pr);
        optiongrup_no34_pr = profilrisk.findViewById(R.id.optiongrup_no34_pr);
        optiongrup_no40_pr = profilrisk.findViewById(R.id.optiongrup_no40_pr);
        optiongrup_no45_pr = profilrisk.findViewById(R.id.optiongrup_no45_pr);
        optiongrup_no51_pr = profilrisk.findViewById(R.id.optiongrup_no51_pr);

        nilai_no1_pr = profilrisk.findViewById(R.id.nilai_no1_pr);
        nilai_no7_pr = profilrisk.findViewById(R.id.nilai_no7_pr);
        nilai_no12_pr = profilrisk.findViewById(R.id.nilai_no12_pr);
        nilai_no18_pr = profilrisk.findViewById(R.id.nilai_no18_pr);
        nilai_no23_pr = profilrisk.findViewById(R.id.nilai_no23_pr);
        nilai_no28_pr = profilrisk.findViewById(R.id.nilai_no28_pr);
        nilai_no34_pr = profilrisk.findViewById(R.id.nilai_no34_pr);
        nilai_no40_pr = profilrisk.findViewById(R.id.nilai_no40_pr);
        nilai_no45_pr = profilrisk.findViewById(R.id.nilai_no45_pr);
        nilai_no51_pr = profilrisk.findViewById(R.id.nilai_no51_pr);

        imgval_no1_pr = profilrisk.findViewById(R.id.imgval_no1_pr);
        imgval_no7_pr = profilrisk.findViewById(R.id.imgval_no7_pr);
        imgval_no12_pr = profilrisk.findViewById(R.id.imgval_no12_pr);
        imgval_no18_pr = profilrisk.findViewById(R.id.imgval_no18_pr);
        imgval_no23_pr = profilrisk.findViewById(R.id.imgval_no23_pr);
        imgval_no28_pr = profilrisk.findViewById(R.id.imgval_no28_pr);
        imgval_no34_pr = profilrisk.findViewById(R.id.imgval_no34_pr);
        imgval_no40_pr = profilrisk.findViewById(R.id.imgval_no40_pr);
        imgval_no45_pr = profilrisk.findViewById(R.id.imgval_no45_pr);
        imgval_no51_pr = profilrisk.findViewById(R.id.imgval_no51_pr);


//1
        MethodSupport.OnRadioButton(option_no2_1pr, this);
        MethodSupport.OnRadioButton(option_no3_1pr, this);
        MethodSupport.OnRadioButton(option_no4_1pr, this);
        MethodSupport.OnRadioButton(option_no5_1pr, this);
        MethodSupport.OnRadioButton(option_no6_1pr, this);
//2
        MethodSupport.OnRadioButton(option_no8_7pr, this);
        MethodSupport.OnRadioButton(option_no9_7pr, this);
        MethodSupport.OnRadioButton(option_no10_7pr, this);
        MethodSupport.OnRadioButton(option_no11_7pr, this);
//3
        MethodSupport.OnRadioButton(option_no13_12pr, this);
        MethodSupport.OnRadioButton(option_no14_12pr, this);
        MethodSupport.OnRadioButton(option_no15_12pr, this);
        MethodSupport.OnRadioButton(option_no16_12pr, this);
        MethodSupport.OnRadioButton(option_no17_12pr, this);
//4
        MethodSupport.OnRadioButton(option_no19_18pr, this);
        MethodSupport.OnRadioButton(option_no20_18pr, this);
        MethodSupport.OnRadioButton(option_no21_18pr, this);
        MethodSupport.OnRadioButton(option_no22_18pr, this);
//5
        MethodSupport.OnRadioButton(option_no24_23pr, this);
        MethodSupport.OnRadioButton(option_no25_23pr, this);
        MethodSupport.OnRadioButton(option_no26_23pr, this);
        MethodSupport.OnRadioButton(option_no27_23pr, this);
//6
        MethodSupport.OnRadioButton(option_no29_28pr, this);
        MethodSupport.OnRadioButton(option_no30_28pr, this);
        MethodSupport.OnRadioButton(option_no31_28pr, this);
        MethodSupport.OnRadioButton(option_no32_28pr, this);
        MethodSupport.OnRadioButton(option_no33_28pr, this);
//7
        MethodSupport.OnRadioButton(option_no35_34pr, this);
        MethodSupport.OnRadioButton(option_no36_34pr, this);
        MethodSupport.OnRadioButton(option_no37_34pr, this);
        MethodSupport.OnRadioButton(option_no38_34pr, this);
        MethodSupport.OnRadioButton(option_no39_34pr, this);
//8
        MethodSupport.OnRadioButton(option_no41_40pr, this);
        MethodSupport.OnRadioButton(option_no42_40pr, this);
        MethodSupport.OnRadioButton(option_no43_40pr, this);
        MethodSupport.OnRadioButton(option_no44_40pr, this);
//9
        MethodSupport.OnRadioButton(option_no46_45pr, this);
        MethodSupport.OnRadioButton(option_no47_45pr, this);
        MethodSupport.OnRadioButton(option_no48_45pr, this);
        MethodSupport.OnRadioButton(option_no49_45pr, this);
        MethodSupport.OnRadioButton(option_no50_45pr, this);
//10
        MethodSupport.OnRadioButton(option_no52_51pr, this);
        MethodSupport.OnRadioButton(option_no53_51pr, this);
        MethodSupport.OnRadioButton(option_no54_51pr, this);
        MethodSupport.OnRadioButton(option_no55_51pr, this);
//RadioGroup
        MethodSupport.OnRadioGroup(optiongrup_no1_pr, this);
        MethodSupport.OnRadioGroup(optiongrup_no7_pr, this);
        MethodSupport.OnRadioGroup(optiongrup_no12_pr, this);
        MethodSupport.OnRadioGroup(optiongrup_no18_pr, this);
        MethodSupport.OnRadioGroup(optiongrup_no23_pr, this);
        MethodSupport.OnRadioGroup(optiongrup_no28_pr, this);
        MethodSupport.OnRadioGroup(optiongrup_no34_pr, this);
        MethodSupport.OnRadioGroup(optiongrup_no40_pr, this);
        MethodSupport.OnRadioGroup(optiongrup_no45_pr, this);
        MethodSupport.OnRadioGroup(optiongrup_no51_pr, this);

        //button di Activity
        btn_lanjut = getActivity().findViewById(R.id.btn_lanjut);
        btn_lanjut.setOnClickListener(this);
        btn_kembali = getActivity().findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(this);
        second_toolbar = ((E_MainActivity) getActivity()).getSecond_toolbar();
        iv_save = second_toolbar.findViewById(R.id.iv_save);
        iv_save.setOnClickListener(this);
        iv_next = second_toolbar.findViewById(R.id.iv_next);
        iv_next.setOnClickListener(this);
        iv_prev = second_toolbar.findViewById(R.id.iv_prev);
        iv_prev.setOnClickListener(this);

        restore();
        return profilrisk;
    }

    @Override
    public void onPause() {
        super.onPause();
        ProgressDialogClass.removeSimpleProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();

        MethodSupport.onFocusviewtest(scroll_pr, cl_child_pr, cl_profile_resiko_no1);

        MethodSupport.active_view(1, btn_lanjut);
        MethodSupport.active_view(1, iv_next);
        MethodSupport.active_view(1, btn_kembali);
        MethodSupport.active_view(1, iv_prev);
        ProgressDialogClass.removeSimpleProgressDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_validasi:
                me.getProfileResikoModel().setValidation(validation());
                loadview();
                MyTaskValidasi taskValidasi = new MyTaskValidasi();
                taskValidasi.execute();
                Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTaskValidasi extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_PROFILE_RESIKO);
            return null;
        }
    }

    private class MyTaskSavePage extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_PROFILE_RESIKO);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_lanjut:
//                if (!validation()) {
//                    AlertDialog.Builder dialog = new AlertDialog.Builder(
//                            getActivity());
//                    dialog.setTitle("DATA BELUM LENGKAP!!!");
//                    dialog.setMessage("Mohon Periksa Kembali, sebelum lanjut kehalaman berikutnya.\n\n\n");
//                    dialog.setPositiveButton("OK",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog,
//                                                    int which) {
//                                    dialog.dismiss();
//                                }
//                            });
//                    dialog.show();
//                    ProgressDialogClass.removeSimpleProgressDialog();
//                } else {
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
//                }
                break;
            case R.id.iv_next:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_save:
                loadview();
                MyTaskSavePage taskSavePage = new MyTaskSavePage();
                taskSavePage.execute();

                break;
            case R.id.iv_prev:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.btn_kembali:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
        }
    }

    private void loadview() {
        me.setProfileResikoModel(new ProfileResikoModel(
                value_no1, value_no7, value_no12, value_no18, value_no23, value_no28,
                value_no34, value_no40, value_no45, value_no51, answer_no1, answer_no7,
                answer_no12, answer_no18, answer_no23, answer_no28, answer_no34, answer_no40,
                answer_no45, answer_no51));
        me.getProfileResikoModel().setValidation(validation());
        E_MainActivity.e_bundle.putParcelable(getString(R.string.modelespaj), me);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.optiongrup_no1_pr:
                MethodSupport.focuslayouttext(hasFocus, layout_pr_no1);
                break;
            case R.id.optiongrup_no7_pr:
                MethodSupport.focuslayouttext(hasFocus, layout_pr_no2);
                break;
            case R.id.optiongrup_no12_pr:
                MethodSupport.focuslayouttext(hasFocus, layout_pr_no3);
                break;
            case R.id.optiongrup_no18_pr:
                MethodSupport.focuslayouttext(hasFocus, layout_pr_no4);
                break;
            case R.id.optiongrup_no23_pr:
                MethodSupport.focuslayouttext(hasFocus, layout_pr_no5);
                break;
            case R.id.optiongrup_no28_pr:
                MethodSupport.focuslayouttext(hasFocus, layout_pr_no6);
                break;
            case R.id.optiongrup_no34_pr:
                MethodSupport.focuslayouttext(hasFocus, layout_pr_no7);
                break;
            case R.id.optiongrup_no40_pr:
                MethodSupport.focuslayouttext(hasFocus, layout_pr_no8);
                break;
            case R.id.optiongrup_no45_pr:
                MethodSupport.focuslayouttext(hasFocus, layout_pr_no9);
                break;
            case R.id.optiongrup_no51_pr:
                MethodSupport.focuslayouttext(hasFocus, layout_pr_no10);
                break;

            //radiobutton
            case R.id.option_no2_1pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no1, optiongrup_no1_pr, R.id.option_no2_1pr);
                break;
            case R.id.option_no3_1pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no1, optiongrup_no1_pr, R.id.option_no3_1pr);
                break;
            case R.id.option_no4_1pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no1, optiongrup_no1_pr, R.id.option_no4_1pr);
                break;
            case R.id.option_no5_1pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no1, optiongrup_no1_pr, R.id.option_no5_1pr);
                break;
            case R.id.option_no6_1pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no1, optiongrup_no1_pr, R.id.option_no6_1pr);
                break;
            case R.id.option_no8_7pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no2, optiongrup_no7_pr, R.id.option_no8_7pr);
                break;
            case R.id.option_no9_7pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no2, optiongrup_no7_pr, R.id.option_no9_7pr);
                break;
            case R.id.option_no10_7pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no2, optiongrup_no7_pr, R.id.option_no10_7pr);
                break;
            case R.id.option_no11_7pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no2, optiongrup_no7_pr, R.id.option_no11_7pr);
                break;
            case R.id.option_no13_12pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no3, optiongrup_no12_pr, R.id.option_no13_12pr);
                break;
            case R.id.option_no14_12pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no3, optiongrup_no12_pr, R.id.option_no14_12pr);
                break;
            case R.id.option_no15_12pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no3, optiongrup_no12_pr, R.id.option_no15_12pr);
                break;
            case R.id.option_no16_12pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no3, optiongrup_no12_pr, R.id.option_no16_12pr);
                break;
            case R.id.option_no17_12pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no3, optiongrup_no12_pr, R.id.option_no17_12pr);
                break;
            case R.id.option_no19_18pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no4, optiongrup_no18_pr, R.id.option_no19_18pr);
                break;
            case R.id.option_no20_18pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no4, optiongrup_no18_pr, R.id.option_no20_18pr);
                break;
            case R.id.option_no21_18pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no4, optiongrup_no18_pr, R.id.option_no21_18pr);
                break;
            case R.id.option_no22_18pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no4, optiongrup_no18_pr, R.id.option_no22_18pr);
                break;
            case R.id.option_no24_23pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no5, optiongrup_no23_pr, R.id.option_no24_23pr);
                break;
            case R.id.option_no25_23pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no5, optiongrup_no23_pr, R.id.option_no25_23pr);
                break;
            case R.id.option_no26_23pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no5, optiongrup_no23_pr, R.id.option_no26_23pr);
                break;
            case R.id.option_no27_23pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no5, optiongrup_no23_pr, R.id.option_no27_23pr);
                break;
            case R.id.option_no29_28pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no6, optiongrup_no28_pr, R.id.option_no29_28pr);
                break;
            case R.id.option_no30_28pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no6, optiongrup_no28_pr, R.id.option_no30_28pr);
                break;
            case R.id.option_no31_28pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no6, optiongrup_no28_pr, R.id.option_no31_28pr);
                break;
            case R.id.option_no32_28pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no6, optiongrup_no28_pr, R.id.option_no32_28pr);
                break;
            case R.id.option_no33_28pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no6, optiongrup_no28_pr, R.id.option_no33_28pr);
                break;
            case R.id.option_no35_34pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no7, optiongrup_no34_pr, R.id.option_no35_34pr);
                break;
            case R.id.option_no36_34pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no7, optiongrup_no34_pr, R.id.option_no36_34pr);
                break;
            case R.id.option_no37_34pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no7, optiongrup_no34_pr, R.id.option_no37_34pr);
                break;
            case R.id.option_no38_34pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no7, optiongrup_no34_pr, R.id.option_no38_34pr);
                break;
            case R.id.option_no39_34pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no7, optiongrup_no34_pr, R.id.option_no39_34pr);
                break;
            case R.id.option_no41_40pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no8, optiongrup_no40_pr, R.id.option_no41_40pr);
                break;
            case R.id.option_no42_40pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no8, optiongrup_no40_pr, R.id.option_no42_40pr);
                break;
            case R.id.option_no43_40pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no8, optiongrup_no40_pr, R.id.option_no43_40pr);
                break;
            case R.id.option_no44_40pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no8, optiongrup_no40_pr, R.id.option_no44_40pr);
                break;
            case R.id.option_no46_45pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no9, optiongrup_no45_pr, R.id.option_no46_45pr);
                break;
            case R.id.option_no47_45pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no9, optiongrup_no45_pr, R.id.option_no47_45pr);
                break;
            case R.id.option_no48_45pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no9, optiongrup_no45_pr, R.id.option_no48_45pr);
                break;
            case R.id.option_no49_45pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no9, optiongrup_no45_pr, R.id.option_no49_45pr);
                break;
            case R.id.option_no50_45pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no9, optiongrup_no45_pr, R.id.option_no50_45pr);
                break;
            case R.id.option_no52_51pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no10, optiongrup_no51_pr, R.id.option_no52_51pr);
                break;
            case R.id.option_no53_51pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no10, optiongrup_no51_pr, R.id.option_no53_51pr);
                break;
            case R.id.option_no54_51pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no10, optiongrup_no51_pr, R.id.option_no54_51pr);
                break;
            case R.id.option_no55_51pr:
                MethodSupport.focuslayoutRadio(hasFocus, layout_pr_no10, optiongrup_no51_pr, R.id.option_no55_51pr);
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (group.getId()) {
            case R.id.optiongrup_no1_pr:
                value_no1 = MethodSupport.OnCheckQuisionerRadioPR(optiongrup_no1_pr, answer_no1, nilai_no1_pr, imgval_no1_pr,
                        checkedId, getContext());
                break;
            case R.id.optiongrup_no7_pr:
                value_no7 = MethodSupport.OnCheckQuisionerRadioPR(optiongrup_no7_pr, answer_no7, nilai_no7_pr, imgval_no7_pr,
                        checkedId, getContext());
                break;
            case R.id.optiongrup_no12_pr:
                value_no12 = MethodSupport.OnCheckQuisionerRadioPR(optiongrup_no12_pr, answer_no12, nilai_no12_pr, imgval_no12_pr,
                        checkedId, getContext());
                break;
            case R.id.optiongrup_no18_pr:
                value_no18 = MethodSupport.OnCheckQuisionerRadioPR(optiongrup_no18_pr, answer_no18, nilai_no18_pr, imgval_no18_pr,
                        checkedId, getContext());
                break;
            case R.id.optiongrup_no23_pr:
                value_no23 = MethodSupport.OnCheckQuisionerRadioPR(optiongrup_no23_pr, answer_no23, nilai_no23_pr, imgval_no23_pr,
                        checkedId, getContext());
                break;
            case R.id.optiongrup_no28_pr:
                value_no28 = MethodSupport.OnCheckQuisionerRadioPR(optiongrup_no28_pr, answer_no28, nilai_no28_pr, imgval_no28_pr,
                        checkedId, getContext());
                break;
            case R.id.optiongrup_no34_pr:
                value_no34 = MethodSupport.OnCheckQuisionerRadioPR(optiongrup_no34_pr, answer_no34, nilai_no34_pr, imgval_no34_pr,
                        checkedId, getContext());
                break;
            case R.id.optiongrup_no40_pr:
                value_no40 = MethodSupport.OnCheckQuisionerRadioPR(optiongrup_no40_pr, answer_no40, nilai_no40_pr, imgval_no40_pr,
                        checkedId, getContext());
                break;
            case R.id.optiongrup_no45_pr:
                value_no45 = MethodSupport.OnCheckQuisionerRadioPR(optiongrup_no45_pr, answer_no45, nilai_no45_pr, imgval_no45_pr,
                        checkedId, getContext());
                break;
            case R.id.optiongrup_no51_pr:
                value_no51 = MethodSupport.OnCheckQuisionerRadioPR(optiongrup_no51_pr, answer_no51, nilai_no51_pr, imgval_no51_pr,
                        checkedId, getContext());
                break;
        }
    }

    private boolean validation() { //bernard
        boolean ready = true;
        if (!Method_Validator.onEmptyRadio(optiongrup_no1_pr, imgval_no1_pr, nilai_no1_pr, layout_pr_no1, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(optiongrup_no7_pr, imgval_no7_pr, nilai_no7_pr, layout_pr_no2, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(optiongrup_no12_pr, imgval_no12_pr, nilai_no12_pr, layout_pr_no3, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(optiongrup_no18_pr, imgval_no18_pr, nilai_no18_pr, layout_pr_no4, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(optiongrup_no23_pr, imgval_no23_pr, nilai_no23_pr, layout_pr_no5, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(optiongrup_no28_pr, imgval_no28_pr, nilai_no28_pr, layout_pr_no6, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(optiongrup_no34_pr, imgval_no34_pr, nilai_no34_pr, layout_pr_no7, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(optiongrup_no40_pr, imgval_no40_pr, nilai_no40_pr, layout_pr_no8, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(optiongrup_no45_pr, imgval_no45_pr, nilai_no45_pr, layout_pr_no9, getContext())) {
            ready = false;
        }
        if (!Method_Validator.onEmptyRadio(optiongrup_no51_pr, imgval_no51_pr, nilai_no51_pr, layout_pr_no10, getContext())) {
            ready = false;
        }
        return ready;
    }


    public void restore() {
        answer_no1 = "";
        answer_no7 = "";
        answer_no12 = "";
        answer_no18 = "";
        answer_no23 = "";
        answer_no28 = "";
        answer_no34 = "";
        answer_no40 = "";
        answer_no45 = "";
        answer_no51 = "";

        value_no1 = 0;
        value_no7 = 0;
        value_no12 = 0;
        value_no18 = 0;
        value_no23 = 0;
        value_no28 = 0;
        value_no34 = 0;
        value_no40 = 0;
        value_no45 = 0;
        value_no51 = 0;

            if (me.getProfileResikoModel().getNilai_no1_pr() == 2) {
                optiongrup_no1_pr.check(R.id.option_no2_1pr);
            } else if (me.getProfileResikoModel().getNilai_no1_pr() == 4) {
                optiongrup_no1_pr.check(R.id.option_no3_1pr);
            } else if (me.getProfileResikoModel().getNilai_no1_pr() == 6) {
                optiongrup_no1_pr.check(R.id.option_no4_1pr);
            } else if (me.getProfileResikoModel().getNilai_no1_pr() == 8) {
                optiongrup_no1_pr.check(R.id.option_no5_1pr);
            } else if (me.getProfileResikoModel().getNilai_no1_pr() == 10) {
                optiongrup_no1_pr.check(R.id.option_no6_1pr);
            }//2
            if (me.getProfileResikoModel().getNilai_no7_pr() == 2) {
                optiongrup_no7_pr.check(R.id.option_no8_7pr);
            } else if (me.getProfileResikoModel().getNilai_no7_pr() == 4) {
                optiongrup_no7_pr.check(R.id.option_no9_7pr);
            } else if (me.getProfileResikoModel().getNilai_no7_pr() == 6) {
                optiongrup_no7_pr.check(R.id.option_no10_7pr);
            } else if (me.getProfileResikoModel().getNilai_no7_pr() == 8) {
                optiongrup_no7_pr.check(R.id.option_no11_7pr);
            }//3
            if (me.getProfileResikoModel().getNilai_no12_pr() == 2) {
                optiongrup_no12_pr.check(R.id.option_no13_12pr);
            } else if (me.getProfileResikoModel().getNilai_no12_pr() == 4) {
                optiongrup_no12_pr.check(R.id.option_no14_12pr);
            } else if (me.getProfileResikoModel().getNilai_no12_pr() == 6) {
                optiongrup_no12_pr.check(R.id.option_no15_12pr);
            } else if (me.getProfileResikoModel().getNilai_no12_pr() == 8) {
                optiongrup_no12_pr.check(R.id.option_no16_12pr);
            } else if (me.getProfileResikoModel().getNilai_no12_pr() == 10) {
                optiongrup_no12_pr.check(R.id.option_no17_12pr);
            }//4
            if (me.getProfileResikoModel().getNilai_no18_pr() == 2) {
                optiongrup_no18_pr.check(R.id.option_no19_18pr);
            } else if (me.getProfileResikoModel().getNilai_no18_pr() == 4) {
                optiongrup_no18_pr.check(R.id.option_no20_18pr);
            } else if (me.getProfileResikoModel().getNilai_no18_pr() == 6) {
                optiongrup_no18_pr.check(R.id.option_no21_18pr);
            } else if (me.getProfileResikoModel().getNilai_no18_pr() == 8) {
                optiongrup_no18_pr.check(R.id.option_no22_18pr);
            }//5
            if (me.getProfileResikoModel().getNilai_no23_pr() == 2) {
                optiongrup_no23_pr.check(R.id.option_no24_23pr);
            } else if (me.getProfileResikoModel().getNilai_no23_pr() == 4) {
                optiongrup_no23_pr.check(R.id.option_no25_23pr);
            } else if (me.getProfileResikoModel().getNilai_no23_pr() == 6) {
                optiongrup_no23_pr.check(R.id.option_no26_23pr);
            } else if (me.getProfileResikoModel().getNilai_no23_pr() == 8) {
                optiongrup_no23_pr.check(R.id.option_no27_23pr);
            }//6
            if (me.getProfileResikoModel().getNilai_no28_pr() == 2) {
                optiongrup_no28_pr.check(R.id.option_no29_28pr);
            } else if (me.getProfileResikoModel().getNilai_no28_pr() == 4) {
                optiongrup_no28_pr.check(R.id.option_no30_28pr);
            } else if (me.getProfileResikoModel().getNilai_no28_pr() == 6) {
                optiongrup_no28_pr.check(R.id.option_no31_28pr);
            } else if (me.getProfileResikoModel().getNilai_no28_pr() == 8) {
                optiongrup_no28_pr.check(R.id.option_no32_28pr);
            } else if (me.getProfileResikoModel().getNilai_no28_pr() == 10) {
                optiongrup_no28_pr.check(R.id.option_no33_28pr);
            }//7
            if (me.getProfileResikoModel().getNilai_no34_pr() == 2) {
                optiongrup_no34_pr.check(R.id.option_no35_34pr);
            } else if (me.getProfileResikoModel().getNilai_no34_pr() == 4) {
                optiongrup_no34_pr.check(R.id.option_no36_34pr);
            } else if (me.getProfileResikoModel().getNilai_no34_pr() == 6) {
                optiongrup_no34_pr.check(R.id.option_no37_34pr);
            } else if (me.getProfileResikoModel().getNilai_no34_pr() == 8) {
                optiongrup_no34_pr.check(R.id.option_no38_34pr);
            } else if (me.getProfileResikoModel().getNilai_no34_pr() == 10) {
                optiongrup_no34_pr.check(R.id.option_no39_34pr);
            }//8
            if (me.getProfileResikoModel().getNilai_no40_pr() == 2) {
                optiongrup_no40_pr.check(R.id.option_no41_40pr);
            } else if (me.getProfileResikoModel().getNilai_no40_pr() == 4) {
                optiongrup_no40_pr.check(R.id.option_no42_40pr);
            } else if (me.getProfileResikoModel().getNilai_no40_pr() == 6) {
                optiongrup_no40_pr.check(R.id.option_no43_40pr);
            } else if (me.getProfileResikoModel().getNilai_no40_pr() == 8) {
                optiongrup_no40_pr.check(R.id.option_no44_40pr);
            }//9
            if (me.getProfileResikoModel().getNilai_no45_pr() == 2) {
                optiongrup_no45_pr.check(R.id.option_no46_45pr);
            } else if (me.getProfileResikoModel().getNilai_no45_pr() == 4) {
                optiongrup_no45_pr.check(R.id.option_no47_45pr);
            } else if (me.getProfileResikoModel().getNilai_no45_pr() == 6) {
                optiongrup_no45_pr.check(R.id.option_no48_45pr);
            } else if (me.getProfileResikoModel().getNilai_no45_pr() == 8) {
                optiongrup_no45_pr.check(R.id.option_no49_45pr);
            } else if (me.getProfileResikoModel().getNilai_no45_pr() == 10) {
                optiongrup_no45_pr.check(R.id.option_no50_45pr);
            }//10
            if (me.getProfileResikoModel().getNilai_no51_pr() == 2) {
                optiongrup_no51_pr.check(R.id.option_no52_51pr);
            } else if (me.getProfileResikoModel().getNilai_no51_pr() == 4) {
                optiongrup_no51_pr.check(R.id.option_no53_51pr);
            } else if (me.getProfileResikoModel().getNilai_no51_pr() == 6) {
                optiongrup_no51_pr.check(R.id.option_no54_51pr);
            } else if (me.getProfileResikoModel().getNilai_no51_pr() == 8) {
                optiongrup_no51_pr.check(R.id.option_no55_51pr);
            }

            if (!me.getValidation()) {
                validation();
            }
            if (me.getModelID().getFlag_aktif() == 1) {
                MethodSupport.disable(false, cl_child_pr);
            }
    }

    private void onClickLanjut() {

        loadview();
        MyTask task = new MyTask();
        task.execute();


    }

    private class MyTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_PROFILE_RESIKO);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    private class MyTaskBack extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_PROFILE_RESIKO);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (me.getModelID().getJns_spaj() == 3) {
                ((E_MainActivity) getActivity()).setFragment(new E_QuestionnaireFragment(), getResources().getString(R.string.questionnaire));
            } else if (me.getModelID().getJns_spaj() == 4) {
                ((E_MainActivity) getActivity()).setFragment(new E_UpdateQuisionerSIOFragment(), getResources().getString(R.string.questionnaire));
            } else {
                ((E_MainActivity) getActivity()).setFragment(new E_DokumenPendukungFragment(), getResources().getString(R.string.dokumen_pendukung));
            }
        }
    }

    private void onClickBack() {

        loadview();
        MyTaskBack taskBack = new MyTaskBack();
        taskBack.execute();

    }

    private ProgressDialog dialogmessage;

    public void startProgress() {
        dialogmessage = new ProgressDialog(getActivity());
        dialogmessage.setMessage("Mohon Menunggu ");
        dialogmessage.setTitle("Sedang Melakukan Perpindahan Halaman ...");
        dialogmessage.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialogmessage.show();
        dialogmessage.setCancelable(false);
        btn_lanjut.setEnabled(false);
        iv_next.setEnabled(false);
        iv_prev.setEnabled(false);
        btn_kembali.setEnabled(false);
        dialogmessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                btn_lanjut.setEnabled(true);
                iv_next.setEnabled(true);
                iv_prev.setEnabled(true);
                btn_kembali.setEnabled(true);
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (dialogmessage.getProgress() <= dialogmessage
                            .getMax()) {
                        Thread.sleep(10);
                        handle.sendMessage(handle.obtainMessage());
                        if (dialogmessage.getProgress() == dialogmessage
                                .getMax()) {
                            dialogmessage.dismiss();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    Handler handle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            dialogmessage.incrementProgressBy(1);
        }
    };

    @BindView(R.id.cl_child_pr)
    ConstraintLayout cl_child_pr;
    @BindView(R.id.cl_profile_resiko)
    ConstraintLayout cl_profile_resiko;
    @BindView(R.id.cl_form_profile_resiko)
    ConstraintLayout cl_form_profile_resiko;
    @BindView(R.id.cl_profile_resiko_no1)
    ConstraintLayout cl_profile_resiko_no1;

    @BindView(R.id.cl_profile_resiko_no2)
    ConstraintLayout cl_profile_resiko_no2;

    @BindView(R.id.cl_profile_resiko_no3)
    ConstraintLayout cl_profile_resiko_no3;

    @BindView(R.id.cl_profile_resiko_no4)
    ConstraintLayout cl_profile_resiko_no4;

    @BindView(R.id.cl_profile_resiko_no5)
    ConstraintLayout cl_profile_resiko_no5;

    @BindView(R.id.cl_profile_resiko_no6)
    ConstraintLayout cl_profile_resiko_no6;

    @BindView(R.id.cl_profile_resiko_no7)
    ConstraintLayout cl_profile_resiko_no7;

    @BindView(R.id.cl_profile_resiko_no8)
    ConstraintLayout cl_profile_resiko_no8;

    @BindView(R.id.cl_profile_resiko_no9)
    ConstraintLayout cl_profile_resiko_no9;
    @BindView(R.id.cl_profile_resiko_no10)
    ConstraintLayout cl_profile_resiko_no10;

    @BindView(R.id.option_no2_1pr)
    RadioButton option_no2_1pr;
    @BindView(R.id.option_no3_1pr)
    RadioButton option_no3_1pr;
    @BindView(R.id.option_no4_1pr)
    RadioButton option_no4_1pr;
    @BindView(R.id.option_no5_1pr)
    RadioButton option_no5_1pr;
    @BindView(R.id.option_no6_1pr)
    RadioButton option_no6_1pr;
    @BindView(R.id.option_no8_7pr)
    RadioButton option_no8_7pr;
    @BindView(R.id.option_no9_7pr)
    RadioButton option_no9_7pr;
    @BindView(R.id.option_no10_7pr)
    RadioButton option_no10_7pr;
    @BindView(R.id.option_no11_7pr)
    RadioButton option_no11_7pr;
    @BindView(R.id.option_no13_12pr)
    RadioButton option_no13_12pr;
    @BindView(R.id.option_no14_12pr)
    RadioButton option_no14_12pr;
    @BindView(R.id.option_no15_12pr)
    RadioButton option_no15_12pr;
    @BindView(R.id.option_no16_12pr)
    RadioButton option_no16_12pr;
    @BindView(R.id.option_no17_12pr)
    RadioButton option_no17_12pr;
    @BindView(R.id.option_no19_18pr)
    RadioButton option_no19_18pr;
    @BindView(R.id.option_no20_18pr)
    RadioButton option_no20_18pr;
    @BindView(R.id.option_no21_18pr)
    RadioButton option_no21_18pr;
    @BindView(R.id.option_no22_18pr)
    RadioButton option_no22_18pr;
    @BindView(R.id.option_no24_23pr)
    RadioButton option_no24_23pr;
    @BindView(R.id.option_no25_23pr)
    RadioButton option_no25_23pr;
    @BindView(R.id.option_no26_23pr)
    RadioButton option_no26_23pr;
    @BindView(R.id.option_no27_23pr)
    RadioButton option_no27_23pr;
    @BindView(R.id.option_no29_28pr)
    RadioButton option_no29_28pr;
    @BindView(R.id.option_no30_28pr)
    RadioButton option_no30_28pr;
    @BindView(R.id.option_no31_28pr)
    RadioButton option_no31_28pr;
    @BindView(R.id.option_no32_28pr)
    RadioButton option_no32_28pr;
    @BindView(R.id.option_no33_28pr)
    RadioButton option_no33_28pr;
    @BindView(R.id.option_no35_34pr)
    RadioButton option_no35_34pr;
    @BindView(R.id.option_no36_34pr)
    RadioButton option_no36_34pr;
    @BindView(R.id.option_no37_34pr)
    RadioButton option_no37_34pr;
    @BindView(R.id.option_no38_34pr)
    RadioButton option_no38_34pr;
    @BindView(R.id.option_no39_34pr)
    RadioButton option_no39_34pr;
    @BindView(R.id.option_no41_40pr)
    RadioButton option_no41_40pr;
    @BindView(R.id.option_no42_40pr)
    RadioButton option_no42_40pr;
    @BindView(R.id.option_no43_40pr)
    RadioButton option_no43_40pr;
    @BindView(R.id.option_no44_40pr)
    RadioButton option_no44_40pr;
    @BindView(R.id.option_no46_45pr)
    RadioButton option_no46_45pr;
    @BindView(R.id.option_no47_45pr)
    RadioButton option_no47_45pr;
    @BindView(R.id.option_no48_45pr)
    RadioButton option_no48_45pr;
    @BindView(R.id.option_no49_45pr)
    RadioButton option_no49_45pr;
    @BindView(R.id.option_no50_45pr)
    RadioButton option_no50_45pr;
    @BindView(R.id.option_no52_51pr)
    RadioButton option_no52_51pr;
    @BindView(R.id.option_no53_51pr)
    RadioButton option_no53_51pr;
    @BindView(R.id.option_no54_51pr)
    RadioButton option_no54_51pr;
    @BindView(R.id.option_no55_51pr)
    RadioButton option_no55_51pr;

    @BindView(R.id.scroll_pr)
    ScrollView scroll_pr;

    private Button btn_lanjut, btn_kembali;
    private ImageView iv_save, iv_next, iv_prev;
    private Toolbar second_toolbar;

    private String answer_no1 = ""; //1
    private String answer_no7 = ""; //2
    private String answer_no12 = ""; //3
    private String answer_no18 = ""; //4
    private String answer_no23 = ""; //5
    private String answer_no28 = ""; //6
    private String answer_no34 = ""; //7
    private String answer_no40 = ""; //8
    private String answer_no45 = ""; //9
    private String answer_no51 = ""; //10

    private int value_no1 = 0;
    private int value_no7 = 0;
    private int value_no12 = 0;
    private int value_no18 = 0;
    private int value_no23 = 0;
    private int value_no28 = 0;
    private int value_no34 = 0;
    private int value_no40 = 0;
    private int value_no45 = 0;
    private int value_no51 = 0;

    EspajModel me;

    View profilrisk;

    private ProgressDialog progressDialog;


    private boolean isAlreadyHaveTransactionNumber() {
        String spajIdTab = me.getModelID().getSPAJ_ID_TAB();
        E_Select select = new E_Select(getActivity());

        String transactionNumber = select.getPaymentTransactionNumber(spajIdTab);

        return !TextUtils.isEmpty(transactionNumber);
    }

    private boolean isCreditCardPaymentType() {
        String spajIdTab = me.getModelID().getSPAJ_ID_TAB();
        E_Select select = new E_Select(getActivity());

        int paymentType = select.getPaymentType(spajIdTab);
        return paymentType == Const.CREDIT_CARD_PAYMENT_CODE;
    }

    private void displayAlertDialog(String title, String message) {
        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> ((E_MainActivity) getActivity()).setFragment(new E_UsulanAsuransiFragment(), getResources().getString(R.string.usulan_asuransi)))
                .setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void showPaymentCreditCardProgress(String title) {
        dialog = new ProgressDialog(getActivity());
        dialog.setTitle(title);
        dialog.setMessage("Mohon tunggu");
        dialog.setCancelable(false);
        dialog.show();
    }

    private void dismissPaymentCreditCardProgress() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
    private void showPaymentSuccessLayout() {
        layoutPaymentSuccess.setVisibility(View.VISIBLE);
    }
    private void hidePaymentSuccessLayout() {
        layoutPaymentSuccess.setVisibility(View.GONE);
    }

    private void payViaCreditCard(String virtualAccountNumber, String noId, String paymentMethod, String channel, String transactionAmount, String currency, String description) {
        showPaymentCreditCardProgress("Menghubungkan ke pembayaran");

        String url = AppConfig.getBaseUrlCreditCardPayment().concat("api/tesVa/submit_va");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        PayViaCcRequest requestBody = new PayViaCcRequest(virtualAccountNumber, noId, paymentMethod, channel, transactionAmount, currency,description);
        Call<PayViaCcResponse> call = api.payUsingCreditCard(url, requestBody);
        call.enqueue(new Callback<PayViaCcResponse>() {
            @Override
            public void onResponse(Call<PayViaCcResponse> call, Response<PayViaCcResponse> response) {
                if (response.body() != null) {
                    PayViaCcResponse responseBody = response.body();

                    handleResponse(responseBody);

                    dismissPaymentCreditCardProgress();
                }
            }

            @Override
            public void onFailure(Call<PayViaCcResponse> call, Throwable t) {
                dismissPaymentCreditCardProgress();
                showAlertDialog("Terjadi kesalahan", "Terjadi kesalahan dalam memproses pembayaran.\nError : " + t.getMessage());
            }
        });
    }

    /**
     * Launch payment page if the payment is not paid
     * @param responseBody
     */
    private void handleResponse(PayViaCcResponse responseBody) {

        if (responseBody != null) {

            String result = responseBody.getResult(); //0 : Failed, 1 : Success
            String message = responseBody.getMessage();
            String redirectUrl = responseBody.getRedirectURL();
            String noVa = responseBody.getNo_va();
            String paymentMethod = responseBody.getPayment_method();

            if (result.equalsIgnoreCase(WAITING_FOR_PAYMENT)) {

                if (!TextUtils.isEmpty(redirectUrl)) {
                    showPaymentUnpaidAlertDialog(redirectUrl);
                } else {
                    showAlertDialog("Terjadi kesalahan", "Tidak dapat mengakses web pembayaran karena redirect URL kosong");
                }

            } else if (result.equalsIgnoreCase(PAID)) {
                onClickLanjut();
            } else {
                showAlertDialog("Terjadi kesalahan", "Status pembayaran tidak diketahui. Pesan error : " + message);
            }

        }

    }

    private void launchWebView(String paymentUrl) {
        Intent intent = new Intent(getActivity(), E_CreditCardPaymentActivity.class);
        intent.putExtra(Const.INTENT_KEY_WEBVIEW_URL, paymentUrl);
        startActivityForResult(intent, RequestCode.REQ_PAYMENT);
    }

    private void showAlertDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss());
        builder.show();
    }

    private void showPaymentUnpaidAlertDialog(String redirectUrl) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Tidak dapat melakukan submit");
        builder.setMessage("Anda harus melakukan pembayaran menggunakan metode pembayaran yang telah dipilih (credit card) untuk dapat melakukan submit SPAJ. Apakah Anda ingin melakukan pembayaran sekarang?");
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            launchWebView(redirectUrl);
            dialog.dismiss();
        });
        builder.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss());
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        switch (requestCode) {
            case RequestCode.REQ_PAYMENT :
                if (resultCode == RESULT_OK) {
                    E_Update update = new E_Update(getActivity());
                    String transactionNumber = data.getStringExtra(Const.INTENT_KEY_TRANSACTION_NUMBER);
                    String paymentStatus = data.getStringExtra(Const.INTENT_KEY_PAYMENT_STATUS);
                    String paymentMessage = data.getStringExtra(Const.INTENT_KEY_PAYMENT_MESSAGE);

                    if (isPaymentSuccessful(paymentStatus)) {

                        showPaymentSuccessLayout();

                        new Handler().postDelayed(() -> {
                            hidePaymentSuccessLayout();

                            String spajIdTab = me.getModelID().getSPAJ_ID_TAB();
                            update.updateTransactionNumber(spajIdTab, String.valueOf(Const.CREDIT_CARD_PAYMENT_CODE), transactionNumber);

                        }, 1000);

                        //Submit data to server
                        displayPaymentSuccessDialog(transactionNumber);

                    } else {
                        showAlertDialog("Terjadi kesalahan dalam pembayaran", "Kode transaksi "+transactionNumber+ ". Kode error pembayaran "+paymentStatus+". "+paymentMessage);
                    }

                }
                break;
            default:
                break;
        }
    }

    private boolean isPaymentSuccessful(String paymentStatus) {
        return paymentStatus.equalsIgnoreCase(SUCCESS_CODE);
    }

    private void displayNoInternetConnection() {
        android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Tidak ada koneksi internet");
        builder.setMessage("Anda membutuhkan koneksi internet untuk melanjutkan ke halaman selanjutnya. Mohon periksa kembali koneksi internet Anda dan coba kembali");
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss());
        builder.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss());
        builder.show();
    }

    private void displayPaymentSuccessDialog(String transactionNumber) {
        android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Berhasil dibayar");
        builder.setMessage("Terima kasih telah melakukan pembayaran. Status pembayaran untuk SPAJ ini sudah dibayar dengan kode transaksi "+transactionNumber);
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            dialog.dismiss();
            onClickLanjut();
        });
        builder.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss());
        builder.show();
    }
}
