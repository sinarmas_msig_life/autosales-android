package id.co.ajsmsig.nb.pointofsale.ui.ahome

import android.app.Application
import android.arch.lifecycle.*
import android.databinding.ObservableInt
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 21/02/18.
 */
class HomeVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository, val sharedViewModel: SharedViewModel): AndroidViewModel(application) {

    var observablePageOptions: MediatorLiveData<List<PageOption>> = MediatorLiveData()
    var row = 3

    var page: Page? = null
    set(value) {

        sharedViewModel.startFromLifeStage = false

        val pageOptions = repository.getPageOptionsFromPage(value)
        observablePageOptions.addSource(pageOptions, observablePageOptions::setValue)
    }


}