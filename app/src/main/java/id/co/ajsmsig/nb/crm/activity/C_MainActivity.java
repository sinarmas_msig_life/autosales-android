package id.co.ajsmsig.nb.crm.activity;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.adapter.C_HomePagerAdapter;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.database.MemberTable;
import id.co.ajsmsig.nb.crm.fragment.AgencyLeadFragment;
import id.co.ajsmsig.nb.crm.fragment.C_AgentListProposalFragment;
import id.co.ajsmsig.nb.crm.fragment.C_AgentListSpajFragment;
import id.co.ajsmsig.nb.crm.fragment.C_DashboardFragment;
import id.co.ajsmsig.nb.crm.fragment.listlead.C_ListLeadFragment;
import id.co.ajsmsig.nb.crm.fragment.C_MoreFragment;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.method.async.WsApiRestClientUsage;
import id.co.ajsmsig.nb.crm.model.MemberModel;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.leader.subordinate.SubordinateFragment;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.util.Const;


public class C_MainActivity extends AppCompatActivity implements WsApiRestClientUsage.MemberIDListener {
    private static final String TAG = C_MainActivity.class.getSimpleName();

    @BindView(R.id.pager)
    ViewPager viewPager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tab_layout)
    TabLayout tab_layout;

    private C_HomePagerAdapter adapter;


    private int JENIS_LOGIN = 0;
    private int JENIS_LOGIN_BC = 0;
    private int ROLE_ID = 0;

    private ProgressDialog progressDialog;

    private WsApiRestClientUsage wsRestClientUsage;

    private MemberModel me;

    private String OTP;

    private Double HITUNG;
    private final String TAB_TITLE_DASHBOARD = "Dashboard";
    private final String TAB_TITLE_LEADS = "Leads";
    private final String TAB_TITLE_LIST_PROPOSAL = "Proposal";
    private final String TAB_TITLE_LIST_SPAJ = "SPAJ";
    private final String TAB_TITLE_LAINNYA = "Lainnya";
    private final String TAB_TITLE_LEADER_MONITORING = "Subordinate";
    private final String TAB_TITLE_LEADER_PROFILE = "Profile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        wsRestClientUsage = new WsApiRestClientUsage(this);
        wsRestClientUsage.setonMemberIDListener(this);
        initiate();

    }

    private void initiate() {

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        String kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
        JENIS_LOGIN = sharedPreferences.getInt("JENIS_LOGIN", 0);
        JENIS_LOGIN_BC = sharedPreferences.getInt("JENIS_LOGIN_BC", 0);
        String lca_id = sharedPreferences.getString("KODE_REG1", "");//37
        String lwk_id = sharedPreferences.getString("KODE_REG2", "");//m1
        String lsrg_id = sharedPreferences.getString("KODE_REG3", "");//05
        ROLE_ID = sharedPreferences.getInt("ROLE_ID",0);
        String KODE_REGIONAL = lca_id + lwk_id + lsrg_id;

        setupFabricLogging(kodeAgen);

        List<String> menuList = new C_Select(C_MainActivity.this).getUserMenuForUser(kodeAgen);

        if (menuList.isEmpty()) {
            displayLogoutRequired();
        }

        setupViewPager(viewPager, menuList);


        //setupTabIcon();

        if (new E_Select(this).getCountDokumen(kodeAgen) > 0) {
            MethodSupport.Alert(this, getString(R.string.title_error), getString(R.string.error_same_dokumen), 0);

        }
    }

    private void displayLogoutRequired() {
        new AlertDialog.Builder(C_MainActivity.this)
                .setTitle("Aksi logout diperlukan")
                .setMessage("Mohon maaf, namun logout diperlukan pada update ini untuk menginstall pembaharuan.")
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    dialog.dismiss();
                    logout();
                })
                .show();
    }
    private void logout() {
        SharedPreferences sharedPreferences = getSharedPreferences(getResources().getString(R.string.app_preferences), MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        Intent intent = new Intent(C_MainActivity.this, C_LoginActivity.class);
        startActivity(intent);
        finish();
    }
    private void setupViewPager(ViewPager viewPager, List<String> menuList) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        if (!menuList.isEmpty()) {

            List<String> sortedOrderMenuId = getSortedOrderMenuId(menuList);

            for (String menuId : sortedOrderMenuId) {

                if (!TextUtils.isEmpty(menuId)) {

                    switch (menuId) {
                        case "2143":
                            adapter.addFragment(new C_DashboardFragment(), TAB_TITLE_DASHBOARD);
                            break;
                        case "2144":
                            adapter.addFragment(new C_ListLeadFragment(), TAB_TITLE_LEADS);
                            break;
                        case "2145":
                            adapter.addFragment(new C_AgentListProposalFragment(), TAB_TITLE_LIST_PROPOSAL);
                            break;
                        case "2146":
                            adapter.addFragment(new C_AgentListSpajFragment(), TAB_TITLE_LIST_SPAJ);
                            break;
                        case "2149" :
                            adapter.addFragment(new AgencyLeadFragment(), TAB_TITLE_LEADS);
                            break;
                        case "2147":
                            adapter.addFragment(new C_MoreFragment(), TAB_TITLE_LAINNYA);
                            break;
                        case "2148":
                            break;
                        case "2159" :
                            adapter.addFragment(new SubordinateFragment(), TAB_TITLE_LEADER_MONITORING);
                            hideTabLayout();
                            setToolbarTitle("Leader Monitoring");
                            break;
                        default:
                            break;
                    }
                }

            }

        }

        viewPager.setAdapter(adapter);
        tab_layout.setupWithViewPager(viewPager);
        setupTabIcon();
    }

    /**
     * Return true if only menu id 2147 (menu 'App General Info') is exist on the list
     *
     * @param menuIdList
     * @return
     */
    private boolean isContainsLainnyaTab(List<String> menuIdList) {
        if (menuIdList != null && !menuIdList.isEmpty()) {
            return menuIdList.contains("2147");
        }
        return false;
    }

    /**
     * Goals : To put menu id 2147 (menu 'App General Info') at the last position of the array
     * by skipping menu id 2147
     *
     * @param menuIdList
     * @return
     */
    private List<String> getSortedOrderMenuId(List<String> menuIdList) {
        if (menuIdList != null && !menuIdList.isEmpty()) {

            List<String> sortedMenuId = new ArrayList<>();

            for (String menuId : menuIdList) {
                if (!menuId.equalsIgnoreCase("2147")) {
                    sortedMenuId.add(menuId);
                }
            }


            if (isContainsLainnyaTab(menuIdList)) {
                //Put menu 2147 (App General Info) to the last position of the tab
                sortedMenuId.add("2147");
            }

            return sortedMenuId;

        }

        return null;
    }

    private void setupTabIcon() {

        int tabCount = tab_layout.getTabCount();
        for (int i = 0; i < tabCount; i ++) {

            if (tab_layout.getTabAt(i).getText() != null) {

                if (!TextUtils.isEmpty(tab_layout.getTabAt(i).getText().toString())) {

                    String tabTitle = tab_layout.getTabAt(i).getText().toString();

                    switch (tabTitle) {
                        case TAB_TITLE_DASHBOARD :
                            if (tab_layout.getTabAt(i) != null) tab_layout.getTabAt(i).setIcon(R.drawable.menu_dashboard);
                            break;
                        case TAB_TITLE_LEADS :
                            if (tab_layout.getTabAt(i) != null) tab_layout.getTabAt(i).setIcon(R.drawable.menu_lead);
                            break;
                        case TAB_TITLE_LIST_PROPOSAL :
                            if (tab_layout.getTabAt(i) != null) tab_layout.getTabAt(i).setIcon(R.drawable.menu_proposal);
                            break;
                        case TAB_TITLE_LIST_SPAJ :
                            if (tab_layout.getTabAt(i) != null) tab_layout.getTabAt(i).setIcon(R.drawable.menu_spaj);
                            break;
                        case TAB_TITLE_LAINNYA :
                            if (tab_layout.getTabAt(i) != null) tab_layout.getTabAt(i).setIcon(R.drawable.menu_more);
                            break;
                    }
                }



            }
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        int pageNum = viewPager.getCurrentItem();
        if (JENIS_LOGIN == 9) {
            hideMenuItem(menu, R.id.tarik_member_id);
            switch (pageNum) {
                case 0:
//                hideMenuItem(menu, R.id.action_logout);
                    hideMenuItem(menu, R.id.action_add_lead);
                    break;
                case 2:
//                showMenuItem(menu, R.id.action_logout);
                    hideMenuItem(menu, R.id.action_add_lead);
                    break;
                default:
                    hideMenuItem(menu, R.id.action_add_lead);
                    break;
            }
        } else {
            hideMenuItem(menu, R.id.action_add_lead);
            if(ROLE_ID == 1){
                hideMenuItem(menu, R.id.tarik_member_id);
            }else {
                showMenuItem(menu, R.id.tarik_member_id);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private void hideMenuItem(Menu menu, int menuId) {
        MenuItem item = menu.findItem(menuId);
        item.setVisible(false);
    }

    private void showMenuItem(Menu menu, int menuId) {
        MenuItem item = menu.findItem(menuId);
        item.setVisible(true);
    }

    public void showYourToast(String yString) {
        Toast.makeText(this, yString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.action_add_lead:
//                Intent intent = new Intent(this, C_FormSimpleLeadActivity.class);
//                startActivity(intent);
//                break; //karena dideklarasiin di dua tempat yaitu di C_MainActivity dan  fragment ListLead, makanya activitynya jadi numpuk

            case R.id.tarik_member_id:
                LayoutInflater inflater = getLayoutInflater();
                View popupSet = inflater.inflate(R.layout.e_popup_member, null);
                final PopupWindow pw = new PopupWindow(popupSet, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,
                        true);
                final EditText et_id_member = popupSet.findViewById(R.id.et_id_member);
//                et_id_member.setText("456790");
                Button btn_batal = popupSet.findViewById(R.id.btn_batal);
                btn_batal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pw.dismiss();
                    }
                });
                Button btn_ok = popupSet.findViewById(R.id.btn_ok);
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (et_id_member.getText().toString().length() > 0) {
                            String str_idmember = et_id_member.getText().toString().trim();
//                            if (str_idmember.equals(new E_Select(C_MainActivity.this).getCheckMember(str_idmember))) {
//                                showYourToast("ID Member hanya bisa di ambil 1x");
//                            } else { //aktifin kalau sudah live
                            if (StaticMethods.isNetworkAvailable(C_MainActivity.this)) {
                                getMember(et_id_member.getText().toString());
                                pw.dismiss();
                            } else {
                                MethodSupport.Alert(C_MainActivity.this, getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi), 0);
                                pw.dismiss();
                            }
//                            }
                        } else {
                            showYourToast("Masukan ID member terlebih dahulu");
                        }
                    }
                });

                pw.showAtLocation(popupSet, Gravity.CENTER, 0, 0);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getMember(String member) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Menarik Data Member");
        progressDialog.setMessage("Mohon Menunggu...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        wsRestClientUsage.getMemberID(member);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Konfirmasi...");
        dialog.setMessage("Apakah anda ingin keluar?");
        dialog.setNegativeButton(getString(R.string.tidak), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                    MethodSupport.active_view(1, btn_lanjut);
//                    MethodSupport.active_view(1, iv_next);
            }
        });
        dialog.setPositiveButton(getString(R.string.iya), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.show();
    }

    @Override
    public void onMemberIDSucceess(int statusCode, Header[] headers, JSONObject response) {
        progressDialog.dismiss();
        Log.i("result", response.toString());
        try {
            String no_policy = "";
            String ERROR = "";
            // JSONObject espaj=json.getJSONObject("ESPAJ");
            if (response.has("NOID")) {
                QueryUtil queryUtil = new QueryUtil(this);
                me = new MemberModel();
                me.setNOID(response.getString("NOID"));
                me.setFULLNAME(response.getString("FULLNAME"));
                me.setBTEXT(response.getString("BDATE"));
                if (response.getString("PREMI").equals("G")) {
                    me.setPACKET("35");
                } else if (response.getString("PREMI").equals("O")) {
                    me.setPACKET("36");
                } else if (response.getString("PREMI").equals("D")) {
                    me.setPACKET("37");
                }
                me.setSOCIALID(response.getString("SOCIALID"));
                me.setMPHONE(response.getString("MPHONE"));
                me.setEMAIL(response.getString("EMAIL"));
                me.setSPONSOR(response.getString("SPONSOR"));
                me.setUPLINE(response.getString("UPLINE"));
                me.setVIRTUAL_ACC(response.getString("NO_VA"));
                if (response.getString("SEXID").equals("F")) {
                    me.setSEXID("0");
                } else if (response.getString("SEXID").equals("M")) {
                    me.setSEXID("1");
                }
                me.setOTP(response.getString("OTP"));
                OTP = me.getOTP();
                me.setHITUNG(response.getDouble("HITUNG"));
                HITUNG = me.getHITUNG();
//                me.setNO_PROPOSAL_TAB(response.getString("NO_PROPOSAL_TAB"));
//                me.setNO_PROPOSAL(response.getString("NO_PROPOSAL"));
//                me.setSPAJ_ID_TAB(response.getString("SPAJ_ID_TAB"));
//                me.setSPAJ_ID(response.getString("SPAJ_ID"));
//                me.setVIRTUAL_ACC(response.getString("VIRTUAL_ACC"));
                ContentValues cv = new MemberTable(this).getContentValues(me);
                queryUtil.insert(getString(R.string.TABLE_MEMBER), cv);

//                if(HITUNG > 10800){//3 jam hangus id member nya (Produk SIAP2U)
//                    MethodSupport.Alert(this, getString(R.string.msg_kesalahan), getString(R.string.id_member_tidak_valid), 0);
//                }else {
                //OTP LAYOUT
                LayoutInflater inflater = getLayoutInflater();
                View popupSet = inflater.inflate(R.layout.e_popup_otp, null);
                final PopupWindow pw = new PopupWindow(popupSet, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,
                        true);
                final EditText et_otp = popupSet.findViewById(R.id.et_otp);

                Button btn_batalotp = popupSet.findViewById(R.id.btn_batalotp);
                btn_batalotp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pw.dismiss();
                    }
                });
                Button btn_okotp = popupSet.findViewById(R.id.btn_okotp);
                btn_okotp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (et_otp.getText().toString().length() > 0) {
                            if (StaticMethods.isNetworkAvailable(C_MainActivity.this)) {
                                String str_top = et_otp.getText().toString();
                                if (!str_top.equals(OTP)) {
                                    showYourToast("Kode OTP yang anda Input salah");
                                } else if (str_top.equals(OTP)) {
                                    Intent intent = new Intent(C_MainActivity.this, P_MainActivity.class);
                                    intent.putExtra(getString(R.string.noid_member), me.getNOID()); //id member di insert ke id lead tab
                                    intent.putExtra(Const.INTENT_KEY_ADD_PROPOSAL_MODE_SIAP2U, true);
                                    startActivity(intent);
                                    pw.dismiss();
                                }

                            } else {
                                MethodSupport.Alert(C_MainActivity.this, getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi), 0);
                                pw.dismiss();
                            }
                        } else {
                            showYourToast("Masukan kode OTP terlebih dahulu");
                        }
                    }
                });
                pw.showAtLocation(popupSet, Gravity.CENTER, 0, 0);
//                } // tutup fungsi val 3 jam id member hangus

            } else if (response.has("message")) {
                ERROR = response.getString("message");
                MethodSupport.Alert(this, getString(R.string.msg_kesalahan), ERROR, 0);
            } else {
                MethodSupport.Alert(this, getString(R.string.msg_kesalahan), getString(R.string.solusi_masalah_koneksi), 0);
            }
            // dialog.dismiss();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMemberIDFailure(String title, String message, JSONObject response) {
        progressDialog.dismiss();
        showYourToast("Tarik data id member gagal");

    }

    private void setupFabricLogging(String agentCode) {
        //Send agent code as an identifier when error occurred.
        Crashlytics.setUserIdentifier("Agent code : " + agentCode);
        Crashlytics.setString("version-name", getVersionName());
        Crashlytics.setInt("version-code", getVersionCode());
    }

    private int getVersionCode() {
        PackageInfo pinfo = null;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (pinfo != null) {
            return pinfo.versionCode;
        }
        return 0;
    }

    private String getVersionName() {
        PackageInfo pinfo = null;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (pinfo != null) {
            return pinfo.versionName;
        }
        return null;
    }

    private void setToolbarTitle(String title) {
        if (toolbar != null) {
            toolbar.setTitle(title);
        }

    }
    private void hideTabLayout() {
        if (tab_layout != null) {
            tab_layout.setVisibility(View.GONE);
        }
    }
}
