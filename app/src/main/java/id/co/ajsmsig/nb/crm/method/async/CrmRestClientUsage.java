package id.co.ajsmsig.nb.crm.method.async;
/*
 Created by faiz_f on 07/07/2017.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.C_Delete;
import id.co.ajsmsig.nb.crm.database.C_Insert;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.database.C_Update;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.apiresponse.DownloadActivityTypeForDropdownResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.DownloadSubActivityTypeForDropdownResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.response.Activity;
import id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.response.Data;
import id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.response.Data__;
import id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.response.UploadLeadResponse;
import id.co.ajsmsig.nb.crm.model.form.SimbakActivityModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakAddressModel;
import id.co.ajsmsig.nb.crm.model.form.SimbakLeadModel;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.util.AppConfig;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrmRestClientUsage {
    private final String TAG = CrmRestClientUsage.class.getSimpleName();
    private Context context;
    private Gson gson;

    private static class ListenerInfo {
        UploadCrmListener mUploadCrmListener;
        UploadLeadListener uploadLeadListener;
    }

    private ListenerInfo mListenerInfo;

    public CrmRestClientUsage(Context context) {
        this.context = context;
        gson = new Gson();
    }

    public void setOnUploadCrmListener(UploadCrmListener l) {
        getListenerInfo().mUploadCrmListener = l;
    }
    public void setUploadLeadListener(UploadLeadListener listener) {
        getListenerInfo().uploadLeadListener = listener;
    }


    private ListenerInfo getListenerInfo() {
        if (mListenerInfo != null) {
            return mListenerInfo;
        }
        mListenerInfo = new ListenerInfo();
        return mListenerInfo;
    }


    public interface UploadCrmListener {
        void onUploadCrmSuccess(Long leadId, String message, Boolean isUpdateLead);

        void onUploadCrmFail(String message);
    }

    public interface UploadLeadListener {
        void onUploadLeadSuccess(String message);
        void onUploadLeadFailed(String errorMessage);
    }

    /**
     * Executed when add new lead or when submit SPAJ (when submit lead data)
     *
     * @param leadTabIds
     */
    public void uploadLead(List<Long> leadTabIds) {
        ArrayList<SimbakLeadModel> leadList = new ArrayList<>();
        C_Select select = new C_Select(context);

        for (Long leadTabId : leadTabIds) {

            //Lead info
            SimbakLeadModel lead = select.getLeadInfo(String.valueOf(leadTabId));

            if (lead != null) {
                //Lead address
                long slTabId = lead.getSL_TAB_ID();
                SimbakAddressModel simbakAddressHouse = new C_Select(context).selectSimbakAddress(slTabId, 1);
                lead.setHome(simbakAddressHouse);

                //Lead activity
                ArrayList<SimbakActivityModel> simbakActivityModels = new C_Select(context).selectListActivityToUploadFromLead(slTabId);
                lead.setLIST_ACTIVITY(simbakActivityModels);

                leadList.add(lead);
            }

        }

        String jsonLead = gson.toJson(leadList);
        StaticMethods.backUpToJsonFile(jsonLead, "JsonLead", "JsonLead");

        uploadLeadDataToServer(jsonLead);

    }

    /**
     * Executed when user add new activity
     *
     * @param activityIds
     */
    public void createUploadActivityRequestBodyFrom(List<Long> activityIds) {
        ArrayList<SimbakLeadModel> leadList = new ArrayList<>();

        C_Select select = new C_Select(context);

        //Get SL TAB ID from the activity list
        List<Long> slTabIdList = new ArrayList<>();
        for (int i = 0; i < activityIds.size(); i++) {
            long slaTabId = activityIds.get(i);
            long slTabId = select.getSlTabIdFrom(slaTabId);
            slTabIdList.add(slTabId);
        }

        Set<Long> sets = new HashSet<>();
        sets.addAll(slTabIdList);

        slTabIdList.clear();

        slTabIdList.addAll(sets);

        for (int i = 0; i < slTabIdList.size(); i++) {
            Long idLead = slTabIdList.get(i);

            //Lead info
            SimbakLeadModel lead = select.getLeadInfo(String.valueOf(idLead));

            if (lead != null) {
                //Lead address
                long slTabId = lead.getSL_TAB_ID();
                SimbakAddressModel simbakAddressHouse = select.selectSimbakAddress(slTabId, 1);
                lead.setHome(simbakAddressHouse);

                //Lead activity
                ArrayList<SimbakActivityModel> simbakActivityModels = select.selectListActivityToUploadFromLead(slTabId);
                lead.setLIST_ACTIVITY(simbakActivityModels);

                leadList.add(lead);
            }

        }

        //Convert list of lead model to json format
        String jsonLead = gson.toJson(leadList);

        StaticMethods.backUpToJsonFile(jsonLead, "JsonLead", "JsonLead");
        uploadLeadDataToServer(jsonLead);

    }

    private void uploadLeadDataToServer(String body) {
        final ListenerInfo li = mListenerInfo;
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_preferences), Context.MODE_PRIVATE);
        final String kodeAgen = sharedPreferences.getString("KODE_AGEN", "");

        C_Update update = new C_Update(context);

        String url = AppConfig.getBaseUrlCRM().concat("v2/toll/sync_lead2?msag_id=" + kodeAgen);
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), body);
        Call<List<UploadLeadResponse>> call = api.uploadListActivity(url, requestBody);
        call.enqueue(new Callback<List<UploadLeadResponse>>() {
            @Override
            public void onResponse(Call<List<UploadLeadResponse>> call, Response<List<UploadLeadResponse>> serverResponse) {
                if (!serverResponse.isSuccessful()) {
                    if (li != null && li.uploadLeadListener != null)  {
                        li.uploadLeadListener.onUploadLeadFailed(serverResponse.message());
                        Log.v(TAG, "Upload Lead Failed");
                    }
                }

                if (serverResponse.body() != null) {
                    List<UploadLeadResponse> response = serverResponse.body();

                    String slId = null;
                    if (response.size() > 0) {


                        for (UploadLeadResponse lead : response) {
                            boolean state = lead.getState();

                            if (state) {

                                Data data = lead.getData();
                                slId = data.getSLID();

                                //Setup listener for validation when lead is not synchronized and user already on Proposal stage
                                if (!TextUtils.isEmpty(slId)) {
                                    if (li != null && li.uploadLeadListener != null)  {
                                        li.uploadLeadListener.onUploadLeadSuccess("Lead berhasil di upload");
                                        Log.v(TAG, "Upload Lead Success");
                                    }
                                }

                                String slTabId = data.getSLTABID();

                                update.updateExistingSlIdWith(slId, slTabId, 0);
                                update.updateExistingSlIdOnSimbakListActivityWith(slId, slTabId);
                                update.updateExistingProposalLeadIdWith(slId, slTabId);

                                if (lead.getActivity() != null) {
                                    List<Activity> activities = lead.getActivity();
                                    String slaId;
                                    for (Activity activity : activities) {

                                        boolean activityState = activity.getState();
                                        String activitySlaTabId = activity.getSlaTabId();

                                        if (activityState && activity.getData() != null) {
                                            Data__ activityData = activity.getData();
                                            slaId = activityData.getSLAID();

                                            update.updateExistingSlaIDWith(slaId, slId, 0);
                                        } else if (!activityState && activity.getData() == null) {
                                            Log.v(TAG, "Data aktivitas sudah ada di database");
                                            update.markAsUpdated(activitySlaTabId);
                                        } else if (!activityState && activity.getMsg().equalsIgnoreCase("data aktifitas sudah ada di database")) {
                                            Log.v(TAG, "Data aktivitas sudah ada di database");
                                            update.markAsUpdated(activitySlaTabId);
                                        }

                                    }
                                }
                            }

                        }

                    }

                    if (slId != null) {
                        if (li != null && li.mUploadCrmListener != null)
                            li.mUploadCrmListener.onUploadCrmSuccess(Long.parseLong(slId), "success", true);
                    } else {
                        if (li != null && li.mUploadCrmListener != null)
                            li.mUploadCrmListener.onUploadCrmSuccess(null, "success", true);
                    }


                } else {
                    //Response is null
                    if (li != null && li.mUploadCrmListener != null)
                        li.mUploadCrmListener.onUploadCrmSuccess(null, "success", false);
                }

            }

            @Override
            public void onFailure(Call<List<UploadLeadResponse>> call, Throwable t) {
                if (li != null && li.mUploadCrmListener != null)
                    li.mUploadCrmListener.onUploadCrmFail(t.getMessage());

                if (li!=null && li.uploadLeadListener != null) {
                    li.uploadLeadListener.onUploadLeadFailed("Tidak dapat mengunggah lead "+t.getMessage());
                }
            }
        });
    }

    public void getActivityTypeForDropdown(final String kodeAgen, final int jn_bank) {
        C_Delete delete = new C_Delete(context);
        C_Insert insert = new C_Insert(context);

        String url = AppConfig.getBaseUrlCRM().concat("v2/toll/get_actType");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);

        Call<List<DownloadActivityTypeForDropdownResponse>> call = api.getActivityTypeForDropdown(url, kodeAgen);
        call.enqueue(new Callback<List<DownloadActivityTypeForDropdownResponse>>() {
            @Override
            public void onResponse(Call<List<DownloadActivityTypeForDropdownResponse>> call, Response<List<DownloadActivityTypeForDropdownResponse>> response) {


                if (response.body() != null && response.body().size() > 0) {
                    List<DownloadActivityTypeForDropdownResponse> responseBody = response.body();
                    delete.deleteAll("JSON_ACTIVITY");

                    for (DownloadActivityTypeForDropdownResponse activity : responseBody) {
                        int savedId = activity.getSavedId();
                        String activityNameIn = activity.getIndonesia();
                        String activityNameEng = activity.getEnglish();
                        int level1 = activity.getLevel1();
                        int level2 = activity.getLevel2();
                        int level3 = activity.getLevel3();
                        int level4 = activity.getLevel4();

                        insert.insertToTableJsonDropdown("JSON_ACTIVITY", savedId, activityNameIn, activityNameEng, level1, level2, level3, level4, jn_bank);
                        getSubActivityTypeDropdown(kodeAgen, savedId, jn_bank);
                    }

                }
            }

            @Override
            public void onFailure(Call<List<DownloadActivityTypeForDropdownResponse>> call, Throwable t) {
                Log.e(TAG, "Terjadi kesalahan dalam mengunduh dropdown aktivitas. " + t.getMessage());
            }
        });

    }

    private void getSubActivityTypeDropdown(String kodeAgen, int limitAppCode, final int jn_bank) {
        C_Insert insert = new C_Insert(context);
        String url = AppConfig.getBaseUrlCRM().concat("v2/toll/get_actCode");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);

        Call<List<DownloadSubActivityTypeForDropdownResponse>> call = api.getSubActivityTypeForDropdown(url, kodeAgen, limitAppCode);
        call.enqueue(new Callback<List<DownloadSubActivityTypeForDropdownResponse>>() {
            @Override
            public void onResponse(Call<List<DownloadSubActivityTypeForDropdownResponse>> call, Response<List<DownloadSubActivityTypeForDropdownResponse>> response) {
                if (response.body() != null) {
                    List<DownloadSubActivityTypeForDropdownResponse> responseBody = response.body();

                    String selection = context.getString(R.string.TYPE) + " = ?";
                    String[] selectionArgs = new String[]{String.valueOf(limitAppCode)};
                    new QueryUtil(context).delete("JSON_SUB_ACTIVITY", selection, selectionArgs);

                    if (response.body().size() > 0) {

                        for (DownloadSubActivityTypeForDropdownResponse subActivity : responseBody) {
                            int savedId = subActivity.getSavedId();
                            String subActivityNameIn = subActivity.getIndonesia();
                            String subActivityNameEng = subActivity.getEnglish();
                            int appCode = subActivity.getAppCode();

                            insert.insertToTableJsonDropdown("JSON_SUB_ACTIVITY", savedId, subActivityNameIn, subActivityNameEng, appCode, 0, jn_bank);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<DownloadSubActivityTypeForDropdownResponse>> call, Throwable t) {
                Log.e(TAG, "Failed to get sub activity dropdown " + t.getMessage());
            }
        });
    }

}
