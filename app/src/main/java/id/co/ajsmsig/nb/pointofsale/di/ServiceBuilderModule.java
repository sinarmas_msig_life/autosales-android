package id.co.ajsmsig.nb.pointofsale.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import id.co.ajsmsig.nb.pointofsale.utils.AnalyticsService;

@Module
abstract class ServiceBuilderModule {

    @ContributesAndroidInjector
    abstract AnalyticsService contributeAnalyticsService();

}