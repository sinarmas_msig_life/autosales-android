package id.co.ajsmsig.nb.pointofsale.model.db.dynamic.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Product
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductWithBenefitAndRider


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface ProductDao: BaseDao<Product> {

    @Query("select * from Product")
    fun loadAllProducts(): LiveData<List<Product>>

    @Query("select * from Product")
    fun loadAllProductWithBenefitAndRider(): LiveData<List<ProductWithBenefitAndRider>>

    @Query("select * from Product where lsbsId = :id and group_Id = :groupId")
    fun loadProductsWithId(id: Int?, groupId: Int?): LiveData<List<Product>>

    @Query("select * from Product where lsbsId = :id and group_Id = :groupId limit 1")
    fun loadProductWithBenefitAndRiderWithId(id: Int?, groupId: Int?): LiveData<ProductWithBenefitAndRider>

    @Query("select * from Product where productId = :id limit 1")
    fun loadProductWith(id: Int?): LiveData<ProductWithBenefitAndRider>



}