package id.co.ajsmsig.nb.pointofsale.binding

import android.databinding.BindingAdapter
import android.widget.ArrayAdapter
import android.widget.Spinner
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.InputChoice

/**
 * Created by andreyyoshuamanik on 11/03/18.
 */

@BindingAdapter("app:selectedItem")
fun setSpinnerSelectedItemFromInput(spinner: Spinner, choice: InputChoice?) {
    if (spinner.adapter == null) return

    val choices = ArrayList<InputChoice>()
    for (i in 0 until spinner.adapter.count) {
        (spinner.adapter.getItem(i) as? InputChoice)?.let {
            choices.add(it)
        }
    }
    if (choices.isNotEmpty()) {
        spinner.setSelection(choices.indexOf(choice))
    }
}


@BindingAdapter("app:entries")
fun setSpinnerFromInputs(spinner: Spinner, input: List<InputChoice>?) {

    input?.let {
        val adapter = ArrayAdapter<InputChoice>(spinner.context, R.layout.pos_row_spinner, it)
        spinner.adapter = adapter
    }

}
