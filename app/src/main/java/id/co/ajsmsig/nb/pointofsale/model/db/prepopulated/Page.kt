package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Entity
class Page(
        @PrimaryKey
        val id: Int,
        var title: String?,
        var subtitle: String?,
        var imageName: String?,
        var additionalText: String?,
        var type: String?,
        var titleImage: String?
): Serializable