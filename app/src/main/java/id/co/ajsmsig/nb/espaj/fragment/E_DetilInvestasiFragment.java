package id.co.ajsmsig.nb.espaj.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.AutoCompleteTextViewClickListener;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.hardcode.RequestCode;
import id.co.ajsmsig.nb.espaj.activity.E_FormDataPenerimaManfaatActivity;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.adapter.ListDanaAdapter;
import id.co.ajsmsig.nb.espaj.adapter.ListManfaatAdapter;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.method.NumberTextWatcher;
import id.co.ajsmsig.nb.espaj.method.ProgressDialogClass;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBank;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBankCab;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDataDitunjukDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelJnsDanaDI;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;
import id.co.ajsmsig.nb.util.Const;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Bernard on 29/05/2017.
 */

public class E_DetilInvestasiFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, AdapterView.OnItemClickListener, CompoundButton.OnCheckedChangeListener, TextView.OnEditorActionListener {
    private boolean isBankValid;
    private boolean isBankBranchValid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);
        groupId = sharedPreferences.getInt("GROUP_ID", 0);
        JENIS_LOGIN = sharedPreferences.getInt("JENIS_LOGIN", 0);
        JENIS_LOGIN_BC = sharedPreferences.getInt("JENIS_LOGIN_BC", 2);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem menu_validasi = menu.findItem(R.id.menu_validasi);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.e_fragment_detil_investasi_layout, container,
                false);
        me = E_MainActivity.e_bundle.getParcelable(getString(R.string.modelespaj));
        ButterKnife.bind(this, view);
        ((E_MainActivity) getActivity()).setSecondToolbar("Detil Investasi (5/", 5);
        ListDana = new ArrayList<ModelJnsDanaDI>();
        ListManfaat = new ArrayList<ModelDataDitunjukDI>();
        //autocompletetextview
        ac_jenis_topup_premi_berkala_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jenis_topup_premi_berkala_di, this));
        ac_jenis_topup_premi_berkala_di.setOnClickListener(this);
        ac_jenis_topup_premi_berkala_di.setOnFocusChangeListener(this);
        ac_jenis_topup_premi_berkala_di.addTextChangedListener(new generalTextWatcher(ac_jenis_topup_premi_berkala_di));

        ac_jenis_topup_premi_tunggal_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jenis_topup_premi_tunggal_di, this));
        ac_jenis_topup_premi_tunggal_di.setOnClickListener(this);
        ac_jenis_topup_premi_tunggal_di.setOnFocusChangeListener(this);
        ac_jenis_topup_premi_tunggal_di.addTextChangedListener(new generalTextWatcher(ac_jenis_topup_premi_tunggal_di));

        ac_bntuk_pmbyranpremi_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_bntuk_pmbyranpremi_di, this));
        ac_bntuk_pmbyranpremi_di.setOnClickListener(this);
        ac_bntuk_pmbyranpremi_di.setOnFocusChangeListener(this);
        ac_bntuk_pmbyranpremi_di.addTextChangedListener(new generalTextWatcher(ac_bntuk_pmbyranpremi_di));

        ac_bank_invest_di.setOnClickListener(this);
        ac_bank_invest_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_bank_invest_di, this));
        ac_bank_invest_di.addTextChangedListener(new generalTextWatcher(ac_bank_invest_di));

        ac_jenis_tabungan_invest_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jenis_tabungan_invest_di, this));
        ac_jenis_tabungan_invest_di.setOnClickListener(this);
        ac_jenis_tabungan_invest_di.setOnFocusChangeListener(this);
        ac_jenis_tabungan_invest_di.addTextChangedListener(new generalTextWatcher(ac_jenis_tabungan_invest_di));

        ac_jenis_nasabah_invest_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jenis_nasabah_invest_di, this));
        ac_jenis_nasabah_invest_di.setOnClickListener(this);
        ac_jenis_nasabah_invest_di.setOnFocusChangeListener(this);
        ac_jenis_nasabah_invest_di.addTextChangedListener(new generalTextWatcher(ac_jenis_nasabah_invest_di));

        ac_sp_bank_autodebet_di.setOnClickListener(this);
        ac_sp_bank_autodebet_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_sp_bank_autodebet_di, this));
        ac_sp_bank_autodebet_di.addTextChangedListener(new generalTextWatcher(ac_sp_bank_autodebet_di));

        ac_jns_tabungan_autodebet_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jns_tabungan_autodebet_di, this));
        ac_jns_tabungan_autodebet_di.setOnClickListener(this);
        ac_jns_tabungan_autodebet_di.setOnFocusChangeListener(this);
        ac_jns_tabungan_autodebet_di.addTextChangedListener(new generalTextWatcher(ac_jns_tabungan_autodebet_di));

        ac_kurs_autodebet_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_kurs_autodebet_di, this));
        ac_kurs_autodebet_di.setOnClickListener(this);
        ac_kurs_autodebet_di.setOnFocusChangeListener(this);
        ac_kurs_autodebet_di.addTextChangedListener(new generalTextWatcher(ac_kurs_autodebet_di));

        ac_kurs_invest_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_kurs_invest_di, this));
        ac_kurs_invest_di.setOnClickListener(this);
        ac_kurs_invest_di.setOnFocusChangeListener(this);
        ac_kurs_invest_di.addTextChangedListener(new generalTextWatcher(ac_kurs_invest_di));

        ac_aktif_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_aktif_di, this));
        ac_aktif_di.setOnClickListener(this);
        ac_aktif_di.setOnFocusChangeListener(this);
        ac_aktif_di.addTextChangedListener(new generalTextWatcher(ac_aktif_di));

        ac_autodebet_premi_pertama_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_autodebet_premi_pertama_di, this));
        ac_autodebet_premi_pertama_di.setOnClickListener(this);
        ac_autodebet_premi_pertama_di.setOnFocusChangeListener(this);
        ac_autodebet_premi_pertama_di.addTextChangedListener(new generalTextWatcher(ac_autodebet_premi_pertama_di));

        ac_dana_invest_alokasi_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_dana_invest_alokasi_di, this));
        ac_dana_invest_alokasi_di.setOnClickListener(this);
        ac_dana_invest_alokasi_di.setOnFocusChangeListener(this);
        ac_dana_invest_alokasi_di.addTextChangedListener(new generalTextWatcher(ac_dana_invest_alokasi_di));

        ac_jangka_waktu_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jangka_waktu_di, this));
        ac_jangka_waktu_di.setOnClickListener(this);
        ac_jangka_waktu_di.setOnFocusChangeListener(this);
        ac_jangka_waktu_di.addTextChangedListener(new generalTextWatcher(ac_jangka_waktu_di));

        ac_jenis_bunga_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jenis_bunga_di, this));
        ac_jenis_bunga_di.setOnClickListener(this);
        ac_jenis_bunga_di.setOnFocusChangeListener(this);
        ac_jenis_bunga_di.addTextChangedListener(new generalTextWatcher(ac_jenis_bunga_di));

        ac_jenis_rollover_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_jenis_rollover_di, this));
        ac_jenis_rollover_di.setOnClickListener(this);
        ac_jenis_rollover_di.setOnFocusChangeListener(this);
        ac_jenis_rollover_di.addTextChangedListener(new generalTextWatcher(ac_jenis_rollover_di));

        ac_status_karyawan_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_status_karyawan_di, this));
        ac_status_karyawan_di.setOnClickListener(this);
        ac_status_karyawan_di.setOnFocusChangeListener(this);
        ac_status_karyawan_di.addTextChangedListener(new generalTextWatcher(ac_status_karyawan_di));

        ac_penarikan_bunga_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_penarikan_bunga_di, this));
        ac_penarikan_bunga_di.setOnClickListener(this);
        ac_penarikan_bunga_di.setOnFocusChangeListener(this);
        ac_penarikan_bunga_di.addTextChangedListener(new generalTextWatcher(ac_penarikan_bunga_di));

        ac_cara_bayar_rider_di.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_cara_bayar_rider_di, this));
        ac_cara_bayar_rider_di.setOnClickListener(this);
        ac_cara_bayar_rider_di.setOnFocusChangeListener(this);
        ac_cara_bayar_rider_di.addTextChangedListener(new generalTextWatcher(ac_cara_bayar_rider_di));

        //button
//        bt_lanjut_di.setOnClickListener(this);

        //editText
        et_premi_pokok_di.addTextChangedListener(new generalTextWatcher(et_premi_pokok_di));
        et_premi_pokok_di.addTextChangedListener(new NumberTextWatcher(et_premi_pokok_di));
        et_premi_berkala_di.addTextChangedListener(new generalTextWatcher(et_premi_berkala_di));
        et_premi_berkala_di.addTextChangedListener(new NumberTextWatcher(et_premi_berkala_di));
        et_premi_tunggal_di.addTextChangedListener(new generalTextWatcher(et_premi_tunggal_di));
        et_premi_tunggal_di.addTextChangedListener(new NumberTextWatcher(et_premi_tunggal_di));
        et_jumlah_di.addTextChangedListener(new generalTextWatcher(et_jumlah_di));
        et_jumlah_di.addTextChangedListener(new NumberTextWatcher(et_jumlah_di));
        et_cabang_di.addTextChangedListener(new generalTextWatcher(et_cabang_di));
        et_kota_di.addTextChangedListener(new generalTextWatcher(et_kota_di));
        et_nomer_rek_invest_di.addTextChangedListener(new generalTextWatcher(et_nomer_rek_invest_di));
        et_atas_nama_di.addTextChangedListener(new generalTextWatcher(et_atas_nama_di));
        et_ttlkuasa_di.addTextChangedListener(new generalTextWatcher(et_ttlkuasa_di));
        et_keterangan_di.addTextChangedListener(new generalTextWatcher(et_keterangan_di));
        et_cabang2_di.addTextChangedListener(new generalTextWatcher(et_cabang2_di));
        et_kota2_di.addTextChangedListener(new generalTextWatcher(et_kota2_di));
        et_no_rek_autodebet_di.addTextChangedListener(new generalTextWatcher(et_no_rek_autodebet_di));
        et_atas_nama_autodebet_di.addTextChangedListener(new generalTextWatcher(et_atas_nama_autodebet_di));
        et_tgl_debet_di.addTextChangedListener(new generalTextWatcher(et_tgl_debet_di));
        et_tgl_valid_di.addTextChangedListener(new generalTextWatcher(et_tgl_valid_di));
        et_premi_tambahan_di.addTextChangedListener(new generalTextWatcher(et_premi_tambahan_di));
        et_tanggal_jatuh_tempo_di.addTextChangedListener(new generalTextWatcher(et_tanggal_jatuh_tempo_di));
        et_bunga_di.addTextChangedListener(new generalTextWatcher(et_bunga_di));
        et_jumlah_investasi_di.addTextChangedListener(new generalTextWatcher(et_jumlah_investasi_di));
        et_jumlah_bunga_di.addTextChangedListener(new generalTextWatcher(et_jumlah_bunga_di));
        et_no_memo_di.addTextChangedListener(new generalTextWatcher(et_no_memo_di));
        et_fee_based_income_di.addTextChangedListener(new generalTextWatcher(et_fee_based_income_di));
        et_tanggal_nab_di.addTextChangedListener(new generalTextWatcher(et_tanggal_nab_di));
        et_jumlah_bunga_nab_di.addTextChangedListener(new generalTextWatcher(et_jumlah_bunga_nab_di));
        et_begdate_topup_di.addTextChangedListener(new generalTextWatcher(et_begdate_topup_di));
        et_presentase_rate_bp_di.addTextChangedListener(new generalTextWatcher(et_presentase_rate_bp_di));


        //CheckBox
        cb_ya_di.setOnCheckedChangeListener(this);
        cb_gunakan_no_rekening_pemegang_polis_untuk_transaksi_di.setOnCheckedChangeListener(this);
        cb_breakable_di.setOnCheckedChangeListener(this);
        cb_telemarketing_di.setOnCheckedChangeListener(this);
        //button di Activity
        btn_lanjut = getActivity().findViewById(R.id.btn_lanjut);
        btn_lanjut.setOnClickListener(this);
        btn_kembali = getActivity().findViewById(R.id.btn_kembali);
        btn_kembali.setOnClickListener(this);
        second_toolbar = ((E_MainActivity) getActivity()).getSecond_toolbar();
        iv_save = second_toolbar.findViewById(R.id.iv_save);
        iv_save.setOnClickListener(this);
        iv_next = second_toolbar.findViewById(R.id.iv_next);
        iv_next.setOnClickListener(this);
        iv_prev = second_toolbar.findViewById(R.id.iv_prev);
        iv_prev.setOnClickListener(this);

        //add Manfaat
        img_tambah_penerima_manfaat_di.setOnClickListener(this);
        tv_tambah_penerima_manfaat_di.setOnClickListener(this);

        restore();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        ProgressDialogClass.removeSimpleProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        MethodSupport.active_view(1, btn_lanjut);
        MethodSupport.active_view(1, iv_next);
        MethodSupport.active_view(1, btn_kembali);
        MethodSupport.active_view(1, iv_prev);
        ProgressDialogClass.removeSimpleProgressDialog();
        me = E_MainActivity.e_bundle.getParcelable(getString(R.string.modelespaj));
        try {
            if (me.getDetilAgenModel().getJENIS_LOGIN() == 2) {
                if (me.getUsulanAsuransiModel().getKd_produk_ua() == 190 && me.getUsulanAsuransiModel().getSub_produk_ua() == 3){
                    MethodSupport.load_setDropXml(R.xml.e_bentukpremi, ac_bntuk_pmbyranpremi_di, getContext(), 10);
                }else {
                    MethodSupport.load_setDropXml(R.xml.e_bentukpremi, ac_bntuk_pmbyranpremi_di, getContext(), 8);
                }
            } else if (me.getDetilAgenModel().getJENIS_LOGIN() == 9) {
                if(me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 12
                        ||me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 5
                        ||me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 8
                        ||me.getUsulanAsuransiModel().getKd_produk_ua() == 190 && me.getUsulanAsuransiModel().getSub_produk_ua() == 7
                        ||me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 7
                        ||me.getUsulanAsuransiModel().getKd_produk_ua() == 215 && me.getUsulanAsuransiModel().getSub_produk_ua() == 3
                        ||me.getUsulanAsuransiModel().getKd_produk_ua() == 215 && me.getUsulanAsuransiModel().getSub_produk_ua() == 1
                        ||me.getUsulanAsuransiModel().getKd_produk_ua() == 215 && me.getUsulanAsuransiModel().getSub_produk_ua() == 4){
                    MethodSupport.load_setDropXml(R.xml.e_bentukpremi, ac_bntuk_pmbyranpremi_di, getContext(), 10);
                }else{
                    MethodSupport.load_setDropXml(R.xml.e_bentukpremi, ac_bntuk_pmbyranpremi_di, getContext(), 3);
                }
            }

            MethodSupport.load_setDropXml(R.xml.e_spinjangka, ac_jangka_waktu_di, getContext(), 0);
            MethodSupport.load_setDropXml(R.xml.e_jenisbunga, ac_jenis_bunga_di, getContext(), 0);
            if (me.getDetilAgenModel().getJENIS_LOGIN() == 9) {
                MethodSupport.load_setDropXml(R.xml.e_jenis_tabungan, ac_jenis_tabungan_invest_di, getContext(), 5);
                MethodSupport.load_setDropXml(R.xml.e_jenis_tabungan, ac_jns_tabungan_autodebet_di, getContext(), 5);
            } else {
                MethodSupport.load_setDropXml(R.xml.e_jenis_tabungan, ac_jenis_tabungan_invest_di, getContext(), 0);
                MethodSupport.load_setDropXml(R.xml.e_jenis_tabungan, ac_jns_tabungan_autodebet_di, getContext(), 0);
            }
            if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 44) {
                MethodSupport.load_setDropXml(R.xml.e_jenis_nasabah, ac_jenis_nasabah_invest_di, getContext(), 6);
//                MethodSupport.load_setDropXml(R.xml.e_jenis_nasabah, ac_jenis_nasabah_invest_di, getContext(), 6);
            } else if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 50 || me.getDetilAgenModel().getJENIS_LOGIN_BC() == 51) {
                MethodSupport.load_setDropXml(R.xml.e_jenis_nasabah, ac_jenis_nasabah_invest_di, getContext(), 7);
            } else if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 56) { // BTN SYARIAH
                MethodSupport.load_setDropXml(R.xml.e_jenis_nasabah, ac_jenis_nasabah_invest_di, getContext(), 10);
            } else if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 60) { // BUKOPIN SYARIAH
                MethodSupport.load_setDropXml(R.xml.e_jenis_nasabah, ac_jenis_nasabah_invest_di, getContext(), 3);
            } else {
                MethodSupport.load_setDropXml(R.xml.e_jenis_nasabah, ac_jenis_nasabah_invest_di, getContext(), 0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        setAdapterBankPusat("", ac_bank_invest_di);
        checkForSelectedCentralBankNameIsOnTheList(ac_bank_invest_di.getText().toString().trim(), ac_bank_invest_di);

        setAdapterBankCabang("", ac_sp_bank_autodebet_di);
        checkForSelectedBankBranchNameIsOnTheList(ac_sp_bank_autodebet_di.getText().toString().trim(), ac_sp_bank_autodebet_di);

        setAdapterBerkala();
        setAdapterTunggal();
//        setAdapterJnsTabungan();
//        setAdapterJnsNasabah();
        setAdapterKurs();
        setAdapterYaTidak();
        setAdapterAlokasiDana();
        setAdapterRollover();
        setAdapterKaryawan();
        setAdapterBayarRider();

        /*if (isAlreadyHaveTransactionNumber() && isCreditCardPaymentType()) {
            disablePaymentDropdown();
        }*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_validasi:
                me.getDetilInvestasiModel().setValidation(Validation());
                loadview();
                MyTaskValidasi taskValidasi = new MyTaskValidasi();
                taskValidasi.execute();
                Method_Validator.onGetAllValidation(me, getContext(), ((E_MainActivity) getActivity()));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTaskValidasi extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_DETIL_INVESTASI);
            return null;
        }
    }

    private class MyTaskSavePage extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_DETIL_INVESTASI);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

   /* private void disablePaymentDropdown() {
        ac_bntuk_pmbyranpremi_di.setClickable(false);
        ac_bntuk_pmbyranpremi_di.setFocusable(false);
        ac_bntuk_pmbyranpremi_di.setFocusableInTouchMode(false);
        ac_bntuk_pmbyranpremi_di.setEnabled(false);
    }*/

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
//View di activity
            case R.id.btn_lanjut:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_next:
                onClickLanjut();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.iv_save:
                loadview();
                MyTaskSavePage taskSavePage = new MyTaskSavePage();
                taskSavePage.execute();

                break;
            case R.id.iv_prev:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;
            case R.id.btn_kembali:
                onClickBack();
//                ((E_MainActivity) getActivity()).loadingpage();
                break;

            //drop down
            case R.id.ac_jenis_topup_premi_berkala_di:
                ac_jenis_topup_premi_berkala_di.showDropDown();
                break;
            case R.id.ac_jenis_topup_premi_tunggal_di:
                ac_jenis_topup_premi_tunggal_di.showDropDown();
                break;
            case R.id.ac_bntuk_pmbyranpremi_di:
                ac_bntuk_pmbyranpremi_di.showDropDown();
                break;
            case R.id.ac_jenis_tabungan_invest_di:
                ac_jenis_tabungan_invest_di.showDropDown();
                break;
            case R.id.ac_jenis_nasabah_invest_di:
                ac_jenis_nasabah_invest_di.showDropDown();
                break;
            case R.id.ac_jns_tabungan_autodebet_di:
                ac_jns_tabungan_autodebet_di.showDropDown();
                break;
            case R.id.ac_kurs_autodebet_di:
                ac_kurs_autodebet_di.showDropDown();
                break;
            case R.id.ac_kurs_invest_di:
                ac_kurs_invest_di.showDropDown();
                break;
            case R.id.ac_aktif_di:
                ac_aktif_di.showDropDown();
                break;
            case R.id.ac_autodebet_premi_pertama_di:
                ac_autodebet_premi_pertama_di.showDropDown();
                break;
            case R.id.ac_dana_invest_alokasi_di:
                ac_dana_invest_alokasi_di.showDropDown();
                break;
            case R.id.ac_jangka_waktu_di:
                ac_jangka_waktu_di.showDropDown();
                break;
            case R.id.ac_jenis_bunga_di:
                ac_jenis_bunga_di.showDropDown();
                break;
            case R.id.ac_jenis_rollover_di:
                ac_jenis_rollover_di.showDropDown();
                break;
            case R.id.ac_status_karyawan_di:
                ac_status_karyawan_di.showDropDown();
                break;
            case R.id.ac_penarikan_bunga_di:
                ac_penarikan_bunga_di.showDropDown();
                break;
            case R.id.ac_cara_bayar_rider_di:
                ac_cara_bayar_rider_di.showDropDown();
                break;
            case R.id.ac_bank_invest_di:
                ac_bank_invest_di.showDropDown();
                break;
            case R.id.ac_sp_bank_autodebet_di:
                ac_sp_bank_autodebet_di.showDropDown();
                break;
//manfaat
            case R.id.img_tambah_penerima_manfaat_di:
                goToForm(ListManfaat.size());
                break;
            case R.id.tv_tambah_penerima_manfaat_di:
                goToForm(ListManfaat.size());
                break;
        }
    }

    public void goToForm(int position) {
        if (!ListManfaat.isEmpty()) {
            ListManfaat.clear();
            ListManfaat.addAll(listManfaatAdapter.getList());
        }
        Intent intent = new Intent(getContext(), E_FormDataPenerimaManfaatActivity.class);
        intent.putExtra(getString(R.string.pos), position);
        intent.putExtra(getString(R.string.modelmanfaat), (ListManfaat.size() == position) ? new ModelDataDitunjukDI() : ListManfaat.get(position));
        startActivityForResult(intent, RequestCode.FILL_MANFAAT_SPAJ);
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.ac_jenis_topup_premi_berkala_di:
                if (hasFocus) ac_jenis_topup_premi_berkala_di.showDropDown();
                break;
            case R.id.ac_jenis_topup_premi_tunggal_di:
                if (hasFocus) ac_jenis_topup_premi_tunggal_di.showDropDown();
                break;
            case R.id.ac_bntuk_pmbyranpremi_di:
                if (hasFocus) ac_bntuk_pmbyranpremi_di.showDropDown();
                break;
            case R.id.ac_jenis_tabungan_invest_di:
                if (hasFocus) ac_jenis_tabungan_invest_di.showDropDown();
                break;
            case R.id.ac_jenis_nasabah_invest_di:
                if (hasFocus) ac_jenis_nasabah_invest_di.showDropDown();
                break;
            case R.id.ac_jns_tabungan_autodebet_di:
                if (hasFocus) ac_jns_tabungan_autodebet_di.showDropDown();
                break;
            case R.id.ac_kurs_autodebet_di:
                if (hasFocus) ac_kurs_autodebet_di.showDropDown();
                break;
            case R.id.ac_kurs_invest_di:
                if (hasFocus) ac_kurs_invest_di.showDropDown();
                break;
            case R.id.ac_aktif_di:
                if (hasFocus) ac_aktif_di.showDropDown();
                break;
            case R.id.ac_autodebet_premi_pertama_di:
                if (hasFocus) ac_autodebet_premi_pertama_di.showDropDown();
                break;
            case R.id.ac_dana_invest_alokasi_di:
                if (hasFocus) ac_dana_invest_alokasi_di.showDropDown();
                break;
            case R.id.ac_jangka_waktu_di:
                if (hasFocus) ac_jangka_waktu_di.showDropDown();
                break;
            case R.id.ac_jenis_bunga_di:
                if (hasFocus) ac_jenis_bunga_di.showDropDown();
                break;
            case R.id.ac_jenis_rollover_di:
                if (hasFocus) ac_jenis_rollover_di.showDropDown();
                break;
            case R.id.ac_status_karyawan_di:
                if (hasFocus) ac_status_karyawan_di.showDropDown();
                break;
            case R.id.ac_penarikan_bunga_di:
                if (hasFocus) ac_penarikan_bunga_di.showDropDown();
                break;
            case R.id.ac_cara_bayar_rider_di:
                if (hasFocus) ac_cara_bayar_rider_di.showDropDown();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.ac_jenis_topup_premi_berkala_di:
//                ModelDropdownInt modelTopUpPremiBerkala = (ModelDropdownInt) parent.getAdapter().getItem(position);

                break;
            case R.id.ac_jenis_topup_premi_tunggal_di:
//                ModelDropdownInt modelTopUpPremiTunggal = (ModelDropdownInt) parent.getAdapter().getItem(position);

                break;
            case R.id.ac_bntuk_pmbyranpremi_di:
                ModelDropdownInt modelBntukPmbyranpremi = (ModelDropdownInt) parent.getAdapter().getItem(position);
                bntuk_pmbyranpremi = modelBntukPmbyranpremi.getId();
                break;
            case R.id.ac_bank_invest_di:
                //To make sure job description is only allowed picked from the dropdown
                isBankValid = true;
                iv_circle_bank_invest_di.setColorFilter(ContextCompat.getColor(getActivity(), R.color.green));


                ModelBank modelBank_invest = (ModelBank) parent.getAdapter().getItem(0);
                bank_invest = modelBank_invest.getLSBP_ID();
                pjg_rek = modelBank_invest.getLSBP_PANJANG_REKENING();
                min_pjg_rek = modelBank_invest.getLSBP_MIN_PANJANG_REKENING();
                if (bntuk_pmbyranpremi == 1) {
                    InputFilter[] filters = new InputFilter[1];
                    filters[0] = new InputFilter.LengthFilter(16);
                    et_nomer_rek_invest_di.setFilters(filters);
                } else {
                    InputFilter[] filters = new InputFilter[1];
                    filters[0] = new InputFilter.LengthFilter(pjg_rek);
                    et_nomer_rek_invest_di.setFilters(filters);
                }
                break;
            case R.id.ac_jenis_tabungan_invest_di:
                ModelDropdownInt model_jnstab = (ModelDropdownInt) parent.getAdapter().getItem(position);
                jenis_tabungan_invest = model_jnstab.getId();
                break;
            case R.id.ac_jenis_nasabah_invest_di:
                ModelDropdownInt model_jnsnas = (ModelDropdownInt) parent.getAdapter().getItem(position);
                jenis_nasabah_invest = model_jnsnas.getId();
                break;
            case R.id.ac_sp_bank_autodebet_di:
                //To make sure job description is only allowed picked from the dropdown
                isBankBranchValid = true;
                iv_circle_sp_bank_autodebet_di.setColorFilter(ContextCompat.getColor(getContext(), R.color.green));

                ModelBankCab modelBank_autodebet = (ModelBankCab) parent.getAdapter().getItem(0);
                bank_autodebet = modelBank_autodebet.getLBN_ID();
                pjg_rek_autodebet = modelBank_autodebet.getLSBP_PANJANG_REKENING();
                InputFilter[] filters = new InputFilter[1];
                filters[0] = new InputFilter.LengthFilter(pjg_rek);
                et_no_rek_autodebet_di.setFilters(filters);
                break;
            case R.id.ac_jns_tabungan_autodebet_di:
                ModelDropdownInt modelJns_tabungan_autodebet = (ModelDropdownInt) parent.getAdapter().getItem(position);
                jns_tabungan_autodebet = modelJns_tabungan_autodebet.getId();
                break;
            case R.id.ac_kurs_autodebet_di:
                ModelDropDownString modelKurs_autodebet = (ModelDropDownString) parent.getAdapter().getItem(position);
                kurs_autodebet = modelKurs_autodebet.getId();
                break;
            case R.id.ac_kurs_invest_di:
                ModelDropDownString modelKurs_autoinvest = (ModelDropDownString) parent.getAdapter().getItem(position);
                kurs_invest = modelKurs_autoinvest.getId();
                break;
            case R.id.ac_aktif_di:
//                ModelDropdownInt modelAktif = (ModelDropdownInt) parent.getAdapter().getItem(position);
//                aktif = modelAktif.getId();
                break;
            case R.id.ac_autodebet_premi_pertama_di:
//                ModelDropdownInt modelAutodebet_premi_pertama = (ModelDropdownInt) parent.getAdapter().getItem(position);
//                autodebet_premi_pertama = modelAutodebet_premi_pertama.getId();
                break;
            case R.id.ac_dana_invest_alokasi_di:
//                ModelDropdownInt modelDana_invest_alokasi = (ModelDropdownInt) parent.getAdapter().getItem(position);
//                dana_invest_alokasi = modelDana_invest_alokasi.getId();
                break;
            case R.id.ac_jangka_waktu_di:
                ModelDropdownInt modelJangka_waktu = (ModelDropdownInt) parent.getAdapter().getItem(position);
                jangka_waktu = modelJangka_waktu.getId();
                break;
            case R.id.ac_jenis_bunga_di:
                ModelDropdownInt modelJenis_bunga = (ModelDropdownInt) parent.getAdapter().getItem(position);
                jenis_bunga = modelJenis_bunga.getId();
                break;
            case R.id.ac_jenis_rollover_di:
//              i  jns_rollover = modelJenis_rollover.getId();
                break;
            case R.id.ac_status_karyawan_di:
//                ModelDropdownInt modelStatus_karyawan = (ModelDropdownInt) parent.getAdapter().getItem(position);
//                sts_kryawan = modelStatus_karyawan.getId();
                break;
            case R.id.ac_penarikan_bunga_di:
                ModelDropdownInt modelPenarikan_bunga = (ModelDropdownInt) parent.getAdapter().getItem(position);
                pnrikan_bunga = modelPenarikan_bunga.getId();
                break;
            case R.id.ac_cara_bayar_rider_di:
//                ModelDropdownInt modelCara_bayar = (ModelDropdownInt) parent.getAdapter().getItem(position);
//                cr_bayar = modelCara_bayar.getId();
                break;

        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_ya_di:
                cb_ya = (isChecked) ? cb_ya_di.getText().toString() : "";
                break;
            case R.id.cb_gunakan_no_rekening_pemegang_polis_untuk_transaksi_di:
                cb_gunakan_no_rekening_pemegang_polis_untuk_transaksi = (isChecked) ? cb_gunakan_no_rekening_pemegang_polis_untuk_transaksi_di.getText().toString() : "";
                break;
            case R.id.cb_breakable_di:
                cb_breakable = (isChecked) ? cb_breakable_di.getText().toString() : "";
                break;
            case R.id.cb_telemarketing_di:
                cb_telemarketing = (isChecked) ? cb_telemarketing_di.getText().toString() : "";
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return false;
    }

    private class generalTextWatcher implements TextWatcher {
        private View view;

        generalTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                case R.id.ac_jenis_topup_premi_berkala_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_jenis_topup_premi_berkala_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_premi_berkala_di.setColorFilter(color);
                    tv_error_jenis_topup_premi_berkala_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_jenis_topup_premi_tunggal_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_jenis_topup_premi_tunggal_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_premi_tunggal_di.setColorFilter(color);
                    tv_error_jenis_topup_premi_tunggal_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_bntuk_pmbyranpremi_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_bntuk_pmbyranpremi_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_bntuk_pmbyranpremi_di.setColorFilter(color);
                    tv_error_bntuk_pmbyranpremi_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_bank_invest_di:
                    String centralBankName = ac_bank_invest_di.getText().toString().trim();
                    setAdapterBankPusat(centralBankName, ac_bank_invest_di);
                    validateCentralBankSelection(centralBankName, ac_bank_invest_di);
                    break;
                case R.id.ac_kurs_invest_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_kurs_invest_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_kurs_invest_di.setColorFilter(color);
                    tv_error_kurs_invest_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_jenis_tabungan_invest_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_jenis_tabungan_invest_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jenis_tabungan_invest_di.setColorFilter(color);
                    tv_error_jenis_tabungan_invest_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_jenis_nasabah_invest_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_jenis_nasabah_invest_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jenis_nasabah_invest_di.setColorFilter(color);
                    tv_error_jenis_nasabah_invest_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_sp_bank_autodebet_di:
                    String query = ac_sp_bank_autodebet_di.getText().toString().trim();
                    setAdapterBankCabang(query, ac_sp_bank_autodebet_di);
                    validateBankSelection(query, ac_sp_bank_autodebet_di);
                    break;
                case R.id.ac_jns_tabungan_autodebet_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_jns_tabungan_autodebet_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jns_tabungan_autodebet_di.setColorFilter(color);
                    tv_error_jns_tabungan_autodebet_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_kurs_autodebet_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_kurs_autodebet_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_kurs_autodebet_di.setColorFilter(color);
                    tv_error_kurs_autodebet_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_aktif_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_aktif_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    break;
                case R.id.ac_autodebet_premi_pertama_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_autodebet_premi_pertama_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
//                    iv_circle_autodebet_premi_pertama_di.setColorFilter(color);
//                    tv_error_autodebet_premi_pertama_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_dana_invest_alokasi_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_dana_invest_alokasi_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_dana_invest_alokasi_di.setColorFilter(color);
                    tv_error_dana_invest_alokasi_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_jangka_waktu_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_jangka_waktu_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jangka_waktu_di.setColorFilter(color);
                    tv_error_jangka_waktu_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_jenis_bunga_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_jenis_bunga_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jenis_bunga_di.setColorFilter(color);
                    tv_error_jenis_bunga_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_jenis_rollover_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_jenis_rollover_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_jenis_rollover_di.setColorFilter(color);
                    tv_error_jenis_rollover_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_status_karyawan_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_status_karyawan_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_status_karyawan_di.setColorFilter(color);
                    tv_error_status_karyawan_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_penarikan_bunga_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_penarikan_bunga_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_penarikan_bunga_di.setColorFilter(color);
                    tv_error_penarikan_bunga_di.setVisibility(View.GONE);
                    break;
                case R.id.ac_cara_bayar_rider_di:
                    if (StaticMethods.isTextWidgetEmpty(ac_cara_bayar_rider_di)) {
                        color = ContextCompat.getColor(getContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getContext(), R.color.green);
                    }
                    iv_circle_cara_bayar_rider_di.setColorFilter(color);
                    tv_error_cara_bayar_rider_di.setVisibility(View.GONE);
                    break;
                //EditText
                case R.id.et_premi_pokok_di:
                    Method_Validator.ValEditWatcher(et_premi_pokok_di, iv_circle_premi_pokok_di,
                            tv_error_premi_pokok_di, getContext());
                    break;
                case R.id.et_premi_berkala_di:
                    Method_Validator.ValEditWatcher(et_premi_berkala_di, iv_circle_premi_berkala_di,
                            tv_error_premi_berkala_di, getContext());
                    break;
                case R.id.et_premi_tunggal_di:
                    Method_Validator.ValEditWatcher(et_premi_tunggal_di, iv_circle_premi_tunggal_di,
                            tv_error_premi_tunggal_di, getContext());
                    break;
                case R.id.et_jumlah_di:
                    Method_Validator.ValEditWatcher(et_jumlah_di, iv_circle_jumlah_di,
                            tv_error_jumlah_di, getContext());
                    break;
                case R.id.et_nomer_rek_invest_di:
                    Method_Validator.ValEditWatcherRek(et_nomer_rek_invest_di, iv_circle_nomer_rek_invest_di,
                            tv_error_nomer_rek_invest_di, getContext(), min_pjg_rek);
                    break;
                case R.id.et_atas_nama_di:
                    Method_Validator.ValEditWatcher(et_atas_nama_di, iv_circle_atas_nama_di,
                            tv_error_atas_nama_di, getContext());
                    break;
                case R.id.et_ttlkuasa_di:
                    Method_Validator.ValEditWatcher(et_ttlkuasa_di, iv_circle_ttlkuasa_di,
                            tv_error_ttlkuasa_di, getContext());
                    break;
                case R.id.et_keterangan_di:
                    Method_Validator.ValEditWatcher(et_keterangan_di, iv_circle_keterangan_di,
                            tv_error_keterangan_di, getContext());
                    break;
                case R.id.et_no_rek_autodebet_di:
                    Method_Validator.ValEditWatcherRek(et_no_rek_autodebet_di, iv_circle_no_rek_autodebet_di,
                            tv_error_no_rek_autodebet_di, getContext(), min_pjg_rek);
                    break;
                case R.id.et_atas_nama_autodebet_di:
                    Method_Validator.ValEditWatcher(et_atas_nama_autodebet_di, iv_circle_atas_nama_autodebet_di,
                            tv_error_atas_nama_autodebet_di, getContext());
                    break;
                case R.id.et_premi_tambahan_di:
                    Method_Validator.ValEditWatcher(et_premi_tambahan_di, iv_circle_premi_tambahan_di,
                            tv_error_premi_tambahan_di, getContext());
                    break;
                case R.id.et_tanggal_jatuh_tempo_di:
                    Method_Validator.ValEditWatcher(et_tanggal_jatuh_tempo_di, iv_circle_tanggal_jatuh_tempo_di,
                            tv_error_tanggal_jatuh_tempo_di, getContext());
                    break;
                case R.id.et_bunga_di:
                    Method_Validator.ValEditWatcher(et_bunga_di, iv_circle_bunga_di,
                            tv_error_bunga_di, getContext());
                    break;
                case R.id.et_jumlah_investasi_di:
                    Method_Validator.ValEditWatcher(et_jumlah_investasi_di, iv_circle_jumlah_investasi_di,
                            tv_error_jumlah_investasi_di, getContext());
                    break;
                case R.id.et_jumlah_bunga_di:
                    Method_Validator.ValEditWatcher(et_jumlah_bunga_di, iv_circle_jumlah_bunga_di,
                            tv_error_jumlah_bunga_di, getContext());
                    break;
                case R.id.et_no_memo_di:
                    Method_Validator.ValEditWatcher(et_no_memo_di, iv_circle_no_memo_di,
                            tv_error_no_memo_di, getContext());
                    break;
                case R.id.et_fee_based_income_di:
                    Method_Validator.ValEditWatcher(et_fee_based_income_di, iv_circle_fee_based_income_di,
                            tv_error_fee_based_income_di, getContext());
                    break;
                case R.id.et_tanggal_nab_di:
                    Method_Validator.ValEditWatcher(et_tanggal_nab_di, iv_circle_tanggal_nab_di,
                            tv_error_tanggal_nab_di, getContext());
                    break;
                case R.id.et_jumlah_bunga_nab_di:
                    Method_Validator.ValEditWatcher(et_jumlah_bunga_nab_di, iv_circle_jumlah_bunga_nab_di,
                            tv_error_jumlah_bunga_nab_di, getContext());
                    break;
                case R.id.et_begdate_topup_di:
                    Method_Validator.ValEditWatcher(et_begdate_topup_di, iv_circle_begdate_topup_di,
                            tv_error_begdate_topup_di, getContext());
                    break;
                case R.id.et_presentase_rate_bp_di:
                    Method_Validator.ValEditWatcher(et_presentase_rate_bp_di, iv_circle_presentase_rate_bp_di,
                            tv_error_presentase_rate_bp_di, getContext());
                    break;
                case R.id.et_cabang_di:
                    Method_Validator.ValEditWatcher(et_cabang_di, iv_circle_cabang_di,
                            tv_error_cabang_di, getContext());
                    break;
                case R.id.et_kota_di:
                    Method_Validator.ValEditWatcher(et_kota_di, iv_circle_kota_di,
                            tv_error_kota_di, getContext());
                    break;
            }
        }
    }

    private void loadview() {
        me.getDetilInvestasiModel().setPremi_pokok_di(MethodSupport.ConverttoDouble(et_premi_pokok_di));
        me.getDetilInvestasiModel().setPremi_berkala_di(MethodSupport.ConverttoDouble(et_premi_berkala_di));
        me.getDetilInvestasiModel().setPremi_tunggal_di(MethodSupport.ConverttoDouble(et_premi_tunggal_di));
        me.getDetilInvestasiModel().setPremi_tambahan_di(premi_tambahan_di);
        me.getDetilInvestasiModel().setJumlah_di(jumlah);
        me.getDetilInvestasiModel().setPremitopup_berkala_di(premitopup_berkala_di);
        me.getDetilInvestasiModel().setPremitopup_tunggal_di(premitopup_tunggal_di);
        me.getDetilInvestasiModel().setInvest_bayar_premi_di(invest_bayar_bayar_premi_di);
        me.getDetilInvestasiModel().setInvest_bank_di(bank_invest);
        me.getDetilInvestasiModel().setInvest_cabang_di(et_cabang_di.getText().toString());
        me.getDetilInvestasiModel().setInvest_kota_di(et_kota_di.getText().toString());
        me.getDetilInvestasiModel().setInvest_jnstab_di(jenis_tabungan_invest);
        me.getDetilInvestasiModel().setInvest_jnsnasabah_di(jenis_nasabah_invest);
        me.getDetilInvestasiModel().setInvest_norek_di(et_nomer_rek_invest_di.getText().toString());
        me.getDetilInvestasiModel().setInvest_nama_di(et_atas_nama_di.getText().toString());
        me.getDetilInvestasiModel().setInvest_kurs_di(kurs_invest);
        me.getDetilInvestasiModel().setInvest_berikuasa_di(berikuasa_di_invest);
        me.getDetilInvestasiModel().setInvest_tglkuasa_di(tglkuasa_di_invest);
        me.getDetilInvestasiModel().setInvest_keterangan_di(keterangan_di_invest);
        me.getDetilInvestasiModel().setAutodbt_bank_di(bank_autodebet);
        me.getDetilInvestasiModel().setAutodbt_jnstab_di(jns_tabungan_autodebet);
        me.getDetilInvestasiModel().setAutodbt_kurs_di(kurs_autodebet);
        me.getDetilInvestasiModel().setAutodbt_norek_di(et_no_rek_autodebet_di.getText().toString());
        me.getDetilInvestasiModel().setAutodbt_nama_di(et_atas_nama_autodebet_di.getText().toString());
        me.getDetilInvestasiModel().setAutodbt_tgldebet_di(et_tgl_debet_di.getText().toString());
        me.getDetilInvestasiModel().setAutodbt_tglvalid_di(et_tgl_valid_di.getText().toString());
        me.getDetilInvestasiModel().setAutodbt_aktif_di(aktif);
        me.getDetilInvestasiModel().setAutodbt_premipertama_di(autodebet_premi_pertama);
        me.getDetilInvestasiModel().setDanainvest_alokasi_di(dana_invest_alokasi);
        me.getDetilInvestasiModel().setAutodbt_no_simascard_di(simascard_di_autodebet);
        me.getDetilInvestasiModel().setJumlah1_di(jumlah1_di);
        me.getDetilInvestasiModel().setPersen1_di(persen1_di);
        me.getDetilInvestasiModel().setJumlah2_di(jumlah2_di);
        me.getDetilInvestasiModel().setPersen2_di(persen2_di);
        me.getDetilInvestasiModel().setJumlah3_di(jumlah3_di);
        me.getDetilInvestasiModel().setPersen3_di(persen3_di);
        me.setListDanaDI(ListDana);
        ListManfaat.clear();
        ListManfaat.addAll(listManfaatAdapter.getList());
        me.setListManfaatDI(ListManfaat);
        E_MainActivity.e_bundle.putParcelable(getString(R.string.modelespaj), me);

    }

    private void restore() {
//        try {
        //edittext
        et_tgl_debet_di.setText(me.getDetilInvestasiModel().getAutodbt_tgldebet_di());
        et_tgl_valid_di.setText(me.getDetilInvestasiModel().getAutodbt_tglvalid_di());
        et_cabang_di.setText(me.getDetilInvestasiModel().getInvest_cabang_di());
        et_kota_di.setText(me.getDetilInvestasiModel().getInvest_kota_di());
        et_nomer_rek_invest_di.setText(me.getDetilInvestasiModel().getInvest_norek_di());
        et_atas_nama_di.setText(me.getDetilInvestasiModel().getInvest_nama_di());
        et_no_rek_autodebet_di.setText(me.getDetilInvestasiModel().getAutodbt_norek_di());
        et_atas_nama_autodebet_di.setText(me.getDetilInvestasiModel().getAutodbt_nama_di());


        if (me.getDetilInvestasiModel().getPremi_berkala_di() != 0) {
            et_premi_berkala_di.setText(new BigDecimal(me.getDetilInvestasiModel().getPremi_berkala_di()).toString());
            ac_jenis_topup_premi_berkala_di.setText("BERKALA");
            premi_berkala = 1;
        }
        MethodSupport.active_view(0, et_premi_berkala_di);
        MethodSupport.active_view(0, ac_jenis_topup_premi_berkala_di);

        if (me.getDetilInvestasiModel().getPremi_tunggal_di() != 0) {
            et_premi_tunggal_di.setText(new BigDecimal(me.getDetilInvestasiModel().getPremi_tunggal_di()).toString());
            ac_jenis_topup_premi_tunggal_di.setText("TUNGGAL");
            premi_tunggal = 1;
        }
        MethodSupport.active_view(0, et_premi_tunggal_di);
        MethodSupport.active_view(0, ac_jenis_topup_premi_tunggal_di);

        et_premi_pokok_di.setText(new BigDecimal(me.getDetilInvestasiModel().getPremi_pokok_di()).toString());
        MethodSupport.active_view(0, et_premi_pokok_di);
        et_jumlah_di.setText(MethodSupport.onSetString(MethodSupport.ConverttoDouble(et_premi_pokok_di) + MethodSupport.ConverttoDouble(et_premi_berkala_di) + MethodSupport.ConverttoDouble(et_premi_tunggal_di) + MethodSupport.ConverttoDouble(et_premi_tambahan_di)));
        MethodSupport.active_view(0, et_jumlah_di);
        et_premi_tambahan_di.setText(new BigDecimal(me.getDetilInvestasiModel().getPremi_tambahan_di()).toString());
        MethodSupport.active_view(0, et_premi_tambahan_di);

        bntuk_pmbyranpremi = me.getUsulanAsuransiModel().getBentukbayar_ua();
        if (me.getDetilAgenModel().getJENIS_LOGIN() == 2) {
            if (me.getUsulanAsuransiModel().getKd_produk_ua() == 190 && me.getUsulanAsuransiModel().getSub_produk_ua() == 3){
                ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_bentukpremi, 10);
            }else {
                ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_bentukpremi, 8);
            }
        } else if (me.getDetilAgenModel().getJENIS_LOGIN() == 9) {
            if(me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 12
                    ||me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 5
                    ||me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 8
                    ||me.getUsulanAsuransiModel().getKd_produk_ua() == 190 && me.getUsulanAsuransiModel().getSub_produk_ua() == 7
                    ||me.getUsulanAsuransiModel().getKd_produk_ua() == 134 && me.getUsulanAsuransiModel().getSub_produk_ua() == 7
                    ||me.getUsulanAsuransiModel().getKd_produk_ua() == 215 && me.getUsulanAsuransiModel().getSub_produk_ua() == 3
                    ||me.getUsulanAsuransiModel().getKd_produk_ua() == 215 && me.getUsulanAsuransiModel().getSub_produk_ua() == 1
                    ||me.getUsulanAsuransiModel().getKd_produk_ua() == 215 && me.getUsulanAsuransiModel().getSub_produk_ua() == 4){
                ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_bentukpremi, 10);
            }else{
                ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_bentukpremi, 3);
            }
        }

        if (bntuk_pmbyranpremi == 0 || me.getDetilAgenModel().getJENIS_LOGIN() == 2) {
            cl_no_rekening_pp_untuk_transaksi_autodebet_di.setVisibility(View.GONE);
        } else {
            cl_no_rekening_pp_untuk_transaksi_autodebet_di.setVisibility(View.VISIBLE);
        }
        ac_bntuk_pmbyranpremi_di.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownInt, bntuk_pmbyranpremi));

        bank_invest = me.getDetilInvestasiModel().getInvest_bank_di();
        pjg_rek = new E_Select(getContext()).getPanjangRek(bank_invest);
        min_pjg_rek = new E_Select(getContext()).getMinPanjangRek(bank_invest);
        ListDropDownBank = new E_Select(getContext()).getBankPusat("", JENIS_LOGIN, JENIS_LOGIN_BC);
        ac_bank_invest_di.setText(MethodSupport.getAdapterBank(ListDropDownBank, bank_invest));

        invest_bayar_bayar_premi_di = me.getDetilInvestasiModel().getInvest_bayar_premi_di();

        jenis_tabungan_invest = me.getDetilInvestasiModel().getInvest_jnstab_di();
        if (me.getDetilAgenModel().getJENIS_LOGIN() == 9) {
            ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_jenis_tabungan, 5);
        } else {
            ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_jenis_tabungan, 0);
        }
        ac_jenis_tabungan_invest_di.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownInt, jenis_tabungan_invest));

        jenis_nasabah_invest = me.getDetilInvestasiModel().getInvest_jnsnasabah_di();
        if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 44) {
            ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_jenis_nasabah, 6);
        } else if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 51 || me.getDetilAgenModel().getJENIS_LOGIN_BC() == 50) {
            ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_jenis_nasabah, 7);
        } else if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 56) {
            ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_jenis_nasabah, 10);
        } else if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 60) {
            ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_jenis_nasabah, 3);
        } else {
            ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_jenis_nasabah, 0);
        }
        ac_jenis_nasabah_invest_di.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownInt, jenis_nasabah_invest));

        kurs_invest = me.getDetilInvestasiModel().getInvest_kurs_di();
        ListDropDownString = new E_Select(getContext()).getLst_Kurs(me.getUsulanAsuransiModel().getKd_produk_ua(), me.getUsulanAsuransiModel().getSub_produk_ua());
        ac_kurs_invest_di.setText(MethodSupport.getAdapterPositionString(ListDropDownString, kurs_invest));

        bank_autodebet = me.getDetilInvestasiModel().getAutodbt_bank_di();
        pjg_rek_autodebet = new E_Select(getContext()).getPanjangRek(bank_invest);
        ListDropDownBankCabang = new E_Select(getContext()).getBankCab("", JENIS_LOGIN, JENIS_LOGIN_BC);
        ac_sp_bank_autodebet_di.setText(MethodSupport.getAdapterBankCab(ListDropDownBankCabang, bank_autodebet));

        jns_tabungan_autodebet = me.getDetilInvestasiModel().getAutodbt_jnstab_di();
        if (me.getDetilAgenModel().getJENIS_LOGIN() == 9) {
            ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_jenis_tabungan, 5);
        } else {
            ListDropDownInt = MethodSupport.getXML(getContext(), R.xml.e_jenis_tabungan, 0);
        }
        ac_jns_tabungan_autodebet_di.setText(MethodSupport.getAdapterPositionSPAJ(ListDropDownInt, jns_tabungan_autodebet));

        kurs_autodebet = me.getDetilInvestasiModel().getAutodbt_kurs_di();
        ListDropDownString = new E_Select(getContext()).getLst_Kurs(me.getUsulanAsuransiModel().getKd_produk_ua(), me.getUsulanAsuransiModel().getSub_produk_ua());
        ac_kurs_autodebet_di.setText(MethodSupport.getAdapterPositionString(ListDropDownString, kurs_autodebet));

        aktif = 1; //autodebet
//            ac_aktif_di.setText(MethodSupport.getAdapterPositionArray(ac_aktif_di.getAdapter(), aktif));
        autodebet_premi_pertama = me.getDetilInvestasiModel().getAutodbt_premipertama_di();

        if (new E_Select(getContext()).selectLinkNonLink(me.getUsulanAsuransiModel().getKd_produk_ua()) == 17) {
            dana_invest_alokasi = 1;
            ac_dana_invest_alokasi_di.setText(MethodSupport.getAdapterPositionArray(getResources().getStringArray(R.array.dana_invest_alokasi), dana_invest_alokasi));
            MethodSupport.active_view(0, ac_dana_invest_alokasi_di);
        } else {
            dana_invest_alokasi = 1;
            cl_dana_investasi_yang_dialokasikan_di.setVisibility(View.GONE);
        }
        ListDana = me.getListDanaDI();
        if (!ListDana.isEmpty()) {
            tv_jenis_data_investasi_ket_di.setVisibility(View.GONE);
//                ll_tambah_rider_ua.setVisibility(View.VISIBLE);
            lv_jenis_dana_investasi.setVisibility(View.VISIBLE);
            lv_jenis_dana_investasi.setAdapter(new ListDanaAdapter(getActivity(), R.layout.e_adapter_rider, ListDana));
            lv_jenis_dana_investasi.setLayoutManager(new LinearLayoutManager(getActivity()));
//                lv_tambah_rider_ua.setHasFixedSize(true);
            cl_jenis_data_investasi_pembatas_di.setVisibility(View.GONE);
            lv_jenis_dana_investasi.setNestedScrollingEnabled(false);
        } else {
            cl_jenis_data_investasi_pembatas_di.setVisibility(View.GONE);
            cl_jenis_data_investasi_di.setVisibility(View.GONE);
//                ll_tambah_rider_ua.setVisibility(View.GONE);
            lv_jenis_dana_investasi.setVisibility(View.GONE);
        }

        ListManfaat = me.getListManfaatDI();
        listManfaatAdapter = new ListManfaatAdapter(getActivity(), R.layout.e_adapter_manfaat, ListManfaat, this);
        lv_jumlah_manfaat_di.setAdapter(listManfaatAdapter);
        lv_jumlah_manfaat_di.setLayoutManager(new LinearLayoutManager(getActivity()));
        for (int i = 0; i < ListManfaat.size(); i++) {
            total_persen_manfaat = total_persen_manfaat + ListManfaat.get(i).getManfaat();
        }
        tv_jumlah_manfaat_di.setText("Total Manfaat " + new BigDecimal(total_persen_manfaat).toString() + "%");
        if (!me.getValidation()) {
            Validation();
        }
        Method_Validator.ValEditWatcherRek(et_nomer_rek_invest_di, iv_circle_nomer_rek_invest_di,
                tv_error_nomer_rek_invest_di, getContext(), min_pjg_rek);
        Method_Validator.ValEditWatcherRek(et_no_rek_autodebet_di, iv_circle_no_rek_autodebet_di,
                tv_error_no_rek_autodebet_di, getContext(), min_pjg_rek);

        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter.LengthFilter(pjg_rek);
        et_nomer_rek_invest_di.setFilters(filters);
        et_no_rek_autodebet_di.setFilters(filters);
        if (me.getModelID().getFlag_aktif() == 1) {
            MethodSupport.disable(false, cl_child_di);
        }
//        } catch (Exception e) {
//            System.err.println(e.getClass().getName() + ": " + e.getMessage());
//        }
    }
//    premitopup_berkala_di
//    premitopup_tunggal_di
//    invest_bayar_bayar_premi_di
//    invest_cabang_di
//    kota_id_invest
//    jenis_tabungan_invest
//    jenis_nasabah_invest
//    norek_id_invest
//    nama_di_invest
//    kurs_invest
//    berikuasa_di_invest
//    tglkuasa_di_invest
//    keterangan_di_invest
//    bank_di_autodebet
//    ac_jns_tabungan_autodebet_di (ada di validation harus di masukkan di restore ?)
//    kurs_autodebet
//    simascard_di_autodebet
//    jumlah1_di
//    persen1_di
//    jumlah2_di
//    persen_di
//    jumlah3_di
//    persen_di


    private boolean Validation() {
        boolean val = true;
        int color = ContextCompat.getColor(getContext(), R.color.red);
//        if (et_premi_pokok_di.isShown() && et_premi_pokok_di.getText().toString().trim().length() == 0) {
//            val = false;
//            tv_error_premi_pokok_di.setVisibility(View.VISIBLE);
//            tv_error_premi_pokok_di.setText(getString(R.string.error_field_required));
//            iv_circle_premi_pokok_di.setColorFilter(color);
//        }
//        if (et_premi_berkala_di.isShown() && et_premi_berkala_di.getText().toString().trim().length() == 0) {
//            val = false;
//            tv_error_premi_berkala_di.setVisibility(View.VISIBLE);
//            tv_error_premi_berkala_di.setText(getString(R.string.error_field_required));
//            iv_circle_premi_berkala_di.setColorFilter(color);
//        }
//        if (ac_jenis_topup_premi_berkala_di.isShown() && ac_jenis_topup_premi_berkala_di.getText().toString().trim().length() == 0) {
//            val = false;
//            tv_error_premi_berkala_di.setVisibility(View.VISIBLE);
//            tv_error_premi_berkala_di.setText(getString(R.string.error_field_required));
//            iv_circle_premi_berkala_di.setColorFilter(color);
//        }
//        if (et_premi_tunggal_di.isShown() && et_premi_tunggal_di.getText().toString().trim().length() == 0) {
//            val = false;
//            tv_error_premi_tunggal_di.setVisibility(View.VISIBLE);
//            tv_error_premi_tunggal_di.setText(getString(R.string.error_field_required));
//            iv_circle_premi_tunggal_di.setColorFilter(color);
//        }
//        if (ac_jenis_topup_premi_tunggal_di.isShown() && ac_jenis_topup_premi_tunggal_di.getText().toString().trim().length() == 0) {
//            val = false;
//            tv_error_premi_tunggal_di.setVisibility(View.VISIBLE);
//            tv_error_premi_tunggal_di.setText(getString(R.string.error_field_required));
//            iv_circle_premi_tunggal_di.setColorFilter(color);
//        }
//        if (et_premi_tambahan_di.isShown() && et_premi_tambahan_di.getText().toString().trim().length() == 0) {
//            val = false;
//            tv_error_premi_tambahan_di.setVisibility(View.VISIBLE);
//            tv_error_premi_tambahan_di.setText(getString(R.string.error_field_required));
//            iv_circle_premi_tambahan_di.setColorFilter(color);
//        }
//        if (et_jumlah_di.isShown() && et_jumlah_di.getText().toString().trim().length() == 0) {
//            val = false;
//            tv_error_jumlah_di.setVisibility(View.VISIBLE);
//            tv_error_jumlah_di.setText(getString(R.string.error_field_required));
//            iv_circle_jumlah_di.setColorFilter(color);
//        }
        if (ac_bntuk_pmbyranpremi_di.isShown() && ac_bntuk_pmbyranpremi_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_bntuk_pmbyranpremi_di, tv_error_bntuk_pmbyranpremi_di, iv_circle_bntuk_pmbyranpremi_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_bntuk_pmbyranpremi_di.setVisibility(View.VISIBLE);
//            tv_error_bntuk_pmbyranpremi_di.setText(getString(R.string.error_field_required));
//            iv_circle_bntuk_pmbyranpremi_di.setColorFilter(color);
        }
        if (ac_bank_invest_di.isShown() && ac_bank_invest_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_bank_invest_di, tv_error_bank_invest_di, iv_circle_bank_invest_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_bank_invest_di.setVisibility(View.VISIBLE);
//            tv_error_bank_invest_di.setText(getString(R.string.error_field_required));
//            iv_circle_bank_invest_di.setColorFilter(color);
        }
        if (ac_jenis_tabungan_invest_di.isShown() && ac_jenis_tabungan_invest_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_jenis_tabungan_invest_di, tv_error_jenis_tabungan_invest_di, iv_circle_jenis_tabungan_invest_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jenis_tabungan_invest_di.setVisibility(View.VISIBLE);
//            tv_error_jenis_tabungan_invest_di.setText(getString(R.string.error_field_required));
//            iv_circle_jenis_tabungan_invest_di.setColorFilter(color);
        }
        if (ac_jenis_nasabah_invest_di.isShown() && ac_jenis_nasabah_invest_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_jenis_nasabah_invest_di, tv_error_jenis_nasabah_invest_di, iv_circle_jenis_nasabah_invest_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jenis_nasabah_invest_di.setVisibility(View.VISIBLE);
//            tv_error_jenis_nasabah_invest_di.setText(getString(R.string.error_field_required));
//            iv_circle_jenis_nasabah_invest_di.setColorFilter(color);
        }
        if (et_nomer_rek_invest_di.isShown() && et_nomer_rek_invest_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_nomer_rek_invest_di, tv_error_nomer_rek_invest_di, iv_circle_nomer_rek_invest_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_nomer_rek_invest_di.setVisibility(View.VISIBLE);
//            tv_error_nomer_rek_invest_di.setText(getString(R.string.error_field_required));
//            iv_circle_nomer_rek_invest_di.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_nomer_rek_invest_di.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_nomer_rek_invest_di, tv_error_nomer_rek_invest_di, iv_circle_nomer_rek_invest_di, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_atas_nama_di.isShown() && et_atas_nama_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_atas_nama_di, tv_error_atas_nama_di, iv_circle_atas_nama_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_atas_nama_di.setVisibility(View.VISIBLE);
//            tv_error_atas_nama_di.setText(getString(R.string.error_field_required));
//            iv_circle_atas_nama_di.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_atas_nama_di.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_atas_nama_di, tv_error_atas_nama_di, iv_circle_atas_nama_di, color, getContext(), getString(R.string.error_regex));
            }
        }
        // ya memberi kuasa checkbox
        //
        //
        //
        if (et_ttlkuasa_di.isShown() && et_ttlkuasa_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_ttlkuasa_di, tv_error_ttlkuasa_di, iv_circle_ttlkuasa_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_ttlkuasa_di.setVisibility(View.VISIBLE);
//            tv_error_ttlkuasa_di.setText(getString(R.string.error_field_required));
//            iv_circle_ttlkuasa_di.setColorFilter(color);
        }
        if (ac_sp_bank_autodebet_di.isShown() && ac_sp_bank_autodebet_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_sp_bank_autodebet_di, tv_error_sp_bank_autodebet_di, iv_circle_sp_bank_autodebet_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_sp_bank_autodebet_di.setVisibility(View.VISIBLE);
//            tv_error_sp_bank_autodebet_di.setText(getString(R.string.error_field_required));
//            iv_circle_sp_bank_autodebet_di.setColorFilter(color);
        }
        if (ac_jns_tabungan_autodebet_di.isShown() && ac_jns_tabungan_autodebet_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_jns_tabungan_autodebet_di, tv_error_jns_tabungan_autodebet_di, iv_circle_jns_tabungan_autodebet_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jns_tabungan_autodebet_di.setVisibility(View.VISIBLE);
//            tv_error_jns_tabungan_autodebet_di.setText(getString(R.string.error_field_required));
//            iv_circle_jns_tabungan_autodebet_di.setColorFilter(color);
        }
        if (ac_kurs_autodebet_di.isShown() && ac_kurs_autodebet_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_kurs_autodebet_di, tv_error_kurs_autodebet_di, iv_circle_kurs_autodebet_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_kurs_autodebet_di.setVisibility(View.VISIBLE);
//            tv_error_kurs_autodebet_di.setText(getString(R.string.error_field_required));
//            iv_circle_kurs_autodebet_di.setColorFilter(color);
        }
        if (ac_kurs_invest_di.isShown() && ac_kurs_invest_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_kurs_invest_di, tv_error_kurs_invest_di, iv_circle_kurs_invest_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_kurs_invest_di.setVisibility(View.VISIBLE);
//            tv_error_kurs_invest_di.setText(getString(R.string.error_field_required));
//            iv_circle_kurs_invest_di.setColorFilter(color);
        }
        if (et_no_rek_autodebet_di.isShown() && et_no_rek_autodebet_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_no_rek_autodebet_di, tv_error_no_rek_autodebet_di, iv_circle_no_rek_autodebet_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_no_rek_autodebet_di.setVisibility(View.VISIBLE);
//            tv_error_no_rek_autodebet_di.setText(getString(R.string.error_field_required));
//            iv_circle_no_rek_autodebet_di.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_no_rek_autodebet_di.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_no_rek_autodebet_di, tv_error_no_rek_autodebet_di, iv_circle_no_rek_autodebet_di, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_atas_nama_autodebet_di.isShown() && et_atas_nama_autodebet_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_atas_nama_autodebet_di, tv_error_atas_nama_autodebet_di, iv_circle_atas_nama_autodebet_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_atas_nama_autodebet_di.setVisibility(View.VISIBLE);
//            tv_error_atas_nama_autodebet_di.setText(getString(R.string.error_field_required));
//            iv_circle_atas_nama_autodebet_di.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_atas_nama_autodebet_di.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_atas_nama_autodebet_di, tv_error_atas_nama_autodebet_di, iv_circle_atas_nama_autodebet_di, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (ac_dana_invest_alokasi_di.isShown() && ac_dana_invest_alokasi_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_dana_invest_alokasi_di, tv_error_dana_invest_alokasi_di, iv_circle_dana_invest_alokasi_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_dana_invest_alokasi_di.setVisibility(View.VISIBLE);
//            tv_error_dana_invest_alokasi_di.setText(getString(R.string.error_field_required));
//            iv_circle_dana_invest_alokasi_di.setColorFilter(color);
        }
        if (ac_jangka_waktu_di.isShown() && ac_jangka_waktu_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_jangka_waktu_di, tv_error_jangka_waktu_di, iv_circle_jangka_waktu_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jangka_waktu_di.setVisibility(View.VISIBLE);
//            tv_error_jangka_waktu_di.setText(getString(R.string.error_field_required));
//            iv_circle_jangka_waktu_di.setColorFilter(color);
        }
        if (et_tanggal_jatuh_tempo_di.isShown() && et_tanggal_jatuh_tempo_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_tanggal_jatuh_tempo_di, tv_error_jangka_waktu_di, iv_circle_jangka_waktu_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jangka_waktu_di.setVisibility(View.VISIBLE);
//            tv_error_jangka_waktu_di.setText(getString(R.string.error_field_required));
//            iv_circle_jangka_waktu_di.setColorFilter(color);
        }
        if (ac_jenis_bunga_di.isShown() && ac_jenis_bunga_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_jenis_bunga_di, tv_error_jenis_bunga_di, iv_circle_jenis_bunga_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jenis_bunga_di.setVisibility(View.VISIBLE);
//            tv_error_jenis_bunga_di.setText(getString(R.string.error_field_required));
//            iv_circle_jenis_bunga_di.setColorFilter(color);
        }
        if (et_bunga_di.isShown() && et_bunga_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_bunga_di, tv_error_jenis_bunga_di, iv_circle_jenis_bunga_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jenis_bunga_di.setVisibility(View.VISIBLE);
//            tv_error_jenis_bunga_di.setText(getString(R.string.error_field_required));
//            iv_circle_jenis_bunga_di.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_bunga_di.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_bunga_di, tv_error_jenis_bunga_di, iv_circle_jenis_bunga_di, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_jumlah_investasi_di.isShown() && et_jumlah_investasi_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_jumlah_investasi_di, tv_error_jumlah_investasi_di, iv_circle_jumlah_investasi_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jumlah_investasi_di.setVisibility(View.VISIBLE);
//            tv_error_jumlah_investasi_di.setText(getString(R.string.error_field_required));
//            iv_circle_jumlah_investasi_di.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_jumlah_investasi_di.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_jumlah_investasi_di, tv_error_jumlah_investasi_di, iv_circle_jumlah_investasi_di, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_jumlah_bunga_di.isShown() && et_jumlah_bunga_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_jumlah_bunga_di, tv_error_jumlah_investasi_di, iv_circle_jumlah_investasi_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jumlah_investasi_di.setVisibility(View.VISIBLE);
//            tv_error_jumlah_investasi_di.setText(getString(R.string.error_field_required));
//            iv_circle_jumlah_investasi_di.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_jumlah_bunga_di.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_jumlah_bunga_di, tv_error_jumlah_investasi_di, iv_circle_jumlah_investasi_di, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (ac_jenis_rollover_di.isShown() && ac_jenis_rollover_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_jenis_rollover_di, tv_error_jenis_rollover_di, iv_circle_jenis_rollover_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jenis_rollover_di.setVisibility(View.VISIBLE);
//            tv_error_jenis_rollover_di.setText(getString(R.string.error_field_required));
//            iv_circle_jenis_rollover_di.setColorFilter(color);
        }
        if (et_no_memo_di.isShown() && et_no_memo_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_no_memo_di, tv_error_jenis_rollover_di, iv_circle_jenis_rollover_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_jenis_rollover_di.setVisibility(View.VISIBLE);
//            tv_error_jenis_rollover_di.setText(getString(R.string.error_field_required));
//            iv_circle_jenis_rollover_di.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_no_memo_di.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_no_memo_di, tv_error_jenis_rollover_di, iv_circle_jenis_rollover_di, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (ac_status_karyawan_di.isShown() && ac_status_karyawan_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_status_karyawan_di, tv_error_status_karyawan_di, iv_circle_status_karyawan_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_status_karyawan_di.setVisibility(View.VISIBLE);
//            tv_error_status_karyawan_di.setText(getString(R.string.error_field_required));
//            iv_circle_status_karyawan_di.setColorFilter(color);
        }
        if (et_fee_based_income_di.isShown() && et_fee_based_income_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_fee_based_income_di, tv_error_fee_based_income_di, iv_circle_fee_based_income_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_fee_based_income_di.setVisibility(View.VISIBLE);
//            tv_error_fee_based_income_di.setText(getString(R.string.error_field_required));
//            iv_circle_fee_based_income_di.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_fee_based_income_di.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_fee_based_income_di, tv_error_fee_based_income_di, iv_circle_fee_based_income_di, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_tanggal_nab_di.isShown() && et_tanggal_nab_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_tanggal_nab_di, tv_error_tanggal_nab_di, iv_circle_tanggal_nab_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_tanggal_nab_di.setVisibility(View.VISIBLE);
//            tv_error_tanggal_nab_di.setText(getString(R.string.error_field_required));
//            iv_circle_tanggal_nab_di.setColorFilter(color);
        }
        if (et_jumlah_bunga_nab_di.isShown() && et_jumlah_bunga_nab_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_jumlah_bunga_nab_di, tv_error_tanggal_nab_di, iv_circle_tanggal_nab_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_tanggal_nab_di.setVisibility(View.VISIBLE);
//            tv_error_tanggal_nab_di.setText(getString(R.string.error_field_required));
//            iv_circle_tanggal_nab_di.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_jumlah_bunga_nab_di.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_jumlah_bunga_nab_di, tv_error_tanggal_nab_di, iv_circle_tanggal_nab_di, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_begdate_topup_di.isShown() && et_begdate_topup_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_begdate_topup_di, tv_error_begdate_topup_di, iv_circle_begdate_topup_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_begdate_topup_di.setVisibility(View.VISIBLE);
//            tv_error_begdate_topup_di.setText(getString(R.string.error_field_required));
//            iv_circle_begdate_topup_di.setColorFilter(color);
        }
        if (et_cabang_di.isShown() && et_cabang_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_cabang_di, tv_error_cabang_di, iv_circle_cabang_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_atas_nama_di.setVisibility(View.VISIBLE);
//            tv_error_atas_nama_di.setText(getString(R.string.error_field_required));
//            iv_circle_atas_nama_di.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_cabang_di.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_cabang_di, tv_error_cabang_di, iv_circle_cabang_di, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_kota_di.isShown() && et_kota_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_kota_di, tv_error_kota_di, iv_circle_kota_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_atas_nama_di.setVisibility(View.VISIBLE);
//            tv_error_atas_nama_di.setText(getString(R.string.error_field_required));
//            iv_circle_atas_nama_di.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_kota_di.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_kota_di, tv_error_kota_di, iv_circle_kota_di, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (et_presentase_rate_bp_di.isShown() && et_presentase_rate_bp_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_presentase_rate_bp_di, tv_error_presentase_rate_bp_di, iv_circle_presentase_rate_bp_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_presentase_rate_bp_di.setVisibility(View.VISIBLE);
//            tv_error_presentase_rate_bp_di.setText(getString(R.string.error_field_required));
//            iv_circle_presentase_rate_bp_di.setColorFilter(color);
        } else {
            if (!Method_Validator.ValEditRegex(et_presentase_rate_bp_di.getText().toString())) {
                val = false;
                MethodSupport.onFocusviewNested(scroll_di, cl_child_di, et_presentase_rate_bp_di, tv_error_presentase_rate_bp_di, iv_circle_presentase_rate_bp_di, color, getContext(), getString(R.string.error_regex));
            }
        }
        if (ac_penarikan_bunga_di.isShown() && ac_penarikan_bunga_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_penarikan_bunga_di, tv_error_penarikan_bunga_di, iv_circle_penarikan_bunga_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_penarikan_bunga_di.setVisibility(View.VISIBLE);
//            tv_error_penarikan_bunga_di.setText(getString(R.string.error_field_required));
//            iv_circle_penarikan_bunga_di.setColorFilter(color);
        }
        //checkbox
        // breakable
        // ya telemarketing
        if (ac_cara_bayar_rider_di.isShown() && ac_cara_bayar_rider_di.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusviewNested(scroll_di, cl_child_di, ac_cara_bayar_rider_di, tv_error_cara_bayar_rider_di, iv_circle_cara_bayar_rider_di, color, getContext(), getString(R.string.error_field_required));
//            tv_error_cara_bayar_rider_di.setVisibility(View.VISIBLE);
//            tv_error_cara_bayar_rider_di.setText(getString(R.string.error_field_required));
//            iv_circle_cara_bayar_rider_di.setColorFilter(color);
        }
        if (ListManfaat.isEmpty()) {
            val = false;
            tv_error_mnfaat_di.setText(getString(R.string.error_field_required));
            tv_error_mnfaat_di.setVisibility(View.VISIBLE);
        } else {
            for (int i = 0; i < ListManfaat.size(); i++) {
                if (!ListManfaat.get(i).getValidation()) {
                    val = false;
                    tv_error_mnfaat_di.setText(getString(R.string.error_field_required));
                    tv_error_mnfaat_di.setVisibility(View.VISIBLE);
                    scroll_di.requestChildFocus(cl_child_di, tv_error_mnfaat_di);
                    tv_error_mnfaat_di.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));
                }
                if (ListManfaat.get(i).getNama().length() == 0 ||
                        ListManfaat.get(i).getManfaat() == (double) 0 ||
                        ListManfaat.get(i).getJekel() == -1 ||
                        ListManfaat.get(i).getTtl().length() == 0 ||
                        ListManfaat.get(i).getHub_dgcalon_tt() == 0 ||
                        ListManfaat.get(i).getWarganegara() == 0) {
                    val = false;
                    tv_error_mnfaat_di.setText(getString(R.string.error_field_required));
                    tv_error_mnfaat_di.setVisibility(View.VISIBLE);
                    scroll_di.requestChildFocus(cl_child_di, tv_error_mnfaat_di);
                    tv_error_mnfaat_di.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));

                }
            }
            if (total_persen_manfaat != 100) {
                val = false;
                tv_error_mnfaat_di.setText(getString(R.string.error_field_total));
                tv_error_mnfaat_di.setVisibility(View.VISIBLE);
                scroll_di.requestChildFocus(cl_child_di, tv_error_mnfaat_di);
                tv_error_mnfaat_di.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));
            }
        }
        return val;
    }

    private Boolean val_page() {
        boolean val = true;
        int color = ContextCompat.getColor(getContext(), R.color.red);
        if (ListManfaat.isEmpty()) {
            val = false;
            tv_error_mnfaat_di.setText(getString(R.string.error_field_benefit));
            tv_error_mnfaat_di.setVisibility(View.VISIBLE);
            scroll_di.requestChildFocus(cl_child_di, tv_error_mnfaat_di);
            tv_error_mnfaat_di.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));
        } else {
            for (int i = 0; i < ListManfaat.size(); i++) {
                if (!ListManfaat.get(i).getValidation()) {
                    val = false;
                    tv_error_mnfaat_di.setText(getString(R.string.error_field_benefit));
                    tv_error_mnfaat_di.setVisibility(View.VISIBLE);
                    scroll_di.requestChildFocus(cl_child_di, tv_error_mnfaat_di);
                    tv_error_mnfaat_di.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));
                }
                if (ListManfaat.get(i).getNama().length() == 0 ||
                        ListManfaat.get(i).getManfaat() == (double) 0 ||
                        ListManfaat.get(i).getJekel() == -1 ||
                        ListManfaat.get(i).getTtl().length() == 0 ||
                        ListManfaat.get(i).getHub_dgcalon_tt() == 0 ||
                        ListManfaat.get(i).getWarganegara() == 0) {
                    val = false;
                    tv_error_mnfaat_di.setText(getString(R.string.error_field_benefit));
                    tv_error_mnfaat_di.setVisibility(View.VISIBLE);
                    scroll_di.requestChildFocus(cl_child_di, tv_error_mnfaat_di);
                    tv_error_mnfaat_di.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));

                }
            }
            if (total_persen_manfaat != 100) {
                val = false;
                tv_error_mnfaat_di.setText(getString(R.string.error_field_total));
                tv_error_mnfaat_di.setVisibility(View.VISIBLE);
                scroll_di.requestChildFocus(cl_child_di, tv_error_mnfaat_di);
                tv_error_mnfaat_di.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));
            }
        }
        return val;
    }

    private void setAdapterBankPusat(String cari, AutoCompleteTextView ac_cari) {
        ArrayList<ModelBank> LIST = new E_Select(getContext()).getBankPusat(cari, JENIS_LOGIN, JENIS_LOGIN_BC);
        ArrayAdapter<ModelBank> dataAdapter = new ArrayAdapter<ModelBank>(getActivity(), android.R.layout.simple_dropdown_item_1line, LIST);
        ac_cari.setAdapter(dataAdapter);
    }

    private void setAdapterBankCabang(String cari, AutoCompleteTextView ac_cari) {
        ArrayList<ModelBankCab> LIST = new E_Select(getContext()).getBankCab(cari, JENIS_LOGIN, JENIS_LOGIN_BC);
        ArrayAdapter<ModelBankCab> dataAdapter = new ArrayAdapter<ModelBankCab>(getActivity(), android.R.layout.simple_dropdown_item_1line, LIST);
        ac_cari.setAdapter(dataAdapter);
    }

    private void setAdapterBerkala() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.topup_berkala));
        ac_jenis_topup_premi_berkala_di.setAdapter(Adapter);
    }

    private void setAdapterTunggal() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.topup_tunggal));
        ac_jenis_topup_premi_tunggal_di.setAdapter(Adapter);
    }

    private void setAdapterJnsTabungan() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.jenis_tabungan));
        ac_jenis_tabungan_invest_di.setAdapter(Adapter);
        ac_jns_tabungan_autodebet_di.setAdapter(Adapter);
    }

    private void setAdapterJnsNasabah() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.jenis_nasabah));
        ac_jenis_nasabah_invest_di.setAdapter(Adapter);
    }

    private void setAdapterKurs() {
        ArrayList<ModelDropDownString> Dropdown = new E_Select(getContext()).getLst_Kurs(me.getUsulanAsuransiModel().getKd_produk_ua(), me.getUsulanAsuransiModel().getSub_produk_ua());
        ArrayAdapter<ModelDropDownString> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_kurs_autodebet_di.setAdapter(Adapter);
        ac_kurs_invest_di.setAdapter(Adapter);
    }

    private void setAdapterYaTidak() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.yatidak));
        ac_aktif_di.setAdapter(Adapter);
        ac_autodebet_premi_pertama_di.setAdapter(Adapter);
    }

    private void setAdapterAlokasiDana() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.dana_invest_alokasi));
        ac_dana_invest_alokasi_di.setAdapter(Adapter);
    }

    private void setAdapterRollover() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.jns_rollover));
        ac_jenis_rollover_di.setAdapter(Adapter);
    }

    private void setAdapterKaryawan() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.sts_krywan));
        ac_status_karyawan_di.setAdapter(Adapter);
    }

    private void setAdapterBayarRider() {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.cara_bayar_rider));
        ac_cara_bayar_rider_di.setAdapter(Adapter);
    }

    private void onClickLanjut() {

        boolean isBankBranchLayoutVisible = cl_no_rekening_pp_untuk_transaksi_autodebet_di.isShown();
        String bankBranchName = ac_sp_bank_autodebet_di.getText().toString().trim();
        //Only do this validation when bank branch layout is visible to the screen
        if (isBankBranchLayoutVisible && TextUtils.isEmpty(bankBranchName)) {
            ac_sp_bank_autodebet_di.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
            ac_sp_bank_autodebet_di.requestFocus();
            return;
        }

        if (!isBankBranchValid) {
            ac_sp_bank_autodebet_di.setError(getResources().getString(R.string.err_msg_bank_is_not_on_the_list));
            ac_sp_bank_autodebet_di.requestFocus();
            return;
        }

        boolean isBankLayoutVisible = cl_form_no_rekening_pp_untuk_transaksi_di.isShown();
        String centralBankName = ac_bank_invest_di.getText().toString().trim();
        //Only do this validation when bank branch layout is visible to the screen
        if (isBankLayoutVisible && TextUtils.isEmpty(centralBankName)) {
            ac_bank_invest_di.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
            ac_bank_invest_di.requestFocus();
            return;
        }

        if (!isBankValid) {
            ac_bank_invest_di.setError(getResources().getString(R.string.err_msg_bank_is_not_on_the_list));
            ac_bank_invest_di.requestFocus();
            return;
        }

        if (!val_page()) {
            MethodSupport.Alert(getContext(), getString(R.string.title_error_val), getString(R.string.msg_val_all), 0);
        } else {
            me.getDetilInvestasiModel().setValidation(Validation());
            loadview();
            MyTask task = new MyTask();
            task.execute();
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setTitle("Melakukan perpindahan halaman");
//            progressDialog.setMessage("Mohon Menunggu...");
//            progressDialog.show();
//            progressDialog.setCancelable(false);
//            Runnable progressRunnable = new Runnable() {
//                @Override
//                public void run() {
            //MethodSupport.active_view(0, btn_lanjut);
            //MethodSupport.active_view(0, iv_next);
            //ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
//            me.getDetilInvestasiModel().setValidation(Validation());
//            loadview();
//            ((E_MainActivity) getActivity()).onSave();
//                    View cl_child_di = getActivity().findViewById(R.id.cl_child_di);
//                    File file_bitmap = new File(
//                            getActivity().getFilesDir(), "ESPAJ/spaj_file/" + me.getModelID().getSPAJ_ID_TAB());
//                    if (!file_bitmap.exists()) {
//                        file_bitmap.mkdirs();
//                    }

            //createJPG
//                    String fileName = "DI_" + me.getModelID().getSPAJ_ID_TAB() + ".jpg";
//                    Context context = getActivity();
//                    MethodSupport.onCreateBitmapScroll(cl_child_di, scroll_di, file_bitmap, fileName, context);

//            ((E_MainActivity) getActivity()).setFragment(new E_DetilAgenFragment(), getResources().getString(R.string.detil_agen));
            //progressDialog.cancel();
//                }
//            };

//            Handler pdCanceller = new Handler();
//            pdCanceller.postDelayed(progressRunnable, 1000);
        }
    }

    private class MyTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_DETIL_INVESTASI);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ((E_MainActivity) getActivity()).setFragment(new E_DetilAgenFragment(), getResources().getString(R.string.detil_agen));
            Toast.makeText(getActivity(), "DATA BERHASIL TERSIMPAN", Toast.LENGTH_SHORT).show();
        }
    }

    private class MyTaskBack extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            ProgressDialogClass.showSimpleProgressDialog(getActivity(),getString(R.string.title_wait),getString(R.string.msg_wait),true) ;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ((E_MainActivity) getActivity()).onSave(Const.PAGE_DETIL_INVESTASI);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ((E_MainActivity) getActivity()).setFragment(new E_UsulanAsuransiFragment(), getResources().getString(R.string.usulan_asuransi));
        }
    }

    private void onClickBack() {

        loadview();
        MyTaskBack taskBack = new MyTaskBack();
        taskBack.execute();

//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setTitle("Melakukan perpindahan halaman");
//        progressDialog.setMessage("Mohon Menunggu...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
//        Runnable progressRunnable = new Runnable() {
//
//            @Override
//            public void run() {
//        MethodSupport.active_view(0, btn_kembali);
//        MethodSupport.active_view(0, iv_prev);
//        ProgressDialogClass.showSimpleProgressDialog(getActivity(), getString(R.string.title_wait), getString(R.string.msg_wait), true);
//
//        loadview();
//        ((E_MainActivity) getActivity()).onSave();
//        ((E_MainActivity) getActivity()).setFragment(new E_UsulanAsuransiFragment(), getResources().getString(R.string.usulan_asuransi));
//                progressDialog.cancel();
//            }
//        };
//
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 1000);
    }

    private ProgressDialog dialogmessage;

    public void startProgress() {
        dialogmessage = new ProgressDialog(getActivity());
        dialogmessage.setMessage("Mohon Menunggu ");
        dialogmessage.setTitle("Sedang Melakukan Perpindahan Halaman ...");
        dialogmessage.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialogmessage.show();
        dialogmessage.setCancelable(false);
        btn_lanjut.setEnabled(false);
        iv_next.setEnabled(false);
        iv_prev.setEnabled(false);
        btn_kembali.setEnabled(false);
        dialogmessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                btn_lanjut.setEnabled(true);
                iv_next.setEnabled(true);
                iv_prev.setEnabled(true);
                btn_kembali.setEnabled(true);
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (dialogmessage.getProgress() <= dialogmessage
                            .getMax()) {
                        Thread.sleep(10);
                        handle.sendMessage(handle.obtainMessage());
                        if (dialogmessage.getProgress() == dialogmessage
                                .getMax()) {
                            dialogmessage.dismiss();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    Handler handle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            dialogmessage.incrementProgressBy(1);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        total_persen_manfaat = (double) 0;
        switch (requestCode) {
            case RequestCode.FILL_MANFAAT_SPAJ:
                // Make sure the request was successful
                if (resultCode == RESULT_OK) {
                    Bundle mBundle = data.getExtras();
                    ModelDataDitunjukDI modelDataDitunjukDI = mBundle.getParcelable(getString(R.string.modelmanfaat));
                    int mPos = mBundle.getInt(getString(R.string.pos));

                    if (ListManfaat.size() == mPos) {
                        ListManfaat.add(modelDataDitunjukDI);
                    } else {
                        ListManfaat.set(mPos, modelDataDitunjukDI);
                    }
                    for (int i = 0; i < ListManfaat.size(); i++) {
                        total_persen_manfaat = total_persen_manfaat + ListManfaat.get(i).getManfaat();
                    }
                    tv_jumlah_manfaat_di.setText("Total Manfaat " + new BigDecimal(total_persen_manfaat).toString() + "%");
                    listManfaatAdapter.updateListTP(ListManfaat);
                }
                break;
            default:
                break;
        }
    }

    /**
     * To determine is user selected bank name is from dropdown or not
     * If user type the bank branch name, the input will not be valid
     * User will have to select from the dropdown
     *
     * @param searchQuery
     * @param actvBankName
     * @return
     */
    private void validateCentralBankSelection(String searchQuery, AutoCompleteTextView
            actvBankName) {
        //Check if job desc search query empty
        if (!TextUtils.isEmpty(searchQuery)) {
            actvBankName.setError(null);
            iv_circle_bank_invest_di.setColorFilter(ContextCompat.getColor(getActivity(), R.color.orange));

            ArrayList<ModelBank> result = new E_Select(getContext()).getBankPusat(searchQuery, JENIS_LOGIN, JENIS_LOGIN_BC);


            //Search from DB
            if (result.isEmpty()) {
                actvBankName.setError(getString(R.string.err_msg_bank_is_not_on_the_list));
            } else {
                actvBankName.setError(null);

                ArrayAdapter<ModelBank> dataAdapter = new ArrayAdapter<ModelBank>(getActivity(), android.R.layout.simple_dropdown_item_1line, result);
                actvBankName.setAdapter(dataAdapter);
            }
            // To make sure user only pick bank branch from dropdown so we make it false
            isBankValid = false;

        } else {
            actvBankName.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
            iv_circle_bank_invest_di.setColorFilter(ContextCompat.getColor(getActivity(), R.color.orange));

            // To make sure user only pick bank branch from dropdown so we make it false
            isBankValid = false;
        }
    }

    /**
     * Check whether user previously selected central bank name is on the suggestion list
     * Invoked when user go to this process from the previous step
     *
     * @param selectedBankName
     * @param actvBankName
     * @return
     */
    private void checkForSelectedCentralBankNameIsOnTheList(String
                                                                    selectedBankName, AutoCompleteTextView actvBankName) {
        //Search from DB
        ArrayList<ModelBank> result = new E_Select(getContext()).getBankPusat(selectedBankName, JENIS_LOGIN, JENIS_LOGIN_BC);
        if (result.isEmpty()) {
            actvBankName.setError(getString(R.string.err_msg_bank_is_not_on_the_list));
            isBankValid = false;
        } else {
            actvBankName.setError(null);
            iv_circle_bank_invest_di.setColorFilter(ContextCompat.getColor(getActivity(), R.color.green));
            ArrayAdapter<ModelBank> dataAdapter = new ArrayAdapter<ModelBank>(getActivity(), android.R.layout.simple_dropdown_item_1line, result);
            actvBankName.setAdapter(dataAdapter);
            isBankValid = true;
        }
    }

    /**
     * To determine is user selected bank branch name is from dropdown or not
     * If user type the bank branch name, the input will not be valid
     * User will have to select from the dropdown
     *
     * @param searchQuery
     * @param actvBankName
     * @return
     */
    private void validateBankSelection(String searchQuery, AutoCompleteTextView actvBankName) {
        //Check if job desc search query empty
        if (!TextUtils.isEmpty(searchQuery)) {
            actvBankName.setError(null);
            iv_circle_sp_bank_autodebet_di.setColorFilter(ContextCompat.getColor(getActivity(), R.color.orange));

            ArrayList<ModelBankCab> result = new E_Select(getContext()).getBankCab(searchQuery, JENIS_LOGIN, JENIS_LOGIN_BC);

            //Search from DB
            if (result.isEmpty()) {
                actvBankName.setError(getString(R.string.err_msg_bank_is_not_on_the_list));
            } else {
                actvBankName.setError(null);
                ArrayAdapter<ModelBankCab> Adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, result);
                actvBankName.setAdapter(Adapter);

            }
            // To make sure user only pick bank branch from dropdown so we make it false
            isBankBranchValid = false;

        } else {
            actvBankName.setError(getResources().getString(R.string.err_msg_field_cannot_be_empty));
            iv_circle_sp_bank_autodebet_di.setColorFilter(ContextCompat.getColor(getActivity(), R.color.orange));

            // To make sure user only pick bank branch from dropdown so we make it false
            isBankBranchValid = false;
        }
    }

    /**
     * Check whether user previously selected bank branch name is on the suggestion list
     * Invoked when user go to this process from the previous step
     *
     * @param selectedBankBranchName
     * @param actvBankName
     * @return
     */
    private void checkForSelectedBankBranchNameIsOnTheList(String
                                                                   selectedBankBranchName, AutoCompleteTextView actvBankName) {
        //Search from DB
        ArrayList<ModelBankCab> result = new E_Select(getActivity()).getBankCab(selectedBankBranchName, JENIS_LOGIN, JENIS_LOGIN_BC);
        if (result.isEmpty()) {
            actvBankName.setError(getString(R.string.err_msg_bank_is_not_on_the_list));
            isBankBranchValid = false;
        } else {
            actvBankName.setError(null);
            iv_circle_sp_bank_autodebet_di.setColorFilter(ContextCompat.getColor(getActivity(), R.color.green));
            ArrayAdapter<ModelBankCab> dataAdapter = new ArrayAdapter<ModelBankCab>(getActivity(), android.R.layout.simple_dropdown_item_1line, result);
            actvBankName.setAdapter(dataAdapter);
            isBankBranchValid = true;
        }
    }

   /* private boolean isAlreadyHaveTransactionNumber() {
        String spajIdTab = me.getModelID().getSPAJ_ID_TAB();
        E_Select select = new E_Select(getActivity());

        String transactionNumber = select.getPaymentTransactionNumber(spajIdTab);

        if (!TextUtils.isEmpty(transactionNumber)) {
            return true;
        }

        return false;
    }
    private boolean isCreditCardPaymentType() {
        String spajIdTab = me.getModelID().getSPAJ_ID_TAB();
        E_Select select = new E_Select(getActivity());

        int paymentType = select.getPaymentType(spajIdTab);
        if (paymentType == Const.CREDIT_CARD_PAYMENT_CODE) {
            return true;
        }

        return false;
    }*/

    @BindView(R.id.tv_perhitungan_premi_di)
    TextView tv_perhitungan_premi_di;
    @BindView(R.id.tv_error_premi_pokok_di)
    TextView tv_error_premi_pokok_di;
    @BindView(R.id.tv_error_premi_berkala_di)
    TextView tv_error_premi_berkala_di;
    @BindView(R.id.tv_error_jenis_topup_premi_berkala_di)
    TextView tv_error_jenis_topup_premi_berkala_di;
    @BindView(R.id.tv_error_premi_tunggal_di)
    TextView tv_error_premi_tunggal_di;
    @BindView(R.id.tv_error_jenis_topup_premi_tunggal_di)
    TextView tv_error_jenis_topup_premi_tunggal_di;
    @BindView(R.id.tv_error_jumlah_di)
    TextView tv_error_jumlah_di;
    @BindView(R.id.tv_no_rekening_pp_untuk_transaksi_di)
    TextView tv_no_rekening_pp_untuk_transaksi_di;
    @BindView(R.id.tv_error_bntuk_pmbyranpremi_di)
    TextView tv_error_bntuk_pmbyranpremi_di;
    @BindView(R.id.tv_error_cari_bank_1_di)
    TextView tv_error_cari_bank_1_di;
    @BindView(R.id.tv_error_bank_invest_di)
    TextView tv_error_bank_invest_di;
    @BindView(R.id.tv_error_cabang_di)
    TextView tv_error_cabang_di;
    @BindView(R.id.tv_error_kota_di)
    TextView tv_error_kota_di;
    @BindView(R.id.tv_error_jenis_tabungan_invest_di)
    TextView tv_error_jenis_tabungan_invest_di;
    @BindView(R.id.tv_error_jenis_nasabah_invest_di)
    TextView tv_error_jenis_nasabah_invest_di;
    @BindView(R.id.tv_error_nomer_rek_invest_di)
    TextView tv_error_nomer_rek_invest_di;
    @BindView(R.id.tv_error_atas_nama_di)
    TextView tv_error_atas_nama_di;
    @BindView(R.id.tv_memberi_kuasa_di)
    TextView tv_memberi_kuasa_di;
    @BindView(R.id.tv_error_ttlkuasa_di)
    TextView tv_error_ttlkuasa_di;
    @BindView(R.id.tv_error_keterangan_di)
    TextView tv_error_keterangan_di;
    @BindView(R.id.tv_no_rekening_pp_untuk_transaksi_autodebet_di)
    TextView tv_no_rekening_pp_untuk_transaksi_autodebet_di;
    @BindView(R.id.tv_error_cari_bank_2_di)
    TextView tv_error_cari_bank_2_di;
    @BindView(R.id.tv_error_sp_bank_autodebet_di)
    TextView tv_error_sp_bank_autodebet_di;
    @BindView(R.id.tv_error_cabang2_di)
    TextView tv_error_cabang2_di;
    @BindView(R.id.tv_error_kota2_di)
    TextView tv_error_kota2_di;
    @BindView(R.id.tv_error_jns_tabungan_autodebet_di)
    TextView tv_error_jns_tabungan_autodebet_di;
    @BindView(R.id.tv_error_mnfaat_di)
    TextView tv_error_mnfaat_di;

    @BindView(R.id.tv_error_kurs_autodebet_di)
    TextView tv_error_kurs_autodebet_di;
    @BindView(R.id.tv_error_kurs_invest_di)
    TextView tv_error_kurs_invest_di;
    @BindView(R.id.tv_error_no_rek_autodebet_di)
    TextView tv_error_no_rek_autodebet_di;
    @BindView(R.id.tv_error_atas_nama_autodebet_di)
    TextView tv_error_atas_nama_autodebet_di;
    @BindView(R.id.tv_error_tgl_debet_di)
    TextView tv_error_tgl_debet_di;
    @BindView(R.id.tv_error_tgl_valid_di)
    TextView tv_error_tgl_valid_di;
    @BindView(R.id.tv_error_aktif_di)
    TextView tv_error_aktif_di;
    @BindView(R.id.tv_error_autodebet_premi_pertama_di)
    TextView tv_error_autodebet_premi_pertama_di;
    @BindView(R.id.tv_dana_investasi_yang_dialokasikan_di)
    TextView tv_dana_investasi_yang_dialokasikan_di;
    @BindView(R.id.tv_error_dana_invest_alokasi_di)
    TextView tv_error_dana_invest_alokasi_di;
    @BindView(R.id.tv_jenis_data_investasi_di)
    TextView tv_jenis_data_investasi_di;
    @BindView(R.id.tv_jenis_data_investasi_ket_di)
    TextView tv_jenis_data_investasi_ket_di;
    @BindView(R.id.tv_jenis_data_investasi_persen_di)
    TextView tv_jenis_data_investasi_persen_di;
    @BindView(R.id.tv_data_yang_ditunjuk_menerima_manfaat_asuransi_di)
    TextView tv_data_yang_ditunjuk_menerima_manfaat_asuransi_di;
    @BindView(R.id.tv_tambah_penerima_manfaat_di)
    TextView tv_tambah_penerima_manfaat_di;
    @BindView(R.id.tv_jumlah_manfaat_di)
    TextView tv_jumlah_manfaat_di;
    @BindView(R.id.tv_error_premi_tambahan_di)
    TextView tv_error_premi_tambahan_di;
    @BindView(R.id.tv_detil_investasi_khusus_di)
    TextView tv_detil_investasi_khusus_di;
    @BindView(R.id.tv_error_jangka_waktu_di)
    TextView tv_error_jangka_waktu_di;
    @BindView(R.id.tv_error_tanggal_jatuh_tempo_di)
    TextView tv_error_tanggal_jatuh_tempo_di;
    @BindView(R.id.tv_error_jenis_bunga_di)
    TextView tv_error_jenis_bunga_di;
    @BindView(R.id.tv_error_bunga_di)
    TextView tv_error_bunga_di;
    @BindView(R.id.tv_error_jumlah_investasi_di)
    TextView tv_error_jumlah_investasi_di;
    @BindView(R.id.tv_error_jumlah_bunga_di)
    TextView tv_error_jumlah_bunga_di;
    @BindView(R.id.tv_error_jenis_rollover_di)
    TextView tv_error_jenis_rollover_di;
    @BindView(R.id.tv_error_no_memo_di)
    TextView tv_error_no_memo_di;
    @BindView(R.id.tv_error_status_karyawan_di)
    TextView tv_error_status_karyawan_di;
    @BindView(R.id.tv_error_fee_based_income_di)
    TextView tv_error_fee_based_income_di;
    @BindView(R.id.tv_error_tanggal_nab_di)
    TextView tv_error_tanggal_nab_di;
    @BindView(R.id.tv_error_jumlah_bunga_nab_di)
    TextView tv_error_jumlah_bunga_nab_di;
    @BindView(R.id.tv_error_begdate_topup_di)
    TextView tv_error_begdate_topup_di;
    @BindView(R.id.tv_error_presentase_rate_bp_di)
    TextView tv_error_presentase_rate_bp_di;
    @BindView(R.id.tv_error_penarikan_bunga_di)
    TextView tv_error_penarikan_bunga_di;
    @BindView(R.id.tv_error_breakable_di)
    TextView tv_error_breakable_di;
    @BindView(R.id.tv_error_telemarketing_di)
    TextView tv_error_telemarketing_di;
    @BindView(R.id.tv_error_cara_bayar_rider_di)
    TextView tv_error_cara_bayar_rider_di;


    @BindView(R.id.iv_circle_premi_pokok_di)
    ImageView iv_circle_premi_pokok_di;
    @BindView(R.id.iv_circle_premi_berkala_di)
    ImageView iv_circle_premi_berkala_di;
    @BindView(R.id.iv_circle_premi_tunggal_di)
    ImageView iv_circle_premi_tunggal_di;
    @BindView(R.id.iv_circle_bntuk_pmbyranpremi_di)
    ImageView iv_circle_bntuk_pmbyranpremi_di;
    @BindView(R.id.iv_circle_bank_invest_di)
    ImageView iv_circle_bank_invest_di;
    @BindView(R.id.iv_circle_jenis_tabungan_invest_di)
    ImageView iv_circle_jenis_tabungan_invest_di;
    @BindView(R.id.iv_circle_jenis_nasabah_invest_di)
    ImageView iv_circle_jenis_nasabah_invest_di;
    @BindView(R.id.iv_circle_nomer_rek_invest_di)
    ImageView iv_circle_nomer_rek_invest_di;
    @BindView(R.id.iv_circle_atas_nama_di)
    ImageView iv_circle_atas_nama_di;
    @BindView(R.id.iv_circle_memberi_kuasa_di)
    ImageView iv_circle_memberi_kuasa_di;
    @BindView(R.id.iv_circle_ttlkuasa_di)
    ImageView iv_circle_ttlkuasa_di;
    @BindView(R.id.iv_circle_sp_bank_autodebet_di)
    ImageView iv_circle_sp_bank_autodebet_di;
    @BindView(R.id.iv_circle_jns_tabungan_autodebet_di)
    ImageView iv_circle_jns_tabungan_autodebet_di;
    @BindView(R.id.iv_circle_kurs_autodebet_di)
    ImageView iv_circle_kurs_autodebet_di;
    @BindView(R.id.iv_circle_kurs_invest_di)
    ImageView iv_circle_kurs_invest_di;
    @BindView(R.id.iv_circle_no_rek_autodebet_di)
    ImageView iv_circle_no_rek_autodebet_di;
    @BindView(R.id.iv_circle_atas_nama_autodebet_di)
    ImageView iv_circle_atas_nama_autodebet_di;
    @BindView(R.id.iv_circle_dana_invest_alokasi_di)
    ImageView iv_circle_dana_invest_alokasi_di;
    @BindView(R.id.iv_circle_premi_tambahan_di)
    ImageView iv_circle_premi_tambahan_di;
    @BindView(R.id.iv_circle_jangka_waktu_di)
    ImageView iv_circle_jangka_waktu_di;
    @BindView(R.id.iv_circle_tanggal_jatuh_tempo_di)
    ImageView iv_circle_tanggal_jatuh_tempo_di;
    @BindView(R.id.iv_circle_jenis_bunga_di)
    ImageView iv_circle_jenis_bunga_di;
    @BindView(R.id.iv_circle_bunga_di)
    ImageView iv_circle_bunga_di;
    @BindView(R.id.iv_circle_jumlah_investasi_di)
    ImageView iv_circle_jumlah_investasi_di;
    @BindView(R.id.iv_circle_jumlah_bunga_di)
    ImageView iv_circle_jumlah_bunga_di;
    @BindView(R.id.iv_circle_jenis_rollover_di)
    ImageView iv_circle_jenis_rollover_di;
    @BindView(R.id.iv_circle_no_memo_di)
    ImageView iv_circle_no_memo_di;
    @BindView(R.id.iv_circle_status_karyawan_di)
    ImageView iv_circle_status_karyawan_di;
    @BindView(R.id.iv_circle_fee_based_income_di)
    ImageView iv_circle_fee_based_income_di;
    @BindView(R.id.iv_circle_tanggal_nab_di)
    ImageView iv_circle_tanggal_nab_di;
    @BindView(R.id.iv_circle_jumlah_bunga_nab_di)
    ImageView iv_circle_jumlah_bunga_nab_di;
    @BindView(R.id.iv_circle_begdate_topup_di)
    ImageView iv_circle_begdate_topup_di;
    @BindView(R.id.iv_circle_presentase_rate_bp_di)
    ImageView iv_circle_presentase_rate_bp_di;
    @BindView(R.id.iv_circle_penarikan_bunga_di)
    ImageView iv_circle_penarikan_bunga_di;
    @BindView(R.id.iv_circle_breakable_di)
    ImageView iv_circle_breakable_di;
    @BindView(R.id.iv_circle_telemarketing_di)
    ImageView iv_circle_telemarketing_di;
    @BindView(R.id.iv_circle_cara_bayar_rider_di)
    ImageView iv_circle_cara_bayar_rider_di;
    @BindView(R.id.iv_circle_jumlah_di)
    ImageView iv_circle_jumlah_di;
    @BindView(R.id.iv_circle_keterangan_di)
    ImageView iv_circle_keterangan_di;
    @BindView(R.id.iv_circle_cabang_di)
    ImageView iv_circle_cabang_di;
    @BindView(R.id.iv_circle_kota_di)
    ImageView iv_circle_kota_di;


    @BindView(R.id.cl_perhitungan_premi_di)
    ConstraintLayout cl_perhitungan_premi_di;
    @BindView(R.id.cl_form_perhitungan_premi_di)
    ConstraintLayout cl_form_perhitungan_premi_di;
    @BindView(R.id.cl_premi_pokok_di)
    ConstraintLayout cl_premi_pokok_di;// sudah diganti
    @BindView(R.id.cl_premi_berkala_di)
    ConstraintLayout cl_premi_berkala_di;
    @BindView(R.id.cl_jenis_topup_premi_berkala_di)
    ConstraintLayout cl_jenis_topup_premi_berkala_di;
    @BindView(R.id.cl_premi_tunggal_di)
    ConstraintLayout cl_premi_tunggal_di;
    @BindView(R.id.cl_jenis_topup_premi_tunggal_di)
    ConstraintLayout cl_jenis_topup_premi_tunggal_di;
    @BindView(R.id.cl_jumlah_di)
    ConstraintLayout cl_jumlah_di;
    @BindView(R.id.cl_no_rekening_pp_untuk_transaksi_di)
    ConstraintLayout cl_no_rekening_pp_untuk_transaksi_di;
    @BindView(R.id.cl_form_no_rekening_pp_untuk_transaksi_di)
    ConstraintLayout cl_form_no_rekening_pp_untuk_transaksi_di;
    @BindView(R.id.cl_bntuk_pmbyranpremi_di)
    ConstraintLayout cl_bntuk_pmbyranpremi_di;
    @BindView(R.id.cl_bank_invest_di)
    ConstraintLayout cl_bank_invest_di;
    @BindView(R.id.cl_cabang_di)
    ConstraintLayout cl_cabang_di;
    @BindView(R.id.cl_kota_di)
    ConstraintLayout cl_kota_di;
    @BindView(R.id.cl_jenis_tabungan_invest_di)
    ConstraintLayout cl_jenis_tabungan_invest_di;
    @BindView(R.id.cl_jenis_nasabah_invest_di)
    ConstraintLayout cl_jenis_nasabah_invest_di;
    @BindView(R.id.cl_nomer_rek_invest_di)
    ConstraintLayout cl_nomer_rek_invest_di;
    @BindView(R.id.cl_atas_nama_di)
    ConstraintLayout cl_atas_nama_di;
    @BindView(R.id.cl_memberi_kuasa_di)
    ConstraintLayout cl_memberi_kuasa_di;
    @BindView(R.id.cl_ttlkuasa_di)
    ConstraintLayout cl_ttlkuasa_di;
    @BindView(R.id.cl_keterangan_di)
    ConstraintLayout cl_keterangan_di;
    @BindView(R.id.cl_no_rekening_pp_untuk_transaksi_autodebet_di)
    ConstraintLayout cl_no_rekening_pp_untuk_transaksi_autodebet_di;
    @BindView(R.id.cl_form_no_rekening_pp_untuk_transaksi_autodebet_di)
    ConstraintLayout cl_form_no_rekening_pp_untuk_transaksi_autodebet_di;
    @BindView(R.id.cl_gunakan_no_rekening_pemegang_polis_untuk_transaksi_di)
    ConstraintLayout cl_gunakan_no_rekening_pemegang_polis_untuk_transaksi_di;
    @BindView(R.id.cl_sp_bank_autodebet_di)
    ConstraintLayout cl_sp_bank_autodebet_di;
    @BindView(R.id.cl_cabang2_di)
    ConstraintLayout cl_cabang2_di;
    @BindView(R.id.cl_kota2_di)
    ConstraintLayout cl_kota2_di;
    @BindView(R.id.cl_jns_tabungan_autodebet_di)
    ConstraintLayout cl_jns_tabungan_autodebet_di;
    @BindView(R.id.cl_kurs_autodebet_di)
    ConstraintLayout cl_kurs_autodebet_di;
    @BindView(R.id.cl_no_rek_autodebet_di)
    ConstraintLayout cl_no_rek_autodebet_di;
    @BindView(R.id.cl_atas_nama_autodebet_di)
    ConstraintLayout cl_atas_nama_autodebet_di;
    @BindView(R.id.cl_tgl_debet_di)
    ConstraintLayout cl_tgl_debet_di;
    @BindView(R.id.cl_tgl_valid_di)
    ConstraintLayout cl_tgl_valid_di;

    @BindView(R.id.cl_aktif_di)
    ConstraintLayout cl_aktif_di;
    @BindView(R.id.cl_autodebet_premi_pertama_di)
    ConstraintLayout cl_autodebet_premi_pertama_di;
    @BindView(R.id.cl_dana_investasi_yang_dialokasikan_di)
    ConstraintLayout cl_dana_investasi_yang_dialokasikan_di;
    @BindView(R.id.cl_form_dana_investasi_yang_dialokasikan_di)
    ConstraintLayout cl_form_dana_investasi_yang_dialokasikan_di;
    @BindView(R.id.cl_dana_invest_alokasi_di)
    ConstraintLayout cl_dana_invest_alokasi_di;
    @BindView(R.id.cl_jenis_data_investasi_di)
    ConstraintLayout cl_jenis_data_investasi_di;
    @BindView(R.id.cl_jenis_data_investasi_pembatas_di)
    ConstraintLayout cl_jenis_data_investasi_pembatas_di;
    @BindView(R.id.cl_jenis_data_investasi_persen_di)
    ConstraintLayout cl_jenis_data_investasi_persen_di;
    @BindView(R.id.cl_data_yang_ditunjuk_menerima_manfaat_asuransi_di)
    ConstraintLayout cl_data_yang_ditunjuk_menerima_manfaat_asuransi_di;
    @BindView(R.id.cl_data_yang_ditunjuk_menerima_manfaat_asuransi_pembatas_di)
    ConstraintLayout cl_data_yang_ditunjuk_menerima_manfaat_asuransi_pembatas_di;
    @BindView(R.id.cl_jumlah_manfaat_di)
    RelativeLayout cl_jumlah_manfaat_di;
    @BindView(R.id.cl_premi_tambahan_di)
    ConstraintLayout cl_premi_tambahan_di;
    @BindView(R.id.cl_detil_investasi_khusus_di)
    ConstraintLayout cl_detil_investasi_khusus_di;
    @BindView(R.id.cl_form_jangka_waktu_di)
    ConstraintLayout cl_form_jangka_waktu_di;
    @BindView(R.id.cl_jangka_waktu_di)
    ConstraintLayout cl_jangka_waktu_di;
    @BindView(R.id.cl_tanggal_jatuh_tempo_di)
    ConstraintLayout cl_tanggal_jatuh_tempo_di;
    @BindView(R.id.cl_jenis_bunga_di)
    ConstraintLayout cl_jenis_bunga_di;
    @BindView(R.id.cl_bunga_di)
    ConstraintLayout cl_bunga_di;
    @BindView(R.id.cl_jumlah_investasi_di)
    ConstraintLayout cl_jumlah_investasi_di;
    @BindView(R.id.cl_jumlah_bunga_di)
    ConstraintLayout cl_jumlah_bunga_di;
    @BindView(R.id.cl_jenis_rollover_di)
    ConstraintLayout cl_jenis_rollover_di;
    @BindView(R.id.cl_no_memo_di)
    ConstraintLayout cl_no_memo_di;
    @BindView(R.id.cl_status_karyawan_di)
    ConstraintLayout cl_status_karyawan_di;
    @BindView(R.id.cl_fee_based_income_di)
    ConstraintLayout cl_fee_based_income_di;
    @BindView(R.id.cl_tanggal_nab_di)
    ConstraintLayout cl_tanggal_nab_di;
    @BindView(R.id.cl_jumlah_bunga_nab_di)
    ConstraintLayout cl_jumlah_bunga_nab_di;
    @BindView(R.id.cl_begdate_topup_di)
    ConstraintLayout cl_begdate_topup_di;
    @BindView(R.id.cl_presentase_rate_bp_di)
    ConstraintLayout cl_presentase_rate_bp_di;
    @BindView(R.id.cl_penarikan_bunga_di)
    ConstraintLayout cl_penarikan_bunga_di;
    @BindView(R.id.cl_breakable_di)
    ConstraintLayout cl_breakable_di;
    @BindView(R.id.cl_telemarketing_di)
    ConstraintLayout cl_telemarketing_di;
    @BindView(R.id.cl_cara_bayar_rider_di)
    ConstraintLayout cl_cara_bayar_rider_di;
    @BindView(R.id.cl_child_di)
    ConstraintLayout cl_child_di;

    @BindView(R.id.et_premi_pokok_di)
    EditText et_premi_pokok_di;
    @BindView(R.id.et_premi_berkala_di)
    EditText et_premi_berkala_di;
    @BindView(R.id.et_premi_tunggal_di)
    EditText et_premi_tunggal_di;
    @BindView(R.id.et_jumlah_di)
    EditText et_jumlah_di;
    @BindView(R.id.et_cabang_di)
    EditText et_cabang_di;
    @BindView(R.id.et_kota_di)
    EditText et_kota_di;
    @BindView(R.id.et_nomer_rek_invest_di)
    EditText et_nomer_rek_invest_di;
    @BindView(R.id.et_atas_nama_di)
    EditText et_atas_nama_di;
    @BindView(R.id.et_ttlkuasa_di)
    EditText et_ttlkuasa_di;
    @BindView(R.id.et_keterangan_di)
    EditText et_keterangan_di;
    @BindView(R.id.et_cabang2_di)
    EditText et_cabang2_di;
    @BindView(R.id.et_kota2_di)
    EditText et_kota2_di;
    @BindView(R.id.et_no_rek_autodebet_di)
    EditText et_no_rek_autodebet_di;
    @BindView(R.id.et_atas_nama_autodebet_di)
    EditText et_atas_nama_autodebet_di;
    @BindView(R.id.et_tgl_debet_di)
    EditText et_tgl_debet_di;
    @BindView(R.id.et_tgl_valid_di)
    EditText et_tgl_valid_di;
    @BindView(R.id.et_premi_tambahan_di)
    EditText et_premi_tambahan_di;
    @BindView(R.id.et_tanggal_jatuh_tempo_di)
    EditText et_tanggal_jatuh_tempo_di;
    @BindView(R.id.et_bunga_di)
    EditText et_bunga_di;
    @BindView(R.id.et_jumlah_investasi_di)
    EditText et_jumlah_investasi_di;
    @BindView(R.id.et_jumlah_bunga_di)
    EditText et_jumlah_bunga_di;
    @BindView(R.id.et_no_memo_di)
    EditText et_no_memo_di;
    @BindView(R.id.et_fee_based_income_di)
    EditText et_fee_based_income_di;
    @BindView(R.id.et_tanggal_nab_di)
    EditText et_tanggal_nab_di;
    @BindView(R.id.et_jumlah_bunga_nab_di)
    EditText et_jumlah_bunga_nab_di;
    @BindView(R.id.et_begdate_topup_di)
    EditText et_begdate_topup_di;
    @BindView(R.id.et_presentase_rate_bp_di)
    EditText et_presentase_rate_bp_di;

    @BindView(R.id.ac_jenis_topup_premi_berkala_di)
    AutoCompleteTextView ac_jenis_topup_premi_berkala_di;
    @BindView(R.id.ac_jenis_topup_premi_tunggal_di)
    AutoCompleteTextView ac_jenis_topup_premi_tunggal_di;
    @BindView(R.id.ac_bntuk_pmbyranpremi_di)
    AutoCompleteTextView ac_bntuk_pmbyranpremi_di;
    @BindView(R.id.ac_bank_invest_di)
    AutoCompleteTextView ac_bank_invest_di;
    @BindView(R.id.ac_jenis_tabungan_invest_di)
    AutoCompleteTextView ac_jenis_tabungan_invest_di;
    @BindView(R.id.ac_jenis_nasabah_invest_di)
    AutoCompleteTextView ac_jenis_nasabah_invest_di;
    @BindView(R.id.ac_sp_bank_autodebet_di)
    AutoCompleteTextView ac_sp_bank_autodebet_di;
    @BindView(R.id.ac_kurs_invest_di)
    AutoCompleteTextView ac_kurs_invest_di;
    @BindView(R.id.ac_jns_tabungan_autodebet_di)
    AutoCompleteTextView ac_jns_tabungan_autodebet_di;
    @BindView(R.id.ac_kurs_autodebet_di)
    AutoCompleteTextView ac_kurs_autodebet_di;
    @BindView(R.id.ac_aktif_di)
    AutoCompleteTextView ac_aktif_di;
    @BindView(R.id.ac_autodebet_premi_pertama_di)
    AutoCompleteTextView ac_autodebet_premi_pertama_di;
    @BindView(R.id.ac_dana_invest_alokasi_di)
    AutoCompleteTextView ac_dana_invest_alokasi_di;
    @BindView(R.id.ac_jangka_waktu_di)
    AutoCompleteTextView ac_jangka_waktu_di;
    @BindView(R.id.ac_jenis_bunga_di)
    AutoCompleteTextView ac_jenis_bunga_di;
    @BindView(R.id.ac_jenis_rollover_di)
    AutoCompleteTextView ac_jenis_rollover_di;
    @BindView(R.id.ac_status_karyawan_di)
    AutoCompleteTextView ac_status_karyawan_di;
    @BindView(R.id.ac_penarikan_bunga_di)
    AutoCompleteTextView ac_penarikan_bunga_di;
    @BindView(R.id.ac_cara_bayar_rider_di)
    AutoCompleteTextView ac_cara_bayar_rider_di;

    @BindView(R.id.cb_ya_di)
    CheckBox cb_ya_di;
    @BindView(R.id.cb_gunakan_no_rekening_pemegang_polis_untuk_transaksi_di)
    CheckBox cb_gunakan_no_rekening_pemegang_polis_untuk_transaksi_di;
    @BindView(R.id.cb_breakable_di)
    CheckBox cb_breakable_di;
    @BindView(R.id.cb_telemarketing_di)
    CheckBox cb_telemarketing_di;

    @BindView(R.id.img_tambah_penerima_manfaat_di)
    ImageButton img_tambah_penerima_manfaat_di;

    @BindView(R.id.lv_jenis_dana_investasi)
    RecyclerView lv_jenis_dana_investasi;
    @BindView(R.id.lv_jumlah_manfaat_di)
    RecyclerView lv_jumlah_manfaat_di;

    @BindView(R.id.scroll_di)
    NestedScrollView scroll_di;

    private Button btn_lanjut, btn_kembali;
    private ImageView iv_save, iv_next, iv_prev;
    private Toolbar second_toolbar;

    private View view;

    private EspajModel me;

    private ListManfaatAdapter listManfaatAdapter;

    private final String TAG = getClass().getSimpleName();

    private int premi_berkala;
    private int premi_tunggal;
    private int bntuk_pmbyranpremi = -1;
    private int bank_invest;
    private int jenis_tabungan_invest;
    private int jenis_nasabah_invest;
    private String kurs_invest = "";
    private int bank_autodebet;
    private int jns_tabungan_autodebet;
    private String kurs_autodebet;
    private int aktif;
    private int autodebet_premi_pertama;
    private int dana_invest_alokasi;
    private int jangka_waktu;
    private int jenis_bunga;
    private int jns_rollover;
    private int sts_kryawan;
    private int pnrikan_bunga;
    private int cr_bayar;
    private int pjg_rek;
    private int pjg_rek_autodebet;
    private int min_pjg_rek = 0;
    private int JENIS_LOGIN = 0;
    private int JENIS_LOGIN_BC = 0;
    private int groupId = 0;

    private Double jumlah = (double) 0;

    private String cb_ya = "";
    private String cb_gunakan_no_rekening_pemegang_polis_untuk_transaksi = "";
    private String cb_breakable = "";
    private String cb_telemarketing = "";

    private ArrayList<ModelJnsDanaDI> ListDana;
    private ArrayList<ModelDataDitunjukDI> ListManfaat;
    private ArrayList<ModelDropdownInt> ListDropDownInt;
    private ArrayList<ModelDropDownString> ListDropDownString;
    private ArrayList<ModelBank> ListDropDownBank;
    private ArrayList<ModelBankCab> ListDropDownBankCabang;

    //not declare in Layout
    private int premitopup_berkala_di = 1;
    private int premitopup_tunggal_di = 1;
    private Double premi_tambahan_di = (double) 1;
    private int invest_bayar_bayar_premi_di = 1;
    private String invest_cabang_di = "";
    private String kota_id_invest = "";
    private String norek_id_invest = "";
    private String nama_di_invest = "";
    private int berikuasa_di_invest = 0;
    private String tglkuasa_di_invest = "";
    private String keterangan_di_invest = "";
    private int bank_di_autodebet = 1;
    private String simascard_di_autodebet = "";
    private Double jumlah1_di = (double) 1;
    private Double persen1_di = (double) 1;
    private Double jumlah2_di = (double) 1;
    private Double persen2_di = (double) 1;
    private Double jumlah3_di = (double) 1;
    private Double persen3_di = (double) 1;
    private Double total_persen_manfaat = (double) 0;

    private ProgressDialog progressDialog;
}
