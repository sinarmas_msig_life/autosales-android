package id.co.ajsmsig.nb.crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.icu.math.BigDecimal;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.SimpleCursorTreeAdapter;
import android.widget.TextView;
import android.widget.Toast;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.activity.C_TrackPolis;
import id.co.ajsmsig.nb.crm.fragment.C_AgentListSpajFragment;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.util.Const;

/**
 * Created by eriza on 10/08/2017.
 */

public class C_AgentListSPAJDraftAdapter extends SimpleCursorTreeAdapter {
    private String TAG = getClass().getSimpleName();
    private Context context;
    private String kodeAgen;
    private FragmentActivity mActivity;
    private C_AgentListSpajFragment c_agentListSpajFragment;
    private SparseIntArray GroupMap;
    private final int SPAJ_UPLOADED_TO_SERVER = 1;

    public C_AgentListSPAJDraftAdapter(FragmentActivity activity, Context context, C_AgentListSpajFragment c_agentListSpajFragment, String kodeAgen, int groupLayout,
                                       int childLayout, String[] groupFrom, int[] groupTo, String[] childrenFrom,
                                       int[] childrenTo) {
        super(context, null, groupLayout, groupFrom, groupTo,
                childLayout, childrenFrom, childrenTo);
        this.mActivity = activity;
        this.context = context;
        this.kodeAgen = kodeAgen;
        this.c_agentListSpajFragment = c_agentListSpajFragment;
        this.GroupMap = new SparseIntArray();
    }

    @Override
    protected Cursor getChildrenCursor(Cursor cursor) {
        int position = cursor.getPosition();
        int groupId = cursor.getInt(cursor.getColumnIndex(mActivity.getString(R.string._ID)));
        GroupMap.put(groupId, position);

        Loader<Object> loader = mActivity.getSupportLoaderManager().getLoader(groupId);
            if (loader != null && !loader.isReset()) {
                mActivity.getSupportLoaderManager().restartLoader(groupId, null, c_agentListSpajFragment);
            } else {
                mActivity.getSupportLoaderManager().initLoader(groupId, null, c_agentListSpajFragment);
            }

        return null;
    }


    @Override
    protected void bindGroupView(View view, Context context, Cursor cursor, boolean isExpanded) {
        super.bindGroupView(view, context, cursor, isExpanded);

        int groupPosition = cursor.getPosition();
        int childrenCount = getChildrenCount(groupPosition);

        ImageView iv_header_list = view.findViewById(R.id.iv_header_list);

        TextView tv_empty_notif = view.findViewById(R.id.tv_empty_notif);

        switch (groupPosition) {
            case 0:
                tv_empty_notif.setText("Belum ada draft SPAJ yang dibuat");
                break;
            case 1:
                tv_empty_notif.setText("Belum ada SPAJ yang disubmit");
                break;
            default:
                break;
        }

        if (isExpanded) {
            iv_header_list.setImageResource(R.drawable.ic_expand_less_light_grey);
            if (childrenCount == 0) {
                tv_empty_notif.setVisibility(View.VISIBLE);
            } else {
                tv_empty_notif.setVisibility(View.GONE);
            }

        } else {
            iv_header_list.setImageResource(R.drawable.ic_expand_more_light_grey);
            tv_empty_notif.setVisibility(View.GONE);
        }

    }

    @Override
    protected void bindChildView(View view, Context context, Cursor cursor, boolean isLastChild) {
        super.bindChildView(view, context, cursor, isLastChild);
        String spaj_id = "";
        int flag_dokumen = 0;
        String productName = "";
        TextView tv_no_proposal = view.findViewById(R.id.tv_no_proposal);
        TextView tv_pp = view.findViewById(R.id.tv_pp);
        TextView tv_tt = view.findViewById(R.id.tv_tt);
        TextView tv_premi = view.findViewById(R.id.tv_premi);
        TextView tv_up = view.findViewById(R.id.tv_up);
        TextView tv_no_va = view.findViewById(R.id.tv_no_va);
        ConstraintLayout layoutSPAJ = view.findViewById(R.id.layoutSPAJ);
        Button btnTrackPolis = view.findViewById(R.id.btnTrackPolis);


        String spajNumber = cursor.getString(cursor.getColumnIndexOrThrow("SPAJ_ID"));
        String ppName = cursor.getString(cursor.getColumnIndexOrThrow("NAMA_PP"));
        btnTrackPolis.setOnClickListener(v -> {
            Intent intent = new Intent(mActivity, C_TrackPolis.class);
            intent.putExtra(Const.INTENT_KEY_SPAJ_NO_TEMP, spajNumber);
            intent.putExtra(Const.INTENT_KEY_SPAJ_PP_NAME, ppName);
            mActivity.startActivity(intent);
        });

        String spajIdTab = cursor.getString(cursor.getColumnIndexOrThrow("SPAJ_ID_TAB"));
        String noProposalTab = cursor.getString(cursor.getColumnIndexOrThrow("NO_PROPOSAL_TAB"));
        layoutSPAJ.setOnClickListener(v -> {
            Intent intent = new Intent(mActivity, E_MainActivity.class);
            intent.putExtra(mActivity.getString(R.string.SPAJ_ID_TAB), spajIdTab);
            intent.putExtra(mActivity.getString(R.string.idproposal_tab), noProposalTab);
            mActivity.startActivity(intent);
        });

        spaj_id = cursor.getString(cursor.getColumnIndexOrThrow("SPAJ_ID"));
        flag_dokumen = cursor.getInt(cursor.getColumnIndexOrThrow("FLAG_DOKUMEN"));
        productName = new P_Select(context).selectProductName(
                cursor.getInt(cursor.getColumnIndexOrThrow("KD_PRODUK_UA")),
                cursor.getInt(cursor.getColumnIndexOrThrow("SUB_PRODUK_UA")));

        int status = new P_Select(context).isSPAJSubmitted(spaj_id);
        if (status == SPAJ_UPLOADED_TO_SERVER) {
            btnTrackPolis.setVisibility(View.VISIBLE);
        } else {
            btnTrackPolis.setVisibility(View.GONE);
        }
        //btnTrackPolis.setVisibility(View.GONE);
        if (!spaj_id.equals("")) {
            tv_no_proposal.setText(spaj_id);
        } else {
            tv_no_proposal.setText(cursor.getString(cursor.getColumnIndexOrThrow("SPAJ_ID_TAB")));
        }

        if (flag_dokumen != 0) {
            tv_no_va.setVisibility(View.GONE);
        } else {
            tv_no_va.setVisibility(View.VISIBLE);
            tv_no_va.setText("Dokumen Pendukung Belum Terupload");
        }

        tv_pp.setText("Nama PP : " + cursor.getString(cursor.getColumnIndexOrThrow("NAMA_PP")));
        tv_tt.setText("Nama TT : " + cursor.getString(cursor.getColumnIndexOrThrow("NAMA_TT")));
        tv_premi.setText("NO PROPOSAL TAB :" + cursor.getString(cursor.getColumnIndexOrThrow("NO_PROPOSAL_TAB")));
        tv_up.setText("PRODUK : " + productName);
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        return super.getGroupView(groupPosition, isExpanded, convertView, parent);
    }

    public SparseIntArray getGroupMap() {
        return GroupMap;
    }
}
