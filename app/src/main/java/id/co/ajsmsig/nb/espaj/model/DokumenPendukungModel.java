package id.co.ajsmsig.nb.espaj.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import id.co.ajsmsig.nb.espaj.model.arraylist.ModelFoto;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTTD;

/**
 * Created by Envy on 10/8/2017.
 */

public class DokumenPendukungModel implements Parcelable {
    private ArrayList<ModelFoto> ListFoto;
    private ArrayList<ModelTTD> ListTTD;
    private int intcheck;
    private Boolean Validation = false;

    public DokumenPendukungModel() {
    }

    protected DokumenPendukungModel(Parcel in) {
        ListFoto = in.createTypedArrayList(ModelFoto.CREATOR);
        ListTTD = in.createTypedArrayList(ModelTTD.CREATOR);
        intcheck = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(ListFoto);
        dest.writeTypedList(ListTTD);
        dest.writeInt(intcheck);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DokumenPendukungModel> CREATOR = new Creator<DokumenPendukungModel>() {
        @Override
        public DokumenPendukungModel createFromParcel(Parcel in) {
            return new DokumenPendukungModel(in);
        }

        @Override
        public DokumenPendukungModel[] newArray(int size) {
            return new DokumenPendukungModel[size];
        }
    };

    public ArrayList<ModelFoto> getListFoto() {
        return ListFoto;
    }

    public void setListFoto(ArrayList<ModelFoto> listFoto) {
        ListFoto = listFoto;
    }

    public ArrayList<ModelTTD> getListTTD() {
        return ListTTD;
    }

    public void setListTTD(ArrayList<ModelTTD> listTTD) {
        ListTTD = listTTD;
    }

    public int getIntcheck() {
        return intcheck;
    }

    public void setIntcheck(int intcheck) {
        this.intcheck = intcheck;
    }

    public Boolean getValidation() {
        return Validation;
    }

    public void setValidation(Boolean validation) {
        Validation = validation;
    }

    public static Creator<DokumenPendukungModel> getCREATOR() {
        return CREATOR;
    }
}
