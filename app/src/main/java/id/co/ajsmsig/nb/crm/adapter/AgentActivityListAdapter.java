package id.co.ajsmsig.nb.crm.adapter;


import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.ActivityListModel;
import id.co.ajsmsig.nb.crm.model.DashboardActivityListModel;

public class AgentActivityListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<DashboardActivityListModel> activities;
    private ClickListener clickListener;
    private final int HEADER_TYPE = 0;
    private final int CONTENT_TYPE = 1;

    public AgentActivityListAdapter(List<DashboardActivityListModel> activities) {
        this.activities = activities;
    }

    class ActivityListVH extends RecyclerView.ViewHolder {
        private TextView tvActivityDate;
        private TextView tvSubActivityName;
        private TextView tvLeadName;
        private ImageView imgActivity;
        private ConstraintLayout layoutActivity;
        private TextView tvActivityName;
        private ImageView ivSyncIndicator;

        public ActivityListVH(View view) {
            super(view);
            tvActivityDate = view.findViewById(R.id.tvActivityDate);
            tvSubActivityName = view.findViewById(R.id.tvSubActivity);
            tvLeadName = view.findViewById(R.id.tvLeadName);
            imgActivity = view.findViewById(R.id.imgActivity);
            layoutActivity = view.findViewById(R.id.layoutActivity);
            tvActivityName = view.findViewById(R.id.tvActivityName);
            ivSyncIndicator = view.findViewById(R.id.ivSyncIndicator);
        }

    }
    class HeaderVH extends RecyclerView.ViewHolder {
        private TextView tvHeader;

        public HeaderVH(View itemView) {
            super(itemView);
            tvHeader = itemView.findViewById(R.id.tvHeader);
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case HEADER_TYPE:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_agent_activity_header, parent, false);
                return new HeaderVH(itemView);
            case CONTENT_TYPE:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_agent_activity, parent, false);
                return new ActivityListVH(itemView);
            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        DashboardActivityListModel model = activities.get(position);
        int viewType = model.getItemViewType();
        switch (viewType) {
            case HEADER_TYPE :
                HeaderVH headerVH = (HeaderVH) holder;
                headerVH.tvHeader.setText(model.getActivityHeader());
                break;
            case CONTENT_TYPE :
                ActivityListVH contentVH = (ActivityListVH) holder;
                contentVH.tvLeadName.setText(model.getSlName());
                contentVH.tvSubActivityName.setText(model.getSubActivityName());
                contentVH.tvActivityDate.setText(model.getActivityCreatedDate());
                contentVH.layoutActivity.setOnClickListener(v -> clickListener.onActivitySelected(holder.getAdapterPosition(), model));

                if (model.isSynced()){
                    contentVH.ivSyncIndicator.setVisibility(View.INVISIBLE);
                } else {
                    contentVH.ivSyncIndicator.setVisibility(View.VISIBLE);
                }


                String activityName = model.getActivityName();
                if (!TextUtils.isEmpty(activityName) && activityName.equalsIgnoreCase("CLOSING")) {
                    contentVH.tvActivityName.setVisibility(View.GONE);
                } else {
                    contentVH.tvActivityName.setVisibility(View.VISIBLE);
                    contentVH.tvActivityName.setText(model.getActivityName());
                }

                break;
        }

    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    @Override
    public int getItemViewType(int position) {
        return activities.get(position).getItemViewType();
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onActivitySelected(int position, DashboardActivityListModel model);
    }

    public void addAll(List<DashboardActivityListModel> activityList) {
        for (DashboardActivityListModel activity : activityList) {
            activities.add(activity);
        }
        notifyDataSetChanged();
    }
}

