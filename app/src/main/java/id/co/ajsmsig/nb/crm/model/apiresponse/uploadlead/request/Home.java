package id.co.ajsmsig.nb.crm.model.apiresponse.uploadlead.request;

public class Home {
    private int SLA2_ADDR_TYPE;
    private String SLA2_EMAIL;
    private String SLA2_HP1;
    private int SLA2_INST_TAB_ID;
    private int SLA2_INST_TYPE;
    private int SLA2_TAB_ID;

    public Home(int SLA2_ADDR_TYPE, String SLA2_EMAIL, String SLA2_HP1, int SLA2_INST_TAB_ID, int SLA2_INST_TYPE, int SLA2_TAB_ID) {
        this.SLA2_ADDR_TYPE = SLA2_ADDR_TYPE;
        this.SLA2_EMAIL = SLA2_EMAIL;
        this.SLA2_HP1 = SLA2_HP1;
        this.SLA2_INST_TAB_ID = SLA2_INST_TAB_ID;
        this.SLA2_INST_TYPE = SLA2_INST_TYPE;
        this.SLA2_TAB_ID = SLA2_TAB_ID;
    }
}
