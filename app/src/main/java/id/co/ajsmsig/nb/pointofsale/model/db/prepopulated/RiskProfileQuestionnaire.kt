package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.lifecycle.MediatorLiveData
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.databinding.ObservableBoolean

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */
@Entity
class RiskProfileQuestionnaire(
        @PrimaryKey
        val id: Int,
        var parentId: Int?,
        var text: String?,
        var value: Int?
) {

        @Ignore
        var answers: MediatorLiveData<List<RiskProfileQuestionnaire>> = MediatorLiveData()

        @Ignore
        var selected: ObservableBoolean = ObservableBoolean(false)
}