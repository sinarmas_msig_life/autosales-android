package id.co.ajsmsig.nb.pointofsale.ui.ffinancialcalculator


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosFragmentFinancialCalculatorBinding
import id.co.ajsmsig.nb.pointofsale.ui.fragment.BaseFragment


/**
 * A simple [Fragment] subclass.
 */
class FinancialCalculatorFragment : BaseFragment() {

    private lateinit var dataBinding: PosFragmentFinancialCalculatorBinding
    private var justCreated = false

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(FinancialCalculatorVM::class.java)
        subscribeUi(viewModel)

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_financial_calculator, container, false)

        return dataBinding.root
    }

    fun subscribeUi(viewModel: FinancialCalculatorVM) {
        dataBinding.vm = viewModel
        viewModel.calculatorResultModel.observe(this, Observer {
            if (it == null) {
                if (justCreated) {
                    navigationController.backPage()
                } else {
                    justCreated = true
                    navigationController.next()
                }
            }
        })
        viewModel.page = page

    }


}// Required empty public constructor
