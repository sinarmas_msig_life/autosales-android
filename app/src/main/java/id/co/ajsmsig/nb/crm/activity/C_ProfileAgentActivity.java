package id.co.ajsmsig.nb.crm.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.adapter.AgentActivityListAdapter;
import id.co.ajsmsig.nb.crm.database.C_Insert;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.DashboardActivityListModel;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.LeadActivityList;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.DateUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class C_ProfileAgentActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, AgentActivityListAdapter.ClickListener, Callback<List<LeadActivityList>> {
    private String kodeAgen;
    private AgentActivityListAdapter mAdapter;
    private List<DashboardActivityListModel> activities;
    private final int LOADER_DISPLAY_DASHBOARD_ACTIVITY = 200;
    private final int HEADER_TYPE = 0;
    private final int CONTENT_TYPE = 1;
    private Cursor cursor;
    private String TAG = getClass().getSimpleName();

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.layoutLoadingActivityList)
    LinearLayout layoutLoadingActivityList;
    @BindView(R.id.layoutNoActivityFound)
    LinearLayout layoutNoActivityFound;
    @BindView(R.id.tvAgentName)
    TextView tvAgentName;
    @BindView(R.id.tvActivityCount)
    TextView tvActivityCount;

    private C_Select select;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_activity_profile_agent);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Lihat semua aktivitas");
        }

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.app_preferences), MODE_PRIVATE);

        TextView tv_profile_name = findViewById(R.id.tv_profile_name);
        TextView tv_kode_agen = findViewById(R.id.tv_kode_agen);
        tvAgentName.setText(sharedPreferences.getString("NAMA_AGEN", ""));
        tv_profile_name.setText(sharedPreferences.getString("NAMA_AGEN", ""));
        tv_kode_agen.setText(sharedPreferences.getString("KODE_AGEN", ""));

        kodeAgen = sharedPreferences.getString("KODE_AGEN", "");


        if (StaticMethods.isNetworkAvailable(C_ProfileAgentActivity.this)) {
            if (!TextUtils.isEmpty(kodeAgen)) {

                //Fetch activity from server, and insert it to the database
                fetchActivityFromServer(kodeAgen, false, "SLA_CRTD_DATE", "desc");
                getSupportLoaderManager().initLoader(LOADER_DISPLAY_DASHBOARD_ACTIVITY, null, this);
            }

        } else {
            hideLoadingIndicator();
            showRecyclerView();
            getSupportLoaderManager().initLoader(LOADER_DISPLAY_DASHBOARD_ACTIVITY, null, this);
        }


        select = new C_Select(C_ProfileAgentActivity.this);

        initRecyclerView();

    }

    /**
     * Init recyclerview to display activity list
     */
    private void initRecyclerView() {
        activities = new ArrayList<>();
        mAdapter = new AgentActivityListAdapter(activities);
        LinearLayoutManager layoutManager = new LinearLayoutManager(C_ProfileAgentActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setClickListener(this);
    }

    /**
     * Fetch list of BC activity
     *
     * @param msagId        agent code
     * @param sortBy        SLA_CRTD_DATEx
     * @param sortDirection asc / desc
     */
    private void fetchActivityFromServer(String msagId, boolean useLimit, String sortBy, String sortDirection) {

        String afterDate = new C_Select(C_ProfileAgentActivity.this).getLatestActivityInputDate(kodeAgen);
        afterDate = DateUtils.addASecondFrom(afterDate);
        Log.v(TAG, "Downloading activity after " + afterDate);
        if (TextUtils.isEmpty(afterDate)) {
            //There are no lead saved on the database. Fetch all lead from server
            downloadActivity(msagId, useLimit, sortBy, sortDirection, null);
        } else {
            //There are already some lead on the database, fetch only new lead after the specified date
            downloadActivity(msagId, useLimit, sortBy, sortDirection, afterDate);
        }

    }
    private void downloadActivity(String msagId, boolean useLimit, String sortBy, String sortDirection, String afterDate) {
        hideRecyclerView();
        showLoadingIndicator();

        String url = AppConfig.getBaseUrlCRM().concat("v2/toll/module_activity2");
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);
        Call<List<LeadActivityList>> call = api.fetchActivityForDashboard(url, msagId, useLimit, sortBy, sortDirection, afterDate);
        call.enqueue(this);
    }
    @Override
    public void onResume() {
        super.onResume();
        getSupportLoaderManager().restartLoader(LOADER_DISPLAY_DASHBOARD_ACTIVITY, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        showLayoutLoadingActivityList();
        hideRecyclerViewActivityList();

        CursorLoader cursorLoader;

        switch (id) {
            case LOADER_DISPLAY_DASHBOARD_ACTIVITY:
                cursor = new C_Select(this).getActivityListFromAgent(kodeAgen);
                break;
        }


        cursorLoader = new CursorLoader(this) {
            @Override
            public Cursor loadInBackground() {
                return cursor;
            }
        };

        return cursorLoader;

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        hideLayoutLoadingActivityList();
        showRecyclerViewActivityList();

        int activityCount = cursor.getCount();
        tvActivityCount.setText(String.valueOf(activityCount));

        switch (loader.getId()) {
            case LOADER_DISPLAY_DASHBOARD_ACTIVITY:

                if (cursor.getCount() > 0) {

                    clearPreviousResult();

                    List<DashboardActivityListModel> activities = new ArrayList<>();
                    C_Select select = new C_Select(C_ProfileAgentActivity.this);

                    String previousDate = "";
                    DashboardActivityListModel activity;

                    while (cursor.moveToNext()) {
                        long slId = cursor.getLong(cursor.getColumnIndexOrThrow("SL_ID_"));
                        long slaInstTabId = cursor.getLong(cursor.getColumnIndexOrThrow("SLA_INST_TAB_ID"));
                        String slName = cursor.getString(cursor.getColumnIndexOrThrow("SL_NAME"));
                        String activityType = cursor.getString(cursor.getColumnIndexOrThrow("SLA_TYPE"));
                        String subActivityType = cursor.getString(cursor.getColumnIndexOrThrow("SLA_SUBTYPE"));
                        String activityDate = cursor.getString(cursor.getColumnIndexOrThrow("SLA_CRTD_DATE"));
                        int slaLastPost = cursor.getInt(cursor.getColumnIndexOrThrow("SLA_LAST_POS"));
                        boolean isSynced = slaLastPost == 0; //Synced = 0

                        String activityName = select.getSelectedActivityFromSlaType(activityType);
                        String subActivityName = select.getSelectedSubActivityFromSlaSubType(subActivityType);
                        String activityCreatedDate = DateUtils.getDateFrom(activityDate);
                        String activityHeader = DateUtils.getMonthAndYearFromDate(activityDate);

                        if (!TextUtils.isEmpty(activityHeader) && !activityHeader.equalsIgnoreCase(previousDate)) {
                            activity = new DashboardActivityListModel(slId, slaInstTabId, slName, activityName, subActivityName, activityCreatedDate, HEADER_TYPE, activityHeader,isSynced);
                        } else {
                            activity = new DashboardActivityListModel(slId, slaInstTabId, slName, activityName, subActivityName, activityCreatedDate, CONTENT_TYPE, null,isSynced);
                        }
                        previousDate = activityHeader;
                        activities.add(activity);
                    }

                    hideLayoutNoActivityFound();

                    //Add and display the activities
                    mAdapter.addAll(activities);

                } else {
                    showLayoutNoActivityFound();
                }

                break;
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


    private void showLayoutLoadingActivityList() {
        layoutLoadingActivityList.setVisibility(View.VISIBLE);
    }

    private void hideLayoutLoadingActivityList() {
        layoutLoadingActivityList.setVisibility(View.GONE);
    }

    private void showRecyclerViewActivityList() {
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void showLayoutNoActivityFound() {
        layoutNoActivityFound.setVisibility(View.VISIBLE);
    }

    private void hideLayoutNoActivityFound() {
        layoutNoActivityFound.setVisibility(View.GONE);
    }

    private void hideRecyclerViewActivityList() {
        recyclerView.setVisibility(View.GONE);
    }

    private void clearPreviousResult() {
        if (activities != null && activities.size() > 0) {
            activities.clear();
        }
    }

    @Override
    public void onActivitySelected(int position, DashboardActivityListModel model) {
        //Ketika aktivitas ditambahkan melalui download dari web service, SL_ID sudah ada. Convert SL_ID menjadi SL_TAB_ID
        long slId = model.getSlId();
        long slTabId = select.getSlTabIdFromSlId(slId);

        //Ketika aktivitas ditambahkan, SL ID belum ada. Karena SL_ID baru didapatkan setelah aktivitas tersebut berhasil di upload
        if (slId == 0) {
            long slaInstTabId = model.getSlaInstTabId();
            slTabId = slaInstTabId;
        }

        //Gunakan sl tab id sebagai params untuk intent
        Intent intent = new Intent(C_ProfileAgentActivity.this, C_LeadActivity.class);
        Bundle bundle = new Bundle();
        bundle.putLong(getString(R.string.SL_TAB_ID), slTabId);
        bundle.putLong(Const.INTENT_KEY_SL_ID, slId);
        intent.putExtras(bundle);
        startActivity(intent);

    }
    private void showRecyclerView() {
        recyclerView.setVisibility(View.VISIBLE);
    }
    private void hideRecyclerView() {
        recyclerView.setVisibility(View.GONE);
    }
    private void showLoadingIndicator() {
        layoutLoadingActivityList.setVisibility(View.VISIBLE);
    }
    private void hideLoadingIndicator() {
        layoutLoadingActivityList.setVisibility(View.GONE);
    }
    @Override
    public void onResponse(Call<List<LeadActivityList>> call, Response<List<LeadActivityList>> response) {

        C_Insert insert = new C_Insert(C_ProfileAgentActivity.this);

        showRecyclerView();
        hideLoadingIndicator();

        if (response.body() != null) {
            List<LeadActivityList> activities = response.body();

            //Refresh  with the latest data
            if (activities != null && activities.size() > 0) {

                Log.v(TAG, "Downloading activity. New data found. Count : " + activities.size());

                for (LeadActivityList activity : activities) {
                    insert.insertActivityToDatabase(activity);
                }

                //Display the data
                getSupportLoaderManager().restartLoader(LOADER_DISPLAY_DASHBOARD_ACTIVITY, null, this);

            }

        } else if (response.body() == null) {
            //If there is no activity from the selected lead, the server return "null"
            Log.v(TAG, "Downloading activity. No new activity found ");
        }
    }

    @Override
    public void onFailure(Call<List<LeadActivityList>> call, Throwable t) {
        hideRecyclerView();
        hideLoadingIndicator();
        Toast.makeText(C_ProfileAgentActivity.this, "Tidak dapat mengunduh aktivitas terbaru. " + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }
}
