
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrackPolisResponse {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("data")
    @Expose
    private List<PolisData> data = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<PolisData> getData() {
        return data;
    }

    public void setData(List<PolisData> data) {
        this.data = data;
    }

}
