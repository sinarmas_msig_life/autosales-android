package id.co.ajsmsig.nb.crm.model.form;

import android.os.Parcel;
import android.os.Parcelable;

/**
  Created by faiz_f on 17/04/2017.
 */

public class SimbakActivityModel implements Parcelable{
    private Long SLA_TAB_ID;
    private Long SLA_ID;
    private Long SLA_INST_ID;
    private Long SLA_INST_TAB_ID;
    private String SLA_NAME;
    private String SLA_DETAIL;
    private Integer SLA_PREMI;
    private String SLA_IDATE;
    private String SLA_SDATE;
    private String SLA_EDATE;
    private String SLA_REMINDER_DATE;
    private String SLA_CRTD_DATE;
    private String SLA_CRTD_ID;
    private String SLA_OWNER_ID;
    private String SLA_UPDTD_DATE;
    private String SLA_UPDTD_ID;
    private String SLA_LON;
    private String SLA_LAT;
    private Integer SLA_NEXT_ID;
    private String SLA_REFF_ID;
    private String SLA_OLD_ID;
    private Integer SLA_INST_TYPE;
    private Integer SLA_ACTIVE;
    private Integer SLA_TYPE;
    private Integer SLA_SUBTYPE;
    private Integer SLA_CORP_STAGE;
    private Integer SLA_STATUS;
    private Integer SLA_REMINDER_TYPE;
    private Integer SLA_OWNER_TYPE;
    private Integer SLA_OLD_TYPE;
    private Integer SLA_OLD_SUBTYPE;
    private String SLA_USER_ID;
    private Integer SLA_RPT_CAT;
    private String SLA_NXT_ACTION;
    private Integer SLA_CURR_LVL;
    private String SLA_CORP_COMMEN;
    private Integer SLA_PREMI2;
    private Integer SLA_PREMI3;
    private Integer SLA_SRC;
    private Integer SLA_LAST_POS;
    private String SL_NAME;
    private String SL_ID_;
    private SimbakSpajModel SPAJ = null;

    public SimbakActivityModel() {
    }


    protected SimbakActivityModel(Parcel in) {
        SLA_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLA_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLA_INST_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLA_INST_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SLA_NAME = in.readString();
        SLA_DETAIL = in.readString();
        SLA_PREMI = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_IDATE = in.readString();
        SLA_SDATE = in.readString();
        SLA_EDATE = in.readString();
        SLA_REMINDER_DATE = in.readString();
        SLA_CRTD_DATE = in.readString();
        SLA_CRTD_ID = in.readString();
        SLA_OWNER_ID = in.readString();
        SLA_UPDTD_DATE = in.readString();
        SLA_UPDTD_ID = in.readString();
        SLA_LON = in.readString();
        SLA_LAT = in.readString();
        SLA_NEXT_ID = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_REFF_ID = in.readString();
        SLA_OLD_ID = in.readString();
        SLA_INST_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_ACTIVE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_SUBTYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_CORP_STAGE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_STATUS = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_REMINDER_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_OWNER_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_OLD_TYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_OLD_SUBTYPE = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_USER_ID = in.readString();
        SLA_RPT_CAT = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_NXT_ACTION = in.readString();
        SLA_CURR_LVL = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_CORP_COMMEN = in.readString();
        SLA_PREMI2 = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_PREMI3 = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_SRC = (Integer) in.readValue(Integer.class.getClassLoader());
        SLA_LAST_POS = (Integer) in.readValue(Integer.class.getClassLoader());
        SPAJ = in.readParcelable(SimbakSpajModel.class.getClassLoader());
        SL_NAME = in.readString();
        SL_ID_ = in.readString();
    }

    public static final Creator<SimbakActivityModel> CREATOR = new Creator<SimbakActivityModel>() {
        @Override
        public SimbakActivityModel createFromParcel(Parcel in) {
            return new SimbakActivityModel(in);
        }

        @Override
        public SimbakActivityModel[] newArray(int size) {
            return new SimbakActivityModel[size];
        }
    };

    public Long getSLA_TAB_ID() {
        return SLA_TAB_ID;
    }

    public void setSLA_TAB_ID(Long SLA_TAB_ID) {
        this.SLA_TAB_ID = SLA_TAB_ID;
    }

    public Long getSLA_ID() {
        return SLA_ID;
    }

    public void setSLA_ID(Long SLA_ID) {
        this.SLA_ID = SLA_ID;
    }

    public Long getSLA_INST_ID() {
        return SLA_INST_ID;
    }

    public void setSLA_INST_ID(Long SLA_INST_ID) {
        this.SLA_INST_ID = SLA_INST_ID;
    }

    public Long getSLA_INST_TAB_ID() {
        return SLA_INST_TAB_ID;
    }

    public void setSLA_INST_TAB_ID(Long SLA_INST_TAB_ID) {
        this.SLA_INST_TAB_ID = SLA_INST_TAB_ID;
    }

    public String getSLA_NAME() {
        return SLA_NAME;
    }

    public void setSLA_NAME(String SLA_NAME) {
        this.SLA_NAME = SLA_NAME;
    }

    public String getSLA_DETAIL() {
        return SLA_DETAIL;
    }

    public void setSLA_DETAIL(String SLA_DETAIL) {
        this.SLA_DETAIL = SLA_DETAIL;
    }

    public Integer getSLA_PREMI() {
        return SLA_PREMI;
    }

    public void setSLA_PREMI(Integer SLA_PREMI) {
        this.SLA_PREMI = SLA_PREMI;
    }

    public String getSLA_IDATE() {
        return SLA_IDATE;
    }

    public void setSLA_IDATE(String SLA_IDATE) {
        this.SLA_IDATE = SLA_IDATE;
    }

    public String getSLA_SDATE() {
        return SLA_SDATE;
    }

    public void setSLA_SDATE(String SLA_SDATE) {
        this.SLA_SDATE = SLA_SDATE;
    }

    public String getSLA_EDATE() {
        return SLA_EDATE;
    }

    public void setSLA_EDATE(String SLA_EDATE) {
        this.SLA_EDATE = SLA_EDATE;
    }

    public String getSLA_REMINDER_DATE() {
        return SLA_REMINDER_DATE;
    }

    public void setSLA_REMINDER_DATE(String SLA_REMINDER_DATE) {
        this.SLA_REMINDER_DATE = SLA_REMINDER_DATE;
    }

    public String getSLA_CRTD_DATE() {
        return SLA_CRTD_DATE;
    }

    public void setSLA_CRTD_DATE(String SLA_CRTD_DATE) {
        this.SLA_CRTD_DATE = SLA_CRTD_DATE;
    }

    public String getSLA_CRTD_ID() {
        return SLA_CRTD_ID;
    }

    public void setSLA_CRTD_ID(String SLA_CRTD_ID) {
        this.SLA_CRTD_ID = SLA_CRTD_ID;
    }

    public String getSLA_OWNER_ID() {
        return SLA_OWNER_ID;
    }

    public void setSLA_OWNER_ID(String SLA_OWNER_ID) {
        this.SLA_OWNER_ID = SLA_OWNER_ID;
    }

    public String getSLA_UPDTD_DATE() {
        return SLA_UPDTD_DATE;
    }

    public void setSLA_UPDTD_DATE(String SLA_UPDTD_DATE) {
        this.SLA_UPDTD_DATE = SLA_UPDTD_DATE;
    }

    public String getSLA_UPDTD_ID() {
        return SLA_UPDTD_ID;
    }

    public void setSLA_UPDTD_ID(String SLA_UPDTD_ID) {
        this.SLA_UPDTD_ID = SLA_UPDTD_ID;
    }

    public String getSLA_LON() {
        return SLA_LON;
    }

    public void setSLA_LON(String SLA_LON) {
        this.SLA_LON = SLA_LON;
    }

    public String getSLA_LAT() {
        return SLA_LAT;
    }

    public void setSLA_LAT(String SLA_LAT) {
        this.SLA_LAT = SLA_LAT;
    }

    public Integer getSLA_NEXT_ID() {
        return SLA_NEXT_ID;
    }

    public void setSLA_NEXT_ID(Integer SLA_NEXT_ID) {
        this.SLA_NEXT_ID = SLA_NEXT_ID;
    }

    public String getSLA_REFF_ID() {
        return SLA_REFF_ID;
    }

    public void setSLA_REFF_ID(String SLA_REFF_ID) {
        this.SLA_REFF_ID = SLA_REFF_ID;
    }

    public String getSLA_OLD_ID() {
        return SLA_OLD_ID;
    }

    public void setSLA_OLD_ID(String SLA_OLD_ID) {
        this.SLA_OLD_ID = SLA_OLD_ID;
    }

    public Integer getSLA_INST_TYPE() {
        return SLA_INST_TYPE;
    }

    public void setSLA_INST_TYPE(Integer SLA_INST_TYPE) {
        this.SLA_INST_TYPE = SLA_INST_TYPE;
    }

    public Integer getSLA_ACTIVE() {
        return SLA_ACTIVE;
    }

    public void setSLA_ACTIVE(Integer SLA_ACTIVE) {
        this.SLA_ACTIVE = SLA_ACTIVE;
    }

    public Integer getSLA_TYPE() {
        return SLA_TYPE;
    }

    public void setSLA_TYPE(Integer SLA_TYPE) {
        this.SLA_TYPE = SLA_TYPE;
    }

    public Integer getSLA_SUBTYPE() {
        return SLA_SUBTYPE;
    }

    public void setSLA_SUBTYPE(Integer SLA_SUBTYPE) {
        this.SLA_SUBTYPE = SLA_SUBTYPE;
    }

    public Integer getSLA_CORP_STAGE() {
        return SLA_CORP_STAGE;
    }

    public void setSLA_CORP_STAGE(Integer SLA_CORP_STAGE) {
        this.SLA_CORP_STAGE = SLA_CORP_STAGE;
    }

    public Integer getSLA_STATUS() {
        return SLA_STATUS;
    }

    public void setSLA_STATUS(Integer SLA_STATUS) {
        this.SLA_STATUS = SLA_STATUS;
    }

    public Integer getSLA_REMINDER_TYPE() {
        return SLA_REMINDER_TYPE;
    }

    public void setSLA_REMINDER_TYPE(Integer SLA_REMINDER_TYPE) {
        this.SLA_REMINDER_TYPE = SLA_REMINDER_TYPE;
    }

    public Integer getSLA_OWNER_TYPE() {
        return SLA_OWNER_TYPE;
    }

    public void setSLA_OWNER_TYPE(Integer SLA_OWNER_TYPE) {
        this.SLA_OWNER_TYPE = SLA_OWNER_TYPE;
    }

    public Integer getSLA_OLD_TYPE() {
        return SLA_OLD_TYPE;
    }

    public void setSLA_OLD_TYPE(Integer SLA_OLD_TYPE) {
        this.SLA_OLD_TYPE = SLA_OLD_TYPE;
    }

    public Integer getSLA_OLD_SUBTYPE() {
        return SLA_OLD_SUBTYPE;
    }

    public void setSLA_OLD_SUBTYPE(Integer SLA_OLD_SUBTYPE) {
        this.SLA_OLD_SUBTYPE = SLA_OLD_SUBTYPE;
    }

    public String getSLA_USER_ID() {
        return SLA_USER_ID;
    }

    public void setSLA_USER_ID(String SLA_USER_ID) {
        this.SLA_USER_ID = SLA_USER_ID;
    }

    public Integer getSLA_RPT_CAT() {
        return SLA_RPT_CAT;
    }

    public void setSLA_RPT_CAT(Integer SLA_RPT_CAT) {
        this.SLA_RPT_CAT = SLA_RPT_CAT;
    }

    public String getSLA_NXT_ACTION() {
        return SLA_NXT_ACTION;
    }

    public void setSLA_NXT_ACTION(String SLA_NXT_ACTION) {
        this.SLA_NXT_ACTION = SLA_NXT_ACTION;
    }

    public Integer getSLA_CURR_LVL() {
        return SLA_CURR_LVL;
    }

    public void setSLA_CURR_LVL(Integer SLA_CURR_LVL) {
        this.SLA_CURR_LVL = SLA_CURR_LVL;
    }

    public String getSLA_CORP_COMMEN() {
        return SLA_CORP_COMMEN;
    }

    public void setSLA_CORP_COMMEN(String SLA_CORP_COMMEN) {
        this.SLA_CORP_COMMEN = SLA_CORP_COMMEN;
    }

    public Integer getSLA_PREMI2() {
        return SLA_PREMI2;
    }

    public void setSLA_PREMI2(Integer SLA_PREMI2) {
        this.SLA_PREMI2 = SLA_PREMI2;
    }

    public Integer getSLA_PREMI3() {
        return SLA_PREMI3;
    }

    public void setSLA_PREMI3(Integer SLA_PREMI3) {
        this.SLA_PREMI3 = SLA_PREMI3;
    }

    public Integer getSLA_SRC() {
        return SLA_SRC;
    }

    public void setSLA_SRC(Integer SLA_SRC) {
        this.SLA_SRC = SLA_SRC;
    }

    public Integer getSLA_LAST_POS() {
        return SLA_LAST_POS;
    }

    public void setSLA_LAST_POS(Integer SLA_LAST_POS) {
        this.SLA_LAST_POS = SLA_LAST_POS;
    }

    public SimbakSpajModel getSPAJ() {
        return SPAJ;
    }

    public void setSPAJ(SimbakSpajModel SPAJ) {
        this.SPAJ = SPAJ;
    }
    public String getSL_NAME() {
        return SL_NAME;
    }

    public void setSL_NAME(String SL_NAME) {
        this.SL_NAME = SL_NAME;
    }

    public String getSL_ID_() {
        return SL_ID_;
    }

    public void setSL_ID_(String SL_ID_) {
        this.SL_ID_ = SL_ID_;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(SLA_TAB_ID);
        dest.writeValue(SLA_ID);
        dest.writeValue(SLA_INST_ID);
        dest.writeValue(SLA_INST_TAB_ID);
        dest.writeString(SLA_NAME);
        dest.writeString(SLA_DETAIL);
        dest.writeValue(SLA_PREMI);
        dest.writeString(SLA_IDATE);
        dest.writeString(SLA_SDATE);
        dest.writeString(SLA_EDATE);
        dest.writeString(SLA_REMINDER_DATE);
        dest.writeString(SLA_CRTD_DATE);
        dest.writeString(SLA_CRTD_ID);
        dest.writeString(SLA_OWNER_ID);
        dest.writeString(SLA_UPDTD_DATE);
        dest.writeString(SLA_UPDTD_ID);
        dest.writeString(SLA_LON);
        dest.writeString(SLA_LAT);
        dest.writeValue(SLA_NEXT_ID);
        dest.writeString(SLA_REFF_ID);
        dest.writeString(SLA_OLD_ID);
        dest.writeValue(SLA_INST_TYPE);
        dest.writeValue(SLA_ACTIVE);
        dest.writeValue(SLA_TYPE);
        dest.writeValue(SLA_SUBTYPE);
        dest.writeValue(SLA_CORP_STAGE);
        dest.writeValue(SLA_STATUS);
        dest.writeValue(SLA_REMINDER_TYPE);
        dest.writeValue(SLA_OWNER_TYPE);
        dest.writeValue(SLA_OLD_TYPE);
        dest.writeValue(SLA_OLD_SUBTYPE);
        dest.writeString(SLA_USER_ID);
        dest.writeValue(SLA_RPT_CAT);
        dest.writeString(SLA_NXT_ACTION);
        dest.writeValue(SLA_CURR_LVL);
        dest.writeString(SLA_CORP_COMMEN);
        dest.writeValue(SLA_PREMI2);
        dest.writeValue(SLA_PREMI3);
        dest.writeValue(SLA_SRC);
        dest.writeValue(SLA_LAST_POS);
        dest.writeParcelable(SPAJ, flags);
        dest.writeString(SL_NAME);
        dest.writeValue(SL_ID_);
    }

}
