package id.co.ajsmsig.nb.prop.model.form;
/*
 *Created by faiz_f on 19/05/2017.
 */

import id.co.ajsmsig.nb.prop.model.DropboxModel;

public class PilihanInvestasiModel {
    private DropboxModel dImvest;
    private DropboxModel dNilaiInvest;

    public PilihanInvestasiModel() {
    }

    public DropboxModel getdImvest() {
        return dImvest;
    }

    public void setdImvest(DropboxModel dImvest) {
        this.dImvest = dImvest;
    }

    public DropboxModel getdNilaiInvest() {
        return dNilaiInvest;
    }

    public void setdNilaiInvest(DropboxModel dNilaiInvest) {
        this.dNilaiInvest = dNilaiInvest;
    }
}
