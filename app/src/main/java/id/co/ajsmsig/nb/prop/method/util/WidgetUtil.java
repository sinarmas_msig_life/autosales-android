package id.co.ajsmsig.nb.prop.method.util;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

/**
 * Created by faiz_f on 17/10/2016.
 */

public class WidgetUtil {

    public static boolean isVisible(LinearLayout linearLayout){
        boolean visible = false;
        if(linearLayout.getVisibility() == View.VISIBLE){
            visible = true;
        }
        return visible;
    }

    public static boolean isVisible(EditText editText){
        boolean visible = false;
        if(editText.getVisibility() == View.VISIBLE){
            visible = true;
        }
        return visible;
    }

    public static boolean isVisible(Spinner spinner){
        boolean visible = false;
        if(spinner.getVisibility() == View.VISIBLE){
            visible = true;
        }
        return visible;
    }
}
