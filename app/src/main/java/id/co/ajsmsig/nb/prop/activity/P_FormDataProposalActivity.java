package id.co.ajsmsig.nb.prop.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;

import static id.co.ajsmsig.nb.crm.method.StaticMethods.toCalendar;
import static id.co.ajsmsig.nb.crm.method.StaticMethods.toStringDate;

public class P_FormDataProposalActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    private P_MstDataProposalModel p_mstDataProposalModel;
    private Intent intent;
    private DropboxModel dSex;
    private int fieldType;
    private Calendar calendar;
    private boolean isClickAble = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_activity_form_data_proposal);
        ButterKnife.bind(this);

        intent = getIntent();
        fieldType = intent.getIntExtra(getString(R.string.FIELD_TYPE), 0);
        p_mstDataProposalModel = intent.getParcelableExtra(getString(R.string.MST_DATA_PROPOSAL));

        btn_ok.setOnClickListener(this);
        et_tl.setOnClickListener(this);
        ac_sex.setOnClickListener(this);
        et_tl.setOnFocusChangeListener(this);
        ac_sex.setOnFocusChangeListener(this);
        et_nama.addTextChangedListener(new MTextWatcher(et_nama));
        et_tl.addTextChangedListener(new MTextWatcher(et_tl));
        et_umur.addTextChangedListener(new MTextWatcher(et_umur));
        ac_sex.addTextChangedListener(new MTextWatcher(ac_sex));

        updateView();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public void updateView() {
        ArrayList<DropboxModel> genders = StaticMethods.getListGender();
        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);
        toolbar.setDisplayShowHomeEnabled(true);
        switch (fieldType) {
            case 0://pp
                toolbar.setTitle("Form Pemegang Polis");
                tv_tittle_form.setText(getString(R.string.pemegang_polis));
                if (p_mstDataProposalModel.getNama_pp() != null) {
                    et_nama.setText(p_mstDataProposalModel.getNama_pp());
                }
                if (p_mstDataProposalModel.getTgl_lahir_pp() != null) {
                    calendar = toCalendar(p_mstDataProposalModel.getTgl_lahir_pp());
                    et_tl.setText(toStringDate(calendar, 2));
                    et_umur.setText(StaticMethods.onCountUsia(calendar));
                }
                if (p_mstDataProposalModel.getSex_pp() != null) {
                    switch (p_mstDataProposalModel.getSex_pp()) {
                        case 0:
                            ac_sex.setText(genders.get(1).toString());
                            dSex = genders.get(1);
                            break;
                        case 1:
                            ac_sex.setText(genders.get(0).toString());
                            dSex = genders.get(0);
                            break;
                        default:
                            break;
                    }
                }
                break;
            case 1:
                toolbar.setTitle("Form Tertanggung");
                tv_tittle_form.setText(getString(R.string.tertanggung));
                if (p_mstDataProposalModel.getNama_tt() != null) {
                    et_nama.setText(p_mstDataProposalModel.getNama_tt());
                }
                if (p_mstDataProposalModel.getTgl_lahir_tt() != null) {
                    calendar = toCalendar(p_mstDataProposalModel.getTgl_lahir_tt());
                    et_tl.setText(toStringDate(calendar, 2));
                    et_umur.setText(StaticMethods.onCountUsia(calendar));
                }
                if (p_mstDataProposalModel.getSex_tt() != null) {
                    switch (p_mstDataProposalModel.getSex_tt()) {
                        case 0:
                            ac_sex.setText(genders.get(1).toString());
                            dSex = genders.get(1);
                            break;
                        case 1:
                            ac_sex.setText(genders.get(0).toString());
                            dSex = genders.get(0);
                            break;
                        default:
                            break;
                    }
                }
                break;
            default:
                break;
        }

        ArrayList<DropboxModel> dropboxModels = StaticMethods.getListGender();
        ArrayAdapter<DropboxModel> actvAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, dropboxModels);

        ac_sex.setAdapter(actvAdapter);
        ac_sex.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dSex = (DropboxModel) parent.getAdapter().getItem(position);
            }
        });


    }

    @Override
    public void onConfigurationChanged(Configuration confi) {
        super.onConfigurationChanged(confi);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_tl:
                if (isClickAble) {
                    isClickAble = false;
                    showDateDialog();
                }
                break;
            case R.id.ac_sex:
                ac_sex.showDropDown();
                break;
            case R.id.btn_ok:
                if (isClickAble) {
                    isClickAble = false;
                    clickOk();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.et_tl:
                if (hasFocus) {
                    showDateDialog();
                }
                break;
            case R.id.ac_sex:
                if (hasFocus) {
                    ac_sex.showDropDown();
                }
                break;
            default:
                break;
        }
    }

    public void showDateDialog() {
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                et_tl.setText(StaticMethods.toStringDate(calendar, 2));
                et_umur.setText(StaticMethods.onCountUsia(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE)));
            }
        };

        String tl = et_tl.getText().toString();
        Calendar mCalendar;
        if (tl.trim().length() == 0) {
            calendar = Calendar.getInstance();
            calendar.add(Calendar.YEAR, -30);
        } else {
            calendar = toCalendar(tl);
        }
        mCalendar = calendar;
        DatePickerDialog dateDialog = new DatePickerDialog(P_FormDataProposalActivity.this, date, mCalendar
                .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH));


        dateDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isClickAble = true;
            }
        });

        dateDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                isClickAble = true;
            }
        });

        dateDialog.getDatePicker().setMaxDate(new Date().getTime());

        if (!dateDialog.isShowing()) {
            dateDialog.show();
        }
    }

    public void clickOk() {
        boolean isOkay = true;

        if (ac_sex.getText().toString().trim().length() == 0) {
            isOkay = false;
            tv_error_sex.setText(R.string.error_sex_empty);
            tv_error_sex.setVisibility(View.VISIBLE);
            iv_circle_sex.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
        }

        if (et_umur.getText().toString().trim().length() == 0) {
            isOkay = false;
            tv_error_umur.setText(R.string.error_age_empty);
            tv_error_umur.setVisibility(View.VISIBLE);
            iv_circle_umur.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
        }

//        if (fieldType == 0) {//pp
//            if (Integer.parseInt(et_umur.getText().toString()) < 17) {
//                tv_error_umur.setText(getString(R.string.pp_umur_min).replace("#{tahun}", "17"));
//                isOkay = false;
//            }
//        }

        if (et_tl.getText().toString().trim().length() == 0) {
            isOkay = false;
            tv_error_tl.setText(R.string.error_birthday_empty);
            tv_error_tl.setVisibility(View.VISIBLE);
            iv_circle_tl.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
        }

        if (et_nama.getText().toString().trim().length() == 0) {
            isOkay = false;
            tv_error_nama.setText(R.string.error_name_empty);
            tv_error_nama.setVisibility(View.VISIBLE);
            iv_circle_nama.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
        }else {
            if(et_nama.getText().toString().contains("'")){
                tv_error_nama.setText("Nama tidak boleh mengandung karakter '");
                tv_error_nama.setVisibility(View.VISIBLE);
                iv_circle_nama.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
            }
        }

        if (isOkay) {
            switch (fieldType) {
                case 0:
                    p_mstDataProposalModel.setNama_pp(et_nama.getText().toString());
                    p_mstDataProposalModel.setTgl_lahir_pp(toStringDate(calendar));
                    p_mstDataProposalModel.setUmur_pp(Integer.parseInt(et_umur.getText().toString()));
                    p_mstDataProposalModel.setSex_pp(dSex.getId());
                    if (p_mstDataProposalModel.getNama_tt() == null) {
                        p_mstDataProposalModel.setNama_tt(et_nama.getText().toString());
                        p_mstDataProposalModel.setTgl_lahir_tt(toStringDate(calendar));
                        p_mstDataProposalModel.setUmur_tt(Integer.parseInt(et_umur.getText().toString()));
                        p_mstDataProposalModel.setSex_tt(dSex.getId());
                    }
                    break;
                case 1:
                    p_mstDataProposalModel.setNama_tt(et_nama.getText().toString());
                    p_mstDataProposalModel.setTgl_lahir_tt(toStringDate(calendar));
                    p_mstDataProposalModel.setUmur_tt(Integer.parseInt(et_umur.getText().toString()));
                    p_mstDataProposalModel.setSex_tt(dSex.getId());
                    break;
                default:
                    break;
            }
            intent = new Intent();
            intent.putExtra(getString(R.string.MST_DATA_PROPOSAL), p_mstDataProposalModel);
            intent.putExtra(getString(R.string.FIELD_TYPE), fieldType);
            setResult(RESULT_OK, intent);
            finish();
//            Toast.makeText(this, et_umur.getText().toString(), Toast.LENGTH_SHORT).show();
        } else {
            isClickAble = true;
        }

    }


    private class MTextWatcher implements TextWatcher {
        private View view;

        MTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                case R.id.et_nama:
                    if (StaticMethods.isTextWidgetEmpty(et_nama)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_nama.setColorFilter(color);
                    tv_error_nama.setVisibility(View.GONE);
                    break;
                case R.id.et_tl:
                    if (StaticMethods.isTextWidgetEmpty(et_tl)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_tl.setColorFilter(color);
                    tv_error_tl.setVisibility(View.GONE);
                    break;
                case R.id.et_umur:
                    if (StaticMethods.isTextWidgetEmpty(et_umur)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_umur.setColorFilter(color);
                    tv_error_umur.setVisibility(View.GONE);
                    break;
                case R.id.ac_sex:
                    if (StaticMethods.isTextWidgetEmpty(ac_sex)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_sex.setColorFilter(color);
                    tv_error_sex.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }
    }

    @BindView(R.id.et_nama)
    EditText et_nama;
    @BindView(R.id.et_tl)
    EditText et_tl;
    @BindView(R.id.et_umur)
    EditText et_umur;
    @BindView(R.id.ac_sex)
    AutoCompleteTextView ac_sex;
    @BindView(R.id.btn_ok)
    Button btn_ok;
    @BindView(R.id.iv_circle_nama)
    ImageView iv_circle_nama;
    @BindView(R.id.iv_circle_tl)
    ImageView iv_circle_tl;
    @BindView(R.id.iv_circle_umur)
    ImageView iv_circle_umur;
    @BindView(R.id.iv_circle_sex)
    ImageView iv_circle_sex;
    @BindView(R.id.tv_error_nama)
    TextView tv_error_nama;
    @BindView(R.id.tv_error_tl)
    TextView tv_error_tl;
    @BindView(R.id.tv_error_umur)
    TextView tv_error_umur;
    @BindView(R.id.tv_error_sex)
    TextView tv_error_sex;
    @BindView(R.id.tv_tittle_form)
    TextView tv_tittle_form;
}
