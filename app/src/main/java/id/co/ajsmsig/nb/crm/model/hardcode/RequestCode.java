package id.co.ajsmsig.nb.crm.model.hardcode;

/**
 * Created by faiz_f on 21/03/2017.
 */

public class RequestCode {
    public static final int CREATE_LEAD_REQUEST = 1;
    public static final int OPEN_LEAD_ACTIVITY = 98;
    public static final int EDIT_FORM_LEAD = 99;
    public static final int FILL_FORM_DATA_PROPOSAL = 100;
    public static final int FILL_FORM_ALAMAT = 151;
    public static final int FILL_FORM_AKTIVITAS = 152;
    public static final int OPEN_PESERTA_ACTIVITY = 200;
    public static final int FILL_FORM_PESERTA_PROPOSAL = 250;
    public static final int FILL_ALAMAT_SPAJ = 500;
    public static final int FILL_HP_SPAJ = 501;
    public static final int FILL_RIDER = 600;
    public static final int FILL_MANFAAT_SPAJ = 700;
    public static final int CALCULATE_ILLUSTRATION = 800;
    public static final int REQ_POS = 900;
    public static final int REQ_PAYMENT = 400;
}
