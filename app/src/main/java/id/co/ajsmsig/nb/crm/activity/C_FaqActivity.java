package id.co.ajsmsig.nb.crm.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.adapter.ExpandableListAdapter;

/**
 * Created by Bernard on 08/11/2017.
 */

public class C_FaqActivity extends AppCompatActivity implements OnGroupClickListener,
        OnGroupExpandListener,
        OnGroupCollapseListener {

    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.expandableListView)
    ExpandableListView expandableListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_activity_faq_layout);
        ButterKnife.bind(this);


        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }


        prepareListData();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        expandableListView.setAdapter(listAdapter);
        expandableListView.setOnGroupClickListener(this);
        expandableListView.setOnGroupExpandListener(this);
        expandableListView.setOnGroupCollapseListener(this);
    }


    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        // Adding child data
        listDataHeader.add("[Informasi SMiLe Go Public] 1. Mengapa Sinarmas MSIG Life memutuskan untuk go public?");
        listDataHeader.add("[Informasi SMiLe Go Public] 2. Apakah upaya go public ini berarti Sinarmas MSIG Life sedang membutuhkan modal?");
        listDataHeader.add("[Informasi SMiLe Go Public] 3. Siapa yang kini menjadi pemegang saham terbesar Sinarmas MSIG Life?");
        listDataHeader.add("[Informasi SMiLe Go Public] 4. Apakah dengan dilakukannya go public ini, Sinarmas MSIG Life akan mengubah kebijakan dan strategi bisnis yang telah dijalankan?");
        listDataHeader.add("[Informasi SMiLe Go Public] 5. Apakah prosedur klaim dan yang lain yang berhubungan dengan kontrak saya akan berubah setelah go public?");

        listDataHeader.add("6. Lupa password login, bagaimana cara mendapatkan password baru?");
        listDataHeader.add("7. Bagaimana jika di lokasi saya berada, sama sekali tidak tersedia jaringan internet dan muncul notifikasi “TIDAK TERHUBUNG KE INTERNET”?");
        listDataHeader.add("8. Apa saja aktivitas di AutoSales?");
        listDataHeader.add("9. Bagaimana sistem perhitungan rasio kesuksesan?");
        listDataHeader.add("10. Mengapa angka SPAJ hari ini / aktivitas hari ini terkadang tidak langsung terupdate?");
        listDataHeader.add("11. Apakah saat aplikasi AutoSales menampilkan PDF proposal, file pdf sudah terdownload secara otomatis?");
        listDataHeader.add("12. PDF viewer apa yang baiknya digunakan sebagai default otomatis?");
        listDataHeader.add("13. Bagaimana cara rename file PDF yang telah terdownload?");
        listDataHeader.add("14. Bagaimana jika ada proposal yang memiliki kondisi tidak layak?");

        listDataHeader.add("15. Bagaimana format penulisan nomor KTP dan NPWP? ");
        listDataHeader.add("16. Bagaimana cara singkat untuk pilih profesi pada klarifikasi pekerjaan?");
        listDataHeader.add("17. Mengapa saat submit gagal karna ada data tidak lengkap, dan kembali ke halaman Usulan Asuransi?");
        listDataHeader.add("18. Mengapa saat submit gagal karna ada data tidak lengkap, dan tampilan kembali ke halaman Detil Investasi?");
        listDataHeader.add("19. Saat melakukan submit muncul notifikasi 'Terjadi kesalahan pada saat pengiriman data ke server untuk no SPAJ ... '");
        listDataHeader.add("20. Apakah diperbolehkan menerima ID Referal dari staff bank cabang?");
        listDataHeader.add("21. Mengapa terus kembali ke halaman Pernyataan Kesehatan?");
        listDataHeader.add("22. Mengapa di SPAJ yang telah submit ada text merah “dokumen pendukung belum terupload” ?");
        listDataHeader.add("23. Apa saja yang wajib diattach pada submission SPAJ?");
        listDataHeader.add("24. Apa saja yang wajib dikirimkan ke Document Center?");
        listDataHeader.add("25. Dimana alamat Document Center?");
        listDataHeader.add("26. Adakah pendistribusian dokumen dari cabang PT Asuransi Jiwa Sinarmas MSIG Tbk. terdekat ke kantor pusat?");
        listDataHeader.add("27. Bagaimanakah cara penggunakan menu 'Tarik Virtual Account'?");
        listDataHeader.add("28. Apa itu Tarik Nomor Virtual Account?");

        listDataHeader.add("29. Bagaimana cara menggunakan Validasi Virtual Account?");
        listDataHeader.add("30. Apa itu Validasi Virtual Account dan dengan kondisi apa dapat digunakan?");
        listDataHeader.add("31. 3 data apa yang wajib diisikan terlebih dahulu untuk menSkip SPAJ ke halaman selanjutnya?");
        listDataHeader.add("32. Tidak menemukan Nama referral yang saya tuju, bagaimana cara menambahkannya?");
        listDataHeader.add("33. Cabang Bank penempatan saya tidak tersedia, bagaimana cara menambahkannya?");
        listDataHeader.add("34. Apa saja aktivitas di AutoSales?");
        listDataHeader.add("35. Dalam penggunaan AutoSales Aktivitas apa saja yang terinput otomatis?");
        listDataHeader.add("36. Bagaimana cara menginput aktivitas closing? Dan bagaimana kondisinya?");

        // Adding child data
        List<String> answerNumberIPO1 = new ArrayList<>();
        answerNumberIPO1.add("Manajemen Sinarmas MSIG Life memberi kesempatan kepada masyarakat luas untuk turut memiliki perusahaan yang selama 34 tahun telah sukses berkiprah di industri asuransi jiwa nasional.");

        List<String> answerNumberIPO2 = new ArrayList<>();
        answerNumberIPO2.add("1. Sinarmas MSIG Life adalah perusahaan yang besar dan kuat. Sejak tahun 2014, Perusahaan senantiasa mencatat nilai aset di atas Rp15,5 triliun. Rasio solvabilitas yang merupakan indikator kemampuan perusahaan untuk mengantisipasi risiko kerugian sejak tahun 2017 bahkan tercatat di atas 1.000%, jauh melampaui ketentuan 120% yang dipersyaratkan Pemerintah.\n\n" +
                "2. Kebijakan untuk melakukan go public ini semata-mata merupakan upaya divestasi sebagian saham Sinarmas Multiartha Tbk sebesar 37.5% ke masyarakat. Untuk itu, tidak ada penambahan dana karena tidak ada penerbitan saham baru.");

        List<String> answerNumberIPO3 = new ArrayList<>();
        answerNumberIPO3.add("1. Pemilik saham terbesar Sinarmas MSIG Life saat ini adalah Mitsui Sumitomo Insurance Co., Ltd.\n\n" +
                "2. MS&AD yang merupakan induk perusahaan Mitsui Sumitomo Insurance memegang pangsa terbesar premi asuransi kerugian di Jepang dan termasuk dalam 10 besar bisnis grup asuransi kerugian untuk kategori pendapatan terbesar di dunia. Berpengalaman panjang selama 90 tahun, MS&AD kini menyelenggarakan aktivitas bisnis global melalui jaringan internasional di 42 negara.");

        List<String> answerNumberIPO4 = new ArrayList<>();
        answerNumberIPO4.add("Keputusan go public tidak serta-merta akan mengubah kebijakan dan strategi bisnis yang telah diputuskan. Berbagai kebijakan yang telah berhasil diimplementasikan dengan baik selama ini akan terus dilaksanakan. Sinarmas MSIG Life akan tetap menghormati kesepakatan bisnis yang telah dijalin dengan mitra bisnis. Demikian pula dengan para nasabah, Sinarmas MSIG Life akan terus menjunjung tinggi komitmen untuk memenuhi kewajiban kepada pemegang polis dengan sebaik-baiknya.");

        List<String> answerNumberIPO5 = new ArrayList<>();
        answerNumberIPO5.add("Tidak ada prosedur baik prosedur klaim dan/atau hal lain yang berhubungan dengan kontrak akan berubah karena go public. Perusahaan tetap komitmen untuk melayani dan menempatkan kepuasan nasabah sebagai prioritas utama.");

        List<String> answerNumber1 = new ArrayList<>();
        answerNumber1.add("Click “request login”, dan masukan kode Agen Anda, password terbaru akan dikirimkan ke email corporate Anda (namaanda@sinarmasmsiglife.co.id).");


        List<String> answerNumber2 = new ArrayList<>();
        answerNumber2.add("Click 'Lanjut', untuk meneruskan dengan offline mode.\n" +
                "Catatan: Pada offline mode semua fitur aplikasi tetap dapat digunakan, kecuali untuk menampilkan PDF proposal, Submit SPAJ & Update CRM");


        List<String> answerNumber3 = new ArrayList<>();
        answerNumber3.add("1. Tambah leads\t-> Setiap kali lakukan tambah leads aktivitas akan otomatis bertambah\n" +
                "2. Tambah aktivitas\t-> Input presentasi, belum ada keputusan/no, beserta deskripsinya\n" +
                "3. SPAJ Submit\t-> Setiap Anda melakukan SPAJ submit aktivitas tidak akan bertambah namun akan berikan ‘Presentasi Yes’ dan menambah rasio kesuksesan.");

        String answerNumber4Hint = "Perhitungan rasio kesuksesan adalah :\t(Jumlah SPAJ Submit/Jumlah Aktivitas) x 100";
        List<String> answerNumber4 = new ArrayList<>();
        answerNumber4.add(answerNumber4Hint);


        List<String> answerNumber5 = new ArrayList<>();
        answerNumber5.add("Kembali cek jaringan pada perangkat Anda, input aktivitas akan terupdate otomatis pada saat jaringan internet online dan stabil");


        List<String> answerNumber6 = new ArrayList<>();
        answerNumber6.add("Ya, file sudah otomatis terdownload pada device, Anda dapat mengakses file tersebut pada My Files / Document");


        List<String> answerNumber7 = new ArrayList<>();
        answerNumber7.add("PDF viewer yang sebaiknya di set sebagai default adalah Drive PDF Viewer (default viewer Samsung dari awal), karena pemrosesan ringan dan stabil");


        List<String> answerNumber8 = new ArrayList<>();
        answerNumber8.add("File PDF yang terdownload otomatis memiliki default nama ‘nomor proposal’, file PDF dapat di rename dengan cara menekan file PDF tersebut selama 2 detik. File ini dapat Anda temukan pada My Files");

                List<String> answerNumber9 = new ArrayList<>();
        answerNumber9.add("Proposal tidak layak tidak akan bisa untuk melanjutkan proses ‘Buat SPAJ’, Bancassurance Consultant diharapkan untuk dapat mambantu lakukan perubahan ilustrasi dengan cara edit proposal untuk menaikan premi atau turunkan uang pertanggungan agar proposal menjadi layak");


        List<String> answerNumber10 = new ArrayList<>();
        answerNumber10.add("Penulisan nomor hanya angka saja, tidak perlu gunakan spasi, tanda ” - “ ataupun tanda “ . “");

        List<String> answerNumber11 = new ArrayList<>();
        answerNumber11.add("Masukan keyword dari pekerjaan yang dicari, dan tekan icon cari yang terdapat pada keyboard perangkat Anda");


        List<String> answerNumber12 = new ArrayList<>();
        answerNumber12.add("Coba cek pada bagian “Jenis Asuransi Tambahan”, klik 2x pada produk rider yang dipilih, dan kembali cek kelengkapan pada setiap fields yang ada.");


        List<String> answerNumber13 = new ArrayList<>();
        answerNumber13.add("Cek pada fields nomor rekening , pastikan jumlah digit sudah sesuai dan kembali cek pada bagian penerima manfaat, pastikan total manfaat dari ahli waris harus 100%");


        List<String> answerNumber14 = new ArrayList<>();
        answerNumber14.add("Kembali cek pada bagian halaman Detil Agen, dan pastikan kode & nama referral yang diinput benar, dan tersedia");


        List<String> answerNumber15 = new ArrayList<>();
        answerNumber15.add("Ya, ID & Nama Referral sebaiknya dikonfirmasikan kepada Bancassurance Distribution Support & ASH bersangkutan terlebih dahulu, harus dipastikan bahwa data referral tersedia di perusahaan. Jika tidak tersedia, Submission SPAJ akan gagal.");

        List<String> answerNumber16 = new ArrayList<>();
        answerNumber16.add("Pastikan jika ada kolom isian “keterangan” dari kelanjutan pilihan jawaban, juga wajib diisi");

        List<String> answerNumber17 = new ArrayList<>();
        answerNumber17.add("Kondisi ini muncul jika SPAJ sudah submit dan diterima BAS namun belum pada dokumen (foto) pendukung \n" +
                "Jika ditemukan text merah tersebut pada SPAJ yang sudah submit harap tidak dibiarkan, jika dibiarkan SPAJ akan pending karna team BAS sebagai penerima submission menganggap dokumen tidak lengkap\n");

        String answerNumber18Hint = "1. Produk Link\n\n"
                .concat("\t - Bukti Identitas Pemegang Polis\n")
                .concat("\t - Bukti Identitas Tertanggung\n")
                .concat("\t - Bukti Setor Bank\n")
                .concat("\t - Surat Kuasa Debet\n")
                .concat("\t - Copy cover buku tabungan/account statement\n")
                .concat("\t - Surat Pernyataan/Surat Provider (jika ada)\n")
                .concat("\n\n")
                .concat("2. Produk Non Link\n\n")
                .concat("\t - Bukti Identitas Pemegang Polis\n")
                .concat("\t - Bukti Identitas Tertanggung\n")
                .concat("\t - Bukti Setor Bank\n")
                .concat("\t - Surat Kuasa Debet\n")
                .concat("\t - Copy cover buku tabungan/account statement\n");

        List<String> answerNumber18 = new ArrayList<>();
        answerNumber18.add(answerNumber18Hint);

        String answerNumber19Hint = "- SKDR (Surat Kuasa Pendebetan Rekening)\n"
                .concat("- Surat Pernyataan Nasabah (jika ada)\n")
                .concat("- Surat provider (jika ada)\n");
        List<String> answerNumber19 = new ArrayList<>();
        answerNumber19.add(answerNumber19Hint);

        String answerNumber20Hint = "PT Asuransi Jiwa Sinarmas MSIG Tbk.\n"
                .concat("Document Center\n")
                .concat("Wisma Eka Jiwa Lt 9\n")
                .concat("Jln Mangga Dua Raya, Jakarta\n")
                .concat("UP. Nurfatiah Document Center\n");
        List<String> answerNumber20 = new ArrayList<>();
        answerNumber20.add(answerNumber20Hint);

        List<String> answerNumber21 = new ArrayList<>();
        answerNumber21.add("Ada, dokumen dapat dititipkan ke cabang PT Asuransi Jiwa Sinarmas MSIG Tbk. terdekat, di kota BC berada untuk dilanjutkan pengiriman ke Document Center pusat.");

        List<String> answerNumber22 = new ArrayList<>();
        answerNumber22.add("-\tFitur dapat diakses dari halaman kesimpulan proposal, dengan Klik icon tiga titik vertical di pojok kanan atas, dan pilih “Tarik Virtual Account”. \n" +
                "-\tDefault Stok dari penyimpanan VA adalah 5 no.VA per produk, yang akan berkurang setiap kali penggunaan dan akan terisi penuh ketika kembali lakukan tarik VA pada saat stok habis \n" +
                "-\tHal ini wajib digunakan sebelum pengisian E-SPAJ, dengan klik “salin” maka nomor VA otomatis tercopy sebagai clipboard sehingga memudahkan user untuk bagikan ke nasabah.\n");

        List<String> answerNumber23 = new ArrayList<>();
        answerNumber23.add("-\tDengan fitur ini user dapat menarik /retrieve no.VA yang kemudian digunakan nasabah untuk melakukan pembayaran premi. \n" +
                "-\tFitur ini hanya memerlukan koneksi internet jika ditemukan stok VA kosong, dan system akan otomatis menarik 5 nomor VA dengan rincian; 1 untuk digunakan dan  4 VA yang tersisa menjadi default stok simpanan. Sehingga untuk 4 penarikan VA selanjutnya dapat dilakukan tanpa koneksi internet.\n" +
                "-\tStok nomor VA per produk akan berkurang setiap kali penggunaan dan akan terisi penuh ketika kembali lakukan tarik VA pada saat stok habis\n");

        List<String> answerNumber24 = new ArrayList<>();
        answerNumber24.add("-\tDapat diakses dari halaman kesimpulan proposal,kemiudian klik  icon tiga titik vertical di pojok kanan dan pilih “Validasi Virtual Account”, akan tampil kolom untuk Input nomor VA. \n" +
                "-\tUntuk menggunakan user dapat mengInput nomor VA kemudian setujui checklist “Gunakan VA ini ketika nomor diatas valid”, kemudian klik “PERIKSA”.\n" +
                "-\tJika nomor VA valid maka dapat langsung digunakan, dan akan otomatis attached dengan E-SPAJ yang akan diisi, kemudian user dapat dilanjutkan ke bagian pengisian informasi nasabah pada bagianSPAJ.\n");

        List<String> answerNumber25 = new ArrayList<>();
        answerNumber25.add("-\tCara ini digunakan jika nasabah telah mengisi SPAJ kertas (hardcopy), dan diperlukan menyalin SPAJ tersebut ke E-SPAJ, dimana nasabah telah mencatat VA yang tertera pada kertas\n" +
                "-\tFitur ini membutuhkan koneksi internet untuk mengaksesnya, karna sistem perlu menValidasikan apakah nomor VA dari SPAJ kertas yang dinput masih dapat digunakan. \n");

        List<String> answerNumber26 = new ArrayList<>();
        answerNumber26.add("Pada bagian SPAJ halaman Pemegang Polis (1/9), terdapat 3 data yang diwajibkan terisi dan tersimpan terlebih dahulu agar User dapat menSkip pengisian SPAJ langsung ke halaman yang dituju, yaitu; \ta.Deskripsi Pekerjaan\n" +
                "b.\tAlamat Rumah\n" +
                "c.\tno handphone\n" +
                "d.\tkemudian di save \n");

        List<String> answerNumber27 = new ArrayList<>();
        answerNumber27.add("Untuk ketidaklengkapan nama referral yang tersedia, harap diinformasikan ke alamat email:\n" +
                "DistributionSupport@sinarmasmsiglife.co.id, dengan format “Tambah  Referral” dan sertakan Channel Bank,  Nama, NIK dan Cabang dari karyawan  bank / referral yang dimaksud  ingin disertakan.\n");

        List<String> answerNumber28 = new ArrayList<>();
        answerNumber28.add("Untuk ketidaklengkapan data Cabang Bank, dapat ajukan permohonan penambahan Cabang ke bagian finance dengan alamat email:  Finance@sinarmasmsiglife.co.id, Sertakan Cabang Pembantu yang dimaksud, alamat Cabang, dan struktur dari kantor cabang terkait jika ada.");


        List<String> answerNumber29 = new ArrayList<>();
        answerNumber29.add("a. Tambah leads : Setiap kali lakukan tambah leads Aktivitas akan otomatis bertambah \n" +
                "b. Tambah aktivitas : Input presentasi; Belum ada Keputusan / No, beserta deskripsi\n" +
                "c. SPAJ Submit : Setiap SPAJ submit tidak menambah aktivitas namun akan berikan ‘presentasi yes’ dan menambah rasio kesuksesan.\n");

        List<String> answerNumber30 = new ArrayList<>();
        answerNumber30.add("Dalam pembuatan aktivitas, beberapa diantaranya sudah terotomatisasi, yaitu:\n" +
                "a.\t“Terima Nasabah“, akan otomatis terbuat setiapkali user menambahkan Lead.\n" +
                "b.\t“Nasabah Tertarik (buat proposal)”, akan otomatis terbuat setiapkali user menawarkan proposal hingga ke penulisan SPAJ, namun aktivitas ini juga dapat diinput secara manual.\n" +
                "c.\t“Closing“, akan otomatis terbuat setiapkali user menyelesaikan dan telah mengirimkan SPAJ.\n");

        List<String> answerNumber31 = new ArrayList<>();
        answerNumber31.add("Manual Input Aktivitas “Closing”. Aktivitas “Closing” diinput secara manual, untuk memasukan point Closing dari SPAJ kertras, dengan cara pilih aktivitas “Kunjungan dan Presentasi”, kemudian pilih hasil aktivitas “Closing” dan input no.SPAJ kertas yang sudah dikirim ke SIV. Regulasi Aktivitas\n" +
                "Aktivitas terdiri dari “Menghubungi Nasabah” dan “Kunjungan Presentasi”, beserta hasil aktivitasnya yang wajib dipilih. Aktifitas dipilih berdasarkan kegiatan yang telah dilakukan user, setiap aktivitas akan menambahkan 1 point.\n");

        listDataChild.put(listDataHeader.get(0), answerNumberIPO1); // Header, Child data
        listDataChild.put(listDataHeader.get(1), answerNumberIPO2);
        listDataChild.put(listDataHeader.get(2), answerNumberIPO3);
        listDataChild.put(listDataHeader.get(3), answerNumberIPO4);
        listDataChild.put(listDataHeader.get(4), answerNumberIPO5);

        listDataChild.put(listDataHeader.get(5), answerNumber1);
        listDataChild.put(listDataHeader.get(6), answerNumber2);
        listDataChild.put(listDataHeader.get(7), answerNumber3);
        listDataChild.put(listDataHeader.get(8), answerNumber4);
        listDataChild.put(listDataHeader.get(9), answerNumber5);
        listDataChild.put(listDataHeader.get(10), answerNumber6);
        listDataChild.put(listDataHeader.get(11), answerNumber7);
        listDataChild.put(listDataHeader.get(12), answerNumber8);
        listDataChild.put(listDataHeader.get(13), answerNumber9);
        listDataChild.put(listDataHeader.get(14), answerNumber10);
        listDataChild.put(listDataHeader.get(15), answerNumber11);
        listDataChild.put(listDataHeader.get(16), answerNumber12);
        listDataChild.put(listDataHeader.get(17), answerNumber13);
        listDataChild.put(listDataHeader.get(18), answerNumber14);
        listDataChild.put(listDataHeader.get(19), answerNumber15);
        listDataChild.put(listDataHeader.get(20), answerNumber16);
        listDataChild.put(listDataHeader.get(21), answerNumber17);
        listDataChild.put(listDataHeader.get(22), answerNumber18);
        listDataChild.put(listDataHeader.get(23), answerNumber19);
        listDataChild.put(listDataHeader.get(24), answerNumber20);
        listDataChild.put(listDataHeader.get(25), answerNumber21);
        listDataChild.put(listDataHeader.get(26), answerNumber22);
        listDataChild.put(listDataHeader.get(27), answerNumber23);
        listDataChild.put(listDataHeader.get(28), answerNumber24);
        listDataChild.put(listDataHeader.get(29), answerNumber25);
        listDataChild.put(listDataHeader.get(30), answerNumber26);
        listDataChild.put(listDataHeader.get(31), answerNumber27);
        listDataChild.put(listDataHeader.get(32), answerNumber28);
        listDataChild.put(listDataHeader.get(33), answerNumber29);
        listDataChild.put(listDataHeader.get(34), answerNumber30);
        listDataChild.put(listDataHeader.get(35), answerNumber31);
    }

    @Override
    public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
        return false;
    }

    @Override
    public void onGroupCollapse(int i) {

    }

    @Override
    public void onGroupExpand(int i) {

    }
}
