package id.co.ajsmsig.nb.prop.fragment;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.w3c.dom.Text;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.method.async.CrmRestClientUsage;
import id.co.ajsmsig.nb.crm.method.async.ProposalApiRestClientUsage;
import id.co.ajsmsig.nb.crm.method.async.SpajApiRestClientUsage;
import id.co.ajsmsig.nb.crm.model.hardcode.RequestCode;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.method.DrawingView;
import id.co.ajsmsig.nb.espaj.method.GetTime;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;
import id.co.ajsmsig.nb.prop.activity.P_IllustrationActivity;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.database.P_Update;
import id.co.ajsmsig.nb.prop.method.P_StaticMethods;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.method.ValueView;
import id.co.ajsmsig.nb.prop.method.calculateIlustration.IlustrasionValue;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductRiderModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductTopUpModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductUlinkModel;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.IllustrationResultVO;
import id.co.ajsmsig.nb.util.Const;

import static id.co.ajsmsig.nb.prop.model.hardCode.P_FragmentPagePosition.DATA_PROPOSAL_PAGE;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FragmentPagePosition.RESULT_PAGE;

/**
 * Created by faiz_f on 03/02/2017.
 */

public class P_ResultFragment extends Fragment implements View.OnClickListener, ProposalApiRestClientUsage.UploadProposalListener, CrmRestClientUsage.UploadCrmListener, ProposalApiRestClientUsage.DownloadPdfProposalListener{
    private static final String TAG = P_ResultFragment.class.getSimpleName();

    private ArrayList<ModelDropDownString> ListDropDownSPAJ;
    private DrawingView drawView;
    private File file_ttd;
    private int count_ttd = 0;

    private P_ProposalModel proposalModel;
    private P_MstProposalProductModel productModel;
    private P_MstDataProposalModel dataProposalModel;
    private ArrayList<P_MstProposalProductRiderModel> riderModels;
    private ArrayList<P_MstProposalProductTopUpModel> topUpModels;
    private ArrayList<P_MstProposalProductUlinkModel> ulinkModels;
    private ViewIlustrasi viewIlustrasi;
    private P_ListImageManfaatFragment listImageManfaatFragment;
    //    private SendProposal sendProposal;
//    private OpenPdfProposal openPdfProposal;
//    private SendAndPullPdfProposal sendAndPullPdfProposal;
    private boolean isClick = true;
    private ProgressDialog dialogOpenPdfProposal;
    private int lsgbId;
    private int GROUP_ID = 0;
    private int jenisLogin = 0;
    private String Premi;
    private int jenisLoginBC;
    private int roleId = 0;
    private String kodeRegional = "";
    private static final int REQUEST_CODE_OPEN_OFFLINE_ILLUSTRATION = 100;
    private static final int REQUEST_CODE_DONT_OPEN_OFFLINE_ILLUSTRATION = 200;
    private static final int REQUEST_CODE_VALIDATE_VA = 300;

//    private boolean isSendProposal;

    private SpajApiRestClientUsage ewsRestClientUsage;
    private CrmRestClientUsage crmRestClientUsage;
    private ProposalApiRestClientUsage eproposalRestClientUsage;
    private String kodeAgen;
    private ProgressDialog progressDialog;
    private AlertDialog validateVaDialogUSD;

    public P_ResultFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onResume() {
        super.onResume();
        isClick = true;
        if (dialogOpenPdfProposal != null)
            dialogOpenPdfProposal.cancel();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.p_fragment_result, container, false);
        ButterKnife.bind(this, view);
        ((P_MainActivity) getActivity()).setSecondToolbarTitle(getString(R.string.fragment_title_hasil_proposal));

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
        kodeRegional = sharedPreferences.getString("KODE_REGIONAL", "");
        jenisLogin = sharedPreferences.getInt("JENIS_LOGIN", 0);
        GROUP_ID = sharedPreferences.getInt("GROUP_ID", 0);
        kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
        jenisLoginBC = sharedPreferences.getInt("JENIS_LOGIN_BC", 0);
        roleId = sharedPreferences.getInt("ROLE_ID", 0);

        ewsRestClientUsage = new SpajApiRestClientUsage(getActivity());
        eproposalRestClientUsage = new ProposalApiRestClientUsage(getActivity());
        eproposalRestClientUsage.setOnUploadProposalListener(this);
        crmRestClientUsage = new CrmRestClientUsage(getActivity());
        crmRestClientUsage.setOnUploadCrmListener(this);
        eproposalRestClientUsage.setOnDownloadPDFProposalListener(this);

        proposalModel = P_MainActivity.myBundle.getParcelable(getString(R.string.proposal_model));
        assert proposalModel != null;
        productModel = proposalModel.getMst_proposal_product();
        dataProposalModel = proposalModel.getMst_data_proposal();
        riderModels = proposalModel.getMst_proposal_product_rider();
        ulinkModels = proposalModel.getMst_proposal_product_ulink();
        topUpModels = proposalModel.getMst_proposal_product_topup();
        lsgbId = new P_Select(getContext()).selectLsgbId(productModel.getLsbs_id());

        /*update lastPagePosition*/
        ContentValues cvLastPosition = new ContentValues();
        cvLastPosition.put(getString(R.string.LAST_PAGE_POSITION), RESULT_PAGE);
        QueryUtil queryUtil = new QueryUtil(getContext());
        queryUtil.update(
                getString(R.string.MST_DATA_PROPOSAL),
                cvLastPosition,
                getString(R.string._ID) + " = ?",
                new String[]{String.valueOf(dataProposalModel.getId())}
        );

//        isSendProposal = dataProposalModel.getFlag_aktif() == 0;
//        btn_buat_spaj.setText((isSendProposal) ? getString(R.string.kirim) : getString(R.string.open_pdf));


        initiateView();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_finish, menu);
    }

    public void initiateView() {
        ll_ilustrasi.setOnClickListener(this);
        ll_pdf_proposal.setOnClickListener(this);
        ll_kirim_pdf.setOnClickListener(this);

        if (jenisLogin == 2) {
            if (GROUP_ID == 40) {
                ll_ilustrasi.setVisibility(View.GONE);
                ll_pdf_proposal.setVisibility(View.GONE);
            }
        }


//        btn_buat_spaj.setOnClickListener(this);
//        btn_edit.setOnClickListener(this);

        Button btnEdit = getActivity().findViewById(R.id.btn_kembali);
        btnEdit.setText(getString(R.string.edit));
        btnEdit.setOnClickListener(this);

        Button btnBuatSpaj = getActivity().findViewById(R.id.btn_lanjut);
        btnBuatSpaj.setText(getString(R.string.buat_spaj));
        btnBuatSpaj.setOnClickListener(v -> onButtonBuatSPAJPressed());

        showViewRangkuman();
        showViewPilihanInvestasi();
//        showViewCalculation();

    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent id.co.ajsmsig.nb.data) {
//        // Check which request we're responding to
//        if (requestCode == RequestCode.CALCULATE_ILLUSTRATION) {
//            // Make sure the request was successful
//            if (resultCode == RESULT_OK) {
//                dataProposalModel.setFlag_proper(id.co.ajsmsig.nb.data.getIntExtra(getString(R.string.FLAG_PROPER), 0));
//                saveToMyBundle();
//                showViewRangkuman();
//            }
//        }
//    }

    private void setAdapterJudulTTD(AutoCompleteTextView auto) throws IOException, XmlPullParserException {
        MethodSupport.load_setDropXmlString(R.xml.e_judul_ttd, auto, getContext(), 0);
    }

    private void popupttd(final int counter3) {

        LayoutInflater inflater = (LayoutInflater) getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupSet = inflater.inflate(R.layout.e_popup_ttd, null);
        Button close = popupSet.findViewById(R.id.Cancel);
        Button set = popupSet.findViewById(R.id.save);
        drawView = popupSet.findViewById(R.id.drawing);

        final PopupWindow pw = new PopupWindow(popupSet, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        set.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                //save drawing
                android.app.AlertDialog.Builder saveDialog = new android.app.AlertDialog.Builder(getActivity());
                saveDialog.setTitle("Set Tanda Tangan");
                saveDialog.setMessage("Simpan tanda Tangan?");
                saveDialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        drawView.setDrawingCacheEnabled(true);
                        savedraw(counter3);
                        pw.dismiss();
                        showDrawingFile(counter3);

                    }
                });
                saveDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                saveDialog.show();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pw.dismiss();
            }
        });

        pw.showAtLocation(popupSet, Gravity.CENTER, 0, 0);
    }

    public void savedraw(int counter3) {
        drawView.setDrawingCacheEnabled(true);
        Bitmap bitmap = drawView.getDrawingCache();

//		File sdCard= Environment.getExternalStorageDirectory();
        file_ttd_dp[counter3] = new File(file_ttd, "ttd" + GetTime.getCurrentDate("yyyyMMddhhmmss") + ".PNG");
        FileOutputStream ostream = null;
        try {
            ostream = new FileOutputStream(file_ttd_dp[counter3]);
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, ostream);

        try {
            ostream.close();
        } catch (IOException e) {

            e.printStackTrace();
        }
        drawView.invalidate();
        drawView.setDrawingCacheEnabled(false);
    }

    private void showDrawingFile(int counter3) {
//		File f = new File( "/sdcard/data/data/id.co.ajsmsig.espaj/ESPAJ/spaj_ttd/"+GetTime.getCurrentDate("yyyyMMddhhmmss")+".jpg");
        System.out.println(file_ttd_dp[counter3]);
        if (file_ttd_dp[counter3].exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(file_ttd_dp[counter3].getAbsolutePath());
            ttd_dp[counter3].setImageBitmap(myBitmap);
        }

    }

    private void inflateAddTTD(final int count_ttd2, String file, String Ket, String nama, final int flag_default, String judul) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowTtd = inflater.inflate(R.layout.e_adapter_ttd, null);
        ttd_dp[count_ttd2] = rowTtd.findViewById(R.id.ttd_dp);
        flag_ttd_dp[count_ttd2] = flag_default;

        file_ttd_dp[count_ttd2] = new File(file);
        if (file_ttd_dp[count_ttd2].exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(file_ttd_dp[count_ttd2].getAbsolutePath());
            ttd_dp[count_ttd2].setImageBitmap(myBitmap);
            ttd_dp[count_ttd2].invalidate();
        }
        hapus_ttd_dp[count_ttd2] = rowTtd.findViewById(R.id.hapus_ttd_dp);
        tambah_ttd_dp[count_ttd2] = rowTtd.findViewById(R.id.tambah_ttd_dp);
        tv_nomor_ttd_dp[count_ttd2] = rowTtd.findViewById(R.id.tv_nomorttd_dp);
        tv_nomor_ttd_dp[count_ttd2].setText(String.valueOf(count_ttd2));
        tv_nomor_ttd_dp[count_ttd2].setVisibility(View.GONE);

        ac_judul_ttd_dp[count_ttd2] = rowTtd.findViewById(R.id.ac_judul_tt_dp);
        try {
            setAdapterJudulTTD(ac_judul_ttd_dp[count_ttd2]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        judul_ttd_dp[count_ttd2] = judul;
        ListDropDownSPAJ = MethodSupport.getXMLString(getContext(), R.xml.e_judul_ttd, 0);
        ac_judul_ttd_dp[count_ttd2].setText(Ket);
        if (flag_default == 1) {
            MethodSupport.active_view(0, ac_judul_ttd_dp[count_ttd2]);
        }

        nama_ttd_dp[count_ttd2] = rowTtd.findViewById(R.id.nama_ttd_dp);
        nama_ttd_dp[count_ttd2].setText(nama);
        if (flag_default == 1) {
            MethodSupport.active_view(0, nama_ttd_dp[count_ttd2]);
            MethodSupport.active_view(0, ac_judul_ttd_dp[count_ttd2]);
        }

        //setonclick
        ac_judul_ttd_dp[count_ttd2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ac_judul_ttd_dp[count_ttd2].setText("");
                ac_judul_ttd_dp[count_ttd2].showDropDown();
            }
        });
        ac_judul_ttd_dp[count_ttd2].setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasfocus) {
                if (hasfocus) ac_judul_ttd_dp[count_ttd2].setText("");
                ac_judul_ttd_dp[count_ttd2].showDropDown();
            }
        });

        ac_judul_ttd_dp[count_ttd2].setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                ModelDropDownString modelJudul = (ModelDropDownString) parent.getAdapter().getItem(i);
                judul_ttd_dp[count_ttd2] = modelJudul.getId();
            }
        });
        tambah_ttd_dp[count_ttd2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupttd(count_ttd2);
            }
        });
        hapus_ttd_dp[count_ttd2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag_default == 1) {
                    MethodSupport.Alert(getActivity(), getString(R.string.title_error), getString(R.string.msg_default_dok), 0);
                } else {
                    if (count_ttd != 1) {
                        android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(getActivity());
                        dialog.setTitle("KONFIRMASI. . .");
                        dialog.setMessage("Apakah Anda Ingin Menghapus Tanda Tangan ini? ");
                        dialog.setPositiveButton("YA", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                count_ttd--;
                                ((ViewGroup) rowTtd.getParent()).removeView(rowTtd);
                                if (count_ttd != 0) {
                                    tv_nomor_ttd_dp[count_ttd].setText(String.valueOf(count_ttd));
                                }
                            }
                        });
                        dialog.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    } else {
                        MethodSupport.Alert(getActivity(), getString(R.string.title_error), getString(R.string.msg_ttd_min), 0);
                    }
                }

            }
        });
//        cl_form_tanda_tangan_dp.addView(rowTtd, cl_form_tanda_tangan_dp.getChildCount() - 1);
    }

    private void showViewRangkuman() {
        P_Select select = new P_Select(getContext());

        DropboxModel dKurs = select.selectKurs(productModel.getLku_id());
        String kurs = dKurs.getLabel();
        double premitahunan = 1; //LSCB_ID = 3
        double premisemesteran = 0.525; // LSCB_ID = 2
        double premitriwulanan = 0.27; //LSCB_ID = 1
        double premibulanan = 0.1; //LSCB_ID = 6

        String[][] viewInfo = new String[16][];

        String noProposal = (dataProposalModel.getNo_proposal() == null) ? dataProposalModel.getNo_proposal_tab() : dataProposalModel.getNo_proposal();
        viewInfo[0] = new String[]{"No.proposal", noProposal};

        DropboxModel dRencanaSub = select.selectRencanaSub(productModel.getLsbs_id(), productModel.getLsdbs_number());
        viewInfo[1] = new String[]{getString(R.string.rencana_asuransi), dRencanaSub.getLabel()};

        String q_namaPP = dataProposalModel.getNama_pp();
        viewInfo[2] = new String[]{getString(R.string.Nama_Pemegang_Polis), q_namaPP};

        int q_umurPP = dataProposalModel.getUmur_pp();
        String umurPP = q_umurPP + " " + getString(R.string.tahun).toLowerCase();
        viewInfo[3] = new String[]{getString(R.string.Usia_Pemegang_Polis), umurPP};

        String q_namaTT = dataProposalModel.getNama_tt();
        viewInfo[4] = new String[]{getString(R.string.Nama_Tertanggung), q_namaTT};

        int q_umurTT = dataProposalModel.getUmur_tt();
        String umurTT = q_umurTT + " " + getString(R.string.tahun).toLowerCase();
        viewInfo[5] = new String[]{getString(R.string.Usia_Tertanggung), umurTT};


        if (dataProposalModel.getFlag_tt_calon_bayi() == 1) {
            for (int i = 0; i < riderModels.size(); i++) {
                BigDecimal q_premiSmileBaby = BigDecimal.valueOf(Long.parseLong(riderModels.get(i).getPremi()));
                BigDecimal q_premi = BigDecimal.valueOf(Long.parseLong(productModel.getPremi()));
                if (productModel.getCara_bayar() == 1) { //triwulanan
                    BigDecimal TotalBaby = (q_premiSmileBaby).multiply(BigDecimal.valueOf(premitriwulanan));
                    BigDecimal TotalPremi = q_premi.add(TotalBaby);
                    String totalPremiTahunan = kurs + StaticMethods.toMoney(TotalPremi);
                    viewInfo[6] = new String[]{getString(R.string.Total_Premi_one_line), totalPremiTahunan};
                } else if (productModel.getCara_bayar() == 2) { //semesteran
                    BigDecimal TotalBaby = (q_premiSmileBaby).multiply(BigDecimal.valueOf(premisemesteran));
                    BigDecimal TotalPremi = q_premi.add(TotalBaby);
                    String totalPremiTahunan = kurs + StaticMethods.toMoney(TotalPremi);
                    viewInfo[6] = new String[]{getString(R.string.Total_Premi_one_line), totalPremiTahunan};
                } else if (productModel.getCara_bayar() == 3) { //tahunan
                    BigDecimal TotalBaby = (q_premiSmileBaby).multiply(BigDecimal.valueOf(premitahunan));
                    BigDecimal TotalPremi = q_premi.add(TotalBaby);
                    String totalPremiTahunan = kurs + StaticMethods.toMoney(TotalPremi);
                    viewInfo[6] = new String[]{getString(R.string.Total_Premi_one_line), totalPremiTahunan};
                } else if (productModel.getCara_bayar() == 6) { //bulanan
                    BigDecimal TotalBaby = (q_premiSmileBaby).multiply(BigDecimal.valueOf(premibulanan));
                    BigDecimal TotalPremi = q_premi.add(TotalBaby);
                    String totalPremiTahunan = kurs + StaticMethods.toMoney(TotalPremi);
                    viewInfo[6] = new String[]{getString(R.string.Total_Premi_one_line), totalPremiTahunan};
                }
            }
        } else {
            String q_premi = productModel.getPremi();
            String totalPremiTahunan = kurs + StaticMethods.toMoney(q_premi);

            viewInfo[6] = new String[]{getString(R.string.Total_Premi_one_line), totalPremiTahunan};
        }

        String q_premiPokok = productModel.getPremi_pokok();
        if (q_premiPokok != null) {
//            String persenPremiPokok = "(" + productModel.getPremi_komb() + "%)";
            String premiPokokTahunan = kurs + StaticMethods.toMoney(q_premiPokok);
            viewInfo[7] = new String[]{getString(R.string.premi_pokok), premiPokokTahunan};
        }

        String q_premiTopup = productModel.getPremi_topup();
        if (q_premiTopup != null) {
//            String persenPremiTopup = "(" + String.valueOf(100 - productModel.getPremi_komb()) + "%)";
            String premiTopupTahunan = kurs + StaticMethods.toMoney(q_premiTopup);
            viewInfo[8] = new String[]{getString(R.string.premi_top_up), premiTopupTahunan};
        }

        //--Baru untuk Premi Smile Baby
        if (productModel.getLsbs_id() == 208 && dataProposalModel.getFlag_tt_calon_bayi() == 1) {
            for (int i = 0; i < riderModels.size(); i++) {
                BigDecimal q_premiSmileBaby = BigDecimal.valueOf(Long.parseLong(riderModels.get(i).getPremi()));

                if (productModel.getCara_bayar() == 1) {
                    BigDecimal TotalBaby = (q_premiSmileBaby).multiply(BigDecimal.valueOf(premitriwulanan));

                    if (TotalBaby != null) {
                        String premiSmileBaby = kurs + StaticMethods.toMoney(TotalBaby);
                        viewInfo[9] = new String[]{getString(R.string.premi_smile_baby), premiSmileBaby};
                    }
                } else if (productModel.getCara_bayar() == 2) {
                    BigDecimal TotalBaby = (q_premiSmileBaby).multiply(BigDecimal.valueOf(premisemesteran));

                    if (TotalBaby != null) {
                        String premiSmileBaby = kurs + StaticMethods.toMoney(TotalBaby);
                        viewInfo[9] = new String[]{getString(R.string.premi_smile_baby), premiSmileBaby};
                    }
                } else if (productModel.getCara_bayar() == 3) {
                    BigDecimal TotalBaby = (q_premiSmileBaby).multiply(BigDecimal.valueOf(premitahunan));

                    if (TotalBaby != null) {
                        String premiSmileBaby = kurs + StaticMethods.toMoney(TotalBaby);
                        viewInfo[9] = new String[]{getString(R.string.premi_smile_baby), premiSmileBaby};
                    }
                } else if (productModel.getCara_bayar() == 6) {
                    BigDecimal TotalBaby = (q_premiSmileBaby).multiply(BigDecimal.valueOf(premibulanan));

                    if (TotalBaby != null) {
                        String premiSmileBaby = kurs + StaticMethods.toMoney(TotalBaby);
                        viewInfo[9] = new String[]{getString(R.string.premi_smile_baby), premiSmileBaby};
                    }
                } else {
                    if (q_premiSmileBaby != null) {
                        String premiSmileBaby = kurs + StaticMethods.toMoney(q_premiSmileBaby);
                        viewInfo[9] = new String[]{getString(R.string.premi_smile_baby), premiSmileBaby};
                    }
                }
            }
        }
        Integer q_caraBayar = productModel.getCara_bayar();
        if (q_caraBayar != null) {
            DropboxModel dboxCaraBayar = select.selectCaraBayar(q_caraBayar);
            String caraBayar = dboxCaraBayar.getLabel();
            viewInfo[10] = new String[]{getString(R.string.cara_pembayaran), caraBayar};
        }

        String q_up = productModel.getUp();
        String up = kurs + StaticMethods.toMoney(q_up);
        viewInfo[11] = new String[]{getString(R.string.Uang_Pertanggungan), up};


        Integer q_thnMasaKontrak = productModel.getThn_masa_kontrak();
        if (q_thnMasaKontrak != null) {
            String masaPertanggungan = q_thnMasaKontrak + " " + getString(R.string.tahun).toLowerCase();
            viewInfo[12] = new String[]{getString(R.string.Masa_Pertanggungan), masaPertanggungan};
        }

        Integer q_masaPembayaranPremi = productModel.getThn_cuti_premi();
        if (q_masaPembayaranPremi != null) {
            String masaPembayaranPremi = q_masaPembayaranPremi + " " + getString(R.string.tahun).toLowerCase();
            viewInfo[13] = new String[]{getString(R.string.masa_pembayaran_premi), masaPembayaranPremi};
        }

        Integer q_thnLamaBayar = productModel.getThn_lama_bayar();
        if (q_thnLamaBayar != null) {
            String lamaBayar = q_thnLamaBayar + " " + getString(R.string.tahun).toLowerCase();
            viewInfo[14] = new String[]{getString(R.string.lama_pembayaran), lamaBayar};
        }

        if (lsgbId == 17) {
            Integer q_layak = proposalModel.getMst_data_proposal().getFlag_proper();
            Log.d(TAG, "showViewRangkuman: q_layak " + q_layak);
            if (q_layak != null) {
                String layak = "";
                switch (q_layak) {
                    case 1:
                        layak = "Proposal layak";
                        break;
                    case 2:
                        layak = "Proposal tidak layak";
                        break;
                }
                if (!layak.equals(""))
                    viewInfo[15] = new String[]{"Kelayakan", layak};
            }
        }


        new ValueView(getContext()).addToLinearLayout(ll_rangkuman, viewInfo);
    }

    private void showViewPilihanInvestasi() {
        if (ulinkModels.size() > 0) {
            tv_pilihan_investasi.setVisibility(View.VISIBLE);
            String[][] viewInfo = new String[ulinkModels.size()][];

            int counter = 0;
            for (P_MstProposalProductUlinkModel ulinkModel : ulinkModels) {
                DropboxModel infoInvest = new P_Select(getContext()).selectInfoInvest(ulinkModel.getLji_id());

                viewInfo[counter] = new String[]{infoInvest.getLabel(), String.valueOf(ulinkModel.getMdu_persen())};
                counter++;
            }

            new ValueView(getContext()).addToLinearLayout(ll_pilihan_investasi, viewInfo);
        }
    }


//    private void showViewCalculation() {
//        String noProposal = (dataProposalModel.getNo_proposal() == null) ? dataProposalModel.getNo_proposal_tab() : dataProposalModel.getNo_proposal();
//        P_Select select = new P_Select(getContext());
//        String q_premi = productModel.getPremi();
//        long q_premiPokok = productModel.getPremi_pokok();
//        long q_premiTopup = productModel.getPremi_topup();
//        String q_kurs = productModel.getLku_id();
//        int q_caraBayar = productModel.getCara_bayar();
//        int q_umurTT = dataProposalModel.getUmur_tt();
//        DropboxModel dKurs = select.selectKurs(productModel.getLku_id());
//        String kurs = dKurs.getLabel();
//
//        CalculateRidersRate calculateRidersRate = new CalculateRidersRate(getContext(), dataProposalModel.getNo_proposal_tab());
//
//
//        int riderSize = riderModels.size();
//        String premiPokokTahunan = StaticMethods.toMoney(q_premiPokok);
//        String premiTopupTahunan = StaticMethods.toMoney(q_premiTopup);
//        double n_alokasiBiayaAkuisisiPremiPokok = (q_caraBayar == 0) ? (q_premiPokok * 0.05) : (q_premiPokok * 0.8);
//        double n_biayaTopupBerkala = q_premiTopup * 0.05;
//
//        HashMap<String, Object> params = new HashMap<>();
//        params.put("no_proposal", noProposal);
//        params.put("liUsia", q_umurTT);
//
//        double n_biayaAsuransiPokokPerbulan = select.selectLdecCoi(params);
//
//        double n_biayaAdministrasiPerBulan = (q_kurs.equals("01")) ? 15000 : 2;
//
//        double n_totalAlokasiInvestasi = Double.valueOf(String.valueOf(BigDecimal.valueOf(q_premi - n_alokasiBiayaAkuisisiPremiPokok - n_biayaTopupBerkala - n_biayaAsuransiPokokPerbulan - n_biayaAdministrasiPerBulan)));
//
//
//        String[][] viewInfo = new String[10 + riderSize][];
////        viewInfo = new String[9][];
//
//        viewInfo[1] = new String[]{getString(R.string.Premi_Pokok_Tahunan), kurs + premiPokokTahunan};
//        viewInfo[2] = new String[]{getString(R.string.Alokasi_Biaya_Akuisisi_Premi_Pokok), kurs + StaticMethods.toMoney(n_alokasiBiayaAkuisisiPremiPokok)};
//        viewInfo[3] = new String[]{getString(R.string.Premi_Top_Up_Berkala_Tahunan), kurs + premiTopupTahunan};
//        viewInfo[4] = new String[]{getString(R.string.Biaya_Top_Up_Berkala_Tahunan), kurs + StaticMethods.toMoney(n_biayaTopupBerkala)};
//        viewInfo[5] = new String[]{getString(R.string.Biaya_Asuransi_Pokok_Perbulan), kurs + StaticMethods.toMoney(n_biayaAsuransiPokokPerbulan)};
//        viewInfo[6] = null;
//        double totalRider = 0;
//        if (riderSize > 0) {
//            int currentLoc = 6;
//            viewInfo[currentLoc] = new String[]{getString(R.string.Biaya_Asuransi_Tambahan_Perbulan), ""};
//            ArrayList<HashMap<String, Object>> hashMaps = calculateRidersRate.getRiderPriceTag(1);
//            for (HashMap<String, Object> hashMap : hashMaps) {
//                String nama = (String) hashMap.get("NAMA");
//                double ldecTotal = (Double) hashMap.get("LDECTOTAL");
//                totalRider += ldecTotal;
//                Log.d("TAG", "initiateView: nama = " + nama + " --nilai = " + ldecTotal);
//                currentLoc++;
//                viewInfo[currentLoc] = new String[]{nama, kurs + StaticMethods.toMoney(ldecTotal)};
//            }
//        }
//        viewInfo[viewInfo.length - 3] = new String[]{getString(R.string.Biaya_Administrasi_Perbulan), kurs + StaticMethods.toMoney(n_biayaAdministrasiPerBulan)};
//        int lengthPremiPokok = premiPokokTahunan.length();
//        StringBuilder stringBuilder = new StringBuilder();
//        for (int i = 0; i < lengthPremiPokok; i++) {
//            stringBuilder.append("-");
//        }
//        viewInfo[viewInfo.length - 2] = new String[]{"", stringBuilder.toString()};
//        viewInfo[viewInfo.length - 1] = new String[]{getString(R.string.Total_Alokasi_Investasi), kurs + StaticMethods.toMoney(n_totalAlokasiInvestasi - totalRider)};
//
//        new ValueView(getContext()).addToLinearLayout(ll_calculation, viewInfo);
//    }

    private void onLanjut() {
        boolean continueToSpaj = true;

        if (jenisLogin == 2) {
            if (GROUP_ID == 40) {
                continueToSpaj = true;
            } else {
                if (lsgbId == 17) {
                    if (dataProposalModel.getFlag_proper() == 0) {
                        showDialogWithOk("Masih belum ditentukan");
                        continueToSpaj = false;
                        isClick = true;
                        ll_ilustrasi.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));
                    } else if (dataProposalModel.getFlag_proper() == 2) {
                        showDialogWithOk("Maaf proposal ini tidak layak untuk dibuat SPAJ");
                        continueToSpaj = false;
                        isClick = true;
                    } else {
                        continueToSpaj = true;
                        Toast.makeText(getContext(), "Proposal Layak", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    continueToSpaj = true;
                }
            }
        } else {
            if (lsgbId == 17) {
                if (dataProposalModel.getFlag_proper() == 0) {
                    showDialogWithOk("Masih belum ditentukan");
                    continueToSpaj = false;
                    isClick = true;
                    ll_ilustrasi.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));
                } else if (dataProposalModel.getFlag_proper() == 2) {
                    showDialogWithOk("Maaf proposal ini tidak layak untuk dibuat SPAJ");
                    continueToSpaj = false;
                    isClick = true;
                } else {
                    continueToSpaj = true;
                    Toast.makeText(getContext(), "Proposal Layak", Toast.LENGTH_SHORT).show();
                }
            } else {
                continueToSpaj = true;
            }
        }

        if (continueToSpaj) {
            Intent intent = new Intent(getContext(), E_MainActivity.class);

            intent.putExtra(getString(R.string.idproposal), dataProposalModel.getNo_proposal());
            intent.putExtra(getString(R.string.idproposal_tab), dataProposalModel.getNo_proposal_tab());
            intent.putExtra(getString(R.string.noid_member), dataProposalModel.getNoid());
            intent.putExtra(Const.INTENT_KEY_VA_EXIST, isProposalAlreadyHasVA());
            intent.putExtra(Const.INTENT_KEY_VA_NUMBER, getSavedVirtualAccountNumber());
            intent.putExtra(Const.INTENT_KEY_PP_NAME, dataProposalModel.getNama_pp());
            intent.putExtra(Const.INTENT_KEY_LEAD_TAB_ID, dataProposalModel.getLead_tab_id());
            intent.putExtra(Const.INTENT_KEY_SHOULD_HIDE_TARIK_VA_MENU, shouldHideTarikVaMenuOnEspaj());

            String spajId = new E_Select(getContext()).getSpajIdFromNoProposalTab(dataProposalModel.getNo_proposal_tab());
            if (!spajId.equals("")) {
                intent.putExtra(getString(R.string.SPAJ_ID_TAB), spajId);
            }
            startActivity(intent);
        }
    }

    private void displayUSDPaymentGuide(boolean isUnitLinkProduct) {

        View view = getLayoutInflater().inflate(R.layout.dialog_validate_va_usd, null);
        Button btnOk = view.findViewById(R.id.btnOk);

        TextView tvHeaderUnitLink = view.findViewById(R.id.tvHeaderUnitLink);
        TextView tvContentUnitLink = view.findViewById(R.id.tvContentUnitLink);

        TextView tvHeaderNonUnitLink = view.findViewById(R.id.tvHeaderNonUnitLink);
        TextView tvContentNonUnitLink = view.findViewById(R.id.tvContentNonUnitLink);

        if (isUnitLinkProduct) {
            tvHeaderNonUnitLink.setVisibility(View.GONE);
            tvContentNonUnitLink.setVisibility(View.GONE);
        } else {
            tvHeaderUnitLink.setVisibility(View.GONE);
            tvContentUnitLink.setVisibility(View.GONE);
        }

        btnOk.setOnClickListener(v -> {
            validateVaDialogUSD.dismiss();
            onLanjut();
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pembayaran Premi");
        builder.setView(view);


        validateVaDialogUSD = builder.create();
        validateVaDialogUSD.show();
    }

    void showDialogWithOk(final String message) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        AlertDialog alertDialog;

//                alertDialogBuilder.setTitle(title);
//                    alertDialogBuilder.setMessage(getString(R.string.solusi_masalah_koneksi));
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();


    }

    private boolean isLinkProduct() {
        if (productModel != null) {
            int lsbsId = productModel.getLsbs_id();
            P_Select select = new P_Select(getActivity());
            int lsgbId = select.selectLsgbId(lsbsId);
            return lsgbId == 17;
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        if (isClick) {
            isClick = false;
            switch (v.getId()) {
                case R.id.ll_ilustrasi:
                    viewIlustrasi = new ViewIlustrasi(REQUEST_CODE_OPEN_OFFLINE_ILLUSTRATION);
                    viewIlustrasi.execute();
                    break;
                case R.id.ll_pdf_proposal:
                    if (StaticMethods.isNetworkAvailable(getContext())) {
                        startSendAndOpenPdfProposal();
                    } else {
                        showDialogNoInternetConnection();
                    }
                    break;
                case R.id.ll_kirim_pdf:
                    break;
                case R.id.btn_kembali:
                    onEditProposal();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Button "Buat SPAJ" click listener
     */
    private void onButtonBuatSPAJPressed() {
        if (jenisLogin == Const.SIAP2U_LOGIN_TYPE) {
            //Siap2U
            if (GROUP_ID == 40) {
                onLanjut();
            } else {
                if (StaticMethods.isNetworkAvailable(getActivity())) {

                    eproposalRestClientUsage.uploadProposalBuatSPAJ(dataProposalModel.getNo_proposal_tab());

                    if (dataProposalModel.getFlag_proper() == 0) {
                        viewIlustrasi = new ViewIlustrasi(REQUEST_CODE_VALIDATE_VA);
                        viewIlustrasi.execute();
                    } else {
                        if (jenisLoginBC == 0 && roleId ==1) {
                            checkIfProposalHasVa();
                        }else {
                            onLanjut();
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), "Tidak terkoneksi dengan internet. Mohon cek kembali koneksi internet Anda", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (jenisLogin == Const.BANCASSURANCE_LOGIN_TYPE) {

                if (dataProposalModel.getFlag_proper() == 0) {
                    viewIlustrasi = new ViewIlustrasi(REQUEST_CODE_VALIDATE_VA);
                    viewIlustrasi.execute();
                } else {

                    if (jenisLoginBC == Const.BANK_CODE_SINARMAS) {
                        checkShouldPayViaVirtualAccount();
                    } else if (jenisLoginBC == Const.BANK_CODE_BTN_SYARIAH) {
                        checkIfProposalHasVa();
                    } else {
                        //Allow user to input SPAJ if the BC is not from BSIM or BTNS
                        onLanjut();
                    }
                }

        }
    }

    private boolean shouldHideTarikVaMenuOnEspaj() {
        return isUsingUSDCurrency() && (isMagnaLink() || isPrimeLink());
    }

    /**
     * If the currency is dollar, user should pay via bank transfer
     * If the currency is rupiah, user should pay via virtual account
     */
    private void checkShouldPayViaVirtualAccount() {
        if (isUsingUSDCurrency() && (isMagnaLink() || isPrimeLink())) {
            //User is using USD currency, select magna or prime link, therefore allow him direcly navigate to SPAJ
            displayUSDPaymentGuide(isLinkProduct());
        } else {
            //User is not using USD currency
            checkIfProposalHasVa();
        }
    }

    /**
     * Check if the proposal already has a virtual account
     * Result :
     * Display alert dialog when proposal doesn't have a virtual account
     * Allow user go to SPAJ if the proposal have a virtual account
     */
    private void checkIfProposalHasVa() {

        if (!isProposalAlreadyHasVA()) {
            //Proposal haven't any virtual account
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.no_va_added)
                    .setMessage(R.string.no_va_pulled)
                    .setCancelable(false)
                    .setPositiveButton(R.string.ok, (dialog, which) -> dialog.dismiss())
                    .show();

        } else {
            //Proposal already has a virtual account
            onLanjut();
        }
    }

    private void onEditProposal() {
        P_Update update = new P_Update(getActivity());
        String spajId = new E_Select(getContext()).getSpajIdFromNoProposalTab(dataProposalModel.getNo_proposal_tab());

        boolean isBankSinarmas = jenisLoginBC == Const.BANK_CODE_SINARMAS || jenisLoginBC == Const.BANK_CODE_SINARMAS_SYARIAH;
        boolean isBankBtnSyariah = jenisLoginBC == Const.BANK_CODE_BTN_SYARIAH;
        boolean isAgencyProduct = jenisLoginBC == 0 && roleId == 1;
        boolean isProposalHasVa = isProposalAlreadyHasVA();

        if ( (isBankBtnSyariah || isBankSinarmas || isAgencyProduct) &&  isProposalHasVa) {
            showDialogCanNotEditProposalVA();

            return;
        }

        if (spajId.equals("")) {
            /*belum ada spaj*/
            /*update lastPagePosition*/

            //column _id from table
            long id = dataProposalModel.getId();
            update.updateProposalValueWith(id, DATA_PROPOSAL_PAGE);

            dataProposalModel.setLast_page_position(DATA_PROPOSAL_PAGE);
            dataProposalModel.setFlag_finish(0);
            dataProposalModel.setFlag_proper(0);
            saveToMyBundle();
            P_MainActivity.saveState(getContext(), false);

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            Fragment fragment = new P_DataProposalFragment();
            ((P_MainActivity) getActivity()).setFragment(fragment);
        } else {
            /*sudah ada spaj*/
            isClick = true;
            showDialogCanNotEditProposal();
        }


    }

    void showDialogCanNotEditProposal() {
        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(getContext());

        alertDialogBuilder.setTitle("Tidak Bisa Edit Proposal");
//                    alertDialogBuilder.setMessage(getString(R.string.solusi_masalah_koneksi));
        alertDialogBuilder.setMessage("Proposal telah menjadi SPAJ dan tidak bisa diedit");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                nothing here
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    void showDialogCanNotEditProposalVA() {
        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(getContext());

        alertDialogBuilder.setTitle("Tidak Bisa Edit Proposal");
//                    alertDialogBuilder.setMessage(getString(R.string.solusi_masalah_koneksi));
        alertDialogBuilder.setMessage("Proposal telah terdapat no VA dan tidak bisa diedit");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                nothing here
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    void showDialogNoInternetConnection() {
        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(getContext());

        alertDialogBuilder.setTitle("Tidak Terhubung Ke Internet ");
//                    alertDialogBuilder.setMessage(getString(R.string.solusi_masalah_koneksi));
        alertDialogBuilder.setMessage("Mohon nyalakan wifi atau mobile data anda terlebih dahulu");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void saveServerNoProposal(String noProposal) {
        dataProposalModel.setNo_proposal(noProposal);
        dataProposalModel.setFlag_aktif(1);
        productModel.setNo_proposal(noProposal);

        for (int i = 0; i < ulinkModels.size(); i++) {
            ulinkModels.get(i).setNo_proposal(noProposal);
        }

        for (int i = 0; i < topUpModels.size(); i++) {
            if (topUpModels.get(i).getSelected())
                topUpModels.get(i).setNo_proposal(noProposal);
        }

        for (int i = 0; i < riderModels.size(); i++) {
            riderModels.get(i).setNo_proposal(noProposal);
            if (riderModels.get(i).getP_mstProposalProductPesertaModels() != null) {
                for (int j = 0; j < riderModels.get(i).getP_mstProposalProductPesertaModels().size(); j++) {
                    riderModels.get(i).getP_mstProposalProductPesertaModels().get(j).setNo_proposal(noProposal);
                }
            }
        }
    }

    public void saveToMyBundle() {
        proposalModel.setMst_data_proposal(dataProposalModel);
        proposalModel.setMst_proposal_product(productModel);
        proposalModel.setMst_proposal_product_rider(riderModels);
        proposalModel.setMst_proposal_product_ulink(ulinkModels);
        proposalModel.setMst_proposal_product_topup(topUpModels);
        P_MainActivity.myBundle.putParcelable(getString(R.string.proposal_model), proposalModel);
    }

//    public void startSendProposal() {
//        sendProposal = new SendProposal();
//        sendProposal.execute(getString(R.string.url_get_no_proposal));
//    }

//    public void startOpenPdfProposal() {
//        openPdfProposal = new OpenPdfProposal();
//        openPdfProposal.execute();
//    }

    public void startSendAndOpenPdfProposal() {
        dialogOpenPdfProposal = new ProgressDialog(getActivity());
        dialogOpenPdfProposal.setMessage("Loading, please wait");
        dialogOpenPdfProposal.setTitle("Mengirim Proposal");
        dialogOpenPdfProposal.show();
        dialogOpenPdfProposal.setCancelable(false);

        /*TODO: hilangkan komen dibawah kalau server crm udah normal*/
//        if (dataProposalModel.getLead_id() == null) {
//            ArrayList<Long> leadTabIds = new ArrayList<>();
//            leadTabIds.add(dataProposalModel.getLead_tab_id());
//            crmRestClientUsage.uploadLead(leadTabIds);
//        } else {
        if (dataProposalModel.getFlag_finish() == null) dataProposalModel.setFlag_finish(0);
        if (dataProposalModel.getFlag_finish() == 0) {
            /*belum di update keserver*/
//                ewsRestClientUsage.performUploadProposal(proposalModel);
            eproposalRestClientUsage.uploadProposal(dataProposalModel.getNo_proposal_tab());
        } else {
            prosesBukaPdf();
            isClick = true;
        }
//        }

//        sendAndPullPdfProposal = new SendAndPullPdfProposal();
//        sendAndPullPdfProposal.execute(getString(R.string.url_get_no_proposal));
    }

    @Override
    public void onUploadCrmSuccess(Long leadId, String message, Boolean isUpdateLead) {
        Log.d(TAG, "onUploadCrmSuccess: success");
        if (isUpdateLead) {
            dataProposalModel.setLead_id(leadId);
            saveToMyBundle();
        }
//        ewsRestClientUsage.performUploadProposal(proposalModel);
        eproposalRestClientUsage.uploadProposal(dataProposalModel.getNo_proposal_tab());
        isClick = true;
    }

    @Override
    public void onUploadCrmFail(String message) {
        Toast.makeText(getContext(), "Upload Lead Fail with message : " + message, Toast.LENGTH_SHORT).show();
        dialogOpenPdfProposal.cancel();
        isClick = true;
    }


    @Override
    public void onUploadProposalSuccess(String noProposal, String message) {
        Log.d(TAG, "onUploadProposalSuccess: success");
        saveServerNoProposal(noProposal);
        dataProposalModel.setFlag_finish(1);
        saveToMyBundle();
        showViewRangkuman();
        File file = P_StaticMethods.getFileProposalPdf(getContext(), dataProposalModel.getNo_proposal());
        if (file.exists()) {
            file.delete();
        }
        prosesBukaPdf();
        isClick = true;
    }

    @Override
    public void onUploadProposalSuccessBuatSPAJ(String noProposal, String message) {
        Log.d(TAG, "onUploadProposalSuccess: success");
        saveServerNoProposal(noProposal);
        dataProposalModel.setFlag_finish(1);
        saveToMyBundle();
        showViewRangkuman();
        File file = P_StaticMethods.getFileProposalPdf(getContext(), dataProposalModel.getNo_proposal());
        if (file.exists()) {
            file.delete();
        }
        isClick = true;
    }

    @Override
    public void onUploadProposalFailure(String message) {
        Log.d(TAG, "onUploadProposalFailure: " + message);
        dialogOpenPdfProposal.cancel();
        isClick = true;
    }

    @Override
    public void onDownloadPDFProposalSuccess() {
        Log.d(TAG, "onDownloadPDFProposalSuccess: sucesss");
        dialogOpenPdfProposal.cancel();
    }

    @Override
    public void onDownloadPDFProposalFailure(String title, String message) {
        Log.d(TAG, "onDownloadPDFProposalFailure: " + message);
        dialogOpenPdfProposal.cancel();
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }


    public void prosesBukaPdf() {
        File pdfFile = P_StaticMethods.getFileProposalPdf(getContext(), dataProposalModel.getNo_proposal());

        if (pdfFile.exists()) {
            Log.d(TAG, "prosesBukaPdf: file exist");
            dialogOpenPdfProposal.cancel();
            StaticMethods.openPDF(getContext(), pdfFile);
            dialogOpenPdfProposal.cancel();
        } else {
            Log.d(TAG, "prosesBukaPdf: file not exist");
            eproposalRestClientUsage.getDownloadPdfProposal(dataProposalModel.getNo_proposal(), true);
        }
    }


    private ProgressDialog dialog;

    @Override
    public void onPause() {
        super.onPause();
        if (dialog != null)
            dialog.dismiss();
    }

    private class ViewIlustrasi extends AsyncTask<Void, Void, HashMap<String, Object>> {
        private int requestCode;

        ViewIlustrasi(int requestCode) {
            this.requestCode = requestCode;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Mohon Menunggu");
            dialog.setTitle("Sedang Melakukan Proses Perhitungan Layak atau Tidak");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected HashMap<String, Object> doInBackground(Void... params) {
            return new IlustrasionValue(getContext(), dataProposalModel.getNo_proposal_tab()).calculateIllustration();
        }

        @Override
        protected void onPostExecute(HashMap<String, Object> result) {
            viewIlustrasi = null;
            dialog.cancel();
            isClick = true;

            if (dataProposalModel.getFlag_proper() == 0) {
                IllustrationResultVO illustrationResult = (IllustrationResultVO) result.get(getString(R.string.Illustration1));
                String validityMsg = illustrationResult.getValidityMsg();

                String selection = getString(R.string.NO_PROPOSAL_TAB) + "=?";
                assert proposalModel != null;
                String[] selectionArgs = new String[]{dataProposalModel.getNo_proposal_tab()};
                ContentValues cvProper = new ContentValues();
                int proper;
                if (validityMsg != null) {
                    if (validityMsg.trim().length() != 0) {
                        /*gak layak*/
                        proper = 2;
                        dataProposalModel.setFlag_proper(proper);
                        cvProper.put(getString(R.string.FLAG_PROPER), proper);
                    } else {
                        /*layak*/
                        proper = 1;
                        dataProposalModel.setFlag_proper(proper);
                        cvProper.put(getString(R.string.FLAG_PROPER), proper);

                    }
                    int rowUpdateSize = new QueryUtil(getContext()).update(getString(R.string.MST_DATA_PROPOSAL), cvProper, selection, selectionArgs);
//                    Log.d(TAG, "onCreate: row update " + rowUpdateSize);
                    if (rowUpdateSize > 0) {
                        saveToMyBundle();
                        showViewRangkuman();
                    }
                }
            }

            switch (requestCode) {
                case REQUEST_CODE_OPEN_OFFLINE_ILLUSTRATION:
                    Intent intentToIlustrasi = new Intent(getContext(), P_IllustrationActivity.class);
                    Bundle sendBundle = new Bundle();
                    sendBundle.putParcelable(getString(R.string.proposal_model), proposalModel);
                    sendBundle.putString(getString(R.string.NO_PROPOSAL_TAB), proposalModel.getMst_data_proposal().getNo_proposal_tab());


//            sendBundle.putString(getString(R.string.VALIDITYMSG), (String) result.get(getString(R.string.VALIDITYMSG)));
                    sendBundle.putSerializable(getString(R.string.Illustrasi), result);
//            intentToIlustrasi.putExtra(MyContentProvider.DATA_PROPOSAL_CONTENT_ITEM_TYPE, uri);
                    intentToIlustrasi.putExtras(sendBundle);
                    startActivityForResult(intentToIlustrasi, RequestCode.CALCULATE_ILLUSTRATION);

                    break;
                case REQUEST_CODE_DONT_OPEN_OFFLINE_ILLUSTRATION:
                    onLanjut();
                    break;
                case REQUEST_CODE_VALIDATE_VA:
                    if (jenisLoginBC == Const.BANK_CODE_SINARMAS) {
                        checkShouldPayViaVirtualAccount();
                    } else if (jenisLoginBC == Const.BANK_CODE_BTN_SYARIAH) {
                        checkIfProposalHasVa();
                    } else if (jenisLoginBC == 0 && roleId == 1) {
                        checkIfProposalHasVa();
                    } else {
                        //Allow user to input SPAJ if the BC is not from BSIM or BTNS
                        onLanjut();
                    }
                    break;
            }

        }

        @Override
        protected void onCancelled() {
        }


    }

    /**
     * Check whether the loaded proposal has already pulled a virtual account
     * Action :
     * true : Disable product reselect. (Disable product spinner)
     * false : Allow product reselect
     */
    private boolean isProposalAlreadyHasVA() {
        P_Select p_select = new P_Select(getActivity());

        if (dataProposalModel != null) {

            String noProposalTab = dataProposalModel.getNo_proposal_tab();
            String vaNumber = p_select.getProposalVirtualAccount(noProposalTab);

            return !TextUtils.isEmpty(vaNumber);

        }

        return false;
    }

    /**
     * Return the proposal saved virtual account number
     *
     * @return
     */
    private String getSavedVirtualAccountNumber() {
        P_Select p_select = new P_Select(getActivity());

        if (dataProposalModel != null) {
            String noProposalTab = dataProposalModel.getNo_proposal_tab();
            String vaNumber = p_select.getProposalVirtualAccount(noProposalTab);
            return vaNumber;
        }

        return null;
    }
    private boolean isUsingUSDCurrency() {
        return productModel != null && productModel.getLku_id().equalsIgnoreCase("02");
    }

    private boolean isMagnaLink() {
        return productModel != null && productModel.getLsbs_id() == 213 && productModel.getLsdbs_number() == 1;
    }

    private boolean isPrimeLink() {
        return productModel != null && productModel.getLsbs_id() == 134 && productModel.getLsdbs_number() == 5;
    }

    @BindView(R.id.ll_ilustrasi)
    LinearLayout ll_ilustrasi;
    @BindView(R.id.ll_pdf_proposal)
    LinearLayout ll_pdf_proposal;
    @BindView(R.id.ll_kirim_pdf)
    LinearLayout ll_kirim_pdf;
    @BindView(R.id.ll_rangkuman)
    LinearLayout ll_rangkuman;
    @BindView(R.id.ll_calculation)
    LinearLayout ll_calculation;
    @BindView(R.id.ll_pilihan_investasi)
    LinearLayout ll_pilihan_investasi;
    @BindView(R.id.tv_pilihan_investasi)
    TextView tv_pilihan_investasi;

    private ImageView foto_dp1, foto_dp2, foto_dp3, foto_dp4, foto_dp5, foto_dp6, foto_dp7, foto_dp8, foto_dp9, foto_dp10, foto_dp11, foto_dp12, foto_dp13, foto_dp14, foto_dp15,
            foto_dp16, foto_dp17, foto_dp18, foto_dp19, foto_dp20, foto_dp21, foto_dp22, foto_dp23, foto_dp24, foto_dp25, foto_dp26,
            iv_menufoto_dp1, iv_menufoto_dp2, iv_menufoto_dp3, iv_menufoto_dp4, iv_menufoto_dp5, iv_menufoto_dp6, iv_menufoto_dp7, iv_menufoto_dp8, iv_menufoto_dp9, iv_menufoto_dp10, iv_menufoto_dp11, iv_menufoto_dp12, iv_menufoto_dp13, iv_menufoto_dp14, iv_menufoto_dp15,
            iv_menufoto_dp16, iv_menufoto_dp17, iv_menufoto_dp18, iv_menufoto_dp19, iv_menufoto_dp20, iv_menufoto_dp21, iv_menufoto_dp22, iv_menufoto_dp23, iv_menufoto_dp24, iv_menufoto_dp25, iv_menufoto_dp26,
            ttd_dp1, ttd_dp2, ttd_dp3, ttd_dp4, ttd_dp5, ttd_dp6, ttd_dp7, ttd_dp8, ttd_dp9, ttd_dp10, ttd_dp11, ttd_dp12, ttd_dp13, ttd_dp14, ttd_dp15;

    private ImageView[] ttd_dp = {ttd_dp1, ttd_dp2, ttd_dp3, ttd_dp4, ttd_dp5, ttd_dp6, ttd_dp7, ttd_dp8, ttd_dp9, ttd_dp10, ttd_dp11, ttd_dp12, ttd_dp13, ttd_dp14, ttd_dp15};

    private int flag_foto_dp1, flag_foto_dp2, flag_foto_dp3, flag_foto_dp4, flag_foto_dp5, flag_foto_dp6, flag_foto_dp7, flag_foto_dp8, flag_foto_dp9, flag_foto_dp10, flag_foto_dp11, flag_foto_dp12, flag_foto_dp13, flag_foto_dp14, flag_foto_dp15,
            flag_foto_dp16, flag_foto_dp17, flag_foto_dp18, flag_foto_dp19, flag_foto_dp20, flag_foto_dp21, flag_foto_dp22, flag_foto_dp23, flag_foto_dp24, flag_foto_dp25, flag_foto_dp26,
            flag_ttd_dp1, flag_ttd_dp2, flag_ttd_dp3, flag_ttd_dp4, flag_ttd_dp5, flag_ttd_dp6, flag_ttd_dp7, flag_ttd_dp8, flag_ttd_dp9, flag_ttd_dp10, flag_ttd_dp11, flag_ttd_dp12, flag_ttd_dp13, flag_ttd_dp14, flag_ttd_dp15;

    private int[] flag_ttd_dp = {flag_ttd_dp1, flag_ttd_dp2, flag_ttd_dp3, flag_ttd_dp4, flag_ttd_dp5, flag_ttd_dp6, flag_ttd_dp7, flag_ttd_dp8, flag_ttd_dp9, flag_ttd_dp10, flag_ttd_dp11, flag_ttd_dp12, flag_ttd_dp13, flag_ttd_dp14, flag_ttd_dp15};

    private File file_ttd_dp1, file_ttd_dp2, file_ttd_dp3, file_ttd_dp4, file_ttd_dp5, file_ttd_dp6, file_ttd_dp7, file_ttd_dp8, file_ttd_dp9, file_ttd_dp10, file_ttd_dp11, file_ttd_dp12, file_ttd_dp13, file_ttd_dp14, file_ttd_dp15;

    private File[] file_ttd_dp = {file_ttd_dp1, file_ttd_dp2, file_ttd_dp3, file_ttd_dp4, file_ttd_dp5, file_ttd_dp6, file_ttd_dp7, file_ttd_dp8, file_ttd_dp9, file_ttd_dp10, file_ttd_dp11, file_ttd_dp12, file_ttd_dp13, file_ttd_dp14, file_ttd_dp15};

    private TextView tv_nomor_foto_dp1, tv_nomor_foto_dp2, tv_nomor_foto_dp3, tv_nomor_foto_dp4, tv_nomor_foto_dp5, tv_nomor_foto_dp6, tv_nomor_foto_dp7, tv_nomor_foto_dp8, tv_nomor_foto_dp9, tv_nomor_foto_dp10, tv_nomor_foto_dp11, tv_nomor_foto_dp12, tv_nomor_foto_dp13, tv_nomor_foto_dp14, tv_nomor_foto_dp15,
            tv_nomor_foto_dp16, tv_nomor_foto_dp17, tv_nomor_foto_dp18, tv_nomor_foto_dp19, tv_nomor_foto_dp20, tv_nomor_foto_dp21, tv_nomor_foto_dp22, tv_nomor_foto_dp23, tv_nomor_foto_dp24, tv_nomor_foto_dp25, tv_nomor_foto_dp26,
            tv_nomor_ttd_dp1, tv_nomor_ttd_dp2, tv_nomor_ttd_dp3, tv_nomor_ttd_dp4, tv_nomor_ttd_dp5, tv_nomor_ttd_dp6, tv_nomor_ttd_dp7, tv_nomor_ttd_dp8, tv_nomor_ttd_dp9, tv_nomor_ttd_dp10, tv_nomor_ttd_dp11, tv_nomor_ttd_dp12, tv_nomor_ttd_dp13, tv_nomor_ttd_dp14, tv_nomor_ttd_dp15,
            hapus_ttd_dp1, hapus_ttd_dp2, hapus_ttd_dp3, hapus_ttd_dp4, hapus_ttd_dp5, hapus_ttd_dp6, hapus_ttd_dp7, hapus_ttd_dp8, hapus_ttd_dp9, hapus_ttd_dp10, hapus_ttd_dp11, hapus_ttd_dp12, hapus_ttd_dp13, hapus_ttd_dp14, hapus_ttd_dp15,
            tambah_ttd_dp1, tambah_ttd_dp2, tambah_ttd_dp3, tambah_ttd_dp4, tambah_ttd_dp5, tambah_ttd_dp6, tambah_ttd_dp7, tambah_ttd_dp8, tambah_ttd_dp9, tambah_ttd_dp10, tambah_ttd_dp11, tambah_ttd_dp12, tambah_ttd_dp13, tambah_ttd_dp14, tambah_ttd_dp15;

    private TextView[] tv_nomor_ttd_dp = {tv_nomor_ttd_dp1, tv_nomor_ttd_dp2, tv_nomor_ttd_dp3, tv_nomor_ttd_dp4, tv_nomor_ttd_dp5, tv_nomor_ttd_dp6, tv_nomor_ttd_dp7, tv_nomor_ttd_dp8, tv_nomor_ttd_dp9, tv_nomor_ttd_dp10, tv_nomor_ttd_dp11, tv_nomor_ttd_dp12, tv_nomor_ttd_dp13, tv_nomor_ttd_dp14, tv_nomor_ttd_dp15};

    private TextView[] hapus_ttd_dp = {hapus_ttd_dp1, hapus_ttd_dp2, hapus_ttd_dp3, hapus_ttd_dp4, hapus_ttd_dp5, hapus_ttd_dp6, hapus_ttd_dp7, hapus_ttd_dp8, hapus_ttd_dp9, hapus_ttd_dp10, hapus_ttd_dp11, hapus_ttd_dp12, hapus_ttd_dp13, hapus_ttd_dp14, hapus_ttd_dp15};

    private TextView[] tambah_ttd_dp = {tambah_ttd_dp1, tambah_ttd_dp2, tambah_ttd_dp3, tambah_ttd_dp4, tambah_ttd_dp5, tambah_ttd_dp6, tambah_ttd_dp7, tambah_ttd_dp8, tambah_ttd_dp9, tambah_ttd_dp10, tambah_ttd_dp11, tambah_ttd_dp12, tambah_ttd_dp13, tambah_ttd_dp14, tambah_ttd_dp15};

    private MaterialBetterSpinner ac_judul_foto_dp1, ac_judul_foto_dp2, ac_judul_foto_dp3, ac_judul_foto_dp4, ac_judul_foto_dp5, ac_judul_foto_dp6, ac_judul_foto_dp7, ac_judul_foto_dp8, ac_judul_foto_dp9, ac_judul_foto_dp10, ac_judul_foto_dp11, ac_judul_foto_dp12, ac_judul_foto_dp13, ac_judul_foto_dp14, ac_judul_foto_dp15,
            ac_judul_foto_dp16, ac_judul_foto_dp17, ac_judul_foto_dp18, ac_judul_foto_dp19, ac_judul_foto_dp20, ac_judul_foto_dp21, ac_judul_foto_dp22, ac_judul_foto_dp23, ac_judul_foto_dp24, ac_judul_foto_dp25, ac_judul_foto_dp26,
            ac_judul_ttd_dp1, ac_judul_ttd_dp2, ac_judul_ttd_dp3, ac_judul_ttd_dp4, ac_judul_ttd_dp5, ac_judul_ttd_dp6, ac_judul_ttd_dp7, ac_judul_ttd_dp8, ac_judul_ttd_dp9, ac_judul_ttd_dp10, ac_judul_ttd_dp11, ac_judul_ttd_dp12, ac_judul_ttd_dp13, ac_judul_ttd_dp14, ac_judul_ttd_dp15;

    private MaterialBetterSpinner[] ac_judul_ttd_dp = {ac_judul_ttd_dp1, ac_judul_ttd_dp2, ac_judul_ttd_dp3, ac_judul_ttd_dp4, ac_judul_ttd_dp5, ac_judul_ttd_dp6, ac_judul_ttd_dp7, ac_judul_ttd_dp8, ac_judul_ttd_dp9, ac_judul_ttd_dp10, ac_judul_ttd_dp11, ac_judul_ttd_dp12, ac_judul_ttd_dp13, ac_judul_ttd_dp14, ac_judul_ttd_dp15};

    private String judul_foto_dp1, judul_foto_dp2, judul_foto_dp3, judul_foto_dp4, judul_foto_dp5, judul_foto_dp6, judul_foto_dp7, judul_foto_dp8, judul_foto_dp9, judul_foto_dp10, judul_foto_dp11, judul_foto_dp12, judul_foto_dp13, judul_foto_dp14, judul_foto_dp15,
            judul_foto_dp16, judul_foto_dp17, judul_foto_dp18, judul_foto_dp19, judul_foto_dp20, judul_foto_dp21, judul_foto_dp22, judul_foto_dp23, judul_foto_dp24, judul_foto_dp25, judul_foto_dp26,
            judul_ttd_dp1, judul_ttd_dp2, judul_ttd_dp3, judul_ttd_dp4, judul_ttd_dp5, judul_ttd_dp6, judul_ttd_dp7, judul_ttd_dp8, judul_ttd_dp9, judul_ttd_dp10, judul_ttd_dp11, judul_ttd_dp12, judul_ttd_dp13, judul_ttd_dp14, judul_ttd_dp15;

    private String[] judul_ttd_dp = {judul_ttd_dp1, judul_ttd_dp2, judul_ttd_dp3, judul_ttd_dp4, judul_ttd_dp5, judul_ttd_dp6, judul_ttd_dp7, judul_ttd_dp8, judul_ttd_dp9, judul_ttd_dp10, judul_ttd_dp11, judul_ttd_dp12, judul_ttd_dp13, judul_ttd_dp14, judul_ttd_dp15};

    private EditText nama_ttd_dp1, nama_ttd_dp2, nama_ttd_dp3, nama_ttd_dp4, nama_ttd_dp5, nama_ttd_dp6, nama_ttd_dp7, nama_ttd_dp8, nama_ttd_dp9, nama_ttd_dp10, nama_ttd_dp11, nama_ttd_dp12, nama_ttd_dp13, nama_ttd_dp14, nama_ttd_dp15;

    private EditText[] nama_ttd_dp = {nama_ttd_dp1, nama_ttd_dp2, nama_ttd_dp3, nama_ttd_dp4, nama_ttd_dp5, nama_ttd_dp6, nama_ttd_dp7, nama_ttd_dp8, nama_ttd_dp9, nama_ttd_dp10, nama_ttd_dp11, nama_ttd_dp12, nama_ttd_dp13, nama_ttd_dp14, nama_ttd_dp15};

}
