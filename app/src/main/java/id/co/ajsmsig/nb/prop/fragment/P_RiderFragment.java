package id.co.ajsmsig.nb.prop.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.C_Insert;
import id.co.ajsmsig.nb.crm.method.AutoCompleteTextViewClickListener;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.form.SimbakActivityModel;
import id.co.ajsmsig.nb.crm.model.hardcode.RequestCode;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.prop.activity.P_KelasActivity;
import id.co.ajsmsig.nb.prop.activity.P_ListPesertaActivity;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.method.MoneyTextWatcher;
import id.co.ajsmsig.nb.prop.method.P_StaticMethods;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductPesertaModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductRiderModel;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;
import id.co.ajsmsig.nb.prop.model.hardCode.P_InsuredAgeFromFlag;
import id.co.ajsmsig.nb.util.DateUtils;

import static android.app.Activity.RESULT_OK;
import static id.co.ajsmsig.nb.prop.method.util.StringUtil.addNewLineToSBuilderIfFilled;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FragmentPagePosition.DATA_PROPOSAL_PAGE;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_BABY;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_CI;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_EARLY_STAGE_CI99;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_EKA_SEHAT;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_EKA_SEHAT_INNER_LIMIT;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_HCP;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_HCP_FAMILY;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_HCP_LADIES;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_HCP_PROVIDER;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_LADIES_INSURANCE;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_LADIES_MED_EXPENSE;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_LADIES_MED_EXPENSE_INNER_LIMIT;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_MEDICAL_EXTRA;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_MEDICAL_PLUS_AC;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_MEDICAL_PLUS_IL;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_PA;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_PAYOR_CI;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_PAYOR_TPD_CI_DEATH;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_PAYOR_TPD_DEATH;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_SCHOLARSHIP;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_TERM;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_TPD;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_WAIVER_CI;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_WAIVER_TPD;
import static id.co.ajsmsig.nb.prop.model.hardCode.Prop_Model_RiderLsbsId.RIDER_WAIVER_TPD_CI;


/*
 Created by faiz_f on 03/02/2017.
 */

public class P_RiderFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, AdapterView.OnItemClickListener, CompoundButton.OnCheckedChangeListener {
    private static final String TAG = "P_RiderFragment";

    private P_ProposalModel proposalModel;
    private P_MstProposalProductModel mstProposalProductModel;
    private P_MstDataProposalModel mstDataProposalModel;
    private ArrayList<P_MstProposalProductRiderModel> mstProposalProductRiderModels;
    private SparseArray<P_MstProposalProductRiderModel> dataRider = new SparseArray<>();
    private SparseArray<ArrayList<P_MstProposalProductPesertaModel>> dataPeserta = new SparseArray<>();
    private int psetId;
    private P_Select select;
    private ArrayList<HashMap<String, Integer>> listRiderView;
    private Integer thnLamaBayar;
    private Integer thnCutiPremi;
    private boolean samePerson;
    private boolean isSaveToBundle = true;
    private boolean isCreateProposalActivity = false;
    private long SLP_TAB_ID, SL_TAB_ID;

    private DropboxModel dbPA = new DropboxModel();
    private DropboxModel dbHPF = new DropboxModel();
    private DropboxModel dbHP = new DropboxModel();
    private DropboxModel dbHPP = new DropboxModel();
    private DropboxModel dbMBAC = new DropboxModel();
    private DropboxModel dbMBIL = new DropboxModel();
    private DropboxModel dbLHP = new DropboxModel();
    private DropboxModel dbLI = new DropboxModel();
    private DropboxModel dbLMBAC = new DropboxModel();
    private DropboxModel dbLMBIL = new DropboxModel();
    private DropboxModel dbCIPercent = new DropboxModel();
    private DropboxModel dbLIPercent = new DropboxModel();
    private DropboxModel dbBabyPlan = new DropboxModel();
    private DropboxModel dbBabyBulanKe = new DropboxModel();
    private DropboxModel dbESPercent = new DropboxModel();
    private DropboxModel dbWaiverTPDCI = new DropboxModel();
    private DropboxModel dbMPBAC = new DropboxModel();
    private DropboxModel dbMPBIL = new DropboxModel();
    private DropboxModel dbME = new DropboxModel();


    public P_RiderFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        Bundle bundle = getActivity().getIntent().getExtras();

        SL_TAB_ID = bundle.getLong(getString(R.string.SL_TAB_ID), -1);
        SLP_TAB_ID = bundle.getLong(getString(R.string.SLP_TAB_ID), -1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.p_fragment_rider, container, false);
        ButterKnife.bind(this, view);
        ((P_MainActivity) getActivity()).setSecondToolbarTitle(getString(R.string.fragment_title_rider_proposal));

        ((P_MainActivity) getActivity()).getSecond_toolbar().setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_save:
                        saveToMyBundle();
                        P_MainActivity.saveState(getContext(), true);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        select = new P_Select(getContext());


        proposalModel = P_MainActivity.myBundle.getParcelable(getString(R.string.proposal_model));
        mstProposalProductModel = proposalModel != null ? proposalModel.getMst_proposal_product() : new P_MstProposalProductModel();
        mstDataProposalModel = proposalModel != null ? proposalModel.getMst_data_proposal() : new P_MstDataProposalModel();
        mstProposalProductRiderModels = proposalModel != null ? proposalModel.getMst_proposal_product_rider() : new ArrayList<P_MstProposalProductRiderModel>();
        if (mstProposalProductRiderModels != null) {
            for (P_MstProposalProductRiderModel mstProposalProductRiderModel : mstProposalProductRiderModels) {
                dataRider.put(mstProposalProductRiderModel.getLsbs_id(), mstProposalProductRiderModel);
                dataPeserta.put(mstProposalProductRiderModel.getLsbs_id(), mstProposalProductRiderModel.getP_mstProposalProductPesertaModels());
            }
        }

        if (mstProposalProductModel != null) {
            psetId = select.selectPsetIdProductSetup(mstProposalProductModel.getLsbs_id(), mstProposalProductModel.getLsdbs_number());
        }

        int lsbsId = mstProposalProductModel.getLsbs_id();
        int lsdbsNumber = mstProposalProductModel.getLsdbs_number();
//        int flagPacket = mstDataProposalModel.getFlag_packet();
        String lkuId = mstProposalProductModel.getLku_id();

        if(mstDataProposalModel.getFlag_packet()!=null){
            MethodSupport.disable(false, scrollView);

        }

        thnLamaBayar = mstProposalProductModel.getThn_lama_bayar();
        thnCutiPremi = mstProposalProductModel.getThn_cuti_premi();
        samePerson = P_StaticMethods.isPPandTTSamePerson(mstDataProposalModel);
//        if(mstDataProposalModel.getFlag_packet()!=null){
//            ArrayList<Integer> listRiderIdSIAP2U = select.selectListRiderIdLstBisnisRiderPACKETSIAP2U(lsbsId, lsdbsNumber, mstDataProposalModel.getFlag_packet());
//        } else{
//            ArrayList<Integer> listRiderId = select.selectListRiderIdLstBisnisRider(lsbsId, lsdbsNumber, lkuId);
//        }
//        listRiderView = new ArrayList<>();
//        Log.i(TAG, "listRiderId: " + listRiderId);
//
//        Log.i(TAG, "listRiderId: " + listRiderId);

        if(mstDataProposalModel.getFlag_packet() !=null){ //SIAP2U
            ArrayList<Integer> listRiderIdSIAP2U = select.selectListRiderIdLstBisnisRiderPACKETSIAP2U(lsbsId, lsdbsNumber, mstDataProposalModel.getFlag_packet());
            listRiderView = new ArrayList<>();
            for (Integer riderId : listRiderIdSIAP2U) {
                HashMap<String, Integer> riderView = new HashMap<>();
                riderView.put(getString(R.string.RIDER_ID), riderId);
                Integer riderNumber = select.selectRiderNumberLstBisnisRiderPACKETSIAP2U(lsbsId, lsdbsNumber, riderId, mstDataProposalModel.getFlag_packet());
                riderView.put(getString(R.string.RIDER_NUMBER), (riderNumber == null) ? -1 : riderNumber);
                listRiderView.add(riderView);
            }
        } else {
            ArrayList<Integer> listRiderId = select.selectListRiderIdLstBisnisRider(lsbsId, lsdbsNumber, lkuId);
            listRiderView = new ArrayList<>();
            for (Integer riderId : listRiderId) {
                HashMap<String, Integer> riderView = new HashMap<>();
                riderView.put(getString(R.string.RIDER_ID), riderId);
                Integer riderNumber = select.selectRiderNumberLstBisnisRider(lsbsId, lsdbsNumber, riderId, lkuId);
                riderView.put(getString(R.string.RIDER_NUMBER), (riderNumber == null) ? -1 : riderNumber);
                listRiderView.add(riderView);
            }
        }

//        btn_lanjut.setOnClickListener(this);
//        btn_kembali.setOnClickListener(this);
        getActivity().findViewById(R.id.btn_lanjut).setOnClickListener(this);
        getActivity().findViewById(R.id.btn_kembali).setOnClickListener(this);
        fillData();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setAdapter();

    }

    public void fillData() {
        int start;
        int end;
        ArrayList<DropboxModel> dropboxModels;
        P_MstProposalProductRiderModel mstProposalProductRiderModel;
        Log.d(TAG, "fillData: " + listRiderView);
        for (HashMap<String, Integer> riderItem : listRiderView) {
            int riderid = riderItem.get(getString(R.string.RIDER_ID));
//            int riderNumber = riderItem.get(getString(R.string.RIDER_NUMBER));
            List<Integer> riderPlans;
            switch (riderid) {
                case RIDER_PA://810
                    //     rider_smile_personal_accident
                    ac_resiko_smile_personal_accident.setOnClickListener(this);
                    ac_jumlah_unit_smile_personal_accident.setOnClickListener(this);
                    ac_kelas_smile_personal_accident.setOnClickListener(this);
                    ac_resiko_smile_personal_accident.setOnFocusChangeListener(this);
                    ac_jumlah_unit_smile_personal_accident.setOnFocusChangeListener(this);
                    ac_kelas_smile_personal_accident.setOnFocusChangeListener(this);
                    ac_resiko_smile_personal_accident.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_resiko_smile_personal_accident, this));
                    tv_baca_info_kelas_smile_personal_accident.setOnClickListener(this);
                    if (mstDataProposalModel.getFlag_packet() != null){
                        tv_baca_info_kelas_smile_personal_accident.setVisibility(View.GONE);
                    }else {
                        tv_baca_info_kelas_smile_personal_accident.setVisibility(View.VISIBLE);
                    }
                    cb_smile_personal_accident.setOnCheckedChangeListener(this);

                    dropboxModels = select.selectListRiderNumber(810, 1, 3);

                    mstProposalProductRiderModel = dataRider.get(RIDER_PA);
//                    if (mstProposalProductModel.getFlag_packet() != 0 || mstProposalProductModel.getFlag_packet() != null){
//                        cb_smile_personal_accident.setChecked(true);
//                        dbPA.setId(mstProposalProductRiderModel.getLsdbs_number());
//                        ac_resiko_smile_personal_accident.setText(select.selectRiderBisnisNamePacket(RIDER_PA, dbPA.getId(), dbPA.getFlagPacket())); //tambah fungsi disini
//                        ac_jumlah_unit_smile_personal_accident.setText("1");
//                        ac_kelas_smile_personal_accident.setText(String.valueOf("1"));
//                    }else {
                    if (mstProposalProductRiderModel != null || mstDataProposalModel.getFlag_packet() != null || cb_smile_personal_accident.isChecked()) {
                        cb_smile_personal_accident.setChecked(true);
                        dbPA.setId( riderItem.get("RIDER_NUMBER"));
                        if (mstDataProposalModel.getFlag_packet() != null) {
                            ac_resiko_smile_personal_accident.setText(select.selectRiderBisnisNamePacket(RIDER_PA, riderItem.get("RIDER_NUMBER"), mstDataProposalModel.getFlag_packet()));
                            ac_jumlah_unit_smile_personal_accident.setText("1");
                            if(mstDataProposalModel.getUmur_tt() < 18){//siap2U
                                ac_kelas_smile_personal_accident.setText("2");
                            } else if (mstDataProposalModel.getUmur_tt() > 17 && mstDataProposalModel.getUmur_tt() < 56){
                                ac_kelas_smile_personal_accident.setText("3");
                            }
                        } else {
                            ac_resiko_smile_personal_accident.setText(select.selectRiderBisnisName(RIDER_PA, mstProposalProductRiderModel.getLsdbs_number()));
                            ac_jumlah_unit_smile_personal_accident.setText(String.valueOf(mstProposalProductRiderModel.getJml_unit()));
                            ac_kelas_smile_personal_accident.setText(String.valueOf(mstProposalProductRiderModel.getKelas()));
                        }

                    } else {
                        dbPA = dropboxModels.get(0);
                        ac_resiko_smile_personal_accident.setText(select.selectRiderBisnisName(RIDER_PA, dbPA.getId()));
                        ac_jumlah_unit_smile_personal_accident.setText("1");
                        ac_kelas_smile_personal_accident.setText("1");
                    }
//                    }

                    ll_smile_personal_accident.setVisibility(View.VISIBLE);
                    cb_smile_personal_accident.setText(getString(R.string.SMiLe_Personal_Accident));
                    break;
                case RIDER_HCP://811
                    ac_smile_hospital_protection.setOnClickListener(this);
                    ac_smile_hospital_protection.setOnFocusChangeListener(this);
                    ac_smile_hospital_protection.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_smile_hospital_protection, this));
                    cb_smile_hospital_protection.setOnCheckedChangeListener(this);

                    switch (mstProposalProductModel.getLku_id()) {
                        case "01":
                            start = 1;
                            end = 10;
                            break;
                        case "02":
                            start = 11;
                            end = 20;
                            break;
                        default:
                            start = 0;
                            end = 0;
                            break;
                    }
                    dropboxModels = select.selectListRiderNumber(RIDER_HCP, start, end);

                    mstProposalProductRiderModel = dataRider.get(RIDER_HCP);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_hospital_protection.setChecked(true);
                        dbHP.setId(mstProposalProductRiderModel.getLsdbs_number());
                        ac_smile_hospital_protection.setText(select.selectRiderBisnisName(RIDER_HCP, dbHP.getId()));
                    } else {
                        dbHP = dropboxModels.get(0);
                        ac_smile_hospital_protection.setText(select.selectRiderBisnisName(RIDER_HCP, dbHP.getId()));
                    }

                    ll_smile_hospital_protection.setVisibility(View.VISIBLE);
                    cb_smile_hospital_protection.setText(getString(R.string.SMiLe_Hospital_Protection));

                    break;
                case RIDER_TPD://812
                    cb_smile_total_permanent_disability_tpd.setOnCheckedChangeListener(this);
                    mstProposalProductRiderModel = dataRider.get(RIDER_TPD);
                    riderPlans = select.selectRiderNumberLstBisnisRiders(
                            mstProposalProductModel.getLsbs_id(),
                            mstProposalProductModel.getLsdbs_number(),
                            RIDER_TPD,
                            mstProposalProductModel.getLku_id());

                    for (Integer riderNumber : riderPlans) {
                        switch (riderNumber) {
                            case 7://tpdFlag
                                if (mstProposalProductRiderModel != null) {
                                    cb_smile_total_permanent_disability_tpd.setChecked(true);
                                }

                                ll_smile_total_permanent_disability_tpd.setVisibility(View.VISIBLE);
                                cb_smile_total_permanent_disability_tpd.setText(getString(R.string.SMiLe_Total_Permanent_Disability_TPD));
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case RIDER_CI://813
                    ac_pup_smile_critical_illness.setOnClickListener(this);
                    ac_pup_smile_critical_illness.setOnFocusChangeListener(this);
                    ac_pup_smile_critical_illness.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_pup_smile_critical_illness, this));
                    cb_smile_critical_illness.setOnCheckedChangeListener(this);

                    mstProposalProductRiderModel = dataRider.get(RIDER_CI);
                    riderPlans = select.selectRiderNumberLstBisnisRiders(
                            mstProposalProductModel.getLsbs_id(),
                            mstProposalProductModel.getLsdbs_number(),
                            RIDER_CI,
                            mstProposalProductModel.getLku_id());

                    for (Integer riderNumber : riderPlans) {
                        switch (riderNumber) {
                            case 8://ciFlag
                                dropboxModels = P_StaticMethods.getRangeNumberDropBox(50, 100, 10, "%");

                                if (mstProposalProductRiderModel != null) {
                                    cb_smile_critical_illness.setChecked(true);
                                    dbCIPercent.setId(mstProposalProductRiderModel.getPersen_up());
                                    dbCIPercent.setLabel(mstProposalProductRiderModel.getPersen_up() + "%");
                                    ac_pup_smile_critical_illness.setText(dbCIPercent.getLabel());
                                } else {
                                    dbCIPercent = dropboxModels.get(0);
                                    ac_pup_smile_critical_illness.setText(dbCIPercent.getLabel());
                                }

                                ll_smile_critical_illness.setVisibility(View.VISIBLE);
                                cb_smile_critical_illness.setText(getString(R.string.SMiLe_Critical_Illness));

                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case RIDER_WAIVER_TPD://814
                    cb_smile_waiver_premium_5_10_tpd.setOnCheckedChangeListener(this);
                    mstProposalProductRiderModel = dataRider.get(RIDER_WAIVER_TPD);
                    riderPlans = select.selectRiderNumberLstBisnisRiders(
                            mstProposalProductModel.getLsbs_id(),
                            mstProposalProductModel.getLsdbs_number(),
                            RIDER_WAIVER_TPD,
                            mstProposalProductModel.getLku_id());

                    for (Integer riderNumber : riderPlans) {
                        switch (riderNumber) {
                            case 4://WaiverTPDFlag
                                if (mstProposalProductRiderModel != null) {
                                    cb_smile_waiver_premium_5_10_tpd.setChecked(true);
                                }
                                ll_smile_waiver_premium_5_10_tpd.setVisibility(View.VISIBLE);
                                cb_smile_waiver_premium_5_10_tpd.setText(getString(R.string.SMiLe_Waiver_Premium_5_10_TPD));

                                break;
                            case 5://WaiverTPDFlag
                                if (mstProposalProductRiderModel != null) {
                                    cb_smile_waiver_premium_5_10_tpd.setChecked(true);
                                }
                                ll_smile_waiver_premium_5_10_tpd.setVisibility(View.VISIBLE);
                                cb_smile_waiver_premium_5_10_tpd.setText(getString(R.string.SMiLe_Waiver_Premium_5_10_TPD));
                                break;
                            default:
                                throw new IllegalArgumentException("Belum diset sub ridernya 814");
                        }
                    }
                    break;
                case RIDER_PAYOR_TPD_DEATH://815
                    cb_smile_payor_5_10_tpd_death.setOnCheckedChangeListener(this);
                    mstProposalProductRiderModel = dataRider.get(RIDER_PAYOR_TPD_DEATH);
                    riderPlans = select.selectRiderNumberLstBisnisRiders(
                            mstProposalProductModel.getLsbs_id(),
                            mstProposalProductModel.getLsdbs_number(),
                            RIDER_PAYOR_TPD_DEATH,
                            mstProposalProductModel.getLku_id());

                    for (Integer riderNumber : riderPlans) {
                        switch (riderNumber) {
                            case 4://PayorTpdDeathFlag
                                if (mstProposalProductRiderModel != null) {
                                    cb_smile_payor_5_10_tpd_death.setChecked(true);
                                }
                                ll_smile_payor_5_10_tpd_death.setVisibility(View.VISIBLE);
//                            cb_smile_payor_5_10_tpd_death.setText(getString(R.string.SMiLe_Payor_5_10_TPDDeath));
                                break;
                            case 5://PayorTpdDeathFlag
                                if (mstProposalProductRiderModel != null) {
                                    cb_smile_payor_5_10_tpd_death.setChecked(true);
                                }

                                ll_smile_payor_5_10_tpd_death.setVisibility(View.VISIBLE);
                                break;
                            case 6: //PayorSpouseTpdDeathFlag

                                break;
                            case 13: //PayorSpouseTpdDeathFlag NEW

                                break;
                            default:
                                break;

                        }
                    }
                    break;
                case RIDER_WAIVER_CI://816
                    cb_waiver_premium_5_10_ci.setOnCheckedChangeListener(this);
                    mstProposalProductRiderModel = dataRider.get(RIDER_WAIVER_CI);
                    riderPlans = select.selectRiderNumberLstBisnisRiders(
                            mstProposalProductModel.getLsbs_id(),
                            mstProposalProductModel.getLsdbs_number(),
                            RIDER_WAIVER_CI,
                            mstProposalProductModel.getLku_id());

                    for (Integer riderNumber : riderPlans) {
                        switch (riderNumber) {
                            case 2://WaiverCiFlag
                                if (mstProposalProductRiderModel != null) {
                                    cb_waiver_premium_5_10_ci.setChecked(true);
                                }

                                ll_waiver_premium_5_10_ci.setVisibility(View.VISIBLE);
                                break;
                            case 3://WaiverCiFlag
                                if (mstProposalProductRiderModel != null) {
                                    cb_waiver_premium_5_10_ci.setChecked(true);
                                }

                                ll_waiver_premium_5_10_ci.setVisibility(View.VISIBLE);

                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case RIDER_PAYOR_CI://817
                    cb_smile_payor_5_10_ci.setOnCheckedChangeListener(this);
                    mstProposalProductRiderModel = dataRider.get(RIDER_PAYOR_CI);
                    riderPlans = select.selectRiderNumberLstBisnisRiders(
                            mstProposalProductModel.getLsbs_id(),
                            mstProposalProductModel.getLsdbs_number(),
                            817,
                            mstProposalProductModel.getLku_id());

                    for (Integer riderNumber : riderPlans) {
                        switch (riderNumber) {
                            case 2://PayorCiDeathFlag5
                                if (mstProposalProductRiderModel != null) {
                                    cb_smile_payor_5_10_ci.setChecked(true);
                                }

                                ll_smile_payor_5_10_ci.setVisibility(View.VISIBLE);
                                break;
                            case 3://PayorCiDeathFlag10
                                if (mstProposalProductRiderModel != null) {
                                    cb_smile_payor_5_10_ci.setChecked(true);
                                }

                                ll_smile_payor_5_10_ci.setVisibility(View.VISIBLE);

                                break;
                            case 4://PayorCiFlag

                                break;
                        }
                    }
                    break;
                case RIDER_TERM://818
                    et_smile_term_rider.addTextChangedListener(new MoneyTextWatcher(et_smile_term_rider));
                    cb_smile_term_rider.setOnCheckedChangeListener(this);
                    mstProposalProductRiderModel = dataRider.get(RIDER_TERM);
                    riderPlans = select.selectRiderNumberLstBisnisRiders(mstProposalProductModel.getLsbs_id(), mstProposalProductModel.getLsdbs_number(), 818, mstProposalProductModel.getLku_id());

                    for (Integer riderNumber : riderPlans) {
                        switch (riderNumber) {
                            case 2://TermRiderFlag
                                et_smile_term_rider.addTextChangedListener(new MoneyTextWatcher(et_smile_term_rider));
//                                et_smile_term_rider.setEnabled(true);
                                if (mstProposalProductRiderModel != null) {
                                    cb_smile_term_rider.setChecked(true);
                                    et_smile_term_rider.setText(String.valueOf(mstProposalProductRiderModel.getUp()));
                                } else {
                                    et_smile_term_rider.setText(String.valueOf(mstProposalProductModel.getUp()));
                                }

                                ll_smile_term_rider.setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                    break;
                case RIDER_HCP_FAMILY://819
                    ac_smile_hospital_protection_family.setOnClickListener(this);
                    ac_smile_hospital_protection_family.setOnFocusChangeListener(this);
                    ac_smile_hospital_protection_family.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_smile_hospital_protection_family, this));
                    tv_peserta_detail_smile_hospital_protection_family.setOnClickListener(this);
                    cb_smile_hospital_protection_family.setOnCheckedChangeListener(this);
                    switch (mstProposalProductModel.getLku_id()) {
                        case "01":
                            start = 1;
                            end = 10;
                            break;
                        case "02":
                            start = 11;
                            end = 20;
                            break;
                        default:
                            start = 0;
                            end = 0;
                            break;
                    }

                    dropboxModels = select.selectListRiderNumber(RIDER_HCP_FAMILY, start, end);

                    mstProposalProductRiderModel = dataRider.get(RIDER_HCP_FAMILY);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_hospital_protection_family.setChecked(true);
                        dbHPF.setId(mstProposalProductRiderModel.getLsdbs_number());
                        ac_smile_hospital_protection_family.setText(select.selectRiderBisnisName(RIDER_HCP_FAMILY, mstProposalProductRiderModel.getLsdbs_number() - 140));

                    } else {
                        ArrayList<P_MstProposalProductPesertaModel> pesertaModels = new ArrayList<>();
                        dataPeserta.put(RIDER_HCP_FAMILY, pesertaModels);
                        dbHPF = dropboxModels.get(0);
                        ac_smile_hospital_protection_family.setText(select.selectRiderBisnisName(RIDER_HCP_FAMILY, dbHPF.getId()));
                    }
                    setPesertaHeader(RIDER_HCP_FAMILY);
                    ll_smile_hospital_protection_family.setVisibility(View.VISIBLE);

                    break;
                case RIDER_EKA_SEHAT://823
                    ac_smile_medical_benefit_as_charge_provider.setOnClickListener(this);
                    ac_smile_medical_benefit_as_charge_provider.setOnFocusChangeListener(this);
                    ac_smile_medical_benefit_as_charge_provider.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_smile_medical_benefit_as_charge_provider, this));
                    tv_peserta_detail_smile_medical_benefit_as_charge_provider.setOnClickListener(this);
                    cb_smile_medical_benefit_as_charge_provider.setOnCheckedChangeListener(this);
                    start = 1;
                    end = 17;

                    dropboxModels = select.selectListRiderNumber(RIDER_EKA_SEHAT, start, end);

                    mstProposalProductRiderModel = dataRider.get(RIDER_EKA_SEHAT);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_medical_benefit_as_charge_provider.setChecked(true);
                        dbMBAC.setId(mstProposalProductRiderModel.getLsdbs_number());
                        ac_smile_medical_benefit_as_charge_provider.setText(select.selectRiderBisnisName(RIDER_EKA_SEHAT, mstProposalProductRiderModel.getLsdbs_number()));
                    } else {
                        ArrayList<P_MstProposalProductPesertaModel> pesertaModels = new ArrayList<>();
                        dataPeserta.put(RIDER_EKA_SEHAT, pesertaModels);
                        dbMBAC = dropboxModels.get(0);
                        ac_smile_medical_benefit_as_charge_provider.setText(select.selectRiderBisnisName(RIDER_EKA_SEHAT, dbMBAC.getId()));
                    }
                    setPesertaHeader(RIDER_EKA_SEHAT);
                    ll_smile_medical_benefit_as_charge_provider.setVisibility(View.VISIBLE);
                    break;
                case RIDER_MEDICAL_EXTRA://848
                    ac_smile_medical_extra.setOnClickListener(this);
                    ac_smile_medical_extra.setOnFocusChangeListener(this);
                    ac_smile_medical_extra.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_smile_medical_extra, this));
                    tv_peserta_detail_smile_medical_extra.setOnClickListener(this);
                    cb_smile_medical_extra.setOnCheckedChangeListener(this);
                    start = 1;
                    end = 10;

                    dropboxModels = select.selectListRiderNumber(RIDER_MEDICAL_EXTRA, start, end);

                    mstProposalProductRiderModel = dataRider.get(RIDER_MEDICAL_EXTRA);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_medical_extra.setChecked(true);
                        dbME.setId(mstProposalProductRiderModel.getLsdbs_number());
                        ac_smile_medical_extra.setText(select.selectRiderBisnisName(RIDER_MEDICAL_EXTRA, mstProposalProductRiderModel.getLsdbs_number()));
                    } else {
                        ArrayList<P_MstProposalProductPesertaModel> pesertaModels = new ArrayList<>();
                        dataPeserta.put(RIDER_MEDICAL_EXTRA, pesertaModels);
                        dbME = dropboxModels.get(0);
                        ac_smile_medical_extra.setText(select.selectRiderBisnisName(RIDER_MEDICAL_EXTRA, dbME.getId()));
                    }
                    setPesertaHeader(RIDER_MEDICAL_EXTRA);
                    ll_smile_medical_extra.setVisibility(View.VISIBLE);

                    break;
                case RIDER_EKA_SEHAT_INNER_LIMIT://825
                    ac_smile_medical_benefit_inner_limit_provider.setOnClickListener(this);
                    ac_smile_medical_benefit_inner_limit_provider.setOnFocusChangeListener(this);
                    ac_smile_medical_benefit_inner_limit_provider.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_smile_medical_benefit_inner_limit_provider, this));
                    tv_peserta_detail_smile_medical_benefit_inner_limit_provider.setOnClickListener(this);
                    cb_smile_medical_benefit_inner_limit_provider.setOnCheckedChangeListener(this);
                    start = 1;
                    end = 15;

                    dropboxModels = select.selectListRiderNumber(RIDER_EKA_SEHAT_INNER_LIMIT, start, end);

                    mstProposalProductRiderModel = dataRider.get(RIDER_EKA_SEHAT_INNER_LIMIT);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_medical_benefit_inner_limit_provider.setChecked(true);
                        dbMBIL.setId(mstProposalProductRiderModel.getLsdbs_number());
                        ac_smile_medical_benefit_inner_limit_provider.setText(select.selectRiderBisnisName(RIDER_EKA_SEHAT_INNER_LIMIT, mstProposalProductRiderModel.getLsdbs_number()));
                    } else {
                        ArrayList<P_MstProposalProductPesertaModel> pesertaModels = new ArrayList<>();
                        dataPeserta.put(RIDER_EKA_SEHAT_INNER_LIMIT, pesertaModels);
                        dbMBIL = dropboxModels.get(0);
                        ac_smile_medical_benefit_inner_limit_provider.setText(select.selectRiderBisnisName(RIDER_EKA_SEHAT_INNER_LIMIT, dbMBIL.getId()));
                    }
                    setPesertaHeader(RIDER_EKA_SEHAT_INNER_LIMIT);
                    ll_smile_medical_benefit_inner_limit_provider.setVisibility(View.VISIBLE);

                    break;
                case RIDER_HCP_PROVIDER://826
                    ac_smile_hospital_protection_plus.setOnClickListener(this);
                    ac_smile_hospital_protection_plus.setOnFocusChangeListener(this);
                    ac_smile_hospital_protection_plus.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_smile_hospital_protection_plus, this));
                    tv_peserta_detail_smile_hospital_protection_plus.setOnClickListener(this);
                    cb_smile_hospital_protection_plus.setOnCheckedChangeListener(this);
                    start = 1;
                    end = 12;

                    dropboxModels = select.selectListRiderNumber(RIDER_HCP_PROVIDER, start, end);

                    mstProposalProductRiderModel = dataRider.get(RIDER_HCP_PROVIDER);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_hospital_protection_plus.setChecked(true);
                        dbHPP.setId(mstProposalProductRiderModel.getLsdbs_number());
                        ac_smile_hospital_protection_plus.setText(select.selectRiderBisnisName(RIDER_HCP_PROVIDER, mstProposalProductRiderModel.getLsdbs_number()));
                    } else {
                        ArrayList<P_MstProposalProductPesertaModel> pesertaModels = new ArrayList<>();
                        dataPeserta.put(RIDER_HCP_PROVIDER, pesertaModels);
                        dbHPP = dropboxModels.get(0);
                        ac_smile_hospital_protection_plus.setText(select.selectRiderBisnisName(RIDER_HCP_PROVIDER, dbHPP.getId()));
                    }
                    setPesertaHeader(RIDER_HCP_PROVIDER);
                    ll_smile_hospital_protection_plus.setVisibility(View.VISIBLE);
                    break;
                case RIDER_WAIVER_TPD_CI://827
                    cb_smile_waiver_5_10_tpd_ci.setOnCheckedChangeListener(this);
                    mstProposalProductRiderModel = dataRider.get(RIDER_WAIVER_TPD_CI);
                    riderPlans = select.selectRiderNumberLstBisnisRiders(mstProposalProductModel.getLsbs_id(), mstProposalProductModel.getLsdbs_number(), 827, mstProposalProductModel.getLku_id());
                    boolean throughtDefault = false;

                    for (Integer riderNumber : riderPlans) {
                        switch (riderNumber) {
//                            case 1://waiver 55
//                                if (mstProposalProductRiderModel != null)
//                                    if (mstProposalProductRiderModel.getLsdbs_number() == 1)
//                                        cb_smile_waiver_55_60_65_tpd_ci.setChecked(true);
//
//                                ll_smile_waiver_55_60_65_tpd_ci.setVisibility(View.VISIBLE);
//                                break;
//                            case 2://waiver 60
//                                if (mstProposalProductRiderModel != null)
//                                    if (mstProposalProductRiderModel.getLsdbs_number() == 2)
//                                        cb_smile_waiver_55_60_65_tpd_ci.setChecked(true);
//
//                                ll_smile_waiver_55_60_65_tpd_ci.setVisibility(View.VISIBLE);
//                                break;
//                            case 3: // waiver 65
//                                if (mstProposalProductRiderModel != null)
//                                    if (mstProposalProductRiderModel.getLsdbs_number() == 3)
//                                        cb_smile_waiver_55_60_65_tpd_ci.setChecked(true);
//
//                                ll_smile_waiver_55_60_65_tpd_ci.setVisibility(View.VISIBLE);
//                                break;
                            case 4://Waiver5
                                if (mstProposalProductRiderModel != null)
                                    if (mstProposalProductRiderModel.getLsdbs_number() == 4)
                                        cb_smile_waiver_5_10_tpd_ci.setChecked(true);

                                ll_smile_waiver_5_10_tpd_ci.setVisibility(View.VISIBLE);
                                break;
                            case 5://Waiver10
                                if (mstProposalProductRiderModel != null)
                                    if (mstProposalProductRiderModel.getLsdbs_number() == 5)
                                        cb_smile_waiver_5_10_tpd_ci.setChecked(true);

                                ll_smile_waiver_5_10_tpd_ci.setVisibility(View.VISIBLE);
                                break;
                            default://WaiverTpdCiFlag
//                            defaultnya 1, 2, & 3
                                if (!throughtDefault) {
                                    throughtDefault = true;

                                    ac_smile_waiver_55_60_65_tpd_ci.setOnClickListener(this);
                                    ac_smile_waiver_55_60_65_tpd_ci.setOnFocusChangeListener(this);
                                    ac_smile_waiver_55_60_65_tpd_ci.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_smile_waiver_55_60_65_tpd_ci, this));

                                    if (mstProposalProductRiderModel != null) {
                                        if (mstProposalProductRiderModel.getLsdbs_number() == 1 ||
                                                mstProposalProductRiderModel.getLsdbs_number() == 2 ||
                                                mstProposalProductRiderModel.getLsdbs_number() == 3) {
                                            cb_smile_waiver_55_60_65_tpd_ci.setChecked(true);

                                            dbWaiverTPDCI = P_StaticMethods.getRiderWaiver556065(mstProposalProductRiderModel.getLsdbs_number());
                                            ac_smile_waiver_55_60_65_tpd_ci.setText(dbWaiverTPDCI.getLabel());
                                        }
                                    } else {
                                        dbWaiverTPDCI = new DropboxModel("55", 1);
                                        ac_smile_waiver_55_60_65_tpd_ci.setText(dbWaiverTPDCI.getLabel());
                                    }

                                    ll_smile_waiver_55_60_65_tpd_ci.setVisibility(View.VISIBLE);
                                }

                                break;
                        }
                    }
                    break;
                case RIDER_PAYOR_TPD_CI_DEATH://828
                    cb_smile_payor_25_tpd_ci_death.setOnCheckedChangeListener(this);
                    riderPlans = select.selectRiderNumberLstBisnisRiders(mstProposalProductModel.getLsbs_id(), mstProposalProductModel.getLsdbs_number(), 828, mstProposalProductModel.getLku_id());
                    for (Integer riderNumber : riderPlans) {
                        switch (riderNumber) {
                            case 1://PayorTpdCiDeathFlag 25
                                mstProposalProductRiderModel = dataRider.get(RIDER_PAYOR_TPD_CI_DEATH);
                                if (mstProposalProductRiderModel != null) {
                                    if (mstProposalProductRiderModel.getLsdbs_number() == 1) {
                                        cb_smile_payor_25_tpd_ci_death.setChecked(true);
                                    }
                                }
                                ll_smile_payor_25_tpd_ci_death.setVisibility(View.VISIBLE);
                                break;
                            case 2: //5
                                mstProposalProductRiderModel = dataRider.get(RIDER_PAYOR_TPD_CI_DEATH);
                                if (mstProposalProductRiderModel != null) {
                                    if (mstProposalProductRiderModel.getLsdbs_number() == 2) {
                                        cb_smile_payor_5_10_tpd_ci_death.setChecked(true);
                                    }
                                }

                                ll_smile_payor_5_10_tpd_ci_death.setVisibility(View.VISIBLE);
                                break;
                            case 3: //10
                                mstProposalProductRiderModel = dataRider.get(RIDER_PAYOR_TPD_CI_DEATH);
                                if (mstProposalProductRiderModel != null) {
                                    if (mstProposalProductRiderModel.getLsdbs_number() == 3) {
                                        cb_smile_payor_5_10_tpd_ci_death.setChecked(true);
                                    }
                                }

                                ll_smile_payor_5_10_tpd_ci_death.setVisibility(View.VISIBLE);
                                break;
                            default://Payor5Tpd10CiDeathFlag
//                            defaultnya 2& 3

                                break;
                        }
                    }


                    break;
                case RIDER_LADIES_INSURANCE://830
                    ac_paket_smile_ladies_insurance.setOnClickListener(this);
                    ac_paket_smile_ladies_insurance.setOnFocusChangeListener(this);
                    ac_paket_smile_ladies_insurance.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_paket_smile_ladies_insurance, this));
                    ac_pup_smile_ladies_insurance.setOnClickListener(this);
                    ac_pup_smile_ladies_insurance.setOnFocusChangeListener(this);
                    ac_pup_smile_ladies_insurance.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_pup_smile_ladies_insurance, this));
                    cb_smile_ladies_insurance.setOnCheckedChangeListener(this);

                    start = 1;
                    end = 3;

                    dropboxModels = select.selectListRiderNumber(833, start, end);
                    ArrayList<DropboxModel> dbLIPercents = P_StaticMethods.getRangeNumberDropBox(50, 100, 10, "%");

                    mstProposalProductRiderModel = dataRider.get(RIDER_LADIES_INSURANCE);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_ladies_insurance.setChecked(true);
                        dbLI.setId(mstProposalProductRiderModel.getLsdbs_number());
                        dbLI.setLabel(select.selectRiderBisnisName(832, mstProposalProductRiderModel.getLsdbs_number()));
                        ac_paket_smile_ladies_insurance.setText(dbLI.toString());
                        dbLIPercent.setId(mstProposalProductRiderModel.getPersen_up());
                        dbLIPercent.setLabel(mstProposalProductRiderModel.getPersen_up() + "%");
                        ac_pup_smile_ladies_insurance.setText(dbLIPercent.getLabel());

                    } else {
                        dbLI = dropboxModels.get(0);
                        dbLIPercent = dbLIPercents.get(0);
                        ac_paket_smile_ladies_insurance.setText(dbLI.getLabel());
                        ac_pup_smile_ladies_insurance.setText(dbLIPercent.getLabel());
                    }

                    ll_smile_ladies_insurance.setVisibility(View.VISIBLE);

                    break;
                case RIDER_HCP_LADIES://831 //
                    ac_smile_ladies_hospital_protection.setOnClickListener(this);
                    ac_smile_ladies_hospital_protection.setOnFocusChangeListener(this);
                    ac_smile_ladies_hospital_protection.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_smile_ladies_hospital_protection, this));
                    tv_peserta_detail_smile_ladies_hospital_protection.setOnClickListener(this);
                    cb_smile_ladies_hospital_protection.setOnCheckedChangeListener(this);
                    switch (mstProposalProductModel.getLku_id()) {
                        case "01":
                            start = 1;
                            end = 12;
                            break;
                        case "02":
                            start = 13;
                            end = 24;
                            break;
                        default:
                            start = 0;
                            end = 0;
                            break;
                    }

                    dropboxModels = select.selectListRiderNumber(RIDER_HCP_LADIES, start, end);

                    mstProposalProductRiderModel = dataRider.get(RIDER_HCP_LADIES);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_ladies_hospital_protection.setChecked(true);
                        dbLHP.setId(mstProposalProductRiderModel.getLsdbs_number());
                        dbLHP.setLabel(select.selectRiderBisnisName(RIDER_HCP_LADIES, dbLHP.getId()));
                        ac_smile_ladies_hospital_protection.setText(dbLHP.getLabel());
                    } else {
                        ArrayList<P_MstProposalProductPesertaModel> pesertaModels = new ArrayList<>();
                        dataPeserta.put(RIDER_HCP_LADIES, pesertaModels);
                        dbLHP = dropboxModels.get(0);
                        ac_smile_ladies_hospital_protection.setText(select.selectRiderBisnisName(RIDER_HCP_LADIES, dbLHP.getId()));
                    }
                    setPesertaHeader(RIDER_HCP_LADIES);
                    ll_smile_ladies_hospital_protection.setVisibility(View.VISIBLE);

                    break;
                case RIDER_LADIES_MED_EXPENSE://832
                    ac_smile_ladies_medical_benefit_as_charge_provider.setOnClickListener(this);
                    ac_smile_ladies_medical_benefit_as_charge_provider.setOnFocusChangeListener(this);
                    ac_smile_ladies_medical_benefit_as_charge_provider.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_smile_ladies_medical_benefit_as_charge_provider, this));
                    tv_peserta_detail_smile_ladies_medical_benefit_as_charge_provider.setOnClickListener(this);
                    cb_smile_ladies_medical_benefit_as_charge_provider.setOnCheckedChangeListener(this);
                    start = 1;
                    end = 7;

                    dropboxModels = select.selectListRiderNumber(RIDER_LADIES_MED_EXPENSE, start, end);

                    mstProposalProductRiderModel = dataRider.get(RIDER_LADIES_MED_EXPENSE);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_ladies_medical_benefit_as_charge_provider.setChecked(true);
                        dbLMBAC.setId(mstProposalProductRiderModel.getLsdbs_number());
                        ac_smile_ladies_medical_benefit_as_charge_provider.setText(select.selectRiderBisnisName(RIDER_LADIES_MED_EXPENSE, mstProposalProductRiderModel.getLsdbs_number()));
                    } else {
                        ArrayList<P_MstProposalProductPesertaModel> pesertaModels = new ArrayList<>();
                        dataPeserta.put(RIDER_LADIES_MED_EXPENSE, pesertaModels);
                        dbLMBAC = dropboxModels.get(0);
                        ac_smile_ladies_medical_benefit_as_charge_provider.setText(select.selectRiderBisnisName(RIDER_LADIES_MED_EXPENSE, dbLMBAC.getId()));
                    }
                    setPesertaHeader(RIDER_LADIES_MED_EXPENSE);
                    ll_smile_ladies_medical_benefit_as_charge_provider.setVisibility(View.VISIBLE);

                    break;
                case RIDER_LADIES_MED_EXPENSE_INNER_LIMIT://833
                    ac_smile_ladies_medical_benefit_inner_limit_provider.setOnClickListener(this);
                    ac_smile_ladies_medical_benefit_inner_limit_provider.setOnFocusChangeListener(this);
                    ac_smile_ladies_medical_benefit_inner_limit_provider.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_smile_ladies_medical_benefit_inner_limit_provider, this));
                    tv_peserta_detail_smile_ladies_medical_benefit_inner_limit_provider.setOnClickListener(this);
                    cb_smile_ladies_medical_benefit_inner_limit_provider.setOnCheckedChangeListener(this);
                    start = 1;
                    end = 7;

                    dropboxModels = select.selectListRiderNumber(RIDER_LADIES_MED_EXPENSE_INNER_LIMIT, start, end);

                    mstProposalProductRiderModel = dataRider.get(RIDER_LADIES_MED_EXPENSE_INNER_LIMIT);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_ladies_medical_benefit_inner_limit_provider.setChecked(true);
                        dbLMBIL.setId(mstProposalProductRiderModel.getLsdbs_number());
                        ac_smile_ladies_medical_benefit_inner_limit_provider.setText(select.selectRiderBisnisName(RIDER_LADIES_MED_EXPENSE_INNER_LIMIT, mstProposalProductRiderModel.getLsdbs_number()));
                    } else {
                        ArrayList<P_MstProposalProductPesertaModel> pesertaModels = new ArrayList<>();
                        dataPeserta.put(RIDER_LADIES_MED_EXPENSE_INNER_LIMIT, pesertaModels);
                        dbLMBIL = dropboxModels.get(0);
                        ac_smile_ladies_medical_benefit_inner_limit_provider.setText(select.selectRiderBisnisName(RIDER_LADIES_MED_EXPENSE_INNER_LIMIT, dbLMBIL.getId()));
                    }
                    setPesertaHeader(RIDER_LADIES_MED_EXPENSE_INNER_LIMIT);
                    ll_smile_ladies_medical_benefit_inner_limit_provider.setVisibility(View.VISIBLE);

                    break;
                case RIDER_SCHOLARSHIP://835
                    ac_plan_smile_scholarship.setOnClickListener(this);
                    ac_plan_smile_scholarship.setOnFocusChangeListener(this);
                    ac_up_smile_scholarship.setOnClickListener(this);
                    ac_up_smile_scholarship.setOnFocusChangeListener(this);
                    cb_smile_scholarship.setOnCheckedChangeListener(this);
                    mstProposalProductRiderModel = dataRider.get(RIDER_SCHOLARSHIP);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_scholarship.setChecked(true);
                        String currentPlanScholarship = (mstProposalProductRiderModel.getLsdbs_number() == 1) ? "22" : "25";
                        ac_plan_smile_scholarship.setText(currentPlanScholarship);
                        ac_up_smile_scholarship.setText(StaticMethods.toMoney(mstProposalProductRiderModel.getUp()));
                    } else {
                        String plan = "22";
                        ac_plan_smile_scholarship.setText(plan);
                        ac_up_smile_scholarship.setText(getString(R.string.jt5));
                    }


                    ll_smile_scholarship.setVisibility(View.VISIBLE);
                    break;
                case RIDER_BABY://836
                    ac_paket_smile_baby.setOnClickListener(this);
                    ac_paket_smile_baby.setOnFocusChangeListener(this);
                    ac_paket_smile_baby.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_paket_smile_baby, this));
                    ac_bk_smile_baby.setOnClickListener(this);
                    ac_bk_smile_baby.setOnFocusChangeListener(this);
                    ac_bk_smile_baby.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_bk_smile_baby, this));
                    cb_smile_baby.setChecked(true);
                    cb_smile_baby.setEnabled(false);

                    dropboxModels = P_StaticMethods.getRiderBabyPlanNumbers();
                    ArrayList<DropboxModel> dbBabyBulanKes = P_StaticMethods.getRangeNumberDropBox(5, 8, 1, "");


                    mstProposalProductRiderModel = dataRider.get(RIDER_BABY);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_baby.setChecked(true);
                        dbBabyPlan.setId(mstProposalProductRiderModel.getLsdbs_number());
                        dbBabyPlan.setLabel(P_StaticMethods.getRiderBabyPlanNumber(mstProposalProductRiderModel.getLsdbs_number()).toString());
                        ac_paket_smile_baby.setText(dbBabyPlan.toString());
                        dbBabyBulanKe.setId(mstProposalProductRiderModel.getBulan_ke());
                        dbBabyBulanKe.setLabel(mstProposalProductRiderModel.getBulan_ke() + "");
                        ac_bk_smile_baby.setText(dbBabyBulanKe.getLabel());

                    } else {
                        dbBabyPlan = dropboxModels.get(0);
                        dbBabyBulanKe = dbBabyBulanKes.get(0);
                        ac_paket_smile_baby.setText(dbBabyPlan.getLabel());
                        ac_bk_smile_baby.setText(String.valueOf(dbBabyBulanKe.getId()));
                    }

                    ll_smile_baby.setVisibility(View.VISIBLE);
                    break;
                case RIDER_EARLY_STAGE_CI99: //837
                    ac_pup_smile_early_stage_ci_99.setOnClickListener(this);
                    ac_pup_smile_early_stage_ci_99.setOnFocusChangeListener(this);
                    ac_pup_smile_early_stage_ci_99.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_pup_smile_early_stage_ci_99, this));
                    cb_smile_early_stage_ci_99.setOnCheckedChangeListener(this);
                    ArrayList<DropboxModel> dbESPercents = P_StaticMethods.getRangeNumberDropBox(50, 100, 10, "%");

                    mstProposalProductRiderModel = dataRider.get(RIDER_EARLY_STAGE_CI99);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_early_stage_ci_99.setChecked(true);
                        dbESPercent.setId(mstProposalProductRiderModel.getPersen_up());
                        dbESPercent.setLabel(mstProposalProductRiderModel.getPersen_up() + "%");
                        ac_pup_smile_early_stage_ci_99.setText(dbESPercent.getLabel());

                    } else {
                        dbESPercent = dbESPercents.get(0);
                        ac_pup_smile_early_stage_ci_99.setText(dbESPercent.getLabel());
                    }

                    ll_smile_early_stage_ci_99.setVisibility(View.VISIBLE);
                    break;
                case RIDER_MEDICAL_PLUS_AC: //838
                    ac_smile_medical_plus_benefit_as_charge.setOnClickListener(this);
                    ac_smile_medical_plus_benefit_as_charge.setOnFocusChangeListener(this);
                    ac_smile_medical_plus_benefit_as_charge.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_smile_medical_plus_benefit_as_charge, this));
                    tv_peserta_detail_smile_medical_plus_benefit_as_charge.setOnClickListener(this);
                    cb_smile_medical_plus_benefit_as_charge.setOnCheckedChangeListener(this);
                    start = 1;
                    end = 20;

                    dropboxModels = select.selectListRiderNumber(RIDER_MEDICAL_PLUS_AC, start, end);

                    mstProposalProductRiderModel = dataRider.get(RIDER_MEDICAL_PLUS_AC);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_medical_plus_benefit_as_charge.setChecked(true);
                        dbMPBAC.setId(mstProposalProductRiderModel.getLsdbs_number());
                        ac_smile_medical_plus_benefit_as_charge.setText(select.selectRiderBisnisName(RIDER_MEDICAL_PLUS_AC, mstProposalProductRiderModel.getLsdbs_number()));
                        ac_smile_medical_plus_benefit_inner_limit.setText(select.selectRiderBisnisName(RIDER_MEDICAL_PLUS_IL, dbMPBAC.getId()));
                    } else {
                        ArrayList<P_MstProposalProductPesertaModel> pesertaModels = new ArrayList<>();
                        dataPeserta.put(RIDER_MEDICAL_PLUS_AC, pesertaModels);
                        dbMPBAC = dropboxModels.get(0);
                        ac_smile_medical_plus_benefit_as_charge.setText(select.selectRiderBisnisName(RIDER_MEDICAL_PLUS_AC, dbMPBAC.getId()));
                        ac_smile_medical_plus_benefit_inner_limit.setText(select.selectRiderBisnisName(RIDER_MEDICAL_PLUS_IL, dbMPBAC.getId()));
                    }
                    setPesertaHeader(RIDER_MEDICAL_PLUS_AC);
                    ll_smile_medical_plus_benefit_as_charge.setVisibility(View.VISIBLE);
                    break;

                case RIDER_MEDICAL_PLUS_IL: //839
                    ac_smile_medical_plus_benefit_inner_limit.setOnClickListener(this);
//                    ac_smile_medical_plus_benefit_inner_limit.setOnFocusChangeListener(this);
                    ac_smile_medical_plus_benefit_inner_limit.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_smile_medical_plus_benefit_inner_limit, this));
                    tv_peserta_detail_smile_medical_plus_benefit_inner_limit.setOnClickListener(this);
                    cb_smile_medical_plus_benefit_inner_limit.setOnCheckedChangeListener(this);
                    start = 1;
                    end = 80;

                    dropboxModels = select.selectListRiderNumber(RIDER_MEDICAL_PLUS_IL, start, end);

                    mstProposalProductRiderModel = dataRider.get(RIDER_MEDICAL_PLUS_IL);
                    if (mstProposalProductRiderModel != null) {
                        cb_smile_medical_plus_benefit_inner_limit.setChecked(true);
                        dbMPBIL.setId(mstProposalProductRiderModel.getLsdbs_number());
//                        ac_smile_medical_plus_benefit_inner_limit.setText(select.selectRiderBisnisName(RIDER_MEDICAL_PLUS_IL, mstProposalProductRiderModel.getLsdbs_number()));
                    } else {
                        ArrayList<P_MstProposalProductPesertaModel> pesertaModels = new ArrayList<>();
                        dataPeserta.put(RIDER_MEDICAL_PLUS_IL, pesertaModels);
                        dbMPBIL = dropboxModels.get(0);
//                        ac_smile_medical_plus_benefit_inner_limit.setText(select.selectRiderBisnisName(RIDER_MEDICAL_PLUS_IL, dbMPBIL.getId()));
                    }
                    setPesertaHeader(RIDER_MEDICAL_PLUS_IL);

                    if (cb_smile_medical_plus_benefit_as_charge.isChecked()) {
                        ll_smile_medical_plus_benefit_inner_limit.setVisibility(View.VISIBLE);
                    } else {
                        ll_smile_medical_plus_benefit_inner_limit.setVisibility(View.GONE);
                    }
                    break;
                default:
                    break;
            }
        }

    }

    public void setAdapter() {
        int start;
        int end;
        final int idSpinnerDefaultStyle = android.R.layout.simple_dropdown_item_1line;
        ArrayList<DropboxModel> dropboxModels;
        ArrayAdapter<DropboxModel> aaDropBoxModel;
        ArrayAdapter<CharSequence> aaDropBoxCHS;
        for (HashMap<String, Integer> riderItem : listRiderView) {
            int riderid = riderItem.get(getString(R.string.RIDER_ID));
            int riderNumber = riderItem.get(getString(R.string.RIDER_NUMBER));
            Log.d(TAG, "setAdapter: test");
            switch (riderid) {
                case RIDER_PA://810

                    dropboxModels = select.selectListRiderNumber(810, 1, 3);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);
                    ac_resiko_smile_personal_accident.setAdapter(aaDropBoxModel);

                    aaDropBoxCHS = ArrayAdapter.createFromResource(getContext(),
                            R.array.kelas_jumlahunit, idSpinnerDefaultStyle);
                    ac_kelas_smile_personal_accident.setAdapter(aaDropBoxCHS);
                    ac_jumlah_unit_smile_personal_accident.setAdapter(aaDropBoxCHS);
                    break;
                case RIDER_HCP://811
                    switch (mstProposalProductModel.getLku_id()) {
                        case "01":
                            start = 1;
                            end = 10;
                            break;
                        case "02":
                            start = 11;
                            end = 20;
                            break;
                        default:
                            start = 0;
                            end = 0;
                            break;
                    }
                    dropboxModels = select.selectListRiderNumber(RIDER_HCP, start, end);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);//masih pakai 819 karena sama
                    ac_smile_hospital_protection.setAdapter(aaDropBoxModel);

                    break;
                case RIDER_CI://813
                    ac_pup_smile_critical_illness.setOnClickListener(this);
                    ac_pup_smile_critical_illness.setOnFocusChangeListener(this);
                    switch (riderNumber) {
                        case 8://ciFlag
//                            dropboxModels = P_StaticMethods.getRangeNumberDropBox(50, 100, 10, "%");

                            ArrayList<DropboxModel> dbCi = P_StaticMethods.getRangeNumberDropBox(50, 100, 10, "%");

                            aaDropBoxModel = new ArrayAdapter<>(getContext(),
                                    idSpinnerDefaultStyle, dbCi);
                            ac_pup_smile_critical_illness.setAdapter(aaDropBoxModel);
                            break;
                        default:
                            break;
                    }
                    break;
                case RIDER_HCP_FAMILY://819
                    switch (mstProposalProductModel.getLku_id()) {
                        case "01":
                            start = 1;
                            end = 10;
                            break;
                        case "02":
                            start = 11;
                            end = 20;
                            break;
                        default:
                            start = 0;
                            end = 0;
                            break;
                    }

                    dropboxModels = select.selectListRiderNumber(RIDER_HCP_FAMILY, start, end);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);
                    ac_smile_hospital_protection_family.setAdapter(aaDropBoxModel);

                    break;
                case RIDER_EKA_SEHAT://823
                    start = 1;
                    end = 17;

                    dropboxModels = select.selectListRiderNumber(RIDER_EKA_SEHAT, start, end);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);
                    ac_smile_medical_benefit_as_charge_provider.setAdapter(aaDropBoxModel);

                    break;
                case RIDER_MEDICAL_EXTRA:// 848
                    start = 1;
                    end = 10;

                    dropboxModels = select.selectListRiderNumber(RIDER_MEDICAL_EXTRA, start, end);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle,dropboxModels);
                    ac_smile_medical_extra.setAdapter(aaDropBoxModel);
                    break;
                case RIDER_EKA_SEHAT_INNER_LIMIT://825
                    start = 1;
                    end = 15;

                    dropboxModels = select.selectListRiderNumber(RIDER_EKA_SEHAT_INNER_LIMIT, start, end);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);
                    ac_smile_medical_benefit_inner_limit_provider.setAdapter(aaDropBoxModel);

                    break;
                case RIDER_HCP_PROVIDER://826
                    start = 1;
                    end = 12;

                    dropboxModels = select.selectListRiderNumber(RIDER_HCP_PROVIDER, start, end);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);
                    ac_smile_hospital_protection_plus.setAdapter(aaDropBoxModel);
                    break;
                case RIDER_WAIVER_TPD_CI://827
                    dropboxModels = P_StaticMethods.getRiderWaiver556065s();

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);
                    ac_smile_waiver_55_60_65_tpd_ci.setAdapter(aaDropBoxModel);
                    break;
                case RIDER_LADIES_INSURANCE://830
                    start = 1;
                    end = 3;

                    dropboxModels = select.selectListRiderNumber(833, start, end);
                    ArrayList<DropboxModel> dbLIPercents = P_StaticMethods.getRangeNumberDropBox(50, 100, 10, "%");

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);
                    ac_paket_smile_ladies_insurance.setAdapter(aaDropBoxModel);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dbLIPercents);
                    ac_pup_smile_ladies_insurance.setAdapter(aaDropBoxModel);

                    break;
                case RIDER_HCP_LADIES://831 //
                    switch (mstProposalProductModel.getLku_id()) {
                        case "01":
                            start = 1;
                            end = 12;
                            break;
                        case "02":
                            start = 13;
                            end = 24;
                            break;
                        default:
                            start = 0;
                            end = 0;
                            break;
                    }

                    dropboxModels = select.selectListRiderNumber(RIDER_HCP_LADIES, start, end);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);
                    ac_smile_ladies_hospital_protection.setAdapter(aaDropBoxModel);
                    break;
                case RIDER_LADIES_MED_EXPENSE://832
                    start = 1;
                    end = 7;

                    dropboxModels = select.selectListRiderNumber(RIDER_LADIES_MED_EXPENSE, start, end);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);
                    ac_smile_ladies_medical_benefit_as_charge_provider.setAdapter(aaDropBoxModel);

                    break;
                case RIDER_LADIES_MED_EXPENSE_INNER_LIMIT://833
                    start = 1;
                    end = 7;

                    dropboxModels = select.selectListRiderNumber(RIDER_LADIES_MED_EXPENSE_INNER_LIMIT, start, end);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);
                    ac_smile_ladies_medical_benefit_inner_limit_provider.setAdapter(aaDropBoxModel);

                    break;
                case RIDER_SCHOLARSHIP://835
                    aaDropBoxCHS = ArrayAdapter.createFromResource(getContext(),
                            R.array.array_22_25, idSpinnerDefaultStyle);
                    ac_plan_smile_scholarship.setAdapter(aaDropBoxCHS);

                    aaDropBoxCHS = ArrayAdapter.createFromResource(getContext(),
                            R.array.array_jt5_jt50, idSpinnerDefaultStyle);
                    ac_up_smile_scholarship.setAdapter(aaDropBoxCHS);
                    break;
                case RIDER_BABY://836
                    dropboxModels = P_StaticMethods.getRiderBabyPlanNumbers();
                    ArrayList<DropboxModel> dbBabyBulanKes = P_StaticMethods.getRangeNumberDropBox(5, 8, 1, "");

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);
                    ac_paket_smile_baby.setAdapter(aaDropBoxModel);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dbBabyBulanKes);
                    ac_bk_smile_baby.setAdapter(aaDropBoxModel);

                    break;
                case RIDER_EARLY_STAGE_CI99: //837
                    ac_pup_smile_early_stage_ci_99.setOnClickListener(this);
                    ac_pup_smile_early_stage_ci_99.setOnFocusChangeListener(this);
                    ArrayList<DropboxModel> dbESPercents = P_StaticMethods.getRangeNumberDropBox(50, 100, 10, "%");

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dbESPercents);
                    ac_pup_smile_early_stage_ci_99.setAdapter(aaDropBoxModel);

                    break;
                case RIDER_MEDICAL_PLUS_AC: //838
                    start = 1;
                    end = 20;

                    dropboxModels = select.selectListRiderNumber(RIDER_MEDICAL_PLUS_AC, start, end);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);
                    ac_smile_medical_plus_benefit_as_charge.setAdapter(aaDropBoxModel);
                    break;
                case RIDER_MEDICAL_PLUS_IL: //839
                    start = 1;
                    end = 80;

                    dropboxModels = select.selectListRiderNumber(RIDER_MEDICAL_PLUS_IL, start, end);

                    aaDropBoxModel = new ArrayAdapter<>(getContext(),
                            idSpinnerDefaultStyle, dropboxModels);
                    ac_smile_medical_plus_benefit_inner_limit.setAdapter(aaDropBoxModel);
                    break;
                default:
                    break;
            }
        }

    }

    public void openPesertaActivity(ArrayList<P_MstProposalProductPesertaModel> mstProposalProductPesertaModels,
                                    int riderId, boolean isLadies, String name) {
        Intent intent = new Intent(getContext(), P_ListPesertaActivity.class);
        intent.putParcelableArrayListExtra(getString(R.string.peserta), mstProposalProductPesertaModels);
        intent.putExtra(getString(R.string.LSBS_ID), riderId);
        intent.putExtra(getString(R.string.isLadies), isLadies);
        intent.putExtra(getString(R.string.nama), name);
        startActivityForResult(intent, RequestCode.OPEN_PESERTA_ACTIVITY);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ac_resiko_smile_personal_accident:
                ac_resiko_smile_personal_accident.showDropDown();
                break;
            case R.id.ac_kelas_smile_personal_accident:
                ac_kelas_smile_personal_accident.showDropDown();
                break;
            case R.id.ac_jumlah_unit_smile_personal_accident:
                ac_jumlah_unit_smile_personal_accident.showDropDown();
                break;
            case R.id.ac_smile_hospital_protection_family:
                ac_smile_hospital_protection_family.showDropDown();
                break;
            case R.id.ac_pup_smile_critical_illness:
                ac_pup_smile_critical_illness.showDropDown();
                break;
            case R.id.ac_smile_medical_benefit_as_charge_provider:
                ac_smile_medical_benefit_as_charge_provider.showDropDown();
                break;
            case R.id.ac_smile_medical_extra:
                ac_smile_medical_extra.showDropDown();
                break;
            case R.id.ac_smile_medical_plus_benefit_as_charge:
                ac_smile_medical_plus_benefit_as_charge.showDropDown();
                break;
            case R.id.ac_smile_hospital_protection:
                ac_smile_hospital_protection.showDropDown();
                break;
            case R.id.ac_smile_medical_benefit_inner_limit_provider:
                ac_smile_medical_benefit_inner_limit_provider.showDropDown();
                break;
            case R.id.ac_smile_medical_plus_benefit_inner_limit:
                ac_smile_medical_plus_benefit_inner_limit.showDropDown();
                break;
            case R.id.ac_smile_hospital_protection_plus:
                ac_smile_hospital_protection_plus.showDropDown();
                break;
            case R.id.ac_paket_smile_ladies_insurance:
                ac_paket_smile_ladies_insurance.showDropDown();
                break;
            case R.id.ac_pup_smile_ladies_insurance:
                ac_pup_smile_ladies_insurance.showDropDown();
                break;
            case R.id.ac_smile_ladies_hospital_protection:
                ac_smile_ladies_hospital_protection.showDropDown();
                break;
            case R.id.ac_smile_ladies_medical_benefit_as_charge_provider:
                ac_smile_ladies_medical_benefit_as_charge_provider.showDropDown();
                break;
            case R.id.ac_smile_ladies_medical_benefit_inner_limit_provider:
                ac_smile_ladies_medical_benefit_inner_limit_provider.showDropDown();
                break;
            case R.id.ac_plan_smile_scholarship:
                ac_plan_smile_scholarship.showDropDown();
                break;
            case R.id.ac_up_smile_scholarship:
                ac_up_smile_scholarship.showDropDown();
                break;
            case R.id.ac_paket_smile_baby:
                ac_paket_smile_baby.showDropDown();
                break;
            case R.id.ac_bk_smile_baby:
                ac_bk_smile_baby.showDropDown();
                break;
            case R.id.ac_pup_smile_early_stage_ci_99:
                ac_pup_smile_early_stage_ci_99.showDropDown();
                break;
            case R.id.ac_smile_waiver_55_60_65_tpd_ci:
                ac_smile_waiver_55_60_65_tpd_ci.showDropDown();
                break;
//            case R.id.tv_info_kelas_smile_personal_accident:
//                break;
            case R.id.tv_baca_info_kelas_smile_personal_accident:
                Intent intentKelas = new Intent(getContext(), P_KelasActivity.class);
                startActivity(intentKelas);
                break;
            case R.id.tv_peserta_detail_smile_hospital_protection_family:
                openPesertaActivity(dataPeserta.get(RIDER_HCP_FAMILY), 819, false, getString(R.string.smile_hospital_protection_family));

                break;
            case R.id.tv_peserta_detail_smile_medical_benefit_as_charge_provider:
                openPesertaActivity(dataPeserta.get(RIDER_EKA_SEHAT), 823, false, getString(R.string.smile_medical_benefit_as_charge_provider));

                break;
            case R.id.tv_peserta_detail_smile_medical_extra:
                openPesertaActivity(dataPeserta.get(RIDER_MEDICAL_EXTRA), 848, false, getString(R.string.smile_medical_extra));

                break;
            case R.id.tv_peserta_detail_smile_medical_benefit_inner_limit_provider:
                openPesertaActivity(dataPeserta.get(RIDER_EKA_SEHAT_INNER_LIMIT), 825, false, getString(R.string.SMiLe_Medical_Benefit_Inner_Limit_Provider));

                break;
            case R.id.tv_peserta_detail_smile_hospital_protection_plus:
                openPesertaActivity(dataPeserta.get(RIDER_HCP_PROVIDER), 826, false, getString(R.string.SMiLe_Hospital_Protection_Plus));
                break;
            case R.id.tv_peserta_detail_smile_ladies_hospital_protection:
                openPesertaActivity(dataPeserta.get(RIDER_HCP_LADIES), 831, true, getString(R.string.SMiLe_Ladies_Hospital_Protection));
                break;
            case R.id.tv_peserta_detail_smile_ladies_medical_benefit_as_charge_provider:
                openPesertaActivity(dataPeserta.get(RIDER_LADIES_MED_EXPENSE), 832, true, getString(R.string.SMiLe_Ladies_Medical_Benefit_As_Charge));
                break;
            case R.id.tv_peserta_detail_smile_ladies_medical_benefit_inner_limit_provider:
                openPesertaActivity(dataPeserta.get(RIDER_LADIES_MED_EXPENSE_INNER_LIMIT), 833, true, getString(R.string.SMiLe_Ladies_Medical_Benefit_Inner_Limit));
                break;
            case R.id.tv_peserta_detail_smile_medical_plus_benefit_as_charge:
                openPesertaActivity(dataPeserta.get(RIDER_MEDICAL_PLUS_AC), 838, false, getString(R.string.SMiLe_Medical_Plus_AC));

                break;
            case R.id.tv_peserta_detail_smile_medical_plus_benefit_inner_limit:
                openPesertaActivity(dataPeserta.get(RIDER_MEDICAL_PLUS_IL), 839, false, getString(R.string.SMiLe_Medical_Plus_IL));

                break;
            case R.id.btn_kembali:
                ((P_MainActivity) getActivity()).onKembaliPressed();
                break;
            case R.id.btn_lanjut:
                attemptOk();
                break;
            default:
                break;
        }


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.ac_resiko_smile_personal_accident:
                dbPA = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_smile_hospital_protection:
                dbHP = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_pup_smile_critical_illness:
                dbCIPercent = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_smile_hospital_protection_family:
                dbHPF = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_smile_medical_benefit_as_charge_provider:
                dbMBAC = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_smile_medical_extra:
                dbME = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_smile_medical_plus_benefit_as_charge:
                dbMPBAC = (DropboxModel) parent.getAdapter().getItem(position);
                ac_smile_medical_plus_benefit_inner_limit.setText(select.selectRiderBisnisName(RIDER_MEDICAL_PLUS_IL, dbMPBAC.getId()));
                break;
            case R.id.ac_smile_medical_benefit_inner_limit_provider:
                dbMBIL = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_smile_medical_plus_benefit_inner_limit:
                dbMPBIL = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_smile_hospital_protection_plus:
                dbHPP = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_paket_smile_ladies_insurance:
                dbLI = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_pup_smile_ladies_insurance:
                dbLIPercent = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_smile_ladies_hospital_protection:
                dbLHP = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_smile_ladies_medical_benefit_as_charge_provider:
                dbLMBAC = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_smile_ladies_medical_benefit_inner_limit_provider:
                dbLMBIL = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_paket_smile_baby:
                dbBabyPlan = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_bk_smile_baby:
                dbBabyBulanKe = (DropboxModel) parent.getAdapter().getItem(position);
                Log.d(TAG, "onItemClick: test");
                break;
            case R.id.ac_pup_smile_early_stage_ci_99:
                dbESPercent = (DropboxModel) parent.getAdapter().getItem(position);
                break;
            case R.id.ac_smile_waiver_55_60_65_tpd_ci:
                dbWaiverTPDCI = (DropboxModel) parent.getAdapter().getItem(position);
                Log.d(TAG, "onItemClick: ");
                break;
            default:
                throw new IllegalArgumentException("Salah id pada OnItemClick");
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.ac_resiko_smile_personal_accident:
                if (hasFocus)
                    ac_resiko_smile_personal_accident.showDropDown();
                break;
            case R.id.ac_kelas_smile_personal_accident:
                if (hasFocus)
                    ac_kelas_smile_personal_accident.showDropDown();
                break;
            case R.id.ac_jumlah_unit_smile_personal_accident:
                if (hasFocus)
                    ac_jumlah_unit_smile_personal_accident.showDropDown();
                break;
            case R.id.ac_smile_hospital_protection_family:
                if (hasFocus)
                    ac_smile_hospital_protection_family.showDropDown();
                break;
            case R.id.ac_pup_smile_critical_illness:
                if (hasFocus)
                    ac_pup_smile_critical_illness.showDropDown();
                break;
            case R.id.ac_smile_medical_benefit_as_charge_provider:
                if (hasFocus)
                    ac_smile_medical_benefit_as_charge_provider.showDropDown();
                break;
            case R.id.ac_smile_medical_extra:
                if (hasFocus)
                    ac_smile_medical_extra.showDropDown();
                break;
            case R.id.ac_smile_medical_plus_benefit_as_charge:
                if (hasFocus)
                    ac_smile_medical_plus_benefit_as_charge.showDropDown();
                break;
            case R.id.ac_smile_hospital_protection:
                if (hasFocus)
                    ac_smile_hospital_protection.showDropDown();
                break;
            case R.id.ac_smile_medical_benefit_inner_limit_provider:
                if (hasFocus)
                    ac_smile_medical_benefit_inner_limit_provider.showDropDown();
                break;
            case R.id.ac_smile_medical_plus_benefit_inner_limit:
                if (hasFocus)
                    ac_smile_medical_plus_benefit_inner_limit.showDropDown();
                break;
            case R.id.ac_smile_hospital_protection_plus:
                if (hasFocus)
                    ac_smile_hospital_protection_plus.showDropDown();
                break;
            case R.id.ac_paket_smile_ladies_insurance:
                if (hasFocus)
                    ac_paket_smile_ladies_insurance.showDropDown();
                break;
            case R.id.ac_pup_smile_ladies_insurance:
                if (hasFocus)
                    ac_pup_smile_ladies_insurance.showDropDown();
                break;
            case R.id.ac_smile_ladies_hospital_protection:
                if (hasFocus)
                    ac_smile_ladies_hospital_protection.showDropDown();
                break;
            case R.id.ac_smile_ladies_medical_benefit_as_charge_provider:
                if (hasFocus)
                    ac_smile_ladies_medical_benefit_as_charge_provider.showDropDown();
                break;
            case R.id.ac_smile_ladies_medical_benefit_inner_limit_provider:
                if (hasFocus)
                    ac_smile_ladies_medical_benefit_inner_limit_provider.showDropDown();
                break;
            case R.id.ac_plan_smile_scholarship:
                if (hasFocus)
                    ac_plan_smile_scholarship.showDropDown();
                break;
            case R.id.ac_up_smile_scholarship:
                if (hasFocus)
                    ac_up_smile_scholarship.showDropDown();
                break;
            case R.id.ac_paket_smile_baby:
                if (hasFocus)
                    ac_paket_smile_baby.showDropDown();
                break;
            case R.id.ac_bk_smile_baby:
                if (hasFocus)
                    ac_bk_smile_baby.showDropDown();
                break;
            case R.id.ac_pup_smile_early_stage_ci_99:
                if (hasFocus)
                    ac_pup_smile_early_stage_ci_99.showDropDown();
                break;
            case R.id.ac_smile_waiver_55_60_65_tpd_ci:
                if (hasFocus)
                    ac_smile_waiver_55_60_65_tpd_ci.showDropDown();
                break;
            default:
                throw new IllegalArgumentException("Id kagak bener");
        }
    }

    public void showTvError(int visibility) {
        if (StaticMethods.isViewVisible(ll_smile_personal_accident))
            tv_error_smile_personal_accident.setVisibility(visibility); //810
        if (StaticMethods.isViewVisible(ll_smile_hospital_protection))
            tv_error_smile_hospital_protection.setVisibility(visibility); //811
        if (StaticMethods.isViewVisible(ll_smile_total_permanent_disability_tpd))
            tv_error_smile_total_permanent_disability_tpd.setVisibility(visibility); //812
        if (StaticMethods.isViewVisible(ll_smile_critical_illness))
            tv_error_smile_critical_illness.setVisibility(visibility); //813
        if (StaticMethods.isViewVisible(ll_smile_waiver_premium_5_10_tpd))
            tv_error_smile_waiver_premium_5_10_tpd.setVisibility(visibility); //814
        if (StaticMethods.isViewVisible(ll_smile_payor_5_10_tpd_death))
            tv_error_smile_payor_5_10_tpd_death.setVisibility(visibility); //815
        if (StaticMethods.isViewVisible(ll_waiver_premium_5_10_ci))
            tv_error_waiver_premium_5_10_ci.setVisibility(visibility); //816
        if (StaticMethods.isViewVisible(ll_smile_payor_5_10_ci))
            tv_error_smile_payor_5_10_ci.setVisibility(visibility); //817
        if (StaticMethods.isViewVisible(ll_smile_term_rider))
            tv_error_smile_term_rider.setVisibility(visibility); //818
        if (StaticMethods.isViewVisible(ll_smile_hospital_protection_family))
            tv_error_smile_hospital_protection_family.setVisibility(visibility); //819
        if (StaticMethods.isViewVisible(ll_smile_medical_benefit_as_charge_provider))
            tv_error_smile_medical_benefit_as_charge_provider.setVisibility(visibility); //823
        if (StaticMethods.isViewVisible(ll_smile_medical_extra))
            tv_error_smile_medical_extra.setVisibility(visibility); //848
        if (StaticMethods.isViewVisible(ll_smile_ladies_medical_benefit_inner_limit_provider))
            tv_error_smile_medical_benefit_inner_limit_provider.setVisibility(visibility); //825
        if (StaticMethods.isViewVisible(ll_smile_hospital_protection_plus))
            tv_error_smile_hospital_protection_plus.setVisibility(visibility); //826
        if (StaticMethods.isViewVisible(ll_smile_waiver_5_10_tpd_ci))
            tv_error_smile_waiver_5_10_tpd_ci.setVisibility(visibility); //827
        if (StaticMethods.isViewVisible(ll_smile_waiver_55_60_65_tpd_ci))
            tv_error_smile_waiver_55_60_65_tpd_ci.setVisibility(visibility); //827
        if (StaticMethods.isViewVisible(ll_smile_payor_5_10_tpd_ci_death))
            tv_error_smile_payor_5_10_tpd_ci_death.setVisibility(visibility); //828
        if (StaticMethods.isViewVisible(ll_smile_payor_25_tpd_ci_death))
            tv_error_smile_payor_25_tpd_ci_death.setVisibility(visibility); //828
        if (StaticMethods.isViewVisible(ll_smile_ladies_insurance))
            tv_error_smile_ladies_insurance.setVisibility(visibility); //830
        if (StaticMethods.isViewVisible(ll_smile_ladies_hospital_protection))
            tv_error_smile_ladies_hospital_protection.setVisibility(visibility); //831
        if (StaticMethods.isViewVisible(ll_smile_ladies_medical_benefit_as_charge_provider))
            tv_error_smile_ladies_medical_benefit_as_charge_provider.setVisibility(visibility); //832
        if (StaticMethods.isViewVisible(ll_smile_ladies_medical_benefit_inner_limit_provider))
            tv_error_smile_ladies_medical_benefit_inner_limit_provider.setVisibility(visibility); //833
        if (StaticMethods.isViewVisible(ll_smile_scholarship))
            tv_error_smile_scholarship.setVisibility(visibility); //835
        if (StaticMethods.isViewVisible(ll_smile_baby))
            tv_error_smile_baby.setVisibility(visibility); //836
        if (StaticMethods.isViewVisible(ll_smile_early_stage_ci_99))
            tv_error_smile_early_stage_ci_99.setVisibility(visibility); //837
        if (StaticMethods.isViewVisible(ll_smile_medical_plus_benefit_as_charge))
            tv_error_smile_medical_plus_benefit_as_charge.setVisibility(visibility); //838
        if (StaticMethods.isViewVisible(ll_smile_medical_plus_benefit_inner_limit))
            tv_error_smile_medical_plus_benefit_inner_limit.setVisibility(visibility); //839
    }

    private boolean checkAgeValidationForRider(boolean success, int product_code, int plan, StringBuilder stringBuilder) {
        int riderPsetId = select.selectPsetIdProductSetup(product_code, plan);
        HashMap<String, Integer> ageBound = select.selectAgeBound(riderPsetId);
        int holderAgeFrom = ageBound.get(getString(R.string.HOLDER_AGE_FROM));
        int holderAgeTo = ageBound.get(getString(R.string.HOLDER_AGE_TO));
        int insuredAgeFrom = ageBound.get(getString(R.string.INSURED_AGE_FROM));
        int insuredAgeTo = ageBound.get(getString(R.string.INSURED_AGE_TO));
        int insuredAgeFromFlag = ageBound.get(getString(R.string.INSURED_AGE_FROM_FLAG));

        if (product_code == 828 && plan == 1) {
            holderAgeTo += mstDataProposalModel.getUmur_tt();
        }

        if (product_code == 835) {
            holderAgeTo -= Integer.valueOf(ac_plan_smile_scholarship.getText().toString());
        }

        if (holderAgeFrom != -1) {
            if (mstDataProposalModel.getUmur_pp() < holderAgeFrom) {
                stringBuilder.append(getString(R.string.pp_umur_min).replace("#{tahun}", String.valueOf(holderAgeFrom)));
                success = false;
            } else if (mstDataProposalModel.getUmur_pp() > holderAgeTo) {
                stringBuilder.append(getString(R.string.pp_umur_max).replace("#{tahun}", String.valueOf(holderAgeTo)));
                success = false;
            }
        }

        if (insuredAgeFrom != -1) {
            Calendar tglLahirTT;
            switch (insuredAgeFromFlag) {
                case P_InsuredAgeFromFlag.YEAR:
                    if (mstDataProposalModel.getUmur_tt() < insuredAgeFrom) {
                        if (stringBuilder.length() != 0) {
                            stringBuilder.append("\n");
                        }
                        stringBuilder.append(getString(R.string.tt_umur_min).replace("#{tahun}", String.valueOf(insuredAgeFrom)));
                        success = false;
                    }
                    break;
                case P_InsuredAgeFromFlag.MONTH:
                    tglLahirTT = StaticMethods.toCalendar(mstDataProposalModel.getTgl_lahir_tt());
                    int umurBulan = StaticMethods.onCountBulan(tglLahirTT);
                    if (umurBulan < insuredAgeFrom) {
                        if (stringBuilder.length() != 0) {
                            stringBuilder.append("\n");
                        }
                        stringBuilder.append(getString(R.string.tt_umur_min_bulan).replace("#{bulan}", String.valueOf(insuredAgeFrom)));
                        success = false;
                    }
                    break;
                case P_InsuredAgeFromFlag.DATE:
                    tglLahirTT = StaticMethods.toCalendar(mstDataProposalModel.getTgl_lahir_tt());
                    int umurTTHari = StaticMethods.onCountHari(tglLahirTT);
                    if (umurTTHari < insuredAgeFrom) {
                        if (stringBuilder.length() != 0) {
                            stringBuilder.append("\n");
                        }
                        stringBuilder.append(getString(R.string.tt_umur_min_zero).replace("#{hari}", String.valueOf(insuredAgeFrom)));
                        success = false;
                    }
                    break;
                default:
                    break;
            }
        }

        if (insuredAgeTo != -1) {
            if (mstDataProposalModel.getUmur_tt() > insuredAgeTo) {
                if (stringBuilder.length() != 0) {
                    stringBuilder.append("\n");
                }
                stringBuilder.append(getString(R.string.tt_umur_max).replace("#{tahun}", String.valueOf(insuredAgeTo)));
                success = false;
            }
        }

        return success;
    }

    private void setPesertaHeader(int riderID) {
        String peserta;
        switch (riderID) {
            case RIDER_HCP_FAMILY://819
                peserta = getString(R.string.daftar_peserta) + " (" + dataPeserta.get(RIDER_HCP_FAMILY).size() + ")";
                tv_peserta_smile_hospital_protection_family.setText(peserta);
                break;
            case RIDER_EKA_SEHAT://823
                peserta = getString(R.string.daftar_peserta) + " (" + dataPeserta.get(RIDER_EKA_SEHAT).size() + ")";
                tv_peserta_smile_medical_benefit_as_charge_provider.setText(peserta);
                break;
            case RIDER_MEDICAL_EXTRA://848
                peserta = getString(R.string.daftar_peserta) + " (" + dataPeserta.get(RIDER_MEDICAL_EXTRA).size() + ")";
                tv_peserta_smile_medical_extra.setText(peserta);
                break;
            case RIDER_EKA_SEHAT_INNER_LIMIT://825
                peserta = getString(R.string.daftar_peserta) + " (" + dataPeserta.get(RIDER_EKA_SEHAT_INNER_LIMIT).size() + ")";
                tv_peserta_smile_medical_benefit_inner_limit_provider.setText(peserta);
                break;
            case RIDER_HCP_PROVIDER://826
                peserta = getString(R.string.daftar_peserta) + " (" + dataPeserta.get(RIDER_HCP_PROVIDER).size() + ")";
                tv_peserta_smile_hospital_protection_plus.setText(peserta);
                break;
            case RIDER_HCP_LADIES://831 //
                peserta = getString(R.string.daftar_peserta) + " (" + dataPeserta.get(RIDER_HCP_LADIES).size() + ")";
                tv_peserta_smile_ladies_hospital_protection.setText(peserta);
                break;
            case RIDER_LADIES_MED_EXPENSE://832
                peserta = getString(R.string.daftar_peserta) + " (" + dataPeserta.get(RIDER_LADIES_MED_EXPENSE).size() + ")";
                tv_peserta_smile_ladies_medical_benefit_as_charge_provider.setText(peserta);
                break;
            case RIDER_LADIES_MED_EXPENSE_INNER_LIMIT://833
                peserta = getString(R.string.daftar_peserta) + " (" + dataPeserta.get(RIDER_LADIES_MED_EXPENSE_INNER_LIMIT).size() + ")";
                tv_peserta_smile_ladies_medical_benefit_inner_limit_provider.setText(peserta);
                break;
            case RIDER_MEDICAL_PLUS_AC://838
                peserta = getString(R.string.daftar_peserta) + " (" + dataPeserta.get(RIDER_MEDICAL_PLUS_AC).size() + ")";
                tv_peserta_smile_medical_plus_benefit_as_charge.setText(peserta);
                break;
            case RIDER_MEDICAL_PLUS_IL://839
//                peserta = getString(R.string.daftar_peserta) + " (" + dataPeserta.get(RIDER_MEDICAL_PLUS_IL).size() + ")";
//                tv_peserta_smile_medical_plus_benefit_inner_limit.setText(peserta);
                break;
            default:
                break;
        }
    }

    private void attemptOk() {
        showTvError(View.GONE);
        boolean success = true;

        StringBuilder sbErrorPersonalAccident = new StringBuilder();//810
        StringBuilder sbErrorHospitalProtection = new StringBuilder();//811
        StringBuilder sbErrorTotalPermanentDisabilityTPD = new StringBuilder();//812
        StringBuilder sbErrorCriticalIllness = new StringBuilder();//813
        StringBuilder sbErrorWaiverPremium510TPD = new StringBuilder();//814
        StringBuilder sbErrorPayor510TPDDeath = new StringBuilder();//815
        StringBuilder sbErrorWaiverPremium510CI = new StringBuilder();//816
        StringBuilder sbErrorPayor510CI = new StringBuilder();//817
        StringBuilder sbErrorTermRider = new StringBuilder();//818
        StringBuilder sbErrorHospitalProtectionFamily = new StringBuilder();//819
        StringBuilder sbErrorMedicalBenefitAsCharge = new StringBuilder();//823
        StringBuilder sbErrorMedicalExtra = new StringBuilder();//848
        StringBuilder sbErrorMedicalBenefitInnerLimit = new StringBuilder();//825
        StringBuilder sbErrorHospitalProtectionPlus = new StringBuilder();//826
        StringBuilder sbErrorWaiver510TPDCI = new StringBuilder();//827
        StringBuilder sbErrorWaiver556065TPDCI = new StringBuilder();//827
        StringBuilder sbErrorPayor510TPDCIDeath = new StringBuilder();//828
        StringBuilder sbErrorPayor25TPDCIDeath = new StringBuilder();//828
        StringBuilder sbErrorLadiesInsurance = new StringBuilder();//830
        StringBuilder sbErrorLadiesHospitalProtection = new StringBuilder();//831
        StringBuilder sbErrorLadiesMedicalBenefitAsCharge = new StringBuilder();//832
        StringBuilder sbErrorLadiesMedicalBenefitInnerLimit = new StringBuilder();//833
        StringBuilder sbErrorScholarship = new StringBuilder();//835
//        StringBuilder sbErrorBaby = new StringBuilder();//836
        StringBuilder sbErrorEarlyStageCI99 = new StringBuilder();//833
        StringBuilder sbErrorMedicalPlusBenefitAsCharge = new StringBuilder();//838
        StringBuilder sbErrorMedicalPlusBenefitInnerLimit = new StringBuilder();//839

        int sumTPD = calculateSumTPD();
        int sumCI = calculateSumCI();
        int sumHCP = calculateSumHCP();
        int sumEkaSehat = calculateSumEkaSehat();
        int sumLadiesM = calculateSumLadiesM();
        int sumPayor = calculateSumPayor();
        int sumWaiver = calculateSumWaiver();
        int sumECI = calculateSumECI();

        if (StaticMethods.isViewVisible(ll_smile_personal_accident)) {//810
            if (cb_smile_personal_accident.isChecked()) {
                success = checkAgeValidationForRider(true, 810, 3, sbErrorPersonalAccident);//kenapa true karena success pada bagian awal pasti true/\
//                810 3, itu karena rider pa yang ada di product setup itu 810 3.
            }
        }


        if (StaticMethods.isViewVisible(ll_smile_hospital_protection)) {//811
            if (cb_smile_hospital_protection.isChecked()) {
                success = checkAgeValidationForRider(success, 811, 1, sbErrorHospitalProtection);

                String sError = P_StaticMethods.getErrorChooseDropDownVerR(mstProposalProductModel.getLku_id(), dbHP.getId(), new BigDecimal(mstProposalProductModel.getUp()));
                if (sError.trim().length() != 0) {
                    addNewLineToSBuilderIfFilled(sbErrorHospitalProtection);
                    sbErrorHospitalProtection.append(sError);
                    success = false;
                }

                success = checkIfSumGreaterThan1(success, sumHCP, sbErrorHospitalProtection, getString(R.string.rider_hcp_hanya_bisa_diambil_satu));
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_total_permanent_disability_tpd)) {//812
            if (cb_smile_total_permanent_disability_tpd.isChecked()) {
                success = checkAgeValidationForRider(success, 812, 7, sbErrorTotalPermanentDisabilityTPD);
                success = checkIfSumGreaterThan1(success, sumTPD, sbErrorTotalPermanentDisabilityTPD, getString(R.string.rider_tpd_hanya_bisa_diambil_satu));
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_critical_illness)) {//813
            if (cb_smile_critical_illness.isChecked()) {
                success = checkAgeValidationForRider(success, 813, 8, sbErrorCriticalIllness);//plan nya harusnya 8 tapi untuk ambil age adanya no 2 doang
                success = checkIfSumGreaterThan1(success, sumCI, sbErrorCriticalIllness, getString(R.string.rider_ci_hanya_bisa_diambil_satu));

                if (sumCI > 0 && sumECI > 0) {
                    addNewLineToSBuilderIfFilled(sbErrorCriticalIllness);
                    sbErrorCriticalIllness.append(getString(R.string.rider_ritical_illness_dan_early_stage_critical_illness_99_hanya_bisa_diambil_satu));
                    success = false;
                }
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_waiver_premium_5_10_tpd)) {//814
            if (cb_smile_waiver_premium_5_10_tpd.isChecked()) {
                success = checkAgeValidationForRider(success, 814, 2, sbErrorWaiverPremium510TPD);
                if (!samePerson) {
                    addNewLineToSBuilderIfFilled(sbErrorWaiverPremium510TPD);
                    sbErrorWaiverPremium510TPD.append(getString(R.string.semua_data_pp_harus_sama_tt));
                    success = false;
                }

                success = checkIfSumGreaterThan1(success, sumWaiver, sbErrorWaiverPremium510TPD, getString(R.string.rider_waiver_hanya_bisa_diambil_satu));
//                success = checkIfSumGreaterThan1(success, sumTPD, sbErrorWaiverPremium510TPD, getString(R.string.rider_tpd_hanya_bisa_diambil_satu));

            }
        }

        if (StaticMethods.isViewVisible(ll_smile_payor_5_10_tpd_death)) {//815
            if (cb_smile_payor_5_10_tpd_death.isChecked()) {
                success = checkAgeValidationForRider(success, 815, 4, sbErrorPayor510TPDDeath);

                if (samePerson) {
                    addNewLineToSBuilderIfFilled(sbErrorPayor510TPDDeath);
                    sbErrorPayor510TPDDeath.append(getString(R.string.pemegang_polis_tidak_boleh_orang_yang_sama_dengan_tertanggung));
                    success = false;
                }

//                success = checkIfSumGreaterThan1(success, sumTPD, sbErrorPayor510TPDDeath, getString(R.string.rider_tpd_hanya_bisa_diambil_satu));
                success = checkIfSumGreaterThan1(success, sumPayor, sbErrorPayor510TPDDeath, getString(R.string.rider_payor_hanya_bisa_diambil_satu));

            }
        }

        if (StaticMethods.isViewVisible(ll_waiver_premium_5_10_ci)) {//816
            if (cb_waiver_premium_5_10_ci.isChecked()) {
                success = checkAgeValidationForRider(success, 816, 1, sbErrorWaiverPremium510CI);

                if (!samePerson) {
                    addNewLineToSBuilderIfFilled(sbErrorWaiverPremium510CI);
                    sbErrorWaiverPremium510CI.append(getString(R.string.semua_data_pp_harus_sama_tt));
                    success = false;
                }
//                success = checkIfSumGreaterThan1(success, sumCI, sbErrorWaiverPremium510CI, getString(R.string.rider_ci_hanya_bisa_diambil_satu));
                success = checkIfSumGreaterThan1(success, sumWaiver, sbErrorWaiverPremium510CI, getString(R.string.rider_waiver_hanya_bisa_diambil_satu));
            }
        }


        if (StaticMethods.isViewVisible(ll_smile_payor_5_10_ci)) {//817
            if (cb_smile_payor_5_10_ci.isChecked()) {
                success = checkAgeValidationForRider(success, 817, 2, sbErrorPayor510CI);
                if (samePerson) {
                    addNewLineToSBuilderIfFilled(sbErrorPayor510CI);
                    sbErrorPayor510CI.append(getString(R.string.pemegang_polis_tidak_boleh_orang_yang_sama_dengan_tertanggung));
                    success = false;
                }

//                success = checkIfSumGreaterThan1(success, sumCI, sbErrorPayor510CI, getString(R.string.rider_ci_hanya_bisa_diambil_satu));
                success = checkIfSumGreaterThan1(success, sumPayor, sbErrorPayor510CI, getString(R.string.rider_payor_hanya_bisa_diambil_satu));

            }
        }

        if (StaticMethods.isViewVisible(ll_smile_term_rider)) {//818
            if (cb_smile_term_rider.isChecked()) {
                success = checkAgeValidationForRider(success, 818, 2, sbErrorTermRider);
                if (et_smile_term_rider.getText().toString().trim().length() == 0) {
                    addNewLineToSBuilderIfFilled(sbErrorTermRider);
                    sbErrorTermRider.append(getString(R.string.nilai_smile_term_rider_harus_diisi));
                    success = false;
                } else {
                    BigDecimal valueTermRider = new BigDecimal(StaticMethods.toStringNumber(et_smile_term_rider.getText().toString()));
                    if (valueTermRider.compareTo(new BigDecimal(mstProposalProductModel.getUp())) == 1) {
                        addNewLineToSBuilderIfFilled(sbErrorTermRider);
                        sbErrorTermRider.append(getString(R.string.nilai_term_rider_tidak_melebihi_up));
                        success = false;
                    }
                }
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_hospital_protection_family)) {//819
            if (cb_smile_hospital_protection_family.isChecked()) {
                success = checkAgeValidationForRider(success, 819, 141, sbErrorHospitalProtectionFamily);

                String sError = P_StaticMethods.getErrorChooseDropDownVerR(mstProposalProductModel.getLku_id(), dbHPF.getId() - 140, new BigDecimal(mstProposalProductModel.getUp()));
                if (sError.trim().length() != 0) {
                    addNewLineToSBuilderIfFilled(sbErrorHospitalProtectionFamily);
                    sbErrorHospitalProtectionFamily.append(sError);
                    success = false;
                }

                success = checkIfSumGreaterThan1(success, sumHCP, sbErrorHospitalProtectionFamily, getString(R.string.rider_hcp_hanya_bisa_diambil_satu));
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_medical_benefit_as_charge_provider)) {//823
            if (cb_smile_medical_benefit_as_charge_provider.isChecked()) {
                success = checkAgeValidationForRider(success, 823, 1, sbErrorMedicalBenefitAsCharge);

                success = checkIfSumGreaterThan1(success, sumEkaSehat, sbErrorMedicalBenefitAsCharge, getString(R.string.rider_eka_sehat_hanya_bisa_diambil_satu));

            }

        }

        if (StaticMethods.isViewVisible(ll_smile_medical_extra)) {//848
            if (cb_smile_medical_extra.isChecked()) {
                success = checkAgeValidationForRider(success, 848, 1, sbErrorMedicalExtra);

                success = checkIfSumGreaterThan1(success, sumEkaSehat, sbErrorMedicalExtra, getString(R.string.rider_eka_sehat_hanya_bisa_diambil_satu));
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_medical_benefit_inner_limit_provider)) {//825
            if (cb_smile_medical_benefit_inner_limit_provider.isChecked()) {
                success = checkAgeValidationForRider(success, 825, 1, sbErrorMedicalBenefitInnerLimit);

                success = checkIfSumGreaterThan1(success, sumEkaSehat, sbErrorMedicalBenefitInnerLimit, getString(R.string.rider_eka_sehat_hanya_bisa_diambil_satu));

            }
        }

        if (StaticMethods.isViewVisible(ll_smile_hospital_protection_plus)) {//826
            if (cb_smile_hospital_protection_plus.isChecked()) {
                success = checkAgeValidationForRider(success, 826, 1, sbErrorHospitalProtectionPlus);

                String sError = P_StaticMethods.getErrorChooseDropDownVerR(mstProposalProductModel.getLku_id(), dbHPP.getId(), new BigDecimal(mstProposalProductModel.getUp()));
                if (sError.trim().length() != 0) {
                    addNewLineToSBuilderIfFilled(sbErrorHospitalProtectionPlus);
                    sbErrorHospitalProtectionPlus.append(sError);
                    success = false;
                }

                success = checkIfSumGreaterThan1(success, sumHCP, sbErrorHospitalProtectionPlus, getString(R.string.rider_hcp_hanya_bisa_diambil_satu));
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_waiver_5_10_tpd_ci)) {//827
            if (cb_smile_waiver_5_10_tpd_ci.isChecked()) {
                Integer thnCutiPremi = mstProposalProductModel.getThn_cuti_premi();
//                boolean isCutiPremiOkay;

                if (thnCutiPremi != null) {
                    switch (thnCutiPremi) {
                        case 5:
//                            isCutiPremiOkay = true;
                            success = checkAgeValidationForRider(success, 827, 4, sbErrorWaiver510TPDCI);
                            break;
                        case 10:
//                            isCutiPremiOkay = true;
                            success = checkAgeValidationForRider(success, 827, 5, sbErrorWaiver510TPDCI);
                            break;
                        default:
//                            isCutiPremiOkay = false;
                            addNewLineToSBuilderIfFilled(sbErrorWaiver510TPDCI);
                            sbErrorWaiver510TPDCI.append(getString(R.string.cuti_premi_harus_5_10));
                            success = false;
                            break;
                    }
                } else {
//                    isCutiPremiOkay = false;
                    addNewLineToSBuilderIfFilled(sbErrorWaiver510TPDCI);
                    sbErrorWaiver510TPDCI.append(getString(R.string.cuti_premi_harus_5_10));
                    success = false;
                }

//                if (isCutiPremiOkay) {
                if (!samePerson) {
                    addNewLineToSBuilderIfFilled(sbErrorWaiver510TPDCI);
                    sbErrorWaiver510TPDCI.append(getString(R.string.semua_data_pp_harus_sama_tt));
                    success = false;
                }

                success = checkIfSumGreaterThan1(success, sumWaiver, sbErrorWaiver510TPDCI, getString(R.string.rider_waiver_hanya_bisa_diambil_satu));
//                success = checkIfSumGreaterThan1(success, sumTPD, sbErrorWaiver510TPDCI, getString(R.string.rider_tpd_hanya_bisa_diambil_satu));
//                success = checkIfSumGreaterThan1(success, sumCI, sbErrorWaiver510TPDCI, getString(R.string.rider_ci_hanya_bisa_diambil_satu));
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_waiver_55_60_65_tpd_ci)) {//827
            if (cb_smile_waiver_55_60_65_tpd_ci.isChecked()) {
                Integer thnCutiPremi = mstProposalProductModel.getThn_cuti_premi();
//                boolean isCutiPremiOkay;

                if (thnCutiPremi != null) {
                    if (thnCutiPremi == 55 && ac_smile_waiver_55_60_65_tpd_ci.getText().toString().equals("55")) {
                        success = checkAgeValidationForRider(success, 827, 1, sbErrorWaiver556065TPDCI);
                    } else if (thnCutiPremi == 60 && ac_smile_waiver_55_60_65_tpd_ci.getText().toString().equals("60")) {
                        success = checkAgeValidationForRider(success, 827, 2, sbErrorWaiver556065TPDCI);
                    } else if (thnCutiPremi == 65 && ac_smile_waiver_55_60_65_tpd_ci.getText().toString().equals("65")) {
                        success = checkAgeValidationForRider(success, 827, 3, sbErrorWaiver556065TPDCI);
                    } else {
                        addNewLineToSBuilderIfFilled(sbErrorWaiver556065TPDCI);
                        sbErrorWaiver556065TPDCI.append(getString(R.string.cuti_premi_harus_55_60_65));
                        success = false;
                    }

                } else {
//                    isCutiPremiOkay = false;
                    addNewLineToSBuilderIfFilled(sbErrorWaiver556065TPDCI);
                    sbErrorWaiver556065TPDCI.append(getString(R.string.cuti_premi_harus_55_60_65));
                    success = false;
                }

//                if (isCutiPremiOkay) {
                if (!samePerson) {
                    addNewLineToSBuilderIfFilled(sbErrorWaiver556065TPDCI);
                    sbErrorWaiver556065TPDCI.append(getString(R.string.semua_data_pp_harus_sama_tt));
                    success = false;
                }

                success = checkIfSumGreaterThan1(success, sumWaiver, sbErrorWaiver556065TPDCI, getString(R.string.rider_waiver_hanya_bisa_diambil_satu));
//                success = checkIfSumGreaterThan1(success, sumTPD, sbErrorWaiver556065TPDCI, getString(R.string.rider_tpd_hanya_bisa_diambil_satu));
//                success = checkIfSumGreaterThan1(success, sumCI, sbErrorWaiver556065TPDCI, getString(R.string.rider_ci_hanya_bisa_diambil_satu));
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_payor_5_10_tpd_ci_death)) {//828
            if (cb_smile_payor_5_10_tpd_ci_death.isChecked()) {
                Integer thnCutiPremi = mstProposalProductModel.getThn_cuti_premi();
//                boolean isCutiPremiOkay;

                if (thnCutiPremi != null) {
                    switch (thnCutiPremi) {
                        case 5:
//                            isCutiPremiOkay = true;
                            success = checkAgeValidationForRider(success, 828, 2, sbErrorPayor510TPDCIDeath);
                            break;
                        case 10:
//                            isCutiPremiOkay = true;
                            success = checkAgeValidationForRider(success, 828, 3, sbErrorPayor510TPDCIDeath);
                            break;
                        default:
//                            isCutiPremiOkay = false;
                            addNewLineToSBuilderIfFilled(sbErrorPayor510TPDCIDeath);
                            sbErrorPayor510TPDCIDeath.append(getString(R.string.cuti_premi_harus_5_10));
                            success = false;
                            break;
                    }
                } else {
//                    isCutiPremiOkay = false;
                    addNewLineToSBuilderIfFilled(sbErrorPayor510TPDCIDeath);
                    sbErrorPayor510TPDCIDeath.append(getString(R.string.cuti_premi_harus_5_10));
                    success = false;
                }

//                if(isCutiPremiOkay){
                if (samePerson) {
                    addNewLineToSBuilderIfFilled(sbErrorPayor510TPDCIDeath);
                    sbErrorPayor510TPDCIDeath.append(getString(R.string.pemegang_polis_tidak_boleh_orang_yang_sama_dengan_tertanggung));
                    success = false;
                }

                success = checkIfSumGreaterThan1(success, sumPayor, sbErrorPayor510TPDCIDeath, getString(R.string.rider_payor_hanya_bisa_diambil_satu));
//                success = checkIfSumGreaterThan1(success, sumTPD, sbErrorPayor510TPDCIDeath, getString(R.string.rider_tpd_hanya_bisa_diambil_satu));
//                success = checkIfSumGreaterThan1(success, sumCI, sbErrorPayor510TPDCIDeath, getString(R.string.rider_ci_hanya_bisa_diambil_satu));
//                }

            }

        }

        if (StaticMethods.isViewVisible(ll_smile_payor_25_tpd_ci_death)) {//828
            if (cb_smile_payor_25_tpd_ci_death.isChecked()) {
                Integer thnCutiPremi = mstProposalProductModel.getThn_cuti_premi();
//                boolean isCutiPremiOkay;

                if (thnCutiPremi != null) {
                    switch (thnCutiPremi) {
                        case 25:
//                            isCutiPremiOkay = true;
                            success = checkAgeValidationForRider(success, 828, 1, sbErrorPayor25TPDCIDeath);
                            break;
                        default:
//                            isCutiPremiOkay = false;
                            addNewLineToSBuilderIfFilled(sbErrorPayor25TPDCIDeath);
                            sbErrorPayor25TPDCIDeath.append(getString(R.string.cuti_premi_harus_25));
                            success = false;
                            break;
                    }
                } else {
//                    isCutiPremiOkay = false;
                    addNewLineToSBuilderIfFilled(sbErrorPayor25TPDCIDeath);
                    sbErrorPayor25TPDCIDeath.append(getString(R.string.cuti_premi_harus_25));
                    success = false;
                }

                if (samePerson) {
                    addNewLineToSBuilderIfFilled(sbErrorPayor25TPDCIDeath);
                    sbErrorPayor25TPDCIDeath.append(getString(R.string.pemegang_polis_tidak_boleh_orang_yang_sama_dengan_tertanggung));
                    success = false;
                }

                success = checkIfSumGreaterThan1(success, sumPayor, sbErrorPayor25TPDCIDeath, getString(R.string.rider_payor_hanya_bisa_diambil_satu));
//                success = checkIfSumGreaterThan1(success, sumTPD, sbErrorPayor25TPDCIDeath, getString(R.string.rider_tpd_hanya_bisa_diambil_satu));
//                success = checkIfSumGreaterThan1(success, sumCI, sbErrorPayor25TPDCIDeath, getString(R.string.rider_ci_hanya_bisa_diambil_satu));

            }

        }

        if (StaticMethods.isViewVisible(ll_smile_ladies_insurance)) {//830
            if (cb_smile_ladies_insurance.isChecked()) {
                success = checkAgeValidationForRider(success, 830, 1, sbErrorLadiesInsurance);
                if (mstDataProposalModel.getSex_tt() == null)
                    mstDataProposalModel.setSex_tt(1);
                if (mstDataProposalModel.getSex_tt() == 1) {
                    addNewLineToSBuilderIfFilled(sbErrorLadiesInsurance);
                    sbErrorLadiesInsurance.append("Rider ini hanya bisa diambil oleh tertanggung perempuan");
                    success = false;
                }
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_ladies_hospital_protection)) {//831
            if (cb_smile_ladies_hospital_protection.isChecked()) {
                success = checkAgeValidationForRider(success, 831, 1, sbErrorLadiesHospitalProtection);

                String sError = P_StaticMethods.getErrorChooseDropDownVerR(mstProposalProductModel.getLku_id(), dbLHP.getId(), new BigDecimal(mstProposalProductModel.getUp()));
                if (sError.trim().length() != 0) {
                    addNewLineToSBuilderIfFilled(sbErrorLadiesHospitalProtection);
                    sbErrorLadiesHospitalProtection.append(sError);
                    success = false;
                }

                if (mstDataProposalModel.getSex_tt() == null)
                    mstDataProposalModel.setSex_tt(1);
                if (mstDataProposalModel.getSex_tt() == 1) {
                    addNewLineToSBuilderIfFilled(sbErrorLadiesHospitalProtection);
                    sbErrorLadiesHospitalProtection.append("Rider ini hanya bisa diambil oleh tertanggung perempuan");
                    success = false;
                }
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_ladies_medical_benefit_as_charge_provider)) {//832
            if (cb_smile_ladies_medical_benefit_as_charge_provider.isChecked()) {
                success = checkAgeValidationForRider(success, 832, 1, sbErrorLadiesMedicalBenefitAsCharge);

                success = checkIfSumGreaterThan1(success, sumLadiesM, sbErrorLadiesMedicalBenefitAsCharge, getString(R.string.rider_ladies_m_hanya_bisa_diambil_satu));

                if (mstDataProposalModel.getSex_tt() == null)
                    mstDataProposalModel.setSex_tt(1);
                if (mstDataProposalModel.getSex_tt() == 1) {
                    addNewLineToSBuilderIfFilled(sbErrorLadiesMedicalBenefitAsCharge);
                    sbErrorLadiesMedicalBenefitAsCharge.append("Rider ini hanya bisa diambil oleh tertanggung perempuan");
                    success = false;
                }
            }

        }

        if (StaticMethods.isViewVisible(ll_smile_ladies_medical_benefit_inner_limit_provider)) {//833
            if (cb_smile_ladies_medical_benefit_inner_limit_provider.isChecked()) {
                success = checkAgeValidationForRider(success, 833, 1, sbErrorLadiesMedicalBenefitInnerLimit);
                success = checkIfSumGreaterThan1(success, sumLadiesM, sbErrorLadiesMedicalBenefitInnerLimit, getString(R.string.rider_ladies_m_hanya_bisa_diambil_satu));

                if (mstDataProposalModel.getSex_tt() == null)
                    mstDataProposalModel.setSex_tt(1);
                if (mstDataProposalModel.getSex_tt() == 1) {
                    addNewLineToSBuilderIfFilled(sbErrorLadiesMedicalBenefitInnerLimit);
                    sbErrorLadiesMedicalBenefitInnerLimit.append("Rider ini hanya bisa diambil oleh tertanggung perempuan");
                    success = false;
                }
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_scholarship)) {//835
            if (cb_smile_scholarship.isChecked()) {
                success = checkAgeValidationForRider(success, 835, 1, sbErrorScholarship);
                if (samePerson) {
                    addNewLineToSBuilderIfFilled(sbErrorScholarship);
                    sbErrorScholarship.append(getString(R.string.pemegang_polis_tidak_boleh_orang_yang_sama_dengan_tertanggung));
                    success = false;
                }
            }
        }

//        if (StaticMethods.isViewVisible(ll_smile_baby)) {//836
//            if (cb_smile_baby.isChecked()) {
////                success = checkAgeValidationForRider(success, RIDER_BABY, 1 , sbErrorLadiesInsurance); gak ada
//                if (mstDataProposalModel.getSex_tt() == null)
//                    mstDataProposalModel.setSex_tt(1);
//                if (mstDataProposalModel.getSex_tt() == 1) {
//                    addNewLineToSBuilderIfFilled(sbErrorLadiesInsurance);
//                    sbErrorLadiesInsurance.append("Rider ini hanya bisa diambil oleh tertanggung perempuan");
//                    success = false;
//                }
//            }
//        }

        if (StaticMethods.isViewVisible(ll_smile_early_stage_ci_99)) {//837
            if (cb_smile_early_stage_ci_99.isChecked()) {
                success = checkAgeValidationForRider(success, 837, 1, sbErrorEarlyStageCI99);
                if (sumCI > 0 && sumECI > 0) {
                    addNewLineToSBuilderIfFilled(sbErrorEarlyStageCI99);
                    sbErrorEarlyStageCI99.append(getString(R.string.rider_ritical_illness_dan_early_stage_critical_illness_99_hanya_bisa_diambil_satu));
                    success = false;
                }
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_medical_plus_benefit_as_charge)) {//838
            if (cb_smile_medical_plus_benefit_as_charge.isChecked()) {
                success = checkAgeValidationForRider(success, 838, 2, sbErrorMedicalPlusBenefitAsCharge);

                success = checkIfSumGreaterThan1(success, sumEkaSehat, sbErrorMedicalPlusBenefitAsCharge, getString(R.string.rider_eka_sehat_hanya_bisa_diambil_satu));

            }

        }

        if (StaticMethods.isViewVisible(ll_smile_medical_plus_benefit_inner_limit)) {//839
            if (cb_smile_medical_plus_benefit_inner_limit.isChecked()) {
                success = checkAgeValidationForRider(success, 839, 2, sbErrorMedicalPlusBenefitInnerLimit);

                success = checkIfSumGreaterThan1(success, sumEkaSehat, sbErrorMedicalPlusBenefitInnerLimit, "Rider ini Mengikuti Rider Medical (+) As Charge");

            }

        }

        if (success) {
//            createProposalActivityIfNotExistInProduct();
            goToNextPage();

////            mstDataProposalModel.setFlag_aktif(1);
//            Log.d(TAG, "attemptOk: saveToMybundle");

//            Log.d(TAG, "attemptOk: saveState");

        } else {
            View focus = null;
            int riderr = 0;
            if (showRiderError(sbErrorPersonalAccident, tv_error_smile_personal_accident)) {//810
                focus = tv_error_smile_personal_accident;
                riderr = 810;
            }
            if (showRiderError(sbErrorHospitalProtection, tv_error_smile_hospital_protection)) {//811
                focus = tv_error_smile_hospital_protection;
                riderr = 811;
            }
            if (showRiderError(sbErrorTotalPermanentDisabilityTPD, tv_error_smile_total_permanent_disability_tpd)) {//812
                focus = tv_error_smile_total_permanent_disability_tpd;
                riderr = 812;
            }
            if (showRiderError(sbErrorCriticalIllness, tv_error_smile_critical_illness)) {//813
                focus = tv_error_smile_critical_illness;
                riderr = 813;
            }
            if (showRiderError(sbErrorWaiverPremium510TPD, tv_error_smile_waiver_premium_5_10_tpd)) {
                //814
                focus = tv_error_smile_waiver_premium_5_10_tpd;
                riderr = 814;
            }
            if (showRiderError(sbErrorPayor510TPDDeath, tv_error_smile_payor_5_10_tpd_death)) {
                //815
                focus = tv_error_smile_payor_5_10_tpd_death;
                riderr = 815;
            }
            if (showRiderError(sbErrorWaiverPremium510CI, tv_error_waiver_premium_5_10_ci)) {
                //816
                focus = tv_error_waiver_premium_5_10_ci;
                riderr = 816;
            }
            if (showRiderError(sbErrorPayor510CI, tv_error_smile_payor_5_10_ci)) {
                //817
                focus = tv_error_smile_payor_5_10_ci;
                riderr = 817;
            }
            if (showRiderError(sbErrorTermRider, tv_error_smile_term_rider)) {
                //818
                focus = tv_error_smile_term_rider;
                riderr = 818;
            }
            if (showRiderError(sbErrorHospitalProtectionFamily, tv_error_smile_hospital_protection_family)) {
                //819
                focus = tv_error_smile_hospital_protection_family;
                riderr = 819;
            }
            if (showRiderError(sbErrorMedicalBenefitAsCharge, tv_error_smile_medical_benefit_as_charge_provider)) {
                //823
                focus = tv_error_smile_medical_benefit_as_charge_provider;
                riderr = 823;
            }
            if (showRiderError(sbErrorMedicalExtra, tv_error_smile_medical_extra)) {
                //848
                focus = tv_error_smile_medical_extra;
                riderr = 848;
            }
            if (showRiderError(sbErrorMedicalBenefitInnerLimit, tv_error_smile_medical_benefit_inner_limit_provider)) {
                //825
                focus = tv_error_smile_medical_benefit_inner_limit_provider;
                riderr = 825;
            }
            if (showRiderError(sbErrorHospitalProtectionPlus, tv_error_smile_hospital_protection_plus)) {
                //826
                focus = tv_error_smile_hospital_protection_plus;
                riderr = 826;
            }
            if (showRiderError(sbErrorWaiver510TPDCI, tv_error_smile_waiver_5_10_tpd_ci)) {
                //827
                focus = tv_error_smile_waiver_5_10_tpd_ci;
                riderr = 827;
            }
            if (showRiderError(sbErrorWaiver556065TPDCI, tv_error_smile_waiver_55_60_65_tpd_ci)) {
                //827
                focus = tv_error_smile_waiver_55_60_65_tpd_ci;
                riderr = 827;
            }
            if (showRiderError(sbErrorPayor510TPDCIDeath, tv_error_smile_payor_5_10_tpd_ci_death)) {
                //828
                focus = tv_error_smile_payor_5_10_tpd_ci_death;
                riderr = 828;
            }
            if (showRiderError(sbErrorPayor25TPDCIDeath, tv_error_smile_payor_25_tpd_ci_death)) {
                //828
                focus = tv_error_smile_payor_25_tpd_ci_death;
                riderr = 828;
            }
            if (showRiderError(sbErrorLadiesInsurance, tv_error_smile_ladies_insurance)) {
                //830
                focus = tv_error_smile_ladies_insurance;
                riderr = 830;
            }
            if (showRiderError(sbErrorLadiesHospitalProtection, tv_error_smile_ladies_hospital_protection)) {
                //831
                focus = tv_error_smile_ladies_hospital_protection;
                riderr = 831;
            }
            if (showRiderError(sbErrorLadiesMedicalBenefitAsCharge, tv_error_smile_ladies_medical_benefit_as_charge_provider)) {
                //832
                focus = tv_error_smile_ladies_medical_benefit_as_charge_provider;
                riderr = 832;
            }
            if (showRiderError(sbErrorLadiesMedicalBenefitInnerLimit, tv_error_smile_ladies_medical_benefit_inner_limit_provider)) {
                //833
                focus = tv_error_smile_ladies_medical_benefit_inner_limit_provider;
                riderr = 833;
            }
            if (showRiderError(sbErrorScholarship, tv_error_smile_scholarship)) {
                //835
                focus = tv_error_smile_scholarship;
                riderr = 835;
            }
            if (showRiderError(sbErrorEarlyStageCI99, tv_error_smile_early_stage_ci_99)) {
                //837
                focus = tv_error_smile_early_stage_ci_99;
                riderr = 837;
            }
            if (showRiderError(sbErrorMedicalPlusBenefitAsCharge, tv_error_smile_medical_plus_benefit_as_charge)) {
                //838
                focus = tv_error_smile_medical_plus_benefit_as_charge;
                riderr = 838;
            }

            if (showRiderError(sbErrorMedicalPlusBenefitInnerLimit, tv_error_smile_medical_plus_benefit_inner_limit)) {
                //839
                focus = tv_error_smile_medical_plus_benefit_inner_limit;
                riderr = 839;
            }

            Log.d(TAG, "attemptOk: riderr = " + riderr);
            if (focus != null) {
//                focusOnView(focus);
//                Log.d(TAG, "attemptOk: " + focus);
                focus.getParent().requestChildFocus(focus, focus);
                focus.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.wobble));

            }

        }
    }

    private void focusOnView(final View v) {
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                int[] location = new int[2];
                v.getLocationInWindow(location);
                Log.d(TAG, "run: location " + location[0] + " and " + location[1]);
                scrollView.scrollTo(location[0], location[1]);
            }
        });
    }

    private int getRelativeTop(View myView) {
        if (myView.getParent() == myView.getRootView())
            return myView.getTop();
        else
            return myView.getTop() + getRelativeTop((View) myView.getParent());
    }

    private void goToNextPage() {
        saveToMyBundle();
        P_MainActivity.saveState(getContext(), true);
        Fragment fragment = new P_ResultFragment();
        ((P_MainActivity) getActivity()).setFragmentBackStack(fragment, "fragment");
    }


    private boolean showRiderError(StringBuilder stringBuilder, TextView tv_error) {
        if (stringBuilder.length() != 0) {
            tv_error.setText(stringBuilder.toString());
            tv_error.setVisibility(View.VISIBLE);
            return true;
        }
        return false;
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.menu_va_proposal, menu);
//    }

    @Override
    public void onPause() {
        super.onPause();
        if (isSaveToBundle)
            saveToMyBundle();
        if (isCreateProposalActivity) {
            SimbakActivityModel simbakActivityModel = new SimbakActivityModel();

            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
            String kodeAgen = sharedPreferences.getString(getString(R.string.KODE_AGEN), "");

            simbakActivityModel.setSLA_CRTD_ID(kodeAgen);
            simbakActivityModel.setSLA_CRTD_DATE(StaticMethods.getTodayDateTime());
            simbakActivityModel.setSLA_UPDTD_ID(kodeAgen);
            simbakActivityModel.setSLA_UPDTD_DATE(StaticMethods.getTodayDateTime());
            simbakActivityModel.setSLA_ACTIVE(1);
            simbakActivityModel.setSLA_LAST_POS(1);
            simbakActivityModel.setSLA_IDATE(StaticMethods.getTodayDateTime());
            simbakActivityModel.setSLA_SDATE(StaticMethods.getTodayDateTime());
            simbakActivityModel.setSLA_TYPE(51);
            simbakActivityModel.setSLA_SUBTYPE(47);
            long timestamp = DateUtils.generateTimeStamp();
            simbakActivityModel.setSLA_TAB_ID(timestamp);

            Cursor cursorLead = new QueryUtil(getContext()).query(
                    getString(R.string.TABLE_SIMBAK_LEAD),
                    new String[]{getString(R.string.SL_BAC_CURR_BRANCH), getString(R.string.SL_BAC_REFF)},
                    getString(R.string.SL_TAB_ID) + "=?",
                    new String[]{String.valueOf(SL_TAB_ID)},
                    null
            );
            cursorLead.moveToFirst();
//            String SL_BAC_CURR_BRANCH = cursorLead.getString(cursorLead.getColumnIndexOrThrow(getString(R.string.SL_BAC_CURR_BRANCH)));
            if (!cursorLead.isNull(cursorLead.getColumnIndexOrThrow(getString(R.string.SL_BAC_REFF)))) {
                String SL_BAC_REFF = cursorLead.getString(cursorLead.getColumnIndexOrThrow(getString(R.string.SL_BAC_REFF)));
                simbakActivityModel.setSLA_REFF_ID(SL_BAC_REFF);
            }
            cursorLead.close();

            simbakActivityModel.setSLA_DETAIL("Aktivitas ini dibuat oleh sistem");
            simbakActivityModel.setSLA_INST_TAB_ID(SLP_TAB_ID);
            simbakActivityModel.setSLA_INST_TYPE(5);

            C_Insert insert = new C_Insert(getActivity());
            insert.insertToSimbakListActivity(simbakActivityModel);
        }
    }

    private void saveToMyBundle() {
        Log.d(TAG, "saveToMyBundle: ");
        String lkuId = mstProposalProductModel.getLku_id();
        mstProposalProductRiderModels = new ArrayList<>();
        int planAdderRider;

        if (StaticMethods.isViewVisible(ll_smile_personal_accident)) {//810 //per lkuid beda lsdbs_number
            if (cb_smile_personal_accident.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_PA);
                if (dbPA.getId() == -1) {
                    if(ac_resiko_smile_personal_accident.getText().toString().equals("Resiko A")){
                        dbPA.setId(1);
                    } else if (ac_resiko_smile_personal_accident.getText().toString().equals("Resiko AB")) {
                        dbPA.setId(2);
                    } else {
                        dbPA.setId(3);
                    }
                    mstProposalProductRiderModel.setLsdbs_number(dbPA.getId());
                } else {
                    mstProposalProductRiderModel.setLsdbs_number(dbPA.getId());
                }
                mstProposalProductRiderModel.setKelas(Integer.parseInt(ac_kelas_smile_personal_accident.getText().toString()));
                mstProposalProductRiderModel.setJml_unit(Integer.parseInt(ac_jumlah_unit_smile_personal_accident.getText().toString()));
                mstProposalProductRiderModel.setLku_id(lkuId);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_hospital_protection)) {//811 //
            if (cb_smile_hospital_protection.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_HCP);
                mstProposalProductRiderModel.setLsdbs_number(dbHP.getId());
                mstProposalProductRiderModel.setLku_id(lkuId);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_total_permanent_disability_tpd)) {//812 //
            if (cb_smile_total_permanent_disability_tpd.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_TPD);
                mstProposalProductRiderModel.setLsdbs_number(7);
                mstProposalProductRiderModel.setLku_id(lkuId);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_critical_illness)) {//813 //
            if (cb_smile_critical_illness.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_CI);
                mstProposalProductRiderModel.setLsdbs_number(8);
                mstProposalProductRiderModel.setLku_id(lkuId);
                mstProposalProductRiderModel.setPersen_up(dbCIPercent.getId());
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_waiver_premium_5_10_tpd)) {//814 //
            if (cb_smile_waiver_premium_5_10_tpd.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_WAIVER_TPD);
                if (thnCutiPremi != 0) {
                    if (thnCutiPremi == 5) {
                        mstProposalProductRiderModel.setLsdbs_number(4);
                    } else if (thnCutiPremi == 10) {
                        mstProposalProductRiderModel.setLsdbs_number(5);
                    }
                }
                mstProposalProductRiderModel.setLku_id(lkuId);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_payor_5_10_tpd_death)) {//815 //
            if (cb_smile_payor_5_10_tpd_death.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_PAYOR_TPD_DEATH);
                if (thnCutiPremi != 0) {
                    if (thnCutiPremi == 5) {
                        mstProposalProductRiderModel.setLsdbs_number(4);
                    } else if (thnCutiPremi == 10) {
                        mstProposalProductRiderModel.setLsdbs_number(5);
                    }
                }
                mstProposalProductRiderModel.setLku_id(lkuId);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_waiver_premium_5_10_ci)) {//816 //
            if (cb_waiver_premium_5_10_ci.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_WAIVER_CI);
                if (thnCutiPremi != 0) {
                    if (thnCutiPremi == 5) {
                        mstProposalProductRiderModel.setLsdbs_number(2);
                    } else if (thnCutiPremi == 10) {
                        mstProposalProductRiderModel.setLsdbs_number(3);
                    }
                }
                mstProposalProductRiderModel.setLku_id(lkuId);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_payor_5_10_ci)) {//817 //
            if (cb_smile_payor_5_10_ci.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_PAYOR_CI);
                if (thnCutiPremi != 0) {
                    if (thnCutiPremi == 5) {
                        mstProposalProductRiderModel.setLsdbs_number(2);
                    } else if (thnCutiPremi == 10) {
                        mstProposalProductRiderModel.setLsdbs_number(3);
                    }
                }
                mstProposalProductRiderModel.setLku_id(lkuId);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_term_rider)) {//818 //
            if (cb_smile_term_rider.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_TERM);
                mstProposalProductRiderModel.setLsdbs_number(2);
                mstProposalProductRiderModel.setLku_id(lkuId);
                String up = (et_smile_term_rider.getText().toString().trim().length() == 0) ? "0" : StaticMethods.toStringNumber(et_smile_term_rider.getText().toString());
                mstProposalProductRiderModel.setUp(up);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_hospital_protection_family)) {//819 //per lkuid beda lsdbs_number
            if (cb_smile_hospital_protection_family.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_HCP_FAMILY);
//                int planDefaultStart = (mstProposalProductModel.getLku_id().equals("01")) ? 140 : 150;
                int planDefaultStart = 140;
                int lsdbsNumber = (dbHPF.getId() > planDefaultStart) ? dbHPF.getId() : (planDefaultStart + dbHPF.getId());
                mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                mstProposalProductRiderModel.setLku_id(lkuId);
                ArrayList<P_MstProposalProductPesertaModel> pesertaModels = dataPeserta.get(RIDER_HCP_FAMILY);

                planAdderRider = 20;
                for (int i = 0; i < pesertaModels.size(); i++) {
                    pesertaModels.get(i).setLsdbs_number(lsdbsNumber + planAdderRider);
                    planAdderRider += 20;
                }
                mstProposalProductRiderModel.setP_mstProposalProductPesertaModels(pesertaModels);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }


        if (StaticMethods.isViewVisible(ll_smile_medical_benefit_as_charge_provider)) {//823 //hanya punya lku_id 01
            if (cb_smile_medical_benefit_as_charge_provider.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_EKA_SEHAT);
                mstProposalProductRiderModel.setLku_id(lkuId);
                Integer lsdbsNumber = dbMBAC.getId();
                mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                ArrayList<P_MstProposalProductPesertaModel> pesertaModels = dataPeserta.get(RIDER_EKA_SEHAT);
                planAdderRider = 15;
                for (int i = 0; i < pesertaModels.size(); i++) {
                    pesertaModels.get(i).setLsdbs_number(lsdbsNumber + planAdderRider);
                    planAdderRider += 15;
                }
                mstProposalProductRiderModel.setP_mstProposalProductPesertaModels(pesertaModels);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);

            }
        }

        if (StaticMethods.isViewVisible(ll_smile_medical_extra)) {//848 hanya di lku id 01
            if (cb_smile_medical_extra.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_MEDICAL_EXTRA);
                mstProposalProductRiderModel.setLku_id(lkuId);
                Integer lsdbsNumber = dbME.getId();
                mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                ArrayList<P_MstProposalProductPesertaModel> pesertaModels = dataPeserta.get(RIDER_MEDICAL_EXTRA);
                planAdderRider = 15;
                for (int i = 0; i < pesertaModels.size(); i++) {
                    pesertaModels.get(i).setLsdbs_number(lsdbsNumber + planAdderRider);
                    planAdderRider += 15;
                }
                mstProposalProductRiderModel.setP_mstProposalProductPesertaModels(pesertaModels);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);

            }
        }

        if (StaticMethods.isViewVisible(ll_smile_medical_benefit_inner_limit_provider)) {//825 hanya di lku_id 01
            if (cb_smile_medical_benefit_inner_limit_provider.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_EKA_SEHAT_INNER_LIMIT);
                mstProposalProductRiderModel.setLku_id(lkuId);
                Integer lsdbsNumber = dbMBIL.getId();
                mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                ArrayList<P_MstProposalProductPesertaModel> pesertaModels = dataPeserta.get(RIDER_EKA_SEHAT_INNER_LIMIT);
                planAdderRider = 15;
                for (int i = 0; i < pesertaModels.size(); i++) {
                    pesertaModels.get(i).setLsdbs_number(lsdbsNumber + planAdderRider);
                    planAdderRider += 15;
                }
                mstProposalProductRiderModel.setP_mstProposalProductPesertaModels(pesertaModels);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);

            }
        }

        if (StaticMethods.isViewVisible(ll_smile_hospital_protection_plus)) {//826 hanya di lku_id 01
            if (cb_smile_hospital_protection_plus.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_HCP_PROVIDER);
                Integer lsdbsNumber = dbHPP.getId();
                mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                mstProposalProductRiderModel.setLku_id(lkuId);
                ArrayList<P_MstProposalProductPesertaModel> pesertaModels = dataPeserta.get(RIDER_HCP_PROVIDER);
                planAdderRider = 12;
                for (int i = 0; i < pesertaModels.size(); i++) {
                    pesertaModels.get(i).setLsdbs_number(lsdbsNumber + planAdderRider);
                    planAdderRider += 12;
                }
                mstProposalProductRiderModel.setP_mstProposalProductPesertaModels(pesertaModels);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_waiver_5_10_tpd_ci)) {//827
            if (cb_smile_waiver_5_10_tpd_ci.isChecked()) {
                int lsdbsNumber;

                switch (thnCutiPremi) {
                    case 5:
                        lsdbsNumber = 4;
                        break;
                    case 10:
                        lsdbsNumber = 5;
                        break;
                    default:
                        lsdbsNumber = -1;
                        break;
                }
                if (lsdbsNumber != -1) {
                    P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                    mstProposalProductRiderModel.setLsbs_id(RIDER_WAIVER_TPD_CI);
                    mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                    mstProposalProductRiderModel.setLku_id(lkuId);

                    mstProposalProductRiderModels.add(mstProposalProductRiderModel);
                } else {
                    Log.e(TAG, "saveToMyBundle: rider 827 cuti preminya gak benar");
                }
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_waiver_55_60_65_tpd_ci)) {//827
            if (cb_smile_waiver_55_60_65_tpd_ci.isChecked()) {
                int lsdbsNumber;

                switch (thnCutiPremi) {
                    case 55:
                        lsdbsNumber = 1;
                        break;
                    case 60:
                        lsdbsNumber = 2;
                        break;
                    case 65:
                        lsdbsNumber = 3;
                        break;
                    default:
                        lsdbsNumber = -1;
                        break;
                }
                if (lsdbsNumber != -1) {
                    P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                    mstProposalProductRiderModel.setLsbs_id(RIDER_WAIVER_TPD_CI);
                    mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                    mstProposalProductRiderModel.setLku_id(lkuId);

                    mstProposalProductRiderModels.add(mstProposalProductRiderModel);
                }
            }
        }


        if (StaticMethods.isViewVisible(ll_smile_payor_5_10_tpd_ci_death)) {//828
            if (cb_smile_payor_5_10_tpd_ci_death.isChecked()) {
                int lsdbsNumber;
                switch (thnCutiPremi) {
                    case 5:
                        lsdbsNumber = 2;
                        break;
                    case 10:
                        lsdbsNumber = 3;
                        break;
                    default:
                        lsdbsNumber = -1;
                        break;
                }
                if (lsdbsNumber != -1) {
                    P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                    mstProposalProductRiderModel.setLsbs_id(RIDER_PAYOR_TPD_CI_DEATH);
                    mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                    mstProposalProductRiderModel.setLku_id(lkuId);
                    mstProposalProductRiderModels.add(mstProposalProductRiderModel);
                } else {
                    Log.e(TAG, "saveToMyBundle: rider 828 cuti preminya gak benar");
                }
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_payor_25_tpd_ci_death)) {//828
            if (cb_smile_payor_25_tpd_ci_death.isChecked()) {
                int lsdbsNumber;
                switch (thnCutiPremi) {
                    case 25:
                        lsdbsNumber = 1;
                        break;
                    default:
                        lsdbsNumber = -1;
                        break;
                }
                if (lsdbsNumber != -1) {
                    P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                    mstProposalProductRiderModel.setLsbs_id(RIDER_PAYOR_TPD_CI_DEATH);
                    mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                    mstProposalProductRiderModel.setLku_id(lkuId);
                    mstProposalProductRiderModels.add(mstProposalProductRiderModel);
                } else {
                    Log.e(TAG, "saveToMyBundle: rider 828 cuti preminya gak benar");
                }
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_ladies_insurance)) {//830
            if (cb_smile_ladies_insurance.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_LADIES_INSURANCE);
                mstProposalProductRiderModel.setLsdbs_number(dbLI.getId());
                mstProposalProductRiderModel.setLku_id(lkuId);
                mstProposalProductRiderModel.setPersen_up(dbLIPercent.getId());
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_ladies_hospital_protection)) {//831 // per lkuid beda lsdbs_number
            if (cb_smile_ladies_hospital_protection.isChecked()) {
//                int lsdbsNumber = (mstProposalProductModel.getLku_id().equals("01")) ? dbLHP.getId() : dbHPF.getId() + 12;
                int lsdbsNumber = dbLHP.getId();
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_HCP_LADIES);
                mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                mstProposalProductRiderModel.setLku_id(lkuId);
                ArrayList<P_MstProposalProductPesertaModel> pesertaModels = dataPeserta.get(RIDER_HCP_LADIES);
                planAdderRider = 24;
                for (int i = 0; i < pesertaModels.size(); i++) {
                    pesertaModels.get(i).setLsdbs_number(lsdbsNumber + planAdderRider);
                    planAdderRider += 24;
                }
                mstProposalProductRiderModel.setP_mstProposalProductPesertaModels(pesertaModels);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_ladies_medical_benefit_as_charge_provider)) {//832 // per lkuid sama lsdbs_number
            if (cb_smile_ladies_medical_benefit_as_charge_provider.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_LADIES_MED_EXPENSE);
                int lsdbsNumber = dbLMBAC.getId();
                mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                mstProposalProductRiderModel.setLku_id(lkuId);
                ArrayList<P_MstProposalProductPesertaModel> pesertaModels = dataPeserta.get(RIDER_LADIES_MED_EXPENSE);
                planAdderRider = 7;
                for (int i = 0; i < pesertaModels.size(); i++) {
                    pesertaModels.get(i).setLsdbs_number(lsdbsNumber + planAdderRider);
                    planAdderRider += 7;
                }
                mstProposalProductRiderModel.setP_mstProposalProductPesertaModels(pesertaModels);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_ladies_medical_benefit_inner_limit_provider)) {//833 // per lkuid sama lsdbs_number
            if (cb_smile_ladies_medical_benefit_inner_limit_provider.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_LADIES_MED_EXPENSE_INNER_LIMIT);
                int lsdbsNumber = dbLMBIL.getId();
                mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                mstProposalProductRiderModel.setLku_id(lkuId);
                ArrayList<P_MstProposalProductPesertaModel> pesertaModels = dataPeserta.get(RIDER_LADIES_MED_EXPENSE_INNER_LIMIT);
                planAdderRider = 7;
                for (int i = 0; i < pesertaModels.size(); i++) {
                    pesertaModels.get(i).setLsdbs_number(lsdbsNumber + planAdderRider);
                    planAdderRider += 7;
                }
                mstProposalProductRiderModel.setP_mstProposalProductPesertaModels(pesertaModels);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_scholarship)) {//835
            if (cb_smile_scholarship.isChecked()) {
                int lsdbs_number;
                switch (ac_plan_smile_scholarship.getText().toString()) {
                    case "22":
                        lsdbs_number = 1;
                        break;
                    case "25":
                        lsdbs_number = 2;
                        break;
                    default:
                        lsdbs_number = 0;
                        break;
                }
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_SCHOLARSHIP);
                mstProposalProductRiderModel.setLsdbs_number(lsdbs_number);
                mstProposalProductRiderModel.setLku_id(lkuId);
                mstProposalProductRiderModel.setUp(StaticMethods.toStringNumber(ac_up_smile_scholarship.getText().toString()));
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_baby) && mstDataProposalModel.getFlag_tt_calon_bayi() == 1) {//836
            if (cb_smile_baby.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_BABY);
                mstProposalProductRiderModel.setLsdbs_number(dbBabyPlan.getId());
                mstProposalProductRiderModel.setLku_id(lkuId);
                mstProposalProductRiderModel.setBulan_ke(dbBabyBulanKe.getId());
                switch (dbBabyPlan.getId()) {
                    case 4:
                        mstProposalProductRiderModel.setPremi((mstProposalProductModel.getLku_id().equals("01")) ? new BigDecimal(1332000).toString() : (new BigDecimal(1332000).divide(new BigDecimal(10000), 2, BigDecimal.ROUND_HALF_UP)).toString());
                        break;
                    case 5:
                        mstProposalProductRiderModel.setPremi((mstProposalProductModel.getLku_id().equals("01")) ? new BigDecimal(2603000).toString() : (new BigDecimal(2603000).divide(new BigDecimal(10000), 2, BigDecimal.ROUND_HALF_UP)).toString());
                        break;
                    case 6:
                        mstProposalProductRiderModel.setPremi((mstProposalProductModel.getLku_id().equals("01")) ? new BigDecimal(4217000).toString() : (new BigDecimal(4217000).divide(new BigDecimal(10000), 2, BigDecimal.ROUND_HALF_UP)).toString());
                        break;
                    default:
                        throw new IllegalArgumentException("id plan rider baby belum diset");
                }

                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_early_stage_ci_99)) {//837
            if (cb_smile_early_stage_ci_99.isChecked()) {
                int lsdbs_number;
                if (mstDataProposalModel.getSex_tt() == 1) {
                    lsdbs_number = 1;
                } else {
                    lsdbs_number = 2;
                }

                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_EARLY_STAGE_CI99);
                mstProposalProductRiderModel.setLsdbs_number(lsdbs_number);
                mstProposalProductRiderModel.setLku_id(lkuId);
                mstProposalProductRiderModel.setPersen_up(dbESPercent.getId());
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);
            }
        }

        if (StaticMethods.isViewVisible(ll_smile_medical_plus_benefit_as_charge)) {//838 //hanya punya lku_id 01
            if (cb_smile_medical_plus_benefit_as_charge.isChecked()) {
                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                mstProposalProductRiderModel.setLsbs_id(RIDER_MEDICAL_PLUS_AC);
                mstProposalProductRiderModel.setLku_id(lkuId);
                Integer lsdbsNumber = dbMPBAC.getId();
                mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                ArrayList<P_MstProposalProductPesertaModel> pesertaModels = dataPeserta.get(RIDER_MEDICAL_PLUS_AC);
                planAdderRider = 15;
                for (int i = 0; i < pesertaModels.size(); i++) {
                    pesertaModels.get(i).setLsdbs_number(lsdbsNumber + planAdderRider);
                    planAdderRider += 15;
                }
                mstProposalProductRiderModel.setP_mstProposalProductPesertaModels(pesertaModels);
                mstProposalProductRiderModels.add(mstProposalProductRiderModel);

            }
        }
        if (StaticMethods.isViewVisible(ll_smile_medical_plus_benefit_inner_limit)) {//839 //hanya punya lku_id 01
            if (cb_smile_medical_plus_benefit_inner_limit.isChecked()) {
//                P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                Integer lsdbsNumber = dbMPBIL.getId();
//                mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                Integer lsdbsNumberRawat = getLsdbsNumberRawatBersalin();
                Integer lsdbsNumberGigi = getLsdbsNumberRawatGigi();
                Integer lsdbsNumberKesehatan = getLsdbsNumberKesehatan();

                for (int i = 0 ; i < 4; i++) {
                    P_MstProposalProductRiderModel mstProposalProductRiderModel = new P_MstProposalProductRiderModel();
                    mstProposalProductRiderModel.setLsbs_id(RIDER_MEDICAL_PLUS_IL);
                    mstProposalProductRiderModel.setLku_id(lkuId);
                    if (lsdbsNumberRawat>0 && i == 0){
                        mstProposalProductRiderModel.setLsdbs_number(lsdbsNumberRawat);
                        mstProposalProductRiderModels.add(mstProposalProductRiderModel);
                    }
                    if (lsdbsNumberGigi > 0 && i == 1) {
                        mstProposalProductRiderModel.setLsdbs_number(lsdbsNumberGigi);
                        mstProposalProductRiderModels.add(mstProposalProductRiderModel);
                    }
                    if (lsdbsNumberKesehatan > 0 && i == 2) {
                        mstProposalProductRiderModel.setLsdbs_number(lsdbsNumberKesehatan);
                        mstProposalProductRiderModels.add(mstProposalProductRiderModel);
                    }
                    if (i == 3) {
                        mstProposalProductRiderModel.setLsdbs_number(lsdbsNumber);
                        mstProposalProductRiderModels.add(mstProposalProductRiderModel);
                    }
                }

                ArrayList<P_MstProposalProductPesertaModel> pesertaModels = dataPeserta.get(RIDER_MEDICAL_PLUS_IL);
                planAdderRider = 15;
                for (int i = 0; i < pesertaModels.size(); i++) {
                    pesertaModels.get(i).setLsdbs_number(lsdbsNumber + planAdderRider);
                    planAdderRider += 15;
                }
//                mstProposalProductRiderModel.setP_mstProposalProductPesertaModels(pesertaModels);
//                mstProposalProductRiderModels.add(mstProposalProductRiderModel);

            }
        }

//        if(mstDataProposalModel.getNo_proposal_tab()==null)
//            mstDataProposalModel.setNo_proposal_tab(StaticMethods.getNoProposalTab());
        mstDataProposalModel.setLast_page_position(DATA_PROPOSAL_PAGE);//semua kembali keawal
        proposalModel.setMst_data_proposal(mstDataProposalModel);
        Log.d(TAG, "ToMyBundle: save saveposition = " + mstProposalProductRiderModels.size());
        proposalModel.setMst_proposal_product_rider(mstProposalProductRiderModels);
        P_MainActivity.myBundle.putParcelable(getString(R.string.proposal_model), proposalModel);
        isSaveToBundle = false;
    }

    private Integer getLsdbsNumberRawatBersalin () {
        Integer lsdbsnumber = 0;
        Integer dbMbac = dbMBAC.getId();
        if (cb_rawat_bersalin.isChecked() && dbMbac == 0){
            lsdbsnumber = 42;
        }else if (cb_rawat_bersalin.isChecked() && dbMbac == 1) {
            lsdbsnumber = 43;
        }else if (cb_rawat_bersalin.isChecked() && dbMbac == 2) {
            lsdbsnumber = 44;
        }
        return lsdbsnumber;
    }

    private Integer getLsdbsNumberRawatGigi () {
        Integer lsdbsnumber = 0;
        Integer dbMbac = dbMBAC.getId();
        if (cb_rawat_gigi.isChecked() && dbMbac == 0){
            lsdbsnumber = 22;
        } else if (cb_rawat_gigi.isChecked() && dbMbac == 1){
            lsdbsnumber = 23;
        } else if (cb_rawat_gigi.isChecked() && dbMbac == 2){
            lsdbsnumber = 24;
        }
        return lsdbsnumber;
    }

    private Integer getLsdbsNumberKesehatan () {
        Integer lsdbsnumber = 0;
        Integer dbMbac = dbMBAC.getId();
        if (cb_penunjang_kesehatan.isChecked() && dbMbac == 0){
            lsdbsnumber = 62;
        } else if (cb_penunjang_kesehatan.isChecked() && dbMbac == 1){
            lsdbsnumber = 63;
        } else if (cb_penunjang_kesehatan.isChecked() && dbMbac == 2){
            lsdbsnumber = 64;
        }
        return lsdbsnumber;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == RequestCode.OPEN_PESERTA_ACTIVITY) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                ArrayList<P_MstProposalProductPesertaModel> pesertaModels = data.getParcelableArrayListExtra(getString(R.string.peserta));
                int lsbsId = data.getIntExtra(getString(R.string.LSBS_ID), 0);
                dataPeserta.put(lsbsId, pesertaModels);
                setPesertaHeader(lsbsId);
            }
        }
    }


    private int calculateSumTPD() {
        int sumTPD = 0;

//        if (ll_smile_waiver_premium_5_10_tpd.getVisibility() == View.VISIBLE) {
//            if (cb_smile_waiver_premium_5_10_tpd.isChecked()) {
//                sumTPD++;
//            }
//        }

        if (ll_smile_total_permanent_disability_tpd.getVisibility() == View.VISIBLE) {
            if (cb_smile_total_permanent_disability_tpd.isChecked()) {
                sumTPD++;
            }
        }

//        if (ll_smile_payor_5_10_tpd_ci_death.getVisibility() == View.VISIBLE) {
//            if (cb_smile_payor_5_10_tpd_ci_death.isChecked()) {
//                sumTPD++;
//            }
//        }

//        if (ll_smile_payor_25_tpd_ci_death.getVisibility() == View.VISIBLE) {
//            if (cb_smile_payor_25_tpd_ci_death.isChecked()) {
//                sumTPD++;
//            }
//        }

//        if (ll_smile_waiver_5_10_tpd_ci.getVisibility() == View.VISIBLE) {
//            if (cb_smile_waiver_5_10_tpd_ci.isChecked()) {
//                sumTPD++;
//            }
//        }

//        if (ll_smile_waiver_55_60_65_tpd_ci.getVisibility() == View.VISIBLE) {
//            if (cb_smile_waiver_55_60_65_tpd_ci.isChecked()) {
//                sumTPD++;
//            }
//        }

//        if (ll_smile_payor_5_10_tpd_death.getVisibility() == View.VISIBLE) {
//            if (cb_smile_payor_5_10_tpd_death.isChecked()) {
//                sumTPD++;
//            }
//        }

        return sumTPD;
    }

    private int calculateSumCI() {
        int sumCI = 0;
//        if (ll_smile_payor_5_10_ci.getVisibility() == View.VISIBLE) {
//            if (cb_smile_payor_5_10_ci.isChecked()) {
//                sumCI++;
//            }
//        }

//        if (ll_waiver_premium_5_10_ci.getVisibility() == View.VISIBLE) {
//            if (cb_waiver_premium_5_10_ci.isChecked()) {
//                sumCI++;
//            }
//        }

        if (ll_smile_critical_illness.getVisibility() == View.VISIBLE) {
            if (cb_smile_critical_illness.isChecked()) {
                sumCI++;
            }
        }

//        if (ll_smile_payor_5_10_tpd_ci_death.getVisibility() == View.VISIBLE) {
//            if (cb_smile_payor_5_10_tpd_ci_death.isChecked()) {
//                sumCI++;
//            }
//        }

//        if (ll_smile_payor_25_tpd_ci_death.getVisibility() == View.VISIBLE) {
//            if (cb_smile_payor_25_tpd_ci_death.isChecked()) {
//                sumCI++;
//            }
//        }

//        if (ll_smile_waiver_5_10_tpd_ci.getVisibility() == View.VISIBLE) {
//            if (cb_smile_waiver_5_10_tpd_ci.isChecked()) {
//                sumCI++;
//            }
//        }

//        if (ll_smile_waiver_55_60_65_tpd_ci.getVisibility() == View.VISIBLE) {
//            if (cb_smile_waiver_55_60_65_tpd_ci.isChecked()) {
//                sumCI++;
//            }
//        }
        return sumCI;
    }

    private int calculateSumHCP() {
        int sumHCP = 0;
        if (ll_smile_hospital_protection.getVisibility() == View.VISIBLE) {
            if (cb_smile_hospital_protection.isChecked()) {
                sumHCP++;
            }
        }

        if (ll_smile_hospital_protection_family.getVisibility() == View.VISIBLE) {
            if (cb_smile_hospital_protection_family.isChecked()) {
                sumHCP++;
            }
        }

        if (ll_smile_hospital_protection_plus.getVisibility() == View.VISIBLE) {
            if (cb_smile_hospital_protection_plus.isChecked()) {
                sumHCP++;
            }
        }

        return sumHCP;
    }

    private int calculateSumEkaSehat() {
        int sumEkaSehat = 0;

        if (ll_smile_medical_benefit_as_charge_provider.getVisibility() == View.VISIBLE) {
            if (cb_smile_medical_benefit_as_charge_provider.isChecked()) {
                sumEkaSehat++;
            }
        }

        if (ll_smile_medical_benefit_inner_limit_provider.getVisibility() == View.VISIBLE) {
            if (cb_smile_medical_benefit_inner_limit_provider.isChecked()) {
                sumEkaSehat++;
            }
        }

        if (ll_smile_medical_plus_benefit_as_charge.getVisibility() == View.VISIBLE) {
            if (cb_smile_medical_plus_benefit_as_charge.isChecked()) {
                sumEkaSehat++;
            }
        }

//        if (ll_smile_medical_plus_benefit_inner_limit.getVisibility() == View.VISIBLE) {
//            if (cb_smile_medical_plus_benefit_inner_limit.isChecked()) {
//                sumEkaSehat++;
//            }
//        }

        if (ll_smile_medical_extra.getVisibility() == View.VISIBLE) {
            if (cb_smile_medical_extra.isChecked()) {
                sumEkaSehat++;
            }
        }

        return sumEkaSehat;
    }

    private int calculateSumLadiesM() {
        int sumLadiesM = 0;

        if (ll_smile_ladies_medical_benefit_as_charge_provider.getVisibility() == View.VISIBLE) {
            if (cb_smile_ladies_medical_benefit_as_charge_provider.isChecked()) {
                sumLadiesM++;
            }
        }

        if (ll_smile_ladies_medical_benefit_inner_limit_provider.getVisibility() == View.VISIBLE) {
            if (cb_smile_ladies_medical_benefit_inner_limit_provider.isChecked()) {
                sumLadiesM++;
            }
        }

        return sumLadiesM;
    }

    private int calculateSumPayor() {
        int sumPayor = 0;

        if (ll_smile_payor_5_10_ci.getVisibility() == View.VISIBLE) {
            if (cb_smile_payor_5_10_ci.isChecked()) {
                sumPayor++;
            }
        }

        if (ll_smile_payor_5_10_tpd_ci_death.getVisibility() == View.VISIBLE) {
            if (cb_smile_payor_5_10_tpd_ci_death.isChecked()) {
                sumPayor++;
            }
        }

        if (ll_smile_payor_25_tpd_ci_death.getVisibility() == View.VISIBLE) {
            if (cb_smile_payor_25_tpd_ci_death.isChecked()) {
                sumPayor++;
            }
        }

        if (ll_smile_payor_5_10_tpd_death.getVisibility() == View.VISIBLE) {
            if (cb_smile_payor_5_10_tpd_death.isChecked()) {
                sumPayor++;
            }
        }

        return sumPayor;
    }

    private int calculateSumWaiver() {
        int sumWaiver = 0;

        if (ll_smile_waiver_premium_5_10_tpd.getVisibility() == View.VISIBLE) {
            if (cb_smile_waiver_premium_5_10_tpd.isChecked()) {
                sumWaiver++;
            }
        }

        if (ll_waiver_premium_5_10_ci.getVisibility() == View.VISIBLE) {
            if (cb_waiver_premium_5_10_ci.isChecked()) {
                sumWaiver++;
            }
        }

        if (ll_smile_waiver_5_10_tpd_ci.getVisibility() == View.VISIBLE) {
            if (cb_smile_waiver_5_10_tpd_ci.isChecked()) {
                sumWaiver++;
            }
        }

        if (ll_smile_waiver_55_60_65_tpd_ci.getVisibility() == View.VISIBLE) {
            if (cb_smile_waiver_55_60_65_tpd_ci.isChecked()) {
                sumWaiver++;
            }
        }

        return sumWaiver;
    }

    private int calculateSumECI() {
        int sumECI = 0;

        if (ll_smile_early_stage_ci_99.getVisibility() == View.VISIBLE) {
            if (cb_smile_early_stage_ci_99.isChecked()) {
                sumECI++;
            }
        }
        return sumECI;
    }

    private boolean checkIfSumGreaterThan1(boolean success, int sum, StringBuilder stringBuilder, String errorMessage) {
        if (sum > 1) {
            addNewLineToSBuilderIfFilled(stringBuilder);
            stringBuilder.append(errorMessage);
            success = false;
        }
        return success;
    }

    private void createProposalActivityIfNotExistInProduct() {
        /*belum kepake karena bjb gak pakai activity ketika proposal dibikin*/
//        if(SLP_TAB_ID!=-1){
//            String selection = getString(R.string.SLA_INST_TAB_ID) + " = ? AND " +
//                    getString(R.string.SLA_TYPE) + " = 51 AND " +
//                    getString(R.string.SLA_SUBTYPE) + " = 47";
//            String[] selectionArgs = new String[]{String.valueOf(SLP_TAB_ID)};
//            Cursor cursor = getActivity().getContentResolver().query(MyContentProvider.CONTENT_URI_ACTIVITY, null, selection, selectionArgs, null);
//            assert cursor != null;
//            Log.d(TAG, "addActivityCreateProposalIfNotExistInProduct: cursor size is " + cursor.getCount());
//            if(cursor.getCount()<1){
//                isCreateProposalActivity = true;
//            }
//            cursor.close();
//        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {
            case R.id.cb_smile_personal_accident://810
                ac_resiko_smile_personal_accident.setEnabled((isChecked));
                ac_jumlah_unit_smile_personal_accident.setEnabled((isChecked));
                ac_kelas_smile_personal_accident.setEnabled((isChecked));
                ac_kelas_smile_personal_accident.setEnabled((isChecked));
                tv_baca_info_kelas_smile_personal_accident.setEnabled((isChecked));
                tv_info_kelas_smile_personal_accident.setEnabled(isChecked);
                break;
            case R.id.cb_smile_hospital_protection://811
                ac_smile_hospital_protection.setEnabled(isChecked);
                break;
            case R.id.cb_smile_total_permanent_disability_tpd://812
//                there is nothing to disable
                break;
            case R.id.cb_smile_critical_illness://813
                ac_pup_smile_critical_illness.setEnabled(isChecked);
                break;
            case R.id.cb_smile_waiver_premium_5_10_tpd://814
//                there is nothing to disable
                break;
            case R.id.cb_smile_payor_5_10_tpd_death://815
//                there is nothing to disable
                break;
            case R.id.cb_waiver_premium_5_10_ci://816
//                there is nothing to disable
                break;
            case R.id.cb_smile_payor_5_10_ci://817
//                there is nothing to disable
                break;
            case R.id.cb_smile_term_rider://818
                et_smile_term_rider.setEnabled(isChecked);
                break;
            case R.id.cb_smile_hospital_protection_family: //819
                ac_smile_hospital_protection_family.setEnabled(isChecked);
                tv_peserta_smile_hospital_protection_family.setEnabled(isChecked);
                tv_peserta_detail_smile_hospital_protection_family.setEnabled(isChecked);
                break;
            case R.id.cb_smile_medical_benefit_as_charge_provider://823
                ac_smile_medical_benefit_as_charge_provider.setEnabled(isChecked);
                tv_peserta_smile_medical_benefit_as_charge_provider.setEnabled(isChecked);
                tv_peserta_detail_smile_medical_benefit_as_charge_provider.setEnabled(isChecked);
                break;
            case R.id.cb_smile_medical_extra://848
                ac_smile_medical_extra.setEnabled(isChecked);
                tv_peserta_smile_medical_extra.setEnabled(isChecked);
                tv_peserta_detail_smile_medical_extra.setEnabled(isChecked);
                break;
            case R.id.cb_smile_medical_benefit_inner_limit_provider://825
                ac_smile_medical_benefit_inner_limit_provider.setEnabled(isChecked);
                tv_peserta_smile_medical_benefit_inner_limit_provider.setEnabled(isChecked);
                tv_peserta_detail_smile_medical_benefit_inner_limit_provider.setEnabled(isChecked);
                break;
            case R.id.cb_smile_hospital_protection_plus://826
                ac_smile_hospital_protection_plus.setEnabled(isChecked);
                tv_peserta_smile_hospital_protection_plus.setEnabled(isChecked);
                tv_peserta_detail_smile_hospital_protection_plus.setEnabled(isChecked);
                break;
            case R.id.cb_smile_waiver_5_10_tpd_ci://827
//                there is nothing to disable
                break;
            case R.id.cb_smile_payor_25_tpd_ci_death://828
//                there is nothing to disable
                break;
            case R.id.cb_smile_ladies_insurance://830
                ac_paket_smile_ladies_insurance.setEnabled(isChecked);
                ac_pup_smile_ladies_insurance.setEnabled(isChecked);
                break;
            case R.id.cb_smile_ladies_hospital_protection://831
                ac_smile_ladies_hospital_protection.setEnabled(isChecked);
                tv_peserta_smile_ladies_hospital_protection.setEnabled(isChecked);
                tv_peserta_detail_smile_ladies_hospital_protection.setEnabled(isChecked);
                break;
            case R.id.cb_smile_ladies_medical_benefit_as_charge_provider://832
                ac_smile_ladies_medical_benefit_as_charge_provider.setEnabled(isChecked);
                tv_peserta_smile_ladies_medical_benefit_as_charge_provider.setEnabled(isChecked);
                tv_peserta_detail_smile_ladies_medical_benefit_as_charge_provider.setEnabled(isChecked);
                break;
            case R.id.cb_smile_ladies_medical_benefit_inner_limit_provider://833
                ac_smile_ladies_medical_benefit_inner_limit_provider.setEnabled(isChecked);
                tv_peserta_smile_ladies_medical_benefit_inner_limit_provider.setEnabled(isChecked);
                tv_peserta_detail_smile_ladies_medical_benefit_inner_limit_provider.setEnabled(isChecked);
                break;
            case R.id.cb_smile_scholarship://835
                ac_plan_smile_scholarship.setEnabled(isChecked);
                ac_up_smile_scholarship.setEnabled(isChecked);
                break;
            case R.id.cb_smile_baby://836
//                for rider baby, there is nothing to disable
                break;
            case R.id.cb_smile_early_stage_ci_99://837
                ac_pup_smile_early_stage_ci_99.setEnabled(isChecked);
                break;
            case R.id.cb_smile_medical_plus_benefit_as_charge://838
                ac_smile_medical_plus_benefit_as_charge.setEnabled(isChecked);
                tv_peserta_smile_medical_plus_benefit_as_charge.setEnabled(isChecked);
                tv_peserta_detail_smile_medical_plus_benefit_as_charge.setEnabled(isChecked);

                if(cb_smile_medical_plus_benefit_as_charge.isChecked()){
                    ll_smile_medical_plus_benefit_inner_limit.setVisibility(View.VISIBLE);
                    ac_smile_medical_plus_benefit_inner_limit.setEnabled(false);
                    tv_peserta_smile_medical_plus_benefit_inner_limit.setEnabled(false);
                    tv_peserta_detail_smile_medical_plus_benefit_inner_limit.setEnabled(false);
                } else {
                    ll_smile_medical_plus_benefit_inner_limit.setVisibility(View.GONE);
                    ac_smile_medical_plus_benefit_inner_limit.setEnabled(false);
                    tv_peserta_smile_medical_plus_benefit_inner_limit.setEnabled(false);
                    tv_peserta_detail_smile_medical_plus_benefit_inner_limit.setEnabled(false);
                    cb_smile_medical_plus_benefit_inner_limit.setChecked(false);
                    ll_medical_plus_benefit_inner_limit_cb.setVisibility(View.GONE);
                    cb_penunjang_kesehatan.setChecked(false);
                    cb_rawat_bersalin.setChecked(false);
                    cb_rawat_gigi.setChecked(false);
                }

                break;
            case R.id.cb_smile_medical_plus_benefit_inner_limit://839
                ll_medical_plus_benefit_inner_limit_cb.setVisibility(View.VISIBLE);
                ll_medical_plus_benefit_inner_limit_cb.setEnabled(true);
                ac_smile_medical_plus_benefit_inner_limit.setEnabled(false);
                tv_peserta_smile_medical_plus_benefit_inner_limit.setEnabled(isChecked);
                tv_peserta_detail_smile_medical_plus_benefit_inner_limit.setEnabled(isChecked);
                cb_penunjang_kesehatan.setEnabled(isChecked);
                cb_rawat_bersalin.setEnabled(isChecked);
                cb_rawat_gigi.setEnabled(isChecked);
                break;
            default:
                throw new IllegalArgumentException("id nya belum ke set");

        }
    }

    @BindView(R.id.ac_resiko_smile_personal_accident)
    AutoCompleteTextView ac_resiko_smile_personal_accident;
    @BindView(R.id.ac_kelas_smile_personal_accident)
    AutoCompleteTextView ac_kelas_smile_personal_accident;
    @BindView(R.id.ac_jumlah_unit_smile_personal_accident)
    AutoCompleteTextView ac_jumlah_unit_smile_personal_accident;
    @BindView(R.id.ac_smile_hospital_protection_family)
    AutoCompleteTextView ac_smile_hospital_protection_family;
    @BindView(R.id.ac_pup_smile_critical_illness)
    AutoCompleteTextView ac_pup_smile_critical_illness;
    @BindView(R.id.ac_smile_medical_benefit_as_charge_provider)
    AutoCompleteTextView ac_smile_medical_benefit_as_charge_provider;
    @BindView(R.id.ac_smile_medical_plus_benefit_as_charge)
    AutoCompleteTextView ac_smile_medical_plus_benefit_as_charge;
    @BindView(R.id.ac_smile_medical_plus_benefit_inner_limit)
    AutoCompleteTextView ac_smile_medical_plus_benefit_inner_limit;
    @BindView(R.id.ac_smile_hospital_protection)
    AutoCompleteTextView ac_smile_hospital_protection;
    @BindView(R.id.ac_smile_medical_benefit_inner_limit_provider)
    AutoCompleteTextView ac_smile_medical_benefit_inner_limit_provider;
    @BindView(R.id.ac_smile_hospital_protection_plus)
    AutoCompleteTextView ac_smile_hospital_protection_plus;
    @BindView(R.id.ac_paket_smile_ladies_insurance)
    AutoCompleteTextView ac_paket_smile_ladies_insurance;
    @BindView(R.id.ac_pup_smile_ladies_insurance)
    AutoCompleteTextView ac_pup_smile_ladies_insurance;
    @BindView(R.id.ac_smile_ladies_hospital_protection)
    AutoCompleteTextView ac_smile_ladies_hospital_protection;
    @BindView(R.id.ac_smile_ladies_medical_benefit_as_charge_provider)
    AutoCompleteTextView ac_smile_ladies_medical_benefit_as_charge_provider;
    @BindView(R.id.ac_smile_ladies_medical_benefit_inner_limit_provider)
    AutoCompleteTextView ac_smile_ladies_medical_benefit_inner_limit_provider;
    @BindView(R.id.ac_plan_smile_scholarship)
    AutoCompleteTextView ac_plan_smile_scholarship;
    @BindView(R.id.ac_up_smile_scholarship)
    AutoCompleteTextView ac_up_smile_scholarship;
    @BindView(R.id.ac_paket_smile_baby)
    AutoCompleteTextView ac_paket_smile_baby;
    @BindView(R.id.ac_bk_smile_baby)
    AutoCompleteTextView ac_bk_smile_baby;
    @BindView(R.id.ac_pup_smile_early_stage_ci_99)
    AutoCompleteTextView ac_pup_smile_early_stage_ci_99;
    @BindView(R.id.ac_smile_waiver_55_60_65_tpd_ci)
    AutoCompleteTextView ac_smile_waiver_55_60_65_tpd_ci;
    @BindView(R.id.ac_smile_medical_extra)
    AutoCompleteTextView ac_smile_medical_extra;


    @BindView(R.id.tv_baca_info_kelas_smile_personal_accident)
    TextView tv_baca_info_kelas_smile_personal_accident;
    @BindView(R.id.tv_info_kelas_smile_personal_accident)
    TextView tv_info_kelas_smile_personal_accident;
    @BindView(R.id.tv_peserta_detail_smile_hospital_protection_family)
    TextView tv_peserta_detail_smile_hospital_protection_family;
    @BindView(R.id.tv_peserta_detail_smile_medical_benefit_as_charge_provider)
    TextView tv_peserta_detail_smile_medical_benefit_as_charge_provider;
    @BindView(R.id.tv_peserta_detail_smile_medical_plus_benefit_as_charge)
    TextView tv_peserta_detail_smile_medical_plus_benefit_as_charge;
    @BindView(R.id.tv_peserta_detail_smile_medical_benefit_inner_limit_provider)
    TextView tv_peserta_detail_smile_medical_benefit_inner_limit_provider;
    @BindView(R.id.tv_peserta_detail_smile_medical_plus_benefit_inner_limit)
    TextView tv_peserta_detail_smile_medical_plus_benefit_inner_limit;
    @BindView(R.id.tv_peserta_detail_smile_hospital_protection_plus)
    TextView tv_peserta_detail_smile_hospital_protection_plus;
    @BindView(R.id.tv_peserta_detail_smile_ladies_hospital_protection)
    TextView tv_peserta_detail_smile_ladies_hospital_protection;
    @BindView(R.id.tv_peserta_detail_smile_ladies_medical_benefit_as_charge_provider)
    TextView tv_peserta_detail_smile_ladies_medical_benefit_as_charge_provider;
    @BindView(R.id.tv_peserta_detail_smile_ladies_medical_benefit_inner_limit_provider)
    TextView tv_peserta_detail_smile_ladies_medical_benefit_inner_limit_provider;
    @BindView(R.id.tv_peserta_detail_smile_medical_extra)
    TextView tv_peserta_detail_smile_medical_extra;


    @BindView(R.id.tv_peserta_smile_hospital_protection_family)
    TextView tv_peserta_smile_hospital_protection_family;
    @BindView(R.id.tv_peserta_smile_hospital_protection_plus)
    TextView tv_peserta_smile_hospital_protection_plus;
    @BindView(R.id.tv_peserta_smile_ladies_medical_benefit_as_charge_provider)
    TextView tv_peserta_smile_ladies_medical_benefit_as_charge_provider;
    @BindView(R.id.tv_peserta_smile_ladies_medical_benefit_inner_limit_provider)
    TextView tv_peserta_smile_ladies_medical_benefit_inner_limit_provider;
    @BindView(R.id.tv_peserta_smile_ladies_hospital_protection)
    TextView tv_peserta_smile_ladies_hospital_protection;
    @BindView(R.id.tv_peserta_smile_medical_benefit_as_charge_provider)
    TextView tv_peserta_smile_medical_benefit_as_charge_provider;
    @BindView(R.id.tv_peserta_smile_medical_benefit_inner_limit_provider)
    TextView tv_peserta_smile_medical_benefit_inner_limit_provider;
    @BindView(R.id.tv_peserta_smile_medical_plus_benefit_inner_limit)
    TextView tv_peserta_smile_medical_plus_benefit_inner_limit;
    @BindView(R.id.tv_peserta_smile_medical_plus_benefit_as_charge)
    TextView tv_peserta_smile_medical_plus_benefit_as_charge;
    @BindView(R.id.tv_peserta_smile_medical_extra)
    TextView tv_peserta_smile_medical_extra;

    @BindView(R.id.et_smile_term_rider)
    EditText et_smile_term_rider;


    @BindView(R.id.ll_smile_personal_accident)
    LinearLayout ll_smile_personal_accident;
    @BindView(R.id.ll_smile_hospital_protection_family)
    LinearLayout ll_smile_hospital_protection_family;
    @BindView(R.id.ll_smile_payor_5_10_tpd_death)
    LinearLayout ll_smile_payor_5_10_tpd_death;
    @BindView(R.id.ll_smile_payor_5_10_ci)
    LinearLayout ll_smile_payor_5_10_ci;
    @BindView(R.id.ll_smile_waiver_premium_5_10_tpd)
    LinearLayout ll_smile_waiver_premium_5_10_tpd;
    @BindView(R.id.ll_waiver_premium_5_10_ci)
    LinearLayout ll_waiver_premium_5_10_ci;
    @BindView(R.id.ll_smile_total_permanent_disability_tpd)
    LinearLayout ll_smile_total_permanent_disability_tpd;
    @BindView(R.id.ll_smile_critical_illness)
    LinearLayout ll_smile_critical_illness;
    @BindView(R.id.ll_smile_term_rider)
    LinearLayout ll_smile_term_rider;
    @BindView(R.id.ll_smile_medical_benefit_as_charge_provider)
    LinearLayout ll_smile_medical_benefit_as_charge_provider;
    @BindView(R.id.ll_smile_medical_plus_benefit_as_charge)
    LinearLayout ll_smile_medical_plus_benefit_as_charge;
    @BindView(R.id.ll_smile_hospital_protection)
    LinearLayout ll_smile_hospital_protection;
    @BindView(R.id.ll_smile_hospital_protection_plus)
    LinearLayout ll_smile_hospital_protection_plus;
    @BindView(R.id.ll_smile_payor_5_10_tpd_ci_death)
    LinearLayout ll_smile_payor_5_10_tpd_ci_death;
    @BindView(R.id.ll_smile_payor_25_tpd_ci_death)
    LinearLayout ll_smile_payor_25_tpd_ci_death;
    @BindView(R.id.ll_smile_waiver_5_10_tpd_ci)
    LinearLayout ll_smile_waiver_5_10_tpd_ci;
    @BindView(R.id.ll_smile_waiver_55_60_65_tpd_ci)
    LinearLayout ll_smile_waiver_55_60_65_tpd_ci;
    @BindView(R.id.ll_smile_medical_benefit_inner_limit_provider)
    LinearLayout ll_smile_medical_benefit_inner_limit_provider;
    @BindView(R.id.ll_smile_medical_plus_benefit_inner_limit)
    LinearLayout ll_smile_medical_plus_benefit_inner_limit;
    @BindView(R.id.ll_smile_ladies_hospital_protection)
    LinearLayout ll_smile_ladies_hospital_protection;
    @BindView(R.id.ll_smile_ladies_insurance)
    LinearLayout ll_smile_ladies_insurance;
    @BindView(R.id.ll_smile_ladies_medical_benefit_as_charge_provider)
    LinearLayout ll_smile_ladies_medical_benefit_as_charge_provider;
    @BindView(R.id.ll_smile_ladies_medical_benefit_inner_limit_provider)
    LinearLayout ll_smile_ladies_medical_benefit_inner_limit_provider;
    @BindView(R.id.ll_smile_scholarship)
    LinearLayout ll_smile_scholarship;
    @BindView(R.id.ll_smile_baby)
    LinearLayout ll_smile_baby;
    @BindView(R.id.ll_smile_early_stage_ci_99)
    LinearLayout ll_smile_early_stage_ci_99;
    @BindView(R.id.ll_smile_medical_extra)
    LinearLayout ll_smile_medical_extra;
    @BindView(R.id.ll_medical_plus_ll_smile_medical_plus_benefit_inner_limit_cb)
    LinearLayout ll_medical_plus_benefit_inner_limit_cb;


    @BindView(R.id.cb_smile_personal_accident)
    CheckBox cb_smile_personal_accident;
    @BindView(R.id.cb_smile_hospital_protection_family)
    CheckBox cb_smile_hospital_protection_family;
    @BindView(R.id.cb_smile_payor_5_10_tpd_death)
    CheckBox cb_smile_payor_5_10_tpd_death;
    @BindView(R.id.cb_smile_payor_5_10_ci)
    CheckBox cb_smile_payor_5_10_ci;
    @BindView(R.id.cb_smile_waiver_premium_5_10_tpd)
    CheckBox cb_smile_waiver_premium_5_10_tpd;
    @BindView(R.id.cb_waiver_premium_5_10_ci)
    CheckBox cb_waiver_premium_5_10_ci;
    @BindView(R.id.cb_smile_total_permanent_disability_tpd)
    CheckBox cb_smile_total_permanent_disability_tpd;
    @BindView(R.id.cb_smile_critical_illness)
    CheckBox cb_smile_critical_illness;
    @BindView(R.id.cb_smile_term_rider)
    CheckBox cb_smile_term_rider;
    @BindView(R.id.cb_smile_medical_benefit_as_charge_provider)
    CheckBox cb_smile_medical_benefit_as_charge_provider;
    @BindView(R.id.cb_smile_medical_plus_benefit_as_charge)
    CheckBox cb_smile_medical_plus_benefit_as_charge;
    @BindView(R.id.cb_smile_hospital_protection)
    CheckBox cb_smile_hospital_protection;
    @BindView(R.id.cb_smile_hospital_protection_plus)
    CheckBox cb_smile_hospital_protection_plus;
    @BindView(R.id.cb_smile_payor_5_10_tpd_ci_death)
    CheckBox cb_smile_payor_5_10_tpd_ci_death;
    @BindView(R.id.cb_smile_payor_25_tpd_ci_death)
    CheckBox cb_smile_payor_25_tpd_ci_death;
    @BindView(R.id.cb_smile_waiver_5_10_tpd_ci)
    CheckBox cb_smile_waiver_5_10_tpd_ci;
    @BindView(R.id.cb_smile_waiver_55_60_65_tpd_ci)
    CheckBox cb_smile_waiver_55_60_65_tpd_ci;
    @BindView(R.id.cb_smile_medical_benefit_inner_limit_provider)
    CheckBox cb_smile_medical_benefit_inner_limit_provider;
    @BindView(R.id.cb_smile_medical_plus_benefit_inner_limit)
    CheckBox cb_smile_medical_plus_benefit_inner_limit;
    @BindView(R.id.cb_smile_ladies_hospital_protection)
    CheckBox cb_smile_ladies_hospital_protection;
    @BindView(R.id.cb_smile_ladies_insurance)
    CheckBox cb_smile_ladies_insurance;
    @BindView(R.id.cb_smile_ladies_medical_benefit_as_charge_provider)
    CheckBox cb_smile_ladies_medical_benefit_as_charge_provider;
    @BindView(R.id.cb_smile_ladies_medical_benefit_inner_limit_provider)
    CheckBox cb_smile_ladies_medical_benefit_inner_limit_provider;
    @BindView(R.id.cb_smile_scholarship)
    CheckBox cb_smile_scholarship;
    @BindView(R.id.cb_smile_baby)
    CheckBox cb_smile_baby;
    @BindView(R.id.cb_smile_early_stage_ci_99)
    CheckBox cb_smile_early_stage_ci_99;
    @BindView(R.id.cb_smile_medical_extra)
    CheckBox cb_smile_medical_extra;
    @BindView(R.id.cb_rawat_gigi)
    CheckBox cb_rawat_gigi;
    @BindView(R.id.cb_rawat_bersalin)
    CheckBox cb_rawat_bersalin;
    @BindView(R.id.cb_penunjang_kesehatan)
    CheckBox cb_penunjang_kesehatan;


    @BindView(R.id.tv_error_smile_personal_accident)
    TextView tv_error_smile_personal_accident;
    @BindView(R.id.tv_error_smile_hospital_protection_family)
    TextView tv_error_smile_hospital_protection_family;
    @BindView(R.id.tv_error_smile_payor_5_10_tpd_death)
    TextView tv_error_smile_payor_5_10_tpd_death;
    @BindView(R.id.tv_error_smile_payor_5_10_ci)
    TextView tv_error_smile_payor_5_10_ci;
    @BindView(R.id.tv_error_smile_waiver_premium_5_10_tpd)
    TextView tv_error_smile_waiver_premium_5_10_tpd;
    @BindView(R.id.tv_error_waiver_premium_5_10_ci)
    TextView tv_error_waiver_premium_5_10_ci;
    @BindView(R.id.tv_error_smile_total_permanent_disability_tpd)
    TextView tv_error_smile_total_permanent_disability_tpd;
    @BindView(R.id.tv_error_smile_critical_illness)
    TextView tv_error_smile_critical_illness;
    @BindView(R.id.tv_error_smile_term_rider)
    TextView tv_error_smile_term_rider;
    @BindView(R.id.tv_error_smile_medical_benefit_as_charge_provider)
    TextView tv_error_smile_medical_benefit_as_charge_provider;
    @BindView(R.id.tv_error_smile_hospital_protection)
    TextView tv_error_smile_hospital_protection;
    @BindView(R.id.tv_error_smile_hospital_protection_plus)
    TextView tv_error_smile_hospital_protection_plus;
    @BindView(R.id.tv_error_smile_payor_5_10_tpd_ci_death)
    TextView tv_error_smile_payor_5_10_tpd_ci_death;
    @BindView(R.id.tv_error_smile_payor_25_tpd_ci_death)
    TextView tv_error_smile_payor_25_tpd_ci_death;
    @BindView(R.id.tv_error_smile_waiver_5_10_tpd_ci)
    TextView tv_error_smile_waiver_5_10_tpd_ci;
    @BindView(R.id.tv_error_smile_waiver_55_60_65_tpd_ci)
    TextView tv_error_smile_waiver_55_60_65_tpd_ci;
    @BindView(R.id.tv_error_smile_medical_benefit_inner_limit_provider)
    TextView tv_error_smile_medical_benefit_inner_limit_provider;
    @BindView(R.id.tv_error_smile_ladies_hospital_protection)
    TextView tv_error_smile_ladies_hospital_protection;
    @BindView(R.id.tv_error_smile_ladies_insurance)
    TextView tv_error_smile_ladies_insurance;
    @BindView(R.id.tv_error_smile_ladies_medical_benefit_as_charge_provider)
    TextView tv_error_smile_ladies_medical_benefit_as_charge_provider;
    @BindView(R.id.tv_error_smile_ladies_medical_benefit_inner_limit_provider)
    TextView tv_error_smile_ladies_medical_benefit_inner_limit_provider;
    @BindView(R.id.tv_error_smile_scholarship)
    TextView tv_error_smile_scholarship;
    @BindView(R.id.tv_error_smile_baby)
    TextView tv_error_smile_baby;
    @BindView(R.id.tv_error_smile_early_stage_ci_99)
    TextView tv_error_smile_early_stage_ci_99;
    @BindView(R.id.tv_error_smile_medical_plus_benefit_as_charge)
    TextView tv_error_smile_medical_plus_benefit_as_charge;
    @BindView(R.id.tv_error_smile_medical_plus_benefit_inner_limit)
    TextView tv_error_smile_medical_plus_benefit_inner_limit;
    @BindView(R.id.tv_error_smile_medical_extra)
    TextView tv_error_smile_medical_extra;
    @BindView(R.id.scrollView)
    ScrollView scrollView;


//    public void attemptReset() {
//        mstProposalProductRiderModels = new ArrayList<>();
//
//        if (StaticMethods.isViewVisible(ll_smile_personal_accident)) {
//            cb_smile_personal_accident.setChecked(false); //810
//            tv_error_smile_personal_accident.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_hospital_protection)) {
//            cb_smile_hospital_protection.setChecked(false); //811
//            tv_error_smile_hospital_protection.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_total_permanent_disability_tpd)) {
//            cb_smile_total_permanent_disability_tpd.setChecked(false); //812
//            tv_error_smile_total_permanent_disability_tpd.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_critical_illness)) {
//            cb_smile_critical_illness.setChecked(false); //813
//            tv_error_smile_critical_illness.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_waiver_premium_5_10_tpd)) {
//            cb_smile_waiver_premium_5_10_tpd.setChecked(false); //814
//
//            tv_error_smile_waiver_premium_5_10_tpd.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_payor_5_10_tpd_death)) {
//            cb_smile_payor_5_10_tpd_death.setChecked(false); //815
//            tv_error_smile_payor_5_10_tpd_death.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_waiver_premium_5_10_ci)) {
//            cb_waiver_premium_5_10_ci.setChecked(false); //816
//            tv_error_waiver_premium_5_10_ci.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_payor_5_10_ci)) {
//            cb_smile_payor_5_10_ci.setChecked(false); //817
//            tv_error_smile_payor_5_10_ci.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_term_rider)) {
//            cb_smile_term_rider.setChecked(false); //818
//            tv_error_smile_term_rider.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_hospital_protection_family)) {
//            cb_smile_hospital_protection_family.setChecked(false); //819
//            tv_error_smile_hospital_protection_family.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_medical_benefit_as_charge_provider)) {
//            cb_smile_medical_benefit_as_charge_provider.setChecked(false); //823
//            tv_error_smile_medical_benefit_as_charge_provider.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_medical_benefit_inner_limit_provider)) {
//            cb_smile_medical_benefit_inner_limit_provider.setChecked(false); //825
//            tv_error_smile_medical_benefit_inner_limit_provider.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_hospital_protection_plus)) {
//            cb_smile_hospital_protection_plus.setChecked(false); //826
//            tv_error_smile_hospital_protection_plus.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_waiver_5_10_tpd_ci)) {
//            cb_smile_waiver_5_10_tpd_ci.setChecked(false); //827
//            tv_error_smile_waiver_5_10_tpd_ci.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_payor_5_10_tpd_ci_death)) {
//            cb_smile_payor_5_10_tpd_ci_death.setChecked(false); //828
//            tv_error_smile_payor_5_10_tpd_ci_death.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_ladies_insurance)) {
//            cb_smile_ladies_insurance.setChecked(false); //830
//            tv_error_smile_ladies_insurance.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_ladies_hospital_protection)) {
//            cb_smile_ladies_hospital_protection.setChecked(false); //831
//            tv_error_smile_ladies_hospital_protection.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_ladies_medical_benefit_as_charge_provider)) {
//            cb_smile_ladies_medical_benefit_as_charge_provider.setChecked(false); //832
//            tv_error_smile_ladies_medical_benefit_as_charge_provider.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_ladies_medical_benefit_inner_limit_provider)) {
//            cb_smile_ladies_medical_benefit_inner_limit_provider.setChecked(false); //833
//            tv_error_smile_ladies_medical_benefit_inner_limit_provider.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_scholarship)) {
//            cb_smile_scholarship.setChecked(false); //835
//            tv_error_smile_scholarship.setText(null);
//        }
//        if (StaticMethods.isViewVisible(ll_smile_early_stage_ci_99)) {
//            cb_smile_early_stage_ci_99.setChecked(false); //837
//            tv_error_smile_early_stage_ci_99.setText(null);
//        }
//    }

}
