package id.co.ajsmsig.nb.prop.database;
/*
 *Created by faiz_f on 10/05/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.HashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;

public class TableDataProposal {
    private Context context;

    public TableDataProposal(Context context) {
        this.context = context;
    }

    public HashMap<String, String> getProjectionMapJoinProposalProduct() {
        HashMap<String, String> projectionMap = new HashMap<>();
        projectionMap.put(context.getString(R.string._ID), context.getString(R.string._ID));
        projectionMap.put(context.getString(R.string.MST_DATA_PROPOSAL) + "." + context.getString(R.string.NO_PROPOSAL_TAB), context.getString(R.string.SOME_QUERY_FOR_NO_PROPOSAL));
        projectionMap.put(context.getString(R.string.NAMA_PP), context.getString(R.string.NAMA_PP));
        projectionMap.put(context.getString(R.string.NAMA_TT), context.getString(R.string.NAMA_TT));
        projectionMap.put(context.getString(R.string.LSBS_ID), context.getString(R.string.LSBS_ID));
        projectionMap.put(context.getString(R.string.LSDBS_NUMBER), context.getString(R.string.LSDBS_NUMBER));
        projectionMap.put(context.getString(R.string.PREMI), context.getString(R.string.PREMI));
        projectionMap.put(context.getString(R.string.LKU_ID), context.getString(R.string.LKU_ID));
        projectionMap.put(context.getString(R.string.UP), context.getString(R.string.UP));
        projectionMap.put(context.getString(R.string.FLAG_FINISH), context.getString(R.string.FLAG_FINISH));
        projectionMap.put(context.getString(R.string.LAST_PAGE_POSITION), context.getString(R.string.LAST_PAGE_POSITION));
        projectionMap.put(context.getString(R.string.FLAG_STAR), context.getString(R.string.FLAG_STAR));
        projectionMap.put(context.getString(R.string.FLAG_PACKET), context.getString(R.string.FLAG_PACKET));

        return projectionMap;
    }

    public ContentValues getContentValues(P_MstDataProposalModel p_mstDataProposalModel) {
        ContentValues cv = new ContentValues();
        cv.put(context.getString(R.string.NO_PROPOSAL_TAB), p_mstDataProposalModel.getNo_proposal_tab());
        cv.put(context.getString(R.string.NO_PROPOSAL), p_mstDataProposalModel.getNo_proposal());
        cv.put(context.getString(R.string.NAMA_PP), p_mstDataProposalModel.getNama_pp());
        cv.put(context.getString(R.string.TGL_LAHIR_PP), p_mstDataProposalModel.getTgl_lahir_pp());
        cv.put(context.getString(R.string.UMUR_PP), p_mstDataProposalModel.getUmur_pp());
        cv.put(context.getString(R.string.SEX_PP), p_mstDataProposalModel.getSex_pp());
        cv.put(context.getString(R.string.NAMA_TT), p_mstDataProposalModel.getNama_tt());
        cv.put(context.getString(R.string.TGL_LAHIR_TT), p_mstDataProposalModel.getTgl_lahir_tt());
        cv.put(context.getString(R.string.UMUR_TT), p_mstDataProposalModel.getUmur_tt());
        cv.put(context.getString(R.string.SEX_TT), p_mstDataProposalModel.getSex_tt());
        cv.put(context.getString(R.string.NAMA_AGEN), p_mstDataProposalModel.getNama_agen());
        cv.put(context.getString(R.string.MSAG_ID), p_mstDataProposalModel.getMsag_id());
        cv.put(context.getString(R.string.TGL_INPUT), p_mstDataProposalModel.getTgl_input());
        cv.put(context.getString(R.string.FLAG_AKTIF), p_mstDataProposalModel.getFlag_aktif());
        cv.put(context.getString(R.string.FLAG_TEST), p_mstDataProposalModel.getFlag_test());
        cv.put(context.getString(R.string.NAMA_USER), p_mstDataProposalModel.getNama_user());
        cv.put(context.getString(R.string.KODE_AGEN), p_mstDataProposalModel.getKode_agen());
        cv.put(context.getString(R.string.LSTB_ID), p_mstDataProposalModel.getLstb_id());
        cv.put(context.getString(R.string.ID_DIST), p_mstDataProposalModel.getId_dist());
        cv.put(context.getString(R.string.JENIS_ID), p_mstDataProposalModel.getJenis_id());
        cv.put(context.getString(R.string.LEAD_ID), p_mstDataProposalModel.getLead_id());
        cv.put(context.getString(R.string.LEAD_TAB_ID), p_mstDataProposalModel.getLead_tab_id());
        cv.put(context.getString(R.string.PROSPECT_ID), p_mstDataProposalModel.getProspect_id());
        cv.put(context.getString(R.string.FLAG_TT_CALON_BAYI), p_mstDataProposalModel.getFlag_tt_calon_bayi());
        cv.put(context.getString(R.string.FLAG_FINISH), p_mstDataProposalModel.getFlag_finish());
        cv.put(context.getString(R.string.FLAG_STAR), p_mstDataProposalModel.getFlag_star());
        cv.put(context.getString(R.string.LAST_PAGE_POSITION), p_mstDataProposalModel.getLast_page_position());
        cv.put(context.getString(R.string.FLAG_PROPER), p_mstDataProposalModel.getFlag_proper());
        cv.put(context.getString(R.string.FLAG_PACKET), p_mstDataProposalModel.getFlag_packet());
        cv.put(context.getString(R.string.va), p_mstDataProposalModel.getVirtual_acc());
        cv.put(context.getString(R.string.noid_member), p_mstDataProposalModel.getNoid());
        cv.put(context.getString(R.string.flag_pos), p_mstDataProposalModel.getFlag_pos());

        return cv;
    }

    public P_MstDataProposalModel getObject(Cursor cursor) {
        P_MstDataProposalModel p_mstDataProposalModel = new P_MstDataProposalModel();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string._ID)))) {
                p_mstDataProposalModel.setId(cursor.getLong(cursor.getColumnIndexOrThrow(context.getString(R.string._ID))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB)))) {
                p_mstDataProposalModel.setNo_proposal_tab(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL)))) {
                p_mstDataProposalModel.setNo_proposal(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NAMA_PP)))) {
                p_mstDataProposalModel.setNama_pp(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NAMA_PP))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.TGL_LAHIR_PP)))) {
                p_mstDataProposalModel.setTgl_lahir_pp(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.TGL_LAHIR_PP))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.UMUR_PP)))) {
                p_mstDataProposalModel.setUmur_pp(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.UMUR_PP))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SEX_PP)))) {
                p_mstDataProposalModel.setSex_pp(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.SEX_PP))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NAMA_TT)))) {
                p_mstDataProposalModel.setNama_tt(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NAMA_TT))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.TGL_LAHIR_TT)))) {
                p_mstDataProposalModel.setTgl_lahir_tt(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.TGL_LAHIR_TT))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.UMUR_TT)))) {
                p_mstDataProposalModel.setUmur_tt(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.UMUR_TT))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.SEX_TT)))) {
                p_mstDataProposalModel.setSex_tt(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.SEX_TT))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NAMA_AGEN)))) {
                p_mstDataProposalModel.setNama_agen(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NAMA_AGEN))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.MSAG_ID)))) {
                p_mstDataProposalModel.setMsag_id(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.MSAG_ID))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.TGL_INPUT)))) {
                p_mstDataProposalModel.setTgl_input(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.TGL_INPUT))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_AKTIF)))) {
                p_mstDataProposalModel.setFlag_aktif(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_AKTIF))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_TEST)))) {
                p_mstDataProposalModel.setFlag_test(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_TEST))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NAMA_USER)))) {
                p_mstDataProposalModel.setNama_user(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NAMA_USER))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.KODE_AGEN)))) {
                p_mstDataProposalModel.setKode_agen(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.KODE_AGEN))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LSTB_ID)))) {
                p_mstDataProposalModel.setLstb_id(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LSTB_ID))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.ID_DIST)))) {
                p_mstDataProposalModel.setId_dist(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.ID_DIST))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.JENIS_ID)))) {
                p_mstDataProposalModel.setJenis_id(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.JENIS_ID))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LEAD_ID)))) {
                p_mstDataProposalModel.setLead_id(cursor.getLong(cursor.getColumnIndexOrThrow(context.getString(R.string.LEAD_ID))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LEAD_TAB_ID)))) {
                p_mstDataProposalModel.setLead_tab_id(cursor.getLong(cursor.getColumnIndexOrThrow(context.getString(R.string.LEAD_TAB_ID))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.PROSPECT_ID)))) {
                p_mstDataProposalModel.setProspect_id(cursor.getLong(cursor.getColumnIndexOrThrow(context.getString(R.string.PROSPECT_ID))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_TT_CALON_BAYI)))) {
                p_mstDataProposalModel.setFlag_tt_calon_bayi(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_TT_CALON_BAYI))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_FINISH)))) {
                p_mstDataProposalModel.setFlag_finish(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_FINISH))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_STAR)))) {
                p_mstDataProposalModel.setFlag_star(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_STAR))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LAST_PAGE_POSITION)))) {
                p_mstDataProposalModel.setLast_page_position(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LAST_PAGE_POSITION))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_PROPER)))) {
                p_mstDataProposalModel.setFlag_proper(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_PROPER))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_PACKET)))) {
                p_mstDataProposalModel.setFlag_packet(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.FLAG_PACKET))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.va)))) {
                p_mstDataProposalModel.setVirtual_acc(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.va))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.noid_member)))) {
                p_mstDataProposalModel.setNoid(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.noid_member))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.flag_pos)))) {
                p_mstDataProposalModel.setFlag_pos(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.flag_pos))));
            }
            // always close the cursor
            cursor.close();
        }

        return p_mstDataProposalModel;
    }
}
