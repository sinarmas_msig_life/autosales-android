package id.co.ajsmsig.nb.prop.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.activity.P_IllustrationActivity;
import id.co.ajsmsig.nb.prop.adapter.P_AdapterListIlustrasiFund;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;

/**
 * Created by faiz_f on 03/02/2017.
 */

public class P_InvestationIllustraionFragment extends Fragment {
    public P_InvestationIllustraionFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.p_fragment_investation_illustration, container, false);


        ListView lv_ilustrasi_fund = view.findViewById(R.id.lv_ilustrasi_fund);
        View footer = inflater.inflate(R.layout.p_fragment_investation_illustration_footer, lv_ilustrasi_fund, false);
        lv_ilustrasi_fund.addFooterView(footer);

        TextView tv_total_rendah = footer.findViewById(R.id.tv_total_rendah);
        TextView tv_total_sedang = footer.findViewById(R.id.tv_total_sedang);
        TextView tv_total_tinggi = footer.findViewById(R.id.tv_total_tinggi);

        P_ProposalModel proposalModel = P_IllustrationActivity.myBundle.getParcelable(getString(R.string.proposal_model));
        assert proposalModel != null;
        ArrayList<HashMap<String, Object>> listInvestRateAllocation = new P_Select(getContext()).selectInvestRateAssumption(proposalModel.getMst_data_proposal().getNo_proposal_tab());

//        Log.d(TAG, "listInvestRateAllocation: " + listInvestRateAllocation);

        P_AdapterListIlustrasiFund propAdapterListIlustrasiFund = new P_AdapterListIlustrasiFund(getContext(), R.layout.p_fragment_investation_illustration_item, listInvestRateAllocation);
        lv_ilustrasi_fund.setAdapter(propAdapterListIlustrasiFund);

        double totalAlokasiRendah = 0;
        double totalAlokasiSedang = 0;
        double totalAlokasiTinggi = 0;

        for (HashMap<String, Object> investRateAllocation : listInvestRateAllocation) {
            totalAlokasiRendah += (Double) investRateAllocation.get(getString(R.string.ALLOCATION_1));
            totalAlokasiSedang += (Double) investRateAllocation.get(getString(R.string.ALLOCATION_2));
            totalAlokasiTinggi += (Double) investRateAllocation.get(getString(R.string.ALLOCATION_3));
        }

        NumberFormat formatter = new DecimalFormat("0.00");
        String textTotalLokasiRendah = formatter.format(totalAlokasiRendah) + "%";
        String textTotalLokasiSedang = formatter.format(totalAlokasiSedang) + "%";
        String textTotalLokasiTinggi = formatter.format(totalAlokasiTinggi) + "%";
        tv_total_rendah.setText(textTotalLokasiRendah);
        tv_total_sedang.setText(textTotalLokasiSedang);
        tv_total_tinggi.setText(textTotalLokasiTinggi);

        return view;
    }


}
