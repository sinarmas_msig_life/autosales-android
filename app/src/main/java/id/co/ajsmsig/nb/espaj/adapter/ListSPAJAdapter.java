package id.co.ajsmsig.nb.espaj.adapter;

import android.content.Context;
import android.database.Cursor;
import android.widget.SimpleCursorTreeAdapter;

/**
 * Created by eriza on 07/09/2017.
 */

public class ListSPAJAdapter extends SimpleCursorTreeAdapter {


    public ListSPAJAdapter(Context context, Cursor cursor, int collapsedGroupLayout, int expandedGroupLayout, String[] groupFrom, int[] groupTo, int childLayout, int lastChildLayout, String[] childFrom, int[] childTo) {
        super(context, cursor, collapsedGroupLayout, expandedGroupLayout, groupFrom, groupTo, childLayout, lastChildLayout, childFrom, childTo);
    }

    @Override
    protected Cursor getChildrenCursor(Cursor cursor) {
        return null;
    }
}
