package id.co.ajsmsig.nb.crm.model.form;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by faiz_f on 12/04/2017.
 */

public class SimbakAccountModel implements Parcelable {
    private Long SA3_TAB_ID;
    private Long SA3_ID;
    private String SA3_NAME;
    private String SA3_BDATE;
    private String SA3_BPLACE;
    private String SA3_REMARK;
    private String SA3_CREATE;
    private String SA3_CRTDID;
    private String SA3_UPDATE;
    private String SA3_UPTDID;
    private Integer SA3_NOSTAFF;
    private String SA3_NPWP_EXP;
    private String SA3_NPWP_DATE;
    private String SA3_SIUP;
    private String SA3_SIUP_DATE;
    private String SA3_SIUP_EXP;
    private String SA3_NPWP;
    private String SA3_OLD_ID;
    private Integer SA3_ABBRID;
    private Integer SA3_SRC;
    private Integer SA3_APP;
    private Integer SA3_ACTIVE;
    private Integer SA3_ASSET;
    private Integer SA3_INCOME;
    private Integer SA3_SRC2;
    private Integer SA3_VIA;
    private Integer SA3_RAW;
    private Integer SA3_LEADER;
    private Integer SA3_EST_PREMI;
    private String SA3_KOMPETITOR;
    private String SA3_REFF_NAME;
    private Integer SA3_EST_BUDGET;
    private String SA3_EFEKTIF_DATE;
    private Integer SA3_CATID;
    private Integer SA3_LAST_POS;
    private Integer SA3_FAV;

    public SimbakAccountModel() {
    }


    protected SimbakAccountModel(Parcel in) {
        SA3_TAB_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SA3_ID = (Long) in.readValue(Integer.class.getClassLoader());
        SA3_NAME = in.readString();
        SA3_BDATE = in.readString();
        SA3_BPLACE = in.readString();
        SA3_REMARK = in.readString();
        SA3_CREATE = in.readString();
        SA3_CRTDID = in.readString();
        SA3_UPDATE = in.readString();
        SA3_UPTDID = in.readString();
        SA3_NOSTAFF = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_NPWP_EXP = in.readString();
        SA3_NPWP_DATE = in.readString();
        SA3_SIUP = in.readString();
        SA3_SIUP_DATE = in.readString();
        SA3_SIUP_EXP = in.readString();
        SA3_NPWP = in.readString();
        SA3_OLD_ID = in.readString();
        SA3_ABBRID = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_SRC = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_APP = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_ACTIVE = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_ASSET = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_INCOME = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_SRC2 = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_VIA = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_RAW = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_LEADER = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_EST_PREMI = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_KOMPETITOR = in.readString();
        SA3_REFF_NAME = in.readString();
        SA3_EST_BUDGET = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_EFEKTIF_DATE = in.readString();
        SA3_CATID = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_LAST_POS = (Integer) in.readValue(Integer.class.getClassLoader());
        SA3_FAV = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<SimbakAccountModel> CREATOR = new Creator<SimbakAccountModel>() {
        @Override
        public SimbakAccountModel createFromParcel(Parcel in) {
            return new SimbakAccountModel(in);
        }

        @Override
        public SimbakAccountModel[] newArray(int size) {
            return new SimbakAccountModel[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(SA3_TAB_ID);
        dest.writeValue(SA3_ID);
        dest.writeString(SA3_NAME);
        dest.writeString(SA3_BDATE);
        dest.writeString(SA3_BPLACE);
        dest.writeString(SA3_REMARK);
        dest.writeString(SA3_CREATE);
        dest.writeString(SA3_CRTDID);
        dest.writeString(SA3_UPDATE);
        dest.writeString(SA3_UPTDID);
        dest.writeValue(SA3_NOSTAFF);
        dest.writeString(SA3_NPWP_EXP);
        dest.writeString(SA3_NPWP_DATE);
        dest.writeString(SA3_SIUP);
        dest.writeString(SA3_SIUP_DATE);
        dest.writeString(SA3_SIUP_EXP);
        dest.writeString(SA3_NPWP);
        dest.writeString(SA3_OLD_ID);
        dest.writeValue(SA3_ABBRID);
        dest.writeValue(SA3_SRC);
        dest.writeValue(SA3_APP);
        dest.writeValue(SA3_ACTIVE);
        dest.writeValue(SA3_ASSET);
        dest.writeValue(SA3_INCOME);
        dest.writeValue(SA3_SRC2);
        dest.writeValue(SA3_VIA);
        dest.writeValue(SA3_RAW);
        dest.writeValue(SA3_LEADER);
        dest.writeValue(SA3_EST_PREMI);
        dest.writeString(SA3_KOMPETITOR);
        dest.writeString(SA3_REFF_NAME);
        dest.writeValue(SA3_EST_BUDGET);
        dest.writeString(SA3_EFEKTIF_DATE);
        dest.writeValue(SA3_CATID);
        dest.writeValue(SA3_LAST_POS);
        dest.writeValue(SA3_FAV);
    }


    public Long getSA3_TAB_ID() {
        return SA3_TAB_ID;
    }

    public void setSA3_TAB_ID(Long SA3_TAB_ID) {
        this.SA3_TAB_ID = SA3_TAB_ID;
    }

    public Long getSA3_ID() {
        return SA3_ID;
    }

    public void setSA3_ID(Long SA3_ID) {
        this.SA3_ID = SA3_ID;
    }

    public String getSA3_NAME() {
        return SA3_NAME;
    }

    public void setSA3_NAME(String SA3_NAME) {
        this.SA3_NAME = SA3_NAME;
    }

    public String getSA3_BDATE() {
        return SA3_BDATE;
    }

    public void setSA3_BDATE(String SA3_BDATE) {
        this.SA3_BDATE = SA3_BDATE;
    }

    public String getSA3_BPLACE() {
        return SA3_BPLACE;
    }

    public void setSA3_BPLACE(String SA3_BPLACE) {
        this.SA3_BPLACE = SA3_BPLACE;
    }

    public String getSA3_REMARK() {
        return SA3_REMARK;
    }

    public void setSA3_REMARK(String SA3_REMARK) {
        this.SA3_REMARK = SA3_REMARK;
    }

    public String getSA3_CREATE() {
        return SA3_CREATE;
    }

    public void setSA3_CREATE(String SA3_CREATE) {
        this.SA3_CREATE = SA3_CREATE;
    }

    public String getSA3_CRTDID() {
        return SA3_CRTDID;
    }

    public void setSA3_CRTDID(String SA3_CRTDID) {
        this.SA3_CRTDID = SA3_CRTDID;
    }

    public String getSA3_UPDATE() {
        return SA3_UPDATE;
    }

    public void setSA3_UPDATE(String SA3_UPDATE) {
        this.SA3_UPDATE = SA3_UPDATE;
    }

    public String getSA3_UPTDID() {
        return SA3_UPTDID;
    }

    public void setSA3_UPTDID(String SA3_UPTDID) {
        this.SA3_UPTDID = SA3_UPTDID;
    }

    public Integer getSA3_NOSTAFF() {
        return SA3_NOSTAFF;
    }

    public void setSA3_NOSTAFF(Integer SA3_NOSTAFF) {
        this.SA3_NOSTAFF = SA3_NOSTAFF;
    }

    public String getSA3_NPWP_EXP() {
        return SA3_NPWP_EXP;
    }

    public void setSA3_NPWP_EXP(String SA3_NPWP_EXP) {
        this.SA3_NPWP_EXP = SA3_NPWP_EXP;
    }

    public String getSA3_NPWP_DATE() {
        return SA3_NPWP_DATE;
    }

    public void setSA3_NPWP_DATE(String SA3_NPWP_DATE) {
        this.SA3_NPWP_DATE = SA3_NPWP_DATE;
    }

    public String getSA3_SIUP() {
        return SA3_SIUP;
    }

    public void setSA3_SIUP(String SA3_SIUP) {
        this.SA3_SIUP = SA3_SIUP;
    }

    public String getSA3_SIUP_DATE() {
        return SA3_SIUP_DATE;
    }

    public void setSA3_SIUP_DATE(String SA3_SIUP_DATE) {
        this.SA3_SIUP_DATE = SA3_SIUP_DATE;
    }

    public String getSA3_SIUP_EXP() {
        return SA3_SIUP_EXP;
    }

    public void setSA3_SIUP_EXP(String SA3_SIUP_EXP) {
        this.SA3_SIUP_EXP = SA3_SIUP_EXP;
    }

    public String getSA3_NPWP() {
        return SA3_NPWP;
    }

    public void setSA3_NPWP(String SA3_NPWP) {
        this.SA3_NPWP = SA3_NPWP;
    }

    public String getSA3_OLD_ID() {
        return SA3_OLD_ID;
    }

    public void setSA3_OLD_ID(String SA3_OLD_ID) {
        this.SA3_OLD_ID = SA3_OLD_ID;
    }

    public Integer getSA3_ABBRID() {
        return SA3_ABBRID;
    }

    public void setSA3_ABBRID(Integer SA3_ABBRID) {
        this.SA3_ABBRID = SA3_ABBRID;
    }

    public Integer getSA3_SRC() {
        return SA3_SRC;
    }

    public void setSA3_SRC(Integer SA3_SRC) {
        this.SA3_SRC = SA3_SRC;
    }

    public Integer getSA3_APP() {
        return SA3_APP;
    }

    public void setSA3_APP(Integer SA3_APP) {
        this.SA3_APP = SA3_APP;
    }

    public Integer getSA3_ACTIVE() {
        return SA3_ACTIVE;
    }

    public void setSA3_ACTIVE(Integer SA3_ACTIVE) {
        this.SA3_ACTIVE = SA3_ACTIVE;
    }

    public Integer getSA3_ASSET() {
        return SA3_ASSET;
    }

    public void setSA3_ASSET(Integer SA3_ASSET) {
        this.SA3_ASSET = SA3_ASSET;
    }

    public Integer getSA3_INCOME() {
        return SA3_INCOME;
    }

    public void setSA3_INCOME(Integer SA3_INCOME) {
        this.SA3_INCOME = SA3_INCOME;
    }

    public Integer getSA3_SRC2() {
        return SA3_SRC2;
    }

    public void setSA3_SRC2(Integer SA3_SRC2) {
        this.SA3_SRC2 = SA3_SRC2;
    }

    public Integer getSA3_VIA() {
        return SA3_VIA;
    }

    public void setSA3_VIA(Integer SA3_VIA) {
        this.SA3_VIA = SA3_VIA;
    }

    public Integer getSA3_RAW() {
        return SA3_RAW;
    }

    public void setSA3_RAW(Integer SA3_RAW) {
        this.SA3_RAW = SA3_RAW;
    }

    public Integer getSA3_LEADER() {
        return SA3_LEADER;
    }

    public void setSA3_LEADER(Integer SA3_LEADER) {
        this.SA3_LEADER = SA3_LEADER;
    }

    public Integer getSA3_EST_PREMI() {
        return SA3_EST_PREMI;
    }

    public void setSA3_EST_PREMI(Integer SA3_EST_PREMI) {
        this.SA3_EST_PREMI = SA3_EST_PREMI;
    }

    public String getSA3_KOMPETITOR() {
        return SA3_KOMPETITOR;
    }

    public void setSA3_KOMPETITOR(String SA3_KOMPETITOR) {
        this.SA3_KOMPETITOR = SA3_KOMPETITOR;
    }

    public String getSA3_REFF_NAME() {
        return SA3_REFF_NAME;
    }

    public void setSA3_REFF_NAME(String SA3_REFF_NAME) {
        this.SA3_REFF_NAME = SA3_REFF_NAME;
    }

    public Integer getSA3_EST_BUDGET() {
        return SA3_EST_BUDGET;
    }

    public void setSA3_EST_BUDGET(Integer SA3_EST_BUDGET) {
        this.SA3_EST_BUDGET = SA3_EST_BUDGET;
    }

    public String getSA3_EFEKTIF_DATE() {
        return SA3_EFEKTIF_DATE;
    }

    public void setSA3_EFEKTIF_DATE(String SA3_EFEKTIF_DATE) {
        this.SA3_EFEKTIF_DATE = SA3_EFEKTIF_DATE;
    }

    public Integer getSA3_CATID() {
        return SA3_CATID;
    }

    public void setSA3_CATID(Integer SA3_CATID) {
        this.SA3_CATID = SA3_CATID;
    }

    public Integer getSA3_LAST_POS() {
        return SA3_LAST_POS;
    }

    public void setSA3_LAST_POS(Integer SA3_LAST_POS) {
        this.SA3_LAST_POS = SA3_LAST_POS;
    }

    public Integer getSA3_FAV() {
        return SA3_FAV;
    }

    public void setSA3_FAV(Integer SA3_FAV) {
        this.SA3_FAV = SA3_FAV;
    }
}