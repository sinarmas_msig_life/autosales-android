package id.co.ajsmsig.nb.espaj.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.DBHelper;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.Referral;
import id.co.ajsmsig.nb.espaj.method.MethodTable;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBank;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBankCab;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelCabangDa;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelReferralDa;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.util.DateUtils;

/**
 * Created by eriza on 04/09/2017.
 */

public class E_Select {
    private final static String TAG = P_Select.class.getSimpleName();
    private DBHelper dbHelper;
    private Context context;

    public E_Select(Context context) {
        this.context = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.update_data_preferences), Context.MODE_PRIVATE);
        int version = sharedPreferences.getInt(context.getString(R.string.lav_data_id), 0);
        this.dbHelper = new DBHelper(context, version);
    }

    public ArrayList<ModelDropDownString> getLstJabatan() {
        ArrayList<ModelDropDownString> List = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT LJB_NOTE, LJB_ID FROM E_LST_JABATAN", null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelDropDownString(cursor.getString(0), cursor.getString(1)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return List;
    }

    public ArrayList<ModelDropdownInt> getLst_Pekerjaan(String cari_kerja) {
        ArrayList<ModelDropdownInt> List = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT LSP_ID, LSP_NAME FROM E_LST_PEKERJAAN WHERE "
                + "LSP_NAME LIKE '%" + cari_kerja + "%' AND LSP_NAME IS NOT NULL ORDER BY LSP_NAME ASC", null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelDropdownInt(cursor.getString(1), cursor.getInt(0)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return List;
    }

    public ArrayList<ModelDropdownInt> getLst_CaraBayar() {
        ArrayList<ModelDropdownInt> List = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT LSCB_ID, LSCB_PAY_MODE FROM LST_PAY_MODE WHERE LSCB_ID BETWEEN 0 AND 6", null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelDropdownInt(cursor.getString(1), cursor.getInt(0)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return List;
    }

    public ArrayList<ModelDropDownString> getLst_Kurs(int produk, int sub_produk) {
        ArrayList<ModelDropDownString> List = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("  SELECT DISTINCT B.LKU_ID,B.LKU_SYMBOL  FROM LST_PRODUCT_CALC A,LST_KURS B " +
                "WHERE A.LKU_ID = B.LKU_ID AND " +
                "A.PSET_ID = (select pset_id from LST_PRODUCT_SETUP where product_code =" + produk +
                " and plan = " + sub_produk + ")", null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelDropDownString(cursor.getString(1), cursor.getString(0)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return List;
    }

    public ArrayList<ModelDropDownString> getLst_Kombinasi(int produk, int sub_produk) {
        ArrayList<ModelDropDownString> List = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(" SELECT COMB_NAME, BASE_PREMIUM, TOPUP_PREMIUM " +
                "FROM LST_PRODUCT_PREMIUM_COMB A, LST_PRODUCT_SETUP B,LST_PREMIUM_COMBINATION C " +
                "WHERE A.PSET_ID = B.PSET_ID " +
                "AND A.LPP_ID = C.LPP_ID AND " +
                "B.PSET_ID = (select pset_id from LST_PRODUCT_SETUP where product_code =" + produk +
                " and plan = " + sub_produk + ")" +
                " ORDER BY BASE_PREMIUM DESC", null);
        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(0);
                String value = cursor.getString(1) + "% PP - " + cursor.getString(2) + "% PTB";
                List.add(new ModelDropDownString(value, id));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return List;
    }

    public ArrayList<ModelDropDownString> getLst_KombinasiNew(int produk, int sub_produk) {
        ArrayList<ModelDropDownString> List = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(" select COMB_NAME, BASE_PREMIUM, TOPUP_PREMIUM  from LST_PREMIUM_COMBINATION", null);
        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(0);
                String value = cursor.getString(1) + "% PP - " + cursor.getString(2) + "% PTB";
                List.add(new ModelDropDownString(value, id));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return List;
    }

    public ArrayList<ModelDropdownInt> getLstChannelDist(int JENIS_LOGIN, int JENIS_LOGIN_BC, String KODE_REG1, String KODE_REG2) {
        ArrayList<ModelDropdownInt> List = new ArrayList<>();
        String selectQuery = MethodTable.selectQueryJenis(JENIS_LOGIN, JENIS_LOGIN_BC,
                KODE_REG1, KODE_REG2);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelDropdownInt(cursor.getString(1), cursor.getInt(0)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return List;
    }

    public Cursor getfetchGroupSPAJ(Context context, String kode_agen, String idleadTab, int flag_view) {
        Cursor cursor = null;
        int count = 0;
        MatrixCursor matrixCursor = new MatrixCursor(new String[]{context.getString(R.string._ID), "COUNT"});
        if (flag_view == 0) { // flag 0 untuk halaman utama autosales
            cursor = dbHelper.getReadableDatabase().rawQuery("select count(*) from " + context.getString(R.string.TABLE_E_MST_SPAJ) +
                    " where KODE_AGEN = '" + kode_agen + "' and FLAG_AKTIF = 0", null);
            cursor.moveToFirst();
            count = cursor.getInt(0);
            matrixCursor.addRow(new Object[]{0 - 10, "Draft " + count});

            cursor = dbHelper.getReadableDatabase().rawQuery("select count(*) from " + context.getString(R.string.TABLE_E_MST_SPAJ) +
                    " where KODE_AGEN = '" + kode_agen + "' and FLAG_AKTIF = 1", null);
            cursor.moveToFirst();
            count = cursor.getInt(0);
            matrixCursor.addRow(new Object[]{1 - 10, "Submit " + count});
        } else if (flag_view == 1) { // flag 1 untuk halaman Lead
            cursor = dbHelper.getReadableDatabase().rawQuery("select count(*) from " + context.getString(R.string.TABLE_E_MST_SPAJ) +
                    " where KODE_AGEN = '" + kode_agen + "' and ID_CRM_TAB = '" + idleadTab + "' and FLAG_AKTIF = 0", null);
            cursor.moveToFirst();
            count = cursor.getInt(0);
            matrixCursor.addRow(new Object[]{0 - 10, "Draft " + count});

            cursor = dbHelper.getReadableDatabase().rawQuery("select count(*) from " + context.getString(R.string.TABLE_E_MST_SPAJ) +
                    " where KODE_AGEN = '" + kode_agen + "' and ID_CRM_TAB = '" + idleadTab + "' and FLAG_AKTIF = 1", null);
            cursor.moveToFirst();
            count = cursor.getInt(0);
            matrixCursor.addRow(new Object[]{1 - 10, "Submit " + count});
        }

        return matrixCursor;
    }

    public Cursor getSPAJList(String kodeAgen) {
        if (!TextUtils.isEmpty(kodeAgen)) {
            Cursor cursor;
            String query = "select a._ID as _id, a.SPAJ_ID_TAB as SPAJ_ID_TAB, a.NO_PROPOSAL_TAB as NO_PROPOSAL_TAB, b.NAMA_PP as NAMA_PP, c.NAMA_TT as NAMA_TT,\n" +
                    " d.PREMI_POKOK_DI as PREMI_POKOK_DI, e.KD_PRODUK_UA as KD_PRODUK_UA, a.VIRTUAL_ACC as VIRTUAL_ACC, a.SPAJ_ID as SPAJ_ID, a.FLAG_DOKUMEN as FLAG_DOKUMEN, e.SUB_PRODUK_UA as SUB_PRODUK_UA from E_MST_SPAJ a, E_PEMEGANG_POLIS b, E_TERTANGGUNG c, E_DETIL_INVESTASI d, E_USULAN_ASURANSI e \n" +
                    "                    where a.SPAJ_ID_TAB = b.SPAJ_ID_TAB\n" +
                    "                    and a.SPAJ_ID_TAB = c.SPAJ_ID_TAB\n" +
                    "                    and a.SPAJ_ID_TAB = d.SPAJ_ID_TAB\n" +
                    "                    and a.SPAJ_ID_TAB = e.SPAJ_ID_TAB\n" +
                    "                    and a.KODE_AGEN = '" + kodeAgen+"'";
            cursor = dbHelper.getReadableDatabase().rawQuery(query, null);
            return cursor;
        }
        return null;
    }

    public Cursor getSPAJListForLeadPage(String kodeAgen, String idLeadTab) {
        if (!TextUtils.isEmpty(kodeAgen) && !TextUtils.isEmpty(idLeadTab)) {
            Cursor cursor;
            String query = "select a._ID as _id, a.SPAJ_ID_TAB as SPAJ_ID_TAB, a.NO_PROPOSAL_TAB as NO_PROPOSAL_TAB, b.NAMA_PP as NAMA_PP, c.NAMA_TT as NAMA_TT,\n" +
                    " d.PREMI_POKOK_DI as PREMI_POKOK_DI, e.KD_PRODUK_UA as KD_PRODUK_UA, a.VIRTUAL_ACC as VIRTUAL_ACC, a.SPAJ_ID as SPAJ_ID, a.FLAG_DOKUMEN as FLAG_DOKUMEN, e.SUB_PRODUK_UA as SUB_PRODUK_UA from E_MST_SPAJ a, E_PEMEGANG_POLIS b, E_TERTANGGUNG c, E_DETIL_INVESTASI d, E_USULAN_ASURANSI e \n" +
                    "                    where a.SPAJ_ID_TAB = b.SPAJ_ID_TAB\n" +
                    "                    and a.SPAJ_ID_TAB = c.SPAJ_ID_TAB\n" +
                    "                    and a.SPAJ_ID_TAB = d.SPAJ_ID_TAB\n" +
                    "                    and a.SPAJ_ID_TAB = e.SPAJ_ID_TAB\n" +
                    "                    and a.KODE_AGEN = '" + kodeAgen + "'"+" and a.ID_CRM_TAB = '"+idLeadTab+"'";
            cursor = dbHelper.getReadableDatabase().rawQuery(query, null);
            return cursor;
        }
        return null;
    }

    public String SelectIDPremiKom(int BASE_PREMIUM) {
        String COMB_NAME = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select COMB_NAME from LST_PREMIUM_COMBINATION where BASE_PREMIUM = " + BASE_PREMIUM, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            COMB_NAME = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        return COMB_NAME;
    }

    public int SelectJnsSPAJ(int lsbs_id) {
        int jns_spaj = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select JENIS_SPAJ from LST_BISNIS where LSBS_ID  = " + lsbs_id, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            jns_spaj = cursor.getInt(0);
            cursor.moveToNext();
        }
        cursor.close();
        return jns_spaj;
    }

    public int getFlagPaket(int lsbs_id, int lsdbs_number) {
        int flag_paket = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select  count(*) from (select  a.lsbs_id, a.lsdbs_number, b.flag_packet, b.nama_packet" +
                " from  E_LST_PACKET b,E_LST_PACKET_DET a" +
                " where a.flag_packet = b.flag_packet" +
                " and a.lsbs_id = " + lsbs_id +
                " and a.lsdbs_number = " + lsdbs_number +
                " and A.FLAG_ACTIVE = 1" +
                " and B.FLAG_ACTIVE = 1)", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            flag_paket = cursor.getInt(0);
            cursor.moveToNext();
        }
        cursor.close();
        return flag_paket;
    }

    public ArrayList<ModelBank> getBankPusat(String bank, int jenis_login, int jenis_login_bc) {
        ArrayList<ModelBank> List = new ArrayList<ModelBank>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "";
        if (jenis_login == 9) {
            if (jenis_login_bc == 44) {
                selectQuery = "SELECT LSBP_ID,LSBP_NAMA,LSBP_PANJANG_REKENING,LSBP_MIN_PANJANG_REKENING FROM E_LST_DATA_BANK where LSBP_NAMA LIKE'%"
                        + bank + "%' and LSBP_ID = 231";
            } else if (jenis_login_bc == 50) { // bukopin
                selectQuery = "SELECT LSBP_ID,LSBP_NAMA,LSBP_PANJANG_REKENING,LSBP_MIN_PANJANG_REKENING FROM E_LST_DATA_BANK where LSBP_NAMA LIKE'%"
                        + bank + "%' and LSBP_ID = 25";
            } else if (jenis_login_bc == 51) { //jatim
                selectQuery = "SELECT LSBP_ID,LSBP_NAMA,LSBP_PANJANG_REKENING,LSBP_MIN_PANJANG_REKENING FROM E_LST_DATA_BANK where LSBP_NAMA LIKE'%"
                        + bank + "%' and LSBP_ID = 162";
            } else if (jenis_login_bc == 56) { //BTN SYARIAH
                selectQuery = "SELECT LSBP_ID,LSBP_NAMA,LSBP_PANJANG_REKENING,LSBP_MIN_PANJANG_REKENING FROM E_LST_DATA_BANK where LSBP_NAMA LIKE'%"
                        + bank + "%' and LSBP_ID = 161";
            } else if (jenis_login_bc == 58){ // Jatim Syariah
                selectQuery = "SELECT LSBP_ID,LSBP_NAMA,LSBP_PANJANG_REKENING,LSBP_MIN_PANJANG_REKENING FROM E_LST_DATA_BANK where LSBP_NAMA LIKE'%"
                        + bank + "%' and LSBP_ID = 312";
            } else if (jenis_login_bc == 60){ // Bukopin Syariah
                selectQuery = "SELECT LSBP_ID,LSBP_NAMA,LSBP_PANJANG_REKENING,LSBP_MIN_PANJANG_REKENING FROM E_LST_DATA_BANK where LSBP_NAMA LIKE'%"
                        + bank + "%' and LSBP_ID = 250";
            } else { // bank sinarmas
                selectQuery = "SELECT LSBP_ID,LSBP_NAMA,LSBP_PANJANG_REKENING,LSBP_MIN_PANJANG_REKENING FROM E_LST_DATA_BANK where LSBP_NAMA LIKE'%"
                        + bank + "%' and LSBP_ID in (156,224)";
            }
        } else {
            selectQuery = "SELECT LSBP_ID,LSBP_NAMA,LSBP_PANJANG_REKENING,LSBP_MIN_PANJANG_REKENING FROM E_LST_DATA_BANK where LSBP_NAMA LIKE'%"
                    + bank + "%'";
        }
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelBank(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3)));
            } while (cursor.moveToNext());
        }
        return List;
    }

    public ArrayList<ModelBankCab> getBankCab(String bankcab, int jenis_login, int jenis_login_bc) {
        ArrayList<ModelBankCab> List = new ArrayList<ModelBankCab>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT E_LST_BANK.LSBP_ID, E_LST_BANK.LBN_ID, E_LST_BANK.LBN_NAMA, E_LST_DATA_BANK.LSBP_PANJANG_REKENING " +
                "FROM " +
                "E_LST_BANK,E_LST_DATA_BANK " +
                "WHERE " +
                "E_LST_BANK.LBN_NAMA like'%" + bankcab + "%' " +
                "and E_LST_DATA_BANK.LSBP_ID = E_LST_BANK.LSBP_ID ";
        if (jenis_login == 9) {
            if (jenis_login_bc == 44) {
                selectQuery = selectQuery + " and E_LST_BANK.LSBP_ID = 231";
            } else if (jenis_login_bc == 50) { // bukopin
                selectQuery = selectQuery + " and E_LST_BANK.LSBP_ID = 25";
            } else if (jenis_login_bc == 51) { //jatim
                selectQuery = selectQuery + " and E_LST_BANK.LSBP_ID = 162";
            } else if (jenis_login_bc == 56) { //btn syariah
                selectQuery = selectQuery + " and E_LST_BANK.LSBP_ID = 161";
            } else if (jenis_login_bc == 58){ // jatim syariah
                selectQuery = selectQuery + " and E_LST_BANK.LSBP_ID = 312";
            } else if (jenis_login_bc == 60) { // Bukopin Syariah
                selectQuery = selectQuery + " and E_LST_BANK.LSBP_ID = 250";
            } else { //sinarmas
                selectQuery = selectQuery + " and E_LST_BANK.LSBP_ID in (156,224)";
            }
        }
        Cursor cursor = db.rawQuery(selectQuery + " ORDER BY E_LST_BANK.LBN_NAMA ASC", null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelBankCab(cursor.getInt(0), cursor.getInt(1),
                        cursor.getString(2), cursor.getInt(3)));
            } while (cursor.moveToNext());
        }
        return List;
    }

    public ArrayList<ModelDropdownInt> getLst_Propinsi() {
        ArrayList<ModelDropdownInt> List = new ArrayList<ModelDropdownInt>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT PROPINSI_ID, PROPINSI FROM E_LST_PROPINSI";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelDropdownInt(cursor.getString(1), cursor.getInt(0)));
            } while (cursor.moveToNext());
        }
        return List;
    }

    public ArrayList<ModelDropDownString> getLst_Propinsi_PP() {

        ArrayList<ModelDropDownString> List = new ArrayList<ModelDropDownString>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT DISTINCT LSPR_ID, PROPINSI FROM V_PROPINSI ORDER BY PROPINSI ASC";
        Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    List.add(new ModelDropDownString(cursor.getString(1), cursor.getString(0)));
                } while (cursor.moveToNext());
            }
            cursor.close();
            return List;

    }

    public ArrayList<ModelDropDownString> getLst_Kabupaten_PP(String provinceCode) {
        ArrayList<ModelDropDownString> List = new ArrayList<ModelDropDownString>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT DISTINCT LSKA_ID, KABUPATEN FROM V_PROPINSI WHERE LSPR_ID = '" + provinceCode+"'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelDropDownString(cursor.getString(1), cursor.getString(0)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return List;
    }

    public ArrayList<ModelDropDownString> getLst_Kecamatan_PP(String kabupatenCode) {
        ArrayList<ModelDropDownString> List = new ArrayList<ModelDropDownString>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT DISTINCT LSKC_ID, KECAMATAN FROM V_PROPINSI WHERE LSKA_ID ='" + kabupatenCode+"'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelDropDownString(cursor.getString(1), cursor.getString(0)));
            } while (cursor.moveToNext());
        }
        return List;
    }

    public ArrayList<ModelDropDownString> getLst_Kelurahan_PP(String kelurahanCode) {
        ArrayList<ModelDropDownString> List = new ArrayList<ModelDropDownString>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT DISTINCT LSKL_ID, KELURAHAN FROM V_PROPINSI WHERE LSKC_ID ='" + kelurahanCode+"'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelDropDownString(cursor.getString(1), cursor.getString(0)));
            } while (cursor.moveToNext());
        }
        return List;
    }

    public ArrayList<ModelDropDownString> getLst_Kodepos_PP(String kodeposCode) {
        ArrayList<ModelDropDownString> List = new ArrayList<ModelDropDownString>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT DISTINCT LSKL_ID, KODEPOS FROM V_PROPINSI WHERE LSKL_ID ='" + kodeposCode+"'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelDropDownString(cursor.getString(1), cursor.getString(0)));
            } while (cursor.moveToNext());
        }
        return List;
    }

    public String getPostalCodeFromDistrict(String districtId) {
        if (!TextUtils.isEmpty(districtId)) {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String selectQuery = "SELECT LSKL_ID, KODEPOS FROM V_PROPINSI WHERE LSKL_ID = ?";
            Cursor cursor = db.rawQuery(selectQuery, new String[]{districtId});
            if (cursor.moveToFirst()) {
                String postalCode = cursor.getString(cursor.getColumnIndexOrThrow("KODEPOS"));
                return postalCode;
            }
        }
        return null;
    }

    public ArrayList<ModelDropdownInt> getCaraBayar(int produk, int sub_produk) {
        ArrayList<ModelDropdownInt> List = new ArrayList<ModelDropdownInt>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT DISTINCT (A.LSCB_ID), A.LSCB_PAY_MODE " +
                "FROM LST_PAY_MODE A, LST_KURS B,LST_PRODUCT_CALC C, LST_PRODUCT_SETUP D " +
                "WHERE C.PSET_ID = D.PSET_ID AND " +
                "C.LKU_ID = B.LKU_ID AND " +
                "C.LSCB_ID = A.LSCB_ID AND " +
                "D.PSET_ID = (select pset_id from LST_PRODUCT_SETUP where product_code =" + produk +
                " and plan = " + sub_produk + ")" +
                "ORDER BY A.LSCB_ID DESC";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelDropdownInt(cursor.getString(1), cursor.getInt(0)));
            } while (cursor.moveToNext());
        }
        return List;
    }

    public int selectLinkNonLink(int lsbs_id) {
        int int_value = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select LSGB_ID from LST_BISNIS where LSBS_ID  = " + lsbs_id, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int_value = cursor.getInt(0);
            cursor.moveToNext();
        }
        cursor.close();
        return int_value;
    }

//    public int SelectNamaFund(int produk, int sub_produk, int lji_id, String kurs) {
//        int int_value = 0;
//        SQLiteDatabase db = dbHelper.getReadableDatabase();
//        Cursor cursor = db.rawQuery("select LSGB_ID from LST_BISNIS where LSBS_ID  = " + lsbs_id, null);
//        cursor.moveToFirst();
//        while (!cursor.isAfterLast()) {, int lsdbs
//            int_value = cursor.getInt(0);
//            cursor.moveToNext();
//        }
//        cursor.close();
//        return int_value;
//    }

    public String getNamaFund(String lji_id) {
        String value = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select LJI_INVEST from LST_JENIS_INVEST where lji_id  = '" + lji_id + "'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            value = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        return value;
    }

    public String getNamaProduk(int lsbs_id) {
        String value = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select lsbs_name from LST_BISNIS where lsbs_id = " + lsbs_id, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            value = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        return value;
    }

    public String getNamaSubProduk(int lsbs_id, int lsdbs_number) {
        String value = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select  lsdbs_name from LST_DET_BISNIS  where lsbs_id = " + lsbs_id +
                " and lsdbs_number = " + lsdbs_number, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            value = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        return value;
    }

    public String getSpajIdFromNoProposalTab(String noProposalTab) {
        String spajId = "";
        Cursor cursor = new QueryUtil(context).query(
                context.getString(R.string.TABLE_E_MST_SPAJ),
                new String[]{context.getString(R.string.SPAJ_ID_TAB)},
                context.getString(R.string.NO_PROPOSAL_TAB) + "=?",
                new String[]{noProposalTab},
                null
        );
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            spajId = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();

        return spajId;
    }

    public String getSubPesertaI(int lsbs_id, int lsdbs_number) {
        String value = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select  lsdbs_name from LST_DET_BISNIS  where lsbs_id = " + lsbs_id +
                " and lsdbs_number = " + lsdbs_number, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            value = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        return value;
    }

    public int getPanjangRek(int bank) {
        int value = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select LSBP_PANJANG_REKENING,LSBP_MIN_PANJANG_REKENING FROM E_LST_DATA_BANK where lsbp_id =" + bank, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            value = cursor.getInt(0);
            cursor.moveToNext();
        }
        cursor.close();
        return value;
    }

    public int getMinPanjangRek(int bank) {
        int value = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select LSBP_MIN_PANJANG_REKENING FROM E_LST_DATA_BANK where lsbp_id =" + bank, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            value = cursor.getInt(0);
            cursor.moveToNext();
        }
        cursor.close();
        return value;
    }

    public ArrayList<ModelReferralDa> getLstReferralAAJI() {
        Cursor cursor;
        cursor = new QueryUtil(context).query(
                context.getString(R.string.TABLE_E_LST_REFF),
                null,
                "FLAG_LISENSI == 1",
                null,
                "NAMA_REFF ASC"
        );

        ArrayList<ModelReferralDa> List = new ArrayList<ModelReferralDa>();
        List = new TableLST_Reff(context).getObjectArray(cursor);
        return List;
    }

    public ArrayList<ModelReferralDa> getLstReferral() {
        Cursor cursor;
        cursor = new QueryUtil(context).query(
                context.getString(R.string.TABLE_E_LST_REFF),
                null,
                null,
                null,
                "NAMA_REFF ASC"
        );

        ArrayList<ModelReferralDa> List = new ArrayList<ModelReferralDa>();
        List = new TableLST_Reff(context).getObjectArray(cursor);
        return List;
    }

    public ArrayList<ModelCabangDa> getLstCabang() {
        ArrayList<ModelCabangDa> List = new ArrayList<ModelCabangDa>();
        String value = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select DISTINCT NAMA_CABANG, LCB_NO from E_LST_REFF", null);
        if (cursor.moveToFirst()) {
            do {
                List.add(new ModelCabangDa(cursor.getString(0),
                        cursor.getString(1)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return List;
    }

    public int getCountReferral() {
        int total = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select count (*) from E_LST_REFF", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            total = cursor.getInt(0);
            cursor.moveToNext();
        }
        cursor.close();
        return total;
    }

    public int getCountBank() {
        int total = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select count (*) from E_LST_BANK", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            total = cursor.getInt(0);
            cursor.moveToNext();
        }
        cursor.close();
        return total;
    }

    //sementara untuk versi ini aja
    public String getCheckMember(String nama, String paket, String ttl) {
        String value = "";
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select NOID from E_LST_MEMBER where FULLNAME  = '" + nama + "'" +
                "and PREMI = '" + paket + "'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            value = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        return value;
    }

    public int getFlagVa(int JENIS_LOGIN, int JENIS_LOGIN_BC, String KODE_REG1, String KODE_REG2) {
        int flag_va = 0;
        int lstb_id = 0;
        String selectQuery = MethodTable.selectQueryJenis(JENIS_LOGIN, JENIS_LOGIN_BC,
                KODE_REG1, KODE_REG2);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                lstb_id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }

        selectQuery = "SELECT FLAG_VA FROM E_LST_CHANNEL_DIST WHERE LSTB_ID = " + lstb_id;
        cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                flag_va = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return flag_va;
    }

    public int getLSBP_VA(int JENIS_LOGIN, int JENIS_LOGIN_BC, String KODE_REG1, String KODE_REG2) {
        int lsbp_id = 0;
        int lstb_id = 0;
        String selectQuery = MethodTable.selectQueryJenis(JENIS_LOGIN, JENIS_LOGIN_BC,
                KODE_REG1, KODE_REG2);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                lstb_id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }

        selectQuery = "SELECT LSBP_ID FROM E_LST_CHANNEL_DIST WHERE LSTB_ID = " + lstb_id;
        cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                lsbp_id = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return lsbp_id;
    }

    //CEK KALAU SYARIAH = 3
    public int getLine_Bus(int lsb_id) {
        int line_bus = 0;
        String selectQuery = "select lsbs_linebus from lst_bisnis where lsbs_id = " + lsb_id;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                line_bus = cursor.getInt(0);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return line_bus;
    }

    public int getCountDokumen(String kode_agen) {
        int total = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select count (*) from E_MST_SPAJ where FLAG_AKTIF = 1" +
                        " and FLAG_DOKUMEN = 0" +
                        " and KODE_AGEN = '" + kode_agen + "'",
                null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            total = cursor.getInt(0);
            cursor.moveToNext();
        }
        cursor.close();
        return total;
    }

    /**
     * Get user last opened proposal page position
     *
     * @param noProposalTab
     * @return
     */
    public int getLastOpenedPagePositionFromSPAJ(String noProposalTab) {
        if (TextUtils.isEmpty(noProposalTab)) {
            return 0;
        }
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT PAGE_POSITION FROM E_MST_SPAJ WHERE NO_PROPOSAL_TAB = '" + noProposalTab + "'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            int pagePosition = cursor.getInt(cursor.getColumnIndexOrThrow("PAGE_POSITION"));
            cursor.close();
            return pagePosition;
        }
        cursor.close();

        return -1;
    }

    public List<Referral> getReferralListFromBranchName(String branchName) {

        if (!TextUtils.isEmpty(branchName)) {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String query = "SELECT AGENT_CODE, NAMA_REFF from E_LST_REFF WHERE NAMA_CABANG = '"+branchName+"' ORDER BY NAMA_REFF ASC";
            Cursor cursor = db.rawQuery(query, null);


            List<Referral> referrals = new ArrayList<>();

            while (cursor.moveToNext()) {
                String agentCode = cursor.getString(cursor.getColumnIndexOrThrow("AGENT_CODE"));
                String reffName = cursor.getString(cursor.getColumnIndexOrThrow("NAMA_REFF"));

                Referral referral = new Referral();

                referral.setReffId(agentCode);
                referral.setReffName(reffName);

                referrals.add(referral);
            }

            cursor.close();
            return referrals;

        }

        return null;
    }
    public int getLastMonthSPAJCount(String msagId) {

        if (!TextUtils.isEmpty(msagId)) {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String query = "SELECT SPAJ_ID_TAB FROM E_MST_SPAJ WHERE KODE_AGEN = '"+msagId+"'";
            Cursor cursor = db.rawQuery(query, null);


            List<String> spajLastMonth = new ArrayList<>();
            int lastMonth = DateUtils.getLastMonthNumber();

            if(lastMonth == 0){ // untuk case kalau bulan januari
                lastMonth = 12;
            }

            while (cursor.moveToNext()) {
                String spajIdTab = cursor.getString(cursor.getColumnIndexOrThrow("SPAJ_ID_TAB"));
                int spajMonthInputDate = DateUtils.getMonthFromSpajIdTab(spajIdTab);

                if (spajMonthInputDate == lastMonth) {
                    spajLastMonth.add(spajIdTab);
                }

            }

            cursor.close();
            return spajLastMonth.size();

        }

        return 0;
    }
    public int getCurrentMonthSPAJCount(String msagId) {

        if (!TextUtils.isEmpty(msagId)) {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String query = "SELECT SPAJ_ID_TAB FROM E_MST_SPAJ WHERE KODE_AGEN = '"+msagId+"'";
            Cursor cursor = db.rawQuery(query, null);


            List<String> spajCurrentMonth = new ArrayList<>();
            int currentMonth = DateUtils.getCurrentMonth();

            while (cursor.moveToNext()) {
                String spajIdTab = cursor.getString(cursor.getColumnIndexOrThrow("SPAJ_ID_TAB"));
                int spajMonthInputDate = DateUtils.getMonthFromSpajIdTab(spajIdTab);

                if (spajMonthInputDate == currentMonth) {
                    spajCurrentMonth.add(spajIdTab);
                }

            }

            cursor.close();
            return spajCurrentMonth.size();

        }

        return 0;
    }
    public int getTodayCreatedSPAJCount(String msagId) {

        if (!TextUtils.isEmpty(msagId)) {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String query = "SELECT SPAJ_ID_TAB FROM E_MST_SPAJ WHERE KODE_AGEN = '"+msagId+"'";
            Cursor cursor = db.rawQuery(query, null);


            List<String> todaySPAJList = new ArrayList<>();
            int currentDayOfMonth = DateUtils.getCurrentDayOfMonth();

            while (cursor.moveToNext()) {
                String spajIdTab = cursor.getString(cursor.getColumnIndexOrThrow("SPAJ_ID_TAB"));
                int spajInputDate = DateUtils.getDayFromSpajIdTab(spajIdTab);

                if (spajInputDate == currentDayOfMonth) {
                    todaySPAJList.add(spajIdTab);
                }

            }

            cursor.close();
            return todaySPAJList.size();

        }

        return 0;
    }
    public String getPaymentTransactionNumber(String spajIdTab) {
        if (!TextUtils.isEmpty(spajIdTab)) {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String query = "select payment_cc_tx_id from E_MST_SPAJ where spaj_id_tab = ?";
            Cursor cursor = db.rawQuery(query, new String[]{spajIdTab});
            if (cursor.moveToFirst()) {
                String transactionNumber = cursor.getString(cursor.getColumnIndexOrThrow("PAYMENT_CC_TX_ID"));
                return transactionNumber;
            }
            cursor.close();

        }

        return null;
    }
    public int getPaymentType(String spajIdTab) {
        if (!TextUtils.isEmpty(spajIdTab)) {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String query = "select payment_type from E_MST_SPAJ where spaj_id_tab = ?";
            Cursor cursor = db.rawQuery(query, new String[]{spajIdTab});
            if (cursor.moveToFirst()) {
                int paymentType = cursor.getInt(cursor.getColumnIndexOrThrow("PAYMENT_TYPE"));
                return paymentType;
            }
            cursor.close();

        }

        return -1;
    }
    public String getProposalNo(String noProposalTab) {
        if (!TextUtils.isEmpty(noProposalTab)) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String query = "select NO_PROPOSAL from MST_DATA_PROPOSAL where NO_PROPOSAL_TAB = ?";
            Cursor cursor = db.rawQuery(query, new String[]{noProposalTab});
            if (cursor.moveToFirst()) {
                String proposalNo = cursor.getString(cursor.getColumnIndexOrThrow("NO_PROPOSAL"));
                return proposalNo;
            }
            cursor.close();
        }
        return "";
    }
}
