package id.co.ajsmsig.nb.pointofsale.ui.hriskprofiling

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.RiskProfileQuestionnaireAllAnswers
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */
class RiskProfilingVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository): AndroidViewModel(application) {

    val observableQuestions: MediatorLiveData<List<RiskProfileQuestionnaireAllAnswers>> = MediatorLiveData()

    init {
        val questions = repository.getAllRiskProfileQuestionnaire()
        observableQuestions.addSource(questions, observableQuestions::setValue)
    }


}