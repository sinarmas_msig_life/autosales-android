package id.co.ajsmsig.nb.crm.model;

public class ProposalListModel {
    private String noProposalTab;
    private String noProposal;
    private String namaPp;
    private String namaTt;
    private long premi;
    private long uangPertanggungan;
    private String lsdbsName;
    private long id;

    public ProposalListModel() {
    }

    public ProposalListModel(long id, String noProposalTab, String noProposal, String namaPp, String namaTt, long premi, long uangPertanggungan, String lsdbsName) {
        this.id = id;
        this.noProposalTab = noProposalTab;
        this.noProposal = noProposal;
        this.namaPp = namaPp;
        this.namaTt = namaTt;
        this.premi = premi;
        this.uangPertanggungan = uangPertanggungan;
        this.lsdbsName = lsdbsName;
    }

    public String getNoProposalTab() {
        return noProposalTab;
    }

    public void setNoProposalTab(String noProposalTab) {
        this.noProposalTab = noProposalTab;
    }

    public String getNoProposal() {
        return noProposal;
    }

    public void setNoProposal(String noProposal) {
        this.noProposal = noProposal;
    }

    public String getNamaPp() {
        return namaPp;
    }

    public void setNamaPp(String namaPp) {
        this.namaPp = namaPp;
    }

    public String getNamaTt() {
        return namaTt;
    }

    public void setNamaTt(String namaTt) {
        this.namaTt = namaTt;
    }

    public long getPremi() {
        return premi;
    }

    public void setPremi(long premi) {
        this.premi = premi;
    }

    public long getUangPertanggungan() {
        return uangPertanggungan;
    }

    public void setUangPertanggungan(long uangPertanggungan) {
        this.uangPertanggungan = uangPertanggungan;
    }

    public String getLsdbsName() {
        return lsdbsName;
    }

    public void setLsdbsName(String lsdbsName) {
        this.lsdbsName = lsdbsName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
