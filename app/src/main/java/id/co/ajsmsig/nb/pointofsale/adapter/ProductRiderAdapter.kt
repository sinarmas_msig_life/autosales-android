package id.co.ajsmsig.nb.pointofsale.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosRowProductRiderBinding
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.ProductRider

/**
 * Created by andreyyoshuamanik on 21/02/18.
 */
class ProductRiderAdapter: RecyclerView.Adapter<ProductRiderAdapter.OptionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionViewHolder {

        val dataBinding = DataBindingUtil.inflate<PosRowProductRiderBinding>(LayoutInflater.from(parent.context), R.layout.pos_row_product_rider, parent, false)
        return OptionViewHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        options?.let {
            holder.bind(it[position])
        }
    }

    var options: Array<ProductRider>? = null
    set(value) {
        field = value

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return options?.size ?: 0
    }

    inner class OptionViewHolder(private val dataBinding: PosRowProductRiderBinding): RecyclerView.ViewHolder(dataBinding.root) {

        fun bind(option: ProductRider) {
            dataBinding.vm = option
        }

    }
}