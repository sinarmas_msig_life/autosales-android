package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by andreyyoshuamanik on 24/02/18.
 */
@Entity
class InputType(
        @PrimaryKey
        val type: String,
        var text: String?
)