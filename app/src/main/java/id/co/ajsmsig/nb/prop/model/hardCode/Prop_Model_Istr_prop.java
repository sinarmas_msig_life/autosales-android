package id.co.ajsmsig.nb.prop.model.hardCode;

import java.io.Serializable;
import java.util.Date;

import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.Prop_Model_S_ekaSehat;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.Prop_Model_S_ekaSehatInnerLimit;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.Prop_Model_S_hcpLad;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.Prop_Model_S_hcpPro;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.Prop_Model_S_hcpf;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.Prop_Model_S_ladiesMedExpense;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.Prop_Model_S_ladiesMedExpenseInnerLimit;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.Prop_Model_S_medicalPlus;

public class Prop_Model_Istr_prop implements Serializable
{
	private static final long serialVersionUID = -3514500141957802519L;
	public double[] fund = new double[ 8 + 1 ];
    public double pct_premi;
    public int ins_per;
    public int umur_tt;
    public String sex_tt;
    public double up;
    public int persen_ins;
    public int usia_schol;
    public double up_schol;
    public String[] nama_plan = new String[ 1 + 1 ];
    public static int[] rider_baru = new int[ 28 + 1 ];
    public static int cb_id;
    public int pa_risk;
    public int pa_class;
    public String kurs_id;
    public double premi;
    public int umur_pp;
    public Prop_Model_S_hcpf hcpf;
    public Prop_Model_S_hcpPro hcpPro;
    public Prop_Model_S_hcpLad hcpLad;
    public Prop_Model_S_ekaSehat eka_sehat;
    public Prop_Model_S_ekaSehatInnerLimit ekaSehatInnerLimit;
    public Prop_Model_S_ladiesMedExpense ladiesMedExpense;
    public Prop_Model_S_ladiesMedExpenseInnerLimit ladiesMedExpenseInnerLimit;
    public Prop_Model_S_medicalPlus medicalPlus;
    public double up_term;
    public int bisnis_id;
    public int bisnis_no;
    public int pay_per;
    public int cuti_premi;
    public Date tgl_prop;
    public int bunga;
    public double topup;
    public double pct_invest;
    public double biaya_ass;
    public int bln_ke;
    public int ci_persen_up;
    public int early_stage_ci_99_persen_up;

}
