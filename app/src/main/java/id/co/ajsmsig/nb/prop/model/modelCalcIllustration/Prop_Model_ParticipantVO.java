package id.co.ajsmsig.nb.prop.model.modelCalcIllustration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.co.ajsmsig.nb.prop.method.util.ListUtil;

public class Prop_Model_ParticipantVO implements Serializable
{
	private static final long serialVersionUID = -5308650094722760194L;
	private String name;
    private Date birthDay;
    private Integer age;
    private String status;
    //=== Added by Daru @since 06 Mar 2014
    private Integer lsre_id;
    private List<Prop_Model_OptionVO> relation;
    private String sexCd;
    private ArrayList<Prop_Model_OptionVO> genderList;
    private String medicalPlusRbFlag;
    //===

    public Prop_Model_ParticipantVO()
    {
    }

    public Prop_Model_ParticipantVO(String name, Date birthDay, Integer age )
    {
        this.name = name;
        this.birthDay = birthDay;
        this.age = age;
    }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public Date getBirthDay()
    {
        return birthDay;
    }

    public void setBirthDay( Date birthDay )
    {
        this.birthDay = birthDay;
    }

    public Integer getAge()
    {
        return age;
    }

    public void setAge( Integer age )
    {
        this.age = age;
    }

	public Integer getLsre_id() {
		return lsre_id;
	}

	public void setLsre_id(Integer lsre_id) {
		this.lsre_id = lsre_id;
	}

	public List<Prop_Model_OptionVO> getRelation() {
		return relation;
	}

	public void setRelation(List<Prop_Model_OptionVO> relation) {
		this.relation = relation;
	}
	
	public ArrayList<Prop_Model_OptionVO> getGenderList()
	{
	    return genderList;
	}

	public void setGenderList( List<Prop_Model_OptionVO> genderList )
	{
	    this.genderList = ListUtil.serializableList(genderList);
	}

	public String getMedicalPlusRbFlag() {
		return medicalPlusRbFlag;
	}

	public void setMedicalPlusRbFlag(String medicalPlusRbFlag) {
		this.medicalPlusRbFlag = medicalPlusRbFlag;
	}

	public String getSexCd() {
		return sexCd;
	}

	public void setSexCd(String sexCd) {
		this.sexCd = sexCd;
	}

}
