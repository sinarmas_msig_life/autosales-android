package id.co.ajsmsig.nb.crm.fragment;
/*
 Created by faiz_f on 12/06/2017.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.co.ajsmsig.nb.R;

public class C_AgentProfileFragment extends Fragment {
    public C_AgentProfileFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.c_fragment_more, container, false);

        return view;
    }
}
