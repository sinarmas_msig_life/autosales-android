/**
 * @author Eriza Siti Mulyani
 * method untuk format nilai uang
 */
package id.co.ajsmsig.nb.espaj.method;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
public class NumberTextWatcher implements TextWatcher {
    
    private DecimalFormat df;
    private DecimalFormat dfnd;
    private boolean hasFractionalPart;
    
    private EditText et;
    
    public NumberTextWatcher(EditText et)
    {
    	//fungsi merubah format jadi 1.000
    NumberFormat nf = NumberFormat.getNumberInstance(Locale.GERMANY);
    df = (DecimalFormat)nf;
   df.setDecimalSeparatorAlwaysShown(true);
    dfnd = (DecimalFormat)nf;
	this.et = et;
	hasFractionalPart = false;
    }
    
    @SuppressWarnings("unused")
	private static final String TAG = "NumberTextWatcher";

    @Override
	public void afterTextChanged(Editable s)
    {
	et.removeTextChangedListener(this);
	
	try {
	    int inilen, endlen;
	    inilen = et.getText().length();
	    
	    String v = s.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "");
	    String a=v.replace(",", "");
	    Number n = df.parse(a);
	    int cp = et.getSelectionStart();
	    if (hasFractionalPart) {
		et.setText(df.format(n));
	    } else {
		et.setText(dfnd.format(n).replace(",", ""));
	    }
	    endlen = et.getText().length();
	    int sel = (cp + (endlen - inilen));
	    if (sel > 0 && sel <= et.getText().length()) {
		et.setSelection(sel);
	    } else {
		
		et.setSelection(et.getText().length() - 1);
	    }
	} catch (NumberFormatException nfe) {

	} catch (ParseException e) {
	
	}
	
	et.addTextChangedListener(this);
    }

    @Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {
    }

    @Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        hasFractionalPart = s.toString().contains(String.valueOf(df.getDecimalFormatSymbols().getDecimalSeparator()));
    }

}
