package id.co.ajsmsig.nb.prop.model.apimodel.request;

public class ValidateVaRequest {
    private String NO_VA;
    private String LSBS_ID;
    private String LSBP_ID;

    public ValidateVaRequest() {

    }
    public String getNO_VA() {
        return NO_VA;
    }

    public void setNO_VA(String NO_VA) {
        this.NO_VA = NO_VA;
    }

    public String getLSBS_ID() {
        return LSBS_ID;
    }

    public void setLSBS_ID(String LSBS_ID) {
        this.LSBS_ID = LSBS_ID;
    }

    public String getLSBP_ID() {
        return LSBP_ID;
    }

    public void setLSBP_ID(String LSBP_ID) {
        this.LSBP_ID = LSBP_ID;
    }

}
