package id.co.ajsmsig.nb.crm.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.TrackPolisModel;

public class C_TrackPolisAdapter extends RecyclerView.Adapter<C_TrackPolisAdapter.TrackPolisVH> {
    private List<TrackPolisModel> polisList;
    private ClickListener clickListener;

    public C_TrackPolisAdapter(List<TrackPolisModel> polisList) {
        this.polisList = polisList;
    }

    public class TrackPolisVH extends RecyclerView.ViewHolder {
        private TextView tvDescription;
        private TextView tvTimestamp;
        private TextView tvStatus;
        private TextView tvStatusAccepted;
        private ImageView imgPolis;

        public TrackPolisVH(View view) {
            super(view);
            tvDescription = view.findViewById(R.id.tvDescription);
            tvTimestamp = view.findViewById(R.id.tvTimestamp);
            tvStatus = view.findViewById(R.id.tvStatus);
            tvStatusAccepted  = view.findViewById(R.id.tvStatusAccepted);
            imgPolis = view.findViewById(R.id.imgPolis);
        }

    }
    @Override
    public TrackPolisVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.c_track_polis_list, parent, false);
        return new TrackPolisVH(itemView);
    }

    @Override
    public void onBindViewHolder(final TrackPolisVH holder, int position) {
        TrackPolisModel model = polisList.get(position);
        holder.tvDescription.setText(model.getMspsDesc());
        holder.tvTimestamp.setText(model.getMspsDate());
        holder.tvStatus.setText(model.getStatusPolis());
        holder.tvStatusAccepted.setText(model.getStatusAccept());
        String statusAccept = model.getStatusAccept();
        String statusPolis = model.getStatusPolis();
        String mspsDesc = model.getMspsDesc();

        if (statusPolis.equalsIgnoreCase("INFORCE")) {
            holder.imgPolis.setImageResource(R.drawable.ic_complete);
        } else if (mspsDesc.contains("FR: ")) {
            holder.imgPolis.setImageResource(R.drawable.ic_warning);
        } else {
            holder.imgPolis.setImageResource(R.drawable.ic_policy) ;
        }
    }
    @Override
    public int getItemCount() {
        return polisList.size();
    }

    public void setClickListener (ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onLeadSelected(int position, List<TrackPolisModel> polistList);
    }
}
