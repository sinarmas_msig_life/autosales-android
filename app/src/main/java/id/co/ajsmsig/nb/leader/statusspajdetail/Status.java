
package id.co.ajsmsig.nb.leader.statusspajdetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status {

    @SerializedName("lssp_status")
    @Expose
    private String lsspStatus;
    @SerializedName("sub_id")
    @Expose
    private Integer subId;
    @SerializedName("reg_spaj")
    @Expose
    private String regSpaj;
    @SerializedName("lus_email")
    @Expose
    private String lusEmail;
    @SerializedName("msps_desc")
    @Expose
    private String mspsDesc;
    @SerializedName("status_accept")
    @Expose
    private String statusAccept;
    @SerializedName("lscb_id")
    @Expose
    private int lscbId;
    @SerializedName("lssa_id")
    @Expose
    private int lssaId;
    @SerializedName("lus_full_name")
    @Expose
    private String lusFullName;
    @SerializedName("lde_dept")
    @Expose
    private String ldeDept;
    @SerializedName("mspo_policy_no")
    @Expose
    private String mspoPolicyNo;
    @SerializedName("msps_date")
    @Expose
    private String mspsDate;
    @SerializedName("lssp_id")
    @Expose
    private int lsspId;
    @SerializedName("sub_desc")
    @Expose
    private String subDesc;

    private boolean isPending;
    private boolean isInforce;

    public boolean isInforce() {
        return isInforce;
    }

    public void setInforce(boolean inforce) {
        isInforce = inforce;
    }

    public boolean isPending() {
        return isPending;
    }

    public void setPending(boolean pending) {
        isPending = pending;
    }

    public String getLsspStatus() {
        return lsspStatus;
    }

    public void setLsspStatus(String lsspStatus) {
        this.lsspStatus = lsspStatus;
    }

    public Integer getSubId() {
        return subId;
    }

    public void setSubId(Integer subId) {
        this.subId = subId;
    }

    public String getRegSpaj() {
        return regSpaj;
    }

    public void setRegSpaj(String regSpaj) {
        this.regSpaj = regSpaj;
    }

    public String getLusEmail() {
        return lusEmail;
    }

    public void setLusEmail(String lusEmail) {
        this.lusEmail = lusEmail;
    }

    public String getMspsDesc() {
        return mspsDesc;
    }

    public void setMspsDesc(String mspsDesc) {
        this.mspsDesc = mspsDesc;
    }

    public String getStatusAccept() {
        return statusAccept;
    }

    public void setStatusAccept(String statusAccept) {
        this.statusAccept = statusAccept;
    }

    public int getLscbId() {
        return lscbId;
    }

    public void setLscbId(int lscbId) {
        this.lscbId = lscbId;
    }

    public int getLssaId() {
        return lssaId;
    }

    public void setLssaId(int lssaId) {
        this.lssaId = lssaId;
    }

    public String getLusFullName() {
        return lusFullName;
    }

    public void setLusFullName(String lusFullName) {
        this.lusFullName = lusFullName;
    }

    public String getLdeDept() {
        return ldeDept;
    }

    public void setLdeDept(String ldeDept) {
        this.ldeDept = ldeDept;
    }

    public String getMspoPolicyNo() {
        return mspoPolicyNo;
    }

    public void setMspoPolicyNo(String mspoPolicyNo) {
        this.mspoPolicyNo = mspoPolicyNo;
    }

    public String getMspsDate() {
        return mspsDate;
    }

    public void setMspsDate(String mspsDate) {
        this.mspsDate = mspsDate;
    }

    public int getLsspId() {
        return lsspId;
    }

    public void setLsspId(int lsspId) {
        this.lsspId = lsspId;
    }

    public String getSubDesc() {
        return subDesc;
    }

    public void setSubDesc(String subDesc) {
        this.subDesc = subDesc;
    }

}
