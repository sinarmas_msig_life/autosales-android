package id.co.ajsmsig.nb.crm.model;

public class ActivityListModel {
    private String slaId;
    private String slaTabId;
    private String activityName;
    private String subActivityName;
    private String activityCreatedDate;
    private String rawActivityCreatedDate;
    private String rawSubActivityType;
    private String activityDetail;
    private boolean isSynced;

    public ActivityListModel(String slaId, String slaTabId, String activityName, String subActivityName, String activityCreatedDate, String rawActivityCreatedDate, String rawSubActivityType, String activityDetail, boolean isSynced) {
        this.slaId = slaId;
        this.slaTabId = slaTabId;
        this.activityName = activityName;
        this.subActivityName = subActivityName;
        this.activityCreatedDate = activityCreatedDate;
        this.rawActivityCreatedDate = rawActivityCreatedDate;
        this.rawSubActivityType = rawSubActivityType;
        this.activityDetail = activityDetail;
        this.isSynced = isSynced;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getSubActivityName() {
        return subActivityName;
    }

    public void setSubActivityName(String subActivityName) {
        this.subActivityName = subActivityName;
    }

    public String getActivityCreatedDate() {
        return activityCreatedDate;
    }

    public void setActivityCreatedDate(String activityCreatedDate) {
        this.activityCreatedDate = activityCreatedDate;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public String getSlaTabId() {
        return slaTabId;
    }

    public void setSlaTabId(String slaTabId) {
        this.slaTabId = slaTabId;
    }

    public String getRawActivityCreatedDate() {
        return rawActivityCreatedDate;
    }
    public String getRawSubActivityType() {
        return rawSubActivityType;
    }

    public String getActivityDetail() {
        return activityDetail;
    }

    public boolean isSynced() {
        return isSynced;
    }

    public void setSynced(boolean synced) {
        isSynced = synced;
    }


}
