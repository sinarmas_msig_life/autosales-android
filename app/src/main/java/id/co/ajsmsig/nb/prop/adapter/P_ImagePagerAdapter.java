package id.co.ajsmsig.nb.prop.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rizky_c on 18/09/2017.
 */

public class P_ImagePagerAdapter extends PagerAdapter {


    private List<Integer> images;

    public P_ImagePagerAdapter(ArrayList<Integer> images) {
        this.images = images;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        P_TouchImageView img = new P_TouchImageView(container.getContext());
        img.setImageResource(images.get(position));
        container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        return img;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }

}