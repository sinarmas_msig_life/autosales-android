package id.co.ajsmsig.nb.pointofsale.ui.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.util.Log
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Product
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.*
import javax.inject.Inject
import com.google.gson.reflect.TypeToken
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.UserRecord
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.repository.MainRepository


/**
 * Created by andreyyoshuamanik on 27/02/18.
 */
class SharedViewModel
@Inject
constructor(application: Application, var repository: MainRepository): AndroidViewModel(application){

    
    // Data got from autosales app
    var groupId: Int? = null
        set(value) {
            field = value

            inputs[Companion.groupId] = value
        }
    var agentCode: String? = null
        set(value) {
            field = value

            inputs[Companion.agentCode] = value
        }
    var agentName: String? = null
        set(value) {
            field = value

            inputs[Companion.agentName] = value
        }

    var leadName: String? = null
        set(value) {
            field = value

            inputs[Companion.leadName] = value
        }

    var leadAge: Int? = null
        set(value) {
            field = value

            inputs[Companion.leadAge] = value
        }

    var leadGender: Int? = null
        set(value) {
            field = value

            inputs[Companion.leadGender] = value
        }

    var leadId: Long? = null
        set(value) {
            field = value

            inputs[Companion.leadId] = value
        }

    // Get data from Main DB
    lateinit var observableUserRecord: LiveData<UserRecord>
    var userRecord: UserRecord? = null

    var selectedLifeStage: PageOption? = null
    set(value) {
        field = value

        inputs[Companion.selectedLifeStage] = value?.title
    }

    var selectedPriorityNeed: PageOption? = null

    var selectedNeedAnalysis: PageOption? = null

    var needCalculatorInputs: ArrayList<InputAllChoices>? = null

    var riskProfileQuestionnaireAnswers: ArrayList<RiskProfileQuestionnaire>? = null

    var selectedRiskProfile: RiskProfileScoring? = null
    set(value) {
        field = value

        inputs[Companion.selectedRiskProfile] = value?.type
    }

    var selectedProduct: Product? = null
    set(value) {
        field = value

        inputs[Companion.selectedProduct] = value?.LsbsName
    }

    var selectedHandlingObjection: HandlingObjection? = null

    var pages = ArrayList<Page>()
    var lastSavedPages: ArrayList<Page>? = null

    val inputs = HashMap<String, Any?>()

    var startFromLifeStage = false


    private fun compileToJsonString(): String {
        val gson = Gson()

        val map = HashMap<String, String>()

        map[Companion.groupId] = gson.toJson(groupId)
        map[Companion.leadName] = gson.toJson(leadName)
        map[Companion.leadAge] = gson.toJson(leadAge)
        map[Companion.leadGender] = gson.toJson(leadGender)
        map[Companion.leadId] = gson.toJson(leadId)
        map[Companion.selectedLifeStage] = gson.toJson(selectedLifeStage)
        map[Companion.selectedPriorityNeed] = gson.toJson(selectedPriorityNeed)
        map[Companion.selectedNeedAnalysis] = gson.toJson(selectedNeedAnalysis)
        map[Companion.needCalculatorInputs] = gson.toJson(needCalculatorInputs)
        map[Companion.riskProfileQuestionnaireAnswers] = gson.toJson(riskProfileQuestionnaireAnswers)
        map[Companion.selectedRiskProfile] = gson.toJson(selectedRiskProfile)
        map[Companion.selectedProduct] = gson.toJson(selectedProduct)
        map[Companion.selectedHandlingObjection] = gson.toJson(selectedHandlingObjection)
        map[Companion.pages] = gson.toJson(pages)

        return gson.toJson(map)
    }

    private fun setValueFromJsonString(str: String?) {
        str?.let {
            val gson = Gson()

            var type = object : TypeToken<LinkedTreeMap<String, String>>() { }.type
            val map = gson.fromJson<LinkedTreeMap<String, String>>(str, type)

//            groupId = gson.fromJson(map[Companion.groupId], Int::class.java)
//            leadName = gson.fromJson(map[Companion.leadName], String::class.java)
//            leadAge = gson.fromJson(map[Companion.leadAge], Int::class.java)
//            leadGender = gson.fromJson(map[Companion.leadGender], Int::class.java)
//            leadId = gson.fromJson(map[Companion.leadId], Long::class.java)
            selectedLifeStage = gson.fromJson(map[Companion.selectedLifeStage], PageOption::class.java)
            selectedPriorityNeed = gson.fromJson(map[Companion.selectedPriorityNeed], PageOption::class.java)
            selectedNeedAnalysis = gson.fromJson(map[Companion.selectedNeedAnalysis], PageOption::class.java)

            type = object : TypeToken<ArrayList<InputAllChoices>>() { }.type
            needCalculatorInputs = gson.fromJson(map[Companion.needCalculatorInputs], type)
            type = object : TypeToken<ArrayList<RiskProfileQuestionnaire>>() {}.type
            riskProfileQuestionnaireAnswers = gson.fromJson(map[Companion.riskProfileQuestionnaireAnswers], type)
            selectedRiskProfile = gson.fromJson(map[Companion.selectedRiskProfile], RiskProfileScoring::class.java)
            selectedProduct = gson.fromJson(map[Companion.selectedProduct], Product::class.java)

            selectedHandlingObjection = gson.fromJson(map[Companion.selectedHandlingObjection], HandlingObjection::class.java)

            type = object : TypeToken<ArrayList<Page>>() {}.type
            lastSavedPages = gson.fromJson(map[Companion.pages], type)
        }
    }

    fun getUserRecord(userRecordListener: (UserRecord?) -> Unit) {

        observableUserRecord = repository.getUserRecordWithUserId(leadId, groupId)
        val observer = object : Observer<UserRecord> {
            override fun onChanged(t: UserRecord?) {

                userRecord = t
                Log.i("INFO", "Get ${t?.savedJson}")
                setValueFromJsonString(t?.savedJson)
                observableUserRecord.removeObserver(this)
                userRecordListener(t)
            }

        }
        observableUserRecord.observeForever(observer)
    }

    fun save() {
        val user = userRecord ?: UserRecord(0)

        user.userId = leadId
        user.groupId = groupId
        user.savedJson = compileToJsonString()

        if (user.id > 0) {
            Log.i("INFO", "Update ${user.savedJson}")
            repository.update(user)
        } else {
            Log.i("INFO", "Insert ${user.savedJson}")
            user.id = repository.insert(user)
        }
        userRecord = user
    }

    companion object {
        // Data got from autosales app
        val groupId = "groupId"
        val agentCode = "agentCode"
        val agentName = "agentName"
        val leadName = "leadName"
        val leadAge = "leadAge"
        val leadGender = "leadGender"
        val leadId = "leadId"
        val selectedLifeStage = "selectedLifeStage"
        val selectedPriorityNeed = "selectedPriorityNeed"
        val selectedNeedAnalysis = "selectedNeedAnalysis"
        val needCalculatorInputs = "needCalculatorInputs"
        val riskProfileQuestionnaireAnswers = "riskProfileQuestionnaireAnswers"
        val selectedRiskProfile = "selectedRiskProfile"
        val selectedProduct = "selectedProduct"
        val selectedHandlingObjection = "selectedHandlingObjection"
        val pages = "pages"
    }
}