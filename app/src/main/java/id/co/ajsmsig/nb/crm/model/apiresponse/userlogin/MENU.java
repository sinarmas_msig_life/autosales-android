
package id.co.ajsmsig.nb.crm.model.apiresponse.userlogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MENU {

    @SerializedName("MENU_NAME")
    @Expose
    private String mENUNAME;
    @SerializedName("MENU_ID")
    @Expose
    private String mENUID;

    public String getMENUNAME() {
        return mENUNAME;
    }

    public void setMENUNAME(String mENUNAME) {
        this.mENUNAME = mENUNAME;
    }

    public String getMENUID() {
        return mENUID;
    }

    public void setMENUID(String mENUID) {
        this.mENUID = mENUID;
    }

}
