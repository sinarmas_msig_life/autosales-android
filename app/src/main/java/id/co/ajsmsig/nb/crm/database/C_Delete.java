package id.co.ajsmsig.nb.crm.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.widget.TextView;

import id.co.ajsmsig.nb.R;

/**
 * Created by faiz_f on 27/04/2017.
 */

public class C_Delete {
    private Context context;
    private DBHelper dbHelper;

    public C_Delete(Context context) {
        this.context = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.update_data_preferences), Context.MODE_PRIVATE);
        int version = sharedPreferences.getInt(context.getString(R.string.lav_data_id), 0);
        this.dbHelper = new DBHelper(context, version);
    }

    public void deleteAll(String tableName){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("DELETE FROM "+tableName);
    }

    /**
     *
     * Remove all activity from the selected lead
     * @param slId = sla_inst_tab_id
     */
    public void deleteActivityFromLead(String slTabId, String slId) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String query = "DELETE from SIMBAK_LIST_ACTIVITY WHERE (sla_inst_tab_id = '"+slTabId+"' or SL_ID_ = '"+slId+"') and sla_last_pos = 0";
        db.execSQL(query);
    }


    /**
     *
     * Remove all activity by specifying sl_tab_id
     */
    public void deleteActivityWithSlaInstId(String slTabId) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("DELETE from SIMBAK_LIST_ACTIVITY WHERE sla_inst_tab_id= '"+slTabId+"'");
    }

    /**
     * Remove all lead
     */
    public void removeAllLead() {
        String query = "delete from SIMBAK_LEAD";
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL(query);
    }

    /**
     * Remove all lead activity
     */
    public void removeAllLeadActivity() {
        String query = "delete from SIMBAK_LIST_ACTIVITY";
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL(query);
    }

    public void removeAllMenuListFromUser(String agentCode) {
        if (!TextUtils.isEmpty(agentCode)) {
            String query = "delete from LST_USER_MENU where msag_id = '"+agentCode+"'";
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.execSQL(query);
        }

    }

    /**
     *
     * Remove agency lead by specifying it's id
     */
    public void deleteAgencyLead(String leadId) {
        if (!TextUtils.isEmpty(leadId)) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String query = "DELETE from LST_AGENCY_LEAD WHERE id = '"+leadId+"'";
            db.execSQL(query);
        }

    }
}
