package id.co.ajsmsig.nb.pointofsale.ui.lproductfabdescription


import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import id.co.ajsmsig.nb.R

import id.co.ajsmsig.nb.pointofsale.adapter.ProductDetailAdapter
import id.co.ajsmsig.nb.pointofsale.custom.LoadingButton
import id.co.ajsmsig.nb.databinding.PosFragmentProductFabdescriptionBinding
import id.co.ajsmsig.nb.pointofsale.handler.SelectionHandler
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import id.co.ajsmsig.nb.pointofsale.ui.fragment.BaseFragment
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.ProductDetail
import id.co.ajsmsig.nb.pointofsale.utils.fileName
import id.co.ajsmsig.nb.pointofsale.utils.share
import java.io.File
import id.co.ajsmsig.nb.pointofsale.utils.*
import org.jetbrains.anko.find


/**
 * A simple [BaseFragment] subclass.
 */
class ProductFABDescriptionFragment : BaseFragment(), SelectionHandler {

    private lateinit var dataBinding: PosFragmentProductFabdescriptionBinding


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductFABDescriptionVM::class.java)
        subscribeUI(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_product_fabdescription, container, false)

        return dataBinding.root
    }

    @SuppressLint("ResourceType")
    fun subscribeUI(viewModel: ProductFABDescriptionVM) {
        dataBinding.vm = viewModel
        dataBinding.handler = this

        viewModel.page = page

        viewModel.observableIsThereRider.observe(this, Observer {

            dataBinding.root.findViewById<View?>(4000)?.visibility = if (it == true) View.VISIBLE  else View.GONE
        })

        mainVM.subtitle.set(sharedViewModel.selectedProduct?.LsbsName)
    }

    override fun onClickSelection(selection: PageOption) {
        navigationController.optionSelected(selection)
    }
}// Required empty public constructor
