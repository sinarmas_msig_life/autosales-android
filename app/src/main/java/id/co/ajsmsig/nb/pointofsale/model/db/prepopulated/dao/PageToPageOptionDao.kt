package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageToPageOption


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface PageToPageOptionDao {
    @Query("SELECT * FROM PageToPageOption ORDER BY `order` asc")
    fun loadAllPageToPageOptions(): LiveData<List<PageToPageOption>>

    @Query("SELECT * FROM PageToPageOption WHERE pageId LIKE :pageId ORDER BY `order` asc")
    fun loadAllPageToPageOptions(pageId: String?): LiveData<List<PageToPageOption>>


    @Query("SELECT * FROM PageToPageOption WHERE pageId LIKE :pageId AND showOnlyIfSelectPageOptionIds LIKE :query ORDER BY `order` asc")
    fun loadAllPageToPageOptionsWithPreviouslySelectedIds(pageId: String?, query: String): LiveData<List<PageToPageOption>>

    @Query("SELECT * FROM PageToPageOption WHERE id = :id limit 1")
    fun loadPageToPageOption(id: Int?): LiveData<PageToPageOption>
}