package id.co.ajsmsig.nb.prop.activity;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.database.MemberTable;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.method.async.CrmRestClientUsage;
import id.co.ajsmsig.nb.crm.model.MemberModel;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.data.DisposableManager;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.espaj.database.E_Insert;
import id.co.ajsmsig.nb.espaj.database.E_Update;
import id.co.ajsmsig.nb.pointofsale.utils.Constants;
import id.co.ajsmsig.nb.prop.database.P_Insert;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.database.P_Update;
import id.co.ajsmsig.nb.prop.database.TableDataProposal;
import id.co.ajsmsig.nb.prop.database.TableProposalProduct;
import id.co.ajsmsig.nb.prop.database.TableProposalProductPeserta;
import id.co.ajsmsig.nb.prop.database.TableProposalProductRider;
import id.co.ajsmsig.nb.prop.database.TableProposalProductTopUp;
import id.co.ajsmsig.nb.prop.database.TableProposalProductULink;
import id.co.ajsmsig.nb.prop.fragment.P_DataProposalFragment;
import id.co.ajsmsig.nb.prop.fragment.P_DetailProposalFragment;
import id.co.ajsmsig.nb.prop.fragment.P_PilihanInvestasiFragment;
import id.co.ajsmsig.nb.prop.fragment.P_ResultFragment;
import id.co.ajsmsig.nb.prop.fragment.P_RiderFragment;
import id.co.ajsmsig.nb.prop.fragment.P_TopUpFragment;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.model.apimodel.request.ReserveVaRequest;
import id.co.ajsmsig.nb.prop.model.apimodel.request.ValidateVaRequest;
import id.co.ajsmsig.nb.prop.model.apimodel.response.ReserveVaResponse;
import id.co.ajsmsig.nb.prop.model.apimodel.response.ValidateVaResponse;
import id.co.ajsmsig.nb.prop.model.apimodel.response.Virtual;
import id.co.ajsmsig.nb.prop.model.form.P_MstDataProposalModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductPesertaModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductRiderModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductTopUpModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductUlinkModel;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;
import id.co.ajsmsig.nb.util.AppConfig;
import id.co.ajsmsig.nb.util.Const;
import id.co.ajsmsig.nb.util.MessageEvent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.co.ajsmsig.nb.prop.method.P_StaticMethods.getDefaultTPValue;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FieldSettingFlag.TOMBOL_TOPUP_PENARIKAN;
import static id.co.ajsmsig.nb.prop.model.hardCode.P_FragmentPagePosition.RESULT_PAGE;

public class P_MainActivity extends AppCompatActivity implements CrmRestClientUsage.UploadLeadListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.second_toolbar)
    Toolbar second_toolbar;
    private TextView tv_toolbar_title;

    private final static String TAG = P_MainActivity.class.getSimpleName();
    public static Bundle myBundle;
    public boolean isSaveState = false;

    public int maxPage = 0;
    public int currentPage = 1;
    private int jenisLoginBC;
    private int jenisLogin = 0;
    private int GROUP_ID = 0;
    private int lsbs_id = 0;
    private int flag_pos = 0;

    private String namaAgen = "";
    private String kodeAgen = "";
    private String kodeRegional = "";
    private String packet = "";
    private String noid = "";
    private String productId = null;
    private String productName = null;
    private String subProductID = null;


    private P_ProposalModel p_proposalModel = new P_ProposalModel();
    private P_MstDataProposalModel p_mstDataProposalModel;
    private P_MstProposalProductModel p_mstProposalProductModel;
    private ArrayList<P_MstProposalProductRiderModel> p_mstProposalProductRiderModels;
    private ArrayList<P_MstProposalProductTopUpModel> p_mstProposalProductTopUpModels;
    private ArrayList<P_MstProposalProductUlinkModel> p_mstProposalProductUlinkModels;
    private static ArrayList<Integer> List_answer;

    private EditText etVaNumber;
    private TextInputLayout inputLayoutVaNumber;
    private AlertDialog validateVaDialog;
    private AlertDialog validateVaDialogUSD;
    private int lastPagePosition;
    private MenuItem menuGenerateVa;
    private MenuItem menuValidateVa;
    private MenuItem menuShowVa;
    private ProgressDialog progressDialog;
    private static P_Insert insert;
    private static P_Update update;

    public void setSecondToolbarTitle(String title) {
        title = title.replace("{current_page}", String.valueOf(currentPage))
                .replace("{max_page}", String.valueOf((maxPage == 0) ? "-" : maxPage));
        tv_toolbar_title.setText(title);
    }

    public Toolbar getSecond_toolbar() {
        return second_toolbar;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        long leadTabId = -1;
        SharedPreferences sharedPreferences = getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
        namaAgen = sharedPreferences.getString("NAMA_AGEN", "");
        kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
        kodeRegional = sharedPreferences.getString("KODE_REGIONAL", "");
        jenisLogin = sharedPreferences.getInt("JENIS_LOGIN", 0);
        GROUP_ID = sharedPreferences.getInt("GROUP_ID", 0);
        jenisLoginBC = sharedPreferences.getInt("JENIS_LOGIN_BC", 0);
        // Or passed from the other activity

        insert = new P_Insert(P_MainActivity.this);
        update = new P_Update(P_MainActivity.this);

        //Kalau udah ada data
        if (extras != null) {
            long slTabId = extras.getLong(Const.INTENT_KEY_SL_TAB_ID);

            leadTabId = slTabId;
            noid = extras.getString(getString(R.string.noid_member), "");
            lsbs_id = extras.getInt(Constants.Companion.getLSBS_ID(), 0);
            if (GROUP_ID == 37 && (lsbs_id == 215 || lsbs_id == 224)) {
                lsbs_id = 0;
                Toast.makeText(this, "Produk yang di pilih belum ada di Autosales", Toast.LENGTH_LONG).show();
            }
            List_answer = extras.getIntegerArrayList("value");
            flag_pos = 1;
        }

        boolean isAddProposalMode = getIntent().getBooleanExtra(Const.INTENT_KEY_ADD_PROPOSAL_MODE, false);
        boolean isAddProposalModeSiap2U = getIntent().getBooleanExtra(Const.INTENT_KEY_ADD_PROPOSAL_MODE_SIAP2U, false);
        boolean isAddProposalModePOS = getIntent().getBooleanExtra(Const.INTENT_KEY_ADD_PROPOSAL_MODE_POS, false);
        String noProposalTab = getIntent().getStringExtra(Const.INTENT_KEY_NO_PROPOSAL_TAB);

        if (isAddProposalMode || isAddProposalModeSiap2U || isAddProposalModePOS) {
            long slTabId = getIntent().getLongExtra(Const.INTENT_KEY_SL_TAB_ID, -100);
            lastPagePosition = setBundle(slTabId, true, noProposalTab);
        } else {
            lastPagePosition = setBundle(leadTabId, false, noProposalTab);
        }

        setContentView(R.layout.p_activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        tv_toolbar_title = second_toolbar.findViewById(R.id.tv_toolbar_title);
        second_toolbar.inflateMenu(R.menu.menu_form_save);//changed


        /*set fragment page*/
        Fragment fragment;
        if (lastPagePosition == RESULT_PAGE) {
            fragment = new P_ResultFragment();
            currentPage = RESULT_PAGE; //User masuk dari tab proposal pada lead atau tab proposal, sehingga harus kita set current page nya jadi result page
        } else {
            fragment = new P_DataProposalFragment();
        }

        setFragment(fragment);

    }


    /**
     * Check whether the loaded proposal has already pulled a virtual account
     * Action :
     * true : Disable product reselect. (Disable product spinner)
     * false : Allow product reselect
     */
    private boolean isProposalAlreadyHasVA() {
        P_Select p_select = new P_Select(this);

        if (p_mstDataProposalModel != null) {

            String noProposalTab = p_mstDataProposalModel.getNo_proposal_tab();
            String vaNumber = p_select.getProposalVirtualAccount(noProposalTab);

            return !TextUtils.isEmpty(vaNumber);

        }

        return false;
    }

    private String getSavedVirtualAccountNumber() {
        P_Select p_select = new P_Select(this);

        if (p_mstDataProposalModel != null) {
            String noProposalTab = p_mstDataProposalModel.getNo_proposal_tab();
            String vaNumber = p_select.getProposalVirtualAccount(noProposalTab);
            return vaNumber;
        }

        return null;
    }

    /**
     * Check whether the loaded proposal is saved.
     * Condition :
     * Saved : no_proposal_tab will contains a string. Example : Prop.18.06.21.22.10.07.098
     * Not saved : no_prosal_tab will be null
     * <p>
     * If the proposal is not saved, hide the 'tarik virtual account' and 'validate virtual account' options menu
     *
     * @return
     */
    private boolean isProposalSaved() {
        if (p_mstDataProposalModel != null) {
            String noProposalTab = p_mstDataProposalModel.getNo_proposal_tab();

            return !TextUtils.isEmpty(noProposalTab);

        }

        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_va_proposal, menu);
        MenuItem menuGenerateVa = menu.findItem(R.id.action_generate_va);
        MenuItem menuValidateVa = menu.findItem(R.id.action_validate_va);
        MenuItem menuShowVa = menu.findItem(R.id.action_show_va);

        this.menuGenerateVa = menuGenerateVa;
        this.menuValidateVa = menuValidateVa;
        this.menuShowVa = menuShowVa;

        switch (jenisLogin) {
            case 2:
                menuGenerateVa.setVisible(true);
                menuValidateVa.setVisible(true);
                menuShowVa.setVisible(false);
                break;

            default:
                if (jenisLoginBC == Const.BANK_CODE_SINARMAS) {
                    menuGenerateVa.setVisible(true);
                    menuValidateVa.setVisible(true);
                    menuShowVa.setVisible(false);
                } else if (jenisLoginBC == Const.BANK_CODE_BTN_SYARIAH) {
                    menuGenerateVa.setVisible(false);
                    menuValidateVa.setVisible(true);
                    menuShowVa.setVisible(false);
                } else {
                    menuGenerateVa.setVisible(false);
                    menuValidateVa.setVisible(false);
                    menuShowVa.setVisible(false);
                }
                break;

        }


        Log.v("Page", "Current page "+currentPage + "of "+maxPage + "of LastPage " + lastPagePosition);
        if (lastPagePosition == RESULT_PAGE){ //ini kalau proposal buat dari awal sampai akhir
            displayOptionsMenu();
        } else if (currentPage == 1 && maxPage == 0 && lastPagePosition == RESULT_PAGE) {
            displayOptionsMenu();
        } else if (maxPage < currentPage && maxPage != 0) {
            displayOptionsMenu();
        } else {
            hideAllOptionsMenu();
        }



        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_keluar:
                onBackPressed();
//                isSaveState = true;
//                finish();
                break;
            case R.id.action_finish:
                finish();
                break;
            case R.id.action_generate_va:

                //Get the selected product id from the saved data
                if (p_mstProposalProductModel.getLsbs_id() != 0 && p_mstProposalProductModel.getLsbs_id() != null) {
                    productId = p_mstProposalProductModel.getLsbs_id().toString();
                    productName = new P_Select(P_MainActivity.this).getProductNameWithProductId(productId);
                    Log.v(TAG, "Product " + productId + " " + productName);
                }

                //This productUd would return null if user hasn't selected any product on P_DataProposalFragment
                if (productId == null) {
                    Toast.makeText(this, R.string.product_not_selected, Toast.LENGTH_SHORT).show();
                } else {
                    fetchVaFromServer("5", productId, "156", kodeAgen);
                }
                break;
            case R.id.action_validate_va:

                if (isBancassurance() && isLeadSlIdNull()) {
                    if (StaticMethods.isNetworkAvailable(P_MainActivity.this)) {
                        showLeadIsNullDialog();
                    } else {
                        Toast.makeText(P_MainActivity.this, "Tidak terkoneksi dengan internet. Mohon cek kembali koneksi internet Anda", Toast.LENGTH_SHORT).show();
                    }
                    break;
                }

                //Get the selected product id from the saved data
                if (p_mstProposalProductModel.getLsbs_id() != 0 && p_mstProposalProductModel.getLsbs_id() != null) {
                    productId = p_mstProposalProductModel.getLsbs_id().toString();
                    productName = new P_Select(P_MainActivity.this).getProductNameWithProductId(productId);
                }

                //This method would return null if user hasn't selected any product
                if (productId == null) {
                    Toast.makeText(this, R.string.product_not_selected, Toast.LENGTH_SHORT).show();
                } else {
                    //Bank Sinarmas
                    if (jenisLoginBC == Const.BANK_CODE_SINARMAS) {

                        if (isUsingUSDCurrency() && (isMagnaLink() || isPrimeLink()) ) {
                            displayUSDPaymentGuide(isLinkProduct());
                        } else {
                            displayValidateVaDialog(productId, "156");
                        }

                    } else if (jenisLoginBC == Const.BANK_CODE_BTN_SYARIAH) {
                        displayValidateVaDialog(productId, "161");
                    }
                }

                break;
            case R.id.action_show_va:
                String namaPp = "";
                String vaNumber = getSavedVirtualAccountNumber();

                if (p_mstDataProposalModel != null) {
                    namaPp = p_mstDataProposalModel.getNama_pp();
                }

                displaySavedVirtualAccount(namaPp, vaNumber);

                break;
            default:
                throw new IllegalArgumentException("Wrong id");
        }

        return super.onOptionsItemSelected(item);
    }

    private void displayOptionsMenu() {
        //Show only options menu if Siap2U, BSIM and BTNS
        if (shouldShowVirtualAccountOptionsMenu()) {

            if (isProposalSaved() && isProposalAlreadyHasVA()) {
                menuGenerateVa.setVisible(false);
                menuValidateVa.setVisible(false);
                menuShowVa.setVisible(true);
                //Disable product reselect spinner
                EventBus.getDefault().post(new MessageEvent(true));
            } else if (isProposalSaved() && !isProposalAlreadyHasVA()) {
                menuGenerateVa.setVisible(true);
                menuValidateVa.setVisible(true);
                menuShowVa.setVisible(false);

                //Hide "Tarik va" options menu
                if (isUsingUSDCurrency() && (isMagnaLink() || isPrimeLink()) ) {
                    menuGenerateVa.setVisible(false);
                }

            } else if (!isProposalSaved()) {
                menuGenerateVa.setVisible(false);
                menuValidateVa.setVisible(false);
                menuShowVa.setVisible(false);
            }

        } else {
            //Hide options menu if jenis login is not Siap2U or jenis login bc is not BSIM and BTNS
            menuGenerateVa.setVisible(false);
            menuValidateVa.setVisible(false);
            menuShowVa.setVisible(false);
        }

    }
    private void hideAllOptionsMenu() {
        menuGenerateVa.setVisible(false);
        menuValidateVa.setVisible(false);
        menuShowVa.setVisible(false);
    }

    /**
     * Show only options menu if login type is  Siap2U or jenis login BC is BSIM and BTNS
     *
     * @return
     */
    private boolean shouldShowVirtualAccountOptionsMenu() {
        return jenisLogin == Const.SIAP2U_LOGIN_TYPE || jenisLoginBC == Const.BANK_CODE_SINARMAS || jenisLoginBC == Const.BANK_CODE_BTN_SYARIAH;
    }

    /**
     * Display saved virtual account of the selected proposal when user pressed 'tampilkan virtual account' options menu
     *
     * @param virtualAccount
     */
    private void displaySavedVirtualAccount(String ppName, String virtualAccount) {
        //Init custom alert dialog
        View view = getLayoutInflater().inflate(R.layout.dialog_display_saved_va, null);
        TextView tvHint = view.findViewById(R.id.tvVirtualAccount);
        TextView tvVaNumber = view.findViewById(R.id.tvVaNumber);
        TextView tvCopy = view.findViewById(R.id.tvCopyVa);
        Button btnOk = view.findViewById(R.id.btnOk);

        String hintMessage = "Silakan gunakan nomor virtual account dibawah ini untuk nabasah\n\n " + "a/n " + ppName;
        tvHint.setText(hintMessage);
        tvVaNumber.setText(virtualAccount);
        tvCopy.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            if (clipboard != null) {
                ClipData clip = ClipData.newPlainText("va-number", virtualAccount);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(P_MainActivity.this, R.string.virtual_account_copied_to_clipboard, Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(P_MainActivity.this);
        builder.setView(view);
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();

        btnOk.setOnClickListener(v -> dialog.dismiss());
    }

    /**
     * Validate user's virtual account to check whether still valid
     *
     * @param lsbsId
     * @param lsbpId
     */
    private void displayValidateVaDialog(final String lsbsId, final String lsbpId) {

        //Init custom alert dialog
        View view = getLayoutInflater().inflate(R.layout.dialog_validate_va, null);
        inputLayoutVaNumber = view.findViewById(R.id.inputLayoutVaNumber);
        etVaNumber = view.findViewById(R.id.etVaNumber);
        CheckBox cbUseVaWhenValid = view.findViewById(R.id.cbUseVaWhenValid);
        etVaNumber.addTextChangedListener(new TextWatcherListener(etVaNumber));
        Button btnCheckVa = view.findViewById(R.id.btnCheckVa);

        AlertDialog.Builder builder = new AlertDialog.Builder(P_MainActivity.this);
        builder.setTitle(R.string.dialog_title_validate_va_no);
        builder.setView(view);

        btnCheckVa.setOnClickListener(v -> {
            boolean shouldUseVaWhenValid = cbUseVaWhenValid.isChecked();

            if (!validateVaNumberInput()) {
                Toast.makeText(P_MainActivity.this, R.string.no_va_is_empty, Toast.LENGTH_LONG).show();
            } else {
                String vaNumber = etVaNumber.getText().toString().trim();
                validateVaNumber(
                        AppConfig.getBaseUrlSPAJ().concat("va/api/json/valid_va"),
                        vaNumber,
                        lsbsId,
                        lsbpId,
                        shouldUseVaWhenValid
                );
                validateVaDialog.dismiss();
            }

        });

        validateVaDialog = builder.create();
        validateVaDialog.show();
    }

    private void displayUSDPaymentGuide(boolean isUnitLinkProduct) {

        View view = getLayoutInflater().inflate(R.layout.dialog_validate_va_usd, null);
        Button btnOk = view.findViewById(R.id.btnOk);

        TextView tvHeaderUnitLink = view.findViewById(R.id.tvHeaderUnitLink);
        TextView tvContentUnitLink = view.findViewById(R.id.tvContentUnitLink);

        TextView tvHeaderNonUnitLink = view.findViewById(R.id.tvHeaderNonUnitLink);
        TextView tvContentNonUnitLink = view.findViewById(R.id.tvContentNonUnitLink);

        if (isUnitLinkProduct) {
            tvHeaderNonUnitLink.setVisibility(View.GONE);
            tvContentNonUnitLink.setVisibility(View.GONE);
        } else {
            tvHeaderUnitLink.setVisibility(View.GONE);
            tvContentUnitLink.setVisibility(View.GONE);
        }

        btnOk.setOnClickListener(v -> {
            validateVaDialogUSD.dismiss();
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pembayaran Premi");
        builder.setView(view);


        validateVaDialogUSD = builder.create();
        validateVaDialogUSD.show();
    }

    /**
     * Do api call to check Virtual account number validity
     *
     * @param url
     * @param virtualAccountNo
     * @param lsbsId
     * @param lsbpId
     */
    private void validateVaNumber(String url, String virtualAccountNo, final String lsbsId, final String lsbpId, boolean shouldUseVaWhenValid) {
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);

        //Make a POST request body
        ValidateVaRequest request = new ValidateVaRequest();
        request.setNO_VA(virtualAccountNo);
        request.setLSBS_ID(lsbsId);
        request.setLSBP_ID(lsbpId);


        final ProgressDialog progressDialog = new ProgressDialog(P_MainActivity.this);
        progressDialog.setTitle(getString(R.string.dialog_title_checking_va_validity));
        progressDialog.setMessage(getString(R.string.pdialog_message_please_wait));
        progressDialog.show();

        Call<ValidateVaResponse> call = api.validateVirtualAccountNumber(url, request);
        call.enqueue(new Callback<ValidateVaResponse>() {
            @Override
            public void onResponse(Call<ValidateVaResponse> call, Response<ValidateVaResponse> response) {

                progressDialog.dismiss();
                String error = response.body().getError();
                String vaNumber = response.body().getNoVa();
                if (error.equals("")) {

                    if (shouldUseVaWhenValid) {

                        //Save the virtual account
                        p_mstDataProposalModel.setVirtual_acc(virtualAccountNo);
                        saveState(P_MainActivity.this, false);

                        //Hide the options menu
                        menuValidateVa.setVisible(false);
                        menuGenerateVa.setVisible(false);

                        //Show 'tampilkan va' dialog
                        menuShowVa.setVisible(true);

                        //Display the Va
                        String namaPp = "";
                        String virtualAccountNumber = getSavedVirtualAccountNumber();

                        if (p_mstDataProposalModel != null) {
                            namaPp = p_mstDataProposalModel.getNama_pp();
                        }
                        displaySavedVirtualAccount(namaPp, virtualAccountNumber);

                    } else {
                        //Hide the options menu
                        menuValidateVa.setVisible(false);
                        menuGenerateVa.setVisible(false);

                        //Show 'tampilkan va' dialog
                        menuShowVa.setVisible(true);

                        //Show re-confirmation dialog to offer user use the valid va
                        new AlertDialog.Builder(P_MainActivity.this)
                                .setTitle(R.string.dialog_title_va_valid)
                                .setMessage(R.string.dialog_msg_use_va_confirmation)
                                .setPositiveButton(R.string.yes, (dialog, which) -> {

                                    //Save the virtual account
                                    p_mstDataProposalModel.setVirtual_acc(virtualAccountNo);
                                    saveState(P_MainActivity.this, false);

                                    //Display the Va
                                    String namaPp = "";
                                    String virtualAccountNumber = getSavedVirtualAccountNumber();
                                    if (p_mstDataProposalModel != null) {
                                        namaPp = p_mstDataProposalModel.getNama_pp();
                                    }
                                    displaySavedVirtualAccount(namaPp, virtualAccountNumber);

                                    //Dismiss the dialog
                                    dialog.dismiss();
                                })
                                .setNegativeButton(R.string.no, (dialog, which) -> dialog.dismiss())
                                .show();
                    }

                } else {
                    Toast.makeText(P_MainActivity.this, error, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<ValidateVaResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(P_MainActivity.this, "Terjadi kesalahan. " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Check whether there are an available (unused) va
     * YES : display the virtual account
     * NO : do api call to fetch new virtual account
     *
     * @param jumlah number of VA to reserve
     * @param lsbsId product id
     * @param lsbpId
     * @param msagID agent code
     */
    private void fetchVaFromServer(final String jumlah, final String lsbsId, final String lsbpId, final String msagID) {
        P_Select p_select = new P_Select(P_MainActivity.this);
        int vaCount = p_select.getVaCount(lsbsId, msagID);

        if (vaCount == 0) {
            displayFetchVAFromServerConfirmation(jumlah, lsbsId, lsbpId, msagID);
        } else {
            displayAvailableVirtualAccountDialog();
        }

    }

    /**
     * Request new VA from the server confirmation dialog
     *
     * @param jumlah
     * @param lsbsId
     * @param lsbpId
     * @param msagID
     */
    private void displayFetchVAFromServerConfirmation(final String jumlah, final String lsbsId, final String lsbpId, final String msagID) {
        subProductID = p_mstProposalProductModel.getLsdbs_number().toString();
        String productNames = new P_Select(P_MainActivity.this).getProductNameForVa(productId, subProductID);
        new AlertDialog.Builder(P_MainActivity.this)
                .setTitle(R.string.dialog_title_virtual_account_not_found)
                .setMessage("Stock Virtual Account untuk product " + productNames + " tablet ini sudah habis. Apakah Anda ingin mendapatkan virtual account terbaru?")
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        reserveVa(
                                AppConfig.getBaseUrlSPAJ().concat("va/api/json/res_va"),
                                jumlah,
                                lsbsId,
                                lsbpId,
                                msagID
                        );
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    /**
     * Display available virtual account dialog to user
     */
    private void displayAvailableVirtualAccountDialog() {
        P_Select p_select = new P_Select(P_MainActivity.this);
        //Show the already existed VA
        final String selectedVirtualAccount = p_select.getVirtualAccount(productId, kodeAgen);
        //Get the available va stock count
        int availableVaStock = p_select.getVaCount(productId, kodeAgen);
        //Decrease it by 1. Because by the time the dialog displayed, it is already used one VA
        availableVaStock = availableVaStock - 1;

        //Init custom alert dialog
        View view = getLayoutInflater().inflate(R.layout.dialog_show_va, null);
        TextView tvVaNumber = view.findViewById(R.id.tvVaNumber);
        TextView tvCopy = view.findViewById(R.id.tvCopyVa);
        TextView tvVaStock = view.findViewById(R.id.tvVaStock);
        Button btnUseVa = view.findViewById(R.id.btnUseVa);
        tvVaNumber.setText(selectedVirtualAccount);
        tvCopy.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            if (clipboard != null) {
                ClipData clip = ClipData.newPlainText("va-number", selectedVirtualAccount);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(P_MainActivity.this, R.string.virtual_account_copied_to_clipboard, Toast.LENGTH_SHORT).show();
            }
        });


        if (availableVaStock > 0) {
            tvVaStock.setText("Sisa virtual account tersedia untuk produk " + productName + ": " + availableVaStock + " buah");
        } else {
            tvVaStock.setText("Sisa virtual account tersedia untuk produk " + productName + ": habis");
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(P_MainActivity.this);
        builder.setView(view);
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();

        btnUseVa.setOnClickListener(v -> {
            //Disable product spinner
            EventBus.getDefault().post(new MessageEvent(true));

            //Disable options menu tarik VA and validate VA
            menuGenerateVa.setVisible(false);
            menuValidateVa.setVisible(false);
            menuShowVa.setVisible(true);

            dialog.dismiss();
        });

        //Save the virtual account
        p_mstDataProposalModel.setVirtual_acc(selectedVirtualAccount);
        saveState(P_MainActivity.this, false);

        //Mark the displayed virtual account has been used by updating the value to '0'
        P_Update p_update = new P_Update(P_MainActivity.this);
        p_update.updateVirtualAccountIsUsed(selectedVirtualAccount);
    }

    /**
     * Do api call to reserve Virtual account number
     * Invoked when all of the va account on the device are used
     *
     * @param url
     * @param jumlah
     * @param lsbsId
     * @param lsbpId
     * @param msagId
     */
    private void reserveVa(String url, String jumlah, final String lsbsId, final String lsbpId, final String msagId) {
        ApiEndpoints api = AppController.getRetrofitInstance().create(ApiEndpoints.class);

        //Make a POST request body
        ReserveVaRequest request = new ReserveVaRequest();
        request.setJUMLAH(jumlah);
        request.setLSBS_ID(lsbsId);
        request.setLSBP_ID(lsbpId);
        request.setMSAG_ID(msagId);

        final ProgressDialog progressDialog = new ProgressDialog(P_MainActivity.this);
        progressDialog.setTitle(getString(R.string.pdialog_title_downloading_va));
        progressDialog.setMessage(getString(R.string.pdialog_message_please_wait));
        progressDialog.show();

        Call<ReserveVaResponse> call = api.reserveVirtualAccount(url, request);
        call.enqueue(new Callback<ReserveVaResponse>() {
            @Override
            public void onResponse(Call<ReserveVaResponse> call, Response<ReserveVaResponse> response) {
                progressDialog.dismiss();
                String error = response.body().getERROR();
                List<Virtual> virtualAccountList = response.body().getVirtual();
                Log.v(TAG, "Reserve VA success");
                if (error.equals("")) {

                    if (!virtualAccountList.isEmpty()) {
                        writeVirtualAccountToDb(lsbsId, lsbpId, msagId, "1", virtualAccountList);
                    } else {
                        Toast.makeText(P_MainActivity.this, R.string.no_va_available_on_the_server, Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(P_MainActivity.this, getString(R.string.error_occured) + error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ReserveVaResponse> call, Throwable t) {
                progressDialog.dismiss();
                if (t instanceof IOException) {
                    Toast.makeText(P_MainActivity.this, "Tidak dapat melakukan reserve virtual account karena tidak ada internet terdeteksi", Toast.LENGTH_LONG).show();
                    return;
                }
                Toast.makeText(P_MainActivity.this, "Terjadi kesalahan. " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Insert received virtual account to local database
     *
     * @param lsbsId
     * @param lsbpId
     * @param msagId
     * @param activeFlag
     * @param virtualAccountList
     */
    private void writeVirtualAccountToDb(String lsbsId, String lsbpId, String msagId, String activeFlag, List<Virtual> virtualAccountList) {
        //Parse to integer, because the table's variable data type is integer
        int lsbs = Integer.parseInt(lsbsId);
        int lsbp = Integer.parseInt(lsbpId);
        int msag = Integer.parseInt(msagId);
        int flag = Integer.parseInt(activeFlag);

        P_Insert p_insert = new P_Insert(P_MainActivity.this);
        p_insert.insertVirtualAccountList(lsbs, lsbp, msag, flag, virtualAccountList);

        displayAvailableVirtualAccountDialog();
    }


    private int setBundle(long leadTabId, boolean isAddProposalMode, String noProposalTab) {
        P_Select select = new P_Select(P_MainActivity.this);
        Cursor cursor;
        QueryUtil queryUtil = new QueryUtil(this);

        p_proposalModel = new P_ProposalModel();

        //Update proposal modex
        if (!isAddProposalMode) {

            cursor = select.getProposalInfo(noProposalTab);
            p_mstDataProposalModel = new TableDataProposal(this).getObject(cursor);

            P_Select p_select = new P_Select(this);

            String selection = getString(R.string.NO_PROPOSAL_TAB) + "=?";
            String[] selectionArgs = new String[]{p_mstDataProposalModel.getNo_proposal_tab()};
            cursor = p_select.query(
                    getString(R.string.MST_PROPOSAL_PRODUCT),
                    null,
                    selection,
                    selectionArgs,
                    null
            );
            p_mstProposalProductModel = new TableProposalProduct(this).getObject(cursor);
            p_mstProposalProductModel.setFlag_packet(p_mstDataProposalModel.getFlag_packet()); //berubah

            cursor = p_select.query(
                    getString(R.string.MST_PROPOSAL_PRODUCT_ULINK),
                    null,
                    selection,
                    selectionArgs,
                    null
            );
            p_mstProposalProductUlinkModels = new TableProposalProductULink(this).getObjectArray(cursor);

            cursor = p_select.query(
                    getString(R.string.MST_PROPOSAL_PRODUCT_TOPUP),
                    null,
                    selection,
                    selectionArgs,
                    null
            );
            TableProposalProductTopUp tableProposalProductTopUp = new TableProposalProductTopUp(this);
            p_mstProposalProductTopUpModels = tableProposalProductTopUp.getObjectArray(cursor);
            p_mstProposalProductTopUpModels = tableProposalProductTopUp.getObjectArrayCustom(p_mstProposalProductTopUpModels);

            cursor = p_select.query(
                    getString(R.string.MST_PROPOSAL_PRODUCT_RIDER),
                    null,
                    selection,
                    selectionArgs,
                    null
            );
            p_mstProposalProductRiderModels = new TableProposalProductRider(this).getObjectArray(cursor, true);


        } else {
            //Add proposal mode
            p_mstDataProposalModel = new P_MstDataProposalModel();
            p_mstProposalProductModel = new P_MstProposalProductModel();
            p_mstProposalProductUlinkModels = new ArrayList<>();
            p_mstProposalProductTopUpModels = getDefaultTPValue();
            p_mstProposalProductRiderModels = new ArrayList<>();

            if (jenisLogin == 2) {
                if (GROUP_ID == 40) {
                    String selection = getString(R.string.noid_member) + " = ?";
                    String[] selectionArgs = new String[]{noid};

                    p_mstDataProposalModel.setNoid(noid);

                    MemberModel memberModel = new MemberModel();
                    cursor = queryUtil.query(getString(R.string.TABLE_MEMBER),
                            null,
                            selection,
                            selectionArgs,
                            null
                    );
                    memberModel = new MemberTable(this).getObject(cursor);
                    p_mstDataProposalModel.setNama_pp(memberModel.getFULLNAME());
                    p_mstDataProposalModel.setNama_tt(memberModel.getFULLNAME());

                    p_mstDataProposalModel.setVirtual_acc(memberModel.getVIRTUAL_ACC());

                    Calendar calendar = StaticMethods.toCalendarMember(memberModel.getBTEXT());
                    p_mstDataProposalModel.setTgl_lahir_pp(StaticMethods.toStringDate(calendar, 0));
                    p_mstDataProposalModel.setTgl_lahir_tt(StaticMethods.toStringDate(calendar, 0));

                    p_mstDataProposalModel.setUmur_pp(Integer.parseInt(StaticMethods.onCountUsia(calendar)));
                    p_mstDataProposalModel.setUmur_tt(Integer.parseInt(StaticMethods.onCountUsia(calendar)));

                    p_mstDataProposalModel.setSex_pp(Integer.parseInt(memberModel.getSEXID()));
                    p_mstDataProposalModel.setSex_tt(Integer.parseInt(memberModel.getSEXID()));

                    if (noid.contains("s") || noid.contains("S")) {
                        p_mstProposalProductModel.setLsbs_id(200);
                        p_mstProposalProductModel.setLsdbs_number(7);
                        p_mstDataProposalModel.setFlag_packet(Integer.parseInt(memberModel.getPACKET()));
                    } else {
                        p_mstProposalProductModel.setLsbs_id(190);
                        p_mstProposalProductModel.setLsdbs_number(9);
                        p_mstDataProposalModel.setFlag_packet(Integer.parseInt(memberModel.getPACKET()));
                    }
                } else if (GROUP_ID == 7) {
                    if (leadTabId != -1) {
                        p_mstProposalProductModel.setLsbs_id(lsbs_id);
                        p_mstDataProposalModel.setLead_tab_id(leadTabId);
                        p_mstDataProposalModel.setFlag_pos(flag_pos);
                        p_mstDataProposalModel.setLast_page_position(0);
                        /*cursor = new C_Select(P_MainActivity.this).getLeadInfoForProposal(String.valueOf(leadTabId));
                        if (cursor != null) {
                            cursor.moveToFirst();
                            if (!cursor.isNull(cursor.getColumnIndexOrThrow(getString(R.string.SL_NAME)))) {
                                p_mstDataProposalModel.setNama_pp(cursor.getString(cursor.getColumnIndexOrThrow(getString(R.string.SL_NAME))));
                                p_mstDataProposalModel.setNama_tt(cursor.getString(cursor.getColumnIndexOrThrow(getString(R.string.SL_NAME))));
                                Log.d(TAG, "setBundle: getNama_PP " + cursor.getString(cursor.getColumnIndexOrThrow(getString(R.string.SL_NAME))));
                            }
                            if (!cursor.isNull(cursor.getColumnIndexOrThrow(getString(R.string.SL_BDATE)))) {
                                String SL_BDATE = cursor.getString(cursor.getColumnIndexOrThrow(getString(R.string.SL_BDATE)));
                                Calendar calendar = StaticMethods.toCalendar(SL_BDATE);

                                int umur = Integer.parseInt(StaticMethods.onCountUsia(calendar));
                                p_mstDataProposalModel.setTgl_lahir_pp(SL_BDATE);
                                p_mstDataProposalModel.setTgl_lahir_tt(SL_BDATE);
                                p_mstDataProposalModel.setUmur_pp(umur);
                                p_mstDataProposalModel.setUmur_tt(umur);
                            }
                            if (!cursor.isNull(cursor.getColumnIndexOrThrow(getString(R.string.SL_GENDER)))) {
                                p_mstDataProposalModel.setSex_pp(cursor.getInt(cursor.getColumnIndexOrThrow(getString(R.string.SL_GENDER))));
                                p_mstDataProposalModel.setSex_tt(cursor.getInt(cursor.getColumnIndexOrThrow(getString(R.string.SL_GENDER))));
                            }
                            // always close the cursor
                            cursor.close();
                        }*/
                    }
                }
            } else if (jenisLogin == 9) {
                if (leadTabId != -1) {
                    p_mstProposalProductModel.setLsbs_id(lsbs_id);
                    p_mstDataProposalModel.setLead_tab_id(leadTabId);
                    p_mstDataProposalModel.setFlag_pos(flag_pos);
                    cursor = new C_Select(P_MainActivity.this).getLeadInfoForProposal(String.valueOf(leadTabId));
                    if (cursor != null) {
                        cursor.moveToFirst();
                        if (!cursor.isNull(cursor.getColumnIndexOrThrow(getString(R.string.SL_NAME)))) {
                            p_mstDataProposalModel.setNama_pp(cursor.getString(cursor.getColumnIndexOrThrow(getString(R.string.SL_NAME))));
                            p_mstDataProposalModel.setNama_tt(cursor.getString(cursor.getColumnIndexOrThrow(getString(R.string.SL_NAME))));
                            Log.d(TAG, "setBundle: getNama_PP " + cursor.getString(cursor.getColumnIndexOrThrow(getString(R.string.SL_NAME))));
                        }
                        if (!cursor.isNull(cursor.getColumnIndexOrThrow(getString(R.string.SL_BDATE)))) {
                            String SL_BDATE = cursor.getString(cursor.getColumnIndexOrThrow(getString(R.string.SL_BDATE)));
                            Calendar calendar = StaticMethods.toCalendar(SL_BDATE);

                            int umur = Integer.parseInt(StaticMethods.onCountUsia(calendar));
                            p_mstDataProposalModel.setTgl_lahir_pp(SL_BDATE);
                            p_mstDataProposalModel.setTgl_lahir_tt(SL_BDATE);
                            p_mstDataProposalModel.setUmur_pp(umur);
                            p_mstDataProposalModel.setUmur_tt(umur);
                        }
                        if (!cursor.isNull(cursor.getColumnIndexOrThrow(getString(R.string.SL_GENDER)))) {
                            p_mstDataProposalModel.setSex_pp(cursor.getInt(cursor.getColumnIndexOrThrow(getString(R.string.SL_GENDER))));
                            p_mstDataProposalModel.setSex_tt(cursor.getInt(cursor.getColumnIndexOrThrow(getString(R.string.SL_GENDER))));
                        }
                        // always close the cursor
                        cursor.close();
                    }
                }
            }
            p_mstDataProposalModel.setNama_agen(namaAgen);
            p_mstDataProposalModel.setKode_agen(kodeAgen);
            p_mstDataProposalModel.setNama_user(namaAgen);
            p_mstDataProposalModel.setMsag_id(kodeAgen);
            p_mstDataProposalModel.setTgl_input(StaticMethods.getTodayDate());
            p_mstDataProposalModel.setFlag_aktif(1);
            p_mstDataProposalModel.setFlag_star(0);
            p_mstDataProposalModel.setFlag_proper(0);
        }

        myBundle = new Bundle();

        p_proposalModel.setMst_data_proposal(p_mstDataProposalModel);
        p_proposalModel.setMst_proposal_product(p_mstProposalProductModel);
        p_proposalModel.setMst_proposal_product_ulink(p_mstProposalProductUlinkModels);
        p_proposalModel.setMst_proposal_product_topup(p_mstProposalProductTopUpModels);
        p_proposalModel.setMst_proposal_product_rider(p_mstProposalProductRiderModels);
        myBundle.putParcelable(getString(R.string.proposal_model), p_proposalModel);

//        Log.d(TAG, "setBundle: last positiion" + p_mstDataProposalModel.getLast_page_position());
        return (p_mstDataProposalModel.getLast_page_position() != null) ? p_mstDataProposalModel.getLast_page_position() : 0;
    }

    private boolean isUsingUSDCurrency() {
        if (p_mstProposalProductModel != null && p_mstProposalProductModel.getLku_id() != null) {
            return p_mstProposalProductModel.getLku_id().equalsIgnoreCase("02");
        }

        return false;
    }

    private boolean isMagnaLink() {
        return p_mstProposalProductModel != null && p_mstProposalProductModel.getLsbs_id() == 213 && p_mstProposalProductModel.getLsdbs_number() == 1;
    }

    private boolean isPrimeLink() {
        return p_mstProposalProductModel != null && p_mstProposalProductModel.getLsbs_id() == 134 && p_mstProposalProductModel.getLsdbs_number() == 5;
    }

    private boolean isLinkProduct() {
        if (p_mstProposalProductModel != null) {
            int lsbsId = p_mstProposalProductModel.getLsbs_id();
            P_Select select = new P_Select(this);
            int lsgbId = select.selectLsgbId(lsbsId);
            return lsgbId == 17;
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        showExitConfirmationDialog();
    }

    public void showExitConfirmationDialog() {
        AlertDialog.Builder msgBox = new AlertDialog.Builder(this);
        msgBox.setTitle("Konfirmasi");
        msgBox.setMessage(R.string.exit_message_confirmation);
        msgBox.setNegativeButton(getString(R.string.tidak), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        msgBox.setPositiveButton(getString(R.string.iya), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        msgBox.show();
    }

    public static void showSavedProposalInformation(Context context) {
        AlertDialog.Builder msgBox = new AlertDialog.Builder(context);
        msgBox.setMessage(R.string.saved_proposal_info);
        msgBox.setNegativeButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //no code
            }
        });
        msgBox.show();
    }

    public void onKembaliPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        } else {
            onBackPressed();
        }
        currentPage--;
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: status");
        if (isSaveState)
            saveState(this, true);
    }


    public void setFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fl_container, fragment);
            fragmentTransaction.commit();
            currentPage = 1;
        }
    }

    public void setFragmentBackStack(Fragment fragment, String title) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
            fragmentTransaction.replace(R.id.fl_container, fragment);
            fragmentTransaction.addToBackStack(title);
            fragmentTransaction.commit();
            currentPage++;
        }
    }

    public void chooseFragmentPageProposal(FragmentManager fragmentManager, int position) {
        Fragment fragment;

        FragmentTransaction transaction;
        transaction = fragmentManager.beginTransaction();
        for (int i = 1; i <= position; i++) {
            switch (i) {
                case 1:
                    fragment = new P_DataProposalFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fl_container, fragment);
                    transaction.commit();
                    break;
                case 2:
                    fragment = new P_DetailProposalFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fl_container, fragment);
                    transaction.addToBackStack("Detail Produk");
                    transaction.commit();
                    break;
                case 3:
                    fragment = new P_PilihanInvestasiFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fl_container, fragment);
                    transaction.addToBackStack("Investasi");
                    transaction.commit();
                    break;
                case 4:
                    fragment = new P_TopUpFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fl_container, fragment);
                    transaction.addToBackStack("Top-Up");
                    transaction.commit();
                    break;
                case 5:
                    fragment = new P_RiderFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fl_container, fragment);
                    transaction.addToBackStack("Rider");
                    transaction.commit();
                    break;
                case 6:
                    fragment = new P_ResultFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fl_container, fragment);
                    transaction.addToBackStack("Result");
                    transaction.commit();
                    break;
            }
        }
//        fragmentTransaction.commit();
    }


    public static void saveState(Context context, boolean isShowSaveNotif) {

//        Log.d(TAG, "saveState: save position");
        P_ProposalModel proposalModel = myBundle.getParcelable(context.getString(R.string.proposal_model));
        assert proposalModel != null;
        P_MstDataProposalModel dataProposalModel = proposalModel.getMst_data_proposal();
        P_MstProposalProductModel productModel = proposalModel.getMst_proposal_product();
        ArrayList<P_MstProposalProductUlinkModel> ulinkModels = proposalModel.getMst_proposal_product_ulink();
        ArrayList<P_MstProposalProductTopUpModel> topUpModels = proposalModel.getMst_proposal_product_topup();
        ArrayList<P_MstProposalProductRiderModel> riderModels = proposalModel.getMst_proposal_product_rider();
        ContentValues cv;
//        String noProposalForInsertAfterDataProposal;
//        String noProposalTabForInsertDataProposal = StaticMethods.getNoProposalTab();
        String fixNoProposalTab = (dataProposalModel.getNo_proposal_tab() != null) ? dataProposalModel.getNo_proposal_tab() : StaticMethods.getNoProposalTab();
        long id;
        if (dataProposalModel.getId() == null) {
//            Log.d(TAG, "saveState: insert");
            //insert
            dataProposalModel.setNo_proposal_tab(fixNoProposalTab);
            id = insert.saveNewProposal(dataProposalModel);

            if (List_answer != null && List_answer.size() != 0) {
                new E_Insert(context).InsertProfilResiko(dataProposalModel.getNo_proposal_tab());
                new E_Update(context).UpdateProfileResikoPOS(dataProposalModel.getNo_proposal_tab(), List_answer);
            }
        } else {
            id = dataProposalModel.getId();
            update.updateProposal(id, dataProposalModel);
        }

        /*Delete smua dulu kecuali data proposal*/
        QueryUtil queryUtil = new QueryUtil(context);

        String selection = context.getString(R.string.NO_PROPOSAL_TAB) + "=?";
        String[] selectionArgs = new String[]{fixNoProposalTab};
        queryUtil.delete(context.getString(R.string.MST_PROPOSAL_PRODUCT),
                selection,
                selectionArgs);
        queryUtil.delete(context.getString(R.string.MST_PROPOSAL_PRODUCT_ULINK),
                selection,
                selectionArgs);
        queryUtil.delete(context.getString(R.string.MST_PROPOSAL_PRODUCT_TOPUP),
                selection,
                selectionArgs);
        queryUtil.delete(context.getString(R.string.MST_PROPOSAL_PRODUCT_RIDER),
                selection,
                selectionArgs);
        queryUtil.delete(context.getString(R.string.MST_PROPOSAL_PRODUCT_PESERTA),
                selection,
                selectionArgs);


        /*baru  insert*/
        dataProposalModel.setNo_proposal_tab(fixNoProposalTab);
        dataProposalModel.setId(id);
        productModel.setNo_proposal_tab(fixNoProposalTab);
        cv = new TableProposalProduct(context).getContentValues(productModel);
        queryUtil.insert(context.getString(R.string.MST_PROPOSAL_PRODUCT), cv);

        for (int i = 0; i < ulinkModels.size(); i++) {
            ulinkModels.get(i).setNo_proposal_tab(fixNoProposalTab);
            cv = new TableProposalProductULink(context).getContentValues(ulinkModels.get(i));
            queryUtil.insert(context.getString(R.string.MST_PROPOSAL_PRODUCT_ULINK), cv);
        }

        for (int i = 0; i < topUpModels.size(); i++) {
            if (topUpModels.get(i).getSelected()) {
                topUpModels.get(i).setNo_proposal_tab(fixNoProposalTab);
                cv = new TableProposalProductTopUp(context).getContentValues(topUpModels.get(i));
                queryUtil.insert(context.getString(R.string.MST_PROPOSAL_PRODUCT_TOPUP), cv);
            }
        }

//        list peserta di set disini karena datanya diloop dari rider

        for (int i = 0; i < riderModels.size(); i++) {
            riderModels.get(i).setNo_proposal_tab(fixNoProposalTab);
            cv = new TableProposalProductRider(context).getContentValues(riderModels.get(i));
            queryUtil.insert(context.getString(R.string.MST_PROPOSAL_PRODUCT_RIDER), cv);

            ArrayList<P_MstProposalProductPesertaModel> pesertaModels = riderModels.get(i).getP_mstProposalProductPesertaModels();
            if (pesertaModels != null) {
                Log.d(TAG, "saveState: peserta " + riderModels.get(i).getLsbs_id() + " size is " + pesertaModels.size());
                for (int y = 0; y < pesertaModels.size(); y++) {
                    P_MstProposalProductPesertaModel pesertaModel = pesertaModels.get(y);
                    pesertaModel.setNo_proposal_tab(fixNoProposalTab);
                    cv = new TableProposalProductPeserta(context).getContentValues(pesertaModel);
                    queryUtil.insert(context.getString(R.string.MST_PROPOSAL_PRODUCT_PESERTA), cv);
                }
            }
        }

        /*last step set noproposal for data proposal model*/
        proposalModel.setMst_data_proposal(dataProposalModel);
        P_MainActivity.myBundle.putParcelable(context.getString(R.string.proposal_model), proposalModel);

        if (isShowSaveNotif)
            showSavedProposalInformation(context);

//        if (dataProposalModel.getFlag_aktif() == 1) {
//            goToItemProposal(context, id);
//        } else {
//            Activity activity = (Activity) context;
//            activity.finish();
//        }
    }


    public void calculateMaxPage(int psetId, int lsbsId, int lsdbsNumber, int flagTTCalonBayi) {
        ArrayList<SparseIntArray> widgetView = new P_Select(this).selectLstProposalField(psetId);
        SparseIntArray display = widgetView.get(0);
//        SparseIntArray enable = widgetView.get(1);
        maxPage = 2;

        /*first check if there are any possible fund*/
        QueryUtil queryUtil = new QueryUtil(this);

        Cursor fundCursor = queryUtil.query(
                getString(R.string.TABLE_LST_PRODUCT_INVEST),
                null,
                getString(R.string.PSET_ID) + " = ?",
                new String[]{String.valueOf(psetId)},
                null
        );
        int fundCursorSize = fundCursor.getCount();
        fundCursor.close();

        if (fundCursorSize > 0) {
            maxPage++;
        }

        if (display.get(TOMBOL_TOPUP_PENARIKAN) == 1) {
            maxPage++;
        }

        Cursor riderCursor = queryUtil.query(
                getString(R.string.TABLE_LST_BISNIS_RIDER),
                new String[]{"RIDER_ID"},
                getString(R.string.LSBS_ID) + " = ? AND " + getString(R.string.LSDBS_NUMBER) + " = ?",
                new String[]{String.valueOf(lsbsId), String.valueOf(lsdbsNumber)},
                null
        );
        int riderCount = riderCursor.getCount();
        riderCursor.close();
        if (riderCount > 0) {
            if (lsbsId != 208) {
                maxPage++;
            } else {
                if (flagTTCalonBayi == 1) {
                    maxPage++;
                }
            }
        } else if (lsbsId == 190 && lsdbsNumber == 9 || lsbsId == 200 && lsdbsNumber == 7) { //khusus smile link ultimate atau SIAP2U dan juga yang syariah
            maxPage++;
        }


    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class TextWatcherListener implements TextWatcher {

        private View view;

        private TextWatcherListener(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etVaNumber:
                    validateVaNumberInput();
                    break;
            }
        }
    }

    private boolean validateVaNumberInput() {
        if (etVaNumber.getText().toString().trim().isEmpty()) {
            inputLayoutVaNumber.setError(getString(R.string.err_msg_field_cannot_be_empty));
            requestFocus(etVaNumber);
            return false;
        } else {
            inputLayoutVaNumber.setErrorEnabled(false);
        }

        return true;
    }

    /**
     * Register the eventbus. To listen when the selected product changed on P_Data Proposal
     */
    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    /**
     * Unregister the eventbus. To listen when the selected product changed on P_Data Proposal
     */
    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    /**
     * Receive and set the product id and product name from the event bus.
     *
     * @param event
     */
    @Subscribe
    public void onEvent(MessageEvent event) {
        productId = event.getProductId();
        productName = event.getProductName();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        DisposableManager.dispose();
    }

    private boolean isBancassurance() {
        if (jenisLogin != 0) {
            return jenisLogin == Const.BANCASSURANCE_LOGIN_TYPE;
        } else {
            Toast.makeText(this, "Login Anda tidak terdeteksi sebagai Bancassurance atau Agency", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private boolean isLeadSlIdNull() {
        if (p_mstDataProposalModel.getLead_tab_id() != null) {
            C_Select select = new C_Select(P_MainActivity.this);
            long leadTabId = p_mstDataProposalModel.getLead_tab_id();
            boolean isLeadSlIdNull = select.isLeadSlIdNull(leadTabId);
            return isLeadSlIdNull;
        }
        return false;
    }

    private void showLeadIsNullDialog() {
        new AlertDialog.Builder(P_MainActivity.this)
                .setTitle("Sinkronisasi lead diperlukan")
                .setMessage("Data lead nasabah ini belum di sinkron (di upload) ke server. Mohon untuk melakukan sinkronisasi terlebih dahulu sebelum melakukan tahap selanjutnya. Apakah Anda ingin melakukan sinkronisasi sekarang?")
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    dialog.dismiss();
                    uploadLeadToServer();
                })
                .setNegativeButton(android.R.string.no, (dialog, which) -> dialog.dismiss())
                .show();

    }

    private void uploadLeadToServer() {
        if (p_mstDataProposalModel.getLead_tab_id() != null) {
            long leadTabId = p_mstDataProposalModel.getLead_tab_id();

            List<Long> leadIds = new ArrayList<>();
            leadIds.add(leadTabId);

            CrmRestClientUsage crmRestClientUsage = new CrmRestClientUsage(this);
            crmRestClientUsage.setUploadLeadListener(this);
            crmRestClientUsage.uploadLead(leadIds);
            showUploadLeadProgressDialog();
        }

    }


    @Override
    public void onUploadLeadSuccess(String message) {
        Toast.makeText(P_MainActivity.this, message, Toast.LENGTH_SHORT).show();
        hideUploadLeadProgressDialog();
    }

    @Override
    public void onUploadLeadFailed(String errorMessage) {
        Toast.makeText(P_MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
        hideUploadLeadProgressDialog();
    }

    private void showUploadLeadProgressDialog() {
        progressDialog = new ProgressDialog(P_MainActivity.this);
        progressDialog.setTitle("Mengunggah lead");
        progressDialog.setMessage("Mohon tunggu");
        progressDialog.show();
    }
    private void hideUploadLeadProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

}
