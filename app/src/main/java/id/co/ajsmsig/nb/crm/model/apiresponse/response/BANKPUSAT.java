
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BANKPUSAT {

    @SerializedName("LSBP_PANJANG_REKENING")
    @Expose
    private int lSBPPANJANGREKENING;
    @SerializedName("LSBP_NAMA")
    @Expose
    private String lSBPNAMA;
    @SerializedName("LSBP_ID")
    @Expose
    private int lSBPID;
    @SerializedName("LSBP_MIN_PANJANG_REKENING")
    @Expose
    private int lSBPMINPANJANGREKENING;

    public int getLSBPPANJANGREKENING() {
        return lSBPPANJANGREKENING;
    }

    public void setLSBPPANJANGREKENING(int lSBPPANJANGREKENING) {
        this.lSBPPANJANGREKENING = lSBPPANJANGREKENING;
    }

    public String getLSBPNAMA() {
        return lSBPNAMA;
    }

    public void setLSBPNAMA(String lSBPNAMA) {
        this.lSBPNAMA = lSBPNAMA;
    }

    public int getLSBPID() {
        return lSBPID;
    }

    public void setLSBPID(int lSBPID) {
        this.lSBPID = lSBPID;
    }

    public int getLSBPMINPANJANGREKENING() {
        return lSBPMINPANJANGREKENING;
    }

    public void setLSBPMINPANJANGREKENING(int lSBPMINPANJANGREKENING) {
        this.lSBPMINPANJANGREKENING = lSBPMINPANJANGREKENING;
    }

}
