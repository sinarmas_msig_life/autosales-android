package id.co.ajsmsig.nb.crm.method.async;
/*
 Created by faiz_f on 07/08/2017.
 */

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.BankPusatResponse;
import id.co.ajsmsig.nb.crm.model.apiresponse.response.DataVersionResponse;
import id.co.ajsmsig.nb.data.ApiEndpoints;
import id.co.ajsmsig.nb.di.AppController;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.activity.E_SertifikatPdfReader;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.method.JSONToString;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.method.P_StaticMethods;
import id.co.ajsmsig.nb.prop.model.form.P_ProposalModel;
import id.co.ajsmsig.nb.util.AppConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpajApiRestClientUsage {
    private final String TAG = SpajApiRestClientUsage.class.getSimpleName();
    private Context context;
    private ListenerInfo mListenerInfo;

    private static class ListenerInfo {
        AuthenticationListener mAuthListner;
        UploadImageListener mUploadImageLIstener;
        SertifikatListener sertifikatListener;
        getVaListener vaListener;
        generateCertificateListener generateCertificateListener;
    }

    public SpajApiRestClientUsage(Context context) {
        this.context = context;
    }

    //set All Listener Here
    private ListenerInfo getListenerInfo() {
        if (mListenerInfo != null) {
            return mListenerInfo;
        }
        mListenerInfo = new ListenerInfo();
        return mListenerInfo;
    }

    public void setOnAuthenticationListener(AuthenticationListener l) {
        getListenerInfo().mAuthListner = l;
    }

    public void setonSertifikatListener(SertifikatListener l) {
        getListenerInfo().sertifikatListener = l;
    }

    public void setonGetVaListener(getVaListener l) {
        getListenerInfo().vaListener = l;
    }

    public void setonUploadImage(UploadImageListener l) {
        getListenerInfo().mUploadImageLIstener = l;
    }

    public void setOnGenerateCertificateListener(generateCertificateListener listener) {
        getListenerInfo().generateCertificateListener = listener;
    }

    //Set Interface Here
    public interface AuthenticationListener {
        void onAuthSuccess(int statusCode, Header[] headers, JSONArray response);

        void onAuthFailure(final int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse);

        void onAuthFailure(int statusCode, Header[] headers, String responseString, Throwable throwable);

    }

    public interface SertifikatListener {
        void onSertifikatSuccess(int statusCode, Header[] headers, JSONObject response);

        void onSertifikatFailure(String title, String message);
    }

    public interface getVaListener {
        void ongetVaSuccess(int statusCode, Header[] headers, JSONObject response);

        void ongetVaFailure(String title, String message);
    }

    public interface UploadImageListener {
        void onUploadImageSuccess(int statusCode, Header[] headers, byte[] responseBody);

        void onUploadImageFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error);
    }

    public interface generateCertificateListener {
        void onGenerateCertificateSuccess(int statusCode, Header[] headers, JSONObject response);

        void onGenerateCertificateSuccess(String title, String message, File file);

        void onGenerateCertificateFailure(String title, String message);
    }

    //set All Method Here
    public boolean performAuthentication(final String email, final String password) {
        final boolean result;
        final ListenerInfo li = mListenerInfo;
        if (li != null && li.mAuthListner != null) {
            try {
                JSONObject jsonParams = new JSONObject();
                jsonParams.put("username", email);
                jsonParams.put("password", password);
                StringEntity entity = new StringEntity(jsonParams.toString());
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                //ini diganti ke SpajApiRestClient kalau production
                EwsLoginRestClient.post(context, "member/api/json/spajAgentLogin", entity, "application/json", 10000, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
//                    Log.d(TAG, "onSuccess: " + response.toString());
                        li.mAuthListner.onAuthSuccess(statusCode, headers, response);
                    }

                    @Override
                    public void onFailure(final int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        li.mAuthListner.onAuthFailure(statusCode, headers, throwable, errorResponse);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        li.mAuthListner.onAuthFailure(statusCode, headers, responseString, throwable);
                    }

                });
            } catch (JSONException | UnsupportedEncodingException e) {
                Log.e(TAG, "login: error is " + e.getMessage());
            }
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    public void getSertifikatSPAJ(Context context, final EspajModel me) {
        final boolean result;
        final ListenerInfo li = mListenerInfo;
        if (li != null && li.sertifikatListener != null) {
            try {
//                Gson gson = new Gson();
                final String spajJson = JSONToString.JSONSertifikat(context,me);
                Log.v(TAG, "Get certificate SPAJ json : " + spajJson);
                /*TODO habis production hapus backup*/
                StaticMethods.backUpToJsonFile(spajJson, "SpajJson", "SertifikatJson");
                StringEntity entity = new StringEntity(spajJson);
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                SpajApiRestClient.post(context, "spaj/api/json/get_nopol", entity, "application/json", 60000, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        li.sertifikatListener.onSertifikatSuccess(statusCode, headers, response);

                    }

                    @Override
                    public void onFailure(final int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        li.sertifikatListener.onSertifikatFailure("Gagal", throwable.getMessage() + errorResponse);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        li.sertifikatListener.onSertifikatFailure("Gagal", throwable.getMessage() + errorResponse);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        li.sertifikatListener.onSertifikatFailure("Gagal", throwable.getMessage() + responseString);
                    }

                });
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "login: error is " + e.getMessage());
            }
        }
    }

    public void getVirtualAcc(final EspajModel me) {
        final boolean result;
        final ListenerInfo li = mListenerInfo;
        if (li != null && li.vaListener != null) {
            try {
                int lsbp_id = 0;
                String spajJson = "";
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("GADGET_SPAJ_KEY", me.getModelID().getSPAJ_ID_TAB());
                jsonObject.put("LSBS_ID", me.getUsulanAsuransiModel().getKd_produk_ua());
                if (new E_Select(context).getLine_Bus(me.getUsulanAsuransiModel().getKd_produk_ua()) == 3) { //SYARIAH
                    if (me.getDetilAgenModel().getJENIS_LOGIN_BC() == 2) {
                        lsbp_id = 224;
                    } else {
                        lsbp_id = new E_Select(context).getLSBP_VA(me.getDetilAgenModel().getJENIS_LOGIN(), me.getDetilAgenModel().getJENIS_LOGIN_BC(),
                                me.getDetilAgenModel().getLca_id(), me.getDetilAgenModel().getLwk_id());
                    }
                } else {
                    lsbp_id = new E_Select(context).getLSBP_VA(me.getDetilAgenModel().getJENIS_LOGIN(), me.getDetilAgenModel().getJENIS_LOGIN_BC(),
                            me.getDetilAgenModel().getLca_id(), me.getDetilAgenModel().getLwk_id());
                }
                jsonObject.put("LSBP_ID", lsbp_id);

                JSONObject JSONFINAL = new JSONObject();
                JSONFINAL.put("ESPAJ", jsonObject);
                JSONFINAL.put("CLIENT", "ANDROID");
                JSONFINAL.put("APP", "AUTOSALES");
                spajJson = JSONFINAL.toString();
                StaticMethods.backUpToJsonFile(spajJson, "JsonVa", "SpajJson");

                StringEntity entity = new StringEntity(spajJson);
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                SpajApiRestClient.post(context, "va/api/json/get_va/", entity, "application/json", 60000, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        li.vaListener.ongetVaSuccess(statusCode, headers, response);

                    }

                    @Override
                    public void onFailure(final int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        li.vaListener.ongetVaFailure("Gagal", throwable.getMessage() + errorResponse);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        li.vaListener.ongetVaFailure("Gagal", throwable.getMessage() + errorResponse);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        li.vaListener.ongetVaFailure("Gagal", throwable.getMessage() + responseString);
                    }

                });
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "login: error is " + e.getMessage());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void performUploadImage(String path, String file) {
        final boolean result;
        final ListenerInfo li = mListenerInfo;
        if (li != null && li.mUploadImageLIstener != null) {
            try {
                RequestParams params = new RequestParams();
                params.put("uploaded_file", new File(file));
                SpajApiRestClient.post(path, params, 100, 60000, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        li.mUploadImageLIstener.onUploadImageSuccess(statusCode, headers, responseBody);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.d(TAG, "onFailure: " + error);
                    }

                });
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

//    public void getBank() {
//        final boolean result;
//        final ListenerInfo li = mListenerInfo;
//        String path = "";
//        path = "spaj/api/json/spaj_bank?type=pusat";
//        if (li != null && li.bankListnener != null) {
//
//            SpajApiBankRestClient.get(path, null, 60000, new JsonHttpResponseHandler() {
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//                    li.bankListnener.onGetBankSuccess(statusCode, headers, response);
//
//                }
//
//                @Override
//                public void onFailure(final int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//                    li.bankListnener.ongetBankFailure("Gagal", throwable.getMessage() + errorResponse);
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
//                    li.bankListnener.ongetBankFailure("Gagal", throwable.getMessage() + errorResponse);
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                    li.bankListnener.ongetBankFailure("Gagal", throwable.getMessage() + responseString);
//                }
//            });
//        }
//    }

//    public void getBankCab() {
//        final boolean result;
//        final ListenerInfo li = mListenerInfo;
//        String path = "";
//        path = "spaj/api/json/spaj_bank?type=cabang";
//        if (li != null && li.bankCabListener != null) {
//
//            SpajApiBankRestClient.get(path, null, 60000, new JsonHttpResponseHandler() {
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//                    li.bankCabListener.onGetBankCabSuccess(statusCode, headers, response);
//
//                }
//
//                @Override
//                public void onFailure(final int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//                    li.bankCabListener.ongetBankCabFailure("Gagal", throwable.getMessage() + errorResponse);
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
//                    li.bankCabListener.ongetBankCabFailure("Gagal", throwable.getMessage() + errorResponse);
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                    li.bankCabListener.ongetBankCabFailure("Gagal", throwable.getMessage() + responseString);
//                }
//            });
//        }
//    }

    public void generateCertificate(Context context, EspajModel model) {
        final ListenerInfo li = mListenerInfo;
        String path = "spaj/api/json/generate_sertifikat";

        if (li != null && li.generateCertificateListener != null) {

            try {
                String spajJson = JSONToString.JSONSertifikat(context, model);
                StaticMethods.backUpToJsonFile(spajJson, "SpajJson", "SertifikatJson");
                StringEntity entity = new StringEntity(spajJson);
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                SpajApiRestClient.post(context, path, entity, "application/json", 100000, new FileAsyncHttpResponseHandler(context) {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                        li.generateCertificateListener.onGenerateCertificateFailure("Gagal melakukan proses download sertifikat","Mohon maaf. Terjadi kesalahan "+statusCode + " " +throwable.getMessage());
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, File file) {
                        li.generateCertificateListener.onGenerateCertificateSuccess("Berhasil!", "File berhasil diunduh", file);
                    }
                });

            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "Generate certificate exception : " + e.getMessage());
            }
        }
    }
}
