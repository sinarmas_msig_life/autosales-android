package id.co.ajsmsig.nb.prop.method.calculateIlustration;

import android.content.Context;
import android.util.Log;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.method.util.ArrUtil;
import id.co.ajsmsig.nb.prop.method.util.CommonUtil;
import id.co.ajsmsig.nb.prop.method.util.FormatNumber;
import id.co.ajsmsig.nb.prop.method.util.ProposalStringFormatter;
import id.co.ajsmsig.nb.prop.model.hardCode.Proposal;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.IllustrationResultVO;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.S_biaya;


public class IllustrationFormula220B extends IllustrationFormula {

//    private static final Logger logger = LoggerFactory.getLogger(IllustrationFormula220.class);

    IllustrationFormula220B(Context context) {
        super(context);
    }

    @Override
    public HashMap<String, Object> getIllustrationResult(String no_proposal) {
        IllustrationResultVO result = new IllustrationResultVO();
        ArrayList<LinkedHashMap<String, String>> mapList = new ArrayList<>();
        HashMap<String, Object> mapResult = new HashMap<>();
        ArrayList<HashMap<String, Object>> topupDrawList = getSelect().selectTopupAndWithdrawList(no_proposal);
        Integer defaultTopupDrawListSize = 50;// Di eproposal topup list selalu 50 walaupun tidak diisi
        String premiumTotal;
        String topup;
        String draw;
        String bonus = "0.00";

        int li_bagi = 1000;
        double[] ldec_bak ;
        double[] ldec_hasil_ppokok = new double[3 + 1];
        double[] ldec_hasil_ptu = new double[3 + 1];
        double ldec_bak_tut = 0.05;
        double ldec_akuisisi;
        double[][] ldec_hasil_invest = new double[5 + 1][3 + 1];
        double[] ldec_tarik = new double[] { Proposal.DUMMY_ZERO, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0};
        double ldec_wdraw;
        double[] ldec_premi_bulan = new double[12 + 1];
        double ldec_topup;
        double[] ldec_bunga_avg = new double[3 + 1]; //= {0.06, 0.1, 0.08, 0.18, 0.12, 0.25} //{0.09, 0.06, 0.11, 0.15, 0.165, 0.25}  //fixed:0.09, 0.11, 0.15 (0.165); dynamic:0.08, 0.11, 0.18 (0.195)
        double ldec_coi = 0, ldec_mfc, ldec_sc, ldec_cost, ldec_ppokok, ldec_ptu, ldec_aph, ldec_ff, ldec_temp = 0; //, ldec_man[10+1] = {1, 1, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7}
        boolean[] lb_minus = { Proposal.DUMMY_FALSE, false, false, false };
        S_biaya lstr;
        double ldec_manfaat, ldec_premi_setahun = 0;
        ldec_mfc = 20000;
        double[] flag_syarat_bonus_premi = new double[3 + 1];
        double[] ldec_premi_ppokok_bulan = new double[12 + 1];
        double ldec_premi_ppokok_setahun = 0;
        double[] ldec_basic_wdraw = new double[3 + 1];

        Integer umur_tt = 0, bisnis_id = 0, bisnis_no = 0, ins_per = 0, lscb_id = 0, thn_cuti_premi = 0;
        double premi = 0, premi_komb = 0, premi_pokok = 0, up = 0;
        String lku_id = "00";
        HashMap<String, Object> dataProposal = getSelect().selectDataProposal(no_proposal);
        if(!CommonUtil.isEmpty(dataProposal)) {
            bisnis_id = ((BigDecimal) dataProposal.get("LSBS_ID")).intValue();
            bisnis_no = ((BigDecimal) dataProposal.get("LSDBS_NUMBER")).intValue();
            umur_tt =  Integer.valueOf(dataProposal.get("UMUR_TT").toString());
            premi =  Double.valueOf(dataProposal.get("PREMI").toString());
            premi_komb =  Double.valueOf(dataProposal.get("PREMI_KOMB").toString());
            premi_pokok =  Double.valueOf(dataProposal.get("PREMI_POKOK").toString());
            up =  Double.valueOf(dataProposal.get("UP").toString());
            ins_per =  Integer.valueOf(dataProposal.get("THN_MASA_KONTRAK").toString());
            lscb_id = Integer.valueOf(dataProposal.get("LSCB_ID").toString());
            lku_id = dataProposal.get("LKU_ID").toString();
            thn_cuti_premi =  Integer.valueOf(dataProposal.get("THN_CUTI_PREMI").toString());
        }

        Integer pset_id = getSelect().selectPsetIdProposal(no_proposal);

        if(Proposal.CUR_USD_CD.equals(lku_id)){
            ldec_mfc = 2;
            li_bagi = 1;
        }
        for( int i = 1; i <= 12; i++ )
        {
            ldec_premi_bulan[ i ] = 0;
            if( i == 1 ){
                ldec_premi_bulan[ i ] = premi;
                ldec_premi_ppokok_bulan[ i ] = premi_pokok;
            }
            if( Proposal.PAY_MODE_CD_TRIWULANAN == lscb_id )
            {
                if( i == 4 || i == 7 || i == 10 ) {
                    ldec_premi_bulan[ i ] = premi;
                    ldec_premi_ppokok_bulan[ i ] = premi_pokok;
                }
            }
            else if( Proposal.PAY_MODE_CD_SEMESTERAN == lscb_id )
            {
                if( i == 7 ) {
                    ldec_premi_bulan[ i ] = premi;
                    ldec_premi_ppokok_bulan[ i ] = premi_pokok;
                }
            }
            else if( Proposal.PAY_MODE_CD_BULANAN == lscb_id )
            {
                ldec_premi_bulan[ i ] = premi;
                ldec_premi_ppokok_bulan[ i ] = premi_pokok;
            }
            ldec_premi_setahun += ldec_premi_bulan[ i ];
            ldec_premi_ppokok_setahun += ldec_premi_ppokok_bulan[ i ];
        }
        //
        for(int i = 1 ; i <= 3 ; i++){
            ldec_hasil_invest[1][i] = 0;
            ldec_hasil_ppokok[i] = 0;
            ldec_hasil_ptu[i] = 0;
            //ldec_pre_pp[i] = 0
            //ldec_pre_tu[i] = 0
            flag_syarat_bonus_premi[i] = 0;
            ldec_basic_wdraw[i] = 0;
        }
        //
        ldec_manfaat = up;
        //    Biaya Akuisisi & asumsi penarikan (1 tarik, 1-nya tdk tarik)
        lstr = getBiaya(bisnis_id, bisnis_no, no_proposal);
        ldec_bak = lstr.bak;

        ldec_bunga_avg = getSelect().selectBungaAvg( no_proposal );

        double[] np = new double[4];
        double[] celaka = new double[4];

        boolean cekAdaTarik = false;
        double ldec_tarik_cek = 0;
        for( int i = 1; i <= ins_per; i++ )
        {

            if( i <= ArrUtil.upperBound( lstr.tarik ) ) ldec_tarik_cek = lstr.tarik[ i ];

            if( ldec_tarik_cek > 0){
                cekAdaTarik = true;
                break;
            }
        }

        int j;
        for( int i = 1; i <= ins_per; i++ )
        {
//          surrender charge
            ldec_sc = 0;
            ldec_wdraw = 0;
            ldec_topup = 0;
            ldec_coi = getLdec_coi(dataProposal, i);

            //if i <= Upperbound(lstr.tarik) Then   ldec_wdraw = lstr.tarik[i]
            if(i <= ArrUtil.upperBound(lstr.topup)) ldec_topup = lstr.topup[i];

            if(ArrUtil.upperBound(ldec_bak) > i ){
                ldec_akuisisi = ldec_bak[i];
            }else{
                ldec_akuisisi = 0;
            }

            if(i <= 7) ldec_sc = ldec_tarik[i];
            //
            ldec_cost = (ldec_coi + ldec_mfc);

            for(int k = 1 ; k <= 3; k++){
                if(i <= ArrUtil.upperBound(lstr.tarik)) ldec_wdraw = lstr.tarik[i];
                for(int li_bulan = 1 ; li_bulan <= 12 ; li_bulan++){
                    ldec_cost = (ldec_coi + ldec_mfc);
                    ldec_ppokok = 0;
                    ldec_ptu = 0;
                    ldec_ff = 0;
                    ldec_aph = 0;
                    if(i <= thn_cuti_premi){
                        //ldec_ppokok = ldec_premi_bulan[li_bulan] * (istr_prop.pct_premi / 100);
                        ldec_ppokok = ldec_premi_bulan[li_bulan] * (premi_komb / 100)* ( 1 - ldec_akuisisi);
                        ldec_ptu = (ldec_premi_bulan[li_bulan] * (100 - premi_komb) / 100);
                    }
                    if(li_bulan == 1) ldec_ptu += ldec_topup;
                    if(li_bulan == 1) {
                        ldec_basic_wdraw[k] = ldec_wdraw*( 1 + ldec_sc );
                    }
                    //ldec_hasil_ppokok[k] = (ldec_ppokok + ldec_hasil_ppokok[k]) * (Math.pow((1 + ldec_bunga_avg[k]),( ( double ) 1/12)));

                    if(!cekAdaTarik){
                        flag_syarat_bonus_premi[k] = flag_syarat_bonus_premi( i , dataProposal , k , ldec_premi_ppokok_setahun);
                    }

                    if(li_bulan == 12){
                        //ldec_hasil_ppokok[k] = FormatNumber.round(( ldec_ppokok + ldec_hasil_ppokok[k] ) * FormatNumber.round(Math.pow( ( 1 + ldec_bunga_avg[ k ] ), ( ( double ) 1 / 12 )), 15),2) + flag_syarat_bonus_premi[k];
                        //ldec_hasil_ppokok[k] =  (( ldec_ppokok + ldec_hasil_ppokok[k] ) * (Math.pow((1 + ldec_bunga_avg[k]),( ( double ) 1/12))) ) - (ldec_basic_wdraw[k] * (Math.pow((1 + ldec_bunga_avg[k]),( ( double ) 1/12)))) + flag_syarat_bonus_premi[k];

                        ldec_hasil_ppokok[k] = FormatNumber.round(( ldec_ppokok * FormatNumber.round(Math.pow( ( 1 + ldec_bunga_avg[ k ] ), ( ( double ) 1 / 12 )), 15))  + (ldec_hasil_ppokok[k]  * FormatNumber.round(Math.pow( ( 1 + ldec_bunga_avg[ k ] ), ( ( double ) 1 / 12 )), 15)) - (ldec_basic_wdraw[k] * FormatNumber.round(Math.pow( ( 1 + ldec_bunga_avg[ k ] ), ( ( double ) 1 / 12 )), 15)),2) + flag_syarat_bonus_premi[k];

                    }else{
                        ldec_hasil_ppokok[k] = FormatNumber.round(( ldec_ppokok + ldec_hasil_ppokok[k]) * FormatNumber.round(Math.pow( ( 1 + ldec_bunga_avg[ k ] ), ( ( double ) 1 / 12 )), 15),2);
                    }

                    if(i == 1){
//                        logger.info("***-------ldec_bunga_avg[k] "+ldec_bunga_avg[k]);
                        Log.d(TAG, "getIllustrationResult: ***-------ldec_bunga_avg[k" + ldec_bunga_avg[k]);
                    }
                    //ldec_ppokok = ldec_ppokok * ((1 + ldec_bunga_avg[k])^(1/12))
                    //ldec_hasil_ppokok[k] = ldec_hasil_ppokok[k] * ((1 + ldec_bunga_avg[k])^(1/12)) + ldec_ppokok

                    ldec_hasil_ptu[k] = (ldec_ptu *( 1 - ldec_bak_tut) * (Math.pow((1 + ldec_bunga_avg[k]),( ( double ) 1/12)))) + ((ldec_hasil_ptu[k]) * (Math.pow((1 + ldec_bunga_avg[k]),( ( double ) 1/12)))) ;

                    //if k = 1 and i < 3 then messagebox(string(i) + '/' + string(li_bulan), string(ldec_hasil_ppokok[k], '#,##0.00'))
                    if(i == 1 && li_bulan == 1){
                        //      ldec_cost *= 2;
                        //ldec_pre_pp[k] = ldec_hasil_ppokok[k]
                        //ldec_pre_tu[k] = ldec_hasil_ptu[k]
                    }

                    //  if(i == istr_prop.ins_per && li_bulan == 12) ldec_cost = 0;

                    //if(i <= 2) ldec_ff = (ldec_hasil_ptu[k] * 0 / 12);
                    //if(i <= istr_prop.pay_per) ldec_ff += (ldec_hasil_ppokok[k] * 0.025 / 12);

                    if(i <= 7) ldec_ff += (ldec_hasil_ppokok[k] * 0.0475 / 12);
                    //premium holiday
                    if(i <= 5 && thn_cuti_premi <= 5){
                        //       if(i > istr_prop.cuti_premi) ldec_aph = ldec_fph[i] / (1 - ldec_fph[i]) * (ldec_cost + ldec_ff);
                    }
                    ldec_temp = ldec_hasil_ptu[k] - (ldec_cost + ldec_ff + ldec_aph);
                    //if k = 2 and i < 3 then messagebox(string(i) + '/' + string(li_bulan), string(ldec_hasil_ppokok[k], '#,##0.00') + ' / ' + string(ldec_hasil_ptu[k], '#,##0.00') + ' / ' + string(ldec_cost, '#,##0.00') + ' / ' + string(ldec_ff, '#,##0.00'))
                    //if k = 2 and i < 3 then messagebox(string(i) + '/' + string(li_bulan), string(ldec_ptu, '#,##0.00') + ' / ' + string(ldec_hasil_ptu[k], '#,##0.00') + ' / ' + string(ldec_cost, '#,##0.00') + ' / ' + string(ldec_ff, '#,##0.00'))
                    if(ldec_temp < 0){
                        ldec_hasil_ppokok[k] += ldec_temp;
                        ldec_hasil_ptu[k] = 0;
                    }else{
                        ldec_hasil_ptu[k] = ldec_temp;
                    }
                    //if k = 2 and i < 3 then messagebox(string(i) + '/' + string(li_bulan), string(ldec_ptu, '#,##0.00') + ' / ' + string(ldec_hasil_ptu[k], '#,##0.00') + ' / ' + string(ldec_cost, '#,##0.00') + ' / ' + string(ldec_ff, '#,##0.00'))

                    /* Adrian-Withdraw
                    if(ldec_wdraw > 0 && li_bulan == 12 ){
                        //messagebox(string(i) + '/' + string(li_bulan), string(ldec_ptu, '#,##0.00') + ' / ' + string(ldec_hasil_ptu[k], '#,##0.00') + ' / ' + string(ldec_cost, '#,##0.00') + ' / ' + string(ldec_ff, '#,##0.00'))
                        if(i <= 2){
                            ldec_wdraw = 0;
                        }else{
                            if(i <= 5) ldec_wdraw = Math.min(ldec_wdraw, ldec_hasil_ptu[k]);
                                ldec_temp = ldec_hasil_ptu[k] - ldec_wdraw;
                            if(ldec_temp < 0){
                                ldec_hasil_ppokok[k] += ldec_temp;
                                ldec_hasil_ptu[k] = 0;
                            }else{
                                ldec_hasil_ptu[k] = ldec_temp;
                            }
                        }
                    }*/
                }
                ldec_hasil_invest[1][k] = ldec_hasil_ppokok[k] + ldec_hasil_ptu[k];
                ldec_hasil_invest[1][k] += -1;
                if( ldec_hasil_invest[ 1 ][ k ] <= 0 &&  ( i <= 10 ) ){
                    lb_minus[k] = true;
                }
                //ldec_hasil_invest[1][k] -= (ldec_wdraw * (1 + ldec_sc));
            }

            j = umur_tt + i;

            HashMap<String, Object> param = new HashMap<String, Object>();
            param.put( "jenis", 0 );
            param.put("pset_id", pset_id);

            ArrayList<HashMap<String, Object>> resultList;
            resultList = getSelect().selectIllustrationShow( param );
            int year_i =     Integer.valueOf(resultList.get(0).get("YEAR").toString());
            param.put( "jenis", 1 );
            resultList = getSelect().selectIllustrationShow( param );

            int[] resultArr = new int[resultList.size()];

            for(int l = 0; l < resultList.size(); l++) {
                resultArr[l] = Integer.valueOf(resultList.get(l).get("YEAR").toString());
            }

            boolean year_j = false;
            for(int l = 0; l < resultArr.length; l++) {
                if(resultArr[l] == j){
                    year_j = true;
                    break;
                }
            }

//            if( i <= 23 || j == 55 || j == 65 || j == 75 || j == 80 || j == 100 )
            if(i <= year_i || year_j)
            {
                for( int k = 1; k <= 3; k++ )
                {
                    np[ k ] = FormatNumber.round( ldec_hasil_invest[ 1 ][ k ] / li_bagi, 0 );
                    celaka[ k ] = FormatNumber.round( ( ldec_hasil_invest[ 1 ][ k ] + ldec_manfaat ) / li_bagi, 0 );
                }

                if( i <= thn_cuti_premi )
                {
                    premiumTotal = ProposalStringFormatter.convertToStringWithoutCent( ldec_premi_setahun / li_bagi );
                }
                else
                {
                    premiumTotal = "";
                }

                if( i < defaultTopupDrawListSize )
                {
                    topup = "0.00";
                    draw = "0.00";
                    // why ( i - 1 )? becoz index in Java start from 0, not like PB programming language
//                    topupDrawVO = topupDrawVOList.get( i - 1 );
//                    topup = editorUtil.convertToString( topupDrawVO.getTopupAmount().divide( new BigDecimal( "1000" ) ) );
//                    draw = editorUtil.convertToString( topupDrawVO.getDrawAmount().divide( new BigDecimal( "1000" ) ) );
                    for(HashMap<String, Object> topupDraw : topupDrawList) {
                        Integer thn_ke = ((BigDecimal) topupDraw.get("THN_KE")).intValue();
                        if(i == thn_ke) {
                            BigDecimal topupAmount = (BigDecimal) topupDraw.get("TOPUP");
                            BigDecimal drawAmount = (BigDecimal) topupDraw.get("TARIK");

                            topup = ProposalStringFormatter.convertToString(topupAmount.divide(new BigDecimal("1000")));
                            draw = ProposalStringFormatter.convertToString(drawAmount.divide(new BigDecimal("1000")));
                        }
                    }

                    if( "0".equals( topup ) ) topup = "0.00";
                    if( "0".equals( draw ) ) draw = "0.00";
                }
                else
                {
                    topup = "0.00";
                    draw = "0.00";
                }


                if(!cekAdaTarik){
                    double bonus2 = flag_syarat_bonus_premi2( i, dataProposal , ldec_premi_ppokok_setahun)/1000;
                    if( bonus2 == 0 ) {
                        //bonus = null;
                        //bonus = " - ";
                        bonus = "0.00";
                    }else{
                        bonus = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative(bonus2);
                    }
                }

                String valueLow = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 1 ] ) );
                String valueMid = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 2 ] ) );
                String valueHi = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 3 ] ) );

                String benefitLow = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 1 ], np[ 1 ] );
                String benefitMid = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 2 ], np[ 2 ] );
                String benefitHi = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 3 ], np[ 3 ] );

                LinkedHashMap<String, String> map = new LinkedHashMap<>();
                map.put( "yearNo", ProposalStringFormatter.convertToString( i ) );
                map.put( "insuredAge", ProposalStringFormatter.convertToString( umur_tt + i ) );
                map.put( "premiumTotal", premiumTotal );
                map.put( "topupAssumption", topup );
                map.put( "drawAssumption", draw );
                map.put( "bonus",  bonus );
                map.put( "valueLow", valueLow );
                map.put( "valueMid", valueMid );
                map.put( "valueHi", valueHi );
                map.put( "benefitLow", benefitLow );
                map.put( "benefitMid", benefitMid );
                map.put( "benefitHi", benefitHi );
                mapList.add( map );

            }
        }

        LinkedHashMap<String, String> map1 = new LinkedHashMap<>();
        map1.put( "yearNo", getContext().getString(R.string.Thn_Polis_Ke));
        map1.put( "insuredAge", getContext().getString(R.string.Usia_Ttg) );
        map1.put( "premiumTotal", getContext().getString(R.string.Total_Premi));
        map1.put( "topupAssumption", getContext().getString(R.string.Asumsi_Top_up) );
        map1.put( "drawAssumption",  getContext().getString(R.string.Asumsi_Penarikan) );
        map1.put( "bonus",  getContext().getString(R.string.Bonus) );
        map1.put( "valueLow",getContext().getString(R.string.ENP) + " " + getContext().getString(R.string.Rendah) );
        map1.put( "valueMid",getContext().getString(R.string.ENP) + " " + getContext().getString(R.string.Sedang) );
        map1.put( "valueHi", getContext().getString(R.string.ENP) + " " +getContext().getString(R.string.Tinggi) );
        map1.put( "benefitLow", getContext().getString(R.string.EMM) + " " +getContext().getString(R.string.Rendah) );
        map1.put( "benefitMid",getContext().getString(R.string.EMM) + " " + getContext().getString(R.string.Sedang) );
        map1.put( "benefitHi",getContext().getString(R.string.EMM) + " " + getContext().getString(R.string.Tinggi) );
        mapList.add(0, map1 );

        result.setValidityMsg( lb_minus[ 1 ] ? "Maaf, Proposal yang Anda buat Tidak Layak Jual." : "" );
        result.setIllustrationList( mapList );
        mapResult.put("Illustration1", result);

        return mapResult;
    }

    public double flag_syarat_bonus_premi(int i, HashMap<String, Object> dataProposal , int k , double ldec_premi_ppokok_setahun){
        double result = 0;
        int thnCutiPremi = Integer.valueOf(dataProposal.get("THN_CUTI_PREMI").toString());

        if( thnCutiPremi >= 7 && thnCutiPremi <= 9 ){
            if( i == 7 && k == 1){
                result =  FormatNumber.round((0.5 * ldec_premi_ppokok_setahun) * FormatNumber.round(Math.pow((1 + 0.05),( ( double ) 1/12)), 15),2) ;
                //           ldec_hasil_ppokok[k] = FormatNumber.round(( ldec_ppokok + ldec_hasil_ppokok[k]) * FormatNumber.round(Math.pow( ( 1 + ldec_bunga_avg[ k ] ), ( ( double ) 1 / 12 )), 15),2) + flag_syarat_bonus_premi[k];

            }else if( i == 7 && k == 2){
                result = (0.5 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.08),( ( double ) 1/12)));
            }else if( i == 7 && k == 3){
                result = (0.5 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.10),( ( double ) 1/12)));
            }
        } else if( thnCutiPremi >= 10 && thnCutiPremi <= 14 ){
            if( i == 7 && k == 1){
                result =  ((0.5 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.05),( ( double ) 1/12))));
            }else if( i == 7 && k == 2){
                result = (0.5 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.08),( ( double ) 1/12)));
            }else if( i == 7 && k == 3){
                result = (0.5 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.10),( ( double ) 1/12)));
            }
            else if( i == 10 && k == 1){
                result = (0.1 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.05),( ( double ) 1/12)));
            }else if( i == 10 && k == 2){
                result = (0.1 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.08),( ( double ) 1/12)));
            }if( i == 10 && k == 3){
                result = (0.1 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.10),( ( double ) 1/12)));
            }
        } else if( thnCutiPremi >= 15 ){
            if( i == 7 && k == 1){
                result =  ((0.5 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.05),( ( double ) 1/12))));
            }else if( i == 7 && k == 2){
                result = (0.5 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.08),( ( double ) 1/12)));
            }else if( i == 7 && k == 3){
                result = (0.5 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.10),( ( double ) 1/12)));
            }
            else if( i == 10 && k == 1){
                result = (0.1 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.05),( ( double ) 1/12)));
            }else if( i == 10 && k == 2){
                result = (0.1 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.08),( ( double ) 1/12)));
            }else if( i == 10 && k == 3){
                result = (0.1 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.10),( ( double ) 1/12)));
            }else if( i == 15 && k == 1){
                result = (0.25 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.05),( ( double ) 1/12)));
            }else if( i == 15 && k == 2){
                result = (0.25 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.08),( ( double ) 1/12)));
            }else if( i == 15 && k == 3){
                result = (0.25 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.10),( ( double ) 1/12)));
            }
        }
//        else if( thnCutiPremi >= 20){
//            if( i == 7 && k == 1){
//                result =  ((0.5 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.05),( ( double ) 1/12))));
//            }else if( i == 7 && k == 2){
//                result = (0.5 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.08),( ( double ) 1/12)));
//            }else if( i == 7 && k == 3){
//                result = (0.5 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.10),( ( double ) 1/12)));
//            }
//            else if( i == 15 && k == 1){
//                result = (0.25 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.05),( ( double ) 1/12)));
//            }else if( i == 15 && k == 2){
//                result = (0.25 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.08),( ( double ) 1/12)));
//            }else if( i == 15 && k == 3){
//                result = (0.25 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.10),( ( double ) 1/12)));
//            }else if( i == 20 && k == 1){
//                result = (0.25 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.05),( ( double ) 1/12)));
//            }else if( i == 20 && k == 2){
//                result = (0.25 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.08),( ( double ) 1/12)));
//            }else if( i == 20 && k == 3){
//                result = (0.25 * ldec_premi_ppokok_setahun) * (Math.pow((1 + 0.10),( ( double ) 1/12)));
//            }
//        }
        //  if ( result == null ) result = 1;
        return result;
    }

    public double flag_syarat_bonus_premi2(int i, HashMap<String, Object> dataProposal ,double ldec_premi_ppokok_setahun){
        double result = 0;
        int thnCutiPremi = Integer.valueOf(dataProposal.get("THN_CUTI_PREMI").toString());


        if( thnCutiPremi >= 7 && thnCutiPremi <= 9 ){
            if( i == 7 ){
                result = 0.5 * ldec_premi_ppokok_setahun;
            }
        } else if( thnCutiPremi >= 10 && thnCutiPremi <= 14 ){
            if( i == 7 ){
                result = 0.5 * ldec_premi_ppokok_setahun;
            } else if ( i == 10 ){
                result = 0.1 * ldec_premi_ppokok_setahun;
            }
        } else if( thnCutiPremi >= 15 && thnCutiPremi <= 19 ){
            if( i == 7 ){
                result = 0.5 * ldec_premi_ppokok_setahun;
            } else if( i == 10 ){
                result = 0.1 * ldec_premi_ppokok_setahun;
            } else if( i == 15 ){
                result = 0.25 * ldec_premi_ppokok_setahun;
            }
        }
        else if( thnCutiPremi >= 20){
            if( i == 7 ){
                result = 0.5 * ldec_premi_ppokok_setahun;
            }else if( i == 10 ){
                result = 0.1 * ldec_premi_ppokok_setahun;
            }else if( i == 15 ){
                result = 0.25 * ldec_premi_ppokok_setahun;
            }
//            else if( i == 20 ){
//                result = 0.25 * ldec_premi_ppokok_setahun;
//            }
        }
        //  if ( result == null ) result = 1;
        return result;
    }

}
