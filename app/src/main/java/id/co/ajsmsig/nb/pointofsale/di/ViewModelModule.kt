package id.co.ajsmsig.nb.pointofsale.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import id.co.ajsmsig.nb.pointofsale.ui.jproductrecomendation.ProductRecommendationVM
import id.co.ajsmsig.nb.pointofsale.ui.kproductdescriptionchoice.ProductDescriptionChoiceVM
import id.co.ajsmsig.nb.pointofsale.ui.lproductfabdescription.ProductFABDescriptionVM
import id.co.ajsmsig.nb.pointofsale.ui.lproductpresentation.ProductPresentationVideoVM
import id.co.ajsmsig.nb.pointofsale.ui.lproductrider.ProductRiderVM
import id.co.ajsmsig.nb.pointofsale.ui.mrecommendationsummary.RecommendationSummaryVM
import id.co.ajsmsig.nb.pointofsale.ui.ahome.HomeVM
import id.co.ajsmsig.nb.pointofsale.ui.blifestage.LifeStageVM
import id.co.ajsmsig.nb.pointofsale.ui.cpriorityneed.PriorityNeedVM
import id.co.ajsmsig.nb.pointofsale.ui.dneedanalysis.NeedAnalysisVM
import id.co.ajsmsig.nb.pointofsale.ui.eneedcalculator.NeedCalculatorVM
import id.co.ajsmsig.nb.pointofsale.ui.ffinancialcalculator.FinancialCalculatorVM
import id.co.ajsmsig.nb.pointofsale.ui.griskprofiletools.RiskProfileToolsVM
import id.co.ajsmsig.nb.pointofsale.ui.hriskprofiling.RiskProfilingVM
import id.co.ajsmsig.nb.pointofsale.ui.iriskprofileresult.RiskProfileResultVM
import id.co.ajsmsig.nb.pointofsale.ui.handlingobjection.HandlingObjectionDetailVM
import id.co.ajsmsig.nb.pointofsale.ui.handlingobjection.HandlingObjectionListVM
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.HeaderVM
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.MainVM
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.PageViewModelFactory
import id.co.ajsmsig.nb.pointofsale.ui.viewmodel.SharedViewModel

@Module
internal abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HeaderVM::class)
    internal abstract fun bindHeaderVM(homeVM: HeaderVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainVM::class)
    internal abstract fun bindMainVM(homeVM: MainVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SharedViewModel::class)
    internal abstract fun bindSharedVM(homeVM: SharedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeVM::class)
    internal abstract fun bindHomeVM(homeVM: HomeVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LifeStageVM::class)
    internal abstract fun bindLifeStageVM(homeVM: LifeStageVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PriorityNeedVM::class)
    internal abstract fun bindPriorityNeedVM(homeVM: PriorityNeedVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NeedAnalysisVM::class)
    internal abstract fun bindNeedAnalysisVM(homeVM: NeedAnalysisVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NeedCalculatorVM::class)
    internal abstract fun bindNeedCalculatorVM(homeVM: NeedCalculatorVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FinancialCalculatorVM::class)
    internal abstract fun bindFinancialCalculatorVM(homeVM: FinancialCalculatorVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RiskProfileToolsVM::class)
    internal abstract fun bindRiskProfilToolsVM(homeVM: RiskProfileToolsVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RiskProfilingVM::class)
    internal abstract fun bindRiskProfilingVM(homeVM: RiskProfilingVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RiskProfileResultVM::class)
    internal abstract fun bindRiskProfileResultVM(homeVM: RiskProfileResultVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HandlingObjectionListVM::class)
    internal abstract fun bindHandlingObjectionListVM(homeVM: HandlingObjectionListVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HandlingObjectionDetailVM::class)
    internal abstract fun bindHandlingObjectionDetailVM(homeVM: HandlingObjectionDetailVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductRecommendationVM::class)
    internal abstract fun bindProductRecommendationVM(homeVM: ProductRecommendationVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductDescriptionChoiceVM::class)
    internal abstract fun bindProductDescriptionChoiceVM(homeVM: ProductDescriptionChoiceVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductFABDescriptionVM::class)
    internal abstract fun bindProductFABDescriptionVM(homeVM: ProductFABDescriptionVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductPresentationVideoVM::class)
    internal abstract fun bindProductPresentationVideoVM(homeVM: ProductPresentationVideoVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductRiderVM::class)
    internal abstract fun bindProductRiderVM(homeVM: ProductRiderVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RecommendationSummaryVM::class)
    internal abstract fun bindRecommendationSummaryVM(homeVM: RecommendationSummaryVM): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: PageViewModelFactory): ViewModelProvider.Factory

}
