package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by andreyyoshuamanik on 02/03/18.
 */
@Entity
class PageToPageOption(
    @PrimaryKey
    val id: Int?,
    var imageName: String?,
    var pageOptionId: Int?,
    var pageId: Int?,
    var order: Int?,
    var showOnlyIfSelectPageOptionIds: String?
)