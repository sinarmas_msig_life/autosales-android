package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface PageOptionDao {

    @Query("select a.id, a.title, b.imageName from PageOption as a inner join PageToPageOption as b on b.pageOptionId = a.id\n" +
            "where b.pageId = :pageId  order by `order`")
    fun loadAllPageOptions(pageId: Int?): LiveData<List<PageOption>>


    @Query("select a.id, a.title, b.imageName from PageOption as a inner join PageToPageOption as b on b.pageOptionId = a.id\n" +
            "where b.pageId = :pageId and showOnlyIfSelectPageOptionIds LIKE :query order by `order`")
    fun loadAllPageOptionsWithPreviouslySelectedIds(pageId: Int?, query: String): LiveData<List<PageOption>>


    @Query("select a.id, a.title, b.imageName from PageOption as a inner join PageToPageOption as b on b.pageOptionId = a.id\n" +
            "where b.pageOptionId = :optionId limit 1")
    fun loadPageOption(optionId: Int?): LiveData<PageOption>
}