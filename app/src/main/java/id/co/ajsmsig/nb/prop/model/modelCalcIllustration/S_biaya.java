package id.co.ajsmsig.nb.prop.model.modelCalcIllustration;


import java.io.Serializable;

public class S_biaya implements Serializable
{
	private static final long serialVersionUID = -988089222980820984L;
	public double[] tarik = new double[50+1];
    public double[] bak = new double[6+1];
    public double[] topup = new double[50+1];
    public double[][] bunga = new double[5+1][3+1];
}
