package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.Ignore
import android.databinding.ObservableBoolean
import java.util.*

/**
 * Created by andreyyoshuamanik on 21/02/18.
 */
open class Option(
        @Ignore
        val selected: ObservableBoolean = ObservableBoolean(false),

        @Ignore
        val disabled: ObservableBoolean = ObservableBoolean(false)): Observable() {

    override fun hasChanged(): Boolean {
        return true
    }
}