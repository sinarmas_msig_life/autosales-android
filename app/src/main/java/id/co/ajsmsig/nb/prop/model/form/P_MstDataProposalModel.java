package id.co.ajsmsig.nb.prop.model.form;


import android.os.Parcel;
import android.os.Parcelable;

public class P_MstDataProposalModel implements Parcelable {
    private Long id;
    private String no_proposal_tab;
    private String no_proposal;
    private String nama_pp;
    private String tgl_lahir_pp;
    private Integer umur_pp;
    private Integer sex_pp;
    private String nama_tt;
    private String tgl_lahir_tt;
    private Integer umur_tt;
    private Integer sex_tt;
    private String nama_agen;
    private String msag_id;
    private String tgl_input;
    private Integer flag_aktif;
    private Integer flag_test;
    private String nama_user;
    private String kode_agen;
    private Long lead_id;
    private Long lead_tab_id;
    private Long prospect_id;
    private Integer lstb_id;
    private Integer id_dist;
    private Integer jenis_id;
    private Integer flag_tt_calon_bayi;
    private Integer flag_finish;
    private Integer flag_star;
    private Integer last_page_position;
    private Integer flag_proper;
    private Integer flag_packet;
    private Integer flag_pos;
    private String virtual_acc = "";
    private String noid = "";

    public P_MstDataProposalModel() {
        umur_pp = 0;
        umur_tt = 0;
    }

    protected P_MstDataProposalModel(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        no_proposal_tab = in.readString();
        no_proposal = in.readString();
        nama_pp = in.readString();
        tgl_lahir_pp = in.readString();
        if (in.readByte() == 0) {
            umur_pp = null;
        } else {
            umur_pp = in.readInt();
        }
        if (in.readByte() == 0) {
            sex_pp = null;
        } else {
            sex_pp = in.readInt();
        }
        nama_tt = in.readString();
        tgl_lahir_tt = in.readString();
        if (in.readByte() == 0) {
            umur_tt = null;
        } else {
            umur_tt = in.readInt();
        }
        if (in.readByte() == 0) {
            sex_tt = null;
        } else {
            sex_tt = in.readInt();
        }
        nama_agen = in.readString();
        msag_id = in.readString();
        tgl_input = in.readString();
        if (in.readByte() == 0) {
            flag_aktif = null;
        } else {
            flag_aktif = in.readInt();
        }
        if (in.readByte() == 0) {
            flag_test = null;
        } else {
            flag_test = in.readInt();
        }
        nama_user = in.readString();
        kode_agen = in.readString();
        if (in.readByte() == 0) {
            lead_id = null;
        } else {
            lead_id = in.readLong();
        }
        if (in.readByte() == 0) {
            lead_tab_id = null;
        } else {
            lead_tab_id = in.readLong();
        }
        if (in.readByte() == 0) {
            prospect_id = null;
        } else {
            prospect_id = in.readLong();
        }
        if (in.readByte() == 0) {
            lstb_id = null;
        } else {
            lstb_id = in.readInt();
        }
        if (in.readByte() == 0) {
            id_dist = null;
        } else {
            id_dist = in.readInt();
        }
        if (in.readByte() == 0) {
            jenis_id = null;
        } else {
            jenis_id = in.readInt();
        }
        if (in.readByte() == 0) {
            flag_tt_calon_bayi = null;
        } else {
            flag_tt_calon_bayi = in.readInt();
        }
        if (in.readByte() == 0) {
            flag_finish = null;
        } else {
            flag_finish = in.readInt();
        }
        if (in.readByte() == 0) {
            flag_star = null;
        } else {
            flag_star = in.readInt();
        }
        if (in.readByte() == 0) {
            last_page_position = null;
        } else {
            last_page_position = in.readInt();
        }
        if (in.readByte() == 0) {
            flag_proper = null;
        } else {
            flag_proper = in.readInt();
        }
        if (in.readByte() == 0) {
            flag_packet = null;
        } else {
            flag_packet = in.readInt();
        }
        if (in.readByte() == 0) {
            flag_pos = null;
        } else {
            flag_pos = in.readInt();
        }
        virtual_acc = in.readString();
        noid = in.readString();
    }

    public static final Creator<P_MstDataProposalModel> CREATOR = new Creator<P_MstDataProposalModel>() {
        @Override
        public P_MstDataProposalModel createFromParcel(Parcel in) {
            return new P_MstDataProposalModel(in);
        }

        @Override
        public P_MstDataProposalModel[] newArray(int size) {
            return new P_MstDataProposalModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(id);
        }
        parcel.writeString(no_proposal_tab);
        parcel.writeString(no_proposal);
        parcel.writeString(nama_pp);
        parcel.writeString(tgl_lahir_pp);
        if (umur_pp == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(umur_pp);
        }
        if (sex_pp == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(sex_pp);
        }
        parcel.writeString(nama_tt);
        parcel.writeString(tgl_lahir_tt);
        if (umur_tt == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(umur_tt);
        }
        if (sex_tt == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(sex_tt);
        }
        parcel.writeString(nama_agen);
        parcel.writeString(msag_id);
        parcel.writeString(tgl_input);
        if (flag_aktif == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(flag_aktif);
        }
        if (flag_test == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(flag_test);
        }
        parcel.writeString(nama_user);
        parcel.writeString(kode_agen);
        if (lead_id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(lead_id);
        }
        if (lead_tab_id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(lead_tab_id);
        }
        if (prospect_id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(prospect_id);
        }
        if (lstb_id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(lstb_id);
        }
        if (id_dist == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id_dist);
        }
        if (jenis_id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(jenis_id);
        }
        if (flag_tt_calon_bayi == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(flag_tt_calon_bayi);
        }
        if (flag_finish == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(flag_finish);
        }
        if (flag_star == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(flag_star);
        }
        if (last_page_position == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(last_page_position);
        }
        if (flag_proper == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(flag_proper);
        }
        if (flag_packet == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(flag_packet);
        }
        if (flag_pos == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(flag_pos);
        }
        parcel.writeString(virtual_acc);
        parcel.writeString(noid);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNo_proposal_tab() {
        return no_proposal_tab;
    }

    public void setNo_proposal_tab(String no_proposal_tab) {
        this.no_proposal_tab = no_proposal_tab;
    }

    public String getNo_proposal() {
        return no_proposal;
    }

    public void setNo_proposal(String no_proposal) {
        this.no_proposal = no_proposal;
    }

    public String getNama_pp() {
        return nama_pp;
    }

    public void setNama_pp(String nama_pp) {
        this.nama_pp = nama_pp;
    }

    public String getTgl_lahir_pp() {
        return tgl_lahir_pp;
    }

    public void setTgl_lahir_pp(String tgl_lahir_pp) {
        this.tgl_lahir_pp = tgl_lahir_pp;
    }

    public Integer getUmur_pp() {
        return umur_pp;
    }

    public void setUmur_pp(Integer umur_pp) {
        this.umur_pp = umur_pp;
    }

    public Integer getSex_pp() {
        return sex_pp;
    }

    public void setSex_pp(Integer sex_pp) {
        this.sex_pp = sex_pp;
    }

    public String getNama_tt() {
        return nama_tt;
    }

    public void setNama_tt(String nama_tt) {
        this.nama_tt = nama_tt;
    }

    public String getTgl_lahir_tt() {
        return tgl_lahir_tt;
    }

    public void setTgl_lahir_tt(String tgl_lahir_tt) {
        this.tgl_lahir_tt = tgl_lahir_tt;
    }

    public Integer getUmur_tt() {
        return umur_tt;
    }

    public void setUmur_tt(Integer umur_tt) {
        this.umur_tt = umur_tt;
    }

    public Integer getSex_tt() {
        return sex_tt;
    }

    public void setSex_tt(Integer sex_tt) {
        this.sex_tt = sex_tt;
    }

    public String getNama_agen() {
        return nama_agen;
    }

    public void setNama_agen(String nama_agen) {
        this.nama_agen = nama_agen;
    }

    public String getMsag_id() {
        return msag_id;
    }

    public void setMsag_id(String msag_id) {
        this.msag_id = msag_id;
    }

    public String getTgl_input() {
        return tgl_input;
    }

    public void setTgl_input(String tgl_input) {
        this.tgl_input = tgl_input;
    }

    public Integer getFlag_aktif() {
        return flag_aktif;
    }

    public void setFlag_aktif(Integer flag_aktif) {
        this.flag_aktif = flag_aktif;
    }

    public Integer getFlag_test() {
        return flag_test;
    }

    public void setFlag_test(Integer flag_test) {
        this.flag_test = flag_test;
    }

    public String getNama_user() {
        return nama_user;
    }

    public void setNama_user(String nama_user) {
        this.nama_user = nama_user;
    }

    public String getKode_agen() {
        return kode_agen;
    }

    public void setKode_agen(String kode_agen) {
        this.kode_agen = kode_agen;
    }

    public Long getLead_id() {
        return lead_id;
    }

    public void setLead_id(Long lead_id) {
        this.lead_id = lead_id;
    }

    public Long getLead_tab_id() {
        return lead_tab_id;
    }

    public void setLead_tab_id(Long lead_tab_id) {
        this.lead_tab_id = lead_tab_id;
    }

    public Long getProspect_id() {
        return prospect_id;
    }

    public void setProspect_id(Long prospect_id) {
        this.prospect_id = prospect_id;
    }

    public Integer getLstb_id() {
        return lstb_id;
    }

    public void setLstb_id(Integer lstb_id) {
        this.lstb_id = lstb_id;
    }

    public Integer getId_dist() {
        return id_dist;
    }

    public void setId_dist(Integer id_dist) {
        this.id_dist = id_dist;
    }

    public Integer getJenis_id() {
        return jenis_id;
    }

    public void setJenis_id(Integer jenis_id) {
        this.jenis_id = jenis_id;
    }

    public Integer getFlag_tt_calon_bayi() {
        return flag_tt_calon_bayi;
    }

    public void setFlag_tt_calon_bayi(Integer flag_tt_calon_bayi) {
        this.flag_tt_calon_bayi = flag_tt_calon_bayi;
    }

    public Integer getFlag_finish() {
        return flag_finish;
    }

    public void setFlag_finish(Integer flag_finish) {
        this.flag_finish = flag_finish;
    }

    public Integer getFlag_star() {
        return flag_star;
    }

    public void setFlag_star(Integer flag_star) {
        this.flag_star = flag_star;
    }

    public Integer getLast_page_position() {
        return last_page_position;
    }

    public void setLast_page_position(Integer last_page_position) {
        this.last_page_position = last_page_position;
    }

    public Integer getFlag_proper() {
        return flag_proper;
    }

    public void setFlag_proper(Integer flag_proper) {
        this.flag_proper = flag_proper;
    }

    public Integer getFlag_packet() {
        return flag_packet;
    }

    public void setFlag_packet(Integer flag_packet) {
        this.flag_packet = flag_packet;
    }

    public Integer getFlag_pos() {
        return flag_pos;
    }

    public void setFlag_pos(Integer flag_pos) {
        this.flag_pos = flag_pos;
    }

    public String getVirtual_acc() {
        return virtual_acc;
    }

    public void setVirtual_acc(String virtual_acc) {
        this.virtual_acc = virtual_acc;
    }

    public String getNoid() {
        return noid;
    }

    public void setNoid(String noid) {
        this.noid = noid;
    }

    public static Creator<P_MstDataProposalModel> getCREATOR() {
        return CREATOR;
    }
}
