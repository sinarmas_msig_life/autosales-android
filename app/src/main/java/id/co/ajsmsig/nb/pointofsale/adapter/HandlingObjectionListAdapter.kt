package id.co.ajsmsig.nb.pointofsale.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.databinding.PosRowHandlingObjectionListBinding
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.HandlingObjection

/**
 * Created by andreyyoshuamanik on 21/02/18.
 */
class HandlingObjectionListAdapter(val clickListener: (HandlingObjection) -> Unit): RecyclerView.Adapter<HandlingObjectionListAdapter.HandlingObjectionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HandlingObjectionViewHolder {

        val dataBinding = DataBindingUtil.inflate<PosRowHandlingObjectionListBinding>(LayoutInflater.from(parent.context), R.layout.pos_row_handling_objection_list, parent, false)
        return HandlingObjectionViewHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: HandlingObjectionViewHolder, position: Int) {
        options?.let {
            holder.bind(it[position])
        }
    }

    var options: Array<HandlingObjection>? = null
    set(value) {
        field = value

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return options?.size ?: 0
    }

    inner class HandlingObjectionViewHolder(private val dataBinding: PosRowHandlingObjectionListBinding): RecyclerView.ViewHolder(dataBinding.root) {

        fun bind(option: HandlingObjection) {
            dataBinding.vm = option
            dataBinding.viewHolder = this
        }

        fun onClickOption(option: HandlingObjection) {

            clickListener(option)
        }
    }
}