
package id.co.ajsmsig.nb.crm.model.apiresponse.userlogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLoginResponse {

    @SerializedName("MESSAGE")
    @Expose
    private String mESSAGE;
    @SerializedName("ERROR")
    @Expose
    private Boolean eRROR;
    @SerializedName("DATA")
    @Expose
    private DATA dATA;

    public String getMESSAGE() {
        return mESSAGE;
    }

    public void setMESSAGE(String mESSAGE) {
        this.mESSAGE = mESSAGE;
    }

    public Boolean getERROR() {
        return eRROR;
    }

    public void setERROR(Boolean eRROR) {
        this.eRROR = eRROR;
    }

    public DATA getDATA() {
        return dATA;
    }

    public void setDATA(DATA dATA) {
        this.dATA = dATA;
    }

}
