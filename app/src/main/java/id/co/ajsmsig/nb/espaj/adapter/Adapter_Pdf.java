package id.co.ajsmsig.nb.espaj.adapter;

import java.io.IOException;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.TouchImageView;
import id.co.ajsmsig.nb.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.pdf.PdfRenderer;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

@SuppressLint("NewApi")
public class Adapter_Pdf extends PagerAdapter{
	Context mContext;
    LayoutInflater mLayoutInflater;
    PdfRenderer renderer;
    
    
    public Adapter_Pdf(Context context, PdfRenderer renderer) {
        this.mContext = context;
        this.renderer = renderer;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

	@Override
	public int getCount() {
		return renderer.getPageCount();
	}
	@Override
	public boolean isViewFromObject(View view, Object object) {
		   return view == object;
	}
	
	 @Override
	    public Object instantiateItem(ViewGroup container, int position) {
	        View itemView = mLayoutInflater.inflate(R.layout.e_adapter_pdfproposal, container, false);
	 
	        TouchImageView imageView = itemView.findViewById(R.id.pdfproposal);
	        try {
	            MethodSupport.openPDF(imageView, renderer,position);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        container.addView(itemView);
	 
	        return itemView;
	    }
	 
	    @Override
	    public void destroyItem(ViewGroup container, int position, Object object) {
	        container.removeView((LinearLayout) object);
	    }

}
