package id.co.ajsmsig.nb.crm.activity;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import id.co.ajsmsig.nb.BuildConfig;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.database.DBHelper;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.crm.method.async.ProposalApiRestClientUsage;
import id.co.ajsmsig.nb.crm.method.async.WsApiRestClientUsage;
import id.co.ajsmsig.nb.crm.model.DataUpdateModel;
import id.co.ajsmsig.nb.util.AppConfig;

public class C_SplashActivity extends AppCompatActivity implements ProposalApiRestClientUsage.CheckLastDataVersionListener, ProposalApiRestClientUsage.DownloadNewDataVersionListener, WsApiRestClientUsage.CheckAppListener {
    private static final String TAG = "SPLASH_ACTIVITY";

    @BindView(R.id.pb_download)
    ProgressBar pb_download;
    @BindView(R.id.pb_install)
    ContentLoadingProgressBar pb_install;
    @BindView(R.id.tv_download)
    TextView tv_download;
    String[] permissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE};

    private ProposalApiRestClientUsage eproposalRestClientUsage;
    private WsApiRestClientUsage wsApiRestClientUsage;
    private String dataVersion = "";
    private int flag_app = 0;

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        eproposalRestClientUsage = new ProposalApiRestClientUsage(this);
        wsApiRestClientUsage = new WsApiRestClientUsage(this);
        eproposalRestClientUsage.setOnCheckLastDataVersionListener(this);
        eproposalRestClientUsage.setOnDownloadNewDataVersionListener(this);
        wsApiRestClientUsage.setonCheckAppListener(this);

//        pb_download.getProgressDrawable().setColorFi[plter(ContextCompat.getColor(this, R.color.colorPrimaryLight), PorterDuff.Mode.SRC_IN);

        dataVersion = new C_Select(this).getCurrentDatabaseSchemaVersion();
        //Check if the version is null or empty string
        if (TextUtils.isEmpty(dataVersion)) {
            dataVersion = "1.0.0";
        }


        try {

            if (BuildConfig.FLAVOR.equals("development"))
            {
                AlertDialog.Builder alertDialogDv;
                alertDialogDv = new AlertDialog.Builder(C_SplashActivity.this);
                alertDialogDv.setTitle("UAT AutoSales");
                alertDialogDv.setMessage("Aplikasi ini merupakan aplikasi test yang digunakan untuk pengembangan fitur didalam autosales");
                alertDialogDv.setCancelable(false);
                alertDialogDv.setPositiveButton(getString(R.string.mengerti), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if (StaticMethods.isNetworkAvailable(C_SplashActivity.this)) {
                            wsApiRestClientUsage.onCheckAppUpdate();
                        } else {
                            showDialogNoInternetConnection();
                        }
                    }
                });
                alertDialogDv.show();
                alertDialogDv.wait();
            } else {
                if (StaticMethods.isNetworkAvailable(C_SplashActivity.this)) {
                wsApiRestClientUsage.onCheckAppUpdate();
            } else {
                showDialogNoInternetConnection();
            }}
        } catch (Exception e) {
            return;
        }
    }





    private void intentLogin() {
        if (permissionCheck()) {
            Intent intent = new Intent(C_SplashActivity.this, C_LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private boolean permissionCheck() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String str_permission : permissions) {
            result = ContextCompat.checkSelfPermission(this, str_permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(str_permission);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 0);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 0: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    intentLogin();

                } else {
                    intentLogin();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onCheckAppSuccess(int statusCode, Header[] headers, JSONObject response) {
        try {
            String APP_NAME = response.getString("APP_NAME");
            String VERSION_NAME = response.getString("VERSION_NAME");
            int VERSION_CODE = response.getInt("VERSION_CODE");
            String DESC_APP = response.getString("DESC_APP");
            flag_app = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            if (VERSION_CODE > flag_app) {
                android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(C_SplashActivity.this);
                dialog.setTitle(getString(R.string.update_alert));
                dialog.setMessage(DESC_APP);
                dialog.setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=id.co.ajsmsig.nb"));
                        startActivity(intent);
                    }
                });
//                dialog.setNegativeButton("LANJUT", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        intentLogin();
//                    }
//                });
                dialog.show();
            } else {
                    if (StaticMethods.isNetworkAvailable(this)) {
                        eproposalRestClientUsage.performCheckLastDataVersionListener();
//                        eproposalRestClientUsage.getLastDataVersion();
                    } else {
                        showDialogNoInternetConnection();
                    }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCheckAppFailure(String title, String message) {
        showDialogTryAgain(getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi));
    }

    @Override
    public void onCheckLastDataVersionSuccess(String updateVersion, ContentValues cvVersion) {
        if (StaticMethods.isNetworkAvailable(getBaseContext())) {
            eproposalRestClientUsage.performDownloadDataUpdate(updateVersion, cvVersion);
        } else {
            showDialogNoInternetConnection();
        }

    }

    @Override
    public void onCheckLastDataVersionUpToDate(String message) {
        intentLogin();
    }

    @Override
    public void onCheckLastDataVersionFailureConnection(String message) {
        showDialogTryAgain(getString(R.string.masalah_koneksi), getString(R.string.solusi_masalah_koneksi));
    }

    @Override
    public void onProgressDownloadNewDataVersion(final long bytesWritten, final long totalSize, final String downloadVersion) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int totalSizeMb = (int) (totalSize / 1000);
                int bytesWrittenMb = (int) (bytesWritten / 1000);
                String statusDownload = "Mengunduh " + bytesWrittenMb + " KB dari " + totalSizeMb + " KB V." + downloadVersion;
                tv_download.setText(statusDownload);
                pb_install.setVisibility(View.GONE);
                pb_download.setMax((totalSizeMb));
                pb_download.setProgress((bytesWrittenMb));

            }
        });
    }

    @Override
    public void onDownloadNewDataVersionSuccess(final File file, final ContentValues cvLatestVersion) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv_download.setText("Menginstall Update....");
                pb_install.setVisibility(View.VISIBLE);
            }
        });


        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    Log.e(TAG, "run: " + e.toString());
                    e.printStackTrace();
                } finally {
                    startInstall(cvLatestVersion, file);
                }
            }
        };
        timerThread.start();
    }

    @Override
    public void onDownloadNewDataVersionFailure(String title, String message) {
        showDialogTryAgain(title, message);
    }


    private void startInstall(ContentValues cvLatestVersion, File file) {
        Log.d(TAG, "startInstall: " + file.getPath());
        File fileDownload = getFileFromFileZip(file);
        Log.d(TAG, "startInstall: fileDownload" + fileDownload.getPath());
        try {
            String sQuery = FileUtils.readFileToString(fileDownload, "UTF-8");
            Log.d(TAG, "startInstall: txt " + sQuery);
            Gson gson = new Gson();
            Type dumListType = new TypeToken<ArrayList<DataUpdateModel>>() {
            }.getType();
            ArrayList<DataUpdateModel> dataUpdateModels = gson.fromJson(sQuery, dumListType);

            int lavDataId = cvLatestVersion.getAsInteger(getString(R.string.lav_data_id));
            Log.d(TAG, "startInstall: lavDataId " + lavDataId);
//                        int lavDataId = 1;
            DBHelper dbHelper = new DBHelper(C_SplashActivity.this, lavDataId, dataUpdateModels, cvLatestVersion);
            int latestVersion = dbHelper.selectLavDataIdAppVersion();
            Log.d(TAG, "startInstall: last version" + latestVersion);
            file.delete();
            fileDownload.delete();
            if (StaticMethods.isNetworkAvailable(getBaseContext())) {
                eproposalRestClientUsage.performCheckLastDataVersionListener();
//                eproposalRestClientUsage.getLastDataVersion();
            } else {
                showDialogNoInternetConnection();
            }

//                        intentLogin();

        } catch (IOException e) {
            Log.d(TAG, "startInstall: error" + e.getMessage());
            e.printStackTrace();
        }
    }

    private AlertDialog alertDialog, alertDialogNoInternet ;

    @Override
    protected void onPause() {
        super.onPause();
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        if (alertDialogNoInternet != null) {
            alertDialogNoInternet.dismiss();
        }
    }

    void showDialogTryAgain(final String title, final String message) {

        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(C_SplashActivity.this);

        alertDialogBuilder.setTitle(title);
//                    alertDialogBuilder.setMessage(getString(R.string.solusi_masalah_koneksi));
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(getString(R.string.coba_kembali), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
//                    sharedPreferences = getSharedPreferences(getString(R.string.update_data_preferences), MODE_PRIVATE);
//                    String dataVersion = sharedPreferences.getString(getString(R.string.data_version), "");
//                    if (dataVersion.trim().length() == 0) dataVersion = "1.0.0";
//                        if (StaticMethods.isNetworkAvailable(C_SplashActivity.this))
                if (StaticMethods.isNetworkAvailable(getBaseContext())) {
                    eproposalRestClientUsage.performCheckLastDataVersionListener();
//                    eproposalRestClientUsage.getLastDataVersion();
                } else {
                    showDialogNoInternetConnection();
                }
            }
        });
        alertDialogBuilder.setNegativeButton("Lanjut", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                intentLogin();
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    void showDialogNoInternetConnection() {

        AlertDialog.Builder alertDialogBuilder;
        alertDialogBuilder = new AlertDialog.Builder(C_SplashActivity.this);

        alertDialogBuilder.setTitle("Tidak Terhubung Ke Internet ");
//                    alertDialogBuilder.setMessage(getString(R.string.solusi_masalah_koneksi));
        alertDialogBuilder.setMessage(R.string.no_internet_body);
        //"Mohon nyalakan koneksi internet anda terlebih dahulu untuk update data"
        //Dibutuhkan sambungan internet untuk sinkronisasi data lead ke server. Tekan "Lanjut" untuk tetap menggunakan aplikasi secara offline
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(getString(R.string.keluar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
//                    sharedPreferences = getSharedPreferences(getString(R.string.update_data_preferences), MODE_PRIVATE);
//                    String dataVersion = sharedPreferences.getString(getString(R.string.data_version), "");
//                    if (dataVersion.trim().length() == 0) dataVersion = "1.0.0";
//                        if (StaticMethods.isNetworkAvailable(C_SplashActivity.this))
//                if (StaticMethods.isNetworkAvailable(getBaseContext())) {
//                    ewsRestClientUsageReal.performCheckLastDataVersionListener();
//                }else {
//                    showDialogNoInternetConnection();
//                }
                finish();
            }
        });
        alertDialogBuilder.setNegativeButton("Lanjut", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                intentLogin();
            }
        });
        alertDialogNoInternet = alertDialogBuilder.create();
        alertDialogNoInternet.show();

    }


    private File getFileFromFileZip(File fileZip) {
        Log.d(TAG, "getFileFromFileZip: file zip not corupt " + isValid(fileZip));
        try {
            Log.d(TAG, "getFileFromFileZip: start try");
            InputStream is = new FileInputStream(fileZip);
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;
            String filename = "";
            while ((ze = zis.getNextEntry()) != null) {
                filename = ze.getName();
                // Need to create directories if not exists, or
                // it will generate an Exception...
                if (ze.isDirectory()) {
                    File fmd = new File(fileZip.getPath() + filename);
                    fmd.mkdirs();
                }

                Log.d(TAG, "getFileFromFileZip: file name " + filename);
                FileOutputStream fout = new FileOutputStream(fileZip.getPath() + filename);

                Log.d(TAG, "getFileFromFileZip: after fout");
                // cteni zipu a zapis
                while ((count = zis.read(buffer)) != -1) {
                    fout.write(buffer, 0, count);
                }
                Log.d(TAG, "getFileFromFileZip: after while");

                fout.close();
                zis.closeEntry();
            }

            zis.close();
            Log.d(TAG, "getFileFromFileZip: close");

            return new File(fileZip.getPath() + filename);

        } catch (IOException e) {
            Log.d(TAG, "getFileFromFileZip: error " + e.getMessage());
        }
        throw new IllegalArgumentException("Filenya kok gak ada?");
    }

    private boolean isValid(final File file) {
        ZipFile zipfile = null;
        try {
            zipfile = new ZipFile(file);
            return true;
        } catch (IOException e) {
            return false;
        } finally {
            try {
                if (zipfile != null) {
                    zipfile.close();
                    zipfile = null;
                }
            } catch (IOException e) {
            }
        }
    }

}
