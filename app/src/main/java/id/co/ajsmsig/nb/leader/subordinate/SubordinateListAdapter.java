package id.co.ajsmsig.nb.leader.subordinate;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ajsmsig.nb.R;

public class SubordinateListAdapter extends RecyclerView.Adapter<SubordinateListAdapter.MyViewHolder> {

    private ArrayList<Subordinate> subordinateListData;
    private OnItemPressed listener;

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView subordinateName;
        private ConstraintLayout layoutSubordinate;
        private TextView tvAgentInitialName;

        public MyViewHolder(View view) {
            super(view);
            subordinateName = view.findViewById(R.id.tvAgentNameSubordinate);
            layoutSubordinate = view.findViewById(R.id.layoutSubordinate);
            tvAgentInitialName = view.findViewById(R.id.tvAgentInitialName);
        }

    }

    public SubordinateListAdapter(ArrayList<Subordinate> subordinateListData, OnItemPressed listener) {
        this.subordinateListData = subordinateListData;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_subordinate, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Subordinate model = subordinateListData.get(position);
        holder.subordinateName.setText(model.getNAMAREFF());
        holder.tvAgentInitialName.setText(model.getInitialName());
        holder.layoutSubordinate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSubordinatePressed(holder.getAdapterPosition(), model);
            }
        });
    }

    @Override
    public int getItemCount() {

        return subordinateListData.size();
    }

    void refreshSubordinate(ArrayList<Subordinate> newSubordinate) {
        //Hapus list yg lama
        subordinateListData.clear();
        //Update dengan data yg baru
        subordinateListData = newSubordinate;
        //Refresh recyclerview
        notifyDataSetChanged();
    }


    interface OnItemPressed {
        void onSubordinatePressed(int position, Subordinate subordinate);
    }
}
