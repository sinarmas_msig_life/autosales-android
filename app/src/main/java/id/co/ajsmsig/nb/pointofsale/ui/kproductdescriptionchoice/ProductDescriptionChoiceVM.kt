package id.co.ajsmsig.nb.pointofsale.ui.kproductdescriptionchoice

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.PageOption
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 08/03/18.
 */
class ProductDescriptionChoiceVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository): AndroidViewModel(application){

    var observablePageOptions = MediatorLiveData<List<PageOption>>()

    var page: Page? = null
    set(value) {
        field = value

        val pageOptions = repository.getPageOptionsFromPage(page)
        observablePageOptions.addSource(pageOptions, observablePageOptions::setValue)

    }
}