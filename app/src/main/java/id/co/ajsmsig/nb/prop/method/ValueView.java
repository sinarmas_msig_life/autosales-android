package id.co.ajsmsig.nb.prop.method;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.co.ajsmsig.nb.R;

/**
  Created by faiz_f on 04/04/2017.
 */

public class ValueView {
    private Context context;
    private Activity activity;

    public ValueView(Context context) {
        this.context = context;
        this.activity = (Activity) context;
    }

    public void addToLinearLayout(LinearLayout linearLayout, String[][] valuess) {
        linearLayout.removeAllViews();
        for (String[] values : valuess) {
            if(values!=null) {
                @SuppressLint("InflateParams") View child = activity.getLayoutInflater().inflate(R.layout.p_item_presentation_form, null);
                TextView tv_title = child.findViewById(R.id.tv_title);
                TextView tv_value = child.findViewById(R.id.tv_value);

                tv_title.setText(values[0]);
                tv_value.setText(values[1]);
                linearLayout.addView(child);
            }
        }
    }

}
