package id.co.ajsmsig.nb.crm.adapter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.ActivityListModel;
import id.co.ajsmsig.nb.crm.model.ProposalListModel;

public class ProposalListAdapter extends RecyclerView.Adapter<ProposalListAdapter.ProposalListVH> {
    private List<ProposalListModel> proposals;
    private ClickListener clickListener;

    public ProposalListAdapter(List<ProposalListModel> proposals) {
        this.proposals = proposals;
    }

    public class ProposalListVH extends RecyclerView.ViewHolder {
        private TextView tvProductName;
        private TextView tvUangPertanggungan;
        private TextView tvNoProposal;
        private TextView tvPemegangPolis;
        private TextView tvTertanggung;
        private TextView tvPremi;
        private ConstraintLayout layoutProposal;

        public ProposalListVH(View view) {
            super(view);
            tvProductName = view.findViewById(R.id.tvProductName);
            tvUangPertanggungan = view.findViewById(R.id.tvUangPertanggungan);
            tvNoProposal = view.findViewById(R.id.tvNoProposal);
            tvPemegangPolis = view.findViewById(R.id.tvPemegangPolis);
            tvTertanggung = view.findViewById(R.id.tvTertanggung);
            tvPremi = view.findViewById(R.id.tvPremi);
            layoutProposal = view.findViewById(R.id.layoutProposal);
        }

    }

    @Override
    public ProposalListVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.c_proposal_list, parent, false);
        return new ProposalListVH(itemView);
    }

    @Override
    public void onBindViewHolder(final ProposalListVH holder, int position) {
        ProposalListModel model = proposals.get(position);
        holder.tvProductName.setText(model.getLsdbsName());
        holder.tvUangPertanggungan.setText("Uang pertanggungan : "+ NumberFormat.getNumberInstance(Locale.GERMAN).format(model.getUangPertanggungan()));
        holder.tvPemegangPolis.setText(model.getNamaPp());
        holder.tvTertanggung.setText(model.getNamaTt());
        holder.tvPremi.setText("Premi : "+ NumberFormat.getNumberInstance(Locale.GERMAN).format(model.getPremi()));
        holder.layoutProposal.setOnClickListener(v -> clickListener.onProposalSelected(holder.getAdapterPosition(), model));
        String noProposal = model.getNoProposal();
        String noProposalTab = model.getNoProposalTab();

        if (TextUtils.isEmpty(noProposal)) {
            holder.tvNoProposal.setText(noProposalTab);
            holder.tvNoProposal.setText("Nomor Proposal "+noProposalTab);
        } else {
            holder.tvNoProposal.setText(noProposal);
            holder.tvNoProposal.setText("Nomor Proposal "+noProposal);
        }



    }

    @Override
    public int getItemCount() {
        return proposals.size();
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onProposalSelected(int position, ProposalListModel model);
    }

    public void addAll(List<ProposalListModel> proposalsList) {
        for (ProposalListModel proposal : proposalsList) {
            proposals.add(proposal);
        }
        notifyDataSetChanged();
    }
}
