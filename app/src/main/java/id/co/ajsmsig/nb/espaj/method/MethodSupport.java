/**
 * @author Eriza Siti Mulyani
 * <p>
 * Method ini digunakan untuk membantu fungsi berulang kali didalam suatu class agar tidak dibuat berulang2 didalam class tersebut
 */
package id.co.ajsmsig.nb.espaj.method;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.DBHelper;
import id.co.ajsmsig.nb.espaj.fragment.E_ProfileResikoFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_QuestionnaireFragment;
import id.co.ajsmsig.nb.espaj.fragment.E_UpdateQuisionerSIOFragment;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBank;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelBankCab;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;
import id.co.ajsmsig.nb.prop.model.DropboxModel;

@SuppressLint("NewApi")
public class MethodSupport {

    public static void load_setDropXml(int id, AutoCompleteTextView ac_view, Context context, int flag_dropdown) throws IOException, XmlPullParserException {
        XmlPullParser XmlDrop = context.getResources().getXml(id);
        ArrayList<ModelDropdownInt> Dropdown = getXML(context, id, flag_dropdown);
        ArrayAdapter<ModelDropdownInt> dataAdapter = new ArrayAdapter<ModelDropdownInt>(context,
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_view.setAdapter(dataAdapter);
    }

    public static void load_setDropXmlString(int id, AutoCompleteTextView ac_view, Context context, int flag_dropdown) throws IOException, XmlPullParserException {
        XmlPullParser XmlDrop = context.getResources().getXml(id);
        ArrayList<ModelDropDownString> Dropdown = getXMLString(context, id, flag_dropdown);
        ArrayAdapter<ModelDropDownString> dataAdapter = new ArrayAdapter<ModelDropDownString>(context,
                android.R.layout.simple_dropdown_item_1line, Dropdown);
        ac_view.setAdapter(dataAdapter);
    }

    //select from xml
    public static ArrayList<ModelDropdownInt> getXML(Context context, int id, int flag_dropdown) {
        XmlPullParser XmlDrop = context.getResources().getXml(id);
        ArrayList<ModelDropdownInt> Dropdown = new ArrayList<ModelDropdownInt>();
        try {
            Dropdown = XMLParser.XMLParse(XmlDrop, flag_dropdown);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Dropdown;
    }

    //select from xml
    public static ArrayList<ModelDropDownString> getXMLString(Context context, int id, int flag_dropdown) {
        XmlPullParser XmlDrop = context.getResources().getXml(id);
        ArrayList<ModelDropDownString> Dropdown = new ArrayList<ModelDropDownString>();
        try {
            Dropdown = XMLParser.XMLParseString(XmlDrop, flag_dropdown);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Dropdown;
    }

    public static int OnCheckRadio(RadioGroup radioGroup, int checkedId) {
        int value = 0;
        RadioButton radioButton = radioGroup.findViewById(checkedId);
//		((RadioButton)radioGroup.getChildAt(0)).setChecked(true)‌​;
        if (radioGroup.indexOfChild(radioButton) == 0) {
            value = 1;
        } else if (radioGroup.indexOfChild(radioButton) == 1) {
            value = 0;
        }
        return value;
    }

    public static void OnsetTwoRadio(RadioGroup radioGroup, int value) {

//        RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
        if (value == 0) {
            ((RadioButton) radioGroup.getChildAt(1)).setChecked(true);
        } else if (value == 1) {
            ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
        } else {
            radioGroup.clearCheck();
        }
    }

    public static void OnRadioButton(RadioButton radioButton, E_ProfileResikoFragment profileresiko) {
        radioButton.setFocusable(true);
        radioButton.setFocusableInTouchMode(true);
        radioButton.setOnFocusChangeListener(profileresiko);
    }

    public static void OnRadioButton(RadioButton radioButton, E_UpdateQuisionerSIOFragment updateQuisioner) {
        radioButton.setFocusable(true);
        radioButton.setFocusableInTouchMode(true);
        radioButton.setOnFocusChangeListener(updateQuisioner);
    }

    public static void OnRadioButton(RadioButton radioButton, E_QuestionnaireFragment updateQuisioner) {
        radioButton.setFocusable(true);
        radioButton.setFocusableInTouchMode(true);
        radioButton.setOnFocusChangeListener(updateQuisioner);
    }

    public static void OnRadioGroup(RadioGroup optiongrup_no1_pr, E_ProfileResikoFragment profileResiko) {
        optiongrup_no1_pr.setFocusable(true);
        optiongrup_no1_pr.setFocusableInTouchMode(true);
        optiongrup_no1_pr.setOnFocusChangeListener(profileResiko);
        optiongrup_no1_pr.setOnCheckedChangeListener(profileResiko);
    }

    public static void OnRadioGroup(RadioGroup option_grup_sio_ppno1, E_UpdateQuisionerSIOFragment updateQuisioner) {
        option_grup_sio_ppno1.setFocusable(true);
        option_grup_sio_ppno1.setFocusableInTouchMode(true);
        option_grup_sio_ppno1.setOnFocusChangeListener(updateQuisioner);
        option_grup_sio_ppno1.setOnCheckedChangeListener(updateQuisioner);
    }

    public static void OnRadioGroup(RadioGroup option_grup_tt_no11b, E_QuestionnaireFragment updateQuisioner) {
        option_grup_tt_no11b.setFocusable(true);
        option_grup_tt_no11b.setFocusableInTouchMode(true);
        option_grup_tt_no11b.setOnFocusChangeListener(updateQuisioner);
        option_grup_tt_no11b.setOnCheckedChangeListener(updateQuisioner);

    }

    public static void focuslayouttext(boolean hasFocus, ConstraintLayout layout) {
        if (!hasFocus) {

        } else {

        }
    }//belum lengkap

    public static void focuslayoutRadio(boolean hasFocus, View layout, RadioGroup radio, int optionYTtNo1) {
        if (!hasFocus) {

        } else {
            radio.check(optionYTtNo1);
        }
    }//belum lengkap

    public static int OnCheckQuisionerRadioPR(RadioGroup radioGroup, String answer_no1, TextView nilai_no1_pr,
                                              ImageView imgval_no1_pr, int checkedId, Context context) {
        int value = 0;
        int color;
        RadioButton radioButton = radioGroup.findViewById(checkedId);
//		((RadioButton)radioGroup.getChildAt(0)).setChecked(true)â€Œâ€‹;
        if (radioGroup.indexOfChild(radioButton) == 0) {
            nilai_no1_pr.setText("2");
            value = 2;
            color = ContextCompat.getColor(context, R.color.green);
            imgval_no1_pr.setColorFilter(color);
        } else if (radioGroup.indexOfChild(radioButton) == 1) {
            nilai_no1_pr.setText("4");
            value = 4;
            color = ContextCompat.getColor(context, R.color.green);
            imgval_no1_pr.setColorFilter(color);
        } else if (radioGroup.indexOfChild(radioButton) == 2) {
            nilai_no1_pr.setText("6");
            value = 6;
            color = ContextCompat.getColor(context, R.color.green);
            imgval_no1_pr.setColorFilter(color);
        } else if (radioGroup.indexOfChild(radioButton) == 3) {
            nilai_no1_pr.setText("8");
            value = 8;
            color = ContextCompat.getColor(context, R.color.green);
            imgval_no1_pr.setColorFilter(color);
        } else if (radioGroup.indexOfChild(radioButton) == 4) {
            nilai_no1_pr.setText("10");
            value = 10;
            color = ContextCompat.getColor(context, R.color.green);
            imgval_no1_pr.setColorFilter(color);
        }
        return value;
    }

    //khusus no 2 4 8 di data kesehatan
    public static HashMap<String, Integer> OnCheckQuisionerRadioKesValueText(RadioGroup radioAktif, RadioGroup radioGroup1, RadioGroup radioGroup2, RadioGroup radioGroup3, RadioGroup radioGroup4, EditText text, int int_y_id1, int int_n_id1,
                                                                             int checkedId, ImageView img_val, Context context) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        boolean val = true;
        int color;
        RadioButton radioButton = radioAktif.findViewById(checkedId);
        if (radioAktif.indexOfChild(radioButton) == 0) {
            int_y_id1 = 1;
            int_n_id1 = 0;
            text.setEnabled(true);
            text.setFocusable(true);
            text.setClickable(true);
            text.setFocusableInTouchMode(true);

        } else if (radioAktif.indexOfChild(radioButton) == 1) {
            int_y_id1 = 0;
            int_n_id1 = 1;
        }
        if (radioGroup1.getVisibility() == View.VISIBLE && (radioGroup1.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (radioGroup2.getVisibility() == View.VISIBLE && (radioGroup2.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (radioGroup3.getVisibility() == View.VISIBLE && (radioGroup3.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (radioGroup4.getVisibility() == View.VISIBLE && (radioGroup4.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (!val) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            color = ContextCompat.getColor(context, R.color.green);
        }
        img_val.setColorFilter(color);
        map.put("int_y_id1", int_y_id1);
        map.put("int_n_id1", int_n_id1);
        return map;
    }

    //membantu value option button
    public static HashMap<String, Integer> OnCheckQuisionerRadio(RadioGroup radioGroup, int int_y_id1, int int_n_id1, EditText text,
                                                                 int checkedId, ImageView img_val, Context context) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        boolean val = true;
        int color;
        int length = text.getText().toString().length();
        text.setEnabled(false);
        text.setFocusable(false);
        text.setClickable(false);
        text.setFocusableInTouchMode(false);
        RadioButton radioButton = radioGroup.findViewById(checkedId);
        if (radioGroup.indexOfChild(radioButton) == 0) {
            int_y_id1 = 1;
            int_n_id1 = 0;
            text.setEnabled(true);
            text.setFocusable(true);
            text.setClickable(true);
            text.setFocusableInTouchMode(true);

            val = false;
            //text.setBackgroundResource(R.drawable.border_corner_4);
        } else if (radioGroup.indexOfChild(radioButton) == 1) {
            int_y_id1 = 0;
            int_n_id1 = 1;
            text.setText("");
            text.setEnabled(false);
            text.setFocusable(false);
            text.setClickable(false);
            text.setFocusableInTouchMode(false);
            val = true;
            //text.setBackgroundResource(R.drawable.borderx);
        }
        if (!val) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            color = ContextCompat.getColor(context, R.color.green);
        }
        img_val.setColorFilter(color);
        map.put("int_y_id1", int_y_id1);
        map.put("int_n_id1", int_n_id1);
        return map;
    }

    //membantu value option button dengan tabel
    public static HashMap<String, Integer> OnCheckQuisionerRadioTabel(RadioGroup radioGroup, int int_y_id1, int int_n_id1, ImageButton button,
                                                                      int checkedId, ImageView img_val, Context context) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        boolean val = true;
        int color;
        active_view(0, button);
        RadioButton radioButton = radioGroup.findViewById(checkedId);
        if (radioGroup.indexOfChild(radioButton) == 0) {
            int_y_id1 = 1;
            int_n_id1 = 0;
            active_view(1, button);
        } else if (radioGroup.indexOfChild(radioButton) == 1) {
            int_y_id1 = 0;
            int_n_id1 = 1;
            active_view(0, button);
            val = true;
        }
        if (!val) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            color = ContextCompat.getColor(context, R.color.green);
        }
        img_val.setColorFilter(color);
        map.put("int_y_id1", int_y_id1);
        map.put("int_n_id1", int_n_id1);
        return map;
    }

    //membantu value option button (khusus no 8 TT)
    public static HashMap<String, Integer> OnCheckQuisionerRadio2(RadioGroup radioGroup, int int_y_id1, int int_n_id1, EditText text, EditText text2,
                                                                  int checkedId, ImageView img_val, Context context) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        boolean val = true;
        int color;
        text.setEnabled(false);
        text.setFocusable(false);
        text.setClickable(false);
        text.setFocusableInTouchMode(false);
        RadioButton radioButton = radioGroup.findViewById(checkedId);
        if (radioGroup.indexOfChild(radioButton) == 0) {
            int_y_id1 = 1;
            int_n_id1 = 0;
            text.setEnabled(true);
            text.setFocusable(true);
            text.setClickable(true);
            text.setFocusableInTouchMode(true);
            //text.setBackgroundResource(R.drawable.border_corner_4);

            text2.setEnabled(true);
            text2.setFocusable(true);
            text2.setClickable(true);
            text2.setFocusableInTouchMode(true);
            //text2.setBackgroundResource(R.drawable.border_corner_4);
        } else if (radioGroup.indexOfChild(radioButton) == 1) {
            int_y_id1 = 0;
            int_n_id1 = 1;
            text.setText("");
            text.setEnabled(false);
            text.setFocusable(false);
            text.setClickable(false);
            text.setFocusableInTouchMode(false);
            //text.setBackgroundResource(R.drawable.borderx);

            text2.setText("");
            text2.setEnabled(false);
            text2.setFocusable(false);
            text2.setClickable(false);
            text2.setFocusableInTouchMode(false);

            val = true;
            //text2.setBackgroundResource(R.drawable.borderx);
        }

        if (!val) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            color = ContextCompat.getColor(context, R.color.green);
        }
        img_val.setColorFilter(color);
        map.put("int_y_id1", int_y_id1);
        map.put("int_n_id1", int_n_id1);
        return map;
    }

    public static HashMap<String, Integer> OnCheckQuisionerRadioText2(RadioGroup radioGroup, int int_y_id1, int int_n_id1,
                                                                      EditText text_1, EditText text_2, int checkedId, ImageView img_val, ImageView img_val2, Context context) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();

        int color;
        int color2;
        boolean val = true;

        text_1.setEnabled(false);
        text_1.setFocusable(false);
        text_1.setClickable(false);
        text_1.setFocusableInTouchMode(false);

        text_2.setEnabled(false);
        text_2.setFocusable(false);
        text_2.setClickable(false);
        text_2.setFocusableInTouchMode(false);
        RadioButton radioButton = radioGroup.findViewById(checkedId);

        if (radioGroup.indexOfChild(radioButton) == 0) {
            int_y_id1 = 1;
            int_n_id1 = 0;
            text_1.setText("");
            text_1.setEnabled(false);
            text_1.setFocusable(false);
            text_1.setClickable(false);
            text_1.setFocusableInTouchMode(false);
            //text_1.setBackgroundResource(R.drawable.borderx);

            text_2.setText("");
            text_2.setEnabled(true);
            text_2.setFocusable(true);
            text_2.setClickable(true);
            text_2.setFocusableInTouchMode(true);

            //text_2.setBackgroundResource(R.drawable.border_corner_4);
        } else if (radioGroup.indexOfChild(radioButton) == 1) {
            int_y_id1 = 0;
            int_n_id1 = 1;
            text_1.setEnabled(true);
            text_1.setFocusable(true);
            text_1.setClickable(true);
            text_1.setFocusableInTouchMode(true);
            //text_1.setBackgroundResource(R.drawable.border_corner_4);

            text_2.setText("");
            text_2.setEnabled(false);
            text_2.setFocusable(false);
            text_2.setClickable(false);
            text_2.setFocusableInTouchMode(false);
            //text_2.setBackgroundResource(R.drawable.borderx);

            val = false;
        }

        if (text_1.isEnabled() && text_1.getText().toString().length() != 0) {
            val = true;
        } else if (text_2.isEnabled() && text_2.getText().toString().length() != 0) {
            val = true;
        }

        if (!val) {
            color = ContextCompat.getColor(context, R.color.orange);
            color2 = ContextCompat.getColor(context, R.color.orange);
        } else {
            color = ContextCompat.getColor(context, R.color.green);
            color2 = ContextCompat.getColor(context, R.color.green);
        }
        img_val.setColorFilter(color);
        img_val2.setColorFilter(color2);
        map.put("int_y_id1", int_y_id1);
        map.put("int_n_id1", int_n_id1);
        return map;
    }

    //membantu value option button tanpa text
    public static HashMap<String, Integer> OnCheckQuisionerRadioValue(RadioGroup radioGroup, int int_y_id1, int int_n_id1,
                                                                      int checkedId, ImageView img_val, Context context) {
        boolean val = true;
        int color;

        HashMap<String, Integer> map = new HashMap<String, Integer>();
        RadioButton radioButton = radioGroup.findViewById(checkedId);
        if (radioGroup.indexOfChild(radioButton) == 0) {
            int_y_id1 = 1;
            int_n_id1 = 0;
            val = true;
        } else if (radioGroup.indexOfChild(radioButton) == 1) {
            int_y_id1 = 0;
            int_n_id1 = 1;
            val = true;
        }

        if (!val) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            color = ContextCompat.getColor(context, R.color.green);
        }
        img_val.setColorFilter(color);
        map.put("int_y_id1", int_y_id1);
        map.put("int_n_id1", int_n_id1);
        return map;
    }

    /**
     * @param radioAktif  : untuk radio group yang aktif di method onCheckedChanged
     * @param radioGroup1 : 1-4 untuk radio group yang ada didalam parent layout yang sama
     * @param radioGroup2
     * @param radioGroup3
     * @param radioGroup4
     * @param int_y_id1   : value radio group yang aktif
     * @param int_n_id1
     * @param checkedId
     * @param img_val     //
     * @param context
     * @return
     */

    //membantu value option button tanpa text untuk produk kesehatan
    public static HashMap<String, Integer> OnCheckQuisionerRadioKesValue(RadioGroup radioAktif, RadioGroup radioGroup1, RadioGroup radioGroup2, RadioGroup radioGroup3, RadioGroup radioGroup4, int int_y_id1, int int_n_id1,
                                                                         int checkedId, ImageView img_val, Context context) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        boolean val = true;
        int color;
        RadioButton radioButton = radioAktif.findViewById(checkedId);
        if (radioAktif.indexOfChild(radioButton) == 0) {
            int_y_id1 = 1;
            int_n_id1 = 0;
        } else if (radioAktif.indexOfChild(radioButton) == 1) {
            int_y_id1 = 0;
            int_n_id1 = 1;
        }
        if (radioGroup1.getVisibility() == View.VISIBLE && (radioGroup1.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (radioGroup2.getVisibility() == View.VISIBLE && (radioGroup2.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (radioGroup3.getVisibility() == View.VISIBLE && (radioGroup3.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (radioGroup4.getVisibility() == View.VISIBLE && (radioGroup4.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (!val) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            color = ContextCompat.getColor(context, R.color.green);
        }
        img_val.setColorFilter(color);
        map.put("int_y_id1", int_y_id1);
        map.put("int_n_id1", int_n_id1);
        return map;
    }

    //membantu value option button ada text khusus no 7 untuk produk kesehatan
    public static HashMap<String, Integer> OnCheckQuisionerRadioKesValuekhususno7(RadioGroup radioAktif, RadioGroup radioGroup1, RadioGroup radioGroup2, RadioGroup radioGroup3, RadioGroup radioGroup4, int int_y_id1, int int_n_id1,
                                                                                  int checkedId, ImageView img_val, Context context, EditText texta, EditText textb, EditText textc, EditText textd, ImageView img_val1, ImageView img_val2,
                                                                                  ImageView img_val3, ImageView img_val4) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        boolean val = true;
        boolean val1 = true;
        int color;
        int color2;
        RadioButton radioButton = radioAktif.findViewById(checkedId);
        if (radioAktif.indexOfChild(radioButton) == 0) {
            int_y_id1 = 1;
            int_n_id1 = 0;

            texta.setEnabled(true);
            texta.setFocusable(true);
            texta.setClickable(true);
            texta.setFocusableInTouchMode(true);

            textb.setEnabled(true);
            textb.setFocusable(true);
            textb.setClickable(true);
            textb.setFocusableInTouchMode(true);

            textc.setEnabled(true);
            textc.setFocusable(true);
            textc.setClickable(true);
            textc.setFocusableInTouchMode(true);

            textd.setEnabled(true);
            textd.setFocusable(true);
            textd.setClickable(true);
            textd.setFocusableInTouchMode(true);

            img_val1.setVisibility(View.VISIBLE);
            img_val2.setVisibility(View.VISIBLE);
            img_val3.setVisibility(View.VISIBLE);
            img_val4.setVisibility(View.VISIBLE);

            val = false;
            val1 = true;

        } else if (radioAktif.indexOfChild(radioButton) == 1) {
            int_y_id1 = 0;
            int_n_id1 = 1;
            texta.setEnabled(false);
            textb.setEnabled(false);
            textc.setEnabled(false);
            textd.setEnabled(false);

            texta.setText("");
            textb.setText("");
            textc.setText("");
            textd.setText("");

            img_val1.setVisibility(View.INVISIBLE);
            img_val2.setVisibility(View.INVISIBLE);
            img_val3.setVisibility(View.INVISIBLE);
            img_val4.setVisibility(View.INVISIBLE);

            val = true;
            val1 = true;
        }
        if (radioGroup1.getVisibility() == View.VISIBLE && (radioGroup1.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (radioGroup2.getVisibility() == View.VISIBLE && (radioGroup2.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (radioGroup3.getVisibility() == View.VISIBLE && (radioGroup3.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (radioGroup4.getVisibility() == View.VISIBLE && (radioGroup4.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (!val) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            color = ContextCompat.getColor(context, R.color.green);
        }

        if (!val1) {
            color2 = ContextCompat.getColor(context, R.color.orange);
        } else {
            color2 = ContextCompat.getColor(context, R.color.green);
        }
        img_val.setColorFilter(color2);
        img_val1.setColorFilter(color);
        img_val2.setColorFilter(color);
        img_val3.setColorFilter(color);
        img_val4.setColorFilter(color);
        map.put("int_y_id1", int_y_id1);
        map.put("int_n_id1", int_n_id1);
        return map;
    }

    //membantu value option button tanpa text untuk produk kesehatan SIO
    public static HashMap<String, Integer> OnCheckQuisionerRadioKesValueSIO(RadioGroup radioAktif, RadioGroup radioGroup1, int int_y_id1, int int_n_id1,
                                                                            int checkedId, ImageView img_val, Context context) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        boolean val = true;
        int color;
        RadioButton radioButton = radioAktif.findViewById(checkedId);
        if (radioAktif.indexOfChild(radioButton) == 0) {
            int_y_id1 = 1;
            int_n_id1 = 0;
        } else if (radioAktif.indexOfChild(radioButton) == 1) {
            int_y_id1 = 0;
            int_n_id1 = 1;
        }
        if (radioGroup1.getVisibility() == View.VISIBLE && (radioGroup1.getCheckedRadioButtonId() == -1)) {
            val = false;
        }
        if (!val) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            color = ContextCompat.getColor(context, R.color.green);
        }
        img_val.setColorFilter(color);
        map.put("int_y_id1", int_y_id1);
        map.put("int_n_id1", int_n_id1);
        return map;
    }

    //membantu value option button ada text untuk produk kesehatan SIO
    public static HashMap<String, Integer> OnCheckQuisionerRadioKesValueEditSIO(RadioGroup radioAktif, RadioGroup radioGroup1, int int_y_id1, int int_n_id1,
                                                                                int checkedId, ImageView img_val, Context context, EditText editText, int int_y_id2, int int_n_id2) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        boolean val = true;
        int color;
        RadioButton radioButton = radioAktif.findViewById(checkedId);
        if (radioAktif.indexOfChild(radioButton) == 0) {
            int_y_id1 = 1;
            int_n_id1 = 0;
//            editText.setEnabled(true);
        } else if (radioAktif.indexOfChild(radioButton) == 1) {
            int_y_id1 = 0;
            int_n_id1 = 1;
            editText.setEnabled(false);
        }

        if (radioGroup1.getVisibility() == View.VISIBLE && (radioGroup1.getCheckedRadioButtonId() == -1)) {
            val = false;
        } else {
            if (int_y_id1 == 0 && int_y_id2 == 0) {
                editText.setEnabled(false);
                editText.setText("");
                val = true;
            } else if (int_y_id1 == 1 && int_y_id2 == 0) {
                editText.setEnabled(true);
//                editText.setText("");
                val = false;
            } else if (int_y_id1 == 0 && int_y_id2 == 1) {
                editText.setEnabled(true);
//                editText.setText("");
                val = false;
            } else if (int_y_id1 == 1 && int_y_id2 == 1) {
                editText.setEnabled(true);
//                editText.setText("");
                val = false;
            }
        }
        if (!val) {
            color = ContextCompat.getColor(context, R.color.orange);
        } else {
            color = ContextCompat.getColor(context, R.color.green);
        }
        img_val.setColorFilter(color);
        map.put("int_y_id1", int_y_id1);
        map.put("int_n_id1", int_n_id1);
        map.put("int_y_id2", int_y_id2);
        map.put("int_n_id2", int_n_id2);
        return map;
    }

    //active or not active view
    public static void active_view(int flag_active, View view) {
        if (flag_active == 0) {
            view.setEnabled(false);
            view.setClickable(false);
            view.setFocusable(false);
        } else {
            view.setEnabled(true);
            view.setClickable(true);
            view.setFocusable(true);
        }
    }

    public static void active_radio(int flag_active, RadioGroup radio) {
        for (int i = 0; i < radio.getChildCount(); i++) {
            if (flag_active == 0) {
                radio.getChildAt(i).setEnabled(false);
                radio.getChildAt(i).setClickable(false);
                radio.getChildAt(i).setFocusable(false);
            } else {
                radio.getChildAt(i).setEnabled(true);
                radio.getChildAt(i).setClickable(true);
                radio.getChildAt(i).setFocusable(true);
            }
        }

    }

    //getAdapterPosition by ID int return String
    public static String getAdapterPosition(ArrayList<DropboxModel> List, int kode_id) {
        String value = "";
        for (int i = 0; i < List.size(); i++) {
            if (List.get(i).getId() == kode_id) {
                value = List.get(i).getLabel();
                break;
            }
        }
        return value;
    }

    //khusus sub produj
    public static String getSubAdapterPosition(ArrayList<DropboxModel> List, int kode_id) {
        String value = "";
        for (int i = 0; i < List.size(); i++) {
            if (List.get(i).getIdSecond() == kode_id) {
                value = List.get(i).getLabel();
                break;
            }
        }
        return value;
    }

    //khusus sub produj
    public static String getSubAdapterPacket(ArrayList<DropboxModel> List, int kode_id) {
        String value = "";
        for (int i = 0; i < List.size(); i++) {
            if (List.get(i).getFlagPacket() == kode_id) {
                value = List.get(i).getLabel();
                break;
            }
        }
        return value;
    }

    public static int getValue(int lsb_id, String nm_table, String parameter) {
        DBHelper dbHelper = null;
        int line_bus = 0;
        String selectQuery = "select " + parameter + " from " + nm_table + " where lsbs_id = " + lsb_id;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                line_bus = cursor.getInt(0);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return line_bus;
    }


    public static String getAdapterPositionSPAJ(ArrayList<ModelDropdownInt> List, int kode_id) {
        String value = "";
        for (int i = 0; i < List.size(); i++) {
            if (List.get(i).getId() == kode_id) {
                value = List.get(i).getValue();
                break;
            }
        }
        return value;
    }

    public static String getAdapterPositionSPAJValue( ArrayList<ModelDropdownInt> List, String kode_id) {
        String value = "";
        for (int i = 0; i < List.size(); i++) {
            if (List.get(i).getValue().equals(kode_id)) {
                value = List.get(i).getValue();
                break;
            }
        }
        return value;
    }

    public static String getAdapterPositionString(ArrayList<ModelDropDownString> List, String kode_id) {
        String value = "";
        for (int i = 0; i < List.size(); i++) {
            if (List.get(i).getId().equals(kode_id)) {
                value = List.get(i).getValue();
                break;
            }
        }
        return value;
    }

    public static String getAdapterPositionArray(String[] srt_array, int kode_id) {
        String value = "";
        for (int i = 0; i < srt_array.length; i++) {
            if (i == kode_id) {
                value = srt_array[i];
                break;
            }
        }
        return value;
    }

    public static String getAdapterPositionArrayString(ListAdapter adapter, String[] srt_array, String kode_id) {
        String value = "";
        for (int i = 0; i < srt_array.length; i++) {
            if (srt_array[i].equals(kode_id)) {
                value = srt_array[i];
                break;
            }
        }
        return value;
    }

    public static String getAdapterBank(ArrayList<ModelBank> List, int kode_id) {
        String value = "";
        for (int i = 0; i < List.size(); i++) {
            if (List.get(i).getLSBP_ID() == kode_id) {
                value = List.get(i).getLSBP_NAMA();
                break;
            }
        }
        return value;
    }

    public static String getAdapterBankCab(ArrayList<ModelBankCab> List, int kode_id) {
        String value = "";
        for (int i = 0; i < List.size(); i++) {
            if (List.get(i).getLBN_ID() == kode_id) {
                value = List.get(i).getLBN_NAMA();
                break;
            }
        }
        return value;
    }

    public static int getArrayPosition(String[] srt_array, String value) {
        int pos = 0;
        for (int i = 0; i < srt_array.length; i++) {
            if (srt_array[i].equals(value)) {
                pos = i;
                break;
            }
        }
        return pos;
    }

    //method untuk Simpan Image dan menghitung view
    public static void onCreateBitmap(View child_pp, ScrollView scroll_pp, File file_bitmap, String fileName, Context context) {
        int totalHeight = scroll_pp.getChildAt(0).getHeight();
        int totalWidth = scroll_pp.getChildAt(0).getWidth();

        Bitmap bitmap = loadBitmapFromView(child_pp, totalWidth, totalHeight);
        File myPath = new File(file_bitmap, fileName);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myPath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 10, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    //method untuk Simpan Image dan menghitung view
    public static void onCreateBitmapScroll(View child_pp, NestedScrollView scroll_pp, File file_bitmap, String fileName, Context context) {
        int totalHeight = scroll_pp.getChildAt(0).getHeight();
        int totalWidth = scroll_pp.getChildAt(0).getWidth();

        Bitmap bitmap = loadBitmapFromView(child_pp, totalWidth, totalHeight);
        File myPath = new File(file_bitmap, fileName);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myPath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 10, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    //method convert View to Bitmap
    public static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap returnedBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);

        try {
            Drawable bgDrawable = v.getBackground();
            if (bgDrawable != null) {
                bgDrawable.draw(canvas);
            } else {
                canvas.drawColor(Color.rgb(242, 242, 242));
            }
            v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
            v.draw(canvas);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return returnedBitmap;
    }

    public static void Alert(Context context, String title, String message, int flag) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static Double ConverttoDouble(EditText edit) {
        String total = "";
        Double value = (double) 0;
        total = edit.getText().toString().replace(".", "")
                .trim();
        if (!(total.equals(""))) {
            value = Double.parseDouble(total);
        } else {
            value = (double) 0;
        }
        return value;
    }

    public static int ConverttoInt(EditText edit) {
        String total = "";
        int value = 0;
        if (edit.getText().toString().length() == 0) {
            value = 0;
        } else {
            total = edit.getText().toString().trim();
            value = Integer.parseInt(total);
        }
        return value;
    }

    public static Double onSetDouble(String total) {
        Double value = (double) 0;
        if (total == null) total = "";
        if (!total.equals("")) {
            value = Double.parseDouble(total);
        } else {
            value = (double) 0;
        }
        return value;
    }

    //method untuk settext dari double ke string
    public static String onSetString(Double value) {
        String text = "";
        if (new BigDecimal(value).toString().equals("0")) {
            text = "";
        } else {
            text = new BigDecimal(value).toString();
        }
        return text;
    }

    public static boolean isExistStringInArray(List<String> theArray, String theString) {
        boolean result = false;
        String comparison = "";
        for (int i = 0; i < theArray.size(); i++) {
            comparison = theArray.get(i);
            if (comparison.equals(theString)) {
                result = true;
            }
        }
        return result;

    }

    public static Boolean isPPandTTSamePerson(EspajModel me) {
        boolean isSame = true;
        if (me.getPemegangPolisModel().getNama_pp() != null) {
            if (!me.getPemegangPolisModel().getNama_pp().equals(me.getTertanggungModel().getNama_tt())) {
                isSame = false;
            }
        }
        if (me.getPemegangPolisModel().getTtl_pp() != null) {
            if (!me.getPemegangPolisModel().getTtl_pp().equals(me.getTertanggungModel().getTtl_tt())) {
                isSame = false;
            }
        }
        if (me.getPemegangPolisModel().getJekel_pp() != -1) {
            if (me.getPemegangPolisModel().getJekel_pp() != me.getTertanggungModel().getJekel_tt()) {
                isSame = false;
            }
        }
        return isSame;
    }

    public static void disable(boolean state, ViewGroup layout) {
        layout.setEnabled(state);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                disable(state, (ViewGroup) child);
            } else {
                child.setEnabled(state);
                child.setClickable(state);
                child.setFocusable(state);
            }
        }
    }

    //method pdf reader
    public static void openPDF(TouchImageView pdfView, PdfRenderer pdfRenderer, int position) throws IOException {

//        ParcelFileDescriptor fileDescriptor = null;
//        fileDescriptor = ParcelFileDescriptor.open(
//                file, ParcelFileDescriptor.MODE_READ_ONLY);
//
//        PdfRenderer pdfRenderer = null;
//        pdfRenderer = new PdfRenderer(fileDescriptor);

//        final int pageCount = pdfRenderer.getPageCount();
//        Toast.makeText(context,
//                "pageCount = " + pageCount,
//                Toast.LENGTH_LONG).show();

        //Display page 0
        PdfRenderer.Page rendererPage = pdfRenderer.openPage(position);
        int rendererPageWidth = rendererPage.getWidth();
        int rendererPageHeight = rendererPage.getHeight();
        Matrix m = pdfView.getImageMatrix();
        Rect rect = new Rect(0, 0, rendererPageWidth, rendererPageHeight);
        Bitmap bitmap = Bitmap.createBitmap(
                rendererPageWidth,
                rendererPageHeight,
                Bitmap.Config.ARGB_8888);
        rendererPage.render(bitmap, rect, m,
                PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
        pdfView.setImageBitmap(bitmap);
        pdfView.setImageMatrix(m);
        pdfView.invalidate();
        rendererPage.close();

//        pdfRenderer.close();
//        fileDescriptor.close();
    }

    public static void onFocusview(ScrollView scroll, View child, View view, TextView text, ImageView img, int color, Context context, String error) {
        text.setVisibility(View.VISIBLE);
        text.setText(error);
        img.setColorFilter(color);

        scroll.requestChildFocus(child, view);
        text.startAnimation(AnimationUtils.loadAnimation(context, R.anim.wobble));
    }

    public static void onFocusviewtest(ScrollView scroll, View child, View view) {
        scroll.requestChildFocus(child, view);
    }

    public static void onFocusviewNested(NestedScrollView scroll, View child, View view, TextView text, ImageView img, int color, Context context, String error) {
        text.setVisibility(View.VISIBLE);
        text.setText(error);
        img.setColorFilter(color);

        scroll.requestChildFocus(child, view);
        text.startAnimation(AnimationUtils.loadAnimation(context, R.anim.wobble));
    }

    public static void onFocusviewNoValidation(ScrollView scroll, View child, View view, TextView text, int color, Context context, String error) {
        text.setVisibility(View.VISIBLE);
        text.setText(error);
//        img.setColorFilter(color);

        scroll.requestChildFocus(child, view);
        text.startAnimation(AnimationUtils.loadAnimation(context, R.anim.wobble));
    }
}