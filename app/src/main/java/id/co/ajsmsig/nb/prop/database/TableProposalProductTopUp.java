package id.co.ajsmsig.nb.prop.database;
/*
 *Created by faiz_f on 10/05/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.math.BigDecimal;
import java.util.ArrayList;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductTopUpModel;

public class TableProposalProductTopUp {
    private Context context;

    public TableProposalProductTopUp(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(P_MstProposalProductTopUpModel p_mstProposalProductTopUpModel){
        ContentValues cv = new ContentValues();
        cv.put(context.getString(R.string.NO_PROPOSAL_TAB), p_mstProposalProductTopUpModel.getNo_proposal_tab());
        cv.put(context.getString(R.string.NO_PROPOSAL), p_mstProposalProductTopUpModel.getNo_proposal());
        cv.put(context.getString(R.string.THN_KE), p_mstProposalProductTopUpModel.getThn_ke());
        cv.put(context.getString(R.string.TOPUP), p_mstProposalProductTopUpModel.getTopup());
        cv.put(context.getString(R.string.TARIK), p_mstProposalProductTopUpModel.getTarik());

        return cv;
    }


    public ArrayList<P_MstProposalProductTopUpModel> getObjectArray(Cursor cursor){
        ArrayList<P_MstProposalProductTopUpModel> p_mstProposalProductTopUpModels = new ArrayList<>();
        if (cursor != null && cursor.getCount()>0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                P_MstProposalProductTopUpModel p_mstProposalProductTopUpModel = new P_MstProposalProductTopUpModel();
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB)))) {
                    p_mstProposalProductTopUpModel.setNo_proposal_tab(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL)))) {
                    p_mstProposalProductTopUpModel.setNo_proposal(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.THN_KE)))) {
                    p_mstProposalProductTopUpModel.setThn_ke(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.THN_KE))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.TOPUP)))) {
                    p_mstProposalProductTopUpModel.setTopup(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.TOPUP))));
                }
                if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.TARIK)))) {
                    p_mstProposalProductTopUpModel.setTarik(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.TARIK))));
                }
                p_mstProposalProductTopUpModel.setSelected(true);
                p_mstProposalProductTopUpModels.add(p_mstProposalProductTopUpModel);
                cursor.moveToNext();
            }
            // always close the cursor
            cursor.close();
        }

        return p_mstProposalProductTopUpModels;
    }

    public ArrayList<P_MstProposalProductTopUpModel> getObjectArrayCustom(ArrayList<P_MstProposalProductTopUpModel> oldTopUp) {
        ArrayList<P_MstProposalProductTopUpModel> newTopUp = new ArrayList<>();
        for (int i = 1; i <= 50; i++) {
            P_MstProposalProductTopUpModel propModelMstProposalProductTopUp = new P_MstProposalProductTopUpModel();
            propModelMstProposalProductTopUp.setThn_ke(i);
            propModelMstProposalProductTopUp.setTopup("0");
            propModelMstProposalProductTopUp.setTarik("0");
            propModelMstProposalProductTopUp.setSelected(false);
            newTopUp.add(i - 1, propModelMstProposalProductTopUp);
        }

        for (P_MstProposalProductTopUpModel propModelMstProposalProductTopUp : oldTopUp) {
            int currentThn = propModelMstProposalProductTopUp.getThn_ke();
            newTopUp.add(currentThn - 1, propModelMstProposalProductTopUp);
        }
        return newTopUp;
    }
}
