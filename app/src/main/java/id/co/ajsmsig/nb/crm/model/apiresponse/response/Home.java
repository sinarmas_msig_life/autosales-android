
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Home {

    @SerializedName("SLA2_ID")
    @Expose
    private String sLA2ID;
    @SerializedName("SLA2_INST_ID")
    @Expose
    private String sLA2INSTID;
    @SerializedName("SLA2_INST_TYPE")
    @Expose
    private String sLA2INSTTYPE;
    @SerializedName("SLA2_ADDR_TYPE")
    @Expose
    private String sLA2ADDRTYPE;
    @SerializedName("SLA2_DETAIL")
    @Expose
    private String sLA2DETAIL;
    @SerializedName("SLA2_POSTCODE")
    @Expose
    private String sLA2POSTCODE;
    @SerializedName("SLA2_CITY")
    @Expose
    private String sLA2CITY;
    @SerializedName("SLA2_STATE")
    @Expose
    private String sLA2STATE;
    @SerializedName("SLA2_HP1")
    @Expose
    private String sLA2HP1;
    @SerializedName("SLA2_HP2")
    @Expose
    private String sLA2HP2;
    @SerializedName("SLA2_PHONECODE")
    @Expose
    private String sLA2PHONECODE;
    @SerializedName("SLA2_PHONE1")
    @Expose
    private String sLA2PHONE1;
    @SerializedName("SLA2_PHONE2")
    @Expose
    private String sLA2PHONE2;
    @SerializedName("SLA2_EMAIL")
    @Expose
    private String sLA2EMAIL;
    @SerializedName("SLA2_FAXCODE")
    @Expose
    private String sLA2FAXCODE;
    @SerializedName("SLA2_FAX")
    @Expose
    private String sLA2FAX;
    @SerializedName("SLA2_LAT")
    @Expose
    private String sLA2LAT;
    @SerializedName("SLA2_LON")
    @Expose
    private String sLA2LON;
    @SerializedName("SLA2_OLD_ID")
    @Expose
    private String sLA2OLDID;
    @SerializedName("SLA2_TAB_ID")
    @Expose
    private String sLA2TABID;
    @SerializedName("SLA2_LSPR_ID")
    @Expose
    private String sLA2LSPRID;
    @SerializedName("SLA2_LSKA_ID")
    @Expose
    private String sLA2LSKAID;
    @SerializedName("SLA2_LSKC_ID")
    @Expose
    private String sLA2LSKCID;
    @SerializedName("SLA2_LSKL_ID")
    @Expose
    private String sLA2LSKLID;
    @SerializedName("RNUM")
    @Expose
    private String rNUM;

    public String getSLA2ID() {
        return sLA2ID;
    }

    public void setSLA2ID(String sLA2ID) {
        this.sLA2ID = sLA2ID;
    }

    public String getSLA2INSTID() {
        return sLA2INSTID;
    }

    public void setSLA2INSTID(String sLA2INSTID) {
        this.sLA2INSTID = sLA2INSTID;
    }

    public String getSLA2INSTTYPE() {
        return sLA2INSTTYPE;
    }

    public void setSLA2INSTTYPE(String sLA2INSTTYPE) {
        this.sLA2INSTTYPE = sLA2INSTTYPE;
    }

    public String getSLA2ADDRTYPE() {
        return sLA2ADDRTYPE;
    }

    public void setSLA2ADDRTYPE(String sLA2ADDRTYPE) {
        this.sLA2ADDRTYPE = sLA2ADDRTYPE;
    }

    public String getSLA2DETAIL() {
        return sLA2DETAIL;
    }

    public void setSLA2DETAIL(String sLA2DETAIL) {
        this.sLA2DETAIL = sLA2DETAIL;
    }

    public String getSLA2POSTCODE() {
        return sLA2POSTCODE;
    }

    public void setSLA2POSTCODE(String sLA2POSTCODE) {
        this.sLA2POSTCODE = sLA2POSTCODE;
    }

    public String getSLA2CITY() {
        return sLA2CITY;
    }

    public void setSLA2CITY(String sLA2CITY) {
        this.sLA2CITY = sLA2CITY;
    }

    public String getSLA2STATE() {
        return sLA2STATE;
    }

    public void setSLA2STATE(String sLA2STATE) {
        this.sLA2STATE = sLA2STATE;
    }

    public String getSLA2HP1() {
        return sLA2HP1;
    }

    public void setSLA2HP1(String sLA2HP1) {
        this.sLA2HP1 = sLA2HP1;
    }

    public String getSLA2HP2() {
        return sLA2HP2;
    }

    public void setSLA2HP2(String sLA2HP2) {
        this.sLA2HP2 = sLA2HP2;
    }

    public String getSLA2PHONECODE() {
        return sLA2PHONECODE;
    }

    public void setSLA2PHONECODE(String sLA2PHONECODE) {
        this.sLA2PHONECODE = sLA2PHONECODE;
    }

    public String getSLA2PHONE1() {
        return sLA2PHONE1;
    }

    public void setSLA2PHONE1(String sLA2PHONE1) {
        this.sLA2PHONE1 = sLA2PHONE1;
    }

    public String getSLA2PHONE2() {
        return sLA2PHONE2;
    }

    public void setSLA2PHONE2(String sLA2PHONE2) {
        this.sLA2PHONE2 = sLA2PHONE2;
    }

    public String getSLA2EMAIL() {
        return sLA2EMAIL;
    }

    public void setSLA2EMAIL(String sLA2EMAIL) {
        this.sLA2EMAIL = sLA2EMAIL;
    }

    public String getSLA2FAXCODE() {
        return sLA2FAXCODE;
    }

    public void setSLA2FAXCODE(String sLA2FAXCODE) {
        this.sLA2FAXCODE = sLA2FAXCODE;
    }

    public String getSLA2FAX() {
        return sLA2FAX;
    }

    public void setSLA2FAX(String sLA2FAX) {
        this.sLA2FAX = sLA2FAX;
    }

    public String getSLA2LAT() {
        return sLA2LAT;
    }

    public void setSLA2LAT(String sLA2LAT) {
        this.sLA2LAT = sLA2LAT;
    }

    public String getSLA2LON() {
        return sLA2LON;
    }

    public void setSLA2LON(String sLA2LON) {
        this.sLA2LON = sLA2LON;
    }

    public String getSLA2OLDID() {
        return sLA2OLDID;
    }

    public void setSLA2OLDID(String sLA2OLDID) {
        this.sLA2OLDID = sLA2OLDID;
    }

    public String getSLA2TABID() {
        return sLA2TABID;
    }

    public void setSLA2TABID(String sLA2TABID) {
        this.sLA2TABID = sLA2TABID;
    }

    public String getSLA2LSPRID() {
        return sLA2LSPRID;
    }

    public void setSLA2LSPRID(String sLA2LSPRID) {
        this.sLA2LSPRID = sLA2LSPRID;
    }

    public String getSLA2LSKAID() {
        return sLA2LSKAID;
    }

    public void setSLA2LSKAID(String sLA2LSKAID) {
        this.sLA2LSKAID = sLA2LSKAID;
    }

    public String getSLA2LSKCID() {
        return sLA2LSKCID;
    }

    public void setSLA2LSKCID(String sLA2LSKCID) {
        this.sLA2LSKCID = sLA2LSKCID;
    }

    public String getSLA2LSKLID() {
        return sLA2LSKLID;
    }

    public void setSLA2LSKLID(String sLA2LSKLID) {
        this.sLA2LSKLID = sLA2LSKLID;
    }

    public String getRNUM() {
        return rNUM;
    }

    public void setRNUM(String rNUM) {
        this.rNUM = rNUM;
    }

}

