package id.co.ajsmsig.nb.espaj.model.arraylist;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bernard on 30/08/2017.
 */

public class ModelMst_Answer implements Parcelable {
    private int counter = 0;//bernard tambahkan counter supaya format yang diambil ada int nya (kurang 1 int untuk get i)
    private String question_valid_date = "";
    private int question_type_id = 0;
    private int question_id = 0;
    private int option_type = 0;
    private int option_group = 0;
    private int option_order = 0;
    private String answer = "";
    private Boolean validation = true;

    public ModelMst_Answer() {
    }

    protected ModelMst_Answer(Parcel in) {
        counter = in.readInt();
        question_valid_date = in.readString();
        question_type_id = in.readInt();
        question_id = in.readInt();
        option_type = in.readInt();
        option_group = in.readInt();
        option_order = in.readInt();
        answer = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(counter);
        dest.writeString(question_valid_date);
        dest.writeInt(question_type_id);
        dest.writeInt(question_id);
        dest.writeInt(option_type);
        dest.writeInt(option_group);
        dest.writeInt(option_order);
        dest.writeString(answer);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelMst_Answer> CREATOR = new Creator<ModelMst_Answer>() {
        @Override
        public ModelMst_Answer createFromParcel(Parcel in) {
            return new ModelMst_Answer(in);
        }

        @Override
        public ModelMst_Answer[] newArray(int size) {
            return new ModelMst_Answer[size];
        }
    };

    public ModelMst_Answer(int counter, String question_valid_date, int question_type_id, int question_id, int option_type, int option_group, int option_order, String answer) {
        this.counter = counter;
        this.question_valid_date = question_valid_date;
        this.question_type_id = question_type_id;
        this.question_id = question_id;
        this.option_type = option_type;
        this.option_group = option_group;
        this.option_order = option_order;
        this.answer = answer;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getQuestion_valid_date() {
        return question_valid_date;
    }

    public void setQuestion_valid_date(String question_valid_date) {
        this.question_valid_date = question_valid_date;
    }

    public int getQuestion_type_id() {
        return question_type_id;
    }

    public void setQuestion_type_id(int question_type_id) {
        this.question_type_id = question_type_id;
    }

    public int getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public int getOption_type() {
        return option_type;
    }

    public void setOption_type(int option_type) {
        this.option_type = option_type;
    }

    public int getOption_group() {
        return option_group;
    }

    public void setOption_group(int option_group) {
        this.option_group = option_group;
    }

    public int getOption_order() {
        return option_order;
    }

    public void setOption_order(int option_order) {
        this.option_order = option_order;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Boolean getValidation() {
        return validation;
    }

    public void setValidation(Boolean validation) {
        this.validation = validation;
    }

    public static Creator<ModelMst_Answer> getCREATOR() {
        return CREATOR;
    }
}
