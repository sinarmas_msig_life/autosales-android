package id.co.ajsmsig.nb.prop.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.prop.database.P_Select;
import id.co.ajsmsig.nb.prop.method.P_StaticMethods;
import id.co.ajsmsig.nb.prop.model.DropboxModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductPesertaModel;
import id.co.ajsmsig.nb.prop.model.hardCode.P_InsuredAgeFromFlag;

import static id.co.ajsmsig.nb.crm.method.StaticMethods.isTextWidgetEmpty;
import static id.co.ajsmsig.nb.crm.method.StaticMethods.onCountUsia;
import static id.co.ajsmsig.nb.crm.method.StaticMethods.toCalendar;
import static id.co.ajsmsig.nb.crm.method.StaticMethods.toStringDate;


public class P_FormPesertaActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {
    private static final String TAG = "FormPesertaActivity";

    private P_Select select;
    private P_MstProposalProductPesertaModel mstProposalProductPesertaModel;
    private int position;
    private DropboxModel dhubungan;
    private Calendar calendar;
    private boolean isClickAble = true;
    private int riderId;
    private boolean isRiderLadies;
    private boolean isSave = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_activity_form_peserta);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        position = bundle.getInt(getString(R.string.FIELD_TYPE), -1);
        mstProposalProductPesertaModel = bundle.getParcelable(getString(R.string.MST_PROPOSAL_PRODUCT_PESERTA));
        riderId = bundle.getInt(getString(R.string.LSBS_ID), 0);
        isRiderLadies = bundle.getBoolean(getString(R.string.isLadies));
        select = new P_Select(this);

        btn_ok.setOnClickListener(this);
        et_tl.setOnClickListener(this);
        ac_hubungan.setOnClickListener(this);
        et_tl.setOnFocusChangeListener(this);
        ac_hubungan.setOnFocusChangeListener(this);
        et_nama.addTextChangedListener(new MTextWatcher(et_nama));
        et_tl.addTextChangedListener(new MTextWatcher(et_tl));
        et_umur.addTextChangedListener(new MTextWatcher(et_umur));
        ac_hubungan.addTextChangedListener(new MTextWatcher(ac_hubungan));

    }

    @Override
    protected void onResume() {
        super.onResume();
        fillData();
    }

    public void fillData() {
        String headerText = P_StaticMethods.getHeaderPeserta(this, isRiderLadies, position);
        tv_tittle_form.setText(headerText);

        if (mstProposalProductPesertaModel.getNama_peserta() != null) {
            et_nama.setText(mstProposalProductPesertaModel.getNama_peserta());
        }
        if (mstProposalProductPesertaModel.getTgl_lahir() != null) {
            calendar = toCalendar(mstProposalProductPesertaModel.getTgl_lahir());
            et_tl.setText(toStringDate(calendar, 2));
            et_umur.setText(onCountUsia(calendar));
        }
        if (mstProposalProductPesertaModel.getLsre_id() != null) {
            dhubungan = select.selectHubunganTtTamb(mstProposalProductPesertaModel.getLsre_id());
            ac_hubungan.setText(dhubungan.getLabel());
        }

        ArrayList<DropboxModel> dropboxModels = select.selectListHubunganTtTamb();
        ArrayAdapter<DropboxModel> actvAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, dropboxModels);

        ac_hubungan.setAdapter(actvAdapter);
        ac_hubungan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dhubungan = (DropboxModel) parent.getAdapter().getItem(position);
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_tl:
                if (isClickAble) {
                    isClickAble = false;
                    showDateDialog();
                }
                break;
            case R.id.ac_hubungan:
                ac_hubungan.showDropDown();
                break;
            case R.id.btn_ok:
                if (isClickAble) {
                    isClickAble = false;
                    clickOk();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.et_tl:
                if (hasFocus) {
                    showDateDialog();
                }
                break;
            case R.id.ac_hubungan:
                if (hasFocus) {
                    ac_hubungan.showDropDown();
                }
                break;
            default:
                break;
        }
    }

    public void showDateDialog() {
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                et_tl.setText(toStringDate(calendar, 2));
                et_umur.setText(onCountUsia(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE)));
            }
        };

        String tl = et_tl.getText().toString();
        Calendar mCalendar;
        if (tl.trim().length() == 0) {
            calendar = Calendar.getInstance();
            calendar.add(Calendar.YEAR, -30);
        } else {
            calendar = toCalendar(tl);
        }
        mCalendar = calendar;
        DatePickerDialog dateDialog = new DatePickerDialog(P_FormPesertaActivity.this, date, mCalendar
                .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH));


        dateDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                isClickAble = true;
            }
        });

        dateDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                isClickAble = true;
            }
        });

        dateDialog.getDatePicker().setMaxDate(new Date().getTime());

        if (!dateDialog.isShowing()) {
            dateDialog.show();
        }
    }

    public void clickOk() {
        boolean isWidgetFilled = true;

        if (ac_hubungan.getText().toString().trim().length() == 0) {
            isWidgetFilled = false;
            tv_error_hubungan.setText(R.string.error_hubungan_kosong);
            tv_error_hubungan.setVisibility(View.VISIBLE);
            iv_circle_hubungan.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
        }

        if (et_umur.getText().toString().trim().length() == 0) {
            isWidgetFilled = false;
            tv_error_umur.setText(R.string.error_age_empty);
            tv_error_umur.setVisibility(View.VISIBLE);
            iv_circle_umur.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
        }

//        if (position > 0) {
//            if (Integer.valueOf(et_umur.getText().toString()) > 24) {
//                tv_error_umur.setVisibility(View.VISIBLE);
//                tv_error_umur.setText(getString(R.string.batas_usia_tertanggung_1_24));
//                isWidgetFilled = false;
//            }
//        }

        if (et_tl.getText().toString().trim().length() == 0) {
            isWidgetFilled = false;
            tv_error_tl.setText(R.string.error_birthday_empty);
            tv_error_tl.setVisibility(View.VISIBLE);
            iv_circle_tl.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
        }

        if (et_nama.getText().toString().trim().length() == 0) {
            isWidgetFilled = false;
            tv_error_nama.setText(R.string.error_name_empty);
            tv_error_nama.setVisibility(View.VISIBLE);
            iv_circle_nama.setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.red));
        }

        boolean isOkay = true;
        if (isWidgetFilled) {
            int minAge = getMinAgePeserta();
            int maxAge = getMaxAgePeserta(dhubungan.getId());
            int insuredAgeFromFlag = getFlagAge();

            int age = Integer.parseInt(et_umur.getText().toString());

            switch (insuredAgeFromFlag) {
                case P_InsuredAgeFromFlag.YEAR:
                    if (age < minAge) {
                        tv_error_umur.setText(getString(R.string.peserta_umur_min).replace("#{tahun}", String.valueOf(minAge)));
                        tv_error_umur.setVisibility(View.VISIBLE);
                        isOkay = false;
                    }
                    break;
                case P_InsuredAgeFromFlag.DATE:
                    int ageDate = StaticMethods.onCountHari(calendar);
                    if (ageDate < minAge) {
                        tv_error_umur.setText(getString(R.string.peserta_umur_min_zero).replace("#{hari}", String.valueOf(minAge)));
                        tv_error_umur.setVisibility(View.VISIBLE);
                        isOkay = false;
                    }
                    break;
                case P_InsuredAgeFromFlag.MONTH:
                    break;
                default:
                    break;
            }


            if (age > maxAge) {
                tv_error_umur.setText(getString(R.string.tt_umur_max).replace("#{tahun}", String.valueOf(maxAge)));
                tv_error_umur.setVisibility(View.VISIBLE);
                isOkay = false;
            }


            if (isOkay) {
                mstProposalProductPesertaModel.setLsbs_id(riderId);
                mstProposalProductPesertaModel.setNama_peserta(et_nama.getText().toString());
                mstProposalProductPesertaModel.setTgl_lahir(toStringDate(calendar));
                mstProposalProductPesertaModel.setUsia(Integer.parseInt(et_umur.getText().toString()));
                mstProposalProductPesertaModel.setLsre_id(dhubungan.getId());
                mstProposalProductPesertaModel.setPeserta_ke(position + 1);

                Intent intent = new Intent();
                intent.putExtra(getString(R.string.MST_PROPOSAL_PRODUCT_PESERTA), mstProposalProductPesertaModel);
                intent.putExtra(getString(R.string.FIELD_TYPE), position);
                setResult(RESULT_OK, intent);

                finish();
            } else {
                isClickAble = true;
            }
        }else {
            isClickAble = true;
        }


    }


//    int insuredAgeFrom = ageBound.get(getString(R.string.INSURED_AGE_FROM));
//    int insuredAgeTo = ageBound.get(getString(R.string.INSURED_AGE_TO));
//    int insuredAgeFromFlag = ageBound.get(getString(R.string.INSURED_AGE_FROM_FLAG));

    private int getMinAgePeserta() {
        if (riderId == 832 || riderId == 833) {
            return 17;
        } else {
            return 15;//ini dalam hari
        }
    }

    private int getMaxAgePeserta(int relation) {
        if(relation == 4){
            return 24;
        }else {
            if (riderId == 826 || riderId == 819 || riderId == 831) {
                return 64;
            } else {
                return 65;
            }
        }
    }

    private int getFlagAge() {
        if (riderId == 832 || riderId == 833) {
            return 0;
        } else {
            return 2;//ini dalam hari
        }
    }

    private class MTextWatcher implements TextWatcher {
        private View view;

        MTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                case R.id.et_nama:
                    if (isTextWidgetEmpty(et_nama)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_nama.setColorFilter(color);
                    tv_error_nama.setVisibility(View.GONE);
                    break;
                case R.id.et_tl:
                    if (isTextWidgetEmpty(et_tl)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_tl.setColorFilter(color);
                    tv_error_tl.setVisibility(View.GONE);
                    break;
                case R.id.et_umur:
                    if (isTextWidgetEmpty(et_umur)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_umur.setColorFilter(color);
                    tv_error_umur.setVisibility(View.GONE);
                    break;
                case R.id.ac_hubungan:
                    if (isTextWidgetEmpty(ac_hubungan)) {
                        color = ContextCompat.getColor(getBaseContext(), R.color.orange);
                    } else {
                        color = ContextCompat.getColor(getBaseContext(), R.color.green);
                    }
                    iv_circle_hubungan.setColorFilter(color);
                    tv_error_hubungan.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }
    }


    @BindView(R.id.tv_tittle_form)
    TextView tv_tittle_form;
    @BindView(R.id.et_nama)
    EditText et_nama;
    @BindView(R.id.et_tl)
    EditText et_tl;
    @BindView(R.id.et_umur)
    EditText et_umur;
    @BindView(R.id.ac_hubungan)
    AutoCompleteTextView ac_hubungan;
    @BindView(R.id.btn_ok)
    Button btn_ok;
    @BindView(R.id.iv_circle_nama)
    ImageView iv_circle_nama;
    @BindView(R.id.iv_circle_tl)
    ImageView iv_circle_tl;
    @BindView(R.id.iv_circle_umur)
    ImageView iv_circle_umur;
    @BindView(R.id.iv_circle_hubungan)
    ImageView iv_circle_hubungan;
    @BindView(R.id.tv_error_nama)
    TextView tv_error_nama;
    @BindView(R.id.tv_error_tl)
    TextView tv_error_tl;
    @BindView(R.id.tv_error_umur)
    TextView tv_error_umur;
    @BindView(R.id.tv_error_hubungan)
    TextView tv_error_hubungan;


}
