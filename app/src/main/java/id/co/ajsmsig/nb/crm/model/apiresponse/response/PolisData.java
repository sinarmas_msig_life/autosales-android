
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PolisData {

    @SerializedName("LSSA_ID")
    @Expose
    private Integer lSSAID;
    @SerializedName("MSPS_DESC")
    @Expose
    private String mSPSDESC;
    @SerializedName("MSPS_DATE")
    @Expose
    private String mSPSDATE;
    @SerializedName("STATUS_POLIS")
    @Expose
    private String sTATUSPOLIS;
    @SerializedName("STATUS_ACCEPT")
    @Expose
    private String sTATUSACCEPT;
    @SerializedName("REG_SPAJ")
    @Expose
    private String rEGSPAJ;
    @SerializedName("LSSP_ID")
    @Expose
    private Integer lSSPID;

    public Integer getLSSAID() {
        return lSSAID;
    }

    public void setLSSAID(Integer lSSAID) {
        this.lSSAID = lSSAID;
    }

    public String getMSPSDESC() {
        return mSPSDESC;
    }

    public void setMSPSDESC(String mSPSDESC) {
        this.mSPSDESC = mSPSDESC;
    }

    public String getMSPSDATE() {
        return mSPSDATE;
    }

    public void setMSPSDATE(String mSPSDATE) {
        this.mSPSDATE = mSPSDATE;
    }

    public String getSTATUSPOLIS() {
        return sTATUSPOLIS;
    }

    public void setSTATUSPOLIS(String sTATUSPOLIS) {
        this.sTATUSPOLIS = sTATUSPOLIS;
    }

    public String getSTATUSACCEPT() {
        return sTATUSACCEPT;
    }

    public void setSTATUSACCEPT(String sTATUSACCEPT) {
        this.sTATUSACCEPT = sTATUSACCEPT;
    }

    public String getREGSPAJ() {
        return rEGSPAJ;
    }

    public void setREGSPAJ(String rEGSPAJ) {
        this.rEGSPAJ = rEGSPAJ;
    }

    public Integer getLSSPID() {
        return lSSPID;
    }

    public void setLSSPID(Integer lSSPID) {
        this.lSSPID = lSSPID;
    }

}
