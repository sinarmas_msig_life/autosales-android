package id.co.ajsmsig.nb.crm.adapter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.model.AgencySearchModel;
import id.co.ajsmsig.nb.crm.model.C_SearchLeadModel;

/**
 * Created by Fajar.Azhar on 09/03/2018.
 */

public class AgencySearchLeadAdapter extends RecyclerView.Adapter<AgencySearchLeadAdapter.LeadVH> {
    private List<AgencySearchModel> leadList;
    private ClickListener clickListener;

    public AgencySearchLeadAdapter(List<AgencySearchModel> leadList) {
        this.leadList = leadList;
    }

    public class LeadVH extends RecyclerView.ViewHolder {
        private ImageView imgProfile;
        private TextView tvName;
        private TextView tvAge;
        private ConstraintLayout layoutLeads;

        public LeadVH(View view) {
            super(view);
            layoutLeads = view.findViewById(R.id.layoutLeads);
            imgProfile = view.findViewById(R.id.imgProfile);
            tvName = view.findViewById(R.id.tvName);
            tvAge = view.findViewById(R.id.tvAge);
        }

    }
    @Override
    public LeadVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_agency_search_lead, parent, false);
        return new LeadVH(itemView);
    }

    @Override
    public void onBindViewHolder(final LeadVH holder, int position) {
        AgencySearchModel model = leadList.get(position);

        if (model.getLeadAge() > 0) {
            holder.tvAge.setVisibility(View.VISIBLE);
            String customerAge = String.valueOf(model.getLeadAge()).concat(" tahun");
            holder.tvAge.setText(customerAge);
        } else {
            holder.tvAge.setVisibility(View.INVISIBLE);
        }


        holder.tvName.setText(model.getLeadName());

        holder.layoutLeads.setOnClickListener(view -> clickListener.onLeadSelected(holder.getAdapterPosition(),leadList));
    }

    @Override
    public int getItemCount() {
        return leadList.size();
    }

    public void setClickListener (ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onLeadSelected(int position, List<AgencySearchModel> leads);
    }
}
