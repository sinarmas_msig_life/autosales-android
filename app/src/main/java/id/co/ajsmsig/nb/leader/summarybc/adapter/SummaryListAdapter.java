package id.co.ajsmsig.nb.leader.summarybc.adapter;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.databinding.NetworkItemBinding;
import id.co.ajsmsig.nb.leader.summarybc.utils.NetworkState;
import id.co.ajsmsig.nb.leader.summarybc.model.SummaryData;

public class SummaryListAdapter extends PagedListAdapter<SummaryData, RecyclerView.ViewHolder> {
    private static final int TYPE_PROGRESS = 0;
    private static final int TYPE_ITEM = 1;

    private Context context;
    private NetworkState networkState;

    private SummaryData summaryData;

    public SummaryListAdapter(Context context) {
        super(SummaryData.DIFF_CALLBACK);
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_PROGRESS) {
            NetworkItemBinding headerBinding = NetworkItemBinding.inflate(layoutInflater, parent, false);
            NetworkStateItemViewHolder viewHolder = new NetworkStateItemViewHolder(headerBinding);
            return viewHolder;

        } else {
            View itemBinding = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_summary_report, parent, false);
            RecyclerView.ViewHolder viewHolder = new SummaryItemViewHolder(itemBinding);
            return viewHolder;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SummaryItemViewHolder) {
            ((SummaryItemViewHolder) holder).bind(Objects.requireNonNull(getItem(position)));
        } else {
            ((NetworkStateItemViewHolder) holder).bindView(networkState);
        }
    }

    private boolean hasExtraRow() {
        return networkState != null && networkState != NetworkState.LOADED;
    }

    @Override
    public int getItemViewType(int position) {
        if (hasExtraRow() && position == getItemCount() - 1) {
            return TYPE_PROGRESS;
        } else {
            return TYPE_ITEM;
        }
    }

    public void setNetworkState(NetworkState newNetworkState) {
        NetworkState previousState = this.networkState;
        boolean previousExtraRow = hasExtraRow();
        this.networkState = newNetworkState;
        boolean newExtraRow = hasExtraRow();
        if (previousExtraRow != newExtraRow) {
            if (previousExtraRow) {
                notifyItemRemoved(getItemCount());
            } else {
                notifyItemInserted(getItemCount());
            }
        } else if (newExtraRow && previousState != newNetworkState) {
            notifyItemChanged(getItemCount() - 1);
        }
    }

    public class SummaryItemViewHolder extends RecyclerView.ViewHolder {
        private TextView agentName;
        private TextView submit, inforce, meetPre, closing;

        public SummaryItemViewHolder(View itemView) {
            super(itemView);
            agentName = itemView.findViewById(R.id.tvNameAgentSummary);
            submit = itemView.findViewById(R.id.tvSubmitSummary);
            inforce = itemView.findViewById(R.id.tvInforceSummary);
            meetPre = itemView.findViewById(R.id.tvMeetAndPre);
            closing = itemView.findViewById(R.id.tvClosingSummary);
        }
        public void bind(SummaryData item) {
            agentName.setText(item.getAgentName());
            submit.setText(String.valueOf(item.getSubmit()));
            inforce.setText(String.valueOf(item.getInforce()));
            meetPre.setText(String.valueOf(item.getMeetAndPresentation()));
            closing.setText(String.valueOf(item.getClosing()));
        }
    }

    public class NetworkStateItemViewHolder extends RecyclerView.ViewHolder {
        private NetworkItemBinding binding;

        public NetworkStateItemViewHolder(NetworkItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindView(NetworkState networkState) {
            if (networkState != null && networkState.getStatus() == NetworkState.Status.RUNNING) {
                binding.progressBar.setVisibility(View.VISIBLE);
            } else {
                binding.progressBar.setVisibility(View.VISIBLE);
            }

            if (networkState != null && networkState.getStatus() == NetworkState.Status.FAILED) {
/*                binding.errorMsg.setVisibility(View.GONE);
                binding.errorMsg.setText(networkState.getMsg());*/
            } else {
//                binding.errorMsg.setVisibility(View.GONE);
            }
        }
    }

}
