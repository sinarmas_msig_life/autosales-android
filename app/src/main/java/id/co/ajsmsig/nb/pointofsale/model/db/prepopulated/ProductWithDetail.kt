package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated

import android.arch.persistence.room.*
import id.co.ajsmsig.nb.pointofsale.model.db.dynamic.Product

/**
 * Created by andreyyoshuamanik on 02/03/18.
 */
class ProductWithDetail{

        @Embedded
        var product: Product? = null

        @Relation(entity = ProductDetail::class, parentColumn = "lsbsId", entityColumn = "productId")
        var productDetails: List<ProductDetail>? = null
}