
package id.co.ajsmsig.nb.leader.subordinate;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetJenisChannelResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public GetJenisChannelResponse() {
    }

    /**
     * 
     * @param message
     * @param error
     * @param data
     */
    public GetJenisChannelResponse(String message, boolean error, List<Data> data) {
        super();
        this.message = message;
        this.error = error;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

}
