package id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.lifecycle.LiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page


/**
 * Created by andreyyoshuamanik on 23/02/18.
 */
@Dao
interface PageDao {
    @Query("SELECT * from Page")
    fun loadAllPages(): LiveData<List<Page>>


    @Query("SELECT * from Page where id = :id limit 1")
    fun loadPage(id: Int): LiveData<Page>

    @Query("SELECT * from Page where type like '%' || :type || '%' limit 1")
    fun loadPage(type: String?): LiveData<Page>
}