package id.co.ajsmsig.nb.prop.method.calculateIlustration;

import android.content.Context;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.method.util.ArrUtil;
import id.co.ajsmsig.nb.prop.method.util.CommonUtil;
import id.co.ajsmsig.nb.prop.method.util.FormatNumber;
import id.co.ajsmsig.nb.prop.method.util.ProposalStringFormatter;
import id.co.ajsmsig.nb.prop.model.hardCode.Proposal;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.IllustrationResultVO;
import id.co.ajsmsig.nb.prop.model.modelCalcIllustration.S_biaya;


public class IllustrationFormula190 extends IllustrationFormula {

    IllustrationFormula190(Context context) {
        super(context);
    }

    @Override
    public HashMap<String, Object> getIllustrationResult(String no_proposal) {
        IllustrationResultVO result = new IllustrationResultVO();
        ArrayList<LinkedHashMap<String, String>> mapList = new ArrayList<>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        ArrayList<HashMap<String, Object>> topupDrawList = getSelect().selectTopupAndWithdrawList(no_proposal);
        Integer defaultTopupDrawListSize = 50;// Di eproposal topup list selalu 50 walaupun tidak diisi
        String premiumTotal;
        String topup;
        String draw;

        int li_ke = 0, li_bagi = 1000, li_hal = 3;
        double[] ldec_bak;
        double ldec_bawal = 100000;
        double[] ldec_man_non = new double[2 + 1];
        double ldec_bak_tu = 0.05;
        double ldec_bak_tut = 0.05;
        double ldec_akuisisi;
        double ldec_premi_invest;
        double[][] ldec_hasil_invest = new double[5 + 1][3 + 1];
        double[] ldec_tarik = { Proposal.DUMMY_ZERO, 0, 0, 0, 0, 0 };
        double ldec_wdraw;
        double[] ldec_premi_bulan = new double[12 + 1];
        double[] premi_bulan_sp = new double[12 + 1];
        double  ldec_premi_setahun_sp = 0;
        double ldec_topup;
        double ldec_bass;
        double[][] ldec_bunga = new double[5 + 1][3 + 1];
        double[] ldec_bunga_avg = new double[3 + 1]; //= {0.06, 0.1, 0.08, 0.18, 0.12, 0.25} //{0.09, 0.06, 0.11, 0.15, 0.165, 0.25}  //fixed:0.09, 0.11, 0.15 (0.165); dynamic:0.08, 0.11, 0.18 (0.195)
        double ldec_fee = 0.020075;
        double ldec_coi = 0, ldec_mfc, ldec_sc, ldec_cost, ldec_man_celaka, ldec_temp = 0; //, ldec_man[10+1] = {1, 1, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7}
        double specialOfferDouble = 0;
        boolean[] lb_minus = { Proposal.DUMMY_FALSE, false, false, false };
        boolean lb_rider = false;
        String ls_sy = "", ls_temp; //ls_dpp = ' dari Premi Pokok', ls_sy = ''
        S_biaya lstr;
  //n_riders ln_riders
        double ldec_manfaat, ldec_premi_setahun = 0, ldec_coi24 = 0;
  ////monthly fix cost
        ldec_mfc = 27500;
        
        Integer umur_tt = 0, bisnis_id = 0, bisnis_no = 0, ins_per = 0, lscb_id = 0, thn_cuti_premi = 0;
        double premi = 0, premi_komb = 0, premi_pokok = 0, up = 0;
        String kondisi_layak_jual = null, lku_id = "00";
        HashMap<String, Object> dataProposal = getSelect().selectDataProposal(no_proposal);
        if(!CommonUtil.isEmpty(dataProposal)) {
            bisnis_id = ((BigDecimal) dataProposal.get("LSBS_ID")).intValue();
            bisnis_no = ((BigDecimal) dataProposal.get("LSDBS_NUMBER")).intValue();
            umur_tt =  Integer.valueOf(dataProposal.get("UMUR_TT").toString());
            premi =  Double.valueOf(dataProposal.get("PREMI").toString());
            premi_komb =  Double.valueOf(dataProposal.get("PREMI_KOMB").toString());
            premi_pokok =  Double.valueOf(dataProposal.get("PREMI_POKOK").toString());
            up =  Double.valueOf(dataProposal.get("UP").toString());
            ins_per =  Integer.valueOf(dataProposal.get("THN_MASA_KONTRAK").toString());
            kondisi_layak_jual = (String) dataProposal.get("NAME_ELIGIBLE");
            lscb_id = Integer.valueOf(dataProposal.get("LSCB_ID").toString());
            lku_id = dataProposal.get("LKU_ID").toString();
            thn_cuti_premi =  Integer.valueOf(dataProposal.get("THN_CUTI_PREMI").toString());
        }
        
        Integer pset_id = getSelect().selectPsetIdProposal(no_proposal);
        
        if( Proposal.CUR_USD_CD.equals( lku_id ) )
        {
            ldec_mfc = 5;
            li_bagi = 1;
        }

        for( int i = 1; i <= 12; i++ )
        {
            ldec_premi_bulan[ i ] = 0;
            premi_bulan_sp[ i ] = 0;
            if( i == 1 ) {
              ldec_premi_bulan[ i ] = premi;
              premi_bulan_sp[i] = premi_pokok;
            }
            if( Proposal.PAY_MODE_CD_TRIWULANAN == lscb_id )
            {
                if( i == 4 || i == 7 || i == 10 ) 
                  {
                      ldec_premi_bulan[ i ] = premi;
                      premi_bulan_sp[i] = premi_pokok;
                  }
            }
            else if( Proposal.PAY_MODE_CD_SEMESTERAN == lscb_id )
            {
                if( i == 7 ) 
                {
                  ldec_premi_bulan[ i ] = premi;
                  premi_bulan_sp[i] = premi_pokok;
                }
            }
            else if( Proposal.PAY_MODE_CD_BULANAN == lscb_id )
            {
                ldec_premi_bulan[ i ] = premi;
                premi_bulan_sp[i] = premi_pokok;
            }
            ldec_premi_setahun += ldec_premi_bulan[ i ];
            ldec_premi_setahun_sp += premi_bulan_sp[ i ];
        }

        for( int i = 1; i <= 3; i++ )
        {
            ldec_hasil_invest[ 1 ][ i ] = 0;
        }

        ldec_manfaat = up;
  ////Biaya Akuisisi & asumsi penarikan (1 tarik, 1-nya tdk tarik)

        lstr = getBiaya(bisnis_id, bisnis_no, no_proposal);
        ldec_bak = lstr.bak;
        ldec_bunga = lstr.bunga;
        
        ldec_bunga_avg = getSelect().selectBungaAvg( no_proposal );

        double[] np = new double[4];
        double[] celaka = new double[4];
        
        int j;
                
        for( int i = 1; i <= ins_per; i++ )
        {
            //surrender charge
            ldec_sc = 0;
            ldec_wdraw = 0;
            ldec_topup = 0;
            ldec_akuisisi = 0;
            // compute tabel Ilustrasi Perkembangan Dana
            ldec_coi = getLdec_coi(dataProposal, i);
            if( i <= ArrUtil.upperBound( lstr.tarik ) ) ldec_wdraw = lstr.tarik[ i ];
            if( i <= ArrUtil.upperBound( lstr.topup ) ) ldec_topup = lstr.topup[ i ];
            if( ArrUtil.upperBound( ldec_bak ) > i ) ldec_akuisisi = ldec_bak[ i ];
            
            if( lscb_id != 0 ){
                if( i == 1 ){
                    ldec_temp = ldec_coi;
                    ldec_coi = 0;
                    ldec_mfc = 0;
                // SE No.101/AJS-SE/VIII/2014 TTG KET.PEMOTONGAN BIAYA ADMINISTRASI DAN BIAYA ASURANSI TAHUN PERTAMA UNTUK PRODUK SMiLe LINK 99 SERIES                  
                //}else if( i == 2 ){   
                }else if( i >= 2 && i <= 5){
                    double kurs = 0.0;
                     if( Proposal.CUR_USD_CD.equals( lku_id ) )
                       {
                         kurs = 5;
                       }else{
                         kurs = 27500;
                       }
                    ldec_coi24 = (ldec_temp + kurs) / 4;
                }else{
                    ldec_coi24 = 0;
                }
                
                if( i >= 2  ){
                    ldec_mfc = 27500;
                    if( Proposal.CUR_USD_CD.equals( lku_id ) )
                      {
                        ldec_mfc = 5;
                      }
                }
            }

            ldec_cost = ( ldec_coi + ldec_mfc + ldec_coi24 );

            for( int k = 1; k <= 3; k++ )
            {
                for( int li_bulan = 1; li_bulan <= 12; li_bulan++ )
                {
                    ldec_premi_invest = 0;
                    if( i <= thn_cuti_premi )
                    {
                        ldec_premi_invest = ( ( ldec_premi_bulan[ li_bulan ] * ( premi_komb / 100 ) ) * ( 1 - ldec_akuisisi ) );
                        ldec_premi_invest += ( ( ldec_premi_bulan[ li_bulan ] * ( 100 - premi_komb ) / 100 ) ) * ( 1 - ldec_bak_tu );  //topup berkala
                    }
                    if( li_bulan == 1 ) ldec_premi_invest += ( ldec_topup * ( 1 - ldec_bak_tut ) );  //topup tunggal
                    double copy = ldec_hasil_invest[ 1 ][ k ];

                    ldec_hasil_invest[ 1 ][ k ] = ( ldec_premi_invest + copy - ldec_cost ) * Math.pow( ( 1 + ldec_bunga_avg[ k ] ), ( ( double ) 1 / 12 ) );
                }

                ldec_hasil_invest[ 1 ][ k ] -= ( ldec_wdraw * ( 1 + ldec_sc ) );

                if((bisnis_no < 5 || bisnis_no > 8) &&  i > 1) {
                    if( ldec_hasil_invest[ 1 ][ k ] <= 0 && ( ( umur_tt < 50 && i <= 15 ) || ( umur_tt >= 50 && i <= 10 ) ) )
                    {
                        lb_minus[ k ] = true;
                    }
                } else if( bisnis_no.equals(5) || bisnis_no.equals(6) ){
                    if( i == 1 ){
                        if( ldec_hasil_invest[ 1 ][ k ] < 0 ){
                            lb_minus[ k ] = true;
                        }
                    }else{
                        if(k >= 2){
                            if( ldec_hasil_invest[ 1 ][ k ] < 0 ){
                                lb_minus[ 1 ] = true;
                            }
                        }
                    }
                } else if( bisnis_no.equals(7) || bisnis_no.equals(8) ) {
                    if( ldec_hasil_invest[ 1 ][ k ] < 0 &&  ( i <= 10 ) ) {
                        lb_minus[k] = true;
                    }
                }
            }
          
            j = umur_tt + i;
            
            HashMap<String, Object> param = new HashMap<String, Object>();
            param.put( "jenis", 0 );
            param.put("pset_id", pset_id);
            
            ArrayList<HashMap<String, Object>> resultList;
            resultList = getSelect().selectIllustrationShow( param );
            int year_i =     Integer.valueOf(resultList.get(0).get("YEAR").toString());
            param.put( "jenis", 1 ); 
            resultList = getSelect().selectIllustrationShow( param );
            
            int[] resultArr = new int[resultList.size()];
            
            for(int l = 0; l < resultList.size(); l++) {
                 resultArr[l] = Integer.valueOf(resultList.get(l).get("YEAR").toString());
            }
            
            boolean year_j = false;
            for(int l = 0; l < resultArr.length; l++) {                  
                 if(resultArr[l] == j){
                     year_j = true;
                     break;
                 }
            }  
            
            if( i <= year_i || year_j )
            {
                for( int k = 1; k <= 3; k++ )
                {
                    np[ k ] = FormatNumber.round( ldec_hasil_invest[ 1 ][ k ] / li_bagi, 0, lku_id );
                    celaka[ k ] = FormatNumber.round( ( ldec_hasil_invest[ 1 ][ k ] + ldec_manfaat ) / li_bagi, 0, lku_id );
                }
                if( i <= thn_cuti_premi )
                {
                    premiumTotal = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( ldec_premi_setahun / li_bagi );
                }
                else
                {
                    premiumTotal = "";
                }

                if( i < defaultTopupDrawListSize )
                {
                    // why ( i - 1 )? becoz index in Java start from 0, not like PB programming language
//                    topupDrawVO = topupDrawVOList.get( i - 1 );
                    topup = "0";
                    draw = "0";
//                    if( topupDrawVO.getYearFlag() != null && topupDrawVO.getYearFlag().equals("true") ){
//                          topup = editorUtil.convertToString( topupDrawVO.getTopupAmount().divide( new BigDecimal( "1000" ) ) );
//                          draw = editorUtil.convertToString( topupDrawVO.getDrawAmount().divide( new BigDecimal( "1000" ) ) );
//                    }
                    for(HashMap<String, Object> topupDraw : topupDrawList) {
                        Integer thn_ke = ((BigDecimal) topupDraw.get("THN_KE")).intValue();
                        if(i == thn_ke) {
                            BigDecimal topupAmount = (BigDecimal) topupDraw.get("TOPUP");
                            BigDecimal drawAmount = (BigDecimal) topupDraw.get("TARIK");
                            
                            topup = ProposalStringFormatter.convertToString(topupAmount.divide(new BigDecimal("1000")));
                            draw = ProposalStringFormatter.convertToString(drawAmount.divide(new BigDecimal("1000")));
                        }
                    }
                    
                    if( "0".equals( topup ) || "0.00".equals( topup ) ) topup = "0";
                    if( "0".equals( draw ) || "0.00".equals( draw ) ) draw = "0";
                }
                else
                {
                    topup = "0";
                    draw = "0";
                }
                LinkedHashMap<String, String> map1 = new LinkedHashMap<>();
                
                if( i == 1 ){
                  String specialOffer = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( specialOfferDouble/1000 ) );
//                  map1.put( "specialOffer", specialOffer );
                }else{
//                    map1.put( "specialOffer", "" );
                }
                String valueLow = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 1 ] ) );
                String valueMid = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 2 ] ) );
                String valueHi = ProposalStringFormatter.convertToStringWithoutCentAndNillIfNegative( new BigDecimal( np[ 3 ] ) );

                String benefitLow = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 1 ], np[ 1 ] );
                String benefitMid = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 2 ], np[ 2 ] );
                String benefitHi = ProposalStringFormatter.convertToStringWithoutCentAndSetNill( celaka[ 3 ], np[ 3 ] );

                if( i == 1 ){
                    benefitLow =  ProposalStringFormatter.convertToStringWithoutCent( ldec_manfaat / li_bagi);
                    benefitMid =  ProposalStringFormatter.convertToStringWithoutCent( ldec_manfaat / li_bagi);
                    benefitHi =  ProposalStringFormatter.convertToStringWithoutCent( ldec_manfaat / li_bagi);
                }
              
                map1.put( "yearNo", ProposalStringFormatter.convertToString( i ) );
                map1.put( "insuredAge", ProposalStringFormatter.convertToString( umur_tt + i ) );
                map1.put( "premiumTotal", premiumTotal );
                map1.put( "topupAssumption", topup );
                map1.put( "drawAssumption", draw );
                map1.put( "valueLow", valueLow );
                map1.put( "valueMid", valueMid );
                map1.put( "valueHi", valueHi );
                map1.put( "benefitLow", benefitLow );
                map1.put( "benefitMid", benefitMid );
                map1.put( "benefitHi", benefitHi );
                mapList.add( map1 );

            }
        }

        LinkedHashMap<String, String> map1 = new LinkedHashMap<>();
        map1.put( "yearNo", getContext().getString(R.string.Thn_Polis_Ke));
        map1.put( "insuredAge", getContext().getString(R.string.Usia_Ttg) );
        map1.put( "premiumTotal", getContext().getString(R.string.Total_Premi));
        map1.put( "topupAssumption", getContext().getString(R.string.Asumsi_Top_up) );
        map1.put( "drawAssumption",  getContext().getString(R.string.Asumsi_Penarikan) );
        map1.put( "valueLow",getContext().getString(R.string.ENP) + " " + getContext().getString(R.string.Rendah) );
        map1.put( "valueMid",getContext().getString(R.string.ENP) + " " + getContext().getString(R.string.Sedang) );
        map1.put( "valueHi", getContext().getString(R.string.ENP) + " " +getContext().getString(R.string.Tinggi) );
        map1.put( "benefitLow", getContext().getString(R.string.EMM) + " " +getContext().getString(R.string.Rendah) );
        map1.put( "benefitMid",getContext().getString(R.string.EMM) + " " + getContext().getString(R.string.Sedang) );
        map1.put( "benefitHi",getContext().getString(R.string.EMM) + " " + getContext().getString(R.string.Tinggi) );
        mapList.add(0, map1 );

        result.setValidityMsg( lb_minus[ 1 ] ? "Maaf, Proposal yang Anda buat Tidak Layak Jual." : "" );
        result.setIllustrationList( mapList );
        map.put("Illustration1", result);
        
        return map;
    }

}
