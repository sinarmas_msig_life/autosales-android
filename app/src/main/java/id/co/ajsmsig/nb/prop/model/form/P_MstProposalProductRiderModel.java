package id.co.ajsmsig.nb.prop.model.form;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class P_MstProposalProductRiderModel implements Parcelable {
    private String no_proposal_tab;
    private String no_proposal;
    private Integer lsbs_id;
    private Integer lsdbs_number;
    private String lku_id;
    private String premi;
    private String up;
    private Integer persen_up;
    private Integer kelas;
    private Integer jml_unit;
    private Integer bulan_ke;
    private Integer flag_packet; // baru di tambah
    private ArrayList<P_MstProposalProductPesertaModel> p_mstProposalProductPesertaModels;



    public P_MstProposalProductRiderModel() {
    }

    protected P_MstProposalProductRiderModel(Parcel in) {
        no_proposal_tab = in.readString();
        no_proposal = in.readString();
        lsbs_id = (Integer) in.readValue(Integer.class.getClassLoader());
        lsdbs_number = (Integer) in.readValue(Integer.class.getClassLoader());
        lku_id = in.readString();
        premi =  in.readString();
        up =  in.readString();
        persen_up = (Integer) in.readValue(Integer.class.getClassLoader());
        kelas = (Integer) in.readValue(Integer.class.getClassLoader());
        jml_unit = (Integer) in.readValue(Integer.class.getClassLoader());
        bulan_ke = (Integer) in.readValue(Integer.class.getClassLoader());
        flag_packet = (Integer) in.readValue(Integer.class.getClassLoader());
        p_mstProposalProductPesertaModels = in.createTypedArrayList(P_MstProposalProductPesertaModel.CREATOR);
    }

    public static final Creator<P_MstProposalProductRiderModel> CREATOR = new Creator<P_MstProposalProductRiderModel>() {
        @Override
        public P_MstProposalProductRiderModel createFromParcel(Parcel in) {
            return new P_MstProposalProductRiderModel(in);
        }

        @Override
        public P_MstProposalProductRiderModel[] newArray(int size) {
            return new P_MstProposalProductRiderModel[size];
        }
    };

    public String getNo_proposal_tab() {
        return no_proposal_tab;
    }

    public void setNo_proposal_tab(String no_proposal_tab) {
        this.no_proposal_tab = no_proposal_tab;
    }

    public String getNo_proposal() {
        return no_proposal;
    }

    public void setNo_proposal(String no_proposal) {
        this.no_proposal = no_proposal;
    }

    public Integer getLsbs_id() {
        return lsbs_id;
    }

    public void setLsbs_id(Integer lsbs_id) {
        this.lsbs_id = lsbs_id;
    }

    public Integer getLsdbs_number() {
        return lsdbs_number;
    }

    public void setLsdbs_number(Integer lsdbs_number) {
        this.lsdbs_number = lsdbs_number;
    }

    public String getLku_id() {
        return lku_id;
    }

    public void setLku_id(String lku_id) {
        this.lku_id = lku_id;
    }

    public String getPremi() {
        return premi;
    }

    public void setPremi(String premi) {
        this.premi = premi;
    }

    public String getUp() {
        return up;
    }

    public void setUp(String up) {
        this.up = up;
    }

    public Integer getPersen_up() {
        return persen_up;
    }

    public void setPersen_up(Integer persen_up) {
        this.persen_up = persen_up;
    }

    public Integer getKelas() {
        return kelas;
    }

    public void setKelas(Integer kelas) {
        this.kelas = kelas;
    }

    public Integer getJml_unit() {
        return jml_unit;
    }

    public void setJml_unit(Integer jml_unit) {
        this.jml_unit = jml_unit;
    }

    public Integer getBulan_ke() {
        return bulan_ke;
    }

    public void setBulan_ke(Integer bulan_ke) {
        this.bulan_ke = bulan_ke;
    }

    public Integer getFlag_packet() {
        return flag_packet;
    }

    public void setFlag_packet(Integer flag_packet) {
        this.flag_packet = flag_packet;
    }

    public ArrayList<P_MstProposalProductPesertaModel> getP_mstProposalProductPesertaModels() {
        return p_mstProposalProductPesertaModels;
    }

    public void setP_mstProposalProductPesertaModels(ArrayList<P_MstProposalProductPesertaModel> p_mstProposalProductPesertaModels) {
        this.p_mstProposalProductPesertaModels = p_mstProposalProductPesertaModels;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(no_proposal_tab);
        dest.writeString(no_proposal);
        dest.writeValue(lsbs_id);
        dest.writeValue(lsdbs_number);
        dest.writeString(lku_id);
        dest.writeString(premi);
        dest.writeString(up);
        dest.writeValue(persen_up);
        dest.writeValue(kelas);
        dest.writeValue(jml_unit);
        dest.writeValue(bulan_ke);
        dest.writeValue(flag_packet);
        dest.writeTypedList(p_mstProposalProductPesertaModels);
    }

}
