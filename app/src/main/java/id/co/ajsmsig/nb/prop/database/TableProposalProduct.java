package id.co.ajsmsig.nb.prop.database;
/*
 *Created by faiz_f on 10/05/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductModel;

public class TableProposalProduct {
    private Context context;

    public TableProposalProduct(Context context) {
        this.context = context;
    }

    public ContentValues getContentValues(P_MstProposalProductModel p_mstProposalProductModel){
        ContentValues cv = new ContentValues();
        cv.put(context.getString(R.string.NO_PROPOSAL_TAB), p_mstProposalProductModel.getNo_proposal_tab());
        cv.put(context.getString(R.string.NO_PROPOSAL), p_mstProposalProductModel.getNo_proposal());
        cv.put(context.getString(R.string.LSBS_ID), p_mstProposalProductModel.getLsbs_id());
        cv.put(context.getString(R.string.LSDBS_NUMBER), p_mstProposalProductModel.getLsdbs_number());
        cv.put(context.getString(R.string.LKU_ID), p_mstProposalProductModel.getLku_id());
        cv.put(context.getString(R.string.GRP_PRODUCT_NAME), p_mstProposalProductModel.getGrp_product_name());
        cv.put(context.getString(R.string.PREMI), p_mstProposalProductModel.getPremi());
        cv.put(context.getString(R.string.PREMI_KOMB), p_mstProposalProductModel.getPremi_komb());
        cv.put(context.getString(R.string.PREMI_POKOK), p_mstProposalProductModel.getPremi_pokok());
        cv.put(context.getString(R.string.PREMI_TOPUP), p_mstProposalProductModel.getPremi_topup());
        cv.put(context.getString(R.string.UP), p_mstProposalProductModel.getUp());
        cv.put(context.getString(R.string.CARA_BAYAR), p_mstProposalProductModel.getCara_bayar());
        cv.put(context.getString(R.string.THN_CUTI_PREMI), p_mstProposalProductModel.getThn_cuti_premi());
        cv.put(context.getString(R.string.THN_MASA_KONTRAK), p_mstProposalProductModel.getThn_masa_kontrak());
        cv.put(context.getString(R.string.THN_LAMA_BAYAR), p_mstProposalProductModel.getThn_lama_bayar());
        cv.put(context.getString(R.string.INV_FIX), p_mstProposalProductModel.getInv_fix());
        cv.put(context.getString(R.string.INV_DYNAMIC), p_mstProposalProductModel.getInv_dynamic());
        cv.put(context.getString(R.string.INV_AGGRESSIVE), p_mstProposalProductModel.getInv_aggressive());
        cv.put(context.getString(R.string.LJI_FIX), p_mstProposalProductModel.getLji_fix());
        cv.put(context.getString(R.string.LJI_DYNAMIC), p_mstProposalProductModel.getLji_dynamic());
        cv.put(context.getString(R.string.LJI_AGGRESIVE), p_mstProposalProductModel.getLji_aggresive());

        return cv;
    }

    public P_MstProposalProductModel getObject(Cursor cursor){
        P_MstProposalProductModel p_mstProposalProductModel = new P_MstProposalProductModel();
        if (cursor != null && cursor.getCount()>0) {
            cursor.moveToFirst();
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB)))) {
                p_mstProposalProductModel.setNo_proposal_tab(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL_TAB))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL)))) {
                p_mstProposalProductModel.setNo_proposal(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.NO_PROPOSAL))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LSBS_ID)))) {
                p_mstProposalProductModel.setLsbs_id(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LSBS_ID))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LSDBS_NUMBER)))) {
                p_mstProposalProductModel.setLsdbs_number(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.LSDBS_NUMBER))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LKU_ID)))) {
                p_mstProposalProductModel.setLku_id(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.LKU_ID))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.GRP_PRODUCT_NAME)))) {
                p_mstProposalProductModel.setGrp_product_name(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.GRP_PRODUCT_NAME))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.PREMI)))) {
                p_mstProposalProductModel.setPremi(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.PREMI))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.PREMI_KOMB)))) {
                p_mstProposalProductModel.setPremi_komb(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.PREMI_KOMB))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.PREMI_POKOK)))) {
                p_mstProposalProductModel.setPremi_pokok(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.PREMI_POKOK))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.PREMI_TOPUP)))) {
                p_mstProposalProductModel.setPremi_topup(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.PREMI_TOPUP))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.UP)))) {
                p_mstProposalProductModel.setUp(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.UP))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.CARA_BAYAR)))) {
                p_mstProposalProductModel.setCara_bayar(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.CARA_BAYAR))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.THN_CUTI_PREMI)))) {
                p_mstProposalProductModel.setThn_cuti_premi(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.THN_CUTI_PREMI))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.THN_MASA_KONTRAK)))) {
                p_mstProposalProductModel.setThn_masa_kontrak(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.THN_MASA_KONTRAK))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.THN_LAMA_BAYAR)))) {
                p_mstProposalProductModel.setThn_lama_bayar(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.THN_LAMA_BAYAR))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.INV_FIX)))) {
                p_mstProposalProductModel.setInv_fix(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.INV_FIX))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.INV_DYNAMIC)))) {
                p_mstProposalProductModel.setInv_dynamic(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.INV_DYNAMIC))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.INV_AGGRESSIVE)))) {
                p_mstProposalProductModel.setInv_aggressive(cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.INV_AGGRESSIVE))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LJI_FIX)))) {
                p_mstProposalProductModel.setLji_fix(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.LJI_FIX))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LJI_DYNAMIC)))) {
                p_mstProposalProductModel.setLji_dynamic(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.LJI_DYNAMIC))));
            }
            if (!cursor.isNull(cursor.getColumnIndexOrThrow(context.getString(R.string.LJI_AGGRESIVE)))) {
                p_mstProposalProductModel.setLji_aggresive(cursor.getString(cursor.getColumnIndexOrThrow(context.getString(R.string.LJI_AGGRESIVE))));
            }

            // always close the cursor
            cursor.close();
        }

        return p_mstProposalProductModel;
    }
}
