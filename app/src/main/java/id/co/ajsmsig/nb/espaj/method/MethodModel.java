package id.co.ajsmsig.nb.espaj.method;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.Calendar;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.database.MemberTable;
import id.co.ajsmsig.nb.crm.model.MemberModel;
import id.co.ajsmsig.nb.espaj.activity.E_MainActivity;
import id.co.ajsmsig.nb.espaj.database.E_Select;
import id.co.ajsmsig.nb.espaj.database.TableAddRiderUA;
import id.co.ajsmsig.nb.espaj.database.TableAskesUA;
import id.co.ajsmsig.nb.espaj.database.TableCalonPembayarPremi;
import id.co.ajsmsig.nb.espaj.database.TableDKno9_pp;
import id.co.ajsmsig.nb.espaj.database.TableDKno9_tt;
import id.co.ajsmsig.nb.espaj.database.TableDP;
import id.co.ajsmsig.nb.espaj.database.TableDataDitunjukDI;
import id.co.ajsmsig.nb.espaj.database.TableDetilAgen;
import id.co.ajsmsig.nb.espaj.database.TableDetilInvestasi;
import id.co.ajsmsig.nb.espaj.database.TableJnsDanaDI;
import id.co.ajsmsig.nb.espaj.database.TableMSTAnswer;
import id.co.ajsmsig.nb.espaj.database.TableMST_SPAJ;
import id.co.ajsmsig.nb.espaj.database.TablePemegangPolis;
import id.co.ajsmsig.nb.espaj.database.TableProfileResiko;
import id.co.ajsmsig.nb.espaj.database.TableTTD;
import id.co.ajsmsig.nb.espaj.database.TableTertanggung;
import id.co.ajsmsig.nb.espaj.database.TableUsulanAsuransi;
import id.co.ajsmsig.nb.espaj.model.CalonPembayarPremiModel;
import id.co.ajsmsig.nb.espaj.model.DetilAgenModel;
import id.co.ajsmsig.nb.espaj.model.DetilInvestasiModel;
import id.co.ajsmsig.nb.espaj.model.DokumenPendukungModel;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.ModelID;
import id.co.ajsmsig.nb.espaj.model.ModelUpdateQuisioner;
import id.co.ajsmsig.nb.espaj.model.PemegangPolisModel;
import id.co.ajsmsig.nb.espaj.model.ProfileResikoModel;
import id.co.ajsmsig.nb.espaj.model.TertanggungModel;
import id.co.ajsmsig.nb.espaj.model.UsulanAsuransiModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAddRiderUA;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelAskesUA;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_KetKesUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_NO9ppUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDK_NO9ttUQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDataDitunjukDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelFoto;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelJnsDanaDI;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelMst_Answer;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTTD;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTT_NO3UQ;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelTT_NO5UQ;
import id.co.ajsmsig.nb.prop.database.TableProposalProductPeserta;
import id.co.ajsmsig.nb.prop.database.TableProposalProductRider;
import id.co.ajsmsig.nb.prop.database.TableProposalProductTopUp;
import id.co.ajsmsig.nb.prop.database.TableProposalProductULink;
import id.co.ajsmsig.nb.prop.method.QueryUtil;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductPesertaModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductRiderModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductTopUpModel;
import id.co.ajsmsig.nb.prop.model.form.P_MstProposalProductUlinkModel;

import static id.co.ajsmsig.nb.crm.method.StaticMethods.toCalendar;
import static id.co.ajsmsig.nb.crm.method.StaticMethods.toStringDate;

/**
 * Created by eriza on 26/08/2017.
 */

public class MethodModel {

    public static EspajModel getNewModel(E_MainActivity context, EspajModel me) {
        me = new EspajModel();
        me.setPemegangPolisModel(new PemegangPolisModel());
        me.setTertanggungModel(new TertanggungModel());
        me.setUsulanAsuransiModel(new UsulanAsuransiModel());
        me.setDetilInvestasiModel(new DetilInvestasiModel());
        me.setDetilAgenModel(new DetilAgenModel());
        me.setProfileResikoModel(new ProfileResikoModel());
        me.setCalonPembayarPremiModel(new CalonPembayarPremiModel());
        me.setModelUpdateQuisioner(new ModelUpdateQuisioner());
        me.setDokumenPendukungModel(new DokumenPendukungModel());
        me.setModelID(new ModelID());
        me.setValidation(true);
        //UA
        ModelAddRiderUA mau = new ModelAddRiderUA();
        ModelAskesUA maua = new ModelAskesUA();
        ArrayList<ModelAddRiderUA> ListRiderUA = new ArrayList<ModelAddRiderUA>();
        me.setListRiderUA(ListRiderUA);
        ArrayList<ModelAskesUA> ListAskesUA = new ArrayList<ModelAskesUA>();
        me.setListASkesUA(ListAskesUA);
        //DI
        ModelJnsDanaDI mjd = new ModelJnsDanaDI();
        ModelDataDitunjukDI mdd = new ModelDataDitunjukDI();
        ArrayList<ModelJnsDanaDI> ListJenisDanaDI = new ArrayList<ModelJnsDanaDI>();
        me.setListDanaDI(ListJenisDanaDI);
        ArrayList<ModelDataDitunjukDI> ListDataDItunjukDI = new ArrayList<ModelDataDitunjukDI>();
        me.setListManfaatDI(ListDataDItunjukDI);
        //DP
        ModelFoto mf = new ModelFoto();
        ModelTTD mt = new ModelTTD();
        ArrayList<ModelFoto> ListFoto = new ArrayList<ModelFoto>();
        me.getDokumenPendukungModel().setListFoto(ListFoto);
        ArrayList<ModelTTD> ListTTD = new ArrayList<ModelTTD>();
        me.getDokumenPendukungModel().setListTTD(ListTTD);
        //new update
        ArrayList<ModelMst_Answer> listUpdateQuisioner = new ArrayList<ModelMst_Answer>();
        me.getModelUpdateQuisioner().setListQuisioner(listUpdateQuisioner);

        ArrayList<ModelTT_NO3UQ> List_ttno3 = new ArrayList<ModelTT_NO3UQ>();
        me.getModelUpdateQuisioner().setList_ttno3(List_ttno3);

        ArrayList<ModelTT_NO5UQ> List_ttno5 = new ArrayList<ModelTT_NO5UQ>();
        me.getModelUpdateQuisioner().setList_ttno5(List_ttno5);

        ArrayList<ModelDK_NO9ttUQ> List_dkno9tt = new ArrayList<ModelDK_NO9ttUQ>();
        me.getModelUpdateQuisioner().setList_dkno9tt(List_dkno9tt);

        ArrayList<ModelDK_NO9ppUQ> List_dkno9pp = new ArrayList<ModelDK_NO9ppUQ>();
        me.getModelUpdateQuisioner().setList_dkno9pp(List_dkno9pp);

        ArrayList<ModelDK_KetKesUQ> List_dkketkes = new ArrayList<ModelDK_KetKesUQ>();
        me.getModelUpdateQuisioner().setList_dkketkes(List_dkketkes);
        return me;
    }

    public static EspajModel getProposal(E_MainActivity context, QueryUtil queryUtil, String idproposal_tab, EspajModel me) {
        String selection = context.getString(R.string.NO_PROPOSAL_TAB) + " = ?";
        String[] selectionArgs = new String[]{idproposal_tab};
        ArrayList<P_MstProposalProductTopUpModel> p_mstProposalProductTopUpModels;
        ArrayList<P_MstProposalProductUlinkModel> p_mstProposalProductUlinkModels;
        Cursor cursor;

//        getNewModel(context, me);
        Cursor cursorData = queryUtil.query(
                context.getString(R.string.MST_DATA_PROPOSAL),
                null,
                selection,
                selectionArgs,
                null
        );
        if (cursorData != null && cursorData.getCount() > 0) {
            cursorData.moveToFirst();
            me.getModelID().setProposal_tab(cursorData.getString(cursorData.getColumnIndexOrThrow("NO_PROPOSAL_TAB")));
            me.getModelID().setProposal(cursorData.getString(cursorData.getColumnIndexOrThrow("NO_PROPOSAL")));
            /*tarik data lead dari proposal*/
            me.getModelID().setId_crm(cursorData.getString(cursorData.getColumnIndexOrThrow(context.getString(R.string.LEAD_ID))));
            me.getModelID().setId_crm_tab(cursorData.getString(cursorData.getColumnIndexOrThrow(context.getString(R.string.LEAD_TAB_ID))));
            me.getModelID().setFlag_pos(cursorData.getInt(cursorData.getColumnIndexOrThrow(context.getString(R.string.flag_pos))));

            me.getPemegangPolisModel().setNama_pp(cursorData.getString(cursorData.getColumnIndexOrThrow("NAMA_PP")).toUpperCase());

            Calendar calendar = toCalendar(cursorData.getString(cursorData.getColumnIndexOrThrow("TGL_LAHIR_PP")));
            me.getPemegangPolisModel().setTtl_pp(toStringDate(calendar, 5));

            me.getPemegangPolisModel().setUsia_pp(cursorData.getString(cursorData.getColumnIndexOrThrow("UMUR_PP")));
            me.getPemegangPolisModel().setJekel_pp(Integer.parseInt(cursorData.getString(cursorData.getColumnIndexOrThrow("SEX_PP"))));

            me.getModelID().setVa_number(cursorData.getString(cursorData.getColumnIndexOrThrow("VIRTUAL_ACC")));

            if (cursorData.getInt(cursorData.getColumnIndexOrThrow("FLAG_TT_CALON_BAYI")) == 1) {
                me.getTertanggungModel().setNama_tt("CALON BAYI DARI IBU " + cursorData.getString(cursorData.getColumnIndexOrThrow("NAMA_TT")).toUpperCase());
                me.getPemegangPolisModel().setHubungan_pp(4);
                me.getTertanggungModel().setTtl_tt(GetTime.getCurrentDate());
                me.getTertanggungModel().setUsia_tt("0");
            } else {
                me.getTertanggungModel().setNama_tt(cursorData.getString(cursorData.getColumnIndexOrThrow("NAMA_TT")).toUpperCase());

                calendar = toCalendar(cursorData.getString(cursorData.getColumnIndexOrThrow("TGL_LAHIR_TT")));
                me.getTertanggungModel().setTtl_tt(toStringDate(calendar, 5));

                me.getTertanggungModel().setUsia_tt(cursorData.getString(cursorData.getColumnIndexOrThrow("UMUR_TT")));
                me.getTertanggungModel().setJekel_tt(Integer.parseInt(cursorData.getString(cursorData.getColumnIndexOrThrow("SEX_TT"))));

                me.getUsulanAsuransiModel().setPaket_ua(cursorData.getInt(cursorData.getColumnIndexOrThrow("FLAG_PACKET")));
            }
        }
        if (MethodSupport.isPPandTTSamePerson(me)) {
            me.getPemegangPolisModel().setHubungan_pp(1);
        } else {
            me.getPemegangPolisModel().setHubungan_pp(0);
        }
        cursor = queryUtil.query(
                context.getString(R.string.MST_PROPOSAL_PRODUCT),
                null,
                selection,
                selectionArgs,
                null
        );

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            int produk = cursor.getInt(cursor.getColumnIndexOrThrow("LSBS_ID"));
            int sub_produk = cursor.getInt(cursor.getColumnIndexOrThrow("LSDBS_NUMBER"));
            me.getUsulanAsuransiModel().setKd_produk_ua(produk);
            me.getUsulanAsuransiModel().setSub_produk_ua(sub_produk);
            me.getUsulanAsuransiModel().setKurs_up_ua(cursor.getString(cursor.getColumnIndexOrThrow("LKU_ID")));
            if (cursor.getInt(cursor.getColumnIndexOrThrow("PREMI_KOMB")) != 0) {
                String COMB_NAME = new E_Select(context).SelectIDPremiKom(cursor.getInt(cursor.getColumnIndexOrThrow("PREMI_KOMB")));
                me.getUsulanAsuransiModel().setUnitlink_opsipremi_ua(0);
                me.getUsulanAsuransiModel().setUnitlink_kombinasi_ua(COMB_NAME);
                me.getUsulanAsuransiModel().setUnitlink_kurs_total_ua(cursor.getString(cursor.getColumnIndexOrThrow("LKU_ID")));
                me.getUsulanAsuransiModel().setUnitlink_total_ua(cursor.getDouble(cursor.getColumnIndexOrThrow("PREMI")));
                me.getUsulanAsuransiModel().setUnitlink_premistandard_ua(cursor.getDouble(cursor.getColumnIndexOrThrow("PREMI")));
            } else {
                me.getUsulanAsuransiModel().setUnitlink_opsipremi_ua(1);
                me.getUsulanAsuransiModel().setUnitlink_kurs_ua(cursor.getString(cursor.getColumnIndexOrThrow("LKU_ID")));
                if (produk == 134 || produk == 190 || produk == 200 || produk == 215) {
                    me.getUsulanAsuransiModel().setUnitlink_total_ua(cursor.getDouble(cursor.getColumnIndexOrThrow("PREMI_POKOK")));
                    me.getUsulanAsuransiModel().setUnitlink_premistandard_ua(cursor.getDouble(cursor.getColumnIndexOrThrow("PREMI_POKOK")));
                } else {
                    me.getUsulanAsuransiModel().setUnitlink_premistandard_ua(cursor.getDouble(cursor.getColumnIndexOrThrow("PREMI")));
                }
            }
            me.getUsulanAsuransiModel().setKurs_up_ua(cursor.getString(cursor.getColumnIndexOrThrow("LKU_ID")));
            me.getDetilInvestasiModel().setPremi_pokok_di(cursor.getDouble(cursor.getColumnIndexOrThrow("PREMI_POKOK")));
            if (produk == 134 || produk == 215) {
                me.getDetilInvestasiModel().setPremi_tunggal_di(cursor.getDouble(cursor.getColumnIndexOrThrow("PREMI_TOPUP")));
            } else {
                me.getDetilInvestasiModel().setPremi_berkala_di(cursor.getDouble(cursor.getColumnIndexOrThrow("PREMI_TOPUP")));
            }
            me.getUsulanAsuransiModel().setUp_ua(cursor.getDouble(cursor.getColumnIndexOrThrow("UP")));
            me.getUsulanAsuransiModel().setCarabayar_ua(cursor.getInt(cursor.getColumnIndexOrThrow("CARA_BAYAR")));
            me.getUsulanAsuransiModel().setCuti_premi_ua(cursor.getInt(cursor.getColumnIndexOrThrow("THN_CUTI_PREMI")));
            me.getUsulanAsuransiModel().setMasa_pertanggungan_ua(cursor.getInt(cursor.getColumnIndexOrThrow("THN_MASA_KONTRAK")));
            me.getUsulanAsuransiModel().setMasa_pembayaran_ua(cursor.getInt(cursor.getColumnIndexOrThrow("THN_LAMA_BAYAR")));
        }

        cursor = queryUtil.query(
                context.getString(R.string.MST_PROPOSAL_PRODUCT_RIDER),
                null,
                selection,
                selectionArgs,
                null
        );

        ArrayList<P_MstProposalProductRiderModel> ListRider = new TableProposalProductRider(context).getObjectArray(cursor, true);
        ArrayList<ModelAddRiderUA> list_Rider = new ArrayList<>();
        ArrayList<ModelAskesUA> List_Askes = new ArrayList<>();
        for (int i = 0; i < ListRider.size(); i++) {
            String peserta = "";
            ModelAddRiderUA modelRider = new ModelAddRiderUA();
            if (cursorData.getInt(cursorData.getColumnIndexOrThrow("FLAG_TT_CALON_BAYI")) == 1) {
                ModelAskesUA modelAskesUA = new ModelAskesUA();
                modelRider.setPeserta_askes(cursorData.getString(cursorData.getColumnIndexOrThrow("NAMA_TT")));
                modelAskesUA.setPeserta_askes(cursorData.getString(cursorData.getColumnIndexOrThrow("NAMA_TT")));
                Calendar cal = toCalendar(cursorData.getString(cursorData.getColumnIndexOrThrow("TGL_LAHIR_TT")));
                modelRider.setTtl_askes(toStringDate(cal, 5));
                modelAskesUA.setTtl_askes(toStringDate(cal, 5));
                modelRider.setJekel_askes(Integer.parseInt(cursorData.getString(cursorData.getColumnIndexOrThrow("SEX_TT"))));
                modelAskesUA.setJekel_askes(Integer.parseInt(cursorData.getString(cursorData.getColumnIndexOrThrow("SEX_TT"))));
                modelRider.setUsia_askes(Integer.parseInt(cursorData.getString(cursorData.getColumnIndexOrThrow("UMUR_TT"))));
                modelAskesUA.setUsia_askes(Integer.parseInt(cursorData.getString(cursorData.getColumnIndexOrThrow("UMUR_TT"))));
                List_Askes.add(modelAskesUA);
            }
            modelRider.setKode_produk_rider(ListRider.get(i).getLsbs_id());
            modelRider.setKode_subproduk_rider(ListRider.get(i).getLsdbs_number());
            if (ListRider.get(i).getPremi() != null) {
                modelRider.setPremi_rider(MethodSupport.onSetDouble(ListRider.get(i).getPremi()));
            }
            if (ListRider.get(i).getUp() != null) {
                modelRider.setUp_rider(MethodSupport.onSetDouble(ListRider.get(i).getUp()));
            }
            if (ListRider.get(i).getPersen_up() != null) {
                modelRider.setPersentase_rider(ListRider.get(i).getPersen_up());
            }
            if (ListRider.get(i).getKelas() != null) {
                modelRider.setKlas_rider(ListRider.get(i).getKelas());
            }
            if (ListRider.get(i).getJml_unit() != null) {
                modelRider.setUnit_rider(ListRider.get(i).getJml_unit());
            }
            peserta = new E_Select(context).getSubPesertaI(ListRider.get(i).getLsbs_id(), ListRider.get(i).getLsdbs_number());
            if (peserta.contains("(TERTANGGUNG I)")) {
                modelRider.setPeserta_askes(cursorData.getString(cursorData.getColumnIndexOrThrow("NAMA_TT")));
                Calendar cal1 = toCalendar(cursorData.getString(cursorData.getColumnIndexOrThrow("TGL_LAHIR_TT")));
                modelRider.setTtl_askes(toStringDate(cal1, 5));
                modelRider.setUsia_askes(Integer.parseInt(cursorData.getString(cursorData.getColumnIndexOrThrow("UMUR_TT"))));
                modelRider.setJekel_askes(Integer.parseInt(cursorData.getString(cursorData.getColumnIndexOrThrow("SEX_TT"))));
                modelRider.setRider_askes(0);

                ModelAskesUA modelAskes = new ModelAskesUA();
                modelAskes.setKode_produk_rider(ListRider.get(i).getLsbs_id());
                modelAskes.setKode_subproduk_rider(ListRider.get(i).getLsdbs_number());
                modelAskes.setPeserta_askes(cursorData.getString(cursorData.getColumnIndexOrThrow("NAMA_TT")));
                modelAskes.setTtl_askes(toStringDate(cal1, 5));
                modelAskes.setJekel_askes(Integer.parseInt(cursorData.getString(cursorData.getColumnIndexOrThrow("SEX_TT"))));
                modelAskes.setUsia_askes(Integer.parseInt(cursorData.getString(cursorData.getColumnIndexOrThrow("UMUR_TT"))));
                modelAskes.setRider_askes(0);
                List_Askes.add(modelAskes);
            }
            selection = context.getString(R.string.NO_PROPOSAL_TAB) + " = ? AND " +
                    context.getString(R.string.LSBS_ID) + " = ?";
            selectionArgs = new String[]{
                    idproposal_tab,
                    String.valueOf(ListRider.get(i).getLsbs_id())
            };
            cursor = queryUtil.query(
                    context.getString(R.string.MST_PROPOSAL_PRODUCT_PESERTA),
                    null,
                    selection,
                    selectionArgs,
                    null
            );
            ArrayList<P_MstProposalProductPesertaModel> ListPeserta = new TableProposalProductPeserta(context).getObjectArray(cursor, true);
            if (ListPeserta.isEmpty() || peserta.contains("(TERTANGGUNG I)")) {
                list_Rider.add(modelRider);
            }
            if (!ListPeserta.isEmpty()) {
                for (int j = 0; j < ListPeserta.size(); j++) {
                    ModelAddRiderUA modelRiderUA = new ModelAddRiderUA();
                    ModelAskesUA modelAskesUA = new ModelAskesUA();
                    modelRiderUA.setKode_produk_rider(ListPeserta.get(j).getLsbs_id());
                    modelRiderUA.setKode_subproduk_rider(ListPeserta.get(j).getLsdbs_number());
                    if (ListRider.get(i).getPremi() != null) {
                        modelRiderUA.setPremi_rider(MethodSupport.onSetDouble(ListRider.get(i).getPremi()));
                    }
                    if (ListRider.get(i).getUp() != null) {
                        modelRiderUA.setUp_rider(MethodSupport.onSetDouble(ListRider.get(i).getUp()));
                    }
                    if (ListRider.get(i).getPersen_up() != null) {
                        modelRiderUA.setPersentase_rider(ListRider.get(i).getPersen_up());
                    }
                    if (ListRider.get(i).getKelas() != null) {
                        modelRiderUA.setKlas_rider(ListRider.get(i).getKelas());
                    }
                    if (ListRider.get(i).getJml_unit() != null) {
                        modelRiderUA.setUnit_rider(ListRider.get(i).getJml_unit());
                    }
                    modelRiderUA.setPeserta_askes(ListPeserta.get(j).getNama_peserta());
                    Calendar cal = toCalendar(ListPeserta.get(j).getTgl_lahir());
                    modelRiderUA.setTtl_askes(toStringDate(cal, 5));
                    modelRiderUA.setUsia_askes(ListPeserta.get(j).getUsia());
                    modelRiderUA.setHubungan_askes(ListPeserta.get(j).getLsre_id());
                    modelRiderUA.setRider_askes(ListPeserta.get(j).getPeserta_ke());
                    modelRiderUA.setVal_rider(false);

                    modelAskesUA.setKode_produk_rider(ListPeserta.get(j).getLsbs_id());
                    modelAskesUA.setKode_subproduk_rider(ListPeserta.get(j).getLsdbs_number());
                    modelAskesUA.setPeserta_askes(ListPeserta.get(j).getNama_peserta().toUpperCase());
                    modelAskesUA.setTtl_askes(toStringDate(cal, 5));
                    modelAskesUA.setUsia_askes(ListPeserta.get(j).getUsia());
                    modelAskesUA.setHubungan_askes(ListPeserta.get(j).getLsre_id());
                    modelAskesUA.setRider_askes(ListPeserta.get(j).getPeserta_ke());

                    list_Rider.add(modelRiderUA);
                    List_Askes.add(modelAskesUA);
                }
            }
        }

        me.setListASkesUA(List_Askes);
        me.setListRiderUA(list_Rider);
        selection = context.getString(R.string.NO_PROPOSAL_TAB) + " = ?";
        selectionArgs = new String[]{idproposal_tab};
        cursor = queryUtil.query(
                context.getString(R.string.MST_PROPOSAL_PRODUCT_ULINK),
                null,
                selection,
                selectionArgs,
                null
        );

        ArrayList<P_MstProposalProductUlinkModel> ListDana = new TableProposalProductULink(context).getObjectArray(cursor);
        ArrayList<ModelJnsDanaDI> ListDanaSPAJ = new ArrayList<>();
        for (int i = 0; i < ListDana.size(); i++) {
            ModelJnsDanaDI jenisdana = new ModelJnsDanaDI();
            jenisdana.setJenis_dana(ListDana.get(i).getLji_id());
            jenisdana.setPersen_alokasi((double) ListDana.get(i).getMdu_persen());
            ListDanaSPAJ.add(jenisdana);
        }
        me.setListDanaDI(ListDanaSPAJ);

        selection = context.getString(R.string.NO_PROPOSAL_TAB) + " = ?";
        selectionArgs = new String[]{idproposal_tab};
        cursor = queryUtil.query(
                context.getString(R.string.MST_PROPOSAL_PRODUCT_TOPUP),
                null,
                selection,
                selectionArgs,
                null
        );
        ArrayList<P_MstProposalProductTopUpModel> LIst_PropTopUp = new TableProposalProductTopUp(context).getObjectArray(cursor);
        for (int i = 0; i < LIst_PropTopUp.size(); i++) {
            if (LIst_PropTopUp.get(i).getThn_ke() == 1) {
                if (LIst_PropTopUp.get(i).getTopup() != null) {
                    me.getDetilInvestasiModel().setPremi_tunggal_di(MethodSupport.onSetDouble(LIst_PropTopUp.get(i).getTopup()));
                    break;
                }
            }
        }
        // ambil dari ID member
        if (me.getDetilAgenModel().getJENIS_LOGIN() == 2) {
            if (me.getDetilAgenModel().getGroup_id_da() == 40) {
                selection = context.getString(R.string.noid_member) + " = ?";
                //Sementara untuk versi ini aja
                String member = me.getDetilAgenModel().getId_member_da();
                if (member.equals("") || member == null) {
                    member = new E_Select(context).getCheckMember(me.getPemegangPolisModel().getNama_pp(), String.valueOf(me.getUsulanAsuransiModel().getPaket_ua()),
                            me.getPemegangPolisModel().getTtl_pp());
                }//Sementara untuk versi ini aja

                selectionArgs = new String[]{member};

                MemberModel memberModel = new MemberModel();
                cursor = queryUtil.query(context.getString(R.string.TABLE_MEMBER),
                        null,
                        selection,
                        selectionArgs,
                        null
                );
                memberModel = new MemberTable(context).getObject(cursor);

                me.getPemegangPolisModel().setHp1_pp(memberModel.getMPHONE());
                me.getPemegangPolisModel().setEmail_pp(memberModel.getEMAIL());
                me.getPemegangPolisModel().setBukti_identitas_pp(1);
                me.getPemegangPolisModel().setNo_identitas_pp(memberModel.getSOCIALID());

                me.getDetilAgenModel().setId_sponsor_da(memberModel.getSPONSOR());
                me.getDetilAgenModel().setId_penempatan_da(memberModel.getUPLINE());
                me.getDetilAgenModel().setId_member_da(memberModel.getNOID());
            }
        }
        Cursor cursorPropPOS = queryUtil.query(
                context.getString(R.string.TABLE_E_PR),
                null,
                context.getString(R.string.SPAJ_ID_TAB) + " = ?",
                new String[]{String.valueOf(me.getModelID().getProposal_tab())},
                null
        );
        int fundCursorSize = cursorPropPOS.getCount();
        cursorPropPOS.close();

        if (fundCursorSize > 0) {
            cursor = queryUtil.query(
                    context.getString(R.string.TABLE_E_PR),
                    null,
                    context.getString(R.string.SPAJ_ID_TAB) + " = ?",
                    new String[]{String.valueOf(me.getModelID().getProposal_tab())},
                    null
            );
            me.setProfileResikoModel(new TableProfileResiko(context).getObject(cursor));
        }

        return me;
    }

    public static EspajModel getSPAJ(E_MainActivity context, QueryUtil queryUtil, String SPAJ_ID_TAB, EspajModel me) {
        String selection = context.getString(R.string.SPAJ_ID_TAB) + " = ?";
        String[] selectionArgs = new String[]{SPAJ_ID_TAB};
        Cursor cursor;

//        getNewModel(context, me);

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_MST_SPAJ),
                null,
                selection,
                selectionArgs,
                null
        );
        me.setModelID(new TableMST_SPAJ(context).getObject(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_PP),
                null,
                selection,
                selectionArgs,
                null
        );
        me.setPemegangPolisModel(new TablePemegangPolis(context).getObject(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_TT),
                null,
                selection,
                selectionArgs,
                null
        );
        me.setTertanggungModel(new TableTertanggung(context).getObject(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_CP),
                null,
                selection,
                selectionArgs,
                null
        );
        me.setCalonPembayarPremiModel(new TableCalonPembayarPremi(context).getObject(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_UA),
                null,
                selection,
                selectionArgs,
                null
        );
        me.setUsulanAsuransiModel(new TableUsulanAsuransi(context).getObject(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_DI),
                null,
                selection,
                selectionArgs,
                null
        );
        me.setDetilInvestasiModel(new TableDetilInvestasi(context).getObject(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_DA),
                null,
                selection,
                selectionArgs,
                null
        );
        me.setDetilAgenModel(new TableDetilAgen(context).getObject(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_PR),
                null,
                selection,
                selectionArgs,
                null
        );
        me.setProfileResikoModel(new TableProfileResiko(context).getObject(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_ASKES),
                null,
                selection,
                selectionArgs,
                null
        );
        me.setListASkesUA(new TableAskesUA(context).getObjectArray(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_RIDER),
                null,
                selection,
                selectionArgs,
                null
        );
        me.setListRiderUA(new TableAddRiderUA(context).getObjectArray(cursor));


        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_DANA),
                null,
                selection,
                selectionArgs,
                null
        );
        me.setListDanaDI(new TableJnsDanaDI(context).getObjectArray(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_MANFAAT),
                null,
                selection,
                selectionArgs,
                null
        );
        me.setListManfaatDI(new TableDataDitunjukDI(context).getObjectArray(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_FOTO),
                null,
                selection,
                selectionArgs,
                null
        );
        me.getDokumenPendukungModel().setListFoto(new TableDP(context).getObjectArray(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_TTD),
                null,
                selection,
                selectionArgs,
                null
        );
        me.getDokumenPendukungModel().setListTTD(new TableTTD(context).getObjectArray(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_QA),
                null,
                selection,
                selectionArgs,
                null
        );
        me.getModelUpdateQuisioner().setListQuisioner(new TableMSTAnswer(context).getObjectArray(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_NO9TTUQ),
                null,
                selection,
                selectionArgs,
                null
        );
        me.getModelUpdateQuisioner().setList_dkno9tt(new TableDKno9_tt(context).getObjectArray(cursor));

        cursor = queryUtil.query(
                context.getString(R.string.TABLE_E_NO9PPUQ),
                null,
                selection,
                selectionArgs,
                null
        );
        me.getModelUpdateQuisioner().setList_dkno9pp(new TableDKno9_pp(context).getObjectArray(cursor));

        // ambil dari ID member sementara aja
        if (me.getDetilAgenModel().getJENIS_LOGIN() == 2) {
            if (me.getDetilAgenModel().getGroup_id_da() == 40) {
                selection = context.getString(R.string.noid_member) + " = ?";
                //Sementara untuk versi ini aja
                String member = me.getDetilAgenModel().getId_member_da();
                if (member.equals("") || member == null) {
                    member = new E_Select(context).getCheckMember(me.getPemegangPolisModel().getNama_pp(), String.valueOf(me.getUsulanAsuransiModel().getPaket_ua()),
                            me.getPemegangPolisModel().getTtl_pp());
                }//Sementara untuk versi ini aja

                selectionArgs = new String[]{member};

                MemberModel memberModel = new MemberModel();
                cursor = queryUtil.query(context.getString(R.string.TABLE_MEMBER),
                        null,
                        selection,
                        selectionArgs,
                        null
                );
                memberModel = new MemberTable(context).getObject(cursor);

                me.getPemegangPolisModel().setHp1_pp(memberModel.getMPHONE());
                me.getPemegangPolisModel().setEmail_pp(memberModel.getEMAIL());
                if (me.getPemegangPolisModel().getBukti_identitas_pp() == 0) {
                    me.getPemegangPolisModel().setBukti_identitas_pp(1);
                }
                me.getPemegangPolisModel().setNo_identitas_pp(memberModel.getSOCIALID());

                me.getDetilAgenModel().setId_sponsor_da(memberModel.getSPONSOR());
                me.getDetilAgenModel().setId_penempatan_da(memberModel.getUPLINE());
                me.getDetilAgenModel().setId_member_da(memberModel.getNOID());
            }
        }
        return me;
    }

}
