package id.co.ajsmsig.nb.pointofsale.ui.handlingobjection


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.ajsmsig.nb.R
import id.co.ajsmsig.nb.pointofsale.adapter.HandlingObjectionDetailAdapter
import id.co.ajsmsig.nb.databinding.PosFragmentHandlingObjectionDetailBinding
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.Page
import id.co.ajsmsig.nb.pointofsale.utils.Constants.Companion.PAGE
import id.co.ajsmsig.nb.pointofsale.ui.fragment.BaseFragment


/**
 * A simple [Fragment] subclass.
 */
class HandlingObjectionDetailFragment : BaseFragment() {

    private lateinit var dataBinding: PosFragmentHandlingObjectionDetailBinding
    private lateinit var handlingObjectionAdapter: HandlingObjectionDetailAdapter


    override fun onActivityCreated(savedInstanceState: Bundle?) {

        sharedViewModel.selectedHandlingObjection?.let {
            (arguments?.getSerializable(PAGE) as? Page)?.let { page = it }
            page?.title = "${page?.title} - ${it.text}"
        }
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, viewModelFactory).get(HandlingObjectionDetailVM::class.java)
        subscribeUi(viewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.pos_fragment_handling_objection_detail, container, false)

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupList(dataBinding.recyclerView)
    }

    fun setupList(recyclerView: RecyclerView) {
        handlingObjectionAdapter = HandlingObjectionDetailAdapter()
        recyclerView.adapter = handlingObjectionAdapter
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
    }

    fun subscribeUi(viewModel: HandlingObjectionDetailVM) {
        dataBinding.vm = viewModel

        sharedViewModel.selectedHandlingObjection?.let {
            viewModel.category = it

            viewModel.observableHandlingObjection.observe(this, Observer {
                it?.let {
                    handlingObjectionAdapter.options = it.toTypedArray()
                }
            })
        }
    }

    companion object {
        val category = "category"
    }
}// Required empty public constructor
