package id.co.ajsmsig.nb.prop.model.hardCode;
/*
 Created by faiz_f on 04/06/2017.
 */

public class P_FragmentPagePosition {
    public static final int DATA_PROPOSAL_PAGE = 1;
    public static final int PRODUK_PAGE = 2;
    public static final int INVESTASI_PAGE = 3;
    public static final int TOPUP_PAGE = 4;
    public static final int RIDER_PAGE = 5;
    public static final int RESULT_PAGE = 6;
}
