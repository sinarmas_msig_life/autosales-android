package id.co.ajsmsig.nb.espaj.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.method.AutoCompleteTextViewClickListener;
import id.co.ajsmsig.nb.crm.method.StaticMethods;
import id.co.ajsmsig.nb.espaj.method.MethodSupport;
import id.co.ajsmsig.nb.espaj.method.Method_Validator;
import id.co.ajsmsig.nb.espaj.model.EspajModel;
import id.co.ajsmsig.nb.espaj.model.arraylist.ModelDataDitunjukDI;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropDownString;
import id.co.ajsmsig.nb.espaj.model.dropdown.ModelDropdownInt;
import id.co.ajsmsig.nb.prop.model.DropboxModel;

import static id.co.ajsmsig.nb.crm.method.StaticMethods.toCalendar;

/**
 * Created by Bernard on 12/07/2017.
 */

public class E_FormDataPenerimaManfaatActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, RadioGroup.OnCheckedChangeListener, View.OnFocusChangeListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getSharedPreferences(getResources().getString(R.string.app_preferences), Context.MODE_PRIVATE);
        kodeRegional = sharedPreferences.getString("KODE_REGIONAL", "");
        GROUP_ID = sharedPreferences.getInt("GROUP_ID", 0);
        setContentView(R.layout.e_acitivy_manfaat);
        ActionBar toolbar = getSupportActionBar();
        toolbar.setDisplayHomeAsUpEnabled(true);
        toolbar.setDisplayShowHomeEnabled(true);
        toolbar.setTitle("Form Data Penerima Manfaat/ Ahli Waris");
        ButterKnife.bind(this);
        Intent intent = getIntent();
        pos = intent.getIntExtra(getString(R.string.pos), 0);
        modelDataDitunjukDI = intent.getParcelableExtra(getString(R.string.modelmanfaat));

        et_nama.addTextChangedListener(new MTextWatcher(et_nama));
        et_tgl_lahir.addTextChangedListener(new MTextWatcher(et_tgl_lahir));
        et_tgl_lahir.setOnClickListener(this);
        et_manfaat.addTextChangedListener(new MTextWatcher(et_manfaat));

        ac_hubungan_dengan_pp.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_hubungan_dengan_pp, this));
        ac_hubungan_dengan_pp.setOnClickListener(this);
        ac_hubungan_dengan_pp.setOnFocusChangeListener(this);
        ac_hubungan_dengan_pp.addTextChangedListener(new MTextWatcher(ac_hubungan_dengan_pp));

        ac_warganegara.setOnItemClickListener(new AutoCompleteTextViewClickListener(ac_warganegara, this));
        ac_warganegara.setOnClickListener(this);
        ac_warganegara.setOnFocusChangeListener(this);
        ac_warganegara.addTextChangedListener(new MTextWatcher(ac_warganegara));

        btn_cancel.setOnClickListener(this);
        btn_lanjut.setOnClickListener(this);

        rg_jeniskel.setOnCheckedChangeListener(this);

//        try {
//            MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_warganegara, this, 0);
//            MethodSupport.load_setDropXml(R.xml.e_relasi, ac_hubungan_dengan_pp, this, 1);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (XmlPullParserException e) {
//            e.printStackTrace();
//        }
        restore();
    }
    @Override
    public void onResume() {
        super.onResume();
        try {
            MethodSupport.load_setDropXml(R.xml.e_warganegara, ac_warganegara, this, 0);
            MethodSupport.load_setDropXml(R.xml.e_relasi, ac_hubungan_dengan_pp, this, 4);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
    }

    private void loadview() {
        modelDataDitunjukDI.setValidation(Validation());
        modelDataDitunjukDI.setWarganegara(warganegara);
        modelDataDitunjukDI.setHub_dgcalon_tt(hubungan);
        modelDataDitunjukDI.setJekel(jekel);
        modelDataDitunjukDI.setNama(et_nama.getText().toString());
        modelDataDitunjukDI.setManfaat(MethodSupport.ConverttoDouble(et_manfaat));
        modelDataDitunjukDI.setTtl(et_tgl_lahir.getText().toString());
    }

    private void restore() {
        warganegara = modelDataDitunjukDI.getWarganegara();
        ListDropDownSPAJ = MethodSupport.getXML(this, R.xml.e_warganegara, 0);
        ac_warganegara.setText(MethodSupport.getAdapterPositionSPAJ( ListDropDownSPAJ, warganegara));

        hubungan = modelDataDitunjukDI.getHub_dgcalon_tt();
        ListDropDownSPAJ = MethodSupport.getXML(this, R.xml.e_relasi, 1);
        ac_hubungan_dengan_pp.setText(MethodSupport.getAdapterPositionSPAJ( ListDropDownSPAJ, hubungan));

        jekel = modelDataDitunjukDI.getJekel();
        MethodSupport.OnsetTwoRadio(rg_jeniskel, jekel);

        et_nama.setText(modelDataDitunjukDI.getNama());
        et_manfaat.setText(new BigDecimal(modelDataDitunjukDI.getManfaat()).toString());
        et_tgl_lahir.setText(String.valueOf(modelDataDitunjukDI.getTtl()));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ac_hubungan_dengan_pp:
                ac_hubungan_dengan_pp.showDropDown();
                break;
            case R.id.ac_warganegara:
                ac_warganegara.showDropDown();
                break;
            case R.id.btn_cancel:
                finish();
                break;
            case R.id.btn_lanjut:
                loadview();
                if (Validation()) {
                    Intent intent = new Intent();
                    intent.putExtra(getString(R.string.modelmanfaat), modelDataDitunjukDI);
                    intent.putExtra(getString(R.string.pos), pos);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
            case R.id.et_tgl_lahir:
                showDateDialog(1, et_tgl_lahir.getText().toString(), et_tgl_lahir);
                break;
        }
    }

    private void showDateDialog(int flag_ttl, String tanggal, EditText edit) {
        switch (flag_ttl) {
            case 1:
                if (tanggal.length() != 0) {
                    calendar = toCalendar(tanggal);
                } else {
                    calendar = Calendar.getInstance();
                }
                DatePickerDialog dialog = new DatePickerDialog(this,
                        datePickerListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                dialog.show();
                break;
        }
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            calendar.set(Calendar.YEAR, selectedYear);
            calendar.set(Calendar.MONTH, selectedMonth);
            calendar.set(Calendar.DAY_OF_MONTH, selectedDay);
            et_tgl_lahir.setText(StaticMethods.toStringDate(calendar, 5));
        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.ac_hubungan_dengan_pp:
                ModelDropdownInt modelNm_produk_ua = (ModelDropdownInt) parent.getAdapter().getItem(position);
                hubungan = modelNm_produk_ua.getId();
                if(GROUP_ID == 40) {
                    val_hubppttmanfaat();
                }
                break;
            case R.id.ac_warganegara:
                ModelDropdownInt modelWn = (ModelDropdownInt) parent.getAdapter().getItem(position);
                warganegara = modelWn.getId();
                break;
        }
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.ac_hubungan_dengan_pp:
                if (hasFocus) ac_hubungan_dengan_pp.showDropDown();
                break;
            case R.id.ac_warganegara:
                if (hasFocus) ac_warganegara.showDropDown();
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
        jekel = MethodSupport.OnCheckRadio(rg_jeniskel, i);
        iv_circle_jenis_kelamin.setColorFilter(ContextCompat.getColor(this, R.color.green));

    }

    private class MTextWatcher implements TextWatcher {
        private View view;

        MTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int color;
            switch (view.getId()) {
                case R.id.et_nama:
                    Method_Validator.ValEditWatcher(et_nama, iv_circle_nama, tv_error_nama, getApplicationContext());
                    break;
                case R.id.et_tgl_lahir:
                    Method_Validator.ValEditWatcher(et_tgl_lahir, iv_circle_tgl_lahir, tv_error_tgl_lahir, getApplicationContext());
                    break;
                case R.id.et_manfaat:
                    Method_Validator.ValEditWatcherKes(et_manfaat, iv_circle_manfaat, tv_error_manfaat, getApplicationContext());
                    break;
                //auto
                case R.id.ac_hubungan_dengan_pp:
                    Method_Validator.ValEditWatcher(ac_hubungan_dengan_pp, iv_circle_hubungan_dengan_pp, tv_error_hubungan_dengan_pp, getApplicationContext());
                    break;
                case R.id.ac_warganegara:
                    Method_Validator.ValEditWatcher(ac_warganegara, iv_circle_warganegara, tv_error_warganegara, getApplicationContext());
                    break;
            }
        }
    }

    private Boolean Validation() {
        Boolean val = true;
        int color = ContextCompat.getColor(this, R.color.red);
        if (et_nama.getVisibility() == View.VISIBLE && et_nama.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_manfaat, cl_child_manfaat, et_nama, tv_error_nama, iv_circle_nama, color, this, getString(R.string.error_field_required));
//            tv_error_nama.setVisibility(View.VISIBLE);
//            tv_error_nama.setText(getString(R.string.error_field_required));
//            iv_circle_nama.setColorFilter(color);
        }else {
            if (!Method_Validator.ValEditRegex(et_nama.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_manfaat, cl_child_manfaat, et_nama, tv_error_nama, iv_circle_nama, color, this, getString(R.string.error_regex));
            }
        }

        if (et_tgl_lahir.getVisibility() == View.VISIBLE && et_tgl_lahir.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_manfaat, cl_child_manfaat, et_tgl_lahir, tv_error_tgl_lahir, iv_circle_tgl_lahir, color, this, getString(R.string.error_field_required));
//            tv_error_tgl_lahir.setVisibility(View.VISIBLE);
//            tv_error_tgl_lahir.setText(getString(R.string.error_field_required));
//            iv_circle_tgl_lahir.setColorFilter(color);
        }

//        if (et_tgl_lahir.getVisibility() == View.VISIBLE && et_tgl_lahir.getText().toString().trim().length() == 0) {
//            val = false;
//            tv_error_tgl_lahir.setVisibility(View.VISIBLE);
//            tv_error_tgl_lahir.setText(getString(R.string.error_field_required));
//            iv_circle_tgl_lahir.setColorFilter(color);
//        }

        if (ac_hubungan_dengan_pp.getVisibility() == View.VISIBLE && ac_hubungan_dengan_pp.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_manfaat, cl_child_manfaat, ac_hubungan_dengan_pp, tv_error_hubungan_dengan_pp, iv_circle_hubungan_dengan_pp, color, this, getString(R.string.error_field_required));
//            tv_error_hubungan_dengan_pp.setVisibility(View.VISIBLE);
//            tv_error_hubungan_dengan_pp.setText(getString(R.string.error_field_required));
//            iv_circle_hubungan_dengan_pp.setColorFilter(color);
        }

        if (ac_warganegara.getVisibility() == View.VISIBLE && ac_warganegara.getText().toString().trim().length() == 0) {
            val = false;
            MethodSupport.onFocusview(scroll_manfaat, cl_child_manfaat, ac_warganegara, tv_error_warganegara, iv_circle_warganegara, color, this, getString(R.string.error_field_required));
//            tv_error_warganegara.setVisibility(View.VISIBLE);
//            tv_error_warganegara.setText(getString(R.string.error_field_required));
//            iv_circle_warganegara.setColorFilter(color);
        }

        if (et_manfaat.getVisibility() == View.VISIBLE && (et_manfaat.getText().toString().trim().length() == 0) || (et_manfaat.getText().toString().trim().equals("0"))) {
            val = false;
            MethodSupport.onFocusview(scroll_manfaat, cl_child_manfaat, et_manfaat, tv_error_manfaat, iv_circle_manfaat, color, this, getString(R.string.error_field_required));
            tv_error_manfaat.setVisibility(View.VISIBLE);
            tv_error_manfaat.setText(getString(R.string.error_field_required));
            iv_circle_manfaat.setColorFilter(color);
        }else {
            if (!Method_Validator.ValEditRegex(et_manfaat.getText().toString())) {
                val = false;
                MethodSupport.onFocusview(scroll_manfaat, cl_child_manfaat, et_manfaat, tv_error_manfaat, iv_circle_manfaat, color, this, getString(R.string.error_regex));
            }
        }

        //radiogroup
        if (jekel == -1) {
            val = false;
            tv_error_jenis_kelamin.setVisibility(View.VISIBLE);
            tv_error_jenis_kelamin.setText(getString(R.string.error_field_required));
            iv_circle_jenis_kelamin.setColorFilter(color);
        }
        return val;
    }

    private void val_hubppttmanfaat(){
        if(hubungan != 1 && hubungan != 7 && hubungan != 4 && hubungan != 2 && hubungan != 5){
            // masih di tanyakan ke Mba Tities
            tv_warn_hubungan_dengan_pp.setVisibility(View.VISIBLE);
        }else {
            tv_warn_hubungan_dengan_pp.setVisibility(View.GONE);
        }
    }

    @BindView(R.id.cl_nama)
    RelativeLayout cl_nama;
    @BindView(R.id.et_nama)
    EditText et_nama;
    @BindView(R.id.iv_circle_nama)
    ImageView iv_circle_nama;
    @BindView(R.id.tv_error_nama)
    TextView tv_error_nama;

    @BindView(R.id.cl_jenis_kelamin)
    RelativeLayout cl_jenis_kelamin;
    @BindView(R.id.iv_circle_jenis_kelamin)
    ImageView iv_circle_jenis_kelamin;
    @BindView(R.id.rg_jeniskel)
    RadioGroup rg_jeniskel;
    @BindView(R.id.rb_pria)
    RadioButton rb_pria;
    @BindView(R.id.rb_wanita)
    RadioButton rb_wanita;
    @BindView(R.id.tv_error_jenis_kelamin)
    TextView tv_error_jenis_kelamin;

    @BindView(R.id.cl_tgl_lahir)
    RelativeLayout cl_tgl_lahir;
    @BindView(R.id.iv_circle_tgl_lahir)
    ImageView iv_circle_tgl_lahir;
    @BindView(R.id.et_tgl_lahir)
    EditText et_tgl_lahir;
    @BindView(R.id.tv_error_tgl_lahir)
    TextView tv_error_tgl_lahir;

    @BindView(R.id.cl_hubungan_dengan_pp)
    RelativeLayout cl_hubungan_dengan_pp;
    @BindView(R.id.iv_circle_hubungan_dengan_pp)
    ImageView iv_circle_hubungan_dengan_pp;
    @BindView(R.id.ac_hubungan_dengan_pp)
    AutoCompleteTextView ac_hubungan_dengan_pp;
    @BindView(R.id.tv_error_hubungan_dengan_pp)
    TextView tv_error_hubungan_dengan_pp;
    @BindView(R.id.tv_warn_hubungan_dengan_pp)
    TextView tv_warn_hubungan_dengan_pp;

    @BindView(R.id.cl_warganegara)
    RelativeLayout cl_warganegara;
    @BindView(R.id.iv_circle_warganegara)
    ImageView iv_circle_warganegara;
    @BindView(R.id.ac_warganegara)
    AutoCompleteTextView ac_warganegara;
    @BindView(R.id.tv_error_warganegara)
    TextView tv_error_warganegara;

    @BindView(R.id.cl_manfaat)
    RelativeLayout cl_manfaat;
    @BindView(R.id.et_manfaat)
    EditText et_manfaat;
    @BindView(R.id.iv_circle_manfaat)
    ImageView iv_circle_manfaat;
    @BindView(R.id.tv_error_manfaat)
    TextView tv_error_manfaat;

    @BindView(R.id.btn_cancel)
    Button btn_cancel;
    @BindView(R.id.btn_lanjut)
    Button btn_lanjut;

    @BindView(R.id.scroll_manfaat)
    ScrollView scroll_manfaat;

    @BindView(R.id.cl_child_manfaat)
    RelativeLayout cl_child_manfaat;

    private Intent intent;
    private int pos = 0;
    private int jekel = -1;
    private int warganegara = 0;
    private int hubungan = 0;
    private int GROUP_ID = 0;

    private Calendar calendar;

    private ArrayList<ModelDropdownInt> ListDropDownSPAJ;
    private ArrayList<ModelDropDownString> ListDropDownString;
    private ArrayList<DropboxModel> ListDropInt;


    private ModelDataDitunjukDI modelDataDitunjukDI;
    private EspajModel me;
    private String kodeRegional = "";
}
