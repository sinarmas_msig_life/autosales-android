package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BankCabangResponse {

    @SerializedName("BANK CABANG")
    @Expose
    private List<BANKCABANG> bANKCABANG = null;

    public List<BANKCABANG> getBANKCABANG() {
        return bANKCABANG;
    }

    public void setBANKCABANG(List<BANKCABANG> bANKCABANG) {
        this.bANKCABANG = bANKCABANG;
    }

}
