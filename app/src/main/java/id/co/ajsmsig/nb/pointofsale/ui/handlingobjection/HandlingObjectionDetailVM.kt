package id.co.ajsmsig.nb.pointofsale.ui.handlingobjection

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MediatorLiveData
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.HandlingObjection
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.HandlingObjectionAllAnswers
import id.co.ajsmsig.nb.pointofsale.model.db.prepopulated.repository.PrePopulatedRepository
import javax.inject.Inject

/**
 * Created by andreyyoshuamanik on 08/03/18.
 */
class HandlingObjectionDetailVM
@Inject
constructor(application: Application, var repository: PrePopulatedRepository): AndroidViewModel(application) {

    val observableHandlingObjection = MediatorLiveData<List<HandlingObjectionAllAnswers>>()

    var category: HandlingObjection? = null
    set(value) {
        field = value

        val handlingObjections = repository.getHandlingObjectionQuestionsWithCategoryId(value?.id)
        observableHandlingObjection.addSource(handlingObjections, observableHandlingObjection::setValue)
    }
}