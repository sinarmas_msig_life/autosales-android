package id.co.ajsmsig.nb.crm.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.Loader;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleCursorTreeAdapter;
import android.widget.TextView;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.fragment.listlead.C_ListLeadFragment;

/*
 *Created by faiz_f on 08/05/2017.
 */

public class LeadsSimpleCursorTreeAdapter extends SimpleCursorTreeAdapter {
    private static final String TAG = LeadsSimpleCursorTreeAdapter.class.getSimpleName();
    private FragmentActivity mActivity;
    private C_ListLeadFragment mFragment;
    private final SparseIntArray mGroupMap;


    public LeadsSimpleCursorTreeAdapter(FragmentActivity activity, C_ListLeadFragment mFragment, int groupLayout, String[] groupFrom, int[] groupTo, int childLayout, String[] childFrom, int[] childTo) {
        super(activity, null, groupLayout, groupFrom, groupTo, childLayout, childFrom, childTo);
        this.mActivity = activity;
        this.mFragment = mFragment;
        this.mGroupMap = new SparseIntArray();
    }

    @Override
    protected Cursor getChildrenCursor(Cursor groupCursor) {
        // Logic to get the child cursor on the basis of selected group.
        int groupPos = groupCursor.getPosition();
        int groupId = groupCursor.getInt(groupCursor.getColumnIndex(mActivity.getString(R.string._ID)));

        mGroupMap.put(groupId, groupPos);
        Loader<Object> loader = mActivity.getSupportLoaderManager().getLoader(groupId);
        if (loader != null && !loader.isReset()) {
            mActivity.getSupportLoaderManager().restartLoader(groupId, null, mFragment);
        } else {
            mActivity.getSupportLoaderManager().initLoader(groupId, null, mFragment);
        }

        return null;

    }

    @Override
    protected void bindGroupView(View view, Context context, Cursor cursor, boolean isExpanded) {
        super.bindGroupView(view, context, cursor, isExpanded);

        int groupPosition = cursor.getPosition();
        int childrenCount = getChildrenCount(groupPosition);

        ImageView iv_header_list = view.findViewById(R.id.iv_header_list);
        TextView tv_empty_notif = view.findViewById(R.id.tv_empty_notif);

        switch (groupPosition) {
            case 0:
                tv_empty_notif.setText("Tidak Ada Lead Pilihan");
                break;
            case 1:
                tv_empty_notif.setText("Tidak Ada Lead");
                break;
            default:
                break;
        }

        if (isExpanded) {
            iv_header_list.setImageResource(R.drawable.ic_expand_less_light_grey);
            int emptyNotifvisibility;
            if (getChildrenCount(groupPosition) == 0) {
                emptyNotifvisibility = View.VISIBLE;
            } else {
                emptyNotifvisibility = View.GONE;
            }
            tv_empty_notif.setVisibility(emptyNotifvisibility);
        } else {
            iv_header_list.setImageResource(R.drawable.ic_expand_more_light_grey);
            tv_empty_notif.setVisibility(View.GONE);
        }

        //Log.v(TAG, "is expanded : "+isExpanded);
    }

    @Override
    protected void bindChildView(View view, Context context, Cursor cursor, boolean isLastChild) {
        super.bindChildView(view, context, cursor, isLastChild);
        TextView tv_sl_name = view.findViewById(R.id.tv_sl_name);
        TextView tv_sl_umur = view.findViewById(R.id.tv_sl_bdate);

        String umur = cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.SL_UMUR))) + " Tahun";
        tv_sl_umur.setText(umur);

//        int SL_LAST_POS = cursor.getInt(cursor.getColumnIndexOrThrow(context.getString(R.string.SL_LAST_POS)));
//        if (SL_LAST_POS == 1) {
//            tv_sl_name.setTextColor(ContextCompat.getColor(context, R.color.green));
//        } else {
//            tv_sl_name.setTextColor(ContextCompat.getColor(context, R.color.tiga));
//        }
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = super.getGroupView(groupPosition, isExpanded, convertView, parent);
        return v;
    }


    public SparseIntArray getGroupMap() {
        return mGroupMap;
    }
}
