/**
 * @author Eriza Siti Mulyani
 *
 */
package id.co.ajsmsig.nb.espaj.method;

import java.util.Calendar;

public class F_Hit_Umur{
	//method hitung usia		
			public static String  OnCountUsia(int year, int month, int day) {
				int li_month = 0;
				int li_Umur = 0;
				int li_add = 0;
				int li_curr_year = 0;
				int li_curr_month = 0;
				int li_curr_day = 0;
				Calendar now=  Calendar.getInstance();  
				li_curr_year = now.get(Calendar.YEAR) ;  
				 li_curr_month = now.get(Calendar.MONTH);  
				 li_curr_day = now.get(Calendar.DATE);
				 
					//if (year != li_curr_year) edit bernard
					//{
						if (li_curr_month >= month)
						{
							li_Umur = li_curr_year - year;
						}else{
							li_Umur = (li_curr_year - year) - 1;
							li_add = 12;
						}
						li_month = li_curr_month + li_add - month;
						if (li_month >= 6)
						{
							if (li_month==6)
							{
								if ((li_curr_day  - day) >= 0)
								{
									li_Umur = li_Umur+1;
								}
							}else{
								li_Umur = li_Umur+1;
							}
							
						}
					//} edit bernard
					if (li_Umur<0){
						li_Umur=0;
					}
	            String umur = li_Umur+" tahun ";  
				return umur;
			}
	//method hitung Hari		
			public static int  OnCountHari(int year, int month, int day) {
				Calendar now=  Calendar.getInstance();  
				Calendar tanggallahir = Calendar.getInstance();  
				tanggallahir.set(year, month, day);  
				long diff = now.getTimeInMillis() - tanggallahir.getTimeInMillis();
				int day_count= (int) (diff / (24 * 60 * 60 * 1000));
				 
				return day_count;
			}	
//menghitung bulan			
			public static int  OnCountBulan(int year, int month, int day) {
				Calendar now=  Calendar.getInstance();  
				Calendar tanggallahir = Calendar.getInstance();  
				 tanggallahir.set(year, month, day);
				 int month_count = Math.round( OnCountHari (year, month,  day)/30);  
				return month_count;
			}
}