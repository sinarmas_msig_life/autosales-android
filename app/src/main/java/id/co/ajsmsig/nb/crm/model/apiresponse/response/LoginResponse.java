
package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("NAMA_AGEN")
    @Expose
    private String nAMAAGEN;
    @SerializedName("GROUP_ID")
    @Expose
    private int gROUPID;
    @SerializedName("FLAG_ADMIN")
    @Expose
    private int fLAGADMIN;
    @SerializedName("NAMA_REG")
    @Expose
    private String nAMAREG;
    @SerializedName("JENIS_LOGIN_BC")
    @Expose
    private int jENISLOGINBC;
    @SerializedName("KODE_LEADER")
    @Expose
    private String kODELEADER;
    @SerializedName("NAMA_BANK")
    @Expose
    private String nAMABANK;
    @SerializedName("JENIS_LOGIN")
    @Expose
    private int jENISLOGIN;
    @SerializedName("KODE_AGEN")
    @Expose
    private String kODEAGEN;
    @SerializedName("KODE_REG2")
    @Expose
    private String kODEREG2;
    @SerializedName("KODE_REG3")
    @Expose
    private String kODEREG3;
    @SerializedName("KODE_REG1")
    @Expose
    private String kODEREG1;
    @SerializedName("EMAIL")
    @Expose
    private String eMAIL;
    @SerializedName("KODE_REGIONAL")
    @Expose
    private String kODEREGIONAL;

    public String getNAMAAGEN() {
        return nAMAAGEN;
    }

    public void setNAMAAGEN(String nAMAAGEN) {
        this.nAMAAGEN = nAMAAGEN;
    }

    public int getGROUPID() {
        return gROUPID;
    }

    public void setGROUPID(int gROUPID) {
        this.gROUPID = gROUPID;
    }

    public int getFLAGADMIN() {
        return fLAGADMIN;
    }

    public void setFLAGADMIN(int fLAGADMIN) {
        this.fLAGADMIN = fLAGADMIN;
    }

    public String getNAMAREG() {
        return nAMAREG;
    }

    public void setNAMAREG(String nAMAREG) {
        this.nAMAREG = nAMAREG;
    }

    public int getJENISLOGINBC() {
        return jENISLOGINBC;
    }

    public void setJENISLOGINBC(int jENISLOGINBC) {
        this.jENISLOGINBC = jENISLOGINBC;
    }

    public String getKODELEADER() {
        return kODELEADER;
    }

    public void setKODELEADER(String kODELEADER) {
        this.kODELEADER = kODELEADER;
    }

    public String getNAMABANK() {
        return nAMABANK;
    }

    public void setNAMABANK(String nAMABANK) {
        this.nAMABANK = nAMABANK;
    }

    public int getJENISLOGIN() {
        return jENISLOGIN;
    }

    public void setJENISLOGIN(int jENISLOGIN) {
        this.jENISLOGIN = jENISLOGIN;
    }

    public String getKODEAGEN() {
        return kODEAGEN;
    }

    public void setKODEAGEN(String kODEAGEN) {
        this.kODEAGEN = kODEAGEN;
    }

    public String getKODEREG2() {
        return kODEREG2;
    }

    public void setKODEREG2(String kODEREG2) {
        this.kODEREG2 = kODEREG2;
    }

    public String getKODEREG3() {
        return kODEREG3;
    }

    public void setKODEREG3(String kODEREG3) {
        this.kODEREG3 = kODEREG3;
    }

    public String getKODEREG1() {
        return kODEREG1;
    }

    public void setKODEREG1(String kODEREG1) {
        this.kODEREG1 = kODEREG1;
    }

    public String getEMAIL() {
        return eMAIL;
    }

    public void setEMAIL(String eMAIL) {
        this.eMAIL = eMAIL;
    }

    public String getKODEREGIONAL() {
        return kODEREGIONAL;
    }

    public void setKODEREGIONAL(String kODEREGIONAL) {
        this.kODEREGIONAL = kODEREGIONAL;
    }

}
