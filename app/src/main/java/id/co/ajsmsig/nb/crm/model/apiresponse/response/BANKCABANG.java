package id.co.ajsmsig.nb.crm.model.apiresponse.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BANKCABANG {

    @SerializedName("LSBP_PANJANG_REKENING")
    @Expose
    private Integer lSBPPANJANGREKENING;
    @SerializedName("LBN_NAMA")
    @Expose
    private String lBNNAMA;
    @SerializedName("LSBP_ID")
    @Expose
    private Integer lSBPID;
    @SerializedName("LBN_ID")
    @Expose
    private Integer lBNID;

    public Integer getLSBPPANJANGREKENING() {
        return lSBPPANJANGREKENING;
    }

    public void setLSBPPANJANGREKENING(Integer lSBPPANJANGREKENING) {
        this.lSBPPANJANGREKENING = lSBPPANJANGREKENING;
    }

    public String getLBNNAMA() {
        return lBNNAMA;
    }

    public void setLBNNAMA(String lBNNAMA) {
        this.lBNNAMA = lBNNAMA;
    }

    public Integer getLSBPID() {
        return lSBPID;
    }

    public void setLSBPID(Integer lSBPID) {
        this.lSBPID = lSBPID;
    }

    public Integer getLBNID() {
        return lBNID;
    }

    public void setLBNID(Integer lBNID) {
        this.lBNID = lBNID;
    }

}
