package id.co.ajsmsig.nb.crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import id.co.ajsmsig.nb.R;
import id.co.ajsmsig.nb.crm.adapter.ProposalListAdapter;
import id.co.ajsmsig.nb.crm.database.C_Select;
import id.co.ajsmsig.nb.crm.model.ProposalListModel;
import id.co.ajsmsig.nb.prop.activity.P_MainActivity;
import id.co.ajsmsig.nb.util.Const;

/*
  Created by faiz_f on 03/02/2017.
 */

public class C_AgentListProposalFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener, ProposalListAdapter.ClickListener {
    private String kodeAgen;
    private int roleId = 0;
    private LinearLayout layoutSearchNotFound;
    private final int LOADER_DISPLAY_LIST_PROPOSAL = 201;
    private Cursor cursor;
    private List<ProposalListModel> proposals;
    private RecyclerView recyclerView;
    private ProposalListAdapter mAdapter;
    private FloatingActionButton floatingActionButton;

    public C_AgentListProposalFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.app_preferences), Context.MODE_PRIVATE);
        kodeAgen = sharedPreferences.getString("KODE_AGEN", "");
        roleId = sharedPreferences.getInt("ROLE_ID", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.c_fragment_list_proposal_agen, container, false);


//        listView = (ListView) view.findViewById(R.id.listView);
        layoutSearchNotFound = (LinearLayout) view.findViewById(R.id.layoutSearchNotFound);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewProposal);
        floatingActionButton = view.findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(this);

        initRecyclerView();
        /* Creating a loader for populating listview from sqlite database */
        /* This statement, invokes the method onCreatedLoader() */

        getActivity().getSupportLoaderManager().initLoader(LOADER_DISPLAY_LIST_PROPOSAL, null, this);

        if (roleId == 1) {
            floatingActionButton.setVisibility(View.VISIBLE);
        }else {
            floatingActionButton.setVisibility(View.GONE);
        }


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getSupportLoaderManager().restartLoader(LOADER_DISPLAY_LIST_PROPOSAL, null, this);
        if (roleId == 1) {
            floatingActionButton.setVisibility(View.VISIBLE);
        }else {
            floatingActionButton.setVisibility(View.GONE);
        }
    }

//    @Override
////    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
////        super.onCreateOptionsMenu(menu, inflater);
////        inflater.inflate(R.menu.menu_proposal_list, menu);
////    }


//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.menu_proposal_list, menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.action_add_proposal:
//                Intent intent = new Intent(getContext(), P_MainActivity.class);
//
//                startActivity(intent);
//                break;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    private void initRecyclerView() {
        proposals = new ArrayList<>();
        mAdapter = new ProposalListAdapter(proposals);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setClickListener(this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
//        listView.setVisibility(View.GONE);
//        layoutSearchNotFound.setVisibility(View.VISIBLE);

        CursorLoader cursorLoader;
        switch (id) {
            case LOADER_DISPLAY_LIST_PROPOSAL:
                cursor = new C_Select(getActivity()).getAgentProposalList(String.valueOf(kodeAgen));
                break;
        }

        cursorLoader = new CursorLoader(getContext()) {
            @Override
            public Cursor loadInBackground() {
                return cursor;
            }
        };

        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        switch (loader.getId()) {
            case LOADER_DISPLAY_LIST_PROPOSAL:

                if (cursor.getCount() > 0) {
                    clearPreviousResult();
                    List<ProposalListModel> proposals = new ArrayList<>();
                    while (cursor.moveToNext()) {
                        long id = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                        String noProposalTab = cursor.getString(cursor.getColumnIndexOrThrow("NO_PROPOSAL_TAB"));
                        String noProposal = cursor.getString(cursor.getColumnIndexOrThrow("NO_PROPOSAL"));
                        String namaPp = "Pemegang polis : "+cursor.getString(cursor.getColumnIndexOrThrow("NAMA_PP"));
                        String namaTt = "Tertanggung : "+cursor.getString(cursor.getColumnIndexOrThrow("NAMA_TT"));
                        long premi = cursor.getLong(cursor.getColumnIndexOrThrow("PREMI"));
                        long uangPertanggungan = cursor.getLong(cursor.getColumnIndexOrThrow("UP"));
                        String lsdbsName = cursor.getString(cursor.getColumnIndexOrThrow("LSDBS_NAME"));

                        ProposalListModel proposal = new ProposalListModel(id, noProposalTab, noProposal, namaPp, namaTt, premi, uangPertanggungan, lsdbsName);
                        proposals.add(proposal);
                    }

                    layoutSearchNotFound.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    mAdapter.addAll(proposals);
                } else {
                    layoutSearchNotFound.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }

        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
//        adapter.swapCursor(null);
    }

    private void clearPreviousResult() {
        if (proposals!= null && proposals.size() >0) {
            proposals.clear();
        }
    }

    @Override
    public void onProposalSelected(int position, ProposalListModel model) {
        long id = model.getId();
        String noProposalTab = model.getNoProposalTab();
        Intent intent = new Intent(getContext(), P_MainActivity.class);
        intent.putExtra(Const.INTENT_KEY_SL_TAB_ID, id);
        intent.putExtra(Const.INTENT_KEY_NO_PROPOSAL_TAB, noProposalTab);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.floatingActionButton :
                Intent intent = new Intent(getActivity(), P_MainActivity.class);
                intent.putExtra(Const.INTENT_KEY_ADD_PROPOSAL_MODE, true);
                intent.putExtra(Const.INTENT_KEY_SL_TAB_ID, 0);
                startActivity(intent);
                break;
        }
    }
}

