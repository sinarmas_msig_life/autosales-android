package id.co.ajsmsig.nb.crm.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import id.co.ajsmsig.nb.R;

/**
  Created by faiz_f on 03/02/2017.
 */

public class C_DetailLeadFragment extends Fragment {

    public C_DetailLeadFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.c_fragment_detail_lead, container, false);
        ButterKnife.bind(this, view);



        return view;
    }

}
