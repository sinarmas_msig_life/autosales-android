package id.co.ajsmsig.nb.pointofsale.model.db.dynamic

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
class Analytics(
        @PrimaryKey(autoGenerate = true)
        var id: Int = 0,

        @SerializedName("AgentName")
        var agentName: String? = null,

        @SerializedName("AgentCode")
        var agentCode: String? = null,

        @SerializedName("Group")
        var group: Int? = null,

        @SerializedName("Event")
        var event: String? = null,

        @SerializedName("Timestamp")
        var timestamp: Long? = null,

        var isSent: Boolean = false
)